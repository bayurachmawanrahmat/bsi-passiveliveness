//
//  DBHelper.m
//  BSM-Mobile
//
//  Created by Alikhsan on 10/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "DBHelper.h"


/** File name of the SQLite database. */
static NSString * const kDBFileName = @"BSMLocal.db";

@interface DBHelper()
@property (nonatomic) NSString *dbFilePath;
@end

@implementation DBHelper

/**
 * Initialize the instance.
 *
 * @return Instance.
 */
- (instancetype)init {
    self = [super init];
    if (self) {
        self.dbFilePath = [self databaseFilePath];
        
        // For debug
        NSLog(@"%@", self.dbFilePath);
    }
    
    return  self;
}

- (instancetype)initWithDBFilePath:(NSString *)path {
    self = [super init];
    if (self) {
        self.dbFilePath = path;
        
        // For debug
        NSLog(@"%@", self.dbFilePath);
    }
    
    return self;
}

/**
 * Get the path of database file.
 *
 * @return file path.
 */
- (NSString *)databaseFilePath {
    NSArray*  paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES );
    NSString* dir   = [paths objectAtIndex:0];
    
    return [dir stringByAppendingPathComponent:kDBFileName];
}


/**
 * Get the database connection.
 *
 * @return Connection.
 */
- (FMDatabase *)connection {
    FMDatabase* db = [FMDatabase databaseWithPath:self.dbFilePath];
    return ([db open] ? db : nil);
}


@end
