//
//  DBHelper.h
//  BSM-Mobile
//
//  Created by Alikhsan on 10/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <fmdb/FMDatabase.h>


@interface DBHelper : NSObject

- (FMDatabase *)connection;
- (NSString *)databaseFilePath;
- (instancetype)initWithDBFilePath:(NSString *)path;


@end


