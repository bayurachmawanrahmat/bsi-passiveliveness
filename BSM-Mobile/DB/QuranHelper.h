//
//  QuranHelper.h
//  BSM-Mobile
//
//  Created by BSM on 9/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <fmdb/FMResultSet.h>
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "DBHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface QuranHelper : NSObject


-(NSArray *) readListSurah : (NSInteger)mType;
-(NSArray *) readListAyat : (NSString *)mId;
-(NSArray *) readListSurahFilter : (NSString *) mStrText
                             type: (NSInteger)mTpe;



@end

NS_ASSUME_NONNULL_END
