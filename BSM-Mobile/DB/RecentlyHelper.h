//
//  RecentlyHelper.h
//  BSM-Mobile
//
//  Created by Alikhsan on 02/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecentlyHelper : NSObject

-(NSArray *) readListRecentlyUse : (NSString *) menuId mCode : (NSString *) nCode;
-(void)insertRecentlyUse : (NSDictionary *) contentValue;
-(NSArray *) readListRecentlyTransfer : (NSString *) menuId1 menuId2 : (NSString *) menuId2;

@end

NS_ASSUME_NONNULL_END
