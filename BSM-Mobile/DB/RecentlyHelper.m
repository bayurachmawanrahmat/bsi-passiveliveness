//
//  RecentlyHelper.m
//  BSM-Mobile
//
//  Created by Alikhsan on 02/10/19.
//  Edited by Angger Binuko on 23/07/21.
//  Copyright © 2019 lds. All rights reserved.
//

#import "RecentlyHelper.h"
#import "DBHelper.h"
#import "AESCipher.h"
#import <fmdb/FMResultSet.h>
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "NSUserdefaultsAes.h"

static NSString * const kTblRecently = @"tbl_recently";

@interface RecentlyHelper()
@property (nonatomic) FMDatabase *db;
@property (nonatomic) DBHelper *dbHelper;
@property (nonatomic) AESCipher *aesChiper;
@property (nonatomic) NSUserDefaults *userDefault;
@end

@implementation RecentlyHelper

-(void) openConn{
    self.dbHelper = [[DBHelper alloc] init];
    self.aesChiper = [[AESCipher alloc] init];
    self.db = self.dbHelper.connection;
    self.userDefault = [NSUserDefaults standardUserDefaults];
}

-(NSArray *) readListRecentlyUse : (NSString *) menuId
                           mCode : (NSString *) nCode{
    NSArray *dataList = [[NSArray alloc]init];
    [self openConn];
    if (self.db != nil) {
//        NSString *acctId = [self.userDefault valueForKey:@"customer_id"];
        NSString *acctId = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT * FROM %@ "
                           @"WHERE %@ = ? AND %@ =? "
                           @"AND %@ = ? "
                           @"ORDER BY %@ DESC "
                           @"LIMIT 5 ", T_RECENTLY_USE, F_ACCT_ID, F_MENU_ID, F_CODE, F_DATE];
        FMResultSet *rs = [self.db executeQuery:query, [_aesChiper aesEncryptString:acctId], [_aesChiper aesEncryptString:menuId], [_aesChiper aesEncryptString:nCode]];
        NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
        while ([rs next]) {
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc]init];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:F_ACCT_REK]] forKey:F_ACCT_REK];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:F_ACCT_TRSCT_NAME]] forKey:F_ACCT_TRSCT_NAME];
            [dataMutArr addObject:dataDict];
        }
        dataList = (NSArray *) dataMutArr;
        NSLog(@"%@",dataList);
        [self.db closeOpenResultSets];
        
    }
    return dataList;
}

-(void)insertRecentlyUse : (NSDictionary *) contentValue{
    [self openConn];
    if (self.db != nil) {
//        NSString *acctId = [self.userDefault valueForKey:@"customer_id"];
        NSString *acctId = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        NSDictionary *contentValEncrypt = [_aesChiper dictAesEncryptString:contentValue];
        NSString *query = @"";
        
        query = [NSString stringWithFormat:@""
                 @"DELETE FROM %@ "
                 @"WHERE %@ = ? AND %@ = ? AND %@ = ? AND %@ = ?", T_RECENTLY_USE, F_ACCT_ID, F_MENU_ID, F_ACCT_REK, F_CODE];
        [self.db executeUpdate:query, [_aesChiper aesEncryptString:acctId],
         [contentValEncrypt valueForKey:F_MENU_ID],
         [contentValEncrypt valueForKey:F_ACCT_REK],
         [contentValEncrypt valueForKey:F_CODE]];
        
        query = [NSString stringWithFormat:@""
                 @"INSERT INTO %@ (%@,%@,%@,%@,%@,%@) "
                 @"VALUES (?,?,?,?,?,?) ", T_RECENTLY_USE, F_MENU_ID, F_ACCT_ID, F_ACCT_REK, F_ACCT_TRSCT_NAME, F_DATE,F_CODE];
        bool result = [self.db executeUpdate:query,[contentValEncrypt valueForKey:F_MENU_ID],
         [_aesChiper aesEncryptString:acctId],
         [contentValEncrypt valueForKey:F_ACCT_REK],
         [contentValEncrypt valueForKey:F_ACCT_TRSCT_NAME],
         [contentValue valueForKey:F_DATE],
         [contentValEncrypt valueForKey:F_CODE]];
        if (result) {
            NSLog(@"%d",(int) [self.db lastInsertRowId]);
        }else{
            NSLog(@"Gagal Insert");
        }
        [self.db close];
    }
}

-(NSArray *) readListRecentlyTransfer : (NSString *) menuId1 menuId2 : (NSString *) menuId2{
    NSArray *dataList = [[NSArray alloc]init];
    [self openConn];
    if (self.db != nil) {
        NSString *acctId = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT * FROM %@ "
                           @"WHERE %@ = ? AND (%@ =? OR %@ = ?) "
                           @"ORDER BY %@ DESC "
                           @"LIMIT 10 ", T_RECENTLY_USE, F_ACCT_ID, F_MENU_ID, F_MENU_ID, F_DATE];
        FMResultSet *rs = [self.db executeQuery:query, [_aesChiper aesEncryptString:acctId], [_aesChiper aesEncryptString:menuId1], [_aesChiper aesEncryptString:menuId2]];
        NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
        while ([rs next]) {
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc]init];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:F_MENU_ID]] forKey:F_MENU_ID];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:F_ACCT_REK]] forKey:F_ACCT_REK];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:F_ACCT_TRSCT_NAME]] forKey:F_ACCT_TRSCT_NAME];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:F_CODE]] forKey:F_CODE];
            [dataMutArr addObject:dataDict];
        }
        dataList = (NSArray *) dataMutArr;
        NSLog(@"%@",dataList);
        [self.db closeOpenResultSets];
        
    }
    return dataList;
}

@end
