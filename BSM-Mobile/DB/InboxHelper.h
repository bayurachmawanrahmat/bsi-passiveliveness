//
//  InboxHelper.h
//  BSM-Mobile
//
//  Created by Alikhsan on 10/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBHelper.h"

@interface InboxHelper : NSObject

-(void)dropTblInbox;
-(void) createTblInbox;
-(void) createTbl;

-(void) insertDataInbox: (NSDictionary *) contentValue;
-(void) insertDataToken : (NSDictionary *) contentValue;

-(NSArray *) listDataInbox;
-(NSArray *) listAllDataInbox;
-(NSDictionary *) readDataInbox : (NSString *) transactionId
                          trxrf : (NSString *) trxRef;
-(NSString *)readToken : (NSString *) mCid;


-(bool) removeDataInbox :(NSString *) trasactionId
                  trxRef: (NSString *) trxRef;

-(NSInteger) readMarked;
-(bool) removeMultiDataInbox : (NSArray *) arrTransactId
                       trxRef: (NSArray *) arrTrxRef;
-(void) insertDataInboxKeychain: (NSDictionary *) contentValue;

-(NSInteger) inboxCount;

@end
