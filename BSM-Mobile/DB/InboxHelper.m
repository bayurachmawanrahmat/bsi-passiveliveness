//
//  InboxHelper.m
//  BSM-Mobile
//
//  Created by Alikhsan on 10/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "InboxHelper.h"
#import <fmdb/FMResultSet.h>
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "NSString+MD5.h"
#import "AESCipher.h"


static NSString * const kTblInbox = @"tbl_inbox";
static NSString * const kTblToken = @"tbl_token";


@interface InboxHelper()

@property (nonatomic) FMDatabase *db;
@property (nonatomic) DBHelper *dbHelper;
@property (nonatomic) AESCipher *aesChiper;

@end

@implementation InboxHelper

-(void) openConn{
    self.dbHelper = [[DBHelper alloc] init];
    self.aesChiper = [[AESCipher alloc] init];
    self.db = self.dbHelper.connection;
}

-(void)dropTblInbox{
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"DROP TABLE IF EXISTS %@", kTblInbox];
        [self.db executeUpdate:query];
        [self.db close];
    }
}

-(void) createTblInbox{
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"CREATE TABLE IF NOT EXISTS %@ "
                           @"( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                           @"trxref TEXT, "
                           @"title TEXT, "
                           @"msg TEXT, "
                           @"footer_msg TEXT, "
                           @"transaction_id TEXT, "
                           @"template TEXT, "
                           @"jenis TEXT, "
                           @"time TEXT, "
                           @"marked TEXT )", kTblInbox];
        
        [self.db executeUpdate:query];
        [self.db close];
    }
}

-(void) createTbl{
    [self openConn];
    if (self.db != nil) {
        NSString *createTblInbox, *createTblToken, *createTblRecently;
        createTblInbox = [NSString stringWithFormat:@""
                          @"CREATE TABLE IF NOT EXISTS %@ "
                          @"( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                          @"trxref TEXT, "
                          @"title TEXT, "
                          @"msg TEXT, "
                          @"footer_msg TEXT, "
                          @"transaction_id TEXT, "
                          @"template TEXT, "
                          @"jenis TEXT, "
                          @"time TEXT, "
                          @"marked TEXT )", kTblInbox];
        createTblToken = [NSString stringWithFormat:@""
                          @"CREATE TABLE IF NOT EXISTS %@ "
                          @"( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                          @"imei TEXT, "
                          @"token TEXT )", kTblToken];
        
        createTblRecently = [NSString stringWithFormat:@""
                             @"CREATE TABLE IF NOT EXISTS %@ "
                             @"( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                             @"%@ TEXT, "
                             @"%@ TEXT, "
                             @"%@ TEXT, "
                             @"%@ TEXT, "
                             @"%@ TEXT, "
                             @"%@ TEXT )", T_RECENTLY_USE, F_MENU_ID, F_ACCT_ID, F_ACCT_REK, F_ACCT_TRSCT_NAME, F_DATE, F_CODE];
        
        
        
        NSString *query = [NSString stringWithFormat:@"%@; %@; %@;", createTblInbox, createTblToken, createTblRecently];
        NSLog(@"%@", query);
        [self.db executeStatements:query];
        [self.db close];
        
    }
}
-(void) insertDataInboxKeychain: contentValue{
    [self openConn];
    NSString *query = [NSString stringWithFormat:@""
                       @"INSERT OR REPLACE INTO %@ "
                       @"( trxref, title, msg, footer_msg, transaction_id, template, "
                       @"jenis, time, marked ) "
                       @"VALUES (?,?,?,?,?,?,?,?,?)", kTblInbox];
    if (self.db != NULL) {
        NSLog(@"%@", contentValue);
        NSArray *arrLastBackup = [contentValue valueForKey:@"data"];
        for(int i=0; i< arrLastBackup.count; i++){
            NSDictionary *data = [[arrLastBackup objectAtIndex:i]valueForKey:@"content"];
            NSLog(@"%d : %@",i,data);
            bool result = [self.db executeUpdate:query,
                           [data valueForKey:@"trxref"],
                           [data valueForKey:@"title"],
                           [data valueForKey:@"msg"],
                           [data valueForKey:@"footer_msg"],
                           [data valueForKey:@"transaction_id"],
                           [data valueForKey:@"template"],
                           [data valueForKey:@"jenis"],
                           [data valueForKey:@"time"],
                           [data valueForKey:@"marked"]];
            
            if (result) {
                NSLog(@"%d",(int) [self.db lastInsertRowId]);
            }else{
                NSLog(@"Gagal Insert");
            }
        }
         [self.db close];
    }
}

-(void) insertDataInbox: (NSDictionary *) contentValue{
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"INSERT OR REPLACE INTO %@ "
                           @"( trxref, title, msg, footer_msg, transaction_id, template, "
                           @"jenis, time, marked ) "
                           @"VALUES (?,?,?,?,?,?,?,?,?)", kTblInbox];
        NSDictionary *dictEncrypt = [_aesChiper dictAesEncryptString:contentValue];
        NSLog(@"enkripsi: %@", dictEncrypt);
        bool result = [self.db executeUpdate:query,
                       [dictEncrypt valueForKey:@"trxref"],
                       [dictEncrypt valueForKey:@"title"],
                       [dictEncrypt valueForKey:@"msg"],
                       [dictEncrypt valueForKey:@"footer_msg"],
                       [dictEncrypt valueForKey:@"transaction_id"],
                       [dictEncrypt valueForKey:@"template"],
                       [dictEncrypt valueForKey:@"jenis"],
                       [dictEncrypt valueForKey:@"time"],
                       [dictEncrypt valueForKey:@"marked"]];
        
        if (result) {
            NSLog(@"%d",(int) [self.db lastInsertRowId]);
        }else{
            NSLog(@"Gagal Insert");
        }
        [self.db close];
        
    }
}


-(void) insertDataToken : (NSDictionary *) contentValue{
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"INSERT OR REPLACE INTO %@ "
                           @"( imei, token )"
                           @"VALUES (?,?)", kTblToken];
        NSDictionary *dictEncrypt = [_aesChiper dictAesEncryptString:contentValue];
        NSLog(@"%@", dictEncrypt);
        bool result = [self.db executeUpdate:query,
                       [dictEncrypt valueForKey:@"imei"],
                       [dictEncrypt valueForKey:@"token"]];
        if (result) {
            NSLog(@"%d",(int) [self.db lastInsertRowId]);
        }else{
            NSLog(@"Gagal Insert");
        }
        [self.db close];
        
    }
}

-(NSArray *) listAllDataInbox{
    NSArray *dataArr = [[NSArray alloc] init];
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT transaction_id, title, msg, footer_msg, trxref, template, jenis, time, marked "
//                           @"FROM %@ ",kTblInbox];
                           @"FROM %@ "
//                           @"WHERE jenis = ? "
//                           @"OR jenis = ? "
                           @"ORDER BY id DESC LIMIT 100", kTblInbox]; //LIMIT 100 added in 5.23
//        FMResultSet *rs = [self.db executeQuery:query,[_aesChiper aesEncryptString:@"General"], [_aesChiper aesEncryptString:@"Qr Pay"]];
        FMResultSet *rs = [self.db executeQuery:query];
        NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
        while ([rs next]) {
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"transaction_id"]] forKey:@"transaction_id"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"trxref"]] forKey:@"trxref"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"title"]] forKey:@"title"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"msg"]] forKey:@"msg"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"footer_msg"]] forKey:@"footer_msg"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"template"]] forKey:@"template"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"jenis"]] forKey:@"jenis"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"time"]] forKey:@"time"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"marked"]] forKey:@"marked"];
            [dataMutArr addObject:dataDict];
        }
        dataArr = (NSArray *) dataMutArr;
        [self.db closeOpenResultSets];
    }
    return dataArr;
}

-(NSArray *) listDataInbox{
    NSArray *dataArr = [[NSArray alloc] init];
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT transaction_id, title, trxref, template, time, marked "
                           //                           @"FROM %@ ",kTblInbox];
                           @"FROM %@ "
                           //                           @"WHERE jenis = ? "
                           //                           @"OR jenis = ? "
                           @"ORDER BY id DESC LIMIT 100", kTblInbox]; //LIMIT 100 Added in 5.23
        //        FMResultSet *rs = [self.db executeQuery:query,[_aesChiper aesEncryptString:@"General"], [_aesChiper aesEncryptString:@"Qr Pay"]];
        FMResultSet *rs = [self.db executeQuery:query];
        NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
        while ([rs next]) {
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"transaction_id"]] forKey:@"transaction_id"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"trxref"]] forKey:@"trxref"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"title"]] forKey:@"title"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"template"]] forKey:@"template"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"time"]] forKey:@"time"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"marked"]] forKey:@"marked"];
            [dataMutArr addObject:dataDict];
        }
        dataArr = (NSArray *) dataMutArr;
        [self.db closeOpenResultSets];
    }
    return dataArr;
}

-(NSDictionary *) readDataInbox : (NSString *) transactionId
                          trxrf : (NSString *) trxRef
{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc]init];
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"UPDATE %@ "
                           @"SET marked = ? "
                           @"WHERE transaction_id = ? "
                           @"AND trxref = ? ", kTblInbox];
        
        bool state = [self.db executeUpdate:query, [_aesChiper aesEncryptString:@"1"], [_aesChiper aesEncryptString:transactionId], [_aesChiper aesEncryptString:trxRef]];
        if(state){
             NSString *query = [NSString stringWithFormat:@"SELECT title, msg, footer_msg, trxref, time FROM %@ "
                                @"WHERE transaction_id = ? "
                                @"AND trxref = ? LIMIT 100", kTblInbox]; // LIMIT 100 added in 5.23
            FMResultSet *rs = [self.db executeQuery:query, [_aesChiper aesEncryptString:transactionId], [_aesChiper aesEncryptString:trxRef]];
            if ([rs next]) {
                [dataDict setValue:[rs stringForColumn:@"title"] forKey:@"title"];
                [dataDict setValue:[rs stringForColumn:@"msg"] forKey:@"msg"];
                [dataDict setValue:[rs stringForColumn:@"footer_msg"] forKey:@"footer_msg"];
                [dataDict setValue:[rs stringForColumn:@"trxref"] forKey:@"trxref"];
                [dataDict setValue:[rs stringForColumn:@"time"] forKey:@"time"];
            }
        }
        [self.db closeOpenResultSets];
    }
   
return [_aesChiper dictAesDecryptString:dataDict];
    
}
-(NSString *)readToken : (NSString *) mCid{
    NSString *token = nil;
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT token FROM %@ "
                           @"WHERE imei = ?", kTblToken];
        FMResultSet *rs = [self.db executeQuery:query, [_aesChiper aesEncryptString:mCid]];
        if ([rs next]) {
            token = [_aesChiper aesDecryptString : [rs stringForColumn:@"token"]];
        }
        
    }
    return token;
}

-(bool) removeDataInbox :(NSString *) trasactionId
                  trxRef: (NSString *) trxRef{
    bool state = false;
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"DELETE FROM %@ "
                           @"WHERE transaction_id = ? "
                           @"AND trxref = ? ", kTblInbox];
        state = [self.db executeUpdate:query, [_aesChiper aesEncryptString:trasactionId], [_aesChiper aesEncryptString:trxRef]];
        [self.db close];
    }
    return state;
}

-(NSInteger) readMarked{
    NSInteger count = 0;
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT COUNT (marked) FROM %@ "
                           @"WHERE marked = ?", kTblInbox];
         FMResultSet *rs = [self.db executeQuery:query, [_aesChiper aesEncryptString:@"0"]];
        if([rs next]){
            count = [rs intForColumnIndex:0];
        }
    }
    return count;
}

-(bool) removeMultiDataInbox : (NSArray *) arrTransactId
                       trxRef: (NSArray *) arrTrxRef{
    bool state = false;
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"DELETE FROM %@ "
                           @"WHERE (transaction_id IN %@) "
                           @"AND (trxref IN %@) ", kTblInbox, arrTransactId, arrTrxRef];
        state = [self.db executeUpdate:query];
        [self.db close];
    }
    return state;
}

-(NSInteger) inboxCount{
    NSInteger count = 0;
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT COUNT (id) FROM %@ ", kTblInbox];
        FMResultSet *rs = [self.db executeQuery:query];
        if([rs next]){
            count = [rs intForColumnIndex:0];
        }
    }
    return count;
}



@end
