//
//  QuranHelper.m
//  BSM-Mobile
//
//  Created by BSM on 9/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "QuranHelper.h"

static NSString * const kTblQuranAyat = @"quran_ayah";
static NSString * const kTblQuranSurah = @"quran_surah";
static NSString * const kDBQuran = @"quran_best_sample_indo.db";


@interface QuranHelper()
@property (nonatomic) FMDatabase *db;
@property (nonatomic) DBHelper *dbHelper;

@end


@implementation QuranHelper

-(void) openConn{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"quran_best_sample_indo" ofType:@"db"];
    self.dbHelper = [[DBHelper alloc] initWithDBFilePath:filePath];
    self.db = self.dbHelper.connection;
}

-(NSArray *) readListSurah : (NSInteger)mType{
    NSArray *dataArr = [[NSArray alloc] init];
       [self openConn];
    if (self.db != nil) {
        
        NSString *query =@"";
        if (mType == 1) {
             query = [NSString stringWithFormat:@"SELECT * FROM %@ "
                      @"WHERE id == 1 OR id >= 78",kTblQuranSurah];
        }else{
            query = [NSString stringWithFormat:@"SELECT * FROM %@ ",kTblQuranSurah];
        }
        FMResultSet *rs = [self.db executeQuery:query];
        NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
        while ([rs next]) {
            NSMutableDictionary *dataSurah = [[NSMutableDictionary alloc]init];
            [dataSurah setValue:[rs stringForColumn:@"id"] forKey:@"id"];
            [dataSurah setValue:[rs stringForColumn:@"surah_name"] forKey:@"surah_name"];
            [dataSurah setValue:[rs stringForColumn:@"arabic"] forKey:@"arabic"];
            [dataSurah setValue:[rs stringForColumn:@"translate"] forKey:@"translate"];
            [dataSurah setValue:[rs stringForColumn:@"location"] forKey:@"location"];
            [dataSurah setValue:[rs stringForColumn:@"num_ayah"] forKey:@"num_ayah"];
            [dataMutArr addObject:dataSurah];
            
        }
        dataArr = (NSArray *) dataMutArr;
        [self.db closeOpenResultSets];
    }
    return dataArr;
}

-(NSArray *) readListSurahFilter : (NSString *) mStrText
                             type:(NSInteger)mTpe{
    NSArray *dataArr = [[NSArray alloc] init];
          [self openConn];
       if (self.db != nil) {
           NSString *query = @"";
           if (mTpe == 1) {
               query =[NSString stringWithFormat:@""
               @"SELECT * FROM %@ "
               @"WHERE (id >= 78 OR id == 1) AND "
               @"(id LIKE '%%%@%%' OR "
               @"surah_name LIKE '%%%@%%' OR "
               @"translate LIKE '%%%@%%' OR "
               @"num_ayah LIKE '%%%@%%' )", kTblQuranSurah, mStrText, mStrText, mStrText, mStrText];
           }else{
               query =[NSString stringWithFormat:@""
               @"SELECT * FROM %@ "
               @"(id LIKE '%%%@%%' OR "
               @"surah_name LIKE '%%%@%%' OR "
               @"translate LIKE '%%%@%%' OR "
               @"num_ayah LIKE '%%%@%%' )", kTblQuranSurah, mStrText, mStrText, mStrText, mStrText];
           }
           
           FMResultSet *rs = [self.db executeQuery:query];
           NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
           while ([rs next]) {
               NSMutableDictionary *dataSurah = [[NSMutableDictionary alloc]init];
               [dataSurah setValue:[rs stringForColumn:@"id"] forKey:@"id"];
               [dataSurah setValue:[rs stringForColumn:@"surah_name"] forKey:@"surah_name"];
               [dataSurah setValue:[rs stringForColumn:@"arabic"] forKey:@"arabic"];
               [dataSurah setValue:[rs stringForColumn:@"translate"] forKey:@"translate"];
               [dataSurah setValue:[rs stringForColumn:@"location"] forKey:@"location"];
               [dataSurah setValue:[rs stringForColumn:@"num_ayah"] forKey:@"num_ayah"];
               [dataMutArr addObject:dataSurah];
               
           }
           dataArr = (NSArray *) dataMutArr;
           [self.db closeOpenResultSets];
       }
    return dataArr;
}

-(NSArray *) readListAyat : (NSString *) mId{
    NSArray *dataArr = [[NSArray alloc] init];
       [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@" "
                           @"SELECT surah, ayah, arabic, translate, page, juz FROM %@ "
                           @"WHERE surah = %@ ",kTblQuranAyat, mId];
        FMResultSet *rs = [self.db executeQuery:query];
        NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
        while ([rs next]) {
        NSMutableDictionary *dataSurah = [[NSMutableDictionary alloc]init];
            [dataSurah setValue:[rs stringForColumn:@"surah"] forKey:@"surah"];
            [dataSurah setValue:[rs stringForColumn:@"ayah"] forKey:@"ayah"];
            [dataSurah setValue:[rs stringForColumn:@"arabic"] forKey:@"arabic"];
            [dataSurah setValue:[rs stringForColumn:@"translate"] forKey:@"translate"];
            [dataSurah setValue:[rs stringForColumn:@"page"] forKey:@"page"];
            [dataSurah setValue:[rs stringForColumn:@"juz"] forKey:@"juz"];
            [dataMutArr addObject:dataSurah];
        }
        dataArr = (NSArray *) dataMutArr;
        [self.db closeOpenResultSets];
    }
    return dataArr;
}

@end
