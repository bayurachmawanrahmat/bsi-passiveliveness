//
//  main.m
//  BSM-Mobile
//
//  Created by lds on 6/27/14.
//  Copyright (c) 2014 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "TIMERUIApplication.h"

NSString *lv_selected = @"";

int main(int argc, char * argv[])
{
    @autoreleasepool {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if(![defaults objectForKey:@"development"]){
            
            [defaults setObject:[NSNumber numberWithBool:true] forKey:@"development"];
            NSMutableArray *data = [[NSMutableArray alloc]initWithObjects:[[NSMutableArray alloc]init],[[NSMutableArray alloc]init],[[NSMutableArray alloc]init],nil];
            [defaults setObject:data forKey:@"favorite"];
//            NSArray *languages = [NSArray arrayWithObject:@"id"];
//            [defaults setObject:languages forKey:@"AppleLanguages"];
//            [defaults synchronize];
        }
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
//        return UIApplicationMain(argc, argv, NSStringFromClass([TIMERUIApplication class]), NSStringFromClass([AppDelegate class]));
    }
}
