//
//  DataManager.m
//  BSM Mobile
//
//  Created by lds on 5/13/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "DataManager.h"
#import "Utility.h"

@implementation DataManager{
}

+ (id)sharedManager {
    static DataManager *g_instance = nil;
    if (g_instance == nil) {
        g_instance = [[self alloc] init];
        [g_instance setDataExtra:[[NSMutableDictionary alloc]init]];
    }
    return g_instance;
}

- (id)getJSONData:(int)position{
    return [self.listMenu objectAtIndex:position];
}

- (void)setAction:(NSDictionary *)data andMenuId:(NSString *)menuId{
    if(menuId == nil){
        menuId = self.menuId;
    }
    NSMutableArray *tempList = [[NSMutableArray alloc]init];
    for(int i = 0; i< [[data objectForKey:@"node"]count]; i++){
        NSMutableDictionary *dataDict = [[NSMutableDictionary alloc]init];
        [dataDict addEntriesFromDictionary:@{@"content":[[data valueForKey:@"content"]objectAtIndex:i],@"favorite":[[data valueForKey:@"favorite"]objectAtIndex:i],@"social":[[data valueForKey:@"social"]objectAtIndex:i],@"template":[[data valueForKey:@"template"]objectAtIndex:i],@"title":[[data valueForKey:@"title"]objectAtIndex:i],@"url":[[data valueForKey:@"url"]objectAtIndex:i],@"url_parm":[[data valueForKey:@"url_parm"]objectAtIndex:i],@"node":[[data valueForKey:@"node"]objectAtIndex:i],@"activation":[[data valueForKey:@"activation"]objectAtIndex:i],@"field_name":[[data valueForKey:@"field_name"]objectAtIndex:i],@"mandatory":[[data valueForKey:@"mandatory"]objectAtIndex:i],@"menu_id":menuId, @"list":[[NSArray alloc]init]}];
        [tempList addObject:dataDict];
    }
    self.listAction = tempList;
    self.currentPosition = -1;
}

- (void)updateObjectDataWitList:(NSArray *)list{
    [[self.listAction objectAtIndex:self.currentPosition]setObject:list forKey:@"list"];
}

- (id)getObjectData{
    return [self getObjectDataAt:self.currentPosition];
}

- (id)getObjectDataAt:(int)position{
    if(self.listAction && position < self.listAction.count){
        return [self.listAction objectAtIndex:position];
    }
    return nil;
}

- (id)getJSONByCurrentMenu{
        for(id dict in self.listMenu){
            for(id action in [[dict objectAtIndex:1]objectForKey:@"action"]){
                NSLog(@"%@", self.menuId);
                if([[action objectAtIndex:0]isEqualToString:self.menuId]){
                    return action;
                }
            }
        }
    return nil;
    
}

- (id)getJsonDataByMenuID{
    for(id dict in self.listMenu){
        if([[dict objectAtIndex:0]isEqualToString:self.menuId]){
            return dict;
        }
    }
    return nil;
}


- (id)getJsonDataWithMenuID : (NSString *)menuID andList : (NSArray*) list{
    if([list isKindOfClass:[NSArray class]]){
        for(id dict in list){
            if([[dict objectAtIndex:0]isEqualToString:menuID]){
                return dict;
            }
            if([[[dict objectAtIndex:1]objectForKey:@"action"] isKindOfClass:[NSArray class]]){
                if([self getJsonDataWithMenuID:menuID andList:[[dict objectAtIndex:1]objectForKey:@"action"]] != nil){
                    return [self getJsonDataWithMenuID:menuID andList:[[dict objectAtIndex:1]objectForKey:@"action"]];
                }
            }
        }
    }
    return nil;
}

- (NSDictionary*)getArrayofActionMenuID : (NSString *)menuID andList : (NSArray*) list{
    if([list isKindOfClass:[NSArray class]]){
        for(id dict in list){
            if([[dict objectAtIndex:0]isEqualToString:menuID]){
                return [[dict objectAtIndex:1]objectForKey:@"action"];
            }
            if([[[dict objectAtIndex:1]objectForKey:@"action"] isKindOfClass:[NSArray class]]){
                if([self getJsonDataWithMenuID:menuID andList:[[dict objectAtIndex:1]objectForKey:@"action"]] != nil){
                    return [self getJsonDataWithMenuID:menuID andList:[[dict objectAtIndex:1]objectForKey:@"action"]];
                }
            }
        }
    }
    return nil;
}

- (void)resetObjectData{
    self.selectedData = nil;
    self.pinNumber = nil;
    self.currentPosition = 0;
    self.listAction = nil;
//    self.txt1 = nil;
//    self.txt2 = nil;
//    self.idMerchant = nil;
//    self.idAccount = nil;
    self.menuId = nil;
//    self.code = nil;
//    self.transactionId = nil;
//    self.idDenom = nil;
//    self.typeBank = nil;
    self.favData = nil;
    self.favData = [[NSMutableDictionary alloc]init];
    self.dataExtra = nil;
    self.dataExtra = [[NSMutableDictionary alloc]init];
}

- (NSArray*)getStoryboardList {
    return [AppProperties getArrayStoryboardList];
}

@end
