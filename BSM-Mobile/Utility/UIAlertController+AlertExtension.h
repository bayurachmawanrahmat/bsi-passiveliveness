//
//  UIAlertController+AlertExtension.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 06/12/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIAlertController (AlertExtension)

- (void) addActionWithTitle:(NSString*) title actionWithStyle:(UIAlertActionStyle) actionStyle withTag:(int)index instanceOf:(id)instanceID;

@end

