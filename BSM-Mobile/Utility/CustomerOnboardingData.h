//
//  CustomerOnboardingData.h
//  BSM-Mobile
//
//  Created by ARS on 21/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerOnboardingData : NSObject

@property (nonatomic, strong) NSString * _Nullable nomorAntrian;
@property (nonatomic, strong) dispatch_group_t _Nullable dispatchGroup;
@property (nonatomic, assign) BOOL requestIsSuccess;
@property (nonatomic, assign) NSMutableDictionary * _Nullable responseNasabah;

- (void)generateNomorAntrian;
- (NSMutableDictionary *_Nonnull) getDataToPost;
- (void) sendDataToBackend;

+ (NSMutableDictionary *)keychainItemForKey:(NSString *)key;
+ (OSStatus)setDict:(NSDictionary *)dict forKey:(NSString *)key;
+ (OSStatus)setValue:(NSString *)value forKey:(NSString *)key;
+ (OSStatus)setObject:(NSObject *)obj forKey:(NSString *)key;
+ (NSString *)valueForKeychainKey:(NSString *)key;
+ (NSDictionary *)dictForKeychainKey:(NSString *)key;
+ (NSObject *)objectForKeychainKey:(NSString *)key;
+ (OSStatus)removeForKeychainKey:(NSString *)key;

+ (void)getSynchronousFromEndpoint:(NSString *)endpoint withParam:(nullable NSDictionary *)param returningData:(NSMutableDictionary **)returnData;
+ (void)getFromEndpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData;
+ (void)getFromEndpointFr:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData;
+ (void)callGetFromEndpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData rootUrl:(NSString *)rootUrl;
+ (void)callGetFromEndpointWithCallback:(NSString *)endpoint rootUrl:(NSString *)rootUrl withHandler:(void(^)(NSMutableDictionary*))handler;
+ (void)postToEndPoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData;
+ (void)postToEndPoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData isResEncrypted:(BOOL)isResEncrypted;
+ (void)postToEndPointFr:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData;
+ (void)postToEndPointUrl:(NSString *)url endpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData;
+ (void)callPostToEndPoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData rootUrl:(NSString *)rootUrl;
+ (void)callPostToEndPointDisableEncryptRes:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData rootUrl:(NSString *)rootUrl;
+ (NSString *)getToken;
+ (NSString *)apiUrl;
+ (NSString *)apiFrUrl;
+ (NSDictionary *)getEncryptedData:(NSDictionary *)data;
+ (NSString *)stringParamFromData:(NSDictionary *)data;
+ (NSString *)safeEndpoint:(NSString *)endpoint;

+ (NSDate *) date:(NSDate *)date withHour:(NSInteger)hour minute:(NSInteger)minute;
+ (BOOL)compareDate:(NSDate *)date withinFirst:(NSDate *)first andLast:(NSDate *)last;

+ (void) logUrlRequest:(NSString *)url withHeaders:(nullable NSDictionary *)header startWithString:(NSString *)startString;

@end
