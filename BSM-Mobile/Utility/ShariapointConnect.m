//
//  ShariapointConnect.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "ShariapointConnect.h"
#import "Encryptor.h"
#import "Utility.h"

@implementation ShariapointConnect
{
    Encryptor *encryptor;
    NSHTTPURLResponse *httpResponse;
}

@synthesize delegate = _delegate;


- (id)initWithDelegate:(id)delegate{
    self = [super init];
    if (self){
        self.delegate = delegate;
        encryptor = [[Encryptor alloc]init];
        data = [[NSMutableData alloc]init];
    }
    
    return self;
}

+ (NSString *)apiUrl
{
    return SYARIAHPOINT_API_URL;
}

+ (NSString *)safeEndpoint:(NSString *)endpoint
{
    if (SYARIAHPOINT_SAFE_MODE)
    {
        return [Encryptor HexEncrypt:endpoint];
    }
    return endpoint;
}

- (void)getDataFromEndPoint:(NSString *)endpoint needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted{
    
    endPointRequest = endpoint;
    txtLoading = textLoading;
    isLoading = needLoading;
    isEncrypted = encrypted;
    
    [self setRequest:@"GET"];
}

- (void)sendPostData:(NSString*)params endPoint:(NSString *)endpoint needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted{
    paramsURL = params;
    endPointRequest = endpoint;
    txtLoading = textLoading;
    isLoading = needLoading;
    isEncrypted = encrypted;
    
    [self setRequest:@"POST"];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if(error == nil){
        [self dataProcessing:error];
    }else{
        NSLog(@"%@",[error localizedDescription]);
        NSError *err = [NSError errorWithDomain:@"shariapoint" code:[error code] userInfo:@{NSLocalizedDescriptionKey:[error localizedDescription]}];
        [self.delegate didFinishLoadData:nil withEndpoint:endPointRequest completeWithError:err];
        [self dismissLoading];
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)aData{
    [data appendData:aData];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler{
    httpResponse = (NSHTTPURLResponse*)response;
    completionHandler(NSURLSessionResponseAllow);
}

- (void)setRequest:(NSString*)method{
    NSString *url = [[[self class] apiUrl] stringByAppendingString:[[self class] safeEndpoint:endPointRequest]];
    NSLog(@"SYARIAH POINT SAFE REQUEST TO : %@",url);
    NSLog(@"SYARIAH POINT REQUEST TO : %@",[[[self class] apiUrl] stringByAppendingString:endPointRequest]);
    url = [url stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];

    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"UTF-8" forHTTPHeaderField:@"charset"];
    [urlRequest setValue:@"close" forHTTPHeaderField:@"Connection"];
    [urlRequest setHTTPMethod:method];
    
    if([method isEqualToString:@"POST"]){
        NSString *body = [self generateParamURL];
        [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if(isLoading){
        if(!loadingAlert){
            [self showLoading:txtLoading];
        }
    }
//    [UIApplication sharedApplication].idleTimerDisabled = YES;
    NSURLSessionConfiguration *defaultConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    connection = [NSURLSession sessionWithConfiguration:defaultConfiguration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionTask *dataTask = [connection dataTaskWithRequest:urlRequest];
    [dataTask resume];
    
}

- (NSString *)stringParamFromData:(NSDictionary *)data
{
    NSMutableString *result = [[NSMutableString alloc] init];
    int i = 0;
    for (NSString *key in data.allKeys) {
        i == 0 ? [result appendFormat:@"%@=%@", key, [data objectForKey:key]] : [result appendFormat:@"&%@=%@", key, [data objectForKey:key]];
        i = i + 1;
    }
    return result;
}

- (void) dataProcessing:(NSError*)error{
    if(data != nil){
        NSDictionary *value = [[NSDictionary alloc]init];
        if(SYARIAHPOINT_SAFE_MODE){
            NSString *res = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//            NSString* res = [NSString stringWithUTF8String:[data bytes]];
            NSLog(@"res : %@",res);
            res = [Encryptor AESDecrypt:res];
            NSData *decryptedData = [Encryptor AESDecryptData:res];
            NSLog(@"res ii : %@",res);
            NSLog(@"decryptedData :%@",decryptedData);
            NSLog(@"data :%@",data);
            @try{
                value = [NSJSONSerialization
                                        JSONObjectWithData:[res dataUsingEncoding:NSUTF8StringEncoding]
                                        options:kNilOptions
                                        error:nil];

            }@catch (NSException *exception) {
                NSLog(@"SYARIAH POINT EXCEPTION : %@", exception);
            }
            
        }else{
            @try{
                value = [NSJSONSerialization
                                        JSONObjectWithData:data
                                        options:kNilOptions
                                        error:nil];
            }@catch (NSException *exception) {
                NSLog(@"SYARIAH POINT EXCEPTION : %@", exception);
            }
        }

        NSLog(@"RESPONSE SYAIRA POINT : %@", value);
        NSLog(@"Status Code %ld:",(long)[httpResponse statusCode]);
        
        if(value != nil && [value count] > 0){
            if(error){
                NSError *err = [NSError errorWithDomain:@"shariapoint" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:[value valueForKey:@"message"]}];
                [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:err];
            }
            
            if((long)[httpResponse statusCode] == 200){
                
                if([endPointRequest containsString:@"?"]){
                    endPointRequest = [endPointRequest componentsSeparatedByString:@"?"][0];
                }
                [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:error];
                
            }else{
                
                NSError *err;
                if([value objectForKey:@"message"] != nil){
                    if([[value valueForKey:@"status"]doubleValue] == -10){
                        NSDictionary *dictMessage = [value objectForKey:@"message"];
                        if([Utility isLanguageID]){
                            err = [NSError errorWithDomain:@"shariapoint" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:[dictMessage valueForKey:@"id"]}];
                        }else{
                            err = [NSError errorWithDomain:@"shariapoint" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:[dictMessage valueForKey:@"en"]}];
                        }

                    }else{
                        err = [NSError errorWithDomain:@"shariapoint" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:[value valueForKey:@"message"]}];
                    }
                }else{
                    err = [NSError errorWithDomain:@"shariapoint" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:lang(@"ERROR_REQUEST")}];
                }
                [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:err];
            }
        }else{
            NSError *err = [NSError errorWithDomain:@"shariapoint" code:404 userInfo:@{NSLocalizedDescriptionKey:lang(@"ERROR_REQUEST")}];
            [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:err];
        }
    }else{
        NSError *err = [NSError errorWithDomain:@"shariapoint" code:404 userInfo:@{NSLocalizedDescriptionKey:@"ERROR_REQUEST"}];
        [self.delegate didFinishLoadData:nil withEndpoint:endPointRequest completeWithError:err];
    }
    
    [self dismissLoading];
}

- (void)showLoading:(NSString *)message{
    
    loadingAlert = [[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];

    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
    [viewLoading setBackgroundColor:[UIColor clearColor]];
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loadingView setColor:[UIColor blackColor]];
    [loadingView startAnimating];
    [viewLoading addSubview:loadingView];
    
    [loadingAlert setContentView:viewLoading];
    [loadingAlert show];
}

- (void)dismissLoading{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [loadingAlert  dismissWithClickedButtonIndex:0 animated:YES];
    loadingAlert = nil;
}

- (NSString *)generateParamURL{
            
    DataManager *dataManager = [DataManager sharedManager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
    NSMutableDictionary *dataParams = [Utility translateParam:paramsURL];
    NSLog(@"DATA Parm: %@", dataParams);
    
    NSDictionary *dict = @{@"dataParam":dataParams, @"encrypted":[NSNumber numberWithBool:isEncrypted]};
    
    NSMutableDictionary *dataParam = [dict objectForKey:@"dataParam"];
    
    if([dataParam objectForKey:@"language"]){
        NSArray *listLanguage = [defaults objectForKey:@"AppleLanguages"];
        [dataParam setObject:[listLanguage objectAtIndex:0] forKey:@"language"];
    }
    
    for(NSString *key in dataParam.allKeys){
        if([[dataParam objectForKey:key]isKindOfClass:[NSNumber class]] && ![[dataParam objectForKey:key]boolValue]){
            if([dataManager.dataExtra objectForKey:key]){
                [dataParam setObject:[dataManager.dataExtra valueForKey:key] forKey:key];
            }else{
                NSLog(@"KEY %@ CHANGED TO EMPTY STRING", key);
                [dataParam setObject:@"" forKey:key];
            }
        }
    }
    NSLog(@"DATA TO SENT: %@", dataParam);
//    return  [self encryptedParamValue:dataParam];
    return [self valueFromKey:dataParam];
}

- (NSString *)valueFromKey:(NSDictionary *)dataParam{
    NSString * result = @"";
    for (NSString *key in [dataParam allKeys]){
        if([[dataParam objectForKey:key]isKindOfClass:[NSArray class]]){
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[dataParam objectForKey:key]
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
            if(SYARIAHPOINT_SAFE_MODE){
                NSLog(@"data : %@ || %@ ",[Encryptor HexEncrypt:key], [Encryptor HexEncrypt:jsonString]);
                
                NSString *encKey = [Encryptor HexEncrypt:key];
                encKey = [encKey stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                NSString *encValue = [Encryptor HexEncrypt:jsonString];
                encValue = [encValue stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                
                result = [NSString stringWithFormat:@"%@%@=%@&",result,encKey,encValue];
            }else{
                result = [NSString stringWithFormat:@"%@%@=%@&",result,key, jsonString];
            }
            
        }else{
                
            if(SYARIAHPOINT_SAFE_MODE){
//                if([self exceptionalKeyCheck:key]){
//                    NSString *encKey = [Encryptor HexEncrypt:key];
//                    encKey = [encKey stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
//
//                    result = [NSString stringWithFormat:@"%@%@=%@&",result,encKey, [dataParam valueForKey:key]];
//                }else{
                    NSLog(@"data : %@ || %@ ",[Encryptor HexEncrypt:key], [Encryptor HexEncrypt:[dataParam valueForKey:key]]);
                    
                    NSString *encKey = [Encryptor HexEncrypt:key];
                    encKey = [encKey stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                    NSString *encValue = [Encryptor HexEncrypt:[dataParam valueForKey:key]];
                    encValue = [encValue stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                    
                    result = [NSString stringWithFormat:@"%@%@=%@&",result,encKey,encValue];
//                }
            }else{
                result = [NSString stringWithFormat:@"%@%@=%@&",result,key, [dataParam valueForKey:key]];
            }
            
        }
    }
    
    NSLog(@"%@",result);
    return result;
}


-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding data:(NSString *)oriString{
    NSCharacterSet * queryKVSet = [NSCharacterSet
        characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"].invertedSet;
    
    return (NSString*)[oriString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
}


@end
