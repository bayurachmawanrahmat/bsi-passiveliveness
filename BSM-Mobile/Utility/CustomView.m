//
//  CustomView.m
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (id)initWithFrame:(CGRect)frame
{
    self                            = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (self) {
        
        self.backgroundColor        = [UIColor lightGrayColor];
        //[self threeButtons];
    }
    return self;
}

//- (void)threeButtons {
//
//    int     buttonCount                 = 3;
//    int     space                       = 5;
//    float   buttonSize                  = 60;
//
//    int     initialOffset               = 60;
//
//    CGFloat horizontallyCentred         = ([UIScreen mainScreen].bounds.size.width - buttonSize) / 2;
//
//    for (int i = 1; i <= buttonCount; i++) {
//
//        CGFloat x                       = horizontallyCentred;
//        CGFloat y                       = initialOffset + i * (buttonSize + space);
//        CGFloat wide                    = buttonSize;
//        CGFloat high                    = buttonSize;
//        UIButton *buttonInView          = [[UIButton alloc] initWithFrame:CGRectMake(x, y, wide, high)];
//
//        [buttonInView setTag:i];
//        [buttonInView addTarget:self.delegate action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
//
//        buttonInView.layer.borderWidth  = 0.25f;
//        buttonInView.layer.cornerRadius = buttonSize/2;
//        [buttonInView setTitle:[NSString stringWithFormat:@"%i", i] forState:UIControlStateNormal];
//        [buttonInView setTitleColor: [UIColor blackColor] forState:UIControlStateNormal];
//        buttonInView.layer.borderColor  = [UIColor blackColor].CGColor;
//        buttonInView.backgroundColor    = UIColor.whiteColor;
//
//        [self addSubview:buttonInView];
//    }
//}

- (void)createButtonMore:(NSString*)titleButton {
    UILabel *lblEtcs = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    lblEtcs.text = titleButton;
    lblEtcs.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblEtcs.numberOfLines = 1;
    lblEtcs.baselineAdjustment = YES;
    lblEtcs.adjustsFontSizeToFitWidth = YES;
    lblEtcs.clipsToBounds = YES;
    lblEtcs.backgroundColor = [UIColor clearColor];
    lblEtcs.textColor = [UIColor blackColor];
    lblEtcs.textAlignment = NSTextAlignmentLeft;
    [self addSubview:lblEtcs];
    
    UIButton *btnEtcs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnEtcs.frame = CGRectMake(60, 0, 50, 50);
    btnEtcs.backgroundColor = UIColor.yellowColor;
    btnEtcs.layer.cornerRadius = btnEtcs.frame.size.height / 2;
    btnEtcs.layer.masksToBounds = true;
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-menu-vertical-24.png"];
    [btnEtcs setImage:btnFloat forState:UIControlStateNormal];
    [btnEtcs addTarget:self action:@selector(moreAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnEtcs];
}

- (void)createButtonFavs:(NSString*)titleButton {
    UILabel *lblFavs = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    lblFavs.text = titleButton;
    lblFavs.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblFavs.numberOfLines = 1;
    lblFavs.baselineAdjustment = YES;
    lblFavs.adjustsFontSizeToFitWidth = YES;
    lblFavs.clipsToBounds = YES;
    lblFavs.backgroundColor = [UIColor clearColor];
    lblFavs.textColor = [UIColor blackColor];
    lblFavs.textAlignment = NSTextAlignmentLeft;
    [self addSubview:lblFavs];
    
    UIButton *btnFavs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnFavs.frame = CGRectMake(60, 0, 50, 50);
    btnFavs.backgroundColor = UIColor.yellowColor;
    btnFavs.layer.cornerRadius = btnFavs.frame.size.height / 2;
    btnFavs.layer.masksToBounds = true;
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-heart-outline-24.png"];
    [btnFavs setImage:btnFloat forState:UIControlStateNormal];
    [btnFavs addTarget:self action:@selector(favAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnFavs];
    [self bringSubviewToFront:btnFavs];
}

- (void)createButtonShare:(NSString*)titleButton {
    UILabel *lblShrs = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    lblShrs.text = titleButton;
    lblShrs.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblShrs.numberOfLines = 1;
    lblShrs.baselineAdjustment = YES;
    lblShrs.adjustsFontSizeToFitWidth = YES;
    lblShrs.clipsToBounds = YES;
    lblShrs.backgroundColor = [UIColor clearColor];
    lblShrs.textColor = [UIColor blackColor];
    lblShrs.textAlignment = NSTextAlignmentLeft;
    [self addSubview:lblShrs];
    
    UIButton *btnShrs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnShrs.frame = CGRectMake(60, 0, 50, 50);
    btnShrs.backgroundColor = UIColor.yellowColor;
    btnShrs.layer.cornerRadius = btnShrs.frame.size.height / 2;
    btnShrs.layer.masksToBounds = true;
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-share-24.png"];
    [btnShrs setImage:btnFloat forState:UIControlStateNormal];
    [btnShrs addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnShrs];
    [self bringSubviewToFront:btnShrs];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
