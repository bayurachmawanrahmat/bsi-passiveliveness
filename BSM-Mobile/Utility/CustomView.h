//
//  CustomView.h
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomViewDelegate <NSObject>

-(void)buttonPressed:(UIButton*)button;

@end

@interface CustomView : UIView

@property (assign) id<CustomViewDelegate> delegate;

@end
