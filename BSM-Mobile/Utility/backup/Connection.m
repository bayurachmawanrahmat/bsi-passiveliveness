//
//  Connection.m
//  Aurora
//
//  Created by lds on 12/23/13.
//  Copyright (c) 2013 Inmagine. All rights reserved.
//

#import "Connection.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "DLAVAlertView.h"
#import "Utility.h"
#import "DESChiper.h"

@interface Connection()<UIAlertViewDelegate>{
    BOOL temp;
//    BOOL first;
    BOOL requestZPK;
    NSString *keyDes;
    DESChiper *desChipper;
    NSMutableData *data;
    DLAVAlertView *loadingAlert;
}
@end

@implementation Connection

@synthesize delegate = _delegate;

#pragma mark - Global Method

- (id)initWithDelegate:(id)delegate {
    self = [super init];
    if(self) {
        self.delegate = delegate;
        desChipper = [[DESChiper alloc]init];
        data = [[NSMutableData alloc]init];
//        first = true;
    }
    return self;
}

- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking{
    continueLoading = false;
    [self sendPostParmUrl:parmUrl needLoading:needLoading textLoading:nil encrypted:encrypted banking:isBanking];
}
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking favorite:(BOOL)aFavorite{
    self.fav = aFavorite;
    continueLoading = false;
    [self sendPostParmUrl:parmUrl needLoading:needLoading textLoading:nil encrypted:encrypted banking:isBanking];
}

- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading  encrypted:(BOOL)encrypted banking:(BOOL)isBanking continueLoading:(BOOL)aContinueLoading{
    continueLoading = aContinueLoading;
    [self sendPostParmUrl:parmUrl needLoading:needLoading textLoading:textLoading encrypted:encrypted banking:isBanking];
}
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking{
    
//    [self showLoading:textLoading];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"URL Parm: %@", parmUrl);
    NSMutableDictionary *dataParam = [Utility translateParam:parmUrl];
    
    requestType = [dataParam valueForKey:@"request_type"];
    needDecrypt = isBanking || [requestType isEqualToString:@"activation"] || self.fav;
    if([requestType isEqualToString:@"list_merchant"] || [requestType isEqualToString:@"list_bank"]){
        needDecrypt = false;
    }
    temp = [requestType isEqualToString:@"list_account"];
    
    if(self.fav){
        NSMutableDictionary *favData = [dataParam mutableCopy];
        [favData setObject:@"insert_favorite" forKey:@"request_type"];
        [[DataManager sharedManager]setFavData:favData];
    }
    [self sendPost:dataParam needLoading:needLoading textLoading:textLoading encrypted:encrypted];
}
- (void)sendPost:(NSMutableDictionary *)dataParam needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted{
    if(!loadingAlert){
        [self showLoading:textLoading];
    }
    NSDictionary *dict = @{@"dataParam":dataParam, @"encrypted":[NSNumber numberWithBool:encrypted]};
    [self performSelectorInBackground:@selector(sendData:) withObject:dict];
}




- (void)sendData:(NSDictionary *)param{
    if(self.fav){
        requestType = @"fav";
    }
    NSMutableDictionary *dataParam = [param objectForKey:@"dataParam"];
    BOOL encrypted = [[param valueForKey:@"encrypted"]boolValue];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *plainRequest = [self generateParamURL:dataParam];
    BOOL isActivation = [requestType isEqualToString:@"activation"];
    NSString *datatosend =plainRequest;
    
    NSString *keyDesEncrypted;
    //
    if(encrypted){
        keyDes = @"";
        if(isActivation){
            keyDes = [dataParam valueForKey:@"activation_code"];
            keyDesEncrypted = @"ACT";
        }else{
            keyDes = [desChipper generateDESKey];
            DLog(@"keydes plain: %@", keyDes);
            NSString *publicKeyServer = [userDefault valueForKey:@"publickey"];
            BDRSACryptorKeyPair *RSAKeyPair = [[BDRSACryptorKeyPair alloc] initWithPublicKey:publicKeyServer privateKey:NULL];
            
            BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
            BDError *error = [[BDError alloc] init];
            keyDesEncrypted = [RSACryptor encrypt:keyDes key:RSAKeyPair.publicKey error:error];
            NSLog(@"%@", keyDesEncrypted);
        }
        plainRequest = [desChipper doDES:plainRequest key:keyDes];
    }else{
        NSData *dataBase64 = [plainRequest dataUsingEncoding:NSUTF8StringEncoding];
        plainRequest = [dataBase64 base64EncodedString];
    }
    if(keyDes)
        plainRequest = [NSString stringWithFormat:@"%@#%@", plainRequest, keyDesEncrypted];
    DLog(@"PLAIN REQUEST: %@", plainRequest);
    plainRequest = [self urlEncodeUsingEncoding:NSASCIIStringEncoding data:plainRequest];
    NSString *body = @"";
    
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc]initWithDictionary:@{@"req_data":plainRequest}];
    if([requestType isEqualToString:@"activation"]){
        [requestData setObject:[dataParam valueForKey:@"msisdn"] forKey:@"msisdn"];
    }
    for (NSString *key in [requestData allKeys]){
        body = [NSString stringWithFormat:@"%@%@=%@&",body,key, [requestData valueForKey:key]];
    }
    body = [body substringToIndex:[body length]-1];
    
    if([userDefault objectForKey:@"session_id"]){
        body = [NSString stringWithFormat:@"%@&session_id=%@",body, [userDefault valueForKey:@"session_id"]];
    }
    
    DLog(@"data to sent %@", body);
    
   
    if([requestType isEqualToString:@"list_merchant"] || [requestType isEqualToString:@"list_bank"] || [requestType isEqualToString:@"list_account"]|| [requestType isEqualToString:@"list_denom"]){
        NSError *jsonError;
        NSData *objectData = [datatosend dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json_parm = [NSJSONSerialization JSONObjectWithData:objectData
                                                                  options:NSJSONReadingMutableContainers
                                                                    error:&jsonError];

       if([requestType isEqualToString:@"list_merchant"] ){
            NSString *id_merchant = @"";
            if([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00013"]){// TELEPON
                id_merchant = @"1";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00025"]) { // VOUCHER HP
                id_merchant = @"2";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00020"]) { // RETAILER
               id_merchant = @"3";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00016"]) { // ANGSURAN
                id_merchant = @"4";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00017"]) { // PEMBAYARAN TIKET
                id_merchant = @"5";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00015"]) { // TV KABEL
                id_merchant = @"6";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00014"]) { // ACADEMIC
                id_merchant = @"7";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00018"]) { // ASURANSI
               id_merchant = @"8";
            }
       }
        
    }
    

    
    NSString *apiUrl = [NSString stringWithFormat:@"%@index.htm",API_URL];
    
    NSLog(@"USING URL: %@", apiUrl);
	NSURL *nsUrl = [NSURL URLWithString:apiUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    connection = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask = [connection dataTaskWithRequest:request];
    [dataTask resume];
}

#pragma mark - NSURLSessionTaskDelegate
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    completionHandler(NSURLSessionResponseAllow);
}
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)aData
{
    [data appendData:aData];
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(!continueLoading){
        [self dismissLoading];
    }
    if(error == nil)
    {
        
        [self processData];
        NSLog(@"API is Succesfull");
    }
    else{
        if ([error code] == -1009)
        {
            [self handleError:lang(@"NEED_INET")];
        }
        else
        {
            [self handleError:lang(@"ERROR")];
        }
    }
        NSLog(@"Error %@",[error userInfo]);
}

- (void)processData{
    
    NSError* error;
    
    NSString *jsonString;
    BOOL needDecryptBase64 = true;
    if(needDecrypt || self.fav){
        
        NSString *tempString = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSASCIIStringEncoding];
        NSArray * listData = [tempString componentsSeparatedByString:@"#"];
        if(listData.count > 1){
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *privateKey = [userDefault valueForKey:@"privateKey"];
            BDRSACryptorKeyPair *RSAKeyPair = [[BDRSACryptorKeyPair alloc] initWithPublicKey:NULL privateKey:privateKey];
            
            BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
            BDError *error = [[BDError alloc] init];
            keyDes = [RSACryptor decrypt:listData[1] key:RSAKeyPair.privateKey error:error];
        }
        if(keyDes){
            NSData *tempData = [[NSData alloc]initWithBase64EncodedString:listData[0] options:NSDataBase64DecodingIgnoreUnknownCharacters];
            
            jsonString = [desChipper doDecryptDES:tempData key:keyDes];
            if(jsonString){
                needDecryptBase64 = false;
            }
            
        }
    }
    
    if(needDecryptBase64){
        NSString *tempString = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
        NSData *tempData = [[NSData alloc]initWithBase64EncodedString:tempString options:NSDataBase64DecodingIgnoreUnknownCharacters];
        
        jsonString = [[NSString alloc]initWithData:tempData encoding:NSASCIIStringEncoding];
    }
    
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
    
    
    NSLog(@"RESPONSE: %@", jsonString);
    id json = [NSJSONSerialization
               JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
               options:NSJSONReadingAllowFragments
               error:&error];
    
    if(([json isKindOfClass:[NSDictionary class]] &&
        [json objectForKey:@"response_code"] &&
        ([[json objectForKey:@"response_code"]isEqualToString:@"0000"]))){
        [loadingAlert  dismissWithClickedButtonIndex:0 animated:YES];
//        [DataManager sharedManager].continueLoading  = nil;
        requestZPK = true;
        [self sendPostParmUrl:@"request_type=login,customer_id" needLoading:true textLoading:@"Log into the application" encrypted:true banking:true continueLoading:false];
    }
    else if(([json isKindOfClass:[NSDictionary class]] &&
              [json objectForKey:@"response_code"] &&
              ([[json objectForKey:@"response_code"]isEqualToString:@"0001"]))){
        NSLog(@"RESPONSE: %s", "new activation");
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[json valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        if(json){
            if(requestZPK){
                requestType = @"zpk";
            }
            [self.delegate didFinishLoadData:json withRequestType:requestType];
            
        }else{
            [self handleError:lang(@"ERROR")];
        }
    }
    
    connection = nil;
}


- (void)showLoading:(NSString *)message{
//    if([DataManager sharedManager].continueLoading){
//        [DataManager sharedManager].continueLoading.message = message;
//        return;
//    }
    DLog(@"create loading");
    loadingAlert = [[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    
    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
    [viewLoading setBackgroundColor:[UIColor clearColor]];
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loadingView setColor:[UIColor blackColor]];
    [loadingView startAnimating];
    [viewLoading addSubview:loadingView];

    [loadingAlert  setContentView:viewLoading];
    [loadingAlert show];
}

#pragma mark - Default Param

- (NSString *)getIPAddress {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return [address isEqualToString:@"error"]?@"":address;
    
}

- (NSString *)generateParamURL:(NSMutableDictionary *)dataParam{
    DataManager *dataManager = [DataManager sharedManager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //add default param
    [dataParam setObject:[UIDevice currentDevice].model forKey:@"device_type"];
    [dataParam setObject:@"iphone" forKey:@"device"];
    [dataParam setObject:[[[UIDevice currentDevice]identifierForVendor]UUIDString] forKey:@"imei"];
    [dataParam setObject:[self getIPAddress] forKey:@"ip_address"];
    
    
    if([dataParam objectForKey:@"date_local"]){
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
        NSString *currentDate = [dateFormat stringFromDate:date];
        [dataParam setObject:currentDate forKey:@"date_local"];
    }
    if([dataParam objectForKey:@"customer_id"] && [defaults objectForKey:@"customer_id"]){
        [dataParam setObject:[defaults stringForKey:@"customer_id"] forKey:@"customer_id"];
    }
    if([dataParam objectForKey:@"menu_id"] && dataManager.menuId){
        [dataParam setObject:dataManager.menuId forKey:@"menu_id"];
    }
    if([dataParam objectForKey:@"language"]){
        NSArray *listLanguage = [defaults objectForKey:@"AppleLanguages"];
        [dataParam setObject:[listLanguage objectAtIndex:0] forKey:@"language"];
    }
    if([dataParam objectForKey:@"pin"] && dataManager.pinNumber){
            NSError *error;
            NSString *newPin = [desChipper doPinBlock:dataManager.pinNumber key:[defaults valueForKey:@"zpk"]];
            if(!error){
                [dataParam setObject:newPin forKey:@"pin"];
            }
    }
    
    if([dataParam objectForKey:@"new_pin"]){
//        if(![defaults boolForKey:@"development"]){
            NSError *error;
            NSString *newPin = [desChipper doPinBlock:[dataParam valueForKey:@"new_pin"] key:[defaults valueForKey:@"zpk"]];
            if(!error){
                [dataParam setObject:newPin forKey:@"new_pin"];
            }
//        }
    }
    if([dataParam objectForKey:@"pun"]){
//        if(![defaults boolForKey:@"development"]){
            NSError *error;
            NSString *newPin = [desChipper doPinBlock:[dataParam valueForKey:@"pun"] key:[defaults valueForKey:@"zpk"]];
            if(!error){
                [dataParam setObject:newPin forKey:@"pun"];
            }
//        }
    }
    if([dataParam objectForKey:@"zpk"]){
        [dataParam setObject:[defaults objectForKey:@"zpk"] forKey:@"zpk"];
    }
    for(NSString *key in dataParam.allKeys){
        if([[dataParam objectForKey:key]isKindOfClass:[NSNumber class]] && ![[dataParam objectForKey:key]boolValue]){
            if([dataManager.dataExtra objectForKey:key]){
                [dataParam setObject:[dataManager.dataExtra valueForKey:key] forKey:key];
            }else{
                NSLog(@"KEY %@ CHANGED TO EMPTY STRING", key);
                [dataParam setObject:@"" forKey:key];
            }
        }
    }
    
    NSLog(@"DATA TO SENT: %@", dataParam);
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dataParam options:0 error:nil];
//    if(encoded){
//        jsonData = [jsonData base64EncodedDataWithOptions:0];
//    }
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    return jsonString;
}

- (void)dismissLoading{
//    if([DataManager sharedManager].continueLoading ){
        [loadingAlert  dismissWithClickedButtonIndex:0 animated:YES];
    loadingAlert = nil;
//        [DataManager sharedManager].continueLoading  = nil;
//    }
}

- (void)handleError:(NSString *)message
{
    [self dismissLoading];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    //call error page later
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:true forKey:@"RTO"];
    [self.delegate reloadApp];
}

#pragma mark - Local Method

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding data:(NSString *)oriString{
	return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                               NULL,
                                                               (CFStringRef)oriString,
                                                               NULL,
                                                               (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                               kCFStringEncodingUTF8 ));
}

-(void)cancelConnection:(NSURLConnection *)conn{
    if(conn)
    {
        [conn cancel];
        connection = nil;
    }
}

@end
