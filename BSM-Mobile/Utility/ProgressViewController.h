//
//  ProgressViewController.h
//  BSM-Mobile
//
//  Created by ARS on 25/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ProgressViewControllerDelegate;

@interface ProgressViewController : UIViewController <NSURLConnectionDataDelegate, ProgressViewDelegate>

@property (weak, nonatomic) NSString *progressViewText;

- (instancetype)initWithDefaultConfigAndProgressViewText:(NSString *)progressViewText;
-(void) doDownloadFromUrl:(NSString *)downloadUrl saveWithFilename:(NSString *)filename;

@property (weak, nonatomic) id<ProgressViewControllerDelegate> delegate;

@end

@protocol ProgressViewControllerDelegate <NSObject>

@required
- (void) progressBarDidFinishLoading:(NSString *)data;

@end


NS_ASSUME_NONNULL_END
