//
//  ArahKiblat.m
//  BSM-Mobile
//
//  Created by BSM on 9/18/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "ArahKiblat.h"

@implementation ArahKiblat

float latitude;
float longitude;
float latKabah = 21.42250833;
float lngKabah = 39.82616111;
float qpi = 3.14159265;

+ (id)objArahKiblat {
    static ArahKiblat *g_instance = nil;
    if (g_instance == nil) {
        g_instance = [[self alloc] init];
    }
    return g_instance;
}

- (float)getLatitude{
    return latitude;
}

- (void)setLatitude:(float)lat{
    latitude = lat;
}

- (float)getLongitude{
    return longitude;
}

- (void)setLongitude:(float)lng{
    longitude = lng;
}

- (float)calcArahKiblat:(float)lat paramLong:(float)lng{
    float lKlM = (lngKabah-lng);
    float sinLKLM = sinf(lKlM*2.0*(qpi/360));
    float cosLKLM = cosf(lKlM*2.0*(qpi/360));
    float sinLM = sinf(lat*2.0*(qpi/360));
    float cosLM = cosf(lat*2.0*(qpi/360));
    float tanLK = tanf(latKabah*2.0*(qpi/360));
    float denominator = (cosLM*tanLK)-(sinLM*cosLKLM);

    float Qibla;
    float direction;

    Qibla = atan2f(sinLKLM, denominator)*180/qpi;
    direction = Qibla<0 ? Qibla+360 : Qibla;
    
    return direction;
}

@end
