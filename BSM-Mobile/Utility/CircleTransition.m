//
//  CircleTransition.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CircleTransition.h"

@implementation CircleTransition{
    UIView *circle;
    CGPoint startingPoint;
    UIColor *circleColor;
    CGFloat duration;
    CircularTransitionMode transitionMode;
}

- (id) init{
    
    circle = [[UIView alloc]init];
    startingPoint = CGPointZero;
    circle.center = startingPoint;
    circleColor = [UIColor whiteColor];
    duration = .5;
    transitionMode = present;
    
    return self;
}
 
- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext
{
    return duration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIView *containerView = transitionContext.containerView;
    
    if(transitionMode == present){
        UIView *presentedView = [transitionContext viewForKey:UITransitionContextToViewKey];
        CGPoint viewCenter = presentedView.center;
        CGSize viewSize = presentedView.frame.size;
        
        circle = [[UIView alloc]init];
        
        circle.frame = [self frameForCircle:viewCenter viewSize:viewSize andStartPoint:startingPoint];
    
        
        circle.layer.cornerRadius = circle.frame.size.height / 2;
        circle.center = startingPoint;
        circle.backgroundColor = circleColor;
        circle.transform = CGAffineTransformMakeScale(0.001, 0.001);
        [containerView addSubview:circle];
        
        presentedView.center = startingPoint;
        presentedView.transform = CGAffineTransformMakeScale(0.001, 0.001);
        presentedView.alpha = 0;
        [containerView addSubview:presentedView];
        
        
        [UIView animateWithDuration:duration animations:^(){
            self->circle.transform = CGAffineTransformIdentity;
            presentedView.transform = CGAffineTransformIdentity;
            presentedView.alpha = 1;
            presentedView.center = viewCenter;
        } completion:^(bool succsess){
            [transitionContext completeTransition:succsess];
        }];
        
    }else{
        UITransitionContextViewKey transitionModeKey;
        if(transitionMode == pop){
            transitionModeKey = UITransitionContextToViewKey;
        }else{
            transitionModeKey = UITransitionContextFromViewKey;
        }
        
        UIView *returningView = [transitionContext viewForKey:transitionModeKey];
        CGPoint viewCenter = returningView.center;
        CGSize viewSize = returningView.frame.size;
        
        circle.frame = [self frameForCircle:viewCenter viewSize:viewSize andStartPoint:startingPoint];
        circle.layer.cornerRadius = circle.frame.size.height / 2;
        circle.center = startingPoint;
        
        [UIView animateWithDuration:duration animations:^(){
            self->circle.transform = CGAffineTransformMakeScale(0.001, 0.001);
            returningView.transform = CGAffineTransformMakeScale(0.001, 0.001);
            returningView.alpha = 0;
            returningView.center = self->startingPoint;
            
            if(self->transitionMode == pop){
                [containerView insertSubview:returningView belowSubview:returningView];
            }else{
                [containerView insertSubview:returningView belowSubview:returningView];
            }
            
        } completion:^(bool succsess){
            
            returningView.center = viewCenter;
            [returningView removeFromSuperview];
            
            [self->circle removeFromSuperview];
            
            [transitionContext completeTransition:succsess];
        }];
    }
    
}

- (CGRect) frameForCircle : (CGPoint) viewCenter viewSize:(CGSize) size andStartPoint:(CGPoint) startPoint {
    
    double xLength = fmax(startPoint.x, size.width - startPoint.x);
    double yLength = fmax(startPoint.y, size.height - startPoint.y);

    double offsetVector = sqrt(xLength*xLength + yLength * yLength) *2;
    
    CGSize newsize = CGSizeMake(offsetVector, offsetVector);
    
    return CGRectMake(0, 0, newsize.width, newsize.height);
}

- (void)setStartingPoint:(CGPoint)point{
    startingPoint = point;
}

- (void)setCircleColor:(UIColor *)color{
    circleColor = color;
}

- (void)setTransitionMode:(CircularTransitionMode)mode{
    transitionMode = mode;
}

@end

 
