//
//  Encryptor.h
//  BSM-Mobile
//
//  Created by ARS on 27/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Encryptor : NSObject

+(NSString *)HexEncrypt:(NSString *)s;
+(NSString *)HexDecrypt:(NSString *)s;
+(NSString *)AESDecrypt:(NSString *)s;
+(NSData *)AESDecryptData:(NSString *)s;
+(NSString *)base64Encode:(NSString *)s;
+(NSString *)base64Decode:(NSString *)s;
+(NSString *)stringToHex:(NSString *)s;
+(NSString *)hexToString:(NSString *)s;

-(void)setUserDefaultsObject:(nullable id)object forKey:(nonnull NSString *)key;
-(id)getUserDefaultsObjectForKey:(NSString *)key;
-(void)removeUserDefaultsObjectForKey:(NSString *)key;
- (void)restoreDataToLocalUsingData:(NSMutableDictionary *)data;
- (void) resetFormData;
- (void)removeImages;
- (void)removeUserDefaults;
- (void)removeVideoAssist;

@end
