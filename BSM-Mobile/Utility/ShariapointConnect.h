//
//  ShariapointConnect.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "DLAVAlertView.h"
NS_ASSUME_NONNULL_BEGIN


@protocol ShariapointConnectDelegate <NSObject>

- (void)didFinishLoadData:(NSDictionary* _Nullable)dict withEndpoint:(NSString*)endPoint completeWithError:(NSError*)error;
- (void)didFinishWithError:(NSError*) error;

@end

@interface ShariapointConnect : NSObject<NSURLSessionTaskDelegate, NSURLSessionDataDelegate>{
    NSURLSession *connection;
    NSString *endPointRequest;
    NSString *paramsURL;
    NSMutableData *data;
    BOOL isLoading;
    BOOL isEncrypted;
    NSString *txtLoading;
    DLAVAlertView *loadingAlert;
}

@property (nonatomic, retain) id <ShariapointConnectDelegate> delegate;
@property (nonatomic, strong) dispatch_group_t _Nullable dispatchGroup;
@property (nonatomic, assign) BOOL requestIsSuccess;

- (id)initWithDelegate:(id)delegate;

- (void)sendPostData:(NSString*)params endPoint:(NSString *)endpoint needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted;

- (void)getDataFromEndPoint:(NSString*) endpoint needLoading:(BOOL)needLoading textLoading:(NSString*)textLoading encrypted:(BOOL)encrypted;

+ (NSString *)getToken;
+ (NSString *)apiUrl;
+ (NSString *)safeEndpoint:(NSString *)endpoint;

@end


NS_ASSUME_NONNULL_END
