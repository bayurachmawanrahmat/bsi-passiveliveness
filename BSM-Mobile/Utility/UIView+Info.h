//
//  UIView+Info.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView(Info)
    @property (nonatomic, strong) id info;
@end

@implementation UIView(Info)

- (void) setInfo:(id)info
{
    objc_setAssociatedObject( self, "_info", info, OBJC_ASSOCIATION_RETAIN_NONATOMIC ) ;
}

- (id)info{
    return objc_getAssociatedObject( self, "_info" ) ;
}

@end

NS_ASSUME_NONNULL_END
