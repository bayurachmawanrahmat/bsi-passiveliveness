//
//  UIDropdown.m
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "UIDropdown.h"

@implementation UIDropdown
{
    NSArray *keys;
    NSMutableArray *values;
    BOOL isResetable;
}

@synthesize options;
@synthesize pickerView;

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // UIPickerView *pickerView = [[UIPickerView alloc] init];
        pickerView = [[UIPickerView alloc] init];
        pickerView.delegate = self;
        pickerView.showsSelectionIndicator = YES;
        
        isResetable = NO;
        
        UIToolbar *toolbar = [[UIToolbar alloc] init];
        toolbar.barStyle = UIBarStyleDefault;
        toolbar.translucent = YES;
        [toolbar sizeToFit];
        
        UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(donePicker)];
        
        UIBarButtonItem *btnCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPicker)];
        
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        [toolbar setItems:[NSArray arrayWithObjects:btnCancel, space, btnDone, nil]];
        [toolbar setUserInteractionEnabled:YES];
        
        self.inputView = pickerView;
        self.inputAccessoryView = toolbar;
        
        self.rightViewMode = UITextFieldViewModeAlways;
        self.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_dropdown_grey@2x.png"]];
    }
    return self;
}

-(void)donePicker
{
    @try {
        if (isResetable) {
            NSArray* arrayData = [[self selectedData] allValues];
            if (arrayData.count == 0 && options.count > 0) {
                self.selectedData = [[NSDictionary alloc] initWithObjectsAndKeys:[values objectAtIndex:0], [keys objectAtIndex:0], nil];
                self.text = [[self selectedData] allValues][0];
            } else if (arrayData.count > 0) {
                self.text = arrayData[0];
            }
        } else {
            self.text = [[self selectedData] allValues][0];
        }
    } @catch (NSException *exception) {
        // do nothing
    } @finally {
        // do nothing
    }
    
    [self.superview endEditing:YES];
}

-(void)cancelPicker
{
    [self.superview endEditing:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return options.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    keys = [[NSArray alloc] init];
    keys = [options allKeys];
    keys = [keys sortedArrayUsingComparator:^(id a, id b){
        return [a compare:b options:NSNumericSearch];
    }];
    values = [[NSMutableArray alloc] init];
    for (id key in keys) {
        [values addObject:[options objectForKey:key]];
    }
    return [values objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedData = [[NSDictionary alloc] initWithObjectsAndKeys:[values objectAtIndex:row], [keys objectAtIndex:row], nil];
}

- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    return CGRectZero;
}

- (NSArray<UITextSelectionRect *> *)selectionRectsForRange:(UITextRange *)range
{
    return [NSArray new];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if(action == @selector(copy) || action == @selector(selectAll:) || action == @selector(paste:))
    {
        return false;
    }
    
    return [super canPerformAction:action withSender:sender];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self endEditing:YES];
}

- (void)clearData{
    if (!isResetable) {
        return;
    }
    keys = [[NSArray alloc] init];
    [values removeAllObjects];
    _selectedData = [[NSDictionary alloc] init];
    options = [[NSMutableDictionary alloc] init];
    self.text = @"";
}

- (void)selectFirstIndex{
    if (!isResetable) {
        return;
    }
    [pickerView selectRow:0 inComponent:0 animated:NO];
}

- (void) setResetable:(BOOL)resetable{
    isResetable = resetable;
}

@end
