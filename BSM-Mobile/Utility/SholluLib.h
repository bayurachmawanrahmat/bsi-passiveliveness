//
//  SholluLib.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 03/11/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SholluLib : NSObject

@property (nonatomic, assign) BOOL flagMinus;
@property (nonatomic, assign) BOOL selectedTime;
@property (nonatomic, assign) BOOL showSyariahpoint;

@property (nonatomic, assign) int leastInterval;
@property (nonatomic, assign) int idxIS;

@property (nonatomic, retain) NSString *placemarkLocality;

@property (nonatomic, retain) NSDate *currentDate;
@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;
@property (nonatomic, retain) NSMutableArray *times;
@property (nonatomic, retain) NSMutableArray *listNames;
@property (nonatomic, retain) NSMutableArray *arrInterval;
@property (nonatomic, retain) NSMutableArray *listData;
@property (nonatomic, retain) NSArray *arrListTimes;

@property (nonatomic, retain) NSTimer *myTimer;

- (id) init;
- (void) callPrayerTime;
- (void) getListData;
- (void) proceedTick;
- (BOOL) isSelectedTime;
- (int) getIdxIS;
- (NSString*) getTimeDifference;
- (NSString*) getJadwalSholat;
- (NSString*) getTimeAdzan;
- (NSString*) getWaktu;
- (NSString*) getWaktuSholat;
- (NSString*) getDayTimes;
- (NSArray*) getListNames;
- (NSArray*) getListTimes;
- (NSMutableAttributedString*) getAttributedJadwalSholat;
+ (NSMutableAttributedString*) getAttributedSalam;
- (NSString*) getTimeSholat:(NSString *) currentTimeSholat
                    noIndex:(NSInteger )xIndex;
+ (void) createNotif:(NSInteger) selectedTime withData:(NSDictionary*)dict;
- (void) setNotificationData;
@end

NS_ASSUME_NONNULL_END
