//
//  SKeychain.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 21/01/21. 
//  Copyright © 2021 lds. All rights reserved.
//

#import "SKeychain.h"
#import "AESCipher.h"

@implementation SKeychain

+ (BOOL) checkOSStatus : (OSStatus)status{
    return status == noErr;
}

+ (NSMutableDictionary *)keychainQueryForKey:(NSString*)key{

    SecAccessControlRef sacRef;
    CFErrorRef *err = nil;

     /*
      Important considerations.
      Please read the docs regarding kSecAttrAccessibleWhenPasscodeSetThisDeviceOnly.
      TL;DR - If the user unsets their device passcode, these keychain items are destroyed.
      You will need to add code to compensate for this, i.e to say that touch ID can only be used if the device has a passcode set.

      Additionally, keychain entries with this flag will not be backed up/restored via iCloud.
      */

    //Gets our Security Access Control ref for user presence policy (requires user AuthN)
    sacRef = SecAccessControlCreateWithFlags(kCFAllocatorDefault,
                                             kSecAttrAccessibleWhenUnlockedThisDeviceOnly,
                                             kSecAccessControlUserPresence,
                                             err);
//    NSString *accessGroup = @"bsimobileuserdata";

    return [@{  (__bridge id)kSecClass : (__bridge id)kSecClassGenericPassword,
                (__bridge id)kSecAttrService : key,
                (__bridge id)kSecAttrAccount : key,
                //Whether or not we want to prompt on insert
                (__bridge id)kSecUseAuthenticationUI: @YES,
                //Our security access control reference
                (__bridge id)kSecAttrAccessControl: (__bridge_transfer id)sacRef
//                (__bridge id)kSecAttrAccessible : (__bridge id)kSecAttrAccessibleWhenUnlockedThisDeviceOnly
            }mutableCopy];
//
}

+ (BOOL) saveObject:(id)object forKey:(NSString*)key{
    
    AESCipher *aesChiper = [[AESCipher alloc]init];
//    [aesChiper aesEncryptString:object];
    
    NSMutableDictionary *keychainQuery = [self keychainQueryForKey:key];
    
    [self deleteObjectForKey:key];
    
//    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:object] forKey:(__bridge id)kSecValueData];
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:[aesChiper aesEncryptString:object]] forKey:(__bridge id)kSecValueData];
    
    return [self checkOSStatus:SecItemAdd((__bridge CFDictionaryRef)keychainQuery, NULL)];
    
}

+ (id)loadObjectForKey:(NSString*)key{
    id object = nil;
    AESCipher *aesChiper = [[AESCipher alloc]init];

    NSMutableDictionary *keyChainQuery = [self keychainQueryForKey:key];
    
    [keyChainQuery setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    [keyChainQuery setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    CFDataRef keyData = NULL;
    
    if([self checkOSStatus:SecItemCopyMatching((__bridge CFDictionaryRef)keyChainQuery, (CFTypeRef*)&keyData)]){
        @try {
            object = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)keyData];
            object = [aesChiper aesDecryptString:object];
        } @catch (NSException *exception) {
            NSLog(@"Unarchiving for key %@ failed with exception %@", key, exception.name);
        } @finally {
            
        }
    }
    
    if(keyData){
        CFRelease(keyData);
    }
    
    return object;
}

+ (BOOL) deleteObjectForKey:(NSString*)key{
    NSMutableDictionary *keychainQuery = [self keychainQueryForKey:key];
    return [self checkOSStatus:SecItemDelete((__bridge CFDictionaryRef)keychainQuery)];
}

@end
