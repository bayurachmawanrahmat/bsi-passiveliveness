//
//  Rekening.m
//  BSM-Mobile
//
//  Created by ARS on 30/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rekening.h"


@implementation Rekening

- (instancetype)init
{
    self = [super init];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self->_alamatCabang forKey:@"alamatCabang"];
    [coder encodeObject:self->_jenisKartu forKey:@"jenisKartu"];
    [coder encodeObject:self->_jenisTabungan forKey:@"jenisTabungan"];
    [coder encodeObject:self->_namaCabang forKey:@"namaCabang"];
    [coder encodeObject:self->_namaNasabah forKey:@"namaNasabah"];
    [coder encodeObject:self->_nomorRekening forKey:@"nomorRekening"];
    [coder encodeObject:self->_kodeCabang forKey:@"kodeCabang"];
    [coder encodeObject:self->_jenisKyc forKey:@"jenis_kyc"];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [self init];
    if (self) {
        self->_alamatCabang = [coder decodeObjectForKey:@"alamatCabang"];
        self->_jenisKartu = [coder decodeObjectForKey:@"jenisKartu"];
        self->_jenisTabungan = [coder decodeObjectForKey:@"jenisTabungan"];
        self->_namaCabang = [coder decodeObjectForKey:@"namaCabang"];
        self->_namaNasabah = [coder decodeObjectForKey:@"namaNasabah"];
        self->_nomorRekening = [coder decodeObjectForKey:@"nomorRekening"];
        self->_kodeCabang = [coder decodeObjectForKey:@"kodeCabang"];
    }
    return self;
}

@end
