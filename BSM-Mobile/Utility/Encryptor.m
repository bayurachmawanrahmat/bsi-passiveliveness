//
//  Encryptor.m
//  BSM-Mobile
//
//  Created by ARS on 27/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "Encryptor.h"
#import "AESCrypt.h"
#import "CustomerOnboardingData.h"

@implementation Encryptor
{
    NSUserDefaults *userDefaults;
}

static NSString *const SALT = @"59731B52B3EC58FA";
static NSString *const KEY = @"05788993F8E4CE6A";
static NSString *const IV = @"46060159617A8U7J";

- (instancetype)init
{
    self = [super init];
    if (self) {
        userDefaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

+ (NSString *)HexEncrypt:(NSString *)s
{
    return [[self class] base64Encode:[[[self class] stringToHex:s] stringByAppendingString:SALT]];
}

+ (NSString *)HexDecrypt:(NSString *)s
{
    return [[self class] hexToString:[[[self class] base64Decode:s] stringByReplacingOccurrencesOfString:SALT withString:@""]];
}

+ (NSString *)AESDecrypt:(NSString *)s
{
    NSData *decrypted = [[self class] AESDecryptData:s];
    if(decrypted)
        return [[NSString alloc] initWithData:decrypted encoding:NSUTF8StringEncoding];
    
    return nil;
}

+ (NSData *)AESDecryptData:(NSString *)s
{
    NSData *data = [[NSData alloc] initWithBase64EncodedString:s options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    char keyPtr[kCCKeySizeAES128 + 1];
    bzero(keyPtr, sizeof(keyPtr));
    [KEY getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    char ivPtr[kCCBlockSizeAES128 + 1];
    bzero(ivPtr, sizeof(ivPtr));
    [IV getCString:ivPtr maxLength:sizeof(ivPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [data length];
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmAES, kCCOptionPKCS7Padding, keyPtr, kCCBlockSizeAES128, ivPtr, [data bytes], [data length], buffer, bufferSize, &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    free(buffer);
    return nil;
}

+ (NSString *)base64Encode:(NSString *)s
{
    NSData *data = [s dataUsingEncoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
}

+ (NSString *)base64Decode:(NSString *)s
{
    NSData *data = [NSData dataWithBase64EncodedString:s];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (NSString *)stringToHex:(NSString *)s
{
    const char *utf8 = [s UTF8String];
    NSMutableString *hex = [NSMutableString string];
    while ( *utf8 ) [hex appendFormat:@"%02X" , *utf8++ & 0x00FF];
    
    return [NSString stringWithFormat:@"%@", hex];
}

+ (NSString *)hexToString:(NSString *)s
{
    NSMutableString *ret = [[NSMutableString alloc] init];
    int i = 0;
    while (i < [s length])
    {
        NSString * hexChar = [s substringWithRange: NSMakeRange(i, 2)];
        int value = 0;
        sscanf([hexChar cStringUsingEncoding:NSASCIIStringEncoding], "%x", &value);
        [ret appendFormat:@"%c", (char)value];
        i+=2;
    }
    
    return ret;
}

- (void)setUserDefaultsObject:(id)object forKey:(NSString *)key
{
    if(ONBOARDING_SAFE_MODE)
    {
        NSString *encKey = [[self class] HexEncrypt:key];
        NSString *encObj;
        if([object isKindOfClass:NSString.class])
        {
            encObj = [[self class] HexEncrypt:object];
            [userDefaults setObject:encObj forKey:encKey];
        }
        else if([object isKindOfClass:NSDictionary.class])
        {
            NSError *error;
            NSData *data = [NSJSONSerialization dataWithJSONObject:object options:kNilOptions error:&error];
            if(!error)
            {
                encObj = [[self class] HexEncrypt:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
                [userDefaults setObject:encObj forKey:encKey];
            }
        }
        else
        {
            @try {
                NSString *strObj = [NSString stringWithFormat:@"%@", object];
                encObj = [[self class] HexEncrypt:strObj];
                [userDefaults setObject:encObj forKey:encKey];
            } @catch (NSException *exception) {
                NSLog(@"Exception : %@", exception.reason);
            } @finally {
                // nothing todo
            }
        }
    }
    else {
        if ([object isKindOfClass:NSDictionary.class]) {
            NSError *error;
            NSData *data = [NSJSONSerialization dataWithJSONObject:object options:kNilOptions error:&error];
            if(!error)
            {
                [userDefaults setObject:data forKey:key];
            }
        }
        else [userDefaults setObject:object forKey:key];
    }
    
    [userDefaults synchronize];
}

- (id)getUserDefaultsObjectForKey:(NSString *)key
{
    if(ONBOARDING_SAFE_MODE)
    {
        NSString *encKey = [[self class] HexEncrypt:key];
        NSString *encObj = [userDefaults objectForKey:encKey];
        if(encObj)
        {
            NSString *str = [[self class] HexDecrypt:encObj];
            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
            NSError *error;
            id obj = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(!error)
            {
                if([obj isKindOfClass:NSDictionary.class])
                    return (NSDictionary *)obj;
                else if([obj isKindOfClass:NSArray.class])
                    return (NSArray *)obj;
            }
            return str;
        }
    }
    else {
        @try {
            if ([[userDefaults objectForKey:key] isKindOfClass:NSData.class]) {
                NSError *error;
                NSDictionary *res = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:[userDefaults objectForKey:key] options:kNilOptions error:&error];
                if (!error)
                    return res;
            }
            else return [userDefaults objectForKey:key];
        } @catch (NSException *exception) {
            return nil;
        }
    }
    
    return nil;
}

- (void)removeUserDefaultsObjectForKey:(NSString *)key
{
    NSString *encKey = [[self class] HexEncrypt:key];
    [userDefaults removeObjectForKey:key];
    [userDefaults removeObjectForKey:encKey];
    [userDefaults synchronize];
}

- (void)restoreDataToLocalUsingData:(NSMutableDictionary *)data
{
    NSLog(@"restoreDataToLocalUsingData : ");
    NSLog(@"%@", data);
    NSInteger isDomisili = [[data objectForKey:@"is_domisili_sesuai_ktp"] integerValue];
    [self setUserDefaultsObject:(isDomisili == 1 ? @"0" : @"1") forKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY];
    [self setUserDefaultsObject:data forKey:ONBOARDING_RESTORE_DATA_KEY];
    [self setUserDefaultsObject:[data objectForKey:@"jenis_kyc"] forKey:ONBOARDING_JENIS_KYC_KEY];
    [self setUserDefaultsObject:[data objectForKey:@"nik"] forKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
    [self setUserDefaultsObject:[data objectForKey:@"email"] forKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY];
    [self setUserDefaultsObject:[data objectForKey:@"no_hp"] forKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    [self setUserDefaultsObject:[data objectForKey:@"nama_lgkp"] forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP];
    [self setUserDefaultsObject:[data objectForKey:@"nik"] forKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
    [self setUserDefaultsObject:[data objectForKey:@"kode_registrasi"] forKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY];
}

- (void) resetFormData
{
    NSString *msmsid = [CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY];
    Encryptor *enc = [[Encryptor alloc] init];
    [self removeImages];
    [self removeUserDefaults];
    [enc setUserDefaultsObject:msmsid forKey:ONBOARDING_MSMSID_KEY];
}

- (void)removeImages
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *directoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSArray *filenames = [[NSArray alloc] initWithObjects:@"KTP", @"NPWP", @"FOTO_DIRI", @"TTD", nil];
    NSString *filePath;
    NSError *error;
    for (NSString *filename in filenames) {
        filePath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", filename]];
        [fileManager removeItemAtPath:filePath error:&error];
    }
}

- (void)removeUserDefaults
{
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_PROGRESS_STEP_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DEVICE_HAS_NOTCH_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_RESTORE_DATA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_REKENING_INFO_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_BEGIN_SCREEN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_ISSHOW_PEMBUKAANAKUNSCREEN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_NEED_CHECK_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_MSMSID_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_TOKEN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASIKONTAK_EMAIL_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASIKONTAK_PHONE_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASIKONTAK_KODEPROMO_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_OTP_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_JENISKARTU_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_JENISKARTU_IDX"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_JENISTABUNGAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_FOTO_KTP_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_KTP_FILEID_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_ALAMAT_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_RT_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_RW_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_KELURAHAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_KECAMATAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_PENDIDIKAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NAMA_LGKP"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NAMA_MASKING"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NAMA_LGKP_AYAH"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_GOL_DARAH"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_JENIS_PKRJN"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_WARGANEGARA"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_PDDK_AKH"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_AGAMA"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_STATUS_KAWIN"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_DUSUN"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_TGL_LHR"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_STAT_HBKEL"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_EKTP_STATUS"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_TMPT_LAHIR"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_JENIS_KLMIN"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NO_KK"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_ID_PROV"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NO_PROP"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_PROP_NAME"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_ID_KAB"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NO_KAB"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_KAB_NAME"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NO_KEC"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_KEC_NAME"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NO_KEL"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_KEL_NAME"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NO_RT"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_NO_RW"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_DUKCAPIL_ALAMAT"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_NPWP_FILEID_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_NPWP_NOMOR_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_NPWP_NAMA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_NPWP_ISPUNYA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_NPWP_JENIS_KEPEMILIKAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_TTD_FILEID_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_SELFIE_FILEID_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_VERIFIKASI_WAJAH_FILEID_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_JENIS_KYC_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_STATUS_PEKERJAAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_JABATAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_PENGHASILAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_BIDANG_USAHA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_ALAMAT_PERUSAHAAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_TELEPON_PERUSAHAAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_NAMA_SUAMI_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_SUAMI_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_ALAMAT_SUAMI_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_KEUANGAN_TELEPON_SUAMI_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_REKENING_TUJUAN_BUKA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_REKENING_SUMBER_DANA_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_REKENING_TRANSAKSI_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_REKENING_ISZAKAT_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_INFORMASI_REKENING_CABANG_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_NASABAH_SETUJU_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_VIDEASSIST_VERSION"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_VIDEOASSIST_FILE_COUNT"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_VIDEOASSIST_FILES"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_VIDEOASSIST_GENDER_OPERATOR"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_VIDEOASSIST_RANDOM_DESCRIPTION_KEY"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_VIDEOASSIST_RANDOM_FILECOUNT"];
    [self removeUserDefaultsObjectForKey:@"ONBOARDING_PENILAIAN_FITUR_IS_DONE"];
}

- (void)removeVideoAssist
{
    Encryptor *enc = [[Encryptor alloc] init];
    NSMutableDictionary *filesInfo = [enc getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_FILES];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    for (NSString *key in filesInfo) {
        NSError *error;
        NSDictionary *file = [filesInfo objectForKey:key];
        NSString *filename = [file objectForKey:@"filename"];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, filename];
        [fileManager removeItemAtPath:filePath error:&error];
        if(!error)
            NSLog(@"File %@ removed.", filename);
        else
            NSLog(@"File %@ failed to remove", filename);
    }
    [enc removeUserDefaultsObjectForKey:ONBOARDING_VIDEASSIST_VERSION];
    [enc removeUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_FILES];
}


@end
