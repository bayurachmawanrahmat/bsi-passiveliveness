//
//  UIDropdown.h
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDropdown : UITextField <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property NSMutableDictionary *options;
@property UIPickerView *pickerView;
@property NSDictionary *selectedData;

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
-(void)clearData;
-(void)selectFirstIndex;
-(void)setResetable:(BOOL)resetable;
-(BOOL)isResetable;
@end

NS_ASSUME_NONNULL_END
