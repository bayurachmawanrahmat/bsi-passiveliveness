//
//  SholluLib.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 03/11/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SholluLib.h"
#import "PrayerTime.h"
#import "PrayerNotification.h"
#import "Utility.h"

@implementation SholluLib

- (id)init{
    self = [super init];
    
    if(self){

    }
    return self;
}

-(NSString *)getTimeSholat:(NSString *) currentTimeSholat
                   noIndex:(NSInteger )xIndex{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *jamSholat;
    
    if(xIndex > 0){
        NSString *customTimeSholat = [userDefault valueForKey:[NSString stringWithFormat:@"custo_time_%d",(int)xIndex]];
        NSLog(@"%@", customTimeSholat);
        if(customTimeSholat == nil || [customTimeSholat isEqualToString:@""]){
            customTimeSholat = @"0";
        }
        if (![customTimeSholat  isEqual: @"0"]) {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"HH:mm"];
            NSDate *currentTime = [dateFormat dateFromString:currentTimeSholat];
            NSDate* newDate = [currentTime dateByAddingTimeInterval:[customTimeSholat integerValue]*60];
            jamSholat = [dateFormat stringFromDate:newDate];
        }else{
            jamSholat = currentTimeSholat;
        }
    }else{
        NSString *cusTimeShubuh = [userDefault valueForKey:@"custo_time_1"];
        
        if (cusTimeShubuh == nil || [cusTimeShubuh isEqualToString:@""]) {
            cusTimeShubuh = @"0";
        }
        if (![cusTimeShubuh isEqualToString:@"0"]) {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"HH:mm"];
            NSDate *currentTime = [dateFormat dateFromString:self.times[0]];
            NSDate* newDate = [currentTime dateByAddingTimeInterval:[cusTimeShubuh integerValue]*60];
            jamSholat = [self getImsakTime:[dateFormat stringFromDate:newDate]];
        }else{
            jamSholat = currentTimeSholat;
        }
    }
    return jamSholat;
}

- (NSString *)getDayTimes{
    NSDate * now = [NSDate date];
    
    NSLocale *idIDPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"id_ID_POSIX"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setLocale:idIDPOSIXLocale];
    [outputFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    
    NSString *date1 = [outputFormatter stringFromDate:now];
    return date1;
}

-(void)checkInterval{
    self.flagMinus = NO;
    
    for (int i=0;i<[self.listData count];i++) {
        [self countIntervalArray:i];
    }
    
    self.idxIS = 0;
    self.leastInterval = 0;
    
    if (self.flagMinus == NO) {
        [self countIntervalPlus];
    } else {
        for (int j=0;j<[_arrInterval count];j++) {
            if ([[_arrInterval objectAtIndex:j] intValue] > 0) {
                
            } else {
                if (self.leastInterval == 0) {
                    self.idxIS = j;
                    self.leastInterval = abs([[_arrInterval objectAtIndex:j] intValue]);
                } else {
                    if (abs([[_arrInterval objectAtIndex:j] intValue]) < self.leastInterval) {
                        self.idxIS = j;
                        self.leastInterval = abs([[_arrInterval objectAtIndex:j] intValue]);
                    }
                }
            }
        }
    }
    //skipping imsak
    if(self.idxIS == 0){
        self.idxIS +=1;
    }
    _selectedTime = YES;
}

- (BOOL) isSelectedTime{
    return _selectedTime;
}

- (int) getIdxIS{
    return _idxIS;
}

-(void)updateInterval:(int)no {
    NSDate * now = [NSDate date];
    
    NSLocale *idIDPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"id_ID_POSIX"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setLocale:idIDPOSIXLocale];
    [outputFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [outputFormatter setDateFormat:@"HH:mm:ss"];

    NSString *date1 = [outputFormatter stringFromDate:now];
    NSString *date2 = [[_listData objectAtIndex:no] stringByAppendingString:@":00"];
    NSArray *arr1 = [date1 componentsSeparatedByString:@":"];
    NSArray *arr2 = [date2 componentsSeparatedByString:@":"];
    
    if (_flagMinus == YES) {
//        NSDate * now = [NSDate date];
//        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//        [outputFormatter setDateFormat:@"HH:mm:ss"];
//        NSString *date1 = [outputFormatter stringFromDate:now];
//        NSString *date2 = [[_listData objectAtIndex:no] stringByAppendingString:@":00"];
//        NSArray *arr1 = [date1 componentsSeparatedByString:@":"];
//        NSArray *arr2 = [date2 componentsSeparatedByString:@":"];
        int timeValue1 = ([arr1[0] intValue] * 3600) + ([arr1[1] intValue] * 60) + [arr1[2] intValue];
        int timeValue2 = ([arr2[0] intValue] * 3600) + ([arr2[1] intValue] * 60) + [arr2[2] intValue];
        _leastInterval = abs(timeValue1 - timeValue2);
    } else if (_flagMinus == NO) {
//        NSDate * now = [NSDate date];
//        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
//        [outputFormatter setDateFormat:@"HH:mm:ss"];
//        NSString *date1 = [outputFormatter stringFromDate:now];
//        NSString *date2 = [[_listData objectAtIndex:no] stringByAppendingString:@":00"];
//        NSArray *arr1 = [date1 componentsSeparatedByString:@":"];
//        NSArray *arr2 = [date2 componentsSeparatedByString:@":"];
        int timeValue1 = ([arr1[0] intValue] * 3600) + ([arr1[1] intValue] * 60) + [arr1[2] intValue];
        int timeValue2 = (24*3600) + ([arr2[0] intValue] * 3600) + ([arr2[1] intValue] * 60) + [arr2[2] intValue];
        _leastInterval = abs(timeValue2 - timeValue1);
    }
}

- (NSString *)getImsakTime:(NSString *)strTime {
    NSArray *arrTime = [strTime componentsSeparatedByString:@":"];
    int hoursTime = [arrTime[0] intValue];
    int minutesTime = [arrTime[1] intValue];
    int hoursImsak = 0;
    int minutesImsak = 0;
    if (minutesTime < 10) {
        hoursImsak = hoursTime - 1;
        minutesImsak = 50 + minutesTime;
    } else {
        hoursImsak = hoursTime;
        minutesImsak = minutesTime - 10;
    }
    
    NSString *strOutput = @"";
    if (hoursImsak < 10) {
        if (minutesImsak < 10) {
            strOutput = [NSString stringWithFormat:@"0%d:0%d",hoursImsak,minutesImsak];
        } else {
            strOutput = [NSString stringWithFormat:@"0%d:%d",hoursImsak,minutesImsak];
        }
    } else {
        if (minutesImsak < 10) {
            strOutput = [NSString stringWithFormat:@"%d:0%d",hoursImsak,minutesImsak];
        } else {
            strOutput = [NSString stringWithFormat:@"%d:%d",hoursImsak,minutesImsak];
        }
    }
    
    return strOutput;
}

- (void)countIntervalArray:(int)id {
    NSDate * now = [NSDate date];
    NSLocale *idIDPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"id_ID_POSIX"];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setLocale:idIDPOSIXLocale];
    [outputFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    
    NSString *date1 = [outputFormatter stringFromDate:now];
    NSString *date2 = [[_listData objectAtIndex:id] stringByAppendingString:@":00"];
    NSArray *arr1 = [date1 componentsSeparatedByString:@":"];
    NSArray *arr2 = [date2 componentsSeparatedByString:@":"];
    int timeValue1 = ([arr1[0] intValue] * 3600) + ([arr1[1] intValue] * 60) + [arr1[2] intValue];
    int timeValue2 = ([arr2[0] intValue] * 3600) + ([arr2[1] intValue] * 60) + [arr2[2] intValue];
    int interval = timeValue1 - timeValue2;
    [_arrInterval insertObject:[NSNumber numberWithInt:interval] atIndex:id];
    
    if (interval < 0) {
        _flagMinus = YES;
    }
}

- (void)countIntervalPlus {
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    NSString *date1 = [outputFormatter stringFromDate:now];
    NSString *date2 = [[_listData objectAtIndex:0] stringByAppendingString:@":00"];
    NSArray *arr1 = [date1 componentsSeparatedByString:@":"];
    NSArray *arr2 = [date2 componentsSeparatedByString:@":"];
    int timeValue1 = ([arr1[0] intValue] * 3600) + ([arr1[1] intValue] * 60) + [arr1[2] intValue];
    int timeValue2 = (24*3600) + ([arr2[0] intValue] * 3600) + ([arr2[1] intValue] * 60) + [arr2[2] intValue];
    _leastInterval = abs(timeValue2 - timeValue1);
}

- (NSString *)getTimeDiff:(int)interval {
    int hh = interval/3600;
    int rem = fmod(interval,3600);
    int mm = rem / 60;
    rem = fmod(rem, 60);
    int ss = rem;
    
    NSString *hstr = @"";
    NSString *mstr = @"";
    NSString *sstr = @"";
    
    if (abs(hh) < 10) {
        hstr = [NSString stringWithFormat:@"0%d",abs(hh)];
    } else {
        hstr = [NSString stringWithFormat:@"%d",abs(hh)];
    }
    
    if (abs(mm) < 10) {
        mstr = [NSString stringWithFormat:@"0%d",abs(mm)];
    } else {
        mstr = [NSString stringWithFormat:@"%d",abs(mm)];
    }
    
    if (abs(ss) < 10) {
        sstr = [NSString stringWithFormat:@"0%d",abs(ss)];
    } else {
        sstr = [NSString stringWithFormat:@"%d",abs(ss)];
    }
    
    return [NSString stringWithFormat:@"%@:%@:%@",hstr,mstr,sstr];
}

- (void) callPrayerTime{
    PrayTime *prayerTime = [[PrayTime alloc] initWithJuristic:JuristicMethodShafii
                                                   andCalculation:CalculationMethodCustom];
    [prayerTime setTimeFormat:TimeFormat24Hour];
    NSArray *tempNames = [prayerTime getTimeNames];

    _listNames = [[NSMutableArray alloc] init];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"en"]){
        _listNames[0] = @"Imsak";
        _listNames[1] = @"Fajr";
        _listNames[2] = @"Sunrise";
        _listNames[3] = @"Dhuhr";
        _listNames[4] = @"Asr";
        _listNames[5] = @"Maghrib";
        _listNames[6] = @"Isha";
    } else {
        _listNames[0] = @"Imsak";
        _listNames[1] = tempNames[0];
        _listNames[2] = tempNames[1];
        _listNames[3] = @"Dzuhur";
        _listNames[4] = tempNames[3];
        _listNames[5] = tempNames[4];
        _listNames[6] = tempNames[5];
    }

    [prayerTime setDhuhrMinutes:3];

    NSMutableArray *times2 = [prayerTime prayerTimesDate:_currentDate
                                                latitude:_latitude
                                               longitude:_longitude
                                             andTimezone:[prayerTime getTimeZone]];

    _times = [NSMutableArray arrayWithCapacity:6];
    _times[0] = times2[0];
    _times[1] = times2[1];
    _times[2] = times2[2];
    _times[3] = times2[3];
    _times[4] = times2[4];
    _times[5] = times2[6];
}

- (void) getListData{
    _listData = [[NSMutableArray alloc]init];
    _listData[0] = [self getImsakTime:_times[0]];
    _listData[1] = _times[0];
    _listData[2] = _times[1];
    _listData[3] = _times[2];
    _listData[4] = _times[3];
    _listData[5] = _times[4];
    _listData[6] = _times[5];
    
    _arrListTimes = (NSArray*) _listData;
    _arrInterval = [NSMutableArray arrayWithCapacity:[_listData count]];
    
    [self checkInterval];
}

- (NSString*) getWaktu{
    NSString *waktu = [NSString stringWithFormat:@"%@",[self getTimeSholat:[_listData objectAtIndex:_idxIS] noIndex:(_idxIS)]];
    return waktu;
}

- (NSString*) getTimeAdzan{
    return [[_listNames objectAtIndex:_idxIS] stringByAppendingString:[NSString stringWithFormat:@" (%@)",[self getTimeDifference]]];
}

- (NSString*) getWaktuSholat{
    return  [_listNames objectAtIndex:_idxIS];
}

- (NSString*) getJadwalSholat{
    NSString *time = [self getWaktu];
    NSString *timeDef = [self getTimeDifference];
    if(_placemarkLocality != nil){
        return [NSString stringWithFormat:@"%@, %@ - %@ (%@)",_placemarkLocality, [_listNames objectAtIndex:_idxIS], time, timeDef];
    }else{
        return [NSString stringWithFormat:@"%@ - %@ (%@)", [_listNames objectAtIndex:_idxIS], time, timeDef];
    }
}

+ (NSMutableAttributedString*) getAttributedSalam{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *cname = [userDefault valueForKey:@"customer_name"];
    
    NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
    NSMutableAttributedString *salam = [[NSMutableAttributedString alloc]initWithString:@"Assalamu'alaikum" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name3 size:13], NSForegroundColorAttributeName : [UIColor blackColor]}];
    NSMutableAttributedString *name = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@", %@", cname] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13], NSForegroundColorAttributeName : [UIColor blackColor]}];
    
        [final appendAttributedString:salam];
    if(cname != nil){
        [final appendAttributedString:name];
    }
    return final;
}

- (NSMutableAttributedString*) getAttributedJadwalSholat{
    /*with Syariapoint*/
    NSMutableAttributedString *final;
    NSMutableAttributedString *place;
    NSMutableAttributedString *adzan;
    NSMutableAttributedString *time;
    NSMutableAttributedString *timeDiff;
    
    if(_showSyariahpoint){
        
        final = [[NSMutableAttributedString alloc]initWithString:@""];
        place = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@ \n%@ ",_placemarkLocality, [_listNames objectAtIndex:_idxIS]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13], NSForegroundColorAttributeName : [UIColor blackColor]}];
        adzan = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Adzan %@", [_listNames objectAtIndex:_idxIS]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name3 size:17], NSForegroundColorAttributeName : UIColorFromRGB(const_color_secondary)}];
        time = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",[self getWaktu]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name3 size:17], NSForegroundColorAttributeName : UIColorFromRGB(const_color_secondary)}];
        timeDiff = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" (-%@)",[self getTimeDifference]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:11], NSForegroundColorAttributeName : UIColorFromRGB(0x888888)}];
    }
    else{
        /*default*/
        final = [[NSMutableAttributedString alloc]initWithString:@""];
        place = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@, %@ ",_placemarkLocality, [_listNames objectAtIndex:_idxIS]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13], NSForegroundColorAttributeName : [UIColor blackColor]}];
        adzan = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"Adzan %@", [_listNames objectAtIndex:_idxIS]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name3 size:17], NSForegroundColorAttributeName : UIColorFromRGB(const_color_secondary)}];
        time = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" %@",[self getWaktu]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name3 size:17], NSForegroundColorAttributeName : UIColorFromRGB(const_color_secondary)}];
        timeDiff = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" (-%@)",[self getTimeDifference]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:11], NSForegroundColorAttributeName : UIColorFromRGB(0x888888)}];
    }

	
    if(_placemarkLocality != nil){
        [final appendAttributedString:place];
        NSLog(@"%@",[self getTimeDifference]);
        if(![[self getTimeDifference]isEqualToString:@"00:00:00"]){
            [final appendAttributedString:time];
            [final appendAttributedString:timeDiff];
        }else{
            [final appendAttributedString:adzan];
            [final appendAttributedString:time];
        }
    }
    return final;
    
}

- (NSArray*) getListNames{
    return _listNames;
}

- (NSArray*) getListTimes{
    return _listData;
}

- (NSString*) getTimeDifference{
    return [self getTimeDiff:_leastInterval];
}

- (void)setPlacemarkLocality:(NSString *)placemarkLocality{
    _placemarkLocality = placemarkLocality;
}

- (void) proceedTick{
    NSString *dt = [self getDayTimes];
    NSLog(@"Day Times : %@",dt);
    if ([dt isEqualToString:@"00:00:00"]) {
        NSString *date2 = [[_listData objectAtIndex:0] stringByAppendingString:@":00"];
        NSArray *arr1 = [dt componentsSeparatedByString:@":"];
        NSArray *arr2 = [date2 componentsSeparatedByString:@":"];
        int timeValue1 = ([arr1[0] intValue] * 3600) + ([arr1[1] intValue] * 60) + [arr1[2] intValue];
        int timeValue2 = ([arr2[0] intValue] * 3600) + ([arr2[1] intValue] * 60) + [arr2[2] intValue];
        _leastInterval = abs(timeValue1 - timeValue2);
    }
    
    _leastInterval = _leastInterval - 1;
}

- (void) setNotificationData{
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    NSMutableArray *listAdzanNotif = [[NSMutableArray alloc]init];
    
    if([userdefault objectForKey:@"data_adzan_notif"] != nil){
        NSMutableArray *myArrayMut = [[userdefault valueForKey:@"data_adzan_notif"] mutableCopy];
        int i = 0;
        while (i < _listData.count) {
            NSMutableDictionary *dict = [myArrayMut[i] mutableCopy];
            NSString *place = @"";
            if(_placemarkLocality){
                place = _placemarkLocality;
            }
            if(![place isEqualToString:@""]){
                [dict setValue:place forKey:@"place"];
            }
            [dict setValue:[self getTimeSholat:_listData[i] noIndex:i] forKey:@"time"];
            [dict setValue:_listNames[i] forKey:@"title"];
            
            myArrayMut[i] = dict;
            [SholluLib createNotif:i withData:dict];
            i++;
        }
        [userdefault setObject:[myArrayMut copy] forKey:@"data_adzan_notif"];
        [userdefault synchronize];
    }else{
        int i = 0;
        while (i < _listData.count) {
            NSString *place = @"";
            if(_placemarkLocality){
                place = _placemarkLocality;
            }
            
            listAdzanNotif[i] = @{
                @"time" : _listData[i],
                @"mode" : @"2",
                @"alarm" : @"0",
                @"title" : _listNames[i],
                @"place" : place
            };
            i++;
        }
        [userdefault setObject:listAdzanNotif forKey:@"data_adzan_notif"];
    }
    
}

+ (void) createNotif:(NSInteger) selectedTime withData:(NSDictionary*)dict{
    NSString *identifier = [NSString stringWithFormat:@"SHOLAT_TIME_%ld",selectedTime];
    NSString *identifierAlarm = [NSString stringWithFormat:@"SHOLAT_TIME_ALARM%ld",selectedTime];
    NSString *title = @"";
    NSString *title_reminder = @"";
    NSString *message = @"";
    
    if([Utility isLanguageID]){
        title = [NSString stringWithFormat:@"Waktunya %@ Pukul %@",[dict objectForKey:@"title"], [dict objectForKey:@"time"]];
        title_reminder = [NSString stringWithFormat:@"%@ menit menuju waktu %@ pukul %@",[dict objectForKey:@"alarm"],[dict objectForKey:@"title"], [dict objectForKey:@"time"]];
        message = [NSString stringWithFormat:@"Lihat informasi waktu sholat di %@",[dict objectForKey:@"place"]];
    }else{
        title = [NSString stringWithFormat:@"%@ time at %@",[dict objectForKey:@"title"], [dict objectForKey:@"time"]];
        title_reminder = [NSString stringWithFormat:@"%@ minutes to %@ time at %@",[dict objectForKey:@"alarm"],[dict objectForKey:@"title"], [dict objectForKey:@"time"]];
        message = [NSString stringWithFormat:@"See prayer time in %@",[dict objectForKey:@"place"]];
    }
    
    if(selectedTime == 0 || selectedTime == 2){
        if([[dict objectForKey:@"mode"]intValue] == 2){
            [PrayerNotification cancelNotification:identifier];
            [PrayerNotification cancelNotification:identifierAlarm];
        }else{
            NSArray *time = [[dict objectForKey:@"time"]componentsSeparatedByString:@":"];
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"HH:mm"];
            NSDate *currentTime = [dateFormat dateFromString:[dict objectForKey:@"time"]];
            NSDate* newDate = [currentTime dateByAddingTimeInterval:[[dict objectForKey:@"alarm"] integerValue]*-60];
            NSString *jamSholat = [dateFormat stringFromDate:newDate];
            NSArray *alarmTime = [jamSholat componentsSeparatedByString:@":"];
            
            if([[dict objectForKey:@"mode"]intValue] == 0){
                //Notification For the time
                [PrayerNotification createNotification:title message:message hour:[time[0]intValue] minute:[time[1]intValue] soundNamed:@"ringtone.mp3" repeats:YES uniqIdentfiedAs:identifier];
                
                //Notification for reminder
                if([[dict objectForKey:@"alarm"]intValue] != 0){
                    [PrayerNotification createNotification:title_reminder message:message hour:[alarmTime[0]intValue] minute:[alarmTime[1]intValue] soundNamed:@"ringtone.mp3" repeats:YES uniqIdentfiedAs:identifierAlarm];
                }
            }else{
                //Notification for the time
                [PrayerNotification createNotification:title message:message hour:[time[0]intValue] minute:[time[1]intValue] soundNamed:nil repeats:YES uniqIdentfiedAs:identifier];
                //Notification for reminder
                if([[dict objectForKey:@"alarm"]intValue] != 0){
                    [PrayerNotification createNotification:title_reminder message:message hour:[alarmTime[0]intValue] minute:[alarmTime[1]intValue] soundNamed:nil repeats:YES uniqIdentfiedAs:identifierAlarm];
                }
            }
            
            if([[dict objectForKey:@"alarm"]intValue] == 0){
                [PrayerNotification cancelNotification:identifierAlarm];
            }
        }
    }else{
        if([[dict objectForKey:@"mode"]intValue] == 3){
            [PrayerNotification cancelNotification:identifier];
            [PrayerNotification cancelNotification:identifierAlarm];
        }else{
            NSArray *time = [[dict objectForKey:@"time"]componentsSeparatedByString:@":"];

            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"HH:mm"];
            NSDate *currentTime = [dateFormat dateFromString:[dict objectForKey:@"time"]];
            NSDate* newDate = [currentTime dateByAddingTimeInterval:[[dict objectForKey:@"alarm"] integerValue]*-60];
            NSString *jamSholat = [dateFormat stringFromDate:newDate];
            NSArray *alarmTime = [jamSholat componentsSeparatedByString:@":"];
            
            if([[dict objectForKey:@"mode"]intValue] == 0){
                //notification for adhan
                [PrayerNotification createNotification:title message:message hour:[time[0]intValue] minute:[time[1]intValue] soundNamed:@"adzan.mpeg" repeats:YES uniqIdentfiedAs:identifier];
                
                //notification for reminder
                if([[dict objectForKey:@"alarm"]intValue] != 0){
                    [PrayerNotification createNotification:title_reminder message:message hour:[alarmTime[0] intValue] minute:[alarmTime[1]intValue] soundNamed:@"ringtone.mp3" repeats:YES uniqIdentfiedAs:identifierAlarm];
                }
            }else if([[dict objectForKey:@"mode"]intValue] == 1){
                //notification for adhan
                [PrayerNotification createNotification:title message:message hour:[time[0]intValue] minute:[time[1]intValue] soundNamed:@"ringtone.mp3" repeats:YES uniqIdentfiedAs:identifier];
                
                //Notification for reminder
                if([[dict objectForKey:@"alarm"]intValue] != 0){
                    [PrayerNotification createNotification:title_reminder message:message hour:[alarmTime[0]intValue] minute:[alarmTime[1]intValue] soundNamed:@"ringtone.mp3" repeats:YES uniqIdentfiedAs:identifierAlarm];
                }
            }else{
                //notification for adhan
                [PrayerNotification createNotification:title message:message hour:[time[0]intValue] minute:[time[1]intValue] soundNamed:nil repeats:YES uniqIdentfiedAs:identifier];
                
                //Notification for reminder
                if([[dict objectForKey:@"alarm"]intValue] != 0){
                    [PrayerNotification createNotification:title_reminder message:message hour:[alarmTime[0]intValue] minute:[alarmTime[1]intValue] soundNamed:nil repeats:YES uniqIdentfiedAs:identifierAlarm];
                }
            }
            
            if([[dict objectForKey:@"alarm"]intValue] == 0){
                [PrayerNotification cancelNotification:identifierAlarm];
            }
        }
    }
}


@end
