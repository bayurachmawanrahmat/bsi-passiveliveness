//
//  Utility.h
//  Aurora
//
//  Created by Adhitya Purwareksa on 12/12/13.
//  Copyright (c) 2013 Inmagine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (UIImage*) getImageJPG:(NSString*)imageName;
+ (NSString *)getRequestType:(NSString *)data;
+ (NSMutableDictionary *)translateParam:(NSString *)urlParm;
+ (NSArray *)changeToArray:(NSArray *)tempArray;
+ (NSArray *)changeToArray:(NSArray *)tempArray combinedSub:(BOOL)combinedSub;
+ (NSString *)datatosend:(NSString *)data;
+ (void)showMessage : (NSString *) nMessage mVwController : (UIViewController *) nVc mTag: (int)nTag;
+(NSAttributedString *)htmlAtributString : (NSString *) strTextHtml;
+(NSString *)convertJsonString : (NSObject *) mSource;
+(NSString *) dateConvert : (NSString *)strDate
                     mType:(int)nType;
+(NSString *)stringTrim : (NSString *) mStr;
//method adding by angger
+ (NSString*) localFormatCurrencyValue:(double)value showSymbol:(BOOL)setSymbol;
//method adding by alikhsan
+ (void)stopTimer;
+ (NSString*) formatCurrencyValue:(double)value;
+ (NSString *) currencyVal: (double)value;
+ (NSString*) messageGenerated:(NSString *) rc
                       defValu: (NSString *) defaultValue;
+(NSString *) dateToday : (int) type;
+(NSDate *)dateStringToDate : (NSString *) dateString
                      mType : (int)type;
+(NSString *) calculateHMAC:(NSString *) mData
                        key: (NSString *) mKey;
+ (CGFloat) dynamicLabelHeight : (UILabel *) mLabel;
+ (CGFloat)heightLabelDynamic :(UILabel *) mLabel
                   sizeFont:(float) mSizeFont;
+(float)heightLabelDynamic :(UILabel *) mLabel
                  sizeWidth:(float) mSizeWidth
                   sizeFont:(float) mSizeFont;
+(bool) vcNotifConnection :(NSString *) mNameVC;

+(NSMutableURLRequest *)BSMHeader : (NSString *) mUrl;
+ (BOOL)validateEmailWithString:(NSString*)checkString;

+ (NSString *)deviceIPAddress;

+ (UIColor *)randomColor;

+ (void)animeBounchCb :(UIButton *) sender;

+ (NSString *) postRequestUrl : (NSString *)urlParam
                    encrypted : (bool)mEncrypted;

+ (NSMutableDictionary *)keychainItemForKey:(NSString *)key;
+ (OSStatus)setValue:(NSString *)value forKey:(NSString *)key;
+ (OSStatus)setDict:(NSDictionary *)dict forKey:(NSString *)key;
+ (NSString *)valueForKeychainKey:(NSString *)key;
+ (OSStatus)removeForKeychainKey:(NSString *)key;
+ (NSDictionary *)dictForKeychainKey:(NSString *)key;

+ (NSAttributedString *) setLineSpasing : (NSString *) strText
                           mSpaseSize : (int) lineSpase;
+(NSString *) findArrConstain : (NSArray *) data
                       strFind:(NSString *) mStr;
+(NSString *) findArrConstain : (NSArray *) data
                       strFind:(NSString *) mStr
                    strMenuId : (NSString *) menuId;
+(BOOL) arContain : (NSArray *) data
          mString : (NSString *) str;

+(CGSize) heightWrapWithText : (NSString *) strText
                    fontName : (UIFont *) mFont
                expectedSize : (CGSize) mExpedtedSize;

+ (UIImage *)image1x1WithColor:(UIColor *)color;
+(CGRect) converRectOfInterest : (CGRect)vwScanner;
+(UILabel *) lblTitleQR;
+(UILabel *) lblTitleQRIS;
+(NSString *) stringTrimmer : (NSString *) strText;
+(DLAVAlertView *) showProgressDialog : (NSString *) strTitle
                      strMessage : (NSString *) mStrMessage;
+(NSString *) deviceName;
+(NSString *) deviceType;
+(NSString *) osVersion;
+(NSString *) deviceOsVersion;

+(NSString *) splitDateWithFormat : (int) format
                        dateString: (NSString *) mDate;
+(CAShapeLayer *) makeARoundedCornerTopLeftAndRight : (UIView *) obView;
+(void) resetTimer;
+ (CGSize)optimalSizeForLabel:(UILabel *)label inMaxSize:(CGSize)maxSize;
+(void) goToBackNavigator : (UINavigationController *) navController;
+(CGFloat)getLabelHeight:(NSString *)label width:(CGFloat) width font:(UIFont*) font;
+ (NSString*) compressAndEncodeToBase64String : (UIImage *)image withMaxSizeInKB:(float) maximumSize;
+ (NSString*) compressandencodebase64string : (UIImage *)image withMaxSizeInKB:(float) maximumSize;
+ (NSString*) providerCheck : (NSString*) msisdn;

+(void) setEventTracker:(NSString*)name withParam:(NSDictionary* _Nullable)dict;
+(void) showMessage:(NSString* _Nonnull)message instance:(id _Nonnull)instanceID;

+ (void)showMessage: (NSString*) message instance : (id _Nullable)instanceID;
+ (void)showMessage: (NSString*_Nullable) messageID enMessage: (NSString*_Nullable) messageEN instance : (id)instanceID;
+ (BOOL) isLanguageID;
+ (NSMutableParagraphStyle*) getParagraphStyle;
+ (UIImage *)imageFromLayer:(CALayer *)layer;
+ (UIView*) showNoData : (UIView*)view;
+ (void) addAccountData : (NSDictionary*)jsonObject;
+ (BOOL) isDeviceHaveNotch;
+ (BOOL) isDeviceHaveNotch2;
+ (BOOL) isDeviceHaveBottomPadding;
+ (BOOL) isOTPPrefixRight : (NSString*)otpcard;

+ (NSString*)sha256HashFor:(NSString*)input;
+(BOOL)isAlphabetOnly:(NSString*)text;
+(BOOL)isNumericOnly:(NSString*)text;
+(NSString*)getUUID;
+ (UIView*) showTextNoData : (UIView*)view;
+ (NSString*) formatCurrencyValue:(double)value withSeparator:(NSString*)separator;
+ (void) removeUserData;
+ (CAGradientLayer*)gradientForFrame:(CGRect)frame;
+ (void)removeAllArrangedViewOf:(UIStackView*) stackview;

@end
