//
//  ProgressView.h
//  BSM-Mobile
//
//  Created by ARS on 25/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ProgressViewDelegate;

@interface ProgressView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *progressText;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@property (weak, nonatomic) id<ProgressViewDelegate> delegate;

@end

@protocol ProgressViewDelegate <NSObject>

@end

NS_ASSUME_NONNULL_END
