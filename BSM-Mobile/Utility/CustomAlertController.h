//
//  CustomAlertController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 06/12/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomAlertController : UIAlertController

@end

NS_ASSUME_NONNULL_END
