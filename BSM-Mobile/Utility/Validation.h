//
//  Validation.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Validation : NSObject{
    id thisInstance;
}

- (id)initWith:(id)instance;

- (BOOL)validateEmptyField:(NSArray*)arra;
   
- (BOOL)validateEmail:(NSString*)checkString;

- (BOOL)validateDebitCard:(NSString*)debitCard;

- (BOOL)validatePIN:(NSString*)pin;

- (BOOL)validateKTP:(NSString*)ktp;

- (BOOL)validatePhoneNumber:(NSString*)noHp;

- (BOOL)validateBirthDate:(NSString*)birthDate;

@end

NS_ASSUME_NONNULL_END
