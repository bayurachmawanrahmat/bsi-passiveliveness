//
//  OnboardingButton.m
//  BSM-Mobile
//
//  Created by ARS on 12/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "OnboardingButton.h"

@implementation OnboardingButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        self.layer.cornerRadius = 25;
        self.layer.masksToBounds = true;
        self.layer.borderWidth = 0;
        [self setBackgroundColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1/1.0]];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 25;
        self.layer.masksToBounds = true;
        self.layer.borderWidth = 0;
        [self setBackgroundColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1/1.0]];
    }
    return self;
}

@end
