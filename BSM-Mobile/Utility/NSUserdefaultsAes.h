//
//  NSU+AES.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 19/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserdefaultsAes : NSObject

+ (void) setObject:(id _Nullable)object forKey:(id)key;

+ (void) removeValueForKey:(id)key;

+ (id) getValueForKey:(id)key;

@end

NS_ASSUME_NONNULL_END
