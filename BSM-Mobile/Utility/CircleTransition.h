//
//  CircleTransition.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum
{
  present = 0,
  dismiss = 1,
  pop = 2
} CircularTransitionMode;

@interface CircleTransition : NSObject <UIViewControllerAnimatedTransitioning>

- (void) setTransitionMode : (CircularTransitionMode) mode;
- (void) setStartingPoint : (CGPoint) point;
- (void) setCircleColor : (UIColor*) color;

@end

NS_ASSUME_NONNULL_END
