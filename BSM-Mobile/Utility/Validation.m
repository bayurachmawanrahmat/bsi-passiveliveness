//
//  Validation.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "Validation.h"
#import "Utility.h"

@implementation Validation

- (id)init{
    self = [super init];
    if(self){
    }
    return self;
}

- (id)initWith:(id)newInstance{
    thisInstance = newInstance;
    self = [super init];
    if(self){
    }
    return self;
}

- (BOOL)validateEmail:(NSString*)checkString{
    if([checkString isEqualToString:@""]){
        [Utility showMessage:@"Email wajib diisi" enMessage:@"Email is required" instance:thisInstance];
        return NO;
    }else
    if(![Utility validateEmailWithString:checkString]){
        [Utility showMessage:@"Email tidak valid, periksa kembali data yang di masukkan" enMessage:@"Email is not valid" instance:thisInstance];
        return NO;
    }
    return YES;
}

- (BOOL)validateDebitCard:(NSString*)debitCard{
    if([debitCard isEqualToString:@""]){
        [Utility showMessage:@"Nomor Kartu wajib diisi" enMessage:@"Card Number is required" instance:thisInstance];
        return NO;
    }else
    if([debitCard length] != 16 || [debitCard doubleValue] == 0){
        [Utility showMessage:@"Data Kartu Debit tidak valid, periksa kembali data yang di masukkan"
                   enMessage:@"Debit Card data is not valid, recheck your entered data" instance:thisInstance];
        return NO;
    }
    return YES;
}

- (BOOL)validatePIN:(NSString*)pin{
    if([pin length] != 6 || [pin doubleValue] == 0){
        [Utility showMessage:@"PIN harus 6 digit" enMessage:@"PIN must be 6 digits" instance:thisInstance];
        return NO;
    }
    return YES;
}

- (BOOL)validateBirthDate:(NSString*)birthDate{
    if([birthDate isEqualToString:@""]){
        [Utility showMessage:@"Tanggal lahir wajib di isi" enMessage:@"Birth date is required" instance:thisInstance];
        return NO;
    }
    return YES;
}

- (BOOL)validateKTP:(NSString*)ktp{
    if([ktp isEqualToString:@""]){
        [Utility showMessage:@"Nomor KTP wajib diisi" enMessage:@"KTP Number is required" instance:thisInstance];
        return NO;
    }else
    if([ktp length] != 16 || [ktp doubleValue] == 0){
        [Utility showMessage:@"Nomor KTP tidak valid, periksa kembali data yang di masukkan" enMessage:@"KTP Number is not valid, recheck your entered data" instance:thisInstance];
        return NO;
    }
    return YES;
}

- (BOOL)validatePhoneNumber:(NSString*)noHp{
    if([noHp doubleValue] == 0){
        [Utility showMessage:@"Nomor handphone tidak valid, periksa kembali data yang di masukkan" enMessage:@"Phonenumber is not valid, recheck your entered data" instance:thisInstance];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmptyField:(NSArray*)arra{
    for(NSString *str in arra){
        if([str isEqualToString:@""]){
            return NO;
        }
    }
    return YES;
}


@end
