//
//  Connection.m
//  Aurora
//
//  Created by lds on 12/23/13.
//  Copyright (c) 2013 Inmagine. All rights reserved.
//

#import "Connection.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "DLAVAlertView.h"
#import "Utility.h"
#import "DESChiper.h"
#import "InboxHelper.h"
#import "AppProperties.h"
#import "NSUserdefaultsAes.h"

@interface Connection()<UIAlertViewDelegate>{
    BOOL temp;
    //    BOOL first;
    BOOL requestZPK;
    NSString *keyDes;
    DESChiper *desChipper;
    NSMutableData *data;
    DLAVAlertView *loadingAlert;

}
@end

@implementation Connection

@synthesize delegate = _delegate;

#pragma mark - Global Method

- (id)initWithDelegate:(id)delegate {
    self = [super init];
    if(self) {
        self.delegate = delegate;
        desChipper = [[DESChiper alloc]init];
        data = [[NSMutableData alloc]init];
        //        first = true;
    }
    return self;
}

- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking{
    continueLoading = false;
    [self sendPostParmUrl:parmUrl needLoading:needLoading textLoading:nil encrypted:encrypted banking:isBanking];
}
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking favorite:(BOOL)aFavorite{
    self.fav = aFavorite;
    continueLoading = false;
    [self sendPostParmUrl:parmUrl needLoading:needLoading textLoading:nil encrypted:encrypted banking:isBanking];
}

- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading  encrypted:(BOOL)encrypted banking:(BOOL)isBanking continueLoading:(BOOL)aContinueLoading{
    continueLoading = aContinueLoading;
    [self sendPostParmUrl:parmUrl needLoading:needLoading textLoading:textLoading encrypted:encrypted banking:isBanking];
}
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking{
    
    //    [self showLoading:textLoading];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    DLog(@"URL Parm: %@", parmUrl);
    NSMutableDictionary *dataParam = [Utility translateParam:parmUrl];
    DLog(@"DATA Parm: %@", dataParam);
    
    requestType = [dataParam valueForKey:@"request_type"];
    needDecrypt = isBanking || [requestType isEqualToString:@"activation"] || [requestType isEqualToString:@"activation_fr"] || self.fav;
    if([requestType isEqualToString:@"list_merchant"] || [requestType isEqualToString:@"list_bank"]){
        needDecrypt = false;
    }
    temp = [requestType isEqualToString:@"list_account"];
    
    if ([requestType isEqualToString:@"check_notif"]) {
        self.fav = false;
    }
    
    if(self.fav){
        NSMutableDictionary *favData = [dataParam mutableCopy];
        [favData setObject:@"insert_favorite" forKey:@"request_type"];
        [[DataManager sharedManager]setFavData:favData];
    }
    
    [self sendPost:dataParam needLoading:needLoading textLoading:textLoading encrypted:encrypted];
}
- (void)sendPost:(NSMutableDictionary *)dataParam needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted{
#pragma mark check tabpos
    [self pushTimerVCConnection:@"stop_timer"];
    if(needLoading){
        if(!loadingAlert){
            [self showLoading:textLoading];
        }
        
    }
    
    NSDictionary *dict = @{@"dataParam":dataParam, @"encrypted":[NSNumber numberWithBool:encrypted]};
    [self performSelectorInBackground:@selector(sendData:) withObject:dict];
}


- (void) pushTimerVCConnection: (NSString *) actionTimer{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *tabPos = [userDefault valueForKey:@"tabPos"];
    
    if ([actionTimer isEqualToString:@"start_timer"]) {
        if ([tabPos isEqualToString:@"HOME"]) {
            NSDictionary* userInfo = @{@"position": @(1112)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        }else if([tabPos isEqualToString:@"WISDOM"]){
            NSDictionary* userInfo = @{@"actionTimer": @"START_TIMER"};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"wisdomTimer" object:self userInfo:userInfo];
        }else if([tabPos isEqualToString:@"ATM"]){
            NSDictionary* userInfo = @{@"actionTimer": @"START_TIMER"};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"ATMTimer" object:self userInfo:userInfo];
        }
    }else{
        if ([tabPos isEqualToString:@"HOME"]) {
            NSDictionary* userInfo = @{@"position": @(1113)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        }else if([tabPos isEqualToString:@"WISDOM"]){
            NSDictionary* userInfo = @{@"actionTimer": @"STOP_TIMER"};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"wisdomTimer" object:self userInfo:userInfo];
        }else if([tabPos isEqualToString:@"ATM"]){
            NSDictionary* userInfo = @{@"actionTimer": @"STOP_TIMER"};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"ATMTimer" object:self userInfo:userInfo];
        }
    }
}

- (void)sendData:(NSDictionary *)param{
        
    if(self.fav){
        requestType = @"fav";
    }
    NSMutableDictionary *dataParam = [param objectForKey:@"dataParam"];
    BOOL encrypted = [[param valueForKey:@"encrypted"]boolValue];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString *plainRequest = [self generateParamURL:dataParam];
    BOOL isActivation = [requestType isEqualToString:@"activation"] || [requestType isEqualToString:@"activation_fr"];
    NSString *datatosend =plainRequest;
    
    NSString *keyDesEncrypted;
    //
    if(encrypted){
        keyDes = @"";
        if(isActivation){
            keyDes = [dataParam valueForKey:@"activation_code"];
            keyDesEncrypted = @"ACT";
        }else{
            keyDes = [desChipper generateDESKey];
            DLog(@"keydes plain: %@", keyDes);
//            NSString *publicKeyServer = [userDefault valueForKey:@"publickey"];
            NSString *publicKeyServer = [NSUserdefaultsAes getValueForKey:@"publickey"];
            BDRSACryptorKeyPair *RSAKeyPair = [[BDRSACryptorKeyPair alloc] initWithPublicKey:publicKeyServer privateKey:NULL];
            
            BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
            BDError *error = [[BDError alloc] init];
            keyDesEncrypted = [RSACryptor encrypt:keyDes key:RSAKeyPair.publicKey error:error];
            DLog(@"%@", keyDesEncrypted);
        }
        plainRequest = [desChipper doDES:plainRequest key:keyDes];
    }else{
        NSData *dataBase64 = [plainRequest dataUsingEncoding:NSUTF8StringEncoding];
        plainRequest = [dataBase64 base64EncodedString];
    }
    
    if(keyDes)
        plainRequest = [NSString stringWithFormat:@"%@#%@", plainRequest, keyDesEncrypted];
    DLog(@"PLAIN REQUEST: %@", plainRequest);
    plainRequest = [self urlEncodeUsingEncoding:NSASCIIStringEncoding data:plainRequest];
    NSString *body = @"";
    
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc]initWithDictionary:@{@"req_data":plainRequest}];
    if([requestType isEqualToString:@"activation"] || [requestType isEqualToString:@"activation_fr"]){
        [requestData setObject:[dataParam valueForKey:@"msisdn"] forKey:@"msisdn"];
    }
    for (NSString *key in [requestData allKeys]){
        body = [NSString stringWithFormat:@"%@%@=%@&",body,key, [requestData valueForKey:key]];
    }
    body = [body substringToIndex:[body length]-1];
    
//    if([userDefault objectForKey:@"session_id"]){
//        body = [NSString stringWithFormat:@"%@&session_id=%@",body, [userDefault valueForKey:@"session_id"]];
//    }
    
    if([NSUserdefaultsAes getValueForKey:@"session_id"]){
        body = [NSString stringWithFormat:@"%@&session_id=%@",body, [NSUserdefaultsAes getValueForKey:@"session_id"]];
    }
    
    DLog(@"data to sent %@", body);
    
    
    if([requestType isEqualToString:@"list_merchant"] || [requestType isEqualToString:@"list_bank"] || [requestType isEqualToString:@"list_account"]|| [requestType isEqualToString:@"list_denom"]){
        NSError *jsonError;
        NSData *objectData = [datatosend dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json_parm = [NSJSONSerialization JSONObjectWithData:objectData
                                                                  options:NSJSONReadingMutableContainers
                                                                    error:&jsonError];
        
        if([requestType isEqualToString:@"list_merchant"] ){
            NSString *id_merchant = @"";
            if([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00013"]){// TELEPON
                id_merchant = @"1";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00025"]) { // VOUCHER HP
                id_merchant = @"2";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00020"]) { // RETAILER
                id_merchant = @"3";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00016"]) { // ANGSURAN
                id_merchant = @"4";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00017"]) { // PEMBAYARAN TIKET
                id_merchant = @"5";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00015"]) { // TV KABEL
                id_merchant = @"6";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00014"]) { // ACADEMIC
                id_merchant = @"7";
            } else if ([[json_parm valueForKey:@"menu_id"] isEqualToString:@"00018"]) { // ASURANSI
                id_merchant = @"8";
            }
        }
        
    }
    
    NSString *apiUrl = [NSString stringWithFormat:@"%@index.htm",API_URL];
    
    NSLog(@"USING URL: %@", apiUrl);

    NSMutableURLRequest *request = [Utility BSMHeader:apiUrl];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"UTF-8" forHTTPHeaderField:@"charset"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    connection = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask * dataTask = [connection dataTaskWithRequest:request];
    [dataTask resume];
}

#pragma mark - NSURLSessionTaskDelegate
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler{
    if ([AppProperties checkSslPinningIsOn]) {
        // Get remote certificate
        SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
        SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
        CFStringRef certSummary = SecCertificateCopySubjectSummary(certificate);
        NSString* certSummaryNs = (__bridge NSString*)certSummary;
        DLog(@"Server Trust Summary Name %@",certSummaryNs);
        DLog(@"Server Trust %@",serverTrust);

        // Set SSL policies for domain name check
        NSMutableArray *policies = [NSMutableArray array];
        [policies addObject:(__bridge_transfer id)SecPolicyCreateSSL(true, (__bridge CFStringRef)challenge.protectionSpace.host)];
        SecTrustSetPolicies(serverTrust, (__bridge CFArrayRef)policies);

        // Evaluate server certificate
        SecTrustResultType result;
        SecTrustEvaluate(serverTrust, &result);
        BOOL certificateIsValid = (result == kSecTrustResultUnspecified || result == kSecTrustResultProceed);

        // Get local and remote cert data
        NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));

        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"certificate" ofType:@"cer"];
//        NSString *pathToCert = [[NSBundle mainBundle]pathForResource:@"*.syariahmandiri.co.id" ofType:@"cer"];
        NSData *localCertificate = [NSData dataWithContentsOfFile:pathToCert];

        // The pinnning check
        if ([AppProperties checkIsProd]) {
            if ([remoteCertificateData isEqualToData:localCertificate] && certificateIsValid) {
                NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
                completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
            } else {
                completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, NULL);
            }
        }else{
            completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
        }
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    completionHandler(NSURLSessionResponseAllow);
}
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)aData
{
    [data appendData:aData];
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(!continueLoading){
        [self dismissLoading];
    }
    if(error == nil)
    {
        
        [self processData];
        NSLog(@"API is Succesfull");
    }
    else{
        if ([error code] == -1009)
        {
            [self handleError:lang(@"NEED_INET")];
        }
        else if ([error code] == -1001)
        {
            [self handleError:lang(@"ERROR_TIMEOUT")];
        }
        else
        {
            [self handleError:[NSString stringWithFormat:lang(@"ERROR_WITHCODE"), [error code]]];
        }
    }
    NSLog(@"Error %@",[error userInfo]);
}

- (void)processData{
    
    NSError* error;
    
    NSString *jsonString;
    BOOL needDecryptBase64 = true;
    if(needDecrypt || self.fav){
        
        NSString *tempString = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSASCIIStringEncoding];
        NSArray * listData = [tempString componentsSeparatedByString:@"#"];
        if(listData.count > 1){
//            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//            NSString *privateKeys = [userDefault valueForKey:@"privateKey"];
            NSString *privateKey = [NSUserdefaultsAes getValueForKey:@"privatekey"];
            BDRSACryptorKeyPair *RSAKeyPair = [[BDRSACryptorKeyPair alloc] initWithPublicKey:NULL privateKey:privateKey];
            
            BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
            BDError *error = [[BDError alloc] init];
            keyDes = [RSACryptor decrypt:listData[1] key:RSAKeyPair.privateKey error:error];
        }
        if(keyDes){
            NSData *tempData = [[NSData alloc]initWithBase64EncodedString:listData[0] options:NSDataBase64DecodingIgnoreUnknownCharacters];
            
            jsonString = [desChipper doDecryptDES:tempData key:keyDes];
            if(jsonString){
                needDecryptBase64 = false;
            }
            
        }
    }
    
    if(needDecryptBase64){
        NSString *tempString = [[NSString alloc] initWithBytes:data.bytes length:data.length encoding:NSUTF8StringEncoding];
        NSData *tempData = [[NSData alloc]initWithBase64EncodedString:tempString options:NSDataBase64DecodingIgnoreUnknownCharacters];
        
        jsonString = [[NSString alloc]initWithData:tempData encoding:NSASCIIStringEncoding];
//        jsonString = [[NSString alloc]initWithData:tempData encoding:NSISOLatin1StringEncoding];
    }
    
    
    NSLog(@"%@", jsonString);
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
     jsonString = [jsonString stringByReplacingOccurrencesOfString:@"null" withString:@""];
    
    
    DLog(@"RESPONSE: %@", jsonString);
    id json = [NSJSONSerialization
               JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
               options:NSJSONReadingAllowFragments
               error:&error];
//    id json = [NSJSONSerialization
//                              JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]
//                              options:NSJSONReadingMutableContainers
//                              error:&error];
    
    NSLog(@"%@", error );
    if ([json isKindOfClass:[NSDictionary class]]){
        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
//        [userDef setObject:@"1" forKey:KEY_HAS_SET_PIN];
        if ([json objectForKey:@"isreactivation"]){
            if([[json objectForKey:@"isreactivation"]isEqualToString:@"1"]){
                [userDef setObject:@"1" forKey:KEY_HAS_SET_PIN];
            } else {
                [userDef setObject:@"0" forKey:KEY_HAS_SET_PIN];
            }
        }
        if ([json objectForKey:@"clearZPK"]){
//            [userDef setObject:[json objectForKey:@"clearZPK"] forKey:BSM_SECRET_KEY];
            [NSUserdefaultsAes setObject:[json objectForKey:@"clearZPK"] forKey:BSM_SECRET_KEY];
        }
        if ([json objectForKey:@"session_id"]){
//            [userDef setObject:[json objectForKey:@"session_id"] forKey:KEY_SESSION_ID];
            [NSUserdefaultsAes setObject:[json objectForKey:@"session_id"] forKey:KEY_SESSION_ID];
        }
        
        if ([json objectForKey: @"transaction_id"]) {
            [userDef setObject:[json objectForKey:@"transaction_id"] forKey:@"transaction_id"];
        }
        
    }
    
    if(([json isKindOfClass:[NSDictionary class]] &&
        [json objectForKey:@"response_code"] &&
        ([[json objectForKey:@"response_code"]isEqualToString:@"0000"]))){
        [loadingAlert  dismissWithClickedButtonIndex:0 animated:YES];
        //        [DataManager sharedManager].continueLoading  = nil;
        requestZPK = true;
        
        [self sendPostParmUrl:@"request_type=login,customer_id" needLoading:true textLoading:@"Log into the application" encrypted:true banking:true continueLoading:false];
    }
    else if(([json isKindOfClass:[NSDictionary class]] &&
             [json objectForKey:@"response_code"] &&
             ([[json objectForKey:@"response_code"]isEqualToString:@"0001"]))){
//        NSLog(@"RESPONSE: %s", "new activation");
        NSString *token = [[NSUserDefaults standardUserDefaults]valueForKey:@"token"];
        if ([requestType isEqualToString:@"list_account2"]) {
            NSLog(@"dont remove persistance in here");
        }else{
           // NSLog(@"before :: %@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
//            [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
            [Utility removeUserData];
         //   NSLog(@"after :: %@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
        }
        
    
        //adding drop query
        InboxHelper *inboxHelper = [[InboxHelper alloc] init];
        [inboxHelper dropTblInbox];
        if (token !=nil) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:token forKey:@"token"];
            [userDefault synchronize];
        }
        if(json){
            if(requestZPK){
                requestType = @"zpk";
            }
            [self.delegate didFinishLoadData:json withRequestType:requestType];
            
        }else{
            [self handleError:lang(@"ERROR")];
        }
        
    }
    else{
        [self pushTimerVCConnection:@"start_timer"];
        if(json){
            if(requestZPK){
                requestType = @"zpk";
            }
            [self.delegate didFinishLoadData:json withRequestType:requestType];
            
        }else{
            [self handleError:lang(@"ERROR")];
        }
    }
    
    connection = nil;
}

- (void)showLoading:(NSString *)message{
    [self pushTimerVCConnection:@"stop_timer"];
    DLog(@"create loading");
    loadingAlert = [[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [loadingAlert.titleLabel setFont:[UIFont fontWithName:const_font_name3 size:14]];
    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
    [viewLoading setBackgroundColor:[UIColor clearColor]];
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loadingView setColor:[UIColor blackColor]];
    [loadingView startAnimating];
    [viewLoading addSubview:loadingView];
    
    [loadingAlert setContentView:[self newLoadingView]];
//    [loadingAlert setContentView:viewLoading];
    [loadingAlert show];
}

- (UIView*) createAnimationCircle:(double) duration beginTime:(double)begin withSize:(CGFloat)size{
    UIView *viewCircle = [[UIView alloc]initWithFrame:CGRectMake(0, 0, size, size)];
    
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, size, size)] CGPath]];
    [circleLayer setLineWidth:3];
    [circleLayer setStrokeColor:[UIColorFromRGB(const_color_secondary) CGColor]];
    [circleLayer setFillColor:[UIColorFromRGB(const_color_primary) CGColor]];

    [viewCircle.layer addSublayer:circleLayer];
    
    CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulseAnimation.duration = duration;
    pulseAnimation.fromValue = [NSNumber numberWithFloat:1];
    pulseAnimation.toValue = [NSNumber numberWithFloat:0];
    pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pulseAnimation.autoreverses = YES;
    pulseAnimation.repeatCount = FLT_MAX;
    pulseAnimation.beginTime = begin;
   
    [viewCircle.layer addAnimation:pulseAnimation forKey:nil];
    return viewCircle;
}

- (UIView*) newLoadingView{
    CGFloat widthContent = 200;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, widthContent, 70)];
    
    //stack view to put circle animation
    // widthstuck is width for stackview from size of circle
    CGFloat circleSize = 22;
    CGFloat stackviewSpace = 10;
    CGFloat stackviewCount = 3;
    CGFloat widthStack = (circleSize*stackviewCount) + (stackviewSpace*(stackviewCount-1));
    
    UIStackView *stackView = [[UIStackView alloc] initWithFrame:CGRectMake((widthContent-widthStack)/2, 10, widthStack, 40)];
    
    stackView.axis = UILayoutConstraintAxisHorizontal;
    stackView.distribution = UIStackViewDistributionFillEqually;
    stackView.alignment = UIStackViewAlignmentTop;
    stackView.spacing = stackviewSpace;
    
    [stackView addArrangedSubview:[self createAnimationCircle:1 beginTime:0.4 withSize:circleSize]];
    [stackView addArrangedSubview:[self createAnimationCircle:1 beginTime:0.7 withSize:circleSize]];
    [stackView addArrangedSubview:[self createAnimationCircle:1 beginTime:1 withSize:circleSize]];

    [view addSubview:stackView];
    
    // label under animation
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 200, 20)];
    [label setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor blackColor]];
    [label setText:lang(@"INPROGRESS_TRANSACTION")];
    
    [view addSubview:label];
    
    return view;
}

#pragma mark - Default Param

- (NSString *)getIPAddress {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return [address isEqualToString:@"error"]?@"":address;
    
}

- (NSString *)generateParamURL:(NSMutableDictionary *)dataParam{
    DataManager *dataManager = [DataManager sharedManager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //add default param
    [dataParam setObject:[UIDevice currentDevice].model forKey:@"device_type"];
    [dataParam setObject:[Utility deviceName] forKey:@"device"];
    
    [dataParam setObject:@"2" forKey:@"os_type"];
    [dataParam setObject:[Utility deviceOsVersion] forKey:@"os_version"];
    
//    if ([defaults objectForKey:@"imei"]) {
//        [dataParam setObject:[defaults objectForKey:@"imei"] forKey:@"imei"];
//    }else{
//        [dataParam setObject:[[[UIDevice currentDevice]identifierForVendor]UUIDString] forKey:@"imei"];
//    }

    if ([NSUserdefaultsAes getValueForKey:@"imei"]) {
        [dataParam setObject:[NSUserdefaultsAes getValueForKey:@"imei"] forKey:@"imei"];
    }else{
        [dataParam setObject:[[[UIDevice currentDevice]identifierForVendor]UUIDString] forKey:@"imei"];
    }
    
    if([defaults objectForKey:@"latitude"] && [defaults objectForKey:@"longitude"]){
        [dataParam setObject:[defaults objectForKey:@"latitude"] forKey:@"latitude"];
        [dataParam setObject:[defaults objectForKey:@"longitude"] forKey:@"longitude"];
    }
    
    
    [dataParam setObject:[self getIPAddress] forKey:@"ip_address"];
    [dataParam setObject:@VERSION_NAME forKey:@"version_name"];
    [dataParam setObject:@VERSION_VALUE forKey:@"version_value"];
    
    if([dataParam objectForKey:@"date_local"]){
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
//        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
        [dateFormat setDateFormat:@"yyyyMMddhhmmss"];
        NSString *currentDate = [dateFormat stringFromDate:date];
        [dataParam setObject:currentDate forKey:@"date_local"];
    }
//    if([dataParam objectForKey:@"customer_id"] && [defaults objectForKey:@"customer_id"]){
//        [dataParam setObject:[defaults stringForKey:@"customer_id"] forKey:@"customer_id"];
//    }
    
    if([dataParam objectForKey:@"customer_id"] && [NSUserdefaultsAes getValueForKey:@"customer_id"]){
        [dataParam setObject:[NSUserdefaultsAes getValueForKey:@"customer_id"] forKey:@"customer_id"];
    }
    
    if([dataParam objectForKey:@"cif"] && [NSUserdefaultsAes getValueForKey:@"usercif"]){
        [dataParam setObject:[NSUserdefaultsAes getValueForKey:@"usercif"] forKey:@"cif"];
    }
    
    if([dataParam objectForKey:@"menu_id"] && dataManager.menuId){
        [dataParam setObject:dataManager.menuId forKey:@"menu_id"];
    }
    
    if([dataParam objectForKey:@"language"] || [defaults objectForKey:@"AppleLanguages"]){
        NSArray *listLanguage = [defaults objectForKey:@"AppleLanguages"];
        [dataParam setObject:[listLanguage objectAtIndex:0] forKey:@"language"];
    }
    if([dataParam objectForKey:@"pin"] && dataManager.pinNumber){
//        NSError *error;
//        NSString *newPin = [desChipper doPinBlock:dataManager.pinNumber key:[defaults valueForKey:@"zpk"]];
        NSString *newPin = [desChipper doPinBlock:dataManager.pinNumber key:[NSUserdefaultsAes getValueForKey:@"zpk"]];
//        if(!error){
//            [dataParam setObject:newPin forKey:@"pin"];
//        }
        [dataParam setObject:newPin forKey:@"pin"];
    }
    
    if([dataParam objectForKey:@"new_pin"]){
        //        if(![defaults boolForKey:@"development"]){
//        NSError *error;
//        NSString *newPin = [desChipper doPinBlock:[dataParam valueForKey:@"new_pin"] key:[defaults valueForKey:@"zpk"]];
        NSString *newPin = [desChipper doPinBlock:[dataParam valueForKey:@"new_pin"] key:[NSUserdefaultsAes getValueForKey:@"zpk"]];
//        if(!error){
//            [dataParam setObject:newPin forKey:@"new_pin"];
//        }
         [dataParam setObject:newPin forKey:@"new_pin"];
        //        }
    }
    if([dataParam objectForKey:@"pan"]){
        //        if(![defaults boolForKey:@"development"]){
//        NSError *error;
//        NSString *newPin = [desChipper doPinBlock:[dataParam valueForKey:@"pan"] key:[defaults valueForKey:@"zpk"]];
        NSString *newPin = [desChipper doPinBlock:[dataParam valueForKey:@"pan"] key:[NSUserdefaultsAes getValueForKey:@"zpk"]];
//        if(!error){
//            [dataParam setObject:newPin forKey:@"pan"];
//        }
         [dataParam setObject:newPin forKey:@"pan"];
        //        }
    }
    if([dataParam objectForKey:@"zpk"]){
//        [dataParam setObject:[defaults objectForKey:@"zpk"] forKey:@"zpk"];
        [dataParam setObject:[NSUserdefaultsAes getValueForKey:@"zpk"] forKey:@"zpk"];
    }
    
    if ([dataParam objectForKey:@"transaction_id"]) {
        if(![[defaults objectForKey:@"transaction_id"] isEqualToString:@""]){
            [dataParam setObject:[defaults objectForKey:@"transaction_id"] forKey:@"transaction_id"];
        }
    }
    
    for(NSString *key in dataParam.allKeys){
        if([[dataParam objectForKey:key]isKindOfClass:[NSNumber class]] && ![[dataParam objectForKey:key]boolValue]){
            if([dataManager.dataExtra objectForKey:key]){
                [dataParam setObject:[dataManager.dataExtra valueForKey:key] forKey:key];
            }else{
                NSLog(@"KEY %@ CHANGED TO EMPTY STRING", key);
                [dataParam setObject:@"" forKey:key];
            }
        }
    }
    
    // set date qris <add by Angger>
    if([dataParam objectForKey:@"date_qris"]){
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
        NSString *currentDate = [dateFormat stringFromDate:date];
        [dataParam setObject:currentDate forKey:@"date_qris"];
    }
    
    DLog(@"DATA TO SENT: %@", dataParam);
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dataParam options:0 error:nil];
    //    if(encoded){
    //        jsonData = [jsonData base64EncodedDataWithOptions:0];
    //    }
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    return jsonString;
}

- (void)dismissLoading{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    //    if([DataManager sharedManager].continueLoading ){
    [self pushTimerVCConnection:@"start_timer"];
    if ([requestType isEqualToString:@"check_notif"] || [requestType isEqualToString:@"login"]) {
        [self pushTimerVCConnection:@"stop_timer"];
    }
    [loadingAlert  dismissWithClickedButtonIndex:0 animated:YES];
    loadingAlert = nil;
    //        [DataManager sharedManager].continueLoading  = nil;
    //    }
}

- (void)handleError:(NSString *)message
{
    [self dismissLoading];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    //call error page later
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:true forKey:@"RTO"];
    [self.delegate reloadApp];
    
}

#pragma mark - Local Method

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding data:(NSString *)oriString{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (CFStringRef)oriString,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8 ));
//    NSCharacterSet * queryKVSet = [NSCharacterSet
//        characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"].invertedSet;
//    return (NSString*)[oriString stringByAddingPercentEncodingWithAllowedCharacters:queryKVSet];
}

-(void)cancelConnection:(NSURLConnection *)conn{
    if(conn)
    {
        [conn cancel];
        connection = nil;
    }
}
@end

