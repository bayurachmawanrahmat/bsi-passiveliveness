//
//  PrayerNotification.h
//  BSM-Mobile
//
//  Created by ARS on 01/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PrayerNotification : NSObject

+ (void) createNotification : (NSString*) title message: (NSString *) message hour : (int) hour minute : (int) minute soundNamed:(NSString* _Nullable) soundName repeats:(BOOL) repeat uniqIdentfiedAs:(NSString*) identifier;

+ (void) cancelNotification : (NSString*) identifier;

@end

NS_ASSUME_NONNULL_END
