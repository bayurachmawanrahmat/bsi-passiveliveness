//
//  BSMPinAnnotation.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/11/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSMPinAnnotation : NSObject<MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;
@property (copy, nonatomic) NSDictionary *branchData;

- (MKAnnotationView *)annotationView;

@end

NS_ASSUME_NONNULL_END
