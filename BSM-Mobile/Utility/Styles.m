//
//  Styles.m
//  BSM-Mobile
//
//  Created by ARS on 28/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "Styles.h"
NS_ENUM(NSInteger, SIDENAME){
    RIGHT = 1,
    LEFT = 0
};

@implementation Styles

+ (void) createProgressBar:(UIView*) view step:(int) step from:(int)from text:(NSString*)text{
    
    CGFloat width = view.frame.size.width;
    CGFloat height = view.frame.size.height;
    CGFloat circleSize = height/2;
    CGFloat circleXPos = 0;
    CGFloat circleYpos = height/2;
    CGFloat barWidth = (width - (from*circleSize) - (2*circleXPos))/(from-1);
    CGFloat barHeight = circleSize/10;
    CGFloat barXPos = circleSize;
    CGFloat barYPos = circleYpos + ((circleSize/2)-(barHeight/2));
    CGFloat labelTextYPos = 0;
    CGFloat labelTextHeight = height/2;
    

    for(int i = 0; i < (from-1); i++){
        
        if(i == step-1){
            UILabel *textlabel = [[UILabel alloc]initWithFrame:CGRectMake(circleXPos, labelTextYPos, circleSize, labelTextHeight)];
            [textlabel setTextAlignment:NSTextAlignmentCenter];
            [textlabel setTextColor: UIColorFromRGB(const_color_primary)];
            [textlabel setAdjustsFontSizeToFitWidth:YES];
            [textlabel setText:text];
            [view addSubview:textlabel];
        }
        
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)] CGPath]];
        
        if(i < step){
            [circleLayer setFillColor:[UIColorFromRGB(const_color_primary)CGColor]];
        }else{
            [circleLayer setFillColor:[UIColorFromRGB(const_color_primary)CGColor]];
        }
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor: [UIColor whiteColor]];
        [label setAdjustsFontSizeToFitWidth:YES];
        [label setText:[NSString stringWithFormat:@"%d", i+1]];
        
        barXPos = circleXPos+circleSize;
                
        CAShapeLayer *rectLayer = [CAShapeLayer layer];
        [rectLayer setPath:[[UIBezierPath bezierPathWithRect:CGRectMake(barXPos - circleSize/2, barYPos, barWidth + circleSize, barHeight)]CGPath]];
        
        if(i < step-1){
            [rectLayer setFillColor:[UIColorFromRGB(const_color_primary)CGColor]];
        }else{
            [rectLayer setFillColor:[UIColorFromRGB(const_color_gray)CGColor]];
        }
        
        circleXPos = barXPos + barWidth;
        
        [[view layer] addSublayer:rectLayer];
        [[view layer] addSublayer:circleLayer];

        [view addSubview:label];
    }
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)] CGPath]];
    [circleLayer setFillColor:[UIColorFromRGB(const_color_gray)CGColor]];
    if(from == step){
        UILabel *textlabel = [[UILabel alloc]initWithFrame:CGRectMake(circleXPos, labelTextYPos, circleSize, labelTextHeight)];
        [textlabel setTextAlignment:NSTextAlignmentCenter];
        [textlabel setTextColor: UIColorFromRGB(const_color_primary)];
        [textlabel setAdjustsFontSizeToFitWidth:YES];
        [textlabel setText:text];
        [view addSubview:textlabel];
        [circleLayer setFillColor:[UIColorFromRGB(const_color_primary)CGColor]];
    }

    UILabel *lastStep = [[UILabel alloc]initWithFrame:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)];
    [lastStep setTextAlignment:NSTextAlignmentCenter];
    [lastStep setTextColor: [UIColor whiteColor]];
    [lastStep setAdjustsFontSizeToFitWidth:YES];
    [lastStep setText:[NSString stringWithFormat:@"%d", from]];

    [[view layer] addSublayer:circleLayer];
    [view addSubview:lastStep];

}

+ (void) createProgress:(UIView*) view step:(int) step from:(int)from{
    
    CGFloat width = view.frame.size.width;
    CGFloat height = view.frame.size.height;
    CGFloat circleSize = height;
    CGFloat circleXPos = 0;
    CGFloat circleYpos = 0;
    CGFloat barWidth = (width - (from*circleSize) - (2*circleXPos))/(from-1);
    CGFloat barHeight = circleSize/10;
    CGFloat barXPos = circleSize;
    CGFloat barYPos = circleYpos + ((circleSize/2)-(barHeight/2));
    

    for(int i = 0; i < (from-1); i++){

        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)] CGPath]];
        
        if(i < step){
            [circleLayer setFillColor:[UIColorFromRGB(const_color_primary)CGColor]];
        }else{
            [circleLayer setFillColor:[UIColorFromRGB(const_color_gray)CGColor]];
        }
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor: [UIColor whiteColor]];
        [label setAdjustsFontSizeToFitWidth:YES];
        [label setText:[NSString stringWithFormat:@"%d", i+1]];
        
        barXPos = circleXPos+circleSize;
                
        CAShapeLayer *rectLayer = [CAShapeLayer layer];
        [rectLayer setPath:[[UIBezierPath bezierPathWithRect:CGRectMake(barXPos - circleSize/2, barYPos, barWidth + circleSize, barHeight)]CGPath]];
        
        if(i < step-1){
            [rectLayer setFillColor:[UIColorFromRGB(const_color_primary)CGColor]];
        }else{
            [rectLayer setFillColor:[UIColorFromRGB(const_color_gray)CGColor]];
        }
        
        circleXPos = barXPos + barWidth;
        
        [[view layer] addSublayer:rectLayer];
        [[view layer] addSublayer:circleLayer];

        [view addSubview:label];
    }
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    
    [circleLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)] CGPath]];
    [circleLayer setFillColor:[UIColorFromRGB(const_color_gray)CGColor]];
    if(from == step){
        [circleLayer setFillColor:[UIColorFromRGB(const_color_primary)CGColor]];
    }

    UILabel *lastStep = [[UILabel alloc]initWithFrame:CGRectMake(circleXPos, circleYpos, circleSize, circleSize)];
    [lastStep setTextAlignment:NSTextAlignmentCenter];
    [lastStep setTextColor: [UIColor whiteColor]];
    [lastStep setAdjustsFontSizeToFitWidth:YES];
    [lastStep setText:[NSString stringWithFormat:@"%d", from]];

    [[view layer] addSublayer:circleLayer];
    [view addSubview:lastStep];

}

+ (void) setTopConstant : (NSLayoutConstraint*) constraint{
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    if (mainWindow.safeAreaInsets.top > 24.0) {
        constraint.constant = 36;
    }else{
        constraint.constant = 58;
    }
}

+ (void) setStyleButton : (UIButton *) button{
    button.layer.borderColor = [UIColorFromRGB(const_color_secondary) CGColor];
    button.layer.backgroundColor = [UIColorFromRGB(const_color_primary) CGColor];
    button.layer.borderWidth = 1;
    button.layer.cornerRadius = button.frame.size.height/2;
    button.layer.masksToBounds = YES;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

+ (void) setStyleTextFieldBorder : (UITextField *)textField andRounded:(BOOL) rounded{
    textField.layer.borderColor = [UIColorFromRGB(const_color_secondary) CGColor];
    textField.layer.borderWidth = 1;
    if(rounded){
        textField.layer.cornerRadius = 10;
    }
    textField.layer.masksToBounds = YES;
}

+ (void) setStyleTextFieldBottomLine : (UITextField *)textField{
    textField.borderStyle = UITextBorderStyleNone;
    textField.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    
    textField.layer.masksToBounds = false;
    textField.layer.shadowColor = [[UIColor grayColor]CGColor];
    textField.layer.shadowOffset = CGSizeMake(0.0, 0.5);
    textField.layer.shadowOpacity = 1.0;
    textField.layer.shadowRadius = 0.0;
}

+ (void) setStyleWithSideImage : (UITextField *)textField imageNamed : (NSString*)imageNamed onSide:(NSInteger) side{
    
    CGFloat height = (textField.frame.size.height)-10;
    
    UIImageView *dropIcon = [[UIImageView alloc] init];
    [dropIcon setFrame:CGRectMake(0, 0, height, height)];
    dropIcon.image = [UIImage imageNamed:imageNamed];
    dropIcon.tintColor = UIColorFromRGB(const_color_secondary);
    dropIcon.contentMode = UIViewContentModeScaleToFill;
    
    UIView *sideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, height, height)];
    [sideView addSubview:dropIcon];
    
    if(side == RIGHT){
        textField.rightViewMode = UITextFieldViewModeAlways;
        textField.rightView = sideView;
    }else if(side == LEFT){
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.leftView = sideView;
    }
}

+ (void) setStyleTextFieldBorderWithRightImage : (UITextField*) textField imageNamed : (NSString*) imageNamed andRounded : (BOOL) rounded{
    [self setStyleWithSideImage:textField imageNamed:imageNamed onSide:RIGHT];
    [self setStyleTextFieldBorder:textField andRounded:rounded];
}

+ (void) setStyleTextFieldBottomLineWithRightImage : (UITextField *)textField imageNamed:(NSString*) imageNamed{
    [self setStyleTextFieldBottomLine:textField];
    [self setStyleWithSideImage:textField imageNamed:imageNamed onSide:RIGHT];
}

+ (void) setStyleTextFieldBorderWithLeftImage : (UITextField*) textField imageNamed : (NSString*) imageNamed andRounded : (BOOL) rounded{
    [self setStyleWithSideImage:textField imageNamed:imageNamed onSide:LEFT];
    [self setStyleTextFieldBorder:textField andRounded:rounded];
}

+ (void) setStyleTextFieldBottomLineWithLeftImage : (UITextField *)textField imageNamed:(NSString*) imageNamed{
    [self setStyleTextFieldBottomLine:textField];
    [self setStyleWithSideImage:textField imageNamed:imageNamed onSide:LEFT];
}

+ (void) setStyleSegmentControl : (UISegmentedControl *) segmentControl{
    segmentControl.backgroundColor = [UIColor whiteColor];
    
    if(@available(iOS 13.0, *)){
        segmentControl.selectedSegmentTintColor = UIColorFromRGB(const_color_secondary);
    }else{
        segmentControl.tintColor = UIColorFromRGB(const_color_secondary);
    }
    
    segmentControl.layer.cornerRadius = 7.0;
    segmentControl.layer.masksToBounds = YES;
    segmentControl.layer.borderWidth = 1;
    segmentControl.layer.borderColor = [UIColorFromRGB(const_color_secondary) CGColor];
}

+ (void) setStyleRoundedView : (UIView*) view{
    view.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    view.layer.borderColor = [UIColorFromRGB(const_color_secondary) CGColor];
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 15;
    view.layer.masksToBounds = YES;
}

+ (UIColor *)backgroundPopupColor {
    return [UIColor colorWithRed:105.0f/255.0f
                            green:105.0f/255.0f
                             blue:105.0f/255.0f
                            alpha:0.55f];
}

+ (void)addShadow:(UIView *)view {
    UIView* bgView = [[UIView alloc]initWithFrame:view.bounds];
    [bgView setBackgroundColor:[UIColor colorWithRed:105.0f/255.0f
                                                  green:105.0f/255.0f
                                                   blue:105.0f/255.0f
                                               alpha:0.60f]];
    [view insertSubview:bgView atIndex:0];
}

+(void)addBlur:(UIView*)view{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    
    UIView* bgView = [[UIView alloc]initWithFrame:view.bounds];
    visualEffectView.frame = bgView.bounds;
    [bgView addSubview:visualEffectView];
    [view insertSubview:bgView atIndex:0];
}
@end
