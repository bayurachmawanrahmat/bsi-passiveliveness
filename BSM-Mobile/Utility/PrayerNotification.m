//
//  AlarmSholat.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 01/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PrayerNotification.h"

@implementation PrayerNotification

+ (void) createNotification : (NSString *) title message: (NSString *) message hour : (int) hour minute : (int) minute soundNamed:(NSString * _Nullable) soundName repeats:(BOOL) repeat uniqIdentfiedAs:(NSString *) identifier{
    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:title arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:message
            arguments:nil];
    content.categoryIdentifier = @"SHOLAT_TIME";
    
    if(soundName != nil){
        content.sound = [UNNotificationSound soundNamed:soundName];
    }else{
        content.sound = nil;
    }
    
    NSDateComponents* date = [[NSDateComponents alloc] init];
    date.hour = hour;
    date.minute = minute;
    UNCalendarNotificationTrigger* trigger = [UNCalendarNotificationTrigger
           triggerWithDateMatchingComponents:date repeats:repeat];
     
    UNNotificationRequest* request = [UNNotificationRequest
           requestWithIdentifier:identifier content:content trigger:trigger];
    
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
       if (error != nil) {
           NSLog(@"%@", error.localizedDescription);
       }
    }];
    
}

+ (void) cancelNotification : (NSString*) identifier{
    NSArray* identifiers = @[identifier];
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center removePendingNotificationRequestsWithIdentifiers:identifiers];
    [center removeDeliveredNotificationsWithIdentifiers:identifiers];
}

@end
