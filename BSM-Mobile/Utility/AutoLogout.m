//
//  AutoLogout.m
//  BSM-Mobile
//
//  Created by BSM on 2/21/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "AutoLogout.h"


@interface AutoLogout()
{
    
}

@end


@implementation AutoLogout


#pragma mark - Detect events

- (void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    // Check if there was a touch event
    NSSet *allTouches = [event allTouches];
    
    if ([allTouches count] > 0)
    {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        
        if (phase == UITouchPhaseBegan)
        {
            if ( self.enableAutoLogout )
            {
                [self resetAlarm];
            }
        }
    }
    
    NSLog(@"Event");
}


#pragma mark - Alarm

- (void)resetAlarm
{
    self.timeTracker = [NSDate date];
    
    if ( [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive )
    {
        [self scheduleAlarm];
    }
    
    NSLog(@"Reset Alarm");
}

- (void)scheduleAlarm
{
    UIApplication* app              = [UIApplication sharedApplication];
    NSArray*    oldNotifications    = [app scheduledLocalNotifications];
    
    // Clear out the old notification before scheduling a new one.
    if ([oldNotifications count] > 0)
    {
        [app cancelAllLocalNotifications];
    }
    
    // Create a new notification.
    UILocalNotification* alarm      = [[UILocalNotification alloc] init];
    
    // We do not want the alert to appear,
    // just the didReceiveLocalNotification to be called
    if (alarm)
    {
        //alarm.fireDate          = [NSDate dateWithTimeIntervalSinceNow:kTimeout];
        alarm.repeatInterval    = 0;
        alarm.timeZone          = [NSTimeZone defaultTimeZone];
        alarm.hasAction         = NO;
        
        [app scheduleLocalNotification:alarm];
    }
}


#pragma Dealloc

- (void)dealloc
{
    self.timeTracker = nil;
}

@end
