//
//  CustomCollectionViewLayout.h
//  BSM-Mobile
//
//  Created by ARS on 10/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomCollectionViewLayout : UICollectionViewLayout

@end

NS_ASSUME_NONNULL_END
