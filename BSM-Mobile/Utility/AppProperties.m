//
//  AppProperties.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "AppProperties.h"

@implementation AppProperties

+ (NSDictionary*) getResources{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"App-Properties" ofType: @"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile: path];
    return dict;
}

+ (BOOL) checkIsProd{
    return [[[self getResources] valueForKey:@"isProd"]boolValue];
}

+ (BOOL) checkSslPinningIsOn{
    return [[[self getResources] valueForKey:@"sslPinningIsOn"]boolValue];
}

+ (BOOL) checkJbProtectIsOn{
    return [[[self getResources] valueForKey:@"jbProtectIsOn"]boolValue];
}

+ (NSString*) getKeyChiper{
    return [[[self getResources] objectForKey:@"AESCHIPER"]valueForKey:@"KEY_CHIPER"];
}

+ (NSString*) getAPIUrl{
    if([self checkIsProd]){
        return [[self getResources] valueForKey:@"API_URL"];
    }else{
        return [[self getResources] valueForKey:@"API_URL_DEV"];
    }
}

+ (NSString*) getAPIUrlCheck{
    if([self checkIsProd]){
        return [[self getResources] valueForKey:@"URL_CHECKED_CONNECTION"];
    }else{
        return [[self getResources] valueForKey:@"URL_CHECKED_CONNECTION_DEV"];
    }
}

+ (NSString*) getAPIUrlGadaiEmas{
    if([self checkIsProd]){
        return [[self getResources] valueForKey:@"GADAI_EMAS_API_URL"];
    }else{
        return [[self getResources] valueForKey:@"GADAI_EMAS_API_URL_DEV"];
    }
}

+ (NSString*) getAPIUrlSyariahpoint{
    if([[[self getResources] valueForKey:@"isProd"]boolValue]){
        return [[self getResources] valueForKey:@"SYARIAHPOINT_API_URL"];
    }else{
        return [[self getResources] valueForKey:@"SYARIAHPOINT_API_URL_DEV"];
    }
}

+ (NSString*) getSliderURL{
    return [[self getResources] valueForKey:@"SLIDER_URL"];
}

+ (NSString*) getGMAPUrl{
    return [[self getResources] valueForKey:@"GMAP_URL"];
}

+ (NSString*) getApiKeyServer{
    return [[self getResources] valueForKey:@"API_KEY_SERVER"];
}

+ (NSString*) getClientId{
    return [[self getResources] valueForKey:@"clientId"];
}

+ (NSString*) getClientSecret{
    return [[self getResources] valueForKey:@"clientSecret"];
}

+ (NSString*) getStatusUniqueDevice{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return @"YES";
    }else{
        return @"NO";
    }
}

+ (NSString*) getAPIUrlOnboardingSafe{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_SAFE_API_URL"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_SAFE_API_URL"];
    }
}

+ (NSString*) getAPIUrlOnboarding{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_API_URL"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_API_URL"];
    }
}

+ (NSString*) getAPIUrlOnboardingSafeFR{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_SAFE_API_FR_URL"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_SAFE_API_FR_URL"];
    }
}

+ (NSString*) getAPIUrlOnboardingFR{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_API_FR_URL"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_API_FR_URL"];
    }
}

+ (NSString*) getOnboardingLXApplicationName{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_LX_APPLICATION_NAME"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_LX_APPLICATION_NAME"];
    }
}

+ (NSString*) getOnboardingLXAddress{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_LX_ADDRESS"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_LX_ADDRESS"];
    }
}

+ (NSString*) getOnboardingLXClientTennant{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_LX_CLIENT_TENNANT"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_LX_CLIENT_TENNANT"];
    }
}

+ (NSString*) getOnboardingLXClientID{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_LX_CLIENT_ID"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_LX_CLIENT_ID"];
    }
}

+ (NSString*) getOnboardingLXClientSecret{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_LX_CLIENT_SECRET"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_LX_CLIENT_SECRET"];
    }
}

+ (NSString*) getOnboardingGOCoderLicenseKey{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_GOCODER_LICENSE_KEY"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_GOCODER_LICENSE_KEY"];
    }
}

+ (NSString*) getOnboardingGOCoderHost{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_GOCODER_HOST"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_GOCODER_HOST"];
    }
}

+ (NSString*) getOnboardingGOCoderPort{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_GOCODER_PORT"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_GOCODER_PORT"];
    }
}

+ (NSString*) getOnboardingGOCoderAppName{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_GOCODER_APPNAME"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_GOCODER_APPNAME"];
    }
}

+ (NSString*) getOnboardingGOCoderUserName{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_GOCODER_USERNAME"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_GOCODER_USERNAME"];
    }
}

+ (NSString*) getOnboardingGOCoderPassword{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"ONBOARDING_GOCODER_PASSWORD"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"ONBOARDING_GOCODER_PASSWORD"];
    }
}

+ (NSString*) getKeyRetrival{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"KEY_RETRIVAL"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"KEY_RETRIVAL"];
    }
}

+ (NSString*) getClientCredential{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"CLIENT_CREDENTIAL"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"CLIENT_CREDENTIAL"];
    }
}

+ (NSString*) getSecretCredential{
    if([[[self getResources] valueForKey:@"isOnboardingProd"]boolValue]){
        return [[[self getResources] objectForKey:@"ONBOARDING"] valueForKey:@"SECRET_CREDENTIAL"];
    }else{
        return [[[self getResources] objectForKey:@"ONBOARDING_DEV"] valueForKey:@"SECRET_CREDENTIAL"];
    }
}

+ (BOOL) getEnableUniqDevice{
    return [[[self getResources] valueForKey:@"isOnboardingProd"]boolValue];
}

+ (NSArray*) getArrayStoryboardList{
    NSDictionary *dictData = [[self getResources] valueForKey:@"STORYBOARD_LIST"];
    NSMutableArray *arrData=[[NSMutableArray alloc] init];
    for (NSString* key in dictData) {
        [arrData addObject:dictData[key]];
    }
    return arrData;
}

@end
