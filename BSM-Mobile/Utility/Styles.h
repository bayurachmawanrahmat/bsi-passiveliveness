//
//  Styles.h
//  BSM-Mobile
//
//  Created by ARS on 28/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Styles : NSObject

+ (void) setTopConstant : (NSLayoutConstraint*) constraint;
+ (void) setStyleButton : (UIButton *) button;
+ (void) setStyleTextFieldBorder : (UITextField *)textField andRounded:(BOOL) rounded;
+ (void) setStyleTextFieldBottomLine : (UITextField *)textField;
+ (void) setStyleWithSideImage : (UITextField *)textField imageNamed : (NSString*)imageNamed onSide:(NSInteger) side;
+ (void) setStyleTextFieldBorderWithLeftImage : (UITextField*) textField imageNamed : (NSString*) imageNamed andRounded : (BOOL) rounded;
+ (void) setStyleTextFieldBorderWithRightImage : (UITextField*) textField imageNamed : (NSString*) imageNamed andRounded : (BOOL) rounded;
+ (void) setStyleTextFieldBottomLineWithRightImage : (UITextField *)textField imageNamed:(NSString*) imageNamed;
+ (void) setStyleTextFieldBottomLineWithLeftImage : (UITextField *)textField imageNamed:(NSString*) imageNamed;
+ (void) setStyleSegmentControl : (UISegmentedControl *) segmentControl;
+ (void) setStyleRoundedView : (UIView*) view;

+ (void) createProgressBar:(UIView*) view step:(int) step from:(int)from text:(NSString*)text;
+ (void) createProgress:(UIView*) view step:(int) step from:(int)from;

+ (UIColor *)backgroundPopupColor;
+ (void)addBlur:(UIView*)view;
+ (void)addShadow:(UIView *)view;

@end

NS_ASSUME_NONNULL_END
