//
//  UIDropdownSearch.h
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol UIDropdownSearchDelegate;

@interface UIDropdownSearch : OnboardingRootController <UITableViewDataSource, UITableViewDelegate, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

@property (nonatomic, weak) IBOutlet UITableView * tableView;
@property (nonatomic, strong) UISearchController * searchController;
@property (nonatomic, strong) NSArray * allItems;
@property (nonatomic, strong) NSMutableArray * filteredItems;
@property (nonatomic, weak) NSArray * displayedItems;
@property (strong) NSDictionary * selectedData;
@property BOOL callApiSucess;
@property (weak, nonatomic) NSString *displayedStringKey;
@property (strong) NSDictionary *searchNotFoundValue;

-(void)setOptions:(NSDictionary *)data;
-(void)setOptionsWithData:(NSDictionary *)data andKeysToStore:(NSArray *)keysToStore andSortUsingKey:(nullable NSString *)sortKey;
-(void)filterSearchControllerOptionsBySearchString:(NSString *)searchString;
-(void)modifySearchControllerOptionsBySearchString:(NSString *)searchString;
-(void)getReferenceDataFromEndPoint:(NSString *)endpoint withKeysToStore:(NSArray *)keysToStore andSortUsingKey:(nullable NSString*) sortKey andDispatchGroup:(dispatch_group_t)dispatchGroup;
-(void)enableSearchControllerFilter:(BOOL)enabled;

@property (weak, nonatomic) id<UIDropdownSearchDelegate> delegate;

@end

@protocol UIDropdownSearchDelegate <NSObject>

@required
-(void)dropdownSearch:(id)uiDropdownSearch didSelectRowWithData:(NSDictionary *)data;

@end

NS_ASSUME_NONNULL_END
