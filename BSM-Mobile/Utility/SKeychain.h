//
//  SKeychain.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 21/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SKeychain : NSObject

+ (BOOL) saveObject:(id)object forKey:(NSString*)key;
+ (id) loadObjectForKey:(NSString*)key;
+ (BOOL) deleteObjectForKey:(NSString*)key;

@end

NS_ASSUME_NONNULL_END
