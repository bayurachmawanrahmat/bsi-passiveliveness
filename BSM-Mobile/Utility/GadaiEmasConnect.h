//
//  GadaiEmasConnect.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "DLAVAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol GadaiEmasConnectDelegate <NSObject>

- (void)didFinishLoadData:(NSDictionary* _Nullable)dict withEndpoint:(NSString*)endPoint completeWithError:(NSError*)error;
- (void)didFinishWithError:(NSError*) error;

@end

@interface GadaiEmasConnect : NSObject<NSURLSessionTaskDelegate, NSURLSessionDataDelegate>{
    NSURLSession *connection;
    NSString *endPointRequest;
    NSString *paramsURL;
    NSMutableData *data;
    BOOL isLoading;
    BOOL isEncrypted;
    NSString *txtLoading;
    DLAVAlertView *loadingAlert;
}

@property (nonatomic, retain) id <GadaiEmasConnectDelegate> delegate;
@property (nonatomic, strong) dispatch_group_t _Nullable dispatchGroup;
@property (nonatomic, assign) BOOL requestIsSuccess;

- (id)initWithDelegate:(id)delegate;
- (void)getDataFromEndPoint:(NSString*) endpoint needLoading:(BOOL)needLoading textLoading:(NSString*)textLoading encrypted:(BOOL)encrypted;
- (void)sendPostData:(NSString*)params endPoint:(NSString *)endpoint needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted;
+ (void)getFromEndpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData;
+ (NSString *)getToken;
+ (NSString *)apiUrl;
+ (NSString *)safeEndpoint:(NSString *)endpoint;

@end

NS_ASSUME_NONNULL_END
