//
//  NSData+Hex.h
//  BSM-Mobile
//
//  Created by lds on 6/2/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSData(Hex)
-(NSString*)hexRepresentationWithSpaces_AS:(BOOL)spaces;
@end

