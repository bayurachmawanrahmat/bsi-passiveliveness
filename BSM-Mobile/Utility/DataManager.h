//
//  DataManager.h
//  BSM Mobile
//
//  Created by lds on 5/13/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DLAVAlertView.h"

@interface DataManager : NSObject

@property (nonatomic, assign) int currentPosition;
//@property (nonatomic, assign) int incrementText;
@property (nonatomic, retain) NSArray *listAction;
@property (nonatomic, retain) NSDictionary *selectedData;
@property (nonatomic, retain) NSString *pinNumber;
//@property (nonatomic, retain) NSString *txt1;
//@property (nonatomic, retain) NSString *txt2;
@property (nonatomic, retain) NSArray *listMenu;
@property (nonatomic, retain) NSMutableDictionary *dataExtra;
//@property (nonatomic, retain) NSString *idMerchant;
//@property (nonatomic, retain) NSString *code;
//@property (nonatomic, retain) NSString *idAccount;
//@property (nonatomic, retain) NSString *idDenom;
@property (nonatomic, retain) NSString *menuId;
@property (nonatomic, retain) DLAVAlertView *continueLoading;
@property (nonatomic, retain) NSMutableDictionary *favData;
//@property (nonatomic, retain) NSString *transactionId;
//@property (nonatomic, retain) NSString *typeBank;

+ (DataManager *)sharedManager;
- (id)getJSONData:(int)position;
- (id)getJSONByCurrentMenu;
- (void)setAction:(NSDictionary *)data andMenuId:(NSString *)menuId;
- (void)updateObjectDataWitList:(NSArray *)list;
- (id)getObjectData;
- (id)getObjectDataAt:(int)position;
- (void)resetObjectData;
- (id)getJsonDataByMenuID;
- (id)getJsonDataWithMenuID : (NSString*) menuID andList : (NSArray*)list;
- (NSDictionary*)getArrayofActionMenuID : (NSString *)menuID andList : (NSArray*) list;
- (NSArray*)getStoryboardList;

@end
