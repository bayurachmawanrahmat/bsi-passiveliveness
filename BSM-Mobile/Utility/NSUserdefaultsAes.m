//
//  NSU+AES.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 19/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "NSUserdefaultsAes.h"
#import "AESCipher.h"
#import "NSString+MD5.h"

@implementation NSUserdefaultsAes

+ (void) setObject:(id _Nullable)object forKey:(id)key {
    AESCipher *aeschip = [[AESCipher alloc]init];
    if(object != nil){
        [[NSUserDefaults standardUserDefaults] setObject:[aeschip aesEncryptString:object] forKey:[key MD5]];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:[key MD5]];
    }
}

+ (id) getValueForKey:(id)key {
    AESCipher *aeschip = [[AESCipher alloc]init];
    id value = [[NSUserDefaults standardUserDefaults] objectForKey:[key MD5]];
    if(value == nil){
        return nil;
    }
    return [aeschip aesDecryptString:value];
}

+ (void) removeValueForKey:(id)key {
//    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:[key MD5]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[key MD5]];
}


@end
