//
//  UIAlertController+AlertExtension.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 06/12/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIAlertController+AlertExtension.h"
#import "RootViewController.h"

@implementation UIAlertController(AlertExtension)

- (void)addActionWithTitle:(NSString *)title actionWithStyle:(UIAlertActionStyle)actionStyle withTag:(int)index instanceOf:(id)instanceID{

    [self addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        switch (index) {
            case 101:
                [(RootViewController*)instanceID backToR];
                break;
            case 404:
                exit(0);
                break;
                
            default:
                break;
        }
    }]];
}
@end
