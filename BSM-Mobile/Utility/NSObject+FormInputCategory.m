//
//  NSObject+FormInputCategory.m
//  BSM-Mobile
//
//  Created by ARS on 26/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "NSObject+FormInputCategory.h"
#import "AMPopTip.h"
#import <objc/runtime.h>
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

static void *AlertLoadingKey = &AlertLoadingKey;
static void *AlertControllerKey = &AlertControllerKey;
static void *ActiveFieldKey = &ActiveFieldKey;
static void *ScrollViewKey = &ScrollViewKey;
static void *MainViewKey = &MainViewKey;

@implementation NSObject (FormInputCategory)

- (void) setAlertLoading:(DLAVAlertView *)alertLoading
{
    objc_setAssociatedObject(self, AlertLoadingKey, alertLoading, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (DLAVAlertView *)alertLoading
{
    return objc_getAssociatedObject(self, AlertLoadingKey);
}

- (void)setAlertController:(UIAlertController *)alertController {
    objc_setAssociatedObject(self, AlertControllerKey, alertController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIAlertController *)alertController {
    return objc_getAssociatedObject(self, AlertControllerKey);
}

- (void)setActiveField:(UITextField *)activeField
{
    objc_setAssociatedObject(self, ActiveFieldKey, activeField, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UITextField *)activeField
{
    return objc_getAssociatedObject(self, ActiveFieldKey);
}

- (void)setScrollView:(UIScrollView *)scrollView
{
    objc_setAssociatedObject(self, ScrollViewKey, scrollView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIScrollView *)scrollView
{
    return objc_getAssociatedObject(self, ScrollViewKey);
}

- (void)setMainView:(UIView *)mainView
{
    objc_setAssociatedObject(self, MainViewKey, mainView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)mainView
{
    return objc_getAssociatedObject(self, MainViewKey);
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    kbSize.height += 10;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    [self scrollView].contentInset = contentInsets;
    [self scrollView].scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = [self mainView].frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, [self activeField].frame.origin) ) {
        [self.scrollView scrollRectToVisible:[self activeField].frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self scrollView].contentInset = contentInsets;
    [self scrollView].scrollIndicatorInsets = contentInsets;
}


- (void)setTextFieldData:(NSString *)key toTextField:(UITextField *)textField
{
    Encryptor *encryptor = [[Encryptor alloc] init];
    NSString *value = [encryptor getUserDefaultsObjectForKey:key];
    textField.text = value;
}

- (void)saveTextFieldData:(UITextField *)textField withKey:(NSString *)key
{
    Encryptor *encryptor = [[Encryptor alloc] init];
    [encryptor setUserDefaultsObject:textField.text forKey:key];
}

- (void)saveTextFieldData:(NSDictionary<NSString *,UITextField *> *)textFields
{
    Encryptor *encryptor = [[Encryptor alloc] init];
    for(NSString *key in textFields)
    {
        UITextField *aTextField = textFields[key];
        [encryptor setUserDefaultsObject:aTextField.text forKey:key];
    }
}

- (void)setTextFieldData:(NSDictionary<NSString *,UITextField *> *)textFields
{
    Encryptor *enc = [[Encryptor alloc] init];
    for(NSString *key in textFields)
    {
        UITextField *aTextField = textFields[key];
        NSString *inputValue = [enc getUserDefaultsObjectForKey:key];
        if ([inputValue isKindOfClass:[NSNumber class]])
        {
            aTextField.text = [NSString stringWithFormat:@"%1.6f", inputValue];
        }
        else {
            aTextField.text = inputValue;
        }
    }
}

- (void)setDropDownData:(NSDictionary<NSString *,UIDropdown *> *)dropDowns
{
    Encryptor *enc = [[Encryptor alloc] init];
    for(NSString *key in dropDowns)
    {
        UIDropdown *aDropDown = dropDowns[key];
        aDropDown.selectedData = [enc getUserDefaultsObjectForKey:key];
        if(aDropDown.selectedData && [aDropDown.selectedData isKindOfClass:NSDictionary.class])
        {
            if ([aDropDown.selectedData allValues].count > 0) {
                [aDropDown setText:[aDropDown.selectedData allValues][0]];
            }
        }
    }
}

- (void)saveDropDownData:(NSDictionary<NSString *,UIDropdown *> *)dropDowns
{
    Encryptor *encryptor = [[Encryptor alloc] init];
    for(NSString *key in dropDowns)
    {
        UIDropdown *aDropDown = dropDowns[key];
        [encryptor setUserDefaultsObject:aDropDown.selectedData forKey:key];
    }
}

- (void)inputHasError:(BOOL)hasError textField:(UITextField *)textField
{
    if(hasError)
    {
        textField.layer.borderColor=[[UIColor redColor]CGColor];
        textField.layer.borderWidth= 0.8f;
    }
    else textField.layer.borderColor=[[UIColor clearColor]CGColor];
}

- (void)showErrorLabel:(NSString *)errorMsg withUiTextfield:(UITextField *)textField
{
    AMPopTip *popTip = [AMPopTip popTip];
    [popTip showText:errorMsg direction:AMPopTipDirectionUp maxWidth:200.0 inView:[textField superview] fromFrame:textField.frame duration:3.0];
}

-(void) saveImageToLocalStorage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath
{
    if ([[extension lowercaseString] isEqualToString:@"png"]) {
        [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
    } else if ([[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"]) {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
        NSLog(@"Image save success. %@.jpg", imageName);
    } else {
        NSLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG)", extension);
    }
}

- (void) removeImageFromLocalStorage:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *directoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", filename]];
    NSError *error;
    [fileManager removeItemAtPath:filePath error:&error];
}


- (BOOL)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename andSaveWithKey:(NSString *)saveKey
{
    [self showLoading];
    Encryptor *encryptor = [[Encryptor alloc] init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    [self saveImageToLocalStorage:image withFileName:filename ofType:@"jpg" inDirectory:documentsDirectory];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.1);
    NSString *urlString = [[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:@"api/document/upload"]];
    NSString *nik = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [request setValue:[CustomerOnboardingData getToken] forHTTPHeaderField:@"Authorization"];
    NSMutableData *postbody = [NSMutableData data];
    
    // FILENAME param
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"filename\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"%@.jpg", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // NIK param
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"nik\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"%@",nik] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // FILE param
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:imageData]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:postbody];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    if(error)
    {
        NSLog(@"Gagal menyimpan file. Reason : \n%@", error);
    }
    else
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if((long)[httpResponse statusCode] == 200)
        {
            NSLog(@"Berhasil menyimpan file dengan ID: %@", returnString);
            [encryptor setUserDefaultsObject:returnString forKey:saveKey];
            return TRUE;
        }
        else
        {
            NSLog(@"Gagal menyimpan file. Reason : \n%@", returnString);
        }
    }
    [self hideLoading];
    return FALSE;
}

- (UIImage *)fixOrientation:(UIImage *)image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename andSaveWithKey:(NSString *)saveKey onCompletionPerformSegueWithIdentifier:(nullable NSString *)segueIdentifier fromController:(UIViewController *)viewController
{
    [self doSaveImageToLogicalDocs:image withFilename:filename saveWithKey:saveKey andWithQuality:0.1 onCompletionPerformSegueWithIdentifier:segueIdentifier fromController:viewController withCompletion:^{
        // do nothing
    }];
}

- (void)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename saveWithKey:(NSString *)saveKey andWithQuality:(double)quality onCompletionPerformSegueWithIdentifier:(nullable NSString *)segueIdentifier fromController:(UIViewController *)viewController
{
    [self doSaveImageToLogicalDocs:image withFilename:filename saveWithKey:saveKey andWithQuality:quality onCompletionPerformSegueWithIdentifier:segueIdentifier fromController:viewController withCompletion:^{
        // do nothing
    }];
}

- (void)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename andSaveWithKey:(NSString *)saveKey onCompletionPerformSegueWithIdentifier:(nullable NSString *)segueIdentifier fromController:(UIViewController *)viewController withCompletion:(void(^)(void))completion
{
    [self doSaveImageToLogicalDocs:image withFilename:filename saveWithKey:saveKey andWithQuality:0.1 onCompletionPerformSegueWithIdentifier:segueIdentifier fromController:viewController withCompletion:completion];
}

- (void)doSaveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename saveWithKey:(NSString *)saveKey andWithQuality:(double)quality onCompletionPerformSegueWithIdentifier:(nullable NSString *)segueIdentifier fromController:(UIViewController *)viewController withCompletion:(void(^)(void))completion
{
    [self showLoading];
    dispatch_async(dispatch_queue_create("uploadFile", nil), ^{
        Encryptor *encryptor = [[Encryptor alloc] init];
        __block BOOL isSuccess;
        
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        [self saveImageToLocalStorage:image withFileName:filename ofType:@"jpg" inDirectory:documentsDirectory];
        
        UIImage *fixImage = [self fixOrientation:image];
        if(fixImage.size.width > 2160.0)
        {
            fixImage = [self imageWithImage:fixImage scaledToSize:CGSizeMake(fixImage.size.width / 8, fixImage.size.height / 8)];
        }
        else if(fixImage.size.width > 1080.0)
        {
            fixImage = [self imageWithImage:fixImage scaledToSize:CGSizeMake(fixImage.size.width / 4, fixImage.size.height / 4)];
        }
        NSData *imageData = UIImageJPEGRepresentation(fixImage, quality);
        NSString *urlString = [[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:@"api/document/upload"]];
        NSString *nik = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
        [request setValue:[CustomerOnboardingData getToken] forHTTPHeaderField:@"Authorization"];
        NSMutableData *postbody = [NSMutableData data];
        
        // FILENAME param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"filename\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"%@.jpg", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // NIK param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"nik\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"%@",nik] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // FILE param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[NSData dataWithData:imageData]];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:postbody];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if(ONBOARDING_SAFE_MODE) {
                returnString = [Encryptor AESDecrypt:returnString];
            }
            
            if(error)
            {
                isSuccess = NO;
            }
            else
            {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if((long)[httpResponse statusCode] == 200)
                {
                    isSuccess = YES;
                    [encryptor setUserDefaultsObject:returnString forKey:saveKey];
                }
                else
                    isSuccess = NO;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideLoading];
                if(isSuccess && !(segueIdentifier == nil || [segueIdentifier isEqual:[NSNull null]]))
                {
                    [viewController performSegueWithIdentifier:segueIdentifier sender:self];
                }
                else if (isSuccess)
                {
                    completion();
                }
                else
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:NSLocalizedString(@"GENERAL_FAILURE", @"General Failure") preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:actionOk];
                    [viewController presentViewController:alert animated:YES completion:nil];
                }
            });
           }] resume];
    });
}

- (UIImage *)loadImageFromLocalStorageWithFileName:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath
{
    UIImage * result = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.%@", directoryPath, fileName, [extension lowercaseString]]];
    
    return result;
}

- (void)showLoading
{
    [self setAlertLoading:[[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil]];
    if(![[self alertLoading] isVisible])
    {
        UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
        [viewLoading setBackgroundColor:[UIColor clearColor]];
        UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [loadingView setColor:[UIColor blackColor]];
        [loadingView startAnimating];
        [viewLoading addSubview:loadingView];
        
        [[self alertLoading] setContentView:viewLoading];
        [[self alertLoading] show];
    }
}

- (void)hideLoading
{
    [[self alertLoading] dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)showAlertErrorWithTitle:(NSString *)title andMessage:(NSString *)message sender:(UIViewController *)sender {
    UIAlertController *uac = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [uac addAction:actionOk];
    [self setAlertController:uac];
    [sender presentViewController:[self alertController] animated:YES completion:nil];
}

- (void)showAlertErrorWithTitle:(NSString *)title andMessage:(NSString *)message sender:(UIViewController *)sender completion:(void (^)(void))completion{
    UIAlertController *uac = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [uac addAction:actionOk];
    [self setAlertController:uac];
    [sender presentViewController:[self alertController] animated:YES completion:nil];
}

- (void)showAlertErrorWithCompletion:(NSString *)title andMessage:(NSString *)message sender:(UIViewController *)sender handler:(void (^)(UIAlertAction * _Nonnull))handler {
    UIAlertController *uac = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:handler];
    [uac addAction:actionOk];
    [self setAlertController:uac];
    [sender presentViewController:[self alertController] animated:YES completion:nil];
}

- (void) setWizardBarOnTop:(UIView *)parentView onYPos:(CGFloat)y withDone:(double)done fromTotal:(double)total
{
    CGFloat width = (done/total) * parentView.frame.size.width;
    CGRect frame = CGRectMake(0, y, width, 5);
    CGRect frameGray = CGRectMake(0, y, parentView.frame.size.width, 5);
    UIView *bar = [[UIView alloc] initWithFrame:frame];
    UIView *barGray = [[UIView alloc] initWithFrame:frameGray];
    [bar setBackgroundColor:[UIColor colorWithRed:247.0f/255.0f green:217.0f/255.0f blue:130.0f/255.0f alpha:1.0]];
    [barGray setBackgroundColor:[UIColor grayColor]];
    [parentView addSubview:barGray];
    [parentView addSubview:bar];
}

@end
