//
//  ToastView.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 8/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToastView : UIView

@property (strong, nonatomic) NSString *text;

+ (void)showToastInParentView: (UIView *)parentView withText:(NSString *)text withDuaration:(float)duration;

@end
