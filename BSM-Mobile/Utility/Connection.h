//
//  Connection.h
//  Aurora
//
//  Created by lds on 12/23/13.
//  Copyright (c) 2013 Inmagine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@protocol ConnectionDelegate<NSObject>
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType;
- (void)errorLoadData:(NSError *)error;
- (void)reloadApp;
@end

@class Preset;
@interface Connection : NSObject<NSURLSessionTaskDelegate>{
    NSURLSession *connection;
    NSString *requestType;
    BOOL needDecrypt;
    BOOL continueLoading;
}

@property (nonatomic, retain) id <ConnectionDelegate> delegate;
@property (nonatomic, assign) BOOL fav;

- (id)initWithDelegate:(id)delegate;
- (void)sendPost:(NSMutableDictionary *)requestData  needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted;
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking;
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking favorite:(BOOL)favorite;
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading  encrypted:(BOOL)encrypted banking:(BOOL)isBanking continueLoading:(BOOL)continueLoading;
- (void)sendPostParmUrl:(NSString *)parmUrl needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted banking:(BOOL)isBanking;

@end
