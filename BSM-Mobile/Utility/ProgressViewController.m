//
//  ProgressViewController.m
//  BSM-Mobile
//
//  Created by ARS on 25/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ProgressViewController.h"

@interface ProgressViewController ()

@property (strong, nonatomic) NSURLConnection *connectionManager;
@property (strong, nonatomic) NSMutableData *downloadedMutableData;
@property (strong, nonatomic) NSURLResponse *urlResponse;
@property (weak, nonatomic) ProgressView *progressView;
@property (weak, nonatomic) NSString *filename;

@end

@implementation ProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (instancetype)initWithDefaultConfigAndProgressViewText:(NSString *)progressViewText
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        self.progressView = (ProgressView *)self.view;
        self.progressView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.80];
        self.progressView.progressText.text = progressViewText;
        [self.progressView.progressBar setProgress:0];
        self.progressView.delegate = self;
    }
    return self;
}

- (void)doDownloadFromUrl:(NSString *)downloadUrl saveWithFilename:(NSString *)filename
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:downloadUrl]
                                                cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                            timeoutInterval:60.0];
    self.connectionManager = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    self.downloadedMutableData = [[NSMutableData alloc] init];
    self.urlResponse = [[NSURLResponse alloc] init];
    self.filename = filename;
}

#pragma mark Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.urlResponse = response;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.downloadedMutableData appendData:data];
    float downloaded = (float) self.downloadedMutableData.length;
    float total = (float) self.urlResponse.expectedContentLength;
    self.progressView.progressBar.progress = (downloaded / total);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, _filename];
    [self.downloadedMutableData writeToFile:filePath atomically:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate progressBarDidFinishLoading:filePath];
    }];
}

@end
