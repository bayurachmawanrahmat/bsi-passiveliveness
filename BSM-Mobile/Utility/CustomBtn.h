//
//  CustomBtn.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 13/11/20.
//  Copyright © 2020 lds. All rights reserved.
//


#import <UIKit/UIKit.h>
//#import "UIViewController+ECSlidingViewController.h"

typedef NS_ENUM(NSInteger, CustomBtnColorSet){
    DISABLEDCOLORSET = 2,
    PRIMARYCOLORSET = 0,
    SECONDARYCOLORSET = 1,
    TERTIARYCOLORSET = 3
};

@interface CustomBtn: UIButton

- (void)setEnabled:(BOOL)enabled;
- (void)setColorSet:(int)colorSet;

@end
