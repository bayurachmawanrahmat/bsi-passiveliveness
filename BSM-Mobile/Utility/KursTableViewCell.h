//
//  KursTableViewCell.h
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KursTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *label1;
@property (strong, nonatomic) UILabel *label2;
@property (strong, nonatomic) UILabel *label3;

@end
