//
//  UITextViewWorkaraound.m
//  BSM-Mobile
//
//  Created by ARS on 07/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UITextViewWorkaround.h"
#import <objc/runtime.h>

@implementation UITextViewWorkaraound

+ (void)executeWorkaround {
    if (@available(iOS 13.2, *)) {
    }
    else {
        const char *className = "_UITextLayoutView";
        Class cls = objc_getClass(className);
        if (cls == nil) {
            cls = objc_allocateClassPair([UIView class], className, 0);
            objc_registerClassPair(cls);
            #if DEBUG
            printf("added %s dynamically\n", className);
            #endif
        }
    }
}

@end
