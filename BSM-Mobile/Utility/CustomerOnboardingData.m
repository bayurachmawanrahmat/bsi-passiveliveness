//
//  CustomerOnboardingData.m
//  BSM-Mobile
//
//  Created by ARS on 21/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@implementation CustomerOnboardingData
{
    Encryptor *encryptor;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        encryptor = [[Encryptor alloc] init];
    }
    return self;
}

- (void)generateNomorAntrian
{
    _requestIsSuccess = NO;
    if(!_dispatchGroup) _dispatchGroup = dispatch_group_create();
    if(!_nomorAntrian) _nomorAntrian = [[NSString alloc] init];
    dispatch_group_enter(_dispatchGroup);
    NSString *url =[[[self class] apiUrl] stringByAppendingString:[[self class] safeEndpoint:@"api/nasabah/getReservationCode"]];
    NSString *token = [[self class] getToken];
    NSLog(@"---------------- %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if((long)[httpResponse statusCode] == 200)
        {
            if(ONBOARDING_SAFE_MODE)
            {
                NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                _nomorAntrian = [Encryptor AESDecrypt:encString];
            }
            else
                _nomorAntrian = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            [encryptor setUserDefaultsObject:[_nomorAntrian stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY];
            _requestIsSuccess = YES;
        }
        else
        {
            _nomorAntrian = @"";
            _requestIsSuccess = NO;
        }
        dispatch_group_leave(_dispatchGroup);
    }] resume];
}

- (NSMutableDictionary *)restoreDataToPost
{
    NSMutableDictionary *modifiedData = [[NSMutableDictionary alloc] initWithDictionary:[encryptor getUserDefaultsObjectForKey:ONBOARDING_RESTORE_DATA_KEY]];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY])
        [modifiedData setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY] forKey:@"kode_registrasi"];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR])
        [modifiedData setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR] forKey:@"video_assist_gender"];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_RANDOM_DESCRIPTION_KEY])
        [modifiedData setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_RANDOM_DESCRIPTION_KEY] forKey:@"desc_video_verifikasi"];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_JENIS_KYC_KEY])
        [modifiedData setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENIS_KYC_KEY] forKey:@"jenis_kyc"];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_LOCATION_LAT])
        [modifiedData setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_LOCATION_LAT] forKey:@"lat"];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_LOCATION_LNG])
            [modifiedData setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_LOCATION_LNG] forKey:@"lng"];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_DEVICE])
        [modifiedData setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_DEVICE] forKey:@"device"];
    return modifiedData;
}

- (NSMutableDictionary *)getDataToPost
{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    @try {
        NSString *jenisTabungan = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY] isEqualToString:ONBOARDING_TAB_MUDHARABAHID] ? ONBOARDING_TAB_MUDHARABAH : ONBOARDING_TAB_WADIAH;
        
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY] forKey:@"msmsid"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY] forKey:@"kode_registrasi"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_CABANG_KEY]objectForKey:@"kode"] forKey:@"kode_cabang"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] forKey:@"email"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY] forKey:@"no_hp"];
        [data setObject:jenisTabungan forKey:@"jenis_tabungan"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY] forKey:@"jenis_tabungan_id"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_PDDK_AKH] forKey:@"pddk_akh"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_AYAH] forKey:@"nama_lgkp_ayah"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_GOL_DARAH] forKey:@"gol_darah"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_KEL_NAME] forKey:@"kel_name"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_KEC_NAME] forKey:@"kec_name"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_KAB_NAME] forKey:@"kab_name"];
        
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_KAB_NAME] forKey:@"prop_name"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP] forKey:@"nama_lgkp"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_AGAMA] forKey:@"agama"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_STATUS_KAWIN] forKey:@"status_kawin"];
        
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_JENIS_PKRJN] forKey:@"jenis_pkrjn"];
        
        NSString *kodePos = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_ZIP];
        if([kodePos isEqual:[NSNull null]] || kodePos == nil || [kodePos isEqualToString:@"null"] || [kodePos isEqualToString:@"<null>"]) {
            [data setObject:@"" forKey:@"kode_pos"];
        } else {
            [data setObject:kodePos forKey:@"kode_pos"];
        }
        if([encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_DUSUN]) [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_DUSUN] forKey:@"dusun"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_TGL_LHR] forKey:@"tgl_lhr"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_STAT_HBKEL] forKey:@"stat_hbkel"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_EKTP_STATUS] forKey:@"ektp_status"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_TMPT_LAHIR] forKey:@"tmpt_lahir"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU] forKey:@"nama_lgkp_ibu"];
        
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_KK] forKey:@"no_kk"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_RT] forKey:@"no_rt"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY] forKey:@"nik"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_ALAMAT] forKey:@"alamat"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN] forKey:@"jenis_klmin"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_RW] forKey:@"no_rw"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_KEL] forKey:@"no_kel"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_KEC] forKey:@"no_kec"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_KAB] forKey:@"no_kab"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_PROP] forKey:@"no_prop"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_ID_KAB] forKey:@"id_kab"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_ID_PROV] forKey:@"id_prov"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_WARGANEGARA] forKey:@"warganegara"];
        NSInteger islamatbeda = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY] integerValue];
        [data setObject:(islamatbeda == 1 ? @"0" : @"1") forKey:@"is_domisili_sesuai_ktp"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_KEY] forKey:@"domisili_alamat"];
        NSString *rtrw = [NSString stringWithFormat:@"%@/%@", [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_RT_KEY], [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_RW_KEY]];
        [data setObject:rtrw forKey:@"domisili_rtrw"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KELURAHAN_KEY] objectForKey:@"nama"] forKey:@"domisili_kelurahan"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KECAMATAN_KEY] objectForKey:@"nama"] forKey:@"domisili_kecamatan"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY] objectForKey:@"bsmname"] forKey:@"domisili_kabkota"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY] objectForKey:@"bsmid"] forKey:@"domisili_kabkota_id"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY] objectForKey:@"bsmname"] forKey:@"domisili_provinsi"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY] objectForKey:@"bsmid"] forKey:@"domisili_provinsi_id"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY] forKey:@"domisili_kodepos"];
        if([[data objectForKey:ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY] isEqualToString:@"null"] || [[data objectForKey:ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY] isEqualToString:@"<null>"])
            [data setObject:@"" forKey:ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY];
        
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_PENDIDIKAN_KEY] objectForKey:@"nilai"] forKey:@"pendidikan_terakhir_id"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_NPWP_ISPUNYA_KEY] forKey:@"is_punya_npwp"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_NPWP_NOMOR_KEY] forKey:@"no_npwp"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_NPWP_JENIS_KEPEMILIKAN_KEY] forKey:@"jenis_kepemilikan_npwp"];
        
        [data setObject:((NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_SUMBER_DANA_KEY]).allKeys[0]  forKey:@"sumber_dana_id"];
        [data setObject:((NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_TUJUAN_BUKA_KEY]).allKeys[0]  forKey:@"tujuan_buka_rekening_id"];
        [data setObject:((NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_TRANSAKSI_KEY]).allKeys[0]  forKey:@"transaksi_id"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_ISZAKAT_KEY] forKey:@"zakat"];
        
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KTP_FILEID_KEY] forKey:@"ktpid"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_SELFIE_FILEID_KEY] forKey:@"selfieid"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_NPWP_FILEID_KEY] forKey:@"npwpid"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_TTD_FILEID_KEY] forKey:@"ttdid"];
        
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENIS_KYC_KEY] forKey:@"jenis_kyc"];
        
        [data setObject:@"IOS" forKey:@"user_agent"];
        
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_STATUS_PEKERJAAN_KEY] objectForKey:@"nilai"] forKey:@"status_pekerjaan"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_KEY] objectForKey:@"nilai"] forKey:@"jenis_pekerjaan_id"];
        
        if ([[encryptor getUserDefaultsObjectForKey:ONBOARDING_INPUT_DATA_IDENTITY_TYPE_KEY] isEqualToString:ONBOARDING_INPUT_DATA_IDENTITY_MANUAL]) {
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_SC] forKey:@"sc_dukcapil_nama"];
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU_SC] forKey:@"sc_dukcapil_namaibu"];
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_TGL_LHR_SC] forKey:@"sc_dukcapil_tgllahir"];
        }
        
        if([[data objectForKey:@"status_pekerjaan"] isEqualToString:@"STUDENT"] || [[data objectForKey:@"status_pekerjaan"] isEqualToString:@"HOUSEWIFE"])
        {
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_SUAMI_KEY] forKey:@"nama_penyumbang_dana"];
            [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_SUAMI_KEY] objectForKey:@"nilai"] forKey:@"jenis_pekerjaan_penyumbang_dana"];
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_ALAMAT_SUAMI_KEY] forKey:@"alamat_penyumbang_dana"];
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_TELEPON_SUAMI_KEY] forKey:@"no_telfon_penyumbang_dana"];
        }
        else
        {
            [data setObject:@"" forKey:@"nama_penyumbang_dana"];
            [data setObject:@"" forKey:@"jenis_pekerjaan_penyumbang_dana"];
            [data setObject:@"" forKey:@"alamat_penyumbang_dana"];
            [data setObject:@"" forKey:@"no_telfon_penyumbang_dana"];
        }
        
        if([[data objectForKey:@"status_pekerjaan"] isEqualToString:@"EMPLOYED"] || [[data objectForKey:@"status_pekerjaan"] isEqualToString:@"SELF-EMPLOYED"])
        {
            [data setObject:((NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_JABATAN_KEY]).allValues[0]  forKey:@"jabatan"];
            [data setObject:((NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PENGHASILAN_KEY]).allKeys[0]  forKey:@"penghasilan_perbulan_id"];
            // old param nama perusahaan
//            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY] forKey:@"nama_perusahaan"];
            [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY] objectForKey:@"value_name_company"] forKey:@"nama_perusahaan"];
            // default company id Lainnya 9999
            if (![[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY] objectForKey:@"id_company"] isEqualToString:@"9999"]) {
                [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY] objectForKey:@"tagging_code"] forKey:@"perusahaan_tagging"];
                [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY] objectForKey:@"cifno"] forKey:@"perusahaan_cif"];
            }
            NSDictionary *bidangUsahaDictionary = (NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_BIDANG_USAHA_KEY];
            NSLog(@"bidang usaha %@", bidangUsahaDictionary);
            [data setObject:((NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_BIDANG_USAHA_KEY]).allKeys[0]  forKey:@"bidang_usaha_id"];
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_ALAMAT_PERUSAHAAN_KEY] forKey:@"alamat_perusahaan"];
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_TELEPON_PERUSAHAAN_KEY] forKey:@"telp_perusahaan"];
            [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY] objectForKey:@"raw"] forKey:@"mulai_bekerja"];
        }
        else
        {
            [data setObject:@""  forKey:@"jabatan"];
            [data setObject:@"" forKey:@"penghasilan_perbulan_id"];
            [data setObject:@"" forKey:@"nama_perusahaan"];
            [data setObject:@""  forKey:@"bidang_usaha_id"];
            [data setObject:@"" forKey:@"alamat_perusahaan"];
            [data setObject:@"" forKey:@"telp_perusahaan"];
            [data setObject:@"" forKey:@"mulai_bekerja"];
        }
        
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISKARTU_KEY] objectForKey:@"msmname"] forKey:@"jenis_kartu"];
        [data setObject:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISKARTU_KEY] objectForKey:@"cardiid"] forKey:@"kode_jenis_kartu"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_KODEPROMO_KEY] forKey:@"kode_promo"];
        if ([[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENIS_KYC_KEY] isEqual:@"VIDEO"]) {
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR] forKey:@"video_assist_gender"];
            [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_RANDOM_DESCRIPTION_KEY] forKey:@"desc_video_verifikasi"];
        }
        else {
            [data setObject:@"L" forKey:@"video_assist_gender"];
        }
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_NASABAH_SETUJU_KEY] forKey:@"persetujuan_nasabah"];
        
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_LOCATION_LAT] forKey:@"lat"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_LOCATION_LNG] forKey:@"lng"];
        [data setObject:[encryptor getUserDefaultsObjectForKey:ONBOARDING_CUSTOMER_DEVICE] forKey:@"device"];
        NSLog(@"-------DATA-------\n%@", data);
    } @catch (NSException *exception) {
        NSLog(@"exception : %@", exception);
    } @finally {
        return data;
    }
}

- (void)sendDataToBackend
{
    _requestIsSuccess = NO;
    NSString *token = [CustomerOnboardingData getToken];
    if(!_dispatchGroup) _dispatchGroup = dispatch_group_create();
    dispatch_group_enter(_dispatchGroup);
    
    if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY])
    {
        _nomorAntrian = [[NSString alloc] init];
        NSString *url =[[[self class] apiUrl] stringByAppendingString:[[self class] safeEndpoint:@"api/nasabah/getReservationCode"]];
        NSLog(@"---------------- %@", url);
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
        [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
        [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if((long)[httpResponse statusCode] == 200)
            {
                if(ONBOARDING_SAFE_MODE)
                {
                    NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    self->_nomorAntrian = [Encryptor AESDecrypt:encString];
                }
                else
                    self->_nomorAntrian = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                [self->encryptor setUserDefaultsObject:[self->_nomorAntrian stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY];
                
                NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
                NSString *endpoint =@"api/nasabah/createv2";
                NSURL *url = [NSURL URLWithString:[[[self class] apiUrl] stringByAppendingString:[[self class] safeEndpoint:endpoint]]];
                
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
                
                [request setHTTPMethod:@"POST"];
                [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
                [request setValue:token forHTTPHeaderField:@"Authorization"];
                
                NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                if([self->encryptor getUserDefaultsObjectForKey:ONBOARDING_RESTORE_DATA_KEY] != nil)
                {
                    data = [self restoreDataToPost];
                }
                else
                    data = [self getDataToPost];
                
                NSDictionary *postDataEncrypted = [[self class] getEncryptedData:data];
                
                NSString *mapData = [[self class] stringParamFromData:postDataEncrypted];
                [request setHTTPBody:[mapData dataUsingEncoding:NSUTF8StringEncoding]];
                
                NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(!error)
                    {
                        NSMutableDictionary *jsonObject;
                        if(ONBOARDING_SAFE_MODE)
                        {
                            NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            NSData *decryptedData = [Encryptor AESDecryptData:ret];
                            NSString *decryptedString = [Encryptor AESDecrypt:ret];
                            jsonObject = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                        } else {
                            jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                        }
                        
                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                        if((long)[httpResponse statusCode] == 200)
                        {
                            self->_requestIsSuccess = YES;
                            self->_responseNasabah = jsonObject;
                            NSLog(@"Berhasil create nasabah.");
                            
                            NSString *msmsid = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY];
                            [self retrieveNasabah:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:self->_dispatchGroup rootUrl:[[self class] apiUrl]];
                        }
                        else
                        {
                            self->_requestIsSuccess = NO;
                            if (jsonObject)
                                NSLog(@"Gagal create nasabah : \n%@", jsonObject);
                            else {
                                NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                NSData *decryptedData = [Encryptor AESDecryptData:encString];
                                jsonObject = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                                NSLog(@"Gagal create nasabah : \n%@", jsonObject);
                            }
                            dispatch_group_leave(self->_dispatchGroup);
                        }
                    }
                    else
                    {
                        NSLog(@"Gagal create nasabah : %@", error);
                        dispatch_group_leave(self->_dispatchGroup);
                    }
                }];
                
                [postDataTask resume];
            }
            else
            {
                self->_nomorAntrian = @"";
                self->_requestIsSuccess = NO;
                NSLog(@"Gagal generate nomor antrian. Reason : \n%@", httpResponse);
            }
        }] resume];
    }
    else
    {
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
        NSString *endpoint =@"api/nasabah/createv2";
        NSURL *url = [NSURL URLWithString:[[[self class] apiUrl] stringByAppendingString:[[self class] safeEndpoint:endpoint]]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
        [request setValue:token forHTTPHeaderField:@"Authorization"];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        if([encryptor getUserDefaultsObjectForKey:ONBOARDING_RESTORE_DATA_KEY] != nil)
        {
            data = [self restoreDataToPost];
        }
        else
            data = [self getDataToPost];
        
        NSDictionary *postDataEncrypted = [[self class] getEncryptedData:data];
        
        NSString *mapData = [[self class] stringParamFromData:postDataEncrypted];
        [request setHTTPBody:[mapData dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *resData, NSURLResponse *response, NSError *error) {
            if(!error)
            {
                NSMutableDictionary *jsonObject;
                if(ONBOARDING_SAFE_MODE)
                {
                    NSString *ret = [[NSMutableString alloc] initWithData:resData encoding:NSUTF8StringEncoding];
                    NSData *decryptedData = [Encryptor AESDecryptData:ret];
                    NSString *decryptedString = [Encryptor AESDecrypt:ret];
                    jsonObject = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                } else {
                    jsonObject = [NSJSONSerialization JSONObjectWithData:resData options:kNilOptions error:&error];
                }
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if((long)[httpResponse statusCode] == 200)
                {
                    self->_requestIsSuccess = YES;
                    self->_responseNasabah = jsonObject;
                    NSLog(@"Berhasil create nasabah.");
                    
                    NSString *msmsid = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY];
                    [self retrieveNasabah:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:self->_dispatchGroup rootUrl:[[self class] apiUrl]];
                }
                else
                {
                    self->_requestIsSuccess = NO;
                    NSLog(@"Gagal create nasabah : \n%@", jsonObject);
                    dispatch_group_leave(self->_dispatchGroup);
                }
            }
            else
            {
                NSLog(@"Gagal create nasabah : %@", error);
                dispatch_group_leave(self->_dispatchGroup);
            }
        }];
        
        [postDataTask resume];
    }
}

+ (void)getSynchronousFromEndpoint:(NSString *)endpoint withParam:(nullable NSDictionary *)param returningData:(NSMutableDictionary **)returnData
{
    NSString *url =[[[[self class] apiUrl] stringByAppendingString:[[self class] safeEndpoint:endpoint]] stringByAppendingString:[[self class] stringParamFromData:param]];
    NSString *token = [[self class] getToken];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *response;
    NSError *error;
    *returnData = [[NSMutableDictionary alloc] init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error)
    {
        [*returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
        [*returnData setValue:error forKey:@"error_message"];
    }
    else
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSDictionary *value  = [NSJSONSerialization
                                JSONObjectWithData:data
                                options:kNilOptions
                                error:&error];
        if((long)[httpResponse statusCode] == 200) {
            [*returnData setValue:[NSNumber numberWithBool:YES] forKey:@"is_success"];
            if(value)
                [*returnData setValue:value forKey:@"data"];
            else
            {
                NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                if(ret)
                {
                    if(ONBOARDING_SAFE_MODE)
                    {
                        NSData *decryptedData = [Encryptor AESDecryptData:ret];
                        value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                        if(value)
                            [*returnData setValue:value forKey:@"data"];
                        else
                            [*returnData setValue:[Encryptor AESDecrypt:ret] forKey:@"data"];
                    }
                    else
                        [*returnData setValue:ret forKey:@"data"];
                }
            }
        }
        else
        {
            if(ONBOARDING_SAFE_MODE)
            {
                NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"ERROR : %@", [Encryptor AESDecrypt:ret]);
            }
            else
            {
                [*returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
                [*returnData setValue:[value objectForKey:@"message"] forKey:@"error_message"];
            }
        }
    }
}

+ (void)getFromEndpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData
{
    [[self class] callGetFromEndpoint:endpoint dispatchGroup:dispatchGroup returnData:returnData rootUrl:[[self class] apiUrl]];
}

+ (void)getFromEndpointFr:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData{
    [[self class] callGetFromEndpoint:endpoint dispatchGroup:dispatchGroup returnData:returnData rootUrl:[[self class] apiFrUrl]];
}

+ (void)callGetFromEndpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData rootUrl:(NSString *)rootUrl{
    dispatch_group_enter(dispatchGroup);
    NSString *url =[rootUrl stringByAppendingString:[[self class] safeEndpoint:endpoint]];
    NSString *token = [[self class] getToken];
    NSLog(@"---------------- %@", [rootUrl stringByAppendingString:endpoint]);
    NSLog(@"---------------- %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
            [returnData setValue:error forKey:@"error_message"];
        }
        else
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *value  = [NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:kNilOptions
                                    error:&error];
            NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if((long)[httpResponse statusCode] == 200) {
                [returnData setValue:[NSNumber numberWithBool:YES] forKey:@"is_success"];
                if(value)
                    [returnData setValue:value forKey:@"data"];
                else
                {
                    if(ret)
                    {
                        if(ONBOARDING_SAFE_MODE)
                        {
                            NSData *decryptedData = [Encryptor AESDecryptData:ret];
                            value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                            if(value)
                                [returnData setValue:value forKey:@"data"];
                            else
                                [returnData setValue:[Encryptor AESDecrypt:ret] forKey:@"data"];
                        }
                        else
                            [returnData setValue:ret forKey:@"data"];
                    }
                }
            }
            else
            {
                if(ONBOARDING_SAFE_MODE)
                {
                    NSData *decryptedData = [Encryptor AESDecryptData:ret];
                    value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                    
                    [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
                    if(value) {
                        [returnData setValue:[value objectForKey:@"message"] forKey:@"error_message"];
                        [returnData setValue:[value objectForKey:@"status"] forKey:@"status"];
                    }
                    
                    NSLog(@"ERROR : %@", [Encryptor AESDecrypt:ret]);
                }
                else
                {
                    [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
                    [returnData setValue:[value objectForKey:@"message"] forKey:@"error_message"];
                    [returnData setValue:[value objectForKey:@"status"] forKey:@"status"];
                }
            }
        }
        dispatch_group_leave(dispatchGroup);
    }] resume];
}

- (void)retrieveNasabah:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup rootUrl:(NSString *)rootUrl{
    
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    NSString *url =[rootUrl stringByAppendingString:[[self class] safeEndpoint:endpoint]];
    NSString *token = [[self class] getToken];
    NSLog(@"---------------- %@", [rootUrl stringByAppendingString:endpoint]);
    NSLog(@"---------------- %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
            [returnData setValue:error forKey:@"error_message"];
        }
        else
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *value  = [NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:kNilOptions
                                    error:&error];
            if((long)[httpResponse statusCode] == 200) {
                [returnData setValue:[NSNumber numberWithBool:YES] forKey:@"is_success"];
                NSDictionary *nasabahData;
                if(value)
                    [returnData setValue:value forKey:@"data"];
                else
                {
                    NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    if(ret)
                    {
                        if(ONBOARDING_SAFE_MODE)
                        {
                            NSData *decryptedData = [Encryptor AESDecryptData:ret];
                            value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                            if(value) {
                                [returnData setValue:value forKey:@"data"];
                                nasabahData = [returnData objectForKey:@"data"];
                                self->_requestIsSuccess = YES;
                                self->_responseNasabah = returnData;
                                [self->encryptor setUserDefaultsObject:nasabahData forKey:ONBOARDING_NASABAH];
                            }else {
                                [returnData setValue:[Encryptor AESDecrypt:ret] forKey:@"data"];
                                self->_requestIsSuccess = YES;
                                self->_responseNasabah = returnData;
                                [self->encryptor setUserDefaultsObject:returnData forKey:ONBOARDING_NASABAH];
                            }
                        }
                        else {
                            [returnData setValue:ret forKey:@"data"];
                            nasabahData = [returnData objectForKey:@"data"];
                            self->_requestIsSuccess = YES;
                            self->_responseNasabah = returnData;
                            [self->encryptor setUserDefaultsObject:nasabahData forKey:ONBOARDING_NASABAH];
                        }
                    }
                }
            }
            else
            {
                if(ONBOARDING_SAFE_MODE)
                {
                    NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    self->_requestIsSuccess = NO;
                    self->_responseNasabah = returnData;
                    NSLog(@"ERROR : %@", [Encryptor AESDecrypt:ret]);
                }
                else
                {
                    [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
                    [returnData setValue:[value objectForKey:@"message"] forKey:@"error_message"];
                    self->_requestIsSuccess = NO;
                    self->_responseNasabah = returnData;
                }
            }
        }
        dispatch_group_leave(dispatchGroup);
    }] resume];
}

+ (void) logUrlRequest:(NSString *)url withHeaders:(nullable NSDictionary *)header startWithString:(NSString *)startString
{
    NSArray *arrayUrl = [url componentsSeparatedByString:@"?"];
    NSString *baseUrl = arrayUrl[0];
    NSMutableString *params = [[NSMutableString alloc] initWithString:@""];
    if(arrayUrl.count > 1)
    {
        NSArray *arrayParams = [(NSString *)arrayUrl[1] componentsSeparatedByString:@"&"];
        for (int i = 0; i < arrayParams.count; i += 1) {
            if([params isEqualToString:@""])
               [params appendString:@"{"];
            [params appendString:[arrayParams objectAtIndex:i]];
        }
        if(header != nil)
        {
            for (NSString *key in header) {
                if([params isEqualToString:@""])
                    [params appendFormat:@"%@=%@", key, [header objectForKey:key]];
                else
                    [params appendFormat:@",%@=%@", key, [header objectForKey:key]];
            }
        }
        if([params isEqualToString:@""])
           [params appendString:@"{}"];
        else
            [params appendString:@"}"];
        
    }
    NSLog(@"%@ (%@) : %@ ", startString, baseUrl, params);
}

+ (void)postToEndPoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData
{
    [[self class] callPostToEndPoint:endpoint dispatchGroup:dispatchGroup postData:postData returnData:returnData rootUrl:[[self class] apiUrl]];
}

+(void)postToEndPoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData isResEncrypted:(BOOL)isResEncrypted {
    if (isResEncrypted == YES) {
        [[self class] callPostToEndPoint:endpoint dispatchGroup:dispatchGroup postData:postData returnData:returnData rootUrl:[[self class] apiUrl]];
    } else {
        [[self class] callPostToEndPointDisableEncryptRes:endpoint dispatchGroup:dispatchGroup postData:postData returnData:returnData rootUrl:[[self class] apiUrl]];
    }
}

+ (void)postToEndPointUrl:(NSString *)url endpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData {
    [[self class] callPostToEndPoint:endpoint dispatchGroup:dispatchGroup postData:postData returnData:returnData rootUrl:url];
}

+ (void)postToEndPointFr:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData{
    [[self class] callPostToEndPoint:endpoint dispatchGroup:dispatchGroup postData:postData returnData:returnData rootUrl:[[self class] apiFrUrl]];
}

+ (void)callPostToEndPoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData rootUrl:(NSString *)rootUrl{
    dispatch_group_enter(dispatchGroup);
    NSString *url =[rootUrl stringByAppendingString:[[self class] safeEndpoint:endpoint]];
    NSString *token = [[self class] getToken];
    NSLog(@"---------------- %@", url);
    
    NSDictionary *postDataEncrypted = [[self class] getEncryptedData:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [urlRequest setHTTPBody:[[[self class] generateStringData:postDataEncrypted] dataUsingEncoding:NSUTF8StringEncoding]];
    [urlRequest HTTPBody];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
            [returnData setValue:[error localizedDescription] forKey:@"error_message"];
        }
        else
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *value  = [NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:kNilOptions
                                    error:&error];
            
            NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if(ONBOARDING_SAFE_MODE)
            {
                NSData *decryptedData = [Encryptor AESDecryptData:ret];
                value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
            }
            
            if([httpResponse statusCode] == 200) {
                [returnData setValue:[NSNumber numberWithBool:YES] forKey:@"is_success"];
                
                if(value) {
                    [returnData setValue:value forKey:@"data"];
                } else {
                    if(ONBOARDING_SAFE_MODE) {
                        NSString *decryptedString = [Encryptor AESDecrypt:ret];
                        [returnData setValue:[Encryptor AESDecrypt:ret] forKey:@"data"];
                    } else {
                        [returnData setValue:ret forKey:@"data"];
                    }
                }
                
            }
            else
            {
                [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
                if(value) {
                    [returnData setValue:value forKey:@"error_message"];
                } else {
                    if(ONBOARDING_SAFE_MODE) {
                        [returnData setValue:[Encryptor AESDecrypt:ret] forKey:@"error_message"];
                    } else {
                        [returnData setValue:[value objectForKey:@"message"] forKey:@"error_message"];
                    }
                }
                NSLog(@"ERROR : %@", [returnData objectForKey:@"error_message"]);
            }
        }
        dispatch_group_leave(dispatchGroup);
    }] resume];
}

+ (void)callPostToEndPointDisableEncryptRes:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup postData:(NSDictionary *)postData returnData:(NSMutableDictionary *)returnData rootUrl:(NSString *)rootUrl {
    dispatch_group_enter(dispatchGroup);
    NSString *url =[rootUrl stringByAppendingString:[[self class] safeEndpoint:endpoint]];
    NSString *token = [[self class] getToken];
    NSLog(@"---------------- %@", url);
    
    NSDictionary *postDataEncrypted = [[self class] getEncryptedData:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [urlRequest setHTTPBody:[[[self class] generateStringData:postDataEncrypted] dataUsingEncoding:NSUTF8StringEncoding]];
    [urlRequest HTTPBody];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
            [returnData setValue:[error localizedDescription] forKey:@"error_message"];
        }
        else
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *value  = [NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:kNilOptions
                                    error:&error];
            
            if([httpResponse statusCode] == 200) {
                [returnData setValue:[NSNumber numberWithBool:YES] forKey:@"is_success"];
                [returnData setValue:value forKey:@"data"];
            }
            else
            {
                [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
                if(value) {
                    [returnData setValue:value forKey:@"error_message"];
                } else {
                    [returnData setValue:[value objectForKey:@"message"] forKey:@"error_message"];
                }
                NSLog(@"ERROR : %@", [returnData objectForKey:@"error_message"]);
            }
        }
        dispatch_group_leave(dispatchGroup);
    }] resume];
}

+ (NSString *)getToken
{
    Encryptor *enc = [[Encryptor alloc] init];
    if([enc getUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY])
        return [enc getUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
    else
    {
        NSMutableString *token;
        NSString *url = [[self class] apiUrl];
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[url stringByAppendingString:[[self class] safeEndpoint:@"getToken"]]]];
        [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
        NSURLResponse *__autoreleasing *response = nil;
        NSError *__autoreleasing *error = nil;
        NSData *data = [[self class] sendSynchronousRequest:urlRequest returningResponse:response error:error];
        if(!error)
        {
            @try {
                if(ONBOARDING_SAFE_MODE)
                    token = [[NSMutableString alloc] initWithString:[Encryptor AESDecrypt:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]]];
                else
                    token = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                [token replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, token.length)];
                
                if ([token isKindOfClass:[NSString class]] && !([token length]<=0))
                    [enc setUserDefaultsObject:token forKey:ONBOARDING_TOKEN_KEY];
                else {
                    if(ONBOARDING_SAFE_MODE) {
                        NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        NSData *decryptedData = [Encryptor AESDecryptData:ret];
                        NSDictionary *value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:error];
                        NSLog(@"Error retrieving token : %@", ret);
                        if(value)
                            NSLog(@"Error retrieving token : %@", value);
                        else
                            NSLog(@"Error retrieving token : %@", decryptedData);
                    }
                    [enc removeUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
                }
            }
            @catch (NSException *e) {
                NSLog(@"Error retrieving token : %@", e.reason);
                [enc removeUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
            }
        }
        else
        {
            NSLog(@"Error retrieving token : %@", *error);
            [enc removeUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
        }
        return token;
    }
}

+ (NSData *)sendSynchronousRequest:(NSURLRequest *)request
                 returningResponse:(NSURLResponse **)response
                             error:(NSError **)error
{
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    
    
    NSError __block *err = NULL;
    NSData __block *data;
    NSURLResponse __block *resp;
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData* _data, NSURLResponse* _response, NSError* _error) {
                                         resp = _response;
                                         err = _error;
                                         data = _data;
                                         dispatch_group_leave(group);
                                         
                                     }] resume];
    
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    
    if (response)
    {
        *response = resp;
    }
    if (error)
    {
        *error = err;
    }
    
    return data;
}

+ (NSString *) generateStringData:(NSDictionary *)data
{
    NSMutableString *result = [[NSMutableString alloc] init];
    int i = 0;
    for (NSString *key in data.allKeys) {
        i == 0 ? [result appendFormat:@"%@=%@", key, [data objectForKey:key]] : [result appendFormat:@"&%@=%@", key, [data objectForKey:key]];
        i = i + 1;
    }
    return result;
}

+ (NSString *)stringParamFromData:(nullable NSDictionary *)data
{
    NSMutableString *result = [[NSMutableString alloc] init];
    int i = 0;
    for (NSString *key in data.allKeys) {
        i == 0 ? [result appendFormat:@"%@=%@", key, [data objectForKey:key]] : [result appendFormat:@"&%@=%@", key, [data objectForKey:key]];
        i = i + 1;
    }
    return result;
}

+ (NSString *)safeEndpoint:(NSString *)endpoint
{
    if (ONBOARDING_SAFE_MODE)
    {
        return [Encryptor HexEncrypt:endpoint];
    }
    
    return endpoint;
}

+ (NSMutableDictionary *)keychainItemForKey:(NSString *)key
{
    NSMutableDictionary *keychainItem = [[NSMutableDictionary alloc] init];
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAlways;
    keychainItem[(__bridge id)kSecAttrAccount] = key;
    keychainItem[(__bridge id)kSecAttrService] = NSStringFromClass([self class]);
    return keychainItem;
}

+ (OSStatus)setValue:(NSString *)value forKey:(NSString *)key {
    NSMutableDictionary *keychainItem = [[NSMutableDictionary alloc] init];
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAlways;
    keychainItem[(__bridge id)kSecAttrAccount] = key;
    keychainItem[(__bridge id)kSecAttrService] = NSStringFromClass([self class]);
    keychainItem[(__bridge id)kSecValueData] = [value dataUsingEncoding:NSUTF8StringEncoding];
    return SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
}

+ (OSStatus)setDict:(NSDictionary *)dict forKey:(NSString *)key
{
    NSMutableDictionary *keychainItem = [[NSMutableDictionary alloc] init];
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAlways;
    keychainItem[(__bridge id)kSecAttrAccount] = key;
    keychainItem[(__bridge id)kSecAttrService] = NSStringFromClass([self class]);
    keychainItem[(__bridge id)kSecValueData] = [NSKeyedArchiver archivedDataWithRootObject:dict];
    return SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
}

+ (OSStatus)setObject:(NSObject *)obj forKey:(NSString *)key
{
    NSMutableDictionary *keychainItem = [[NSMutableDictionary alloc] init];
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAlways;
    keychainItem[(__bridge id)kSecAttrAccount] = key;
    keychainItem[(__bridge id)kSecAttrService] = NSStringFromClass([self class]);
    keychainItem[(__bridge id)kSecValueData] = [NSKeyedArchiver archivedDataWithRootObject:obj];
    return SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
}

+ (NSString *)valueForKeychainKey:(NSString *)key
{
    OSStatus status;
    NSMutableDictionary *keychainItem = [[self class] keychainItemForKey:key];
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    CFDictionaryRef result = nil;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    if (status != noErr) {
        return nil;
    }
    NSDictionary *resultDict = (__bridge_transfer NSDictionary *)result;
    NSData *data = resultDict[(__bridge id)kSecValueData];
    if (!data) {
        return nil;
    }
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (NSDictionary *)dictForKeychainKey:(NSString *)key
{
    OSStatus status;
    NSMutableDictionary *keychainItem = [[self class] keychainItemForKey:key];
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    CFDictionaryRef result = nil;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    if (status != noErr) {
        return nil;
    }
    NSDictionary *resultDict = (__bridge_transfer NSDictionary *)result;
    NSData *data = resultDict[(__bridge id)kSecValueData];
    if (!data) {
        return nil;
    }
    return (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+ (NSObject *)objectForKeychainKey:(NSString *)key
{
    OSStatus status;
    NSMutableDictionary *keychainItem = [[self class] keychainItemForKey:key];
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    CFDictionaryRef result = nil;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    if (status != noErr) {
        return nil;
    }
    NSDictionary *resultDict = (__bridge_transfer NSDictionary *)result;
    NSData *data = resultDict[(__bridge id)kSecValueData];
    if (!data) {
        return nil;
    }
    return (NSObject *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+ (OSStatus)removeForKeychainKey:(NSString *)key
{
    NSMutableDictionary *keychainItem = [[self class] keychainItemForKey:key];
    return SecItemDelete((__bridge CFDictionaryRef)keychainItem);
}

+ (NSString *)apiUrl
{
    if(ONBOARDING_SAFE_MODE)
        return ONBOARDING_SAFE_API_URL;
    else
        return ONBOARDING_API_URL;
}
+ (NSString *)apiFrUrl{
    if(ONBOARDING_SAFE_MODE)
        return ONBOARDING_SAFE_API_FR_URL;
    else
        return ONBOARDING_API_FR_URL;
}
+ (NSDictionary *)getEncryptedData:(NSDictionary *)data {
    NSMutableDictionary *postDataEncrypted = [[NSMutableDictionary alloc] init];
    NSCharacterSet* set = [NSCharacterSet characterSetWithCharactersInString:@"!*'();@&=+$,?%#[]"];
    for (id key in data) {
        NSString *escapedKey = [[Encryptor HexEncrypt:key] stringByAddingPercentEncodingWithAllowedCharacters:[set invertedSet]];
        NSString *escapedData = [[Encryptor HexEncrypt:[data objectForKey:key]] stringByAddingPercentEncodingWithAllowedCharacters:[set invertedSet]];
        [postDataEncrypted setObject:escapedData forKey:escapedKey];
    }
    
    return postDataEncrypted;
}

+ (NSDate *) date:(NSDate *)date withHour:(NSInteger)hour minute:(NSInteger)minute
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components: NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date];
    [components setHour:hour];
    [components setMinute:minute];
    return [calendar dateFromComponents:components];
}

+ (BOOL)compareDate:(NSDate *)date withinFirst:(NSDate *)first andLast:(NSDate *)last
{
    return [date compare:first] == NSOrderedDescending && [date compare:last] == NSOrderedAscending;
}


@end

