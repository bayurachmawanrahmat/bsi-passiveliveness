//
//  AutoLogout.h
//  BSM-Mobile
//
//  Created by BSM on 2/21/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoLogout : UIApplication


@property(nonatomic, strong) NSDate *timeTracker;
@property(nonatomic) BOOL           enableAutoLogout;

- (void)resetAlarm;

@end
