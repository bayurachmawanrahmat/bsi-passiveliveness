//
//  NSObject+FormInputCategory.h
//  BSM-Mobile
//
//  Created by ARS on 26/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIDropdown.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (FormInputCategory)

@property (readwrite) DLAVAlertView *alertLoading;
@property (readwrite) UIAlertController *alertController;
@property (readwrite) UITextField *activeField;
@property (readwrite) UIScrollView *scrollView;
@property (readwrite) UIView *mainView;

- (void)registerForKeyboardNotifications;

- (void)setTextFieldData:(NSString *)key toTextField:(UITextField *)textField;
- (void)saveTextFieldData:(UITextField *)textField withKey:(NSString *)key;
- (void)setTextFieldData:(NSDictionary <NSString *, UITextField *> *)textFields;
- (void)saveTextFieldData:(NSDictionary <NSString *, UITextField *> *)textFields;
- (void)setDropDownData:(NSDictionary <NSString *, UIDropdown *> *)dropDowns;
- (void)saveDropDownData:(NSDictionary <NSString *, UIDropdown *> *)dropDowns;

- (void) inputHasError:(BOOL)hasError textField:(UITextField *)textField;
- (void)showErrorLabel:(NSString *)errorMsg withUiTextfield:(UITextField *)textField;

- (void)showLoading;
- (void)hideLoading;

- (void)showAlertErrorWithTitle:(NSString *)title andMessage:(NSString *)message sender:(UIViewController *)sender;

- (void)showAlertErrorWithCompletion:(NSString *)title andMessage:(NSString *)message sender:(UIViewController *)sender handler:(void (^ __nullable)(UIAlertAction *action))handler;

- (void) setWizardBarOnTop:(UIView *)parentView onYPos:(CGFloat)y withDone:(double)done fromTotal:(double)total;

-(void)saveImageToLocalStorage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath;
- (void) removeImageFromLocalStorage:(NSString *)filename;
- (BOOL)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename andSaveWithKey:(NSString *)saveKey;

- (void)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename andSaveWithKey:(NSString *)saveKey onCompletionPerformSegueWithIdentifier:(nullable NSString *)segueIdentifier fromController:(UIViewController *)viewController;

- (void)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename saveWithKey:(NSString *)saveKey andWithQuality:(double)quality onCompletionPerformSegueWithIdentifier:(nullable NSString *)segueIdentifier fromController:(UIViewController *)viewController;

- (void)saveImageToLogicalDocs:(UIImage *)image withFilename:(NSString *)filename andSaveWithKey:(NSString *)saveKey onCompletionPerformSegueWithIdentifier:(nullable NSString *)segueIdentifier fromController:(UIViewController *)viewController withCompletion:(void(^)(void))completion;

-(UIImage *)loadImageFromLocalStorageWithFileName:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath;

@end

NS_ASSUME_NONNULL_END
