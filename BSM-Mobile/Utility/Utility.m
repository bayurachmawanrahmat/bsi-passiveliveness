//
//  Utility.m
//  Aurora
//
//  Created by Adhitya Purwareksa on 12/12/13.
//  Copyright (c) 2013 Inmagine. All rights reserved.
//

#import "Utility.h"
#import <CommonCrypto/CommonHMAC.h>
#import <Foundation/Foundation.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import "DESChiper.h"
#import "DLAVAlertView.h"
#import <sys/utsname.h>
#import "AppProperties.h"
#import "NSUserdefaultsAes.h"
#import <CommonCrypto/CommonDigest.h>

@import Firebase;

@implementation Utility

+(void) setEventTracker:(NSString*)name withParam:(NSDictionary* _Nullable)dict{
    NSString *titleName = [name stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    
    if(!EVENT_TRACKER_MODE){
        titleName = [NSString stringWithFormat:@"DEV_%@",titleName];
    }
    [FIRAnalytics logEventWithName:titleName parameters:dict];

}

+(UIImage*) getImageJPG:(NSString*)imageName{
    
    UIImage *image;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00) {
        
        // RETINA DISPLAY
        NSString *jpegFile = [imageName stringByAppendingString:@"@2x.jpg"];
        image = [UIImage imageNamed:jpegFile];
    }
    else{
        
        NSString *jpegFile = [imageName stringByAppendingString:@".jpg"];
        image = [UIImage imageNamed:jpegFile];
    }
    
    return image;
}
+(NSDictionary *)convertActionToDict:(NSDictionary *)data atPosition:(int)position{
    
    NSDictionary *newData = @{@"content":[[data valueForKey:@"content"]objectAtIndex:position],@"favorite":[[data valueForKey:@"favorite"]objectAtIndex:position],@"social":[[data valueForKey:@"social"]objectAtIndex:position],@"template":[[data valueForKey:@"template"]objectAtIndex:position],@"title":[[data valueForKey:@"title"]objectAtIndex:position],@"url":[[data valueForKey:@"url"]objectAtIndex:position],@"url_parm":[[data valueForKey:@"url_parm"]objectAtIndex:position], @"field_name":[[data valueForKey:@"field_name"]objectAtIndex:position]};
    return newData;
}

+ (NSString *)getRequestType:(NSString *)data{
    NSArray *temp = [data componentsSeparatedByString:@","];
    if(temp.count>0){
        NSArray *requestType = [[temp objectAtIndex:0]componentsSeparatedByString:@"="];
        if(requestType.count>1)
            return [requestType objectAtIndex:1];
    }
    return @"";
}

+ (NSMutableDictionary *)translateParam:(NSString *)urlParm{
    NSArray *temp = [urlParm componentsSeparatedByString:@","];
    if(temp.count>0){
        NSMutableDictionary *dataParam = [[NSMutableDictionary alloc]init];
        for(int i =0; i<temp.count; i++){
            NSArray *separatedString = [[temp objectAtIndex:i]componentsSeparatedByString:@"="];
            if(separatedString.count > 1){
                [dataParam setObject:[separatedString objectAtIndex:1] forKey:[separatedString objectAtIndex:0]];
            }else{
                [dataParam setObject:[NSNumber numberWithBool:false] forKey:[temp objectAtIndex:i]];
            }
        }
        return dataParam;
    }
    return nil;
    
}

+ (NSArray *)changeToArray:(NSArray *)tempArray combinedSub:(BOOL)combinedSub{
    NSMutableArray *tempData = [[NSMutableArray alloc]init];
    for(id data in tempArray){
        if([data isKindOfClass:[NSString class]]){
            [tempData addObject:data];
        }else if([data isKindOfClass:[NSArray class]]){
            if(combinedSub){
                [tempData addObjectsFromArray:[self changeToArray:data]];
            }else{
                [tempData addObject:[data componentsJoinedByString:@"\n"]];
            }
        }
    }
    return tempData;

    
}
+ (NSArray *)changeToArray:(NSArray *)tempArray{
    return [Utility changeToArray:tempArray combinedSub:true];
}

+ (NSString *)datatosend:(NSString *)data{
    
    return data;
}

#pragma mark adding method by Angger
+(NSString*) localFormatCurrencyValue:(double)value showSymbol:(BOOL)setSymbol
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"id-ID"]];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    if (setSymbol){
        [numberFormatter setCurrencySymbol:@"Rp "];
    } else {
        [numberFormatter setCurrencySymbol:@""];
    }
    [numberFormatter setGroupingSeparator:@"."];
    [numberFormatter setMaximumFractionDigits:0];
    
    NSNumber *c = [NSNumber numberWithDouble:value];
    NSString *frmtCurr = [numberFormatter stringFromNumber:c];
    return [frmtCurr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

#pragma mark adding method by Alikhsan
+(NSString*) formatCurrencyValue:(double)value{
    return [self formatCurrencyValue:value withSeparator:@","];
}

+(NSString*) formatCurrencyValue:(double)value withSeparator:(NSString*)separator{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setGroupingSeparator:separator];
    NSNumber *c = [NSNumber numberWithDouble:value];
    NSString *frmtCurr = [[numberFormatter stringFromNumber:c]stringByReplacingOccurrencesOfString:@"$" withString:@""];
    return [frmtCurr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+(NSString *) currencyVal: (double)value{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setGroupingSeparator:@","];
    NSLocale *mLocal = [[NSLocale alloc]initWithLocaleIdentifier:@"id_ID"];
    [numberFormatter setLocale:mLocal];
    NSNumber *c = [NSNumber numberWithDouble:value];
    return [numberFormatter stringFromNumber:c];
}

+(NSString *) messageGenerated:(NSString *) rc
                       defValu: (NSString *) defaultValue{
    NSString *msg = defaultValue;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        if ([rc isEqualToString:@"03"]) {
            msg = @"Maaf, transaksi anda gagal karena kesalahan pada jumlah pembayaran.";
        } else   if ([rc isEqualToString:@"05"]) {
//            msg = @"Transaksi gagal, kesalahan sistem";
            msg = @"Transaksi gagal, silahkan ulangi kembali";
        } else   if ([rc isEqualToString:@"08"]) {
            msg = @"Transaksi dalam proses. Periksa transaksi anda melalui menu 'Status Tunai'";
        } else   if ([rc isEqualToString:@"09"]) {
            msg = @"Transaksi gagal, pembatalan dalam proses.";
        } else   if ([rc isEqualToString:@"13"]) {
            msg = @"MAAF, UNTUK SEMENTARA TIDAK DAPAT MENJALANKAN TRANSAKSI YANG DIMINTA. SILAKAN HUBUNGI LAYANAN CALL CENTER.";
        } else   if ([rc isEqualToString:@"14"]) {
            msg = @"Maaf, nomor anda tidak ditemukan.";
        } else   if ([rc isEqualToString:@"15"]) {
            msg = @"Rekening tidak terdaftar";
        } else   if ([rc isEqualToString:@"31"]) {
            msg = @"Silakan hubungi cabang anda untuk menggunakan layanan ini.";
        } else   if ([rc isEqualToString:@"33"]) {
            msg = @"Maaf, kartu ATM anda yang terdaftar untuk Syariah Indonesia MBG sudah tidak aktif.";
        } else   if ([rc isEqualToString:@"38"]) {
            msg = @"PIN anda diblokir, hubungi Syariah Indonesia Customer Service atau Call Center";
        } else   if ([rc isEqualToString:@"40"]) {
            msg = @"Maaf, layanan ini untuk sementara tidak bisa digunakan.";
        } else   if ([rc isEqualToString:@"51"]) {
            msg = @"Maaf, jumlah saldo anda tidak mencukupi.";
        } else   if ([rc isEqualToString:@"52"]) {
            msg = @"Rekening tujuan tidak dikenal.";
        } else   if ([rc isEqualToString:@"53"]) {
            msg = @"Rekening tujuan tidak dikenal.";
        } else   if ([rc isEqualToString:@"54"]) {
            msg = @"Maaf, kartu ATM yang terdaftar untuk Syariah Indonesia MBG sudah kadaluwarsa.";
        } else   if ([rc isEqualToString:@"55"]) {
            msg = @"PIN anda salah.";
        } else   if ([rc isEqualToString:@"56"]) {
            msg = @"Maaf, kartu ATM anda belum terdaftar untuk layanan Syariah Indonesia MBG.";
        } else   if ([rc isEqualToString:@"57"]) {
            msg = @"Layanan ini tidak tersedia untuk jenis kartu anda.";
        } else   if ([rc isEqualToString:@"61"]) {
            msg = @"Maaf, anda telah melebihi batas maksimum jumlah transaksi untuk hari ini.";
        } else   if ([rc isEqualToString:@"62"]) {
            msg = @"Maaf, kartu anda diblokir";
        } else   if ([rc isEqualToString:@"65"]) {
            msg = @"Maaf, anda telah melebihi batas maksimum frekuensi transaksi untuk hari ini.";
        } else   if ([rc isEqualToString:@"68"]) {
            msg = @"Transaksi Anda kehabisan waktu. Silahkan cek saldo/mutasi sebelum melakukan transaksi lagi";
        } else   if ([rc isEqualToString:@"75"]) {
            msg = @"Maaf, PIN anda diblokir. Silakan hubungi Syariah Indonesia Customer Service atau Call Center.";
        } else   if ([rc isEqualToString:@"76"]) {
            msg = @"Rekening tujuan tidak dikenal.";
        } else   if ([rc isEqualToString:@"77"]) {
            msg = @"Transaksi gagal, nomor rekening salah";
        } else   if ([rc isEqualToString:@"82"]) {
            msg = @"Tagihan bulan ini belum tersedia";
        } else   if ([rc isEqualToString:@"88"]) {
            msg = @"Tagihan anda telah terbayar";
        } else   if ([rc isEqualToString:@"89"]) {
            msg = @"Maaf, jaringan terputus, untuk sementara transaksi anda tidak dapat dilayani. ";
        } else   if ([rc isEqualToString:@"91"]) {
            msg = @"Maaf, untuk sementara transaksi anda tidak dapat dilayani. ";
        } else   if ([rc isEqualToString:@"92"]) {
            msg = @"Maaf, institusi tujuan sedang tidak dapat melayani transaksi. Cobalah beberapa saat lagi.";
        } else   if ([rc isEqualToString:@"96"]) {
            msg = @"Maaf, untuk sementara transaksi anda tidak dapat dilayani. ";
        } else   if ([rc isEqualToString:@"99"]) {
            msg = @"Jumlah saldo anda tidak mencukupi.";
        }
    } else {
        if ([rc isEqualToString:@"03"]) {
            msg = @"Sorry, we can not process your transaction at this moment. Please try again later.";
        } else   if ([rc isEqualToString:@"05"]) {
            msg = @"Transaction failed, please try again";
        } else   if ([rc isEqualToString:@"08"]) {
            msg = @"Transaction in progress, check your transaction using 'Tunai Status' menu under Infomation menu";
        } else   if ([rc isEqualToString:@"09"]) {
            msg = @"Transaction fail,  reversal in progress.";
        } else   if ([rc isEqualToString:@"13"]) {
            msg = @"SORRY, THIS PAYMENT IS TEMPORARILY UNAVAILABLE. PLEASE CONTACT CALL CENTER.";
        } else   if ([rc isEqualToString:@"14"]) {
            msg = @"Sorry, your identifier not found.";
        } else   if ([rc isEqualToString:@"15"]) {
            msg = @"The account number is not registered.";
        } else   if ([rc isEqualToString:@"31"]) {
            msg = @"Please contact your Syariah Indonesia branch office for this service.";
        } else   if ([rc isEqualToString:@"33"]) {
            msg = @"Sorry, your registered ATM card for Syariah Indonesia MBG is no longer active.";
        } else   if ([rc isEqualToString:@"38"]) {
            msg = @"Your PIN has been blocked. Please contact Syariah Indonesia Customer Service or Call Center";
        } else   if ([rc isEqualToString:@"40"]) {
            msg = @"Sorry, this service is not available at this moment.  Please try again later.";
        } else   if ([rc isEqualToString:@"51"]) {
            msg = @"Your available balance is not sufficient for this transaction.";
        } else   if ([rc isEqualToString:@"52"]) {
            msg = @"Beneficiary account number is not recognized.";
        } else   if ([rc isEqualToString:@"53"]) {
            msg = @"Beneficiary account number is not recognized.";
        } else   if ([rc isEqualToString:@"54"]) {
            msg = @"Sorry, your registered ATM card for Syariah Indonesia MBG has expired.";
        } else   if ([rc isEqualToString:@"55"]) {
            msg = @"Wrong PIN.";
        } else   if ([rc isEqualToString:@"56"]) {
            msg = @"Your ATM card has not been registered for Syariah Indonesia MBG service.";
        } else   if ([rc isEqualToString:@"57"]) {
            msg = @"The service is not available for your card.";
        } else   if ([rc isEqualToString:@"61"]) {
            msg = @"You have achieved maximum amount of transaction within a day.";
        } else   if ([rc isEqualToString:@"62"]) {
            msg = @"Your card is blocked.";
        } else   if ([rc isEqualToString:@"65"]) {
            msg = @"You have achieved maximum number of transactions within a day.";
        } else   if ([rc isEqualToString:@"68"]) {
            msg = @"Your transaction timed out. Please check your balance/history prior to performing other transaction";
        } else   if ([rc isEqualToString:@"75"]) {
            msg = @"Your PIN has been blocked. Please contact Syariah Indonesia Customer Service or Call Center.";
        } else   if ([rc isEqualToString:@"76"]) {
            msg = @"Beneficiary account number is not recognized. Please try again.";
        } else   if ([rc isEqualToString:@"77"]) {
            msg = @"Transaction fail, invalid account";
        } else   if ([rc isEqualToString:@"82"]) {
            msg = @"Your bill is not yet available";
        } else   if ([rc isEqualToString:@"88"]) {
            msg = @"Your bill already paid";
        } else   if ([rc isEqualToString:@"89"]) {
            msg = @"Network problem, we can not process your transaction at this moment. Please try again later.";
        } else   if ([rc isEqualToString:@"91"]) {
            msg = @"Sorry, we can not process your transaction at this moment. Please try again later.";
        } else   if ([rc isEqualToString:@"92"]) {
            msg = @"Sorry, beneficiary institution not available right now. Please try again later.";
        } else   if ([rc isEqualToString:@"96"]) {
            msg = @"Sorry, we can not process your transaction at this moment. Please try again later.";
        } else   if ([rc isEqualToString:@"99"]) {
            msg = @"Your available balance is not sufficient for this transaction.";
        }
    }
    
    return msg;
}

+(NSString *)dateToday : (int) type{
    NSString *strDateToday;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    switch (type) {
        case 1:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
        case 2:
            [dateFormatter setDateFormat:@"dd MMM yyyy"];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
        case 3:
            [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
        case 4:
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
        case 5:
            [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
        case 6:
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
			
        case 7:
            [dateFormatter setDateFormat:@"zzz"];
            [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
        default:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            strDateToday = [dateFormatter stringFromDate:[NSDate date]];
            break;
    }
    
    return strDateToday;
}
+(NSDate *)dateStringToDate : (NSString *) dateString
                       mType:(int)type{
    NSDate *mDate;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    switch (type) {
        case 1:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            mDate = [dateFormatter dateFromString:dateString];
            break;
        case 2:
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            mDate = [dateFormatter dateFromString:dateString];
            break;
        case 3:
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            mDate = [dateFormatter dateFromString:dateString];
            break;
        default:
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            mDate = [dateFormatter dateFromString:dateString];
            break;
            
    }
    return mDate;
            
}

+(NSString *) calculateHMAC:(NSString *) mData
                        key: (NSString *) mKey{
    NSData *dataKey = [mKey dataUsingEncoding:NSUTF8StringEncoding];
    NSData *dataParam = [mData dataUsingEncoding:NSUTF8StringEncoding];
//    const char *HMAC_SHA512 = [@"HmacSHA512" cStringUsingEncoding:NSASCIIStringEncoding];
    NSMutableData *hash = [NSMutableData dataWithLength:CC_SHA512_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA512, dataKey.bytes, dataKey.length, dataParam.bytes, dataParam.length, hash.mutableBytes);
   // NSString *base64Hash = [hash base64EncodedStringWithOptions:0];
    
//    const char *cKey = [mKey cStringUsingEncoding:NSASCIIStringEncoding];
//    const char *cData = [mData cStringUsingEncoding:NSASCIIStringEncoding];
//
//    NSLog(@"key : %s" ,cKey);
//    NSLog(@"data : %s", cData);
//    unsigned char cHMAC[CC_SHA512_DIGEST_LENGTH];
//    CCHmac(kCCHmacAlgSHA512, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
//    NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    return [self stringDataToHex:(NSData*)hash];
}

+ (NSString *) stringDataToHex:(NSData *)hash{
    NSUInteger capacity = hash.length * 2;
    NSMutableString *sbuf = [NSMutableString stringWithCapacity:capacity];
    const unsigned char *buf = hash.bytes;
    NSInteger i;
    for (i=0; i<hash.length; ++i) {
        [sbuf appendFormat:@"%02lX", (unsigned long)buf[i]];
    }
    NSLog(@"HASH 512 : %@",[sbuf lowercaseString]);
    return [sbuf lowercaseString];
    
//    const uint8_t* input = (const uint8_t*)[hash bytes];
//    NSInteger length = [hash length];
//
//    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
//
//
//
//    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
//    uint8_t* output = (uint8_t*)data.mutableBytes;
//
//    NSInteger i;
//    for (i=0; i < length; i += 3) {
//        NSInteger value = 0;
//        NSInteger j;
//        for (j = i; j < (i + 3); j++) {
//            value <<= 8;
//
//            if (j < length) {  value |= (0xFF & input[j]);  }  }  NSInteger theIndex = (i / 3) * 4;  output[theIndex + 0] = table[(value >> 18) & 0x3F];
//        output[theIndex + 1] = table[(value >> 12) & 0x3F];
//        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
//        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
//    }
//    NSLog(@"HASH 512 : %@",[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]);
//    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    
}
+(NSString *)stringTrim : (NSString *) mStr {
    return [mStr stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+(NSString *) dateConvert : (NSString *)strDate
                     mType:(int)nType{
    NSString *strDateResult;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    NSDate *mDate =  [NSDate date];
    switch (nType) {
        case 1:
            [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm:ss"];
            mDate = [dateFormatter dateFromString:strDate];
            break;
            
        default:
            strDateResult = @"";
            break;
    }
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    strDateResult = [dateFormatter stringFromDate:mDate];
    return strDateResult;
}

+(NSString *)convertJsonString : (NSObject *) mSource{
    NSError *error;
    NSString *jsonString = @"";
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:mSource options:kNilOptions error:&error];
    if (error == nil) {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
       
    }
    return jsonString;
}

+(NSAttributedString *)htmlAtributString : (NSString *) strTextHtml{
    return [[NSAttributedString alloc] initWithData:[strTextHtml dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                documentAttributes:nil error:nil];
}

+(float)dynamicLabelHeight:(UILabel *)mLabel{
    [mLabel setNumberOfLines:0];
    [mLabel setLineBreakMode:NSLineBreakByWordWrapping];
    CGSize maxLabelSize = CGSizeMake(mLabel.frame.size.width, 9999);
    CGSize dynamicLblSize = [[mLabel text] sizeWithFont:[mLabel font] constrainedToSize:maxLabelSize lineBreakMode:[mLabel lineBreakMode]];
    return dynamicLblSize.height;
    
}

+ (CGFloat)getLabelHeight:(NSString*)labelText width:(CGFloat) width font:(UIFont*) font
{
//    UIFont *font = [UIFont fontWithName:@"Helvetica" size:fontSize];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, CGFLOAT_MAX)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = font;
    label.text = labelText;
    [label sizeToFit];
    return label.frame.size.height;
}

+(float)heightLabelDynamic :(UILabel *) mLabel
                   sizeFont:(float) mSizeFont{
    [mLabel setNumberOfLines:0];
    [mLabel setLineBreakMode:NSLineBreakByWordWrapping];
    CGSize maxLabelSize = CGSizeMake(SCREEN_WIDTH, 2000.0f);
    CGSize dynamicLblSize = [[mLabel text] sizeWithFont:[UIFont systemFontOfSize:mSizeFont] constrainedToSize:maxLabelSize lineBreakMode:[mLabel lineBreakMode]];
    return dynamicLblSize.height;
}

+(float)heightLabelDynamic :(UILabel *) mLabel
                  sizeWidth:(float) mSizeWidth
                   sizeFont:(float) mSizeFont{
    [mLabel setNumberOfLines:0];
    [mLabel setLineBreakMode:NSLineBreakByWordWrapping];
    CGSize maxLabelSize = CGSizeMake(mSizeWidth, 2000.0f);
    CGSize dynamicLblSize = [[mLabel text] sizeWithFont:[UIFont systemFontOfSize:mSizeFont] constrainedToSize:maxLabelSize lineBreakMode:[mLabel lineBreakMode]];
    return dynamicLblSize.height;
}


+ (CGSize)optimalSizeForLabel:(UILabel *)label inMaxSize:(CGSize)maxSize {
    CGSize size = CGSizeMake(0.0, 0.0);
    
    if (!label.text) {
        return size;
    }
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [label.text sizeWithFont:label.font
                      constrainedToSize:maxSize
                          lineBreakMode:NSLineBreakByWordWrapping];
#pragma clang diagnostic pop
    } else {
        NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
        context.minimumScaleFactor = 1.0;
        size = [label.text boundingRectWithSize:maxSize
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : label.font }
                                        context:context].size;
    }
    
    return size;
}

+(bool) vcNotifConnection:(NSString *) mNameVC{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    bool callbackVc = false ;
    if([[userDefault valueForKey:@"state_indicator"] isEqualToString:@"0"] || [[userDefault valueForKey:@"state_indicator"] isEqualToString:@"2"]){
        NSArray *vcAccepted = @[@"ChangeLngVC", @"SMVC", @"ASVC"];
        for(int i = 0; i < [vcAccepted count];i++){
            if([mNameVC isEqualToString:[vcAccepted objectAtIndex:i]]){
                callbackVc = false;
                return callbackVc;
            }else{
                callbackVc = true;
            }
        }
    }else{
        callbackVc = false;
    }
   
    return callbackVc;
    
}
+(NSMutableURLRequest *)BSMHeader : (NSString *) mUrl{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:mUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];
    NSString *xSignature = [NSString stringWithFormat:@"%@|%@",clientId,[self dateToday:1]];
    [request setValue:[self dateToday:1] forHTTPHeaderField:@"X-TIMESTAMP"];
    [request setValue:clientId forHTTPHeaderField:@"X-Key"];
    [request setValue:@"close" forHTTPHeaderField:@"Connection"];
    [request setValue:[self calculateHMAC:xSignature key:clientSecret] forHTTPHeaderField:@"X-SIGNATURE"];
    return request;
}
+ (BOOL)validateEmailWithString:(NSString*)checkString{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._-]+[A-Z0-9a-z]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    //NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (NSString *)deviceIPAddress{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    NSString *networkInterface = @"en0";
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:networkInterface]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
}

+ (UIColor *)randomColor
{
    NSArray *colors;
    if (!colors) {
        colors = @[
                    UIColorFromRGB(0xff7f7f),
                    UIColorFromRGB(0xff7fbf),
                    UIColorFromRGB(0xff7fff),
                    UIColorFromRGB(0xbf7fff),
                    UIColorFromRGB(0x7f7fff),
                    UIColorFromRGB(0x7fbfff),
                    UIColorFromRGB(0x7fffff),
                    UIColorFromRGB(0x7fffbf),
                    UIColorFromRGB(0x7fff7f),
                    UIColorFromRGB(0xbfff7f),
                    UIColorFromRGB(0xffff7f),
                    UIColorFromRGB(0xffbf7f)
                    ];
    }
    
    NSInteger count = colors.count;
    NSInteger r = arc4random() % count;
    return colors[r];
}

+ (void)animeBounchCb :(UIButton *) sender{
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform"];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = 0.125;
    anim.repeatCount = 1;
    anim.autoreverses = YES;
    anim.removedOnCompletion = YES;
    anim.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)];
    [sender.layer addAnimation:anim forKey:nil];
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

+ (NSString *) postRequestUrl : (NSString *)urlParam encrypted : (bool)mEncrypted{

    __block NSString *response;
    NSMutableDictionary *dataParam = [self translateParam:urlParam];
    NSLog(@"%@", dataParam);
    NSString *plainRequest = [self genParamUrl:dataParam];
    NSString *keyDesEncrypted;
    __block NSString *keyDes;
    DESChiper *desChiper = [[DESChiper alloc]init];
    
    bool needDecryptBase64 = true;
    
    if (mEncrypted) {
        needDecryptBase64 = false;
        keyDes = [desChiper generateDESKey];
        NSString *publicKeyServer = [NSUserdefaultsAes getValueForKey:@"publickey"];
        BDRSACryptorKeyPair *RSAKeyPair = [[BDRSACryptorKeyPair alloc] initWithPublicKey:publicKeyServer privateKey:NULL];
        
        BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
        BDError *error = [[BDError alloc] init];
        keyDesEncrypted = [RSACryptor encrypt:keyDes key:RSAKeyPair.publicKey error:error];
        NSLog(@"%@", keyDesEncrypted);
        plainRequest = [desChiper doDES:plainRequest key:keyDes];
    }else{
        NSData *dataBase64 = [plainRequest dataUsingEncoding:NSUTF8StringEncoding];
        plainRequest = [dataBase64 base64EncodedString];
    }
    if(keyDes)
        plainRequest = [NSString stringWithFormat:@"%@#%@", plainRequest, keyDesEncrypted];
    DLog(@"PLAIN REQUEST: %@", plainRequest);
    plainRequest = [self urlEncodeUsingEncoding:NSASCIIStringEncoding data:plainRequest];
    NSString *body = @"";
    NSMutableDictionary *requestData = [[NSMutableDictionary alloc]initWithDictionary:@{@"req_data":plainRequest}];
    for (NSString *key in [requestData allKeys]){
        body = [NSString stringWithFormat:@"%@%@=%@&",body,key, [requestData valueForKey:key]];
    }
    body = [body substringToIndex:[body length]-1];
    
    if([NSUserdefaultsAes getValueForKey:@"session_id"]){
        body = [NSString stringWithFormat:@"%@&session_id=%@",body, [NSUserdefaultsAes getValueForKey:@"session_id"]];
    }
    
    DLog(@"data to sent %@", body);
    NSString *apiUrl = [NSString stringWithFormat:@"%@index.htm",API_URL];
    
    NSLog(@"USING URL: %@", apiUrl);
    
    NSMutableURLRequest *request = [self BSMHeader:apiUrl];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    
//   __block NSString *tempString;
    dispatch_semaphore_t semaphore;
    
    semaphore = dispatch_semaphore_create(0);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * urlResponse, NSError * error) {
        if (!error) {
            NSString* tempString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];

            if(tempString != nil){
                NSArray * listData = [tempString componentsSeparatedByString:@"#"];
                if(listData.count > 1){
        //            NSString *privateKey = [userDefault valueForKey:@"privateKey"];
                    NSString *privateKey = [NSUserdefaultsAes getValueForKey:@"privatekey"];
                    BDRSACryptorKeyPair *RSAKeyPair = [[BDRSACryptorKeyPair alloc] initWithPublicKey:NULL privateKey:privateKey];
                    
                    BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
                    BDError *error = [[BDError alloc] init];
                    keyDes = [RSACryptor decrypt:listData[1] key:RSAKeyPair.privateKey error:error];
                }
                if(keyDes){
                    NSData *tempData = [[NSData alloc]initWithBase64EncodedString:listData[0] options:NSDataBase64DecodingIgnoreUnknownCharacters];
                    
                    response = [desChiper doDecryptDES:tempData key:keyDes];
                    
                    if(needDecryptBase64){
                        response = [[NSString alloc]initWithData:tempData encoding:NSASCIIStringEncoding];
                    }
                }
            }

        }
        else {
            NSLog(@"Error: %@", [error localizedDescription]);
        }
        dispatch_semaphore_signal(semaphore);
    }];
    [task resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);

    return response;
}

+ (NSString *) genParamUrl : (NSMutableDictionary *) dataParam{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    [dataParam setObject:[UIDevice currentDevice].model forKey:@"device_type"];
    [dataParam setObject:@"iphone" forKey:@"device"];
    [dataParam setObject:[[[UIDevice currentDevice]identifierForVendor]UUIDString] forKey:@"imei"];
    [dataParam setObject:[self deviceIPAddress] forKey:@"ip_address"];
    [dataParam setObject:@VERSION_NAME forKey:@"version_name"];
    [dataParam setObject:@VERSION_VALUE forKey:@"version_value"];
//    [dataParam setObject:[self dateToday:3] forKey:@"date_local"];
    if([dataParam objectForKey:@"date_local"]){
        [dataParam setObject:[self dateToday:3] forKey:@"date_local"];
    }
//    if([dataParam objectForKey:@"customer_id"] && [userDefault objectForKey:@"customer_id"]){
    if([dataParam objectForKey:@"customer_id"] && [NSUserdefaultsAes getValueForKey:@"customer_id"]){
        [dataParam setObject:[NSUserdefaultsAes getValueForKey:@"customer_id"] forKey:@"customer_id"];
    }
    if([dataParam objectForKey:@"language"]){
        NSArray *listLanguage = [userDefault objectForKey:@"AppleLanguages"];
        [dataParam setObject:[listLanguage objectAtIndex:0] forKey:@"language"];
    }if([dataParam objectForKey:@"zpk"]){
//        [dataParam setObject:[userDefault objectForKey:@"zpk"] forKey:@"zpk"];
        [dataParam setObject:[NSUserdefaultsAes getValueForKey:@"zpk"] forKey:@"zpk"];
    }
    NSLog(@"DATA TO SENT: %@", dataParam);
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dataParam options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    return jsonString;
    
}

+(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding data:(NSString *)oriString{
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (CFStringRef)oriString,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8 ));
}
#pragma mark-keychain
+ (NSMutableDictionary *)keychainItemForKey:(NSString *)key
{
    NSMutableDictionary *keychainItem = [[NSMutableDictionary alloc] init];
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAlways;
    keychainItem[(__bridge id)kSecAttrAccount] = key;
    keychainItem[(__bridge id)kSecAttrService] = NSStringFromClass([self class]);
    return keychainItem;
}
//setter
+ (OSStatus)setValue:(NSString *)value forKey:(NSString *)key {
    NSMutableDictionary *keychainItem = [[NSMutableDictionary alloc] init];
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAlways;
    keychainItem[(__bridge id)kSecAttrAccount] = key;
    keychainItem[(__bridge id)kSecAttrService] = NSStringFromClass([self class]);
    keychainItem[(__bridge id)kSecValueData] = [value dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"chain: %@",keychainItem);
    return SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
}

+ (OSStatus)setDict:(NSDictionary *)dict forKey:(NSString *)key
{
    NSMutableDictionary *keychainItem = [[NSMutableDictionary alloc] init];
    keychainItem[(__bridge id)kSecClass] = (__bridge id)kSecClassGenericPassword;
    keychainItem[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleAlways;
    keychainItem[(__bridge id)kSecAttrAccount] = key;
    keychainItem[(__bridge id)kSecAttrService] = NSStringFromClass([self class]);
    keychainItem[(__bridge id)kSecValueData] = [NSKeyedArchiver archivedDataWithRootObject:dict];
//     NSLog(@"chain2: %@",keychainItem);
//    NSLog(@"coba: %@", dict);
//    NSLog(@"key: %@", key);
    return SecItemAdd((__bridge CFDictionaryRef)keychainItem, NULL);
}

//getter
+ (NSString *)valueForKeychainKey:(NSString *)key
{
    OSStatus status;
    NSMutableDictionary *keychainItem = [[self class] keychainItemForKey:key];
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    CFDictionaryRef result = nil;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    if (status != noErr) {
        return nil;
    }
    NSDictionary *resultDict = (__bridge_transfer NSDictionary *)result;
    NSData *data = resultDict[(__bridge id)kSecValueData];
     NSLog(@"valueForKeychainKey: %@", data);
    NSLog(@"Key: %@", key);
    if (!data) {
        return nil;
    }
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}
+ (NSDictionary *)dictForKeychainKey:(NSString *)key
{
    OSStatus status;
    NSMutableDictionary *keychainItem = [[self class] keychainItemForKey:key];
    keychainItem[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;
    keychainItem[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;
    CFDictionaryRef result = nil;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)keychainItem, (CFTypeRef *)&result);
    if (status != noErr) {
        return nil;
    }
    NSDictionary *resultDict = (__bridge_transfer NSDictionary *)result;
    NSData *data = resultDict[(__bridge id)kSecValueData];
    NSLog(@"dictForKeychainKey: %@", data);
    NSLog(@"Key2: %@", key);
    if (!data) {
        return nil;
    }
    return (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    
}
+ (OSStatus)removeForKeychainKey:(NSString *)key
{
    NSMutableDictionary *keychainItem = [[self class] keychainItemForKey:key];
    return SecItemDelete((__bridge CFDictionaryRef)keychainItem);
}

+ (NSAttributedString *) setLineSpasing : (NSString *) strText
                          mSpaseSize : (int) lineSpase{
    NSMutableAttributedString* attrString = [[NSMutableAttributedString  alloc] initWithString:strText];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:lineSpase];
    [style setAlignment:NSTextAlignmentRight];
    [attrString addAttribute:NSParagraphStyleAttributeName
                       value:style
                       range:NSMakeRange(0, [strText length])];
    return attrString;
}

+(NSString *) findArrConstain : (NSArray *) data
                       strFind:(NSString *) mStr{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",mStr];
    NSArray *arFilter = [data filteredArrayUsingPredicate:predicate];
    if (arFilter.count > 0) {
        NSArray *arFindFilter = [[arFilter objectAtIndex:(arFilter.count-1)]componentsSeparatedByString:@": "];
        return [arFindFilter objectAtIndex:1];
    }else{
        return @"";
    }
}

+(NSString *) findArrConstain : (NSArray *) data
                       strFind:(NSString *) mStr
                    strMenuId : (NSString *) menuId{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",mStr];
    NSArray *arFilter = [data filteredArrayUsingPredicate:predicate];
    if (arFilter.count > 0) {
        NSArray *arFindFilter = [[arFilter objectAtIndex:(0)]componentsSeparatedByString:@": "];
        return [arFindFilter objectAtIndex:1];
    }else{
        return @"";
    }
}

+(BOOL) arContain : (NSArray *) data
          mString : (NSString *) str{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",str];
    NSArray *arFilter = [data filteredArrayUsingPredicate:predicate];
    if (arFilter.count > 0) {
        return true;
    }else{
        return false;
    }
}

+(CGSize) heightWrapWithText : (NSString *) strText
                    fontName : (UIFont *) mFont
                expectedSize : (CGSize) mExpedtedSize{
    // Let's make an NSAttributedString first
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:strText];
    //Add LineBreakMode
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    [attributedString setAttributes:@{NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, attributedString.length)];
    // Add Font
    [attributedString setAttributes:@{NSFontAttributeName:mFont} range:NSMakeRange(0, attributedString.length)];

    //Now let's make the Bounding Rect
    CGSize expectedSize = [attributedString boundingRectWithSize:mExpedtedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
    return expectedSize;
}

+ (UIImage *)image1x1WithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.f, 0.f, 1.f, 1.f);

    UIGraphicsBeginImageContext(rect.size);

    CGContextRef context = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
}

+(CGRect) converRectOfInterest : (CGRect)vwScanner{
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = (vwScanner.size.height - transparantWidth) * 0.5;
    return CGRectMake(transparantX,transparantY,transparantWidth, transparantWidth);
}

+(UILabel *) lblTitleQR{
    UILabel *lblTitle = [[UILabel alloc]init];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
        lblTitle.text = @"Letakan kode QR pada kotak untuk dipindai.";
    }else{
        lblTitle.text = @"Put the QR code on the scanning box below.";
    }
    
    if (IPHONE_5) {
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    }else{
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    }
    [lblTitle setTextColor:[UIColor whiteColor]];
    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
    lblTitle.numberOfLines = 0;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [lblTitle sizeToFit];
    return lblTitle;
}

+(UILabel *) lblTitleQRIS{
    UILabel *lblTitle = [[UILabel alloc]init];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
        lblTitle.text = @"Letakan kode QRIS pada kotak untuk dipindai.";
    }else{
        lblTitle.text = @"Put the QRIS code on the scanning box below.";
    }
    
    if (IPHONE_5) {
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    }else{
        [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    }
    [lblTitle setTextColor:[UIColor whiteColor]];
    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
    lblTitle.numberOfLines = 0;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [lblTitle sizeToFit];
    return lblTitle;
}

+(NSString *) stringTrimmer : (NSString *) strText{
    return [strText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+(void)showMessage : (NSString *) nMessage
     mVwController : (UIViewController *) nVc
               mTag: (int)nTag{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessage delegate:nVc cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    } else {
        // Fallback on earlier versions
    }
    
    alert.tag = nTag;
    [alert show];
}

+(void)showMessage: (NSString*) message
         instance : (id)instanceID{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [instanceID presentViewController:alert animated:YES completion:nil];
}

+(void)showMessage: (NSString*) messageID
         enMessage: (NSString*) messageEN
         instance : (id)instanceID{
    
    NSString *message = @"";
    if([Utility isLanguageID]){
        message = messageID;
    }else{
        message = messageEN;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [instanceID presentViewController:alert animated:YES completion:nil];
}

+(void)stopTimer{
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

+(DLAVAlertView *) showProgressDialog : (NSString *) strTitle
                           strMessage : (NSString *) mStrMessage{
    
    if (strTitle == nil) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if ([lang isEqualToString:@"id"]) {
            strTitle = @"Mohon Menunggu";
        }else{
            strTitle = @"Please Wait";
        }
    }
    
    DLAVAlertView *loadingAlert = [[DLAVAlertView alloc]initWithTitle:strTitle message:mStrMessage delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    
    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
    [viewLoading setBackgroundColor:[UIColor clearColor]];
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loadingView setColor:[UIColor blackColor]];
    [loadingView startAnimating];
    [viewLoading addSubview:loadingView];
    
    [loadingAlert  setContentView:viewLoading];
    return loadingAlert;
}

+(NSString *) deviceName {
     struct utsname systemInfo;

       uname(&systemInfo);

       NSString* code = [NSString stringWithCString:systemInfo.machine
                                           encoding:NSUTF8StringEncoding];

       static NSDictionary* deviceNamesByCode = nil;

       if (!deviceNamesByCode) {

           deviceNamesByCode = @{@"i386"      : @"Simulator",
                                 @"x86_64"    : @"Simulator",
                                 @"iPod1,1"   : @"iPod Touch",        // (Original)
                                 @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                                 @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                                 @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                                 @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                                 @"iPhone1,1" : @"iPhone",            // (Original)
                                 @"iPhone1,2" : @"iPhone",            // (3G)
                                 @"iPhone2,1" : @"iPhone",            // (3GS)
                                 @"iPad1,1"   : @"iPad",              // (Original)
                                 @"iPad2,1"   : @"iPad 2",            //
                                 @"iPad3,1"   : @"iPad",              // (3rd Generation)
                                 @"iPhone3,1" : @"iPhone 4",          // (GSM)
                                 @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                                 @"iPhone4,1" : @"iPhone 4S",         //
                                 @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                                 @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                                 @"iPad3,4"   : @"iPad",              // (4th Generation)
                                 @"iPad2,5"   : @"iPad Mini",         // (Original)
                                 @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                                 @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                                 @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                                 @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                                 @"iPhone7,1" : @"iPhone 6 Plus",     //
                                 @"iPhone7,2" : @"iPhone 6",          //
                                 @"iPhone8,1" : @"iPhone 6S",         //
                                 @"iPhone8,2" : @"iPhone 6S Plus",    //
                                 @"iPhone8,4" : @"iPhone SE",         //
                                 @"iPhone9,1" : @"iPhone 7",          //
                                 @"iPhone9,3" : @"iPhone 7",          //
                                 @"iPhone9,2" : @"iPhone 7 Plus",     //
                                 @"iPhone9,4" : @"iPhone 7 Plus",     //
                                 @"iPhone10,1": @"iPhone 8",          // CDMA
                                 @"iPhone10,4": @"iPhone 8",          // GSM
                                 @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                                 @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                                 @"iPhone10,3": @"iPhone X",          // CDMA
                                 @"iPhone10,6": @"iPhone X",          // GSM
                                 @"iPhone11,2": @"iPhone XS",         //
                                 @"iPhone11,4": @"iPhone XS Max",     //
                                 @"iPhone11,6": @"iPhone XS Max",     // China
                                 @"iPhone11,8": @"iPhone XR",         //
                                 @"iPhone12,1": @"iPhone 11",         //
                                 @"iPhone12,3": @"iPhone 11 Pro",     //
                                 @"iPhone12,5": @"iPhone 11 Pro Max", //

                                 @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                                 @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                                 @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                                 @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                                 @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                                 @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                                 @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                                 @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                                 @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                                 };
       }

       NSString* deviceName = [deviceNamesByCode objectForKey:code];

       if (!deviceName) {
           // Not found on database. At least guess main device type from string contents:

           if ([code rangeOfString:@"iPod"].location != NSNotFound) {
               deviceName = @"iPod Touch";
           }
           else if([code rangeOfString:@"iPad"].location != NSNotFound) {
               deviceName = @"iPad";
           }
           else if([code rangeOfString:@"iPhone"].location != NSNotFound){
               deviceName = @"iPhone";
           }
           else {
               deviceName = @"Unknown";
           }
       }

       return deviceName;
}

+(NSString *) deviceType{
    struct utsname systemInfo;

    uname(&systemInfo);

    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    return code;
}

+(NSString *) osVersion{
    struct utsname systemInfo;
    
    uname(&systemInfo);

    NSString* code = [NSString stringWithCString:systemInfo.version
                                        encoding:NSUTF8StringEncoding];
    return code;
}
+(NSString *) deviceOsVersion{
    NSString *strSystemName , *strSystemVers;
    strSystemName = [[UIDevice currentDevice]systemName];
    strSystemVers = [[UIDevice currentDevice]systemVersion];
    if (strSystemName != nil || strSystemVers != nil) {
        return [NSString stringWithFormat:@"%@ %@", strSystemName, strSystemVers];
    }else{
        return @"iPhone";
    }
}

+(NSString *) splitDateWithFormat : (int) format
                        dateString: (NSString *) mDate{
    NSArray *splitDate = [mDate componentsSeparatedByString:@"-"];
    if (format == 1) {
        return [NSString stringWithFormat:@"%@-%@-%@", splitDate[2], splitDate[1], splitDate[0]];
    }else{
        return @"";
    }
}

+(CAShapeLayer *) makeARoundedCornerTopLeftAndRight :  (UIView *) obView{
    
    UIBezierPath *maskPath = [UIBezierPath
                              bezierPathWithRoundedRect:obView.bounds
                              byRoundingCorners:(UIRectCornerTopLeft| UIRectCornerTopRight)
                              cornerRadii:CGSizeMake(20, 20)
                              ];

    CAShapeLayer *maskLayer = [CAShapeLayer layer];

    maskLayer.frame = obView.bounds;
    maskLayer.path = maskPath.CGPath;
    return maskLayer;
}

//    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
//        self.view.backgroundColor = [UIColor clearColor];
//
//        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//        //always fill the view
//        blurEffectView.frame = self.vwMainScreen.bounds;
//        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//
//        [self.vwMainScreen insertSubview:blurEffectView atIndex:0]; //if you have more UIViews, use an insertSubview API to place it where needed
//    }else{
//        self.vwMainScreen.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
//    }

+(void)resetTimer{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

+(void) goToBackNavigator : (UINavigationController *) navController{
    UINavigationController *navigationController = navController;
    [navigationController popViewControllerAnimated:YES];
}

+ (NSString*) compressAndEncodeToBase64String : (UIImage *)image withMaxSizeInKB:(float) maximumSize{
    float ratio = image.size.width / image.size.height;
    float xFinal = 700;
    float yFinal = xFinal/ratio;
    
    UIImage *scaledImage = [self imageWithImage:image scaledToSize:CGSizeMake(xFinal, yFinal)];
    
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    float maxSize = maximumSize*1024;
    
    NSData *imageData = UIImageJPEGRepresentation(scaledImage, compression);
    
//    NSData *iamgeData = UIImageje
    while ([imageData length] > maxSize && compression > maxCompression) {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(scaledImage, compression);
    }
        NSLog(@"Final Image Size: %.2f MB",(float)imageData.length/1024.0f/1024.0f);

    NSString *compressedImageString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    
    return compressedImageString;
    
}

+ (NSString*) compressandencodebase64string : (UIImage *)image withMaxSizeInKB:(float) maximumSize{
    float ratio = image.size.width / image.size.height;
    float xFinal = 700;
    float yFinal = xFinal/ratio;
    
    UIImage *scaledImage = [self imageWithImage:image scaledToSize:CGSizeMake(xFinal, yFinal)];
    
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
    float maxSize = maximumSize*1024;
    
    NSData *imageData = UIImageJPEGRepresentation(scaledImage, compression);
    
//    NSData *iamgeData = UIImageje
    while ([imageData length] > maxSize && compression > maxCompression) {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(scaledImage, compression);
    }
        NSLog(@"Final Image Size: %.2f MB",(float)imageData.length/1024.0f/1024.0f);

    NSString *base64Encoded = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];

    return base64Encoded;
    
}

+ (UIImage *) imageWithImage : (UIImage*)originalImage scaledToSize:(CGSize)newSize{
    @synchronized (self) {
        UIGraphicsBeginImageContext(newSize);
        [originalImage drawInRect:CGRectMake(0,0,newSize.width, newSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    return nil;
}

+ (NSString*) providerCheck : (NSString*) msisdn{
    
    NSMutableDictionary *dictTSel = [[NSMutableDictionary alloc]init];
    [dictTSel setValue:@"1" forKey:@"provider"];
    [dictTSel setValue:@[@"0811", @"0812", @"0813", @"0821", @"0822", @"0823", @"0851", @"0852", @"0853", @"0851"] forKey:@"numbers"];
    
    NSMutableDictionary *dictIndosat = [[NSMutableDictionary alloc]init];
    [dictIndosat setValue:@"2" forKey:@"provider"];
    [dictIndosat setValue:@[@"0814", @"0815", @"0816", @"0855", @"0856", @"0857", @"0858"] forKey:@"numbers"];
    
    NSMutableDictionary *dictXL = [[NSMutableDictionary alloc]init];
    [dictXL setValue:@"3" forKey:@"provider"];
    [dictXL setValue:@[@"0831",@"0832",@"0833",@"0838",@"0817",@"0818",@"0819",@"0859",@"0878"] forKey:@"numbers"];
    
    NSMutableDictionary *dictThree = [[NSMutableDictionary alloc]init];
    [dictThree setValue:@"4" forKey:@"provider"];
    [dictThree setValue:@[@"0895",@"0896",@"0897",@"0898",@"0899"] forKey:@"numbers"];
    
    NSMutableDictionary *dictSmartfren = [[NSMutableDictionary alloc]init];
    [dictSmartfren setValue:@"5" forKey:@"provider"];
    [dictSmartfren setValue:@[@"0881",@"0882",@"0883",@"0884",@"0885",@"0886",@"0887",@"0888",@"0889"] forKey:@"numbers"];
    
    
    NSMutableArray *arrProv = [[NSMutableArray alloc]init];
    [arrProv addObject:dictTSel];
    [arrProv addObject:dictXL];
    [arrProv addObject:dictThree];
    [arrProv addObject:dictIndosat];
    [arrProv addObject:dictSmartfren];
    
    if([msisdn hasPrefix:@"62"]){
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"0"];
    }
    
    for(NSDictionary *provider in arrProv){
        for(NSString* numbers in [provider objectForKey:@"numbers"]){
            if([numbers hasPrefix:[msisdn substringToIndex:4]]) {
                return [provider objectForKey:@"provider"];
            }
        }
    }
    
    return @"0";
}

+ (BOOL) isOTPPrefixRight : (NSString*)otpcard{
    NSArray *listPrefixCard = [[[NSUserDefaults standardUserDefaults]valueForKey:@"config_prefix_otpcard"]componentsSeparatedByString:@"|"];
    bool state = false;
    if(listPrefixCard != nil){
        for(NSString *prefixCard in listPrefixCard){
            state = [otpcard hasPrefix:prefixCard];
            if(state){
                return state;
            }
        }
    }
    return state;
}

+ (BOOL)isLanguageID{
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    return [[[userdefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"];
}

+(NSMutableParagraphStyle*) getParagraphStyle{
    NSMutableParagraphStyle *paragraphStyle;
    paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setTabStops:@[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:15 options:@{}]]];
    [paragraphStyle setDefaultTabInterval:15];
    [paragraphStyle setFirstLineHeadIndent:0];
    [paragraphStyle setHeadIndent:15];
    
    return paragraphStyle;
}

+ (UIImage *)imageFromLayer:(CALayer *)layer{
  UIGraphicsBeginImageContextWithOptions(layer.frame.size, NO, 0);

  [layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();

  UIGraphicsEndImageContext();

  return outputImage;
}

+ (UIView*) showNoData : (UIView*)view{
    UIView* containerView = [[UIView alloc]initWithFrame:view.bounds];
    CGFloat layoutWidth = view.frame.size.width;
    CGFloat layoutHeight = view.frame.size.height;
    CGFloat size = layoutWidth/2;
    CGFloat xPosLlayout = (layoutHeight/2) - (size/2);
    
    UIImageView *imgNoData = [[UIImageView alloc]initWithFrame:CGRectMake(size/2, xPosLlayout, size, size)];
    [imgNoData setImage:[UIImage imageNamed:@"ic_nodata_default"]];

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(imgNoData.frame.origin.x, imgNoData.frame.origin.y + size + 4, size, 15)];
    [label setText:lang(@"NO_DATA")];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
//    [label setMinimumFontSize:8];
    
    [containerView addSubview:imgNoData];
    [containerView addSubview:label];
    
    return containerView;
}

+ (UIView*) showTextNoData : (UIView*)view{
    UIView* containerView = [[UIView alloc]initWithFrame:view.bounds];
    CGFloat layoutWidth = view.frame.size.width;
    CGFloat layoutHeight = view.frame.size.height;
    CGFloat size = layoutWidth/2;
    CGFloat xPosLlayout = (layoutHeight/2) - (size/2);
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(size/2, xPosLlayout, size, size)];

    [label setText:lang(@"NO_DATA")];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
    [containerView addSubview:label];
    
    return containerView;
}

+ (void) addAccountData : (NSDictionary*)jsonObject{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[jsonObject objectForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
    [userDefaults setObject:[jsonObject objectForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
    [userDefaults setObject:[jsonObject objectForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
    [userDefaults setObject:[[[jsonObject objectForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
    [userDefaults setObject:[[[jsonObject objectForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
    [userDefaults synchronize];
}

+(BOOL) isDeviceHaveBottomPadding{
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate]window];
    if(mainWindow.safeAreaInsets.bottom > 0){
        return true;
    }
    return NO;
}

+(BOOL) isDeviceHaveNotch{
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate]window];
    if(mainWindow.safeAreaInsets.top > 0){
        return true;
    }
    return NO;
}

+(BOOL) isDeviceHaveNotch2{
    if ([[[UIApplication sharedApplication] keyWindow] safeAreaInsets].bottom > 0) {
        return true;
    }
    return NO;
}

+ (NSString*)sha256HashFor:(NSString*)input
{
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

+ (BOOL)isAlphabetOnly:(NSString*)text {
    NSString *nameRegex = @"^[A-Za-z]+((\\s)?([A-Za-z])+)*$";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [nameTest evaluateWithObject:text];
}

+(BOOL)isNumericOnly:(NSString *)text {
    NSString *phoneRegex = @"^[0-9]+$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:text];
}

+(NSString *)getUUID {
    NSString *str = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return str;
}

+ (CAGradientLayer*)gradientForFrame:(CGRect)frame{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = frame;
    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(const_color_primary) CGColor],(id)[UIColorFromRGB(const_color_lightgray) CGColor], nil];
    gradient.locations = @[@0.0, @0.8];
    
    return gradient;
}

+ (void) removeUserData{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [NSUserdefaultsAes removeValueForKey:@"customer_id"];
    [userDefault removeObjectForKey:@"customer_id"];

    [NSUserdefaultsAes removeValueForKey:@"password"];
    [userDefault removeObjectForKey:@"password"];

    [NSUserdefaultsAes removeValueForKey:@"msisdn"];
    [userDefault removeObjectForKey:@"msisdn"];

    [NSUserdefaultsAes removeValueForKey:@"mobilenumber"];
    [userDefault removeObjectForKey:@"mobilenumber"];

    [NSUserdefaultsAes removeValueForKey:@"email"];
    [userDefault removeObjectForKey:@"email"];

    [NSUserdefaultsAes removeValueForKey:@"publickey"];
    [userDefault removeObjectForKey:@"publickey"];

    [NSUserdefaultsAes removeValueForKey:@"privatekey"];
    [userDefault removeObjectForKey:@"privateKey"];

    [NSUserdefaultsAes removeValueForKey:@"session_id"];
    [userDefault removeObjectForKey:@"session_id"];

    [NSUserdefaultsAes removeValueForKey:@"zpk"];
    [userDefault removeObjectForKey:@"zpk"];

    [NSUserdefaultsAes removeValueForKey:@"iccid"];
    [userDefault removeObjectForKey:@"iccid"];

    [NSUserdefaultsAes removeValueForKey:@"imei"];
    [userDefault removeObjectForKey:@"imei"];
    
    [userDefault removeObjectForKey:@"customer_name"];
    
    [userDefault setObject:nil forKey:@"listRecipt"];
    [userDefault setObject:nil forKey:@"listInbox"];
    
    [userDefault setObject:nil forKey:@"mid"];
    [userDefault setObject:nil forKey:@"action"];
    [userDefault setObject:@"NO" forKey:@"hasLogin"];
    [userDefault setObject:@"YES" forKey:@"firstLogin"];
    [userDefault setValue:@"-" forKey:@"last"];
    [userDefault setValue:@"YES" forKey:ONBOARDING_NEED_CHECK_KEY];
    [userDefault setObject:nil forKey:@"req_login"];
    [userDefault setObject:@(1) forKey:@"financing_message_pop"];
    [userDefault setObject:nil forKey:@"param_deep_link"];
    [userDefault synchronize];
    
}


+ (void)removeAllArrangedViewOf:(UIStackView*) stackview{
    
    for(UIView* subview in stackview.arrangedSubviews){
        [stackview removeArrangedSubview:subview]; // <-- Removes Constraints as well
        [subview removeConstraints:subview.constraints];//  <--- Constraints will be invalid or non existing
        [subview removeFromSuperview];
    }
}

@end
