//
//  UIDropdownSearch.m
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "UIDropdownSearch.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@interface UIDropdownSearch ()
{
    BOOL filterEnabled;
}

@end

@implementation UIDropdownSearch

@synthesize tableView;
@synthesize searchController;
@synthesize allItems;
@synthesize displayedItems;
@synthesize filteredItems;
@synthesize callApiSucess;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Create a list to hold search results (filtered list)
    self.filteredItems = [[NSMutableArray alloc] init];
    
    // Initially display the full list.  This variable will toggle between the full and the filtered lists.
    self.displayedItems = self.allItems;
    
    // Here's where we create our UISearchController
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.obscuresBackgroundDuringPresentation = NO;
    
    // Add the UISearchBar to the top header of the table view
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self.searchController.searchBar setShowsCancelButton:YES];
    
    // Hides search bar initially.  When the user pulls down on the list, the search bar is revealed.
//    [self.tableView setContentOffset:CGPointMake(0, self.searchController.searchBar.frame.size.height)];
    
    self.tableView.delegate = self;
    filterEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)enableSearchControllerFilter:(BOOL)enabled
{
    filterEnabled = enabled;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [self.displayedItems count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)anIndexPath {
    
    UITableViewCell * cell = [aTableView dequeueReusableCellWithIdentifier:@"ItemCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] init];
    }
    cell.textLabel.text = [[self.displayedItems objectAtIndex:anIndexPath.row] objectForKey:[self displayedStringKey]];
    return cell;
}

// When the user types in the search bar, this method gets called.
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    NSString *searchString = aSearchController.searchBar.text;
    
    // Check if the user cancelled or deleted the search term so we can display the full list instead.
    if(filterEnabled)
        [self filterSearchControllerOptionsBySearchString:searchString];
}

- (void)filterSearchControllerOptionsBySearchString:(NSString *)searchString
{
    if (![searchString isEqualToString:@""]) {
        [self.filteredItems removeAllObjects];
        for (NSDictionary *str in self.allItems) {
            if ([searchString isEqualToString:@""] || [[str objectForKey:[self displayedStringKey]] localizedCaseInsensitiveContainsString:searchString] == YES) {
                [self.filteredItems addObject:str];
            }
        }
        
        // add search not found value if filtered items = 0 and search not found value defined
        if ([self.filteredItems count] == 0 && self.searchNotFoundValue != nil) {
            [self.filteredItems addObject:self.searchNotFoundValue];
        }
        
        self.displayedItems = self.filteredItems;
    }
    else {
        self.displayedItems = self.allItems;
    }
    [self.tableView reloadData];
}

-(void)modifySearchControllerOptionsBySearchString:(NSString *)searchString
{
    if(!self.filteredItems) self.filteredItems = [[NSMutableArray alloc] init];
    
    if (![searchString isEqualToString:@""]) {
        [self.filteredItems removeAllObjects];
        for (NSDictionary *str in self.allItems) {
            if ([searchString isEqualToString:@""] || [[str objectForKey:[self displayedStringKey]] localizedCaseInsensitiveContainsString:searchString] == YES) {
                [self.filteredItems addObject:str];
            }
        }
        self.allItems = self.filteredItems;
    }
    self.displayedItems = self.allItems;
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedData= [self.displayedItems objectAtIndex:indexPath.row];
    [_delegate dropdownSearch:self didSelectRowWithData:self.selectedData];
    [self.searchController dismissViewControllerAnimated:YES completion:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setOptions:(NSDictionary *)data
{
    NSMutableArray *tempData = [[NSMutableArray alloc] init];
    for (NSDictionary *aData in data){
        NSMutableDictionary *dictX = [NSMutableDictionary new];
        [dictX setValue:[aData objectForKey:@"nama"] forKey:@"nama"];
        [dictX setValue:[aData objectForKey:@"nilai"] forKey:@"nilai"];
        [dictX setValue:[aData objectForKey:@"nourut"] forKey:@"nourut"];
        [dictX setValue:[aData objectForKey:@"refid"] forKey:@"refid"];
        [tempData addObject:dictX];
    }
    
    if ([tempData count] > 0)
    {
        NSArray *tempArray = [[NSArray alloc] init];
        tempArray = tempData;
        NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"nourut" ascending:YES];
        self.allItems = [tempArray sortedArrayUsingDescriptors:@[descriptor]];
        [self.tableView reloadData];
    }
}

- (void)setOptionsWithData:(NSDictionary *)data andKeysToStore:(NSArray *)keysToStore andSortUsingKey:(nullable NSString *)sortKey
{
    NSMutableArray *tempData = [[NSMutableArray alloc] init];
    for (NSDictionary *aData in data){
        NSMutableDictionary *dictX = [NSMutableDictionary new];
        for (NSString *key in keysToStore ) {
            if ([aData objectForKey:key] == nil || [[aData objectForKey:key] isEqual:[NSNull null]]) {
                [dictX setValue:@"" forKey:key];
            } else {
                [dictX setValue:[aData objectForKey:key] forKey:key];
            }
        }
        [tempData addObject:dictX];
    }
    
    if ([tempData count] > 0)
    {
        if(sortKey != nil)
        {
            NSArray *tempArray = [[NSArray alloc] init];
            tempArray = tempData;
            NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:YES];
            self.allItems = [tempArray sortedArrayUsingDescriptors:@[descriptor]];
        }
        else self.allItems = tempData;
    }
}

- (void)getReferenceDataFromEndPoint:(NSString *)endpoint withKeysToStore:(NSArray *)keysToStore andSortUsingKey:(nullable NSString *)sortKey andDispatchGroup:(dispatch_group_t)dispatchGroup
{
    dispatch_group_enter(dispatchGroup);
    callApiSucess = NO;
    NSString *url =[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:endpoint]];
    NSString *token = [CustomerOnboardingData getToken];
    NSLog(@"---------------- %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error)
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *value;
            if([httpResponse statusCode] == 200) {
                if(ONBOARDING_SAFE_MODE)
                {
                    NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    if(encString)
                    {
                        NSData *decData = [Encryptor AESDecryptData:encString];
                        value = [NSJSONSerialization JSONObjectWithData:decData options:kNilOptions error:&error];
                        if(value)
                        {
                            self->callApiSucess = YES;
                            [self setOptionsWithData:value andKeysToStore:keysToStore andSortUsingKey:sortKey];
                        }
                        else self->callApiSucess = NO;
                    }
                    else self->callApiSucess = NO;
                }
                else
                {
                    value = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    if(value)
                    {
                        self->callApiSucess = YES;
                        [self setOptionsWithData:value andKeysToStore:keysToStore andSortUsingKey:sortKey];
                    }
                    else self->callApiSucess = NO;
                }
            }
            else
            {
                self->callApiSucess = NO;
                NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"ERROR FROM URL %@ : %@", url, [Encryptor AESDecrypt:encString]);
            }
        }
        else self->callApiSucess = NO;
        
        dispatch_group_leave(dispatchGroup);
    }] resume];
}

- (NSString *)getToken
{
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    if([ns objectForKey:ONBOARDING_TOKEN_KEY])
        return [ns objectForKey:ONBOARDING_TOKEN_KEY];
    else
    {
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[[CustomerOnboardingData apiUrl] stringByAppendingString:@"getToken"]]];
        [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
        NSMutableString *token = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [token replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, token.length)];
        [ns setObject:token forKey:ONBOARDING_TOKEN_KEY];
        [ns synchronize];
        return token;
    }
}

@end
