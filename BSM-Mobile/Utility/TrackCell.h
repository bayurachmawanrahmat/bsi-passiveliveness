//
//  TrackCell.h
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackCell : UICollectionViewCell
@property (strong,nonatomic) IBOutlet UILabel *lblTab;
@property (weak, nonatomic) IBOutlet UIView *vwLine;

@end
