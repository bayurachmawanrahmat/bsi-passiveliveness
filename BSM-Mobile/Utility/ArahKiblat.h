//
//  ArahKiblat.h
//  BSM-Mobile
//
//  Created by BSM on 9/18/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArahKiblat : NSObject

@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) float longitude;

+ (ArahKiblat *)objArahKiblat;
- (float)getLatitude;
- (void)setLatitude:(float)lat;
- (float)getLongitude;
- (void)setLongitude:(float)lng;
- (float)calcArahKiblat:(float)lat paramLong:(float)lng;

@end
