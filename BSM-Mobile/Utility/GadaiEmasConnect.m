//
//  GadaiEmasConnect.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GadaiEmasConnect.h"
#import "Encryptor.h"
#import "DESChiper.h"
#import "Utility.h"

#include <ifaddrs.h>
#include <arpa/inet.h>

@implementation GadaiEmasConnect
{
    NSHTTPURLResponse *httpResponse;
    Encryptor *encryptor;
    DESChiper *desChipper;

}

@synthesize delegate = _delegate;

- (id)initWithDelegate:(id)delegate{
    self = [super init];
    if (self){
        self.delegate = delegate;
        encryptor = [[Encryptor alloc]init];
        desChipper = [[DESChiper alloc]init];
        data = [[NSMutableData alloc]init];
    }
    
    return self;
}

+ (NSString *)apiUrl
{
    return GADAI_EMAS_API_URL;
}

+ (NSString *)safeEndpoint:(NSString *)endpoint
{
    if (ONBOARDING_SAFE_MODE)
    {
        return [Encryptor HexEncrypt:endpoint];
    }
    return endpoint;
}

- (void)getDataFromEndPoint:(NSString *)endpoint needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted{
    
    endPointRequest = endpoint;
    txtLoading = textLoading;
    isLoading = needLoading;
    isEncrypted = encrypted;
    
    [self setRequest:@"GET"];
}

- (void)sendPostData:(NSString*)params endPoint:(NSString *)endpoint needLoading:(BOOL)needLoading textLoading:(NSString *)textLoading encrypted:(BOOL)encrypted{
    paramsURL = params;
    endPointRequest = endpoint;
    txtLoading = textLoading;
    isLoading = needLoading;
    isEncrypted = encrypted;
    
    [self setRequest:@"POST"];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if(error == nil){
        [self dataProcessing:error];
    }else{
        NSLog(@"%@",[error localizedDescription]);
        NSError *err = [NSError errorWithDomain:@"gadaiemas" code:[error code] userInfo:@{NSLocalizedDescriptionKey:[error localizedDescription]}];
        [self.delegate didFinishLoadData:nil withEndpoint:endPointRequest completeWithError:err];
        [self dismissLoading];
    }
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)aData{
//    data = aData;
    [data appendData:aData];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response completionHandler:(void (^)(NSURLSessionResponseDisposition))completionHandler{
    httpResponse = (NSHTTPURLResponse*)response;
    completionHandler(NSURLSessionResponseAllow);
}

- (void)setRequest:(NSString*)method{
//    NSString *token = [GadaiEmasConnect getToken];
    NSLog(@"%@",[[[self class] apiUrl] stringByAppendingString:endPointRequest]);
    NSString *url = [[[self class] apiUrl] stringByAppendingString:[[self class] safeEndpoint:endPointRequest]];
    NSLog(@"%@",url);
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];

//    NSString *token = [[self class] getToken];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:@"application/x-www-form-urlencoded;text/plain;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"close" forHTTPHeaderField:@"Connection"];
//    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [urlRequest setHTTPMethod:method];
    
//    NSLog(@"REQUEST TO : %@",endPointRequest);
    
    if([method isEqualToString:@"POST"]){
        NSString *body = [self generateParamURL];
        [urlRequest setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    if(isLoading){
        if(!loadingAlert){
            [self showLoading:txtLoading];
        }
    }
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    NSURLSessionConfiguration *defaultConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    connection = [NSURLSession sessionWithConfiguration:defaultConfiguration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURLSessionTask *dataTask = [connection dataTaskWithRequest:urlRequest];
    [dataTask resume];
    
}

- (NSString *)stringParamFromData:(NSDictionary *)data
{
    NSMutableString *result = [[NSMutableString alloc] init];
    int i = 0;
    for (NSString *key in data.allKeys) {
        i == 0 ? [result appendFormat:@"%@=%@", key, [data objectForKey:key]] : [result appendFormat:@"&%@=%@", key, [data objectForKey:key]];
        i = i + 1;
    }
    return result;
}

- (void) dataProcessing:(NSError*)error{
    if(data != nil){
        NSDictionary *value = [[NSDictionary alloc]init];
        if(ONBOARDING_SAFE_MODE){
            NSString *res = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            res = [Encryptor AESDecrypt:res];
            @try{
                value = [NSJSONSerialization
                                        JSONObjectWithData:[res dataUsingEncoding:NSUTF8StringEncoding]
                                        options:kNilOptions
                                        error:nil];
            }@catch (NSException *exception) {
                NSLog(@"exception : %@", exception);
            }@finally{
//                NSError *err = [NSError errorWithDomain:@"gadaiemas" code:404 userInfo:@{NSLocalizedDescriptionKey:@"url not found"}];
//                [self.delegate didFinishLoadData:nil withEndpoint:endPointRequest completeWithError:err];
            }
        }else{
            @try{
                value = [NSJSONSerialization
                                        JSONObjectWithData:data
                                        options:kNilOptions
                                        error:nil];
            }@catch (NSException *exception) {
                NSLog(@"exception : %@", exception);
            }@finally{
//                NSError *err = [NSError errorWithDomain:@"gadaiemas" code:404 userInfo:@{NSLocalizedDescriptionKey:@"url not found"}];
//                [self.delegate didFinishLoadData:nil withEndpoint:endPointRequest completeWithError:err];
            }
        }

//        NSLog(@"RESPONSE : %@", value);
//        NSLog(@"Status Code %ld:",(long)[httpResponse statusCode]);
        
        if(value != nil){
            if(error){
                NSError *err = [NSError errorWithDomain:@"gadaiemas" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:[value valueForKey:@"message"]}];
                [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:err];
            }
            
            if((long)[httpResponse statusCode] == 200){
                NSString *message = [value valueForKey:@"message"];
                if([message isEqualToString:@"OK"]){
                    [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:error];
                }else{
                    NSError *err = [NSError errorWithDomain:@"gadaiemas" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:[value valueForKey:@"message"]}];
                    [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:err];
                }
            }else{
                NSError *err = [NSError errorWithDomain:@"gadaiemas" code:(long)[value valueForKey:@"status"] userInfo:@{NSLocalizedDescriptionKey:[value valueForKey:@"message"]}];
                [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:err];
            }
        }else{
            NSError *err = [NSError errorWithDomain:@"gadaiemas" code:404 userInfo:@{NSLocalizedDescriptionKey:@"url not found"}];
            [self.delegate didFinishLoadData:value withEndpoint:endPointRequest completeWithError:err];
        }
    }else{
        NSError *err = [NSError errorWithDomain:@"gadaiemas" code:404 userInfo:@{NSLocalizedDescriptionKey:@"url not found"}];
        [self.delegate didFinishLoadData:nil withEndpoint:endPointRequest completeWithError:err];
    }
    
    [self dismissLoading];
}

- (void)showLoading:(NSString *)message{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *strTitle;
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        strTitle = @"Mohon Menunggu";
    }else{
        strTitle = @"Please Wait";
    }
    loadingAlert = [[DLAVAlertView alloc]initWithTitle:strTitle message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];

    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
    [viewLoading setBackgroundColor:[UIColor clearColor]];
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loadingView setColor:[UIColor blackColor]];
    [loadingView startAnimating];
    [viewLoading addSubview:loadingView];
    
    [loadingAlert setContentView:viewLoading];
    [loadingAlert show];
}

- (void)dismissLoading{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [loadingAlert  dismissWithClickedButtonIndex:0 animated:YES];
    loadingAlert = nil;
}

- (NSString *)generateParamURL{
            
    DataManager *dataManager = [DataManager sharedManager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
    NSMutableDictionary *dataParams = [Utility translateParam:paramsURL];
//    NSLog(@"DATA Parm: %@", dataParams);
    
    NSDictionary *dict = @{@"dataParam":dataParams, @"encrypted":[NSNumber numberWithBool:isEncrypted]};
    
    NSMutableDictionary *dataParam = [dict objectForKey:@"dataParam"];
    
    if([dataParam objectForKey:@"language"]){
        NSArray *listLanguage = [defaults objectForKey:@"AppleLanguages"];
        [dataParam setObject:[listLanguage objectAtIndex:0] forKey:@"language"];
    }
    
    for(NSString *key in dataParam.allKeys){
        if([[dataParam objectForKey:key]isKindOfClass:[NSNumber class]] && ![[dataParam objectForKey:key]boolValue]){
            if([dataManager.dataExtra objectForKey:key]){
                [dataParam setObject:[dataManager.dataExtra valueForKey:key] forKey:key];
            }else{
//                NSLog(@"KEY %@ CHANGED TO EMPTY STRING", key);
                [dataParam setObject:@"" forKey:key];
            }
        }
    }
//    NSLog(@"DATA TO SENT: %@", dataParam);
//    return  [self encryptedParamValue:dataParam];
    return [self valueFromKey:dataParam];
}

- (NSString *)valueFromKey:(NSDictionary *)dataParam{
    NSString * result = @"";
    for (NSString *key in [dataParam allKeys]){
        if([[dataParam objectForKey:key]isKindOfClass:[NSArray class]]){
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[dataParam objectForKey:key]
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
            if(ONBOARDING_SAFE_MODE){
                NSLog(@"data : %@ || %@ ",[Encryptor HexEncrypt:key], [Encryptor HexEncrypt:jsonString]);
                
                NSString *encKey = [Encryptor HexEncrypt:key];
                encKey = [encKey stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                NSString *encValue = [Encryptor HexEncrypt:jsonString];
                encValue = [encValue stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                
                result = [NSString stringWithFormat:@"%@%@=%@&",result,encKey,encValue];
            }else{
                result = [NSString stringWithFormat:@"%@%@=%@&",result,key, jsonString];
            }
            
        }else{
                
            if(ONBOARDING_SAFE_MODE){
                if([self exceptionalKeyCheck:key]){
                    NSString *encKey = [Encryptor HexEncrypt:key];
                    encKey = [encKey stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                    
                    result = [NSString stringWithFormat:@"%@%@=%@&",result,encKey, [dataParam valueForKey:key]];
                }else{
//                    NSLog(@"data : %@ || %@ ",[Encryptor HexEncrypt:key], [Encryptor HexEncrypt:[dataParam valueForKey:key]]);
                    
                    NSString *encKey = [Encryptor HexEncrypt:key];
                    encKey = [encKey stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                    NSString *encValue = [Encryptor HexEncrypt:[dataParam valueForKey:key]];
                    encValue = [encValue stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
                    
                    result = [NSString stringWithFormat:@"%@%@=%@&",result,encKey,encValue];
                }
            }else{
                result = [NSString stringWithFormat:@"%@%@=%@&",result,key, [dataParam valueForKey:key]];
            }
            
        }
    }
    
    NSLog(@"%@",result);
    return result;
}

- (NSString *)encryptedParamValue:(NSDictionary *)dataParam{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    NSString * result = @"";
    for (NSString *key in [dataParam allKeys]){
        if([[dataParam objectForKey:key]isKindOfClass:[NSArray class]]){
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[dataParam objectForKey:key]
                                                               options:NSJSONWritingPrettyPrinted
                                                                 error:&error];
            NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//                NSLog(@"data : %@ || %@ ",[Encryptor HexEncrypt:key], [Encryptor HexEncrypt:jsonString]);
                result = [NSString stringWithFormat:@"%@%@=%@&",result,[Encryptor HexEncrypt:key],[Encryptor HexEncrypt:jsonString]];
            
                [dict setObject:[Encryptor HexEncrypt:jsonString] forKey:[Encryptor HexEncrypt:key]];
        }else{
                
            if([self exceptionalKeyCheck:key]){
                result = [NSString stringWithFormat:@"%@%@=%@&",result,[Encryptor HexEncrypt:key], [dataParam valueForKey:key]];
            }else{
//                NSLog(@"data : %@ || %@ ",[Encryptor HexEncrypt:key], [Encryptor HexEncrypt:[dataParam valueForKey:key]]);
                result = [NSString stringWithFormat:@"%@%@=%@&",result,[Encryptor HexEncrypt:key],[Encryptor HexEncrypt:[dataParam valueForKey:key]]];
                
                [dict setObject:[Encryptor HexEncrypt:[Encryptor HexEncrypt:[dataParam valueForKey:key]]] forKey:[Encryptor HexEncrypt:key]];
            }
        }
    }
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    result = jsonString;
    
    return result;
}

- (BOOL)exceptionalKeyCheck : (NSString*)key{
    if([key isEqualToString:@"fotoDiri"] ||
        [key isEqualToString:@"fotoKTP"] ||
        [key isEqualToString:@"fotoDiriKTP"] ||
        [key isEqualToString:@"fotoEmas"]){
        return YES;
    }
    return NO;
}


- (NSString *)getIPAddress {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return [address isEqualToString:@"error"]?@"":address;
    
}

+ (NSString *)getToken
{
    Encryptor *enc = [[Encryptor alloc] init];
    if([enc getUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY])
        return [enc getUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
    else
    {
        NSMutableString *token;
        NSString *url = [[self class] apiUrl];
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[url stringByAppendingString:[[self class] safeEndpoint:@"getToken"]]]];
        [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
        NSURLResponse *__autoreleasing *response = nil;
        NSError *__autoreleasing *error = nil;
        NSData *data = [[self class] sendSynchronousRequest:urlRequest returningResponse:response error:error];
        if(!error)
        {
            if(ONBOARDING_SAFE_MODE)
                token = [[NSMutableString alloc] initWithString:[Encryptor AESDecrypt:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]]];
            else
                token = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            [token replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, token.length)];

            if ([token isKindOfClass:[NSString class]] && !([token length]<=0))
                [enc setUserDefaultsObject:token forKey:ONBOARDING_TOKEN_KEY];
            else {
                if(ONBOARDING_SAFE_MODE) {
                    NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSData *decryptedData = [Encryptor AESDecryptData:ret];
                    NSDictionary *value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:error];
                    NSLog(@"Error retrieving token : %@", ret);
                    if(value)
                        NSLog(@"Error retrieving token : %@", value);
                    else
                        NSLog(@"Error retrieving token : %@", decryptedData);
                }
                [enc removeUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
            }
        }
        else
        {
            NSLog(@"Error retrieving token : %@", *error);
            [enc removeUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
        }
        return token;
    }
}

+ (void)getFromEndpoint:(NSString *)endpoint dispatchGroup:(dispatch_group_t)dispatchGroup returnData:(NSMutableDictionary *)returnData
{
    dispatch_group_enter(dispatchGroup);

    NSString *url = [[[self class]apiUrl] stringByAppendingString:[[self class] safeEndpoint:endpoint]];
//    NSString *token = [[self class] getToken];
//    NSLog(@"---------------- %@", [[[self class] apiUrl] stringByAppendingString:endpoint]);
//    NSLog(@"---------------- %@", url);
//
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
//    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
            [returnData setValue:error forKey:@"error_message"];
        }
        else
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *value  = [NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:kNilOptions
                                    error:&error];
            if((long)[httpResponse statusCode] == 200) {
                [returnData setValue:[NSNumber numberWithBool:YES] forKey:@"is_success"];
                if(value)
                    [returnData setValue:value forKey:@"data"];
                else
                {
                    NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    if(ret)
                    {
                        if(ONBOARDING_SAFE_MODE)
                        {
                            NSData *decryptedData = [Encryptor AESDecryptData:ret];
                            value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
                            if(value)
                                [returnData setValue:value forKey:@"data"];
                            else
                                [returnData setValue:[Encryptor AESDecrypt:ret] forKey:@"data"];
                        }
                        else
                            [returnData setValue:ret forKey:@"data"];
                    }
                }
            }
            else
            {
                if(ONBOARDING_SAFE_MODE)
                {
                    NSString *ret = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                    NSLog(@"ERROR : %@", [Encryptor AESDecrypt:ret]);
                }
                else
                {
                    [returnData setValue:[NSNumber numberWithBool:NO] forKey:@"is_success"];
                    [returnData setValue:[value objectForKey:@"message"] forKey:@"error_message"];
                }
            }
        }
        dispatch_group_leave(dispatchGroup);
    }] resume];
}
@end
