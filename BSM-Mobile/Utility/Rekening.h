//
//  Rekening.h
//  BSM-Mobile
//
//  Created by ARS on 30/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rekening : NSObject

@property NSString *jenisKyc;
@property NSString *namaNasabah;
@property NSString *nomorRekening;
@property NSString *jenisTabungan;
@property NSString *jenisKartu;
@property NSString *namaCabang;
@property NSString *alamatCabang;
@property NSString *kodeCabang;

- (instancetype)init;
- (void)encodeWithCoder:(NSCoder *)coder;
- (instancetype)initWithCoder:(NSCoder *)coder;

@end
