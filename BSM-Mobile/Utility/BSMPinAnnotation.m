//
//  BSMPinAnnotation.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/11/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "BSMPinAnnotation.h"

@implementation BSMPinAnnotation

- (MKAnnotationView *)annotationView{
    MKMarkerAnnotationView *anno = [[MKMarkerAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"BSMPIN"];
    anno.enabled = YES;
    anno.canShowCallout = YES;
    anno.accessibilityLabel = @"bsm_annotation";
    
    anno.markerTintColor = UIColorFromRGB(const_color_primary);
//    anno.glyphImage = [UIImage imageNamed:@"Icon-bsm-47.png"];
//    anno.glyphTintColor = [UIColor whiteColor];
    
    return anno;
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate{
    _coordinate = newCoordinate;
}

- (void)setTitle:(NSString *)title{
    _title = title;
}

- (void)setSubtitle:(NSString *)subtitle{
    _subtitle = subtitle;
}

- (void)setBranchData:(NSDictionary *)branchData{
    _branchData = branchData;
}
@end
