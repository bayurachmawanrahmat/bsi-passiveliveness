//
//  Biometric.h
//  BSM-Mobile
//
//  Created by BSM on 2/19/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Biometric : NSObject

typedef NS_ENUM(NSUInteger, BPDeviceType) {
    BPDeviceTypeUnknown,
    BPDeviceTypeiPhone4,
    BPDeviceTypeiPhone5,
    BPDeviceTypeiPhone6,
    BPDeviceTypeiPhone6Plus,
    BPDeviceTypeiPhone7,
    BPDeviceTypeiPhone7Plus,
    BPDeviceTypeiPhoneX,
    BPDeviceTypeiPad
};

+ (Biometric *)objBiometric;
+ (BPDeviceType)getDeviceType;
- (BOOL) isBiometricIDAvailable;
- (BOOL) isTouchIDAvailable;
- (BOOL) supportFaceID;
- (BOOL) isFaceIDAvailable;

@end
