//
//  CustomBtn.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 13/11/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CustomBtn.h"


@interface CustomBtn (){
    int colorSet;
}

@end

@implementation CustomBtn

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}

//added custum properities to button
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        // Initialization code
        self.layer.cornerRadius = self.layer.frame.size.height/2;
        self.layer.masksToBounds = true;
        self.titleLabel.font = [UIFont fontWithName:const_font_name3 size:14.0];
//        self.layer.borderWidth = 2;
        colorSet = PRIMARYCOLORSET;
        [self setColorSet:colorSet];
    }
    return self;
}

- (void)setEnabled:(BOOL)enabled{
    [super setEnabled:enabled];
    if(enabled){
        [self setColorSet:colorSet];
    }else{
        [self setColorSet:DISABLEDCOLORSET];
    }
}

- (void)setColorSet:(int)newColorSet{
    switch (newColorSet) {
//        case PRIMARYCOLORSET:{
//            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            self.layer.masksToBounds = true;
//            self.layer.cornerRadius = 20;
//
//            CAGradientLayer *gradient = [CAGradientLayer layer];
//            gradient.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
//            gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(const_color_gold) CGColor],(id)[UIColorFromRGB(const_color_gold2) CGColor], nil];
//            gradient.locations = @[@0.0, @0.5];
//            [gradient setStartPoint:CGPointMake(0.0, 0.5)];
//            [gradient setEndPoint:CGPointMake(1.0, 0.5)];
//            self.layer.borderWidth = 0;
//
//            [self setBackgroundImage:[self imageFromLayer:gradient] forState:UIControlStateNormal];
//
//            break;
//        }
        case PRIMARYCOLORSET:{
            self.layer.borderColor = [UIColorFromRGB(const_color_primary) CGColor];
            self.layer.backgroundColor = [UIColorFromRGB(const_color_primary) CGColor];
//            self.layer.borderColor = [UIColorFromRGB(const_color_secondary) CGColor];
//            self.layer.backgroundColor = [UIColorFromRGB(const_color_secondary) CGColor];
            [self setBackgroundImage:nil forState:UIControlStateNormal];
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.layer.masksToBounds = true;
//            self.layer.borderWidth = 2;
            self.layer.cornerRadius = 20;
            
            break;
        }
        case SECONDARYCOLORSET:
            self.layer.borderColor = [UIColorFromRGB(const_color_gray) CGColor];
            self.layer.backgroundColor = [UIColorFromRGB(const_color_lightgray) CGColor];
            [self setBackgroundImage:nil forState:UIControlStateNormal];
            [self setTitleColor:UIColorFromRGB(const_color_darkgray) forState:UIControlStateNormal];
            self.layer.masksToBounds = true;
            self.layer.borderWidth = 2;
            self.layer.cornerRadius = 20;

            break;
            
        case TERTIARYCOLORSET:
//            self.layer.borderColor = [UIColorFromRGB(const_color_primary) CGColor];
//            self.layer.backgroundColor = [UIColorFromRGB(const_color_primary) CGColor];
            self.layer.borderColor = [UIColorFromRGB(const_color_secondary) CGColor];
            self.layer.backgroundColor = [UIColorFromRGB(const_color_secondary) CGColor];
            [self setBackgroundImage:nil forState:UIControlStateNormal];
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.layer.masksToBounds = true;
            self.layer.cornerRadius = 20;

            break;
        
        case DISABLEDCOLORSET:
            self.layer.borderColor = [UIColorFromRGB(const_color_gray) CGColor];
            self.layer.backgroundColor = [UIColorFromRGB(const_color_gray) CGColor];
            [self setBackgroundImage:nil forState:UIControlStateNormal];
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.layer.masksToBounds = true;
            self.layer.borderWidth = 2;
            self.layer.cornerRadius = 20;

            break;
            
        default:
            break;
    }
}

- (UIImage *)imageFromLayer:(CALayer *)layer{
  UIGraphicsBeginImageContextWithOptions(layer.frame.size, NO, 0);

  [layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();

  UIGraphicsEndImageContext();

  return outputImage;
}


@end
