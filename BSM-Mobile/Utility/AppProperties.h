//
//  AppProperties.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppProperties : NSObject

+ (NSDictionary*) getResources;
+ (BOOL) checkIsProd;
+ (BOOL) checkSslPinningIsOn;
+ (BOOL) checkJbProtectIsOn;
+ (NSString*) getAPIUrl;
+ (NSString*) getKeyChiper;
+ (NSString*) getAPIUrlCheck;

+ (NSString*) getAPIUrlGadaiEmas;
+ (NSString*) getAPIUrlSyariahpoint;

+ (NSString*) getSliderURL;

+ (NSString*) getGMAPUrl;
+ (NSString*) getApiKeyServer;

+ (NSString*) getClientId;
+ (NSString*) getClientSecret;

+ (NSString*) getStatusUniqueDevice;
+ (NSString*) getAPIUrlOnboardingSafe;
+ (NSString*) getAPIUrlOnboarding;
+ (NSString*) getAPIUrlOnboardingSafeFR;
+ (NSString*) getAPIUrlOnboardingFR;

+ (NSString*) getOnboardingLXApplicationName;
+ (NSString*) getOnboardingLXAddress;
+ (NSString*) getOnboardingLXClientTennant;
+ (NSString*) getOnboardingLXClientID;
+ (NSString*) getOnboardingLXClientSecret;
+ (NSString*) getOnboardingGOCoderLicenseKey;

+ (NSString*) getOnboardingGOCoderHost;
+ (NSString*) getOnboardingGOCoderPort;
+ (NSString*) getOnboardingGOCoderAppName;
+ (NSString*) getOnboardingGOCoderUserName;
+ (NSString*) getOnboardingGOCoderPassword;
+ (NSString*) getKeyRetrival;
+ (NSString*) getClientCredential;
+ (NSString*) getSecretCredential;
+ (BOOL) getEnableUniqDevice;
+ (NSArray*) getArrayStoryboardList;

@end

NS_ASSUME_NONNULL_END
