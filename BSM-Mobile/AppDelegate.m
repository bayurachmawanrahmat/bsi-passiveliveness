//
//  AppDelegate.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "AppDelegate.h"
#import "NSData+Hex.h"
#import "Utility.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
//#import "BasicEncodingRules.h"
#import "Utility.h"
#import "Firebase.h"
#import "InboxHelper.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AESCipher.h"
#import "JBDetect.h"
#import <FirebaseDynamicLinks/FirebaseDynamicLinks.h>
#import "UITextViewWorkaround.h"
#import "AppProperties.h"
#import "JailbreakChecker.h"
#import "NSUserdefaultsAes.h"
#import <AudioToolbox/AudioToolbox.h>

@import Firebase;
@import UserNotifications;
//@import FirebaseMessaging;

@interface AppDelegate(){
    NSString *InstanceID;
    UIApplication *mApps;
    InboxHelper *inboxHelper;
    AESCipher *aesChiper;
    UIView* blurView;
    
}
@property (strong, nonatomic) Reachability *mReachConnect;
@property (strong, nonatomic) NSString *reachState;
@property (assign, nonatomic) NSInteger countReach;

@end
@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";


//UIBackgroundTaskIdentifier backgroundUpdateTask;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"running");
    
    [self preventUserIntefaceMode];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    inboxHelper = [[InboxHelper alloc]init];
    aesChiper = [[AESCipher alloc]init];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang containsString:@"en"]) {
        [userDefault setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
        [userDefault synchronize];
    }else{
        [userDefault setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
        [userDefault synchronize];
    }
    
    [self moveUserdata];
    
//    [inboxHelper createTblInbox];
    [inboxHelper createTbl];
    
    //base line for import last list
    NSArray *listReceipt = [userDefault valueForKey:@"listRecipt"];
    if (listReceipt.count > 0 || listReceipt != nil) {
        for(int i=0; i<listReceipt.count; i++){
            NSMutableDictionary *dataImport = [[NSMutableDictionary alloc]init];
            NSDictionary *dictDataList = [listReceipt objectAtIndex:i];
            NSString *strTemplate = [dictDataList valueForKey:@"template"];
             NSDictionary *dataRespon = [NSJSONSerialization JSONObjectWithData:[[dictDataList valueForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            if ([strTemplate isEqualToString:@"GENCF02"]) {
                [dataImport setValue:[dataRespon valueForKey:@"msg"] forKey:@"msg"];
                
            }else if([strTemplate isEqualToString:@"QRPCF02"]){
                [dataImport setValue:[dictDataList valueForKey:@"response"] forKey:@"msg"];
            }
            [dataImport setValue:[dataRespon valueForKey:@"title"] forKey:@"title"];
            [dataImport setValue:[dataRespon valueForKey:@"trxref"] forKey:@"trxref"];
             [dataImport setValue:[dataRespon valueForKey:@"footer_msg"] forKey:@"footer_msg"];
            [dataImport setValue:@"0" forKey:@"marked"];
            [dataImport setValue:[dictDataList valueForKey:@"template"] forKey:@"template"];
            [dataImport setValue:[dictDataList valueForKey:@"time"] forKey:@"time"];
            [dataImport setValue:[dictDataList valueForKey:@"transaction_id"] forKey:@"transaction_id"];
            [dataImport setValue:[dictDataList valueForKey:@"jenis"] forKey:@"jenis"];
            [inboxHelper insertDataInbox:dataImport];
        }
        [userDefault setObject:nil forKey:@"listRecipt"];
        [userDefault setObject:nil forKey:@"listInbox"];
    }
    
    
    //-----------------------------
    
    
    mApps = application;
    //mApps.applicationIconBadgeNumber = 0;
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    
    
    [userDefault setObject:nil forKey:@"mid"];
    [userDefault setObject:nil forKey:@"action"];
    //[userDefault removeObjectForKey:@"hasLogin"];
    [userDefault setObject:@"NO" forKey:@"hasLogin"];
    [userDefault setObject:@"YES" forKey:@"firstLogin"];
    //[userDefault setObject:@"" forKey:@"fromSliding"];
    [userDefault setValue:@"-" forKey:@"last"];
	[userDefault setValue:@"YES" forKey:ONBOARDING_NEED_CHECK_KEY];
	[userDefault setObject:nil forKey:@"req_login"];
    [userDefault setObject:@(1) forKey:@"financing_message_pop"];
    [userDefault setObject:nil forKey:@"param_deep_link"];
    [userDefault synchronize];
    
    
    //this handle notification when app is terminate
    NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo != nil)
    {
        NSInteger countNotif = [[userDefault valueForKey:@"count_notif"]integerValue];
        if (countNotif > 0) {
            countNotif = countNotif - 1;
        }
        
        [userDefault setValue: @(countNotif) forKey:@"count_notif"];
        [userDefault setObject:[userInfo valueForKey:@"mid"] forKey:@"mid"];
        [userDefault setValue:[userInfo valueForKey:@"msg_type"] forKey:@"msg_type"];
        [userDefault setObject:@"RECEIVING" forKey:@"action"];
        [userDefault synchronize];
        
        NSDictionary* mUserInfo = @{@"position": @(1117)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:mUserInfo];
        
       
    }
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    _splashImageView = [[UIView alloc]init];
    _splashImageView = [[NSBundle mainBundle] loadNibNamed:@"LandingView" owner:self options:nil].firstObject;
    _splashImageView.frame = _window.safeAreaLayoutGuide.layoutFrame;
    if(_window.safeAreaInsets.top > 24){
        _splashImageView.frame = CGRectMake(0, 0, width, height);
    }
    [_splashImageView translatesAutoresizingMaskIntoConstraints];
    _splashImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.window.rootViewController.view addSubview:_splashImageView];
    [self.window.rootViewController.view bringSubviewToFront:_splashImageView];
    /*[UIView animateWithDuration:0.5f
                          delay:3.0f
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         _splashImageView.alpha = .0f;
                     } completion:^(BOOL finished){
                         if (finished) {
                             [_splashImageView removeFromSuperview];
                         }
                     }];*/
    
    //base line import sqlite to backup
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //Run your loop here
       // [userDefault setValue:@"0" forKey:@"isBackup"];
        bool isBackup = [[userDefault valueForKey:@"isBackup"]boolValue];
        if (!isBackup) {
            NSInteger countInbox = [self->inboxHelper inboxCount];
            NSLog(@"%ld",countInbox);
            if(countInbox != 0){
//                NSString *strCID = [NSString stringWithFormat:@"%@", [userDefault valueForKey:@"customer_id"]];
                NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
                NSArray *arInbox = [self->inboxHelper listAllDataInbox];
                NSLog(@"%@", arInbox);
                NSMutableArray *arEncryptInbox = [[NSMutableArray alloc]init];
                for(NSDictionary *data in arInbox){
                    NSDictionary *dataEncrypt = [self->aesChiper dictAesEncryptString:data];
                    NSDictionary *dataContent = @{@"content" : dataEncrypt};
                    [arEncryptInbox addObject:dataContent];
                }
                NSDictionary *mData = @{@"data" : arEncryptInbox};
                NSLog(@"ar encrypt : %@", arEncryptInbox);
                [Utility removeForKeychainKey:strCID];
                [Utility setDict:mData forKey:strCID];
                [userDefault setValue:@"1" forKey:@"isBackup"];
                [userDefault synchronize];
                NSDictionary *dataBacup = [Utility dictForKeychainKey:strCID];
                NSLog(@"data berhasil backup : %@", dataBacup);
            }
            
        }
        
    });
    
    
    //load config di position no background old version
    [self getConfigApp];
    
    NSString *configIndicator = [userDefault objectForKey:@"config_indicator"];
    bool stateIndicator = [configIndicator boolValue];
    if (stateIndicator) {
//        [self isNetworkConnected];
        
        [userDefault setObject:@"0" forKey:@"state_indicator"];
        [userDefault synchronize];
        
        [self getMainMenu];
        //
        [self checkUpdate];
        
        [self checkConnection];
        [self mStartCountDown];
        
//        [self afnCheckConnectionServer];
    }else{
        [self getMainMenu];
        //
        [self checkUpdate];
        
        
    }
    
    [self checkingJailBreak:application];
    
    //[Fabric with:@[[Crashlytics class]]];
    
    // Fixing XCode 11.2 bug
    [UITextViewWorkaraound executeWorkaround];

//    [[UILabel appearance] setFont:[UIFont fontWithName:@"Monstserrat-Regular" size:[UILabel appearance].font.pointSize]];
    
    // add MyMenu setup
    if(![userDefault objectForKey:@"showMyMenu"]){
        [userDefault setObject:@"YES" forKey:@"showMyMenu"];
    }
    
    return YES;
}

#pragma mark added this method on 6.0.1 pentest findings
- (void) moveUserdata{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if([userDefault objectForKey:@"customer_id"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"customer_id"] forKey:@"customer_id"];
        [userDefault removeObjectForKey:@"customer_id"];
    }
    
    if([userDefault objectForKey:@"password"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"password"] forKey:@"password"];
        [userDefault removeObjectForKey:@"password"];
    }
    
    if([userDefault objectForKey:@"msisdn"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"msisdn"] forKey:@"msisdn"];
        [userDefault removeObjectForKey:@"msisdn"];
    }
    
    if([userDefault objectForKey:@"mobilenumber"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"mobilenumber"] forKey:@"msisdn"];
        [userDefault removeObjectForKey:@"mobilenumber"];
    }
    
    if([userDefault objectForKey:@"email"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"email"] forKey:@"email"];
        [userDefault removeObjectForKey:@"email"];
    }
    
    if([userDefault objectForKey:@"publickey"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"publickey"] forKey:@"publickey"];
        [userDefault removeObjectForKey:@"publickey"];
    }
    
    if([userDefault objectForKey:@"privateKey"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"privateKey"] forKey:@"privatekey"];
        [userDefault removeObjectForKey:@"privateKey"];
    }
    
    if([userDefault objectForKey:@"session_id"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"session_id"] forKey:@"session_id"];
        [userDefault removeObjectForKey:@"session_id"];
    }
    
    if([userDefault objectForKey:@"zpk"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"zpk"] forKey:@"zpk"];
        [userDefault removeObjectForKey:@"zpk"];
    }
    
    if([userDefault objectForKey:@"iccid"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"iccid"] forKey:@"iccid"];
        [userDefault removeObjectForKey:@"iccid"];
    }
    
    if([userDefault objectForKey:@"imei"]){
        [NSUserdefaultsAes setObject:[userDefault objectForKey:@"imei"] forKey:@"imei"];
        [userDefault removeObjectForKey:@"imei"];
    }
}

- (void) registeringNotification{
    // Create the custom actions for expired timer notifications.
    UNNotificationAction* snoozeAction = [UNNotificationAction
          actionWithIdentifier:@"SNOOZE_ACTION"
          title:@"Snooze"
          options:UNNotificationActionOptionNone];
     
    UNNotificationAction* stopAction = [UNNotificationAction
          actionWithIdentifier:@"STOP_ACTION"
          title:@"Stop"
          options:UNNotificationActionOptionForeground];
     
    // Create the category with the custom actions.
    UNNotificationCategory* category = [UNNotificationCategory
          categoryWithIdentifier:@"SHOLAT_TIME"
          actions:@[snoozeAction, stopAction]
          intentIdentifiers:@[]
          options:UNNotificationCategoryOptionNone];
     
    // Register the notification categories.
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center setNotificationCategories:[NSSet setWithObjects: category,
          nil]];
}
-(void)checkingJailBreak : (UIApplication *) application
{
//    BOOL isValid = true;
//    if (isJB()) {
//        isValid = false;
//    }
    
    BOOL isValid = false;
    if (isSecurityCheckPassed()) {
        isValid = true;
    }
    
//    if (isCracked()) {
//        isValid = false;
//    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isValid forKey:@"is_jail_break"];
    [userDefault synchronize];
    
    if (!isValid) {
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        NSString *strMessage;
        if ([lang isEqualToString:@"id"]) {
            strMessage = @"RC99 Rooted Device Detected: \"Kami mendeteksi anda menggunakan perangkat yang dimodifikasi (rooted/jailbreak). Dengan demikian, anda tidak dapat menggunakan BSI Mobile. Harap pulihkan perangkat anda ke pengaturan aslinya. Info lebih lanjut silahkan menghubungi Call Center.\"";
        }else{
            strMessage = @"RC99 Rooted Device Detected: \"We detected you are using a modified device (rooted / jailbreak). Therefore, you cannot use BSI Mobile. Please restore your device to its original settings. For further information please contact Center.\"";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 999;
        [alert show];
    }else{
        [self firebaseInit:application];
//        if ([[userDefault objectForKey:@"customer_id"]]) {
        if ([NSUserdefaultsAes getValueForKey:@"customer_id"]) {
            
            [userDefault setValue:nil forKey:OBJ_FINANCE];
            [userDefault setValue:nil forKey:OBJ_SPESIAL];
            [userDefault setValue:nil forKey:OBJ_ALL_ACCT];
            [userDefault setValue:nil forKey:OBJ_ROLE_FINANCE];
            [userDefault setValue:nil forKey:OBJ_ROLE_SPESIAL];
            [userDefault setValue:nil forKey:OBJ_SEARCH_FEATURE];
            [userDefault synchronize];

        }
        
        if (![[userDefault valueForKey:@"config_onboarding"] boolValue]){
            [UIView animateWithDuration:0.5f
                                  delay:1.0f
                                options:UIViewAnimationOptionTransitionFlipFromRight
                             animations:^{
                self->_splashImageView.alpha = .0f;
            } completion:^(BOOL finished){
                if (finished) {
                    [self->_splashImageView removeFromSuperview];
                }
            }];
        }
//
    }
    
}
-(RACSignal *) mSignalCountDown{
    return [[RACSignal return:@"token"]delay:5];
}
-(RACSignal *) mStopSignal{
    return [RACSignal empty];
}

-(void) mStartCountDown{
    NSInteger refreshInterval = 6;
    RACSignal *refreshSignalCountDown = [[RACSignal interval:refreshInterval onScheduler:[RACScheduler mainThreadScheduler]]startWith:[NSDate date]];
    [[[[refreshSignalCountDown
        flattenMap:^RACStream *(id _)
        {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            return [self mSignalCountDown];
        }]
       map:^NSDate *(NSString *countDown)
       {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
           // display paymentToken here
           NSLog(@"network count down : %@", countDown);
           [self->_mReachConnect stopNotifier];
           [self->_mReachConnect startNotifier];
           return [[NSDate date] dateByAddingTimeInterval:refreshInterval];
       }]
      flattenMap:^RACStream *(NSDate *expiryDate)
      {
          return [[[[RACSignal interval:1 onScheduler:[RACScheduler mainThreadScheduler]]
                    startWith:[NSDate date]]
                   takeUntil:[refreshSignalCountDown skip:1]]
                  map:^NSNumber *(NSDate *now)
                  {
                      return @([expiryDate timeIntervalSinceDate:now]);
                  }];
      }]
     subscribeNext:^(NSNumber *remaining)
     {
         // update timer readout here
         NSLog(@"Remaining Count down : %@", remaining);
     }];
    
}

-(void) checkConnection{
    DLog(@"CEK CONNECTION");
    NSURL *xUrl = [NSURL URLWithString:@"https://google.com"];
    _mReachConnect = [Reachability reachabilityWithURL:xUrl];
    _reachState = @"unreach";
    _countReach = 0;
    
    //    __weak typeof(self) weakSelf = self;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
   [_mReachConnect startNotifier];
}
- (void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    
    bool stateConnected = false;
    
    NetworkStatus internetStatus = [self.mReachConnect currentReachabilityStatus];
    
    switch (internetStatus) {
        case NotReachable:
            NSLog(@"Not Reachable");
            stateConnected = FALSE;
            break;
        case ReachableViaWiFi:
            NSLog(@"Reach Wifi");
            stateConnected = TRUE;
            break;
        case ReachableViaWWAN:
            NSLog(@"Reach WMAN");
            stateConnected = TRUE;
            break;
            
    }
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    //[_mReachConnect stopNotifier];
    if(stateConnected){
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_CHECKED_CONNECTION] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:3.0];
        [request setValue:@"1" forHTTPHeaderField:@"check"];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   // optionally update the UI to say 'done'
                                   if(error){
                                       // update the UI to indicate error
                                       [userDefault setObject:@"2" forKey:@"state_indicator"];
                                       NSDictionary* reachUserInfo = @{@"reachCallback": @"net_reach", @"code_state" : @"2"};
                                       NSNotificationCenter* reachNc = [NSNotificationCenter defaultCenter];
                                       [reachNc postNotificationName:@"reachListener" object:self userInfo:reachUserInfo];
                                   }else{
                                       // update the UI here (and only here to the extent it depends on the json)
                                       NSString *resp =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                       NSLog(@"response : %@", resp);
                                       if ([resp isEqualToString:@"OK"]) {
                                           [userDefault setObject:@"1" forKey:@"state_indicator"];
                                           NSDictionary* reachUserInfo = @{@"reachCallback": @"reach", @"code_state" : @"1"};
                                           NSNotificationCenter* reachNc = [NSNotificationCenter defaultCenter];
                                           [reachNc postNotificationName:@"reachListener" object:self userInfo:reachUserInfo];
                                       }
                                   }
                               }];
        
        
    }else{
        [userDefault setObject:@"0" forKey:@"state_indicator"];
        NSDictionary* reachUserInfo = @{@"reachCallback": @"unreach", @"code_state" : @"0"};
        NSNotificationCenter* reachNc = [NSNotificationCenter defaultCenter];
        [reachNc postNotificationName:@"reachListener" object:self userInfo:reachUserInfo];
        [_mReachConnect stopNotifier];
        [_mReachConnect startNotifier];
       
    }
    
    [userDefault synchronize];
    
    
}

- (void) firebaseInit: (UIApplication *) application{
    
    FIROptions.defaultOptions.deepLinkURLScheme = @"co.id.BSM-Mobile.MII";
    
    // Use Firebase library to configure APIs
    [FIRApp configure];
    
    // [START set_messaging_delegate]
    [FIRMessaging messaging].delegate = self;
    // [END set_messaging_delegate]
    
    
    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        
        UNNotificationCategory* generalCategory = [UNNotificationCategory
                                                   categoryWithIdentifier:@"GENERAL"
                                                   actions:@[]
                                                   intentIdentifiers:@[]
                                                   options:UNNotificationCategoryOptionCustomDismissAction];
        // Register the notification categories.
        UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
        [center setNotificationCategories:[NSSet setWithObjects:generalCategory, nil]];
        
        center.delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [center requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             NSLog(@"%@",error);
             // ...
         }];
    }
//    else {
//        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
//        UIUserNotificationType allNotificationTypes =
//        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
//        UIUserNotificationSettings *settings =
//        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
//        [application registerUserNotificationSettings:settings];
//
//    }
    //deleting instance ID
//    [[FIRInstanceID instanceID]deleteIDWithHandler:^(NSError * _Nullable err){
//        NSLog(@"%@", err.description);
//    }];
    
    [application registerForRemoteNotifications];
    
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self selector:@selector(tokenRefreshCallback:)
//     name:kFIRInstanceIDTokenRefreshNotification object:nil ];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appInActiveForeground:) name:UIApplicationDidBecomeActiveNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appInActiveBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

#pragma mark override method hadling deeplink firebase

- (BOOL)application:(UIApplication *)application
continueUserActivity:(nonnull NSUserActivity *)userActivity
 restorationHandler:
#if defined(__IPHONE_12_0) && (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_12_0)
(nonnull void (^)(NSArray<id<UIUserActivityRestoring>> *_Nullable))restorationHandler {
#else
    (nonnull void (^)(NSArray *_Nullable))restorationHandler {
#endif  // __IPHONE_12_0
        BOOL handled = [[FIRDynamicLinks dynamicLinks] handleUniversalLink:userActivity.webpageURL
                                                                completion:^(FIRDynamicLink * _Nullable dynamicLink,
                                                                             NSError * _Nullable error) {
            // ...
            NSLog(@"link : %@", userActivity.webpageURL.absoluteString);
            NSLog(@"link parameter : %@", dynamicLink.url.absoluteString);
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *strDynamicLink = dynamicLink.url.absoluteString;
            [userDefault removeObjectForKey:@"deeplink_nodeindex"];
            [userDefault removeObjectForKey:@"param_deep_link"];

            BOOL isValid = false;
            if (strDynamicLink == nil) {
                if ([userDefault objectForKey:@"param_deep_link"] != nil){
                    strDynamicLink = [userDefault objectForKey:@"param_deep_link"];
                    isValid = true;
                }
            }else{
                [userDefault setObject:strDynamicLink forKey:@"param_deep_link"];
                [userDefault synchronize];
                isValid = true;
            }
            
            if(isValid){
                NSString *param = [strDynamicLink stringByReplacingOccurrencesOfString:@"https://syariahmandiri.co.id/" withString:@""];
                NSArray *mNode = [param componentsSeparatedByString:@"/"];
                NSDictionary* userInfo = @{@"position": @(1124),
                                           @"node_index" : mNode};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            }else{
                NSLog(@"%@", strDynamicLink);
            }
            
            
            
        }];
        return handled;
    }


- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    FIRDynamicLink *dynamicLink = [[FIRDynamicLinks dynamicLinks] dynamicLinkFromCustomSchemeURL:url];
           
    if (dynamicLink) {
      if (dynamicLink.url) {
          // Handle the deep link. For example, show the deep-linked content,
          // apply a promotional offer to the user's account or show customized onboarding view.
          // ...
          NSLog(@"dynamic link : %@", dynamicLink.url.absoluteString);
      } else {
          // Dynamic link has empty deep link. This situation will happens if
          // Firebase Dynamic Links iOS SDK tried to retrieve pending dynamic link,
          // but pending link is not available for this device/App combination.
          // At this point you may display default onboarding view.
      }
      return YES;
    }
    return NO;
}

-(void) preventUserIntefaceMode {
    if (@available(iOS 13.0, *)) {
        if ([self respondsToSelector:NSSelectorFromString(@"overrideUserInterfaceStyle")]) {
            [self setValue:@(UIUserInterfaceStyleLight) forKey:@"overrideUserInterfaceStyle"];
        }
    }
}

#pragma mark firebase delegate

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
//
//    // Print full message.
//    NSLog(@"%@", userInfo);
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
     [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//        UNMutableNotificationContent *content = [UNMutableNotificationContent new];
////        content.title =userInfo[@"title"];
////        content.body = userInfo[@"body"];
////        content.sound = [UNNotificationSound defaultSound];
//        NSURL *imageURL = [NSURL URLWithString:userInfo[@"pict_url"]];
//        NSError *error;
//        UNNotificationAttachment *icon = [UNNotificationAttachment attachmentWithIdentifier:@"image" URL:imageURL options:nil error:&error];
//        if (error)
//        {
//            NSLog(@"error while storing image attachment in notification: %@", error);
//        }
//        if (icon)
//        {
//            content.attachments = @[icon];
//        }
        
       
        
       
    }
//    mApps.applicationIconBadgeNumber = mApps.applicationIconBadgeNumber + 1;
    // Print full message.
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSInteger countNotif = [[userDefault valueForKey:@"count_notif"]integerValue];
    countNotif = countNotif + 1;
    [userDefault setValue: @(countNotif) forKey:@"count_notif"];
    [userDefault synchronize];
    
    NSLog(@"%@", userInfo);
    
    NSDictionary* mUserInfo = @{@"position": @(1117)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:mUserInfo];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"changeIcon"
     object:self];
    
    completionHandler(UIBackgroundFetchResultNewData);
}
// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSDictionary *userInfo = (NSDictionary * ) notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
     [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    //[self getNotificationUnRead];
    
    NSDictionary* mUserInfo = @{@"position": @(1117)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:mUserInfo];
    
    // Change this to your preferred presentation option
     completionHandler(UNNotificationPresentationOptionNone | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionSound);
     AudioServicesPlaySystemSoundWithCompletion(kSystemSoundID_Vibrate, ^(){
         NSLog(@"completion");
     });

}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)(void))completionHandler {

         if([response.notification.request.content.categoryIdentifier isEqualToString:@"SHOLAT_TIME"]){
             NSDictionary* userInfo = @{@"position": @(1002)};
             NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
             [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
         }else{
             NSDictionary *userInfo = response.notification.request.content.userInfo;
             NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
             if (userInfo[kGCMMessageIDKey]) {
                 NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
             }
             
             NSInteger countNotif = [[userDefault valueForKey:@"count_notif"]integerValue];
             if (countNotif > 0) {
                 countNotif = countNotif - 1;
             }
             
             [userDefault setValue: @(countNotif) forKey:@"count_notif"];
             [userDefault setValue:[userInfo valueForKey:@"msg_type"] forKey:@"msg_type"];
             [userDefault synchronize];
             
             
             NSDictionary* mUserInfo = @{@"position": @(1115), @"mid" :[userInfo valueForKey:@"mid"]};
             NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
             [nc postNotificationName:@"listenerNavigate" object:self userInfo:mUserInfo];
             // Print full message.
             NSLog(@"%@", userInfo);
         }
        completionHandler();
}

// [END ios_10_message_handling]

// [START refresh_token]
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
//    NSLog(@"FCM registration token: %@", fcmToken);
//    // Notify about received token.
//    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:
//     @"FCMToken" object:nil userInfo:dataDict];
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result, NSError * _Nullable error) {
      if (error != nil) {
        NSLog(@"Error fetching remote instance ID: %@", error);
      } else {
        NSLog(@"Remote instance ID token: %@", result.token);
        NSString* message =
          [NSString stringWithFormat:@"Remote InstanceID token: %@", result.token];
          NSLog(@"%@", message);
      }
    }];
    
    if (fcmToken != nil || ![fcmToken isEqualToString:@""]) {
        NSString *nImei = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
        NSDictionary *data = @{@"imei" : nImei, @"token" : fcmToken};
        [inboxHelper insertDataToken:data];
    }
}

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
#pragma mark Notify about received token.
//    NSDictionary *dataDict = @{@"position" : @(1116), @"token" : fcmToken};
//    [[NSNotificationCenter defaultCenter] postNotificationName:
//     @"listenerNavigate" object:nil userInfo:dataDict];
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    [userDefault setObject:[NSDictionary dictionaryWithObject:fcmToken forKey:@"token"] forKey:@"token"];
//    [userDefault synchronize];
    
    if (fcmToken != nil || ![fcmToken isEqualToString:@""]) {
        NSString *nImei = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
        NSDictionary *data = @{@"imei" : nImei, @"token" : fcmToken};
        [inboxHelper insertDataToken:data];
    }
   
}


// [END refresh_token]

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
//- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
//    NSLog(@"Received data message: %@", remoteMessage.appData);
//
//}
// [END ios_10_data_message]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs device token can be paired to
// the FCM registration token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs device token here.
     [FIRMessaging messaging].APNSToken = deviceToken;
//    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
    NSLog(@"deviceToken1 = %@",deviceToken);
    
}



- (void)reloadApp{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    if([userDef boolForKey:@"RTO"]){
        [userDef setBool:false forKey:@"RTO"];
    }else{
        [self getSessionAndKey];
    }
}

#pragma mark application resign active
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//    backgroundUpdateTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
//        [self endBackgroundUpdateTask];
//    }];
    NSUserDefaults *mUserDefault = [NSUserDefaults standardUserDefaults];
    [mUserDefault setValue:[Utility dateToday:1] forKey:@"current_date"];
    [mUserDefault synchronize];
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    
    blurView = [[UIView alloc]initWithFrame:self.window.bounds];
    visualEffectView.frame = blurView.bounds;
    [blurView addSubview:visualEffectView];
    [self.window addSubview:blurView];
}

//- (void) endBackgroundUpdateTask
//{
//    [[UIApplication sharedApplication] endBackgroundTask: backgroundUpdateTask];
//    backgroundUpdateTask = UIBackgroundTaskInvalid;
//}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSUserDefaults *mUserDefault = [NSUserDefaults standardUserDefaults];
    [mUserDefault setValue:[Utility dateToday:1] forKey:@"current_date"];
    [mUserDefault synchronize];
}

#pragma mark application enter foreground
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//    [self endBackgroundUpdateTask];
    [self handleTimerInBackground];
}

-(void) handleTimerInBackground{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([userDefault valueForKey:@"current_date"]) {
        NSDate * now = [NSDate date];
        NSString *tabPosition = [userDefault valueForKey:@"tabPos"];
        NSString *strCurrentDate = [userDefault valueForKey:@"current_date"];
        NSInteger nMaxIdle = [[userDefault valueForKey:@"config_timeout_session"]integerValue] / 1000;
        NSDate *currentDate = [Utility dateStringToDate:strCurrentDate mType:1];
        NSTimeInterval diffTime = [now timeIntervalSinceDate:currentDate];
        if (diffTime > (double) nMaxIdle) {
            NSLog(@"Timeout YOOOOO");
            if ([tabPosition isEqualToString:@"HOME"]) {
                NSDictionary* userInfo = @{@"position": @(1111)};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            }else if([tabPosition isEqualToString:@"WISDOM"]){
                NSDictionary* userInfo = @{@"actionTimer": @"TIME_OUT"};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
            }else if([tabPosition isEqualToString:@"JADWAL"]){
                NSDictionary* userInfo = @{@"actionTimer": @"TIME_OUT"};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"isViewTimer" object:self userInfo:userInfo];
                
            }else if([tabPosition isEqualToString:@"ATM"]){
                NSDictionary* userInfo = @{@"actionTimer": @"TIME_OUT"};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"ATMTimer" object:self userInfo:userInfo];
            }
            
        }else{
            if ([tabPosition isEqualToString:@"HOME"]) {
                NSDictionary* userInfo = @{@"position": @(1112)};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            }else if([tabPosition isEqualToString:@"WISDOM"]){
                NSDictionary* userInfo = @{@"actionTimer": @"START_TIMER"};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
            }else if([tabPosition isEqualToString:@"JADWAL"]){
                NSDictionary* userInfo = @{@"actionTimer": @"START_TIMER"};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"isViewTimer" object:self userInfo:userInfo];
                
            }else if([tabPosition isEqualToString:@"ATM"]){
                NSDictionary* userInfo = @{@"actionTimer": @"START_TIMER"};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"ATMTimer" object:self userInfo:userInfo];
            }
            NSLog(@"Lanjut YOOOOO");
        }
        
    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
//    [self getMainMenu];
//    // [self checkUpdate];
//    [Fabric with:@[[Crashlytics class]]];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    BOOL isValid = false;
    if (isSecurityCheckPassed()) {
        isValid = true;
    }
    
//    if (isCracked()) {
//        isValid = false;
//    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isValid forKey:@"is_jail_break"];
    [userDefault synchronize];
    
    if (!isValid) {
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        NSString *strMessage;
        if ([lang isEqualToString:@"id"]) {
            strMessage = @"RC99 Rooted Device Detected: \"Kami mendeteksi anda menggunakan perangkat yang dimodifikasi (rooted/jailbreak). Dengan demikian, anda tidak dapat menggunakan BSI Mobile. Harap pulihkan perangkat anda ke pengaturan aslinya. Info lebih lanjut silahkan menghubungi Call Center.\"";
        }else{
            strMessage = @"RC99 Rooted Device Detected: \"We detected you are using a modified device (rooted / jailbreak). Therefore, you cannot use BSI Mobile. Please restore your device to its original settings. For further information please contact Center.\"";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 999;
        [alert show];
    }
    
    if(blurView != nil){
        [blurView removeFromSuperview];
        blurView = nil;
    }
    [_mReachConnect startNotifier];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    if ([userDefault objectForKey:@"customer_id"]) {
    if ([NSUserdefaultsAes getValueForKey:@"customer_id"]) {
        [userDefault setValue:nil forKey:OBJ_FINANCE];
        [userDefault setValue:nil forKey:OBJ_SPESIAL];
        [userDefault setValue:nil forKey:OBJ_ALL_ACCT];
        [userDefault setValue:nil forKey:OBJ_ROLE_FINANCE];
        [userDefault setValue:nil forKey:OBJ_ROLE_SPESIAL];
        [userDefault setValue:nil forKey:OBJ_SEARCH_FEATURE];
        [userDefault synchronize];
    }
}

-(void)tokenRefreshCallback:(NSNotification *)notification{
     NSLog(@"instanceId_notification=>%@",[notification object]);
//    NSString * refreshedToken = [[FIRInstanceID instanceID]token];
//    NSLog(@"refresh Token is :  %@",refreshedToken);
   
    InstanceID = [NSString stringWithFormat:@"%@",[notification object]];
    NSString *nImei = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    NSDictionary *data = @{@"imei" : nImei, @"token" : InstanceID};
    [inboxHelper insertDataToken:data];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[notification object] forKey:@"token"];
    [userDefault synchronize];
    
//    [self connectToFireBase];

}
//
//-(void)appInActiveForeground:(UIApplication *) application{
//    NSLog(@"Did Become Active");
//    [self connectToFireBase];
//}
//-(void) appInActiveBackground:(UIApplication *) application{
//    NSLog(@"Did Become Background");
//    [self connectToFireBase];
//}
//
-(void)connectToFireBase
{
//    [[FIRMessaging messaging] setShouldEstablishDirectChannel:YES];
//    NSLog((@"Connected To Firebase"));
//    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
//        if (error != nil) {
//            NSLog(@"Unable to connect to FCM. %@", error);
//        } else {
//
//            NSLog(@"InstanceID_connectToFcm = %@", InstanceID);
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    NSLog(@"instanceId_tokenRefreshNotification22=>%@",[[FIRInstanceID instanceID] token]);
//
//                });
//            });
//
//
//        }
//    }];

}

- (void)login{
    //    if([defaults objectForKey:@"customer_id"]){
    //        DLog(@"login into app");
    //        Connection *conn = [[Connection alloc]initWithDelegate:self];
    //        [conn sendPostParmUrl:@"request_type=login,customer_id" needLoading:true textLoading:@"Log into the application" encrypted:true banking:true continueLoading:true];
    //    }else{
    [self getMainMenu];
    //    }
}

- (void)getSessionAndKey{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=key" needLoading:true textLoading:@"Initialization App." encrypted:false banking:false continueLoading:true];
}

- (void)writeStringToFile:(NSString*)aString {
    
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = self.menu_file;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    
    [[aString dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}


- (NSString*)readStringFromFile {
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = self.menu_file;
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    return [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
}

- (BOOL)downloadMenu{
    DLog(@"DOWNLOD MENU START");
     NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    self.this_version = @"1";
    
    //NSString *versionText = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *versionText = @VERSION_NAME;
    NSString* fileName;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"en"]) {
        fileName = @"menu_version_en.txt";
    }
    else {
        fileName = @"menu_version_id.txt";
    }
    
    //NSString* fileName = @"menu_version.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
 
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fileAtPath];
    
    if (fileExists) {
      self.this_version = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
        NSLog(@"readStringFromFile: %@", self.this_version);
    }
    else {
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
      [[self.this_version dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
    //NSURL *TheUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@version_ios.html",API_URL]];
    NSURL *TheUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@version_ios.html?vn=%@",API_URL,versionText]];
//    NSString *response = [NSString stringWithContentsOfURL:TheUrl
//                                                 encoding:NSASCIIStringEncoding
//                                                    error:nil];
    
//
    NSLog(@"URL : %@", TheUrl);
    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@version_ios.html?vn=%@",API_URL,versionText]] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:180.0];
    
#pragma mark header X-SIGNATURE
//    NSString *xSignature = [NSString stringWithFormat:@"%@|%@",clientId,[Utility dateToday]];
//
//    [request setValue:[Utility dateToday] forHTTPHeaderField:@"X-TIMESTAMP"];
//    [request setValue:clientId forHTTPHeaderField:@"X-Key"];
//    [request setValue:[Utility calculateHMAC:xSignature key:clientSecret] forHTTPHeaderField:@"X-SIGNATURE"];
    NSMutableURLRequest *request = [Utility BSMHeader:[NSString stringWithFormat:@"%@version_ios.html?vn=%@",API_URL,versionText]];
    [request setHTTPMethod:@"GET"];
    
    @try {
        NSError *error;
        NSURLResponse *urlResponse;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        
        NSLog(@"File Version: %@", self.this_version);
        
        
        if (error == nil) {
            NSString *response = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
            NSLog(@"Server Version: %@", response);
            
            if (response != nil) {
                if (![response isEqualToString:@""]) {
                    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    if ([response isEqualToString:self.this_version]) {
                        return true;
                    }else {
                        self.this_version = response;
                        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
                            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
                        }
                        [[self.this_version dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
                        return false;
                    }
                }
            }
            
            DLog(@"DOWNLOAD MENU DONE");
        }else{
            NSLog(@"%@", error);
             [_mReachConnect stopNotifier];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[error localizedDescription] delegate:self cancelButtonTitle:lang(@"TRY") otherButtonTitles:nil, nil];
            alert.tag = 404;
            [alert show];
        }
        
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
        return false;
    }
    
    return false;
    
}

- (void)getMainMenu{
    //NSString *versionText = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    DLog(@"GET MAIN MENU");
    NSString *versionText = @VERSION_NAME;
    NSArray *listLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* menu_lang = @"id";
    self.menu_file = @"menu_ios_id.html";
    
    if ([[listLanguage objectAtIndex:0] isEqualToString:@"en"]) {
        menu_lang = @"en";
        self.menu_file = @"menu_ios_en.html";
    }
    
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:self.menu_file];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fileAtPath];
    
    if(fileExists && [self downloadMenu]) {
        NSArray *jsonObject = [NSJSONSerialization
                               JSONObjectWithData:[NSData dataWithContentsOfFile:fileAtPath]
                               options:kNilOptions
                               error:nil];
        if(jsonObject){
            [[DataManager sharedManager]setListMenu:jsonObject];
        }
        [[[DataManager sharedManager]continueLoading]dismissWithClickedButtonIndex:0 animated:true];
        [DataManager sharedManager].continueLoading  = nil;
        
       // [self getMenuIdx];
        
    } else {
        NSString *url =[NSString stringWithFormat:@"%@main_menu_ios.html?language=%@&vn=%@",API_URL,[listLanguage objectAtIndex:0],versionText];
        
        #pragma mark header X-SIGNATURE
//        NSString *xSignature = [NSString stringWithFormat:@"%@|%@",clientId,[Utility dateToday]];
//
//        [request setValue:[Utility dateToday] forHTTPHeaderField:@"X-TIMESTAMP"];
//        [request setValue:clientId forHTTPHeaderField:@"X-Key"];
//        [request setValue:[Utility calculateHMAC:xSignature key:clientSecret] forHTTPHeaderField:@"X-SIGNATURE"];
        NSMutableURLRequest *request = [Utility BSMHeader:url];
        [request setHTTPMethod:@"GET"];

        [DataManager sharedManager].continueLoading = [[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:lang(@"INIT") delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
        
        UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
        [viewLoading setBackgroundColor:[UIColor clearColor]];
        UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [loadingView setColor:[UIColor blackColor]];
        [loadingView startAnimating];
        [viewLoading addSubview:loadingView];
        
        [[DataManager sharedManager].continueLoading  setContentView:viewLoading];
        [[DataManager sharedManager].continueLoading  show];
        
        @try {
            
            
            NSError *error;
            NSURLResponse *urlResponse;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
            
            
            if(error == nil){
                NSString *response = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                
                NSArray *jsonObject = [NSJSONSerialization
                                       JSONObjectWithData:urlData
                                       options:kNilOptions
                                       error:&error];
                
                [self writeStringToFile:response];
                [[DataManager sharedManager]setListMenu:jsonObject];
               
                DLog(@"GET MAIN MENU DONE");

            }else{
//                NSLog(@"%@", error);
                 [_mReachConnect stopNotifier];
                NSLog(@"%@", [error localizedDescription]);
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[error localizedDescription] delegate:self cancelButtonTitle:lang(@"TRY") otherButtonTitles:nil, nil];
                alert.tag = 404;
                [alert show];
            }
            [[[DataManager sharedManager]continueLoading]dismissWithClickedButtonIndex:0 animated:true];
            [DataManager sharedManager].continueLoading  = nil;
            
        } @catch (NSError *exception) {
            NSLog(@"%@", exception);
        }
//        [self getMenuIdx];
    }
    
    // [_splashImageView removeFromSuperview];
}

- (void)checkUpdate{
    //NSString *versionText = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    DLog(@"CEK UPDATE");
    NSString *versionText = @VERSION_NAME;
    //NSString *url =[NSString stringWithFormat:@"%@version_ios_update.html",API_URL];
    NSString *url =[NSString stringWithFormat:@"%@version_ios_update.html?vn=%@",API_URL,versionText];
//    NSLog(@"---------------- %@", url);
//    [DataManager sharedManager].continueLoading = [[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:lang(@"INIT") delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    
//    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
//    [viewLoading setBackgroundColor:[UIColor clearColor]];
//    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    [loadingView setColor:[UIColor blackColor]];
//    [loadingView startAnimating];
//    [viewLoading addSubview:loadingView];
//
//    [[DataManager sharedManager].continueLoading setContentView:viewLoading];
//    [[DataManager sharedManager].continueLoading show];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                
                if (error == nil){
                NSDictionary *jsonObject = [NSJSONSerialization
                                            JSONObjectWithData:data
                                            options:kNilOptions
                                            error:&error];
                if(jsonObject) {
                    //                    NSString *html = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSString *beVersion = [jsonObject objectForKey:@"version.code"];
                    //                    NSString *beVersion = @"5.2.29";
                    NSString *version =  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
                    NSString *vClear = [version
                                        stringByReplacingOccurrencesOfString:@"." withString:@""];
                    NSString *beClear = [beVersion
                                         stringByReplacingOccurrencesOfString:@"." withString:@""];
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                    NSString *msg = @"";
                    if([lang isEqualToString:@"id"]){
                        msg = @"Versi baru tersedia. Perbarui sekarang?";
                    } else {
                        msg = @"New version available. Update now?";
                    }
                
                    if ([beClear floatValue] > [vClear floatValue]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [userDefault setObject:[jsonObject objectForKey:@"url.appstore"] forKey:@"path"];
                            UIAlertView *al = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [al show];
                            al.tag = 322;
                        });
                    }
                    DLog(@"CEK UPDATE DONE");

                }
                }
//                    } else {
//                        [self.splashImageView removeFromSuperview];
//                    }
//                } else {
//                     [self.splashImageView removeFromSuperview];
//                }
            }] resume];
}

-(void)getMenuIdx {
    
    NSString *url =[NSString stringWithFormat:@"%@menu_index_ios.html?vn=%@",API_URL,@VERSION_NAME];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
        if (error == nil){
        NSDictionary *jsonObject = [NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:kNilOptions
                                    error:&error];
            if(jsonObject) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                for(NSString *key in jsonObject.allKeys){
                    [userDefault setValue:[jsonObject valueForKey:key] forKey:key];
                    [userDefault synchronize];
                }
            }
            
        }
    }]resume];
}

#pragma mark - Connection Delegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    //    if([requestType isEqualToString:@"key"]){
    //        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
    //            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //            [defaults setObject:[jsonObject valueForKey:@"publickey"] forKey:@"publickey"];
    //            [defaults setObject:[jsonObject valueForKey:@"session_id"] forKey:@"session_id"];
    //            [defaults synchronize];
    //            [self login];
    //        }else{
    //            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops." message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //            [alert show];
    //              [[[DataManager sharedManager]continueLoading]dismissWithClickedButtonIndex:0 animated:true];
    ////            exit(0);
    //        }
    //    }else if([requestType isEqualToString:@"login"]){
    //        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
    //            [defaults setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
    //            [defaults synchronize];
    //            [self getMainMenu];
    //        }else{
    //            [self getSessionAndKey];
    //        }
    //    }else{
    //        if([jsonObject isKindOfClass:[NSDictionary class]]){ //check if session invalid
    //            [self getSessionAndKey];
    //        }else{
    if (![requestType isEqualToString:@"check_notif"]) {
        [[DataManager sharedManager]setListMenu:jsonObject];
        
    }
    
    //        }
    //    }
}
- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    alert.tag = 123;
    [[[DataManager sharedManager]continueLoading]dismissWithClickedButtonIndex:0 animated:true];
    [DataManager sharedManager].continueLoading  = nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 123) {
        exit(0);
    } else if (alertView.tag == 322) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *path = [userDefault objectForKey:@"path"];
        [userDefault removeObjectForKey:@"path"];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:path]];
        exit(0);
    }else if(alertView.tag == 404){
//        [_mReachConnect startNotifier];
         exit(0);
    }else if (alertView.tag == 999){
        exit(0);
    }
}

-(void) getConfigApp{
    DLog(@"GET CONFIG APP");
    NSMutableURLRequest *request = [Utility BSMHeader:[NSString stringWithFormat:@"%@config.html",API_URL]];
    [request setHTTPMethod:@"GET"];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *menuAcceptRecentlyDef = @"00013,00017,00018,00025,00027,00028,00044,00049,00065,00066";
    @try {
        NSError *error;
        NSURLResponse *urlResponse;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if(error == nil){
            NSDictionary *mJsonObject = [NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&error];
            if(mJsonObject){
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.registration"] forKey:@"config_registration"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.onboarding"] forKey:@"config_onboarding"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.indicator"] forKey:@"config_indicator"];
//                [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
                //config.banner2.url config.banner2.link config.enable.banner
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner.url"] forKey:@"config_banner_url"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.type"] forKey:@"config_banner2_type"];

                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2a.link"] forKey:@"config_banner2a_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2a.url"] forKey:@"config_banner2a_url"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2b.link"] forKey:@"config_banner2b_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2b.url"] forKey:@"config_banner2b_url"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.url"] forKey:@"config_webchat_url"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.link"] forKey:@"config_webchat_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.clientid"] forKey:@"config_webchat_clientid"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.clientsecret"] forKey:@"config_webchat_clientsecret"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.trx_type.order"] forKey:@"config_trxtype_order"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.sms.longnumber"] forKey:@"config_sms_longnumber"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.source.activation"] forKey:@"config_source_activation"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.source.activation2"] forKey:@"config_source_activation2"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.sfeature.caption.id"] forKey:@"config_sfeature_captionid"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.sfeature.caption.en"] forKey:@"config_sfeature_captionen"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.prefix.otpcard"] forKey:@"config_prefix_otpcard"];                
                    
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.activationfr"] forKey:@"config_enable_activationfr"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.activationfr.versionvalue"] forKey:@"config_activationfr_versionvalue"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.helpdesk.title.id"] forKey:@"config.helpdesk.title.id"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.helpdesk.title.en"] forKey:@"config.helpdesk.title.en"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2"] forKey:@"config_banner2"];

                NSString *conf = [userDefault objectForKey:@"config_banner2"];
                if(conf != nil){
                    NSArray *getData = [NSJSONSerialization JSONObjectWithData:[conf dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
//                    NSArray *ver = [@VERSION_NAME componentsSeparatedByString:@"."];
//                    double versionNumber = [ver[1]doubleValue];
//
//                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc]init];
//                    for(NSDictionary *data in getData){
//                        NSArray *verB = [[data objectForKey:@"version"] componentsSeparatedByString:@"."];
//                        [newDict addEntriesFromDictionary:@{verB[1]: data}];
//                    }
                    
                    double versionNumber = [@VERSION_VALUE doubleValue];
                    NSMutableArray *arrs = [[NSMutableArray alloc]init];
                    for(NSDictionary *data in getData){
                        NSMutableDictionary *newDict = [[NSMutableDictionary alloc]init];
                        [newDict addEntriesFromDictionary:@{@"version":[[data objectForKey:@"version"] stringByReplacingOccurrencesOfString:@"." withString:@""], @"data": data}];
                        [arrs addObject:newDict];
                    }
                    for(int i = (int)arrs.count-1; i > 0 ; i--){
//                        if([newDict objectForKey:[NSString stringWithFormat:@"%d",i]] != nil){
                        if([[[arrs objectAtIndex:i]objectForKey:@"version"]doubleValue] <= versionNumber){
                            NSDictionary *data = [[arrs objectAtIndex:i]objectForKey:@"data"];
                            [userDefault setObject:[data valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
                            [userDefault setObject:[data valueForKey:@"config.banner2.type"] forKey:@"config_banner2_type"];

                            [userDefault setObject:[data valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
                            [userDefault setObject:[data valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];

                            [userDefault setObject:[data valueForKey:@"config.banner2a.link"] forKey:@"config_banner2a_link"];
                            [userDefault setObject:[data valueForKey:@"config.banner2a.url"] forKey:@"config_banner2a_url"];

                            [userDefault setObject:[data valueForKey:@"config.banner2b.link"] forKey:@"config_banner2b_link"];
                            [userDefault setObject:[data valueForKey:@"config.banner2b.url"] forKey:@"config_banner2b_url"];
                            break;
                        }
//                        }else{
//                        }
                    }
//                    for(int i = versionNumber; i > 0; i--){
//                        if([newDict objectForKey:[NSString stringWithFormat:@"%d",i]] != nil){
//                            NSDictionary *data = [newDict objectForKey:[NSString stringWithFormat:@"%d",i]];
//                            [userDefault setObject:[data valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
//                            [userDefault setObject:[data valueForKey:@"config.banner2.type"] forKey:@"config_banner2_type"];
//
//                            [userDefault setObject:[data valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
//                            [userDefault setObject:[data valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];
//
//                            [userDefault setObject:[data valueForKey:@"config.banner2a.link"] forKey:@"config_banner2a_link"];
//                            [userDefault setObject:[data valueForKey:@"config.banner2a.url"] forKey:@"config_banner2a_url"];
//
//                            [userDefault setObject:[data valueForKey:@"config.banner2b.link"] forKey:@"config_banner2b_link"];
//                            [userDefault setObject:[data valueForKey:@"config.banner2b.url"] forKey:@"config_banner2b_url"];
//                            break;
//                        }
//                    }
                }

                if ([[mJsonObject valueForKey:@"config.mutation.range"] integerValue] != 0) {
                     [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
                }else{
                    [userDefault setValue:@"15" forKey:@"config_mutation_range"];
                }
                
                if ([[mJsonObject valueForKey:@"config.timeout.session"] integerValue] != 0) {
                     [userDefault setObject:[mJsonObject valueForKey:@"config.timeout.session"] forKey:@"config_timeout_session"];
                }else{
                    [userDefault setValue:@"180" forKey:@"config_timeout_session"];
                }
               
                [userDefault setObject:[mJsonObject valueForKey:@"config.act.iccid"] forKey:@"config.act.iccid"];
                
                if ([[mJsonObject valueForKey:@"config.allow.recently"] isEqualToString:@""] || [mJsonObject valueForKey:@"config.allow.recently"] == nil) {
                    [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
                }else{
                    [userDefault setObject:[mJsonObject valueForKey:@"config.allow.recently"] forKey:@"config.allow.recently"];
                }
                
                if ([mJsonObject objectForKey:@"config.enable.financing"] != 0 || [mJsonObject objectForKey:@"config.enable.financing"] != nil){
                    [userDefault setObject:[mJsonObject valueForKey:@"config.enable.financing"] forKey:@"config.enable.financing"];
                }
                
                DLog(@"GET CONFIG APP DONE");
            }
        }else{
            NSLog(@"%@", error);
            [userDefault setObject:@(15) forKey:@"config_mutation_range"];
            [userDefault setObject:@(180) forKey:@"config_timeout_session"];
            [userDefault setObject:@(1) forKey:@"config.act.iccid"];
            [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
            [userDefault setObject:@(0) forKey:@"config_onboarding"];
			[userDefault setObject:@(0) forKey:@"config.enable.financing"];
        }
        
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
        [userDefault setObject:@(15) forKey:@"config_mutation_range"];
        [userDefault setObject:@(180) forKey:@"config_timeout_session"];
        [userDefault setObject:@(1) forKey:@"config.act.iccid"];
        [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
        [userDefault setObject:@(0) forKey:@"config_onboarding"];
		[userDefault setObject:@(0) forKey:@"config.enable.financing"];
    }
    [userDefault synchronize];
}





@end
