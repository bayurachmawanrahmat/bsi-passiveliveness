//
//  GagalVerifikasiController.h
//  BSM-Mobile
//
//  Created by ARS on 26/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrasiSelesaiController.h"
#import "Rekening.h"

NS_ASSUME_NONNULL_BEGIN

@interface GagalVerifikasiController : RegistrasiSelesaiController

- (instancetype)initForRejectVideoWithRekening:(Rekening *)rekening;
- (instancetype)initForRejectedWithRekening:(Rekening *)rekening;

@end

NS_ASSUME_NONNULL_END
