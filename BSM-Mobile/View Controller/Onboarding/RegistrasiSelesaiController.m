//
//  RegistrasiSelesaiController.m
//  BSM-Mobile
//
//  Created by ARS on 25/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "RegistrasiSelesaiController.h"

@interface RegistrasiSelesaiController ()

@end

@implementation RegistrasiSelesaiController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    mainButtonHeight = 50.0;
    mainButtonMargin = 30.0;
    
    if([self.parentViewController isKindOfClass:OnboardingTemplate01ViewController.class])
    {
        self.parentController = (OnboardingTemplate01ViewController *)self.parentViewController;
        self.parentController.delegate = self;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
