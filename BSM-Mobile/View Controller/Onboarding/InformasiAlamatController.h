//
//  InformasiAlamatController.h
//  BSM-Mobile
//
//  Created by ARS on 17/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "UIDropdown.h"
#import "UIDropdownSearch.h"
#import "OnboardingRootController.h"
#import "OnboardingButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiAlamatController : OnboardingRootController <UINavigationControllerDelegate, UITextFieldDelegate, UIDropdownSearchDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *fieldProvinsi;
@property (weak, nonatomic) IBOutlet UITextField *fieldKota;
@property (weak, nonatomic) IBOutlet UITextField *fieldKecamatan;
@property (weak, nonatomic) IBOutlet UITextField *fieldKelurahan;
@property (weak, nonatomic) IBOutlet UITextField *fieldAlamat;
@property (weak, nonatomic) IBOutlet UITextField *fieldRT;
@property (weak, nonatomic) IBOutlet UITextField *fieldRW;
@property (weak, nonatomic) IBOutlet UITextField *fieldKodePos;
@property (weak, nonatomic) IBOutlet UIButton *checkAlamatBeda;

@property (weak, nonatomic) IBOutlet UITextField *fieldProvinsiDiff;
@property (weak, nonatomic) IBOutlet UITextField *fieldKotaDiff;
@property (weak, nonatomic) IBOutlet UITextField *fieldKecamatanDiff;
@property (weak, nonatomic) IBOutlet UITextField *fieldKelurahanDiff;
@property (weak, nonatomic) IBOutlet UITextField *fieldAlamatDiff;
@property (weak, nonatomic) IBOutlet UITextField *fieldRTDiff;
@property (weak, nonatomic) IBOutlet UITextField *fieldRWDiff;
@property (weak, nonatomic) IBOutlet UITextField *fieldKodePosDiff;

@property (weak, nonatomic) IBOutlet UIView *viewAlamatBeda;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopViewAlamatBeda;

- (IBAction)hitBtnCheckAlamatBeda:(id)sender;

- (IBAction)hitBtnNext:(id)sender;

@end

NS_ASSUME_NONNULL_END
