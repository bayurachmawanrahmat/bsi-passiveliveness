//
//  CreatePinOnboardingController.h
//  BSM-Mobile
//
//  Created by ARS on 14/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CreatePinOnboardingController : OnboardingRootController <NSURLSessionTaskDelegate, NSURLSessionDataDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *viewCreatePIN;
@property (weak, nonatomic) IBOutlet UITextField *fieldPin;
@property (weak, nonatomic) IBOutlet UITextField *fieldConfirmPin;
- (IBAction)hitBtnNext:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonEyePin;
@property (weak, nonatomic) IBOutlet UIButton *buttonEyeConfirmPin;
@property (weak, nonatomic) IBOutlet UIButton *btnPinToggle;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmPinToggle;
- (IBAction)hitTogglePin:(id)sender;
- (IBAction)hitToggleConfirmPin:(id)sender;


@end

NS_ASSUME_NONNULL_END
