//
//  DetailCabangViewController.m
//  BSM-Mobile
//
//  Created by ARS on 30/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "DetailCabangViewController.h"

@interface DetailCabangViewController ()
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
- (IBAction)backButtonTappedHandler:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property NSDictionary *data;

@end

@implementation DetailCabangViewController

- (instancetype)initWithData:(NSDictionary *)data
{
    self = [super init];
    if (self) {
        self->_data = data;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.navigationBar.topItem setTitle:[self.data valueForKey:@"nama"]];
    
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
    
    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake([[self.data valueForKey:@"latitude"]floatValue], [[self.data valueForKey:@"longitude"]floatValue]);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 3000, 3000)];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[self.data valueForKey:@"latitude"]floatValue], [[self.data valueForKey:@"longitude"]floatValue]);
    point.coordinate = coordinate;
    point.title = [self.data valueForKey:@"nama"];
    point.subtitle = [self.data valueForKey:@"alamat"];
    
    [self.mapView setRegion:adjustedRegion animated:YES];
    [self.mapView addAnnotation:point];
}

- (IBAction)backButtonTappedHandler:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
