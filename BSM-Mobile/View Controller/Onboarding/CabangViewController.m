//
//  CabangViewController.m
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CabangViewController.h"
#import "CabangTableViewCell.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@interface CabangViewController ()<ConnectionDelegate,CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate>{
    NSArray *listData;
    NSArray *listBranch;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;
    double deviceLat;
    double deviceLng;
    dispatch_group_t referenceGroup;
    NSDictionary *apiResult;
    NSMutableDictionary *listBranchResult;
    NSString *apiMessage;
    UIRefreshControl *refreshControl;
}

@property (weak, nonatomic) IBOutlet UITableView *tableData;

@end

@implementation CabangViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.showDetailOnSelectRow = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableData.dataSource = self;
    self.tableData.delegate = self;
    
    // Here's where we create our UISearchController
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    [self.searchController.searchBar sizeToFit];
    self.searchController.obscuresBackgroundDuringPresentation = NO;
    
    
    // Add the UISearchBar to the top header of the table view
    _tableData.tableHeaderView = self.searchController.searchBar;
    [self.searchController.searchBar setShowsCancelButton:YES];
    
    self.selectedData = [[NSDictionary alloc] init];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(currentLocationIdentifier) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [self.tableData setRefreshControl:refreshControl];
    } else {
        [self.tableData addSubview:refreshControl];
    }
    
    [self currentLocationIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)currentLocationIdentifier
{
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else {
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
    }
    if(refreshControl) [refreshControl endRefreshing];
}

-(double) distanceCallculator:(double)lat1 long1:(double)lng1 lat2:(double)lat2 long2:(double)lng2 {
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:lat1 longitude:lng1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:lat2 longitude:lng2];
    double dMeters = [loc1 distanceFromLocation:loc2];
    return dMeters;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    deviceLng =currentLocation.coordinate.longitude;
    deviceLat = currentLocation.coordinate.latitude;
    
//    lat = -6.914744;
//    lng = 107.609810;
    
    [self retrieveBranches];
}

- (void) retrieveBranches
{
//    NSString *paramData = [NSString stringWithFormat:@"request_type=list_atm_branch,latitude=%f,longitude=%f",  lat, lng];
//    Connection *conn = [[Connection alloc]initWithDelegate:self];
//    [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
//
    
    listBranchResult = [[NSMutableDictionary alloc] init];
    dispatch_group_t dispatchBranch = dispatch_group_create();
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/master/branch/list?lng=%f&lat=%f", lng, lat] dispatchGroup:dispatchBranch returnData:listBranchResult];
    dispatch_group_notify(dispatchBranch, dispatch_get_main_queue(), ^{
        NSLog(@"branch result : %@", self->listBranchResult);
        
        if ([[self->listBranchResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
            NSArray *branch = [self->listBranchResult objectForKey:@"data"];
            NSMutableArray *tempBranch = [[NSMutableArray alloc] init];
            for (NSDictionary *data in branch){
                NSMutableDictionary *dictX = [NSMutableDictionary new];
                if ([data objectForKey:@"branch_address"]){
                    [dictX setValue:[data objectForKey:@"branch_address"] forKey:@"address"];
                } else {
                    [dictX setValue: @"" forKey:@"address"];
                }
                [dictX setValue:[data objectForKey:@"alamat"] forKey:@"alamat"];
                [dictX setValue:[data objectForKey:@"isoallowed"] forKey:@"isoallowed"];
                [dictX setValue:[data objectForKey:@"kabkotaid"] forKey:@"kabkotaid"];
                [dictX setValue:[data objectForKey:@"provinsiid"] forKey:@"provinsiid"];
                [dictX setValue:[data objectForKey:@"nama"] forKey:@"nama"];
                [dictX setValue:[data objectForKey:@"kode"] forKey:@"kode"];
                [dictX setValue:[data objectForKey:@"nama"] forKey:@"name"];
                [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
                [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
                [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
                [dictX setValue:[NSNumber numberWithDouble: [[NSString stringWithFormat:@"%@", [data objectForKey:@"distance"]] doubleValue]] forKey:@"distance"];
                [tempBranch addObject:dictX];
            }
    
            if ([tempBranch count] > 0)
            {
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
                self->listBranch = [tempBranch sortedArrayUsingDescriptors:@[descriptor]];
                self->listData = self->listBranch;
                [self.tableData reloadData];
            }
            else
            {
                NSString *msg = NSLocalizedString(@"ERR_CABANG", @"ERR: Failed to retrieve branch nearby");
                [self showAlertErrorWithTitle:lang(@"INFO") andMessage:msg sender:self];
            }
        }
        else {
            NSString *msg = NSLocalizedString(@"ERR_CABANG", @"ERR: Failed to retrieve branch nearby");
            [self showAlertErrorWithTitle:@"Ooops" andMessage:msg sender:self];
        }
    });
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"cellLocation";
    CabangTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CabangTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    
//    UIImageView *imgCell = (UIImageView *)[cell viewWithTag:77];
//    UIImage *iconTable =[UIImage imageNamed:@"02.png"];
//    [imgCell setImage:[self imageWithImage:iconTable scaledToSize:imgCell.frame.size]];
    
    cell.name.text = [data objectForKey:@"name"];
    cell.address.text = [data objectForKey:@"address"];
    double distance =  [[data objectForKey:@"distance"] doubleValue];
//    if (distance < 1000) {
//        cell.distance.text = [NSString stringWithFormat:@"%.2f m", distance];
//    } else {
//        cell.distance.text = [NSString stringWithFormat:@"%.2f km", (distance / 1000)];
//    }
    cell.distance.text = [NSString stringWithFormat:@"%.2f km", distance];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedData = [listData objectAtIndex:indexPath.row];
    [self->_delegate cabangViewController:self didSelectRowWithData:self.selectedData];
    if([self.searchController isActive])
        [self.searchController setActive:NO];
    if(!self->_showDetailOnSelectRow)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
    {
        DetailCabangViewController *vc = [[DetailCabangViewController alloc] initWithData:self.selectedData];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:YES completion:nil];
    }
    
//    NSDictionary *rawCabang = [listData objectAtIndex:indexPath.row];
//    NSString *gmaps = [rawCabang objectForKey:@"google_map"];
//    [self showLoading];
//    referenceGroup = dispatch_group_create();
//    [self retrieveCabangByGmaps:gmaps];
//
//    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
//        [self hideLoading];
//        if(self->apiResult)
//        {
//            self.selectedData = self->apiResult;
//            NSLog(@"selectedData: %@", self.selectedData);
//            [self->_delegate cabangViewController:self didSelectRowWithData:self.selectedData];
//            if([self.searchController isActive])
//                [self.searchController setActive:NO];
//            if(!self->_showDetailOnSelectRow)
//                [self dismissViewControllerAnimated:YES completion:nil];
//            else
//            {
//                DetailCabangViewController *vc = [[DetailCabangViewController alloc] initWithData:self.selectedData];
//                vc.modalPresentationStyle = UIModalPresentationFullScreen;
//                [self presentViewController:vc animated:YES completion:nil];
//            }
//        }
//        else
//        {
//            [self showAlertErrorWithTitle:@"Ooops" andMessage:@"Error Test" sender:self];
//        }
//    });
}

- (void) retrieveCabangByGmaps:(NSString *)gmaps
{
    dispatch_group_enter(referenceGroup);
    NSString *url =[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:@"api/master/branch/retrieveGmaps"]];
    NSString *token = [CustomerOnboardingData getToken];
    NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithString:url];
    [urlComponents setQueryItems:[[NSArray alloc] initWithObjects:[[NSURLQueryItem alloc] initWithName:@"latlng" value:gmaps], nil]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[urlComponents URL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        self->apiResult = nil;
        if(!error)
        {
            NSDictionary *jsonObject;
            if(ONBOARDING_SAFE_MODE)
            {
                NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSData *decData = [Encryptor AESDecryptData:encString];
                jsonObject = [NSJSONSerialization JSONObjectWithData:decData options:kNilOptions error:&error];
            }
            else
                jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if((long)[httpResponse statusCode] == 200)
                self->apiResult = jsonObject;
            else
                self->apiMessage = NSLocalizedString(@"ERR_DETAIL_CABANG", @"ERR: Failed to retrieve branch detail information");
        }
        else
            self->apiMessage = NSLocalizedString(@"ERR_DETAIL_CABANG", @"ERR: Failed to retrieve branch detail information");
        
        dispatch_group_leave(self->referenceGroup);
    }] resume];
}

#pragma mark - Connection
-(void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([jsonObject isKindOfClass:[NSDictionary class]])
    {
        NSArray *branch = [jsonObject objectForKey:@"branch"];
        NSMutableArray *tempBranch = [[NSMutableArray alloc] init];
        for (NSDictionary *data in branch){
            NSMutableDictionary *dictX = [NSMutableDictionary new];
            if ([data objectForKey:@"branch_address"]){
                [dictX setValue:[data objectForKey:@"branch_address"] forKey:@"address"];
            } else {
                [dictX setValue: @"" forKey:@"address"];
            }
            [dictX setValue:[data objectForKey:@"branch_name"] forKey:@"name"];
            [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
            [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
            [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
            double distance = [self distanceCallculator:lat long1:lng lat2:[[data objectForKey:@"latitude"] doubleValue] long2:[[data objectForKey:@"longitude"] doubleValue]];
            [dictX setValue:[NSNumber numberWithDouble: distance] forKey:@"distance"];
            [tempBranch addObject:dictX];
        }
        
        if ([tempBranch count] > 0)
        {
            NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
            listBranch = [tempBranch sortedArrayUsingDescriptors:@[descriptor]];
            listData = listBranch;
            [self.tableData reloadData];
        }
        else
        {
            NSString *msg = NSLocalizedString(@"ERR_CABANG", @"ERR: Failed to retrieve branch nearby");
            [self showAlertErrorWithTitle:lang(@"INFO") andMessage:msg sender:self];
        }
    }
    else
    {
        [self showAlertErrorWithTitle:lang(@"INFO") andMessage:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]]  sender:self];
    }
}

-(void)errorLoadData:(NSError *)error{
    [self showAlertErrorWithTitle:lang(@"INFO") andMessage:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] sender:self];
}

- (void)reloadApp{
    DataManager *dataManager;
    [dataManager resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

// When the user types in the search bar, this method gets called.
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController
{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchString = searchBar.text;
    [self getLatLongFromString:searchString];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) getLatLongFromString:(NSString *)addressStr
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:addressStr completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        for (CLPlacemark *placemark in placemarks) {
            self->lat = placemark.location.coordinate.latitude;
            self->lng = placemark.location.coordinate.longitude;
            
            [self retrieveBranches];
        }
    }];
}

@end
