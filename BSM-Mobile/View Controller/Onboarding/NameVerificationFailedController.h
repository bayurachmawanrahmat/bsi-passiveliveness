//
//  NameVerificationFailedController.h
//  BSM-Mobile
//
//  Created by ARS on 30/08/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AddPopupNameFailedListener;

@interface NameVerificationFailedController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewController;
- (IBAction)btnNext:(id)sender;
@property (nonatomic, weak) id<AddPopupNameFailedListener> delegate;

@end

@protocol AddPopupNameFailedListener <NSObject>
    - (void)onNextNameFailed;
@end

NS_ASSUME_NONNULL_END
