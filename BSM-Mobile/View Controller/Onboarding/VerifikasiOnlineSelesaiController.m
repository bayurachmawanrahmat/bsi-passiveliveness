//
//  VerifikasiOnlineSelesaiController.m
//  BSM-Mobile
//
//  Created by ARS on 17/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "VerifikasiOnlineSelesaiController.h"
#import "CustomerOnboardingData.h"
#import "LoginViewController.h"
#import "Encryptor.h"
#import "NSObject+FormInputCategory.h"
#import "OnboardingTemplate01ViewController.h"
#import "TabunganBerhasilController.h"
#import "GagalVerifikasiController.h"
#import "Rekening.h"
#import "ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"
#import "Encryptor.h"

@interface VerifikasiOnlineSelesaiController () <UIApplicationDelegate>
{
    CGFloat nextStatusCheck;
    Encryptor *encryptor;
}
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@end

@implementation VerifikasiOnlineSelesaiController

- (void)viewDidLoad {
    [super viewDidLoad];

    encryptor = [[Encryptor alloc] init];
    
    _btnCheckStatus.layer.cornerRadius = 25;
    _btnCheckStatus.layer.masksToBounds = true;
    _btnCheckStatus.layer.borderWidth = 0;
    
    [_progressView setProgress:1];
//    NSDate *lastStatusCheck = [[NSDate date] dateByAddingTimeInterval:60.0];
//    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
//    [ns setObject:lastStatusCheck forKey:@"LastStatusCheck"];
//    [ns synchronize];
//    nextStatusCheck = [lastStatusCheck timeIntervalSinceNow];
    
    [_btnCheckStatus setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_btnCheckStatus setTitleColor:[UIColor colorWithRed:20.0/255.0 green:124.0/255.0 blue:113.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btnCheckStatus.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_btnCheckStatus addTarget:self action:@selector(btnCheckStatusTappedHandler) forControlEvents:UIControlEventTouchUpInside];
    [_btnClose addTarget:self action:@selector(btnCloseTappedHandler) forControlEvents:UIControlEventTouchUpInside];
    
    // [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self animateProgressView];
}

- (void)animateProgressView
{
    [_progressView setProgress:1];
    [UIView animateWithDuration:nextStatusCheck animations:^{
        [self->_progressView layoutIfNeeded];
    } completion:^(BOOL finished){
        [self->_btnCheckStatus setEnabled:YES];
    }];
}

- (void)btnCheckStatusTappedHandler
{
    [self showLoading];
    [_progressView setProgress:0];
    dispatch_group_t dispatchGroup = dispatch_group_create();
    NSMutableDictionary *checkResult = [[NSMutableDictionary alloc] init];
    NSString *msmsid = [CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:dispatchGroup returnData:checkResult];
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        [self->_btnCheckStatus setEnabled:NO];
        NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
        NSDate *lastStatusCheck = [[NSDate date] dateByAddingTimeInterval:300.0];
        [ns setObject:lastStatusCheck forKey:@"LastStatusCheck"];
        [ns synchronize];
        self->nextStatusCheck = [lastStatusCheck timeIntervalSinceNow];
        [self animateProgressView];
        
        if([[checkResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *nasabah = [checkResult objectForKey:@"data"];
            NSString *status = [nasabah objectForKey:@"status"];
            Rekening *rekening = [[Rekening alloc] init];
            [rekening setNamaNasabah:[nasabah objectForKey:@"nama_lgkp"]];
            [rekening setNomorRekening:[nasabah objectForKey:@"norek"]];
            [rekening setJenisTabungan:[nasabah objectForKey:@"jenis_tabungan"]];
            [rekening setJenisKartu:[nasabah objectForKey:@"jenis_kartu"]];
            [rekening setKodeCabang:[nasabah objectForKey:@"kode_cabang"]];
            [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
            [CustomerOnboardingData setObject:rekening forKey:ONBOARDING_REKENING_INFO_KEY];
            
            if([status isEqualToString:@"APPROVED"])
            {
                NSString* step = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY];
                if ([step isEqualToString:@"InformasiNoHandphoneScreen"]) {
                    // GO TO OTP Create PIN
                    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:step];
                    vc.modalPresentationStyle = UIModalPresentationFullScreen;
                    [self presentViewController:vc animated:YES completion:nil];
                } else {
                    [ns removeObjectForKey:@"LastStatusCheck"];
                    // OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                    // TabunganBerhasilController *contentVC = [[TabunganBerhasilController alloc] initWithRekening:rekening];
                    // [templateVC setChildController:contentVC];
                    // [self presentViewController:templateVC animated:YES completion:nil];
                    
                    // GO TO OTP Create PIN
                    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"CreatePinOnboardingScreen"];
                    vc.modalPresentationStyle = UIModalPresentationFullScreen;
                    [self presentViewController:vc animated:YES completion:nil];
                }
            }
            else if([status isEqualToString:@"REJECTEDVIDEO"])
            {
                // CHECK IF LOCAL DATA STILL EXISTS
                Encryptor *encryptor = [[Encryptor alloc] init];
                if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] && ![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY])
                    [encryptor restoreDataToLocalUsingData:nasabah];
                
                [ns removeObjectForKey:@"LastStatusCheck"];
                OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                GagalVerifikasiController *contentVC = [[GagalVerifikasiController alloc] initForRejectVideoWithRekening:rekening];
                [templateVC setChildController:contentVC];
                [self presentViewController:templateVC animated:YES completion:nil];
            }
            else if([status isEqualToString:@"NEW"])
            {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Status" message:@"Masih dalam proses verifikasi." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:alertAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else if([status isEqualToString:@"REJECTED"])
            {
                [ns removeObjectForKey:@"LastStatusCheck"];
                OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                GagalVerifikasiController *contentVC = [[GagalVerifikasiController alloc] initForRejectedWithRekening:rekening];
                [templateVC setChildController:contentVC];
                [self presentViewController:templateVC animated:YES completion:nil];
            }
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:@"Terjadi kesalahan. Silahkan coba beberapa saat lagi." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    });
}

- (void)btnCloseTappedHandler {
    // prevent onboarding checking when dismissed
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:NO forKey:ONBOARDING_NEED_CHECK_KEY];
    
    // GO TO MSM
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}

#pragma mark - UIApplicationDelegate methods
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self animateProgressView];
}
@end
