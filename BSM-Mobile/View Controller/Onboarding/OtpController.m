//
//  OtpController.m
//  BSM-Mobile
//
//  Created by ARS on 11/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "OtpController.h"
#import "AMPopTip.h"
#import "InformasiKontakController.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "CreatePinOnboardingController.h"
#import "ECSlidingViewController.h"
#import "HomeViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Utility.h"
#import "NSObject+FormInputCategory.h"
#import "InformasiKontakController.h"

@interface OtpController () <UITextFieldDelegate>
{
    NSTimer *timer;
    int currMinute;
    int currSecond;
    Encryptor *encryptor;
    NSInteger currentTag;
    NSInteger lastBtnTag;
    dispatch_group_t referenceGroup;
    NSString* tokenOtp;
}

@property IBOutlet UITextField *dig1;
@property IBOutlet UITextField *dig2;
@property IBOutlet UITextField *dig3;
@property IBOutlet UITextField *dig4;
@property IBOutlet UITextField *dig5;
@property IBOutlet UITextField *dig6;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *btn7;
@property (weak, nonatomic) IBOutlet UIButton *btn8;
@property (weak, nonatomic) IBOutlet UIButton *btn9;
@property (weak, nonatomic) IBOutlet UIButton *btn0;
@property (weak, nonatomic) IBOutlet UIButton *btnDel;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *labelTimer;
@property (weak, nonatomic) IBOutlet UIButton *btnResendOtp;
- (IBAction)hitBtnResendOtp:(id)sender;
@property AMPopTip *errorTooltip;

@end

@implementation OtpController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    
    referenceGroup = dispatch_group_create();
    encryptor = [[Encryptor alloc] init];
    
    currMinute=0;
    currSecond=59;
    
    currentTag = 20;
    lastBtnTag = 0;
    
    self.dig1.delegate = self;
    [self.dig1.layer setCornerRadius:15.0];
    [self.dig1 setTag:20];
    [self.dig1 becomeFirstResponder];
    
    self.dig2.delegate = self;
    [self.dig2.layer setCornerRadius:15.0];
    [self.dig2 setTag:21];
    
    self.dig3.delegate = self;
    [self.dig3.layer setCornerRadius:15.0];
    [self.dig3 setTag:22];
    
    self.dig4.delegate = self;
    [self.dig4.layer setCornerRadius:15.0];
    [self.dig4 setTag:23];
    
    self.dig5.delegate = self;
    [self.dig5.layer setCornerRadius:15.0];
    [self.dig5 setTag:24];
    
    self.dig6.delegate = self;
    [self.dig6.layer setCornerRadius:15.0];
    [self.dig6 setTag:25];
    
    UIView *digInputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    _dig1.inputView = digInputView;
    _dig2.inputView = digInputView;
    _dig3.inputView = digInputView;
    _dig4.inputView = digInputView;
    _dig5.inputView = digInputView;
    _dig6.inputView = digInputView;
    
    [_btn1 setTag:1];
    [_btn2 setTag:2];
    [_btn3 setTag:3];
    [_btn4 setTag:4];
    [_btn5 setTag:5];
    [_btn6 setTag:6];
    [_btn7 setTag:7];
    [_btn8 setTag:8];
    [_btn9 setTag:9];
    [_btn0 setTag:0];
    [_btnDel setTag:-1];
    [_btnNext setTag:-2];
    [_btn1 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn2 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn3 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn4 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn5 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn6 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn7 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn8 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn9 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn0 addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btnDel addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(numpadDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btn1 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn2 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn3 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn4 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn5 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn6 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn7 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn8 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn9 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [_btn0 setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    
    NSString *nomorHp = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    self.labelNomorHp.text = nomorHp;
    
    [_buttonUbahNomor setHidden:YES];
    
    tokenOtp = [encryptor getUserDefaultsObjectForKey:ONBOARDING_OTP_TOKEN];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self startTimer];
}

- (void)numpadDidHit:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *btn = (UIButton *)sender;
        if(btn.tag > -1)
        {
            UITextField *activeField = [self findCurrentResponder];
            if(activeField) {
                activeField.secureTextEntry = false;
                [activeField setText: [NSString stringWithFormat:@"%ld", (long)btn.tag]];
                UITextField *prevField = [self.contentView viewWithTag:activeField.tag - 1];
                if(prevField) prevField.secureTextEntry = true;
            }
        }
        else if (btn.tag == -1)
        {
            // delete
            UITextField *activeField = [self findCurrentResponder];
            if(activeField) {
                UITextField *currentField = [self.contentView viewWithTag:activeField.tag - 1];
                if(currentField) [currentField setText: @""];
            }
            else {
                UITextField *currentField = [self.contentView viewWithTag:25];
                if(currentField) [currentField setText: @""];
            }
        }
        else if(btn.tag == -2)
        {
            // next
            if(_OTP_TYPE == 0)
            {
                [self validateOtpV2];
            }
            
            if (_OTP_TYPE == 1) {
                [self goToCreatePin];
            }
        }
    }
}

- (void) goToCreatePin {
    if (tokenOtp == nil || [tokenOtp isEqual:[NSNull null]]) {
        [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
        return;
    }
    
    NSString *inputOtp = [NSString stringWithFormat:@"%@%@%@%@%@%@", self.dig1.text, self.dig2.text, self.dig3.text, self.dig4.text, self.dig5.text, self.dig6.text];
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              tokenOtp, @"token",
                              inputOtp, @"otp",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];

    [CustomerOnboardingData postToEndPoint:@"/api/pin/validateOTP" dispatchGroup:referenceGroup postData:postData returnData:returnData];
    
    [self showLoading];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSString *result = [returnData objectForKey:@"data"];
            if ([result isEqualToString:@"true"]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                CreatePinOnboardingController *createPinOnboardingController = [storyboard instantiateViewControllerWithIdentifier:@"CreatePinOnboardingScreen"];
                [self presentViewController:createPinOnboardingController animated:YES completion:nil];
            } else {
                [self showErrorMsg:NSLocalizedString(@"OTP_NOT_VALID", @"") withTextField:self.dig3];
            }
        } else {
            NSDictionary *errorMessage = [returnData objectForKey:@"error_message"];
            if (((NSNumber*)[errorMessage objectForKey:@"status"]).longValue == 400) {
                [self showErrorMsg:NSLocalizedString(@"OTP_NOT_VALID", @"") withTextField:self.dig3];
                return;
            }
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
        }
    });
}

- (nullable UITextField *)findFirstResponder
{
    for (UIView *current in self.contentView.subviews) {
        if([current isKindOfClass:UITextField.class])
        {
            UITextField *tField = (UITextField *)current;
            if([tField isFirstResponder]) return tField;
        }
    }
    return nil;
}

-(nullable UITextField *)findCurrentResponder {
    for (int i = 20; i < 26; i++) {
        UITextField *tfield = [self.contentView viewWithTag:i];
        if([tfield.text isEqualToString:@""]) return tfield;
    }
    return nil;
}

- (void) startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
}

- (void) timerFired
{
    if(currSecond>0)
    {
        currSecond-=1;
        if(currSecond>-1)
            [_labelTimer setText:[NSString stringWithFormat:@"00:%02d", currSecond]];
    }
    else
    {
        [timer invalidate];
        [_labelTimer setHidden:YES];
        [_btnResendOtp setHidden:NO];
        if (_OTP_TYPE == 0) {
            [_buttonUbahNomor setHidden:NO];
        }
    }
}

- (void)resendOtp
{
    dispatch_group_t dispatchGroup = dispatch_group_create();
    NSString *msmsid = [encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY];
    NSString *dest = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    NSString *imei = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    NSString *ipAddress = [Utility deviceIPAddress];
    // NSString *captcha = [encryptor getUserDefaultsObjectForKey:ONBOARDING_CAPTCHA_KEY];
    // NSString *captchaToken = [encryptor getUserDefaultsObjectForKey:ONBOARDING_CAPTCHA_TOKEN_KEY];
    NSString *rawHkey = [Utility sha256HashFor:[NSString stringWithFormat:@"%@%@%@%@", dest, imei, ipAddress, msmsid]];
    NSString *hKey = [rawHkey substringToIndex:42];
    
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              msmsid, @"msmsid",
                              dest, @"dest",
                              ipAddress, @"ip",
                              imei, @"imei",
                              hKey, @"at",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    
    [CustomerOnboardingData postToEndPoint:@"api/otp/resend" dispatchGroup:dispatchGroup postData:postData returnData:returnData];
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
            NSString *tokenOtp = [[returnData objectForKey:@"data"] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            [self->encryptor setUserDefaultsObject:tokenOtp forKey:ONBOARDING_OTP_KEY];
        }
    });
}

- (void) resendOtpPin {
    NSDictionary *nasabahData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_NASABAH];
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              [nasabahData objectForKey:@"no_hp"], @"dest",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];

    [CustomerOnboardingData postToEndPoint:@"/api/pin/createOTP" dispatchGroup:referenceGroup postData:postData returnData:returnData];
    
    [self showLoading];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *data = [returnData objectForKey:@"data"];
            [self->encryptor setUserDefaultsObject:[data objectForKey:@"token"] forKey:ONBOARDING_OTP_TOKEN];
            self->tokenOtp = [data objectForKey:@"token"];
        } else {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
        }
    });
}

- (void)hitBtnResendOtp:(id)sender
{
    if (_OTP_TYPE == 0) {
        [self resendOtp];
    } else {
        [self resendOtpPin];
    }
    currSecond = 59;
    [_labelTimer setText:[NSString stringWithFormat:@"00:%02d", currSecond]];
    [_labelTimer setHidden:NO];
    [_btnResendOtp setHidden:YES];
    [_buttonUbahNomor setHidden:YES];
    [self startTimer];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.secureTextEntry = false;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.secureTextEntry = true;
}

- (BOOL)validateOtp
{
    NSString *validOtp = [encryptor getUserDefaultsObjectForKey:ONBOARDING_OTP_KEY];
    if (validOtp == nil || [validOtp isEqual:[NSNull null]]) {
        return NO;
    }
    
    NSString *inputOtp = [NSString stringWithFormat:@"%@%@%@%@%@%@", self.dig1.text, self.dig2.text, self.dig3.text, self.dig4.text, self.dig5.text, self.dig6.text];
    
    if([validOtp isEqualToString:inputOtp])
    {
        return YES;
    }
    else
    {
        [self showErrorMsg:NSLocalizedString(@"OTP_NOT_VALID", @"") withTextField:self.dig3];
        return NO;
    }
}

- (void) validateOtpV2 {
    [self showLoading];
    NSString *tokenOtp = [encryptor getUserDefaultsObjectForKey:ONBOARDING_OTP_KEY];
    NSString *inputOtp = [NSString stringWithFormat:@"%@%@%@%@%@%@", self.dig1.text, self.dig2.text, self.dig3.text, self.dig4.text, self.dig5.text, self.dig6.text];
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              tokenOtp, @"token",
                              inputOtp, @"otp",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    
    [CustomerOnboardingData postToEndPoint:@"api/otp/validate" dispatchGroup:referenceGroup postData:postData returnData:returnData];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
            NSString *result = [returnData objectForKey:@"data"];
            if ([result isEqualToString:@"true"]) {
                [self validateUser];
            } else {
                [self showErrorMsg:NSLocalizedString(@"OTP_NOT_VALID", @"") withTextField:self.dig3];
            }
        } else {
            [self showErrorMsg:NSLocalizedString(@"OTP_NOT_VALID", @"") withTextField:self.dig3];
        }
    });
}

- (void) validateUser {
    NSMutableDictionary *nasabahCountResult = [[NSMutableDictionary alloc] init];
    NSString *email = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY];
    NSString *dest = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/nasabah/count?nohp=%@&email=%@", dest, email] dispatchGroup:referenceGroup returnData:nasabahCountResult];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        if([[nasabahCountResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSString *res = [nasabahCountResult objectForKey:@"data"];
            if([res isEqualToString:@"0"])
            {
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                UIViewController *nextController = [sb instantiateViewControllerWithIdentifier:@"DokumenDanDataDiriScreen"];
                [self presentViewController:nextController animated:YES completion:nil];
            }
            else
            {
                [self showAlertErrorWithCompletion:@"Ooops" andMessage:NSLocalizedString(@"EMAIL_OR_PHONE_REGISTERED", @"") sender:self handler:^(UIAlertAction * _Nonnull action) {
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                    InformasiKontakController *prevController = [sb instantiateViewControllerWithIdentifier:@"InformasiKontakScreen"];
                    [self presentViewController:prevController animated:YES completion:nil];
                }];
            }
        } else {
            long status = [[nasabahCountResult objectForKey:@"status"] longValue];
            if (status == 401) {
                [self showAlertErrorWithCompletion:NSLocalizedString(@"SORRY", @"") andMessage:NSLocalizedString(@"PHONE_ALREADY_REGISTERED", @"") sender:self handler:^(UIAlertAction * _Nonnull action) {
                    [self goToActivation];
                }];
            } else {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
            }
        }
    });
}

- (void)goToActivation
{
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // TODO SHOW ACTIVATION SCREEN
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        HomeViewController *homeVC = (HomeViewController *)firstVC.topViewController;
        UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"ActivationVC"];
        homeVC.slidingViewController.topViewController = viewCont;
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length == 0) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        
        if (nextResponder)
        {
            [nextResponder becomeFirstResponder];
            textField.text = string;
        }
        else
        {
            return YES;
        }
    }
    return NO;
}

-(void)cancelNumberPad{
    [_dig6 resignFirstResponder];
    _dig6.text = @"";
}

-(void)doneWithNumberPad{
    [_dig6 resignFirstResponder];
    // next
    if(_OTP_TYPE == 0)
    {
        [self validateOtpV2];
    }
    
    if (_OTP_TYPE == 1) {
        [self goToCreatePin];
    }
}

- (void)showErrorMsg:(NSString *)errorMsg withTextField:(UITextField *)textField
{
    self.errorTooltip = [AMPopTip popTip];
//    [self.errorTooltip showText:errorMsg direction:AMPopTipDirectionDown maxWidth:200.0 inView:[self contentView] fromFrame:textField.frame];
    CGRect frame = textField.frame;
    frame.origin = CGPointMake(frame.origin.x, frame.origin.y + 38);
    [self.errorTooltip showText:errorMsg direction:AMPopTipDirectionDown maxWidth:200.0 inView:[self contentView] fromFrame:frame duration:3];
}
@end
