//
//  InformasiTTDController.m
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "InformasiTTDController.h"
#import "CameraOverlayController.h"
#import "CameraOverlayView.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"

@interface InformasiTTDController ()
{
    Encryptor *encryptor;
}
@end

@implementation InformasiTTDController

static const NSString *ScreenName = @"InformasiTTDScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    self.view.hidden = NO;
    self.contentView.layer.cornerRadius = 15.0f;
    _btnRetake.layer.cornerRadius = 24.0f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    if(!_imagePicked && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _imagePicked = [self loadImageFromLocalStorageWithFileName:@"TTD" ofType:@"jpg" inDirectory:documentsDirectory];

        if(!_imagePicked)
        {
            CameraOverlayController *overlayController = [[CameraOverlayController alloc] init];

            _cameraPicker = [[UIImagePickerController alloc] init];
            _cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            _cameraPicker.delegate = self;
            _cameraPicker.showsCameraControls = false;
            _cameraPicker.allowsEditing = false;

            CameraOverlayView *cameraView = (CameraOverlayView *)overlayController.view;
            cameraView.frame = _cameraPicker.view.frame;
            cameraView.textUnderBox.text = NSLocalizedString(@"SIGNATURE_ABOVE_WHITE_PAPER", @"");
            cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_ktp@2x.png"];
            [cameraView.navigationBar.topItem setTitle:NSLocalizedString(@"SIGNATURE", @"")];
            cameraView.delegate = self;

            _cameraPicker.cameraOverlayView = cameraView;

            [self presentViewController:_cameraPicker animated:YES completion:nil];
        }
        else
        {
            self.view.hidden = FALSE;
            [self setImageView];
        }
    }
    else self.view.hidden = FALSE;
    
    CGFloat yPos = self.imageView.frame.origin.y;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:4 fromTotal:7];
}

- (void)didTakePhoto:(CameraOverlayView *)overlayView
{
    [_cameraPicker takePicture];
}

- (void)didBack:(CameraOverlayView *)overlayView
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"InformasiNpwpScreen"];
    [_cameraPicker presentViewController:vc animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    [_cameraPicker dismissViewControllerAnimated:YES completion:nil];
    
    self.imagePicked = info[UIImagePickerControllerOriginalImage];
    
    [self setImageView];
}

- (IBAction)hitBtnRetake:(id)sender {
    _imagePicked = nil;
    [self removeImageFromLocalStorage:@"TTD"];
    [self viewDidLoad];
    [self viewDidAppear:YES];
}

- (void)setImageView
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    CGRect rect = _imageView.frame;
    
    _imageHeight.constant = (screenHeight / 2) - 60;
    
    _imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, (screenWidth - 20), _imageHeight.constant);
    _imageView.image = _imagePicked;
    _imageView.clipsToBounds = YES;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if(self.imagePicked)
            {
                [self saveImageToLogicalDocs:self.imagePicked withFilename:@"TTD" andSaveWithKey:ONBOARDING_INFORMASI_TTD_FILEID_KEY onCompletionPerformSegueWithIdentifier:identifier fromController:self];
            }
            else return YES;
            return NO;
        }
    }
    
    return YES;
}
@end
