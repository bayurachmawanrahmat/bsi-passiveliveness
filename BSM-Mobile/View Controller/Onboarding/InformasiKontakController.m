//
//  InformasiKontakController.m
//  BSM-Mobile
//
//  Created by ARS on 10/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "InformasiKontakController.h"
#import "AMPopTip.h"
#import "CustomerOnboardingData.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"
#import "ECSlidingViewController.h"
#import "HomeViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "NSObject+FormInputCategory.h"
#import "Utility.h"

@interface InformasiKontakController () <UITextFieldDelegate>
{
    Encryptor *encryptor;
    dispatch_group_t dispatchGroup;
    NSString *captchaToken;
}

@property IBOutlet UITextField *emailField;
@property IBOutlet UILabel *emailInfoLabel;
@property IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *promoField;
@property AMPopTip *errorTooltip;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imageCaptcha;
@property (weak, nonatomic) IBOutlet UITextField *fieldCaptcha;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnTextRefresh;

@end

@implementation InformasiKontakController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.emailField.delegate = self;
    self.promoField.delegate = self;
    
    encryptor = [[Encryptor alloc] init];
    
    dispatchGroup = dispatch_group_create();
    self->captchaToken = @"";
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"CANCEL", @"") style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"DONE", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _phoneField.inputAccessoryView = numberToolbar;
    
    // input field toolbar text
    UIToolbar* textToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    textToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DONE", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(textToolbarDone)];
    [textToolbar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    [textToolbar setUserInteractionEnabled:YES];
    [textToolbar sizeToFit];
    _emailField.inputAccessoryView = textToolbar;
    _promoField.inputAccessoryView = textToolbar;
    _fieldCaptcha.inputAccessoryView = textToolbar;
    
    [self setInputValue];
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self setScrollView:_scrollView];
    [self setMainView:self.view];
    [self registerForKeyboardNotifications];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    if([[ns objectForKey:ONBOARDING_DEVICE_HAS_NOTCH_KEY] isEqual:[NSNumber numberWithBool:YES]])
        _contentViewHeight.constant = -(_scrollView.frame.origin.y + 20);
    else
        _contentViewHeight.constant = -_scrollView.frame.origin.y;
    
    CGRect frame = _contentView.frame;
    while (frame.size.height <= 521) {
        frame.size.height += 5;
        _contentViewHeight.constant += 5;
        [_contentView setFrame:frame];
    }
    
    [self requestCaptcha];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)validatePhoneWithString:(NSString*)phone
{
    NSString *phoneRegex = @"^[0-9]+$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phone];
}

- (void) inputHasError:(BOOL)inputHasError textField:(UITextField *)textField
{
    if(inputHasError)
    {
        textField.layer.borderColor=[[UIColor redColor]CGColor];
        textField.layer.borderWidth= 0.8f;
    }
    else
    {
        [_emailField resignFirstResponder];
        [_phoneField becomeFirstResponder];
        textField.layer.borderColor=[[UIColor clearColor]CGColor];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setActiveField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _emailField) {
        [_emailField resignFirstResponder];
        [_phoneField becomeFirstResponder];
    }
    else if (textField == _promoField) {
        [_promoField resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(
       (
        (textField == _emailField && textField.text.length == 100)
        || (textField == _phoneField && textField.text.length == 13)
        || (textField == _promoField && textField.text.length == 20)
        )
       && ![textField.text isEqualToString:@""] && ![string isEqualToString:@""]) {
        return NO;
    }
    return YES;
}

-(void)cancelNumberPad{
    if(_phoneField.isFirstResponder){
        [_phoneField resignFirstResponder];
        _phoneField.text = @"";
    }
}

-(void)doneWithNumberPad{
    if(_phoneField.isFirstResponder){
        NSString *numberFromTheKeyboard = _phoneField.text;
        if([numberFromTheKeyboard isEqualToString:@""]) [self showErrorMsg:NSLocalizedString(@"PLEASE_FILL_NO_PHONE", @"") withUiTextfield:_phoneField];
        [_phoneField resignFirstResponder];
    }
}

-(void)textToolbarDone {
    if ([self.emailField isFirstResponder])
    {
        [self.emailField resignFirstResponder];
    } else if ([self.promoField isFirstResponder])
    {
        [self.promoField resignFirstResponder];
    } else if ([self.fieldCaptcha isFirstResponder])
    {
        [self.fieldCaptcha resignFirstResponder];
    }
}

- (BOOL) checkInput: (NSString *) email phonenumber:(NSString *) phone{
    if([email isEqualToString:@""] || [phone isEqualToString:@""]) {
        if([email isEqualToString:@""]) [self showErrorMsg:@"Harap isi email" withUiTextfield:_emailField];
        if([phone isEqualToString:@""]) [self showErrorMsg:NSLocalizedString(@"PLEASE_FILL_NO_PHONE", @"") withUiTextfield:_phoneField];
        return FALSE;
    }
    else if (![self validateEmailWithString:email])
    {
        [self showErrorMsg:NSLocalizedString(@"EMAIL_NOT_VALID", @"") withUiTextfield:_emailField];
        return FALSE;
    }
    else if(![self validatePhoneWithString:phone])
    {
        [self showErrorMsg:NSLocalizedString(@"PHONE_NOT_VALID", @"") withUiTextfield:_phoneField];
        return FALSE;
    }
    else if(_promoField.text.length > 20)
    {
        [self showErrorMsg:NSLocalizedString(@"PROMO_NOT_VALID", @"") withUiTextfield:_promoField];
        return FALSE;
    }
    else if ([_fieldCaptcha.text isEqualToString:@""]) {
        [self showErrorMsg:NSLocalizedString(@"PLEASE_FILL_CAPTCHA", @"") withUiTextfield:_fieldCaptcha];
        return FALSE;
    }
    else return TRUE;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *btn = (UIButton *) sender;
        if([btn.titleLabel.text isEqualToString:@"SELANJUTNYA"]){
            if([self checkInput:_emailField.text phonenumber:_phoneField.text])
            {
                [self saveInputValue];
                [self requestOtp];
            }
        }
    }
    else return YES;
    return NO;
}

-(void) saveInputValue{
    NSString *email = _emailField.text;
    NSString *phone = _phoneField.text;
    NSString *kodepromo = _promoField.text;
    [encryptor setUserDefaultsObject:email forKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY];
    [encryptor setUserDefaultsObject:phone forKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    [encryptor setUserDefaultsObject:kodepromo forKey:ONBOARDING_INFORMASIKONTAK_KODEPROMO_KEY];
    
}

-(void) setInputValue{
    NSString *email = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY];
    NSString *phone = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    NSString *kodepromo = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_KODEPROMO_KEY];
    
    _emailField.text = email;
    _phoneField.text = phone;
    _promoField.text = kodepromo;
}

- (void)showErrorMsg:(NSString *)errorMsg withUiTextfield:(UITextField *)textField
{
    [self showErrorLabel:errorMsg withUiTextfield:textField];
}

+(NSString *) generateOtp {
    NSString *letters = @"0123456789";
    int len = 6;
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
        
        while (i == 0 && [randomString characterAtIndex:0] == '0') {
            [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
        }
    }
    
    return randomString;
//    return @"111111";
}

-(void) requestOtp
{
    [self showLoading];
    
    NSString *msmsid = [encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY];
    NSString *dest = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    NSString *imei = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    NSString *ipAddress = [Utility deviceIPAddress];
    NSString *captcha = _fieldCaptcha.text;
    NSString *rawHkey = [Utility sha256HashFor:[NSString stringWithFormat:@"%@%@%@%@", dest, imei, ipAddress, msmsid]];
    NSString *hKey = [rawHkey substringToIndex:42];
    
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              msmsid, @"msmsid",
                              dest, @"dest",
                              ipAddress, @"ip",
                              imei, @"imei",
                              hKey, @"at",
                              captcha, @"captcha",
                              self->captchaToken, @"captcha_token",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    
    [CustomerOnboardingData postToEndPoint:@"api/otp/create" dispatchGroup:dispatchGroup postData:postData returnData:returnData];
    
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
            [self->encryptor setUserDefaultsObject:captcha forKey:ONBOARDING_CAPTCHA_KEY];
            [self->encryptor setUserDefaultsObject:self->captchaToken forKey:ONBOARDING_CAPTCHA_TOKEN_KEY];
            NSString *tokenOtp = [[returnData objectForKey:@"data"] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            [self->encryptor setUserDefaultsObject:tokenOtp forKey:ONBOARDING_OTP_KEY];
            
            [self performSegueWithIdentifier:@"InformasiKontakSegue" sender:self];
        } else {
            [self requestCaptcha];
            [self->_fieldCaptcha setText:@""];
            
            NSDictionary *errorMessage = [returnData objectForKey:@"error_message"];
            
            if ([errorMessage isEqual:[NSNull null]] || [errorMessage isEqual:nil]) {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
                return;
            }
            
            if ([[errorMessage valueForKey:@"status"] isEqualToNumber:[NSNumber numberWithInt:403]]) {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"CAPTCHA_NOT_VALID", @"") sender:self];
                return;
            }
            
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
        }
    });
}

- (void)goToActivation
{
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // TODO SHOW ACTIVATION SCREEN
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        HomeViewController *homeVC = (HomeViewController *)firstVC.topViewController;
        UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"ActivationVC"];
        homeVC.slidingViewController.topViewController = viewCont;
    }];
}

- (void)requestCaptcha
{
    NSString *ipAddress = [Utility deviceIPAddress];
    NSString *msmsid = [encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY];
    
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              msmsid, @"msmsid",
                              ipAddress, @"ip",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    
    [CustomerOnboardingData postToEndPoint:@"api/captcha/request" dispatchGroup:dispatchGroup postData:postData returnData:returnData isResEncrypted:NO];
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
            NSDictionary *data = [returnData objectForKey:@"data"];
            if (data) {
                NSString *image = [data objectForKey:@"i"];
                self->captchaToken = [data objectForKey:@"t"];
                [self setupCaptcha:image];
            }
        } else {
            self->captchaToken = @"";
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
        }
    });
}

- (void)setupCaptcha: (NSString*) image {
    @try {
        NSString *str = @"data:image/jpg;base64,";
        str = [str stringByAppendingString:image];
        NSURL *url = [NSURL URLWithString:str];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *ret = [UIImage imageWithData:imageData];
        
        [_imageCaptcha setImage:ret];
    } @catch(NSException *exception) {
        [self showAlertErrorWithTitle:@"Ooops" andMessage:@"Gagal mendapatkan captcha" sender:self];
    }
}

- (IBAction)hitBtnRefresh:(id)sender {
    [self requestCaptcha];
}

- (IBAction)hitBtnTextRefresh:(id)sender {
    
}


@end
