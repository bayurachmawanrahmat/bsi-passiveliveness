//
//  TabunganBerhasilController.h
//  BSM-Mobile
//
//  Created by ARS on 25/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrasiSelesaiController.h"
#import "Rekening.h"

NS_ASSUME_NONNULL_BEGIN

@interface TabunganBerhasilController : RegistrasiSelesaiController

- (instancetype)initWithRekening:(Rekening *)rekening;
- (IBAction)hitBtnGuide:(id)sender;

@end

NS_ASSUME_NONNULL_END
