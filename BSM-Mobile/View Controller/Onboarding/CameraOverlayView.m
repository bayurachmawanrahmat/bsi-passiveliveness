//
//  CameraOverlayView.m
//  BSM-Mobile
//
//  Created by ARS on 16/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CameraOverlayView.h"

@implementation CameraOverlayView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)hitBtnBack:(id)sender {
    [_delegate didBack: self];
}

- (IBAction)hitBtnTakePhoto:(id)sender {
    [_delegate didTakePhoto: self];
}
@end
