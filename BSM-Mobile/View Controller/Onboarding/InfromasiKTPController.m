//
//  InfromasiKTPController.m
//  BSM-Mobile
//
//  Created by ARS on 17/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CameraOverlayController.h"
#import "CameraOverlayView.h"
#import "InfromasiKTPController.h"
#import "AMPopTip.h"
#import "NSObject+FormInputCategory.h"
#import "UIDropdownSearch.h"
#import "CustomerOnboardingData.h"
#import "NikTerdaftarController.h"
#import "Encryptor.h"
#import "InformasiAlamatController.h"
#import "Utility.h"

@interface InfromasiKTPController ()
{
    CGFloat screenWidth;
    CGFloat screenHeight;
    CGFloat defaultConstraintPendidikanTop;
    NSDictionary *formInputDatapribadi;
    NSDictionary *formInputDomisili;
    NSDictionary *formDropdownDatapribadi;
    dispatch_group_t referenceGroup;
    NSDictionary *dukcapilData;
    int dukcapilStatus;
    NSString *pageError;
    NikTerdaftarController *modalController;
    Encryptor *encryptor;
    UIRefreshControl *refreshControl;
    NSString *name;
    NSString *motherName;
    UIDatePicker *datePicker;
    NSString* selectedDate;
}
@end

@implementation InfromasiKTPController

static const NSString *ScreenName = @"InformasiPribadiScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    self.view.hidden = NO;
    self.contentView.layer.cornerRadius = 15.0f;
    self.btnRetake.layer.cornerRadius = 24.0f;

    referenceGroup = dispatch_group_create();

    self.fieldNomorKtp.delegate = self;
    self.fieldNama.delegate = self;
    self.fieldNamaIbu.delegate = self;
    self.fieldTempatLahir.delegate = self;
    self.fieldTanggalLahir.delegate = self;
    
    modalController = [[NikTerdaftarController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    modalController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    // input field toolbar numberpad
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"CANCEL", @"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"DONE", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.fieldNomorKtp.inputAccessoryView = numberToolbar;
    
    // input field toolbar text
    UIToolbar* textToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    textToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DONE", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(textToolbarDone)];
    [textToolbar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    [textToolbar setUserInteractionEnabled:YES];
    [textToolbar sizeToFit];
    self.fieldNama.inputAccessoryView = textToolbar;
    self.fieldNamaIbu.inputAccessoryView = textToolbar;
    self.fieldTempatLahir.inputAccessoryView = textToolbar;
    self.fieldTanggalLahir.inputAccessoryView = textToolbar;
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [_scrollView setRefreshControl:refreshControl];
    } else {
        [_scrollView addSubview:refreshControl];
    }
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self setScrollView:_scrollView];
    [self setMainView:self.view];
    [self registerForKeyboardNotifications];
    
    // Setup date picker
    datePicker = [[UIDatePicker alloc]init];
    //NSString *maxDateString = @"01-Jan-1996";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    //NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    if (@available(iOS 13.4, *)) {
        [datePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels];
    }
    [datePicker addTarget:self action:@selector(updateTanggalLahirField:) forControlEvents:UIControlEventValueChanged];
    
    [self loadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    if(!_imagePicked && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        NSString * documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _imagePicked = [self loadImageFromLocalStorageWithFileName:@"KTP" ofType:@"jpg" inDirectory:documentsDirectory];

        if(!_imagePicked)
        {
            CameraOverlayController *overlayController = [[CameraOverlayController alloc] init];

            _cameraPicker = [[UIImagePickerController alloc] init];
            _cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            _cameraPicker.delegate = self;
            _cameraPicker.showsCameraControls = false;
            _cameraPicker.allowsEditing = false;

            CameraOverlayView *cameraView = (CameraOverlayView *)overlayController.view;
            cameraView.frame = _cameraPicker.view.frame;
            cameraView.textUnderBox.text = NSLocalizedString(@"DESC_KTP_POSITION", @"");
            cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_ktp@2x.png"];
            [cameraView.navigationBar.topItem setTitle:NSLocalizedString(@"PHOTO_KTP", @"")];
            cameraView.delegate = self;

            _cameraPicker.cameraOverlayView = cameraView;

            [self presentViewController:_cameraPicker animated:NO completion:nil];
        }
        else
        {
            [self setImageView];
            self.view.hidden = NO;
        }
    }
    else self.view.hidden = NO;
    
    CGFloat yPos = self.scrollView.frame.origin.y;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:1 fromTotal:7];
}

- (void) loadData {
    NSString *nomorKtp = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
    motherName = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU];
    name = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP];
    NSString *birthPlace = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_TMPT_LAHIR];
    NSString *birthDate = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_TGL_LHR];
    
    formInputDatapribadi = [[NSDictionary alloc] initWithObjectsAndKeys:
                            _fieldNomorKtp, ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY,
                            nil];
    
    // todo hapus is alamat beda checkbox
    self.isAlamatBeda = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY] integerValue];
    
    if(name) {
        // [_fieldNama setText:[self maskingName:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP]]];
        [_fieldNama setText:name];
    }
    if(motherName) {
        [_fieldNamaIbu setText:motherName];
    }
    
    if(nomorKtp) {
        [_fieldNomorKtp setText:nomorKtp];
    }
    
    if (birthPlace) {
        [_fieldTempatLahir setText:birthPlace];
    }
    
    if (birthDate) {
        NSDateFormatter *formatter;
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *currentBirthDate=[formatter dateFromString:birthDate];
        [datePicker setDate:currentBirthDate];
        [_fieldTanggalLahir setText:[self formatDate:currentBirthDate withFormat:@"dd/MM/yyyy"]];
        selectedDate = [self formatDate:currentBirthDate withFormat:@"yyyy-MM-dd"];
    } else {
        NSDate* currentDate = [NSDate date];
        [datePicker setDate:currentDate];
        selectedDate = [self formatDate:currentDate withFormat:@"yyyy-MM-dd"];
    }
    [_fieldTanggalLahir setInputView:datePicker];
}

- (IBAction)hitBtnRetake:(id)sender {
    _imagePicked = nil;
    [self removeImageFromLocalStorage:@"KTP"];
    [self viewDidLoad];
    [self viewDidAppear:YES];
}

- (void) saveData:(NSDictionary *)data {
    [self saveTextFieldData:formInputDatapribadi];
    [self saveDropDownData:formDropdownDatapribadi];
    [encryptor setUserDefaultsObject:_fieldNama.text forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP];
    [encryptor setUserDefaultsObject:_fieldNamaIbu.text forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU];
    [encryptor setUserDefaultsObject:_fieldTempatLahir.text forKey:ONBOARDING_DUKCAPIL_TMPT_LAHIR];
    [encryptor setUserDefaultsObject:_fieldTanggalLahir.text forKey:ONBOARDING_DUKCAPIL_TGL_LHR];
    [encryptor setUserDefaultsObject:_fieldTempatLahir.text forKey:ONBOARDING_DUKCAPIL_TMPT_LAHIR];
    [encryptor setUserDefaultsObject:selectedDate forKey:ONBOARDING_DUKCAPIL_TGL_LHR];
    
    [encryptor setUserDefaultsObject:[data objectForKey:@"NAMA_LGKP"] forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_SC];
    [encryptor setUserDefaultsObject:[data objectForKey:@"NAMA_LGKP_IBU"] forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU_SC];
    [encryptor setUserDefaultsObject:[data objectForKey:@"TGL_LHR"] forKey:ONBOARDING_DUKCAPIL_TGL_LHR_SC];
    [encryptor setUserDefaultsObject:[data objectForKey:@"isvalid"] forKey:ONBOARDING_DUKCAPIL_IS_VALID];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if([self checkInput])
            {
                [self showLoading];
                
                // check NIK
                NSMutableDictionary *checkNIKResult = [[NSMutableDictionary alloc] init];
                NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                          _fieldNomorKtp.text, @"nik",
                                          _fieldNama.text, @"nama",
                                          _fieldNamaIbu.text, @"namaIbu",
                                          selectedDate, @"tglLahir",
                                          nil];
                [CustomerOnboardingData postToEndPoint:@"api/onboarding/checkNikV2" dispatchGroup:referenceGroup postData:postData returnData:checkNIKResult];
                dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
                    [self hideLoading];
                    
                    if([[checkNIKResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
                    {
                        NSDictionary *resultData = [checkNIKResult objectForKey:@"data"];
                        [self saveData:resultData];
                        if (![[resultData objectForKey:@"RESPON"] isKindOfClass:[NSNull class]]) {
                            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"NIK_NOT_FOUND", @"") sender:self];
                        } else {
                            [self saveData:resultData];
                            if(self->_imagePicked)
                            {
                                // SAVE IMAGE
                                [self saveImageToLogicalDocs:self.imagePicked withFilename:@"KTP" andSaveWithKey:ONBOARDING_INFORMASI_PRIBADI_KTP_FILEID_KEY onCompletionPerformSegueWithIdentifier:identifier fromController:self];
                            }
                        }
                    } else {
                        NSDictionary *errorMessage = [checkNIKResult objectForKey:@"error_message"];
                        if (errorMessage != nil && ![errorMessage isEqual:[NSNull null]]) {
                            NSString *message = [errorMessage objectForKey:@"message"];
                            [self showAlertErrorWithTitle:@"Ooops" andMessage:(message != nil && ![message isEqual:[NSNull null]] ? message : NSLocalizedString(@"ERROR", @"")) sender:self];
                        } else {
                            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
                        }
                    }
                });
                
                return NO;
                // todo balikin return NO
                // return YES;
            } else {
                return NO;
            }
        }
    }
    
    return YES;
}

- (void)didTakePhoto:(CameraOverlayView *)overlayView
{
    [_cameraPicker takePicture];
}

- (void)didBack:(CameraOverlayView *)overlayView
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"JenisTabunganScreen"];
    [_cameraPicker presentViewController:vc animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    [_cameraPicker dismissViewControllerAnimated:YES completion:nil];
    
    _imagePicked = info[UIImagePickerControllerOriginalImage];
    
    [self setImageView];
}

-(void)cancelNumberPad{
    if([self.fieldNomorKtp isFirstResponder])
    {
        [self.fieldNomorKtp resignFirstResponder];
        self.fieldNomorKtp.text = @"";
    }
}

-(void)doneWithNumberPad{
    if([self.fieldNomorKtp isFirstResponder])
    {
        [self.fieldNomorKtp resignFirstResponder];
    }
}

-(void)textToolbarDone {
    if ([self.fieldNama isFirstResponder])
    {
        [self.fieldNama resignFirstResponder];
    } else if ([self.fieldNamaIbu isFirstResponder])
    {
        [self.fieldNamaIbu resignFirstResponder];
    }  else if ([self.fieldTempatLahir isFirstResponder])
    {
        [self.fieldTempatLahir resignFirstResponder];
    } else if ([self.fieldTanggalLahir isFirstResponder])
    {
        [self.fieldTanggalLahir resignFirstResponder];
    }
}

- (BOOL)checkInput
{
    BOOL isValid = TRUE;
    if([_fieldNomorKtp.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"KTP_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    }
    else if (dukcapilStatus == 401)
    {
        [self showErrorLabel:NSLocalizedString(@"NIK_ALREADY_REGISTERED", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    } else if(self.fieldNomorKtp.text.length > 16 || self.fieldNomorKtp.text.length < 16 || ![Utility isNumericOnly:self.fieldNomorKtp.text])
    {
        [self showErrorLabel:NSLocalizedString(@"KTP_NOT_VALID", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldNomorKtp];
    
    if([_fieldNama.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"NAME_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldNama];
        [self inputHasError:YES textField:_fieldNama];
        isValid = FALSE;
    } else if(self.fieldNama.text.length > 35 || ![Utility isAlphabetOnly:self.fieldNama.text])
    {
        [self showErrorLabel:NSLocalizedString(@"NAME_NOT_VALID", @"") withUiTextfield:_fieldNama];
        [self inputHasError:YES textField:_fieldNama];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldNama];
    
    if([_fieldNamaIbu.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"MOTHER_NAME_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldNamaIbu];
        [self inputHasError:YES textField:_fieldNamaIbu];
        isValid = FALSE;
    } else if(self.fieldNamaIbu.text.length > 35 || ![Utility isAlphabetOnly:self.fieldNamaIbu.text])
    {
        [self showErrorLabel:NSLocalizedString(@"MOTHER_NAME_NOT_VALID", @"") withUiTextfield:_fieldNamaIbu];
        [self inputHasError:YES textField:_fieldNamaIbu];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldNamaIbu];
    
    if([_fieldTempatLahir.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"BIRTHPLACE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldTempatLahir];
        [self inputHasError:YES textField:_fieldTempatLahir];
        isValid = FALSE;
    } else if(self.fieldTempatLahir.text.length > 35 || ![Utility isAlphabetOnly:self.fieldTempatLahir.text])
    {
        [self showErrorLabel:NSLocalizedString(@"BIRTHPLACE_NOT_VALID", @"") withUiTextfield:_fieldTempatLahir];
        [self inputHasError:YES textField:_fieldTempatLahir];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldTempatLahir];
    
    if([_fieldTanggalLahir.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"BIRTHDATE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldTanggalLahir];
        [self inputHasError:YES textField:_fieldTanggalLahir];
        isValid = FALSE;
    } else if(self.fieldTanggalLahir.text.length > 35)
    {
        [self showErrorLabel:NSLocalizedString(@"BIRTHDATE_NOT_VALID", @"") withUiTextfield:_fieldTanggalLahir];
        [self inputHasError:YES textField:_fieldTanggalLahir];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldTanggalLahir];
    
    return isValid;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self inputHasError:NO textField:textField];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setActiveField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _fieldNomorKtp && textField.text.length >= 16 && ![string isEqualToString:@""]) {
        return NO;
    } else if(textField == _fieldNama && textField.text.length >= 35 && ![string isEqualToString:@""]) {
        return NO;
    } else if(textField == _fieldNamaIbu && textField.text.length >= 35 && ![string isEqualToString:@""]) {
        return NO;
    } else if(textField == _fieldTempatLahir && textField.text.length >= 35 && ![string isEqualToString:@""]) {
        return NO;
    } else if(textField == _fieldTanggalLahir && textField.text.length >= 10 && ![string isEqualToString:@""]) {
        return NO;
    }
    return YES;
}

- (void)setImageView
{
    CGRect rect = _imageView.frame;
    
    _imageHeight.constant = (screenHeight / 2.5);
    
    _imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, (screenWidth - 20), _imageHeight.constant);
    _imageView.image = _imagePicked;
    _imageView.clipsToBounds = YES;
}

- (void) requestDukcapil
{
    dispatch_group_enter(referenceGroup);
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSString *nik = self.fieldNomorKtp.text;
    NSString *endpoint = [NSString stringWithFormat:@"api/onboarding/checkNik?nik=%@", nik];
    NSURL *url = [NSURL URLWithString:[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:endpoint]]];
    NSString *token = [CustomerOnboardingData getToken];
    NSLog(@"---------------- %@", url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    dukcapilData = nil;
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error)
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *returnData;
            if(ONBOARDING_SAFE_MODE)
            {
                NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSData *decData = [Encryptor AESDecryptData:encString];
                if(decData)
                    returnData = [NSJSONSerialization JSONObjectWithData:decData options:kNilOptions error:&error];
            }
            else
            {
                returnData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            }
            
            if(!error)
            {
                if((long)[httpResponse statusCode] == 200)
                {
                    if([returnData objectForKey:@"content"])
                    {
                        NSArray *content = [returnData objectForKey:@"content"];
                        if(content.count > 0) self->dukcapilData = content[0];
                        self->dukcapilStatus = 200;
                    }
                    else
                    {
                        self->dukcapilStatus = 500;
                        self->pageError = [returnData objectForKey:@"message"];
                    }
                }
                else if((long)[httpResponse statusCode] == 401)
                {
                    if([returnData objectForKey:@"message"])
                    {
                        self->dukcapilStatus = 401;
                        self->pageError = [returnData objectForKey:@"message"];
                    }
                    else
                    {
                        self->dukcapilStatus = 500;
                        self->pageError = NSLocalizedString(@"RESPONSE_NOT_VALID", @"");
                    }
                }
                else if((long)[httpResponse statusCode] == 400)
                {
                    self->dukcapilStatus = 400;
                    self->pageError = NSLocalizedString(@"NIK_NOT_FOUND", @"");
                }
                else
                {
                    self->dukcapilStatus = 500;
                    self->pageError = NSLocalizedString(@"INTERNAL_SERVER_ERROR", @"");
                }
            }
            else
            {
                self->dukcapilStatus = 500;
                self->pageError = NSLocalizedString(@"RESPONSE_NOT_VALID", @"");
            }
        }
        else
        {
            self->dukcapilStatus = 500;
            self->pageError = NSLocalizedString(@"FAILED_FETCH_DUKCAPIL", @"");
        }
        dispatch_group_leave(self->referenceGroup);
    }];
    
    [postDataTask resume];
}

- (BOOL):(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {

            return NO;
        }
    }
    return YES;
}

-(NSString *) maskingName:(NSString *)nama{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < [nama length]; i++) {
        NSString *ch = [nama substringWithRange:NSMakeRange(i, 1)];
        [array addObject:ch];
    }
    
    for (int i = 0; i < [array count]; i++) {
        if ((i%2 == 0 || i%3 == 0) && i != 0 ){
            if(![array[i] isEqualToString:@" "]){
                array[i] = @"*";
            }
        }
    }
    
    return [array componentsJoinedByString:@""];
}

- (void) showAlertMessage:(UIViewController *)viewController title:(NSString *)title message:(NSString *)message {

    UIAlertController * alert = [UIAlertController alertControllerWithTitle : title
                                                                    message : message
                                                             preferredStyle : UIAlertControllerStyleAlert];

    UIAlertAction * ok = [UIAlertAction
                          actionWithTitle:@"OK"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          { }];

    [alert addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewController presentViewController:alert animated:YES completion:nil];
    });
}

- (void) updateTanggalLahirField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)self.fieldTanggalLahir.inputView;
    self.fieldTanggalLahir.text = [self formatDate:picker.date withFormat:@"dd/MM/yyyy"];
    selectedDate = [self formatDate:picker.date withFormat:@"yyyy-MM-dd"];
}

- (NSString *)formatDate:(NSDate *)date withFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:format];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

@end
