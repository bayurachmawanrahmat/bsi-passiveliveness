//
//  PernyataanNasabahController.h
//  BSM-Mobile
//
//  Created by ARS on 25/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PernyataanNasabahDelegate;

@interface PernyataanNasabahController : OnboardingRootController

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *snkView;
@property (weak, nonatomic) IBOutlet UIView *akadMudharabahView;
@property (weak, nonatomic) IBOutlet UIView *akadWadiahView;
- (IBAction)closeModalAction:(id)sender;
- (IBAction)agreeAction:(id)sender;
@property (weak, nonatomic) id<PernyataanNasabahDelegate> delegate;

@end

@protocol PernyataanNasabahDelegate <NSObject>

@required
- (void) didSetujuPernyataanNasabah:(PernyataanNasabahController *) controller;

@end

NS_ASSUME_NONNULL_END
