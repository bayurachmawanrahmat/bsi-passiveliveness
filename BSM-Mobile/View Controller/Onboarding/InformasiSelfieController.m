//
//  InformasiSelfieController.m
//  BSM-Mobile
//
//  Created by ARS on 17/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "InformasiSelfieController.h"
#import "CameraOverlayController.h"
#import "CameraOverlayView.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"
#import "InformasiKTPDukcapilController.h"
#import "InformasiStatusController.h"

@interface InformasiSelfieController ()
{
    Encryptor *encryptor;
}
@end

@implementation InformasiSelfieController

static const NSString *ScreenName = @"InformasiSelfieScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encryptor = [[Encryptor alloc] init];
    
    self.contentView.layer.cornerRadius = 15.f;
    self.btnRetake.layer.cornerRadius = 24.f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.view.hidden = NO;
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    if(!_imagePicked && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _imagePicked = [self loadImageFromLocalStorageWithFileName:@"FOTO_DIRI" ofType:@"jpg" inDirectory:documentsDirectory];
        
        if(!_imagePicked)
        {
            CameraOverlayController *overlayController = [[CameraOverlayController alloc] init];
            
            _cameraPicker = [[UIImagePickerController alloc] init];
            _cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            _cameraPicker.delegate = self;
            _cameraPicker.showsCameraControls = false;
            _cameraPicker.allowsEditing = false;
            _cameraPicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            
            CameraOverlayView *cameraView = (CameraOverlayView *)overlayController.view;
            cameraView.frame = _cameraPicker.view.frame;
            cameraView.textUnderBox.text = NSLocalizedString(@"DESC_KTP_POSITION_2", @"");
            [cameraView.textUnderBox sizeToFit];
            cameraView.constraintImageOverlayMarginBottom.constant = 132.0;
            // cameraView.constraintImageOverlayMarginBottom.constant = self.view.frame.size.height * 0.3;
            // cameraView.constraintImageOverlayMarginBottom.constant = 0;
            cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_selfie2-03.png"];
            [cameraView.navigationBar.topItem setTitle:@"Foto dengan KTP"];
            cameraView.delegate = self;
            
            _cameraPicker.cameraOverlayView = cameraView;
            
            [self presentViewController:_cameraPicker animated:YES completion:nil];
        }
        else
        {
            self.view.hidden = NO;
            [self setImageView];
        }
    }
    else self.view.hidden = NO;
    CGFloat yPos = self.imageView.frame.origin.y + 15.0;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:2 fromTotal:7];
}

- (void)didTakePhoto:(CameraOverlayView *)overlayView
{
    [_cameraPicker takePicture];
}

- (void)didBack:(CameraOverlayView *)overlayView
{
    if ([[encryptor getUserDefaultsObjectForKey:ONBOARDING_INPUT_DATA_IDENTITY_TYPE_KEY] isEqualToString:ONBOARDING_INPUT_DATA_IDENTITY_DUKCAPIL]) {
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"InformasiPribadiDukcapilScreen"];
        [_cameraPicker presentViewController:vc animated:YES completion:nil];
    } else {
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"InformasiPribadiThirdScreen"];
        [_cameraPicker presentViewController:vc animated:YES completion:nil];
    }
}

- (IBAction)hitBtnRetake:(id)sender {
    _imagePicked = nil;
    [self removeImageFromLocalStorage:@"FOTO_DIRI"];
    [self viewDidLoad];
    [self viewDidAppear:YES];
}

- (void)setImageView
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    CGRect rect = _imageView.frame;
    
    _imageHeight.constant = (screenHeight / 1.5) - 60;
    
    _imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, (screenWidth - 20), _imageHeight.constant);
    _imageView.image = _imagePicked;
    _imageView.clipsToBounds = YES;
}

- (IBAction)hitBtnBack:(id)sender {
    if ([[encryptor getUserDefaultsObjectForKey:ONBOARDING_INPUT_DATA_IDENTITY_TYPE_KEY] isEqualToString:ONBOARDING_INPUT_DATA_IDENTITY_DUKCAPIL]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        InformasiKTPDukcapilController *informasiKTPDukcapilController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiPribadiDukcapilScreen"];
        [self presentViewController:informasiKTPDukcapilController animated:YES completion:nil];
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        InformasiStatusController *informasiStatusController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiPribadiThirdScreen"];
        [self presentViewController:informasiStatusController animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    [_cameraPicker dismissViewControllerAnimated:YES completion:nil];
    
    _imagePicked = info[UIImagePickerControllerOriginalImage];
    
    [self setImageView];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if(_imagePicked)
            {
                // SAVE IMAGE
                [self saveImageToLogicalDocs:self.imagePicked withFilename:@"FOTO_DIRI" andSaveWithKey:ONBOARDING_INFORMASI_SELFIE_FILEID_KEY onCompletionPerformSegueWithIdentifier:identifier fromController:self];
            }
            else return YES;
            return NO;
        }
    }
    
    return YES;
}

@end
