//
//  PernyataanNasabahController.m
//  BSM-Mobile
//
//  Created by ARS on 25/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PernyataanNasabahController.h"

@interface PernyataanNasabahController ()

@end

@implementation PernyataanNasabahController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.80];
    self.contentView.layer.cornerRadius = 30.0f;
    self.contentView.layer.masksToBounds = YES;
}

- (void)closeModal
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)closeModalAction:(id)sender {
    [self closeModal];
}

- (IBAction)agreeAction:(id)sender {
    [self.delegate didSetujuPernyataanNasabah:self];
    [self closeModal];
}

@end
