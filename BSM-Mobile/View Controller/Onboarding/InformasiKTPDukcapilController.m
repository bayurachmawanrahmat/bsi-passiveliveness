//
//  informasiKTPDukcapilController.m
//  BSM-Mobile
//
//  Created by ARS on 16/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "InformasiKTPDukcapilController.h"
#import "CameraOverlayController.h"
#import "CameraOverlayView.h"
#import "AMPopTip.h"
#import "NSObject+FormInputCategory.h"
#import "UIDropdownSearch.h"
#import "CustomerOnboardingData.h"
#import "NikTerdaftarController.h"
#import "Encryptor.h"
#import "InformasiSelfieController.h"
#import "NameVerificationFailedController.h"
#import "Utility.h"

@interface InformasiKTPDukcapilController ()
{
    CGFloat screenWidth;
    CGFloat screenHeight;
    CGFloat defaultConstraintPendidikanTop;
    NSDictionary *formInputDatapribadi;
    NSDictionary *formInputDomisili;
    NSDictionary *formDropdownDatapribadi;
    dispatch_group_t referenceGroup;
    NSDictionary *dukcapilData;
    int dukcapilStatus;
    UIDropdownSearch *dropdownPropinsi;
    UIDropdownSearch *dropdownKabupatenKota;
    UIDropdownSearch *dropdownKecamatan;
    UIDropdownSearch *dropdownKelurahan;
    NSString *pageError;
    NikTerdaftarController *modalController;
    NameVerificationFailedController *nameVerifController;
    
    Encryptor *encryptor;
    UIRefreshControl *refreshControl;
    NSString *name;
    NSString *motherName;
    int verifNameAttempt;
    
    __weak IBOutlet NSLayoutConstraint *labelRwMarginRight;
    __weak IBOutlet NSLayoutConstraint *fieldRtWidth;
    __weak IBOutlet NSLayoutConstraint *fieldRwWidth;
    
}
@end

@implementation InformasiKTPDukcapilController

static const NSString *ScreenName = @"InformasiPribadiDukcapilScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    self.view.hidden = NO;
    self.contentView.layer.cornerRadius = 15.0f;
    self.btnRetake.layer.cornerRadius = 24.0f;

    referenceGroup = dispatch_group_create();
    
    [self refreshData];

    self.fieldNomorKtp.delegate = self;
    self.fieldNama.delegate = self;
    self.fieldNamaIbu.delegate = self;
    self.fieldAlamat.delegate = self;
    self.fieldZip.delegate = self;
    self.fieldRt.delegate = self;
    self.fieldRw.delegate = self;
    self.fieldKelurahan.delegate = self;
    self.fieldKecamatan.delegate = self;
    self.fieldKabupatenKota.delegate = self;
    self.fieldPropinsi.delegate = self;
    self.fieldPendidikan.delegate = self;
    
    modalController = [[NikTerdaftarController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    modalController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    nameVerifController = [[NameVerificationFailedController alloc] init];
    nameVerifController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    nameVerifController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    nameVerifController.delegate = self;
    
    UIImage *iconChecked = [UIImage imageNamed:@"ic_checkbox_active.png"];
    UIImage *iconUnchecked = [UIImage imageNamed:@"ic_checkbox_inactive.png"];
    
    [self.checkAlamatBeda setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
    [self.checkAlamatBeda setBackgroundImage:iconChecked forState:UIControlStateSelected];
    
    // input field toolbar numberpad
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"CANCEL", @"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"DONE", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.fieldNomorKtp.inputAccessoryView = numberToolbar;
    self.fieldZip.inputAccessoryView = numberToolbar;
    self.fieldRt.inputAccessoryView = numberToolbar;
    self.fieldRw.inputAccessoryView = numberToolbar;
    
    // input field toolbar text
    UIToolbar* textToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    textToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DONE", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(textToolbarDone)];
    [textToolbar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    [textToolbar setUserInteractionEnabled:YES];
    [textToolbar sizeToFit];
    self.fieldNama.inputAccessoryView = textToolbar;
    self.fieldNamaIbu.inputAccessoryView = textToolbar;
    
    CGFloat tWidth = (screenWidth / 2) - 20.0f;
    labelRwMarginRight.constant = tWidth;
    fieldRtWidth.constant = tWidth;
    fieldRwWidth.constant = tWidth;
    [self.view layoutIfNeeded];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [_scrollView setRefreshControl:refreshControl];
    } else {
        [_scrollView addSubview:refreshControl];
    }
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self setScrollView:_scrollView];
    [self setMainView:self.view];
    [self registerForKeyboardNotifications];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    NSNumber *verifNameAttemptNumber = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_ATTEMPT_NAME];
    if (verifNameAttemptNumber != nil && ![verifNameAttemptNumber isEqual:[NSNull null]]) {
        verifNameAttempt = verifNameAttemptNumber.intValue;
        
        if (verifNameAttempt >= 3) {
            // 0  = tidak valid, 1 valid
            // [encryptor setUserDefaultsObject:@"0" forKey:ONBOARDING_VERIFICATION_NAME_VALID];
            // [self presentViewController:self->nameVerifController animated:YES completion:nil];
            // return;
            verifNameAttempt = 2;
        }
    }
    
    if(!_imagePicked && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        NSString * documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _imagePicked = [self loadImageFromLocalStorageWithFileName:@"KTP" ofType:@"jpg" inDirectory:documentsDirectory];

        if(!_imagePicked)
        {
            CameraOverlayController *overlayController = [[CameraOverlayController alloc] init];

            _cameraPicker = [[UIImagePickerController alloc] init];
            _cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            _cameraPicker.delegate = self;
            _cameraPicker.showsCameraControls = false;
            _cameraPicker.allowsEditing = false;

            CameraOverlayView *cameraView = (CameraOverlayView *)overlayController.view;
            cameraView.frame = _cameraPicker.view.frame;
            cameraView.textUnderBox.text = NSLocalizedString(@"DESC_KTP_POSITION", @"");
            cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_ktp@2x.png"];
            [cameraView.navigationBar.topItem setTitle:NSLocalizedString(@"PHOTO_KTP", @"")];
            cameraView.delegate = self;

            _cameraPicker.cameraOverlayView = cameraView;

            [self presentViewController:_cameraPicker animated:NO completion:nil];
        }
        else
        {
            [self setImageView];
            self.view.hidden = NO;
        }
    }
    else self.view.hidden = NO;
    
    CGFloat yPos = self.scrollView.frame.origin.y;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:1 fromTotal:7];
}

- (void) refreshData
{
    [self showLoading];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    dropdownPropinsi = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownPropinsi.displayedStringKey = @"bsmname";
    [dropdownPropinsi getReferenceDataFromEndPoint:@"api/master/province/listAll" withKeysToStore:[[NSArray alloc] initWithObjects:@"bsmid", @"bsmname", @"fullcode", nil] andSortUsingKey:@"bsmname" andDispatchGroup:referenceGroup];
    dropdownPropinsi.delegate = self;
    
    dropdownKabupatenKota = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKabupatenKota.displayedStringKey = @"bsmname";
    dropdownKabupatenKota.delegate = self;
    
    dropdownKecamatan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKecamatan.displayedStringKey = @"nama";
    dropdownKecamatan.delegate = self;
    
    dropdownKelurahan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKelurahan.displayedStringKey = @"nama";
    dropdownKelurahan.delegate = self;
    
    NSMutableDictionary *listPendidikan = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/reference/listAllByKategori?kategori=PENDIDIKAN" dispatchGroup:referenceGroup returnData:listPendidikan];
    
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if(self->refreshControl)
            [self->refreshControl endRefreshing];
        
        if(self.fieldNomorKtp.text.length == 16 && [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY])
        {
            if(!self->dukcapilData)
            {
                if(self->dukcapilStatus == 401)
                {
                    [self presentViewController:self->modalController animated:YES completion:nil];
                } else {
                    [self showAlertErrorWithTitle:NSLocalizedString(@"SORRY", @"") andMessage:self->pageError sender:self];
                }
            }
            else [self setFormDataWithData:self->dukcapilData emptyFieldDomisili:!self.isAlamatBeda];
        }
        
        if(self->dropdownPropinsi.callApiSucess)
        {
            [self->dropdownPropinsi.tableView reloadData];
            self->dropdownPropinsi.displayedItems = self->dropdownPropinsi.allItems;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_PROVINCE", @"") sender:self];
        }
        
        if([[listPendidikan objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [listPendidikan objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldPendidikan.options = options;
            
            NSDictionary *rawSelectedPendidikan = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_PENDIDIKAN_KEY];
            if([rawSelectedPendidikan isKindOfClass:NSDictionary.class]) {
                NSMutableDictionary *selectedPendidikan = [[NSMutableDictionary alloc] init];
                [selectedPendidikan setObject:[rawSelectedPendidikan objectForKey:@"nama"] forKey:[rawSelectedPendidikan objectForKey:@"nilai"]];
                
                self->_fieldPendidikan.selectedData = selectedPendidikan;
                self->_fieldPendidikan.text = [rawSelectedPendidikan objectForKey:@"nama"];
            }
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_EDUCATION", @"") sender:self];
        }
    });
    
    formInputDatapribadi = [[NSDictionary alloc] initWithObjectsAndKeys:
                            _fieldNomorKtp, ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY,
                            nil];
    
    formInputDomisili = [[NSDictionary alloc] initWithObjectsAndKeys:
                         _fieldAlamat, ONBOARDING_INFORMASI_PRIBADI_ALAMAT_KEY,
                         _fieldZip, ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY,
                         _fieldRt, ONBOARDING_INFORMASI_PRIBADI_RT_KEY,
                         _fieldRw, ONBOARDING_INFORMASI_PRIBADI_RW_KEY,
                         nil];
    
    self.isAlamatBeda = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY] integerValue];
    [self toggleSubViewDisplay:self.isAlamatBeda];
    
    [self setDropDownData];
    [self setTextFieldData:formInputDatapribadi];
    [self setTextFieldData:formInputDomisili];
     [self setDropDownData:formDropdownDatapribadi];
    if([encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP]) {
        // [_fieldNama setText:[self maskingName:[encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP]]];
        name = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP];
        [_fieldNama setText:name];
    }
    if([encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU]) {
        motherName = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU];
        [_fieldNamaIbu setText:motherName];
    }
    
    if(self.fieldNomorKtp.text.length == 16 && [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY])
        [self requestDukcapil];
}


- (void) setDropDownData
{
    dropdownPropinsi.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY];
    if([dropdownPropinsi.selectedData isKindOfClass:NSDictionary.class])
        _fieldPropinsi.text = [dropdownPropinsi.selectedData objectForKey:dropdownPropinsi.displayedStringKey];
    
    dropdownKabupatenKota.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY];
    if([dropdownKabupatenKota.selectedData isKindOfClass:NSDictionary.class])
        _fieldKabupatenKota.text = [dropdownKabupatenKota.selectedData objectForKey:dropdownKabupatenKota.displayedStringKey];
    
    dropdownKecamatan.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KECAMATAN_KEY];
    if([dropdownKecamatan.selectedData isKindOfClass:NSDictionary.class])
        _fieldKecamatan.text = [dropdownKecamatan.selectedData objectForKey:dropdownKecamatan.displayedStringKey];
    
    dropdownKelurahan.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KELURAHAN_KEY];
    if([dropdownKelurahan.selectedData isKindOfClass:NSDictionary.class])
        _fieldKelurahan.text = [dropdownKelurahan.selectedData objectForKey:dropdownKelurahan.displayedStringKey];
}

-(void)toggleSubViewDisplay:(BOOL)isShowing
{
    if(isShowing)
    {
        self.checkAlamatBeda.selected = true;
        self.subView.hidden = NO;
        _contentViewHeight.constant = 0;
        CGRect frame = _contentView.frame;
        frame.size.height = screenHeight + 100;
        [_contentView setFrame:frame];
        while (_contentView.frame.size.height < 928) {
            frame.size.height += 10;
            [_contentView setFrame:frame];
            _contentViewHeight.constant += 10;
        }
        if (@available(iOS 13.0, *)) {
            self.constraintPendidikanTop.constant = 487;
        }
        else {
            self.constraintPendidikanTop.constant = 427;
        }
        self.isAlamatBeda = 1;
        [self emptyFieldDomisili];
    }
    else
    {
        self.checkAlamatBeda.selected = false;
        self.subView.hidden = YES;
        _contentViewHeight.constant = 0;
        CGRect frame = _contentView.frame;
        frame.size.height = screenHeight + 100;
        [_contentView setFrame:frame];
        while (_contentView.frame.size.height < 397) {
            frame.size.height += 10;
            [_contentView setFrame:frame];
            _contentViewHeight.constant += 10;
        }
        _contentViewHeight.constant = (447 - screenHeight);
        self.constraintPendidikanTop.constant = 10;
        self.isAlamatBeda = 0;
        [self setDomilisiWithData:dukcapilData];
    }
}

- (void)hitCheckAlamatBeda:(id)sender
{
    if(!self.checkAlamatBeda.selected) [self toggleSubViewDisplay:YES];
    else [self toggleSubViewDisplay:NO];
}

- (IBAction)hitBtnRetake:(id)sender {
    _imagePicked = nil;
    [self removeImageFromLocalStorage:@"KTP"];
    [self viewDidLoad];
    [self viewDidAppear:YES];
}

- (IBAction)hitBtnNext:(id)sender {
    if([self checkInput])
    {
        [self saveDataToLocal];
        
        // 0  = tidak valid, 1 valid
        [encryptor setUserDefaultsObject:@"1" forKey:ONBOARDING_VERIFICATION_NAME_VALID];
        
        if(_imagePicked)
        {
            // SAVE IMAGE
            [self saveImageToLogicalDocs:self.imagePicked withFilename:@"KTP" andSaveWithKey:ONBOARDING_INFORMASI_PRIBADI_KTP_FILEID_KEY onCompletionPerformSegueWithIdentifier:nil fromController:self withCompletion:^void{
                // navigate next
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                InformasiSelfieController *informasiSelfieController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiSelfieScreen"];
                [self presentViewController:informasiSelfieController animated:YES completion:nil];
            }];
        }
    }
}

- (void)didTakePhoto:(CameraOverlayView *)overlayView
{
    [_cameraPicker takePicture];
}

- (void)didBack:(CameraOverlayView *)overlayView
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"JenisTabunganScreen"];
    [_cameraPicker presentViewController:vc animated:YES completion:nil];
}

- (void)saveDataToLocal{
    [self saveTextFieldData:formInputDatapribadi];
    // [self saveDropDownData:formDropdownDatapribadi];
    NSMutableDictionary *pendidikanObject = [[NSMutableDictionary alloc] init];
    [pendidikanObject setObject:_fieldPendidikan.selectedData.allValues[0] forKey:@"nama"];
    [pendidikanObject setObject:_fieldPendidikan.selectedData.allKeys[0] forKey:@"nilai"];
    [encryptor setUserDefaultsObject:pendidikanObject forKey:ONBOARDING_INFORMASI_PRIBADI_PENDIDIKAN_KEY];
    [encryptor setUserDefaultsObject:_fieldNama.text forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP];
    [encryptor setUserDefaultsObject:_fieldNamaIbu.text forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU];
    [encryptor setUserDefaultsObject:[NSNumber numberWithInteger:[self isAlamatBeda]] forKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY];
    if([self isAlamatBeda])
    {
        [self saveTextFieldData:formInputDomisili];
        [encryptor setUserDefaultsObject:dropdownPropinsi.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY];
        [encryptor setUserDefaultsObject:dropdownKabupatenKota.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY];
        [encryptor setUserDefaultsObject:dropdownKecamatan.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_KECAMATAN_KEY];
        [encryptor setUserDefaultsObject:dropdownKelurahan.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_KELURAHAN_KEY];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    [_cameraPicker dismissViewControllerAnimated:YES completion:nil];
    
    _imagePicked = info[UIImagePickerControllerOriginalImage];
    
    [self setImageView];
}

-(void)cancelNumberPad{
    if([self.fieldNomorKtp isFirstResponder])
    {
        [self.fieldNomorKtp resignFirstResponder];
        self.fieldNomorKtp.text = @"";
    }
    else if ([self.fieldZip isFirstResponder])
    {
        [self.fieldZip resignFirstResponder];
        self.fieldZip.text = @"";
    }
}

-(void)doneWithNumberPad{
    if([self.fieldNomorKtp isFirstResponder])
    {
        [self.fieldNomorKtp resignFirstResponder];
    }
    else if ([self.fieldZip isFirstResponder])
    {
        [self.fieldZip resignFirstResponder];
    }
    else if ([self.fieldRt isFirstResponder])
    {
        [self.fieldRt resignFirstResponder];
    }
    else if ([self.fieldRw isFirstResponder])
    {
        [self.fieldRw resignFirstResponder];
    }
}

-(void)textToolbarDone {
    if ([self.fieldNama isFirstResponder])
    {
        [self.fieldNama resignFirstResponder];
    } else if ([self.fieldNamaIbu isFirstResponder])
    {
        [self.fieldNamaIbu resignFirstResponder];
    }
}

-(void)checkKTP{
    if(self.fieldNomorKtp.text.length > 16 || self.fieldNomorKtp.text.length < 16)
    {
        [self showErrorLabel:NSLocalizedString(@"KTP_NOT_VALID", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
    }
    else
    {
        [self.fieldNomorKtp resignFirstResponder];
        
        [self showLoading];
        [self requestDukcapil];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            [self toggleSubViewDisplay:NO];
            if(!self->dukcapilData)
            {
                if(self->dukcapilStatus == 401)
                {
                    [self presentViewController:self->modalController animated:YES completion:nil];
                } else {
                    [self showAlertErrorWithTitle:NSLocalizedString(@"SORRY", @"") andMessage:self->pageError sender:self];
                }
            }
            else [self setFormDataWithData:self->dukcapilData emptyFieldDomisili:YES];
        });
    }
}

- (BOOL)checkInput
{
    BOOL isValid = TRUE;
    BOOL isNameValid = TRUE;
    BOOL isMotherNameValid = TRUE;
    if([_fieldNomorKtp.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"KTP_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    } else if(_fieldNomorKtp.text.length > 16 || ![Utility isNumericOnly:_fieldNomorKtp.text] || dukcapilStatus == 0) {
        [self showErrorLabel:NSLocalizedString(@"KTP_NOT_VALID", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    }
    else if (dukcapilStatus == 401)
    {
        [self showErrorLabel:NSLocalizedString(@"NIK_ALREADY_REGISTERED", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    }
    else if (dukcapilStatus == 421)
    {
        [self showErrorLabel:NSLocalizedString(@"DUKCAPIL_NOT_ACCESSIBLE", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    }
    
    else if (dukcapilStatus == 403)
    {
        [self showErrorLabel:NSLocalizedString(@"AGE_NOT_VALID", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    }
    else if (dukcapilStatus == 400)
    {
        [self showErrorLabel:NSLocalizedString(@"DUKCAPIL_NOT_FOUND", @"") withUiTextfield:_fieldNomorKtp];
        [self inputHasError:YES textField:_fieldNomorKtp];
        isValid = FALSE;
    }
    else if (dukcapilStatus == 500)
    {
        [self showAlertErrorWithTitle:NSLocalizedString(@"SORRY", @"") andMessage:NSLocalizedString(@"RESPONSE_NOT_VALID", @"") sender:self];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldNomorKtp];
    
    if([_fieldNama.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"NAME_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldNama];
        [self inputHasError:YES textField:_fieldNama];
        isValid = FALSE;
    } else if(_fieldNama.text.length > 35 || ![Utility isAlphabetOnly:_fieldNama.text]) {
        [self showErrorLabel:NSLocalizedString(@"NAME_NOT_VALID", @"") withUiTextfield:_fieldNama];
        [self inputHasError:YES textField:_fieldNama];
        isValid = FALSE;
    } else if (name == nil || [name isEqual:[NSNull null]]) {
        isNameValid = FALSE;
    }
    else if (![name isEqualToString:_fieldNama.text]) {
        isNameValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldNama];
    
    if([_fieldNamaIbu.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"MOTHER_NAME_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldNamaIbu];
        [self inputHasError:YES textField:_fieldNamaIbu];
        isValid = FALSE;
    } else if(_fieldNamaIbu.text.length > 35 || ![Utility isAlphabetOnly:_fieldNamaIbu.text]) {
        [self showErrorLabel:NSLocalizedString(@"MOTHER_NAME_NOT_VALID", @"") withUiTextfield:_fieldNamaIbu];
        [self inputHasError:YES textField:_fieldNamaIbu];
        isValid = FALSE;
    } else if (motherName == nil || [motherName isEqual:[NSNull null]]) {
        isMotherNameValid = FALSE;
    } else if (![motherName isEqualToString:_fieldNamaIbu.text]) {
        isMotherNameValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldNamaIbu];
    
    if (!(isNameValid && isMotherNameValid) && isValid && ![_fieldNomorKtp.text isEqualToString:@""] && dukcapilStatus == 200) {
        [self increaseVerifNameAttemp];
    }
    
    if ([_fieldPendidikan.text isEqualToString:@""]) {
        [self showErrorLabel:NSLocalizedString(@"EDUCATION_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldPendidikan];
        [self inputHasError:YES textField:_fieldPendidikan];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldPendidikan];
    
    if(_checkAlamatBeda.selected)
    {
        if([_fieldAlamat.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"ADDRESS_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldAlamat];
            [self inputHasError:YES textField:_fieldAlamat];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldAlamat];
        
        if([_fieldZip.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"POSTCODE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldZip];
            [self inputHasError:YES textField:_fieldZip];
            isValid = FALSE;
        }
        else if (_fieldZip.text.length != 5)
        {
            [self showErrorLabel:NSLocalizedString(@"POSTCODE_NOT_VALID", @"") withUiTextfield:_fieldZip];
            [self inputHasError:YES textField:_fieldZip];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldZip];
        
        if([_fieldRt.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"RT_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldRt];
            [self inputHasError:YES textField:_fieldRt];
            isValid = FALSE;
        }
        else if (_fieldRt.text.length > 3)
        {
            [self showErrorLabel:NSLocalizedString(@"RT_NOT_VALID", @"") withUiTextfield:_fieldRt];
            [self inputHasError:YES textField:_fieldRt];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldRt];
        
        if([_fieldRw.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"RW_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldRw];
            [self inputHasError:YES textField:_fieldRw];
            isValid = FALSE;
        }
        else if (_fieldRw.text.length > 3)
        {
            [self showErrorLabel:NSLocalizedString(@"RW_NOT_VALID", @"") withUiTextfield:_fieldRw];
            [self inputHasError:YES textField:_fieldRw];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldRw];
        
        if ([_fieldKelurahan.text isEqualToString:@""]) {
            [self showErrorLabel:NSLocalizedString(@"VILLAGE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKelurahan];
            [self inputHasError:YES textField:_fieldKelurahan];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldKelurahan];
        
        if ([_fieldKecamatan.text isEqualToString:@""]) {
            [self showErrorLabel:NSLocalizedString(@"DISTRICT_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKecamatan];
            [self inputHasError:YES textField:_fieldKecamatan];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldKecamatan];
        
        if ([_fieldKabupatenKota.text isEqualToString:@""]) {
            [self showErrorLabel:NSLocalizedString(@"CITY_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKabupatenKota];
            [self inputHasError:YES textField:_fieldKabupatenKota];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldKecamatan];
        
        if ([_fieldPropinsi.text isEqualToString:@""]) {
            [self showErrorLabel:NSLocalizedString(@"PROVINCE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldPropinsi];
            [self inputHasError:YES textField:_fieldPropinsi];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldPropinsi];
    }
    
    return isValid;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self inputHasError:NO textField:textField];
    
    if(textField == _fieldPropinsi)
    {
        [self presentViewController:dropdownPropinsi animated:YES completion:nil];
        [self.fieldKabupatenKota setEnabled:FALSE];
        [self.fieldKecamatan setEnabled:FALSE];
        [self.fieldKelurahan setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKabupatenKota)
    {
        [self presentViewController:dropdownKabupatenKota animated:YES completion:nil];
        [self.fieldKelurahan setEnabled:FALSE];
        [self.fieldKecamatan setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKecamatan)
    {
        [self presentViewController:dropdownKecamatan animated:YES completion:nil];
        [self.fieldKelurahan setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKelurahan)
    {
        [self presentViewController:dropdownKelurahan animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setActiveField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(self.isAlamatBeda && textField != _fieldAlamat)
    {
        [[self.view viewWithTag:(textField.tag + 1)] becomeFirstResponder];
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _fieldAlamat && textField.text.length == 35 && ![string isEqualToString:@""]) return NO;
    else if(textField == _fieldZip && textField.text.length == 5 && ![string isEqualToString:@""]) return NO;
    else if(textField == _fieldNomorKtp && textField.text.length == 16 && ![string isEqualToString:@""]) return NO;
    else if(textField == _fieldRt && textField.text.length == 3 && ![string isEqualToString:@""]) return NO;
    else if(textField == _fieldRw && textField.text.length == 3 && ![string isEqualToString:@""]) return NO;
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if([self.fieldNomorKtp isFirstResponder])
    {
        [self checkKTP];
    }
    return YES;
}

- (void)dropdownSearch:(id)uiDropdownSearch didSelectRowWithData:(NSDictionary *)data
{
    if (uiDropdownSearch == dropdownPropinsi) {
        [self.fieldPropinsi setText:[data objectForKey:@"bsmname"]];
        [self.fieldKabupatenKota setText:@""];
        [self.fieldKecamatan setText:@""];
        [self.fieldKelurahan setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/city/listByD1?fullcode=%@", [dropdownPropinsi.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKabupatenKota getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"bsmid", @"bsmname", @"fullcode", nil] andSortUsingKey:@"bsmname" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKabupatenKota.callApiSucess)
            {
                self->dropdownKabupatenKota.displayedItems = self->dropdownKabupatenKota.allItems;
                [self->dropdownKabupatenKota.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_CITY", @"") sender:self];
            }
        });
        [self.fieldKabupatenKota setEnabled:TRUE];
    }
    
    else if (uiDropdownSearch == dropdownKabupatenKota)
    {
        [self.fieldKabupatenKota setText:[data objectForKey:@"bsmname"]];
        [self.fieldKecamatan setText:@""];
        [self.fieldKelurahan setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/district/listByD2?fullcode=%@", [dropdownKabupatenKota.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKecamatan getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"dati3id", @"nama", @"fullcode", nil] andSortUsingKey:@"nama" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKecamatan.callApiSucess)
            {
                self->dropdownKecamatan.displayedItems = self->dropdownKecamatan.allItems;
                [self->dropdownKecamatan.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_DISTRICT", @"") sender:self];
            }
        });
        [self.fieldKecamatan setEnabled:TRUE];
    }
    
    else if (uiDropdownSearch == dropdownKecamatan)
    {
        [self.fieldKecamatan setText:[data objectForKey:dropdownKecamatan.displayedStringKey]];
        [self.fieldKelurahan setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/village/listByD3?fullcode=%@", [dropdownKecamatan.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKelurahan getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"dati4id", @"nama", @"fullcode", nil] andSortUsingKey:@"nama" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKelurahan.callApiSucess)
            {
                self->dropdownKelurahan.displayedItems = self->dropdownKelurahan.allItems;
                [self->dropdownKelurahan.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_VILLAGE", @"") sender:self];
            }
        });
        [self.fieldKelurahan setEnabled:TRUE];
    }
    
    else if (uiDropdownSearch == dropdownKelurahan)
    {
        [self.fieldKelurahan setText:[data objectForKey:dropdownKelurahan.displayedStringKey]];
    }
}


- (void)setImageView
{
    CGRect rect = _imageView.frame;
    
    _imageHeight.constant = (screenHeight / 2.5);
    
    CGFloat heightDiff = _imageHeight.constant - 125;
    CGFloat heigthCurrent = _contentViewHeight.constant;
    _contentViewHeight.constant = heigthCurrent + heightDiff;
    
    _imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, (screenWidth - 20), _imageHeight.constant);
    _imageView.image = _imagePicked;
    _imageView.clipsToBounds = YES;
}

- (void) requestDukcapil
{
    dispatch_group_enter(referenceGroup);
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSString *nik = self.fieldNomorKtp.text;
    NSString *endpoint = [NSString stringWithFormat:@"api/onboarding/checkNik?nik=%@", nik];
    NSURL *url = [NSURL URLWithString:[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:endpoint]]];
    NSString *token = [CustomerOnboardingData getToken];
    NSLog(@"---------------- %@", url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [request setValue:token forHTTPHeaderField:@"Authorization"];
    dukcapilData = nil;
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error)
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *returnData;
            if(ONBOARDING_SAFE_MODE)
            {
                NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSData *decData = [Encryptor AESDecryptData:encString];
                if(decData)
                    returnData = [NSJSONSerialization JSONObjectWithData:decData options:kNilOptions error:&error];
            }
            else
            {
                returnData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            }
            
            if(!error)
            {
                if((long)[httpResponse statusCode] == 200)
                {
                    if([returnData objectForKey:@"content"])
                    {
                        NSArray *content = [returnData objectForKey:@"content"];
                        if(content.count > 0) self->dukcapilData = content[0];
                        self->dukcapilStatus = 200;
                    }
                    else
                    {
                        self->dukcapilStatus = 500;
                        self->pageError = [returnData objectForKey:@"message"];
                    }
                }
                else if((long)[httpResponse statusCode] == 401)
                {
                    if([returnData objectForKey:@"message"])
                    {
                        self->dukcapilStatus = 401;
                        self->pageError = [returnData objectForKey:@"message"];
                    }
                    else
                    {
                        self->dukcapilStatus = 500;
                        self->pageError = NSLocalizedString(@"RESPONSE_NOT_VALID", @"");
                    }
                }
                else if((long)[httpResponse statusCode] == 400)
                {
                    self->dukcapilStatus = 400;
                    self->pageError = NSLocalizedString(@"DUKCAPIL_NOT_FOUND", @"");
                }
                else if((long)[httpResponse statusCode] == 403)
                {
                    self->dukcapilStatus = 403;
                    self->pageError = NSLocalizedString(@"AGE_NOT_VALID", @"");
                }
                else if((long)[httpResponse statusCode] == 421)
                {
                    self->dukcapilStatus = 421;
                    self->pageError = NSLocalizedString(@"DUKCAPIL_NOT_ACCESSIBLE", @"");
                }
                else
                {
                    self->dukcapilStatus = 500;
                    self->pageError = NSLocalizedString(@"RESPONSE_NOT_VALID", @"");
                }
            }
            else
            {
                self->dukcapilStatus = 500;
                self->pageError = NSLocalizedString(@"RESPONSE_NOT_VALID", @"");
            }
        }
        else
        {
            self->dukcapilStatus = 500;
            self->pageError = NSLocalizedString(@"FAILED_FETCH_DUKCAPIL", @"");
        }
        dispatch_group_leave(self->referenceGroup);
    }];
    
    [postDataTask resume];
}

-(void) emptyFieldDomisili
{
    self.fieldAlamat.text = @"";
    self.fieldPropinsi.text = @"";
    self.fieldKabupatenKota.text = @"";
    self.fieldKecamatan.text = @"";
    self.fieldKelurahan.text = @"";
    self.fieldRt.text = @"";
    self.fieldRw.text = @"";
    self.fieldZip.text = @"";
}

- (void) setFormDataWithData:(NSDictionary *)data emptyFieldDomisili:(BOOL)emptyFields
{
    name = [data objectForKey:@"NAMA_LGKP"];
    motherName = [data objectForKey:@"NAMA_LGKP_IBU"];
    
    // disbale auto input name
    // NSString *name = [self maskingName:getName];
    // self.fieldNama.text = name;
    if(emptyFields) [self emptyFieldDomisili];
    if(data)
    {
        if(![[data objectForKey:@"DUSUN"] isKindOfClass:NSNull.class]) [encryptor setUserDefaultsObject:[data objectForKey:@"DUSUN"] forKey:ONBOARDING_DUKCAPIL_DUSUN];
        [encryptor setUserDefaultsObject:@"ID" forKey:ONBOARDING_DUKCAPIL_WARGANEGARA];
        [encryptor setUserDefaultsObject:[data objectForKey:@"AGAMA"] forKey:ONBOARDING_DUKCAPIL_AGAMA];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_KK"] forKey:ONBOARDING_DUKCAPIL_NO_KK];
        [encryptor setUserDefaultsObject:[data objectForKey:@"ALAMAT"] forKey:ONBOARDING_DUKCAPIL_ALAMAT];
        [encryptor setUserDefaultsObject:[data objectForKey:@"ID_PROV"] forKey:ONBOARDING_DUKCAPIL_ID_PROV];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_PROP"] forKey:ONBOARDING_DUKCAPIL_NO_PROP];
        [encryptor setUserDefaultsObject:[data objectForKey:@"PROP_NAME"] forKey:ONBOARDING_DUKCAPIL_PROP_NAME];
        [encryptor setUserDefaultsObject:[data objectForKey:@"ID_KAB"] forKey:ONBOARDING_DUKCAPIL_ID_KAB];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_KAB"] forKey:ONBOARDING_DUKCAPIL_NO_KAB];
        [encryptor setUserDefaultsObject:[data objectForKey:@"KAB_NAME"] forKey:ONBOARDING_DUKCAPIL_KAB_NAME];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_KEC"] forKey:ONBOARDING_DUKCAPIL_NO_KEC];
        [encryptor setUserDefaultsObject:[data objectForKey:@"KEC_NAME"] forKey:ONBOARDING_DUKCAPIL_KEC_NAME];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_KEL"] forKey:ONBOARDING_DUKCAPIL_NO_KEL];
        [encryptor setUserDefaultsObject:[data objectForKey:@"KEL_NAME"] forKey:ONBOARDING_DUKCAPIL_KEL_NAME];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_RT"] forKey:ONBOARDING_DUKCAPIL_NO_RT];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_RW"] forKey:ONBOARDING_DUKCAPIL_NO_RW];
        [encryptor setUserDefaultsObject:[data objectForKey:@"GOL_DARAH"] forKey:ONBOARDING_DUKCAPIL_GOL_DARAH];
        [encryptor setUserDefaultsObject:[data objectForKey:@"PDDK_AKH"] forKey:ONBOARDING_DUKCAPIL_PDDK_AKH];
        [encryptor setUserDefaultsObject:[data objectForKey:@"STAT_HBKEL"] forKey:ONBOARDING_DUKCAPIL_STAT_HBKEL];
        [encryptor setUserDefaultsObject:[data objectForKey:@"TMPT_LHR"] forKey:ONBOARDING_DUKCAPIL_TMPT_LAHIR];
        [encryptor setUserDefaultsObject:[data objectForKey:@"TGL_LHR"] forKey:ONBOARDING_DUKCAPIL_TGL_LHR];
        [encryptor setUserDefaultsObject:[data objectForKey:@"EKTP_STATUS"] forKey:ONBOARDING_DUKCAPIL_EKTP_STATUS];
        [encryptor setUserDefaultsObject:[data objectForKey:@"JENIS_KLMIN"] forKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN];
        [encryptor setUserDefaultsObject:[data objectForKey:@"JENIS_PKRJN"] forKey:ONBOARDING_DUKCAPIL_JENIS_PKRJN];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NAMA_MASKING"] forKey:ONBOARDING_DUKCAPIL_NAMA_MASKING];
        // disable set nama & nama ibu otomatis
        // [encryptor setUserDefaultsObject:[data objectForKey:@"NAMA_LGKP"] forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP];
        // [encryptor setUserDefaultsObject:[data objectForKey:@"NAMA_LGKP_IBU"] forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_IBU];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NAMA_LGKP_AYAH"] forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_AYAH];
        [encryptor setUserDefaultsObject:[data objectForKey:@"STATUS_KAWIN"] forKey:ONBOARDING_DUKCAPIL_STATUS_KAWIN];

        
        // IF ALAMAT SAMA DENGAN KTP
        if(!self.isAlamatBeda) [self setDomilisiWithData:data];
    }
}

- (void)setDomilisiWithData:(NSDictionary *)data
{
    if(data)
    {
        NSDictionary *provinsi = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  [data objectForKey:@"ID_PROV"], @"bsmid",
                                  [data objectForKey:@"PROP_NAME"], @"bsmname",
                                  nil];
        NSDictionary *kabkota = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 [data objectForKey:@"ID_KAB"], @"bsmid",
                                 [data objectForKey:@"KAB_NAME"], @"bsmname",
                                 nil];
        NSDictionary *kec = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [data objectForKey:@"KEC_NAME"], dropdownKecamatan.displayedStringKey,
                             nil];
        NSDictionary *kel = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [data objectForKey:@"KEL_NAME"], dropdownKelurahan.displayedStringKey,
                             nil];
        [encryptor setUserDefaultsObject:provinsi forKey:ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY];
        [encryptor setUserDefaultsObject:kabkota forKey:ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY];
        [encryptor setUserDefaultsObject:kec forKey:ONBOARDING_INFORMASI_PRIBADI_KECAMATAN_KEY];
        [encryptor setUserDefaultsObject:kel forKey:ONBOARDING_INFORMASI_PRIBADI_KELURAHAN_KEY];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_RT"] forKey:ONBOARDING_INFORMASI_PRIBADI_RT_KEY];
        [encryptor setUserDefaultsObject:[data objectForKey:@"NO_RW"] forKey:ONBOARDING_INFORMASI_PRIBADI_RW_KEY];
        [encryptor setUserDefaultsObject:[data objectForKey:@"KODE_POS"] forKey:ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY];
        [encryptor setUserDefaultsObject:[data objectForKey:@"KODE_POS"] forKey:ONBOARDING_DUKCAPIL_ZIP];
        [encryptor setUserDefaultsObject:[data objectForKey:@"ALAMAT"] forKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_KEY];
    }
}

- (BOOL):(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {

            return NO;
        }
    }
    return YES;
}

-(NSString *) maskingName:(NSString *)nama{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < [nama length]; i++) {
        NSString *ch = [nama substringWithRange:NSMakeRange(i, 1)];
        [array addObject:ch];
    }
    
    for (int i = 0; i < [array count]; i++) {
        if ((i%2 == 0 || i%3 == 0) && i != 0 ){
            if(![array[i] isEqualToString:@" "]){
                array[i] = @"*";
            }
        }
    }
    
    return [array componentsJoinedByString:@""];
}

- (void) showAlertMessage:(UIViewController *)viewController title:(NSString *)title message:(NSString *)message {

    UIAlertController * alert = [UIAlertController alertControllerWithTitle : title
                                                                    message : message
                                                             preferredStyle : UIAlertControllerStyleAlert];

    UIAlertAction * ok = [UIAlertAction
                          actionWithTitle:@"OK"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          { }];

    [alert addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewController presentViewController:alert animated:YES completion:nil];
    });
}

-(void) increaseVerifNameAttemp {
    verifNameAttempt = verifNameAttempt + 1;
    NSNumber *attempt = [NSNumber numberWithInt:(verifNameAttempt)];
    [encryptor setUserDefaultsObject:attempt forKey:ONBOARDING_ATTEMPT_NAME];
    
    if (verifNameAttempt >= 3) {
        // 0  = tidak valid, 1 valid
        [encryptor setUserDefaultsObject:@"0" forKey:ONBOARDING_VERIFICATION_NAME_VALID];
        [self presentViewController:self->nameVerifController animated:YES completion:nil];
        return;
    }
    
    [self showAlertMessage:self title:NSLocalizedString(@"DATA_CANNOT_BE_VERIFIED", @"") message:NSLocalizedString(@"MOTHER_NAME_AND_NAME_NOT_VALID", @"")];
    [self inputHasError:YES textField:_fieldNama];
}

- (void)onNextNameFailed {
    [self saveDataToLocal];
    
    if(_imagePicked)
    {
        // SAVE IMAGE
        [self saveImageToLogicalDocs:self.imagePicked withFilename:@"KTP" andSaveWithKey:ONBOARDING_INFORMASI_PRIBADI_KTP_FILEID_KEY onCompletionPerformSegueWithIdentifier:nil fromController:self withCompletion:^void{
            
            NSString *ktpfileId = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KTP_FILEID_KEY];
            
            // navigate next
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
            InformasiSelfieController *informasiSelfieController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiSelfieScreen"];
            [self presentViewController:informasiSelfieController animated:YES completion:nil];
        }];
    }
}

@end
