//
//  ProsesVerifikasiController.h
//  BSM-Mobile
//
//  Created by ARS on 04/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProsesVerifikasiController.h"
#import "ViewController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProsesVerifikasiController : OnboardingRootController <UINavigationControllerDelegate>

@property (strong, nonatomic) UIImage *imageVerification;
- (IBAction)hitBtnClose:(id)sender;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
- (IBAction)hitBtnCekStatus:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCekStatus;

@end

NS_ASSUME_NONNULL_END
