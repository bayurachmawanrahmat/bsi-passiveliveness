//
//  InformasiNPWPController.h
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ViewController.h"
#import "CameraOverlayView.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiNPWPController : OnboardingRootController <UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CameraOverlayDelegate>

@property (strong, nonatomic) UIImagePickerController *cameraPicker;
@property (strong, nonatomic) UIImage *imagePicked;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (weak, nonatomic) IBOutlet UITextField *fieldNomorNPWP;
@property (weak, nonatomic) IBOutlet UIButton *btnRetake;
- (IBAction)hitBtnRetake:(id)sender;

- (BOOL)checkInput;
- (void)setImageView;

@end

NS_ASSUME_NONNULL_END
