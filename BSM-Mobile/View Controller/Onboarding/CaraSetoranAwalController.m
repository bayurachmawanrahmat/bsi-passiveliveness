//
//  CaraSetoranAwalController.m
//  BSM-Mobile
//
//  Created by ARS on 26/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CaraSetoranAwalController.h"
#import "OnboardingButton.h"
#import "TabunganBerhasilController.h"
#import "HomeViewController.h"
#import "OnboardingTemplate01ViewController.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"
#import "Encryptor.h"
#import "NCreatePasswordViewController.h"
#import "MenuViewController.h"

@interface CaraSetoranAwalController ()
{
    Encryptor *enc;
    CabangViewController *cabangVC;
    Rekening *rekeningInfo;
    BOOL childHasLoaded;
}
@property (weak, nonatomic) IBOutlet UILabel *labelNomorRekening;
@property (weak, nonatomic) IBOutlet UILabel *labelNamaNasabah;
@end

@implementation CaraSetoranAwalController

- (instancetype)initWithRekening:(Rekening *)rekening
{
    self = [super init];
    if (self) {
        self->rekeningInfo = rekening;
        self->childHasLoaded = NO;
        enc = [[Encryptor alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    
    if(rekeningInfo)
    {
        [_labelNomorRekening setText:[NSString stringWithFormat:@": %@", rekeningInfo.nomorRekening]];
        [_labelNamaNasabah setText:[NSString stringWithFormat:@": %@", rekeningInfo.namaNasabah]];
    }
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTappedHandler)];
    [self.view addGestureRecognizer:viewTap];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    cabangVC = [sb instantiateViewControllerWithIdentifier:@"CabangViewScreen"];
    [cabangVC setShowDetailOnSelectRow:YES];
    cabangVC.modalPresentationStyle = UIModalPresentationFullScreen;
    cabangVC.delegate = self;
}

- (void)childHasLoaded:(OnboardingTemplate01ViewController *)parent
{
    if(!self->childHasLoaded)
    {
        [parent.navigationBar.topItem setTitle:NSLocalizedString(@"ACCOUNT_DEPOSIT_TEXT", @"How to deposit")];
        [parent.navigationBarButtonLeft setTarget:self];
        [parent.navigationBarButtonLeft setAction:@selector(btnBackTappedHandler)];
        
        [self setBoxShadow:self.box1];
        [self setBoxShadow:self.box2];
        
        parent.paddingViewHeight.constant -= 45.0;
        parent.contentViewHeight.constant += 45.0;

        OnboardingButton *mainButton = [[OnboardingButton alloc] initWithFrame:CGRectMake(10.0, parent.contentView.frame.size.height - mainButtonMargin, self.view.frame.size.width - 20.0, mainButtonHeight)];
        [mainButton setTitle:@"Mulai Bertransaksi" forState:UIControlStateNormal];
        [self.view addSubview:mainButton];
        [mainButton addTarget:self action:@selector(mainButtonDidTappedHandler:) forControlEvents:UIControlEventTouchUpInside];
        
        // parent.containerViewTop.constant = -(parent.navigationBar.frame.size.height);
        [parent.view layoutIfNeeded];
        
        self->childHasLoaded = YES;
    }
}

- (void) setBoxShadow:(UIView *)box
{
    [box.layer setShadowOpacity:0.25];
    [box.layer setShadowColor:[[UIColor grayColor] CGColor]];
    [box.layer setShadowOffset:CGSizeMake(0.0, 0.0)];
    [box.layer setShadowRadius:5.0];
}

- (void)viewTappedHandler
{
    [self presentViewController:cabangVC animated:YES completion:nil];
}

- (void) btnBackTappedHandler
{
    Rekening *rekening = [[Rekening alloc] init];
    [rekening setNamaNasabah:rekeningInfo.namaNasabah];
    [rekening setNomorRekening:rekeningInfo.nomorRekening];
    [rekening setJenisTabungan:rekeningInfo.jenisTabungan];
    [rekening setJenisKartu:rekeningInfo.jenisKartu];
    [rekening setKodeCabang:rekeningInfo.kodeCabang];
    
    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
    TabunganBerhasilController *contentVC = [[TabunganBerhasilController alloc] initWithRekening:rekening];
    [templateVC setChildController:contentVC];
    templateVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:templateVC animated:YES completion:nil];
}

- (void)mainButtonDidTappedHandler:(id)sender
{
    [self goToHome];
}

- (void) goToMsm {
    // remove progress step
    [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}

- (void) goToCreatePassword {
    // remove progress step
    [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // TODO SHOW CREATE PASSWORD SCREEN
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        NCreatePasswordViewController *createPasswordVC = (NCreatePasswordViewController *)firstVC.topViewController;
        UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"NCreatePwdVC"];
        createPasswordVC.slidingViewController.topViewController = viewCont;
    }];
}

- (void) backToHome{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
    }
}

- (void) goToHome {
    // remove progress step
    [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:nil];
}

#pragma CabangViewController delegate method
- (void)cabangViewController:(id)cabangViewController didSelectRowWithData:(NSDictionary *)data
{
    
}

@end
