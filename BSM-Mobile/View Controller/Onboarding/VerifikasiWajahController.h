//
//  VerifikasiWajahController.h
//  BSM-Mobile
//
//  Created by ARS on 01/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerifikasiWajahController.h"
#import "ViewController.h"
#import "CameraOverlayView.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifikasiWajahController : OnboardingRootController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, CameraOverlayDelegate>
@property (strong, nonatomic) UIImagePickerController *cameraPicker;
@property (strong, nonatomic) UIImage *imagePicked;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imagePhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnRetake;
- (IBAction)hitBtnRetake:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *textDesc;

@end

NS_ASSUME_NONNULL_END
