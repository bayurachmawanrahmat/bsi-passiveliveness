//
//  OnboardingTemplate01ViewController.h
//  BSM-Mobile
//
//  Created by ARS on 25/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol OnboardingTemplate01ViewControllerDelegate;
@interface OnboardingTemplate01ViewController : OnboardingRootController
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *navigationBarButtonLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewTop;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageBackgroundHeight;
@property (weak, nonatomic) IBOutlet UIView *paddingView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubtitle;

@property (weak, nonatomic) id<OnboardingTemplate01ViewControllerDelegate> delegate;

@property CGFloat defaultContentViewHeight;

- (void)setChildController:(UIViewController *)child;
- (void)changeImageBackgroundHeight:(CGFloat)height;

@end

@protocol OnboardingTemplate01ViewControllerDelegate <NSObject>

@optional
- (void) childHasLoaded:(OnboardingTemplate01ViewController *)parent;

@end

NS_ASSUME_NONNULL_END
