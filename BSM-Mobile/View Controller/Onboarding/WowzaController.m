//
//  WowzaController.m
//  BSM-Mobile
//
//  Created by ARS on 18/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "WowzaController.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@interface WowzaController ()
{
    int currentStep;
    NSURL *videoURL;
    NSString *streamName;
    CAShapeLayer *circleLayer;
    CABasicAnimation *basicAnimation;
    CustomerOnboardingData *onboardingData;
    Encryptor *encryptor;
    NSMutableDictionary *filesInfo;
    __weak IBOutlet UIView *modalView;
    __weak IBOutlet NSLayoutConstraint *videoContainerTop;
}
@end

@implementation WowzaController

@synthesize btnAction;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    encryptor = [[Encryptor alloc] init];
    filesInfo = [encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_FILES];
    currentStep = 0;

    // Adjust component appearance
    self.btnStepNum.layer.cornerRadius = 15.0f;
    [self.btnAction setBackgroundImage:[UIImage imageNamed:@"ic_launcher_start.png"] forState:UIControlStateNormal];
    [self.btnAction setBackgroundImage:[UIImage imageNamed:@"ic_launcher_stop.png"] forState:UIControlStateSelected];

    basicAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    basicAnimation.duration = 15.0;
    basicAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    basicAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    basicAnimation.delegate = self;

    // Register the GoCoder SDK license key
    NSError *goCoderLicensingError = [WowzaGoCoder registerLicenseKey:ONBOARDING_GOCODER_LICENSE_KEY];
    if (goCoderLicensingError != nil) {
        // Log license key registration failure
        [self showAlertErrorWithTitle:@"Error" andMessage:[goCoderLicensingError localizedDescription] sender:self];
        NSLog(@"Log license key registration failure : %@", [goCoderLicensingError localizedDescription]);
    } else {
        // Initialize the GoCoder SDK
        self.goCoder = [WowzaGoCoder sharedInstance];
    }

    if (self.goCoder != nil) {
        // Set view frame
        CGRect appFrame = [[UIScreen mainScreen] bounds];
        self.view.frame = appFrame;

        // Associate the U/I view with the SDK camera preview
        self.goCoder.cameraView = self.view;

        // Set the active camera to the front camera if it is not already active
        if (self.goCoder.cameraPreview.camera.direction != WOWZCameraDirectionFront)
            [self.goCoder.cameraPreview switchCamera];

        // Set the cropping mode so that the full frame is displayed, potentially cropped
        self.goCoder.cameraPreview.previewGravity = WOWZCameraPreviewGravityResizeAspectFill;

        streamName = [NSString stringWithFormat:@"%@.%@.",
                                [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY],
                                [encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY]
                                ];

        // Get a copy of the active config
        WowzaConfig *goCoderBroadcastConfig = self.goCoder.config;
        // Set the defaults for 720p video
        [goCoderBroadcastConfig loadPreset:WOWZFrameSizePreset640x480];
        [goCoderBroadcastConfig setBroadcastVideoOrientation:WOWZBroadcastOrientationAlwaysPortrait];
        // Set the connection properties for the target Wowza Streaming Engine server or Wowza Streaming Cloud live stream
        goCoderBroadcastConfig.hostAddress = ONBOARDING_GOCODER_HOST;
        goCoderBroadcastConfig.portNumber = ONBOARDING_GOCODER_PORT;
        goCoderBroadcastConfig.applicationName = ONBOARDING_GOCODER_APPNAME;
        goCoderBroadcastConfig.streamName = streamName;

        // If source authentication is enabled in Wowza Streaming Engine or Wowza Streaming Cloud
        goCoderBroadcastConfig.username = ONBOARDING_GOCODER_USERNAME;
        goCoderBroadcastConfig.password = ONBOARDING_GOCODER_PASSWORD;

        // Update the active config
        self.goCoder.config = goCoderBroadcastConfig;

        // Start the camera preview
        [self.goCoder.cameraPreview startPreview];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    // Video assist
    [self alterVideoAssistStep];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)btnActionStartTimer
{
    CGFloat radius = self.btnAction.frame.size.width/2;
    UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:self.btnAction.center radius:radius startAngle:1.5*M_PI endAngle:-0.5*M_PI clockwise:NO];

    if(!circleLayer)
    {
        circleLayer = [CAShapeLayer layer];
        circleLayer.bounds = CGRectMake(self.btnAction.frame.origin.x - (self.btnAction.frame.size.width/2), self.btnAction.frame.origin.y - (self.btnAction.frame.size.height/2), radius*2.0, radius*2.0);
        circleLayer.path = circle.CGPath;
        circleLayer.fillColor = [UIColor colorWithWhite:0 alpha:0].CGColor;
        circleLayer.strokeColor = [UIColor whiteColor].CGColor;
        circleLayer.lineWidth = 3.0;
    }
    [circleLayer addAnimation:basicAnimation forKey:@"strokeEndAnimation"];
    [self.btnAction.layer addSublayer:circleLayer];
}

- (void)btnActionHit:(id)sender
{
    // Ensure the minimum set of configuration settings have been specified necessary to
    // initiate a broadcast streaming session
    NSError *configValidationError = [self.goCoder.config validateForBroadcast];
    
    if (configValidationError != nil) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Incomplete Streaming Settings"
            message:self.goCoder.status.description
            preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else if (self.goCoder.status.state != WOWZBroadcastStateBroadcasting) {
        [_player pause];
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryRecord error:nil];
        // Start the live stream
        [self.goCoder startStreaming:self];
    }
    else {
        // Stop the broadcast that is currently running
        [self btnActionStopTimer];
    }
}

- (void)btnActionStopTimer
{
    [self.goCoder endStreaming:self];
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    [self alterVideoAssistStep];
    [self->circleLayer removeAllAnimations];
    [self->circleLayer removeFromSuperlayer];
    [self.btnAction setSelected:NO];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if(flag)
    {
        // Stop the broadcast that is currently running
        [self btnActionStopTimer];
    }
}

- (void) alterVideoAssistStep
{
    int next = 0;
    BOOL isAlamatBeda = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY] integerValue] == 1;
    BOOL isLaki = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR] isEqualToString:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR_M];
    NSString *filename;
    NSString *description;
    NSMutableString *instruksi = [[NSMutableString alloc] initWithString:@"Tekan tombol di bawah lalu "];
    WowzaConfig *goCoderBroadcastConfig = self.goCoder.config;

    if(currentStep == 0)
    {
        next = 1;
        filename = isLaki ? @"L1.mp4" : @"P1.mp4";
        videoURL = [self getVideoAssistUrlWithFilename:filename];
        [self.btnStepNum setTitle:@"1" forState:UIControlStateNormal];
        description = [self videoDescriptionWithFilename:filename];
        [instruksi appendString:description];
        self.labelCoderStatus.text = instruksi;
        goCoderBroadcastConfig.streamName = [streamName stringByAppendingString:@"NAMA"];
    }
    else if (currentStep == 1)
    {
        next = 2;
        filename = isLaki ? @"L2.mp4" : @"P2.mp4";
        videoURL = [self getVideoAssistUrlWithFilename:filename];
        [self.btnStepNum setTitle:@"2" forState:UIControlStateNormal];
        description = [self videoDescriptionWithFilename:filename];
        [instruksi appendString:description];
        self.labelCoderStatus.text = instruksi;
        goCoderBroadcastConfig.streamName = [streamName stringByAppendingString:@"TTL"];
    }
    else if (currentStep == 2)
    {
        next = 3;
        filename = isLaki ? @"L3.mp4" : @"P3.mp4";
        videoURL = [self getVideoAssistUrlWithFilename:filename];
        [self.btnStepNum setTitle:@"3" forState:UIControlStateNormal];
        description = [self videoDescriptionWithFilename:filename];
        [instruksi appendString:description];
        self.labelCoderStatus.text = instruksi;
        goCoderBroadcastConfig.streamName = [streamName stringByAppendingString:@"IBU"];
    }
    else if (currentStep == 3 && isAlamatBeda)
    {
        next = 4;
        filename = isLaki ? @"L4.mp4" : @"P4.mp4";
        videoURL = [self getVideoAssistUrlWithFilename:filename];
        [self.btnStepNum setTitle:@"4" forState:UIControlStateNormal];
        description = [self videoDescriptionWithFilename:filename];
        [instruksi appendString:description];
        self.labelCoderStatus.text = instruksi;
        goCoderBroadcastConfig.streamName = [streamName stringByAppendingString:@"DOMISILI"];
    }
    else if ((currentStep == 3 && !isAlamatBeda) || currentStep == 4)
    {
        next = 5;
        // RANDOM VIDEO
        int idx = 0;
        NSNumber *fileCount = [encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_FILE_COUNT];
        for (int i = 0; i < 10; i++) {
            idx = arc4random_uniform([fileCount intValue]);
        }
        filename = [[NSString alloc] initWithFormat:@"Random%@%d.mp4", isLaki ? @"L" : @"P", idx];
        videoURL = [self getVideoAssistUrlWithFilename:filename];
        NSString *title = currentStep == 4 ? @"5" : @"4";
        [self.btnStepNum setTitle:title forState:UIControlStateNormal];
        description = [self videoDescriptionWithFilename:filename];
        [instruksi appendString:description];
        self.labelCoderStatus.text = instruksi;
        goCoderBroadcastConfig.streamName = [streamName stringByAppendingString:@"VERIFIKASI"];
        [encryptor setUserDefaultsObject:description forKey:ONBOARDING_VIDEOASSIST_RANDOM_DESCRIPTION_KEY];
    }
    else
    {
        next = 0;
    }

    currentStep = next;
    self.goCoder.config = goCoderBroadcastConfig;
    if(currentStep > 0)
        [self initVideoAssist];
    else
    {
        // DONE VIDEO ASSIST. SEND DATA TO BACKEND
        dispatch_group_t dispatchGroup = dispatch_group_create();
        onboardingData = [[CustomerOnboardingData alloc] init];
        onboardingData.dispatchGroup = dispatchGroup;
        [onboardingData sendDataToBackend];
        dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
            if(self->onboardingData.requestIsSuccess)
            {
                UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"PengajuanSelesaiScene"];
                [self presentViewController:vc animated:YES completion:nil];
            }
            else
            {
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    // ERROR SEND TO BACKEND, REPLAY VIDEO ASSIST
                    self->currentStep = 0;
                    [self alterVideoAssistStep];
                }];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:NSLocalizedString(@"GENERAL_FAILURE", @"GENERAL_FAILURE") preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:actionOk];
            }
        });
    }
}

- (void) initVideoAssist
{
    [self showLoading];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    if(self->_playerLayer) {
        [self->_playerLayer removeFromSuperlayer];
    }
    self->_player = [AVPlayer playerWithURL:self->videoURL];
    [self->_player setAutomaticallyWaitsToMinimizeStalling:YES];
    self->_playerLayer = [AVPlayerLayer playerLayerWithPlayer:self->_player];
    self->_playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self->_playerLayer.frame = self->_videoContainer.bounds;
    [self->_videoContainer.layer addSublayer:self->_playerLayer];
    [self->_player play];
    
    [self hideLoading];
}

- (NSString *)videoDescriptionWithFilename:(NSString *)filename
{
    NSString *found = @"";
    for (NSString *key in filesInfo) {
        NSDictionary *file = [filesInfo objectForKey:key];
        if([[file objectForKey:@"filename"] isEqualToString:filename] || [[file objectForKey:@"filename"] isEqualToString:[NSString stringWithFormat:@"%@_NOT_FOUND", filename]])
        {
            found = [file objectForKey:@"description"];
        }
    }
    return found;
}

- (NSURL *)getVideoAssistUrlWithFilename:(NSString *)filename
{
    NSURL *url = [[NSURL alloc]  initWithString:@""];
    for (NSString *key in filesInfo) {
        NSDictionary *file = [filesInfo objectForKey:key];
        if([[file objectForKey:@"filename"] isEqualToString:filename]) {
            url = [NSURL fileURLWithPath:[file objectForKey:@"url"]];
        }
        else if ([[file objectForKey:@"filename"] isEqualToString:[NSString stringWithFormat:@"%@_NOT_FOUND", filename]]) {
            url = [NSURL URLWithString:[file objectForKey:@"url"]];
        }
    }
    return url;
}

- (void)onWOWZStatus:(WOWZBroadcastStatus *)status {
    switch (status.state) {
        case WOWZBroadcastStateBroadcasting:
            [self showStartModal];
            break;
        case WOWZBroadcastStateIdle:
            break;
        case WOWZBroadcastStateReady:
            break;
        default:
            break;
    }
}

- (void) onWOWZError:(WOWZBroadcastStatus *) goCoderStatus {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Live Streaming Error"
            message:self.goCoder.status.description
            preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
            handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    });
}

- (void) showStartModal
{
    [modalView setAlpha:0];
    [modalView setHidden:NO];
    [UIView animateWithDuration:0.7f animations:^{
        [self->modalView setAlpha:0.3f];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.7f animations:^{
            [self->modalView setAlpha:0.0f];
            [self btnActionStartTimer];
            [self.btnAction setSelected:YES];
        } completion:nil];
        [self->modalView setHidden:YES];
    }];
}
@end
