//
//  PenilaianFiturController.m
//  BSM-Mobile
//
//  Created by ARS on 17/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PenilaianFiturController.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "OnboardingTemplate01ViewController.h"
#import "SetoranAwalController.h"
#import "TabunganBerhasilController.h"

@interface PenilaianFiturController () <UITextViewDelegate>
{
    NSInteger rating;
    BOOL requestIsSuccess;
    dispatch_group_t dispatchGroup;
    Encryptor *encryptor;
}
- (IBAction)btnCloseTappedHandler:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextView *tBoxMasukan;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIView *ratingsView;
@property (weak, nonatomic) IBOutlet UIButton *btnStar1;
@property (weak, nonatomic) IBOutlet UIButton *btnStar2;
@property (weak, nonatomic) IBOutlet UIButton *btnStar3;
@property (weak, nonatomic) IBOutlet UIButton *btnStar4;
@property (weak, nonatomic) IBOutlet UIButton *btnStar5;
@end

@implementation PenilaianFiturController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encryptor = [[Encryptor alloc] init];
    
    _tBoxMasukan.delegate = self;
    _tBoxMasukan.layer.borderWidth = 1;
    _tBoxMasukan.layer.borderColor = [UIColor darkGrayColor].CGColor;
    
    _scrollView.delegate = self;
    _scrollView.contentSize = CGSizeMake(_contentView.frame.size.width, _contentView.frame.origin.y + _contentView.frame.size.height);
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.translucent = YES;
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"done" style:UIBarButtonItemStyleDone target:self action:@selector(keyboardDone:)];
    
    [toolbar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    [toolbar setUserInteractionEnabled:YES];
    [toolbar sizeToFit];
    
    _tBoxMasukan.inputAccessoryView = toolbar;
    
    UIImage *starEmpty = [UIImage imageNamed:@"icon_star_empty.png"];
    UIImage *starFilled = [UIImage imageNamed:@"icon_star_filled.png"];
    
    [_btnStar1 setBackgroundImage:starEmpty forState:UIControlStateNormal];
    [_btnStar2 setBackgroundImage:starEmpty forState:UIControlStateNormal];
    [_btnStar3 setBackgroundImage:starEmpty forState:UIControlStateNormal];
    [_btnStar4 setBackgroundImage:starEmpty forState:UIControlStateNormal];
    [_btnStar5 setBackgroundImage:starEmpty forState:UIControlStateNormal];
    [_btnStar1 setBackgroundImage:starFilled forState:UIControlStateSelected];
    [_btnStar2 setBackgroundImage:starFilled forState:UIControlStateSelected];
    [_btnStar3 setBackgroundImage:starFilled forState:UIControlStateSelected];
    [_btnStar4 setBackgroundImage:starFilled forState:UIControlStateSelected];
    [_btnStar5 setBackgroundImage:starFilled forState:UIControlStateSelected];
    [_btnStar1 addTarget:self action:@selector(btnStarDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btnStar2 addTarget:self action:@selector(btnStarDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btnStar3 addTarget:self action:@selector(btnStarDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btnStar4 addTarget:self action:@selector(btnStarDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btnStar5 addTarget:self action:@selector(btnStarDidHit:) forControlEvents:UIControlEventTouchUpInside];
    [_btnSend addTarget:self action:@selector(btnSendDidHit:) forControlEvents:UIControlEventTouchUpInside];
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self registerForKeyboardNotification];
}

- (void)registerForKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    kbSize.height += 10;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    CGRect tRect = _tBoxMasukan.frame;
    tRect.size.height += 75.0;
    if (!CGRectContainsPoint(aRect, _btnSend.frame.origin) ) {
        [_scrollView scrollRectToVisible:tRect animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self scrollView].contentInset = contentInsets;
    [self scrollView].scrollIndicatorInsets = contentInsets;
}

- (void)sendFeedback
{
    [self showLoading];
    dispatchGroup = dispatch_group_create();
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSString *endpoint =@"api/feedback/insert";
    NSURL *url = [NSURL URLWithString:[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:endpoint]]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:
                          @"Online onboarding", @"fitur",
                          _tBoxMasukan.text, @"masukan",
                          @(rating), @"rating",
                          [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY], @"user",
                          nil];
    NSString *mapData = [CustomerOnboardingData stringParamFromData:data];
    [request setHTTPBody:[mapData dataUsingEncoding:NSUTF8StringEncoding]];
    [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [request setValue:[CustomerOnboardingData getToken] forHTTPHeaderField:@"Authorization"];
    dispatch_group_enter(dispatchGroup);
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error)
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if((long)[httpResponse statusCode] == 200)
                self->requestIsSuccess = YES;
            else
                self->requestIsSuccess = NO;
        }
        else
            self->requestIsSuccess = NO;
        
        dispatch_group_leave(self->dispatchGroup);
    }];
    
    [postDataTask resume];
    
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        if(!self->requestIsSuccess)
        {
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *alert) {
                [self showNextScreen];
            }];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:NSLocalizedString(@"GENERAL_FAILURE", @"General failure") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:actionOk];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else [self showNextScreen];
    });
}

- (void)btnStarDidHit:(UIButton *)sender
{
    rating = sender.tag;
    for (UIButton *starBtn in _ratingsView.subviews) {
        if(starBtn.tag <= rating)
            [starBtn setSelected:YES];
        else
            [starBtn setSelected:NO];
    }
}

- (void)btnSendDidHit:(UIButton *)sender
{
    [self sendFeedback];
}

- (void)keyboardDone:(UIBarButtonItem *)sender
{
    [_tBoxMasukan resignFirstResponder];
}

- (void) showNextScreen
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    NSString *vcName;
    if ([[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENIS_KYC_KEY] isEqual:@"CABANG"]) {
        vcName = @"NomorTiketScene";
    }
    else {
        vcName = @"VerifikasiOnlineSelesaiScene";
    }
    
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:vcName];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark : UITextViewDelegate Methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:NSLocalizedString(@"FIELD_REVIEW_PLACEHOLDER", @"Field review placeholder")])
    {
        textView.text = nil;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:@""])
        textView.text = NSLocalizedString(@"FIELD_REVIEW_PLACEHOLDER", @"Field review placeholder");
    else if (textView.text.length > 300000)
        textView.text = [textView.text substringToIndex:300000];
}

- (IBAction)btnCloseTappedHandler:(id)sender {
//    Rekening *rekening = [[Rekening alloc] init];
//    if([CustomerOnboardingData objectForKeychainKey:ONBOARDING_REKENING_INFO_KEY] != nil && [[CustomerOnboardingData objectForKeychainKey:ONBOARDING_REKENING_INFO_KEY] isKindOfClass:Rekening.class])
//        rekening = (Rekening *)[CustomerOnboardingData objectForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
//
//    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
//    TabunganBerhasilController *contentVC = [[TabunganBerhasilController alloc] initWithRekening:rekening];
//    [templateVC setChildController:contentVC];
//    [self presentViewController:templateVC animated:YES completion:nil];
    
    [self showNextScreen];
}
@end
