//
//  OtpController.h
//  BSM-Mobile
//
//  Created by ARS on 11/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OtpController : OnboardingRootController

- (void)showErrorMsg:(NSString *)errorMsg withTextField:(UITextField *)textField;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *labelNomorHp;
@property (nonatomic, assign) int OTP_TYPE; // 0 = REGISTER, 1 = CREATE PIN 
@property (weak, nonatomic) IBOutlet UIButton *buttonUbahNomor;

@end

NS_ASSUME_NONNULL_END
