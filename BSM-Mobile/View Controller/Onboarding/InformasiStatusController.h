//
//  InformasiStatusController.h
//  BSM-Mobile
//
//  Created by ARS on 22/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "UIDropdown.h"
#import "UIDropdownSearch.h"
#import "OnboardingRootController.h"
#import "OnboardingButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiStatusController : OnboardingRootController <UINavigationControllerDelegate, UITextFieldDelegate, UIDropdownSearchDelegate>

@property (weak, nonatomic) IBOutlet UIButton *radioMale;
@property (weak, nonatomic) IBOutlet UIButton *radioFemale;
@property (weak, nonatomic) IBOutlet UITextField *fieldPernikahan;
@property (weak, nonatomic) IBOutlet UITextField *fieldAgama;
@property (weak, nonatomic) IBOutlet UITextField *fieldPendidikan;

- (IBAction)hitRadioMale:(id)sender;
- (IBAction)hitRadioFemale:(id)sender;
- (IBAction)hitBtnNext:(id)sender;


@end

NS_ASSUME_NONNULL_END
