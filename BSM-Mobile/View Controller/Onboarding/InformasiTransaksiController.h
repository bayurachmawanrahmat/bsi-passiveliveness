//
//  InformasiTransaksiController.h
//  BSM-Mobile
//
//  Created by ARS on 25/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIDropdown.h"
#import "CabangViewController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiTransaksiController : OnboardingRootController <UIPickerViewDelegate, UITextFieldDelegate, CabangViewDelegate>

@property (weak, nonatomic) IBOutlet UIDropdown *fieldTujuanBukaRekening;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldSumberdana;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldTransaksi;
@property (weak, nonatomic) IBOutlet UITextField *fieldCabang;
@property (weak, nonatomic) IBOutlet UIButton *btnZakat;
@property (weak, nonatomic) IBOutlet UILabel *labelZakat;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@end

NS_ASSUME_NONNULL_END
