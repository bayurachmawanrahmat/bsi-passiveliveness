//
//  AvailabilityViewController.h
//  BSM-Mobile
//
//  Created by ARS on 06/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AvailabilityViewController : OnboardingRootController <NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

- (IBAction)hitBtnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *maintenanceView;
@property (weak, nonatomic) IBOutlet UILabel *maintenanceText;
- (IBAction)hitBtnOk:(id)sender;

@property (nonatomic, assign) NSString *cobMessage;

@end

NS_ASSUME_NONNULL_END
