//
//  PembukaanAkunController.h
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"
#import "GeneralPopupController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PembukaanAkunController : OnboardingRootController <GeneralPopupDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnVideoCall;
@property (weak, nonatomic) IBOutlet UIButton *btnCs;
@property (weak, nonatomic) IBOutlet UIButton *btnCabang;
@property (weak, nonatomic) IBOutlet UIButton *btnVideoAssist;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UILabel *noTiketLabel;
- (IBAction)hitBtnCabang:(id)sender;
- (IBAction)hitBtnVideAssist:(id)sender;
- (IBAction)hitBtnVideoCall:(id)sender;

- (void)generateButtons;

@end

NS_ASSUME_NONNULL_END
