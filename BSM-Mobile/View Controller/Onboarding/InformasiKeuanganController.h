//
//  InformasiKeuanganController.h
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ViewController.h"
#import "UIDropdown.h"
#import "UIDropdownSearch.h"
#import "OnboardingButton.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiKeuanganController : OnboardingRootController <UIPickerViewDelegate, UITextFieldDelegate, UIDropdownSearchDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldStatusPekerjaan;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldPekerjaan;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldJabatan;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldPenghasilan;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldNamaPerusahaan;
@property (weak, nonatomic) IBOutlet UITextField *fieldNamaPerusahaanLainnya;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldBidangUsaha;
@property (weak, nonatomic) IBOutlet UILabel *labelAlamatPerusahaan;
@property (weak, nonatomic) IBOutlet UITextField *fieldAlamatPerusahaan;
@property (weak, nonatomic) IBOutlet UITextField *fieldTeleponPerusahaan;
@property (weak, nonatomic) IBOutlet UITextField *fieldMulaiBekerja;
@property (weak, nonatomic) IBOutlet UITextField *fieldNamaSuami;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldPekerjaanSuami;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldAlamatSuami;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldTeleponSuami;
@property (weak, nonatomic) IBOutlet OnboardingButton *btnNext;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabelAlamatPerusahaan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintFieldAlamatPerusahaan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabelBidangUsaha;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabelBidangUsahaAlamat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabelAlamatPerLainnya;

@end

NS_ASSUME_NONNULL_END
