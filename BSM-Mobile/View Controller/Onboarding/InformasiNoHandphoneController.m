//
//  InformasiNoHandphoneController.m
//  BSM-Mobile
//
//  Created by ARS on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "InformasiNoHandphoneController.h"
#import "Utility.h"
#import "AMPopTip.h"
#import "Encryptor.h"
#import "CustomerOnboardingData.h"
#import "NSObject+FormInputCategory.h"
#import "OtpController.h"
#import "OnboardingTemplate01ViewController.h"
#import "HomeViewController.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"

@interface InformasiNoHandphoneController ()
{
    CustomerOnboardingData *onboardingData;
    dispatch_group_t referenceGroup;
    Encryptor *encryptor;
}
@end

@implementation InformasiNoHandphoneController

static const NSString *ScreenName = @"InformasiNoHandphoneScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    referenceGroup = dispatch_group_create();
    encryptor = [[Encryptor alloc] init];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    self.fieldNoHandphone.inputAccessoryView = keyboardDoneButtonView;
    self.fieldNoHandphone.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
}

- (BOOL)validatePhoneWithString:(NSString*)phone
{
    NSString *phoneRegex = @"^[0-9]+$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phone];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(
       (textField == _fieldNoHandphone && textField.text.length == 13)
       && ![textField.text isEqualToString:@""] && ![string isEqualToString:@""]) {
        return NO;
    }
    return YES;
}

- (BOOL) checkInput:(NSString *) phone{
    NSDictionary *nasabahData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_NASABAH];
    
    if([phone isEqualToString:@""]) {
        [self showErrorLabel:NSLocalizedString(@"PLEASE_FILL_NO_PHONE", @"") withUiTextfield:_fieldNoHandphone];
        return FALSE;
    }
    else if(![self validatePhoneWithString:phone])
    {
        [self showErrorLabel:NSLocalizedString(@"PHONE_NOT_VALID", @"") withUiTextfield: _fieldNoHandphone];
        return FALSE;
    } else if (![phone isEqualToString:[nasabahData objectForKey:@"no_hp"]]) {
        [self showErrorLabel:NSLocalizedString(@"PHONE_NOT_REGISTERED", @"") withUiTextfield: _fieldNoHandphone];
        return FALSE;
    }
    else return TRUE;
}

- (void) requestOtp {
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              self->_fieldNoHandphone.text, @"dest",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];

    [CustomerOnboardingData postToEndPoint:@"/api/pin/createOTP" dispatchGroup:referenceGroup postData:postData returnData:returnData];
    
    [self showLoading];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *data = [returnData objectForKey:@"data"];
            [self->encryptor setUserDefaultsObject:[data objectForKey:@"token"] forKey:ONBOARDING_OTP_TOKEN];
            [self goToOtpScreen];
        } else {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
        }
    });
}

- (void) goToOtpScreen {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    OtpController *otpController = [storyboard instantiateViewControllerWithIdentifier:@"OtpScreen"];
    otpController.OTP_TYPE = 1;
    [self presentViewController:otpController animated:YES completion:nil];
}

- (void) goToMsm {
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)hitBtnNext:(id)sender {
    if ([self checkInput:_fieldNoHandphone.text]) {
        [self requestOtp];
    }
}

- (IBAction)hitBtnBack:(id)sender {
    [self goToMsm];
}
@end
