//
//  PembukaanAkunController.m
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PembukaanAkunUlangController.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "WowzaController.h"

@interface PembukaanAkunUlangController ()
{
    NSString *nomorAntrian;
    dispatch_group_t referenceGroup;
    BOOL requestIsSuccess;
    NSMutableArray *listButtons;
    NSString *verifikasiOnlineMode;
    CustomerOnboardingData *onboardingData;
    Encryptor *encryptor;
}
@end

@implementation PembukaanAkunUlangController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    [encryptor setUserDefaultsObject:@"PilihUlangMetodeScreen" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    self.contentView.layer.cornerRadius = 30.0f;
    
    [self.noTiketLabel setText:[NSString stringWithFormat:@"No Tiket : %@", [encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY]]];
    
    [self showLoading];
    referenceGroup = dispatch_group_create();
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/setting/getKycMenu" dispatchGroup:referenceGroup returnData:returnData];
    NSMutableDictionary *returnOnlineMode = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/setting/getKycOnlineMode" dispatchGroup:referenceGroup returnData:returnOnlineMode];
    // save data nasabah set jenis kyc CABANG
    [encryptor setUserDefaultsObject:@"CABANG" forKey:ONBOARDING_JENIS_KYC_KEY];
    CustomerOnboardingData *onboardingData = [[CustomerOnboardingData alloc] init];
    onboardingData.dispatchGroup = referenceGroup;
    [onboardingData sendDataToBackend];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]] && [[returnOnlineMode objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *data = [returnData objectForKey:@"data"];
            if(data && data.count > 0)
            {
                self->listButtons = [[NSMutableArray alloc] init];
                for (NSString *button in data) {
                    [self->listButtons addObject:button];
                }
                self->verifikasiOnlineMode = [returnOnlineMode objectForKey:@"data"];
                self->requestIsSuccess = YES;
            }
            else
            {
                self->requestIsSuccess = NO;
            }
        }
        else
        {
            self->requestIsSuccess = NO;
        }
        
        if(self->requestIsSuccess && self->listButtons)
        {
            [self generateButtons];
        }
        else
        {
            [self showAlertErrorWithTitle:@"Error" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
        }
    });
}

- (void)generateButtons
{
    CGRect pos1 = self.btnVideoCall.frame;
    CGRect pos2 = self.btnCs.frame;
    CGRect pos3 = self.btnCabang.frame;
    CGRect pos4 = self.btnVideoAssist.frame;
    NSArray *listPost = [[NSArray alloc] initWithObjects:[NSValue valueWithCGRect:pos1], [NSValue valueWithCGRect:pos2], [NSValue valueWithCGRect:pos3], [NSValue valueWithCGRect:pos4], nil];
    
    int i = 0;
    for (NSString *btnText in listButtons) {
        if([btnText isEqualToString:@"VIDEO"])
        {
            [self.btnVideoAssist setFrame:[[listPost objectAtIndex:i] CGRectValue]];
            [self.btnVideoAssist setHidden:NO];
        }
        else if ([btnText isEqualToString:@"CABANG"])
        {
            [self.btnCabang setFrame:[[listPost objectAtIndex:i] CGRectValue]];
            [self.btnCabang setHidden:NO];
        }
        else if ([btnText isEqualToString:@"VIDEOCALL"])
        {
            [self.btnVideoCall setFrame:[[listPost objectAtIndex:i] CGRectValue]];
            [self.btnVideoCall setHidden:NO];
        }
        else if ([btnText isEqualToString:@"CS"])
        {
            [self.btnCs setFrame:[[listPost objectAtIndex:i] CGRectValue]];
            [self.btnCs setHidden:NO];
        }
        i = i + 1;
    }
}

#pragma mark GeneralPopupDelegate
- (void)didOk:(GeneralPopupController *)controller {
    [encryptor setUserDefaultsObject:@"CABANG" forKey:ONBOARDING_JENIS_KYC_KEY];
    [self sendDataToBackendAndShowViewControllerWithName:@"NomorTiketScene"];
}

- (IBAction)hitBtnCabang:(id)sender {
    GeneralPopupController *pop = [[GeneralPopupController alloc] init];
    pop.modalPresentationStyle = UIModalPresentationOverFullScreen;
    pop.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    pop.delegate = self;
    [self presentViewController:pop animated:YES completion:nil];
}

- (IBAction)hitBtnVideAssist:(id)sender {
    [encryptor setUserDefaultsObject:@"VIDEO" forKey:ONBOARDING_JENIS_KYC_KEY];
    
    NSString *vcName = @"VerifikasiOnlineScreen";
    [self sendDataToBackendAndShowViewControllerWithName:vcName];
    // UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    // UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiOnlineScreen"];
    // [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)hitBtnVideoCall:(id)sender {
    NSString *vcName = @"VerifikasiOnlineScreen";
    #if !(TARGET_IPHONE_SIMULATOR)
    // v3 purposes. disabled on v6
//    if (![self->verifikasiOnlineMode isEqualToString:@"2"]) {
//        [encryptor setUserDefaultsObject:@"VIDEOCALL" forKey:ONBOARDING_JENIS_KYC_KEY];
//        [self sendDataToBackendAndShowViewControllerWithName:vcName];
//    }
//    else {
//        [encryptor setUserDefaultsObject:@"VIDEO" forKey:ONBOARDING_JENIS_KYC_KEY];
//        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
//        UIViewController *vc = [ui instantiateViewControllerWithIdentifier:vcName];
//        [self presentViewController:vc animated:YES completion:nil];
//    }
    [encryptor setUserDefaultsObject:@"VIDEOCALL" forKey:ONBOARDING_JENIS_KYC_KEY];
    [self sendDataToBackendAndShowViewControllerWithName:vcName];
    #else
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:vcName];
    [self presentViewController:vc animated:YES completion:nil];
    #endif
}

- (void)sendDataToBackendAndShowViewControllerWithName:(NSString *)vcName
{
    dispatch_group_t dispatchGroup = dispatch_group_create();
    onboardingData = [[CustomerOnboardingData alloc] init];
    onboardingData.dispatchGroup = dispatchGroup;
    [onboardingData sendDataToBackend];
    [self showLoading];
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        if(self->onboardingData.requestIsSuccess)
        {
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:vcName];
            [self presentViewController:vc animated:YES completion:nil];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:NSLocalizedString(@"GENERAL_FAILUER", @"") preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:actionOk];
            [self presentViewController:alert animated:YES completion:nil];
        }
    });
}

@end
