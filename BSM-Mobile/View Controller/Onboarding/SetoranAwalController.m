//
//  SetoranAwalController.m
//  BSM-Mobile
//
//  Created by ARS on 26/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SetoranAwalController.h"
#import "OnboardingButton.h"
#import "CaraSetoranAwalController.h"
#import "Rekening.h"
#import "CustomerOnboardingData.h"
#import "TabunganBerhasilController.h"
#import "ECSlidingViewController.h"
#import "HomeViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface SetoranAwalController ()
{
    Rekening *rekeningInfo;
}

- (IBAction)btnCaraSetoranTappedHandler:(id)sender;

@end

@implementation SetoranAwalController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if([CustomerOnboardingData objectForKeychainKey:ONBOARDING_REKENING_INFO_KEY] != nil && [[CustomerOnboardingData objectForKeychainKey:ONBOARDING_REKENING_INFO_KEY] isKindOfClass:Rekening.class])
    {
        rekeningInfo = (Rekening *)[CustomerOnboardingData objectForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
    }
    else
        rekeningInfo = [[Rekening alloc] init];
    
}

- (void)childHasLoaded:(OnboardingTemplate01ViewController *)parent
{
    parent.imageView.image = [UIImage imageNamed:@"img_lacak_kartu@2x.png"];
    CGRect imageViewFrame = parent.imageView.frame;
    imageViewFrame.origin = CGPointMake((parent.view.frame.size.width / 2) - (parent.imageView.frame.size.width / 2), 0);
    [parent.imageView setFrame:imageViewFrame];
    
    [parent.navigationBar.topItem setTitle:@""];
    [parent.navigationBarButtonLeft setTarget:self];
    [parent.navigationBarButtonLeft setAction:@selector(backButtonTappedHandler)];
    
    OnboardingButton *mainButton = [[OnboardingButton alloc] initWithFrame:CGRectMake(10.0, parent.contentView.frame.size.height - mainButtonHeight - mainButtonMargin, self.view.frame.size.width - 20.0, mainButtonHeight)];
    [mainButton setTitle:@"AKTIVASI MOBILE BANKING" forState:UIControlStateNormal];
    [self.view addSubview:mainButton];
    [mainButton addTarget:self action:@selector(mainButtonDidTappedHandler:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)mainButtonDidTappedHandler:(id)sender
{
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // TODO SHOW ACTIVATION SCREEN
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        HomeViewController *homeVC = (HomeViewController *)firstVC.topViewController;
        UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"ActivationVC"];
        homeVC.slidingViewController.topViewController = viewCont;
    }];
}

- (void)backButtonTappedHandler
{
    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
    TabunganBerhasilController *contentVC = [[TabunganBerhasilController alloc] initWithRekening:rekeningInfo];
    [templateVC setChildController:contentVC];
    [self presentViewController:templateVC animated:YES completion:nil];
}

- (IBAction)btnCaraSetoranTappedHandler:(id)sender {
    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
    CaraSetoranAwalController *contentVC = [[CaraSetoranAwalController alloc] initWithRekening:rekeningInfo];
    [templateVC setChildController:contentVC];
    [self presentViewController:templateVC animated:YES completion:nil];
}
@end
