//
//  OnboardingRootController.m
//  BSM-Mobile
//
//  Created by ARS on 19/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "OnboardingRootController.h"
#import "InformasiKTPDukcapilController.h"
#import "InfromasiKTPController.h"
#import "Encryptor.h"

@interface OnboardingRootController ()
{
    Encryptor *encryptor;
}
@end

@implementation OnboardingRootController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    if (@available(iOS 13.0, *)) {
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
}

- (IBAction)hitBtnNext:(id)sender {
    if ([[encryptor getUserDefaultsObjectForKey:ONBOARDING_INPUT_DATA_IDENTITY_TYPE_KEY] isEqualToString:ONBOARDING_INPUT_DATA_IDENTITY_DUKCAPIL]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        InformasiKTPDukcapilController *informasiKTPDukcapilController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiPribadiDukcapilScreen"];
        [self presentViewController:informasiKTPDukcapilController animated:YES completion:nil];
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        InfromasiKTPController *informasiKTPController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiPribadiScreen"];
        [self presentViewController:informasiKTPController animated:YES completion:nil];
    }
}
@end
