//
//  GeneralPopupController.m
//  BSM-Mobile
//
//  Created by ARS on 26/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GeneralPopupController.h"

@interface GeneralPopupController ()
- (IBAction)btnCancelDidHit:(id)sender;
- (IBAction)btnOkDidHit:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation GeneralPopupController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.80];
    self.contentView.layer.cornerRadius = 30.0f;
    self.contentView.layer.masksToBounds = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancelDidHit:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnOkDidHit:(id)sender {
    [self.delegate didOk:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
