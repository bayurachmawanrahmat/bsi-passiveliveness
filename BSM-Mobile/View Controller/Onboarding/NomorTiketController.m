//
//  NomorTiketController.m
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "NomorTiketController.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"

@interface NomorTiketController ()
{
    Encryptor *encryptor;
}
@end

@implementation NomorTiketController

- (void)viewDidLoad {
    [super viewDidLoad];
    encryptor = [[Encryptor alloc] init];
    NSString *nomorAntrian = [[NSString alloc] init];
    nomorAntrian = [encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY];
    self.lblNomorTiket.text = nomorAntrian;
    
    // [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
}

- (IBAction)btnSelesaiDidHit:(id)sender {
    // REMOVE LOCAL DATA
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    [encryptor removeImages];
    [encryptor removeVideoAssist];

    // GO TO MSM
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
    PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}
@end
