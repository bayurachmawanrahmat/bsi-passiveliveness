//
//  InformasiKeuanganController.m
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "InformasiKeuanganController.h"
#import "NSObject+FormInputCategory.h"
#import "AMPopTip.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "Utility.h"

@interface InformasiKeuanganController ()
{
    UIScrollView *scrollView;
    UIDropdownSearch *dropdownPekerjaan;
    UIDropdownSearch *dropdownPekerjaanSuami;
    UIDropdownSearch *dropdownStatusPekerjaan;
    UIDropdownSearch *dropdownNamaPerusahaan;
    NSDictionary *formTextFields;
    NSDictionary *formDropDowns;
    dispatch_group_t referenceGroup;
    NSMutableDictionary *listPenghasilan;
    NSMutableDictionary *listJabatan;
    NSMutableDictionary *listBidangusaha;
    Encryptor *encryptor;
    UIRefreshControl *refreshControl;
    
    // scenario 0 = status pekerjaan = Pengangguran, Pensiunan, nil
    // scenario 1 = status pekerjaan = Karyawan, Pekerja Bebas
    // scenario 2 = status pekerjaan = Ibu Rumah Tangga, Pelajar
    int scenario;
    CGFloat LongScreenHeight;
    CGFloat DefaultScreenHeight;
    NSDictionary *dataMulaiKerja;
    NSArray *pekerjaanOptions;
}

@end

@implementation InformasiKeuanganController

static const NSString *ScreenName = @"InformasiKeuanganScreen";
static NSString * const companyIdLainnya = @"9999";
CGRect fieldAlamatPerusahaanFrame;
CGRect labelAlamatPerusahaanFrame;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    scenario = 0;
    
    LongScreenHeight = _contentView.frame.size.height;
    scrollView = (UIScrollView *)_contentView.superview;
    scrollView.delegate = self;
    
    _fieldStatusPekerjaan.delegate = self;
    _fieldPekerjaan.delegate = self;
    _fieldJabatan.delegate = self;
    [_fieldJabatan setResetable:YES];
    _fieldPekerjaan.delegate = self;
    _fieldNamaPerusahaan.delegate = self;
    _fieldBidangUsaha.delegate = self;
    _fieldPenghasilan.delegate = self;
    _fieldAlamatPerusahaan.delegate = self;
    _fieldMulaiBekerja.delegate = self;
    _fieldTeleponPerusahaan.delegate = self;
    _fieldNamaSuami.delegate = self;
    _fieldPekerjaanSuami.delegate = self;
    _fieldAlamatSuami.delegate = self;
    _fieldTeleponSuami.delegate = self;
    _fieldNamaPerusahaanLainnya.delegate = self;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.translucent = YES;
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"done" style:UIBarButtonItemStyleDone target:self action:@selector(numpadToolbarDone)];
    
    [toolbar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    [toolbar setUserInteractionEnabled:YES];
    [toolbar sizeToFit];
    
    _fieldTeleponSuami.inputAccessoryView = toolbar;
    _fieldTeleponPerusahaan.inputAccessoryView = toolbar;
    
    referenceGroup = dispatch_group_create();
    
    [self refreshData];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [scrollView setRefreshControl:refreshControl];
    } else {
        [scrollView addSubview:refreshControl];
    }
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self setScrollView:scrollView];
    [self setMainView:self.view];
    [self registerForKeyboardNotifications];
    
    fieldAlamatPerusahaanFrame = _fieldAlamatPerusahaan.frame;
    labelAlamatPerusahaanFrame = _labelAlamatPerusahaan.frame;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    DefaultScreenHeight = self.view.frame.size.height - scrollView.frame.origin.y;
    if([[ns objectForKey:ONBOARDING_DEVICE_HAS_NOTCH_KEY] isEqual:[NSNumber numberWithBool:YES]])
        DefaultScreenHeight -= 20;
    [self changeContentViewHeight];
    CGFloat yPos = self.scrollView.frame.origin.y;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:5 fromTotal:7];
}

- (void) refreshData
{
    [self showLoading];
    [self loadDatepicker];
    [self loadPekerjaanView];
    [self getPenghasilan];
    [self getJabatan];
    [self getBidangUsaha];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        if(self->refreshControl)
            [self->refreshControl endRefreshing];
        
        if(self->dropdownPekerjaan.callApiSucess)
        {
            self->dropdownPekerjaanSuami.allItems = [[NSArray alloc] initWithArray:self->dropdownPekerjaan.allItems copyItems:YES];
            
            // REMOVE IBU RUMAH TANGGA IF GENDER IS MALE
            if([[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN] isEqualToString:@"MALE"])
            {
                NSMutableArray *currentArray = [[NSMutableArray alloc] initWithArray:self->dropdownPekerjaan.allItems];
                NSMutableArray *discardArray = [[NSMutableArray alloc] init];
                for (NSMutableDictionary *statusPekerjaan in currentArray) {
                    if ([[statusPekerjaan objectForKey:@"nilai"] isEqualToString:@"21"] || [[statusPekerjaan objectForKey:@"nilai"] isEqualToString:@"0021"])
                        [discardArray addObject:statusPekerjaan];
                }
                [currentArray removeObjectsInArray:discardArray];
                self->dropdownPekerjaan.allItems = currentArray;
            }
            
            // REMOVE IBU RUMAH TANGGA IF GENDER IS FEMALE
            if([[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN] isEqualToString:@"FEMALE"])
            {
                NSMutableArray *currentArray = [[NSMutableArray alloc] initWithArray:self->dropdownPekerjaanSuami.allItems];
                NSMutableArray *discardArray = [[NSMutableArray alloc] init];
                for (NSMutableDictionary *statusPekerjaan in currentArray) {
                    if ([[statusPekerjaan objectForKey:@"nilai"] isEqualToString:@"21"] || [[statusPekerjaan objectForKey:@"nilai"] isEqualToString:@"0021"])
                        [discardArray addObject:statusPekerjaan];
                }
                [currentArray removeObjectsInArray:discardArray];
                self->dropdownPekerjaanSuami.allItems = currentArray;
            }
            
            self->dropdownPekerjaan.displayedItems = self->dropdownPekerjaan.allItems;
            self->dropdownPekerjaanSuami.displayedItems = self->dropdownPekerjaan.allItems;
            [self->dropdownPekerjaan.tableView reloadData];
            [self->dropdownPekerjaanSuami.tableView reloadData];
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_OCCUPATION", @"") sender:self];
        }
        
        if(self->dropdownStatusPekerjaan.callApiSucess)
        {
            // REMOVE IBU RUMAH TANGGA IF GENDER IS MALE
            if([[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN] isEqualToString:@"MALE"])
            {
                NSMutableArray *currentArray = [[NSMutableArray alloc] initWithArray:self->dropdownStatusPekerjaan.allItems];
                NSMutableArray *discardArray = [[NSMutableArray alloc] init];
                for (NSMutableDictionary *statusPekerjaan in currentArray) {
                    if ([[statusPekerjaan objectForKey:@"nilai"] isEqualToString:@"HOUSEWIFE"])
                        [discardArray addObject:statusPekerjaan];
                }
                [currentArray removeObjectsInArray:discardArray];
                self->dropdownStatusPekerjaan.allItems = currentArray;
            }
            self->dropdownStatusPekerjaan.displayedItems = self->dropdownStatusPekerjaan.allItems;
            [self->dropdownStatusPekerjaan.tableView reloadData];
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_OCCUPATION_STATUS", @"") sender:self];
        }
        
        if (self->dropdownNamaPerusahaan.callApiSucess) {
            NSMutableArray *currentArray = [[NSMutableArray alloc] initWithArray:self->dropdownNamaPerusahaan.allItems];
            NSMutableDictionary *namaLainnya = [[NSMutableDictionary alloc] init];
            [namaLainnya setValue:@"Lainnya" forKey:@"name_company"];
            [namaLainnya setValue:companyIdLainnya forKey:@"id_company"];
            [namaLainnya setValue:@"" forKey:@"address"];
            [currentArray addObject:namaLainnya];
            
            // set search not found value nama perusahaan dropdown
            self->dropdownNamaPerusahaan.searchNotFoundValue = namaLainnya;
            
            NSMutableArray *formattedArray = [[NSMutableArray alloc] init];
            for (NSMutableDictionary *perusahaan in currentArray) {
                NSString *valueNamaPerusahaan = [perusahaan objectForKey:@"name_company"];
                NSString *valueAddressPerusahaan = [perusahaan objectForKey:@"address"];
                if (valueNamaPerusahaan.length >= 35) {
                    valueNamaPerusahaan = [valueNamaPerusahaan substringToIndex:35];
                }
                if (valueAddressPerusahaan.length >= 35) {
                    valueAddressPerusahaan = [valueAddressPerusahaan substringToIndex:35];
                }
                [perusahaan setObject:valueNamaPerusahaan forKey:@"value_name_company"];
                [perusahaan setObject:valueAddressPerusahaan forKey:@"value_address"];
                [formattedArray addObject:perusahaan];
            }
            
            self->dropdownNamaPerusahaan.allItems = formattedArray;
            [self->dropdownNamaPerusahaan.tableView reloadData];
        } else {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_COMPANY_NAME", @"") sender:self];
        }
        
        if([[self->listPenghasilan objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [self->listPenghasilan objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldPenghasilan.options = options;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_INCOME", @"") sender:self];
        }
        
        if([[self->listJabatan objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [self->listJabatan objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldJabatan.options = options;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_POSITION", @"") sender:self];
        }
        
        if([[self->listBidangusaha objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [self->listBidangusaha objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldBidangUsaha.options = options;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_BUSINESS_FIELD", @"") sender:self];
        }
        
        self->dropdownPekerjaan.selectedData = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_KEY];
        self->dropdownPekerjaanSuami.selectedData = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_SUAMI_KEY];
        self->dropdownStatusPekerjaan.selectedData = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_STATUS_PEKERJAAN_KEY];
        self->dropdownNamaPerusahaan.selectedData = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY];
        
        NSString *status = [self->dropdownStatusPekerjaan.selectedData objectForKey:@"nilai"];
        [self scenarioDoChange:status];
        
        self->_fieldPekerjaan.text = [self->dropdownPekerjaan.selectedData objectForKey:@"nama"];
        self->_fieldPekerjaanSuami.text = [self->dropdownPekerjaanSuami.selectedData objectForKey:@"nama"];
        self->_fieldStatusPekerjaan.text = [self->dropdownStatusPekerjaan.selectedData objectForKey:@"nama"];
        
        NSDictionary *namaPerusahaanSelected = self->dropdownNamaPerusahaan.selectedData;
        if (![namaPerusahaanSelected isEqual:nil]) {
            if ([[namaPerusahaanSelected objectForKey:@"id_company"] isEqualToString:companyIdLainnya]) {
                self->_fieldNamaPerusahaan.text = [namaPerusahaanSelected objectForKey:@"name_company"];
                self->_fieldNamaPerusahaanLainnya.text = [namaPerusahaanSelected objectForKey:@"value_name_company"];
            } else {
                self->_fieldNamaPerusahaan.text = [namaPerusahaanSelected objectForKey:@"value_name_company"];
            }
            [self namaPerusahaanDidChange:[namaPerusahaanSelected objectForKey:@"id_company"] withAddress:[namaPerusahaanSelected objectForKey:@"value_address"]];
        }
    });
    
    formTextFields = [[NSDictionary alloc] initWithObjectsAndKeys:
                      //_fieldNamaPerusahaan, ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY,
                      //_fieldAlamatPerusahaan, ONBOARDING_INFORMASI_KEUANGAN_ALAMAT_PERUSAHAAN_KEY,
                      _fieldTeleponPerusahaan, ONBOARDING_INFORMASI_KEUANGAN_TELEPON_PERUSAHAAN_KEY,
                      _fieldNamaSuami, ONBOARDING_INFORMASI_KEUANGAN_NAMA_SUAMI_KEY,
                      _fieldAlamatSuami, ONBOARDING_INFORMASI_KEUANGAN_ALAMAT_SUAMI_KEY,
                      _fieldTeleponSuami, ONBOARDING_INFORMASI_KEUANGAN_TELEPON_SUAMI_KEY,
                      nil];
    
    formDropDowns = [[NSDictionary alloc] initWithObjectsAndKeys:
                     _fieldPenghasilan, ONBOARDING_INFORMASI_KEUANGAN_PENGHASILAN_KEY,
                     _fieldJabatan, ONBOARDING_INFORMASI_KEUANGAN_JABATAN_KEY,
                     _fieldBidangUsaha, ONBOARDING_INFORMASI_KEUANGAN_BIDANG_USAHA_KEY,
                     nil];
    
    [self setTextFieldData:formTextFields];
    [self setDropDownData:formDropDowns];
}

- (void) changeContentViewHeight
{
    CGFloat height = scenario != 1 ? DefaultScreenHeight : DefaultScreenHeight > LongScreenHeight ? DefaultScreenHeight : LongScreenHeight;
    
    CGRect frame = scrollView.frame;
    frame.size.height = height;
    scrollView.frame = frame;
    
    _contentViewHeight.constant = height;
    
    scrollView.contentSize = CGSizeMake(frame.size.width, frame.size.height);
    
    [self setBtnNextConstraints];
}

- (void) setBtnNextConstraints
{
    CGFloat width = _contentView.frame.size.width-20;
    CGFloat yAxis = scrollView.contentSize.height-scrollView.frame.origin.y;
    CGRect btnRect = CGRectMake(10, yAxis, width, 50.0);
    _btnNext.frame = btnRect;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(0.0, scrollView.contentOffset.y)];
}

- (void) loadDatepicker
{
    dataMulaiKerja = ![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY] ? nil : [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY] isKindOfClass:NSString.class] ? nil : [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY];
    if(dataMulaiKerja)
        _fieldMulaiBekerja.text = [dataMulaiKerja objectForKey:@"formatted"];
    
    UIDatePicker *datepicker = [[UIDatePicker alloc] init];
    datepicker.datePickerMode = UIDatePickerModeDate;
    _fieldMulaiBekerja.inputView = datepicker;
    [datepicker addTarget:self action:@selector(datepickerDidChange:) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.translucent = YES;
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:@"done" style:UIBarButtonItemStyleDone target:self action:@selector(datepickerToolbarDone)];
    
    [toolbar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    [toolbar setUserInteractionEnabled:YES];
    [toolbar sizeToFit];
    
    _fieldMulaiBekerja.inputAccessoryView = toolbar;
}

- (void) loadPekerjaanView
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    
    dropdownPekerjaan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownPekerjaan.displayedStringKey = @"nama";
    [dropdownPekerjaan getReferenceDataFromEndPoint:@"api/master/reference/listAllByKategori?kategori=PEKERJAAN" withKeysToStore:[[NSArray alloc] initWithObjects:@"nilai", @"nama", nil] andSortUsingKey:@"nourut" andDispatchGroup:referenceGroup];
    dropdownPekerjaan.delegate = self;
    
    dropdownPekerjaanSuami = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownPekerjaanSuami.displayedStringKey = @"nama";
    dropdownPekerjaanSuami.delegate = self;
    
    dropdownStatusPekerjaan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownStatusPekerjaan.displayedStringKey = @"nama";
    [dropdownStatusPekerjaan getReferenceDataFromEndPoint:@"api/master/reference/listAllByKategori?kategori=STATUSPEKERJAAN" withKeysToStore:[[NSArray alloc] initWithObjects:@"nilai", @"nama", nil] andSortUsingKey:@"nourut" andDispatchGroup:referenceGroup];
    dropdownStatusPekerjaan.delegate = self;
    
    dropdownNamaPerusahaan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownNamaPerusahaan.displayedStringKey = @"name_company";
    [dropdownNamaPerusahaan getReferenceDataFromEndPoint:@"api/master/perusahaan/list" withKeysToStore:[[NSArray alloc] initWithObjects:@"cifno", @"tagging_code", @"address", @"value_address", @"name_company", @"value_name_company", nil] andSortUsingKey:@"id_company" andDispatchGroup:referenceGroup];
    dropdownNamaPerusahaan.delegate = self;
}

- (void) numpadToolbarDone
{
    [_fieldTeleponSuami resignFirstResponder];
    [_fieldTeleponPerusahaan resignFirstResponder];
}

- (void) datepickerToolbarDone
{
    [_fieldMulaiBekerja resignFirstResponder];
}

- (void) datepickerDidChange:(UIDatePicker *)sender
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterLongStyle;
    _fieldMulaiBekerja.text = [df stringFromDate:sender.date];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    dataMulaiKerja = [[NSDictionary alloc] initWithObjectsAndKeys:
                      [df stringFromDate:sender.date], @"raw",
                      _fieldMulaiBekerja.text, @"formatted",
                      nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self inputHasError:FALSE textField:textField];
    [self setActiveField:textField];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self inputHasError:FALSE textField:textField];
    
    if(textField == _fieldPekerjaan)
    {
        [self presentViewController:dropdownPekerjaan animated:YES completion:nil];
        return NO;
    }
    else if(textField == _fieldPekerjaanSuami)
    {
        [self presentViewController:dropdownPekerjaanSuami animated:YES completion:nil];
        return NO;
    }
    else if(textField == _fieldStatusPekerjaan)
    {
        [self presentViewController:dropdownStatusPekerjaan animated:YES completion:nil];
        return NO;
    }
    else if(textField == _fieldNamaPerusahaan)
    {
        [self presentViewController:dropdownNamaPerusahaan animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isKindOfClass:UIDropdown.class] || [[textField inputView] isKindOfClass:UIDatePicker.class]) return FALSE;
    else {
        if( (textField == _fieldNamaPerusahaan || textField == _fieldAlamatPerusahaan || textField == _fieldAlamatSuami || textField == _fieldNamaSuami || textField == _fieldNamaPerusahaanLainnya) &&
           textField.text.length == 35 && ![string isEqualToString:@""] ) {
            return NO;
            
        }
        else if ( textField == _fieldTeleponPerusahaan && textField.text.length == 12 && ![string isEqualToString:@""] ) {
            return NO;
        }
        else if ( textField == _fieldTeleponSuami && textField.text.length == 15 && ![string isEqualToString:@""] ) {
            return NO;
        }
    }
    return TRUE;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)dropdownSearch:(id)uiDropdownSearch didSelectRowWithData:(NSDictionary *)data
{
    if(uiDropdownSearch == dropdownPekerjaan)
    {
        _fieldPekerjaan.text = [data objectForKey:@"nama"];
        NSString* kodePekerjaan = [data objectForKey:@"nilai"];
        if (kodePekerjaan == nil || [kodePekerjaan isEqual:[NSNull null]]) {
            return;
        }
        [self getJabatan:kodePekerjaan];
    }
    else if(uiDropdownSearch == dropdownPekerjaanSuami)
    {
        _fieldPekerjaanSuami.text = [data objectForKey:@"nama"];
    }
    else if(uiDropdownSearch == dropdownStatusPekerjaan)
    {
        _fieldStatusPekerjaan.text = [data objectForKey:@"nama"];
        NSString *status = [data objectForKey:@"nilai"];
        [self scenarioDoChange:status];
    }
    else if(uiDropdownSearch == dropdownNamaPerusahaan)
    {
        NSString *companyId = [data objectForKey:@"id_company"];
        NSString *companyAddress = [data objectForKey:@"value_address"];
        if ([companyId isEqualToString:companyIdLainnya]) {
            _fieldNamaPerusahaan.text = [data objectForKey:@"name_company"];
        } else {
            _fieldNamaPerusahaan.text = [data objectForKey:@"value_name_company"];
        }
        [self namaPerusahaanDidChange:companyId withAddress:companyAddress];
    }
}

- (void) scenarioDoChange:(NSString *)status
{
    _fieldPekerjaan.text = @"";
    _fieldNamaPerusahaan.text = @"";
    
    if([status isEqualToString:@"EMPLOYED"] || [status isEqualToString:@"SELF-EMPLOYED"])
    {
        [self hideAlamatPerusahaanField];
        [self hideNamaPerusahaanLainnya];
        scenario = 1;
    }
    else if ([status isEqualToString:@"HOUSEWIFE"] || [status isEqualToString:@"STUDENT"])
        scenario = 2;
    else
    {
        scenario = 0;
    }
    
    NSString *endpoint = [NSString stringWithFormat:@"api/master/reference/listAllByKategori?kategori=PEKERJAAN&reference=%@", status];
    [dropdownPekerjaan getReferenceDataFromEndPoint:endpoint withKeysToStore:[[NSArray alloc] initWithObjects:@"nilai", @"nama", nil] andSortUsingKey:@"nourut" andDispatchGroup:referenceGroup];
    [self showLoading];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        // REMOVE IBU RUMAH TANGGA IF GENDER IS MALE
        if([[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN] isEqualToString:@"MALE"])
        {
            NSMutableArray *currentArray = [[NSMutableArray alloc] initWithArray:self->dropdownPekerjaan.allItems];
            NSMutableArray *discardArray = [[NSMutableArray alloc] init];
            for (NSMutableDictionary *statusPekerjaan in currentArray) {
                if ([[statusPekerjaan objectForKey:@"nilai"] isEqualToString:@"HOUSEWIFE"])
                    [discardArray addObject:statusPekerjaan];
            }
            [currentArray removeObjectsInArray:discardArray];
            self->dropdownPekerjaan.allItems = currentArray;
        }
        
        self->dropdownPekerjaan.displayedItems = self->dropdownPekerjaan.allItems;
        [self->dropdownPekerjaan.tableView reloadData];
    });
    
    [self scenarioDidChange];
}

- (void) scenarioDidChange
{
    for (UIView *subView in _contentView.subviews) {
        if([subView isKindOfClass:UIView.class])
        {
            if(subView.tag == scenario)
                [subView setHidden:NO];
            else if (subView.tag != 0)
            {
                [subView setHidden:YES];
                for (UIView *aView in subView.subviews) {
                    if([aView isKindOfClass:UITextField.class])
                    {
                        UITextField *field = (UITextField *)aView;
                        field.text = @"";
                    }
                }
            }
        }
    }
    [self changeContentViewHeight];
}

- (void)namaPerusahaanDidChange:(NSString *)companyId withAddress:(NSString *)address {
    if ([companyId isEqualToString:companyIdLainnya]) {
        [_fieldAlamatPerusahaan setText:address];
        [self showNamaPerusahaanLainnya];
    } else {
        [_fieldAlamatPerusahaan setText:address];
        [self hideNamaPerusahaanLainnya];
    }
    [self showAlamatPerusahaanField];
}

- (void)hideAlamatPerusahaanField {
    if (!_labelAlamatPerusahaan.isHidden) {
        CGRect newLabelAlamatPerusahaanFrame = _labelAlamatPerusahaan.frame;
        newLabelAlamatPerusahaanFrame.size.height = 0;
        CGRect newFieldAlamatPerusahaanFrame = _fieldAlamatPerusahaan.frame;
        newFieldAlamatPerusahaanFrame.size.height = 0;
        
        [_fieldAlamatPerusahaan setText:@""];
        [_labelAlamatPerusahaan setClipsToBounds:YES];
        [_fieldAlamatPerusahaan setClipsToBounds:YES];
        [_labelAlamatPerusahaan setFrame:newLabelAlamatPerusahaanFrame];
        [_fieldAlamatPerusahaan setFrame:newFieldAlamatPerusahaanFrame];
        [_labelAlamatPerusahaan setHidden:YES];
        [_fieldAlamatPerusahaan setHidden:YES];
        _constraintLabelAlamatPerusahaan.constant = 0;
        _constraintFieldAlamatPerusahaan.constant = 0;
        _constraintLabelBidangUsaha.priority = 999;
        _constraintLabelBidangUsahaAlamat.priority = 500;
        _constraintLabelBidangUsaha.active = YES;
        _constraintLabelBidangUsahaAlamat.active = YES;
        
        [self.view layoutIfNeeded];
    }
}

- (void)showAlamatPerusahaanField {
    if (_labelAlamatPerusahaan.isHidden) {
        [_labelAlamatPerusahaan setFrame:labelAlamatPerusahaanFrame];
        [_fieldAlamatPerusahaan setFrame:fieldAlamatPerusahaanFrame];
        [_labelAlamatPerusahaan setHidden:NO];
        [_fieldAlamatPerusahaan setHidden:NO];
        _constraintLabelAlamatPerusahaan.constant = 12;
        _constraintFieldAlamatPerusahaan.constant = 4;
        _constraintLabelBidangUsaha.priority = 500;
        _constraintLabelBidangUsahaAlamat.priority = 999;
        _constraintLabelBidangUsaha.active = YES;
        _constraintLabelBidangUsahaAlamat.active = YES;
        
        [self.view layoutIfNeeded];
    }
}

- (void)hideNamaPerusahaanLainnya {
    [_fieldNamaPerusahaanLainnya setText:@""];
    [_fieldNamaPerusahaanLainnya setHidden:YES];
    _constraintLabelAlamatPerusahaan.priority = 999;
    _constraintLabelAlamatPerLainnya.priority = 500;
    _constraintFieldAlamatPerusahaan.active = YES;
    _constraintLabelAlamatPerLainnya.active = YES;
}

- (void)showNamaPerusahaanLainnya {
    [_fieldNamaPerusahaanLainnya setHidden:NO];
    _constraintLabelAlamatPerusahaan.priority = 500;
    _constraintLabelAlamatPerLainnya.priority = 999;
    _constraintFieldAlamatPerusahaan.active = YES;
    _constraintLabelAlamatPerLainnya.active = YES;
}

- (void)getPenghasilan
{
    listPenghasilan = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/reference/listAllByKategori?kategori=PENGHASILAN" dispatchGroup:referenceGroup returnData:listPenghasilan];
}

- (void)getJabatan
{
    listJabatan = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/reference/listAllByKategori?kategori=JABATAN" dispatchGroup:referenceGroup returnData:listJabatan];
}
- (void)getJabatan:(NSString *)kodePekerjaan
{
    [self->_fieldJabatan clearData];
    
    NSString* urlJabatan = [NSString stringWithFormat:@"api/master/reference/listAllByKategori?kategori=JABATAN&reference=%@", kodePekerjaan];
    listJabatan = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:urlJabatan dispatchGroup:referenceGroup returnData:listJabatan];
    [self showLoading];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        if([[self->listJabatan objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [self->listJabatan objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldJabatan.options = options;
            if ([self->_fieldJabatan.options count] > 0) {
                [self->_fieldJabatan selectFirstIndex];
            }
        }
    });
}

-(void)getBidangUsaha
{
    listBidangusaha = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/reference/listAllByKategori?kategori=BIDANGUSAHA" dispatchGroup:referenceGroup returnData:listBidangusaha];
}

- (void) getReferenceFromEndPoint:(NSString *)endpoint forField:(UIDropdown *)field
{
    dispatch_group_enter(referenceGroup);
    NSString *url =[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:endpoint]];
    NSString *token = [CustomerOnboardingData getToken];
    NSLog(@"---------------- %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    [[session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error)
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSDictionary *value;
            if(ONBOARDING_SAFE_MODE)
            {
                NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSData *decData = [Encryptor AESDecryptData:encString];
                value = [NSJSONSerialization JSONObjectWithData:decData options:kNilOptions error:&error];
            }
            else
                value = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if([httpResponse statusCode] == 200) {
                if(value)
                {
                    NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
                    for(NSDictionary *aData in value)
                    {
                        [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
                    }
                    field.options = options;
                }
            }
            else
                NSLog(@"Error retrieve from %@ : %@", url, value);
        }
        else
            NSLog(@"Error retrieve from %@ : %@", url, error);
        dispatch_group_leave(self->referenceGroup);
    }] resume];
}

- (BOOL)checkInput
{
    int countIsValid = 0;
    if (![self checkNullForField:_fieldStatusPekerjaan withErrorText:NSLocalizedString(@"OCCUPATION_STATUS_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
    if (![self checkNullForField:_fieldPekerjaan withErrorText:NSLocalizedString(@"OCCUPATION_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
    
    if(scenario == 1)
    {
        if(![self checkNullForField:_fieldJabatan withErrorText:NSLocalizedString(@"POSITION_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldPenghasilan withErrorText:NSLocalizedString(@"INCOME_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldNamaPerusahaan withErrorText:NSLocalizedString(@"COMPANY_NAME_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        else if(![self checkLengthForField:_fieldNamaPerusahaan withMaxLength:35 andErrorText:NSLocalizedString(@"COMPANY_NAME_NOT_VALID", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldBidangUsaha withErrorText:NSLocalizedString(@"BUSINESS_FIELD_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldAlamatPerusahaan withErrorText:NSLocalizedString(@"COMPANY_ADDRESS_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        else if(![self checkLengthForField:_fieldAlamatPerusahaan withMaxLength:35 andErrorText:NSLocalizedString(@"COMPANY_ADDRESS_NOT_VALID", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldMulaiBekerja withErrorText:NSLocalizedString(@"DATE_OF_WORK_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldTeleponPerusahaan withErrorText:NSLocalizedString(@"COMPANY_PHONE_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        else if (![self checkLengthForField:_fieldTeleponPerusahaan withMaxLength:12 andErrorText:NSLocalizedString(@"COMPANY_PHONE_MAX_LENGTH", @"")]) countIsValid +=1;
        else if (![Utility isNumericOnly:_fieldTeleponPerusahaan.text]) {
            countIsValid +=1;
            [self showErrorLabel:NSLocalizedString(@"COMPANY_PHONE_NOT_VALID", @"") withUiTextfield:_fieldTeleponPerusahaan];
        }
        
        
        NSString *companyId = [self->dropdownNamaPerusahaan.selectedData objectForKey:@"id_company"];
        if (![companyId isEqual:nil]) {
            NSMutableDictionary *namaPerusahaanLainnya = [[NSMutableDictionary alloc] initWithDictionary:self->dropdownNamaPerusahaan.selectedData copyItems:YES];
            if ([companyId isEqualToString:companyIdLainnya]) {
                if(![self checkLengthForField:_fieldNamaPerusahaanLainnya withMaxLength:35 andErrorText:NSLocalizedString(@"COMPANY_NAME_CANNOT_BE_EMPTY", @"")]) {
                    countIsValid +=1;
                } else {
                    [namaPerusahaanLainnya setObject:_fieldNamaPerusahaanLainnya.text forKey:@"value_name_company"];
                }
            }
            [namaPerusahaanLainnya setObject:_fieldAlamatPerusahaan.text forKey:@"value_address"];
            [self->dropdownNamaPerusahaan setSelectedData:namaPerusahaanLainnya];
        }
    }
    else if(scenario == 2)
    {
        if(![self checkNullForField:_fieldNamaSuami withErrorText:NSLocalizedString(@"HUSBAND_NAME_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        else if (![self checkLengthForField:_fieldNamaSuami withMaxLength:35 andErrorText:NSLocalizedString(@"HUSBAND_NAME_NOT_VALID", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldPekerjaanSuami withErrorText:NSLocalizedString(@"HUSBAND_OCCUPATION_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldAlamatSuami withErrorText:NSLocalizedString(@"HUSBAND_ADDRESS_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        else if(![self checkLengthForField:_fieldAlamatSuami withMaxLength:35 andErrorText:NSLocalizedString(@"HUSBAND_ADDRESS_NOT_VALID", @"")]) countIsValid +=1;
        if(![self checkNullForField:_fieldTeleponSuami withErrorText:NSLocalizedString(@"HUSBAND_PHONE_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
        else if (![self checkLengthForField:_fieldTeleponSuami withMaxLength:15 andErrorText:NSLocalizedString(@"HUSBAND_PHONE_MAX_LENGTH", @"")]) countIsValid +=1;
        else if (![Utility isNumericOnly:_fieldTeleponSuami.text]) {
            countIsValid +=1;
            [self showErrorLabel:NSLocalizedString(@"HUSBAND_PHONE_NOT_VALID", @"") withUiTextfield:_fieldTeleponSuami];
        }
    }
    
    return countIsValid == 0 ? TRUE : FALSE;
}

- (BOOL) checkNullForField:(UITextField *)field withErrorText:(NSString *)error
{
    if([field.text isEqualToString:@""])
    {
        [self inputHasError:TRUE textField:field];
        [self showErrorLabel:error withUiTextfield:field];
        return FALSE;
    }
    return TRUE;
}

- (BOOL) checkLengthForField:(UITextField *)field withMaxLength:(NSInteger)length andErrorText:(NSString *)error
{
    if(field.text.length > length)
    {
        [self inputHasError:TRUE textField:field];
        [self showErrorLabel:error withUiTextfield:field];
        return FALSE;
    }
    return TRUE;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if(![self checkInput]) return NO;
            else
            {
                [self saveDropDownData:formDropDowns];
                [self saveTextFieldData:formTextFields];
                [encryptor setUserDefaultsObject:dropdownStatusPekerjaan.selectedData forKey:ONBOARDING_INFORMASI_KEUANGAN_STATUS_PEKERJAAN_KEY];
                [encryptor setUserDefaultsObject:dropdownPekerjaan.selectedData forKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_KEY];
                
                if(scenario == 1)
                {
                    [encryptor setUserDefaultsObject:dataMulaiKerja forKey:ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY];
                    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_SUAMI_KEY];
                    [encryptor setUserDefaultsObject:dropdownNamaPerusahaan.selectedData forKey:ONBOARDING_INFORMASI_KEUANGAN_NAMA_PERUSAHAAN_KEY];
                    [encryptor setUserDefaultsObject:_fieldAlamatPerusahaan.text forKey:ONBOARDING_INFORMASI_KEUANGAN_ALAMAT_PERUSAHAAN_KEY];
                }
                else if(scenario == 2)
                {
                    [encryptor setUserDefaultsObject:dropdownPekerjaanSuami.selectedData forKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_SUAMI_KEY];
                    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY];
                }
                else
                {
                    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_SUAMI_KEY];
                    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_MULAI_BEKERJA_KEY];
                }
            }
        }
    }
    
    return YES;
}

@end
