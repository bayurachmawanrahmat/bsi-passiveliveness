//
//  JenisTabunganDetailController.h
//  BSM-Mobile
//
//  Created by ARS on 20/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol JenisTabunganDetailDelegate;

@interface JenisTabunganDetailController : OnboardingRootController

@property (weak, nonatomic) id<JenisTabunganDetailDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *name1;
@property (weak, nonatomic) IBOutlet UILabel *longdesc;
@property (weak, nonatomic) IBOutlet UILabel *name2;
@property (weak, nonatomic) IBOutlet UILabel *feature;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
- (IBAction)hitBtnSelect:(id)sender;
- (IBAction)hitBtnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *textContainerView;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

- (void) setName1Text:(NSString *)text;
- (void) setName2Text:(NSString *)text;
- (void) setLongdescText:(NSString *)text;
- (void) setFeatureText:(NSString *)text;

@end

@protocol JenisTabunganDetailDelegate <NSObject>

@end

NS_ASSUME_NONNULL_END
