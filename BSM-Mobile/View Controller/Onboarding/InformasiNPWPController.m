//
//  InformasiNPWPController.m
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "InformasiNPWPController.h"
#import "CameraOverlayController.h"
#import "CameraOverlayView.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"
#import "Utility.h"

@interface InformasiNPWPController ()
{
    NSDictionary *formInputs;
    Encryptor *encryptor;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@end

@implementation InformasiNPWPController

static const NSString *ScreenName = @"InformasiNpwpScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    encryptor = [[Encryptor alloc] init];
    self.view.hidden = NO;
    self.contentView.layer.cornerRadius = 15.0f;
    _btnRetake.layer.cornerRadius = 24.0f;
    
    formInputs = [[NSDictionary alloc] initWithObjectsAndKeys:
                  _fieldNomorNPWP, ONBOARDING_INFORMASI_NPWP_NOMOR_KEY,
                  nil];
    
    [self setTextFieldData:formInputs];
    
    _fieldNomorNPWP.delegate = self;
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self setScrollView:_scrollView];
    [self setMainView:self.view];
    [self registerForKeyboardNotifications];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    if(!_imagePicked && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _imagePicked = [self loadImageFromLocalStorageWithFileName:@"NPWP" ofType:@"jpg" inDirectory:documentsDirectory];
        
        if(!_imagePicked)
        {
            CGRect screenBound = [[UIScreen mainScreen] bounds];
            CGSize screenSize = screenBound.size;
            CGFloat screenWidth = screenSize.width;
            CGFloat screenHeight = screenSize.height;

            // add button Tidak Punya NPWP
            UIButton *btnNoNpwp = [UIButton buttonWithType:UIButtonTypeCustom];
            [btnNoNpwp addTarget:self action:@selector(didNoNpwp:) forControlEvents:UIControlEventTouchUpInside];
            [btnNoNpwp setTitle:NSLocalizedString(@"DONT_HAVE_NPWP", @"") forState:UIControlStateNormal];
            btnNoNpwp.titleLabel.font = [UIFont boldSystemFontOfSize:12.0f];
            btnNoNpwp.layer.cornerRadius = 20;
            btnNoNpwp.layer.masksToBounds = true;
            btnNoNpwp.layer.borderWidth = 2;
            btnNoNpwp.layer.borderColor = [[UIColor whiteColor] CGColor];
            btnNoNpwp.frame = CGRectMake(10, (screenHeight - 160), (screenWidth - 20), 50);

            CameraOverlayController *overlayController = [[CameraOverlayController alloc] init];

            _cameraPicker = [[UIImagePickerController alloc] init];
            _cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            _cameraPicker.delegate = self;
            _cameraPicker.showsCameraControls = false;
            _cameraPicker.allowsEditing = false;

            CameraOverlayView *cameraView = (CameraOverlayView *)overlayController.view;
            cameraView.frame = _cameraPicker.view.frame;
            cameraView.textUnderBox.text = NSLocalizedString(@"DESC_NPWP_POSITION", @"");
            cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_ktp@2x.png"];
            [cameraView.navigationBar.topItem setTitle:NSLocalizedString(@"PHOTO_NPWP", @"")];
            cameraView.constraintTextMarginBottom.constant = 90.0f;

            [cameraView addSubview:btnNoNpwp];
            cameraView.delegate = self;

            _cameraPicker.cameraOverlayView = cameraView;

            [self presentViewController:_cameraPicker animated:YES completion:nil];
        }
        else
        {
            self.view.hidden = FALSE;
            [self setImageView];
        }
    }
    else self.view.hidden = FALSE;
    
    CGFloat yPos = self.scrollView.frame.origin.y;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:3 fromTotal:7];
    
    _contentViewHeight.constant = SCREEN_HEIGHT - (yPos + _imageView.frame.size.height + 15);
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"CANCEL", @"") style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"DONE", @"") style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.fieldNomorNPWP.inputAccessoryView = numberToolbar;
}

- (void)didNoNpwp:(UIButton *)sender
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"TidakPunyaNPWPScreen"];
    [_cameraPicker presentViewController:vc animated:YES completion:nil];
}

- (void)didTakePhoto:(CameraOverlayView *)overlayView
{
    [_cameraPicker takePicture];
}

- (void)didBack:(CameraOverlayView *)overlayView
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiWajahScreen"];
    [_cameraPicker presentViewController:vc animated:YES completion:nil];
}

-(void)cancelNumberPad{
    if([self.fieldNomorNPWP isFirstResponder])
    {
        [self.fieldNomorNPWP resignFirstResponder];
        self.fieldNomorNPWP.text = @"";
    }
}

-(void)doneWithNumberPad{
    if([self.fieldNomorNPWP isFirstResponder])
    {
        [self.fieldNomorNPWP resignFirstResponder];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    [_cameraPicker dismissViewControllerAnimated:YES completion:nil];
    
    _imagePicked = info[UIImagePickerControllerOriginalImage];
    
    [self setImageView];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self inputHasError:NO textField:textField];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _fieldNomorNPWP && textField.text.length == 15 && ![string isEqualToString:@""]) return NO;
    return YES;
}


- (IBAction)hitBtnRetake:(id)sender {
    _imagePicked = nil;
    [self removeImageFromLocalStorage:@"NPWP"];
    [self viewDidLoad];
    [self viewDidAppear:YES];
}

- (BOOL)checkInput
{
    BOOL isValid = TRUE;
    
    if([_fieldNomorNPWP.text isEqualToString:@""])
    {
        [self inputHasError:YES textField:_fieldNomorNPWP];
        [self showErrorLabel:NSLocalizedString(@"NPWP_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldNomorNPWP];
        isValid = FALSE;
    }
    else if(_fieldNomorNPWP.text.length != 15 || ![Utility isNumericOnly:_fieldNomorNPWP.text])
    {
        [self inputHasError:YES textField:_fieldNomorNPWP];
        [self showErrorLabel:NSLocalizedString(@"NPWP_NOT_VALID", @"") withUiTextfield:_fieldNomorNPWP];
        isValid = FALSE;
    }
    
    return isValid;
}

- (void)setImageView
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    CGRect rect = _imageView.frame;
    
    _imageHeight.constant = (screenHeight / 2.5);
    
    _imageView.frame = CGRectMake(rect.origin.x, rect.origin.y, screenWidth, _imageHeight.constant);
    _imageView.image = _imagePicked;
    _imageView.clipsToBounds = YES;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if([self checkInput])
            {
                [self saveTextFieldData:formInputs];
                [encryptor setUserDefaultsObject:@"1" forKey:ONBOARDING_INFORMASI_NPWP_ISPUNYA_KEY];
                [encryptor setUserDefaultsObject:@"0" forKey:ONBOARDING_INFORMASI_NPWP_JENIS_KEPEMILIKAN_KEY];
                
                if(_imagePicked)
                {
                    // SAVE IMAGE
                    [self saveImageToLogicalDocs:self.imagePicked withFilename:@"NPWP" andSaveWithKey:ONBOARDING_INFORMASI_NPWP_FILEID_KEY onCompletionPerformSegueWithIdentifier:identifier fromController:self];
                }
                else return YES;
            }
            return NO;
        }
    }
    
    return YES;
}

@end
