//
//  InformasiStatusController.m
//  BSM-Mobile
//
//  Created by ARS on 22/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "InformasiStatusController.h"
#import "AMPopTip.h"
#import "NSObject+FormInputCategory.h"
#import "UIDropdownSearch.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "InformasiSelfieController.h"

@interface InformasiStatusController () {
    UIDropdownSearch *dropdownPernikahan;
    UIDropdownSearch *dropdownAgama;
    UIDropdownSearch *dropdownPendidikan;
    NSString *pageError;
    Encryptor *encryptor;
    dispatch_group_t referenceGroup;
}

@end

@implementation InformasiStatusController

static const NSString *ScreenName = @"InformasiPribadiThirdScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    referenceGroup = dispatch_group_create();
    
    self.fieldPernikahan.delegate = self;
    self.fieldAgama.delegate = self;
    self.fieldPendidikan.delegate = self;
    
    // init gender value
    [self setupGenderOption];
    NSString *nomorKtp = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
    if (nomorKtp.length == 16) {
        unichar genderValue = [nomorKtp characterAtIndex:6];
        if (genderValue == '0' || genderValue == '1' || genderValue == '2' || genderValue == '3') {
            [self chooseGender:YES];
        } else {
            [self chooseGender:NO];
        }
    }
    
    [self refreshData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
}


- (void) refreshData {
    [self showLoading];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    
    // setup dropdown
    dropdownPernikahan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownPernikahan.displayedStringKey = @"nama";
    [dropdownPernikahan getReferenceDataFromEndPoint:@"api/master/reference/listByKategori?kategori=STATUSNIKAH" withKeysToStore:[[NSArray alloc] initWithObjects:@"refid", @"nama", @"nilai", @"nourut", nil] andSortUsingKey:@"nourut" andDispatchGroup:referenceGroup];
    dropdownPernikahan.delegate = self;
    
    dropdownAgama = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownAgama.displayedStringKey = @"nama";
    [dropdownAgama getReferenceDataFromEndPoint:@"api/master/reference/listByKategori?kategori=AGAMA" withKeysToStore:[[NSArray alloc] initWithObjects:@"refid", @"nama", @"nilai", @"nourut", nil] andSortUsingKey:@"nourut" andDispatchGroup:referenceGroup];
    dropdownAgama.delegate = self;
    
    dropdownPendidikan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownPendidikan.displayedStringKey = @"nama";
    [dropdownPendidikan getReferenceDataFromEndPoint:@"api/master/reference/listByKategori?kategori=PENDIDIKAN" withKeysToStore:[[NSArray alloc] initWithObjects:@"refid", @"nama", @"nilai", @"nourut", nil] andSortUsingKey:@"nourut" andDispatchGroup:referenceGroup];
    dropdownPendidikan.delegate = self;
    
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if(self->dropdownPernikahan.callApiSucess)
        {
            [self->dropdownPernikahan.tableView reloadData];
            self->dropdownPernikahan.displayedItems = self->dropdownPernikahan.allItems;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_MARRIAGE", @"") sender:self];
        }
        
        if(self->dropdownAgama.callApiSucess)
        {
            [self->dropdownAgama.tableView reloadData];
            self->dropdownAgama.displayedItems = self->dropdownAgama.allItems;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_RELIGION", @"") sender:self];
        }
        
        if(self->dropdownPendidikan.callApiSucess)
        {
            [self->dropdownPendidikan.tableView reloadData];
            self->dropdownPendidikan.displayedItems = self->dropdownPendidikan.allItems;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_EDUCATION", @"") sender:self];
        }
    });
    
    [self setSavedData];
}

- (void)setupGenderOption {
    UIImage *iconGenderActive = [UIImage imageNamed:@"ic_radio_active.png"];
    UIImage *iconGenderInactive = [UIImage imageNamed:@"ic_radio_inactive.png"];
    [self.radioMale setBackgroundImage:iconGenderInactive  forState:UIControlStateNormal];
    [self.radioMale setBackgroundImage:iconGenderActive forState:UIControlStateSelected];
    [self.radioFemale setBackgroundImage:iconGenderInactive  forState:UIControlStateNormal];
    [self.radioFemale setBackgroundImage:iconGenderActive forState:UIControlStateSelected];
}

- (void)chooseGender:(BOOL)isMale {
    self.radioMale.selected = isMale;
    self.radioFemale.selected = !isMale;
    
    [self.view layoutIfNeeded];
}

- (void) setSavedData {
    NSString *gender = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN];
    if (gender != nil) {
        [self chooseGender:([gender isEqualToString:@"MALE"])];
    }
    
    dropdownPernikahan.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_STATUS_KAWIN_OBJECT];
    if([dropdownPernikahan.selectedData isKindOfClass:NSDictionary.class])
        _fieldPernikahan.text = [dropdownPernikahan.selectedData objectForKey:dropdownPernikahan.displayedStringKey];
    
    dropdownAgama.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_AGAMA_OBJECT];
    if([dropdownAgama.selectedData isKindOfClass:NSDictionary.class])
        _fieldAgama.text = [dropdownAgama.selectedData objectForKey:dropdownAgama.displayedStringKey];
    
    dropdownPendidikan.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_PENDIDIKAN_KEY];
    if([dropdownPendidikan.selectedData isKindOfClass:NSDictionary.class])
        _fieldPendidikan.text = [dropdownPendidikan.selectedData objectForKey:dropdownPendidikan.displayedStringKey];
}

- (void)dropdownSearch:(id)uiDropdownSearch didSelectRowWithData:(NSDictionary *)data {
    if (uiDropdownSearch == dropdownPernikahan) {
        [self.fieldPernikahan setText:[data objectForKey:@"nama"]];
    }
    else if (uiDropdownSearch == dropdownAgama) {
        [self.fieldAgama setText:[data objectForKey:@"nama"]];
    }
    else if (uiDropdownSearch == dropdownPendidikan) {
        [self.fieldPendidikan setText:[data objectForKey:@"nama"]];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self inputHasError:NO textField:textField];
    
    if(textField == _fieldPernikahan)
    {
        [self presentViewController:dropdownPernikahan animated:YES completion:nil];
        return NO;
    }
    else if(textField == _fieldAgama)
    {
        [self presentViewController:dropdownAgama animated:YES completion:nil];
        return NO;
    }
    else if(textField == _fieldPendidikan)
    {
        [self presentViewController:dropdownPendidikan animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (void) saveData {
    NSString *gender = _radioMale.isSelected ? @"MALE" : @"FEMALE";
    [encryptor setUserDefaultsObject:gender forKey:ONBOARDING_DUKCAPIL_JENIS_KLMIN];
    [encryptor setUserDefaultsObject:dropdownPernikahan.selectedData forKey:ONBOARDING_DUKCAPIL_STATUS_KAWIN_OBJECT];
    [encryptor setUserDefaultsObject:[dropdownPernikahan.selectedData objectForKey:@"nilai"] forKey:ONBOARDING_DUKCAPIL_STATUS_KAWIN];
    [encryptor setUserDefaultsObject:dropdownAgama.selectedData forKey:ONBOARDING_DUKCAPIL_AGAMA_OBJECT];
    [encryptor setUserDefaultsObject:[dropdownAgama.selectedData objectForKey:@"nilai"] forKey:ONBOARDING_DUKCAPIL_AGAMA];
    [encryptor setUserDefaultsObject:dropdownPendidikan.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_PENDIDIKAN_KEY];
    
    // empty existing value
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_NO_KK];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_DUSUN];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_GOL_DARAH];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_PDDK_AKH];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_STAT_HBKEL];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_EKTP_STATUS];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_JENIS_PKRJN];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_NAMA_MASKING];
    [encryptor setUserDefaultsObject:@"" forKey:ONBOARDING_DUKCAPIL_NAMA_LGKP_AYAH];
}

- (BOOL)checkInput {
    BOOL isValid = TRUE;
    
    if([_fieldPernikahan.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"MARRIAGE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldPernikahan];
        [self inputHasError:YES textField:_fieldPernikahan];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldPernikahan];
    
    if([_fieldAgama.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"RELIGION_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldAgama];
        [self inputHasError:YES textField:_fieldAgama];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldAgama];
    
    if([_fieldPendidikan.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"EDUCATION_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldPendidikan];
        [self inputHasError:YES textField:_fieldPendidikan];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldPendidikan];
    
    return isValid;
}

- (IBAction)hitBtnNext:(id)sender {
    if([self checkInput])
    {
        // save data
        [self saveData];
        
        // navigate next
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        InformasiSelfieController *informasiSelfieController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiSelfieScreen"];
        [self presentViewController:informasiSelfieController animated:YES completion:nil];
    }
}

- (IBAction)hitRadioFemale:(id)sender {
    [self chooseGender:NO];
}

- (IBAction)hitRadioMale:(id)sender {
    [self chooseGender:YES];
}
@end
