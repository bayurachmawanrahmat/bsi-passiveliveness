//
//  CameraOverlayView.h
//  BSM-Mobile
//
//  Created by ARS on 16/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CameraOverlayDelegate;

@interface CameraOverlayView : UIView

@property (weak, nonatomic) IBOutlet UILabel *textUnderBox;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (strong, nonatomic) IBOutlet UIImageView *imageOverlay;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTextMarginBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintImageOverlayMarginBottom;

@property (weak, nonatomic) id<CameraOverlayDelegate> delegate;


- (IBAction)hitBtnBack:(id)sender;
- (IBAction)hitBtnTakePhoto:(id)sender;

@end

@protocol CameraOverlayDelegate <NSObject>

@required
-(void) didTakePhoto:(CameraOverlayView *) overlayView;
-(void) didBack:(CameraOverlayView *) overlayView;

@end

NS_ASSUME_NONNULL_END
