//
//  WowzaController.h
//  BSM-Mobile
//
//  Created by ARS on 18/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WowzaGoCoderSDK/WowzaGoCoderSDK.h>
#import <AVKit/AVKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WowzaController : UIViewController <CAAnimationDelegate, WOWZBroadcastStatusCallback>

@property (nonatomic, strong) WowzaGoCoder *goCoder;
@property (weak, nonatomic) IBOutlet UIButton *btnStepNum;
@property (weak, nonatomic) IBOutlet UILabel *labelCoderStatus;
@property (strong, nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerLayer *playerLayer;
@property (strong, nonatomic) AVPlayerViewController *playerVc;
@property (weak, nonatomic) IBOutlet UIView *videoContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;
- (IBAction)btnActionHit:(id)sender;

@end

NS_ASSUME_NONNULL_END
