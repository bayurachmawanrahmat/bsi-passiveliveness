//
//  CreatePinOnboardingController.m
//  BSM-Mobile
//
//  Created by ARS on 14/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "CreatePinOnboardingController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Styles.h"
#import "Encryptor.h"
#import "CustomerOnboardingData.h"
#import "NSObject+FormInputCategory.h"
#import "OnboardingTemplate01ViewController.h"
#import "GagalVerifikasiController.h"
#import "Rekening.h"
#import "TabunganBerhasilController.h"
#import "PembukaanAkunController.h"
#import "ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"
#import "NSUserdefaultsAes.h"

@interface CreatePinOnboardingController ()
{
    CustomerOnboardingData *onboardingData;
    dispatch_group_t referenceGroup;
    Encryptor *encryptor;
    NSString *flagTP1;
    NSString *flagTP2;
    NSDictionary *nasabahData;
}
@end

@implementation CreatePinOnboardingController

static const NSString *ScreenOtpName = @"InformasiNoHandphoneScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    referenceGroup = dispatch_group_create();
    encryptor = [[Encryptor alloc] init];
    
    flagTP1 = @"HIDE";
    flagTP2 = @"HIDE";
    
    [Styles setStyleTextFieldBottomLine:_fieldPin];
    [Styles setStyleTextFieldBottomLine:_fieldConfirmPin];
    
    self.viewCreatePIN.backgroundColor = [UIColor whiteColor];
    self.viewCreatePIN.layer.cornerRadius = 10;
    self.viewCreatePIN.layer.borderWidth = 1;
    self.viewCreatePIN.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    [self.vwScroll setContentSize:CGSizeMake(self.viewCreatePIN.frame.size.width, self.viewCreatePIN.frame.size.height)];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.fieldPin.inputAccessoryView = keyboardDoneButtonView;
    self.fieldConfirmPin.inputAccessoryView = keyboardDoneButtonView;
    
    [self.fieldPin setKeyboardType:UIKeyboardTypeNumberPad];
    [self.fieldConfirmPin setKeyboardType:UIKeyboardTypeNumberPad];
    
    [self.fieldPin setSecureTextEntry:true];
    [self.fieldConfirmPin setSecureTextEntry:true];
    self.fieldPin.delegate = self;
    self.fieldConfirmPin.delegate = self;
    
    [self registerForKeyboardNotifications];
    
    nasabahData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_NASABAH];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenOtpName forKey:ONBOARDING_PROGRESS_STEP_KEY];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.viewCreatePIN.frame.size.width, self.viewCreatePIN.frame.size.height + kbSize.height + 20)];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.viewCreatePIN.frame.size.width, self.viewCreatePIN.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (void) createPin{
    if (nasabahData == nil || [nasabahData isEqual:[NSNull null]]) {
        [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
    }
    NSString *nasabahId = [nasabahData objectForKey:@"nasabahid"];
    NSString *versionName = @VERSION_NAME;
    NSString *versionValue = @VERSION_VALUE;
    //generate public key
    BDError *error = [[BDError alloc] init];
    BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
    BDRSACryptorKeyPair *RSAKeyPair = [RSACryptor generateKeyPairWithKeyIdentifier:BSM_SERVER_KEY error:error];
    NSString *publicKey = RSAKeyPair.publicKey;
    NSString *privateKey = RSAKeyPair.privateKey;
    NSString *msisdn = [nasabahData objectForKey:@"no_hp"];
    NSString *device = [Utility deviceName];
    NSString *deviceType = [UIDevice currentDevice].model;
    //generate IMEI/ICCID
    NSString *unique_id = [self getUUID];
    NSString *paramIMEI = [unique_id substringToIndex:16]; //imei
    NSString *paramICCID = [unique_id substringFromIndex:16]; //iccid
    NSString *ipAddress = [Utility deviceIPAddress];
    //NSString *dateLocal = [Utility dateToday:5];
    NSString *osType = @"2";
    NSString *osVersion = [Utility deviceOsVersion];
    NSString *language = @"id";
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                              self->_fieldPin.text, @"pin",
                              self->_fieldConfirmPin.text, @"cpin",
                              nasabahId, @"nasabahid",
                              versionName, @"version_name",
                              versionValue, @"version_value",
                              [self getUrlEncoded:publicKey], @"publickey",
                              msisdn, @"no_hp",
                              device, @"device",
                              deviceType, @"deviceType",
                              paramIMEI, @"imei",
                              paramICCID, @"iccid",
                              ipAddress, @"ip_address",
                              osType, @"os_type",
                              osVersion, @"os_version",
                              language, @"language",
                              nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];

    [CustomerOnboardingData postToEndPoint:@"/api/pin/create" dispatchGroup:referenceGroup postData:postData returnData:returnData];
    
    [self showLoading];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *data = [returnData objectForKey:@"data"];
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:[data valueForKey:@"name"] forKey:@"customer_name"];
            [NSUserdefaultsAes setObject:[data valueForKey:@"customer_id"] forKey:@"customer_id"];
            [NSUserdefaultsAes setObject:[data valueForKey:@"session_id"] forKey:@"session_id"];
            [NSUserdefaultsAes setObject:[data valueForKey:@"msisdn"] forKey:@"msisdn"];
            [NSUserdefaultsAes setObject:[data valueForKey:@"email"] forKey:@"email"];
            [NSUserdefaultsAes setObject:[data valueForKey:@"name"] forKey:@"name"];
            [NSUserdefaultsAes setObject:[data valueForKey:@"clearZPK"] forKey:@"zpk"];
            [NSUserdefaultsAes setObject:[data valueForKey:@"public_key"] forKey:@"publickey"];
            [NSUserdefaultsAes setObject:privateKey forKey:@"privatekey"];
            [userDefault synchronize];
            
            NSString *sessionId = [NSUserdefaultsAes getValueForKey:@"session_id"];
            NSString *msisdn = [NSUserdefaultsAes getValueForKey:@"msisdn"];
            NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
            NSString *name = [NSUserdefaultsAes getValueForKey:@"name"];
            NSString *zpk = [NSUserdefaultsAes getValueForKey:@"zpk"];
            NSString *publickey = [NSUserdefaultsAes getValueForKey:@"publickey"];
            NSString *privatekey = [NSUserdefaultsAes getValueForKey:@"privatekey"];
            
            [self checkNasabahAndGoToBerhasilScreen];
        } else {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
        }
    });
}

-(void)checkNasabahAndGoToBerhasilScreen {
    //if (nasabahData != nil && ![nasabahData isEqual:[NSNull null]]) {
    //    [self goToBerhasilScreen:nasabahData];
    //    return;
    //}
    dispatch_group_t dispatchGroup = dispatch_group_create();
    NSMutableDictionary *checkResult = [[NSMutableDictionary alloc] init];
    NSString *msmsid = [CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:dispatchGroup returnData:checkResult];
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[checkResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *nasabah = [checkResult objectForKey:@"data"];
            
            [self goToBerhasilScreen:nasabah];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:@"Terjadi kesalahan. Silahkan coba beberapa saat lagi." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:alertAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    });
}

- (void) goToBerhasilScreen:(NSDictionary *)nasabah {
    Rekening *rekening = [[Rekening alloc] init];
    [rekening setNamaNasabah:[nasabah objectForKey:@"nama_lgkp"]];
    [rekening setNomorRekening:[nasabah objectForKey:@"norek"]];
    [rekening setJenisTabungan:[nasabah objectForKey:@"jenis_tabungan"]];
    [rekening setJenisKartu:[nasabah objectForKey:@"jenis_kartu"]];
    [rekening setKodeCabang:[nasabah objectForKey:@"kode_cabang"]];
    [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
    [CustomerOnboardingData setObject:rekening forKey:ONBOARDING_REKENING_INFO_KEY];
    
    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
    TabunganBerhasilController *contentVC = [[TabunganBerhasilController alloc] initWithRekening:rekening];
    [templateVC setChildController:contentVC];
    [self presentViewController:templateVC animated:YES completion:nil];
}

- (NSString*)getUUID {
    NSString *str = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return str;
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)hitBtnNext:(id)sender {
    if ([self.fieldPin.text length] != 6){
        [Utility showMessage:lang(@"PIN_MUST_6_DIGIT") instance:self];
    } else if([self.fieldConfirmPin.text isEqualToString:@""]){
        [Utility showMessage:lang(@"NOT_EMPTY") instance:self];
    }else if([self.fieldPin.text isEqualToString:@""]){
        [Utility showMessage:lang(@"NOT_EMPTY") instance:self];
    } else if(![self.fieldPin.text isEqualToString:self.fieldConfirmPin.text]){
        [Utility showMessage:lang(@"PIN_NOT_SAME") instance:self];
    } else if(![Utility isNumericOnly:_fieldPin.text]){
        [Utility showMessage:lang(@"PIN_NOT_VALID") instance:self];
    } else if(![Utility isNumericOnly:_fieldConfirmPin.text]){
        [Utility showMessage:lang(@"CONFIRMATION_PIN_NOT_VALID") instance:self];
    }  else {
        [self createPin];
    }
}

- (IBAction)hitToggleConfirmPin:(id)sender {
    if ([flagTP2 isEqualToString:@"SHOW"]) {
        flagTP2 = @"HIDE";
        [self.fieldConfirmPin setSecureTextEntry:true];
        [self.btnConfirmPinToggle setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTP2 isEqualToString:@"HIDE"]) {
        flagTP2 = @"SHOW";
        [self.fieldConfirmPin setSecureTextEntry:false];
        [self.btnConfirmPinToggle setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)hitTogglePin:(id)sender {
    if ([flagTP1 isEqualToString:@"SHOW"]) {
        flagTP1 = @"HIDE";
        [self.fieldPin setSecureTextEntry:true];
        [self.btnPinToggle setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTP1 isEqualToString:@"HIDE"]) {
        flagTP1 = @"SHOW";
        [self.fieldPin setSecureTextEntry:false];
        [self.btnPinToggle setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == self.fieldPin && textField.text.length == 6 && ![string isEqualToString:@""]){
        return NO;
    }
    else if(textField == self.fieldConfirmPin && textField.text.length == 6 && ![string isEqualToString:@""]){
        return NO;
    }
    return YES;
}

- (NSString *)getUrlEncoded:(NSString *)text {
    NSCharacterSet* set = [NSCharacterSet characterSetWithCharactersInString:@"!*'();@&=+$,?%#[]"];
    NSString *escapedText = [text stringByAddingPercentEncodingWithAllowedCharacters:[set invertedSet]];
    
    return escapedText;
}
@end
