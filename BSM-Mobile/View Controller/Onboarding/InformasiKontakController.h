//
//  InformasiKontakController.h
//  BSM-Mobile
//
//  Created by ARS on 10/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiKontakController : OnboardingRootController

- (BOOL)validateEmailWithString:(NSString*)email;
- (void) showErrorMsg:(NSString *)errorMsg withUiTextfield:(UITextField *)textField;
-(void) requestOtp;
+(NSString *) generateOtp;
@end

NS_ASSUME_NONNULL_END
