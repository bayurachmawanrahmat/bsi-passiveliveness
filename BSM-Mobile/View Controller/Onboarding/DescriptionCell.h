//
//  DescriptionCell.h
//  BSM-Mobile
//
//  Created by ARS on 27/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDesc;

@end

NS_ASSUME_NONNULL_END
