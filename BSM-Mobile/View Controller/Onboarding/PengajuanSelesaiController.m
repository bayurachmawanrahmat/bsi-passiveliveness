//
//  PengajuanSelesaiController.m
//  BSM-Mobile
//
//  Created by ARS on 26/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PengajuanSelesaiController.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@interface PengajuanSelesaiController ()

@end

@implementation PengajuanSelesaiController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    Encryptor *encryptor = [[Encryptor alloc] init];
    NSLog(@"POSTED USERID: %@", [NSString stringWithFormat:@"%@.%@@Onboarding", [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY], [encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY]]);
    dispatch_group_t dispatchGroup = dispatch_group_create();
    NSDictionary *postData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  [NSString stringWithFormat:@"%@.%@@Onboarding", [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY], [encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY]], @"userid",
                                  nil];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData postToEndPoint:@"api/onboarding/downloadVideoOracle" dispatchGroup:dispatchGroup postData:postData returnData:returnData];
    [encryptor setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
