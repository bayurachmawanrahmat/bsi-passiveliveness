//
//  JenisKartuController.m
//  BSM-Mobile
//
//  Created by ARS on 21/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "JenisKartuController.h"
#import "AMPopTip.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@interface JenisKartuController ()
{
    
    __weak IBOutlet UIButton *btnCheck;
    NSDictionary *selectedCard;
    NSInteger selectedCardID;
    NSMutableArray *listCards;
    NSMutableDictionary *refCards;
    dispatch_group_t referenceGroup;
    Encryptor *encryptor;
    BOOL apiSuccess;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;
@end

@implementation JenisKartuController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIView *topBox = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 120)];
    topBox.backgroundColor = [UIColor colorWithRed:236.0f/255.0f
                                             green:243.0f/255.0f
                                              blue:242.0f/255.0f alpha:1.0];
    
    [btnCheck setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"] forState:UIControlStateNormal];
    [btnCheck setImage:[UIImage imageNamed:@"ic_checkbox_active.png"] forState:UIControlStateSelected];
    [btnCheck addTarget:self action:@selector(hitBtnCheck:) forControlEvents:UIControlEventTouchUpInside];
    
    encryptor = [[Encryptor alloc] init];
    selectedCard = [[NSDictionary alloc] init];
    if ([encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISKARTU_KEY])
        selectedCard = [encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISKARTU_KEY];
    selectedCardID = -1;
    apiSuccess = FALSE;
    referenceGroup = dispatch_group_create();
    [self showLoading];
    [self.contentView setHidden:YES];
    refCards = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/card/list" dispatchGroup:referenceGroup returnData:refCards];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        apiSuccess = refCards && [[refCards valueForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]];
        if(apiSuccess)
        {
            listCards = [[NSMutableArray alloc] init];
            for (NSDictionary *aData in [refCards valueForKey:@"data"]) {
                [listCards addObject:aData];
            }
            
            CGFloat desirableWidth = screenWidth;
            
            _scrollView.delegate = self;
            _scrollView.contentSize = CGSizeMake(desirableWidth * listCards.count, _scrollView.frame.size.height);
            CGRect scrollViewFrame = _scrollView.frame;
            scrollViewFrame.size = CGSizeMake(screenWidth, scrollViewFrame.size.height);
            [_scrollView setFrame:scrollViewFrame];
            [_contentView addSubview:topBox];
            [_contentView bringSubviewToFront:_scrollView];
            
            CGRect pageControlFrame = _pageControl.frame;
            pageControlFrame.origin.x = (screenWidth/2) - (pageControlFrame.size.width/2);
            _pageControl.numberOfPages = listCards.count;
            _pageControl.currentPage = 0;
            _pageControl.frame = pageControlFrame;
            
            int i = 0;
            for (NSDictionary *card in listCards) {
                NSData *imageData;
                if([encryptor getUserDefaultsObjectForKey:[NSString stringWithFormat:@"ATM_CARD_IMAGE_%@", [card objectForKey:@"cardiid"]]])
                    imageData = [encryptor getUserDefaultsObjectForKey:[NSString stringWithFormat:@"ATM_CARD_IMAGE_%@", [card objectForKey:@"cardiid"]]];
                else
                {
                    imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[card objectForKey:@"img"]]];
                    [encryptor setUserDefaultsObject:imageData forKey:[NSString stringWithFormat:@"ATM_CARD_IMAGE_%@", [card objectForKey:@"cardiid"]]];
                }
                
                imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[card objectForKey:@"img"]]];
                [encryptor setUserDefaultsObject:imageData forKey:[NSString stringWithFormat:@"ATM_CARD_IMAGE_%@", [card objectForKey:@"cardiid"]]];
                
                UIImageView *imageView = [[UIImageView alloc] init];
                imageView.image = [UIImage imageWithData:imageData];
                imageView.contentMode = UIViewContentModeScaleAspectFit;
                imageView.clipsToBounds = YES;
                CGFloat x = desirableWidth * (CGFloat)i;
                imageView.frame = CGRectMake(x, 0, desirableWidth, _scrollView.frame.size.height);
                imageView.tag = i+1;
                [_scrollView addSubview:imageView];
                [_scrollView bringSubviewToFront:imageView];
                
                if([[selectedCard objectForKey:@"cardiid"] isEqualToString:[card objectForKey:@"cardiid"]])
                    selectedCardID = i;
                i+=1;
            }
            
            [self pageOnChanged];
            [self.contentView setHidden:NO];
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
        }
    });
}

- (void) hitBtnCheck:(UIButton *)sender
{
    [sender setSelected:YES];
    selectedCard = [listCards objectAtIndex:_pageControl.currentPage];
    selectedCardID = _pageControl.currentPage;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self pageOnChanged];
}

- (void) pageOnChanged
{
    NSString *text = [[listCards objectAtIndex:_pageControl.currentPage] objectForKey:@"msmname"];
    [self.namaKartu setText:text];
    if(_pageControl.currentPage == selectedCardID) [btnCheck setSelected:YES];
    else [btnCheck setSelected:NO];
    
    text = [[listCards objectAtIndex:_pageControl.currentPage] objectForKey:@"description"];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    UIFont *styleFont = self.deskripsiKartu.font;
    [attrStr setAttributes:@{ NSFontAttributeName: styleFont } range:NSMakeRange(0, attrStr.length)];
    [self.deskripsiKartu setAttributedText:attrStr];
    [self.deskripsiKartu sizeToFit];
    
    for (UIView *aView in _scrollView.subviews) {
        if([aView isKindOfClass:UIImageView.class])
        {
            if(aView.tag == _pageControl.currentPage + 2)
                [UIView animateWithDuration:0.1 animations:^{
                    [aView setTransform:CGAffineTransformMakeTranslation(-100.0, 0)];
                }];
            else
                [UIView animateWithDuration:0.1 animations:^{
                    [aView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                }];

            if(aView.tag == _pageControl.currentPage)
                [UIView animateWithDuration:0.1 animations:^{
                   [aView setTransform:CGAffineTransformMakeTranslation(100.0, 0)];
                }];
        }
    }
}

#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == _scrollView)
    {
        double pageIndex = round(scrollView.contentOffset.x / scrollView.superview.frame.size.width);
        if (pageIndex != _pageControl.currentPage) {
            _pageControl.currentPage = pageIndex;
            [self pageOnChanged];
        }
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *btn = (UIButton *) sender;
        if([btn.titleLabel.text isEqualToString:@"SELANJUTNYA"])
        {
            if(selectedCard.count > 0)
            {
                [encryptor setUserDefaultsObject:selectedCard forKey:ONBOARDING_JENISKARTU_KEY];
            }
            else
            {
                AMPopTip *popTip = [AMPopTip popTip];
                [popTip showText:NSLocalizedString(@"PICK_A_CARD", @"") direction:AMPopTipDirectionDown maxWidth:200.0 inView:[btnCheck superview] fromFrame:btnCheck.frame duration:3.0];
                return FALSE;
            }
        }
    }
    return TRUE;
}

@end

