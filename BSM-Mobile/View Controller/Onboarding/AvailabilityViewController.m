//
//  AvailabilityViewController.m
//  BSM-Mobile
//
//  Created by ARS on 06/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "AvailabilityViewController.h"
#import "CustomerOnboardingData.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"
#import "OnboardingTemplate01ViewController.h"
#import "HomeViewController.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"

@interface AvailabilityViewController ()
{
    Encryptor *encryptor;
    dispatch_group_t dispatchGroup;
}
@end

@implementation AvailabilityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.maintenanceView.layer.cornerRadius = 30.0f;
    [self.maintenanceView setHidden:NO];
    
    dispatchGroup = dispatch_group_create();
    
    if (_cobMessage == nil || [_cobMessage isEqual:[NSNull null]]) {
        return;
    }
    self.maintenanceText.text = _cobMessage;
}

- (IBAction)hitBtnBack:(id)sender {
    [self goToMsm];
}
- (IBAction)hitBtnOk:(id)sender {
    [self goToMsm];
}

- (void) goToMsm {
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}
@end
