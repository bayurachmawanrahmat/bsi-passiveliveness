//
//  JenisTabunganDynamicController.m
//  BSM-Mobile
//
//  Created by ARS on 20/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "JenisTabunganDynamicController.h"
#import "JenisTabunganDynamicCell.h"
#import "NSObject+FormInputCategory.h"
#import "JenisTabunganDetailController.h"
#import "AMPopTip.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "DescriptionCell.h"

@interface JenisTabunganDynamicController ()
{
    NSMutableArray *listData;
    NSString *jenisTabungan;
    Encryptor *encryptor;
    dispatch_group_t dispatchGroup;
    UIRefreshControl *refreshControl;
}

@end

@implementation JenisTabunganDynamicController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    
    encryptor = [[Encryptor alloc] init];
    dispatchGroup = dispatch_group_create();
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [_scrollView setRefreshControl:refreshControl];
    } else {
        [_scrollView addSubview:refreshControl];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"DescriptionCell" bundle:nil] forCellReuseIdentifier:@"CellDescription"];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    jenisTabungan = @"";
    if([encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY])
    {
        jenisTabungan = [encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY];
    }
    [self getJenisTabungan];
}

- (void)refreshTable {
    [self getJenisTabungan];
}

#pragma mark TableViewDelegate
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 175.0;
//}

#pragma mark TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return listData.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark TableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == (listData.count - 1)) {
        static NSString *CellIdentifier = @"CellDescription";
        DescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[DescriptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        // todo show e-statement jenis tabungan
        // cell.labelDesc.text = NSLocalizedString(@"JENIS_TABUNGAN_DESC", @"");
        cell.labelDesc.text = @"";
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        static NSString *CellIdentifier = @"cellJenisTabungan";
        JenisTabunganDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[JenisTabunganDynamicCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        NSDictionary *data = [listData objectAtIndex:indexPath.row];
        cell.name.text = [data objectForKey:@"name"];
        NSString *shortdesc = [data objectForKey:@"shortdesc"];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithData:[shortdesc dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        UIFont *styleFont = cell.shortdesc.font;
        [attrStr setAttributes:@{ NSFontAttributeName: styleFont } range:NSMakeRange(0, attrStr.length)];
        [cell.shortdesc setAttributedText:attrStr];
        [cell.shortdesc sizeToFit];
        NSString *imageName = ([[data objectForKey:@"kode"] isEqualToString:@"6001"] ? @"img_tabungan_wadiah.png" : @"img_tabungan_mudharabah.png");
        [cell.img setImage:[UIImage imageNamed:imageName]];
        cell.feature = [data objectForKey:@"feature"];
        cell.longdesc = [data objectForKey:@"description"];
        cell.kode = [data objectForKey:@"kode"];
        cell.delegate = self;
        
        NSDictionary *selectedJenis = [encryptor getUserDefaultsObjectForKey:@"SELECTEDJENISTABUNGANCELL"];
        if(selectedJenis)
        {
            if([[selectedJenis objectForKey:@"name"] isEqualToString:cell.name.text])
            {
                [cell.btnSelect setSelected:YES];
                jenisTabungan = cell.kode;
                [encryptor setUserDefaultsObject:jenisTabungan forKey:ONBOARDING_JENISTABUNGAN_KEY];
            }
        }
        else if([jenisTabungan isEqualToString:cell.kode])
        {
            [cell.btnSelect setSelected:YES];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

#pragma mark JenisTabunganDynamicCellDelegate
- (void)didHitBtnSelect:(UIButton *)sender
{
    NSArray<UITableViewCell *> *cells = self.tableView.visibleCells;
    for (UITableViewCell *aCell in cells) {
        if ([aCell.reuseIdentifier isEqualToString:@"CellDescription"]) {
            continue;
        }
        JenisTabunganDynamicCell *cell = (JenisTabunganDynamicCell *) aCell;
        [cell.btnSelect setSelected:FALSE];
        if(cell.btnSelect == sender)
        {
            jenisTabungan = cell.kode;
            [encryptor setUserDefaultsObject:jenisTabungan forKey:ONBOARDING_JENISTABUNGAN_KEY];
        }
    }
}

#pragma mark JenisTabunganDynamicCellDelegate
- (void)didHitBtnDetail:(JenisTabunganDynamicCell *)sender
{
    NSDictionary *data = [[NSDictionary alloc] initWithObjectsAndKeys:
                          sender.name.text, @"name",
                          sender.longdesc, @"longdesc",
                          sender.feature, @"feature",
                          sender.kode, @"kode",
                          nil]; // TODO pass image detail dari DB
    
    [encryptor setUserDefaultsObject:data forKey:@"SELECTEDJENISTABUNGANCELL"];
    
    JenisTabunganDetailController *controller = [[JenisTabunganDetailController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)getJenisTabungan
{
    [self showLoading];
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/tabungan/listAll" dispatchGroup:dispatchGroup returnData:returnData];
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        [self.tableView reloadData];
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            self->listData = [[NSMutableArray alloc] init];
            NSDictionary *data = [returnData objectForKey:@"data"];
            for (NSDictionary *item in data) {
                [self->listData addObject:item];
            }
            // add description item
            NSMutableDictionary *description = [[NSMutableDictionary alloc] init];
            [description setObject:@"desc" forKey:@"Test"];
            [self->listData addObject:description];
            
            [self.tableView reloadData];
        }
        else
        {
            NSString* message = NSLocalizedString(@"GENERAL_FAILURE", @"");
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [alert addAction:actionOk];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        if(self->refreshControl)
            [self->refreshControl endRefreshing];
    });
}

- (void)showErrorMsg:(NSString *)errorMsg withInput:(UIButton *)button
{
    AMPopTip *popTip = [AMPopTip popTip];
    [popTip showText:errorMsg direction:AMPopTipDirectionDown maxWidth:200.0 inView:[button superview] fromFrame:button.frame duration:3.0];
}


- (BOOL)checkInput
{
    if(![jenisTabungan isEqualToString:@""]) return TRUE;
    else
    {
        NSArray<UITableViewCell *> *cells = _tableView.visibleCells;
        for (UITableViewCell *aCell in cells) {
            JenisTabunganDynamicCell *cell = (JenisTabunganDynamicCell *) aCell;
            [self showErrorMsg:NSLocalizedString(@"PICK_A_TYPE", @"") withInput:cell.btnSelect];
        }
        return FALSE;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *btn = (UIButton *) sender;
        if([btn.titleLabel.text isEqualToString:@"SELANJUTNYA"])
        {
            if([self checkInput])
            {
                [encryptor removeUserDefaultsObjectForKey:@"SELECTEDJENISTABUNGANCELL"];
                [encryptor setUserDefaultsObject:jenisTabungan forKey:ONBOARDING_JENISTABUNGAN_KEY];
            }
            else return FALSE;
        }
    }
    return TRUE;
}

@end
