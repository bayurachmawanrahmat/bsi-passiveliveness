//
//  CameraOverlayController.h
//  BSM-Mobile
//
//  Created by ARS on 16/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraOverlayView.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CameraOverlayController : OnboardingRootController

@end

NS_ASSUME_NONNULL_END
