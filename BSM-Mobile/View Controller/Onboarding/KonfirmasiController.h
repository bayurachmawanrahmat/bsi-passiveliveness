//
//  KonfirmasiController.h
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressViewController.h"
#import "ProgressView.h"
#import "PernyataanNasabahController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface KonfirmasiController : OnboardingRootController <ProgressViewControllerDelegate, ProgressViewDelegate, PernyataanNasabahDelegate, UIDocumentInteractionControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblNama;
@property (weak, nonatomic) IBOutlet UILabel *lblHp;
@property (weak, nonatomic) IBOutlet UILabel *lblPekerjaan;
@property (weak, nonatomic) IBOutlet UILabel *lblJenisTabungan;
@property (weak, nonatomic) IBOutlet UILabel *lblFasilitasProduk;
@property (weak, nonatomic) IBOutlet UILabel *lblJenisKartuAtm;
@property (weak, nonatomic) IBOutlet UILabel *lblNoKtp;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPerusahaan;
@property (weak, nonatomic) IBOutlet UIButton *btnSetuju;
@property (weak, nonatomic) IBOutlet UIButton *btnPernyataanNasabah;
@property (weak, nonatomic) IBOutlet UIButton *btnSnK;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)actionSetuju:(id)sender;
@property ProgressViewController *progressViewController;
@property ProgressView *progressView;
- (IBAction)showPenyataanNasabah:(id)sender;
- (IBAction)downloadSnK:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelEStatement;

@end

NS_ASSUME_NONNULL_END
