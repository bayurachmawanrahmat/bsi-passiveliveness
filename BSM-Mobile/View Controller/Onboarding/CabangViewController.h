//
//  CabangViewController.h
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailCabangViewController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol CabangViewDelegate;

@interface CabangViewController : OnboardingRootController <UISearchResultsUpdating, UISearchBarDelegate>

@property (nonatomic, strong) UISearchController * searchController;
@property (weak, nonatomic) id<CabangViewDelegate> delegate;
@property (strong, nonatomic) NSDictionary *selectedData;
@property BOOL showDetailOnSelectRow;

@end

@protocol CabangViewDelegate <NSObject>

@required
-(void)cabangViewController:(id)cabangViewController didSelectRowWithData:(NSDictionary *)data;

@end


NS_ASSUME_NONNULL_END
