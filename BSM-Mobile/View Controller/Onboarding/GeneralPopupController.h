//
//  GeneralPopupController.h
//  BSM-Mobile
//
//  Created by ARS on 26/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol GeneralPopupDelegate;

@interface GeneralPopupController : OnboardingRootController

@property (weak, nonatomic) id<GeneralPopupDelegate> delegate;

@end

@protocol GeneralPopupDelegate <NSObject>

@required
- (void) didOk:(GeneralPopupController *) controller;

@end


NS_ASSUME_NONNULL_END
