//
//  OnboardingTemplate01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 25/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "OnboardingTemplate01ViewController.h"
#import "OnboardingButton.h"

@interface OnboardingTemplate01ViewController ()
{
    UIViewController *childController;
}
@end

@implementation OnboardingTemplate01ViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.modalPresentationStyle = UIModalPresentationFullScreen;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.paddingView.layer.cornerRadius = 30.0f;
    [self hideContent];
    
    [self.scrollView bringSubviewToFront:self.contentView];
    [self.scrollView bringSubviewToFront:self.imageView];
    self.defaultContentViewHeight = self.contentView.frame.size.height;
    
    if(childController != nil)
    {
        [self addChildViewController:childController];
        [self.contentView addSubview:childController.view];
        [childController didMoveToParentViewController:self];
        [self.view layoutIfNeeded];
        
        CGFloat childContentHeight = [self getChildContentHeight];
        [childController.view setFrame:CGRectMake(0, 0, self.contentView.frame.size.width, childContentHeight)];
        if(childContentHeight > self.defaultContentViewHeight)
            self.contentViewHeight.constant = childContentHeight - self.defaultContentViewHeight - 300.0;
        else
            [childController.view setFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.defaultContentViewHeight)];
        [self.view layoutIfNeeded];
        
        [self.delegate childHasLoaded:self];
    }
    [self showContent];
}

- (void)hideContent
{
    [self.paddingView setAlpha:0];
    [self.contentView setAlpha:0];
}

- (void)showContent
{
    [self.paddingView setAlpha:1.0];
    [self.contentView setAlpha:1.0];
}

- (void)setChildController:(UIViewController *)child
{
    childController = child;
}

- (CGFloat)getChildContentHeight
{
    CGFloat height = 0.0;
    for (UIView *view in childController.view.subviews) {
        if(![view isHidden] && view.frame.origin.y > height)
        {
            height = view.frame.origin.y + view.frame.size.height;
        }
    }
    return height + 110.0;
}

- (void)changeImageBackgroundHeight:(CGFloat)height
{
    self.imageBackgroundHeight.constant = height;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
