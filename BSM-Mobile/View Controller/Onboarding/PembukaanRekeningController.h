//
//  PembukaanRekeningController.h
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PernyataanNasabahController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PembukaanRekeningController : OnboardingRootController <UITextViewDelegate, PernyataanNasabahDelegate>
@property (weak, nonatomic) IBOutlet UITextView *lblText;
@property (weak, nonatomic) IBOutlet UIView *contentVIew;


@end

NS_ASSUME_NONNULL_END
