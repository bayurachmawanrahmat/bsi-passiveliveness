//
//  VerifikasiWajahController.m
//  BSM-Mobile
//
//  Created by ARS on 01/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "VerifikasiWajahController.h"
#import "CameraOverlayController.h"
#import "CameraOverlayView.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"

@interface VerifikasiWajahController ()
{
    Encryptor *encryptor;
}
@end

@implementation VerifikasiWajahController

static const NSString *ScreenName = @"VerifikasiWajahScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encryptor = [[Encryptor alloc] init];
    
    self.contentView.layer.cornerRadius = 15.f;
    self.btnRetake.layer.cornerRadius = 24.f;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    self.view.hidden = NO;
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    _textDesc.text = NSLocalizedString(@"DESC_KTP_POSITION_FACE_CAPTURED", @"");
    
    if(!_imagePicked && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        _imagePicked = [self loadImageFromLocalStorageWithFileName:@"VERIFIKASI" ofType:@"jpg" inDirectory:documentsDirectory];
        
        if(!_imagePicked)
        {
            CameraOverlayController *overlayController = [[CameraOverlayController alloc] init];
            
            _cameraPicker = [[UIImagePickerController alloc] init];
            _cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            _cameraPicker.delegate = self;
            _cameraPicker.showsCameraControls = false;
            _cameraPicker.allowsEditing = false;
            _cameraPicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            
            CameraOverlayView *cameraView = (CameraOverlayView *)overlayController.view;
            cameraView.frame = _cameraPicker.view.frame;
            cameraView.textUnderBox.text = NSLocalizedString(@"DESC_KTP_POSITION_FACE", @"");
            [cameraView.textUnderBox sizeToFit];
            cameraView.constraintImageOverlayMarginBottom.constant = 132.0;
            // cameraView.constraintImageOverlayMarginBottom.constant = self.view.frame.size.height * 0.3;
            cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_selfie_fr.png"];
            [cameraView.navigationBar.topItem setTitle:@"Foto Wajah"];
            cameraView.delegate = self;
            
            _cameraPicker.cameraOverlayView = cameraView;
            
            [self presentViewController:_cameraPicker animated:YES completion:nil];
        }
        else
        {
            self.view.hidden = NO;
            [self setImageView];
        }
    }
    else self.view.hidden = NO;
    CGFloat yPos = self.imagePhoto.frame.origin.y + 15.0;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:2 fromTotal:7];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info
{
    [_cameraPicker dismissViewControllerAnimated:YES completion:nil];
    
    _imagePicked = info[UIImagePickerControllerOriginalImage];
    
    [self setImageView];
}

- (void)didTakePhoto:(CameraOverlayView *)overlayView{
    [_cameraPicker takePicture];
}

- (void)didBack:(CameraOverlayView *)overlayView{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"InformasiSelfieScreen"];
    [_cameraPicker presentViewController:vc animated:YES completion:nil];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if(_imagePicked)
            {
                // SAVE IMAGE
                [self saveImageToLogicalDocs:self.imagePicked withFilename:@"VERIFIKASI" saveWithKey:ONBOARDING_VERIFIKASI_WAJAH_FILEID_KEY andWithQuality: 0.8 onCompletionPerformSegueWithIdentifier:identifier fromController:self];
            }
            else return YES;
            return NO;
        }
    }
    
    return YES;
}

- (void)setImageView
{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    CGRect rect = _imagePhoto.frame;
    
    _imageHeight.constant = (screenHeight / 1.5) - 60;
    
    _imagePhoto.frame = CGRectMake(rect.origin.x, rect.origin.y, (screenWidth - 20), _imageHeight.constant);
    _imagePhoto.image = _imagePicked;
    _imagePhoto.clipsToBounds = YES;
}

- (IBAction)hitBtnRetake:(id)sender {
    _imagePicked = nil;
    [self removeImageFromLocalStorage:@"VERIFIKASI"];
    [self viewDidLoad];
    [self viewDidAppear:YES];
}
@end
