//
//  PopUpController.h
//  BSM-Mobile
//
//  Created by ARS on 02/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopUpController : OnboardingRootController

@end

NS_ASSUME_NONNULL_END
