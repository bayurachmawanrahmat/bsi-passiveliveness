//
//  KonfirmasiController.m
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "KonfirmasiController.h"
#import "PernyataanNasabahController.h"
#import "ProgressViewController.h"
#import "ProgressView.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@interface KonfirmasiController ()
{
    dispatch_group_t referenceGroup;
    NSDictionary *referenceEula;
    Encryptor *encryptor;
    UIRefreshControl *refreshControl;
}
@end

@implementation KonfirmasiController

@synthesize progressView;
@synthesize progressViewController;

static const NSString *ScreenName = @"KonfirmasiScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    UIImage *iconChecked = [UIImage imageNamed:@"ic_checkbox_active.png"];
    UIImage *iconUnchecked = [UIImage imageNamed:@"ic_checkbox_inactive.png"];
    
    [self.btnSetuju setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
    [self.btnSetuju setBackgroundImage:iconChecked forState:UIControlStateSelected];
    
    [self.btnPernyataanNasabah setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
    [self.btnPernyataanNasabah setBackgroundImage:iconChecked forState:UIControlStateSelected];
    [self.btnPernyataanNasabah addTarget:self action:@selector(toggleCheckbox:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnSnK setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
    [self.btnSnK setBackgroundImage:iconChecked forState:UIControlStateSelected];
    [self.btnSnK addTarget:self action:@selector(toggleCheckbox:) forControlEvents:UIControlEventTouchUpInside];
    
    // todo show e-statement
    // _labelEStatement.text = NSLocalizedString(@"E_STATEMENT_DESC", @"");
    _labelEStatement.text = @"";
    [_labelEStatement setHidden:YES];
    
    progressViewController = [[ProgressViewController alloc] init];
    progressViewController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    progressViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    progressView = (ProgressView *)progressViewController.view;
    progressView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.80];
    progressView.progressText.text = NSLocalizedString(@"DOWNLOAD_TOS", @"");
    [progressView.progressBar setProgress:0];
    progressView.delegate = self;
    
    referenceEula = [[NSDictionary alloc] init];
    referenceGroup = dispatch_group_create();
    
    [self refreshData];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [_scrollView setRefreshControl:refreshControl];
    } else {
        [_scrollView addSubview:refreshControl];
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    CGFloat yPos = self.scrollView.frame.origin.y;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:7 fromTotal:7];
}

- (void) refreshData
{
    [self showLoading];
    [self loadLabelText];
    
    NSMutableDictionary *returnEula = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/files/getByCategory?category=EULA" dispatchGroup:referenceGroup returnData:returnEula];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        if(self->refreshControl) [self->refreshControl endRefreshing];
        if([[returnEula objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *eula = [returnEula objectForKey:@"data"];
            if(eula && eula.count > 0)
            {
                for (NSDictionary *item in eula) {
                    self->referenceEula = item;
                }
            }
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_EULA", @"") sender:self];
        }
    });
}

- (void) loadLabelText
{
    _lblNama.text = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP];
    _lblHp.text = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY];
    _lblEmail.text = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY];
    _lblNoKtp.text = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
    _lblPekerjaan.text = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_KEUANGAN_PEKERJAAN_KEY] objectForKey:@"nama"];
    _lblPerusahaan.text = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_CABANG_KEY] objectForKey:@"nama"];
    _lblFasilitasProduk.text = @"Mbanking";
    _lblJenisKartuAtm.text = [(NSDictionary *)[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISKARTU_KEY] objectForKey:@"msmname"];
    if([[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY] isEqualToString:ONBOARDING_TAB_MUDHARABAHID])
        _lblJenisTabungan.text = ONBOARDING_TAB_MUDHARABAH;
    else
        _lblJenisTabungan.text = ONBOARDING_TAB_WADIAH;
}

- (void)animateProgressView:(UIProgressView *)progressView withDuration:(float)duration
{
    [progressView setProgress:1];
    [UIView animateWithDuration:3.0 animations:^{
        [progressView layoutIfNeeded];
    } completion:^(BOOL finished){
        if (finished)
        {
            [self->progressViewController dismissViewControllerAnimated:YES completion:nil];
            self.btnSnK.selected = TRUE;
        }
    }];
}

-(void)didSetujuPernyataanNasabah:(PernyataanNasabahController *)controller
{
    self.btnPernyataanNasabah.selected = TRUE;
}

- (void)toggleCheckbox:(UIButton *)button
{
    if(!button.selected)
    {
        if(button == self.btnSnK)
        {
            progressViewController = [[ProgressViewController alloc] initWithDefaultConfigAndProgressViewText:NSLocalizedString(@"DOWNLOAD_TOS", @"")];
            progressViewController.delegate = self;
            [self presentViewController:progressViewController animated:YES completion:^{
                [self->progressViewController doDownloadFromUrl:[self->referenceEula objectForKey:@"fileurl"] saveWithFilename:[self->referenceEula objectForKey:@"filename"]];
            }];
        }
        else if(button == self.btnPernyataanNasabah)
        {
            PernyataanNasabahController *modalController = [[PernyataanNasabahController alloc] init];
            modalController.modalPresentationStyle = UIModalPresentationOverFullScreen;
            modalController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            modalController.delegate = self;
            
            [self presentViewController:modalController animated:YES completion:^{
                [modalController.snkView setHidden:FALSE];
            }];
        }
        else
        {
            button.selected = YES;
        }
    }
    else
    {
        button.selected = NO;
    }
}

- (IBAction)showPenyataanNasabah:(id)sender {
    PernyataanNasabahController *modalController = [[PernyataanNasabahController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    modalController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    modalController.delegate = self;
    
    [self presentViewController:modalController animated:YES completion:^{
        [modalController.snkView setHidden:NO];
    }];
}

- (void) getEulaReference
{
    dispatch_group_enter(referenceGroup);
    NSString *url =[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:@"api/files/getByCategory?category=EULA"]];
    NSLog(@"---------------- %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:url]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if(error)
                {
                    [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
                }
                else
                {
                    NSDictionary *jsonObject;
                    if(ONBOARDING_SAFE_MODE)
                    {
                        NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        NSData *decData = [Encryptor AESDecryptData:encString];
                        jsonObject = [NSJSONSerialization JSONObjectWithData:decData options:kNilOptions error:&error];
                    }
                    else
                        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                    if([httpResponse statusCode] == (long)200 && jsonObject && jsonObject.count > 0)
                    {
                        for (NSDictionary *item in jsonObject) {
                            self->referenceEula = item;
                        }
                    }
                    else {
                        [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
                    }
                }
        dispatch_group_leave(self->referenceGroup);
            }] resume];
}

- (void)openPdfFile:(NSString *)filePath
{
    UIDocumentInteractionController *documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:filePath]];
    documentController.delegate = self;
    if(![documentController presentPreviewAnimated:YES])
    {
        [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_OPEN_EULA", @"") sender:self];
    }
    else
    {
        [_btnSnK setSelected:TRUE];
    }
}

- (IBAction)downloadSnK:(id)sender {
    progressViewController = [[ProgressViewController alloc] initWithDefaultConfigAndProgressViewText:NSLocalizedString(@"DOWNLOAD_TOS", @"")];
    progressViewController.delegate = self;
    [self presentViewController:progressViewController animated:YES completion:^{
        [self->progressViewController doDownloadFromUrl:[self->referenceEula objectForKey:@"fileurl"] saveWithFilename:[self->referenceEula objectForKey:@"filename"]];
    }];
}
- (IBAction)actionSetuju:(id)sender {
    if(sender == _btnSetuju)
    {
        [self toggleCheckbox:sender];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            // [encryptor setUserDefaultsObject:@"1" forKey:ONBOARDING_NASABAH_SETUJU_KEY];
            
            if(![_btnSnK isSelected] || ![_btnPernyataanNasabah isSelected])
                return NO;
            else
            {
                if(_btnSetuju.isSelected) [encryptor setUserDefaultsObject:@"1" forKey:ONBOARDING_NASABAH_SETUJU_KEY];
                else [encryptor setUserDefaultsObject:@"0" forKey:ONBOARDING_NASABAH_SETUJU_KEY];
            }
        }
    }
    
    return YES;
}

#pragma mark DELEGATE methods
- (void)progressBarDidFinishLoading:(NSString *)data
{
    [self openPdfFile:data];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}
@end
