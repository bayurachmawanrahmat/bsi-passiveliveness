//
//  InformasiTransaksiController.m
//  BSM-Mobile
//
//  Created by ARS on 25/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "InformasiTransaksiController.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"

@interface InformasiTransaksiController ()
{
    CabangViewController *dropdownCabang;
    dispatch_group_t referenceGroup;
    NSDictionary *formDropDowns;
    NSMutableDictionary *listTujuanbukarekening;
    NSMutableDictionary *listSumberdana;
    NSMutableDictionary *listTransaksi;
    Encryptor *encryptor;
    UIRefreshControl *refreshControl;
}
@end

@implementation InformasiTransaksiController

static const NSString *ScreenName = @"InformasiTransaksiScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    _fieldTujuanBukaRekening.delegate = self;
    _fieldSumberdana.delegate = self;
    _fieldTransaksi.delegate = self;
    _fieldCabang.delegate = self;
    
    UIImageView *rightIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"place grey.png"]];
    CGRect iconFrame = rightIcon.frame;
    iconFrame.size = CGSizeMake(15.0, 15.0);
    [rightIcon setFrame:iconFrame];
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, iconFrame.size.width + 5, iconFrame.size.height)];
    [rightView addSubview:rightIcon];
    _fieldCabang.rightViewMode = UITextFieldViewModeAlways;
    _fieldCabang.rightView = rightView;
    
    referenceGroup = dispatch_group_create();
    
    UIImage *iconChecked = [UIImage imageNamed:@"ic_checkbox_active.png"];
    UIImage *iconUnchecked = [UIImage imageNamed:@"ic_checkbox_inactive.png"];
    
    [_btnZakat setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
    [_btnZakat setBackgroundImage:iconChecked forState:UIControlStateSelected];
    [_btnZakat addTarget:self action:@selector(btnZakatDidHit) forControlEvents:UIControlEventTouchUpInside];
    [_btnZakat setSelected:YES];
    
    [self refreshData];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [_scrollView setRefreshControl:refreshControl];
    } else {
        [_scrollView addSubview:refreshControl];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    CGFloat yPos = self.scrollView.frame.origin.y;
    [self setWizardBarOnTop:self.view onYPos:yPos withDone:6 fromTotal:7];
}

- (void)refreshData
{
    [self showLoading];
    [self getTujuanBukaRekening];
    [self getSumberdana];
    [self getTransaksi];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        [self->refreshControl endRefreshing];
        
        if(self->listTransaksi && [[self->listTransaksi objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [self->listTransaksi objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldTransaksi.options = options;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_TRANSACTION", @"") sender:self];
        }
        
        if(self->listSumberdana && [[self->listSumberdana objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [self->listSumberdana objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldSumberdana.options = options;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:@"Ooops" sender:self];
        }
        
        if(self->listTujuanbukarekening && [[self->listTujuanbukarekening objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
            for(NSDictionary *aData in [self->listTujuanbukarekening objectForKey:@"data"])
            {
                [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
            }
            self->_fieldTujuanBukaRekening.options = options;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_ACCOUNT_OPENING_PURPOSE", @"") sender:self];
        }
    });
    
    formDropDowns = [[NSDictionary alloc] initWithObjectsAndKeys:
                     _fieldTujuanBukaRekening, ONBOARDING_INFORMASI_REKENING_TUJUAN_BUKA_KEY,
                     _fieldSumberdana, ONBOARDING_INFORMASI_REKENING_SUMBER_DANA_KEY,
                     _fieldTransaksi, ONBOARDING_INFORMASI_REKENING_TRANSAKSI_KEY,
                     nil];
    
    if([encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_ISZAKAT_KEY])
    {
        [_btnZakat setSelected:[[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_ISZAKAT_KEY] isEqualToString:@"Y"]];
    }
    
    [[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY] isEqualToString:ONBOARDING_TAB_MUDHARABAHID] ? [_labelZakat setText:NSLocalizedString(@"ZAKAT_PROFIT", @"")] : [_labelZakat setText:NSLocalizedString(@"ZAKAT_BONUS", @"")];
    
    [self setDropDownData:formDropDowns];
    [self loadCabangView];
}

- (void) loadCabangView
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    dropdownCabang = [sb instantiateViewControllerWithIdentifier:@"CabangViewScreen"];
    dropdownCabang.delegate = self;
    
    dropdownCabang.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_CABANG_KEY];
    if(dropdownCabang.selectedData)
        _fieldCabang.text = [[dropdownCabang selectedData] objectForKey:@"nama"];
}

- (void) saveCabangData
{
    [encryptor setUserDefaultsObject:[dropdownCabang selectedData] forKey:ONBOARDING_INFORMASI_REKENING_CABANG_KEY];
}

- (void) btnZakatDidHit
{
    [_btnZakat setSelected: ![_btnZakat isSelected]];
}

- (void) saveIsZakat
{
    [encryptor setUserDefaultsObject:([_btnZakat isSelected] ? @"Y" : @"N") forKey:ONBOARDING_INFORMASI_REKENING_ISZAKAT_KEY];
}

- (void) getTujuanBukaRekening
{
    listTujuanbukarekening = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/reference/listAllByKategori?kategori=TUJUANBUKA" dispatchGroup:referenceGroup returnData:listTujuanbukarekening];
}

- (void) getSumberdana
{
    listSumberdana = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/reference/listAllByKategori?kategori=SUMBERDANA" dispatchGroup:referenceGroup returnData:listSumberdana];
}

- (void) getTransaksi
{
    listTransaksi = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/master/reference/listAllByKategori?kategori=TRANSAKSI" dispatchGroup:referenceGroup returnData:listTransaksi];
}

- (void) getReferenceFromEndPoint:(NSString *)endpoint forField:(UIDropdown *)field
{
    dispatch_group_enter(referenceGroup);
    NSString *url =[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:endpoint]];
    NSLog(@"---------------- %@", url);
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:url]
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                
                if(!error)
                {
                    NSDictionary *jsonObject;
                    if(ONBOARDING_SAFE_MODE)
                    {
                        NSString *encString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                        NSData *decData = [Encryptor AESDecryptData:encString];
                        jsonObject = [NSJSONSerialization JSONObjectWithData:decData options:kNilOptions error:&error];
                    }
                    else
                        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                    
                    if(!error && jsonObject) {
                        NSMutableDictionary *options = [[NSMutableDictionary alloc] init];
                        for(NSDictionary *aData in jsonObject)
                        {
                            [options setObject:[aData objectForKey:@"nama"] forKey:[aData objectForKey:@"nilai"]];
                        }
                        field.options = options;
                    }
                }
        dispatch_group_leave(self->referenceGroup);
            }] resume];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self inputHasError:FALSE textField:textField];
    if (textField == _fieldCabang)
    {
        [self presentViewController:dropdownCabang animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self inputHasError:FALSE textField:textField];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)cabangViewController:(id)cabangViewController didSelectRowWithData:(NSDictionary *)data
{
    _fieldCabang.text = [data objectForKey:@"nama"];
}

- (BOOL)checkInput
{
    int countIsValid = 0;
    if(![self checkNullForField:_fieldTujuanBukaRekening withErrorText:NSLocalizedString(@"ACCOUNT_OPENING_PURPOSE_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
    if(![self checkNullForField:_fieldSumberdana withErrorText:NSLocalizedString(@"INCOME_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
    if(![self checkNullForField:_fieldTransaksi withErrorText:NSLocalizedString(@"APPROXIMATE_TRANSACTION_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
    if(![self checkNullForField:_fieldCabang withErrorText:NSLocalizedString(@"BRANCH_OFFICE_CANNOT_BE_EMPTY", @"")]) countIsValid +=1;
    
    return countIsValid == 0 ? TRUE : FALSE;
}

- (BOOL) checkNullForField:(UITextField *)field withErrorText:(NSString *)error
{
    if([field.text isEqualToString:@""])
    {
        [self inputHasError:TRUE textField:field];
        [self showErrorLabel:error withUiTextfield:field];
        return FALSE;
    }
    return TRUE;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if(![self checkInput]) return NO;
            else
            {
                [self saveDropDownData:formDropDowns];
                [self saveIsZakat];
                [self saveCabangData];
            }
        }
    }
    
    return YES;
}

@end
