//
//  InformasiNoHandphoneController.h
//  BSM-Mobile
//
//  Created by ARS on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiNoHandphoneController : OnboardingRootController <NSURLSessionTaskDelegate, NSURLSessionDataDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *fieldNoHandphone;
- (IBAction)hitBtnBack:(id)sender;
- (IBAction)hitBtnNext:(id)sender;

@end

NS_ASSUME_NONNULL_END
