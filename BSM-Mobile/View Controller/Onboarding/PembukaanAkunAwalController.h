//
//  PembukaanAkunAwalController.h
//  BSM-Mobile
//
//  Created by ARS on 02/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PembukaanAkunAwalController : OnboardingRootController <NSURLSessionTaskDelegate, NSURLSessionDataDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImageHeightConstrain;
@property (weak, nonatomic) IBOutlet UIView *contentView;

- (instancetype)initWithVideoAssistVersion:(NSInteger)version;
- (void) downloadVideoAssistVersion;

@end

NS_ASSUME_NONNULL_END
