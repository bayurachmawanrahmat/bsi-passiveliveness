//
//  GagalVerifikasiController.m
//  BSM-Mobile
//
//  Created by ARS on 26/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "GagalVerifikasiController.h"
#import "OnboardingButton.h"
#import "Encryptor.h"
#import "LoginViewController.h"
#import "CustomerOnboardingData.h"
#import "NSObject+FormInputCategory.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"
#import <sys/utsname.h>
#import <CoreLocation/CoreLocation.h>

@interface GagalVerifikasiController () <CLLocationManagerDelegate>
{
    NSString *jenisGagal;
    Encryptor *enc;
    Rekening *rekeningInfo;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;

}
@property (weak, nonatomic) IBOutlet UIView *rejectedCabangView;
@property (weak, nonatomic) IBOutlet UIView *rejectedView;
@property (weak, nonatomic) IBOutlet UIView *rejectvideoView;
@property (weak, nonatomic) IBOutlet UITextView *textViewRejectVideo;
@property (weak, nonatomic) IBOutlet UILabel *labelNamaCabang;
@property (weak, nonatomic) IBOutlet UILabel *labelAlamatCabang;
@property (weak, nonatomic) IBOutlet UILabel *labelKotaCabang;
@end

@implementation GagalVerifikasiController

- (instancetype)initForRejectVideoWithRekening:(Rekening *)rekening
{
    self = [super init];
    if (self) {
        jenisGagal = @"RejectVideo";
        self->rekeningInfo = rekening;
    }
    return self;
}

- (instancetype)initForRejectedWithRekening:(Rekening *)rekening
{
    self = [super init];
    if (self) {
        jenisGagal = @"Rejected";
        self->rekeningInfo = rekening;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    enc = [[Encryptor alloc] init];
    
    if([jenisGagal isEqualToString:@"RejectVideo"])
        [self adjustViewForRejectVideo];
    else {
        [self adjustViewForRejected];
        [enc resetFormData];
        [enc setUserDefaultsObject:@"NEW" forKey:ONBOARDING_PROGRESS_STEP_KEY];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:ONBOARDING_NEED_CHECK_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    
    [self currentLocationIdentifier];
}

#pragma mark : CLLocationManagerDelegate Methods
-(void)currentLocationIdentifier
{
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else {
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
}

- (void)childHasLoaded:(OnboardingTemplate01ViewController *)parent
{
    CGFloat addHeight = 60.0;
    parent.imageBackgroundHeight.constant += addHeight;
    if(parent.scrollView.contentSize.height <= parent.scrollView.frame.size.height)
        parent.contentViewHeight.constant -= addHeight;
    [parent.navigationBar setHidden:YES];
    [parent.containerViewTop setConstant:-44.0];
    [parent.view layoutIfNeeded];
    
    [parent.labelTitle setText:NSLocalizedString(@"SORRY", @"Sorry")];
    [parent.labelSubtitle setText:NSLocalizedString(@"ACCOUNT_REJECTED_TEXT", @"Account opening rejected text")];
    [parent.labelTitle setAlpha:1.0];
    [parent.labelSubtitle setAlpha:1.0];
    
    parent.imageView.image = [UIImage imageNamed:@"img_gagal_verifikasi.png"];
    CGRect imageViewFrame = parent.imageView.frame;
    imageViewFrame.origin = CGPointMake((parent.view.frame.size.width / 2) - (parent.imageView.frame.size.width / 2), 1.5*addHeight);
    [parent.imageView setFrame:imageViewFrame];
    
    if([jenisGagal isEqualToString:@"RejectVideo"])
    {
        OnboardingButton *mainButton = [[OnboardingButton alloc] initWithFrame:CGRectMake(10.0, parent.contentView.frame.size.height - mainButtonHeight - mainButtonMargin, self.view.frame.size.width - 20.0, mainButtonHeight)];
        [mainButton setTitle:@"COBA LAGI" forState:UIControlStateNormal];
        [self.view addSubview:mainButton];
        [mainButton addTarget:self action:@selector(mainButtonDidTappedHandler:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        if(parent.scrollView.contentSize.height > parent.scrollView.frame.size.height)
            parent.contentViewHeight.constant -= (mainButtonHeight + mainButtonMargin);
    }
    
    UIImage *imgClose = [UIImage imageNamed:@"ic_close_white@2x.png"];
    CGRect frameClose = CGRectMake(parent.contentView.frame.size.width - 44.0, 10.0, 24.0, 24.0);
    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClose setImage:imgClose forState:UIControlStateNormal];
    [btnClose setFrame:frameClose];
    [btnClose addTarget:self action:@selector(btnCloseTappedHandler) forControlEvents:UIControlEventTouchUpInside];
    [parent.scrollView addSubview:btnClose];
}

- (void)mainButtonDidTappedHandler:(id)sender
{
    Encryptor *encryptor = [[Encryptor alloc] init];
    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY];
    [encryptor setUserDefaultsObject:@"VerifikasiOnlineScreen" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    [encryptor setUserDefaultsObject:[NSString stringWithFormat:@"%f", lat] forKey:ONBOARDING_CUSTOMER_LOCATION_LAT];
    [encryptor setUserDefaultsObject:[NSString stringWithFormat:@"%f", lng] forKey:ONBOARDING_CUSTOMER_LOCATION_LNG];
    [encryptor setUserDefaultsObject:[self getDeviceType] forKey:ONBOARDING_CUSTOMER_DEVICE];
    if (![self->rekeningInfo.jenisKyc isEqualToString:@"VIDEO"]) {
        [self showLoading];
        dispatch_group_t dispatch = dispatch_group_create();
        CustomerOnboardingData *cod = [[CustomerOnboardingData alloc] init];
        [cod setDispatchGroup:dispatch];
        [cod sendDataToBackend];
        dispatch_group_notify(dispatch, dispatch_get_main_queue(), ^{
            [self hideLoading];
            if(cod.requestIsSuccess && ![cod.nomorAntrian isEqualToString:@""])
            {
                UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiOnlineScreen"];
                vc.modalPresentationStyle = UIModalPresentationFullScreen;
                [self presentViewController:vc animated:YES completion:nil];
            }
            else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ooops" message:NSLocalizedString(@"GENERAL_FAILURE", @"ERR: Failed to save nasabah data") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:actionOk];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        });
    }
    else {
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiOnlineScreen"];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (void)btnCloseTappedHandler
{
    // GO TO MSM
//    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
//    vc.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:vc animated:YES completion:nil];
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}

- (void)adjustViewForRejectVideo
{
    [self.rejectvideoView setHidden:NO];
    
    NSString *text = [NSString stringWithFormat:NSLocalizedString(@"ACCOUNT_REJECTEDVIDEO_TEXT", @"Account opening rejected video text"), rekeningInfo.namaNasabah, rekeningInfo.jenisTabungan];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text];
    UIFont *styleFont = self.textViewRejectVideo.font;
    [attrStr setAttributes:@{ NSFontAttributeName: styleFont } range:NSMakeRange(0, attrStr.length)];
    NSMutableParagraphStyle *ps = NSMutableParagraphStyle.new;
    ps.alignment = NSTextAlignmentCenter;
    [attrStr addAttribute:NSParagraphStyleAttributeName value:ps range:NSMakeRange(0, attrStr.length)];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:[attrStr.string rangeOfString:rekeningInfo.namaNasabah]];
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:[attrStr.string rangeOfString:rekeningInfo.jenisTabungan]];
    [_textViewRejectVideo setAttributedText:attrStr];
}

- (void)adjustViewForRejected
{
    if ([[self->rekeningInfo jenisKyc] isEqualToString:@"CABANG"])
    {
        [self.rejectedCabangView setHidden:NO];
    }
    else
    {
        [self.rejectedView setHidden:NO];
        
        NSDictionary *dataCabang = [enc getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_REKENING_CABANG_KEY];
        if(dataCabang)
        {
            [_labelNamaCabang setText:[dataCabang objectForKey:@"nama"]];
            [_labelAlamatCabang setText:[dataCabang objectForKey:@"alamat"]];
        }
        else
        {
            NSMutableDictionary * cabangResult = [[NSMutableDictionary alloc] init];
            dispatch_group_t dispatchGroup = dispatch_group_create();
            [self showLoading];
            [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/master/branch/retrieve/%@", rekeningInfo.kodeCabang] dispatchGroup:dispatchGroup returnData:cabangResult];
            dispatch_group_notify(dispatchGroup,dispatch_get_main_queue(), ^{
                [self hideLoading];
                if([[cabangResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
                {
                    NSDictionary *dataCabang = [cabangResult objectForKey:@"data"];
                    [self->_labelNamaCabang setText:[dataCabang objectForKey:@"nama"]];
                    [self->_labelAlamatCabang setText:[dataCabang objectForKey:@"alamat"]];
                }
            });
        }
    }
}

- (NSString *)getDeviceType {
    @try {
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *deviceCode = [NSString stringWithCString:systemInfo.machine
                                                  encoding:NSUTF8StringEncoding];
        static NSDictionary* deviceNamesByCode = nil;
        if (!deviceNamesByCode) {
            deviceNamesByCode = @{@"i386"      : @"Simulator",
                                  @"x86_64"    : @"Simulator",
                                  @"iPod1,1"   : @"iPod Touch",        // (Original)
                                  @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                                  @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                                  @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                                  @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                                  @"iPhone1,1" : @"iPhone",            // (Original)
                                  @"iPhone1,2" : @"iPhone",            // (3G)
                                  @"iPhone2,1" : @"iPhone",            // (3GS)
                                  @"iPad1,1"   : @"iPad",              // (Original)
                                  @"iPad2,1"   : @"iPad 2",            //
                                  @"iPad3,1"   : @"iPad",              // (3rd Generation)
                                  @"iPhone3,1" : @"iPhone 4",          // (GSM)
                                  @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                                  @"iPhone4,1" : @"iPhone 4S",         //
                                  @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                                  @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                                  @"iPad3,4"   : @"iPad",              // (4th Generation)
                                  @"iPad2,5"   : @"iPad Mini",         // (Original)
                                  @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                                  @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                                  @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                                  @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                                  @"iPhone7,1" : @"iPhone 6 Plus",     //
                                  @"iPhone7,2" : @"iPhone 6",          //
                                  @"iPhone8,1" : @"iPhone 6S",         //
                                  @"iPhone8,2" : @"iPhone 6S Plus",    //
                                  @"iPhone8,4" : @"iPhone SE",         //
                                  @"iPhone9,1" : @"iPhone 7",          //
                                  @"iPhone9,3" : @"iPhone 7",          //
                                  @"iPhone9,2" : @"iPhone 7 Plus",     //
                                  @"iPhone9,4" : @"iPhone 7 Plus",     //
                                  @"iPhone10,1": @"iPhone 8",          // CDMA
                                  @"iPhone10,4": @"iPhone 8",          // GSM
                                  @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                                  @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                                  @"iPhone10,3": @"iPhone X",          // CDMA
                                  @"iPhone10,6": @"iPhone X",          // GSM
                                  @"iPhone11,2": @"iPhone XS",         //
                                  @"iPhone11,4": @"iPhone XS Max",     //
                                  @"iPhone11,6": @"iPhone XS Max",     // China
                                  @"iPhone11,8": @"iPhone XR",         //
                                  @"iPhone12,1": @"iPhone 11",         //
                                  @"iPhone12,3": @"iPhone 11 Pro",     //
                                  @"iPhone12,5": @"iPhone 11 Pro Max", //

                                  @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                                  @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                                  @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                                  @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                                  @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                                  @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                                  @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                                  @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                                  @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                                  };
        }
        
        return [deviceNamesByCode objectForKey:deviceCode];
    } @catch (NSException *exception) {
        return @"iPhone";
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
