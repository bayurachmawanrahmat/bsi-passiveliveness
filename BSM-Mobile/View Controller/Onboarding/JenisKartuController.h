//
//  JenisKartuController.h
//  BSM-Mobile
//
//  Created by ARS on 21/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JenisKartuController : OnboardingRootController <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *namaKartu;
@property (weak, nonatomic) IBOutlet UILabel *deskripsiKartu;

@end

NS_ASSUME_NONNULL_END
