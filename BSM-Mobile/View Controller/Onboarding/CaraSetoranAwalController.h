//
//  CaraSetoranAwalController.h
//  BSM-Mobile
//
//  Created by ARS on 26/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrasiSelesaiController.h"
#import "Rekening.h"
#import "CabangViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CaraSetoranAwalController : RegistrasiSelesaiController <CabangViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *box1;
@property (weak, nonatomic) IBOutlet UIView *box2;

- (instancetype)initWithRekening:(Rekening *)rekening;

@end

NS_ASSUME_NONNULL_END
