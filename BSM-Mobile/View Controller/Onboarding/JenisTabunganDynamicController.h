//
//  JenisTabunganDynamicController.h
//  BSM-Mobile
//
//  Created by ARS on 20/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JenisTabunganDynamicCell.h"
#import "JenisTabunganDetailController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JenisTabunganDynamicController : OnboardingRootController <UITableViewDelegate, UITableViewDataSource, JenisTabunganDynamicCellDelegate, JenisTabunganDetailDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void)getJenisTabungan;

@end

NS_ASSUME_NONNULL_END
