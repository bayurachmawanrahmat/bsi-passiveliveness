//
//  TabunganBerhasilController.m
//  BSM-Mobile
//
//  Created by ARS on 25/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TabunganBerhasilController.h"
#import "OnboardingButton.h"
#import "Encryptor.h"
#import "SetoranAwalController.h"
#import "CaraSetoranAwalController.h"
#import "HomeViewController.h"
#import "OnboardingTemplate01ViewController.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"
#import "NCreatePasswordViewController.h"
#import "MenuViewController.h"
#import "ScrollHomeViewController.h"

@interface TabunganBerhasilController ()
{
    Encryptor *enc;
    Rekening *rekeningInfo;
}
@property (weak, nonatomic) IBOutlet UILabel *labelNomorRekening;
@property (weak, nonatomic) IBOutlet UILabel *labelJenisKartu;

@end

@implementation TabunganBerhasilController

- (IBAction)hitBtnGuide:(id)sender {
    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
    CaraSetoranAwalController *contentVC = [[CaraSetoranAwalController alloc] initWithRekening:rekeningInfo];
    [templateVC setChildController:contentVC];
    [self presentViewController:templateVC animated:YES completion:nil];
}

- (instancetype)initWithRekening:(Rekening *)rekening
{
    self = [super init];
    if (self) {
        enc = [[Encryptor alloc] init];
        self->rekeningInfo = rekening;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //[enc setUserDefaultsObject:@"InformasiNoHandphoneScreen" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    if ([rekeningInfo.nomorRekening isEqual:[NSNull null]])
        [_labelNomorRekening setText:@""];
    else
        [_labelNomorRekening setText:rekeningInfo.nomorRekening];
    if ([rekeningInfo.jenisKartu isEqual:[NSNull null]])
        [_labelJenisKartu setText:@""];
    else
        [_labelJenisKartu setText:rekeningInfo.jenisKartu];
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    
    [enc setUserDefaultsObject:@"TabunganBerhasil" forKey:ONBOARDING_BEGIN_SCREEN_KEY];
}

- (void)childHasLoaded:(OnboardingTemplate01ViewController *)parent
{
    CGFloat addHeight = 60.0;
    parent.imageBackgroundHeight.constant += addHeight;
    if(parent.scrollView.contentSize.height <= parent.scrollView.frame.size.height)
        parent.contentViewHeight.constant -= addHeight;
    [parent.view layoutIfNeeded];
    [parent.scrollView layoutIfNeeded];

    parent.imageView.image = [UIImage imageNamed:@"img_tabungan_berhasil@3x.png"];
    CGRect imageViewFrame = parent.imageView.frame;
    imageViewFrame.origin = CGPointMake((parent.view.frame.size.width / 2) - (parent.imageView.frame.size.width / 2), (parent.imageView.frame.size.height / 10));
    [parent.imageView setFrame:imageViewFrame];
    
    [parent.labelTitle setText:@"Alhamdulillah"];
    [parent.labelSubtitle setText:NSLocalizedString(@"ACCOUNT_SUCCESS_TEXT", @"Account opening success text")];
    [parent.labelTitle setAlpha:1.0];
    [parent.labelSubtitle setAlpha:1.0];
    
    [parent.navigationBar setHidden:YES];
    
    OnboardingButton *mainButton = [[OnboardingButton alloc] initWithFrame:CGRectMake(10.0, parent.contentView.frame.size.height - mainButtonMargin, self.view.frame.size.width - 20.0, mainButtonHeight)];
    [mainButton setTitle:@"Mulai Bertransaksi" forState:UIControlStateNormal];
    [self.view addSubview:mainButton];
    [mainButton addTarget:self action:@selector(mainButtonDidTappedHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    parent.containerViewTop.constant = -(parent.navigationBar.frame.size.height);
    [parent.view layoutIfNeeded];
}

- (void)mainButtonDidTappedHandler:(id)sender
{
    NSString *sessionId = [NSUserdefaultsAes getValueForKey:@"session_id"];
    NSString *msisdn = [NSUserdefaultsAes getValueForKey:@"msisdn"];
    NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
    NSString *name = [NSUserdefaultsAes getValueForKey:@"name"];
    NSString *zpk = [NSUserdefaultsAes getValueForKey:@"zpk"];
    NSString *publickey = [NSUserdefaultsAes getValueForKey:@"publickey"];
    NSString *privatekey = [NSUserdefaultsAes getValueForKey:@"privatekey"];
    
    [self goToHome];
}

- (void)goToSetoranAwal {
    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
    SetoranAwalController *contentVC = [[SetoranAwalController alloc] init];
    [templateVC setChildController:contentVC];
    [self presentViewController:templateVC animated:YES completion:nil];
}

- (void) goToMsm {
    // remove progress step
    [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}

- (void) goToActivationScreen {
    // remove progress step
    [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // TODO SHOW ACTIVATION SCREEN
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        HomeViewController *homeVC = (HomeViewController *)firstVC.topViewController;
        UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"ActivationVC"];
        homeVC.slidingViewController.topViewController = viewCont;
    }];
}

- (void) goToCreatePassword {
    // remove progress step
    [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // TODO SHOW CREATE PASSWORD SCREEN
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        NCreatePasswordViewController *createPasswordVC = (NCreatePasswordViewController *)firstVC.topViewController;
        UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"NCreatePwdVC"];
        createPasswordVC.slidingViewController.topViewController = viewCont;
    }];
}

- (void) backToHome{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
    }
}

- (void) goToHome {
    // remove progress step
    [[[Encryptor alloc] init] setUserDefaultsObject:@"DONE" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:nil];
}

@end
