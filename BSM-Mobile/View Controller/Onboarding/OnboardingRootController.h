//
//  OnboardingRootController.h
//  BSM-Mobile
//
//  Created by ARS on 19/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OnboardingRootController : UIViewController

- (IBAction)hitBtnNext:(id)sender;

@end

NS_ASSUME_NONNULL_END
