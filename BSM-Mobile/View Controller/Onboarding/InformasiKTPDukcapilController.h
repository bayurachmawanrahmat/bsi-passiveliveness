//
//  informasiKTPDukcapilController.h
//  BSM-Mobile
//
//  Created by ARS on 16/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "ViewController.h"
#import "CameraOverlayView.h"
#import "UIDropdown.h"
#import "UIDropdownSearch.h"
#import "OnboardingRootController.h"
#import "OnboardingButton.h"
#import "NameVerificationFailedController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiKTPDukcapilController : OnboardingRootController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, CameraOverlayDelegate, UITextFieldDelegate, UIDropdownSearchDelegate, AddPopupNameFailedListener>

@property (strong, nonatomic) UIImagePickerController *cameraPicker;
@property (strong, nonatomic) UIImage *imagePicked;
@property IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *fieldNomorKtp;
@property (weak, nonatomic) IBOutlet UITextField *fieldNama;
@property (weak, nonatomic) IBOutlet UITextField *fieldNamaIbu;
@property (weak, nonatomic) IBOutlet UITextField *fieldAlamat;
@property (weak, nonatomic) IBOutlet UITextField *fieldPropinsi;
@property (weak, nonatomic) IBOutlet UITextField *fieldKabupatenKota;
@property (weak, nonatomic) IBOutlet UITextField *fieldKecamatan;
@property (weak, nonatomic) IBOutlet UITextField *fieldKelurahan;
@property (weak, nonatomic) IBOutlet UITextField *fieldRt;
@property (weak, nonatomic) IBOutlet UITextField *fieldRw;

@property (weak, nonatomic) IBOutlet UITextField *fieldZip;
@property (weak, nonatomic) IBOutlet UIDropdown *fieldPendidikan;
@property (weak, nonatomic) IBOutlet UIButton *checkAlamatBeda;
@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, assign) NSInteger isAlamatBeda;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintPendidikanTop;

- (void)setImageView;
@property (weak, nonatomic) IBOutlet UIButton *btnRetake;
@property (weak, nonatomic) IBOutlet OnboardingButton *btnNext;

-(IBAction)hitCheckAlamatBeda:(id)sender;
- (IBAction)hitBtnRetake:(id)sender;
- (IBAction)hitBtnNext:(id)sender;

- (BOOL)checkInput;

@end

NS_ASSUME_NONNULL_END
