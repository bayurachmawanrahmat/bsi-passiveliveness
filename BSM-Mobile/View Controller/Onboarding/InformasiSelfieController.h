//
//  InformasiSelfieController.h
//  BSM-Mobile
//
//  Created by ARS on 17/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ViewController.h"
#import "CameraOverlayView.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface InformasiSelfieController : OnboardingRootController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, CameraOverlayDelegate>

@property (strong, nonatomic) UIImagePickerController *cameraPicker;
@property (strong, nonatomic) UIImage *imagePicked;
@property IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (weak, nonatomic) IBOutlet UIView *contentView;
- (IBAction)hitBtnRetake:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnRetake;
- (IBAction)hitBtnBack:(id)sender;



- (void)setImageView;


@end

NS_ASSUME_NONNULL_END
