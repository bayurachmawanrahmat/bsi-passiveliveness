//
//  NameVerificationFailedController.m
//  BSM-Mobile
//
//  Created by ARS on 30/08/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "NameVerificationFailedController.h"
#import "InformasiSelfieController.h"

@interface NameVerificationFailedController ()

@end

@implementation NameVerificationFailedController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewController.layer.cornerRadius = 30.0f;
    self.viewController.layer.masksToBounds = true;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnNext:(id)sender {
    id<AddPopupNameFailedListener> strongDelegate = self.delegate;
    if ([strongDelegate respondsToSelector:@selector(onNextNameFailed)]) {
        [strongDelegate onNextNameFailed];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void) goToSelfie {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    InformasiSelfieController *informasiSelfieController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiSelfieScreen"];
    [self presentViewController:informasiSelfieController animated:YES completion:nil];
}

@end
