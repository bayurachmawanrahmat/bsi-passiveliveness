//
//  NikTerdaftarController.m
//  BSM-Mobile
//
//  Created by ARS on 27/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "NikTerdaftarController.h"
#import "ECSlidingViewController.h"
#import "HomeViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface NikTerdaftarController ()
@property (weak, nonatomic) IBOutlet UIButton *btnAktivasi;
@property (weak, nonatomic) IBOutlet UIButton *btnLupaKode;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
- (IBAction)btnCloseDidHit:(id)sender;

@end

@implementation NikTerdaftarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.viewContainer.layer.cornerRadius = 30.0f;
    self.viewContainer.layer.masksToBounds = true;
    
    self.btnAktivasi.layer.cornerRadius = 25.0f;
    self.btnAktivasi.layer.masksToBounds = true;
    self.btnAktivasi.layer.borderWidth = 2.0f;
    self.btnAktivasi.layer.borderColor = [[UIColor colorWithRed:20/255.0 green:124/255.0 blue:113/255.0 alpha:1/1.0] CGColor];
    [self.btnAktivasi addTarget:self action:@selector(btnAktivasiDidHit) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnLupaKode addTarget:self action:@selector(btnLupaKodeDidHit) forControlEvents:UIControlEventTouchUpInside];
}

- (void)btnAktivasiDidHit
{
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // TODO SHOW ACTIVATION SCREEN
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        HomeViewController *homeVC = (HomeViewController *)firstVC.topViewController;
        UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"ActivationVC"];
        homeVC.slidingViewController.topViewController = viewCont;
    }];
}

- (void)btnLupaKodeDidHit
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"ActivationRetrival"];
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)btnCloseDidHit:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
