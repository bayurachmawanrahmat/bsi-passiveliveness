//
//  VerifikasiOnlineSelesaiController.h
//  BSM-Mobile
//
//  Created by ARS on 17/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerifikasiOnlineSelesaiController : OnboardingRootController

@end

NS_ASSUME_NONNULL_END
