//
//  PembukaanAkunAwalController.m
//  BSM-Mobile
//
//  Created by ARS on 02/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PembukaanAkunAwalController.h"
#import "CustomerOnboardingData.h"
#import "NSObject+FormInputCategory.h"
#import "Encryptor.h"
#import "OnboardingTemplate01ViewController.h"
#import "TabunganBerhasilController.h"
#import "GagalVerifikasiController.h"
#import "Rekening.h"
#import "HomeViewController.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"

@interface PembukaanAkunAwalController ()
{
    BOOL isContinueProgress;
    BOOL needUpdateVideoAssist;
    NSMutableDictionary *videoAssists;
    Encryptor *encryptor;
    NSInteger videoAssistVersion;
    NSMutableData *videoAssistData;
    dispatch_group_t dispatchGroup;
    void (^_completionGetSchemaHandler)(BOOL isSuccess);
}
- (IBAction)hitBtnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIView *maintenanceView;
@property (weak, nonatomic) IBOutlet UILabel *maintenanceText;
@property (weak, nonatomic) IBOutlet UIView *connectionView;
- (IBAction)hitBtnNext:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *hitBtnMaintenance;
- (IBAction)hitBtnTryAgain:(id)sender;

@end

@implementation PembukaanAkunAwalController

- (instancetype)initWithVideoAssistVersion:(NSInteger)version
{
    self = [super init];
    if (self) {
        videoAssistVersion = version;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    
    self.topImageHeightConstrain.constant = (screenHeight / 3);
    
    self.contentView.layer.cornerRadius = 30.0f;
    self.maintenanceView.layer.cornerRadius = 30.0f;
    
    dispatchGroup = dispatch_group_create();
    
    needUpdateVideoAssist = NO;
    [self checkDeviceHasNotch];
    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_RESTORE_DATA_KEY];
//    [encryptor setUserDefaultsObject:@"PembukaanRekeningScreen" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
    
    [self.hitBtnMaintenance addTarget:self action:@selector(btnMaintenanceTappedHandler) forControlEvents:UIControlEventTouchUpInside];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self initScreen];
}

- (void) initScreen {
    [self showLoading];
    
    [self.connectionView setHidden:YES];
    
    // CHECK COB ENABLED
    NSMutableDictionary *checkCobResult = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=COB_ENABLED" dispatchGroup:dispatchGroup returnData:checkCobResult];
    
    // INIT MSMSID
    [self initMsmsid];
    
    // CHECK VIDEO ASSIST VERSION
//    NSMutableDictionary *checkVersionResult = [[NSMutableDictionary alloc] init];
//    videoAssistVersion = 1;
//    [CustomerOnboardingData getFromEndpoint:@"api/setting/getVideoVer" dispatchGroup:dispatchGroup returnData:checkVersionResult];
    
    // CHECK IF MSMSID DID APPLIED
    NSString *msmsid = [CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY];
    NSMutableDictionary *checkMsmsidResult = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:dispatchGroup returnData:checkMsmsidResult];
    
    // CHECK MAX MIN COB
//    NSMutableDictionary *checkMaxResult = [[NSMutableDictionary alloc] init];
//    NSMutableDictionary *checkMinResult = [[NSMutableDictionary alloc] init];
//    [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=MAX_COB" dispatchGroup:dispatchGroup returnData:checkMaxResult];
//    [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=MIN_COB" dispatchGroup:dispatchGroup returnData:checkMinResult];
    
    // CHECK COB TIME
    NSMutableDictionary *checkTimeResult = [[NSMutableDictionary alloc] init];
    NSString *mobversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/setting/getCobTimesNew?mobver=%@", mobversion] dispatchGroup:dispatchGroup returnData:checkTimeResult];
    
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        if( [[checkCobResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]] &&
           [[checkTimeResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]] )
        {
            NSDictionary *data;
            if([[checkCobResult objectForKey:@"data"] isKindOfClass:NSArray.class])
                data = [(NSArray *)[checkCobResult objectForKey:@"data"] objectAtIndex:0];
            else
                data = [checkCobResult objectForKey:@"data"];
            BOOL cobEnabled = [[data objectForKey:@"setval"] isEqualToString:@"true"] ? YES : NO;
            if(!cobEnabled)
            {
//                if([[checkMaxResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]
//                   && [[checkMinResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
//                {
//                    NSString *max = [[checkMaxResult objectForKey:@"data"] objectForKey:@"setval"];
//                    NSString *min = [[checkMinResult objectForKey:@"data"] objectForKey:@"setval"];
//                    [self.maintenanceText setText:[NSString stringWithFormat:@"Layanan dapat diakses mulai pukul %@ s.d %@ WIB", min, max]];
//                    [self.maintenanceText setNumberOfLines:0];
//                }
                if([[checkTimeResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
                    NSString *max = [[checkTimeResult objectForKey:@"data"] objectForKey:@"MAX"];
                    NSString *min = [[checkTimeResult objectForKey:@"data"] objectForKey:@"MIN"];
                    [self.maintenanceText setText:[NSString stringWithFormat:NSLocalizedString(@"ONBOARDING_MAINTENACE_TEXT", @"ONBOARDING_MAINTENACE_TEXT"), min, max]];

                }
                [self.maintenanceView setHidden:NO];
            }
            else
            {
//                if([[checkMaxResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]
//                   && [[checkMinResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
//                {
//                    NSString *max;
//                    NSString *min;
//                    if([[checkMaxResult objectForKey:@"data"] isKindOfClass:NSArray.class])
//                        max = [[(NSArray *)[checkMaxResult objectForKey:@"data"] objectAtIndex:0] objectForKey:@"setval"];
//                    else
//                        max = [[checkMaxResult objectForKey:@"data"] objectForKey:@"setval"];
//                    if([[checkMinResult objectForKey:@"data"] isKindOfClass:NSArray.class])
//                        min = [[(NSArray *)[checkMinResult objectForKey:@"data"] objectAtIndex:0] objectForKey:@"setval"];
//                    else
//                        min = [[checkMinResult objectForKey:@"data"] objectForKey:@"setval"];
//
//                    NSDate *now = [NSDate date];
//                    NSDate *first = [CustomerOnboardingData date:now withHour:[[min substringToIndex:2] intValue] minute:[[min substringFromIndex:3] intValue]];
//                    NSDate *last = [CustomerOnboardingData date:now withHour:[[max substringToIndex:2] intValue] minute:[[max substringFromIndex:3] intValue]];
//
//                    if(![CustomerOnboardingData compareDate:now withinFirst:first andLast:last])
//                    {
//                        [self.maintenanceText setText:[NSString stringWithFormat:@"Layanan dapat diakses mulai pukul %@ s.d %@ WIB", min, max]];
//                        [self.maintenanceText setNumberOfLines:0];
//                        [self.maintenanceView setHidden:NO];
//                    }
//                }
                
                if([[checkTimeResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
                    NSString *max;
                    NSString *min;
                    if([[checkTimeResult objectForKey:@"data"] isKindOfClass:NSArray.class])
                        max = [[(NSArray *)[checkTimeResult objectForKey:@"data"] objectAtIndex:0] objectForKey:@"MAX"];
                    else
                        max = [[checkTimeResult objectForKey:@"data"] objectForKey:@"MAX"];
                    if([[checkTimeResult objectForKey:@"data"] isKindOfClass:NSArray.class])
                        min = [[(NSArray *)[checkTimeResult objectForKey:@"data"] objectAtIndex:0] objectForKey:@"MIN"];
                    else
                        min = [[checkTimeResult objectForKey:@"data"] objectForKey:@"MIN"];

                    NSLog(@"checkTimeResult : %@", [checkTimeResult objectForKey:@"data"]);
                    if(![[[checkTimeResult objectForKey:@"data"] objectForKey:@"iscobtime"] isEqualToNumber:[NSNumber numberWithBool:YES]])
                    {
                        [self.maintenanceText setText:[NSString stringWithFormat:NSLocalizedString(@"ONBOARDING_MAINTENACE_TEXT", @"ONBOARDING_MAINTENACE_TEXT"), min, max]];
                        [self.maintenanceText setNumberOfLines:0];
                        [self.maintenanceView setHidden:NO];
                    }
                    else if(![[[checkTimeResult objectForKey:@"data"] objectForKey:@"isVersionAcceptable"] isEqualToNumber:[NSNumber numberWithBool:YES]]) {
                        [self.maintenanceText setText:NSLocalizedString(@"ONBOARDING_MOBVERSION_FAILED", @"ONBOARDING_MOBVERSION_FAILED")];
                        [self.maintenanceText setNumberOfLines:0];
                        [self.maintenanceView setHidden:NO];
                    }
                }
            }
            
            if([self.maintenanceView isHidden])
            {
                // DOWNLOAD VIDEO ASSIST
//                if([[checkVersionResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
//                {
//                    if([encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEASSIST_VERSION])
//                        videoAssistVersion = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEASSIST_VERSION] integerValue];
//
//                    if(videoAssistVersion == [[checkVersionResult objectForKey:@"data"] integerValue])
//                        needUpdateVideoAssist = NO;
//                    else
//                        videoAssistVersion = [[checkVersionResult objectForKey:@"data"] integerValue];
//
//                    if(needUpdateVideoAssist)
//                        [self downloadVideoAssistVersion];
//                }
                
                if([[checkMsmsidResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
                {
                    NSMutableDictionary *nasabah = [checkMsmsidResult objectForKey:@"data"];
                    NSString *status = [nasabah objectForKey:@"status"];
                    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                    Rekening *rekening = [[Rekening alloc] init];
                    [rekening setNamaNasabah:[nasabah objectForKey:@"nama_lgkp"]];
                    [rekening setNomorRekening:[nasabah objectForKey:@"norek"]];
                    [rekening setJenisTabungan:[nasabah objectForKey:@"jenis_tabungan"]];
                    [rekening setJenisKartu:[nasabah objectForKey:@"jenis_kartu"]];
                    [rekening setKodeCabang:[nasabah objectForKey:@"kode_cabang"]];
                    [rekening setJenisKyc:[nasabah objectForKey:@"jenis_kyc"]];
                    [CustomerOnboardingData setObject:rekening forKey:ONBOARDING_REKENING_INFO_KEY];
                    
                    if([status isEqualToString:@"APPROVED"])
                    {
                        [self->encryptor removeVideoAssist];
                        
                        // GO TO TabunganBerhasil Screen
                        NSString* step = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY];
                        if ([step isEqualToString:@"InformasiNoHandphoneScreen"]) {
                            // GO TO Create PIN
                            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:step];
                            vc.modalPresentationStyle = UIModalPresentationFullScreen;
                            [self presentViewController:vc animated:YES completion:nil];
                        } else {
                            OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                            TabunganBerhasilController *contentVC = [[TabunganBerhasilController alloc] initWithRekening:rekening];
                            [templateVC setChildController:contentVC];
                            [self presentViewController:templateVC animated:YES completion:nil];
                        }
                    }
                    else if([status isEqualToString:@"REJECTEDVIDEO"])
                    {
                        // CHECK IF LOCAL DATA STILL EXISTS
                        if(![self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] && ![self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY])
                            [self->encryptor restoreDataToLocalUsingData:nasabah];
                        
                        // GO TO GagalVerifikasi Screen
                        OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                        GagalVerifikasiController *contentVC = [[GagalVerifikasiController alloc] initForRejectVideoWithRekening:rekening];
                        [templateVC setChildController:contentVC];
                        [self presentViewController:templateVC animated:YES completion:nil];
                    }
                    else if([status isEqualToString:@"NEW"] && [[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"DONE"])
                    {
                        // CHECK EXPIRY DATE
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSString *expiryString = [nasabah objectForKey:@"expiry_date"];
                        NSDate *now = [NSDate date];
                        NSDate *expiryDate = [dateFormatter dateFromString: [NSString stringWithFormat:@"%@ 23:59:59", expiryString]];
                        
                        if([expiryDate compare:now] == NSOrderedDescending)
                        {
                            NSString *metodeVerifikasi = [nasabah objectForKey:@"jenis_kyc"];
                            if([metodeVerifikasi isEqualToString:@"CABANG"])
                            {
                                // GO TO NomorTiketScene
                                UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"NomorTiketScene"];
                                [self presentViewController:vc animated:YES completion:nil];
                            }
                            else if ([metodeVerifikasi isEqualToString:@"VIDEO"])
                            {
                                // GO TO VerifikasiOnlineSelesaiScene
                                UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiOnlineSelesaiScene"];
                                [self presentViewController:vc animated:YES completion:nil];
                            }
                        }
                        else [self->encryptor resetFormData];
                    }
                    else if ([status isEqualToString:@"REJECTED"] && ([[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"DONE"] || [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] == nil || [[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"PilihMetodeScreen"] || [[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"PilihUlangMetodeScreen"]))
                    {
                        // GO TO GagalVerifikasi Screen
                        OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                        GagalVerifikasiController *contentVC = [[GagalVerifikasiController alloc] initForRejectedWithRekening:rekening];
                        [templateVC setChildController:contentVC];
                        [self presentViewController:templateVC animated:YES completion:nil];
                    }
                }
            }
            
            // CHECK CONTINUE PROGRESS
            if([self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] && ![[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"DONE"] && ![[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"NEW"])
            {
                self->isContinueProgress = YES;
                self->_labelTitle.text = @"Lanjut Pembukaan Rekening";
                self->_labelContent.text = @"Anda sudah melakukan beberapa proses pembukaan rekening, beberapa langkah lagi Rekening Bank Syariah Indonesia akan menjadi milik Anda";
                [self->_btnNext setTitle:@"LANJUT PEMBUKAAN REKENING" forState:UIControlStateNormal];
            }
            else
            {
                self->isContinueProgress = NO;
                self->_labelTitle.text = @"Buat Rekening Bank\n Syariah Indonesia Anda";
                self->_labelContent.text = @"Dapatkan Rekening Anda dan nikmati\n pengalaman #lebihberkah bersama Bank Syariah Indonesia";
            }
            
            [self hideLoading];
            if([self.maintenanceView isHidden]) [self.contentView setHidden:NO];
        }
        // connection failed
        else {
            [self hideLoading];
            [self.connectionView setHidden:NO];
        }
    });
}

- (void) checkDeviceHasNotch
{
    NSNumber *hasNotch = [[NSNumber alloc] initWithBool:NO];
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        int height = (int)[[UIScreen mainScreen] nativeBounds].size.height;
        if(height >= 2436 || height == 1792)
            hasNotch = [NSNumber numberWithBool:YES];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:hasNotch forKey:ONBOARDING_DEVICE_HAS_NOTCH_KEY];
}

- (void) initMsmsid
{
    if(![CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY])
    {
        NSUUID *uuid = [NSUUID UUID];
        NSString *stringUUID = [uuid UUIDString];
        
        [encryptor setUserDefaultsObject:stringUUID forKey:ONBOARDING_MSMSID_KEY];
        [CustomerOnboardingData setValue:stringUUID forKey:ONBOARDING_MSMSID_KEY];
    }
    else [encryptor setUserDefaultsObject:[CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY] forKey:ONBOARDING_MSMSID_KEY];
}

- (void) downloadVideoAssistVersion
{
    if(!encryptor) encryptor = [[Encryptor alloc] init];
    [encryptor removeVideoAssist];
    NSString *endpoint = @"api/files/getByCategory?category=VIDEO_KYC";
    NSString *url =[[CustomerOnboardingData apiUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:endpoint]];
    NSString *token = [CustomerOnboardingData getToken];
    videoAssists = [[NSMutableDictionary alloc] init];
    NSLog(@"---------------- %@", url);
    
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"DOWNLOAD_VIDEO_ASSISTS"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [urlRequest setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
    [urlRequest setValue:token forHTTPHeaderField:@"Authorization"];
    NSURLSessionDataTask *sessionDataTask = [session dataTaskWithRequest:urlRequest];
    [sessionDataTask resume];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    if(!videoAssistData)
    {
        videoAssistData = [NSMutableData dataWithData:data];
    }
    else
    {
        [videoAssistData appendData:data];
    }
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if(!error)
    {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)[task response];
        if([httpResponse statusCode] == 200) {
            NSDictionary *value;
            if(ONBOARDING_SAFE_MODE)
            {
                NSString *encryptedString = [[NSMutableString alloc] initWithData:videoAssistData encoding:NSUTF8StringEncoding];
                NSData *decryptedData = [Encryptor AESDecryptData:encryptedString];
                value = [NSJSONSerialization JSONObjectWithData:decryptedData options:kNilOptions error:&error];
            }
            else
            {
                value  = [NSJSONSerialization JSONObjectWithData:videoAssistData options:kNilOptions error:&error];
            }
            
            if(value)
            {
                int randomVidPIdx = 0;
                int randomVidLIdx = 0;
                for(NSDictionary *aData in value)
                {
                    NSString *videoUrl = [aData objectForKey:@"fileurl"];
                    NSString *description = [aData objectForKey:@"description"];
                    NSString *filename;
                    if([[aData objectForKey:@"israndom"] integerValue] == 0)
                    {
                        filename = [NSString stringWithFormat:@"%@%@.mp4", [aData objectForKey:@"gender"], [aData objectForKey:@"step"]];
                    }
                    else
                    {
                        NSString *gender = [aData objectForKey:@"gender"];
                        if([gender isEqualToString:@"L"])
                        {
                            filename = [NSString stringWithFormat:@"Random%@%d.mp4", gender, randomVidLIdx];
                            randomVidLIdx += 1;
                        }
                        else
                        {
                            filename = [NSString stringWithFormat:@"Random%@%d.mp4", gender, randomVidPIdx];
                            randomVidPIdx += 1;
                        }
                    }
                    [self downloadVideoFromUrl:videoUrl saveWithFilename:filename andDescription:description];
                }
                if(encryptor == nil) {
                    encryptor = [[Encryptor alloc] init];
                }
                if (!needUpdateVideoAssist) {
                    [encryptor setUserDefaultsObject:[NSString stringWithFormat:@"%ld", (long)videoAssistVersion] forKey:ONBOARDING_VIDEASSIST_VERSION];
                }
                [encryptor setUserDefaultsObject:videoAssists forKey:ONBOARDING_VIDEOASSIST_FILES];
                [encryptor setUserDefaultsObject:[NSNumber numberWithInt:randomVidLIdx] forKey:ONBOARDING_VIDEOASSIST_FILE_COUNT];
            }
        }
    }
    else
    {
        if(ONBOARDING_SAFE_MODE)
        {
            NSString *asd = [[NSMutableString alloc] initWithData:videoAssistData encoding:NSUTF8StringEncoding];
            NSData *zxc = [Encryptor AESDecryptData:asd];
            NSDictionary *qwe = [NSJSONSerialization JSONObjectWithData:zxc options:kNilOptions error:&error];
            NSLog(@"ERROR : %@", qwe);
        }
    }
}

- (void) downloadVideoFromUrl:(NSString *)stringUrl saveWithFilename:(NSString *)stringFilename andDescription:(NSString *)description
{
    NSData *videoAssist = [NSData dataWithContentsOfURL:[NSURL URLWithString:stringUrl]];
    if (videoAssist) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, stringFilename];
        
        if([videoAssist writeToFile:filePath atomically:YES])
        {
            NSDictionary *videoObject = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         stringFilename, @"filename",
                                         description, @"description",
                                         filePath, @"url",
                                         nil];
            [videoAssists setObject:videoObject forKey:[NSString stringWithFormat:@"%lu", (unsigned long)videoAssists.count]];
            NSLog(@"Added file %@", stringFilename);
        }
    }
    else {
        NSDictionary *videoObject = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     [NSString stringWithFormat:@"%@_NOT_FOUND", stringFilename], @"filename",
                                     description, @"description",
                                     stringUrl, @"url",
                                     nil];
        [videoAssists setObject:videoObject forKey:[NSString stringWithFormat:@"%lu", (unsigned long)videoAssists.count]];
        needUpdateVideoAssist = YES;
    }
}

- (IBAction)hitBtnNext:(id)sender {
    if(isContinueProgress)
    {
        [self getDukcapilSchema:^(BOOL isSuccess) {
            NSString *nextScreen = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY];
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:nextScreen];
            vc.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:vc animated:YES completion:nil];
        }];
    } else {
        [self getDukcapilSchema:^(BOOL isSuccess) {
            NSString *nextScreen = @"PrinsipPendanaanScreen";
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:nextScreen];
            vc.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:vc animated:YES completion:nil];
        }];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *btn = (UIButton *)sender;
        if(btn.tag == 11)
        {
            return NO;
        }
    }
    return YES;
}

- (void)btnMaintenanceTappedHandler
{
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // GO TO MSM
//    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
//    vc.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:vc animated:YES completion:nil];
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}
- (IBAction)hitBtnBack:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *ns = [NSUserDefaults standardUserDefaults];
    [ns setObject:@"-" forKey:@"last"];
    [ns removeObjectForKey:ONBOARDING_NEED_CHECK_KEY];
    [ns synchronize];
    
    // GO TO MSM
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}
- (IBAction)hitBtnTryAgain:(id)sender {
    [self initScreen];
}

- (void)getDukcapilSchema:(void(^)(BOOL))handler {
    [self showLoading];
    
    NSMutableDictionary *schemaResult = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/setting/get?key=%@", @"DUKCAPIL_SCHEMA"] dispatchGroup:dispatchGroup returnData:schemaResult];
    
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        self->_completionGetSchemaHandler = [handler copy];
        
        [self hideLoading];
        
        if([[schemaResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            @try {
                NSDictionary *schemaData = [schemaResult objectForKey:@"data"];
                NSString *schema = [schemaData objectForKey:@"setval"];
                [self->encryptor setUserDefaultsObject: [schema isEqualToString:@"1"] ? ONBOARDING_INPUT_DATA_IDENTITY_DUKCAPIL : ONBOARDING_INPUT_DATA_IDENTITY_MANUAL forKey:ONBOARDING_INPUT_DATA_IDENTITY_TYPE_KEY];
                
                self->_completionGetSchemaHandler(YES);
                self->_completionGetSchemaHandler = nil;
            } @catch (NSException *exception) {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
            } @finally {
                // do nothing
            }
        } else {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"ERROR", @"") sender:self];
        }
    });
}
@end

