//
//  TidakPunyaNpwpController.h
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ViewController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TidakPunyaNpwpController : OnboardingRootController

@property (weak, nonatomic) IBOutlet UIButton *brnRadio1;
@property (weak, nonatomic) IBOutlet UIButton *brnRadio2;
@property (weak, nonatomic) IBOutlet UIButton *brnRadio3;

- (IBAction)hitBtnRadio:(UIButton *)sender;

@end

NS_ASSUME_NONNULL_END
