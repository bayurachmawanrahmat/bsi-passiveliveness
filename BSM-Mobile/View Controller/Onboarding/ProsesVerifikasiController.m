//
//  ProsesVerifikasiController.m
//  BSM-Mobile
//
//  Created by ARS on 04/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "ProsesVerifikasiController.h"
#import "Encryptor.h"
#import "ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"
#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "OnboardingTemplate01ViewController.h"
#import "GagalVerifikasiController.h"
#import "Rekening.h"
#import "TabunganBerhasilController.h"
#import "PembukaanAkunController.h"
#import "Utility.h"
#import "CreatePinOnboardingController.h"

@interface ProsesVerifikasiController ()

@end

@implementation ProsesVerifikasiController{
    Encryptor *encryptor;
}

static const NSString *ScreenName = @"ProsesVerifikasiScreen";

- (void)viewDidLoad {
    [super viewDidLoad];

    _btnCekStatus.layer.cornerRadius = 25;
    _btnCekStatus.layer.masksToBounds = true;
    _btnCekStatus.layer.borderWidth = 0;
    [_btnCekStatus setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_btnCekStatus setTitleColor:[UIColor colorWithRed:20.0/255.0 green:124.0/255.0 blue:113.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_btnCekStatus.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_progressView setProgress:1];
        
    encryptor = [[Encryptor alloc] init];
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    [_btnCekStatus setHidden:YES];
    [self checkStatusVerification];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)checkVerificationStatus{
    [self showLoading];
}

- (IBAction)hitBtnClose:(id)sender {
    // GO TO MSM
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:YES completion:^{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}

- (IBAction)hitBtnCekStatus:(id)sender {
    
}

- (void)checkStatusVerification{
    [self showLoading];
    [_progressView setProgress:0];
    
    dispatch_async(dispatch_queue_create("validateFr", nil), ^{
        NSString *filename = @"VERIFIKASI";
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        self->_imageVerification = [self loadImageFromLocalStorageWithFileName:@"VERIFIKASI" ofType:@"jpg" inDirectory:documentsDirectory];
        UIImage *fixImage = [self fixOrientation:self->_imageVerification];
        if(fixImage.size.width > 2160.0)
        {
            fixImage = [self imageWithImage:fixImage scaledToSize:CGSizeMake(fixImage.size.width / 8, fixImage.size.height / 8)];
        }
        else if(fixImage.size.width > 1080.0)
        {
            fixImage = [self imageWithImage:fixImage scaledToSize:CGSizeMake(fixImage.size.width / 4, fixImage.size.height / 4)];
        }
        NSData *imageData = UIImageJPEGRepresentation(fixImage, 0.8);
        NSString *urlString = [[CustomerOnboardingData apiFrUrl] stringByAppendingString:[CustomerOnboardingData safeEndpoint:@"fr/validation"]];
        NSString *nik = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
        NSString *fileId = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_VERIFIKASI_WAJAH_FILEID_KEY];
        NSString *kodeRegistrasi = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY];
        
        NSString *currentDate = [Utility dateToday:6];
        NSString *rawHkey = [Utility sha256HashFor:[NSString stringWithFormat:@"%@%@%@", CLIENT_CREDENTIAL, SECRET_CREDENTIAL, currentDate]];
        NSString *hKey = [rawHkey substringToIndex:42];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request setValue:@"MSM_ONBOARDING" forHTTPHeaderField:@"User-Agent"];
        [request setValue:[CustomerOnboardingData getToken] forHTTPHeaderField:@"Authorization"];
        [request setValue:currentDate forHTTPHeaderField:@"X-BSI-TIMESTAMP"];
        [request setValue:hKey forHTTPHeaderField:@"X-BSI-HKEY"];
        NSMutableData *postbody = [NSMutableData data];
        
        // FILENAME param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"filename\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"%@.jpg", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // FILE ID param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"img_verifikasiid\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"%@",fileId] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // KODE REGISTRASI param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"kode_registrasi\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"%@",kodeRegistrasi] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // NIK param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"nik\"; \r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"%@",nik] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // FILE param
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[NSData dataWithData:imageData]];
        [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:postbody];
        
        // comment deprecated method
        // NSURLResponse *response = nil;
        // NSError *error = nil;
        // NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        // NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = 120.0;
        NSURLSessionTask *task = [[NSURLSession sessionWithConfiguration:sessionConfig] uploadTaskWithRequest:request fromData:postbody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            BOOL isSuccess;
            if(ONBOARDING_SAFE_MODE)
                returnString = [Encryptor AESDecrypt:returnString];
            if(error)
            {
                isSuccess = NO;
            }
            else
            {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if((long)[httpResponse statusCode] == 200)
                {
                    isSuccess = YES;
                }
                else
                    isSuccess = NO;
            }
            
            NSDictionary *resultJson = [self stringToJson:returnString];
            @try {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self->_progressView setProgress:1];
                    [self hideLoading];
                    long statusCode = [[resultJson valueForKey:@"statusCode"] integerValue];
                    NSDictionary *response = [resultJson objectForKey:@"response"];
                    
                    if (response == nil || [response isEqual:[NSNull null]]) {
                        [self goToPilihMetodeScreen];
                        return;
                    }
                    
                    switch (statusCode) {
                        case 0:
                            //[self goToBerhasilScreen];
                            [self goToCreatePin];
                            break;
                        case -1:
                            [self goToPilihMetodeScreen];
                            break;
                        case -2:
                        {
                            NSString *norek = [response objectForKey:@"norek"];
                            Rekening *rekening = [[Rekening alloc] init];
                            [rekening setNamaNasabah:@""];
                            [rekening setNomorRekening:norek];
                            [rekening setJenisTabungan:@""];
                            [rekening setJenisKartu:@""];
                            [rekening setKodeCabang:@""];
                            [self goToGagalScreen:rekening];
                            break;
                        }
                        case 99:
                            [self goToPilihMetodeScreen];
                            break;
                        default:
                            [self goToPilihMetodeScreen];
                            break;
                    }
                });
            }
            @catch (NSException *exception) {
               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:@"Terjadi kesalahan. Silahkan coba beberapa saat lagi." preferredStyle:UIAlertControllerStyleAlert];
               UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
               [alert addAction:alertAction];
               [self presentViewController:alert animated:YES completion:nil];
            }
        }];
        [task resume];
    });
}

-(NSDictionary *)stringToJson:(NSString *)stringJson{
    NSError *jsonError;
    NSData *objectData = [stringJson dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                          options:NSJSONReadingMutableContainers
                                            error:&jsonError];
    return json;
}

//-(void)goToBerhasilScreen {
//    dispatch_group_t dispatchGroup = dispatch_group_create();
//    NSMutableDictionary *checkResult = [[NSMutableDictionary alloc] init];
//    NSString *msmsid = [CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY];
//    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:dispatchGroup returnData:checkResult];
//    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
//        [self hideLoading];
//
//        if([[checkResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
//        {
//            NSMutableDictionary *nasabah = [checkResult objectForKey:@"data"];
//            Rekening *rekening = [[Rekening alloc] init];
//            [rekening setNamaNasabah:[nasabah objectForKey:@"nama_lgkp"]];
//            [rekening setNomorRekening:[nasabah objectForKey:@"norek"]];
//            [rekening setJenisTabungan:[nasabah objectForKey:@"jenis_tabungan"]];
//            [rekening setJenisKartu:[nasabah objectForKey:@"jenis_kartu"]];
//            [rekening setKodeCabang:[nasabah objectForKey:@"kode_cabang"]];
//            [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
//            [CustomerOnboardingData setObject:rekening forKey:ONBOARDING_REKENING_INFO_KEY];
//
//            OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
//            TabunganBerhasilController *contentVC = [[TabunganBerhasilController alloc] initWithRekening:rekening];
//            [templateVC setChildController:contentVC];
//            [self presentViewController:templateVC animated:YES completion:nil];
//        }
//        else
//        {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ooops" message:@"Terjadi kesalahan. Silahkan coba beberapa saat lagi." preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
//            [alert addAction:alertAction];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    });
//}

-(void)goToCreatePin{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    CreatePinOnboardingController *createPinController = [storyboard instantiateViewControllerWithIdentifier:@"CreatePinOnboardingScreen"];
    [self presentViewController:createPinController animated:YES completion:nil];
}

-(void)goToPilihMetodeScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    PembukaanAkunController *pembukaanAkunController = [storyboard instantiateViewControllerWithIdentifier:@"PilihMetodeScreen"];
    [self presentViewController:pembukaanAkunController animated:YES completion:nil];
}

-(void)goToGagalScreen:(Rekening *)rekening{
    OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
    GagalVerifikasiController *contentVC = [[GagalVerifikasiController alloc] initForRejectedWithRekening:rekening];
    [templateVC setChildController:contentVC];
    [self presentViewController:templateVC animated:YES completion:nil];
}

- (UIImage *)fixOrientation:(UIImage *)image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
