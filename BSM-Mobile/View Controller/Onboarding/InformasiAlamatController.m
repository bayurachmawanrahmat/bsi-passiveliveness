//
//  InformasiAlamatController.m
//  BSM-Mobile
//
//  Created by ARS on 17/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "InformasiAlamatController.h"
#import "AMPopTip.h"
#import "NSObject+FormInputCategory.h"
#import "UIDropdownSearch.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "InformasiStatusController.h"

@interface InformasiAlamatController () {
    UIDropdownSearch *dropdownProvinsi;
    UIDropdownSearch *dropdownKabupatenKota;
    UIDropdownSearch *dropdownKecamatan;
    UIDropdownSearch *dropdownKelurahan;
    UIDropdownSearch *dropdownProvinsiDiff;
    UIDropdownSearch *dropdownKabupatenKotaDiff;
    UIDropdownSearch *dropdownKecamatanDiff;
    UIDropdownSearch *dropdownKelurahanDiff;
    NSString *pageError;
    Encryptor *encryptor;
    dispatch_group_t referenceGroup;
    UIRefreshControl *refreshControl;
    NSDictionary *formInputDomisili;
    NSDictionary *formInputKTP;
    NSInteger isAlamatBeda;
    CGRect viewAlamatBedaFrame;
    NSLayoutConstraint *constraintHideViewAlamatBeda;
}

@end

@implementation InformasiAlamatController

static const NSString *ScreenName = @"InformasiPribadiSecondScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    referenceGroup = dispatch_group_create();
    
    self.fieldProvinsi.delegate = self;
    self.fieldKota.delegate = self;
    self.fieldKecamatan.delegate = self;
    self.fieldKelurahan.delegate = self;
    self.fieldKecamatan.delegate = self;
    self.fieldKelurahan.delegate = self;
    self.fieldAlamat.delegate = self;
    self.fieldRW.delegate = self;
    self.fieldRT.delegate = self;
    self.fieldKodePos.delegate = self;
    self.fieldProvinsiDiff.delegate = self;
    self.fieldKotaDiff.delegate = self;
    self.fieldKecamatanDiff.delegate = self;
    self.fieldKelurahanDiff.delegate = self;
    self.fieldKecamatanDiff.delegate = self;
    self.fieldKelurahanDiff.delegate = self;
    self.fieldAlamatDiff.delegate = self;
    self.fieldRWDiff.delegate = self;
    self.fieldRTDiff.delegate = self;
    self.fieldKodePosDiff.delegate = self;
    
    // setup check alamat berbeda
    UIImage *iconChecked = [UIImage imageNamed:@"ic_checkbox_active.png"];
    UIImage *iconUnchecked = [UIImage imageNamed:@"ic_checkbox_inactive.png"];
    [self.checkAlamatBeda setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
    [self.checkAlamatBeda setBackgroundImage:iconChecked forState:UIControlStateSelected];
    
    // input field toolbar numberpad
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"CANCEL", @"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"DONE", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.fieldKodePos.inputAccessoryView = numberToolbar;
    self.fieldKodePosDiff.inputAccessoryView = numberToolbar;
    self.fieldRT.inputAccessoryView = numberToolbar;
    self.fieldRTDiff.inputAccessoryView = numberToolbar;
    self.fieldRW.inputAccessoryView = numberToolbar;
    self.fieldRWDiff.inputAccessoryView = numberToolbar;
    
    // input field toolbar text
    UIToolbar* textToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    textToolbar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"DONE", @"Done") style:UIBarButtonItemStyleDone target:self action:@selector(textToolbarDone)];
    [textToolbar setItems:[[NSArray alloc] initWithObjects:space, done, nil]];
    [textToolbar setUserInteractionEnabled:YES];
    [textToolbar sizeToFit];
    self.fieldProvinsi.inputAccessoryView = textToolbar;
    self.fieldProvinsiDiff.inputAccessoryView = textToolbar;
    self.fieldKota.inputAccessoryView = textToolbar;
    self.fieldKotaDiff.inputAccessoryView = textToolbar;
    self.fieldKecamatan.inputAccessoryView = textToolbar;
    self.fieldKecamatanDiff.inputAccessoryView = textToolbar;
    self.fieldKelurahan.inputAccessoryView = textToolbar;
    self.fieldKelurahanDiff.inputAccessoryView = textToolbar;
    self.fieldAlamat.inputAccessoryView = textToolbar;
    self.fieldAlamatDiff.inputAccessoryView = textToolbar;
    
    // refresh control
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 10.0, *)) {
        [_scrollView setRefreshControl:refreshControl];
    } else {
        [_scrollView addSubview:refreshControl];
    }
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self setScrollView:_scrollView];
    [self setMainView:self.view];
    [self registerForKeyboardNotifications];
    
    viewAlamatBedaFrame = _viewAlamatBeda.frame;
    constraintHideViewAlamatBeda = [NSLayoutConstraint constraintWithItem:self.viewAlamatBeda attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:0];
    
    isAlamatBeda = 0;
    
    [self refreshData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [encryptor setUserDefaultsObject:ScreenName forKey:ONBOARDING_PROGRESS_STEP_KEY];
}

- (void) refreshData {
    [self showLoading];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    
    // setup dropdown
    dropdownProvinsi = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownProvinsi.displayedStringKey = @"bsmname";
    [dropdownProvinsi getReferenceDataFromEndPoint:@"api/master/province/listAll" withKeysToStore:[[NSArray alloc] initWithObjects:@"bsmid", @"bsmname", @"fullcode", nil] andSortUsingKey:@"bsmname" andDispatchGroup:referenceGroup];
    dropdownProvinsi.delegate = self;
    
    dropdownKabupatenKota = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKabupatenKota.displayedStringKey = @"bsmname";
    dropdownKabupatenKota.delegate = self;
    
    dropdownKecamatan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKecamatan.displayedStringKey = @"nama";
    dropdownKecamatan.delegate = self;
    
    dropdownKelurahan = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKelurahan.displayedStringKey = @"nama";
    dropdownKelurahan.delegate = self;
    
    dropdownProvinsiDiff = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownProvinsiDiff.displayedStringKey = @"bsmname";
    [dropdownProvinsiDiff getReferenceDataFromEndPoint:@"api/master/province/listAll" withKeysToStore:[[NSArray alloc] initWithObjects:@"bsmid", @"bsmname", @"fullcode", nil] andSortUsingKey:@"bsmname" andDispatchGroup:referenceGroup];
    dropdownProvinsiDiff.delegate = self;
    
    dropdownKabupatenKotaDiff = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKabupatenKotaDiff.displayedStringKey = @"bsmname";
    dropdownKabupatenKotaDiff.delegate = self;
    
    dropdownKecamatanDiff = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKecamatanDiff.displayedStringKey = @"nama";
    dropdownKecamatanDiff.delegate = self;
    
    dropdownKelurahanDiff = [sb instantiateViewControllerWithIdentifier:@"DropdownSearchScreen"];
    dropdownKelurahanDiff.displayedStringKey = @"nama";
    dropdownKelurahanDiff.delegate = self;
    
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if(self->refreshControl)
            [self->refreshControl endRefreshing];
        
        if(self->dropdownProvinsi.callApiSucess)
        {
            [self->dropdownProvinsi.tableView reloadData];
            self->dropdownProvinsi.displayedItems = self->dropdownProvinsi.allItems;
        }
        else
        {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_PROVINCE", @"") sender:self];
        }
    });
    
    formInputKTP = [[NSDictionary alloc] initWithObjectsAndKeys:
                         _fieldAlamat, ONBOARDING_DUKCAPIL_ALAMAT,
                         _fieldKodePos, ONBOARDING_DUKCAPIL_ZIP,
                         _fieldRT, ONBOARDING_DUKCAPIL_NO_RT,
                         _fieldRW, ONBOARDING_DUKCAPIL_NO_RW,
                         nil];
    formInputDomisili = [[NSDictionary alloc] initWithObjectsAndKeys:
                         _fieldAlamatDiff, ONBOARDING_INFORMASI_PRIBADI_ALAMAT_KEY,
                         _fieldKodePosDiff, ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY,
                         _fieldRTDiff, ONBOARDING_INFORMASI_PRIBADI_RT_KEY,
                         _fieldRWDiff, ONBOARDING_INFORMASI_PRIBADI_RW_KEY,
                         nil];
    
    isAlamatBeda = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY] integerValue];
    [self toggleSubViewDisplay:isAlamatBeda];
    
    [self setDropDownData];
    [self setTextFieldData:formInputKTP];
    if (isAlamatBeda) {
        [self setTextFieldData:formInputDomisili];
    }
}

-(void)doneWithNumberPad{
    if([self.fieldKodePos isFirstResponder])
    {
        [self.fieldKodePos resignFirstResponder];
    }
    else if ([self.fieldKodePosDiff isFirstResponder])
    {
        [self.fieldKodePosDiff resignFirstResponder];
    }
    else if ([self.fieldRT isFirstResponder])
    {
        [self.fieldRT resignFirstResponder];
    }
    else if ([self.fieldRTDiff isFirstResponder])
    {
        [self.fieldRTDiff resignFirstResponder];
    }
    else if ([self.fieldRW isFirstResponder])
    {
        [self.fieldRW resignFirstResponder];
    }
    else if ([self.fieldRWDiff isFirstResponder])
    {
        [self.fieldRWDiff resignFirstResponder];
    }
}

-(void)cancelNumberPad{
    if([self.fieldKodePos isFirstResponder])
    {
        [self.fieldKodePos resignFirstResponder];
        self.fieldKodePos.text = @"";
    }
    else if ([self.fieldKodePosDiff isFirstResponder])
    {
        [self.fieldKodePosDiff resignFirstResponder];
        self.fieldKodePosDiff.text = @"";
    }
    else if ([self.fieldRT isFirstResponder])
    {
        [self.fieldRT resignFirstResponder];
        self.fieldRT.text = @"";
    }
    else if ([self.fieldRTDiff isFirstResponder])
    {
        [self.fieldRTDiff resignFirstResponder];
        self.fieldRTDiff.text = @"";
    }
    else if ([self.fieldRW isFirstResponder])
    {
        [self.fieldRW resignFirstResponder];
        self.fieldRW.text = @"";
    }
    else if ([self.fieldRWDiff isFirstResponder])
    {
        [self.fieldRWDiff resignFirstResponder];
        self.fieldRWDiff.text = @"";
    }
}

-(void)textToolbarDone {
    if ([self.fieldProvinsi isFirstResponder])
    {
        [self.fieldProvinsi resignFirstResponder];
    }
    else if ([self.fieldProvinsiDiff isFirstResponder])
    {
        [self.fieldProvinsiDiff resignFirstResponder];
    }
    else if ([self.fieldKota isFirstResponder])
    {
        [self.fieldKota resignFirstResponder];
    }
    else if ([self.fieldKotaDiff isFirstResponder])
    {
        [self.fieldKotaDiff resignFirstResponder];
    }
    else if ([self.fieldKecamatan isFirstResponder])
    {
        [self.fieldKecamatan resignFirstResponder];
    }
    else if ([self.fieldKecamatanDiff isFirstResponder])
    {
        [self.fieldKecamatanDiff resignFirstResponder];
    }
    else if ([self.fieldKelurahan isFirstResponder])
    {
        [self.fieldKelurahan resignFirstResponder];
    }
    else if ([self.fieldKelurahanDiff isFirstResponder])
    {
        [self.fieldKelurahanDiff resignFirstResponder];
    }
    else if ([self.fieldAlamat isFirstResponder])
    {
        [self.fieldAlamat resignFirstResponder];
    }
    else if ([self.fieldAlamatDiff isFirstResponder])
    {
        [self.fieldAlamatDiff resignFirstResponder];
    }
}

- (void)dropdownSearch:(id)uiDropdownSearch didSelectRowWithData:(NSDictionary *)data
{
    if (uiDropdownSearch == dropdownProvinsi) {
        [self.fieldProvinsi setText:[data objectForKey:@"bsmname"]];
        [self.fieldKota setText:@""];
        [self.fieldKecamatan setText:@""];
        [self.fieldKelurahan setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/city/listByD1?fullcode=%@", [dropdownProvinsi.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKabupatenKota getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"bsmid", @"bsmname", @"fullcode", nil] andSortUsingKey:@"bsmname" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKabupatenKota.callApiSucess)
            {
                self->dropdownKabupatenKota.displayedItems = self->dropdownKabupatenKota.allItems;
                [self->dropdownKabupatenKota.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_CITY", @"") sender:self];
            }
        });
        [self.fieldKota setEnabled:TRUE];
    }
    else if (uiDropdownSearch == dropdownKabupatenKota)
    {
        [self.fieldKota setText:[data objectForKey:@"bsmname"]];
        [self.fieldKecamatan setText:@""];
        [self.fieldKelurahan setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/district/listByD2?fullcode=%@", [dropdownKabupatenKota.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKecamatan getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"dati3id", @"nama", @"fullcode", nil] andSortUsingKey:@"nama" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKecamatan.callApiSucess)
            {
                self->dropdownKecamatan.displayedItems = self->dropdownKecamatan.allItems;
                [self->dropdownKecamatan.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_DISTRICT", @"") sender:self];
            }
        });
        [self.fieldKecamatan setEnabled:TRUE];
    }
    else if (uiDropdownSearch == dropdownKecamatan)
    {
        [self.fieldKecamatan setText:[data objectForKey:dropdownKecamatan.displayedStringKey]];
        [self.fieldKelurahan setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/village/listByD3?fullcode=%@", [dropdownKecamatan.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKelurahan getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"dati4id", @"nama", @"fullcode", nil] andSortUsingKey:@"nama" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKelurahan.callApiSucess)
            {
                self->dropdownKelurahan.displayedItems = self->dropdownKelurahan.allItems;
                [self->dropdownKelurahan.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_VILLAGE", @"") sender:self];
            }
        });
        [self.fieldKelurahan setEnabled:TRUE];
    }
    else if (uiDropdownSearch == dropdownKelurahan)
    {
        [self.fieldKelurahan setText:[data objectForKey:dropdownKelurahan.displayedStringKey]];
    }
    else if (uiDropdownSearch == dropdownProvinsiDiff) {
        [self.fieldProvinsiDiff setText:[data objectForKey:@"bsmname"]];
        [self.fieldKotaDiff setText:@""];
        [self.fieldKecamatanDiff setText:@""];
        [self.fieldKelurahanDiff setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/city/listByD1?fullcode=%@", [dropdownProvinsiDiff.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKabupatenKotaDiff getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"bsmid", @"bsmname", @"fullcode", nil] andSortUsingKey:@"bsmname" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKabupatenKotaDiff.callApiSucess)
            {
                self->dropdownKabupatenKotaDiff.displayedItems = self->dropdownKabupatenKotaDiff.allItems;
                [self->dropdownKabupatenKotaDiff.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_CITY", @"") sender:self];
            }
        });
        [self.fieldKotaDiff setEnabled:TRUE];
    }
    else if (uiDropdownSearch == dropdownKabupatenKotaDiff)
    {
        [self.fieldKotaDiff setText:[data objectForKey:@"bsmname"]];
        [self.fieldKecamatanDiff setText:@""];
        [self.fieldKelurahanDiff setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/district/listByD2?fullcode=%@", [dropdownKabupatenKotaDiff.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKecamatanDiff getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"dati3id", @"nama", @"fullcode", nil] andSortUsingKey:@"nama" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKecamatanDiff.callApiSucess)
            {
                self->dropdownKecamatanDiff.displayedItems = self->dropdownKecamatanDiff.allItems;
                [self->dropdownKecamatanDiff.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_DISTRICT", @"") sender:self];
            }
        });
        [self.fieldKecamatanDiff setEnabled:TRUE];
    }
    else if (uiDropdownSearch == dropdownKecamatanDiff)
    {
        [self.fieldKecamatanDiff setText:[data objectForKey:dropdownKecamatanDiff.displayedStringKey]];
        [self.fieldKelurahanDiff setText:@""];
        
        NSString *endPoint = [NSString stringWithFormat:@"api/master/village/listByD3?fullcode=%@", [dropdownKecamatanDiff.selectedData objectForKey:@"fullcode"]];
        [self showLoading];
        [dropdownKelurahanDiff getReferenceDataFromEndPoint:endPoint withKeysToStore:[[NSArray alloc] initWithObjects:@"dati4id", @"nama", @"fullcode", nil] andSortUsingKey:@"nama" andDispatchGroup:referenceGroup];
        dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            
            if(self->dropdownKelurahanDiff.callApiSucess)
            {
                self->dropdownKelurahanDiff.displayedItems = self->dropdownKelurahanDiff.allItems;
                [self->dropdownKelurahanDiff.tableView reloadData];
            }
            else
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"FAILED_FETCH_VILLAGE", @"") sender:self];
            }
        });
        [self.fieldKelurahanDiff setEnabled:TRUE];
    }
    else if (uiDropdownSearch == dropdownKelurahanDiff)
    {
        [self.fieldKelurahanDiff setText:[data objectForKey:dropdownKelurahanDiff.displayedStringKey]];
    }
}

- (void) setDropDownData
{
    NSString *provinsiCode = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_PROP];
    NSString *kotaCode = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_KAB];
    NSString *kecamatanCode = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_KEC];
    NSString *kelurahanCode = [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NO_KEL];
    if (provinsiCode == nil) {
        provinsiCode = @"";
        kotaCode = @"";
        kecamatanCode = @"";
        kelurahanCode = @"";
    }
    
    NSDictionary *provinsi = [[NSDictionary alloc] initWithObjectsAndKeys:
                              [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_ID_PROV], @"bsmid",
                              [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_PROP_NAME], @"bsmname",
                              provinsiCode, @"fullcode",
                              nil];
    NSDictionary *kabkota = [[NSDictionary alloc] initWithObjectsAndKeys:
                             [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_ID_KAB], @"bsmid",
                             [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_KAB_NAME], @"bsmname",
                             [NSString stringWithFormat:@"%@.%@", provinsiCode, kotaCode], @"fullcode",
                             nil];
    NSDictionary *kecamatan = [[NSDictionary alloc] initWithObjectsAndKeys:
                               [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_KEC_NAME], dropdownKecamatan.displayedStringKey,
                               [NSString stringWithFormat:@"%@.%@.%@", provinsiCode, kotaCode, kecamatanCode], @"fullcode",
                         nil];
    NSDictionary *kelurahan = [[NSDictionary alloc] initWithObjectsAndKeys:
                               [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_KEL_NAME], dropdownKelurahan.displayedStringKey,
                               [NSString stringWithFormat:@"%@.%@.%@.%@", provinsiCode, kotaCode, kecamatanCode, kelurahanCode], @"fullcode",
                         nil];
    
    dropdownProvinsi.selectedData = provinsi;
    if([dropdownProvinsi.selectedData isKindOfClass:NSDictionary.class])
        _fieldProvinsi.text = [dropdownProvinsi.selectedData objectForKey:dropdownProvinsi.displayedStringKey];
    
    dropdownKabupatenKota.selectedData = kabkota;
    if([dropdownKabupatenKota.selectedData isKindOfClass:NSDictionary.class])
        _fieldKota.text = [dropdownKabupatenKota.selectedData objectForKey:dropdownKabupatenKota.displayedStringKey];
    
    dropdownKecamatan.selectedData = kecamatan;
    if([dropdownKecamatan.selectedData isKindOfClass:NSDictionary.class])
        _fieldKecamatan.text = [dropdownKecamatan.selectedData objectForKey:dropdownKecamatan.displayedStringKey];
    
    dropdownKelurahan.selectedData = kelurahan;
    if([dropdownKelurahan.selectedData isKindOfClass:NSDictionary.class])
        _fieldKelurahan.text = [dropdownKelurahan.selectedData objectForKey:dropdownKelurahan.displayedStringKey];
    
    if (isAlamatBeda == 1) {
        dropdownProvinsiDiff.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY];
        if([dropdownProvinsiDiff.selectedData isKindOfClass:NSDictionary.class])
            _fieldProvinsiDiff.text = [dropdownProvinsiDiff.selectedData objectForKey:dropdownProvinsiDiff.displayedStringKey];
        
        dropdownKabupatenKotaDiff.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY];
        if([dropdownKabupatenKotaDiff.selectedData isKindOfClass:NSDictionary.class])
            _fieldKotaDiff.text = [dropdownKabupatenKotaDiff.selectedData objectForKey:dropdownKabupatenKotaDiff.displayedStringKey];
        
        dropdownKecamatanDiff.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KECAMATAN_KEY];
        if([dropdownKecamatanDiff.selectedData isKindOfClass:NSDictionary.class])
            _fieldKecamatanDiff.text = [dropdownKecamatanDiff.selectedData objectForKey:dropdownKecamatanDiff.displayedStringKey];
        
        dropdownKelurahanDiff.selectedData = [encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_KELURAHAN_KEY];
        if([dropdownKelurahanDiff.selectedData isKindOfClass:NSDictionary.class])
            _fieldKelurahanDiff.text = [dropdownKelurahanDiff.selectedData objectForKey:dropdownKelurahanDiff.displayedStringKey];
    }
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self inputHasError:NO textField:textField];
    
    if(textField == _fieldProvinsi)
    {
        [self presentViewController:dropdownProvinsi animated:YES completion:nil];
        [self.fieldKota setEnabled:FALSE];
        [self.fieldKecamatan setEnabled:FALSE];
        [self.fieldKelurahan setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKota)
    {
        [self presentViewController:dropdownKabupatenKota animated:YES completion:nil];
        [self.fieldKelurahan setEnabled:FALSE];
        [self.fieldKecamatan setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKecamatan)
    {
        [self presentViewController:dropdownKecamatan animated:YES completion:nil];
        [self.fieldKelurahan setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKelurahan)
    {
        [self presentViewController:dropdownKelurahan animated:YES completion:nil];
        return NO;
    }
    
    else if(textField == _fieldProvinsiDiff)
    {
        [self presentViewController:dropdownProvinsiDiff animated:YES completion:nil];
        [self.fieldKotaDiff setEnabled:FALSE];
        [self.fieldKecamatanDiff setEnabled:FALSE];
        [self.fieldKelurahanDiff setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKotaDiff)
    {
        [self presentViewController:dropdownKabupatenKotaDiff animated:YES completion:nil];
        [self.fieldKelurahanDiff setEnabled:FALSE];
        [self.fieldKecamatanDiff setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKecamatanDiff)
    {
        [self presentViewController:dropdownKecamatanDiff animated:YES completion:nil];
        [self.fieldKelurahanDiff setEnabled:FALSE];
        return NO;
    }
    
    else if([textField isEnabled] && textField == _fieldKelurahanDiff)
    {
        [self presentViewController:dropdownKelurahanDiff animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setActiveField:textField];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if((textField == _fieldAlamat || textField == _fieldAlamatDiff) && textField.text.length >= 35 && ![string isEqualToString:@""]) return NO;
    else if((textField == _fieldKodePos || textField == _fieldKodePosDiff) && textField.text.length >= 5 && ![string isEqualToString:@""]) return NO;
    else if((textField == _fieldRT || textField == _fieldRTDiff) && textField.text.length >= 3 && ![string isEqualToString:@""]) return NO;
    else if((textField == _fieldRW || textField == _fieldRWDiff) && textField.text.length >= 3 && ![string isEqualToString:@""]) return NO;
    
    return YES;
}

-(void)toggleSubViewDisplay:(BOOL)isShowing
{
    if(isShowing) {
        self.checkAlamatBeda.selected = true;
        [_viewAlamatBeda setFrame:viewAlamatBedaFrame];
        [_viewAlamatBeda setHidden:NO];
        _constraintTopViewAlamatBeda.constant = 16;
        _constraintTopViewAlamatBeda.priority = 999;
        _constraintTopViewAlamatBeda.active = YES;
        [self.view removeConstraint:constraintHideViewAlamatBeda];
        isAlamatBeda = 1;
    } else {
        self.checkAlamatBeda.selected = false;
        CGRect newViewAlamatBedaFrame = _viewAlamatBeda.frame;
        newViewAlamatBedaFrame.size.height = 0;
        [_viewAlamatBeda setFrame:newViewAlamatBedaFrame];
        [_viewAlamatBeda setHidden:YES];
        _constraintTopViewAlamatBeda.constant = 0;
        _constraintTopViewAlamatBeda.priority = 500;
        _constraintTopViewAlamatBeda.active = YES;
        [self.view addConstraint:constraintHideViewAlamatBeda];
        isAlamatBeda = 0;
    }
    
    [self.view layoutIfNeeded];
}

- (void) saveData
{
    NSArray *kotaArrayString = [[dropdownKabupatenKota.selectedData objectForKey:@"fullcode"] componentsSeparatedByString:@"."];
    NSArray *kecamatanArrayString = [[dropdownKecamatan.selectedData objectForKey:@"fullcode"] componentsSeparatedByString:@"."];
    NSArray *kelurahanArrayString = [[dropdownKelurahan.selectedData objectForKey:@"fullcode"] componentsSeparatedByString:@"."];
    
    if ([kotaArrayString count] < 2) {
        [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"CITY_NOT_VALID", @"") sender:self];
        return;
    } else if ([kecamatanArrayString count] < 3) {
        [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"DISTRICT_NOT_VALID", @"") sender:self];
        return;
    } else if ([kelurahanArrayString count] < 4) {
        [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"VILLAGE_NOT_VALID", @"") sender:self];
        return;
    }
    
    [encryptor setUserDefaultsObject:@"ID" forKey:ONBOARDING_DUKCAPIL_WARGANEGARA];
    [encryptor setUserDefaultsObject:[dropdownProvinsi.selectedData objectForKey:@"bsmid"] forKey:ONBOARDING_DUKCAPIL_ID_PROV];
    [encryptor setUserDefaultsObject:[dropdownProvinsi.selectedData objectForKey:@"fullcode"] forKey:ONBOARDING_DUKCAPIL_NO_PROP];
    [encryptor setUserDefaultsObject:[dropdownProvinsi.selectedData objectForKey:@"bsmname"] forKey:ONBOARDING_DUKCAPIL_PROP_NAME];
    [encryptor setUserDefaultsObject:[dropdownKabupatenKota.selectedData objectForKey:@"bsmid"] forKey:ONBOARDING_DUKCAPIL_ID_KAB];
    [encryptor setUserDefaultsObject:kotaArrayString[1] forKey:ONBOARDING_DUKCAPIL_NO_KAB];
    [encryptor setUserDefaultsObject:[dropdownKabupatenKota.selectedData objectForKey:@"bsmname"] forKey:ONBOARDING_DUKCAPIL_KAB_NAME];
    [encryptor setUserDefaultsObject:kecamatanArrayString[2] forKey:ONBOARDING_DUKCAPIL_NO_KEC];
    [encryptor setUserDefaultsObject:[dropdownKecamatan.selectedData objectForKey:@"nama"] forKey:ONBOARDING_DUKCAPIL_KEC_NAME];
    [encryptor setUserDefaultsObject:kelurahanArrayString[3] forKey:ONBOARDING_DUKCAPIL_NO_KEL];
    [encryptor setUserDefaultsObject:[dropdownKelurahan.selectedData objectForKey:@"nama"] forKey:ONBOARDING_DUKCAPIL_KEL_NAME];
    [encryptor setUserDefaultsObject:_fieldAlamat.text forKey:ONBOARDING_DUKCAPIL_ALAMAT];
    [encryptor setUserDefaultsObject:_fieldRT.text forKey:ONBOARDING_DUKCAPIL_NO_RT];
    [encryptor setUserDefaultsObject:_fieldRW.text forKey:ONBOARDING_DUKCAPIL_NO_RW];
    [encryptor setUserDefaultsObject:_fieldKodePos.text forKey:ONBOARDING_DUKCAPIL_ZIP];
    [encryptor setUserDefaultsObject:[NSNumber numberWithInteger:isAlamatBeda] forKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_BEDA_KEY];

    [self setDomilisiWithData:isAlamatBeda];
}

- (void)setDomilisiWithData:(NSInteger) isAlamatBeda
{
    [encryptor setUserDefaultsObject:isAlamatBeda ? dropdownProvinsiDiff.selectedData : dropdownProvinsi.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_PROPINSI_KEY];
    [encryptor setUserDefaultsObject:isAlamatBeda ? dropdownKabupatenKotaDiff.selectedData : dropdownKabupatenKota.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_KABUPATEN_KOTA_KEY];
    [encryptor setUserDefaultsObject:isAlamatBeda ? dropdownKecamatanDiff.selectedData : dropdownKecamatan.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_KECAMATAN_KEY];
    [encryptor setUserDefaultsObject:isAlamatBeda ? dropdownKelurahanDiff.selectedData : dropdownKelurahan.selectedData forKey:ONBOARDING_INFORMASI_PRIBADI_KELURAHAN_KEY];
    [encryptor setUserDefaultsObject:isAlamatBeda ? _fieldRTDiff.text : _fieldRT.text forKey:ONBOARDING_INFORMASI_PRIBADI_RT_KEY];
    [encryptor setUserDefaultsObject:isAlamatBeda ? _fieldRWDiff.text : _fieldRW.text forKey:ONBOARDING_INFORMASI_PRIBADI_RW_KEY];
    [encryptor setUserDefaultsObject:isAlamatBeda ? _fieldKodePosDiff.text : _fieldKodePos.text forKey:ONBOARDING_INFORMASI_PRIBADI_ZIP_KEY];
    [encryptor setUserDefaultsObject:isAlamatBeda ? _fieldAlamatDiff.text : _fieldAlamat.text forKey:ONBOARDING_INFORMASI_PRIBADI_ALAMAT_KEY];
}

- (BOOL)checkInput
{
    BOOL isValid = TRUE;
    if([_fieldProvinsi.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"PROVINCE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldProvinsi];
        [self inputHasError:YES textField:_fieldProvinsi];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldProvinsi];
    
    if([_fieldKota.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"CITY_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKota];
        [self inputHasError:YES textField:_fieldKota];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldKota];
    
    if([_fieldKecamatan.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"DISTRICT_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKecamatan];
        [self inputHasError:YES textField:_fieldKecamatan];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldKecamatan];
    
    if([_fieldKelurahan.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"VILLAGE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKelurahan];
        [self inputHasError:YES textField:_fieldKelurahan];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldKelurahan];
    
    if([_fieldAlamat.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"ADDRESS_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldAlamat];
        [self inputHasError:YES textField:_fieldAlamat];
        isValid = FALSE;
    }
    else if(self.fieldAlamat.text.length > 35)
    {
        [self showErrorLabel:NSLocalizedString(@"ADDRESS_NOT_VALID", @"") withUiTextfield:_fieldAlamat];
        [self inputHasError:YES textField:_fieldAlamat];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldAlamat];
    
    if([_fieldRW.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"RW_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldRW];
        [self inputHasError:YES textField:_fieldRW];
        isValid = FALSE;
    }
    else if(_fieldRW.text.length > 3) {
        [self showErrorLabel:NSLocalizedString(@"RW_NOT_VALID", @"") withUiTextfield:_fieldRW];
        [self inputHasError:YES textField:_fieldRW];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldRW];
    
    if([_fieldRT.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"RT_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldRT];
        [self inputHasError:YES textField:_fieldRT];
        isValid = FALSE;
    }
    else if(_fieldRT.text.length > 3) {
        [self showErrorLabel:NSLocalizedString(@"RT_NOT_VALID", @"") withUiTextfield:_fieldRT];
        [self inputHasError:YES textField:_fieldRT];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldRT];
    
    if([_fieldKodePos.text isEqualToString:@""])
    {
        [self showErrorLabel:NSLocalizedString(@"POSTCODE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKodePos];
        [self inputHasError:YES textField:_fieldKodePos];
        isValid = FALSE;
    }
    else if(_fieldKodePos.text.length != 5) {
        [self showErrorLabel:NSLocalizedString(@"POSTCODE_NOT_VALID", @"") withUiTextfield:_fieldKodePos];
        [self inputHasError:YES textField:_fieldKodePos];
        isValid = FALSE;
    }
    else [self inputHasError:NO textField:_fieldKodePos];
    
    if (isAlamatBeda == 1) {
        if([_fieldProvinsiDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"PROVINCE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldProvinsiDiff];
            [self inputHasError:YES textField:_fieldProvinsiDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldProvinsiDiff];
        
        if([_fieldKotaDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"CITY_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKotaDiff];
            [self inputHasError:YES textField:_fieldKotaDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldKotaDiff];
        
        if([_fieldKecamatanDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"DISTRICT_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKecamatanDiff];
            [self inputHasError:YES textField:_fieldKecamatanDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldKecamatanDiff];
        
        if([_fieldKelurahanDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"VILLAGE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKelurahanDiff];
            [self inputHasError:YES textField:_fieldKelurahanDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldKelurahanDiff];
        
        if([_fieldAlamatDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"ADDRESS_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldAlamatDiff];
            [self inputHasError:YES textField:_fieldAlamatDiff];
            isValid = FALSE;
        }
        else if(self.fieldAlamatDiff.text.length > 35)
        {
            [self showErrorLabel:NSLocalizedString(@"ADDRESS_NOT_VALID", @"") withUiTextfield:_fieldAlamatDiff];
            [self inputHasError:YES textField:_fieldAlamatDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldAlamatDiff];
        
        if([_fieldRWDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"RW_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldRWDiff];
            [self inputHasError:YES textField:_fieldRWDiff];
            isValid = FALSE;
        }
        else if(_fieldRWDiff.text.length > 3) {
            [self showErrorLabel:NSLocalizedString(@"RW_NOT_VALID", @"") withUiTextfield:_fieldRWDiff];
            [self inputHasError:YES textField:_fieldRWDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldRWDiff];
        
        if([_fieldRTDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"RT_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldRTDiff];
            [self inputHasError:YES textField:_fieldRTDiff];
            isValid = FALSE;
        }
        else if(_fieldRTDiff.text.length > 3) {
            [self showErrorLabel:NSLocalizedString(@"RT_NOT_VALID", @"") withUiTextfield:_fieldRTDiff];
            [self inputHasError:YES textField:_fieldRTDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldRTDiff];
        
        if([_fieldKodePosDiff.text isEqualToString:@""])
        {
            [self showErrorLabel:NSLocalizedString(@"POSTCODE_CANNOT_BE_EMPTY", @"") withUiTextfield:_fieldKodePosDiff];
            [self inputHasError:YES textField:_fieldKodePosDiff];
            isValid = FALSE;
        }
        else if(_fieldKodePosDiff.text.length != 5) {
            [self showErrorLabel:NSLocalizedString(@"POSTCODE_NOT_VALID", @"") withUiTextfield:_fieldKodePosDiff];
            [self inputHasError:YES textField:_fieldKodePosDiff];
            isValid = FALSE;
        }
        else [self inputHasError:NO textField:_fieldKodePosDiff];
    }
    
    return isValid;
}

- (IBAction)hitBtnNext:(id)sender {
    if([self checkInput])
    {
        // save data
        [self saveData];
        
        // navigate next
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
        InformasiStatusController *informasiStatusController = [storyboard instantiateViewControllerWithIdentifier:@"InformasiPribadiThirdScreen"];
        [self presentViewController:informasiStatusController animated:YES completion:nil];
    }
}

- (IBAction)hitBtnCheckAlamatBeda:(id)sender {
    if(!self.checkAlamatBeda.selected) [self toggleSubViewDisplay:YES];
    else [self toggleSubViewDisplay:NO];
}
@end
