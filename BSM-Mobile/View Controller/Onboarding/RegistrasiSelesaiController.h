//
//  RegistrasiSelesaiController.h
//  BSM-Mobile
//
//  Created by ARS on 25/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingTemplate01ViewController.h"
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegistrasiSelesaiController : OnboardingRootController <OnboardingTemplate01ViewControllerDelegate>
{
    CGFloat mainButtonHeight;
    CGFloat mainButtonMargin;
}

@property OnboardingTemplate01ViewController *parentController;

@end

NS_ASSUME_NONNULL_END
