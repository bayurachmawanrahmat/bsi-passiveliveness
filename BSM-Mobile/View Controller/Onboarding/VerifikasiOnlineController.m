//
//  VerifikasiOnlineController.m
//  BSM-Mobile
//
//  Created by ARS on 17/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "VerifikasiOnlineController.h"
#import "Encryptor.h"
#import "CustomerOnboardingData.h"
#import "NSObject+FormInputCategory.h"
#import <OracleLive/OracleLive-Swift.h>
#import <sys/utsname.h>
#import "ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"
#import "WowzaController.h"
#import "AvailabilityViewController.h"

@interface VerifikasiOnlineController ()
{
    NSString *verifikasiOnlineMode;
    Encryptor *encryptor;
    BOOL requestIsSuccess;
    BOOL callIsEnded;
    NSString *cobMessage;
    BOOL isCobAvailable;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *checkBtnPria;
@property (weak, nonatomic) IBOutlet UIButton *checkBtnWanita;
@property (weak, nonatomic) IBOutlet UIView *contentVIdeoAssist;
@property (weak, nonatomic) IBOutlet UIButton *btnStartVideoAssist;
- (IBAction)btnBackDidHit:(id)sender;

@end

@implementation VerifikasiOnlineController

#pragma mark - UIViewController Delegates
- (void)viewDidLoad {
    [super viewDidLoad];
    
//    if (@available(iOS 13.0, *)) {
//        // self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
//        [self setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
//        [self.view setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
//    }
    
    // init global
    encryptor = [[Encryptor alloc] init];
    [encryptor setUserDefaultsObject:@"VerifikasiOnlineScreen" forKey:ONBOARDING_PROGRESS_STEP_KEY];
    
    self->callIsEnded = NO;
    
    // adjust view
    _contentView.layer.cornerRadius = 30.0f;
    
    self->verifikasiOnlineMode = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_JENIS_KYC_KEY];
    if ([self->verifikasiOnlineMode isEqualToString:@"VIDEOCALL"])
    {
        // for videocall
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OracleLive.Notifications.LiveEndedNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OracleLive.Notifications.LiveCancelNotification" object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"OracleLive.Notifications.LiveErrorNotification" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(liveEndedNotificationHandler) name:@"OracleLive.Notifications.LiveEndedNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(liveCanceledNotificationHandler) name:@"OracleLive.Notifications.LiveCanceledNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(liveErrorNotificationHandler) name:@"OracleLive.Notifications.LiveErrorNotification" object:nil];
        
        [self logging];
    }
    else {
        // for videoassist
        UIImage *iconChecked = [UIImage imageNamed:@"ic_checklist_filed@2x.png"];
        UIImage *iconUnchecked = [UIImage imageNamed:@"ic_checklist_off@2x.png"];

        [self->_contentVIdeoAssist setHidden:NO];
        [self->_btnStartVideoAssist setHidden:NO];
        [self->_checkBtnPria setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
        [self->_checkBtnPria setBackgroundImage:iconChecked forState:UIControlStateSelected];
        [self->_checkBtnWanita setBackgroundImage:iconUnchecked forState:UIControlStateNormal];
        [self->_checkBtnWanita setBackgroundImage:iconChecked forState:UIControlStateSelected];
        [self->_checkBtnPria addTarget:self action:@selector(checkBtnDidHit:) forControlEvents:UIControlEventTouchUpInside];
        [self->_checkBtnWanita addTarget:self action:@selector(checkBtnDidHit:) forControlEvents:UIControlEventTouchUpInside];
        [self->_btnStartVideoAssist addTarget:self action:@selector(startVideoAssist:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    #if (TARGET_IPHONE_SIMULATOR)
    [self showAlertErrorWithTitle:@"Info" andMessage:@"Oracle live experience can only run on real device" sender:self];
    #else
    if ([self->verifikasiOnlineMode isEqualToString:@"VIDEOCALL"]) {
        if (!self->callIsEnded) {
            [self checkCOBTime];
        }
        else {
            // Live Ended
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"PengajuanSelesaiScene"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
    else {
        
    }
    #endif
}

#pragma mark - Private methods
- (void)startVideoAssist:(id)sender {
    // for videoassist
    if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY])
    {
        dispatch_group_t dispatch = dispatch_group_create();
        CustomerOnboardingData *cod = [[CustomerOnboardingData alloc] init];
        [cod setDispatchGroup:dispatch];
        [cod generateNomorAntrian];
        dispatch_group_notify(dispatch, dispatch_get_main_queue(), ^{
            if(cod.requestIsSuccess && ![cod.nomorAntrian isEqualToString:@""])
            {
                if ([self->encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_FILES] == nil)
                {
                    [self showAlertErrorWithTitle:NSLocalizedString(@"SORRY", @"Sorry") andMessage:NSLocalizedString(@"ERR_VIDEO_ASSIST_NOT_AVAIL", @"ERR: Video assist not available") sender:self];
                }
                else
                {
                    WowzaController *controller = [[WowzaController alloc] init];
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }
            else
            {
                [self hideLoading];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ooops" message:NSLocalizedString(@"ERR_REGISTRATION_CODE", @"ERR: Registration code not found") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:actionOk];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        });
    }
    else {
        if ([self->encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEOASSIST_FILES] == nil)
        {
            [self showAlertErrorWithTitle:NSLocalizedString(@"SORRY", @"Sorry") andMessage:NSLocalizedString(@"ERR_VIDEO_ASSIST_NOT_AVAIL", @"ERR: Video assist not available") sender:self];
        }
        else
        {
            WowzaController *controller = [[WowzaController alloc] init];
            [self presentViewController:controller animated:YES completion:nil];
        }
    }
}

- (void) setupVideoCall {
    // for videocall
    [self showLoading];
    if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY])
    {
        dispatch_group_t dispatch = dispatch_group_create();
        CustomerOnboardingData *cod = [[CustomerOnboardingData alloc] init];
        [cod setDispatchGroup:dispatch];
        [cod generateNomorAntrian];
        dispatch_group_notify(dispatch, dispatch_get_main_queue(), ^{
            if(cod.requestIsSuccess && ![cod.nomorAntrian isEqualToString:@""])
                [self initOracleLive];
            else
            {
                [self hideLoading];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Ooops" message:NSLocalizedString(@"ERR_REGISTRATION_CODE", @"ERR: Registration code not found") preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:actionOk];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        });
    }
    else [self initOracleLive];
}

- (void) checkBtnDidHit:(UIButton *)sender
{
    [sender setSelected:YES];
    if(sender == _checkBtnPria)
    {
        [_checkBtnWanita setSelected:NO];
        [encryptor setUserDefaultsObject:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR_M forKey:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR];
    }
    else
    {
        [_checkBtnPria setSelected:NO];
        [encryptor setUserDefaultsObject:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR_F forKey:ONBOARDING_VIDEOASSIST_GENDER_OPERATOR];
    }
}

- (IBAction)btnBackDidHit:(id)sender {
    if([encryptor getUserDefaultsObjectForKey:ONBOARDING_RESTORE_DATA_KEY] != nil)
    {
        // GO TO MSM
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
        firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:firstVC animated:YES completion:^{
            PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
            [firstVC presentViewController:viewCont animated:YES completion:nil];
        }];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:ONBOARDING_NEED_CHECK_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // GO TO MSM
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
        firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:firstVC animated:YES completion:^{
            PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
            [firstVC presentViewController:viewCont animated:YES completion:nil];
        }];
    }
}

- (void) logging {
    NSString *nomorKtp = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY];
    NSString *nomorAntrian = [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY];
    NSString *userID = [NSString stringWithFormat:@"%@.%@@Onboarding", nomorKtp, nomorAntrian];
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    [postData setObject:userID forKey:@"userid"];
    [postData setObject:ONBOARDING_LX_CLIENT_TENNANT forKey:@"tenant"];
    [postData setObject:[Encryptor HexDecrypt:ONBOARDING_LX_CLIENT_ID] forKey:@"clientid"];
    [postData setObject:ONBOARDING_LX_ADDRESS forKey:@"addressurl"];
    [postData setObject:[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] forKey:@"email"];
    [postData setObject:[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY] forKey:@"phone"];
    [postData setObject:[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP] forKey:@"fullname"];
    [postData setObject:nomorKtp forKey:@"nik"];
    [postData setObject:nomorAntrian forKey:@"noticket"];
    
    NSString *deviceType = [self getDeviceType];
    [postData setObject:deviceType forKey:@"deviceType"];

    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    dispatch_group_t dispatchGroupInner = dispatch_group_create();
    [CustomerOnboardingData postToEndPoint:@"api/log/oracle-call" dispatchGroup:dispatchGroupInner postData:postData returnData:returnData];
    
    dispatch_group_notify(dispatchGroupInner, dispatch_get_main_queue(), ^{
        NSLog(@"VIDEO CALL LOGGING : %@", postData);
    });
}

#pragma mark - Oracle Live methods
- (void) initOracleLive {
    #if !(TARGET_IPHONE_SIMULATOR)
    NSString *clientID = [Encryptor HexDecrypt:ONBOARDING_LX_CLIENT_ID];
    NSString *clientS = [Encryptor HexDecrypt:ONBOARDING_LX_CLIENT_SECRET];
    NSURL *url = [NSURL URLWithString:@"https://live.oraclecloud.com/auth/apps/api/access-token?grant_type=client_credentials&state=0&scope=optional&nonce=36838"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", clientID, clientS];
    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest addValue:authValue forHTTPHeaderField:@"Authorization"];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:urlRequest
    completionHandler:^(NSData* _data, NSURLResponse* _response, NSError* _error) {
        if(!_error)
        {
            NSError *error = nil;
            NSDictionary *value  = [NSJSONSerialization JSONObjectWithData:_data options:kNilOptions error:&error];
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)_response;
            if(!error && [httpResponse statusCode] == 200)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([[NSThread currentThread] isMainThread]){
                        Controller.shared.service.authToken = [value objectForKey:@"access_token"];
                    }
                });
            }
        }
        
        [self hideLoading];
    }] resume];
    
    Controller.shared.settings.startVideoInFullScreen = YES;
    Controller.shared.settings.startVideoWithFrontCamera = YES;

    // UserID : nik.kode_reservasi@Onboarding
    NSString *userID = [NSString stringWithFormat:@"%@.%@@Onboarding", [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY], [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY]];
    [Controller.shared.service setUserID: [userID copy]];
    [Controller.shared.service setTenantID:ONBOARDING_LX_CLIENT_TENNANT];
    [Controller.shared.service setClientID:clientID];
    [Controller.shared.service setAddress:ONBOARDING_LX_ADDRESS];

    NSString *lang = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    [Controller.shared.service setLanguage: ([lang isEqualToString:@"en"] ? @"en" : @"in")];
    // [Controller.shared.service setLanguage:@"en"];
    [Controller.shared.service setSkipRecordingPermissionRequest:NO];
    
    [Controller.shared.contextAttributes setValue: [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP] forKey: @"fullName"];
    [Controller.shared.contextAttributes setValue: [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] forKey: @"email"];
    [Controller.shared.contextAttributes setValue: [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY] forKey: @"phone"];
    [Controller.shared.contextAttributes setValue: [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASI_PRIBADI_NOMOR_KTP_KEY] forKey: @"NIK"];
    [Controller.shared.contextAttributes setValue: [self->encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY] forKey: @"ticketNo"];
    
//    if (@available(iOS 13.0, *)) {
//        [Controller.shared.mainView setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
//        [Controller.shared.settings.mainView setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
//    }
    
    [Controller.shared addComponentWithViewController:nil forceUpdate:NO autoUpdated:NO];
    [Controller.shared addComponentWithViewController:self forceUpdate:NO autoUpdated:NO];
    #endif
}

#pragma mark - Notification handler

- (void) liveErrorNotificationHandler
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"PilihUlangMetodeScreen"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) liveCanceledNotificationHandler
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"PilihUlangMetodeScreen"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) liveEndedNotificationHandler
{
    self->callIsEnded = YES;
//    Not working on ORACLE LX SDK 20.5.1
//    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
//    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"PengajuanSelesaiScene"];
//    [self presentViewController:vc animated:YES completion:nil];
}

- (NSString *)getDeviceType {
    @try {
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *deviceCode = [NSString stringWithCString:systemInfo.machine
                                                  encoding:NSUTF8StringEncoding];
        static NSDictionary* deviceNamesByCode = nil;
        if (!deviceNamesByCode) {
            deviceNamesByCode = @{@"i386"      : @"Simulator",
                                  @"x86_64"    : @"Simulator",
                                  @"iPod1,1"   : @"iPod Touch",        // (Original)
                                  @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                                  @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                                  @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                                  @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                                  @"iPhone1,1" : @"iPhone",            // (Original)
                                  @"iPhone1,2" : @"iPhone",            // (3G)
                                  @"iPhone2,1" : @"iPhone",            // (3GS)
                                  @"iPad1,1"   : @"iPad",              // (Original)
                                  @"iPad2,1"   : @"iPad 2",            //
                                  @"iPad3,1"   : @"iPad",              // (3rd Generation)
                                  @"iPhone3,1" : @"iPhone 4",          // (GSM)
                                  @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                                  @"iPhone4,1" : @"iPhone 4S",         //
                                  @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                                  @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                                  @"iPad3,4"   : @"iPad",              // (4th Generation)
                                  @"iPad2,5"   : @"iPad Mini",         // (Original)
                                  @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                                  @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                                  @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                                  @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                                  @"iPhone7,1" : @"iPhone 6 Plus",     //
                                  @"iPhone7,2" : @"iPhone 6",          //
                                  @"iPhone8,1" : @"iPhone 6S",         //
                                  @"iPhone8,2" : @"iPhone 6S Plus",    //
                                  @"iPhone8,4" : @"iPhone SE",         //
                                  @"iPhone9,1" : @"iPhone 7",          //
                                  @"iPhone9,3" : @"iPhone 7",          //
                                  @"iPhone9,2" : @"iPhone 7 Plus",     //
                                  @"iPhone9,4" : @"iPhone 7 Plus",     //
                                  @"iPhone10,1": @"iPhone 8",          // CDMA
                                  @"iPhone10,4": @"iPhone 8",          // GSM
                                  @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                                  @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                                  @"iPhone10,3": @"iPhone X",          // CDMA
                                  @"iPhone10,6": @"iPhone X",          // GSM
                                  @"iPhone11,2": @"iPhone XS",         //
                                  @"iPhone11,4": @"iPhone XS Max",     //
                                  @"iPhone11,6": @"iPhone XS Max",     // China
                                  @"iPhone11,8": @"iPhone XR",         //
                                  @"iPhone12,1": @"iPhone 11",         //
                                  @"iPhone12,3": @"iPhone 11 Pro",     //
                                  @"iPhone12,5": @"iPhone 11 Pro Max", //

                                  @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                                  @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                                  @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                                  @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                                  @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                                  @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                                  @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                                  @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                                  @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                                  };
        }
        
        return [deviceNamesByCode objectForKey:deviceCode];
    } @catch (NSException *exception) {
        return @"iPhone";
    }
}

- (void)checkCOBTime {
    dispatch_group_t referenceGroup = dispatch_group_create();
    
    // CHECK COB TIME
    NSMutableDictionary *checkCobResult = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/setting/isValidVideoCall" dispatchGroup:referenceGroup returnData:checkCobResult];
    
    self->isCobAvailable = YES;
    self->cobMessage = @"";
    
    [self showLoading];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        
        if([[checkCobResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]){
            NSDictionary *data = [checkCobResult objectForKey:@"data"];
            if ([[data objectForKey:@"is_video_call_time"] isEqual:[NSNumber numberWithBool:YES]]) {
                self->isCobAvailable = YES;
                self->cobMessage = @"";
                [self setupVideoCall];
            } else {
                self->isCobAvailable = NO;
                self->cobMessage = @"";
                
                NSString *max = [data objectForKey:@"max"];
                NSString *min = [data objectForKey:@"min"];
                
                self->cobMessage = [NSString stringWithFormat:NSLocalizedString(@"ONBOARDING_MAINTENACE_TEXT", @"ONBOARDING_MAINTENACE_TEXT"), min, max];
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                AvailabilityViewController *availabilityViewController = [storyboard instantiateViewControllerWithIdentifier:@"AvailabilityScreen"];
                availabilityViewController.cobMessage = self->cobMessage;
                [self presentViewController:availabilityViewController animated:YES completion:nil];
            }
        } else {
            [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
        }
    });
}
@end
