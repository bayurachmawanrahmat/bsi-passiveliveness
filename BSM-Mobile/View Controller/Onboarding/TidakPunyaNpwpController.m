//
//  TidakPunyaNpwpController.m
//  BSM-Mobile
//
//  Created by ARS on 18/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TidakPunyaNpwpController.h"
#import "Encryptor.h"

@interface TidakPunyaNpwpController ()
{
    NSString *jenisKepemilikanNpwp;
    Encryptor *encryptor;
}
@end

@implementation TidakPunyaNpwpController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    encryptor = [[Encryptor alloc] init];
    
    UIImage *radioChecked = [UIImage imageNamed:@"ic_checklist_filed@2x.png"];
    UIImage *radioUnchecked = [UIImage imageNamed:@"ic_checklist_off@2x.png"];
    
    [_brnRadio1 setBackgroundImage:radioUnchecked forState:UIControlStateNormal];
    [_brnRadio1 setBackgroundImage:radioChecked forState:UIControlStateSelected];
    
    [_brnRadio2 setBackgroundImage:radioUnchecked forState:UIControlStateNormal];
    [_brnRadio2 setBackgroundImage:radioChecked forState:UIControlStateSelected];
    
    [_brnRadio3 setBackgroundImage:radioUnchecked forState:UIControlStateNormal];
    [_brnRadio3 setBackgroundImage:radioChecked forState:UIControlStateSelected];
}

- (void)viewDidAppear:(BOOL)animated
{
    
}

- (void)hitBtnRadio:(UIButton *)sender
{
    switch (sender.tag) {
        case 1:
            _brnRadio1.selected = YES;
            _brnRadio2.selected = NO;
            _brnRadio3.selected = NO;
            jenisKepemilikanNpwp = @"1";
            break;
        case 2:
            _brnRadio2.selected = YES;
            _brnRadio1.selected = NO;
            _brnRadio3.selected = NO;
            jenisKepemilikanNpwp = @"2";
            break;
        case 3:
            _brnRadio3.selected = YES;
            _brnRadio1.selected = NO;
            _brnRadio2.selected = NO;
            jenisKepemilikanNpwp = @"3";
            break;
        default:
            break;
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
        if ([button.titleLabel.text isEqualToString:@"SELANJUTNYA"]) {
            if(!jenisKepemilikanNpwp) return NO;
            else
            {
                [encryptor setUserDefaultsObject:@"000000000000000" forKey:ONBOARDING_INFORMASI_NPWP_NOMOR_KEY];
                [encryptor setUserDefaultsObject:@"0" forKey:ONBOARDING_INFORMASI_NPWP_ISPUNYA_KEY];
                [encryptor setUserDefaultsObject:@"0" forKey:ONBOARDING_INFORMASI_NPWP_FILEID_KEY];
                [encryptor setUserDefaultsObject:jenisKepemilikanNpwp forKey:ONBOARDING_INFORMASI_NPWP_JENIS_KEPEMILIKAN_KEY];
            }
        }
    }
    
    return YES;
}

@end
