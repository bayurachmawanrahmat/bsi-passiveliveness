//
//  JenisTabunganDetailController.m
//  BSM-Mobile
//
//  Created by ARS on 20/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "JenisTabunganDetailController.h"
#import "JenisTabunganDynamicCell.h"
#import "Encryptor.h"

@interface JenisTabunganDetailController ()
{
    Encryptor *encryptor;
}
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeight;
@end

@implementation JenisTabunganDetailController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationFullScreen;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    NSDictionary *data = [encryptor getUserDefaultsObjectForKey:@"SELECTEDJENISTABUNGANCELL"];
    NSString *imgName = ([[data objectForKey:@"kode"] isEqualToString:@"6001"] ? @"img_tabungan_detail_wadiah@2x.png" : @"img_tabungan_detail.png");
    [_bgImage setImage:[UIImage imageNamed:imgName]];
    
    [self setName1Text:[data objectForKey:@"name"]];
    [self setName2Text:[data objectForKey:@"name"]];
    [self setLongdescText:[data objectForKey:@"longdesc"]];
    [self setFeatureText:[data objectForKey:@"feature"]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    CGFloat totalHeight = 0.0;
    for (UIView *aView in _textContainerView.subviews) {
        totalHeight = totalHeight + aView.bounds.size.height;
    }
    _contentViewHeight.constant = totalHeight;
    CGRect frame = _textContainerView.frame;
    frame.size.height = totalHeight;
    [_textContainerView setFrame:frame];
    
    totalHeight = 0;
    for (UIView *aView in _contentView.subviews) {
        totalHeight += aView.frame.size.height;
    }
    _contentHeight.constant = totalHeight + 110;
    
    CGFloat defaultHeight = [UIScreen mainScreen].bounds.size.height - _scrollView.frame.origin.y;
    if (_contentHeight.constant < defaultHeight)
        _contentHeight.constant = defaultHeight;
    
    frame = _btnSelect.frame;
    frame.origin = CGPointMake(10, _contentHeight.constant - 30 - _btnSelect.frame.size.height);
    frame.size = CGSizeMake(self.view.frame.size.width - 20, _btnSelect.frame.size.height);
    [_btnSelect setFrame:frame];
}

- (IBAction)hitBtnSelect:(id)sender {
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"JenisTabunganScreen"];
    [self presentViewController:vc animated:YES completion:^{}];
}

- (IBAction)hitBtnBack:(id)sender {
    [encryptor removeUserDefaultsObjectForKey:@"SELECTEDJENISTABUNGANCELL"];
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"JenisTabunganScreen"];
    [self presentViewController:vc animated:YES completion:^{}];
}

- (void)setName1Text:(NSString *)text
{
    _name1.text = text;
}

- (void)setName2Text:(NSString *)text
{
    _name2.text = text;
}

- (void)setLongdescText:(NSString *)text
{
    _longdesc.text = text;
}

- (void)setFeatureText:(NSString *)text
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    UIFont *styleFont = _feature.font;
    [attrStr setAttributes:@{ NSFontAttributeName: styleFont } range:NSMakeRange(0, attrStr.length)];
    _feature.attributedText = attrStr;
    [_feature sizeToFit];
}

@end
