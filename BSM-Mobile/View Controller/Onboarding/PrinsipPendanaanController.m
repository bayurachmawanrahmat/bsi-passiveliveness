//
//  PrinsipPerbedaanController.m
//  BSM-Mobile
//
//  Created by ARS on 17/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PrinsipPendanaanController.h"
@interface PrinsipPendanaanController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *btnMain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@end

@implementation PrinsipPendanaanController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    _contentView.layer.cornerRadius = 30.0f;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CGRect btnFrame = _btnMain.frame;
    CGFloat viewHeight = btnFrame.origin.y + btnFrame.size.height + 30.0f;
    
    [_contentViewHeight setConstant:viewHeight];
    [self.view layoutIfNeeded];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
