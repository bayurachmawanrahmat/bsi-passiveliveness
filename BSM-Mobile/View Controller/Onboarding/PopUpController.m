//
//  PopUpController.m
//  BSM-Mobile
//
//  Created by ARS on 02/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PopUpController.h"

@interface PopUpController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *btnBukaRekening;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@end

@implementation PopUpController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.modalPresentationStyle = UIModalPresentationOverFullScreen;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.containerView.layer.cornerRadius = 30.0;
    self.btnBukaRekening.layer.borderColor = [UIColor colorWithRed:94/255.0 green:168/255.0 blue:139/255.0 alpha:1.0].CGColor;
    [self.btnBukaRekening addTarget:self action:@selector(btnBukaRekeningTappedHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.btnClose addTarget:self action:@selector(btnCloseTappedHandler) forControlEvents:UIControlEventTouchUpInside];
}

- (void)btnBukaRekeningTappedHandler
{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"PembukaanAkunAwalScreen"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)btnCloseTappedHandler
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
