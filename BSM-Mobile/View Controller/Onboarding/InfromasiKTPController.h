//
//  InfromasiKTPController.h
//  BSM-Mobile
//
//  Created by ARS on 17/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ViewController.h"
#import "CameraOverlayView.h"
#import "UIDropdown.h"
#import "OnboardingRootController.h"
#import "OnboardingButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface InfromasiKTPController : OnboardingRootController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, CameraOverlayDelegate, UITextFieldDelegate>

@property (strong, nonatomic) UIImagePickerController *cameraPicker;
@property (strong, nonatomic) UIImage *imagePicked;
@property IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *fieldNomorKtp;
@property (weak, nonatomic) IBOutlet UITextField *fieldNama;
@property (weak, nonatomic) IBOutlet UITextField *fieldNamaIbu;
@property (weak, nonatomic) IBOutlet UITextField *fieldTempatLahir;
@property (weak, nonatomic) IBOutlet UITextField *fieldTanggalLahir;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, assign) NSInteger isAlamatBeda;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;

- (void)setImageView;
@property (weak, nonatomic) IBOutlet UIButton *btnRetake;
@property (weak, nonatomic) IBOutlet OnboardingButton *btnNext;

- (IBAction)hitBtnRetake:(id)sender;

- (BOOL)checkInput;

@end

NS_ASSUME_NONNULL_END
