//
//  NomorTiketController.h
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OnboardingRootController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NomorTiketController : OnboardingRootController

@property (weak, nonatomic) IBOutlet UILabel *lblNomorTiket;
- (IBAction)btnSelesaiDidHit:(id)sender;

@end

NS_ASSUME_NONNULL_END
