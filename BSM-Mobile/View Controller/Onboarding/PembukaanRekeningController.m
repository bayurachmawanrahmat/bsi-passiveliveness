//
//  PembukaanRekeningController.m
//  BSM-Mobile
//
//  Created by ARS on 22/04/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PembukaanRekeningController.h"
#import "NSObject+FormInputCategory.h"
#import "PernyataanNasabahController.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import <CoreLocation/CoreLocation.h>
#import <sys/utsname.h>
#import "PembukaanAkunController.h"

@interface PembukaanRekeningController () <CLLocationManagerDelegate>
{
    dispatch_group_t referenceGroup;
    BOOL requestIsSuccess;
    NSString *nextScreenName;
    Encryptor *encryptor;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;
}
- (IBAction)btnNextDidHit:(id)sender;
@end

@implementation PembukaanRekeningController

NSString *const VerifikasiOnlineScreen = @"VerifikasiOnlineScreen";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encryptor = [[Encryptor alloc] init];
    
    _lblText.delegate = self;
    
    NSString *akad = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY] isEqualToString:ONBOARDING_TAB_WADIAHID] ? @"Akad Wadiah" : @"Akad Mudharabah";
    
    NSString *text = [NSString stringWithFormat:@"Dengan ini nasabah (%@) menyatakan</br>    kebenaran seluruh data dan pernyataan yang telah diberikan, dan menyetujui untuk membuka <b>Tabungan</b> dengan <b>%@</b> melalui BSI Mobile sesuai dengan syarat dan ketentuan yang telah disepakati", [encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_NAMA_LGKP], akad];
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    UIFont *styleFont = _lblText.font;
    [attrStr setAttributes:@{ NSFontAttributeName: styleFont } range:NSMakeRange(0, attrStr.length)];
    NSMutableParagraphStyle *ps = NSMutableParagraphStyle.new;
    ps.alignment = NSTextAlignmentCenter;
    [attrStr addAttribute:NSParagraphStyleAttributeName value:ps range:NSMakeRange(0, attrStr.length)];
    
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:[attrStr.string rangeOfString:@"Tabungan"]];
    
    [attrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:13] range:[attrStr.string rangeOfString:akad]];
    [attrStr addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"http://www.google.com"] range:[attrStr.string rangeOfString:akad]];
    
    UIColor *linkColor = [[UIColor alloc] initWithRed:92.0/255.0
                                                green:168.0/255.0
                                                 blue:139.0/255.0
                                                alpha:1];
    _lblText.linkTextAttributes = @{NSForegroundColorAttributeName:linkColor};
    
    _lblText.attributedText = attrStr;
    
    self.contentVIew.layer.cornerRadius = 30.0f;
    
    [self currentLocationIdentifier];
    [self getKycMenu];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_lblText setContentOffset:CGPointZero];
}


#pragma mark : CLLocationManagerDelegate Methods
-(void)currentLocationIdentifier
{
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else {
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    
    [encryptor setUserDefaultsObject:[NSString stringWithFormat:@"%f", lat] forKey:ONBOARDING_CUSTOMER_LOCATION_LAT];
    [encryptor setUserDefaultsObject:[NSString stringWithFormat:@"%f", lng] forKey:ONBOARDING_CUSTOMER_LOCATION_LNG];
    [encryptor setUserDefaultsObject:[self getDeviceType] forKey:ONBOARDING_CUSTOMER_DEVICE];
}

#pragma mark : UITextViewDelegate Methods
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    PernyataanNasabahController *modalController = [[PernyataanNasabahController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    modalController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    modalController.delegate = self;
    
    [self presentViewController:modalController animated:YES completion:^{
        if([[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_JENISTABUNGAN_KEY] isEqualToString:ONBOARDING_TAB_WADIAHID])
            [modalController.akadWadiahView setHidden:NO];
        else
            [modalController.akadMudharabahView setHidden:NO];
    }];
    return NO;
}

#pragma mark : PernyataanNasabahDelegate Methods
- (void)didSetujuPernyataanNasabah:(PernyataanNasabahController *)controller
{
    
}

#pragma mark : Private Methods
- (NSString *)getDeviceType {
    @try {
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *deviceCode = [NSString stringWithCString:systemInfo.machine
                                                  encoding:NSUTF8StringEncoding];
        static NSDictionary* deviceNamesByCode = nil;
        if (!deviceNamesByCode) {
            deviceNamesByCode = @{@"i386"      : @"Simulator",
                                  @"x86_64"    : @"Simulator",
                                  @"iPod1,1"   : @"iPod Touch",        // (Original)
                                  @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                                  @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                                  @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                                  @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                                  @"iPhone1,1" : @"iPhone",            // (Original)
                                  @"iPhone1,2" : @"iPhone",            // (3G)
                                  @"iPhone2,1" : @"iPhone",            // (3GS)
                                  @"iPad1,1"   : @"iPad",              // (Original)
                                  @"iPad2,1"   : @"iPad 2",            //
                                  @"iPad3,1"   : @"iPad",              // (3rd Generation)
                                  @"iPhone3,1" : @"iPhone 4",          // (GSM)
                                  @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                                  @"iPhone4,1" : @"iPhone 4S",         //
                                  @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                                  @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                                  @"iPad3,4"   : @"iPad",              // (4th Generation)
                                  @"iPad2,5"   : @"iPad Mini",         // (Original)
                                  @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                                  @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                                  @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                                  @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                                  @"iPhone7,1" : @"iPhone 6 Plus",     //
                                  @"iPhone7,2" : @"iPhone 6",          //
                                  @"iPhone8,1" : @"iPhone 6S",         //
                                  @"iPhone8,2" : @"iPhone 6S Plus",    //
                                  @"iPhone8,4" : @"iPhone SE",         //
                                  @"iPhone9,1" : @"iPhone 7",          //
                                  @"iPhone9,3" : @"iPhone 7",          //
                                  @"iPhone9,2" : @"iPhone 7 Plus",     //
                                  @"iPhone9,4" : @"iPhone 7 Plus",     //
                                  @"iPhone10,1": @"iPhone 8",          // CDMA
                                  @"iPhone10,4": @"iPhone 8",          // GSM
                                  @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                                  @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                                  @"iPhone10,3": @"iPhone X",          // CDMA
                                  @"iPhone10,6": @"iPhone X",          // GSM
                                  @"iPhone11,2": @"iPhone XS",         //
                                  @"iPhone11,4": @"iPhone XS Max",     //
                                  @"iPhone11,6": @"iPhone XS Max",     // China
                                  @"iPhone11,8": @"iPhone XR",         //
                                  @"iPhone12,1": @"iPhone 11",         //
                                  @"iPhone12,3": @"iPhone 11 Pro",     //
                                  @"iPhone12,5": @"iPhone 11 Pro Max", //

                                  @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                                  @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                                  @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                                  @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                                  @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                                  @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                                  @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                                  @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                                  @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                                  };
        }
        
        return [deviceNamesByCode objectForKey:deviceCode];
    } @catch (NSException *exception) {
        return @"iPhone";
    }
}

- (void) getKycMenu {
    [self showLoading];
    referenceGroup = dispatch_group_create();
    NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
    if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_PEMBUKAAN_AKUN_NOMOR_ANTRIAN_KEY])
    {
        CustomerOnboardingData *cod = [[CustomerOnboardingData alloc] init];
        [cod setDispatchGroup:referenceGroup];
        [cod generateNomorAntrian];
    }
    [CustomerOnboardingData getFromEndpoint:@"api/setting/getKycMenu" dispatchGroup:referenceGroup returnData:returnData];
    dispatch_group_notify(referenceGroup, dispatch_get_main_queue(), ^{
        [self hideLoading];
        if([[returnData objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSMutableArray *listButtons = [[NSMutableArray alloc] init];
            NSDictionary *data = [returnData objectForKey:@"data"];
            for (NSString *button in data) {
                [listButtons addObject:button];
            }
            
            if(listButtons.count == 1 && [[listButtons objectAtIndex:0] isEqualToString:@"VIDEOCALL"])
            {
                // disable on v6
//                self->nextScreenName = VerifikasiOnlineScreen;
//                [self->encryptor setUserDefaultsObject:[NSNumber numberWithBool:NO] forKey:ONBOARDING_ISSHOW_PEMBUKAANAKUNSCREEN_KEY];
                self->nextScreenName = @"Default";
                [self->encryptor setUserDefaultsObject:[NSNumber numberWithBool:YES] forKey:ONBOARDING_ISSHOW_PEMBUKAANAKUNSCREEN_KEY];
            }
            else
            {
                self->nextScreenName = @"Default";
                [self->encryptor setUserDefaultsObject:[NSNumber numberWithBool:YES] forKey:ONBOARDING_ISSHOW_PEMBUKAANAKUNSCREEN_KEY];
            }
            self->requestIsSuccess = YES;
        }
        else
        {
            self->nextScreenName = @"Default";
            [self->encryptor setUserDefaultsObject:[NSNumber numberWithBool:YES] forKey:ONBOARDING_ISSHOW_PEMBUKAANAKUNSCREEN_KEY];
            self->requestIsSuccess = NO;
        }
    });
}

- (void) goToVerifikasiOnline {
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *nextVc = [ui instantiateViewControllerWithIdentifier:VerifikasiOnlineScreen];
    [self presentViewController:nextVc animated:YES completion:nil];
}

-(void)goToPilihMetodeScreen{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    PembukaanAkunController *pembukaanAkunController = [storyboard instantiateViewControllerWithIdentifier:@"PilihMetodeScreen"];
    [self presentViewController:pembukaanAkunController animated:YES completion:nil];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([sender isKindOfClass:UIButton.class])
    {
        UIButton *button = (UIButton *)sender;
//        if (button.tag == 11 && [self->nextScreenName isEqualToString:VerifikasiOnlineScreen]) {
        if (button.tag == 11) {
            return NO;
        }
    }
    
    return YES;
}
- (IBAction)btnNextDidHit:(id)sender {
    [self showLoading];
    
    // send data to backend
    CustomerOnboardingData *onboardingData = [[CustomerOnboardingData alloc] init];
    dispatch_group_t dispatchGroup = dispatch_group_create();
    onboardingData.dispatchGroup = dispatchGroup;
    if ([self->nextScreenName isEqualToString:VerifikasiOnlineScreen] && self->requestIsSuccess) {
        #if !(TARGET_IPHONE_SIMULATOR)
        [self->encryptor setUserDefaultsObject:@"VIDEOCALL" forKey:ONBOARDING_JENIS_KYC_KEY];
        [onboardingData sendDataToBackend];
        dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            if(!onboardingData.requestIsSuccess)
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
            }
            else {
                if(self->requestIsSuccess)
                {
                    [self goToVerifikasiOnline];
                }
            }
        });
        #endif
    }
    else if (self->requestIsSuccess) {
        [self->encryptor setUserDefaultsObject:@"BIOMETRIC" forKey:ONBOARDING_JENIS_KYC_KEY];
        [onboardingData sendDataToBackend];
        dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
            [self hideLoading];
            if(!onboardingData.requestIsSuccess)
            {
                [self showAlertErrorWithTitle:@"Ooops" andMessage:NSLocalizedString(@"GENERAL_FAILURE", @"") sender:self];
            }
            else {
                if(self->requestIsSuccess)
                {
                    if ([[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_INPUT_DATA_IDENTITY_TYPE_KEY] isEqualToString:ONBOARDING_INPUT_DATA_IDENTITY_MANUAL]) {
                        NSString *isValid = (NSString*)[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_DUKCAPIL_IS_VALID];
                        if ([isValid isEqualToString:@"1"]) {
                            [self performSegueWithIdentifier:@"ProsesVerifikasiSegue" sender:self];
                        } else {
                            [self goToPilihMetodeScreen];
                        }
                    } else {
                        NSString *isValid = (NSString*)[self->encryptor getUserDefaultsObjectForKey:ONBOARDING_VERIFICATION_NAME_VALID];
                        if ([isValid isEqualToString:@"1"]) {
                            [self performSegueWithIdentifier:@"ProsesVerifikasiSegue" sender:self];
                        } else {
                            [self goToPilihMetodeScreen];
                        }
                    }
                }
            }
        });
    }
}

@end
