//
//  FavoriteViewController.m
//  BSM-Mobile
//
//  Created by lds on 6/1/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import "FavoriteViewController.h"
#import "HomeViewController.h"
#import "FavoriteDetailViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface FavoriteViewController ()<UITableViewDataSource, UITableViewDelegate>{
    int indexSelected;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation FavoriteViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];

    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
        [self.lblTitleMenu setText:@"Favorit"];
    }else{
        [self.lblTitleMenu setText:@"Favorite"];
    }
    
    [Styles setTopConstant:self.topConstraint];
    
    self.lblTitleMenu.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleMenu.textAlignment = const_textalignment_title;
    self.lblTitleMenu.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleMenu.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
}
- (void)viewWillAppear:(BOOL)animated{
    
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    NSString *custmerid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    if(custmerid == nil){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"en"]){
            valTitle = @"Information";
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            valCancel = @"No";
            valAgree = @"Ok";
            
        } else {
            valTitle = @"Informasi";
            valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            valCancel = @"Tidak";
            valAgree = @"Ok";
            
        }
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:valTitle
                              message:valMsg
                              delegate:self
                              cancelButtonTitle:valCancel
                              otherButtonTitles:valAgree, nil];

        alert.tag = 229;
        [alert show];

    }
    [self.tableView reloadData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavCell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    NSArray *listLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    //    [dataParam setObject:[listLanguage objectAtIndex:0] forKey:@"language"];
    BOOL isEn = [[listLanguage objectAtIndex:0]isEqualToString:@"en"];
    switch (indexPath.row) {
        case 0:
            label.text= isEn?@"Purchase":@"Pembelian";
            break;
        case 1:
            label.text= isEn?@"Payment":@"Pembayaran";
            break;
        case 2:
            label.text= @"Transfer";
            break;
            
        default:
            break;
    }
    return cell;
}

#pragma mark - TableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    indexSelected = indexPath.row;
    [self performSegueWithIdentifier:@"FavDetail" sender:self];
}


#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"FavDetail"]){
        FavoriteDetailViewController *favDetail = (FavoriteDetailViewController *)segue.destinationViewController;
        [favDetail setSelectedIndex:indexSelected];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 229) {
        //Check activate or not
        if (buttonIndex == 0) {
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        } else if (buttonIndex == 1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            if (![[userDefault valueForKey:@"config_onboarding"] boolValue]){
                           
                [userDefault setObject:@"NO" forKey:@"fromSliding"];
                [userDefault synchronize];
                
                UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
                self.slidingViewController.topViewController = viewCont;
            }else{
                NSDictionary* userInfo = @{@"position": @(313)};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
                
                NSDictionary* userInfos = @{@"position": @(1119)};
                           NSNotificationCenter* ncs = [NSNotificationCenter defaultCenter];
                           [ncs postNotificationName:@"listenerNavigate" object:self userInfo:userInfos];
            }

            
            /*NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"id"]){
                valTitle = @"Konfirmasi";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Batal";
                valAgree = @"Lanjut";
            } else {
                valTitle = @"Confirmation";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Cancel";
                valAgree = @"Next";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            
            alert.tag = 230;
            [alert show];*/
        }
    } else if (alertView.tag == 230) {
        //Check register or not
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 231;
            [alert show];            
        } else if (buttonIndex == 1) {
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
        }
    } else if (alertView.tag == 231) {
        //Back to Home
        NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
