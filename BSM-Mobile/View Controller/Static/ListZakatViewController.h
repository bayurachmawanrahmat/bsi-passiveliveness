//
//  ListZakatViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/18/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ListZakatViewController : TemplateViewController
-(void) setTitleMenu : (NSString *) strTitle;
@end

NS_ASSUME_NONNULL_END
