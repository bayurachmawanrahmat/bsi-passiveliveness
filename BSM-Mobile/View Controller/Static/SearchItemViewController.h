//
//  SearchItemViewController.h
//  BSM-Mobile
//
//  Created by ARS on 11/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchItemViewController : UIViewController{
    id delegate;
}
- (void) setDelegate:(id)newDelegate;
- (void) setSize: (CGRect) size;
@end

@protocol SearchItemDelegate

@required

- (void) selectedFeature : (int) index;
- (void) openAppStore;

@end


NS_ASSUME_NONNULL_END
