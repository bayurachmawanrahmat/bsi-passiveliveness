//
//  CobTimesViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 25/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CobTimesViewController : TemplateViewController

- (void) setMessage:(NSString*)string;

@end

NS_ASSUME_NONNULL_END
