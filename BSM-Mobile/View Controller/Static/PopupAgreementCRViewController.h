//
//  PopupAgreementCRViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface PopupAgreementCRViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate : (id) newDelegate;
- (void) setData : (NSString*) string;

@end

@protocol AgreementCRDelegate

@required

- (void) completionAgreementCR : (BOOL) state;

@end

NS_ASSUME_NONNULL_END
