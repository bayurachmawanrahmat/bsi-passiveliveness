//
//  DetailMasjidLocationViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/18/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "DetailMasjidLocationViewController.h"
#import "HomeViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Styles.h"

@interface DetailMasjidLocationViewController ()<MKMapViewDelegate>{
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl;

@end

@implementation DetailMasjidLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dt = [userDefault objectForKey:@"passingData"];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]) {
        self.lbl.text = @"Lokasi Masjid";
    } else {
        self.lbl.text = @"Mosque Location";
    }
    
    //update amanah styles
    [self.imgIcon setHidden:YES];
    self.lbl.textColor = UIColorFromRGB(const_color_title);
    self.lbl.textAlignment = const_textalignment_title;
    self.lbl.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lbl.frame;
    
    frmTitle.origin.x = 0;
    frmTitle.origin.y = TOP_NAV;
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    frmLblTitle.size.height = frmTitle.size.height;
    //frmTitle.origin.x = 0;
    //frmTitle.origin.y = 84;
    self.vwTitle.frame = frmTitle;
    self.lbl.frame = frmLblTitle;
    
    CGRect frmMap = self.mapView.frame;
    frmMap.origin.y = frmTitle.origin.y + frmTitle.size.height;
    frmMap.size.width = screenWidth;
    frmMap.size.height = screenHeight - frmMap.origin.y - 50;
    self.mapView.frame = frmMap;

    /*UITabBarController *tabCont = self.tabBarController;
    UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
    HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
    [homeCont startIdleTimer];*/
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSString *namePos = [dt valueForKey:@"name"];
    NSString *addrPos = [dt valueForKey:@"vicinity"];
    NSString *latPos = [[[dt objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lat"];
    NSString *lngPos = [[[dt objectForKey:@"geometry"] objectForKey:@"location"] valueForKey:@"lng"];
    
    //CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake([[dt valueForKey:@"latitude"]floatValue], [[dt valueForKey:@"longitude"]floatValue]);
    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake([latPos floatValue], [lngPos floatValue]);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 3000, 3000)];
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    //CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[dt valueForKey:@"latitude"]floatValue], [[dt valueForKey:@"longitude"]floatValue]);
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([latPos floatValue], [lngPos floatValue]);
    
    point.coordinate = coordinate;
    point.title = namePos;
    point.subtitle = addrPos;
    
    [self.mapView addAnnotation:point];
    [userDefault removeObjectForKey:@"passingData"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
