//
//  ShalatViewController.m
//  BSM-Mobile
//
//  Created by lds on 5/26/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import "ShalatViewController.h"
#import "Connection.h"

@interface ShalatHelper: NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int value;
+ (instancetype)create:(NSString *)title andValue:(int)value;
@end

@implementation ShalatHelper

+ (instancetype)create:(NSString *)title andValue:(int)value{
    ShalatHelper *shalatHelper = [[ShalatHelper alloc]init];
    [shalatHelper setValue:value];
    [shalatHelper setTitle:title];
    return shalatHelper;
}

@end

@interface ShalatViewController ()<ConnectionDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate>{
    NSDictionary *listData;
//    NSArray *listData;
    NSArray *listPlace;
    int selectedIndex;
}
@property (weak, nonatomic) IBOutlet UILabel *lblSchedule;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSholat;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *txtPlace;
@property (weak, nonatomic) IBOutlet UILabel *labelDay;
@end

@implementation ShalatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    //[dateFormat setDateFormat:@"dd MMM yyyy"];
    [dateFormat setDateFormat:@"dd-MMM-yyyy"];
    NSString *currentDate = [dateFormat stringFromDate:date];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        _lblTitleSholat.text = @"Waktu Sholat";
        _lblSchedule.text = @"Jadwal Shalat";
    } else {
        _lblTitleSholat.text = @"Sholat Time";
        _lblSchedule.text = @"Sholat Time";
    }

    [self.labelDay setText:currentDate];
    
    /*listPlace = [NSArray arrayWithObjects:[ShalatHelper create:@"Jakarta" andValue:1],
                 [ShalatHelper create:@"Bali" andValue:4],
                 [ShalatHelper create:@"Bandung" andValue:7],
                 [ShalatHelper create:@"Kota Jayapura" andValue:10],
                 [ShalatHelper create:@"Batam" andValue:35],
                 [ShalatHelper create:@"Bukit Tinggi" andValue:38],
                 [ShalatHelper create:@"Menado" andValue:7],nil];*/

    listPlace = [NSArray arrayWithObjects:[ShalatHelper create:@"Jakarta" andValue:1],
                 [ShalatHelper create:@"Bandung" andValue:7],
                 [ShalatHelper create:@"Denpasar" andValue:4],
                 [ShalatHelper create:@"Bukittinggi" andValue:38],
                 [ShalatHelper create:@"Menado" andValue:72],
                 [ShalatHelper create:@"Surabaya" andValue:49],
                 [ShalatHelper create:@"Yogyakarta" andValue:19],
                 [ShalatHelper create:@"Solo" andValue:230],
                 [ShalatHelper create:@"Semarang" andValue:53],
                 [ShalatHelper create:@"Malang" andValue:52],
                 [ShalatHelper create:@"Serang" andValue:224],
                 [ShalatHelper create:@"Padang" andValue:23],
                 [ShalatHelper create:@"Batam" andValue:35],
                 [ShalatHelper create:@"Pekanbaru" andValue:204],
                 [ShalatHelper create:@"Banda Aceh" andValue:3],
                 [ShalatHelper create:@"Bandar Lampung" andValue:6],
                 [ShalatHelper create:@"Bengkulu" andValue:9],
                 [ShalatHelper create:@"Palembang" andValue:51],
                 [ShalatHelper create:@"Medan" andValue:24],
                 [ShalatHelper create:@"Jambi" andValue:32],
                 [ShalatHelper create:@"Balikpapan" andValue:5],
                 [ShalatHelper create:@"Banjarmasin" andValue:8],
                 [ShalatHelper create:@"Samarinda" andValue:66],
                 [ShalatHelper create:@"Palangkaraya" andValue:21],
                 [ShalatHelper create:@"Pontianak" andValue:22],
                 [ShalatHelper create:@"Gorontalo" andValue:18],
                 [ShalatHelper create:@"Palu" andValue:74],
                 [ShalatHelper create:@"Makassar" andValue:63],
                 [ShalatHelper create:@"Kendari" andValue:70],
                 [ShalatHelper create:@"Ambon" andValue:76],
                 [ShalatHelper create:@"Jayapura" andValue:10],
                 [ShalatHelper create:@"Merauke" andValue:173],
                 [ShalatHelper create:@"Sorong" andValue:203],
                 [ShalatHelper create:@"Mataram" andValue:25],
                 [ShalatHelper create:@"Kupang" andValue:202],nil];

                 
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    [pickerView setDataSource:self];
    [pickerView setDelegate:self];
//    [datePicker2 addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    self.txtPlace.inputView = pickerView;
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doRequest)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.txtPlace.inputAccessoryView = keyboardDoneButtonView;
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=jadwal_shalat,city=1" needLoading:true encrypted:false banking:false];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return listData.allKeys.count;
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TausiahCell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    UILabel *labelTime = (UILabel *)[cell viewWithTag:2];
//    NSDictionary *data = [listData.allKeys objectAtIndex:indexPath.row];
    
//    label.text = [[listData allKeys]objectAtIndex:indexPath.row];
//    labelTime.text = [listData valueForKey:label.text];
    NSString *key = @"";
    switch (indexPath.row) {
        case 0:
            key = @"Subuh";
            break;
        case 1:
            key = @"Syuruq";
            break;
        case 2:
            key = @"Zhuhur";
            break;
        case 3:
            key = @"Ashar";
            break;
        case 4:
            key = @"Maghrib";
            break;
        case 5:
            key = @"Isya";
            break;
            
        default:
            break;
    }
    label.text = key;
    labelTime.text = [listData valueForKey:key];
//    label.text = [[[listData objectAtIndex:indexPath.row]allKeys]objectAtIndex:0];
//    labelTime.text = [[listData objectAtIndex:indexPath.row]valueForKey:label.text];
    
    return cell;
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
            //        NSString *tempStr = @"[{\"Subuh\":\"4:06:00 AM\"},{\"Syuruq\":\"\"},{\"Zhuhur\":\"11:43:00 AM\"},{\"Ashar\":\"3:07:00 PM\"},{\"Maghrib\":\"5:57:00 PM\"},{\"Isya\":\"7:12:00 PM\"}]";
            
            //        NSArray *temp = [NSJSONSerialization JSONObjectWithData:[tempStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
            
            NSArray *temp = (NSArray *)jsonObject;
            //        for(NSString *key in [[temp objectAtIndex:0]allKeys] ){
            //            NSLog(@"%@", key);
            //        }
            if(temp && temp.count> 0){
                DLog(@"%@", temp);
                listData = [temp objectAtIndex:0];
                [self.tableView reloadData];
            }
        }
        
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)reloadApp{
    [BSMDelegate reloadApp];
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listPlace.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    ShalatHelper *data = [listPlace objectAtIndex:row];
    return data.title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    selectedIndex = row;
    self.txtPlace.text = [(ShalatHelper *)[listPlace objectAtIndex:row]title];
}

- (void)doRequest{
    [self.txtPlace resignFirstResponder];
    ShalatHelper *data = [listPlace objectAtIndex:selectedIndex];
    
    NSString *paramData = [NSString stringWithFormat:@"request_type=jadwal_shalat,city=%d", data.value];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
}
@end
