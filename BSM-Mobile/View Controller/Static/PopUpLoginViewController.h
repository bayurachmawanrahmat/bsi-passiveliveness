//
//  PopUpLoginViewController.h
//  BSM-Mobile
//
//  Created by ARS on 13/12/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "RootViewController.h"
#import "TemplateViewController.h"

@interface PopUpLoginViewController : UIViewController{
    id delegate;
    NSString *identifier;
    NSString *menuID;
}


- (void) setMFeat : (NSString *) strFeat;
- (void) setDelegate : (id)newDelegate;
- (void) setIdentfier : (NSString*) newIdentifier;
- (void) setMenuID : (NSString*) newMenuID;


@end

@protocol PopupLoginDelegate

@required

- (void) loginDisappear : (BOOL) loginStatus;
- (void) loginDoneState : (NSString*) identifier;


@end


