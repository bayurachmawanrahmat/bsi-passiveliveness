//
//  ERGoldViewController.m
//  BSM-Mobile
//
//  Created by ARS on 14/01/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "ERGoldViewController.h"
#import "Styles.h"

@interface ERGoldViewController ()<ConnectionDelegate, UITableViewDelegate, UITableViewDataSource>{
    
    NSArray *listDataKurs;
    NSArray *listDataGold;
    NSArray *listDataDinar;
    NSArray *listDataMotifBatik;
    
    NSString *infoType;
    NSUserDefaults *userDefault;
    NSString *language;
    NSString *stockLangAval;
    NSString *stockLangNot;
}

@property (weak, nonatomic) IBOutlet UILabel *tblTitle1;
@property (weak, nonatomic) IBOutlet UILabel *tblTitle2;
@property (weak, nonatomic) IBOutlet UILabel *tblTitle3;
@property (weak, nonatomic) IBOutlet UIView *vwTab;
@property (weak, nonatomic) IBOutlet UIView *vwTableTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconERGold;

@property (weak, nonatomic) IBOutlet UIButton *btnKurs;
@property (weak, nonatomic) IBOutlet UIButton *btnGold;
@property (weak, nonatomic) IBOutlet UIView *vwSeparator;
@property (weak, nonatomic) IBOutlet UIView *lineGold;
@property (weak, nonatomic) IBOutlet UIView *lineKurs;
@property (weak, nonatomic) IBOutlet UILabel *sceneTitle;
@property (weak, nonatomic) IBOutlet UIImageView *sceneIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargins;
@property (weak, nonatomic) IBOutlet UITableView *tableData;
@property (weak, nonatomic) IBOutlet UIView *sceneVwTitle;

@end

@implementation ERGoldViewController

- (void)viewWillAppear:(BOOL)animated{
    NSString *paramData = [NSString stringWithFormat:@"request_type=list_er_gold"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [Styles setTopConstant:_topMargins];
    
    if(IPHONE_5){
        _tblTitle1.font = [UIFont fontWithName:const_font_name1 size:11];
        _tblTitle2.font = [UIFont fontWithName:const_font_name1 size:11];
        _tblTitle3.font = [UIFont fontWithName:const_font_name1 size:11];
    }
    if([language isEqualToString:@"id"]){
        self.sceneTitle.text = @"Informasi Kurs dan Emas";
        self.tblTitle1.text = @"Mata Uang";
        self.tblTitle2.text = @"Harga Jual";
        self.tblTitle3.text = @"Harga Beli";
        stockLangAval = @"   Tersedia";
        stockLangNot = @"   Tidak Tersedia";
        [_btnKurs setTitle:@"KURS" forState:UIControlStateNormal];
        [_btnGold setTitle:@"EMAS" forState:UIControlStateNormal];

    } else {
        self.sceneTitle.text = @"Exchange Rate and Gold Information";
        self.tblTitle1.text = @"Currency";
        self.tblTitle2.text = @"Sell";
        self.tblTitle3.text = @"Buy";
        stockLangAval = @"   Available";
        stockLangNot = @"   Not Available";
        [_btnKurs setTitle:@"EXCHANGE RATE" forState:UIControlStateNormal];
        [_btnGold setTitle:@"GOLD" forState:UIControlStateNormal];
    }
    
    [_lineKurs setHidden:NO];
    [_lineGold setHidden:YES];
    
    [_vwTab setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    
    [_btnGold setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    _btnGold.titleLabel.font = [UIFont fontWithName:const_font_name3 size:16];
    
    [_btnGold setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
    
    [_btnKurs setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    [_btnKurs setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    _btnKurs.titleLabel.font = [UIFont fontWithName:const_font_name3 size:16];
    
    [_lineKurs setBackgroundColor:UIColorFromRGB(const_color_secondary)];
    [_lineGold setBackgroundColor:UIColorFromRGB(const_color_secondary)];
    
    self.sceneTitle.textColor = UIColorFromRGB(const_color_title);
    self.sceneTitle.textAlignment = const_textalignment_title;
    self.sceneTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.sceneVwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.sceneIcon setHidden:YES];
    
    infoType = @"KURS";
    
    self.tableData.dataSource = self;
    self.tableData.delegate = self;
    self.tableData.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    if([[DataManager sharedManager].dataExtra objectForKey:@"kursgoldvc"]){
        if([[[DataManager sharedManager].dataExtra valueForKey:@"kursgoldvc"]isEqualToString:@"0"]){
            [self actionKurs:self];
        }else{
            [self actionGold:self];
        }
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([infoType isEqualToString:@"KURS"]){
        return 1;
    }else{
        return 3;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([infoType isEqualToString:@"KURS"]){
        return listDataKurs.count;
    }else{
        if(section == 0){
            return listDataGold.count;
        }else if(section == 2){
            return listDataDinar.count;

        }else{
            return listDataMotifBatik.count;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *header = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 10, 10)];
    
    
    if([infoType isEqualToString:@"GOLD"]){
        if(section == 0){
            header.text = @"Regular";
        }else if(section == 2){
            header.text = @"Dinar";
        }else{
            header.text = @"Motif Batik";
        }
        
        if(IPHONE_5){
            header.font = [UIFont fontWithName:const_font_name3 size:13];
        }else{
            header.font = [UIFont fontWithName:const_font_name3 size:14];
        }
//        header.backgroundColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
        header.backgroundColor = UIColorFromRGB(const_color_primary);
        header.textColor = [UIColor whiteColor];
    }else{
        header = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    }
    return header;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KursGoldCell" forIndexPath:indexPath];
    
    
    UILabel *lblCol1 = (UILabel *)[cell viewWithTag:101];
    UILabel *lblCol2 = (UILabel *)[cell viewWithTag:102];
    UILabel *lblCol3 = (UILabel *)[cell viewWithTag:103];
    
    if(IPHONE_5){
        lblCol2.font = [UIFont fontWithName:const_font_name1 size:12.0f];
        lblCol3.font = [UIFont fontWithName:const_font_name1 size:12.0f];
        lblCol1.font = [UIFont fontWithName:const_font_name1 size:12.0f];

    }
    
    if([infoType isEqualToString:@"KURS"]){
        NSDictionary *dataKurs = [listDataKurs objectAtIndex:indexPath.row];

        lblCol1.text = [dataKurs objectForKey:@"mata_uang"];
        lblCol2.text = [dataKurs objectForKey:@"harga_jual"];
        lblCol3.text = [dataKurs objectForKey:@"harga_beli"];
        
        lblCol1.textAlignment = NSTextAlignmentLeft;
        lblCol2.textAlignment = NSTextAlignmentRight;
        lblCol3.textAlignment = NSTextAlignmentRight;
        
    }else{

        if (indexPath.section == 0){
            NSDictionary *dataGoldReg = [listDataGold objectAtIndex:indexPath.row];

            lblCol1.text = [dataGoldReg objectForKey:@"gram"];

            lblCol2.text = [NSString stringWithFormat:@"Rp.%@",[self getFormatCurrency:[[dataGoldReg objectForKey:@"price_gram"]floatValue]]];
            
            if([[dataGoldReg objectForKey:@"stock_status"] isEqualToString:@"Available"]){
                lblCol3.text = stockLangAval;
            }else{
                lblCol3.text = stockLangNot;
            }
        }else if (indexPath.section == 2){
            NSDictionary *dataGoldDinar = [listDataDinar objectAtIndex:indexPath.row];
            
            lblCol1.text = [NSString stringWithFormat:@"%@ %@ Gr",[dataGoldDinar objectForKey:@"motif"],[dataGoldDinar objectForKey:@"gram"]];
            
            lblCol2.text = [NSString stringWithFormat:@"Rp.%@",[self getFormatCurrency:[[dataGoldDinar objectForKey:@"price_gram"]floatValue]]];

            if([[dataGoldDinar objectForKey:@"stock_status"] isEqualToString:@"Available"]){
                lblCol3.text = stockLangAval;
            }else{
                lblCol3.text = stockLangNot;
            }
        }else{
            NSDictionary *dataGoldMotifBatik = [listDataMotifBatik objectAtIndex:indexPath.row];

            lblCol1.text = [NSString stringWithFormat:@"%@ %@ Gr",[dataGoldMotifBatik objectForKey:@"motif"],[dataGoldMotifBatik objectForKey:@"gram"]];

            lblCol2.text = [NSString stringWithFormat:@"Rp.%@",[self getFormatCurrency:[[dataGoldMotifBatik objectForKey:@"price_gram"]floatValue]]];
            
            if([[dataGoldMotifBatik objectForKey:@"stock_status"] isEqualToString:@"Available"]){
                lblCol3.text = stockLangAval;
            }else{
                lblCol3.text = stockLangNot;
            }
        }
        lblCol1.textAlignment = NSTextAlignmentLeft;
        lblCol2.textAlignment = NSTextAlignmentRight;
        lblCol3.textAlignment = NSTextAlignmentLeft;
    }
    
    [lblCol1 sizeToFit];
    [lblCol2 sizeToFit];
    [lblCol3 sizeToFit];
    
    return cell;
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}

- (CGFloat) getTextLabelWidth:(UILabel*) label{
    CGSize textSize = [[label text] sizeWithAttributes:@{NSFontAttributeName:[label font]}];
    return textSize.width;
}

-(NSString*) getFormatCurrency:(float)currency{
    NSString * convertedString = [NSString stringWithFormat:@"%.2f", currency];

    NSString * leftPart;
    NSString * rightPart;

    if (([convertedString rangeOfString:@"."].location != NSNotFound)) {
        rightPart = [[convertedString componentsSeparatedByString:@"."] objectAtIndex:1];
        leftPart = [[convertedString componentsSeparatedByString:@"."] objectAtIndex:0];
    }

    //NSLog(@"%d",[leftPart length]);
    NSMutableString *mu = [NSMutableString stringWithString:leftPart];

    if ([mu length] > 3) {
        [mu insertString:@"." atIndex:[mu length] - 3];
        //NSLog(@"String is %@ and length is %d", mu, [mu length]);
    }

    for (int i=7; i<[mu length]; i=i+4) {
        [mu insertString:@"." atIndex:[mu length] - i];
        //NSLog(@"%d",mu.length);
    }

    convertedString = [[mu stringByAppendingString:@","] stringByAppendingString:rightPart];

    return convertedString;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return UITableViewAutomaticDimension;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    
    if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
        if (![requestType isEqualToString:@"check_notif"]) {
            if([jsonObject isKindOfClass:[NSDictionary class]]){
                NSString *jsonData = [jsonObject objectForKey:@"response"];
                NSData *jsonNSData = [jsonData dataUsingEncoding:NSUTF8StringEncoding];
                NSError *error;
                
                id jsonObj = [NSJSONSerialization JSONObjectWithData:jsonNSData options:0 error:&error];
                
                if(error){
                    NSLog(@"Error Parsing JSON : %@", error);
                }else{
                    NSDictionary *jsonDictionary = (NSDictionary *)jsonObj;
                    
                    NSString *exRate = [jsonDictionary objectForKey:@"erates"];
                    NSData *jsonExRate = [exRate dataUsingEncoding:NSUTF8StringEncoding];
                    NSError *erros;
                    id jsonObjExRate = [NSJSONSerialization JSONObjectWithData:jsonExRate options:0 error:&erros];
                    NSArray *jsonObjExRateArr = (NSArray*) jsonObjExRate;
                    NSMutableArray *tempRate = [[NSMutableArray alloc]init];
                    for(NSDictionary *data in jsonObjExRateArr){
                        NSMutableDictionary *dictX = [NSMutableDictionary new];
                        [dictX setValue:[data objectForKey:@"buyrate"] forKey:@"harga_beli"];
                        [dictX setValue:[data objectForKey:@"midrate"] forKey:@"harga_tengah"];
                        [dictX setValue:[data objectForKey:@"sellrate"] forKey:@"harga_jual"];
                        [dictX setValue:[data objectForKey:@"id"] forKey:@"mata_uang"];

                        [tempRate addObject:dictX];
                    }

                    if([tempRate count] > 0){
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
                        listDataKurs = [tempRate sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    
                    NSDictionary *goldDict = (NSDictionary *)[jsonDictionary objectForKey:@"gold"];
                    NSArray *goldBatik = [goldDict objectForKey:@"batik"];
                    NSArray *goldDinar = [goldDict objectForKey:@"dinar"];
                    NSArray *goldReg = [goldDict objectForKey:@"reguler"];
                    
                    NSMutableArray *tempBatik = [[NSMutableArray alloc]init];
                    
                    for(NSDictionary *data in goldBatik){
                        NSMutableDictionary *dictX = [NSMutableDictionary new];
                        [dictX setValue:[data objectForKey:@"buyback"] forKey:@"buyback"];
                        [dictX setValue:[data objectForKey:@"date"] forKey:@"tanggal"];
                        [dictX setValue:[data objectForKey:@"gram"] forKey:@"gram"];
                        [dictX setValue:[data objectForKey:@"motif"] forKey:@"motif"];
                        [dictX setValue:[data objectForKey:@"price_gram"] forKey:@"price_gram"];
                        [dictX setValue:[data objectForKey:@"stock_status"] forKey:@"stock_status"];
                        
                        [tempBatik addObject:dictX];
                    }
                    
                    if([tempBatik count] > 0){
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
                        listDataMotifBatik = [tempBatik sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    
                    NSMutableArray *tempDinar = [[NSMutableArray alloc]init];
                    
                    for(NSDictionary *data in goldDinar){
                        NSMutableDictionary *dictX = [NSMutableDictionary new];
                        [dictX setValue:[data objectForKey:@"buyback"] forKey:@"buyback"];
                        [dictX setValue:[data objectForKey:@"date"] forKey:@"tanggal"];
                        [dictX setValue:[data objectForKey:@"gram"] forKey:@"gram"];
                        [dictX setValue:[data objectForKey:@"motif"] forKey:@"motif"];
                        [dictX setValue:[data objectForKey:@"price_gram"] forKey:@"price_gram"];
                        [dictX setValue:[data objectForKey:@"stock_status"] forKey:@"stock_status"];
                        
                        [tempDinar addObject:dictX];
                    }
                    
                    if([tempBatik count] > 0){
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
                        listDataDinar = [tempDinar sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    
                    NSMutableArray *tempReg = [[NSMutableArray alloc]init];
                    
                    for(NSDictionary *data in goldReg){
                        NSMutableDictionary *dictX = [NSMutableDictionary new];
                        [dictX setValue:[data objectForKey:@"buyback"] forKey:@"buyback"];
                        [dictX setValue:[data objectForKey:@"date"] forKey:@"tanggal"];
                        [dictX setValue:[data objectForKey:@"gram"] forKey:@"gram"];
                        [dictX setValue:[data objectForKey:@"price_gram"] forKey:@"price_gram"];
                        [dictX setValue:[data objectForKey:@"stock_status"] forKey:@"stock_status"];
                        
                        [tempReg addObject:dictX];
                    }
                    
                    if([tempReg count] > 0){
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
                        listDataGold = [tempReg sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    
                }
                [self.tableData reloadData];
            }
            
        }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [BSMDelegate reloadApp];
}

-(IBAction)actionKurs:(id)sender{
    infoType = @"KURS";
    if([language isEqualToString:@"id"]){
        self.tblTitle1.text = @"Mata Uang";
        self.tblTitle2.text = @"Harga Jual";
        self.tblTitle3.text = @"Harga Beli";
        _tblTitle3.textAlignment = NSTextAlignmentCenter;

    }else{
        self.tblTitle1.text = @"Currency";
        self.tblTitle2.text = @"Sell";
        _tblTitle3.textAlignment = NSTextAlignmentCenter;
        self.tblTitle3.text = @"Buy";
    }
    [_btnGold setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
    [_btnKurs setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    [_lineGold setHidden:YES];
    [_lineKurs setHidden:NO];
    [self.tableData reloadData];
}

-(IBAction)actionGold:(id)sender{
    infoType = @"GOLD";
    if([language isEqualToString:@"id"]){
        self.tblTitle1.text = @"Gram";
        self.tblTitle2.text = @"Harga Per Gram";
        self.tblTitle3.text = @"        Stok";
        _tblTitle3.textAlignment = NSTextAlignmentLeft;
    }else{
        self.tblTitle1.text = @"Gram";
        self.tblTitle2.text = @"Price Per Gram";
        self.tblTitle3.text = @"        Stock";
        _tblTitle3.textAlignment = NSTextAlignmentLeft;

    }
    [_lineGold setHidden:NO];
    [_lineKurs setHidden:YES];
    [_btnGold setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    [_btnKurs setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
    [self.tableData reloadData];
}


@end
