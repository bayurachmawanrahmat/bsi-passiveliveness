//
//  ListZakatViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/18/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ListZakatViewController.h"
#import "CellFeatureIslamic.h"
#import "PopupZakatViewController.h"
#import "Utility.h"
#import "TabViewController.h"
#import "AppProperties.h"

@interface ListZakatViewController ()<UITableViewDelegate, UITableViewDataSource, ConnectionDelegate,UIAlertViewDelegate, PopZakatDelegate>{
    NSArray *arContentZakat;
    NSString *lang;
    NSUserDefaults *userDefault;
    NSArray *arrAccount;
    NSString *menuTitle;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContentTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblContentZakat;

@end

@implementation ListZakatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrAccount = [[NSArray alloc]init];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if ([lang isEqualToString:@"id"]) {
        [self.lblTitle setText:@"Kalkulator Zakat"];
        arContentZakat = @[
                           @{@"id" : @"1", @"name" : @"Zakat Profesi"},
                           @{@"id" : @"2", @"name" : @"Zakat Perdagangan"},
                           @{@"id" : @"3", @"name" : @"Zakat Emas"},
                           @{@"id" : @"4", @"name" : @"Zakat Tabungan"},
                           @{@"id" : @"5", @"name" : @"Zakat Maal"}
                           ];
    }else{
        [self.lblTitle setText:@"Zakat Calculator"];
        arContentZakat = @[
                           @{@"id" : @"1", @"name" : @"Professional Alms"},
                           @{@"id" : @"2", @"name" : @"Trade Alms"},
                           @{@"id" : @"3", @"name" : @"Gold Alms"},
                           @{@"id" : @"4", @"name" : @"Savings Alms"},
                           @{@"id" : @"5", @"name" : @"Maal Alms"}
                           ];
    }
    
    if ([lang isEqualToString:@"id"]) {
         [self.lblContentTitle setText:@"Zakat adalah harta yang wajib dikeluarkan apabila telah memenuhi syarat-syarat yang telah ditentukan oleh agama, dan disalurkan kepada orang-orang yang telah ditentukan pula, yaitu delapan golongan berhak menerima zakat sebagai mana yang tercantum dalam Al-Quran surat At-Taubah ayat 60."];
    }else{
         [self.lblContentTitle setText:@"Zakat is a treasure that must be issued if it fulfills the conditions set by religion, and is distributed to people who have also been determined, namely eight groups are entitled to receive zakat as which is listed in Al-Quran At-Taubah verse 60."];
    }
    
   
    
    if ([Utility isDeviceHaveNotch]) {
        [self.lblContentTitle setFont:[UIFont fontWithName:const_font_name1 size:16]];
        [self.lblContentTitle setNumberOfLines:7];
    }else{
        [self.lblContentTitle setFont:[UIFont fontWithName:const_font_name1 size:14]];
        [self.lblContentTitle setNumberOfLines:6];
    }
  
    
    [self.lblContentTitle setLineBreakMode:NSLineBreakByWordWrapping];
    [self.lblContentTitle sizeToFit];
    
    
    
    [self setupLayout];
    
   
    
    self.tblContentZakat.delegate = self;
    self.tblContentZakat.dataSource = self;
    [self.tblContentZakat setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.tblContentZakat registerClass:[CellFeatureIslamic class] forCellReuseIdentifier:@"cell"];
    self.tblContentZakat.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    
//    if ([[userDefault valueForKey:@"customer_id"] isEqualToString:@""] || [userDefault valueForKey:@"customer_id"] == nil) {
//        NSString *strMsg;
//        if ([lang isEqualToString:@"id"]) {
//            strMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
//        }else{
//            strMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
//        }
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        alert.tag = 1;
//        [alert show];
//    }else{
//        [[DataManager sharedManager]resetObjectData];
//        [dataManager setMenuId:@"00021"];
//        Connection *conn = [[Connection alloc]initWithDelegate:self];
//        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_account,menu_id,device,device_type,ip_address,language,date_local,customer_id"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
//        
//    }
    
//    if ([userDefault objectForKey:@"customer_id"]) {
    if([NSUserdefaultsAes getValueForKey:@"customer_id"]){
//        [[DataManager sharedManager]resetObjectData];
//        [dataManager setMenuId:@"00021"];
//        Connection *conn = [[Connection alloc]initWithDelegate:self];
//        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_account,menu_id,device,device_type,ip_address,language,date_local,customer_id"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        [self getListAccount];
    }else{
        [self getSettingData];
    }
    [self getSettingData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(zakatBC:)
                                                 name:@"zakatBC"
                                               object:nil];
    
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1118)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    // Do any additional setup after loading the view.
}

- (void)getListAccount{
    [[DataManager sharedManager]resetObjectData];
        [dataManager setMenuId:@"00021"];
        
        NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
        NSArray *arRolespesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
        NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
        BOOL isSpesial, isFinance;
            isSpesial = false;
            isFinance = false;
        NSArray *listAcct = nil;
        
        
        if(arRolespesial.count > 0 || arRolespesial != nil){
            for(NSString *xmenuid in arRolespesial){
                if([[dataParam valueForKey:@"code"]boolValue]){
                    if([[dataParam objectForKey:@"code"]isEqualToString:xmenuid]){
                        isSpesial = true;
                        break;
                    }
                }
                if([dataManager.dataExtra objectForKey:@"code"]){
                    if([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuid]){
                        isSpesial = true;
                        break;
                    }
                }
            }
            
        }
            
        if (isSpesial) {
            listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
        }else{
            if (arRoleFinance.count > 0 || arRoleFinance != nil) {
                for(NSString *xmenuId in arRoleFinance){
                    if ([xmenuId isEqualToString:@"00021"]) {
                        isFinance = true;
                        break;
                    }
                }
                
                if (isFinance) {
                    listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];
                }else{
                    listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
                }
            }
        }
        
        if (listAcct.count != 0 || listAcct != nil) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            
            if ([lang isEqualToString:@"id"]) {
                [newData addObject:@"Pilih Nomor Rekening"];
            }else{
                [newData addObject:@"Select Account Number"];
            }
            
            for(NSDictionary *temp in listAcct){
                //new list_account2 hanlder
                NSMutableDictionary *newDict = [[NSMutableDictionary alloc]init];
                NSString* stringDisplay = @"";
                if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
                    if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                        stringDisplay = [NSString stringWithFormat:@"%@ - %@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"], [temp valueForKey:@"altname"]];
                    }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                        stringDisplay = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
                    }else{
                        stringDisplay = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
                    }
                }
                else{
                    stringDisplay = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
                }
                
                [newDict setValue:stringDisplay forKey:@"title_account"];
                [newDict setValue:[temp valueForKey:@"id_account"] forKey:@"id_account"];

                [newData addObject:newDict];
                //old handler list_account1
                //[newData addObject:[temp valueForKey:@"id_account"]];
            }
            arrAccount = newData;
            
            if ([newData count] == 1) {
                NSDictionary *idAccount = [newData objectAtIndex:0];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":[idAccount valueForKey:@"id_account"]}];
            }
        }else{
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_account2,menu_id,device,device_type,ip_address,language,date_local,customer_id"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
//            [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_account1,menu_id,device,device_type,ip_address,language,date_local,customer_id"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
}

- (void)viewWillAppear:(BOOL)animated{
    [userDefault setValue:@"0" forKey:@"state_bayar_zakat"];
    [userDefault synchronize];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void) setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmLblContentTitle = self.lblContentTitle.frame;
    CGRect frmTblContentZakat = self.tblContentZakat.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmLblTitle.size.width = SCREEN_WIDTH - (frmLblTitle.origin.x * 2);
    
    frmLblContentTitle.origin.x = 16;
    frmLblContentTitle.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmLblContentTitle.size.width = SCREEN_WIDTH - (frmLblContentTitle.origin.x * 2);
    
    frmTblContentZakat.origin.y = frmLblContentTitle.origin.y + frmLblContentTitle.size.height + 16;
    frmTblContentZakat.origin.x = 0;
    frmTblContentZakat.size.width = SCREEN_WIDTH;
    frmTblContentZakat.size.height = SCREEN_HEIGHT - frmTblContentZakat.origin.y - BOTTOM_NAV - 20;
    
    if ([Utility isDeviceHaveNotch]) {
        frmTblContentZakat.size.height = SCREEN_HEIGHT - frmTblContentZakat.origin.y - BOTTOM_NAV - 40;
    }
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.lblContentTitle.frame = frmLblContentTitle;
    self.tblContentZakat.frame = frmTblContentZakat;
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arContentZakat.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellFeatureIslamic *cell = (CellFeatureIslamic *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dataDict = [arContentZakat objectAtIndex:indexPath.row];
    
    [cell.lblTitle setText:[dataDict valueForKey:@"name"]];
    [cell.lblTitle setFont:[UIFont fontWithName:const_font_name1 size:14]];
    [cell.lblTitle sizeToFit];
    
    CGRect frmVwCard = cell.cardView.frame;
    CGRect frmImgNext = cell.imgNext.frame;
    CGRect frmLblTitle = cell.lblTitle.frame;
    
    frmVwCard.origin.x = 16;
    frmVwCard.origin.y = 8;
    frmVwCard.size.width = cell.contentView.frame.size.width - (frmVwCard.origin.x * 2);
    frmVwCard.size.height = 48;
    
    
    frmImgNext.size.width = 30;
    frmImgNext.size.height = 30;
    frmImgNext.origin.x = frmVwCard.size.width - frmImgNext.size.width - 16;
    frmImgNext.origin.y = frmVwCard.size.height/2 - frmImgNext.size.height/2;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = frmVwCard.size.height/2 - frmLblTitle.size.height/2;
    frmLblTitle.size.width = frmImgNext.origin.x - frmLblTitle.origin.x - 8;
    
    cell.cardView.frame = frmVwCard;
    cell.imgNext.frame = frmImgNext;
    cell.lblTitle.frame = frmLblTitle;
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSMutableDictionary *mutDataDict = [[NSMutableDictionary alloc]init];
    [mutDataDict setObject:[arContentZakat objectAtIndex:indexPath.row] forKey:@"data_content"];
    [mutDataDict setValue:[userDefault valueForKey:@"islamic.gold.price"] forKey:@"harga_emas"];
    [mutDataDict setValue:[userDefault valueForKey:@"islamic.rice.price"] forKey:@"harga_beras"];
    [mutDataDict setObject:arrAccount forKey:@"list_account"];
    [mutDataDict setValue:menuTitle forKey:@"menu_title"];
    
    PopupZakatViewController *popZakat = [self.storyboard instantiateViewControllerWithIdentifier:@"POPZAKAT"];
    [popZakat dataBundle:mutDataDict];
    [popZakat setDelegate:self];
    [self presentViewController:popZakat animated:YES completion:nil];
   
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
//    NSDictionary* userInfo = @{@"position": @(1118)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
//    if([requestType isEqualToString:@"list_account1"]){
    if([requestType isEqualToString:@"list_account2"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
            
            [self getListAccount];
            [self getMenuIdx];
            [self getSettingData];
//            [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            if (!isShowing) {
//                [alert show];
//                isShowing = true;
//            }
        }
        
    }
    
    if (![requestType isEqualToString:@"check_notif"]) {
        
        if ([requestType isEqualToString:@"list_account"]) {
            NSMutableArray *arrListAccount = [[NSMutableArray alloc] init];
            if ([lang isEqualToString:@"id"]) {
                [arrListAccount addObject:@"Pilih Nomor Rekening"];
            }else{
                [arrListAccount addObject:@"Select Account Number"];
            }
            
            for(NSDictionary *data in (NSArray *)jsonObject){
                [arrListAccount addObject:[data valueForKey:@"id_account"]];
            }
            arrAccount = arrListAccount;
            [self getMenuIdx];
            [self getSettingData];
        }
        
        if([requestType isEqualToString:@"setting"]){
            NSDictionary *data = (NSDictionary * )jsonObject;
            NSArray *arVMap = [[data valueForKey:@"islamic.menu.vmap"] componentsSeparatedByString:@"|"];
            NSString *strVersionMap = @"v1";
            for(NSString *strVmap in arVMap){
                NSArray *arCVmap = [strVmap componentsSeparatedByString:@"="];
                if ([[arCVmap objectAtIndex:0] isEqualToString:@VERSION_VALUE]) {
                    strVersionMap =[arCVmap objectAtIndex:1];
                    break;
                }
            }

            [userDefault setValue:[data valueForKey:@"islamic.gold.price"] forKey:@"islamic.gold.price"];
            [userDefault setValue:[data valueForKey:@"islamic.rice.price"] forKey:@"islamic.rice.price"];
        }
        
    }
}

- (void) getSettingData{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=setting,prefix=islamic"] needLoading:false encrypted:false banking:false favorite:false];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        [self backToRoot];
    }
}

#pragma mark observer signal broadcast Zakat
-(void)zakatBC:(NSNotification *) notification{
    if ([notification.name isEqualToString:@"zakatBC"]) {
//        bool stateBayarZakat = [[userDefault valueForKey:@"state_bayar_zakat"]boolValue];
//        if (!stateBayarZakat) {
//            
//            NSDictionary *userInfo = notification.userInfo;
//            
//            [self goToZakat:[userInfo valueForKey:@"str_nominal"] idAccount:[userInfo valueForKey:@"str_idAccount"]];
//            
//            [userDefault setValue:@"1" forKey:@"state_bayar_zakat"];
//            [userDefault synchronize];
//        }
        
        NSDictionary *userInfo = notification.userInfo;
        
        [self goToZakat:[userInfo valueForKey:@"str_nominal"] idAccount:[userInfo valueForKey:@"str_idAccount"]];
        
        [userDefault setValue:@"1" forKey:@"state_bayar_zakat"];
        [userDefault synchronize];
    }
}

- (void)dataZakat:(NSString *)nominal withAccount:(NSString *)account{
    [self goToZakat:nominal idAccount:account];
    [userDefault setValue:@"1" forKey:@"state_bayar_zakat"];
    [userDefault synchronize];
}

-(void) goToZakat : (NSString *) strNominal
        idAccount : (NSString *) strIdAccount{
    
//     [self handleTimer:@"STOP_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
     [[DataManager sharedManager]resetObjectData];
     
     NSInteger idxNodeInfaq, idxPayment, idxInfaq;
     idxNodeInfaq = [[userDefault valueForKey:@"nodeInfaqCalculate"]integerValue];
     idxPayment = [[userDefault valueForKey:@"indexPayment"]integerValue];
     idxInfaq = [[userDefault valueForKey:@"indexInfaq"]integerValue];
    
//    if (idxNodeInfaq == 0) {
//        idxNodeInfaq = 3;
//    }
//
//    if(idxPayment == 0){
//        idxPayment = 1;
//    }
//
//    if (idxInfaq == 0) {
//        idxInfaq = 7;
//    }
     
     NSArray *arrMenu = [dataManager.listMenu objectAtIndex:idxPayment];
     NSDictionary *dictMenu = arrMenu[1];
     NSArray *actPembayaran = [dictMenu objectForKey:@"action"];
     NSArray *arrPembayaran = [actPembayaran objectAtIndex:idxInfaq];
     NSDictionary *dictPembayaran = arrPembayaran[1];
     
    
     [dataManager setAction:[dictPembayaran objectForKey:@"action"] andMenuId:[arrPembayaran objectAtIndex:0]];
     dataManager.currentPosition = (int) idxNodeInfaq;
     
     NSString *strNominalZakat = [strNominal stringByReplacingOccurrencesOfString:@"," withString:@""];
     NSString *amount = [strNominalZakat stringByReplacingOccurrencesOfString:@"Rp. " withString:@""];
     
     
     [dataManager.dataExtra addEntriesFromDictionary:@{@"amount" : amount ,
                                                       @"id_account" : strIdAccount,
                                                       @"code" : @"8001"
     }];
    
    NSDictionary *tempData = [dataManager getObjectData];
    
    userInfo = @{@"position": @(1121),
                               @"data_temp" : tempData
    };
    nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
//    NSDictionary *dataTemp = [userInfo objectForKey:@"data_temp"];
    NSDictionary *dataTemp = tempData;
    TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[dataTemp valueForKey:@"template"] ];
    [templateViews setJsonData:dataTemp];
    bool stateAcepted = [Utility vcNotifConnection: [dataTemp valueForKey:@"template"] ];
    if (stateAcepted) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //            alert.tag = 404;
        [alert show];
    }else{
        [self.tabBarController setSelectedIndex:0];
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC popToRootViewControllerAnimated:NO];
        [currentVC pushViewController:templateViews animated:YES];
        
    }
}

-(void) setTitleMenu : (NSString *) strTitle{
    menuTitle = strTitle;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


-(void)getMenuIdx{
    NSString *url =[NSString stringWithFormat:@"%@menu_index_ios.html?vn=%@",API_URL,@VERSION_NAME];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *urlResponse, NSError *error){
            if(error == nil){
                NSDictionary *mJsonObject = [NSJSONSerialization
                                             JSONObjectWithData:data
                                             options:kNilOptions
                                             error:&error];
                if(mJsonObject){
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setValue:[mJsonObject valueForKey:@"nodeInfaqCalculate"] forKey:@"nodeInfaqCalculate"];
                    [userDefault setValue:[mJsonObject valueForKey:@"indexPayment"] forKey:@"indexPayment"];
                    [userDefault setValue:[mJsonObject valueForKey:@"indexInfaq"] forKey:@"indexInfaq"];
                    [userDefault synchronize];
                }
                
            }
        }]resume];
    
}
-(void)handleTimer : (NSString *) modeStr{
    NSDictionary* userInfo = @{@"actionTimer": modeStr};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

@end
