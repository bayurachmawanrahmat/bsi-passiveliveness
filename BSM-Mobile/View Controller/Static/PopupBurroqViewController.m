//
//  PopupBurroqViewController.m
//  BSM-Mobile
//
//  Created by BSM on 8/14/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PopupBurroqViewController.h"
#import "Connection.h"
#import "Utility.h"

@interface PopupBurroqViewController (){
    NSString *_dataText;
    NSString *_findID;
    
}

@property (weak, nonatomic) IBOutlet UIView *vwContentTac;
@property (weak, nonatomic) IBOutlet UIButton *btnExit;
@property (weak, nonatomic) IBOutlet UILabel *lblDecription;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UILabel *lblDontShow;
@property (weak, nonatomic) IBOutlet UILabel *lblContinue;

//@property (weak, nonatomic) IBOutlet UIButton *btnLebihLanjut;
- (IBAction)actionDismiss:(id)sender;
//- (IBAction)actionMoreInfo:(id)sender;


@end

@implementation PopupBurroqViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    if([userdefaults objectForKey:@"show.burroq.enable"] == nil){
        [userdefaults setValue:@"YES" forKey:@"show.burroq.enable"];
    }
      
    if([[[userdefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        self.lblDontShow.text = @"Jangan Tampilkan Lagi";
        self.lblContinue.text = @"Info Lebih Lanjut";
//        [self.btnLebihLanjut setTitle:@"Info Lebih Lanjut" forState:UIControlStateNormal];
    }else{
        self.lblDontShow.text = @"Don't Show Again";
//        [self.btnLebihLanjut setTitle:@"Continue" forState:UIControlStateNormal];
        self.lblContinue.text = @"Continue";
    }
    self.lblContinue.textAlignment = NSTextAlignmentRight;
    self.vwContentTac.layer.cornerRadius = 10.0f;
    self.vwContentTac.layer.shadowColor = [[UIColor grayColor]CGColor];
    self.vwContentTac.layer.shadowOffset = CGSizeMake(0, 8);
    self.vwContentTac.layer.shadowOpacity = 0.5f;
    self.vwContentTac.backgroundColor = [UIColor whiteColor];
    
    if (_dataText != nil) {
        
        NSDictionary* userInfo = @{@"position": @(1113)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
        NSArray *dataMessage = [_dataText componentsSeparatedByString:@"\n"];
        NSString *mDesc = [[dataMessage objectAtIndex:1]stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        [self.lblText setText:[dataMessage objectAtIndex:0]];
        [self.lblDecription setText:mDesc];
//        [self setupLayout];
    }
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dontShowAgainTap)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.lblDontShow addGestureRecognizer:tapGestureRecognizer];
    self.lblDontShow.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionMoreInfo:)];
    tapGestureRecognizer2.numberOfTapsRequired = 1;
    [self.lblContinue addGestureRecognizer:tapGestureRecognizer2];
    self.lblContinue.userInteractionEnabled = YES;
    
}

- (void) dontShowAgainTap{
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    [userdefaults setValue:@"NO" forKey:@"show.burroq.enable"];
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setupLayout{
    CGRect frmContentTac = self.vwContentTac.frame;
    CGRect frmBtnExit = self.btnExit.frame;
    CGRect frmLblText = self.lblText.frame;
    CGRect frmLblDesc = self.lblDecription.frame;
    CGRect frmBtnLanjut = self.lblText.frame;
    
    frmContentTac.origin.y = SCREEN_HEIGHT/2 - frmContentTac.size.height/2;
    frmContentTac.size.width = SCREEN_WIDTH - (frmContentTac.origin.x * 2);
    
    frmBtnExit.origin.x = frmContentTac.size.width - frmBtnExit.size.width - 8;
    frmBtnExit.origin.y = 8;
    
    frmLblText.origin.x = 24;
    frmLblText.origin.y = frmBtnExit.origin.y + frmBtnExit.size.height + 16;
    frmLblText.size.height = [Utility heightLabelDynamic:self.lblText sizeFont:15];
    frmLblText.size.width = frmContentTac.size.width - (frmLblText.origin.x *2);
//    frmLblText.size.height = [Utility heightLabelDynamic:self.lblText sizeFont:15];

    
    frmLblDesc.origin.x = frmLblText.origin.x;
    frmLblDesc.origin.y = frmLblText.origin.y + frmLblText.size.height + 16;
    frmLblDesc.size.height = [Utility heightLabelDynamic:self.lblDecription sizeFont:12];
    frmLblDesc.size.width = frmContentTac.size.width - (frmLblDesc.origin.x * 2);
    
    frmBtnLanjut.size.width = 114;
    frmBtnLanjut.origin.x = frmContentTac.size.width - frmBtnLanjut.size.width - 16;
    frmBtnLanjut.origin.y = frmLblDesc.origin.y + frmLblDesc.size.height + 16;
    
    frmContentTac.size.height = frmBtnLanjut.origin.y + frmBtnLanjut.size.height + 24;
    
    self.vwContentTac.frame = frmContentTac;
    self.btnExit.frame = frmBtnExit;
    self.lblText.frame = frmLblText;
    self.lblDecription.frame = frmLblDesc;
//    self.btnLebihLanjut.frame = frmBtnLanjut;
    
}


-(void)DataMessage:(NSString *)dataText{
    _dataText = dataText;
    NSLog(@"DATA TEXT %@", dataText);
}

- (void)findID:(NSString *)findId{
    _findID = findId;
}



- (IBAction)actionDismiss:(id)sender {
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionMoreInfo:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:nil];
//    NSDictionary* userInfo = @{@"position": @(1125), @"dataMessage" : _dataText};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSDictionary* userInfo = @{@"position": @(1125), @"dataMessage" : self->_dataText, @"findID": self->_findID};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }];
}
@end
