//
//  NCreatePasswordViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/30/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "NCreatePasswordViewController.h"
#import "RootViewController.h"
//#import "TemplateViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "Connection.h"
#import "NSString+MD5.h"
#import "SKeychain.h"

@interface NCreatePasswordViewController() {
    NSString *flagTP1;
    NSString *flagTP2;
}

@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIImageView *bgScreen;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption2;
@property (weak, nonatomic) IBOutlet UITextField *txtPass1;
@property (weak, nonatomic) IBOutlet UITextField *txtPass2;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
- (IBAction)create:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEye1;
@property (weak, nonatomic) IBOutlet UIButton *btnEye2;
- (IBAction)togglePwd1:(id)sender;
- (IBAction)togglePwd2:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vwLine1;
@property (weak, nonatomic) IBOutlet UIView *vwLine2;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle2;
@property (weak, nonatomic) IBOutlet UIView *vwCreatePassword;

@end

@implementation NCreatePasswordViewController

- (void)viewWillAppear:(BOOL)animated {
    //NSString *paramData = [NSString stringWithFormat:@"request_type=list_er"];
    //Connection *conn = [[Connection alloc]initWithDelegate:self];
    //[conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    flagTP1 = @"HIDE";
    flagTP2 = @"HIDE";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgToolbar;
    if([lang isEqualToString:@"id"]){
        self.lblCaption.text = @"Kata Sandi";
        self.lblCaption2.text = @"Konfirmasi";
        self.lblTitle.text = @"Silahkan membuat kata sandi untuk menu login";
        self.lblTitle2.text = @"(fitur login dapat di-nonaktifkan di menu pengaturan kata sandi)";
        [_btnCreate setTitle:@"BUAT KATA SANDI" forState:UIControlStateNormal];
        msgToolbar = @"Selesai";
    } else {
        self.lblCaption.text = @"Password";
        self.lblCaption2.text = @"Confirmation";
        self.lblTitle.text = @"Please create Password to login";
        self.lblTitle2.text = @"(This feature can be inactivated in password setting menu)";
        [_btnCreate setTitle:@"CREATE PASSWORD" forState:UIControlStateNormal];
        msgToolbar = @"Done";
    }
    
//    CGRect screenBound = [[UIScreen mainScreen] bounds];
//    CGSize screenSize = screenBound.size;
//    CGFloat screenWidth = screenSize.width;
//    CGFloat screenHeight = screenSize.height;
//
//    CGRect frmVwScroll = self.vwScroll.frame;
//    CGRect frmScreen = self.bgScreen.frame;
//    CGRect frmCaption = self.lblCaption.frame;
//    CGRect frmCaption2 = self.lblCaption2.frame;
//    CGRect frmText1 = self.txtPass1.frame;
//    CGRect frmText2 = self.txtPass2.frame;
//    CGRect frmLogo = self.imgLogo.frame;
//    CGRect frmButton = self.btnCreate.frame;
//    CGRect frmEye1 = self.btnEye1.frame;
//    CGRect frmEye2 = self.btnEye2.frame;
//    CGRect frmLine1 = self.vwLine1.frame;
//    CGRect frmLine2 = self.vwLine2.frame;
//    CGRect frmLblTitle = self.lblTitle.frame;
//    CGRect frmLblTitle2 = self.lblTitle2.frame;
//    CGRect frmCreatePassword = self.vwCreatePassword.frame;
//
//    frmScreen.size.height = screenHeight;
//    frmScreen.size.width = screenWidth;
//
//    frmVwScroll.origin.y = 42;
//    frmVwScroll.size.width = SCREEN_WIDTH;
//    frmVwScroll.size.height = SCREEN_HEIGHT - 102;
//
//
//    frmCreatePassword.size.width = frmVwScroll.size.width - 32;
//    //frmCreatePassword.origin.y = frmCreatePassword.origin.y - 50;
//    frmCreatePassword.origin.y = screenHeight/2 - frmCreatePassword.size.height/2 - 80;
//
//    frmText1.size.width = frmCreatePassword.size.width - 32 - frmEye1.size.width;
//    frmText2.size.width = frmCreatePassword.size.width - 32 - frmEye2.size.width;
//    frmLogo.origin.x = frmCreatePassword.size.width - frmLogo.size.width - 16;
//    frmLblTitle.size.width = frmCreatePassword.size.width - 16;
//    frmLblTitle2.size.width = frmLblTitle.size.width;
//    frmButton.size.width = frmCreatePassword.size.width - 16;
//    frmEye1.origin.x = frmCreatePassword.size.width - 16 - frmEye1.size.width;
//    frmEye2.origin.x = frmCreatePassword.size.width - 16 - frmEye2.size.width;
//
//    if (IPHONE_5) {
//        frmLogo.origin.y = frmLogo.origin.y - 50;
//        frmLblTitle.origin.y = frmLogo.origin.y + frmLogo.size.height + 5;
//        frmLblTitle2.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height + 4;
//        frmCaption.origin.y = frmLblTitle2.origin.y + frmLblTitle2.size.height + 10;
//        frmText1.origin.y = frmCaption.origin.y + frmCaption.size.height + 10;
//        frmLine1.origin.y = frmText1.origin.y + frmText1.size.height;
//        frmLine1.origin.x = frmText1.origin.x;
//        frmLine1.size.width = frmLblTitle.size.width;
//        frmCaption2.origin.y = frmText1.origin.y + frmText1.size.height + 10;
//        frmText2.origin.y = frmCaption2.origin.y + frmCaption2.size.height + 10;
//        frmLine2.origin.y = frmText2.origin.y + frmText2.size.height;
//        frmLine2.origin.x = frmText2.origin.x;
//        frmLine2.size.width = frmLblTitle2.size.width;
//        frmButton.origin.y = frmText2.origin.y + frmText2.size.height + 10;
//        frmEye1.origin.y = frmText1.origin.y;
//        frmEye2.origin.y = frmText2.origin.y;
//        frmCreatePassword.origin.y = screenHeight/2 - frmCreatePassword.size.height/2;
//        frmCreatePassword.size.height = frmButton.origin.y + frmButton.size.height + 5;
//    } else {
//        frmLine1.origin.y = frmText1.origin.y + frmText1.size.height;
//        frmLine1.origin.x = frmText1.origin.x;
//        frmLine1.size.width = frmLblTitle.size.width;
//        frmLine2.origin.y = frmText2.origin.y + frmText2.size.height;
//        frmLine2.origin.x = frmText2.origin.x;
//        frmLine2.size.width = frmLblTitle2.size.width;
//
//    }
//
//    self.bgScreen.frame = frmScreen;
//    self.vwScroll.frame = frmVwScroll;
//    self.txtPass1.frame = frmText1;
//    self.txtPass2.frame = frmText2;
//    self.imgLogo.frame = frmLogo;
//    self.lblTitle.frame = frmLblTitle;
//    self.lblTitle2.frame = frmLblTitle2;
//    self.btnCreate.frame = frmButton;
//    self.lblCaption.frame = frmCaption;
//    self.lblCaption2.frame = frmCaption2;
//    self.btnEye1.frame = frmEye1;
//    self.btnEye2.frame = frmEye2;
//    self.vwLine1.frame = frmLine1;
//    self.vwLine2.frame = frmLine2;
//    self.vwCreatePassword.frame = frmCreatePassword;
    self.vwCreatePassword.backgroundColor = [UIColor whiteColor];
    self.vwCreatePassword.layer.cornerRadius = 10;
    self.vwCreatePassword.layer.borderWidth = 1;
    self.vwCreatePassword.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwCreatePassword.frame.size.width, self.vwCreatePassword.frame.size.height)];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.txtPass1.inputAccessoryView = keyboardDoneButtonView;
    self.txtPass2.inputAccessoryView = keyboardDoneButtonView;
    
    [self.txtPass1 setSecureTextEntry:true];
    [self.txtPass2 setSecureTextEntry:true];
    
    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.vwCreatePassword.frame.size.width, self.vwCreatePassword.frame.size.height + kbSize.height + 20)];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwCreatePassword.frame.size.width, self.vwCreatePassword.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)togglePwd1:(id)sender {
    if ([flagTP1 isEqualToString:@"SHOW"]) {
        flagTP1 = @"HIDE";
        [self.txtPass1 setSecureTextEntry:true];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTP1 isEqualToString:@"HIDE"]) {
        flagTP1 = @"SHOW";
        [self.txtPass1 setSecureTextEntry:false];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)togglePwd2:(id)sender {
    if ([flagTP2 isEqualToString:@"SHOW"]) {
        flagTP2 = @"HIDE";
        [self.txtPass2 setSecureTextEntry:true];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTP2 isEqualToString:@"HIDE"]) {
        flagTP2 = @"SHOW";
        [self.txtPass2 setSecureTextEntry:false];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)create:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if([self.txtPass1.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if([self.txtPass2.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if(![self.txtPass1.text isEqualToString:self.txtPass2.text]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:lang(@"PWD_NOT_SAME") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldPwd1 = [self.txtPass1.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        NSString *trimmedStringtextFieldPwd2 = [self.txtPass2.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if (([trimmedStringtextFieldPwd1 length] == 0) || ([trimmedStringtextFieldPwd2 length] == 0)) {
            message = lang(@"PWD_NOT_ALPHANUM");
        }
        
        NSString *trimmedDectextFieldPwd1 = [[self.txtPass1.text uppercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        NSString *trimmedDectextFieldPwd2 = [[self.txtPass2.text uppercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        if (([trimmedDectextFieldPwd1 length] == 0) || ([trimmedDectextFieldPwd2 length] == 0)) {
            message = lang(@"PWD_NOT_ALPHANUM");
        }
        
        if (([self.txtPass1.text length] < 6) || ([self.txtPass2.text length] < 6) || ([self.txtPass1.text length] > 8) || ([self.txtPass2.text length] > 8)) {
            message = lang(@"PWD_MUST_6_DIGIT");
        }
        
        if([message isEqualToString:@""]){
            NSString *pwd = self.txtPass1.text;
//            [userDefault setObject:[pwd MD5] forKey:@"password"];
            [NSUserdefaultsAes setObject:[pwd MD5] forKey:@"password"];
//            [SKeychain saveObject:[pwd MD5] forKey:@"password"];
            
            [userDefault setObject:@"YES" forKey:@"mustLogin"];
            [userDefault setObject:@"NO" forKey:@"hasLogin"];
            [userDefault setValue:nil forKey:OBJ_FINANCE];
            [userDefault setValue:nil forKey:OBJ_SPESIAL];
            [userDefault setValue:nil forKey:OBJ_ALL_ACCT];
            [userDefault setValue:nil forKey:OBJ_ROLE_FINANCE];
            [userDefault setValue:nil forKey:OBJ_ROLE_SPESIAL];
//            [userDefault synchronize];
            [userDefault synchronize];
            
//            CGRect screenBound = [[UIScreen mainScreen] bounds];
//            CGSize screenSize = screenBound.size;
//            CGFloat screenWidth = screenSize.width;
//            CGFloat screenHeight = screenSize.height;
            
//            MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
//            [menuVC awakeFromNib];
//            self.slidingViewController.topViewController = menuVC.homeNavigationController;
            
            //[self.view removeFromSuperview];
//            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
//            TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
//            templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
//            [currentVC pushViewController:templateView animated:YES];
            [currentVC popToRootViewControllerAnimated:true];
            
            /*[self addChildViewController:templateView];
             [self.view addSubview:templateView.view];
             [templateView.view.superview bringSubviewToFront:templateView.view];
             [templateView didMoveToParentViewController:self];*/
            
            //[self openTemplate:111];
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (void)openTemplate:(int)position{
    NSDictionary* userInfo = @{@"position": @(position)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
