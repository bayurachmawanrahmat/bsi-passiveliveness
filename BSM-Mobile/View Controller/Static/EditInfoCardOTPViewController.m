//
//  EditInfoCardOTPViewController.m
//  BSM-Mobile
//
//  Created by ARS on 05/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "EditInfoCardOTPViewController.h"
#import "Styles.h"
#import "CustomBtn.h"

@interface EditInfoCardOTPViewController ()<UITextFieldDelegate, ConnectionDelegate, EditInfoCardOTPDelegate>{
    NSUserDefaults *userDefaults;
    NSDictionary *mData;
    
    UIView* lineView;
    UILabel* labelAlert;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleField;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIView *baseView;

@end

@implementation EditInfoCardOTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
//    [Styles setStyleButton:_btnNext];
//    [Styles setStyleButton:_btnCancel];
    
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [Styles setStyleTextFieldBottomLine:_textField];
    
    [self.btnClose setHidden:YES];
    
    [self.baseView.layer setBorderWidth:.3];
    [self.baseView.layer setCornerRadius:16.0];
    [self.baseView.layer setBorderColor:[[UIColor lightGrayColor]CGColor]];
    
    lineView = [[UIView alloc]init];
    [lineView setFrame:CGRectMake(_textField.frame.origin.x, _textField.frame.origin.y+_textField.frame.size.height, _textField.frame.size.width, 1)];
    [lineView setBackgroundColor:[UIColor lightGrayColor]];
    
    labelAlert = [[UILabel alloc]init];
    [labelAlert setFrame:CGRectMake(lineView.frame.origin.x, lineView.frame.origin.y + lineView.frame.size.height, lineView.frame.size.width, 20)];
    [labelAlert setFont:[UIFont fontWithName:const_font_name1 size:12]];
    
    NSString *string = @"";
    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        
        string = [NSString stringWithFormat:@"Info\nNomor Kartu : %@", [mData objectForKey:@"card_no"]];
        
        [self.lblTitleBar setText:@"Edit Info Kartu"];
        [self.textField setPlaceholder:@"Masukkan Informasi Kartu"];
        [self.btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        
        [labelAlert setText:@"Maximum 36 Karakter"];
    }else{
        
        string = [NSString stringWithFormat:@"Info\nCard Number : %@", [mData objectForKey:@"card_no"]];
        
        [self.lblTitleBar setText:@"Edit Card Info"];
        [self.textField setPlaceholder:@"Enter Card Information"];
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [labelAlert setText:@"Maximum 36 Characters"];
    }
    
    [self.lblTitleField setText:string];
    [self.lblTitleField setFont: [UIFont fontWithName:const_font_name1 size:14]];
    [self.textField setDelegate:self];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    [self.textField setInputAccessoryView:toolbar];


//    [self.view addSubview:lineView];
//    [self.view addSubview:labelAlert];
    
//    [labelAlert setHidden:YES];
    
    [self.btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.btnClose addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) setDelegate:(id)newDelegate {
   delegate = newDelegate;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField.text.length > 36){
        [labelAlert setHidden:NO];
        return NO;
    }
    [labelAlert setHidden:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [lineView setBackgroundColor:UIColorFromRGB(const_color_primary)];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [lineView setBackgroundColor:UIColorFromRGB(const_color_gray)];
}

- (void)actionCancel{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionNext{
    if(![_textField.text isEqualToString:@""]){
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=update_otpcard,txt1=0,txt2=%@,card_no=%@", self->_textField.text, [self->mData objectForKey:@"card_no"]]  needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)DataMessage:(NSDictionary *)data{
    mData = data;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
        if ([requestType isEqualToString:@"update_otpcard"]) {
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
               NSError *error;
               NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[[jsonObject valueForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding]
               options:NSJSONReadingAllowFragments
                 error:&error];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[dataDict objectForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self->delegate finishState:YES];
                    [self actionCancel];
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self actionCancel];
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    
}
@end
