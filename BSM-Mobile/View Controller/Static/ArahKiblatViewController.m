//
//  ArahKiblatViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/19/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "ArahKiblatViewController.h"
#import "RootViewController.h"
#import "HomeViewController.h"
#import "ArahKiblat.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>

@interface ArahKiblatViewController ()<CLLocationManagerDelegate>{
    //CLLocationManager *locationManager;
    //CLLocation *currentLocation;
    float lat;
    float lng;
    float degreesDirection;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

@implementation ArahKiblatViewController
@synthesize needle,compass,locationManager;
CLLocationCoordinate2D  currentLocation;
CLLocationDirection     currentHeading;
CLLocationDirection     cityHeading;
#define toRad(X) (X*M_PI/180.0)
#define toDeg(X) (X*180.0/M_PI)
#define FONT_SIZE_AK 14.0f
#define CELL_CONTENT_WIDTH_AK 320.0f
#define CELL_CONTENT_MARGIN_AK 10.0f

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.imgIcon setImage:[self imageWithImage:[UIImage imageNamed:@"11.png"] scaledToSize:CGSizeMake(40, 39)]];
    currentHeading=0.0;
    
    // Do any additional setup after loading the view.
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]) {
        self.lblTitle.text = @"Arah Kiblat";
    } else {
        self.lblTitle.text = @"Qiblat Direction";
    }
    
    [self.imgIcon setHidden:YES];
    //Update amanah styles
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 40;
    frmTitle.origin.x = 0;
    frmTitle.origin.y = TOP_NAV;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.height = frmTitle.size.height;
    frmLblTitle.size.width = frmTitle.size.width - (frmLblTitle.origin.x*2);
    
    self.vwTitle.frame = frmTitle;
    self.lblTitle.frame = frmLblTitle;
    
    CGRect frmImage = self.compass.frame;
    frmImage.origin.x = (screenWidth / 2) - (self.compass.frame.size.width / 2);
    frmImage.origin.y = SCREEN_HEIGHT/2 - frmImage.size.height/2 + frmTitle.size.height;
    self.compass.frame = frmImage;
    
    CGRect frmNeedle = self.needle.frame;
    frmNeedle.origin.x = (screenWidth / 2) - (self.needle.frame.size.width / 2);
    frmNeedle.origin.y = (self.compass.frame.origin.y+(self.compass.frame.size.height/2)) - (self.needle.frame.size.height/2);
    //frmNeedle.size.height = frmNeedle.size.height - 20;
    self.needle.frame = frmNeedle;
    
    CGRect frmDesc = self.lblDescription.frame;
    frmDesc.origin.x = (screenWidth / 2) - (self.lblDescription.frame.size.width / 2);
    self.lblDescription.frame = frmDesc;
    
    /*UITabBarController *tabCont = self.tabBarController;
    UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
    HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
    [homeCont startIdleTimer];*/
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([CLLocationManager locationServicesEnabled]){
        [locationManager startUpdatingLocation];
    }
    [locationManager startUpdatingHeading];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

/*-(void)currentLocationIdentifier
{
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.headingFilter = 1;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
    } else{
        
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"Continue",nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert setDelegate:self];
    }
}*/

-(float) distanceCallculator:(float)lat1 long1:(float)lng1 lat2:(float)lat2 long2:(float)lng2 {
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:lat1 longitude:lng1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:lat2 longitude:lng2];
    float dMeters = [loc1 distanceFromLocation:loc2];
    return dMeters;
}

- (void)updateHeadingDisplays {
    // Animate Compass
    [UIView     animateWithDuration:0.6
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             CGAffineTransform headingRotation = CGAffineTransformRotate(CGAffineTransformIdentity, (CGFloat)toRad(cityHeading)-toRad(currentHeading));
                             
                             needle.transform = headingRotation;
                         }
                         completion:^(BOOL finished) {
                             
                         }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    currentLocation = newLocation.coordinate;
    
    [self updateHeadingDisplays];
}

/*- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    
    //lat = -6.183712;
    //lng = 106.823641;
    
    [self rotateCompass:0.0];
    //[locationManager startUpdatingHeading];
}*/

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    float oldRad =  -manager.heading.trueHeading * M_PI / 180.0f;
    float newRad =  -newHeading.trueHeading * M_PI / 180.0f;
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    theAnimation.fromValue = [NSNumber numberWithFloat:oldRad];
    theAnimation.toValue=[NSNumber numberWithFloat:newRad];
    theAnimation.duration = 0.5f;
    [compass.layer addAnimation:theAnimation forKey:@"animateMyRotation"];
    compass.transform = CGAffineTransformMakeRotation(newRad);
    NSLog(@"%f (%f) => %f (%f)", manager.heading.trueHeading, oldRad, newHeading.trueHeading, newRad);
    // if (newHeading.headingAccuracy < 0)
    //    return;
    
    // Use the true heading if it is valid.
    CLLocationDirection  theHeading = ((newHeading.trueHeading > 0) ?
                                       newHeading.trueHeading : newHeading.magneticHeading);
    
    cityHeading = [self directionFrom:currentLocation];
    
    currentHeading = theHeading;
    
    [self updateHeadingDisplays];
}

-(CLLocationDirection) directionFrom: (CLLocationCoordinate2D) startPt  {
    double lat1 = toRad(startPt.latitude); //Your Location
    //double lat2 = toRad(21.4266700); //Kabah Location
    double lat2 = toRad(21.4225);
    double lon1 = toRad(startPt.longitude); //Your Location
    //double lon2 = toRad(39.8261100); //Kabah Location
    double lon2 = toRad(39.8262);
    double dLon = (lon2-lon1);
    
    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
    double brng = toDeg(atan2(y, x));
    
    brng = (brng+360);
    brng = (brng>360)? (brng-360) : brng;
    
    return brng;
}

/*- (void) rotateCompass: (double) degrees{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView commitAnimations];
    
    ArahKiblat *ak = [ArahKiblat objArahKiblat];
    degreesDirection = [ak calcArahKiblat:lat paramLong:lng];
    degreesDirection = roundf(degreesDirection * 100) * .01;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]) {
        self.lblDescription.text = [NSString stringWithFormat:@"ARAH KIBLAT : %.02f DERAJAT", degreesDirection];
    } else {
        self.lblDescription.text = [NSString stringWithFormat:@"QIBLAT DIRECTION : %.02f DEGREES", degreesDirection];
    }
    degreesDirection = roundf(degreesDirection);
    self.imgDirection.transform = CGAffineTransformMakeRotation((CGFloat)degreesDirection);
    
    //compassRose.transform = CGAffineTransformMakeRotation(degrees);
}*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
