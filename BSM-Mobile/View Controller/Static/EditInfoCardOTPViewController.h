//
//  EditInfoCardOTPViewController.h
//  BSM-Mobile
//
//  Created by ARS on 05/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface EditInfoCardOTPViewController : UIViewController{
    id delegate;
}

- (void) DataMessage :(NSDictionary *) data;
- (void) setDelegate:(id)newDelegate;

@end

@protocol EditInfoCardOTPDelegate

@optional
- (void) finishState : (BOOL) state;
@end

NS_ASSUME_NONNULL_END
