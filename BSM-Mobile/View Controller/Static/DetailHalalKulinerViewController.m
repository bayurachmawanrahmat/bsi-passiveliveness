//
//  DetailHalalKulinerViewController.m
//  BSM-Mobile
//
//  Created by BSM on 12/11/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "DetailHalalKulinerViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface DetailHalalKulinerViewController ()<MKMapViewDelegate>{
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbl;

@end

@implementation DetailHalalKulinerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dt = [userDefault objectForKey:@"passingDataHK"];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]) {
        self.lbl.text = @"Lokasi Restoran";
    } else {
        self.lbl.text = @"Restaurant Location";
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 40;
    frmTitle.origin.x = 0;
    frmTitle.origin.y = 84;
    self.vwTitle.frame = frmTitle;
    
    CGRect frmMap = self.mapView.frame;
    frmMap.size.width = screenWidth;
    frmMap.size.height = 494;
    frmMap.origin.x = 0;
    frmMap.origin.y = 124;
    self.mapView.frame = frmMap;
    
    NSString *namePos = [dt valueForKey:@"name"];
    NSString *addrPos = [dt valueForKey:@"address"];
    NSString *latPos = [dt valueForKey:@"latitude"];
    NSString *lngPos = [dt valueForKey:@"longitude"];
    
    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake([latPos floatValue], [lngPos floatValue]);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 3000, 3000)];
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([latPos floatValue], [lngPos floatValue]);
    
    point.coordinate = coordinate;
    point.title = namePos;
    point.subtitle = addrPos;
    
    [self.mapView addAnnotation:point];
    [userDefault removeObjectForKey:@"passingDataHK"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
