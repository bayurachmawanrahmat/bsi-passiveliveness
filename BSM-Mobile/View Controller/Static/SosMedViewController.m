//
//  SosMedViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/25/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SosMedViewController.h"
#import "RootViewController.h"
#import "Connection.h"
#import "Utility.h"

@interface SosMedViewController () {
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UIView *vwFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIImageView *imgFacebook;
@property (weak, nonatomic) IBOutlet UILabel *lblFacebook;
@property (weak, nonatomic) IBOutlet UIImageView *arrowFacebook;
@property (weak, nonatomic) IBOutlet UIView *vwTwitter;
@property (weak, nonatomic) IBOutlet UIButton *btnTwitter;
@property (weak, nonatomic) IBOutlet UIImageView *imgTwitter;
@property (weak, nonatomic) IBOutlet UILabel *lblTwitter;
@property (weak, nonatomic) IBOutlet UIImageView *arrowTwitter;
@property (weak, nonatomic) IBOutlet UIView *vwInstagram;
@property (weak, nonatomic) IBOutlet UIButton *btnInstagram;
@property (weak, nonatomic) IBOutlet UIImageView *imgInstagram;
@property (weak, nonatomic) IBOutlet UILabel *lblInstagram;
@property (weak, nonatomic) IBOutlet UIImageView *arrowInstagram;
@property (weak, nonatomic) IBOutlet UIView *vwYoutube;
@property (weak, nonatomic) IBOutlet UIButton *btnYoutube;
@property (weak, nonatomic) IBOutlet UIImageView *imgYoutube;
@property (weak, nonatomic) IBOutlet UILabel *lblYoutube;
@property (weak, nonatomic) IBOutlet UIImageView *arrowYoutube;

- (IBAction)openFacebook:(id)sender;
- (IBAction)openTwitter:(id)sender;
- (IBAction)openInstagram:(id)sender;
- (IBAction)openYoutube:(id)sender;

@end

@implementation SosMedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmFB = self.vwFacebook.frame;
    CGRect frmTW = self.vwTwitter.frame;
    CGRect frmIG = self.vwInstagram.frame;
    CGRect frmYT = self.vwYoutube.frame;
    CGRect frmAFB = self.arrowFacebook.frame;
    CGRect frmATW = self.arrowTwitter.frame;
    CGRect frmAIG = self.arrowInstagram.frame;
    CGRect frmAYT = self.arrowYoutube.frame;
    
    frmTitle.origin.y = TOP_NAV;
    frmTitle.origin.x = 0;
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmFB.size.width = screenWidth - 40;
    frmTW.size.width = screenWidth - 40;
    frmIG.size.width = screenWidth - 40;
    frmYT.size.width = screenWidth - 40;
    frmAFB.origin.x = frmFB.size.width - frmAFB.size.width - 10;

    frmATW.origin.x = frmTW.size.width - frmATW.size.width - 10;

    frmAIG.origin.x = frmIG.size.width - frmAIG.size.width - 10;

    frmAYT.origin.x = frmYT.size.width - frmAYT.size.width - 10;

    self.vwTitle.frame = frmTitle;
    self.lblTitle.frame = frmLblTitle;
    self.vwFacebook.frame = frmFB;
    self.vwTwitter.frame = frmTW;
    self.vwInstagram.frame = frmIG;
    self.vwYoutube.frame = frmYT;
    self.arrowFacebook.frame = frmAFB;
    self.arrowTwitter.frame = frmATW;
    self.arrowInstagram.frame = frmAIG;
    self.arrowYoutube.frame = frmAYT;
    
    self.vwFacebook.layer.borderColor = [UIColor blackColor].CGColor;
    self.vwFacebook.layer.borderWidth = 1.0f;
    self.vwFacebook.layer.cornerRadius = 7.0f;

    self.vwTwitter.layer.borderColor = [UIColor blackColor].CGColor;
    self.vwTwitter.layer.borderWidth = 1.0f;
    self.vwTwitter.layer.cornerRadius = 7.0f;

    self.vwInstagram.layer.borderColor = [UIColor blackColor].CGColor;
    self.vwInstagram.layer.borderWidth = 1.0f;
    self.vwInstagram.layer.cornerRadius = 7.0f;

    self.vwYoutube.layer.borderColor = [UIColor blackColor].CGColor;
    self.vwYoutube.layer.borderWidth = 1.0f;
    self.vwYoutube.layer.cornerRadius = 7.0f;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        _lblTitle.text = @"Media Sosial";
    }else{
        _lblTitle.text = @"Social Media";
    }
    
    _lblFacebook.text = @"syariahmandiri";
    _lblTwitter.text = @"syariahmandiri";
    _lblYoutube.text = @"Bank Syariah Mandiri";
    _lblInstagram.text = @"banksyariahmandiri";
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
}

- (IBAction)openFacebook:(id)sender {
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://id-id.facebook.com/syariahmandiri"]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://web.facebook.com/syariahmandiri"]];
    }
}

- (IBAction)openTwitter:(id)sender {
    NSURL *linkTwiter = [NSURL URLWithString:@"twitter://user?screen_name=syariahmandiri"];
    NSURL *urlTwiter = [NSURL URLWithString:@"https://twitter.com/syariahmandiri"];
    if ([[UIApplication sharedApplication] canOpenURL:linkTwiter]) {
        [[UIApplication sharedApplication] openURL:linkTwiter];
    } else {
        [[UIApplication sharedApplication] openURL:urlTwiter];
    }
}

- (IBAction)openInstagram:(id)sender {
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://user?username=banksyariahmandiri"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com/banksyariahmandiri/?hl=id"]];
    }
}

- (IBAction)openYoutube:(id)sender {
    NSString *channelName = @"UCDCCcaZKA85NUb_y49MiUlQ";
    NSURL *linkToAppURL = [NSURL URLWithString:[NSString stringWithFormat:@"youtube://channel/%@",channelName]];
    NSURL *linkToWebURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/channel/%@",channelName]];
    
    if ([[UIApplication sharedApplication] canOpenURL:linkToAppURL]) {
        [[UIApplication sharedApplication] openURL:linkToAppURL];
    }
    else{
        // Can't open the youtube app URL so launch Safari instead
        [[UIApplication sharedApplication] openURL:linkToWebURL];
    }
}

@end
