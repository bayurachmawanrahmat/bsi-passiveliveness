//
//  JuzAmmaViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/23/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JuzAmmaViewController : TemplateViewController

-(void) setTitleMenu : (NSString *) mStrTitle;

@end

NS_ASSUME_NONNULL_END
