//
//  CalendarQurbanViewController.h
//  BSM-Mobile
//
//  Created by ARS on 09/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CalendarQurbanViewController : UIViewController{
    id delegate;
}

- (void) setDateList:(NSArray *)dateQurbanList;
- (void) setDelegate : (id)newDelegate;

@end

@protocol CalendarQurbanDelegate

- (void) selectedDate : (NSString *)date;

@end

NS_ASSUME_NONNULL_END
