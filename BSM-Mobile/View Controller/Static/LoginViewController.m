//
//  LoginViewController.m
//  BSM-Mobile
//
//  Created by BSM on 1/9/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "LoginViewController.h"
#import "TemplateViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "Connection.h"
#import "Biometric.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+MD5.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface LoginViewController()<UIAlertViewDelegate> {
    Biometric *bm;
    int ctrlog;
    NSString *flagTPLVC;
}
@property (weak, nonatomic) IBOutlet UIView *vwLoginBox;
@property (weak, nonatomic) IBOutlet UIImageView *imgBSMLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *bgScreen;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)checkLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEye;
- (IBAction)togglePwd:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UILabel *lblBioLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnBioLogin;
-(IBAction)callBioLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblLupa;
@property (weak, nonatomic) IBOutlet UIButton *btnLupa;
-(IBAction)forgetPwd:(id)sender;

@end

@implementation LoginViewController

- (void)viewWillAppear:(BOOL)animated {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *firstLogin = [userDefault objectForKey:@"firstLogin"];
    
    bm = [Biometric objBiometric];
    if (bm.isFaceIDAvailable == NO) {
        if (bm.isTouchIDAvailable == YES) {
            [self showBiometricLogin];
        }
    } else {
        if ([firstLogin isEqualToString:@"YES"]) {
            [self showBiometricLogin];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
        
    ctrlog = 0;
    self.parentViewController.tabBarController.tabBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
    flagTPLVC = @"HIDE";
    bm = [Biometric objBiometric];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Kata Sandi";
        self.lblLupa.text = @"Lupa Kata Sandi?";

        if (bm.isTouchIDAvailable == YES) {
            self.lblBioLogin.text = @"Masuk dengan Touch ID";
        } else if (bm.isFaceIDAvailable == YES) {
            self.lblBioLogin.text = @"Masuk dengan Face ID";
        }
    } else {
        self.lblTitle.text = @"Password";
        self.lblLupa.text = @"Forgot password?";
        
        if (bm.isTouchIDAvailable == YES) {
            self.lblBioLogin.text = @"Login with Touch ID";
        } else if (bm.isFaceIDAvailable == YES) {
            self.lblBioLogin.text = @"Login with Face ID";
        }
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmBox = self.vwLoginBox.frame;
    CGRect frmScreen = self.bgScreen.frame;
    CGRect frmButton = self.btnLogin.frame;
    CGRect frmLogo = self.imgBSMLogo.frame;
    CGRect frmText = self.txtPassword.frame;
    CGRect frmLabel = self.lblTitle.frame;
    CGRect frmEye = self.btnEye.frame;
    CGRect frmLine = self.vwLine.frame;
    CGRect frmLblBio = self.lblBioLogin.frame;
    CGRect frmBtnBio = self.btnBioLogin.frame;
    CGRect frmLblLupa = self.lblLupa.frame;
    CGRect frmBtnLupa = self.btnLupa.frame;
    
    frmScreen.size.height = screenHeight;
    frmScreen.size.width = screenWidth;
    frmBox.size.width = screenWidth - 32;
    frmBox.origin.y = frmBox.origin.y - 50;

    frmLogo.origin.x = frmBox.size.width - frmLogo.size.width - 5;
    frmLogo.origin.y = 5;
    
    frmText.origin.y = frmButton.origin.y - frmText.size.height - 10;
    frmText.origin.x = 10;
    frmText.size.width = frmBox.size.width - 20;
    
    frmLabel.origin.y = frmText.origin.y - frmLabel.size.height;
    frmLabel.origin.x = 10;
    
    frmEye.origin.x = screenWidth - 55 - frmEye.size.width;
    frmEye.origin.y = frmText.origin.y + 5;
    
    frmLine.origin.y = frmText.origin.y + frmText.size.height;
    frmLine.origin.x = frmText.origin.x;
    frmLine.size.width = frmText.size.width;
    
    //frmButton.origin.y = frmBox.size.height - frmButton.size.height - 20;
    frmLblLupa.origin.y = frmLine.origin.y + 1;
    frmLblLupa.size.width = frmLine.size.width;
    
    frmBtnLupa.origin.y = frmLblLupa.origin.y;
    frmBtnLupa.size.width = frmLblLupa.size.width;
    
    frmButton.origin.y = frmBtnLupa.origin.y + 30;
    frmButton.origin.x = 10;
    frmButton.size.width = frmBox.size.width - 20;
    
    frmBtnBio.origin.x = 10;
    frmBtnBio.origin.y = frmButton.origin.y + frmButton.size.height + 15;
    //frmBtnBio.origin.y = frmBox.size.height - frmBtnBio.size.height;
    frmBtnBio.size.width = frmBox.size.width - 20;
    frmLblBio.origin.x = 10;
    frmLblBio.origin.y = frmBtnBio.origin.y;
    frmLblBio.size.width = frmBtnBio.size.width;
    
    self.vwLoginBox.frame = frmBox;
    self.bgScreen.frame = frmScreen;
    self.btnLogin.frame = frmButton;
    self.imgBSMLogo.frame = frmLogo;
    self.txtPassword.frame = frmText;
    self.lblTitle.frame = frmLabel;
    self.btnEye.frame = frmEye;
    self.vwLine.frame = frmLine;
    self.lblLupa.frame = frmLblLupa;
    self.btnLupa.frame = frmBtnLupa;
    self.lblBioLogin.frame = frmLblBio;
    self.btnBioLogin.frame = frmBtnBio;
    
    self.vwLoginBox.layer.borderColor = [UIColor blackColor].CGColor;
    self.vwLoginBox.layer.borderWidth = 1.0f;
    [self.vwLoginBox setClipsToBounds:YES];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.txtPassword.inputAccessoryView = keyboardDoneButtonView;
    [self.txtPassword setSecureTextEntry:true];
    
    if (bm.isFaceIDAvailable == NO) {
        if (bm.isTouchIDAvailable == YES) {
            [self.lblBioLogin setHidden:NO];
            [self.btnBioLogin setHidden:NO];
        } else {
            [self.lblBioLogin setHidden:YES];
            [self.btnBioLogin setHidden:YES];
        }
    } else {
        //[self.lblBioLogin setHidden:YES];
        //[self.btnBioLogin setHidden:YES];
        [self.lblBioLogin setHidden:NO];
        [self.btnBioLogin setHidden:NO];
    }
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)callBioLogin:(id)sender {
    [self showBiometricLogin];
}

- (void)showBiometricLogin {
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *langLG = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    bm = [Biometric objBiometric];
    if([langLG isEqualToString:@"id"]){
        if (bm.isTouchIDAvailable == YES) {
            myLocalizedReasonString = @"Masuk dengan Touch ID";
        } else if (bm.isFaceIDAvailable == YES) {
            myLocalizedReasonString = @"Masuk dengan Face ID";
        }
    } else {
        if (bm.isTouchIDAvailable == YES) {
            myLocalizedReasonString = @"Use Touch ID to Continue";
        } else if (bm.isFaceIDAvailable == YES) {
            myLocalizedReasonString = @"Use Face ID to Continue";
        }
    }
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    NSLog(@"Success Login with Touch / Face ID");
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                                        NSString *langLG = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                                        NSString *msg1 = @"";
                                        bm = [Biometric objBiometric];
                                        if([langLG isEqualToString:@"id"]){
                                            if (bm.isTouchIDAvailable == YES) {
                                                msg1 = @"Touch ID berhasil.";
                                            } else if (bm.isFaceIDAvailable == YES) {
                                                msg1 = @"Face ID berhasil.";
                                            }
                                        } else {
                                            if (bm.isTouchIDAvailable == YES) {
                                                msg1 = @"Touch ID success.";
                                            } else if (bm.isFaceIDAvailable == YES) {
                                                msg1 = @"Face ID success.";
                                            }
                                        }
                                        
                                        [self loginApp];
                                        /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:msg1 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                        alertView.tag = 212;
                                        [alertView show];*/
                                    });                                    
                                } else {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSString *errMsg = @"";
                                        bm = [Biometric objBiometric];
                                        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                                        NSString *langLG = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                                        if([langLG isEqualToString:@"id"]){
                                            switch([error code]) {
                                                case kLAErrorAuthenticationFailed:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"Gagal otentikasi. Touch ID tidak cocok";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"Gagal otentikasi. Face ID tidak cocok";
                                                    }
                                                    break;
                                                case kLAErrorUserCancel:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorUserFallback:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorTouchIDNotEnrolled:
                                                    errMsg = @"Touch ID belum terdaftar";
                                                    break;
                                                case kLAErrorBiometryNotAvailable:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"Device ini tidak mendukung Touch ID";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"Device ini tidak mendukung Face ID";
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        } else {
                                            switch([error code]) {
                                                case kLAErrorAuthenticationFailed:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"Authentication failed\nTouch ID doesn't match.";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"Authentication failed\nFace ID doesn't match.";
                                                    }
                                                    break;
                                                case kLAErrorUserCancel:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorUserFallback:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorTouchIDNotEnrolled:
                                                    errMsg = @"Touch ID not recognized.";
                                                    break;
                                                case kLAErrorBiometryNotAvailable:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"This device doesn't support Touch ID.";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"This device doesn't support Face ID";
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        
                                        if (![errMsg isEqualToString:@""]) {
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                            [alertView show];
                                            // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
                                        }
                                    });
                                }
                            }];
    } else {
        /*dispatch_async(dispatch_get_main_queue(), ^{
         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
         message:authError.description
         delegate:self
         cancelButtonTitle:@"OK"
         otherButtonTitles:nil, nil];
         [alertView show];
         // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
         });*/
    }
}

- (IBAction)forgetPwd:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *whdr = @"";
    NSString *wmsg = @"";
    if([lang isEqualToString:@"id"]){
        whdr = @"Informasi";
        wmsg = @"Mohon lakukan instal ulang aplikasi untuk membuat kata sandi baru.";
    } else {
        whdr = @"Information";
        wmsg = @"Please reinstall the application, to create new password.";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:whdr message:wmsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

- (IBAction)togglePwd:(id)sender {
    if ([flagTPLVC isEqualToString:@"SHOW"]) {
        flagTPLVC = @"HIDE";
        [self.txtPassword setSecureTextEntry:true];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPLVC isEqualToString:@"HIDE"]) {
        flagTPLVC = @"SHOW";
        [self.txtPassword setSecureTextEntry:false];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)checkLogin:(id)sender {
    NSString *pwd = self.txtPassword.text;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    NSString *upwd = [userDefault objectForKey:@"password"];
    NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];

    if ([[pwd MD5] isEqualToString:upwd]) {
        [userDefault setObject:@"YES" forKey:@"hasLogin"];
        [userDefault setObject:@"NO" forKey:@"firstLogin"];
        [userDefault synchronize];
        
        /*UITabBarController *tabCont = self.tabBarController;
        UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
        HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
        [homeCont startIdleTimer];*/
        
        NSDictionary* userInfo = @{@"position": @(1112)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
        MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
        [menuVC awakeFromNib];
                
        self.parentViewController.tabBarController.tabBar.hidden = NO;
        [self.slidingViewController resetTopViewAnimated:YES];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.parentViewController.tabBarController.selectedIndex != 0){
            [self.parentViewController.tabBarController setSelectedIndex:0];
        }
    } else {
        /*ctrlog = ctrlog + 1;
        NSString *err_msg = @"";
        NSString *alert_title = @"";
        if([lang isEqualToString:@"id"]){
            if (ctrlog >= 3) {
                err_msg = @"Kata Sandi Salah\n\nSilahkan coba lagi, atau install ulang aplikasi Mandiri Syariah Mobile untuk mendapatkan password baru.";
                alert_title = @"Informasi";
            } else {
                err_msg = @"Kata Sandi Salah";
                alert_title = @"Informasi";
            }
        } else {
            if (ctrlog >= 3) {
                err_msg = @"Wrong Password\n\nPlease try again, or reinstall Mandiri Syariah Mobile to get a new password.";
                alert_title = @"Information";
            } else {
                err_msg = @"Wrong Password";
                alert_title = @"Information";
            }
        }*/
        
        NSString *err_msg = @"";
        NSString *alert_title = @"";
        if([lang isEqualToString:@"id"]){
            err_msg = @"Kata Sandi Salah";
            alert_title = @"Informasi";
        } else {
            err_msg = @"Wrong Password";
            alert_title = @"Information";
        }

        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alert_title message:err_msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)loginApp {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"YES" forKey:@"hasLogin"];
    [userDefault setObject:@"NO" forKey:@"firstLogin"];
    [userDefault synchronize];

    /*UITabBarController *tabCont = self.tabBarController;
    UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
    HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
    [homeCont startIdleTimer];*/
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    [menuVC awakeFromNib];
    
    self.parentViewController.tabBarController.tabBar.hidden = NO;
    [self.slidingViewController resetTopViewAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.parentViewController.tabBarController.selectedIndex != 0){
        [self.parentViewController.tabBarController setSelectedIndex:0];
    }
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 212) {
        [self loginApp];
    }
}

@end
