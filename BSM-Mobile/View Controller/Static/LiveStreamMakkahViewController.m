//
//  LiveStreamMakkahViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 9/15/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "LiveStreamMakkahViewController.h"
#import "YTPlayerView.h"
#import "Utility.h"
#import "Styles.h"

@interface LiveStreamMakkahViewController ()<YTPlayerViewDelegate,UIAlertViewDelegate>{
    NSString *menuTitle;
    NSUserDefaults *userDefault;
}
@property (nonatomic, strong) IBOutlet YTPlayerView *ytubePlayerview;
@property (weak, nonatomic) IBOutlet UIButton *btnNavBack;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginTopVwTitle;

@end

@implementation LiveStreamMakkahViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@", [userDefault valueForKey:@"islamic.youtube.id"]);
    self.ytubePlayerview.delegate = self;
    [self.ytubePlayerview loadWithVideoId:[userDefault valueForKey:@"islamic.youtube.id"]];
//    [self.ytubePlayerview loadWithVideoId:@"XHxbq2eJrtU"];
    if (menuTitle != nil) {
        self.lblTitle.text = menuTitle;
    }
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
//    if (IPHONE_X | IPHONE_XS_MAX) {
//        _marginTopVwTitle.constant = 40;
//    }
    
    [Styles setTopConstant:_marginTopVwTitle];
    
    [self.btnNavBack setImage:[UIImage imageNamed:@"ic_mtitle_back"] forState:UIControlStateNormal];
    [self.btnNavBack addTarget:self action:@selector(actionBackNav:) forControlEvents:UIControlEventTouchUpInside];
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self backToRoot];
}

-(void) setYoutubeId : (NSString *) ytubeId{
    ytubeId = ytubeId;
}


-(void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
        case kYTPlayerStatePlaying:
            NSLog(@"Started playback");
            [self.ytubePlayerview playVideo];
            break;
        case kYTPlayerStatePaused:
            NSLog(@"Paused playback");
            [self.ytubePlayerview playVideo];
            
            break;
        default:
            break;
    }
}

-(void) setTitleMenu : (NSString *) strTitle{
    menuTitle = strTitle;
}

-(void) actionBackNav : (UIButton *) sender{
    TemplateViewController *templateViews = [self.storyboard instantiateViewControllerWithIdentifier:@"FEISLAMIC"];
    [self.tabBarController setSelectedIndex:0];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateViews animated:YES];
}

-(void)handleTimer : (NSString *) modeStr{
    NSDictionary* userInfo = @{@"actionTimer": modeStr};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
    [self handleTimer:@"STOP_TIMER"];
}

@end
