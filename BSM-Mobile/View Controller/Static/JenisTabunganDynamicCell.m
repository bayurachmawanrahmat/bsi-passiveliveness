//
//  JenisTabunganDynamicCell.m
//  BSM-Mobile
//
//  Created by ARS on 20/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "JenisTabunganDynamicCell.h"

@implementation JenisTabunganDynamicCell
{
    UIImage *radioChecked;
    UIImage *radioUnchecked;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    radioChecked = [UIImage imageNamed:@"ic_checklist_filed@2x.png"];
    radioUnchecked = [UIImage imageNamed:@"ic_checklist_off@2x.png"];
    
    [_btnSelect setImage:radioChecked forState:UIControlStateSelected];
    [_btnSelect setImage:radioUnchecked forState:UIControlStateNormal];
    
    [_btnSelect addTarget:self action:@selector(btnSelectDidSelected:) forControlEvents:UIControlEventTouchDown];
    [_btnDetail addTarget:self action:@selector(btnDetailDidSelected:) forControlEvents:UIControlEventTouchDown];
}

- (void) btnSelectDidSelected:(UIButton *)sender
{
    [_delegate didHitBtnSelect:sender];
    [sender setSelected:TRUE];
}

- (void) btnDetailDidSelected:(UIButton *)sender
{
    [_delegate didHitBtnDetail:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
@end
