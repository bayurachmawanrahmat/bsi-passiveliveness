//
//  Tab4ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "Tab4ViewController.h"
#import "TemplateViewController.h"

@interface Tab4ViewController () {
}

@end

@implementation Tab4ViewController

- (void)viewWillAppear:(BOOL)animated {
    /*NSDictionary* userInfo = @{@"position": @(313)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    userInfo = @{@"position": @(7)};
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];*/
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"ATMVC"];
    templateView.view.frame = CGRectMake(0, 84, screenWidth, screenHeight - 133);
    [self.view addSubview:templateView.view];
}

@end
