//
//  PopupIsiPasanganViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/4/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupIsiPasanganViewController : TemplateViewController
{
    id delegate;
}
- (void) setMartialState : (int) mState;
- (void) setDelegate:(id)newDelegate;

@end

@protocol DataPasanganDelegate

@required

- (void) doneState : (BOOL) state;
- (void) namaPasangan : (NSString*) string;

@end
NS_ASSUME_NONNULL_END
