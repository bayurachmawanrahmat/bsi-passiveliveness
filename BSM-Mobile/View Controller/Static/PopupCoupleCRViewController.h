//
//  PopupCoupleCRViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupCoupleCRViewController : TemplateViewController
{
    id delegate;
}
- (void) setMartialState : (int) mState;
- (void) setDelegate:(id)newDelegate;
@end

@protocol DataCoupleDelegate

@required

- (void) doneState : (BOOL) state;
- (void) namaPasangan : (NSString*) string;

@end

NS_ASSUME_NONNULL_END
