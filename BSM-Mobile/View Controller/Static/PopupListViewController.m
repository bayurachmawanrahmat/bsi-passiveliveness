//
//  PopupListViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PopupListViewController.h"
#import "LVPCell.h"
#import "Styles.h"

@interface PopupListViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    NSArray *listData, *listFiltered;
    BOOL isSearchActive;
    UIToolbar *toolBar;
    NSString* tfPlacholder;
}

@end

@implementation PopupListViewController
- (IBAction)actionClose:(id)sender {
    [self dissmissList];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
       self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    isSearchActive = NO;
    _searchImg.hidden = YES;
    
    [self.listView registerNib:[UINib nibWithNibName:@"LVPCell" bundle:nil]
    forCellReuseIdentifier:@"LVPCELL"];
    
    [self.tfSearch setDelegate:self];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    self.tfSearch.inputAccessoryView = toolBar;
    [Styles setStyleTextFieldBottomLine:_tfSearch];
    
    [self.listView setDelegate:self];
    [self.listView setDataSource:self];
    [self.listView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.listView reloadData];
    
    self.vwContent.layer.cornerRadius = 10;
    self.vwContent.layer.borderWidth = 1;
    self.vwContent.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    
    [_searchImg setUserInteractionEnabled:YES];
    [_searchImg addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionClearSearch)]];
    
    [Styles addShadow:self.view];
    
    _tfSearch.placeholder = tfPlacholder;
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) actionClearSearch{
    _tfSearch.text = @"";
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    isSearchActive = YES;
    _searchImg.hidden = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@",@"name",newText];
    NSArray *filterlist = [listData filteredArrayUsingPredicate:predicate];
    
    listFiltered = filterlist;
    if([newText isEqualToString:@""]){
        listFiltered = listData;
    }
    [self.listView reloadData];
    
    return YES;
}

- (void) dissmissList{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isSearchActive){
        [delegate selectedRow:indexPath.row withList:listFiltered];
        [self dissmissList];
    }else{
        [delegate selectedRow:indexPath.row withList:listData];
        [self dissmissList];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LVPCell *cell = (LVPCell*)[tableView dequeueReusableCellWithIdentifier:@"LVPCELL"];
    
    if(isSearchActive){
        cell.name.text = [listFiltered[indexPath.row] objectForKey:@"name"];
    }else{
        cell.name.text = [listData[indexPath.row] objectForKey:@"name"];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isSearchActive){
        return listFiltered.count;
    }else{
        return listData.count;
    }
}

- (void)setList:(NSArray *)list{
    listData = [[NSArray alloc]init];
    listFiltered = [[NSArray alloc]init];
    listData = list;
    listFiltered = list;
    [self.listView reloadData];
}

- (void)setDelegate:(id)newDelagate{
    delegate = newDelagate;
}

- (void)setPlaceholder:(NSString *)placeholder{
    tfPlacholder = placeholder;
}

@end
