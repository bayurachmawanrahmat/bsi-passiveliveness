//
//  WebviewViewController.m
//  BSM-Mobile
//
//  Created by ARS on 02/01/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "WebviewViewController.h"
#import "PopUpLoginViewController.h"
#import "Utility.h"
#import <WebKit/WebKit.h>

@interface WebviewViewController ()<
    UIAlertViewDelegate,
    UIGestureRecognizerDelegate,
    WKUIDelegate,
    WKNavigationDelegate>
{
    Reachability *internetReachable;
    NSUserDefaults *userDefaults;
    DLAVAlertView *loadingAlert;
    NSTimer *idleTimer;
    int maxIdle;
    WKWebView *wkWebView;
}

@property (weak, nonatomic) IBOutlet UIView *wkWebviewContainer;

@end

@implementation WebviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    userDefaults = [NSUserDefaults standardUserDefaults];

    NSString *loadURL = [userDefaults valueForKey:@"config_webchat_link"];
    
    NSURL *url = [NSURL URLWithString:loadURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30];
    [self showLoading:@""];
    
    CGRect frmWebView = self.wkWebviewContainer.frame;
    frmWebView.origin.x = 0;
    frmWebView.origin.y = TOP_NAV;
    frmWebView.size.width = SCREEN_WIDTH;
    frmWebView.size.height = SCREEN_HEIGHT - TOP_NAV - BOTTOM_NAV;
    if ([Utility isDeviceHaveNotch]) {
        frmWebView.size.height = SCREEN_HEIGHT - TOP_NAV - BOTTOM_NAV_NOTCH;
    }
    self.wkWebviewContainer.frame = frmWebView;

    
    wkWebView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, frmWebView.size.width, frmWebView.size.height)];
    
    [wkWebView setUserInteractionEnabled:true];
    [wkWebView setUIDelegate:self];
    [wkWebView loadRequest:request];
    [wkWebView setNavigationDelegate:self];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.delegate = self;
    [wkWebView addGestureRecognizer:tapGesture];
    
    [self.wkWebviewContainer addSubview:wkWebView];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(nonnull UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    [self dismissLoading];
    
    NSString *contentMsg = @"";
    
    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        contentMsg = @"Maaf, Chat Aisyah tidak dapat di jangkau.";
    }else{
        contentMsg = @"I am Sorry, Chat Aisyah cannot be reach.";
    }
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:contentMsg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    BOOL ignoreError = FALSE;
    
    NSLog(@"webview error %@", error.domain);
    NSLog(@"webview error %ld", (long)error.code);
//    NSLog(@"%@", error.domain);
    
    if([error.domain isEqualToString:@"NSURLErrorDomain"])
    {
        if(error.code == NSURLErrorCancelled)
        {
            ignoreError = TRUE;
//            [alert show];
        }
        //if this error shouldn't be ignored
        NSString *urlStr = [error.userInfo objectForKey:@"NSErrorFallingURLStringKey"];
        
        if([urlStr hasPrefix:@"about:Blank"])
        {
            ignoreError = TRUE;
//            [alert show];
        }
        
        if(!ignoreError)
        {
//            [alert show];
            [self presentViewController:alert animated:YES completion:nil];
        }
//        [alert show];
        
    }
}

- (void)showLoading:(NSString *)message{
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [userDefaults setValue:@"YES" forKey:@"isExist"];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    loadingAlert = [[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    
    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];
    [viewLoading setBackgroundColor:[UIColor clearColor]];
    if (@available(iOS 13.0, *)) {
        UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleLarge];
        [loadingView setColor:[UIColor blackColor]];
        [loadingView startAnimating];
        [viewLoading addSubview:loadingView];
    } else {
        // Fallback on earlier versions
    }
        
    [loadingAlert setContentView:viewLoading];
    [loadingAlert show];
    
}

- (void) dismissLoading{
    NSDictionary* userInfo = @{@"position" : @(1112)};
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    [loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
    
    loadingAlert = nil;
}

- (void)webViewDidFinishLoad:(WKWebView *)webView
{
    if(webView.loading){
        return;
    }else{
        NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
        NSString *jsInjection = [NSString stringWithFormat:@"javascript:(function() {" // Auto Login
                                "var webviewCookie = true;"
                                "Chat.name = '%@';"
                                "Chat.email = '%@';"
                                "Chat.phone = '%@';"

                                "if (Chat.name !== '' && Chat.email !== '' && Chat.phone !== '') {"
                                "    Chat.webview = false;"
                                "    webviewCookie = true;"
                                "} else { "
                                "    Chat.webview = false;"
                                "    webviewCookie = false;"
                                "}"
                                "Chat.mobile = true;"
                                "Chat.init({ "
                                "     connect_message: 'Do you have questions ? <br>Come with us, we are here to help you',"
                                "     loading_meessage: 'Thank you for using our chat <br>Please wait a moment...', "
                                "     loader_sub_header: 'Connecting...', "
                                "     login_sub_header: 'Please tell us about yourself', "
                                "     chat_sub_header: 'ASISTEN INTERAKTIF BANK SYARIAH INDONESIA', "
                                "     url: '%@', "
                                "     client_id:  '%@', "
                                "     client_secret:  '%@', "
                                "     type_placeholder: 'Type message...', "
                                "     avatar: 'https://bsmmobilethree.bsm.co.id/wc/images/aisyah.jpg', "
                                "     agent_avatar: 'https://bsmmobilethree.bsm.co.id/wc/images/aisyah.jpg',"
                                "     guestName: Chat.name, "
                                "     header: 'AISYAH', "
                                "     webview: webviewCookie, "
                                "});"
                                 "})()", [userDefaults valueForKey:@"customer_name"],email,[userDefaults valueForKey:@"mobilenumber"],[userDefaults valueForKey:@"config_webchat_url"],[userDefaults valueForKey:@"config_webchat_clientid"],[userDefaults valueForKey:@"config_webchat_clientsecret"]];
        NSString *chatIconTrigger = [NSString stringWithFormat:@"javascript:(function() {"// Auto Display
                                    "   $('#chat-icon').trigger('click');"
                                    "})()"];
        NSString *chatClose = [NSString stringWithFormat:@"javascript:(function() {"// Auto Display
        "   $('#chat-close').css('display', 'none');"
        "})()"];

        [wkWebView evaluateJavaScript:jsInjection completionHandler:nil];
        [wkWebView evaluateJavaScript:chatIconTrigger completionHandler:nil];
        [wkWebView evaluateJavaScript:chatClose completionHandler:nil];
        [wkWebView evaluateJavaScript:jsInjection completionHandler:nil];

//            [self.webView stringByEvaluatingJavaScriptFromString:@"$('#chat-close').css('display','none');"];
        
            [self dismissLoading];
    }
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [self webViewDidFinishLoad:webView];
}


@end
