//
//  CollectionViewCell.h
//  BSM-Mobile
//
//  Created by ARS on 15/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic)  UILabel *lblContent;
@end

NS_ASSUME_NONNULL_END
