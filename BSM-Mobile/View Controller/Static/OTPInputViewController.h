//
//  OTPInputViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 05/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OTPInputViewController : UIViewController{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;

@end

@protocol OTPVerifierDelegate

- (void) verifiedOTP;

@end

NS_ASSUME_NONNULL_END
