//
//  MSMRegistrationViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 15/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "MSMRegistrationViewController.h"
#import "Styles.h"

@interface MSMRegistrationViewController (){
    NSUserDefaults *userDefaults;
    NSString *language;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnReg;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo1;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo2;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *backBtn;

@end

@implementation MSMRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
//    if([language isEqualToString:@"id"]){
//        self.lblTitleBar.text = @"Registrasi BSI Mobile";
//        self.labelTitle.text = @"Registrasi BSI Mobile Via \nCall Center";
//        self.lblInfo.text = @"Informasi";
//        self.lblInfo1.text = @"Persiapkan data kartu identitas (KTP/Passpor) dan informasi nomor rekening tabungan Anda sebelum memulai panggilan.";
//        self.lblInfo2.text = @"Pastikan pulsa anda cukup untuk melakukan panggilan ke Call Center.";
//        self.lblInfo3.text = @"Jangan beritahukan PIN ATM dan Kode Aktivasi kepada siapapun termasuk pihak Call Center.";
//        [self.btnReg setTitle:@"Registrasi Sekarang" forState:UIControlStateNormal];
//    }else{
//        self.lblTitleBar.text = @"BSI Mobile Registration";
//        self.labelTitle.text = @"BSI Mobile Registration Via \nCall Center";
//        self.lblInfo.text = @"Information";
//        self.lblInfo1.text = @"Prepare your ID card (KTP/Passport) and your Account Number before start the call.";
//        self.lblInfo2.text = @"Make sure your phone bill is available when start the call with Call Center.";
//        self.lblInfo3.text = @"Don't share your ATM PIN and Activation Code to everyone even though Call Center Agency.";
//        [self.btnReg setTitle:@"Registration Now" forState:UIControlStateNormal];
//    }
    self.lblTitleBar.text = lang(@"REGISTRATION_TITLE");
    self.labelTitle.text = lang(@"REGISTRATION_TITLE_CALLCENTER");
    self.lblInfo.text = lang(@"INFO");
    self.lblInfo1.text = lang(@"REGISTRATION_INFO1");
    self.lblInfo2.text = lang(@"REGISTRATION_INFO2");
    self.lblInfo3.text = lang(@"REGISTRATION_INFO3");
    [self.btnReg setTitle:lang(@"REGISTRATION_NOW") forState:UIControlStateNormal];
    
    
    self.btnReg.layer.cornerRadius = 20.0f;
    self.btnReg.layer.masksToBounds = YES;
    
    [self.backBtn setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack)];
    [self.backBtn addGestureRecognizer:tapBack];
}

- (void)actionBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionCall:(id)sender {
//    NSString *strBtnYes = @"";
//    NSString *strBtnNo = @"";
//    NSString *strMsg = @"";
    
//    if([language isEqualToString:@"id"]){
//        strBtnNo = @"Tidak";
//        strBtnYes = @"Ya";
//        strMsg = @"Anda akan dihubungkan dengan Call Center untuk pendaftaran BSI Mobile";
//    }else{
//        strBtnNo = @"No";
//        strBtnYes = @"Yes";
//        strMsg = @"Your call will be forwarded to Call Center for BSI Mobile Registration";
//    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"CONNECT_CALLCENTER") preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:lang(@"YES") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        UIApplication *application = [UIApplication sharedApplication];
        NSURL *URL = [NSURL URLWithString:@"tel://14040"];
        [application openURL:URL options:@{} completionHandler:^(BOOL success) {}];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:lang(@"NOPE") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
