//
//  ISViewController.m
//  BSM-Mobile
//
//  Created by BSM on 2/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ISViewController.h"
#import "PrayerTime.h"
#import "TemplateViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import "PenyesuainPersonalViewController.h"
#import "Utility.h"
#import "SholluLib.h"
#import "PopUpLoginViewController.h"
#import "Styles.h"
#import "PrayerNotification.h"
#import "CellJadwalSholat.h"
#import "AdzanNotifSettingViewController.h"
#import "TabViewController.h"

@interface IslamicServices: NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int value;
+ (instancetype)create:(NSString *)title andValue:(int)value;
@end

@implementation IslamicServices

+ (instancetype)create:(NSString *)title andValue:(int)value{
    IslamicServices *shalatHelper = [[IslamicServices alloc]init];
    [shalatHelper setValue:value];
    [shalatHelper setTitle:title];
    return shalatHelper;
}

@end

@interface ISViewController ()<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,UIAlertViewDelegate,AdzanNotifSettingDelegate> {
    NSMutableArray *listData;
    NSMutableArray *listNames;
    NSMutableArray *times;
    NSMutableArray *arrInterval;
    NSArray * arrListTimes;
    NSArray * arrListNames;
    NSMutableArray * arrListNotif;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;
    NSDate *curDate;
    int leastInterval;
    BOOL flagMinus;
    NSTimer *myTimer;
    BOOL selectedTime;
    NSString *islamicDateString;
    
    #pragma mark - init timer standalone
    NSTimer *idleTimer;
    int maxIdle;
    NSInteger nMaxIdle;
    
    SholluLib *sholluLib;
    
    NSUserDefaults *userdefaults;
    
    UIVisualEffect *blurEffect;
    UIVisualEffectView *visualEffectView;
    UIView* bgView;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UIView *vwDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblWaktuSholat;
@property (weak, nonatomic) IBOutlet UIButton *btnArahKiblat;
- (IBAction)actionMasjidLoc:(id)sender;
- (IBAction)actionArahKiblat:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMasjidLoc;
@property (weak, nonatomic) IBOutlet UITableView *tblJadwal;
@property (weak, nonatomic) IBOutlet UILabel *lblTanggalMasehi;
@property (weak, nonatomic) IBOutlet UILabel *lblTanggalHijriyah;
@property (weak, nonatomic) IBOutlet UIView *vwTanggal;
@property (weak, nonatomic) IBOutlet UILabel *lblLokasiMasjid;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc1;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc2;
- (IBAction)btnPersonalJadwal:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnPersonalJadwal;
@property (weak, nonatomic) IBOutlet UIView *vwGradient;
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UIView *vwMasjid;
@property (weak, nonatomic) IBOutlet UIView *vwLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargins;

@end

@implementation ISViewController

- (void)stopIdleTimer{
}

//static int idxIS;

- (void)viewWillAppear:(BOOL)animated {
    
    bool stateAcepted = [Utility vcNotifConnection: @"ISVC"];
    if (stateAcepted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    userdefaults = [NSUserDefaults standardUserDefaults];
    
    curDate = [NSDate date];
    sholluLib.currentDate = curDate;
//    [self currentLocationIdentifier];
    
    NSDictionary* userInfo = @{@"position": @(1118)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)viewWillDisappear:(BOOL)animated {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    if ([isExist isEqualToString:@"YES"]) {
        [self stopIdleTimer];
    }
}

- (void) hijriConverter {
    NSCalendar *islamicCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierIslamicCivil];
    
    NSDate *today = [NSDate date];
    [islamicCalendar rangeOfUnit:NSCalendarUnitDay startDate:&today interval:NULL forDate:today];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setCalendar:islamicCalendar];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"id_ID"];
    
    islamicDateString = [dateFormatter stringFromDate:today];
    NSLog(@"%@", islamicDateString);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sholluLib = [[SholluLib alloc]init];
    
    [Styles setTopConstant:_topMargins];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"EEEE, dd MMM yyyy"];
    NSString *currentDate = [dateFormat stringFromDate:date];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    nMaxIdle = [[userDefault objectForKey:@"config_timeout_session"]integerValue] / 1000;
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Waktu Sholat";
        self.lblDesc1.text = @"";
        self.lblDesc2.text = @"Arah Kiblat";
        self.lblLokasiMasjid.text = @"Lokasi Masjid";
        
    } else {
        self.lblTitle.text = @"Sholat Time";
        self.lblDesc1.text = @"";
        self.lblDesc2.text = @"Qibla Compass";
        self.lblLokasiMasjid.text = @"Mosque Location";
    }
    
    [_btnPersonalJadwal setImage:[UIImage imageNamed:@"ic_mtitle_setting"] forState:UIControlStateNormal];
    [_imgTitle setImage:[UIImage imageNamed:@"ic_islamic_on"]];

    
    //update amanah styles
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.lblDesc2 sizeToFit];

    arrListTimes = [[NSArray alloc]init];
    arrListNames = [[NSArray alloc]init];
    
    [self hijriConverter];
    
    self.tblJadwal.dataSource = self;
    self.tblJadwal.delegate = self;
    [self.lblTanggalMasehi setText:currentDate];
    [self.lblTanggalMasehi setFont:[UIFont fontWithName:const_font_name1 size:13]];
    [self.lblTanggalHijriyah setText:islamicDateString];
    [self.lblTanggalHijriyah setFont:[UIFont fontWithName:const_font_name1 size:13]];

//    _tblJadwal.allowsSelection = NO;
    _tblJadwal.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(optimasiJadwal:)
                                                 name:@"optimasiJadwal"
                                               object:nil];
    
    [self.tblJadwal registerNib:[UINib nibWithNibName:@"CellJadwalSholat" bundle:nil] forCellReuseIdentifier:@"CELLJADWALSHOLAT"];
    [self currentLocationIdentifier];
}

- (void)viewDidAppear:(BOOL)animated{
    [self setupBackgroundColor];
    [self currentLocationIdentifier];
}

- (IBAction)actionMasjidLoc:(id)sender {

    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 1){
        [self.tabBarController setSelectedIndex:1];
    }
    
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"MasjidVC"];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
    
}

- (IBAction)actionArahKiblat:(id)sender {
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"QiblatVC"];
    [self.tabBarController setSelectedIndex:1];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)getCurrentLocation : (CLLocation*)location
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                           
                       }
                       
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                        self->sholluLib.placemarkLocality = placemark.locality;
                        
                        NSString *subLocal = @"";
                        NSString *local = @"";
                        NSString *prefix = @"";
                        if(placemark.subLocality != nil){
                            subLocal = placemark.subLocality;
                        }
                        
                        if(placemark.locality != nil){
                            local = placemark.locality;
                        }
        
                        if([Utility isLanguageID]){
                            prefix = @"Lokasi saat ini";
                        }else{
                            prefix = @"Current Location";
                        }
        
                        if(![subLocal isEqualToString:@""] && ![local isEqualToString:@""]){
                            self->_lblDesc1.text = [NSString stringWithFormat:@"%@, %@, %@", prefix, subLocal, local];
                        }else if([subLocal isEqualToString:@""] && ![local isEqualToString:@""]){
                            self->_lblDesc1.text = [NSString stringWithFormat:@"%@, %@", prefix, local];
                        }else if(![subLocal isEqualToString:@""] && [local isEqualToString:@""]){
                            self->_lblDesc1.text = [NSString stringWithFormat:@"%@, %@", prefix, subLocal];
                        }else{
                            self->_lblDesc1.text = @"";
                        }
                        
                        
                        [self->sholluLib setNotificationData];
                   }];
}

-(void)currentLocationIdentifier
{
    [self checkGPS];
}

- (void)checkGPS {
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    visualEffectView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    bgView = [[UIView alloc]initWithFrame:self.view.bounds];
    visualEffectView.frame = bgView.bounds;
//    [bgView setBackgroundColor:UIColorFromRGB(const_color_red)];
    [Styles addShadow:bgView];
//    [bgView addSubview:visualEffectView];
    
    if ([CLLocationManager locationServicesEnabled] && status == kCLAuthorizationStatusAuthorizedWhenInUse){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else{
        [self showLastLocation];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"LOCATION_SERVICE_STATE") message:lang(@"LOCATION_SERVICE_MESSAGE") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self.tabBarController setSelectedIndex:0];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            [self->bgView removeFromSuperview];
        }]];
        
//        [self.view insertSubview:bgView atIndex:0];
        [self.view addSubview:bgView];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void) showLastLocation{
    lng = [[userdefaults valueForKey:@"LAST_LAT_SHOLAT"]doubleValue];
    lat = [[userdefaults valueForKey:@"LAST_LNG_SHOLAT"]doubleValue];
    
    CLLocation *loca = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
    
    sholluLib.latitude = lat;
    sholluLib.longitude = lng;
    
    [sholluLib callPrayerTime];

    [self getCurrentLocation:loca];
    
    [sholluLib getListData];
    
    if(sholluLib.leastInterval == 0){
        self.lblWaktuSholat.text = @"";
    }

    arrListTimes = [sholluLib getListTimes];
    arrListNames = [sholluLib getListNames];
    arrListNotif = [[[NSUserDefaults standardUserDefaults] objectForKey:@"data_adzan_notif"] mutableCopy];
    
    [self.tblJadwal reloadData];
    
    [myTimer invalidate];
    myTimer = nil;
    myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                target:self
                                                selector:@selector(onTick:)
                                                userInfo:nil repeats:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{    
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;

    [userdefaults setValue:[NSString stringWithFormat:@"%f",lng] forKey:@"LAST_LAT_SHOLAT"];
    [userdefaults setValue:[NSString stringWithFormat:@"%f",lat] forKey:@"LAST_LNG_SHOLAT"];

    
    sholluLib.latitude = lat;
    sholluLib.longitude = lng;
    
    [sholluLib callPrayerTime];

    [self getCurrentLocation:locationManager.location];
    
    [sholluLib getListData];
    
    if(sholluLib.leastInterval == 0){
        self.lblWaktuSholat.text = @"";
    }

    arrListTimes = [sholluLib getListTimes];
    arrListNames = [sholluLib getListNames];
    arrListNotif = [[[NSUserDefaults standardUserDefaults] objectForKey:@"data_adzan_notif"] mutableCopy];
    
    [self.tblJadwal reloadData];
    
    [myTimer invalidate];
    myTimer = nil;
    myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                target:self
                                                selector:@selector(onTick:)
                                                userInfo:nil repeats:YES];
    
    [bgView removeFromSuperview];
}

-(void)onTick:(NSTimer *)timer {
    [sholluLib proceedTick];
    NSString *timeDiff = [sholluLib getTimeDifference];
    
    if ([timeDiff isEqualToString:@"00:00:00"]) {
        [myTimer invalidate];
        myTimer = nil;
        self.lblWaktuSholat.text = [sholluLib getTimeAdzan];
    } else {
        self.lblWaktuSholat.text = [sholluLib getWaktu];
    }
    self.lblTimer.text = [NSString stringWithFormat:@"%@ (- %@)",[sholluLib getWaktuSholat],timeDiff];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrListNames.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AdzanNotifSettingViewController *setupAdzan = [[UIStoryboard storyboardWithName:@"Popup" bundle:nil] instantiateViewControllerWithIdentifier:@"AdzanNotifSetting"];
    [setupAdzan setData:arrListNotif];
    [setupAdzan setSelectedTime:indexPath.row];
    [setupAdzan setDelegate:self];
    
    [self presentViewController:setupAdzan animated:YES completion:nil];
}

- (void)didSettingNotification{
    arrListNotif = [[[NSUserDefaults standardUserDefaults]objectForKey:@"data_adzan_notif"]mutableCopy];
    [self.tblJadwal reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *modeImage = @[@"ic_ntf_adzan",@"ic_ntf_adzan",@"ic_ntf_senyap",@"ic_ntf_non"];
    if(indexPath.row == 0 || indexPath.row == 2){
        modeImage = @[@"ic_ntf_adzan",@"ic_ntf_senyap",@"ic_ntf_non"];
    }
    
    CellJadwalSholat *cell = (CellJadwalSholat*)[tableView dequeueReusableCellWithIdentifier:@"CELLJADWALSHOLAT"];
    
    cell.labelTitle.text = [arrListNames objectAtIndex:indexPath.row];
    cell.labelTime.text = [sholluLib getTimeSholat:[arrListTimes objectAtIndex:indexPath.row] noIndex:(indexPath.row)];
    
    int modeImageIndex = [[arrListNotif[indexPath.row] objectForKey:@"mode"]intValue];
    [cell.imgIcon setImage:[UIImage imageNamed:modeImage[modeImageIndex]]];
    
    if ([sholluLib getIdxIS] == indexPath.row) {
        cell.labelTitle.textColor = UIColorFromRGB(const_color_primary);
        cell.labelTitle.font = [UIFont fontWithName:@"Lato-Bold" size:15];
        cell.labelTime.textColor = UIColorFromRGB(const_color_primary);
        cell.labelTime.font = [UIFont fontWithName:@"Lato-Bold" size:15];
        cell.contentView.backgroundColor = UIColorFromRGB(const_highlight_primary);
        [cell.imgIcon setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_active",modeImage[modeImageIndex]]]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (IBAction)btnPersonalJadwal:(id)sender {
    UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"PenyesuainPersonalVC"];
    [self presentViewController:viewCont animated:YES completion:nil];
}

- (void) optimasiJadwal:(NSNotification *) notification {
    if ([notification.name isEqualToString:@"optimasiJadwal"]){
        [self stopRootTimer];
        NSDictionary* userInfo = notification.userInfo;
        NSString *state = [userInfo valueForKey:@"state"];
        if ([state isEqualToString:@"done"]) {
            NSLog(@"%@", state);
            [self checkGPS];
            
        }
    }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

-(void)stopRootTimer{
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)dealloc
{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) disposePenyesuaianTimerFiltering{
    @try {
        UINavigationController *nav = (UINavigationController*)self.presentingViewController;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            [nav popToRootViewControllerAnimated:YES];
        }];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}

- (void) setupBackgroundColor{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, self.vwGradient.frame.size.width, self.vwGradient.frame.size.height);

    gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(const_color_primary) CGColor],(id)[[UIColor colorWithRed:0.0f/255.0f
                                                                                                                         green:163.0f/255.0f
                                                                                                                          blue:158.0f/255.0f
                                                                                                                         alpha:0.55f] CGColor], nil];
    gradient.locations = @[@0.0, @0.5];
    [gradient setStartPoint:CGPointMake(0.0, 0.5)];
    [gradient setEndPoint:CGPointMake(1.0, 0.5)];
    self.vwGradient.layer.borderWidth = 0;
    
    UIGraphicsBeginImageContext(self.vwGradient.frame.size);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    self.vwGradient.backgroundColor = [UIColor colorWithPatternImage:outputImage];
    [UIColor colorWithRed:248.0f/255.0f
                    green:173.0f/255.0f
                     blue:60.0f/255.0f
                    alpha:0.55f];
    
    [self.vwGradient setAlpha:0.7f];
    self.vwGradient.layer.cornerRadius = 10;
    self.imgBg.layer.cornerRadius = 10;
    self.imgBg.layer.masksToBounds = YES;
    
    self.vwMasjid.layer.cornerRadius = 8;
    self.vwLocation.layer.cornerRadius = 8;
    
    self.lblDesc1.textColor = [UIColor whiteColor];
    self.lblTimer.textColor = [UIColor whiteColor];
    self.lblWaktuSholat.textColor = [UIColor whiteColor];
}

@end
