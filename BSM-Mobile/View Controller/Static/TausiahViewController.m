//
//  TausiahViewController.m
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "TausiahViewController.h"
#import "HomeViewController.h"
#import "Connection.h"
#import "Utility.h"
#import "TausiahCell.h"

#define FONT_SIZE 14.0f
#define CELL_CONTENT_MARGIN 23.0


@interface TausiahViewController ()<ConnectionDelegate,UITableViewDataSource,UIAlertViewDelegate>{
    NSArray *listData;
    NSString *menuTitle;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;

@end

@implementation TausiahViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if (menuTitle != nil) {
        self.lblTitle.text = menuTitle;
    }else{
        if([lang isEqualToString:@"id"]){
            _lblTitle.text = @"Hikmah";
        } else {
            _lblTitle.text = @"Quotes";
        }
    }

    [_lblTitle sizeToFit];
    
    [_tableView registerNib:[UINib nibWithNibName:@"TausiahCell" bundle:nil] forCellReuseIdentifier:@"TausiahCell"];
    
    [self setupLayout];
}

- (void)viewWillAppear:(BOOL)animated{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=tausiah" needLoading:true encrypted:false banking:false];
}

-(void) setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmTbl = self.tableView.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmTbl.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmTbl.origin.x = 0;
    frmTbl.size.width = SCREEN_WIDTH;
    frmTbl.size.height = SCREEN_HEIGHT - frmTbl.origin.y - BOTTOM_NAV;
    
    if ([Utility isDeviceHaveNotch]) {
        frmTbl.size.height = SCREEN_HEIGHT - frmTbl.origin.y - BOTTOM_NAV - 20;
    }
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.size.height = 40;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmVwTitle.size.width - (frmLblTitle.origin.x*2);
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.tableView.frame = frmTbl;
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TausiahCell *cell = (TausiahCell*)[tableView dequeueReusableCellWithIdentifier:@"TausiahCell"];

    NSString *text = [listData objectAtIndex:indexPath.row];
    cell.label.text = text;
    cell.imgIcon.image = [UIImage imageNamed:@"ic_alquranhikmahlist_hikmah"];
    return cell;
}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewAutomaticDimension;
//}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 100.0;
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{

    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
            listData = (NSArray *)jsonObject;
            [self.tableView reloadData];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    
    //[self startIdleTimer];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)reloadApp{
    [BSMDelegate reloadApp];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 404){
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    }
}

- (void)openStatic:(NSString *)templateName{
    RootViewController *templateView =  [self.storyboard instantiateViewControllerWithIdentifier:templateName];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}

-(void)handleTimer : (NSString *) modeStr{
    NSDictionary* userInfo = @{@"actionTimer": modeStr};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
//    [self handleTimer:@"STOP_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1113)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void) setTitleMenu : (NSString *) strTitle{
    menuTitle = strTitle;
}

@end
