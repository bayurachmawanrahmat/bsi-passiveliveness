//
//  JenisTabunganDynamicCell.h
//  BSM-Mobile
//
//  Created by ARS on 20/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol JenisTabunganDynamicCellDelegate;

@interface JenisTabunganDynamicCell : UITableViewCell

@property (weak, nonatomic) id<JenisTabunganDynamicCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *shortdesc;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UIButton *btnDetail;

@property (weak, nonatomic) NSString *feature;
@property (weak, nonatomic) NSString *longdesc;
@property (weak, nonatomic) NSString *kode;
@property (weak, nonatomic) NSString *tabunganid;

@end

@protocol JenisTabunganDynamicCellDelegate <NSObject>

@optional
- (void) didHitBtnSelect:(UIButton *)sender;
- (void) didHitBtnDetail:(JenisTabunganDynamicCell *)sender;
@end

NS_ASSUME_NONNULL_END
