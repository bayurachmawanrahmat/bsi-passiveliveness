//
//  LoginSlideViewController.m
//  BSM-Mobile
//
//  Created by BSM on 1/21/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "LoginSlideViewController.h"
#import "TemplateViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "Connection.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+MD5.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface LoginSlideViewController()<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *vwLoginBox;
@property (weak, nonatomic) IBOutlet UIImageView *imgBSMLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *bgScreen;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)checkLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEye;
- (IBAction)togglePwd:(id)sender;

@end

@implementation LoginSlideViewController

NSString *flagTPLSVC;

- (void)viewWillAppear:(BOOL)animated {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"Use Touch ID to Continue";
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    NSLog(@"Success Login with Touch / Face ID");
                                    [userDefault removeObjectForKey:@"hasLogin"];
                                    [userDefault setObject:@"YES" forKey:@"hasLogin"];
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Touch/Face ID success" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                        alertView.tag = 212;
                                        [alertView show];
                                    });
                                } else {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:error.description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                        [alertView show];
                                        // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
                                    });
                                }
                            }];
    } else {
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    flagTPLSVC = @"HIDE";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Kata Sandi";
    } else {
        self.lblTitle.text = @"Password";
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmBox = self.vwLoginBox.frame;
    CGRect frmScreen = self.bgScreen.frame;
    CGRect frmButton = self.btnLogin.frame;
    CGRect frmLogo = self.imgBSMLogo.frame;
    CGRect frmText = self.txtPassword.frame;
    CGRect frmLabel = self.lblTitle.frame;
    CGRect frmEye = self.btnEye.frame;
    
    frmScreen.size.height = screenHeight;
    frmScreen.size.width = screenWidth;
    frmBox.size.width = screenWidth - 32;
    frmButton.origin.y = frmBox.size.height - frmButton.size.height - 20;
    frmButton.origin.x = 10;
    frmButton.size.width = frmBox.size.width - 20;
    frmLogo.origin.x = frmBox.size.width - frmLogo.size.width - 5;
    frmLogo.origin.y = 5;
    frmText.origin.y = frmButton.origin.y - frmText.size.height - 10;
    frmText.origin.x = 10;
    frmText.size.width = frmBox.size.width - 20;
    frmLabel.origin.y = frmText.origin.y - frmLabel.size.height;
    frmLabel.origin.x = 10;
    frmEye.origin.x = screenWidth - 55 - frmEye.size.width;
    frmEye.origin.y = frmText.origin.y + 5;
    
    self.vwLoginBox.frame = frmBox;
    self.bgScreen.frame = frmScreen;
    self.btnLogin.frame = frmButton;
    self.imgBSMLogo.frame = frmLogo;
    self.txtPassword.frame = frmText;
    self.lblTitle.frame = frmLabel;
    self.btnEye.frame = frmEye;
    
    self.vwLoginBox.layer.borderColor = [UIColor blackColor].CGColor;
    self.vwLoginBox.layer.borderWidth = 3.0f;
    [self.vwLoginBox setClipsToBounds:YES];
    
    [self.txtPassword setSecureTextEntry:true];
}

- (IBAction)togglePwd:(id)sender {
    if ([flagTPLSVC isEqualToString:@"SHOW"]) {
        flagTPLSVC = @"HIDE";
        [self.txtPassword setSecureTextEntry:true];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPLSVC isEqualToString:@"HIDE"]) {
        flagTPLSVC = @"SHOW";
        [self.txtPassword setSecureTextEntry:false];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)checkLogin:(id)sender {
    NSString *pwd = self.txtPassword.text;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    NSString *upwd = [userDefault objectForKey:@"password"];
    NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];

    if ([[pwd MD5] isEqualToString:upwd]) {
        [userDefault removeObjectForKey:@"hasLogin"];
        [userDefault setObject:@"YES" forKey:@"hasLogin"];
        
        //self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        //[self.slidingViewController resetTopViewAnimated:NO];
        
        self.tabBarController.tabBar.hidden = NO;
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        NSString *err_msg = @"";
        if([lang isEqualToString:@"id"]){
            err_msg = @"Kata Sandi Salah";
        } else {
            err_msg = @"Wrong Password";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:err_msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 212) {
        //[self.view removeFromSuperview];
        
        self.parentViewController.tabBarController.tabBar.hidden = NO;
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
        
        /*int position = 313;
        NSDictionary* userInfo = @{@"position": @(position)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];*/
    }
}

@end
