//
//  JuzAmmaFilteringViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/24/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "JuzAmmaFilteringViewController.h"
#import "CellListSurah.h"
#import "Utility.h"

@interface JuzAmmaFilteringViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    BOOL isFilterSurah;
    NSString *lang;
    NSUserDefaults *userDefault;
    CGFloat mSizeTextPhone;
    NSArray *listSurah;
    NSArray *listSurahDefault;
}
@property (weak, nonatomic) IBOutlet UIView *vwMainTac;
@property (weak, nonatomic) IBOutlet UIButton *btnMainDismiss;
@property (weak, nonatomic) IBOutlet UIView *vwContentTac;
@property (weak, nonatomic) IBOutlet UIView *vwSearch;
@property (weak, nonatomic) IBOutlet UITableView *tblListSurah;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSurah;
@property (weak, nonatomic) IBOutlet UIView *vwLine;

@end

@implementation JuzAmmaFilteringViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if (IPHONE_5) {
        mSizeTextPhone = 12;
    }else{
        mSizeTextPhone = 15;
    }
    
    if ([lang isEqualToString:@"id"]) {
        self.txtFieldSurah.placeholder = @"Cari Ayat";
    }else{
        self.txtFieldSurah.placeholder = @"Find Verse";
    }
    
    self.txtFieldSurah.delegate = self;
    self.txtFieldSurah.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    isFilterSurah = false;
    listSurah  = [[NSArray alloc]init];
    listSurahDefault = [[NSArray alloc]init];
    

    [self setupLayout];
    
    
    self.tblListSurah.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    NSString *mSurahs = @"78//An Naba’//Berita Besar//The Announcement//40//23||79//An Naazi’at//Malaikat-malaikat Yang Mencabut//Those who drag forth//46//22||80//‘Abasa//Ia Bermuka Masam//He frowned//42//20||81//At Takwir//Menggulung//The Overthrowing//29//19||82//Al Infithar//Terbelah//The Cleaving//19//18||83//Al Muthaffifin//Orang-orang Yang Curang//Defrauding//36//18||84//Al Insyiqaq//Terbelah//The Splitting Open//25//16||85//Al Buruuj//Gugusan Bintang//The Constellations//22//15||86//Ath Thariq//Yang Datang Dimalam Hari//The Night Comer//17//14||87//Al A’laa//Yang Paling Tinggi//The Most High//19//14||88//Al Ghaasyiah//Hari Pembalasan//The Overwhelming//26//13||89//Al Fajr//Fajar//The Dawn//30//12||90//Al Balad//Negeri//The City//20//11||91//Asy Syams//Matahari//The Sun//15//10||92//Al Lail//Malam//The Night//21//10||93//Adh Dhuhaa//Waktu matahari sepenggalahan naik (Dhuha)//The Morning Hours//11//9||94//Asy Syarh//Melapangkan//The Consolation//8//9||95//At Tiin//Buah Tin//The Fig//8//8||96//Al ‘Alaq//Segumpal Darah//The Clot//19//8||97//Al Qadr//Kemuliaan//The Power, Fate//5//7||98//Al Bayyinah//Pembuktian//The Evidence//8//7||99//Az Zalzalah//Kegoncangan//The Earthquake//8//6||100//Al ‘Aadiyah//Berlari kencang//The Chargers//11//6||101//Al Qaari’ah//Hari Kiamat//The Calamity//11//5||102//At Takaatsur//Bermegah-megahan//Competition//8//5||103//Al ‘Ashr//Masa (Waktu)//The Declining Day//3//4||104//Al Humazah//Pengumpat//The Traducer//9//4||105//Al Fiil//Gajah//The Elephant//5//4||106//Quraisy//Suku Quraisy//Quraysh//4//3||107//Al Maa’uun//Barang-barang yang berguna//Almsgiving//7//3||108//Al Kautsar//Nikmat yang berlimpah//Abundance//3//3||109//Al Kafirun//Orang-orang kafir//The Disbelievers//6//2||110//An Nashr//Pertolongan//Divine Support//3//2||111//Al Lahab//Gejolak Api//The Father of Flame//5//2||112//Al Ikhlash//Ikhlas//Sincerity//4//1||113//Al Falaq//Waktu Subuh//The Dawn//5//1||114//An Naas//Manusia//Mankind//6//1";
    
    NSArray *mSplitSurah = [mSurahs componentsSeparatedByString:@"||"];
    NSMutableArray *mDictSurahs = [NSMutableArray new];
    if ([mSplitSurah count] != 0) {
        for (int i=0; i<[mSplitSurah count]; i++) {
            NSArray *mSplitDtlSurah = [[mSplitSurah objectAtIndex:i]componentsSeparatedByString:@"//"];
            if ([mSplitDtlSurah count] !=0) {
                NSMutableDictionary *objSurah = [NSMutableDictionary new];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:0] forKey:@"surah_no"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:1] forKey:@"ayat_name"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:2] forKey:@"meaning_id"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:3] forKey:@"meaning_en"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:4] forKey:@"total_surah"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:5] forKey:@"idx"];
                [mDictSurahs addObject:objSurah];
            }
            
        }
    }
    
    listSurahDefault = (NSArray *) mDictSurahs;
    listSurah = (NSArray *) mDictSurahs;
    
    self.tblListSurah.delegate = self;
    self.tblListSurah.dataSource = self;
    [self.tblListSurah setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.tblListSurah beginUpdates];
    [self.tblListSurah reloadData];
    [self.tblListSurah endUpdates];
    
    [self.btnMainDismiss addTarget:self action:@selector(actionDismiss:) forControlEvents:UIControlEventTouchUpInside];
    
//    [self handleTimer:@"STOP_TIMER"];
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
          NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
          [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    [self.tblListSurah beginUpdates];
    [self.tblListSurah endUpdates];
}

-(void)setupLayout{
    CGRect frmVwMainTac = self.vwMainTac.frame;
    CGRect frmBtnMainDissmiss = self.btnMainDismiss.frame;
    CGRect frmVwContentTac = self.vwContentTac.frame;
    CGRect frmVwSearch = self.vwSearch.frame;
    CGRect frmTblListSurah = self.tblListSurah.frame;
    
    CGRect frmImgIcon = self.imgIcon.frame;
    CGRect frmTxtField = self.txtFieldSurah.frame;
    CGRect frmVwLine = self.vwLine.frame;

    
    frmVwMainTac.origin.x = 0;
    frmVwMainTac.origin.y = 0;
    frmVwMainTac.size.width = SCREEN_WIDTH;
    frmVwMainTac.size.height = SCREEN_HEIGHT;
    
    frmBtnMainDissmiss.origin.x = frmVwMainTac.origin.x;
    frmBtnMainDissmiss.origin.y = frmVwMainTac.origin.y;
    frmBtnMainDissmiss.size.width = frmVwMainTac.size.width;
    frmBtnMainDissmiss.size.height = frmVwMainTac.size.height;
    
    frmVwContentTac.origin.x = 16;
    frmVwContentTac.origin.y = (frmVwMainTac.size.height * 15)/100;
    frmVwContentTac.size.width = frmVwMainTac.size.width - (frmVwContentTac.origin.x *2);
    frmVwContentTac.size.height = frmVwMainTac.size.height - frmVwContentTac.origin.y - BOTTOM_NAV - 20;
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveBottomPadding]) {
        frmVwContentTac.size.height = frmVwMainTac.size.height - frmVwContentTac.origin.y - BOTTOM_NAV - 40;
    }
    
    frmVwSearch.origin.x = 0;
    frmVwSearch.origin.y = 0;
    frmVwSearch.size.width = frmVwContentTac.size.width;
    
    frmTxtField.origin.x = frmImgIcon.origin.x + frmImgIcon.size.width + 8;
    frmTxtField.size.width = frmVwSearch.size.width - frmTxtField.origin.x - 8;
    
    frmVwLine.origin.x = frmImgIcon.origin.x;
    frmVwLine.size.width = frmVwSearch.size.width - frmVwLine.origin.x - 8;
    
    
    frmTblListSurah.origin.y = frmVwSearch.origin.y + frmVwSearch.size.height + 8;
    frmTblListSurah.origin.x = 0;
    frmTblListSurah.size.width = frmVwContentTac.size.width;
    frmTblListSurah.size.height = frmVwContentTac.size.height - frmTblListSurah.origin.y;
    
    
    self.vwMainTac.frame = frmVwMainTac;
    self.btnMainDismiss.frame = frmBtnMainDissmiss;
    self.vwContentTac.frame = frmVwContentTac;
    self.vwSearch.frame = frmVwSearch;
    self.tblListSurah.frame = frmTblListSurah;
    self.txtFieldSurah.frame = frmTxtField;
    self.vwLine.frame = frmVwLine;
    
    self.vwContentTac.layer.cornerRadius = 10.0f;
    self.vwContentTac.layer.shadowColor = [[UIColor grayColor]CGColor];
    self.vwContentTac.layer.shadowOffset = CGSizeMake(0, 2);
    self.vwContentTac.layer.shadowOpacity = 0.5f;
    self.vwContentTac.backgroundColor = [UIColor whiteColor];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listSurah count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellListSurah *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if(cell == nil){
        cell = [[CellListSurah alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    
    NSDictionary *dataSurah = [listSurah objectAtIndex:indexPath.row];
    [cell.lblSurahName setText:[dataSurah valueForKey:@"ayat_name"]];
    if ([lang isEqualToString:@"id"]) {
         [cell.lblTranslateSurah setText:[NSString stringWithFormat:@"%@ Ayat | %@", [dataSurah valueForKey:@"total_surah"],[dataSurah valueForKey:@"meaning_id"]]];
    }else{
        [cell.lblTranslateSurah setText:[NSString stringWithFormat:@"%@ Ayat | %@", [dataSurah valueForKey:@"total_surah"],[dataSurah valueForKey:@"meaning_en"]]];
    }
   
    
    [cell.lblSurahName setFont:[UIFont fontWithName:const_font_name3 size:mSizeTextPhone]];
    [cell.lblTranslateSurah setFont:[UIFont fontWithName:const_font_name1 size:mSizeTextPhone]];
    
    
    [cell.lblSurahName sizeToFit];
    
    [cell.lblTranslateSurah sizeToFit];
    cell.lblTranslateSurah.numberOfLines = 2;
    
    CGRect frmLblSurahName =  cell.lblSurahName.frame;
    CGRect frmLblTransSurah = cell.lblTranslateSurah.frame;
    
    frmLblSurahName.origin.x = 16;
    frmLblSurahName.origin.y = 16;
    frmLblSurahName.size.width = cell.frame.size.width - (frmLblSurahName.origin.x *2);
    
    frmLblTransSurah.origin.x = frmLblSurahName.origin.x;
    frmLblTransSurah.origin.y = frmLblSurahName.origin.y + frmLblSurahName.size.height + 5;
    frmLblTransSurah.size.width = frmLblSurahName.size.width;
    
    cell.lblSurahName.frame = frmLblSurahName;
    cell.lblTranslateSurah.frame = frmLblTransSurah;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat actualWidth = SCREEN_WIDTH - 32;
    CGFloat paddingLeftAndRight = 16;
    CGFloat paddingTopAndBottom = 16;
    
    NSDictionary *dataDict = [listSurah objectAtIndex:indexPath.row];
    
    CGSize mSzSurahName = [[dataDict valueForKey:@"ayat_name"] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:const_font_name3 size:mSizeTextPhone]}];
    
    CGSize mSzTranslateSurah = [[NSString stringWithFormat:@"%@ Ayat | %@", [dataDict valueForKey:@"total_surah"],[dataDict valueForKey:@"meaning_id"]] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:const_font_name1 size:mSizeTextPhone]}];
    
    CGFloat constrainHeight = 2000 + mSzSurahName.height +mSzTranslateSurah.height;
    
    CGSize constraint = CGSizeMake(actualWidth -(paddingLeftAndRight *2),constrainHeight);
    
    CGSize sizeTextArab = [Utility heightWrapWithText:[dataDict valueForKey:@"ayat_name"]
                                               fontName:[UIFont fontWithName:const_font_name3 size:mSizeTextPhone] expectedSize:constraint];
    CGSize sizeTextTranslater = [Utility heightWrapWithText:[NSString stringWithFormat:@"%@ Ayat | %@", [dataDict valueForKey:@"total_surah"],[dataDict valueForKey:@"meaning_id"]]
                                               fontName:[UIFont fontWithName:const_font_name1 size:mSizeTextPhone] expectedSize:constraint];
    CGFloat mTotalHeight = (paddingTopAndBottom * 2) + ceil(sizeTextArab.height) + 5 + ceil(sizeTextTranslater.height) + 8;
    CGFloat mHeight = MAX(mTotalHeight, 50);
    
    return mHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
          NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
          [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSDictionary *mDataSurah = [listSurah objectAtIndex:indexPath.row];
    userInfo = @{@"idx": [mDataSurah valueForKey:@"idx"],
                               @"ayat_name" : [mDataSurah valueForKey:@"ayat_name"],
                               @"total_surah" : [mDataSurah valueForKey:@"total_surah"]
    };
    [self dismissViewControllerAnimated:YES completion:nil];
    nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"filterSurah" object:self userInfo:userInfo];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *searchText = [NSString stringWithString:textField.text];
    searchText = [searchText
                 stringByReplacingCharactersInRange:range withString:string];
    if ([searchText length] > 0) {
        isFilterSurah = true;
//        [_mSurahFilter removeAllObjects];
        NSMutableArray *listFilterSurah = [[NSMutableArray alloc]init];
        for(NSDictionary *xDictSurahs in listSurah){
            NSString *surahNo = [[xDictSurahs valueForKey:@"surah_no"] lowercaseString];
            NSString *nameSurah = [[xDictSurahs valueForKey:@"ayat_name"]lowercaseString];
            NSString *totalSurah = [[xDictSurahs valueForKey:@"total_surah"]lowercaseString];
            NSString *meaning;
            if ([lang isEqualToString:@"id"]) {
                meaning = [[xDictSurahs valueForKey:@"meaning_id"]lowercaseString];
            }else{
                meaning = [[xDictSurahs valueForKey:@"meaning_en"]lowercaseString];
            }
            
            NSRange nmRangeSurahNo = [surahNo rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            NSRange nmRange = [nameSurah rangeOfString:searchText options:NSCaseInsensitiveSearch];

            NSRange nmRangeTotalSurah = [totalSurah rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            NSRange nmRangeMeaning = [meaning rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (nmRangeSurahNo.location != NSNotFound) {
               [listFilterSurah addObject:xDictSurahs];
            }

            else if(nmRange.location != NSNotFound){
                [listFilterSurah addObject:xDictSurahs];
            }
            else if (nmRangeTotalSurah.location != NSNotFound) {
                [listFilterSurah addObject:xDictSurahs];
            }
            else if (nmRangeMeaning.location != NSNotFound) {
                [listFilterSurah addObject:xDictSurahs];
            }
        }
        listSurah = (NSArray *) listFilterSurah;
        [_tblListSurah reloadData];
        
    }else{
        isFilterSurah = false;
    }
    
    if(!isFilterSurah){
        listSurah = (NSArray *)listSurahDefault;
        [_tblListSurah reloadData];
    }
    [_tblListSurah beginUpdates];
    [_tblListSurah endUpdates];
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
          NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
          [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
          NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
          [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    [textField setText:@""];
    isFilterSurah = false;
    listSurah = (NSArray *)listSurahDefault;
    [_tblListSurah reloadData];
    [_tblListSurah beginUpdates];
    [_tblListSurah endUpdates];
    return NO;
}

-(void)actionDismiss:(UIButton *) sender{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
          NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
          [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)handleTimer : (NSString *) modeStr{
//    NSDictionary* userInfo = @{@"actionTimer": modeStr};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
          NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
          [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
