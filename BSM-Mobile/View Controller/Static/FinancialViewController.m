//
//  FinancialViewController.m
//  BSM-Mobile
//
//  Created by lds on 5/26/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import "FinancialViewController.h"
#import "Connection.h"
#import <WebKit/WebKit.h>

@interface FinancialViewController ()
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation FinancialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     _WebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 80, 320, 440)];
    
     NSString *fullURL = @"https://www.syariahmandiri.co.id/";
     NSURL *url = [NSURL URLWithString:fullURL];
     NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
     [_WebView loadRequest:requestObj];
    [self.view addSubview:_WebView];
    
    // Connection *conn = [[Connection alloc]initWithDelegate:self];
    // [conn sendPostParmUrl:@"request_type=financial" needLoading:true encrypted:false banking:false];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/*
 
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
 return listData.count;
 }
 
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TausiahCell" forIndexPath:indexPath];
 UILabel *label = (UILabel *)[cell viewWithTag:1];
 
 label.text=[listData objectAtIndex:indexPath.row];
 [label sizeToFit];
 
 return cell;
 }
 
 #pragma mark - ConnectionDelegate
 - (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
 if([jsonObject isKindOfClass:[NSDictionary class]]){
 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
 [alert show];
 }else{
 listData = (NSArray *)jsonObject;
 [self.tableView reloadData];
 }
 }
 
 - (void)errorLoadData:(NSError *)error{
 
 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
 [alert show];
 }
 
 - (void)reloadApp{
 [BSMDelegate reloadApp];
 }
 */

@end
