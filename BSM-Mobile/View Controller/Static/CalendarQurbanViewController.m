//
//  CalendarQurbanViewController.m
//  BSM-Mobile
//
//  Created by ARS on 09/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CalendarQurbanViewController.h"
#import "Styles.h"

@interface CalendarQurbanViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>{
    NSArray *lisData, *listDate, *calendarDate;
    int position;
    NSString *language, *selectedDateQurban;
    NSUserDefaults *userDefaults;
    UIToolbar *toolbar;
    UIPickerView *datePickerView;
    
}
@property (weak, nonatomic) IBOutlet UILabel *labelTarget;
@property (weak, nonatomic) IBOutlet UILabel *labelMonth;
@property (weak, nonatomic) IBOutlet UIButton *buttonDown;
@property (weak, nonatomic) IBOutlet UIButton *buttonUp;
@property (weak, nonatomic) IBOutlet UITextField *tfTarget;
@property (weak, nonatomic) IBOutlet UILabel *labelSunday;
@property (weak, nonatomic) IBOutlet UILabel *labelMonday;
@property (weak, nonatomic) IBOutlet UILabel *labelTuesday;
@property (weak, nonatomic) IBOutlet UILabel *labelWednesday;
@property (weak, nonatomic) IBOutlet UILabel *labelThrusday;
@property (weak, nonatomic) IBOutlet UILabel *labelFriday;
@property (weak, nonatomic) IBOutlet UILabel *labelSaturday;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heighView;
@property (weak, nonatomic) IBOutlet UIView *calendarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation CalendarQurbanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if(@available(iOS 13.0, *)){
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    self.contentView.layer.shadowColor = [[UIColor grayColor]CGColor];
    self.contentView.layer.shadowOpacity = 2.0f;
    self.contentView.layer.shadowRadius = 7.0;
    self.contentView.layer.masksToBounds = NO;
    self.contentView.layer.cornerRadius = 5.0f;
    [self.contentView setUserInteractionEnabled:YES];
    [self.view setUserInteractionEnabled:YES];
        
    datePickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    datePickerView.showsSelectionIndicator = YES;
    datePickerView.delegate = self;
    datePickerView.dataSource = self;
    
//    [Styles setStyleTextFieldBottomLineWithRightImage:self.tfTarget imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBorderWithRightImage:self.tfTarget imageNamed:@"baseline_keyboard_arrow_down_black_24pt" andRounded:NO];

    
    [self createToolbar];
    [self labelDays];
    self.tfTarget.inputView = datePickerView;
    self.tfTarget.inputAccessoryView = toolbar;
    [self.buttonUp addTarget:self action:@selector(buttonUpDownHanlder:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonDown addTarget:self action:@selector(buttonUpDownHanlder:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.tfTarget setText:lisData[0]];
    [self createCalendar:lisData[0]];
    [self changeLabelMonth:0];
    selectedDateQurban = lisData[0];
    position = 0;
//    [delegate selectedDate:lisData[0]];
    
    if([language isEqualToString:@"id"]){
        self.labelTarget.text = @"Target Waktu Qurban";
        [self.tfTarget setPlaceholder:@"Pilih Tanggal"];

    }else{
        self.labelTarget.text = @"Qurban Time Target";
        [self.tfTarget setPlaceholder:@"Select Date"];
    }

//    UITapGestureRecognizer *tapDismiss =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(dismissPopUp:)];
//    [self.view addGestureRecognizer:tapDismiss];
    
}

//-(void)dismissPopUp:(UITapGestureRecognizer *)recognizer{
////    [delegate selectedDate:selectedDateQurban];
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

-(void)dismissPopUp{
    [delegate selectedDate:selectedDateQurban];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) createToolbar{
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    
    NSString *toolbarTitle = @"";
    if([language isEqualToString:@"id"]){
        toolbarTitle = @"selesai";
    }else{
        toolbarTitle = @"done";
    }
    
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:toolbarTitle style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (IBAction)buttonUpDownHanlder:(id)sender{
    if(sender == _buttonUp){
        position = position - 1;
        if(position > -1){
            _tfTarget.text = lisData[position];
//            self.tfTarget.text = @"";
            [self changeCalendar:position];
        }else{
            position = 0;
        }
    }
    if(sender == _buttonDown){
        position = position + 1;
        if(position < lisData.count){
            _tfTarget.text = lisData[position];
//            self.tfTarget.text = @"";
            [self changeCalendar:position];
        }else{
            position = (int)lisData.count-1;
        }
        
    }
}

- (void) createCalendar : (NSString*) dateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
    
    int dayOfQurban = (int)dateComponents.weekday;
    NSDate *dateToShow = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                  value:-(28+(long)dateComponents.weekday)
                                                                 toDate:date
                                                                options:0];

    dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth fromDate: dateToShow];
    int colorState = 0;
    UIColor *color;
    CGFloat size = (SCREEN_WIDTH-18)/7;
    NSMutableArray *calArrTemp = [[NSMutableArray alloc]init];

    int idx = 0;
    if(colorState == 0){
//                    color = [UIColorFromRGB(const_color_gray) colorWithAlphaComponent:0.3];
        color = [UIColor whiteColor];
        colorState = 1;
    }
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 7; j++){
            [dateComponents setDay:([dateComponents day] + 1)];
            dateToShow  = [calendar dateFromComponents:dateComponents];
            dateComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth fromDate: dateToShow];
            
            UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake((j*size), i*size, size, size)];
            dateLabel.tag = idx;
            [calArrTemp addObject:dateToShow];
            
            if(dateComponents.day == 1 || (j==0 && i==0)){
                dateLabel.text = [NSString stringWithFormat:@"%ld/%ld",(long)dateComponents.day,(long)dateComponents.month];
                NSLog(@"%@",dateLabel.text);
//                if(colorState == 0){
////                    color = [UIColorFromRGB(const_color_gray) colorWithAlphaComponent:0.3];
//                    color = [UIColor whiteColor];
//                    colorState = 1;
//                }
//                else{
//                    color = [UIColorFromRGB(const_color_gray) colorWithAlphaComponent:0.3];
////                    color = [UIColor whiteColor];
//                    colorState = 0;
//                }
            }else{
                dateLabel.text = [NSString stringWithFormat:@"%ld",(long)dateComponents.day];
                NSLog(@"%@",dateLabel.text);
            }
            dateLabel.numberOfLines = 0;
            dateLabel.font = [UIFont fontWithName:@"Lato-Regular" size:11];
            dateLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
            dateLabel.textColor = [UIColor blackColor];
            dateLabel.textAlignment = NSTextAlignmentLeft;
            [dateLabel setAccessibilityLabel:@"NO"];
            [dateLabel sizeToFit];
            
            if(j==(dayOfQurban-1) && i==4){
                dateLabel.font = [UIFont fontWithName:@"Lato-Regular" size:11];
                
                UILabel *iedLabel = [[UILabel alloc]initWithFrame:CGRectMake((j*size), i*size, size, size)];
                iedLabel.text = [NSString stringWithFormat:@"Idul Qurban\n1444 H"];
                iedLabel.numberOfLines = 0;
                iedLabel.font = [UIFont fontWithName:@"Lato-Regular" size:10];
                iedLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
                iedLabel.textColor = [UIColor blackColor];
                iedLabel.textAlignment = NSTextAlignmentCenter;
                iedLabel.backgroundColor = UIColorFromRGB(const_color_secondary);
                [iedLabel setAccessibilityLabel:@"YES"];
                iedLabel.tag = idx;
                [self.calendarView addSubview:iedLabel];
                color = [UIColorFromRGB(const_color_gray) colorWithAlphaComponent:0.3];
//                    color = [UIColor whiteColor];
                colorState = 0;
            }else{
//                UIView *dateView = [[UIView alloc]initWithFrame:CGRectMake((j*size), i*size, size, size)];
//                UIButton *dateView = [[UIButton alloc]initWithFrame:CGRectMake((j*size), i*size, size, size)];
                UIButton *dateView = [UIButton buttonWithType:UIButtonTypeCustom];
                dateView.frame = CGRectMake((j*size), i*size, size, size);
                [dateView setBackgroundColor:color];
                [dateView.layer setBorderWidth:.3];
                dateView.tag = idx;
                [dateView setAccessibilityLabel:@"NO"];
                if(color == [UIColor whiteColor]){
//                    [dateView addGestureRecognizer:tap];
                    [dateView setUserInteractionEnabled:YES];
                    [dateView setAccessibilityLabel:@"YES"];
                    [dateView.layer setBorderColor:[UIColorFromRGB(const_color_primary)CGColor]];
//                    dateView addges
//                    dateView.tag = dateLabel.tag;
                }
                [self.calendarView addSubview:dateView];
            }
            [self.calendarView addSubview:dateLabel];
            idx = idx + 1;
        }
        UIView *barView = [[UIView alloc]initWithFrame:CGRectMake(0, i*size, size*7, size)];
        barView.layer.backgroundColor = [[UIColor clearColor]CGColor];
        barView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        barView.layer.borderWidth = 0.5;
        [self.calendarView addSubview:barView];
    }
//    [self.calendarView setUserInteractionEnabled:YES];
//    for(UIView* btn in [self.calendarView subviews]){
//        if([btn isKindOfClass:[UIButton class]]){
//            UIButton *btns = (UIButton*)btn;
//            [btns addTarget:self action:@selector(handleTap:) forControlEvents:UIControlEventTouchUpInside];
//        }
//    }
    _heighView.constant = 5*size;
    calendarDate = calArrTemp;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return lisData.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return lisData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self changeCalendar:row];
    position = (int)row;
    selectedDateQurban = lisData[row];
}

- (void) changeCalendar : (NSInteger)row{
    [[self.calendarView subviews]makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self createCalendar:lisData[row]];
    [self.tfTarget setText:lisData[row]];
    [self changeLabelMonth:row];
    [datePickerView reloadAllComponents];
}

- (void) changeLabelMonth : (NSInteger)row{
    NSString *dateString = lisData[row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
    
    [dateFormatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:language?@"en":@"id"]];
    NSString *month = [[dateFormatter monthSymbols] objectAtIndex:dateComponents.month -1];
    
    [UIView transitionWithView:self.labelMonth
                      duration:.5
       options:UIViewAnimationOptionTransitionCrossDissolve
    animations:^{
                    [self.labelMonth setText:[NSString stringWithFormat:@"%@ %ld",month, (long)dateComponents.year]];
                }
    completion:NULL];
}

- (void)setDateList:(NSArray *)dateQurbanList{
    lisData = dateQurbanList;
    [self createDateList];
}

- (void) setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void) labelDays
{
    if([language isEqualToString:@"id"]){
        self.labelSunday.text = @"Minggu";
        self.labelMonday.text = @"Senin";
        self.labelTuesday.text = @"Selasa";
        self.labelWednesday.text = @"Rabu";
        self.labelThrusday.text = @"Kamis";
        self.labelFriday.text = @"Jumat";
        self.labelSaturday.text = @"Sabtu";
    }
}

- (void) createDateList{
    NSMutableArray *list = [[NSMutableArray alloc]init];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    for(NSString* dateString in lisData){
        NSDate *date = [dateFormatter dateFromString:dateString];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        NSDateComponents * dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
        
        NSDate *dateToShow = date;
        NSInteger theDate = dateComponents.day;
        [dateComponents setDay:(dateComponents.day - 1)];
            
        dateToShow  = [calendar dateFromComponents:dateComponents];
        dateComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth fromDate: dateToShow];
        
        NSMutableArray *listDates = [[NSMutableArray alloc]init];
        while(theDate != dateComponents.day){
            [listDates addObject:dateToShow];
            [dateComponents setDay:(dateComponents.day - 1)];
                
            dateToShow  = [calendar dateFromComponents:dateComponents];
            dateComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth fromDate: dateToShow];
        }
        
        [list addObject:listDates];
    }
    listDate = list;
}

- (NSString*) dateToString:(NSDate*) date{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *retur = [dateFormatter stringFromDate:date];
    
    return retur;
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    CGPoint locationPoint1 = [[touches anyObject] locationInView:self.view];
    CGPoint viewPoint1 = [self.calendarView convertPoint:locationPoint1 fromView:self.view];
    if ([self.calendarView pointInside:viewPoint1 withEvent:event]){
        CGPoint locationPoint = [[touches anyObject] locationInView:self.calendarView];

        for(UIView *v in self.calendarView.subviews){

            if([v isKindOfClass:[UIButton class]]){
                UIButton *btn = (UIButton*)v;
                CGPoint viewPoint = [btn convertPoint:locationPoint fromView:self.calendarView];

                if ([btn pointInside:viewPoint withEvent:event]) {
                   // do something
                    if([btn.accessibilityLabel isEqualToString:@"YES"]){
                        NSLog(@"do Something %ld",(long)btn.tag);
                        NSLog(@"%@",[calendarDate objectAtIndex:btn.tag]);
                        [btn setBackgroundColor:[UIColorFromRGB(const_color_primary) colorWithAlphaComponent:0.3]];
                        NSString*dateSlect = [self dateToString:[calendarDate objectAtIndex:btn.tag]];
                        selectedDateQurban = dateSlect;
                        [delegate selectedDate:dateSlect];
                    }
                }else{
                    if([btn.accessibilityLabel isEqualToString:@"YES"]){
                        [btn setBackgroundColor:[UIColor whiteColor]];
                    }else{
                        [btn setBackgroundColor:[UIColorFromRGB(const_color_gray) colorWithAlphaComponent:0.3]];
                    }
                }
            }
            
            if([v isKindOfClass:[UILabel class]]){
                UILabel *btn = (UILabel*)v;
                CGPoint viewPoint = [btn convertPoint:locationPoint fromView:self.calendarView];

                if ([btn pointInside:viewPoint withEvent:event]) {
                   // do something
                    if([btn.accessibilityLabel isEqualToString:@"YES"]){
                        NSLog(@"do Something %ld",(long)btn.tag);
                        NSLog(@"%@",[calendarDate objectAtIndex:btn.tag]);
//                        selectedDateQurban
                        NSString*dateSlect = [self dateToString:[calendarDate objectAtIndex:btn.tag]];
                        selectedDateQurban = dateSlect;
                        [delegate selectedDate:dateSlect];
                    }
                }
            }
        }
    }
    
    CGPoint locationPoint2 = [[touches anyObject] locationInView:self.view];
    CGPoint viewPoint2 = [self.contentView convertPoint:locationPoint2 fromView:self.view];
    if(![self.contentView pointInside:viewPoint2 withEvent:event]){
        [self dismissPopUp];
    }
}
@end
