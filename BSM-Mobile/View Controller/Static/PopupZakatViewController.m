//
//  PopupZakatViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/18/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PopupZakatViewController.h"
#import "JVFloatLabeledTextField.h"
#import "Utility.h"
#import "DataManager.h"

@interface PopupZakatViewController ()<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource, UIAlertViewDelegate, UIScrollViewDelegate, PopZakatDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang, *selectedAccount;
    CGFloat heightFrame;
    NSDictionary *data;
    NSArray *arTtlLabel;
    NSNumberFormatter* currencyFormatter;
    UIToolbar *toolBar;
    NSDictionary *dataContent;
    NSArray *arListAccount;
    DataManager *dataManager;
}
@property (weak, nonatomic) IBOutlet UIView *vwMainTac;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContentScrol;
@property (weak, nonatomic) IBOutlet UIView *vwContentTAC;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnExit;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleZakat;
@property (weak, nonatomic) IBOutlet UILabel *lblNominalZakat;
@property (weak, nonatomic) IBOutlet UIView *vwLineSeparator;
@property (weak, nonatomic) IBOutlet UIButton *btnBayar;
@property (weak, nonatomic) IBOutlet UIView *vwListTabungan;
@property (weak, nonatomic) IBOutlet UITextField *txtAccountTabungan;
@property (weak, nonatomic) IBOutlet UIImageView *ic_arrow_down;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UILabel *lblRekSumberDana;

@end

@implementation PopupZakatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    dataManager = [DataManager sharedManager];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    arListAccount = [[NSArray alloc]init];
    selectedAccount = @"";
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
//    currencyFormatter.maximumSignificantDigits = 0;
    
    [self.btnExit addTarget:self action:@selector(actionExitListener:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *ttlToolbar;
    if ([lang isEqualToString:@"id"]) {
        [self.lblTitle setText:@"Kalkulator Zakat"];
        ttlToolbar = @"Selesai";
        self.txtAccountTabungan.placeholder = @"Pilih Nomor Rekening";
        [self.btnBayar setTitle:@"BAYAR ZAKAT" forState:UIControlStateNormal];
        self.lblRekSumberDana.text = @"Rekening Sumber Dana";
    }else{
        [self.lblTitle setText:@"Zakat Calculator"];
        ttlToolbar = @"Done";
        self.txtAccountTabungan.placeholder = @"Select Account Number";
        [self.btnBayar setTitle:@"PAY ZAKAT" forState:UIControlStateNormal];
        self.lblRekSumberDana.text = @"Source of Funds";
    }
    
    
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    [self.lblNominalZakat setText:@"Rp. 0"];
    [self.lblNominalZakat setTextColor:[UIColor blackColor]];
    
    [self.lblTitle setFont:[UIFont fontWithName:@"Lato-Bold" size:16]];
    [self.lblNominalZakat setFont:[UIFont fontWithName:@"Lato-Bold" size:15]];
    [self.lblTitleZakat setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];

    [self.lblRekSumberDana sizeToFit];
    
    heightFrame = SCREEN_HEIGHT - TOP_NAV - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveBottomPadding]) {
        heightFrame = SCREEN_HEIGHT - TOP_NAV - BOTTOM_NAV_NOTCH;
        
        [self.lblTitle setFont:[UIFont fontWithName:@"Lato-Bold" size:18]];
        [self.lblNominalZakat setFont:[UIFont fontWithName:@"Lato-Bold" size:17]];
        [self.lblTitleZakat setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
    }
    
    [self setupLayout];
    
    if (data.allKeys.count > 0) {
        dataContent = [data valueForKey:@"data_content"];
        arListAccount = [data valueForKey:@"list_account"];
        [self.lblTitleZakat setText: [dataContent valueForKey:@"name"]];
        [self addingDynamicView:[[dataContent valueForKey:@"id"]integerValue]];
        [self setupRelocationPosition];
        [self OptiimizePosition];
        
    }
    
    
    UIPickerView *objPickerAccount = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    objPickerAccount.showsSelectionIndicator = YES;
    objPickerAccount.delegate = self;
    objPickerAccount.dataSource = self;
    
    self.txtAccountTabungan.inputView = objPickerAccount;
    self.txtAccountTabungan.inputAccessoryView = toolBar;
    
    if (arListAccount.count == 0 || arListAccount == nil) {
        self.txtAccountTabungan.enabled = NO;
    }else{
        self.txtAccountTabungan.enabled = YES;
    }
    
    [self.btnBayar addTarget:self action:@selector(actionBayar:) forControlEvents:UIControlEventTouchUpInside];
    
    _vwScroll.delegate = self;
    
    
    [Styles addShadow:self.view];
}


-(void) dataBundle : (NSDictionary *) dataBind{
    data = dataBind;
}


-(void) addingDynamicView : (NSInteger) mId{
    switch (mId) {
        case 1:
            if ([lang isEqualToString:@"id"]) {
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Pendapatan Perbulan (Wajib)"},
                               @{@"state" : @"0", @"name" : @"Pendapatan Lain (Optional)"},
                               @{@"state" : @"0", @"name" : @"Hutang/Cicilan (Optional)"},
                               @{@"state" : @"2", @"name" : @"Harga Beras saat ini (per kg)"},
                               @{@"state" : @"3", @"name" : @"Nishab (85 kg beras)"},
                               @{@"state" : @"5", @"name" : @"Wajib zakat profesi"}];
            }else{
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Revenue Per Month (Required)"},
                               @{@"state" : @"0", @"name" : @"Other Revenue (Optional)"},
                               @{@"state" : @"0", @"name" : @"Debt / Installments (Optional)"},
                               @{@"state" : @"2", @"name" : @"Current Rice Price (per kg)"},
                               @{@"state" : @"3", @"name" : @"Nishab (85 kg of rice)"},
                               @{@"state" : @"5", @"name" : @"Obligatory to profession alms"}];
            }
            
            break;
        case 2:
            if ([lang isEqualToString:@"id"]) {
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Modal yang diputar selama 1 Tahun"},
                               @{@"state" : @"1", @"name" : @"Keuntungan selama setahun"},
                               @{@"state" : @"0", @"name" : @"Piutang dagang"},
                               @{@"state" : @"0", @"name" : @"Hutang jatuh tempo"},
                               @{@"state" : @"0", @"name" : @"Kerugian selama setahun"},
                               @{@"state" : @"6", @"name" : @"Harga emas saat ini (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 gram)"},
                               @{@"state" : @"5", @"name" : @"Wajib zakat perdagangan"}
                               ];
            }else{
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Capital played for 1 Year"},
                               @{@"state" : @"1", @"name" : @"Profit for one year"},
                               @{@"state" : @"0", @"name" : @"Account receivable"},
                               @{@"state" : @"0", @"name" : @"Debt due"},
                               @{@"state" : @"0", @"name" : @"Loss for one year"},
                               @{@"state" : @"6", @"name" : @"Current gold price (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 grams)"},
                               @{@"state" : @"5", @"name" : @"Obligatory to trade alms"}
                               ];
            }
            break;
        case 3:
            if ([lang isEqualToString:@"id"]) {
                arTtlLabel = @[@{@"state" : @"10", @"name" : @"Jumlah Emas yang dimiliki (dalam gram)"},
                               @{@"state" : @"6", @"name" : @"Harga Emas saat ini (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 gram)"},
                               @{@"state" : @"5", @"name" : @"Wajib zakat emas"}
                               ];
            }else{
                arTtlLabel = @[@{@"state" : @"10", @"name" : @"Amount of Gold held (in grams)"},
                               @{@"state" : @"6", @"name" : @"Current Gold Price (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 grams)"},
                               @{@"state" : @"5", @"name" : @"Obligatory to gold alms"}
                               ];
            }
            break;
        case 4:
            if ([lang isEqualToString:@"id"]) {
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Saldo tabungan"},
                               @{@"state" : @"0", @"name" : @"Bagi hasil"},
                               @{@"state" : @"6", @"name" : @"Harga Emas saat ini (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 gram)"},
                               @{@"state" : @"5", @"name" : @"Wajib zakat tabungan"}
                               ];
            }else{
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Savings balance"},
                               @{@"state" : @"0", @"name" : @"Profit sharing"},
                               @{@"state" : @"6", @"name" : @"Current Gold Price (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 grams)"},
                               @{@"state" : @"5", @"name" : @"Obligatory to savings alms"}
                               ];
            }
            break;
        case 5:
//            if ([lang isEqualToString:@"id"]) {
//                arTtlLabel = @[@{@"state" : @"8", @"name" : @"Waktu haul (waktu awal mencapai nishab)"},
//                               @{@"state" : @"1", @"name" : @"Nilai emas dan/atau perak (Rp)"},
//                               @{@"state" : @"1", @"name" : @"Uang tunai, tabungan (Rp)"},
//                               @{@"state" : @"1", @"name" : @"Kendaraan, rumah, asset lain (Rp)"},
//                               @{@"state" : @"0", @"name" : @"Hutang/Cicilan (optional)"},
//                               @{@"state" : @"5", @"name" : @"Wajib zakat maal"}
//                               ];
//            }else{
//                arTtlLabel = @[@{@"state" : @"8", @"name" : @"Haul time (initial time reached nishab)"},
//                               @{@"state" : @"1", @"name" : @"Gold and / or silver value (Rp)"},
//                               @{@"state" : @"1", @"name" : @"Cash, savings (Rp)"},
//                               @{@"state" : @"1", @"name" : @"Vehicles, houses, other assets (Rp)"},
//                               @{@"state" : @"0", @"name" : @"Debt / Installments (optional)"},
//                               @{@"state" : @"5", @"name" : @"Mandatory zakat mal"}
//                               ];
//            }
            if ([lang isEqualToString:@"id"]) {
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Nilai emas dan/atau perak (Rp)"},
                               @{@"state" : @"1", @"name" : @"Uang tunai, tabungan (Rp)"},
                               @{@"state" : @"1", @"name" : @"Kendaraan, rumah, asset lain (Rp)"},
                               @{@"state" : @"0", @"name" : @"Hutang/Cicilan (optional)"},
                               @{@"state" : @"6", @"name" : @"Harga Emas saat ini (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 gram)"},
                               @{@"state" : @"5", @"name" : @"Wajib zakat maal"}
                               ];
            }else{
                arTtlLabel = @[@{@"state" : @"1", @"name" : @"Gold and / or silver value (Rp)"},
                               @{@"state" : @"1", @"name" : @"Cash, savings (Rp)"},
                               @{@"state" : @"1", @"name" : @"Vehicles, houses, other assets (Rp)"},
                               @{@"state" : @"0", @"name" : @"Debt / Installments (optional)"},
                               @{@"state" : @"6", @"name" : @"Current Gold Price (per gram)"},
                               @{@"state" : @"7", @"name" : @"Nishab (85 grams)"},
                               @{@"state" : @"5", @"name" : @"Obligatory to savings alms"}
                               ];
            }
            break;
            
        default:
            arTtlLabel = [[NSArray alloc]init];
            break;
            
    }
    
    if (arTtlLabel.count > 0) {
        for (int i =0; i< arTtlLabel.count; i++) {
            NSDictionary *dataDict = [arTtlLabel objectAtIndex:i];
            UIView *vwEditable = [self makeAddingView:i strLabel:[dataDict valueForKey:@"name"]];
            [self.vwContentScrol addSubview:vwEditable];
        }
    }
     [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height + 30 + kbSize.height/2)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height/2, 0);
    if ([[data valueForKey:@"id"]integerValue] == 2) {
        contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    }
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 10, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

-(UIView *) makeAddingView : (NSInteger ) mId
              strLabel : (NSString *) mLabel{
    
//    NSArray *arName = [[dataContent valueForKey:@"name"]componentsSeparatedByString:@" "];
    //NSString *unxTag = [[arName objectAtIndex:0]substringToIndex:3];
    NSInteger nTagId = [[dataContent valueForKey:@"id"]integerValue];
    UIView *vwBase = [[UIView alloc]init];
    
    NSInteger mStateIdx = [[[arTtlLabel objectAtIndex:mId]valueForKey:@"state"]integerValue];
    if (mStateIdx == 2) {
        vwBase = [self addFieldDefaultVal:mLabel
                              txtFieldVal:[Utility formatCurrencyValue:[[data valueForKey:@"harga_beras"]doubleValue]]
                                      tag:nTagId mId:mId];
    }else if(mStateIdx == 3){
        double nishabBeras = [self mathNishabBeras:[[data valueForKey:@"harga_beras"]doubleValue]];
        vwBase = [self addFieldDefaultVal:mLabel
                              txtFieldVal:[Utility formatCurrencyValue:nishabBeras]
                                      tag:nTagId mId:mId];
    }else if(mStateIdx == 5){
        NSString *str;
        if ([lang isEqualToString:@"id"]) {
            str =  @"Tidak";
        }else{
            str = @"No";
        }
        vwBase = [self addFieldDefaultVal:mLabel
                                     txtFieldVal:str
                                             tag:nTagId mId:mId];
    }else if(mStateIdx == 6){
        vwBase = [self addFieldDefaultVal:mLabel
        txtFieldVal:[Utility formatCurrencyValue:[[data valueForKey:@"harga_emas"]doubleValue]]
                tag:nTagId mId:mId];
    }else if(mStateIdx == 7){
        double nishabBeras = [self mathNishabEmas:[[data valueForKey:@"harga_emas"]doubleValue]];
        vwBase = [self addFieldDefaultVal:mLabel
                              txtFieldVal:[Utility formatCurrencyValue:nishabBeras]
                                      tag:nTagId mId:mId];
    }
    
    
    else{
        JVFloatLabeledTextField *txtField = [[JVFloatLabeledTextField alloc]init];
        [txtField setPlaceholder:mLabel];
        [txtField setPlaceholderColor:UIColorFromRGB(const_color_gray)];
        [txtField setFloatingLabelTextColor:UIColorFromRGB(const_color_primary)];
        [txtField setFloatingLabelFont:[UIFont fontWithName:@"HelvetivaNeue" size:10]];
        txtField.clearButtonMode = UITextFieldViewModeWhileEditing;
        txtField.keyboardType = UIKeyboardTypeNumberPad;
        txtField.inputAccessoryView = toolBar;
        txtField.accessibilityLabel = [NSString stringWithFormat:@"%ld_%ld", nTagId,mId];
        txtField.delegate = self;
        [txtField setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        if ([Utility isDeviceHaveBottomPadding]) {
            [txtField setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
        }
        txtField.keepBaseline = YES;
        txtField.tag = mStateIdx;
        
        if (mStateIdx == 8) {
           // [txtField setPlaceholder:@"dd-MM-yyyy"];
            UIDatePicker *datePicker = [[UIDatePicker alloc]init];
            if (@available(iOS 13.4, *)) {
                [datePicker setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
                [datePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels];
             }
            [datePicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:lang]];
            [datePicker setBackgroundColor:[UIColor whiteColor]];
            [datePicker setDatePickerMode:UIDatePickerModeDate];
            datePicker.tag = 1;
            [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
            txtField.inputView = datePicker;
        }
        
        UIView *vwLine = [[UIView alloc]init];
        [vwLine setBackgroundColor:[[UIColor lightGrayColor]colorWithAlphaComponent:0.3f]];
        
        [vwBase addSubview:txtField];
        [vwBase addSubview:vwLine];
    }
    
    return vwBase;
}

-(UIView *) addFieldDefaultVal : (NSString *) ttlLabel
                   txtFieldVal : (NSString *) defFieldVal
                            tag: (NSInteger) unxTag
                           mId : (NSInteger) mId{
    UIView *vwBase = [[UIView alloc]init];
    UILabel *lblTitle = [[UILabel alloc]init];
    UITextField *txtDefault = [[UITextField alloc]init];
    [lblTitle setText:ttlLabel];
    [lblTitle setTextColor: UIColorFromRGB(const_color_primary)];
    [lblTitle setFont:[UIFont fontWithName:@"Lato-Regular" size:10]];
    [lblTitle sizeToFit];
    
    txtDefault.text = defFieldVal;
    [txtDefault setEnabled:false];
    txtDefault.accessibilityLabel = [NSString stringWithFormat:@"%ld_%ld", unxTag,mId];
    txtDefault.delegate = self;
    [txtDefault setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
    if ([Utility isDeviceHaveBottomPadding]) {
        [txtDefault setFont:[UIFont fontWithName:@"Lato-Regular" size:16]];
    }
    
    UIView *vwLine = [[UIView alloc]init];
    [vwLine setBackgroundColor:[[UIColor lightGrayColor]colorWithAlphaComponent:0.3f]];
    
    [vwBase addSubview:lblTitle];
    [vwBase addSubview:txtDefault];
    [vwBase addSubview:vwLine];
    return vwBase;
}

-(void) setupRelocationPosition{
    if (self.vwContentScrol.subviews.count>0) {
        for(int y = 0; y< self.vwContentScrol.subviews.count; y++){
            UIView *vwBaseEditable = (UIView *) [self.vwContentScrol.subviews objectAtIndex:y];
            UITextField *txtFieldEditable;
            UIView *vwLine;
            UILabel *lblTitle;
            
            CGRect frmVwBaseEditable = vwBaseEditable.frame;
            
            if (y == 0) {
                frmVwBaseEditable.origin.y = 0;
                frmVwBaseEditable.origin.x = 16;
                frmVwBaseEditable.size.width = self.vwContentScrol.frame.size.width - (frmVwBaseEditable.origin.x *2);
            }else{
                UIView *vwLastBaseEditable = (UIView *) [self.vwContentScrol.subviews objectAtIndex:y-1];
                CGRect frmVwLastBaseEditable = vwLastBaseEditable.frame;
                
                frmVwBaseEditable.origin.x = frmVwLastBaseEditable.origin.x;
                frmVwBaseEditable.origin.y = frmVwLastBaseEditable.origin.y + frmVwLastBaseEditable.size.height + 8;
                frmVwBaseEditable.size.width = frmVwLastBaseEditable.size.width;
            }
            
            NSInteger mStateIdx = [[[arTtlLabel objectAtIndex:y]valueForKey:@"state"]integerValue];
            if (mStateIdx == 2 || mStateIdx == 3 || mStateIdx == 5 || mStateIdx == 6 || mStateIdx == 7) {
                lblTitle = (UILabel *) [vwBaseEditable.subviews objectAtIndex:0];
                txtFieldEditable = (UITextField *) [vwBaseEditable.subviews objectAtIndex:1];
                vwLine = (UIView *) [vwBaseEditable.subviews objectAtIndex:2];
            }else{
                txtFieldEditable = (UITextField *) [vwBaseEditable.subviews objectAtIndex:0];
                vwLine = (UIView *) [vwBaseEditable.subviews objectAtIndex:1];
            }
            
            
            
            CGRect frmLblTitle = lblTitle.frame;
            CGRect frmTxtField = txtFieldEditable.frame;
            CGRect frmVwLine = vwLine.frame;
            
          
            if (mStateIdx == 2 || mStateIdx == 3 || mStateIdx == 5 || mStateIdx == 6 || mStateIdx == 7) {
                frmLblTitle.origin.x = 0;
                frmLblTitle.origin.y = 0;
                frmLblTitle.size.width = frmVwBaseEditable.size.width;
                
                frmTxtField.origin.x = 0;
                frmTxtField.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height - 6;
                frmTxtField.size.height = 50;
                frmTxtField.size.width = frmVwBaseEditable.size.width;
                
                frmVwLine.origin.x = frmTxtField.origin.x;
                frmVwLine.origin.y = frmTxtField.origin.y + frmTxtField.size.height - 6;
                frmVwLine.size.width = frmTxtField.size.width;
                frmVwLine.size.height = 1;
                
                frmVwBaseEditable.size.height = frmVwLine.origin.y + frmVwLine.size.height;
                
                vwBaseEditable.frame = frmVwBaseEditable;
                lblTitle.frame = frmLblTitle;
                txtFieldEditable.frame = frmTxtField;
                vwLine.frame = frmVwLine;
                
            }else{
                frmTxtField.origin.x = 0;
                frmTxtField.origin.y = 0;
                frmTxtField.size.height = 50;
                frmTxtField.size.width = frmVwBaseEditable.size.width;
                
                frmVwLine.origin.x = frmTxtField.origin.x;
                frmVwLine.origin.y = frmTxtField.origin.y + frmTxtField.size.height;
                frmVwLine.size.width = frmTxtField.size.width;
                frmVwLine.size.height = 1;
                
                frmVwBaseEditable.size.height = frmVwLine.origin.y + frmVwLine.size.height;
                
                vwBaseEditable.frame = frmVwBaseEditable;
                txtFieldEditable.frame = frmTxtField;
                vwLine.frame = frmVwLine;
            }
            
            
            
        }
    }
}

-(void) OptiimizePosition{
    CGRect frmVwMainTac = self.vwMainTac.frame;
    CGRect frmVwContentTac = self.vwContentTAC.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwBtnByar = self.btnBayar.frame;
    CGRect frmVwListTabungan = self.vwListTabungan.frame;
    CGRect frmVwContentScroll = self.vwContentScrol.frame;
    
    UIView *vwLast = [self.vwContentScrol.subviews objectAtIndex:[self.vwContentScrol.subviews count]-1];
    
    CGRect frmVwLast = vwLast.frame;
    
    frmVwContentScroll.size.height = frmVwLast.origin.y + frmVwLast.size.height;
    
    NSLog(@"Height vw content scroll : %d", (int)frmVwContentScroll.size.height);
#pragma mark hidden vwlisttabungan when list is empty
    if (arListAccount.count == 0 || arListAccount == nil) {
        frmVwListTabungan.size.height = 1;
        self.vwListTabungan.hidden = YES;
        
    }
    
    if (frmVwContentScroll.size.height <= 228) {
        frmVwScroll.size.height = 230;
    }else if(frmVwContentScroll.size.height == 234){
        frmVwScroll.size.height = 244;
    }
//    else if(frmVwContentScroll.size.height == 287){
//        frmVwScroll.size.height = 289;
//    }
    else{
        frmVwScroll.size.height = SCREEN_HEIGHT - TOP_NAV - frmVwScroll.origin.y - BOTTOM_NAV_DEF - frmVwBtnByar.size.height - frmVwListTabungan.size.height;
        if ([Utility isDeviceHaveBottomPadding]) {
            frmVwScroll.size.height = frmVwContentScroll.size.height - frmVwListTabungan.size.height;
        }
    }
    
    frmVwListTabungan.origin.y = frmVwScroll.origin.y + frmVwScroll.size.height + 16;
    frmVwBtnByar.origin.y = frmVwListTabungan.origin.y + frmVwListTabungan.size.height + 8;
    frmVwBtnByar.size.height = 40;
    frmVwContentTac.size.height = frmVwBtnByar.origin.y + frmVwBtnByar.size.height + 16;
    
//    frmVwBtnByar.origin.y = frmVwScroll.origin.y + frmVwScroll.size.height + 16;
//    frmVwListTabungan.origin.y = frmVwBtnByar.origin.y - frmVwListTabungan.size.height - 8;
//    frmVwContentTac.size.height = frmVwBtnByar.origin.y + frmVwBtnByar.size.height + 16;
    
    frmVwContentTac.origin.y = (frmVwMainTac.size.height/2 - frmVwContentTac.size.height/2);
    
    self.vwContentTAC.frame = frmVwContentTac;
    self.vwScroll.frame = frmVwScroll;
    self.btnBayar.frame = frmVwBtnByar;
    self.vwListTabungan.frame = frmVwListTabungan;
    self.vwContentScrol.frame = frmVwContentScroll;
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height)];
//    #pragma mark setcontentsize when list is empty
//    if (arListAccount.count > 0 || arListAccount != nil) {
//        [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height + self.vwListTabungan.frame.size.height)];
//    }
    
}


-(void) setupLayout{
    CGRect frmVwMainTac = self.vwMainTac.frame;
    CGRect frmVwContentTac = self.vwContentTAC.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwListTabungan = self.vwListTabungan.frame;
    CGRect frmBtnBayar = self.btnBayar.frame;
    CGRect frmVwContentScroll = self.vwContentScrol.frame;
    
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmBtnExit = self.btnExit.frame;
    CGRect frmLblTitleZakat = self.lblTitleZakat.frame;
    CGRect frmLblNomZakat = self.lblNominalZakat.frame;
    CGRect frmVwLineSeparator= self.vwLineSeparator.frame;
    
    CGRect frmLblRecSumber = self.lblRekSumberDana.frame;
    CGRect frmTxtTabungan = self.txtAccountTabungan.frame;
    CGRect frmVwLine = self.vwListTabungan.frame;
    CGRect frmIcArrowDown = self.ic_arrow_down.frame;
    
    frmVwMainTac.origin.x = 0;
    frmVwMainTac.origin.y = 0;
    frmVwMainTac.size.width = SCREEN_WIDTH;
    frmVwMainTac.size.height = SCREEN_HEIGHT;
    
    frmVwContentTac.origin.x = 16;
    frmVwContentTac.origin.y = (frmVwMainTac.size.height * 10)/100;
    frmVwContentTac.size.width = frmVwMainTac.size.width - (frmVwContentTac.origin.x * 2);
    frmVwContentTac.size.height = frmVwMainTac.size.height - frmVwContentTac.origin.y - BOTTOM_NAV;
    
    frmLblTitle.origin.x = 0;
    frmLblTitle.origin.y = 16;
    frmLblTitle.size.width = frmVwContentTac.size.width;
    
    frmBtnExit.origin.x = frmVwContentTac.size.width - frmBtnExit.size.width - 16;
    frmBtnExit.origin.y = frmLblTitle.origin.y - 5;
    
    frmLblTitleZakat.origin.x = 16;
    frmLblTitleZakat.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height + 24;
    frmLblTitleZakat.size.width = frmVwContentTac.size.width - (frmLblTitleZakat.origin.x * 2);
    
    frmLblNomZakat.origin.x = 16;
    frmLblNomZakat.origin.y = frmLblTitleZakat.origin.y + frmLblTitleZakat.size.height + 8;
    frmLblNomZakat.size.width = frmVwContentTac.size.width - (frmLblNomZakat.origin.x * 2);
    
    
    frmVwLineSeparator.origin.y = frmLblNomZakat.origin.y + frmLblNomZakat.size.height + 16;
    frmVwLineSeparator.origin.x = 0;
    frmVwLineSeparator.size.width = frmVwContentTac.size.width;
    
    frmBtnBayar.origin.y =  frmVwContentTac.size.height - frmBtnBayar.size.height - 16;
    frmBtnBayar.origin.x = 16;
    frmBtnBayar.size.width = frmVwContentTac.size.width - (frmBtnBayar.origin.x*2);
    
    frmVwListTabungan.origin.x = 0;
    frmVwListTabungan.size.width = frmVwContentTac.size.width;
    
    frmIcArrowDown.origin.x = frmVwListTabungan.size.width - frmIcArrowDown.size.width - 16;
    frmIcArrowDown.origin.y = frmVwListTabungan.size.height/2 - frmIcArrowDown.size.height/2;
    
    frmLblRecSumber.origin.x = 16;
    frmLblRecSumber.origin.y = 0;
    frmLblRecSumber.size.width = frmVwListTabungan.size.width - (frmLblRecSumber.origin.x * 2);
    
    frmTxtTabungan.origin.x = 16;
    frmTxtTabungan.origin.y = frmLblRecSumber.origin.y + frmLblRecSumber.size.height + 5;
    frmTxtTabungan.size.width = frmIcArrowDown.origin.x - frmTxtTabungan.origin.x - 8;
    
    frmVwLine.origin.x = 16;
    frmVwLine.size.height = 1;
    frmVwLine.origin.y = frmTxtTabungan.origin.y + frmTxtTabungan.size.height + frmVwLine.size.height;
    frmVwLine.size.width = frmVwListTabungan.size.width - (frmVwLine.origin.x *2);
    
    frmVwListTabungan.size.height = frmVwLine.origin.y + frmVwLine.size.height + 5;
    frmVwListTabungan.origin.y = frmBtnBayar.origin.y - frmVwListTabungan.size.height - 8;
    
    frmVwScroll.origin.x = 0;
    frmVwScroll.origin.y = frmVwLineSeparator.origin.y + frmVwLineSeparator.size.height + 16;
    frmVwScroll.size.width = frmVwContentTac.size.width;
    frmVwScroll.size.height = frmVwListTabungan.origin.y - frmVwListTabungan.size.height - frmVwScroll.origin.y;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = frmVwScroll.size.width;
    
    

    
    self.vwMainTac.frame = frmVwMainTac;
    self.vwContentTAC.frame = frmVwContentTac;
    self.vwScroll.frame = frmVwScroll;
    self.btnBayar.frame=  frmBtnBayar;
    self.vwListTabungan.frame = frmVwListTabungan;
    self.vwContentScrol.frame = frmVwContentScroll;
    
    self.lblTitle.frame = frmLblTitle;
    self.btnExit.frame = frmBtnExit;
    self.lblTitleZakat.frame = frmLblTitleZakat;
    self.lblNominalZakat.frame = frmLblNomZakat;
    self.vwLineSeparator.frame = frmVwLineSeparator;
    
    self.lblRekSumberDana.frame = frmLblRecSumber;
    self.txtAccountTabungan.frame = frmTxtTabungan;
    self.ic_arrow_down.frame = frmIcArrowDown;
    self.vwLine.frame = frmVwLine;
    
    //self.vwContentTAC.clipsToBounds = YES;
    self.vwContentTAC.layer.cornerRadius = 8;
    self.vwContentTAC.layer.shadowColor = [[UIColor grayColor]CGColor];
    self.vwContentTAC.layer.shadowOffset = CGSizeMake(0, 5);
    self.vwContentTAC.layer.shadowOpacity = 0.5f;
    self.vwContentTAC.backgroundColor = [UIColor whiteColor];
    
//    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height + 72)];
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height)];
    
}

-(void) actionExitListener : (UIButton *) sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
    double newValue = [newText doubleValue];
    
    if ([newText length] == 0 || newValue == 0){
        textField.text = nil;
    }
    else{
        if (textField.tag == 10) {
            NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            textField.text = newText;
        }else{
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
        }
        
        [self handlingInputUserText:textField.accessibilityLabel];
        
        return NO;
        
    }
    
    
    return YES;

}

-(void) handlingInputUserText : (NSString *) accessLabelity{
    NSArray *dataAccessLabelity = [accessLabelity componentsSeparatedByString:@"_"];
    NSInteger nId = [[dataContent valueForKey:@"id"]integerValue];
    if ([[dataAccessLabelity objectAtIndex:0]integerValue] == nId) {
        
        NSMutableArray *valDataNom = [[NSMutableArray alloc]init];
        for(int i=0 ; i<arTtlLabel.count;i++){
            NSInteger stateIdx = [[[arTtlLabel objectAtIndex:i] valueForKey:@"state"] integerValue];
            UIView *vw = [self.vwContentScrol.subviews objectAtIndex:i];
            UITextField *txtField = [[UITextField alloc]init];
            if (stateIdx == 2 || stateIdx == 3) {
                txtField = [vw.subviews objectAtIndex:1];
            }else{
                txtField = [vw.subviews objectAtIndex:0];
            }
            NSString *nominalStr = [txtField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
            [valDataNom addObject:nominalStr];
            
        }
        NSString *strNominalZakat = @"";
        switch (nId) {
            case 1:
            {
                double valPemasukkan, valPemasukkanOther, valPengeluaran;
                valPemasukkan = [self validasiValueNom: [valDataNom objectAtIndex:0]];
                valPemasukkanOther = [self validasiValueNom:[valDataNom objectAtIndex:1]];
                valPengeluaran = [self validasiValueNom:[valDataNom objectAtIndex:2]];
                
                long vOut = [[NSNumber numberWithDouble:[self mathZakatProfersi:valPemasukkan pemasukkanLain:valPemasukkanOther pengeluaran:valPengeluaran]] longValue];
                
                strNominalZakat = [Utility formatCurrencyValue: vOut];
            }
                break;
            case 2:
            {
                double valModal, valKeuntungan, valPiutangDagang, valHutang, valKerugian;
                valModal = [self validasiValueNom: [valDataNom objectAtIndex:0]];
                valKeuntungan = [self validasiValueNom: [valDataNom objectAtIndex:1]];
                valPiutangDagang = [self validasiValueNom: [valDataNom objectAtIndex:2]];
                valHutang = [self validasiValueNom: [valDataNom objectAtIndex:3]];
                valKerugian = [self validasiValueNom: [valDataNom objectAtIndex:4]];
                
                long vOut = [[NSNumber numberWithDouble:[self mathZakatPerdagangan:valModal
                     mKeuntungan:valKeuntungan
                   piutangDagang:valPiutangDagang
                hutangJatuhTempo:valHutang
                       mKerugian:valKerugian]] longValue];
                
                strNominalZakat = [Utility formatCurrencyValue: vOut];
                
            }break;
                
            case 3:{
                double valJumEmas = [self validasiValueNom:[valDataNom objectAtIndex:0]];
                long vOut = [[NSNumber numberWithDouble:[self mathZakatEmas:valJumEmas]] longValue];
                strNominalZakat = [Utility formatCurrencyValue:vOut];
            }break;
                
            case 4: {
                double valSaldoTabungan, valBagiHasil;
                valSaldoTabungan = [self validasiValueNom:[valDataNom objectAtIndex:0]];
                valBagiHasil = [self validasiValueNom:[valDataNom objectAtIndex:1]];
                
                long vOut = [[NSNumber numberWithDouble:[self mathZakatTabungan:valSaldoTabungan nBagiHasil:valBagiHasil]] longValue];
                strNominalZakat = [Utility formatCurrencyValue:
                                   vOut];
            }break;
                
            case 5:{
//                NSString *strDateHaul = [valDataNom objectAtIndex:0];
//                double valHartaBerharga, valTabungan, valAsset, valPengeluaran;
//                valHartaBerharga = [self validasiValueNom:[valDataNom objectAtIndex:1]];
//                valTabungan = [self validasiValueNom:[valDataNom objectAtIndex:2]];
//                valAsset = [self validasiValueNom:[valDataNom objectAtIndex:3]];
//                valPengeluaran = [self validasiValueNom:[valDataNom objectAtIndex:4]];
                NSString *strDateHaul = @"";
                double valHartaBerharga, valTabungan, valAsset, valPengeluaran;
                valHartaBerharga = [self validasiValueNom:[valDataNom objectAtIndex:0]];
                valTabungan = [self validasiValueNom:[valDataNom objectAtIndex:1]];
                valAsset = [self validasiValueNom:[valDataNom objectAtIndex:2]];
                valPengeluaran = [self validasiValueNom:[valDataNom objectAtIndex:3]];
                
                long vOut = [[NSNumber numberWithDouble:[self mathZakatMaal:valHartaBerharga
                                                              hartaTabungan:valTabungan
                                                                 hartaAsset:valAsset pengeluaran:valPengeluaran waktuHaul:strDateHaul]] longValue];

                strNominalZakat = [Utility formatCurrencyValue:vOut];
            }break;
                
            default:
                break;
        }
        
        [self.lblNominalZakat setText:[NSString stringWithFormat:@"Rp. %@", strNominalZakat]];
        UIView *vwWajibZakat = (UIView *) [self.vwContentScrol.subviews objectAtIndex: [self.vwContentScrol.subviews count]-1];
        UITextField *txtWajib = (UITextField *) [vwWajibZakat.subviews objectAtIndex:1];
        NSString *strTxtWajib = @"";
        if ([strNominalZakat isEqualToString:@"0"]) {
            [self.lblNominalZakat setTextColor:[UIColor blackColor]];
            if ([lang isEqualToString:@"id"]) {
                strTxtWajib = @"Tidak";
            }else{
                strTxtWajib = @"No";
            }
        }else{
            [self.lblNominalZakat setTextColor:UIColorFromRGB(const_color_primary)];
            if ([lang isEqualToString:@"id"]) {
                strTxtWajib = @"Iya";
            }else{
                strTxtWajib = @"Yes";
            }
        }
        [txtWajib setText:strTxtWajib];
    }
    
}

-(NSInteger) validasiValueNom : (NSString *) mValue{
    NSInteger valNom = [mValue integerValue];
    if ([mValue length] == 0 || valNom == 0) {
        return 0;
    }else{
        return valNom;
    }
    
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height)];
}


-(double)mathZakatProfersi : (double) mPemasukkan
           pemasukkanLain: (double) mPemasukkanLain
             pengeluaran : (double) mPengeluaran{
    double sumPemasukkan = mPemasukkan + mPemasukkanLain - mPengeluaran;
    double mNishabBeras = [self mathNishabBeras: [[data valueForKey:@"harga_beras"]doubleValue]];
    if (sumPemasukkan > mNishabBeras) {
        return sumPemasukkan * 0.025;
    }else{
        return 0;
    }
    
}

-(double)mathNishabBeras : (double) hargaBeras{
    return hargaBeras * 520;
}

-(double)mathNishabEmas : (double) hargaEmas{
    return hargaEmas * 85;
}

-(double) mathZakatMaal : (double) mHartaBerharga
          hartaTabungan : (double) mHartaTabungan
             hartaAsset : (double) mHartaAsset
            pengeluaran : (double) mPengeluaran
              waktuHaul : (NSString *) mWaktuHaul{
//    NSString *strDateNow = [Utility dateToday:4];
//
//    NSDate *dateNow, *dateHaul;
//    dateNow = [Utility dateStringToDate:strDateNow mType:2];
//    dateHaul = [Utility dateStringToDate:mWaktuHaul mType:2];
//    NSTimeInterval diffTimeHaul = [dateNow timeIntervalSinceDate:dateHaul];
    
    double sumPemasukkan = mHartaBerharga + mHartaTabungan + mHartaAsset - mPengeluaran;
//    if (sumPemasukkan > [self mathNishabEmas:[[data valueForKey:@"harga_emas"]doubleValue]] && diffTimeHaul >= 354) {
//        return sumPemasukkan * 0.025;
//    }else{
//        return 0;
//    }
    if (sumPemasukkan > [self mathNishabEmas:[[data valueForKey:@"harga_emas"]doubleValue]]) {
           return sumPemasukkan * 0.025;
       }else{
           return 0;
       }
}

-(double) mathZakatPerdagangan : (double) nModal
                   mKeuntungan : (double) nKeuntungan
                 piutangDagang : (double) nPiutangDagang
              hutangJatuhTempo : (double) nHutang
                     mKerugian : (double) nKerugian{
    double sumPemasukkan = nModal + nKeuntungan - nPiutangDagang - nHutang - nKerugian;
    if (sumPemasukkan > [self mathNishabEmas:[[data valueForKey:@"harga_emas"]doubleValue]]) {
        return sumPemasukkan * 0.025;
    }else{
        return 0;
    }
}

-(double) mathZakatEmas : (double) nJumlahEmas {
//    if (nJumlahEmas > [self mathNishabEmas:[[data valueForKey:@"harga_emas"]doubleValue]]) {
//        return nJumlahTotalEmas * 0.025;
//    }else{
//        return 0;
//    }
    
    if (nJumlahEmas > 85) {
        double nTotalEmas = [[data valueForKey:@"harga_emas"]doubleValue] * nJumlahEmas;
       return nTotalEmas * 0.025;
    }else{
        return 0;
    }
}

-(double) mathZakatTabungan : (double) saldoTabungan
                  nBagiHasil : (double) mBagiHasil{
    double sumPemasukkan = saldoTabungan + mBagiHasil;
    if (sumPemasukkan > [self mathNishabEmas:[[data valueForKey:@"harga_emas"]doubleValue]]) {
        return sumPemasukkan * 0.025;
    }else{
        return 0;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [arListAccount count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    //new list_account2
    if(row == 0){
        return [arListAccount objectAtIndex:row];
    }else{
        return [[arListAccount objectAtIndex:row] objectForKey:@"title_account"];
    }

    
    //old list_account1
//    return [arListAccount objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSString *strAccount = @"";
    if(row != 0){
        strAccount = [[arListAccount objectAtIndex:row] objectForKey:@"title_account"];
        self.txtAccountTabungan.text = strAccount;
        selectedAccount = [[arListAccount objectAtIndex:row] objectForKey:@"id_account"];
    }else{
         strAccount = [arListAccount objectAtIndex:row];
        self.txtAccountTabungan.text = strAccount;
    }
    
    if ([strAccount isEqualToString:@"Pilih Nomor Rekening"] || [strAccount isEqualToString:@"Select Account Number"] ) {
        self.txtAccountTabungan.text = @"";
        self.txtAccountTabungan.placeholder = strAccount;
    }
    
}

- (void)dateChanged:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *selectedDate = [dateFormat stringFromDate:datePicker.date];
    UIView *vwDate = (UIView *) [self.vwContentScrol.subviews objectAtIndex:0];
    UITextView *txtDate = (UITextView *) [vwDate.subviews objectAtIndex:0];
    if(datePicker.tag == 1){
        txtDate.text = selectedDate;
        
    }
}

-(void)actionBayar:(UIButton *) sender{
    
//    if ([[userDefault valueForKey:@"customer_id"] isEqualToString:@""] || [userDefault valueForKey:@"customer_id"] == nil) {
    if ([[NSUserdefaultsAes getValueForKey:@"customer_id"] isEqualToString:@""] || [NSUserdefaultsAes getValueForKey:@"customer_id"] == nil) {
        NSString *strMsg;
        if ([lang isEqualToString:@"id"]) {
            strMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
        }else{
            strMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 1;
        [alert show];
    }else{
        [userDefault setValue:@"0" forKey:@"state_bayar_zakat"];
        [userDefault synchronize];
        
        UIView *vwWajibZakat = (UIView *) [self.vwContentScrol.subviews objectAtIndex:[self.vwContentScrol.subviews count]-1];
        UITextField *txtWajibZakat = (UITextField *) [vwWajibZakat.subviews objectAtIndex:1];
        NSString *strWajibZakat, *strIdAccount;
        strWajibZakat = txtWajibZakat.text;
        strIdAccount = self.txtAccountTabungan.text;
        bool stateWajibZakat = false;
        NSString *strMsg;
        if ([strWajibZakat isEqualToString:@"Iya"] || [strWajibZakat isEqualToString:@"Yes"]) {
            if (![[Utility stringTrimmer:strIdAccount] isEqualToString:@""]) {
                stateWajibZakat = true;
            }else{
                if ([lang isEqualToString:@"id"]) {
                    strMsg = @"Silahkan pilih nomor rekening terlebih dahulu";
                }else{
                    strMsg = @"Please choose an account number first";
                }
            }
        }else{
            if ([lang isEqualToString:@"id"]) {
                strMsg = @"Anda tidak wajib zakat";
            }else{
                strMsg = @"You are not obliged to zakat";
            }
        }
        
        if (stateWajibZakat) {
            
            NSString *msgBayarZakat, *strYes, *strNo;
            
            if ([lang isEqualToString:@"id"]) {
                msgBayarZakat = [NSString stringWithFormat:@"Anda akan membayar %@, dengan nominal %@", [dataContent valueForKey:@"name"], self.lblNominalZakat.text];
                strYes = @"LANJUT";
                strNo = @"BATAL";
            }else{
                msgBayarZakat = [NSString stringWithFormat:@"You will pay %@, with a nominal of %@", [dataContent valueForKey:@"name"], self.lblNominalZakat.text];
                strYes = @"NEXT";
                strNo = @"CANCEL";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msgBayarZakat delegate:self cancelButtonTitle:strNo otherButtonTitles:strYes, nil];
            alert.tag = 2;
            [alert show];
            
           
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                       [alert show];
        }
    }
    
    
    
}

-(void)handleTimer : (NSString *) modeStr{
    NSDictionary* userInfo = @{@"actionTimer": modeStr};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        NSDictionary* userInfo = @{@"position": @(513)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }else if(alertView.tag == 2){
//        if (buttonIndex == 1) {
//            NSDictionary* userInfo = @{@"str_nominal": self.lblNominalZakat.text,
////                                       @"str_idAccount" : self.txtAccountTabungan.text
//                                       @"str_idAccount" : selectedAccount
//            };
//            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//            [nc postNotificationName:@"zakatBC" object:self userInfo:userInfo];
//
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
        [delegate dataZakat:self.lblNominalZakat.text
                withAccount:selectedAccount];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



- (void) setDelegate:(id)newDelegate {
  delegate = newDelegate;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
