//
//  AsmaulHusnaViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/25/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AsmaulHusnaViewController : TemplateViewController
-(void) setTitleMenu : (NSString *) strTitle;
@end

NS_ASSUME_NONNULL_END
