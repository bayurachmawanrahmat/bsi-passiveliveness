//
//  BottomModalViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 26/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BottomModalViewController : ViewController
-(void) setBundleExtras : (NSDictionary *) dataSet;
@end

NS_ASSUME_NONNULL_END
