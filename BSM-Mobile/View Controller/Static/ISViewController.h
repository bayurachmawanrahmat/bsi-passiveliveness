//
//  ISViewController.h
//  BSM-Mobile
//
//  Created by BSM on 2/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

@interface ISViewController : TemplateViewController
- (void)stopIdleTimer;
@end
