//
//  PopupCoupleCRViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PopupCoupleCRViewController.h"
#import "Utility.h"

@interface PopupCoupleCRViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIAlertViewDelegate, DataCoupleDelegate, ConnectionDelegate, UIScrollViewDelegate>{
    
    int _mState;
    
    UIDatePicker *datePicker;
    UIPickerView *pickPisahHarta;
    
    NSArray *arDataPisahHarta;
    
    NSUserDefaults *userDefault;
    NSString *lang;
    
    UIToolbar *toolBar;
    
    NSInteger nPrenup;
    
}


@property (weak, nonatomic) IBOutlet UIView *vwMainTac;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwContentTac;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIButton *btnExit;

@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;

@property (weak, nonatomic) IBOutlet UIView *vwJumTanggungan;
@property (weak, nonatomic) IBOutlet UIView *vwNamaPasangan;
@property (weak, nonatomic) IBOutlet UIView *vwNoID;
@property (weak, nonatomic) IBOutlet UIView *vwTglLahirPasangan;
@property (weak, nonatomic) IBOutlet UIView *vwPerjanjianPisahHarta;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleJumTanggungan;
@property (weak, nonatomic) IBOutlet UITextField *txtJumTanggungan;
@property (weak, nonatomic) IBOutlet UIView *vwLineJumTanggungan;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleNamaPas;
@property (weak, nonatomic) IBOutlet UITextField *txtNamaPas;
@property (weak, nonatomic) IBOutlet UIView *vwLineNamaPas;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleNoID;
@property (weak, nonatomic) IBOutlet UITextField *txtNoId;
@property (weak, nonatomic) IBOutlet UIView *vwLineNoId;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleTglLahirPas;
@property (weak, nonatomic) IBOutlet UITextField *txtTglLahirPas;
@property (weak, nonatomic) IBOutlet UIView *vwLinTglLahirPas;

@property (weak, nonatomic) IBOutlet UILabel *lblTitlePerjanjian;
@property (weak, nonatomic) IBOutlet UITextField *txtPerjanjianPisah;
@property (weak, nonatomic) IBOutlet UIView *vwLinePernjanjianPisah;

@property (weak, nonatomic) IBOutlet UIButton *btnOk;

@property (weak, nonatomic) IBOutlet UIButton *btnCalender;
@property (weak, nonatomic) IBOutlet UIButton *btnDown;

@end

@implementation PopupCoupleCRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *ttlToolbar;
    if([lang isEqualToString:@"id"]){
        arDataPisahHarta = @[@"Ada", @"Tidak Ada"];
        ttlToolbar = @"Selesai";
        self.lblTitle.text = @"Data Pasangan";
        self.lblTitleNoID.text = @"Nomor Identitas Pasangan";
        self.txtNoId.placeholder = @"Masukkan Nomor Identitas Pasangan";
        self.lblTitleJumTanggungan.text = @"Jumlah Tanggungan";
        self.txtJumTanggungan.placeholder = @"Masukkan Jumlah Tanggungan";
        self.lblTitleNamaPas.text = @"Nama";
        self.txtNamaPas.placeholder = @"Masukkan Nama Pasangan";
        self.lblTitleTglLahirPas.text = @"Tanggal Lahir Pasangan";
        self.txtTglLahirPas.placeholder = @"Masukkan Tanggal Lahir Pasangan";
    }else{
         arDataPisahHarta = @[@"Have", @"No Have"];
         ttlToolbar = @"Done";
        self.lblTitle.text = @"Spouse Data";
        self.lblTitleNoID.text = @"Spouse ID Card Number";
        self.txtNoId.placeholder = @"Enter The Spouse ID Card Number";
        self.lblTitleJumTanggungan.text = @"Number of Dependents";
        self.txtJumTanggungan.placeholder = @"Enter The Number of Dependants";
        self.lblTitleNamaPas.text = @"Spouse Name";
        self.txtNamaPas.placeholder = @"Enter The Spouse Name";
        self.lblTitleTglLahirPas.text = @"Date of Birth";
        self.txtTglLahirPas.placeholder = @"Enter The Date of Birth";
    }
    
    
    if (_mState == 2) {
        [_vwNamaPasangan setHidden:YES];
        [_vwNoID setHidden:YES];
        [_vwTglLahirPasangan setHidden:YES];
        [_vwPerjanjianPisahHarta setHidden:YES];
        [self setupLayoutMState2];
    }else{
        [self setupLayout];
    }
    
    self.vwContentTac.layer.cornerRadius = 16;
    self.vwContentTac.layer.masksToBounds = YES;
//    [self.btnOk setTitleColor:UIColorFromRGB(const_color_primary) forState:UIControlStateNormal];
//    [self.btnOk setTitle:@"OK" forState:UIControlStateNormal];
    self.txtJumTanggungan.keyboardType = UIKeyboardTypeNumberPad;
    self.txtNoId.keyboardType = UIKeyboardTypeNumberPad;
    
    
    
    [_btnOk addTarget:self action:@selector(saveAdditionalParam:) forControlEvents:UIControlEventTouchUpInside];
    [_btnExit addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    BOOL isIna = [[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"];
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.tag = 1;
    [datePicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePicker setBackgroundColor:[UIColor whiteColor]];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    if (@available(iOS 13.4, *)) {
        [datePicker setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels];
    }
    [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    pickPisahHarta = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    pickPisahHarta.showsSelectionIndicator = YES;
    pickPisahHarta.delegate = self;
    pickPisahHarta.dataSource = self;
    pickPisahHarta.tag = 2;
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.txtTglLahirPas.inputView = datePicker;
    self.txtTglLahirPas.inputAccessoryView = toolBar;
    
    self.txtPerjanjianPisah.inputView = pickPisahHarta;
    self.txtPerjanjianPisah.inputAccessoryView = toolBar;
    
    self.txtJumTanggungan.inputAccessoryView = toolBar;
    self.txtJumTanggungan.tag = 1;
    self.txtJumTanggungan.delegate = self;
    
    self.txtNoId.inputAccessoryView = toolBar;
    self.txtNoId.tag = 2;
    self.txtNoId.delegate = self;
    
    self.txtNamaPas.inputAccessoryView = toolBar;
    self.txtNamaPas.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    [self registerForKeyboardNotifications];
    
    if([dataManager.dataExtra objectForKey:@"spouse_name"] != nil && _mState == 1){
        [self.txtNamaPas setText:[dataManager.dataExtra valueForKey:@"spouse_name"]];
        [self.txtNoId setText:[dataManager.dataExtra valueForKey:@"spouse_id"]];

        NSString *dateString = [dataManager.dataExtra valueForKey:@"spouse_bod"];

        [self.txtTglLahirPas setText:[NSString stringWithFormat:@"%@-%@-%@",[dateString substringFromIndex:6],[dateString substringWithRange:NSMakeRange(4, 2)],[dateString substringToIndex:4]]];
//        [self.txtTglLahirPas setText:[NSString stringWithFormat:@"%@-%@-%@",arDate[0],arDate[1],arDate[2]]];
//        [self.txtTglLahirPas setText:[dataManager.dataExtra valueForKey:@"spouse_bod"]];
        [self.txtJumTanggungan setText:[dataManager.dataExtra valueForKey:@"no_family_dependants"]];
    }else if([dataManager.dataExtra objectForKey:@"no_family_dependants"] != nil && _mState == 2){
        [self.txtJumTanggungan setText:[dataManager.dataExtra valueForKey:@"no_family_dependants"]];
    }
    [Utility resetTimer];
    
    [self.vwScroll setDelegate:self];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if(_mState == 1){
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height + kbSize.height + 20)];
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        self.vwScroll.contentInset = contentInsets;
        self.vwScroll.scrollIndicatorInsets = contentInsets;
    }
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwScroll.frame.size.width, self.vwScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
}

- (void)dateChanged:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *selectedDate = [dateFormat stringFromDate:datePicker.date];
    if(datePicker.tag == 1){
        self.txtTglLahirPas.text = selectedDate;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring
                 stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag == 1) {
        if ([substring length] > 2) {
            return NO;
        }
        if(range.length + range.location > 2){
            return NO;
        }
    }else if(textField.tag == 2){
        if ([substring length] > 16) {
            return NO;
        }
        if(range.length + range.location > 16){
            return NO;
        }
    }
    return YES;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;  // Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
     return arDataPisahHarta.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *strPicker = [arDataPisahHarta objectAtIndex:row];
    return strPicker;
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    nPrenup = row;
    NSString *strPicker = [arDataPisahHarta objectAtIndex:row];
    [self.txtPerjanjianPisah setText:strPicker];
}

- (void) setupLayoutMState2{
    CGRect frmVwMainTac = self.vwMainTac.frame;
        
    CGRect frmVwContentTac = self.vwContentTac.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmBtnExit = self.btnExit.frame;
    
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    
    CGRect frmVwJumlahTanggungan = self.vwJumTanggungan.frame;
    
    CGRect frmLblTtlJumTanggungan = self.lblTitleJumTanggungan.frame;
    CGRect frmTxtJumTanggungan = self.txtJumTanggungan.frame;
    CGRect frmVwLineJumTanggungan = self.vwLineJumTanggungan.frame;
    
    CGRect frmBtnOk = self.btnOk.frame;
    
    frmVwMainTac.size.width = SCREEN_WIDTH;
    frmVwMainTac.size.height = SCREEN_HEIGHT;
    frmVwMainTac.origin.x = 0;
    frmVwMainTac.origin.y = 0;

    
    frmVwContentTac.origin.y = (frmVwMainTac.size.height * 18)/100;
    frmVwContentTac.origin.x = 24;
    frmVwContentTac.size.width = frmVwMainTac.size.width - (frmVwContentTac.origin.x * 2);
    frmVwContentTac.size.height = frmVwMainTac.size.height - frmVwContentTac.origin.y - BOTTOM_NAV - 20;
    if (IPHONE_X || IPHONE_XS_MAX) {
        frmVwContentTac.size.height = frmVwMainTac.size.height - frmVwContentTac.origin.y - BOTTOM_NAV - 40;
    }
    
    frmBtnExit.origin.x = frmVwContentTac.size.width - frmBtnExit.size.width - 16;
    frmBtnExit.origin.y = 16;
    
    
    
    frmVwScroll.origin.y = frmBtnExit.origin.y + frmBtnExit.size.height + 8;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = frmVwContentTac.size.width;
   
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = frmVwScroll.size.width;
    
//    frmVwJumlahTanggungan.origin.x = 0;
//    frmVwJumlahTanggungan.origin.y = 0;
    frmVwJumlahTanggungan.size.width = frmVwContentScroll.size.width;
    
    frmLblTtlJumTanggungan.origin.x = 16;
    frmLblTtlJumTanggungan.size.width = frmVwJumlahTanggungan.size.width - (frmLblTtlJumTanggungan.origin.x * 2);
    
    frmTxtJumTanggungan.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmTxtJumTanggungan.origin.y = frmLblTtlJumTanggungan.origin.y + frmLblTtlJumTanggungan.size.height + 8;
    frmTxtJumTanggungan.size.width = frmVwJumlahTanggungan.size.width - (frmTxtJumTanggungan.origin.x *2);
    
    frmVwLineJumTanggungan.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmVwLineJumTanggungan.origin.y = frmTxtJumTanggungan.origin.y + frmTxtJumTanggungan.size.height + 8;
    frmVwLineJumTanggungan.size.width = frmVwJumlahTanggungan.size.width - (frmVwLineJumTanggungan.origin.x * 2);
    
    frmVwJumlahTanggungan.size.height = frmVwLineJumTanggungan.origin.y + frmVwLineJumTanggungan.size.height + 8;
    
    frmVwContentScroll.size.height = frmVwJumlahTanggungan.origin.y + frmVwJumlahTanggungan.size.height + 16;
    
    //frmVwScroll.size.height = frmVwContentScroll.size.height;
    
    frmVwContentTac.size.height = frmVwContentScroll.size.height + frmBtnOk.size.height + frmVwJumlahTanggungan.size.height + 30;
    
    frmBtnOk.origin.x = frmVwContentTac.size.width - frmBtnOk.size.width - 24;
    frmBtnOk.origin.y = frmVwContentTac.size.height - frmBtnOk.size.height - 16;
    
    frmVwScroll.size.height = frmBtnOk.origin.y - frmBtnOk.size.height - 16;
    
    //frmVwContentTac.size.height = frmBtnOk.origin.y + frmBtnOk.size.height + 24;
    
    self.vwMainTac.frame = frmVwMainTac;
    self.vwContentTac.frame = frmVwContentTac;
    self.btnExit.frame = frmBtnExit;
    self.btnOk.frame = frmBtnOk;
    self.vwScroll.frame = frmVwScroll;
    self.vwContentScroll.frame = frmVwContentScroll;
    self.vwJumTanggungan.frame = frmVwJumlahTanggungan;
    self.lblTitleJumTanggungan.frame = frmLblTtlJumTanggungan;
    self.txtJumTanggungan.frame = frmTxtJumTanggungan;
    self.vwLineJumTanggungan.frame = frmVwLineJumTanggungan;
    

//    self.vwPerjanjianPisahHarta.frame = frmVwPerjanjianPisahHarta;
//    self.lblTitlePerjanjian.frame = frmLblPerjanPisahHarta;
    [self.vwPerjanjianPisahHarta setHidden:YES];
    [self.lblTitlePerjanjian setHidden:YES];

//    self.btnDown.frame = frmBtnDown;
//    self.txtPerjanjianPisah.frame = frmTxtPerjanPisahHarta;
//    self.vwLinePernjanjianPisah.frame = frmVwLinePerjan;
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
}

-(void)setupLayout{
    CGRect frmVwMainTac = self.vwMainTac.frame;
    
    CGRect frmVwContentTac = self.vwContentTac.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmBtnExit = self.btnExit.frame;
    
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    
    CGRect frmVwJumlahTanggungan = self.vwJumTanggungan.frame;
    CGRect frmVwNamaPasangan = self.vwNamaPasangan.frame;
    CGRect frmVwNoID = self.vwNoID.frame;
    CGRect frmVwTglLahir = self.vwTglLahirPasangan.frame;
    CGRect frmVwPerjanjianPisahHarta = self.vwPerjanjianPisahHarta.frame;
    
    CGRect frmLblTtlJumTanggungan = self.lblTitleJumTanggungan.frame;
    CGRect frmTxtJumTanggungan = self.txtJumTanggungan.frame;
    CGRect frmVwLineJumTanggungan = self.vwLineJumTanggungan.frame;
    
    CGRect frmLblTtlNamaPas = self.lblTitleNamaPas.frame;
    CGRect frmTxtNamaPas = self.txtNamaPas.frame;
    CGRect frmVwLineNamaPas = self.vwLineNamaPas.frame;
    
    CGRect frmLblTtlNoId = self.lblTitleNoID.frame;
    CGRect frmTxtNoId = self.txtNoId.frame;
    CGRect frmVwLineNoID = self.vwLineNoId.frame;
    
    CGRect frmLblTglLahir= self.lblTitleTglLahirPas.frame;
    CGRect frmTxtTglLahir = self.txtTglLahirPas.frame;
    CGRect frmVwLineTglLahir = self.vwTglLahirPasangan.frame;
    CGRect frmBtnCalender = self.btnCalender.frame;
    
    CGRect frmLblPerjanPisahHarta = self.lblTitlePerjanjian.frame;
    CGRect frmTxtPerjanPisahHarta = self.txtPerjanjianPisah.frame;
    CGRect frmVwLinePerjan = self.vwLinePernjanjianPisah.frame;
    CGRect frmBtnDown = self.btnDown.frame;
    
    CGRect frmBtnOk = self.btnOk.frame;
    
    frmVwMainTac.size.width = SCREEN_WIDTH;
    frmVwMainTac.size.height = SCREEN_HEIGHT;
    frmVwMainTac.origin.x = 0;
    frmVwMainTac.origin.y = 0;

    
    frmVwContentTac.origin.y = (frmVwMainTac.size.height * 18)/100;
    frmVwContentTac.origin.x = 24;
    frmVwContentTac.size.width = frmVwMainTac.size.width - (frmVwContentTac.origin.x * 2);
    frmVwContentTac.size.height = frmVwMainTac.size.height - frmVwContentTac.origin.y - BOTTOM_NAV - 20;
    if (IPHONE_X || IPHONE_XS_MAX) {
        frmVwContentTac.size.height = frmVwMainTac.size.height - frmVwContentTac.origin.y - BOTTOM_NAV - 40;
    }
    
    frmBtnExit.origin.x = frmVwContentTac.size.width - frmBtnExit.size.width - 16;
    frmBtnExit.origin.y = 16;
    
    
    
    frmVwScroll.origin.y = frmBtnExit.origin.y + frmBtnExit.size.height + 8;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = frmVwContentTac.size.width;
   
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = frmVwScroll.size.width;
    
//    frmVwJumlahTanggungan.origin.x = 0;
//    frmVwJumlahTanggungan.origin.y = 0;
    frmVwJumlahTanggungan.size.width = frmVwContentScroll.size.width;
    
    frmLblTtlJumTanggungan.origin.x = 16;
    frmLblTtlJumTanggungan.size.width = frmVwJumlahTanggungan.size.width - (frmLblTtlJumTanggungan.origin.x * 2);
    
    frmTxtJumTanggungan.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmTxtJumTanggungan.origin.y = frmLblTtlJumTanggungan.origin.y + frmLblTtlJumTanggungan.size.height + 8;
    frmTxtJumTanggungan.size.width = frmVwJumlahTanggungan.size.width - (frmTxtJumTanggungan.origin.x *2);
    
    frmVwLineJumTanggungan.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmVwLineJumTanggungan.origin.y = frmTxtJumTanggungan.origin.y + frmTxtJumTanggungan.size.height + 8;
    frmVwLineJumTanggungan.size.width = frmVwJumlahTanggungan.size.width - (frmVwLineJumTanggungan.origin.x * 2);
    
    frmVwJumlahTanggungan.size.height = frmVwLineJumTanggungan.origin.y + frmVwLineJumTanggungan.size.height + 8;
  
    frmVwNamaPasangan.origin.x = 0;
    frmVwNamaPasangan.origin.y = 0;
//    frmVwNamaPasangan.origin.x = frmVwJumlahTanggungan.origin.x;
//    frmVwNamaPasangan.origin.y = frmVwJumlahTanggungan.origin.y + frmVwJumlahTanggungan.size.height + 16;
    frmVwNamaPasangan.size.width = frmVwJumlahTanggungan.size.width;
    
    frmLblTtlNamaPas.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmLblTtlNamaPas.size.width = frmLblTtlJumTanggungan.size.width;
    
    frmTxtNamaPas.origin.x = frmTxtJumTanggungan.origin.x;
    frmTxtNamaPas.origin.y = frmTxtJumTanggungan.origin.y;
    frmTxtNamaPas.size.width = frmTxtJumTanggungan.size.width;
    
    frmVwLineNamaPas.origin.y = frmVwLineJumTanggungan.origin.y;
    frmVwLineNamaPas.origin.x = frmVwLineJumTanggungan.origin.x;
    frmVwLineNamaPas.size.width = frmVwLineJumTanggungan.size.width;
    
    frmVwNamaPasangan.size.height = frmVwLineNamaPas.origin.y + frmVwLineNamaPas.size.height + 8;
    
    frmVwNoID.origin.y = frmVwNamaPasangan.origin.y + frmVwNamaPasangan.size.height + 16;
    frmVwNoID.origin.x = frmVwJumlahTanggungan.origin.x;
    frmVwNoID.size.width = frmVwJumlahTanggungan.size.width;
    
    frmLblTtlNoId.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmLblTtlNoId.size.width = frmLblTtlJumTanggungan.size.width;
    
    frmTxtNoId.origin.x = frmTxtJumTanggungan.origin.x;
    frmTxtNoId.origin.y = frmTxtJumTanggungan.origin.y;
    frmTxtNoId.size.width = frmTxtJumTanggungan.size.width;
    
    frmVwLineNoID.origin.y = frmVwLineJumTanggungan.origin.y;
    frmVwLineNoID.origin.x = frmVwLineJumTanggungan.origin.x;
    frmVwLineNoID.size.width = frmVwLineJumTanggungan.size.width;
    
    frmVwNoID.size.height = frmVwLineNoID.origin.y + frmVwLineNoID.size.height + 8;
    
    frmVwTglLahir.origin.y = frmVwNoID.origin.y + frmVwNoID.size.height + 16;
    frmVwTglLahir.origin.x = frmVwJumlahTanggungan.origin.x;
    frmVwTglLahir.size.width = frmVwJumlahTanggungan.size.width;
    
    frmLblTglLahir.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmLblTglLahir.size.width = frmLblTtlJumTanggungan.size.width;
    
    frmVwJumlahTanggungan.origin.x = 0;
    frmVwJumlahTanggungan.origin.y = frmVwTglLahir.origin.y + frmVwTglLahir.size.height + 16;
    frmVwJumlahTanggungan.size.width = frmVwContentScroll.size.width;
    
    
    frmBtnCalender.origin.y = frmVwTglLahir.size.height/2 - frmBtnCalender.size.height/2;
    frmBtnCalender.origin.x = frmVwTglLahir.size.width - frmBtnCalender.size.width - 24;
    
    frmTxtTglLahir.origin.x = frmTxtJumTanggungan.origin.x;
    frmTxtTglLahir.origin.y = frmTxtJumTanggungan.origin.y;
    frmTxtTglLahir.size.width = frmBtnCalender.origin.x - (frmTxtTglLahir.origin.x * 2);
    
    frmVwLineTglLahir.origin.y = frmVwLineJumTanggungan.origin.y;
    frmVwLineTglLahir.origin.x = frmVwLineJumTanggungan.origin.x;
    frmVwLineTglLahir.size.width = frmVwLineJumTanggungan.size.width;
    frmVwLineTglLahir.size.height = 1;
    
    frmVwTglLahir.size.height = frmVwLineTglLahir.origin.y + frmVwLineTglLahir.size.height + 8;
    
    frmVwPerjanjianPisahHarta.origin.y = frmVwTglLahir.origin.y + frmVwTglLahir.size.height + 16;
    frmVwPerjanjianPisahHarta.origin.x = frmVwJumlahTanggungan.origin.x;
    frmVwPerjanjianPisahHarta.size.width = frmVwJumlahTanggungan.size.width;
    
    frmLblPerjanPisahHarta.origin.x = frmLblTtlJumTanggungan.origin.x;
    frmLblPerjanPisahHarta.size.width = frmLblTtlJumTanggungan.size.width;
    
    frmTxtPerjanPisahHarta.origin.x = frmTxtJumTanggungan.origin.x;
    frmTxtPerjanPisahHarta.origin.y = frmTxtJumTanggungan.origin.y;
    frmTxtPerjanPisahHarta.size.width = frmTxtJumTanggungan.size.width - frmBtnDown.size.width;
    
    frmBtnDown.origin.y = frmTxtPerjanPisahHarta.origin.y;
    frmBtnDown.origin.x = frmVwPerjanjianPisahHarta.size.width - frmBtnDown.size.width - 24;
    
    frmVwLinePerjan.origin.y = frmVwLineJumTanggungan.origin.y;
    frmVwLinePerjan.origin.x = frmVwLineJumTanggungan.origin.x;
    frmVwLinePerjan.size.width = frmVwLineJumTanggungan.size.width;
    
    frmVwContentScroll.size.height = frmVwPerjanjianPisahHarta.origin.y + frmVwPerjanjianPisahHarta.size.height + 16;
    
    //frmVwScroll.size.height = frmVwContentScroll.size.height;
    
    frmBtnOk.origin.x = frmVwContentTac.size.width - frmBtnOk.size.width - 24;
    frmBtnOk.origin.y = frmVwContentTac.size.height - frmBtnOk.size.height - 16;
    
    frmVwScroll.size.height = frmBtnOk.origin.y - frmBtnOk.size.height - 16;
    
    //frmVwContentTac.size.height = frmBtnOk.origin.y + frmBtnOk.size.height + 24;
    
    self.vwMainTac.frame = frmVwMainTac;
    self.vwContentTac.frame = frmVwContentTac;
    self.btnExit.frame = frmBtnExit;
    self.btnOk.frame = frmBtnOk;
    self.vwScroll.frame = frmVwScroll;
    self.vwContentScroll.frame = frmVwContentScroll;
    self.vwJumTanggungan.frame = frmVwJumlahTanggungan;
    self.lblTitleJumTanggungan.frame = frmLblTtlJumTanggungan;
    self.txtJumTanggungan.frame = frmTxtJumTanggungan;
    self.vwLineJumTanggungan.frame = frmVwLineJumTanggungan;
    self.vwNamaPasangan.frame = frmVwNamaPasangan;
    self.lblTitleNamaPas.frame = frmLblTtlNamaPas;
    self.txtNamaPas.frame = frmTxtNamaPas;
    self.vwLineNamaPas.frame = frmVwLineNamaPas;
    self.vwNoID.frame = frmVwNoID;
    self.lblTitleNoID.frame = frmLblTtlNoId;
    self.txtNoId.frame = frmTxtNoId;
    self.vwLineNoId.frame = frmVwLineNoID;
    self.vwTglLahirPasangan.frame = frmVwTglLahir;
    self.lblTitleTglLahirPas.frame = frmLblTglLahir;
    self.btnCalender.frame = frmBtnCalender;
    self.txtTglLahirPas.frame = frmTxtTglLahir;
    self.vwLinTglLahirPas.frame = frmVwLineTglLahir;
    

//    self.vwPerjanjianPisahHarta.frame = frmVwPerjanjianPisahHarta;
//    self.lblTitlePerjanjian.frame = frmLblPerjanPisahHarta;
    [self.vwPerjanjianPisahHarta setHidden:YES];
    [self.lblTitlePerjanjian setHidden:YES];

    self.btnDown.frame = frmBtnDown;
    self.txtPerjanjianPisah.frame = frmTxtPerjanPisahHarta;
    self.vwLinePernjanjianPisah.frame = frmVwLinePerjan;
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    
}

-(void)setMartialState : (int) mState{
    _mState = mState;
}

-(void) saveAdditionalParam : (UIButton *) sender{
    bool isValid = TRUE;
    NSString *spouseBod, *spouseName, *spouseId, *sParenup, *strFamilyDepedent;
    NSArray *arDate = [_txtTglLahirPas.text componentsSeparatedByString:@"-"];
    NSString *msg = @"";

    if ([arDate count] > 1) {
        spouseBod = [NSString stringWithFormat:@"%@%@%@", arDate[2], arDate[1], arDate[0]];
    }
    else{
        spouseBod = @"";
    }
    
    spouseName = [Utility stringTrim:_txtNamaPas.text];
    spouseId = [Utility stringTrim:_txtNoId.text];
    
    if (nPrenup == 0) {
        sParenup = @"Y";
    }else if(nPrenup == 1){
        sParenup = @"N";
    }else{
        sParenup = @"";
    }
    
    
    strFamilyDepedent = [Utility stringTrim:_txtJumTanggungan.text];
    
    if (_mState == 1) {
        
        if(spouseId.length != 16 || [spouseId doubleValue] == 0){
            spouseId = @"";
            if([lang isEqualToString:@"id"]){
                msg = @"Data Nomor Identitas Tidak Valid, Periksa kembali data anda";
            }else{
                msg = @"Invalid ID Data, Please check your data again";
            }
        }
        
        if (![spouseName isEqualToString:@""] &&
            ![spouseId isEqualToString:@""] &&
            ![spouseBod isEqualToString:@""]
//            && ![sParenup isEqualToString:@""]
            ) {
            [dataManager.dataExtra setValue:spouseName forKey:@"spouse_name"];
            [dataManager.dataExtra setValue:spouseId forKey:@"spouse_id"];
            [dataManager.dataExtra setValue:spouseBod forKey:@"spouse_bod"];
//            [dataManager.dataExtra setValue:sParenup forKey:@"prenup"];
        }else{
            isValid = false;
        }
    }
    
    if (![strFamilyDepedent isEqualToString:@""]) {
        [dataManager.dataExtra setValue:strFamilyDepedent forKey:@"no_family_dependants"];
    }else{
        isValid = false;
    }
    
    
    if (isValid) {
        NSDictionary* userInfo = @{@"data_state": @"1"};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"popupDataPas" object:self userInfo:userInfo];
        
        [delegate doneState:YES];
        if(_mState == 1){
//            [delegate namaPasangan:[dataManager.dataExtra valueForKey:@"spouse_name"]];
            [dataManager.dataExtra setValue:spouseId forKey:@"nik_no"];
            [dataManager.dataExtra setValue:spouseName forKey:@"spouse_name"];
            [dataManager.dataExtra setValue:spouseBod forKey:@"spouse_bod"];
            
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:@"request_type=cashfin_requested,step=verify_dukcapil,nik_no,spouse_name,spouse_bod" needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }else{
            [delegate namaPasangan:[dataManager.dataExtra valueForKey:@"no_family_dependants"]];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }else{
        if([msg isEqualToString:@""]){
            if([lang isEqualToString:@"id"]){
                msg = @"Lengkapi Data Terlebih dahulu";
            }else{
                msg = @"Compelete the Data First";
            }
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
   
    
}

- (void) close{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) setDelegate:(id)newDelegate {
   delegate = newDelegate;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"cashfin_requested"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            [delegate doneState:YES];
            if(_mState == 1){
                [delegate namaPasangan:[dataManager.dataExtra valueForKey:@"spouse_name"]];
            }
            [self close];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [Utility resetTimer];
}

@end
