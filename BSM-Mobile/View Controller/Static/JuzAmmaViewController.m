//
//  JuzAmmaViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/23/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "JuzAmmaViewController.h"
#import "UIView+SimpleRipple.h"
@interface JuzAmmaViewController ()< UIGestureRecognizerDelegate, UIScrollViewDelegate>{
    NSString *lang;
    NSUserDefaults *userDefault;
    NSMutableArray *textFields;
    NSDictionary *data;
    //FSPagerView *surahView;
    CGFloat lastScale;
    UIImageView *imgSurah;
    UIScrollView *scvSurah;
    int posX, _current_page, _prev_next_index;
    bool stateDeepNotif;
    NSString *menuTitle;
    
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSurah;
@property (weak, nonatomic) IBOutlet UIView *vwDisplaySurah;
@property (weak, nonatomic) IBOutlet UIView *vwBtnSurah;
@property (weak, nonatomic) IBOutlet UIButton *btnSurah;

@property (strong, nonatomic) NSMutableArray<NSString *> *imageNames;
@property (strong, nonatomic) NSString *mSurahs;
@property (strong, nonatomic) NSMutableArray *mDictSurahs;
@property (strong, nonatomic) NSArray *listParamData;
@property (weak, nonatomic) IBOutlet UILabel *lblJumAyatSurah;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

@implementation JuzAmmaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    stateDeepNotif = TRUE;

    [self.vwTitle setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    
    if (menuTitle != nil) {
        [self.lblTitle setText:menuTitle];
    }else{
        if ([lang isEqualToString:@"id"]) {
               [self.lblTitle setText:@"Juz Amma"];
           }else{
               [self.lblTitle setText:@"Juz Amma"];
           }
    }
    
    [self.lblTitle sizeToFit];
    
    self.imageNames = [[NSMutableArray alloc]init];
    for (int i = 604; i>581; i-- ){
        [self.imageNames addObject:[NSString stringWithFormat:@"%d.png",i]];
       
    }
    
    _mSurahs = @"78//An Naba’//Berita Besar//The Announcement//40//23||79//An Naazi’at//Malaikat-malaikat Yang Mencabut//Those who drag forth//46//22||80//‘Abasa//Ia Bermuka Masam//He frowned//42//20||81//At Takwir//Menggulung//The Overthrowing//29//19||82//Al Infithar//Terbelah//The Cleaving//19//18||83//Al Muthaffifin//Orang-orang Yang Curang//Defrauding//36//18||84//Al Insyiqaq//Terbelah//The Splitting Open//25//16||85//Al Buruuj//Gugusan Bintang//The Constellations//22//15||86//Ath Thariq//Yang Datang Dimalam Hari//The Night Comer//17//14||87//Al A’laa//Yang Paling Tinggi//The Most High//19//14||88//Al Ghaasyiah//Hari Pembalasan//The Overwhelming//26//13||89//Al Fajr//Fajar//The Dawn//30//12||90//Al Balad//Negeri//The City//20//11||91//Asy Syams//Matahari//The Sun//15//10||92//Al Lail//Malam//The Night//21//10||93//Adh Dhuhaa//Waktu matahari sepenggalahan naik (Dhuha)//The Morning Hours//11//9||94//Asy Syarh//Melapangkan//The Consolation//8//9||95//At Tiin//Buah Tin//The Fig//8//8||96//Al ‘Alaq//Segumpal Darah//The Clot//19//8||97//Al Qadr//Kemuliaan//The Power, Fate//5//7||98//Al Bayyinah//Pembuktian//The Evidence//8//7||99//Az Zalzalah//Kegoncangan//The Earthquake//8//6||100//Al ‘Aadiyah//Berlari kencang//The Chargers//11//6||101//Al Qaari’ah//Hari Kiamat//The Calamity//11//5||102//At Takaatsur//Bermegah-megahan//Competition//8//5||103//Al ‘Ashr//Masa (Waktu)//The Declining Day//3//4||104//Al Humazah//Pengumpat//The Traducer//9//4||105//Al Fiil//Gajah//The Elephant//5//4||106//Quraisy//Suku Quraisy//Quraysh//4//3||107//Al Maa’uun//Barang-barang yang berguna//Almsgiving//7//3||108//Al Kautsar//Nikmat yang berlimpah//Abundance//3//3||109//Al Kafirun//Orang-orang kafir//The Disbelievers//6//2||110//An Nashr//Pertolongan//Divine Support//3//2||111//Al Lahab//Gejolak Api//The Father of Flame//5//2||112//Al Ikhlash//Ikhlas//Sincerity//4//1||113//Al Falaq//Waktu Subuh//The Dawn//5//1||114//An Naas//Manusia//Mankind//6//1";
    
    NSArray *mSplitSurah = [_mSurahs componentsSeparatedByString:@"||"];
    self.mDictSurahs = [NSMutableArray new];
    if ([mSplitSurah count] != 0) {
        for (int i=0; i<[mSplitSurah count]; i++) {
            NSArray *mSplitDtlSurah = [[mSplitSurah objectAtIndex:i]componentsSeparatedByString:@"//"];
            if ([mSplitDtlSurah count] !=0) {
                NSMutableDictionary *objSurah = [NSMutableDictionary new];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:0] forKey:@"surah_no"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:1] forKey:@"ayat_name"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:2] forKey:@"meaning_id"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:3] forKey:@"meaning_en"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:4] forKey:@"total_surah"];
                [objSurah setValue:[mSplitDtlSurah objectAtIndex:5] forKey:@"idx"];
                [_mDictSurahs addObject:objSurah];
            }
            
        }
    }
    data = [self.mDictSurahs objectAtIndex:0];
    [self.lblJumAyatSurah setTextColor:UIColorFromRGB(const_color_primary)];
    
    [self setupLayout];
    
    
    posX = 0;
    _current_page=0;
    _prev_next_index = 0;
    scvSurah = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.vwDisplaySurah.frame.size.width, self.vwDisplaySurah.frame.size.height)];
    scvSurah.pagingEnabled=YES;
    scvSurah.delegate = self;
    
    [self setUpPager];
    [self.vwDisplaySurah addSubview:scvSurah];
    
    [self.btnSurah addTarget:self action:@selector(actionSelectedTochDown:event:) forControlEvents:UIControlEventTouchDown];
    [self.btnSurah addTarget:self action:@selector(actionOpenFiltering:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
//    [self handleTimer:@"START_TIMER"];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(filterSurah:)
        name:@"filterSurah"
      object:nil];
}


- (void)setUpPager
{
       for(int i=0;i<[self.imageNames count];i++)
   {
    // Image view
    UIScrollView  *image_scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(i * self.vwDisplaySurah.frame.size.width, 0, self.vwDisplaySurah.frame.size.width, self.vwDisplaySurah.frame.size.height)];
    UIImageView *image_view = [[UIImageView alloc] initWithFrame:image_scroll.bounds];
    image_view.contentMode = UIViewContentModeRedraw;
    image_view.backgroundColor=[UIColor clearColor];
    image_view.clipsToBounds = true;
    image_view.image = [UIImage imageNamed:self.imageNames[i]];
//       if (IPHONE_8 || IPHONE_8P ) {
//           CGRect frmImg = image_view.frame;
//
//           frmImg.origin.x = 24;
//           frmImg.size.width = SCREEN_WIDTH - (frmImg.origin.x *2);
//
//           image_view.frame = frmImg;
//       }

    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    [doubleTap setNumberOfTapsRequired:2];
    [image_scroll setDelegate:self];
    [image_scroll setShowsHorizontalScrollIndicator:NO];
    [image_scroll setShowsVerticalScrollIndicator:NO];

    image_scroll.tag=i;
    image_scroll.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    image_view.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;

    
    image_scroll.contentSize = image_scroll.bounds.size;
    image_scroll.contentInset = UIEdgeInsetsMake(0, 0, 0, 8);
//    image_scroll.contentSize = CGSizeMake(image_view.frame.size.width, image_scroll.frame.size.height);
    image_scroll.decelerationRate = UIScrollViewDecelerationRateFast;

    image_scroll.minimumZoomScale = 1.0f;
    image_scroll.maximumZoomScale = 3.0f;

    [image_scroll addSubview:image_view];
    [image_scroll addGestureRecognizer:doubleTap];
    [scvSurah addSubview:image_scroll];
    posX=posX+self.vwDisplaySurah.frame.size.width;
//    posX=posX+image_view.frame.size.width;
   }
   scvSurah.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
   scvSurah.tag=100;
   scvSurah.contentSize=CGSizeMake(posX, scvSurah.frame.size.height);
   scvSurah.contentOffset=CGPointMake(0, 0);
 }

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer
{
 // zoom in
  NSLog(@"handleDoubleTap");
  //[self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
  if(gestureRecognizer.view == scvSurah) return;
  UIScrollView *doubleTapScroll = [self getImageScroller];
  if(doubleTapScroll)
  {
     //zoom
     CGPoint pointInView = [gestureRecognizer locationInView:self.view];

     CGFloat newZoomScale = doubleTapScroll.zoomScale * 2.0f;

     newZoomScale = MIN(newZoomScale, doubleTapScroll.maximumZoomScale);
     CGRect rectToZoomTo = CGRectZero;
     CGFloat currentZoomScale = doubleTapScroll.zoomScale;
     if(currentZoomScale == 1.0f)
     {
        rectToZoomTo = [self zoomRectForScrollView:doubleTapScroll withScale:newZoomScale withCenter:pointInView];//CGRectMake(x, y, w, h);
     }
     else if (currentZoomScale >= 1.0f)
     {
         rectToZoomTo = [self zoomRectForScrollView:doubleTapScroll withScale:1 withCenter:self.view.center];
     }
     [doubleTapScroll zoomToRect:rectToZoomTo animated:YES];
  }
}

- (CGRect)zoomRectForScrollView:(UIScrollView *)scrollView withScale:(float)scale withCenter:(CGPoint)center
{
//   [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
   CGRect zoomRect;

   zoomRect.size.height = scrollView.frame.size.height / scale;
   zoomRect.size.width  = scrollView.frame.size.width  / scale;

   zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0);
   zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0);
   return zoomRect;
}

- (UIScrollView *)getImageScroller
{
    return  [scvSurah viewWithTag:_current_page];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    stateDeepNotif = true;
    if(scrollView == scvSurah)
    {
       float currentPage = scrollView.contentOffset.x /scrollView.frame.size.width;
       if(currentPage != _current_page)
       {
           _prev_next_index = _current_page;
           _current_page = ceil(currentPage);
           [self resetScrollerAtViewIndex:_prev_next_index];
           [self moveToPosSurah:currentPage];
           
       }
    }
}

-(void) moveToPosSurah : (float) currentPage{
    int pageIdx  = (int) currentPage + 1;
    for(NSDictionary *xDictSurahs in _mDictSurahs){
        int mIdx = [[xDictSurahs valueForKey:@"idx"]intValue];
        if (mIdx == pageIdx) {
            [self.lblSurah setText:[xDictSurahs valueForKey:@"ayat_name"]];
            [self.lblJumAyatSurah setText:[NSString stringWithFormat:@"%@ Ayat",[xDictSurahs valueForKey:@"total_surah"]]];
            
            [self.lblSurah sizeToFit];
            [self.lblJumAyatSurah sizeToFit];
            
            [self reconPost];
            break;
        }
    }
}

-(void)actionSelectedTochDown : (UIButton *) sender
                        event : (UIEvent *) event{
   UITouch *touch = [[event touchesForView:sender] anyObject];
    CGPoint origin = [touch locationInView:sender];
    float radius = sender.frame.size.height;
    float duration = 0.5f;
    float fadeAfter = duration * 0.75f;
    
    [sender rippleStartingAt:origin withColor:[UIColor colorWithWhite:0.0f alpha:0.20f] duration:duration radius:radius fadeAfter:fadeAfter];
    
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView1
{
//  [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
  if(scrollView1 == scvSurah)
      return nil;
  else
  {
      UIScrollView  *image_scroll = [self getImageScroller];
      if(image_scroll)
          if([image_scroll.subviews count]>0)
          {
              return [image_scroll.subviews objectAtIndex:0];
          }
   }
   return nil;
}

- (void)resetScrollerAtViewIndex:(NSInteger)index
{
// [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
  //CGFloat zoomScale = 1.0;
  UIScrollView *scrollView = [scvSurah viewWithTag:index];
  CGRect rectToZoomTo = CGRectZero;

  rectToZoomTo = [self zoomRectForScrollView:scrollView withScale:1 withCenter:self.vwDisplaySurah.center];
  [scrollView zoomToRect:self.vwDisplaySurah.bounds animated:YES];
  [scrollView setContentOffset:CGPointZero];
  [scrollView setContentSize:self.vwDisplaySurah.bounds.size];
    
//    NSPredicate *filter = [NSPredicate predicateWithFormat:@"idx contains[c] %@ ",[NSString stringWithFormat:@"%ld",index +1]];
//    NSArray *filteredSurah = [_mDictSurahs filteredArrayUsingPredicate:filter];
//    if (filteredSurah.count != 0) {
//        NSDictionary *dataSurah = [filteredSurah objectAtIndex:filteredSurah.count-1];
//        [self.lblSurah setText:[dataSurah valueForKey:@"ayat_name"]];
//        [self.lblJumAyatSurah setText:[NSString stringWithFormat:@"%@ Ayat",[dataSurah valueForKey:@"total_surah"]]];
//
//        [self.lblSurah sizeToFit];
//        [self.lblJumAyatSurah sizeToFit];
//    }
}



-(void)setupLayout{
    CGRect frmVwTitle  = self.vwTitle.frame;
    CGRect frmVwDisplaySurah = self.vwDisplaySurah.frame;
    
    CGRect frmImgIcon = self.imgIcon.frame;
    CGRect frmlblTitle = self.lblTitle.frame;
    CGRect frmVwBtnSurah = self.vwBtnSurah.frame;
    
    CGRect frmLblSurah = self.lblSurah.frame;
    CGRect frmLblJumSurah = self.lblJumAyatSurah.frame;
    CGRect frmImgDropDown = self.imgDropDown.frame;
    CGRect frmBtnSurah = self.btnSurah.frame;
    
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.origin.x = 0;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    CGFloat sizeActualContent = frmVwTitle.size.width - frmImgIcon.origin.x;
    
    frmlblTitle.origin.x = frmImgIcon.origin.x + frmImgIcon.size.width + 8;
    frmlblTitle.origin.y = frmVwTitle.size.height/2 - frmlblTitle.size.height/2;
    
    frmVwBtnSurah.origin.x = frmlblTitle.origin.x + frmlblTitle.size.width + 8;
    frmVwBtnSurah.origin.y = 0;
    frmVwBtnSurah.size.width = frmVwTitle.size.width - frmVwBtnSurah.origin.x;
    frmVwBtnSurah.size.height = frmVwTitle.size.height;
    
    frmImgDropDown.origin.x = frmVwBtnSurah.size.width - frmImgDropDown.size.width - 16;
    frmImgDropDown.origin.y = frmVwBtnSurah.size.height/2 - frmImgDropDown.size.height/2;
    
    frmBtnSurah.origin.y = 0;
    frmBtnSurah.size.width = 16 + frmImgDropDown.size.width + 16;
    frmBtnSurah.origin.x = frmVwBtnSurah.size.width - frmBtnSurah.size.width;
    
    frmLblSurah.origin.x = 0;
    frmLblSurah.origin.y = 8;
    frmLblSurah.size.width = frmVwBtnSurah.size.width - frmBtnSurah.size.width - 10;
    
    frmLblJumSurah.origin.x = 0;
    frmLblJumSurah.origin.y = frmLblSurah.origin.y + frmLblSurah.size.height;
    frmLblJumSurah.size.width = frmLblSurah.size.width;
    
    
    
    frmVwDisplaySurah.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmVwDisplaySurah.origin.x = 0;
    frmVwDisplaySurah.size.width = SCREEN_WIDTH;
    frmVwDisplaySurah.size.height = SCREEN_HEIGHT - frmVwDisplaySurah.origin.y - BOTTOM_NAV - 20;
    
    if ([Utility isDeviceHaveBottomPadding]) {
        frmVwDisplaySurah.size.height = SCREEN_HEIGHT - frmVwDisplaySurah.origin.y - BOTTOM_NAV - 40;
    }
    
    if (IPHONE_8 || IPHONE_8P) {
        frmVwDisplaySurah.origin.x = 24;
        frmVwDisplaySurah.size.width = SCREEN_WIDTH - (frmVwDisplaySurah.origin.x*2);
    }
    
    if (IPHONE_5) {
        frmVwDisplaySurah.origin.x = 16;
        frmVwDisplaySurah.size.width = SCREEN_WIDTH - (frmVwDisplaySurah.origin.x*2);
    }
    
    self.vwTitle.frame = frmVwTitle;
    self.vwDisplaySurah.frame = frmVwDisplaySurah;
    self.lblTitle.frame = frmlblTitle;
    self.vwBtnSurah.frame = frmVwBtnSurah;
    self.btnSurah.frame = frmBtnSurah;
    self.lblSurah.frame = frmLblSurah;
    self.lblJumAyatSurah.frame = frmLblJumSurah;
    self.imgDropDown.frame=frmImgDropDown;
    self.btnSurah.frame = frmBtnSurah;
    
    
}


- (void)viewDidAppear:(BOOL)animated{
    float idx = self.imageNames.count -1;
   // [surahView scrollToItemAtIndex:idx animated:false];
    _current_page = idx;
    _prev_next_index = _current_page;
    [self resetScrollerAtViewIndex:_prev_next_index];
    
     NSDictionary *dataSurah = [_mDictSurahs objectAtIndex:0];
    
    [self.lblSurah setText:[dataSurah valueForKey:@"ayat_name"]];
    [self.lblJumAyatSurah setText:[NSString stringWithFormat:@"%@ Ayat",[dataSurah valueForKey:@"total_surah"]]];
    
    [self.lblSurah sizeToFit];
    [self.lblJumAyatSurah sizeToFit];
    
    [self reconPost];
   
    CGPoint rightOffset = CGPointMake(scvSurah.contentSize.width - scvSurah.bounds.size.width + scvSurah.contentInset.right, 0);
    [scvSurah setContentOffset:rightOffset animated:YES];
    
    
}

-(void)actionOpenFiltering : (UIButton *) sender{
    stateDeepNotif = true;
    UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"JuzAmmaFilterVC"];
    [self presentViewController:viewCont animated:YES completion:nil];
}

- (void) filterSurah:(NSNotification *) notification {
    if ([notification.name isEqualToString:@"filterSurah"]){
        if (stateDeepNotif) {
            stateDeepNotif = false;
            NSDictionary* userInfo = notification.userInfo;
            
            [self.lblSurah setText:[userInfo valueForKey:@"ayat_name"]];
            [self.lblJumAyatSurah setText:[NSString stringWithFormat:@"%@ Ayat",[userInfo valueForKey:@"total_surah"]]];
            
            [self.lblSurah sizeToFit];
            [self.lblJumAyatSurah sizeToFit];
            
            [self reconPost];
            
            NSString *sIdx = [userInfo valueForKey:@"idx"];
            NSInteger idx = [sIdx integerValue]-1;
            CGPoint moveToIdx = CGPointMake(idx * self.vwDisplaySurah.frame.size.width, 0);
            [scvSurah setContentOffset:moveToIdx animated:YES];
            float currentPage = moveToIdx.x /scvSurah.frame.size.width;
            if(currentPage != _current_page)
            {
                _prev_next_index = _current_page;
                _current_page = ceil(currentPage);
                [self resetScrollerAtViewIndex:_prev_next_index];
                
            }
            
//            float currentPage = moveToIdx.x /self.vwDisplaySurah.frame.size.width;
//            [self moveToPosSurah:currentPage];
            
        }
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
//    [self handleTimer:@"STOP_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void) reconPost{
    CGRect frmVwBtnSurah = self.vwBtnSurah.frame;
    CGRect frmLblSurah = self.lblSurah.frame;
    CGRect frmLblJumSurah = self.lblJumAyatSurah.frame;
    CGRect frmBtnSurah = self.btnSurah.frame;
    
    frmLblSurah.origin.x = 0;
    frmLblSurah.size.width = frmVwBtnSurah.size.width - frmBtnSurah.size.width;
    
    frmLblJumSurah.origin.x = 0;
    frmLblJumSurah.size.width = frmLblSurah.size.width;
    
    self.lblSurah.frame = frmLblSurah;
    self.lblJumAyatSurah.frame = frmLblJumSurah;
}


-(void)handleTimer : (NSString *) modeStr{
//    NSDictionary* userInfo = @{@"actionTimer": modeStr};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

-(void) setTitleMenu : (NSString *) mStrTitle{
    menuTitle = mStrTitle;
}


@end
