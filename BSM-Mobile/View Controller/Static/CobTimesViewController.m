//
//  CobTimesViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 25/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "CobTimesViewController.h"

@interface CobTimesViewController (){
    NSString *message;
}
@property (weak, nonatomic) IBOutlet UIView *viewBackgroundMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonOK;

@end

@implementation CobTimesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];

    if([Utility isLanguageID]){
        self.labelTitle.text = @"Pembiayaan";
    }else{
        self.labelTitle.text = @"Financing";
    }
    
    [self.buttonOK addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
    [self.btnBack addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
    self.labelMessage.text = message;
    [self.viewBackgroundMessage.layer setCornerRadius:10];
    [self.view setBackgroundColor:UIColorFromRGB(const_color_primary)];
    
}

- (void)setMessage:(NSString *)string{
    message = string;
}

- (void)actionOK{
    [self backToR];
}

@end

