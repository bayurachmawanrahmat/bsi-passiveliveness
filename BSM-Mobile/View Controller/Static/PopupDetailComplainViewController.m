//
//  PopupDetailComplainViewController.m
//  BSM-Mobile
//
//  Created by ARS on 24/01/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PopupDetailComplainViewController.h"
#import "CustomBtn.h"
#import "NSUserdefaultsAes.h"

@interface PopupDetailComplainViewController (){
    NSDictionary *data;
    UIImage *image;
    NSUserDefaults *userDefault;
    NSString *lang;
}
@property (strong, nonatomic) IBOutlet UIView *vwRoot;

@property (weak, nonatomic) IBOutlet UIView *vwPopup;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwContainer;
@property (weak, nonatomic) IBOutlet UIView *vwDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblFullDate;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleAccName;
@property (weak, nonatomic) IBOutlet UILabel *lblAccName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNoRek;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRek;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNoRef;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRef;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleReason;
@property (weak, nonatomic) IBOutlet UILabel *lblReason;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleTanggapan;
@property (weak, nonatomic) IBOutlet UILabel *lblTanggapan;

@property (weak, nonatomic) IBOutlet CustomBtn *btnOK;

@end

@implementation PopupDetailComplainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if(@available(iOS 13.0, *)){
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([[data objectForKey:@"popimage"]isEqualToString:@"YES"]){
        [self setupImage];
    }else{
        [self setupLanguage];
        [self setupContent];
        [self setupLayout];
    }
    
    UITapGestureRecognizer *tapDismiss =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(dismissPopUp:)];
    [self.vwRoot addGestureRecognizer:tapDismiss];
    
}

-(void)dismissPopUp:(UITapGestureRecognizer *)recognizer{
    if([[data objectForKey:@"popimage"]isEqualToString:@"YES"]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void) dataBundle : (NSDictionary *) dataBind{
    data = dataBind;
}

- (void) setImages : (UIImage *)passingImage{
    image = passingImage;
}
- (void) setupImage{
    
    float ratio = image.size.width /image.size.height;
    float xFinal = SCREEN_WIDTH;
    float yfinal = xFinal/ratio;
//    UIImage *scaledImage = [self imageWithImage:image scaledToSize:CGSizeMake(xFinal, yfinal)];
    
    UIImageView *viewImage =[[UIImageView alloc] initWithFrame:CGRectMake(0,(SCREEN_HEIGHT-yfinal)/2,xFinal,yfinal)];
    viewImage.image= image;
    [self.vwPopup setHidden:true];
    [self.view addSubview:viewImage];
    
}

- (UIImage *)imageWithImage:(UIImage*)originalImage scaledToSize:(CGSize)newSize;
{
    @synchronized(self)
    {
        UIGraphicsBeginImageContext(newSize);
        [originalImage drawInRect:CGRectMake(0,0,newSize.width, newSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    return nil;
}

- (void) setupLayout{
    
    CGRect frmVwRoot = self.vwRoot.frame;
    CGRect frmVwPopup = self.vwPopup.frame;
    CGRect frmVwTtlLbl = self.lblTitle.frame;
    CGRect frmVwContain = self.vwContainer.frame;
    
    CGRect frmVwDate = self.vwDate.frame;
    CGRect frmLblDate = self.lblDate.frame;
    CGRect frmLblMonth = self.lblMonth.frame;
    
    CGRect frmTtlLblName = self.lblTitleAccName.frame;
    CGRect frmlblName = self.lblAccName.frame;
    
    CGRect frmTtlEmail = self.lblTitleEmail.frame;
    CGRect frmLblEmail = self.lblEmail.frame;
    
    CGRect frmTtlNorek = self.lblTitleNoRek.frame;
    CGRect frmLblNorek = self.lblNoRek.frame;
    
    CGRect frmTtlNoref = self.lblTitleNoRef.frame;
    CGRect frmlblNoref = self.lblNoRef.frame;
    
    CGRect frmTtlCategory = self.lblTitleCategory.frame;
    CGRect frmLblCategory = self.lblCategory.frame;
    
    CGRect frmTtlReason = self.lblTitleReason.frame;
    CGRect frmLblReason = self.lblReason.frame;
    
    CGRect frmTtlTanggapan = self.lblTitleTanggapan.frame;
    CGRect frmLblTanggapan = self.lblTanggapan.frame;
    
//    [self.lblTitleAccName sizeToFit];
    [_lblAccName sizeToFit];
    [_lblAccName setLineBreakMode:NSLineBreakByWordWrapping];

//    [self.lblTitleEmail sizeToFit];
    [_lblEmail sizeToFit];
    [_lblEmail setLineBreakMode:NSLineBreakByCharWrapping];
//    [self.lblTitleNoRek sizeToFit];
    [_lblNoRek sizeToFit];
    
//    [self.lblTitleNoRef sizeToFit];
    [_lblNoRef sizeToFit];
    
//    [self.lblTitleCategory sizeToFit];
    [_lblCategory sizeToFit];
    [_lblCategory setLineBreakMode:NSLineBreakByWordWrapping];

//    [self.lblTitleReason sizeToFit];
    [_lblReason sizeToFit];
    [_lblReason setLineBreakMode:NSLineBreakByWordWrapping];

//    [self.lblTitleTanggapan sizeToFit];
    [_lblTanggapan sizeToFit];
    
    
    frmVwRoot.origin.x = 0;
    frmVwRoot.origin.y = 0;
    frmVwRoot.size.width = SCREEN_WIDTH;
    frmVwRoot.size.height = SCREEN_HEIGHT;
    
    frmVwPopup.origin.x = 16;
    frmVwPopup.size.width = SCREEN_WIDTH - 32;
    
    frmVwTtlLbl.origin.x = 0;
    frmVwTtlLbl.origin.y = 0;
    frmVwTtlLbl.size.width = frmVwPopup.size.width;
    frmVwTtlLbl.size.height = 40;
    
    frmVwContain.origin.x = 0;
    frmVwContain.origin.y = frmVwTtlLbl.origin.y + frmVwTtlLbl.size.height;
    frmVwContain.size.width = frmVwPopup.size.width;
    
    frmVwDate.origin.x = 8;
    frmVwDate.origin.y = 8;
    frmVwDate.size.height = 75;
    frmVwDate.size.width = 75;
    
    frmLblDate.origin.x = 0;
    frmLblDate.size.width = frmVwDate.size.width;
    
    frmLblMonth.origin.x = 0;
    frmLblMonth.size.width = frmVwDate.size.width;
    
    CGFloat lblWidth = (frmVwContain.size.width - 32) - (frmVwDate.size.width - 8);
    //name
    frmTtlLblName.origin.x = frmVwDate.size.width + frmVwDate.origin.x + 8;
    frmTtlLblName.size.width = lblWidth;
    
    frmlblName.origin.x = frmTtlLblName.origin.x;
    frmlblName.origin.y = frmTtlLblName.origin.y + frmTtlLblName.size.height;
    frmlblName.size.width = lblWidth;
    frmlblName.size.height = [self heightForLabel:_lblAccName withWidth:lblWidth];
    //email
    frmTtlEmail.origin.x = frmlblName.origin.x;
    frmTtlEmail.origin.y = frmlblName.origin.y + frmlblName.size.height + 8;
    frmTtlEmail.size.width = lblWidth;
    
    frmLblEmail.origin.x = frmlblName.origin.x;
    frmLblEmail.origin.y = frmTtlEmail.origin.y + frmTtlEmail.size.height;
    frmLblEmail.size.width = lblWidth;
    frmLblEmail.size.height = [self heightForLabel:_lblEmail withWidth:lblWidth];
    
    //norek
    frmTtlNorek.origin.x = frmlblName.origin.x;
    frmTtlNorek.origin.y = frmLblEmail.origin.y + frmLblEmail.size.height + 8;
    frmTtlNorek.size.width = lblWidth;
    
    frmLblNorek.origin.x = frmlblName.origin.x;
    frmLblNorek.origin.y = frmTtlNorek.origin.y + frmTtlNorek.size.height;
    frmLblNorek.size.width = lblWidth;
    
    //noref
    frmTtlNoref.origin.x = frmlblName.origin.x;
    frmTtlNoref.origin.y = frmLblNorek.origin.y + frmLblNorek.size.height + 8;
    frmTtlNoref.size.width = lblWidth;
    
    frmlblNoref.origin.x = frmlblName.origin.x;
    frmlblNoref.origin.y = frmTtlNoref.origin.y + frmTtlNoref.size.height;
    frmlblNoref.size.width = lblWidth;
    
    //category
    frmTtlCategory.origin.x = frmlblName.origin.x;
    frmTtlCategory.origin.y = frmlblNoref.origin.y + frmlblNoref.size.height + 8;
    frmTtlCategory.size.width = lblWidth;
    
    frmLblCategory.origin.x = frmlblName.origin.x;
    frmLblCategory.origin.y = frmTtlCategory.origin.y + frmTtlCategory.size.height;
    frmLblCategory.size.width = lblWidth;
    frmLblCategory.size.height = [self heightForLabel:_lblCategory withWidth:lblWidth];
    
    //reason
    frmTtlReason.origin.x = frmlblName.origin.x;
    frmTtlReason.origin.y = frmLblCategory.origin.y + frmLblCategory.size.height + 8;
    frmTtlReason.size.width = lblWidth;
    
    frmLblReason.origin.x = frmlblName.origin.x;
    frmLblReason.origin.y = frmTtlReason.origin.y + frmTtlReason.size.height;
    frmLblReason.size.width = lblWidth;
    frmLblReason.size.height = [self heightForLabel:_lblReason withWidth:lblWidth];
//    frmLblReason.size.height = [self getLabelHeight:_lblReason];
    
    //tanggapan
    frmTtlTanggapan.origin.x = frmlblName.origin.x;
    frmTtlTanggapan.origin.y = frmLblReason.origin.y + frmLblReason.size.height + 8;
    frmTtlTanggapan.size.width = lblWidth;
    
    frmLblTanggapan.origin.x = frmlblName.origin.x;
    frmLblTanggapan.origin.y = frmTtlTanggapan.origin.y + frmTtlTanggapan.size.height;
    frmLblTanggapan.size.width = lblWidth;
    
    frmVwContain.size.height = frmLblTanggapan.origin.y + frmLblTanggapan.size.height + 16;
    
    frmVwPopup.size.height = frmVwContain.origin.y + frmVwContain.size.height;
    
    
//    self.vwRoot.frame = frmVwRoot;
    
//    self.vwPopup.frame = frmVwPopup;
    self.vwPopup.layer.shadowColor = [[UIColor grayColor] CGColor];
//    self.vwPopup.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.vwPopup.layer.shadowOpacity = 2.0f;
    self.vwPopup.layer.shadowRadius = 7.0;
    self.vwPopup.layer.masksToBounds = NO;
    self.vwPopup.layer.cornerRadius = 16.0f;
    
//    self.lblTitle.frame = frmVwTtlLbl;
    self.lblTitle.textColor = [UIColor blackColor];
//    self.lblTitle.backgroundColor = UIColorFromRGB();
//    self.vwContainer.frame = frmVwContain;
//    self.vwContainer.layer.shadowOpacity = 2.0f;
//    self.vwContainer.layer.shadowRadius = 7.0;
    self.vwContainer.layer.masksToBounds = NO;
    self.vwContainer.layer.cornerRadius = 16.0f;
    self.vwContainer.layer.masksToBounds = YES;
    
//    self.vwDate.frame = frmVwDate;
    
//    self.lblDate.frame = frmLblDate;
//    self.lblDate.textAlignment = NSTextAlignmentCenter;
    self.lblDate.textColor = UIColorFromRGB(const_color_primary);
    
//    self.lblMonth.frame = frmLblMonth;
//    self.lblMonth.textAlignment = NSTextAlignmentCenter;
    self.lblMonth.textColor = UIColorFromRGB(const_color_primary);
    [self.lblMonth superview].backgroundColor = UIColorFromRGB(const_highlight_primary);
    
//    self.lblTitleAccName.frame = frmTtlLblName;
    self.lblTitleAccName.textColor = UIColorFromRGB(const_color_gray);
//    self.lblAccName.frame = frmlblName;
    _lblAccName.numberOfLines = 0;
    
//    self.lblTitleEmail.frame = frmTtlEmail;
    self.lblTitleEmail.textColor = UIColorFromRGB(const_color_gray);
//    self.lblEmail.frame = frmLblEmail;
    _lblEmail.numberOfLines = 0;
    
//    self.lblTitleNoRek.frame = frmTtlNorek;
    self.lblTitleNoRek.textColor = UIColorFromRGB(const_color_gray);
//    self.lblNoRek.frame = frmLblNorek;
    _lblNoRek.numberOfLines = 0;
    
//    self.lblTitleNoRef.frame = frmTtlNoref;
    self.lblTitleNoRef.textColor = UIColorFromRGB(const_color_gray);
//    self.lblNoRef.frame = frmlblNoref;
    _lblNoRef.numberOfLines = 0;
    
//    self.lblTitleCategory.frame = frmTtlCategory;
    self.lblTitleCategory.textColor = UIColorFromRGB(const_color_gray);
//    self.lblCategory.frame = frmLblCategory;
    self.lblCategory.numberOfLines = 0;
//    _lblCategory.numberOfLines
    
//    self.lblTitleReason.frame = frmTtlReason;
    self.lblTitleReason.textColor = UIColorFromRGB(const_color_gray);
//    self.lblReason.frame = frmLblReason;
    self.lblReason.numberOfLines = 0;
    
//    self.lblTitleTanggapan.frame = frmTtlTanggapan;
    self.lblTitleTanggapan.textColor = UIColorFromRGB(const_color_gray);
//    self.lblTanggapan.frame = frmLblTanggapan;
    _lblTanggapan.numberOfLines = 0;
    /*
     viewCheck.layer.shadowRadius  = 1.5f;
     viewCheck.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
     viewCheck.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
     viewCheck.layer.shadowOpacity = 0.9f;
     viewCheck.layer.masksToBounds = NO;

     UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
     UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(viewCheck.bounds, shadowInsets)];
     viewCheck.layer.shadowPath    = shadowPath.CGPath;
     **/
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}

-(CGFloat) heightForLabel:(UILabel*)label withWidth:(double)width{

    CGSize constraint = CGSizeMake(width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}


//var height = heightForView("This is just a load of text", font: font, width: 100.0)

- (void) setupContent{
    NSArray *listCek = [[data valueForKey:@"DATE_TIKET"] componentsSeparatedByString:@"-"];
    NSArray *listItems = [[listCek objectAtIndex:2] componentsSeparatedByString:@" "];
    NSArray *bulan = @[@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun", @"Jul", @"Aug", @"Sep", @"Oct", @"Nov", @"Dec"];
    if([lang isEqualToString:@"id"]){
        bulan = @[@"Jan", @"Feb", @"Mar", @"Apr", @"Mei", @"Jun", @"Jul", @"Agu", @"Sep", @"Okt", @"Nov", @"Des"];
    }
    
    //replaced
    self.lblDate.text = [listItems objectAtIndex:0];
    NSInteger bln = ([[listCek objectAtIndex:1] integerValue])-1;
    self.lblMonth.text = [bulan objectAtIndex: bln];
    self.lblDate.hidden = YES;
    self.lblMonth.hidden = YES;
    //replacement
    self.lblFullDate.text = [NSString stringWithFormat:@"%@ %@ %@",[listItems objectAtIndex:0], [bulan objectAtIndex: bln], [listCek objectAtIndex:0]];
    self.lblFullDate.textColor = UIColorFromRGB(const_color_primary);
    
//    self.lblEmail.text = [userDefault objectForKey:@"email"];
    self.lblEmail.text = [NSUserdefaultsAes getValueForKey:@"email"];
    self.lblNoRek.text = [data objectForKey:@"NO_REKENING"];
    self.lblAccName.text = [userDefault objectForKey:@"customer_name"];
    self.lblCategory.text = [data objectForKey:@"SUBJEK_KELUHAN"];
    self.lblNoRef.text = [data objectForKey:@"FT_TRANSAKSI"];
    self.lblReason.text = [data objectForKey:@"DESKRIPSI"];
    self.lblTanggapan.text = [data objectForKey:@"TANGGAPAN"];

}

- (void) setupLanguage{
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Detail Keluhan";
        self.lblTitleAccName.text = @"Nama Nasabah";
        self.lblTitleNoRef.text = @"No Referensi (FT)";
        self.lblTitleNoRek.text = @"No Rekening";
        self.lblTitleReason.text = @"Alasan Gagal Transaksi";
        self.lblTitleCategory.text = @"Kategori Permasalahan";
        self.lblTitleTanggapan.text = @"Tanggapan";
    }else{
        self.lblTitle.text = @"Complain Detail";
        self.lblTitleAccName.text = @"Customer Name";
        self.lblTitleNoRef.text = @"No Reference (FT)";
        self.lblTitleNoRek.text = @"No Account";
        self.lblTitleReason.text = @"Reason";
        self.lblTitleCategory.text = @"Category";
        self.lblTitleTanggapan.text = @"Tanggap";
    }
}
- (IBAction)actionOK:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
