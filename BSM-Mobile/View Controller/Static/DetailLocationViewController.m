//
//  DetailLocationViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 29/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "DetailLocationViewController.h"
#import "HomeViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Styles.h"

@interface DetailLocationViewController ()<MKMapViewDelegate>{
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation DetailLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dt = [userDefault objectForKey:@"passingData"];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [Styles setTopConstant:_topConstant];
    
    if([lang isEqualToString:@"id"]) {
        _lbl.text = @"Lokasi ATM";
    } else {
        _lbl.text = @"ATM Location";
    }
    

    self.lbl.textColor = UIColorFromRGB(const_color_title);
    self.lbl.textAlignment = const_textalignment_title;
    self.lbl.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lbl.superview.backgroundColor = UIColorFromRGB(const_color_topbar);


    /*UITabBarController *tabCont = self.tabBarController;
    UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
    HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
    [homeCont startIdleTimer];*/
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake([[dt valueForKey:@"latitude"]floatValue], [[dt valueForKey:@"longitude"]floatValue]);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 3000, 3000)];
    [self.mapView setRegion:adjustedRegion animated:YES];

    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(currentLocation.coordinate, 800, 800);
////    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[dt valueForKey:@"latitude"]floatValue], [[dt valueForKey:@"longitude"]floatValue]);
    
    point.coordinate = coordinate;
    point.title = [dt valueForKey:@"name"];
    point.subtitle = [dt valueForKey:@"address"];
    
    [self.mapView addAnnotation:point];
    [userDefault removeObjectForKey:@"passingData"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
//    MKCoordinateRegion region;
//    MKCoordinateSpan span;
//    span.latitudeDelta = 0.005;
//    span.longitudeDelta = 0.005;
//    CLLocationCoordinate2D location;
//    location.latitude = aUserLocation.coordinate.latitude;
//    location.longitude = aUserLocation.coordinate.longitude;
////    region.span = span;
//    // region.center = location;
//    [self.mapView setRegion:region animated:YES];
}


@end
