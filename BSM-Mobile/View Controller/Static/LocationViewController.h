//
//  LocationViewController.h
//  BSM-Mobile
//
//  Created by lds on 7/7/14.
//  Copyright (c) 2014 lds. All rights reserved.
//

#import "RootViewController.h"

@interface LocationViewController : RootViewController
-(void)currentLocationIdentifier;
@end
