//
//  PopUpLoginViewController.m
//  BSM-Mobile
//
//  Created by ARS on 13/12/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PopUpLoginViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "Connection.h"
#import "Biometric.h"
#import "NSString+MD5.h"
#import "Utility.h"
#import "InboxHelper.h"
#import "ScrollHomeViewController.h"
#import "CustomBtn.h"
#import "SKeychain.h"
#import <QuartzCore/QuartzCore.h>
#import <LocalAuthentication/LocalAuthentication.h>

@interface PopUpLoginViewController ()<UIAlertViewDelegate, ConnectionDelegate>{
    Biometric *bm;
    int ctrlog;
    NSString *flagTPLVC;
    NSUserDefaults *userDefaults;
    
    NSString *firstLogin;
    NSString *language;
    NSString *message;
    
    NSString *messageHeaderFrgtPssword;
    NSString *messageContentFrgtPssword;
    
    NSString *localizedReasonString;
    
    InboxHelper *inboxHelper;
    NSString *mFeat;
    BOOL loginStatus;
}

@property (weak, nonatomic) IBOutlet UIScrollView *vsScrollRoot;
@property (weak, nonatomic) IBOutlet UIView *vwRoot;
@property (weak, nonatomic) IBOutlet UIImageView *mandiriSyariaLogo;
@property (weak, nonatomic) IBOutlet UILabel *teksWelcome;
@property (weak, nonatomic) IBOutlet UIButton *icEye;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
//@property (weak, nonatomic) IBOutlet CustomBtn *btnFaceID;
@property (weak, nonatomic) IBOutlet CustomBtn *btnLogin;
@property (weak, nonatomic) IBOutlet UIView *uiContainer;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UILabel *lblLupa;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *viewFaceID;
@property (weak, nonatomic) IBOutlet UILabel *labelFaceID;
@property (weak, nonatomic) IBOutlet UIView *btnFaceID;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewFaceID;


@end

@implementation PopUpLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"LOGIN" forKey:@"VC"];
    [userDefaults synchronize];
    
    firstLogin = [userDefaults objectForKey:@"firstLogin"];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    inboxHelper = [[InboxHelper alloc]init];
    
    ctrlog = 0;
    self.parentViewController.tabBarController.tabBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
    
    flagTPLVC = @"HIDE";
    bm = [Biometric objBiometric];
    
    [self setupLanguage];
    [self setupLayout];
    
    UITapGestureRecognizer *tapViewLogin =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(dismissPopUp:)];
    [self.vwRoot addGestureRecognizer:tapViewLogin];

    UITapGestureRecognizer *tapForgot =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(forgotPassword)];
    
    [self.lblLupa addGestureRecognizer:tapForgot];
    [self.lblLupa setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapFaceID =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(btnFaceTouchLogin:)];
    [self.btnFaceID addGestureRecognizer:tapFaceID];
    [self.btnFaceID setUserInteractionEnabled:YES];
    
    [self.btnFaceID.layer setCornerRadius:26];
    
    [self registerForKeyboardNotifications];
    [Styles addShadow:self.view];
//    [Styles addBlur:self.view];
}

- (void) setMFeat : (NSString *) strFeat{
    mFeat = strFeat;
}

-(void)dismissPopUp:(UITapGestureRecognizer *)recognizer{
    [self dismissViewControllerAnimated:YES completion:^{

        NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    
    bm = [Biometric objBiometric];
    if(bm.isFaceIDAvailable == NO)
    {
        if(bm.isTouchIDAvailable == YES)
        {
            [self showBiometricLogin];
        }
    }
    else
    {
        if([firstLogin isEqualToString:@"YES"])
        {
            [self showBiometricLogin];
        }
    }
    
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated{
    if([self->identifier isEqualToString:@"qriscpm"]){
        [self->delegate loginDisappear:loginStatus];
    }
}

-(void)showBiometricLogin{
    LAContext *context = [[LAContext alloc]init];
    NSError *authError = nil;
    
    if([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]){
        [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics localizedReason:localizedReasonString reply:^(BOOL success, NSError *error) {
            //Testing Frida Injection Controlling IPA
            //Pengecekan Error tidak mungkin terdekteksi karena yang di inject adalah hasil success dari api LocalAuthentication nya
            
            if(!error){
                if(success){
                    NSLog(@"Success Login with Touch / Face ID");
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self doLogin];
                    });
                } else {
                    NSLog(@"Failed Login with Touch / Face ID");
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self biometricErroMsg:error];
                    });
                }
            }else{
                [self biometricErroMsg:error];
            }
        }];
    }
}

- (void) doLogin{
//    if([SKeychain loadObjectForKey:@"password"]){
    if([NSUserdefaultsAes getValueForKey:@"password"]){
        [userDefaults setObject:@"YES" forKey:@"hasLogin"];
        [userDefaults setObject:@"NO" forKey:@"firstLogin"];
        [userDefaults setValue:@"NO" forKey:@"isExist"];
        [userDefaults synchronize];
        loginStatus = YES;
        [self callResetTimer];
        
        if(identifier){
            [self dismissViewControllerAnimated:YES completion:^{
                [self->delegate loginDoneState:self->identifier];
            }];
        }else if(menuID){
            [self dismissViewControllerAnimated:YES completion:^{
                [self->delegate loginDoneState:self->menuID];
            }];
        }else{
            [self dismissWithBroadcast];
        }
    }else{
        NSString *msg = @"";
        if([language isEqualToString:@""]){
            msg = @"Authentikasi Gagal";
        }else{
            msg = @"Authentication Failed";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void) dismissWithBroadcast{
    [self dismissViewControllerAnimated:YES completion:^{
        
        if (self->mFeat == nil){
            self->mFeat = @"";
        }
        
        NSDictionary *userInfo = @{@"position": @(1112)};
        NSDictionary *loginInfo = @{@"logged_in": @"YES",
                                    @"feat_type" : self->mFeat
        };
        NSDictionary *favoriteInfo = @{@"logged_in": @"YES"};
        
        NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
        [notifCenter postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        if ([self->mFeat  isEqual: @"favorite"])
        {
            [notifCenter postNotificationName:@"favoriteDetail" object:self userInfo:favoriteInfo];

        }
        else if ([self->mFeat  isEqual: @"complainHandler"])
        {
            [notifCenter postNotificationName:@"complainHandler" object:self userInfo:favoriteInfo];

        }
        else if ([self->mFeat  isEqual: @"chatAisyah"])
        {
            [notifCenter postNotificationName:@"chatAisyah" object:self userInfo:favoriteInfo];

        }
        else if ([self->mFeat  isEqual: @"helpDesk"])
        {
//            [notifCenter postNotificationName:@"popLogin" object:self userInfo:loginInfo];
            [notifCenter postNotificationName:@"helpDesk" object:self userInfo:favoriteInfo];

        }
        else if([self->mFeat isEqual:@"gold"]){
            NSDictionary *userI = @{@"position": @(1127)};
            [notifCenter postNotificationName:@"listenerNavigate" object:self userInfo:userI];
        }
        else if ([self->mFeat  isEqual: @"limitTrx"])
        {
//            [notifCenter postNotificationName:@"popLogin" object:self userInfo:loginInfo];
            [notifCenter postNotificationName:@"limitTrx" object:self userInfo:favoriteInfo];

        }
        else {
            [notifCenter postNotificationName:@"popLogin" object:self userInfo:loginInfo];
//            [notifCenter postNotificationName:@"favoriteDetail" object:self userInfo:favoriteInfo];
        }
    }];
    
}

-(void)biometricErroMsg:(NSError*)error{
    NSString *errMsg = @"";
    bm = [Biometric objBiometric];
    NSString *langLG = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([langLG isEqualToString:@"id"]){
        switch([error code]) {
            case kLAErrorAuthenticationFailed:
                if (bm.isTouchIDAvailable == YES) {
                    errMsg = @"Gagal otentikasi. Touch ID tidak cocok";
                } else if (bm.isFaceIDAvailable == YES) {
                    errMsg = @"Gagal otentikasi. Face ID tidak cocok";
                }
                break;
            case kLAErrorUserCancel:
                //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                break;
            case kLAErrorUserFallback:
                //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                break;
            case kLAErrorTouchIDNotEnrolled:
                errMsg = @"Touch ID belum terdaftar";
                break;
            case kLAErrorBiometryNotAvailable:
                if (bm.isTouchIDAvailable == YES) {
                    errMsg = @"Device ini tidak mendukung Touch ID";
                } else if (bm.isFaceIDAvailable == YES) {
                    errMsg = @"Device ini tidak mendukung Face ID";
                }
                break;
            default:
                break;
        }
    } else {
        switch([error code]) {
            case kLAErrorAuthenticationFailed:
                if (bm.isTouchIDAvailable == YES) {
                    errMsg = @"Authentication failed\nTouch ID doesn't match.";
                } else if (bm.isFaceIDAvailable == YES) {
                    errMsg = @"Authentication failed\nFace ID doesn't match.";
                }
                break;
            case kLAErrorUserCancel:
                //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                break;
            case kLAErrorUserFallback:
                //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                break;
            case kLAErrorTouchIDNotEnrolled:
                errMsg = @"Touch ID not recognized.";
                break;
            case kLAErrorBiometryNotAvailable:
                if (bm.isTouchIDAvailable == YES) {
                    errMsg = @"This device doesn't support Touch ID.";
                } else if (bm.isFaceIDAvailable == YES) {
                    errMsg = @"This device doesn't support Face ID";
                }
                break;
            default:
                break;
        }
    }
    
    if (![errMsg isEqualToString:@""]) {
        // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:errMsg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }

}

-(void) openTappedNotification{
    NSString *receiving = [userDefaults valueForKey:@"RECEIVING"];
    
    if(receiving != nil){
        if([receiving isEqualToString:@"1"] ||
           [receiving isEqualToString:@"Instant Payment"]){
            
            NSLog(@"Instant Payment");
        } else if([receiving isEqualToString:@"2"] ||
                  [receiving isEqualToString:@"Website"]){
            
            NSLog(@"Website");
        } else {
            self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
        }
    }
}

-(void)errorLoadData:(NSError *)error{
    NSLog(@"%@", [error localizedDescription]);
}

-(void)setupLanguage{
    self.lblLupa.textColor = UIColorFromRGB(const_color_primary);
     
    if([language isEqualToString:@"id"]){
        
        self.lblLupa.text = @"Lupa Kata Sandi ?";
        self.teksWelcome.text = @"Untuk melanjutkan, silahkan masukan kata sandi anda";
        [self.btnLogin setTitle:@"Masuk" forState:UIControlStateNormal];
//        [self.btnFaceID setTitle:@"Masuk dengan Face/Touch ID" forState:UIControlStateNormal];
        [self.labelFaceID setText:@"Masuk dengan Face/Touch ID"];
        self.textFieldPassword.placeholder = @"Kata Sandi";
        self.lblTitle.text = @"Kata Sandi";

        messageHeaderFrgtPssword = @"Informasi";
        messageContentFrgtPssword = @"Mohon lakukan install ulang aplikasi untuk membuat kata sandi baru.";
        
        if(bm.isTouchIDAvailable == YES){
            localizedReasonString = @"Masuk dengan Touch ID";
            message = @"Touch ID Berhasil";
        } else if (bm.isFaceIDAvailable == YES){
            localizedReasonString = @"Masuk dengan Face ID";
            message = @"Face ID Berhasil";
        }
        
    }else{
        self.lblLupa.text = @"Forgot Password ?";
        self.teksWelcome.text = @"To continue, please enter your password";
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
//        [self.btnFaceID setTitle:@"Login with Face/Touch ID" forState:UIControlStateNormal];
        [self.labelFaceID setText:@"Login with Face/Touch ID"];
        
        self.textFieldPassword.placeholder = @"Password";
        self.lblTitle.text = @"Password";
        
        messageHeaderFrgtPssword = @"Information";
        messageContentFrgtPssword = @"Please do reinstall the app to create a new password.";
        
        if(bm.isTouchIDAvailable == YES){
            localizedReasonString = @"Use Touch ID to Continue";
            message = @"Touch ID Success";
        }else{
            localizedReasonString = @"Use Face ID to Continue";
            message = @"Face ID Success";
        }
    }
}

-(void)setupLayout{
    
    self.uiContainer.layer.shadowColor = [[UIColor grayColor] CGColor];
    self.uiContainer.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.uiContainer.layer.shadowOpacity = 2.0f;
    self.uiContainer.layer.shadowRadius = 7.0;
    self.uiContainer.layer.cornerRadius = 16.0f;
    self.uiContainer.layer.masksToBounds = YES;

    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    self.textFieldPassword.inputAccessoryView = keyboardDoneButtonView;
    [self.textFieldPassword setSecureTextEntry:true];
    
    
    if(bm.isFaceIDAvailable == NO){
        if(bm.isTouchIDAvailable == YES){
//            [self.btnFaceID setHidden:NO];
            [self.viewFaceID setHidden:NO];
            [self.labelFaceID setHidden:NO];
            [self.btnFaceID setHidden:NO];
            self.heightViewFaceID.constant = 127;
            
        } else {
//            [self.btnFaceID setHidden:YES];
            [self.viewFaceID setHidden:YES];
            [self.labelFaceID setHidden:YES];
            [self.btnFaceID setHidden:YES];
            self.heightViewFaceID.constant = 0;
            
        }
    } else {
//        [self.btnFaceID setHidden:NO];
        [self.viewFaceID setHidden:NO];
        [self.labelFaceID setHidden:NO];
        [self.btnFaceID setHidden:NO];
        self.heightViewFaceID.constant = 127;
        
    }
    
//    [self.btnFaceID setColorSet:SECONDARYCOLORSET];
    [self.btnFaceID setUserInteractionEnabled:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + 100, 0.0);
    self.vsScrollRoot.contentInset = contentInsets;
    self.vsScrollRoot.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{

    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vsScrollRoot.contentInset = contentInsets;
    self.vsScrollRoot.scrollIndicatorInsets = contentInsets;
}

- (void)reloadApp{
    
}

-(IBAction)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (IBAction)btnToggleShowPassword:(id)sender {
    if ([flagTPLVC isEqualToString:@"SHOW"]) {
        flagTPLVC = @"HIDE";
        [self.textFieldPassword setSecureTextEntry:true];
        [self.icEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPLVC isEqualToString:@"HIDE"]) {
        flagTPLVC = @"SHOW";
        [self.textFieldPassword setSecureTextEntry:false];
        [self.icEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (void)forgotPassword{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:messageHeaderFrgtPssword message:messageContentFrgtPssword preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)btnFaceTouchLogin:(id)sender {
    [self showBiometricLogin];
}
- (IBAction)btnLogin:(id)sender {
    NSString *passwrd = self.textFieldPassword.text;
//    NSString *upwd = [userDefaults objectForKey:@"password"];
    NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
//    id upwd = [SKeychain loadObjectForKey:@"password"];
    
    
    if([[passwrd MD5] isEqualToString:upwd]){
        [self doLogin];
    }else{
        NSString *errMsg = @"";
        NSString *alertTitle = @"";
        if([language isEqualToString:@"id"]){
            errMsg = @"Kata Sandi Salah";
            alertTitle = @"Informasi";
        }else{
            errMsg = @"Wrong Password";
            alertTitle = @"Information";
        }
        
        UIAlertController *alerts = [UIAlertController alertControllerWithTitle:alertTitle message:errMsg preferredStyle:UIAlertControllerStyleAlert];
        [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alerts animated:YES completion:nil];
        
    }
    [userDefaults setValue:@"NO" forKey:@"isExist"];
    [userDefaults synchronize];
}

-(void)callResetTimer{
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    userInfo = @{@"position": @(1112)};
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setIdentfier:(NSString *)newIdentifier{
    identifier = newIdentifier;
}

- (void)setMenuID:(NSString *)newMenuID{
    menuID = newMenuID;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if ([_textFieldPassword isFirstResponder]) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
        }];
    }
    return [super canPerformAction:action withSender:sender];
}

@end
