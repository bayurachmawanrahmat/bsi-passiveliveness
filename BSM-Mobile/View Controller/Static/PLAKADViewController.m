//
//  PLAKADViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 23/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PLAKADViewController.h"
#import "Utility.h"
#import "CustomBtn.h"

@interface PLAKADViewController ()<UIScrollViewDelegate>{
    NSString *akadString;
}
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;

@end

@implementation PLAKADViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    // Do any additional setup after loading the view.
    
    self.textAkad.attributedText = [Utility htmlAtributString:akadString];
    [self.viewFrame.layer setCornerRadius:10];
    self.viewFrame.layer.shadowColor = [[UIColor grayColor]CGColor];
    self.viewFrame.layer.shadowOffset = CGSizeMake(0, 5);
    self.viewFrame.layer.shadowOpacity = 0.5f;
    self.viewFrame.backgroundColor = [UIColor whiteColor];
    self.viewFrame.layer.borderColor = [UIColorFromRGB(const_color_lightgray)CGColor];
    self.viewFrame.layer.borderWidth = 0.3;
    
    [self.cancelButton.layer setCornerRadius:20];
    [self.agreeButton.layer setCornerRadius:20];
    
    [self.textAkad setUserInteractionEnabled:YES];
    [self.textAkad addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resettingTimer)]];
    
    [self.scrollingView setDelegate:self];
    
    if([Utility isLanguageID]){
        [self.agreeButton setTitle:@"Setuju" forState:UIControlStateNormal];
        [self.cancelButton setTitle:@"Batal" forState:UIControlStateNormal];
    }else{
        [self.agreeButton setTitle:@"Agree" forState:UIControlStateNormal];
        [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self resettingTimer];
}

- (void) resettingTimer{
    NSDictionary* userinfo = @{@"position": @(1112)};
    NSNotificationCenter* notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter postNotificationName:@"listenerNavigate" object:self userInfo:userinfo];
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (IBAction)actionAgree:(id)sender {
    [delegate clickedButtonAgreement:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)actionDisagree:(id)sender {
    [delegate clickedButtonAgreement:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) setAkadText : (NSString*)akadText{
    akadString = akadText;
}


@end
