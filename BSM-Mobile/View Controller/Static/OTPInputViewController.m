//
//  OTPInputViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 05/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "OTPInputViewController.h"
#import "Connection.h"
#import "Styles.h"

@interface OTPInputViewController ()<ConnectionDelegate, UITextFieldDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *state;
}
@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleOTP;
@property (weak, nonatomic) IBOutlet UITextField *tfInputOTP;
@property (weak, nonatomic) IBOutlet UIButton *buttonIdentify;
@property (weak, nonatomic) IBOutlet UILabel *resendLabel;

@end

@implementation OTPInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([language isEqualToString:@"id"]){
        self.labelTitleOTP.text = @"Kode OTP";
        self.resendLabel.text = @"Kirim Ulang Kode OTP";
        [self.buttonIdentify setTitle:@"Otentifikasi" forState:UIControlStateNormal];
    }else{
        self.labelTitleOTP.text = @"OTP Code";
        [self.buttonIdentify setTitle:@"Authentication" forState:UIControlStateNormal];
        self.resendLabel.text = @"Resend OTP Code";
    }
    
    self.viewFrame.layer.cornerRadius = 8;
    self.viewFrame.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    self.viewFrame.layer.borderWidth = 1;
    
    self.tfInputOTP.delegate = self;
    [Styles setStyleButton:self.buttonIdentify];

    [self requestOTP];
    [self.resendLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(requestOTP)]];
    [self.resendLabel setUserInteractionEnabled:YES];
    
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissOTP)]];
}


- (void)dismissOTP{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)requestOTP{
    state = @"send_otp";
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=cashfin_requested,language,step=send_otp" needLoading:true encrypted:true banking:true favorite:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(newText.length == 6){
        textField.text = newText;
        [[DataManager sharedManager].dataExtra setValue:newText forKey:@"otp"];
        state = @"verify_otp";
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:@"request_type=cashfin_requested,step=verify_otp,transaction_id,otp,language" needLoading:true encrypted:true banking:true favorite:nil];
    }
    
    return YES;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"cashfin_requested"]){
        if([state isEqualToString:@"send_otp"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else if ([state isEqualToString:@"verify_otp"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                [delegate verifiedOTP];
                [self dismissViewControllerAnimated:YES completion:nil];
            }else{
//                NSString *msg = @"";
//                if([language isEqualToString:@"id"]){
//                    msg = @"Nomor OTP yang anda masukan salah";
//                }else{
//                    msg = @"The OTP number you entered is incorrect";
//                }
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
}

- (void)reloadApp{
    
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

@end
