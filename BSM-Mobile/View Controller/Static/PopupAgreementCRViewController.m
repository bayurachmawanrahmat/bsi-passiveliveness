//
//  PopupAgreementCRViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PopupAgreementCRViewController.h"
#import "CustomBtn.h"
#import "Utility.h"

@interface PopupAgreementCRViewController ()<UIScrollViewDelegate>{
    NSString *data;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *backButton;

@end

@implementation PopupAgreementCRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    
    if([[[userdefault objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
        self.labelTitle.text = @"Persetujuan Pengajuan";
        [self.btnBack setTitle:@"Setuju" forState:UIControlStateNormal];
    }else{
        self.labelTitle.text = @"Application Approval";
        [self.btnBack setTitle:@"Agree" forState:UIControlStateNormal];
    }
    
    self.labelContent.text = [data stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    
    [self.viewFrame.layer setCornerRadius:16];
    [self.viewFrame.layer setBorderWidth:1];
    [self.viewFrame.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    [self.viewFrame.layer setMasksToBounds:YES];
    
    [self.btnBack addTarget:self action:@selector(actionAgree) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton setUserInteractionEnabled:YES];
    [self.backButton addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBack)]];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [Utility resetTimer];
}

- (void)setData:(NSString *)string{
    data = string;
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void) actionBack{
    [delegate completionAgreementCR:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) actionAgree{
    [delegate completionAgreementCR:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
