//
//  FavoriteDetailViewController.h
//  BSM-Mobile
//
//  Created by lds on 6/15/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import "TemplateViewController.h"

@interface FavoriteDetailViewController : TemplateViewController

- (void)setSelectedIndex:(int)index;

@end
