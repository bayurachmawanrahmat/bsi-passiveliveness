//
//  AboutViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 08/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface AboutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIImageView *headerImg;
@property (weak, nonatomic) IBOutlet UILabel *labelVersionTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelVersion;
@property (weak, nonatomic) IBOutlet UILabel *labelHakCipta;
@property (weak, nonatomic) IBOutlet UILabel *labelInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelCopyright;
@property (weak, nonatomic) IBOutlet CustomBtn *btnOk;

@end

NS_ASSUME_NONNULL_END
