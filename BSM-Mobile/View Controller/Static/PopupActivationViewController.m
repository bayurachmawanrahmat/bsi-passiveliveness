//
//  PopupActivationViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 11/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PopupActivationViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "TabViewController.h"
#import "TemplateViewController.h"

@interface PopupActivationViewController (){
    NSUserDefaults *userDefault;
    NSString *lang;
}

@property (weak, nonatomic) IBOutlet UIView *vwMainTac;
@property (weak, nonatomic) IBOutlet UIView *vwContentTac;

@property (weak, nonatomic) IBOutlet UIImageView *imgBaner;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleTag;
@property (weak, nonatomic) IBOutlet UILabel *lblDescTag;
@property (weak, nonatomic) IBOutlet CustomBtn *btnActivasi;
@property (weak, nonatomic) IBOutlet UIButton *btnForget;

@property (weak, nonatomic) IBOutlet UILabel *lblDescRegistration;
@property (weak, nonatomic) IBOutlet CustomBtn *btnRegistration;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstantLblTitleTag;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstantLblDesTag;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstantLblDesReg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heighConstantImgBanner;


- (IBAction)actionActivationListener:(id)sender;
- (IBAction)actionForgetListener:(id)sender;
- (IBAction)actionRegistration:(id)sender;



@end

@implementation PopupActivationViewController

- (void)viewWillAppear:(BOOL)animated{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *strTitle, *strDetail, *strBtnActivasi, *strBtnFogot, *strBtnOpenRek, *strBtnRegistration, *strDescRegistration;
    
    if ([lang isEqualToString:@"id"]) {
        strTitle = @"Anda belum melakukan\n aktivasi";
        strDetail= @"Gunakan kode aktivasi yang telah dikirimkan\n ke nomor Anda.";
        strBtnActivasi = @"AKTIVASI";
        strBtnFogot = @"Lupa kode aktivasi";
        strBtnOpenRek = @"Buka Rekening >";
        strBtnRegistration = @"REGISTRASI";
        strDescRegistration = @"Bila belum pernah melakukan registrasi BSI Mobile, silahkan klik Registrasi.";
    }else{
        strTitle = @"You haven't activated";
        strDetail= @"Use the sent activation code\n to your number.";
        strBtnActivasi = @"ACTIVATION";
        strBtnFogot = @"Forgot activation code";
        strBtnOpenRek = @"Open an account >";
        strBtnRegistration = @"REGISTRATION";
        strDescRegistration = @"If not yet Register at BSI Mobile, please click Registration.";
    }
    
//    [self.lblTitleTag sizeToFit];
    [self.lblTitleTag setText:strTitle];
    
//    [self.lblDescTag sizeToFit];
    [self.lblDescTag setText:strDetail];
    
//    [self.lblDescRegistration sizeToFit];
    [self.lblDescRegistration setText:strDescRegistration];
//    [self.lblDescRegistration setTextAlignment:NSTextAlignmentCenter];
    
    [self.btnActivasi setTitle:strBtnActivasi forState:UIControlStateNormal];
    [self.btnForget setTitle:strBtnFogot forState:UIControlStateNormal];
    
    [self.btnForget setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
//    [self.btnForget.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.btnRegistration setTitle:strBtnRegistration forState:UIControlStateNormal];
    
    
//    [self setupLayout];
    [self setStyles];
    
    [self.vwMainTac addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissPopup)]];
    
//    if(IPHONE_8 || IPHONE_8P){
//        self.topConstantLblDesReg.constant = 16;
//        self.topConstantLblDesTag.constant = 16;
//        self.topConstantLblTitleTag.constant = 16;
//        self.heighConstantImgBanner.constant = 200;
//    }else if ([Utility isDeviceHaveNotch]){
//        self.topConstantLblDesReg.constant = 32;
//        self.topConstantLblDesTag.constant = 32;
//        self.topConstantLblTitleTag.constant = 32;
//        self.heighConstantImgBanner.constant = 250;
//    }
}

-(void) dismissPopup{
    [self dismissViewControllerAnimated:true completion:nil];
}

//-(void) setupLayout{
//    CGRect frmVwMainTac = self.vwMainTac.frame;
//    CGRect frmVwContentTac = self.vwContentTac.frame;
//    CGRect frmImgBanner = self.imgBaner.frame;
//    CGRect frmLblTitleTag = self.lblTitleTag.frame;
//    CGRect frmLblDescTag = self.lblDescTag.frame;
//    CGRect frmBtnActivasi = self.btnActivasi.frame;
//    CGRect frmBtnForget = self.btnForget.frame;
//    CGRect frmLblDescReg = self.lblDescRegistration.frame;
//    CGRect frmBtnReg = self.btnRegistration.frame;
//
//
//    frmVwMainTac.origin.x = 0;
//    frmVwMainTac.origin.y = 0;
//    frmVwMainTac.size.width = SCREEN_WIDTH;
//    frmVwMainTac.size.height = SCREEN_HEIGHT;
//
//    if (IPHONE_5) {
//        frmVwContentTac.origin.x = 16;
//        frmVwContentTac.origin.y = TOP_NAV / 2;
//        frmVwContentTac.size.height = frmVwMainTac.size.height - (frmVwContentTac.origin.y + BOTTOM_NAV + 10);
//
//    }else{
//        frmVwContentTac.origin.x = 24;
//
//        if (IPHONE_8 || IPHONE_8P) {
//            frmVwContentTac.origin.y = TOP_NAV/2;
//             frmVwContentTac.size.height = frmVwMainTac.size.height - (frmVwContentTac.origin.y + BOTTOM_NAV + 20);
//        }else{
//            frmVwContentTac.origin.y = (TOP_NAV/2) + 40;
//            frmVwContentTac.size.height = frmVwMainTac.size.height - (frmVwContentTac.origin.y + BOTTOM_NAV + 40);
//        }
//    }
//    frmVwContentTac.size.width = frmVwMainTac.size.width - (frmVwContentTac.origin.x * 2);
//
//    frmImgBanner.origin.x = 0;
//    frmImgBanner.origin.y = 0;
//    frmImgBanner.size.width = frmVwContentTac.size.width;
//    frmImgBanner.size.height = 220;
//
//    frmLblTitleTag.origin.x = 0;
//    frmLblTitleTag.origin.y = frmImgBanner.origin.y + frmImgBanner.size.height + 16;
////    frmLblTitleTag.size.height =[Utility dynamicLabelHeight:self.lblTitleTag];
//    frmLblTitleTag.size.width = frmVwContentTac.size.width;
//
//    frmLblDescTag.origin.x = 16;
//    frmLblDescTag.origin.y = frmLblTitleTag.origin.y + frmLblTitleTag.size.height + 24;
////    frmLblDescTag.size.height = [Utility dynamicLabelHeight:self.lblDescTag];
//    frmLblDescTag.size.width = frmVwContentTac.size.width - (frmLblDescTag.origin.x *2);
//
//    frmBtnActivasi.origin.x = 16;
//    frmBtnActivasi.origin.y = frmLblDescTag.origin.y + frmLblDescTag.size.height + 32;
//    frmBtnActivasi.size.width = frmVwContentTac.size.width - (frmBtnActivasi.origin.x *2);
//    frmBtnActivasi.size.height = 45;
//
//    frmBtnForget.origin.x = frmBtnActivasi.origin.x + 4;
//    frmBtnForget.size.width = frmVwContentTac.size.width - (frmBtnForget.origin.x * 2);
//    frmBtnForget.origin.y = frmBtnActivasi.origin.y + frmBtnActivasi.size.height;
//
//    frmLblDescReg.origin.x = 16;
//    frmLblDescReg.origin.y = frmBtnForget.origin.y + frmBtnForget.size.height + 16;
//    frmLblDescReg.size.width = frmVwContentTac.size.width - (frmBtnActivasi.origin.x *2);
//
//    frmBtnReg.origin.x = frmBtnActivasi.origin.x;
//    frmBtnReg.size.width = frmVwContentTac.size.width - (frmBtnForget.origin.x * 2);
//    frmBtnReg.origin.y = frmLblDescReg.origin.y + frmLblDescReg.size.height + 8;
//    frmBtnReg.size.height = 45;
//
//    if (IPHONE_5) {
//        frmImgBanner.size.height = 180;
//        frmLblTitleTag.origin.y = frmImgBanner.origin.y + frmImgBanner.size.height + 16;
//        frmLblDescTag.origin.y = frmLblTitleTag.origin.y + frmLblTitleTag.size.height + 16;
//        frmBtnActivasi.origin.y = frmLblDescTag.origin.y + frmLblDescTag.size.height + 24;
//        frmBtnForget.origin.y = frmBtnActivasi.origin.y + frmBtnActivasi.size.height;
//        frmLblDescReg.origin.y = frmBtnForget.origin.y + frmBtnForget.size.height + 16;
//        frmBtnReg.origin.y = frmLblDescReg.origin.y + frmLblDescReg.size.height + 4;
//
//    }else if([Utility isDeviceHaveNotch]){
//        frmImgBanner.size.height = 250;
//        frmLblTitleTag.origin.y = frmImgBanner.origin.y + frmImgBanner.size.height + 24;
//        frmLblDescTag.origin.y = frmLblTitleTag.origin.y + frmLblTitleTag.size.height + 24;
//        frmBtnActivasi.origin.y = frmLblDescTag.origin.y + frmLblDescTag.size.height + 32;
//        frmBtnForget.origin.y = frmBtnActivasi.origin.y + frmBtnActivasi.size.height;
//        frmLblDescReg.origin.y = frmBtnForget.origin.y + frmBtnForget.size.height + 32;
//        frmBtnReg.origin.y = frmLblDescReg.origin.y + frmLblDescReg.size.height + 8;
//
//    }
//
//
//    self.vwMainTac.frame = frmVwMainTac;
//    self.vwContentTac.frame = frmVwContentTac;
//    self.imgBaner.frame = frmImgBanner;
//    self.lblTitleTag.frame = frmLblTitleTag;
//    self.lblDescTag.frame =  frmLblDescTag;
//    self.btnActivasi.frame = frmBtnActivasi;
//    self.btnForget.frame = frmBtnForget;
//    self.lblDescRegistration.frame = frmLblDescReg;
//    self.btnRegistration.frame = frmBtnReg;
//
//
//    [self.lblTitleTag setFont:[UIFont fontWithName:const_font_name3 size:16]];
//    [self.lblDescTag setFont:[UIFont fontWithName:const_font_name1 size:13]];
//    [self.btnActivasi.titleLabel setFont:[UIFont fontWithName:const_font_name3 size:16]];
//    [self.btnForget.titleLabel setFont:[UIFont fontWithName:const_font_name1 size:14]];
//    [self.lblDescRegistration setFont:[UIFont fontWithName:const_font_name1 size:13]];
//    [self.btnRegistration.titleLabel setFont:[UIFont fontWithName:const_font_name3 size:17]];
//
//    if (IPHONE_5) {
//        [self.lblTitleTag setFont:[UIFont fontWithName:const_font_name3 size:14]];
//        [self.lblDescTag setFont:[UIFont fontWithName:const_font_name1 size:12]];
//        [self.btnActivasi.titleLabel setFont:[UIFont fontWithName:const_font_name3 size:15]];
//        [self.btnForget.titleLabel setFont:[UIFont fontWithName:const_font_name1 size:13]];
//        [self.lblDescRegistration setFont:[UIFont fontWithName:const_font_name1 size:12]];
//        [self.btnRegistration.titleLabel setFont:[UIFont fontWithName:const_font_name3 size:15]];
//    }
//
//    self.btnActivasi.layer.borderColor = [[UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1]CGColor];
//    self.btnActivasi.layer.cornerRadius = 24.0f;
//    self.btnActivasi.layer.masksToBounds = YES;
//    self.btnActivasi.layer.borderWidth = 1.0f;
//
//    self.btnRegistration.layer.borderColor = [[UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1]CGColor];
//    self.btnRegistration.layer.cornerRadius = 24.0f;
//    self.btnRegistration.layer.masksToBounds = YES;
//    self.btnRegistration.layer.borderWidth = 1.0f;
//
//    self.btnForget.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//    self.btnForget.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
//
//
//    self.vwContentTac.layer.cornerRadius = 16.0f;
//    self.vwContentTac.clipsToBounds = true;
//
//    self.imgBaner.layer.cornerRadius = 16.0f;
//    UIBezierPath *maskPath = [UIBezierPath
//                            bezierPathWithRoundedRect:self.imgBaner.bounds
//                              byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
//                              cornerRadii:CGSizeMake(16, 16)
//                              ];
//
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//    maskLayer.path = maskPath.CGPath;
//    self.imgBaner.layer.mask = maskLayer;
//
//}

- (void) setStyles{
//    self.btnActivasi.layer.borderColor = [[UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1]CGColor];
//    self.btnActivasi.layer.cornerRadius = 20.0f;
//    self.btnActivasi.layer.masksToBounds = YES;
//    self.btnActivasi.layer.borderWidth = 1.0f;
//
//    self.btnRegistration.layer.borderColor = [[UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1]CGColor];
//    self.btnRegistration.layer.cornerRadius = 20.0f;
//    self.btnRegistration.layer.masksToBounds = YES;
//    self.btnRegistration.layer.borderWidth = 1.0f;
    
    [self.btnActivasi setColorSet:TERTIARYCOLORSET];
    [self.btnRegistration setColorSet:PRIMARYCOLORSET];
    
    self.vwContentTac.layer.cornerRadius = 16.0f;
    self.vwContentTac.clipsToBounds = true;
    self.imgBaner.layer.cornerRadius = 16.0f;
}

- (IBAction)actionActivationListener:(id)sender {
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    UIViewController *pop = [self topViewController];
    [self->userDefault setObject:@"NO" forKey:@"fromSliding"];
    UIViewController *viewCont = [ui instantiateViewControllerWithIdentifier:@"ActivationVC"];
    self.slidingViewController.topViewController = viewCont;
    [pop dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (IBAction)actionRegistration:(id)sender {
//    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Registrasi" bundle:nil];
//////
////    UIViewController *pop = [self topViewController];
////
//    TemplateViewController *templateView =  [ui instantiateViewControllerWithIdentifier:@"MOBREG"];
////    UIViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ActivationVC"];
////    self.slidingViewController.topViewController = viewCont;
//////    [pop dismissViewControllerAnimated:YES completion:nil];
//////
//    [pop dismissViewControllerAnimated:true completion:^{
//        TemplateViewController *templateView = [ui instantiateViewControllerWithIdentifier:@"MOBREG"];
//        [viewCont pushViewController:templateView animated:YES];
////        [viewCont presentViewController:templateView animated:YES completion:^{
////        }];
//
//        [self->delegate gotoTemplate:@"MOBREG"];
//    }];
    
    bool config_registrasi = [[userDefault objectForKey:@"config_registration"]boolValue];
    
    if(config_registrasi){
        [self dismissViewControllerAnimated:true completion:^{
            [self->delegate gotoTemplate:@"MOBREG"];
        }];
    }else{
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *pop = [self topViewController];
        TemplateViewController *templateView =  [ui instantiateViewControllerWithIdentifier:@"MSMREG"];
        self.slidingViewController.topViewController = templateView;
        [pop dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)actionForgetListener:(id)sender {
    // [self dismissViewControllerAnimated:YES completion:nil];
    
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *pop = [self topViewController];
    
    UIViewController *templateView =  [ui instantiateViewControllerWithIdentifier:@"ActivationRetrival"];
    
    self.slidingViewController.topViewController = templateView;
    [pop dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (IBAction)actionOpenRek:(id)sender {
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"OPNREKVC"];
    [self presentViewController:vc animated:YES completion:nil];
    
    //langsung ending onboarding
//    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
//    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiOnlineSelesaiScene"];
//    [self presentViewController:vc animated:YES completion:nil];
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    int i = 0;
    while (topController.presentedViewController) {
        if (i==0) {
            topController = topController.presentedViewController;
            break;
        }
        i++;
        
    }
    
    return topController;
}

@end
