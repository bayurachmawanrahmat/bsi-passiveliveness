//
//  CollectionViewCell.m
//  BSM-Mobile
//
//  Created by ARS on 15/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

@synthesize lblContent = _lblContent;


- (instancetype)initWithFrame:(CGRect)rec{
    self = [super initWithFrame:rec];
    if (self) {
        [self.contentView addSubview:self.lblContent];
    }
    return self;
}

-(UILabel *)lblContent
{
    if (_lblContent) return _lblContent;
    _lblContent = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    //[_lblContent setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContent.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    
//    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];

    return _lblContent;
}
@end
