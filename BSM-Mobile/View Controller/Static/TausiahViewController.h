//
//  TausiahViewController.h
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "RootViewController.h"
#import "TemplateViewController.h"

@interface TausiahViewController : TemplateViewController
-(void) setTitleMenu : (NSString *) strTitle;
@end
