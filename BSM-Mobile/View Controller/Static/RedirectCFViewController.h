//
//  RedirectCFViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 28/05/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

@interface RedirectCFViewController : TemplateViewController

- (void) redirectTo:(NSString *)redirecto andCode:(NSString*)code isMenuID:(BOOL)ismenuid;
- (void) setTextConfirmation:(NSString*)text;
- (void) setTitle:(NSString*)title;
- (void) setTitleButton:(NSString*)titleBtn;

@end
