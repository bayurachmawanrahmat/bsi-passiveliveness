//
//  SearchItemViewController.m
//  BSM-Mobile
//
//  Created by ARS on 11/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SearchItemViewController.h"
#import "Styles.h"
#import "CellLabel.h"
#import "Utility.h"

@interface SearchItemViewController ()<ConnectionDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>{
    NSUserDefaults *userDefaults;
    
    NSArray *displayList, *filteredList;
    BOOL isSearchActive;
    
    NSString *language;
    
    UIButton *buttonSearch;
    UIView *imgEmpty;
    
    CGRect mSize;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation SearchItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    imgEmpty = [[UIImageView alloc]init];
    
    self.lblTitle.hidden = YES;
    self.searchBar.hidden = YES;
    self.tableView.hidden = YES;
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    isSearchActive = NO;
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([language isEqualToString:@"id"]){
        [self.lblTitle setText:@"Pencarian Fitur"];
        [self.searchBar setPlaceholder:[userDefaults objectForKey:@"config_sfeature_captionid"]];
    }else{
        [self.lblTitle setText:@"Search Feature"];
        [self.searchBar setPlaceholder: [userDefaults objectForKey:@"config_sfeature_captionen"]];
    }
    
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    [self.searchBar setDelegate:self];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CellLabel" bundle:nil] forCellReuseIdentifier:@"CELLLABEL"];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    [toolbar setBarStyle:UIBarStyleDefault];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    
    if([userDefaults objectForKey:OBJ_SEARCH_FEATURE] == nil){
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:@"request_type=list_feature" needLoading:true encrypted:false banking:true favorite:nil];
    }else{
        displayList = [userDefaults objectForKey:OBJ_SEARCH_FEATURE];
        [self addImage];
        if(displayList.count < 1){
            [imgEmpty setHidden:NO];
        }
    }
    
    
    [self.searchBar setInputAccessoryView:toolbar];
    [self.searchBar becomeFirstResponder];
}

- (void) addImage{
    imgEmpty = [Utility showNoData:self.tableView];
    [self.tableView addSubview:imgEmpty];
    [imgEmpty setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self startAnimation];
}

- (void) doneEditButtonTapped{
    [self.view endEditing:YES];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if ([requestType isEqualToString:@"list_feature"]) {
        [self addImage];
        if ([[jsonObject valueForKey:@"responsecode"] isEqualToString:@"00"]) {
            NSArray *lista = (NSArray*)[jsonObject valueForKey:@"info"];
            [userDefaults setValue:lista forKey:OBJ_SEARCH_FEATURE];
            displayList = lista;
            if(isSearchActive){
                filteredList = displayList;
            }
            
            if(displayList.count < 1){
                [imgEmpty setHidden:NO];
            }
            
            [_searchBar resignFirstResponder];
            [self.tableView reloadData];
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}
- (void)errorLoadData:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)reloadApp {
    [BSMDelegate reloadApp];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    isSearchActive = YES;
    filteredList = displayList;
    [self.tableView reloadData];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    isSearchActive = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    isSearchActive = NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(isSearchActive){
        [self dismissViewControllerAnimated:YES completion:^{
            
            if([[self->filteredList[indexPath.row] objectForKey:@"launcher"] isEqualToString:@"-1"]){
                [self openFeature:[self->filteredList[indexPath.row] objectForKey:@"menuid"]];
            }else if([[self->filteredList[indexPath.row] objectForKey:@"launcher"] isEqualToString:@""]){
                [self openStore];
            }else{
                if([[self->filteredList[indexPath.row] objectForKey:@"servicecode"] isEqualToString:@""]){
                    [self openNodes:[self->filteredList[indexPath.row] objectForKey:@"launcher"]];
                }else{
                    NSString *nodes = [NSString stringWithFormat:@"%@/code=%@",[self->filteredList[indexPath.row] objectForKey:@"launcher"],[self->filteredList[indexPath.row] objectForKey:@"servicecode"]];
                    [self openNodes:nodes];
                }
            }
        }];
    }else{
        [self dismissViewControllerAnimated:YES completion:^{
            if([[self->displayList[indexPath.row] objectForKey:@"launcher"] isEqualToString:@"-1"]){
                [self openFeature:[self->displayList[indexPath.row] objectForKey:@"menuid"]];
            }else if([[self->filteredList[indexPath.row] objectForKey:@"launcher"] isEqualToString:@""]){
                [self openStore];
            }else{

                if([[self->displayList[indexPath.row] objectForKey:@"servicecode"] isEqualToString:@""]){
                    [self openNodes:[self->filteredList[indexPath.row] objectForKey:@"launcher"]];
                }else{
                    NSString *nodes = [NSString stringWithFormat:@"%@/=%@",[self->displayList[indexPath.row] objectForKey:@"launcher"],[self->displayList[indexPath.row] objectForKey:@"servicecode"]];
                    [self openNodes:nodes];
                }
            }
        }];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellLabel *cell = (CellLabel*)  [tableView dequeueReusableCellWithIdentifier:@"CELLLABEL"];
    
    if(isSearchActive){
        [cell.labelText setText:[filteredList[indexPath.row] objectForKey:@"feature"]];

    }else{
        [cell.labelText setText:[displayList[indexPath.row] objectForKey:@"feature"]];
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isSearchActive){
        return filteredList.count;
    }else{
        return displayList.count;
    }
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"feature", searchText];
    NSArray *filterlist = [displayList filteredArrayUsingPredicate:predicateString];
    
    filteredList = filterlist;
    if([searchText isEqualToString:@""]){
        filteredList = displayList;
    }
    [self.tableView reloadData];
}

- (void) openNodes : (NSString *)param{
    NSArray *mNode = [param componentsSeparatedByString:@"/"];
    NSDictionary* userInfo = @{@"position": @(1124),
                               @"node_index" : mNode};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void) openFeature : (NSString*) identifier{
    [delegate selectedFeature:[identifier intValue]];
}

- (void) openStore{
    [delegate openAppStore];
}

- (void) startAnimation{
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view insertSubview:backgroundView atIndex:0];
    
    UIView *topView = [[UIView alloc] init];
    [topView setFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width,84.0)];
    topView.backgroundColor = UIColor.whiteColor;
    
    self.contentView.frame = CGRectMake(0, TOP_NAV, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - TOP_NAV);
    
    buttonSearch = [[UIButton alloc]initWithFrame:mSize];

    [self.searchBar setFrame:mSize];
    self.searchBar.hidden = NO;
    [self.searchBar.layer setBorderColor:[UIColorFromRGB(const_color_primary)CGColor]];
    
    [self.contentView insertSubview:buttonSearch aboveSubview:topView];
    
    [UIView animateWithDuration: 1
    animations:^ {
        
        CGRect frmExit = self->buttonSearch.frame;
        CGRect newBounds = self.searchBar.frame;
        frmExit.origin.x = 8;
        
        newBounds.origin.x = 4 + (frmExit.origin.x + self.searchBar.frame.size.height);
        newBounds.size.width = SCREEN_WIDTH - (self->mSize.size.height+32); //newBounds.size.width -= 215; to contract
        self.searchBar.frame = newBounds;

        frmExit.size.height = self.searchBar.frame.size.height;
        frmExit.size.width = self.searchBar.frame.size.height;
        frmExit.origin.y = self.searchBar.frame.origin.y;
        [self->buttonSearch setFrame:frmExit];
        
        UIImage *imge = [UIImage imageNamed:@"ic_mtitle_back"];
        [self->buttonSearch setImage:imge forState:UIControlStateNormal];
        [self->buttonSearch setTintColor:[UIColor blackColor]];
        [self->buttonSearch.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [self->buttonSearch addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
        
        
        [self.searchBar layoutSubviews];
    } completion:^(BOOL done){
        if(done){
            self.tableView.hidden = NO;
        }
    }];
    
    [self.tableView setFrame:CGRectMake(16, self.searchBar.frame.origin.y + self.searchBar.frame.size.height, self.contentView.frame.size.width - self.searchBar.frame.origin.x, self.contentView.frame.size.height - (self.searchBar.frame.origin.y + self.searchBar.frame.size.height))];
}

- (void)setSize:(CGRect)size{
    mSize = size;
}

- (void)exit{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
