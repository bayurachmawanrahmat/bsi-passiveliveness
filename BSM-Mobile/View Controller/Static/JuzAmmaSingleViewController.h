//
//  JuzAmmaSingleViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JuzAmmaSingleViewController : TemplateViewController

-(void) sendData : (NSDictionary *) data;

@end

NS_ASSUME_NONNULL_END
