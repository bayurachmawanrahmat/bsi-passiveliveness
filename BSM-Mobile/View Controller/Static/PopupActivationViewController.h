//
//  PopupActivationViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 11/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "RootViewController.h"



@interface PopupActivationViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;

@end

@protocol PopupActivationDelegate

@required

- (void) gotoTemplate : (NSString*) string;

@end


