//
//  PopupZakatViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/18/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupZakatViewController : UIViewController{
    id delegate;
}

- (void) dataBundle : (NSDictionary *) dataBind;
- (void) setDelegate:(id)newDelegate;

@end

@protocol PopZakatDelegate

@required

- (void) dataZakat : (NSString*) nominal withAccount: (NSString*) account;

@end

NS_ASSUME_NONNULL_END
