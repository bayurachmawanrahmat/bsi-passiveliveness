//
//  QRPaymentViewController.m
//  BSM-Mobile
//
//  Created by Tegarizki on 22/01/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "QRPaymentViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIView+SimpleRipple.h"
#import <UIKit/UIKit.h>
#import "Utility.h"
#import "PopUpLoginViewController.h"

@interface QRPaymentViewController() <AVCaptureMetadataOutputObjectsDelegate, UIAlertViewDelegate, CAAnimationDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate, UIActionSheetDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    
    AVCaptureSession *session;
    AVCaptureDevice *device;
    AVCaptureDeviceInput *input;
    AVCaptureMetadataOutput *output;
    AVCaptureVideoPreviewLayer *previewLayer;
    BOOL stateFlash;
    
    UIImagePickerController *imagePicker;
    
    NSTimer *idleTimer;
    int maxIdle;
    
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwScan;
@property (weak, nonatomic) IBOutlet UILabel *lblTittle;

@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet UIView *vwBtnFlash;
@property (weak, nonatomic) IBOutlet UIView *vwBtnPhotos;

@property (weak, nonatomic) IBOutlet UILabel *lblFlash;
@property (weak, nonatomic) IBOutlet UIImageView *imgFlash;
@property (weak, nonatomic) IBOutlet UIButton *btnFlash;

@property (weak, nonatomic) IBOutlet UILabel *lblOpnPhotos;
@property (weak, nonatomic) IBOutlet UIImageView *imgOpnPhotos;
@property (weak, nonatomic) IBOutlet UIButton *btnOpnPhotos;

@property (weak, nonatomic) IBOutlet UIView *vwQrisLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblQrisLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imgQrisLogo;

@property (weak, nonatomic) IBOutlet UIView *vwQrisOption;
@property (weak, nonatomic) IBOutlet UIView *ctQrisOption;
@property (weak, nonatomic) IBOutlet UIView *fvRbQrisMpm;
@property (weak, nonatomic) IBOutlet UIView *fvRbQrisCpm;

@end

int *tagIdQrisMpm = 10098;
int *tagIdQrisCpm = 10099;

@implementation QRPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
        self.lblFlash.text = @"Senter";
        self.lblOpnPhotos.text = @"Gunakan Photo";
        self.lblQrisLogo.text = @"MENDUKUNG";
    }else{
        self.lblFlash.text = @"Flashlight";
        self.lblOpnPhotos.text = @"Use Photo";
        self.lblQrisLogo.text = @"SUPPORTED";
    }
    self.lblQrisLogo.textAlignment = NSTextAlignmentCenter;
    [self.lblFlash sizeToFit];
    self.lblFlash.numberOfLines = 2;
    
    [self.lblOpnPhotos sizeToFit];
    self.lblOpnPhotos.numberOfLines = 2;
    
    self.lblTittle.text = [self.jsonData valueForKey:@"title"];
    self.lblTittle.textAlignment = const_textalignment_title;
    self.lblTittle.textColor = UIColorFromRGB(const_color_title);
    self.lblTittle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];

    [self setupLayout];
    
    session = [[AVCaptureSession alloc] init];
    device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (input) {
        [session addInput:input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    if (error == nil) {
        output = [[AVCaptureMetadataOutput alloc] init];
        [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        
        [session addOutput:output];
        
        output.metadataObjectTypes = [output availableMetadataObjectTypes];
        
        previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        previewLayer.frame = self.vwScan.frame;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.bounds = self.vwScan.bounds;
        
        
        [self.vwScan.layer addSublayer:previewLayer];
        
        [session startRunning];
        
        CGRect rectBorderArea = [previewLayer metadataOutputRectOfInterestForRect:[self converRectOfInterest:self.vwScan.bounds]];
        output.rectOfInterest = rectBorderArea;
    }else{
        NSString *strMsg, *strBtn;
        if ([lang isEqualToString:@"id"]) {
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"Sepertinya pengaturan privasi Anda mencegah kami mengakses kamera Anda untuk melakukan pemindaian barcode. Anda dapat memperbaikinya dengan melakukan hal berikut: \n\nSentuh tombol Buka di bawah untuk membuka aplikasi Pengaturan. \n\nHidupkan Kamera. \n\nBuka aplikasi ini dan coba lagi."];
            strBtn = @"Buka";
        }else{
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following: \n\nTouch the Go button below to open the Settings app.\n\nTurn the Camera on.\n\nOpen this app and try again."];
            strBtn = @"Go";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:strBtn otherButtonTitles:nil, nil];
        alert.tag = 1;
        [alert show];
    }
    
    // Added by Angger : Start QRIS CPM
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _ctQrisOption.bounds  byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii: (CGSize){20.0,20.0,20.0,20.0}].CGPath;
    [_ctQrisOption.layer setMask:maskLayer];
    [_ctQrisOption.layer setCornerRadius:20.0f];
    [_ctQrisOption.layer setBorderWidth:1.0f];
    [_ctQrisOption.layer setBorderColor:[UIColor colorWithRed:248/255.0 green:173/255.0 blue:59/255.0 alpha:1].CGColor];
    [self.fvRbQrisMpm addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionQrisOption:)]];
    [self.fvRbQrisCpm addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionQrisOption:)]];
    // End QRIS CPM
    
    stateFlash = false;
    [self.btnFlash addTarget:self action:@selector(actionSelectedTochDown:event:) forControlEvents:UIControlEventTouchDown];
    [self.btnFlash addTarget:self action:@selector(actionFlashOnOff:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnOpnPhotos addTarget:self action:@selector(actionBrowsePhotos:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnOpnPhotos addTarget:self action:@selector(actionSelectedTochDown:event:) forControlEvents:UIControlEventTouchDown];
    
    [self makeFrameScanner];
    
    [self goToQRPayment];
    
}

- (void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    bool stateAcepted = [Utility vcNotifConnection: @"TABQRIS"];
    if (stateAcepted) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 404;
        [alert show];
    }
    
    if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
        [self startIdleTimer];
    } else {
        [self refreshIdleTimer];
    }
    
    if (session != nil){
        [session startRunning];
        [self goToQRPayment];
    }
    [self setButtonQrisStyle:tagIdQrisMpm];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self stopIdleTimer];
    [session stopRunning];
}

//- (void)viewDidAppear:(BOOL)animated{
//    NSString *isExist = [userDefault objectForKey:@"isExist"];
//    if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
//        [self startIdleTimer];
//    } else {
//        [self refreshIdleTimer];
//    }
//}

- (void)actionQrisOption:(UITapGestureRecognizer *)tapGesture{
    [self setButtonQrisStyle:tapGesture.view.tag];
    if(tapGesture.view.tag == tagIdQrisCpm) {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"mustLogin"] isEqualToString:@"YES"] &&
           [[[NSUserDefaults standardUserDefaults] valueForKey:@"hasLogin"] isEqualToString:@"NO"]){
            PopUpLoginViewController *popLogin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupLogin"];
            [popLogin setDelegate:self];
            [popLogin setIdentfier:@"qriscpm"];
            [self presentViewController:popLogin animated:YES completion:nil];
        }else{
            [self gotoQrisCpm];
        }
    }
}

- (void)setButtonQrisStyle:(int)tag{
    UIColor *clrSelected = [UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1];
    UIColor *clrUnselected = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1];
    if(tag == tagIdQrisMpm){
        [_fvRbQrisMpm.layer setCornerRadius:20.0f];
        [_fvRbQrisMpm setBackgroundColor:clrSelected];
        [_fvRbQrisCpm setBackgroundColor:clrUnselected];
    } else if(tag == tagIdQrisCpm) {
        [_fvRbQrisCpm.layer setCornerRadius:20.0f];
        [_fvRbQrisMpm setBackgroundColor:clrUnselected];
        [_fvRbQrisCpm setBackgroundColor:clrSelected];
    }
}

-(void)gotoQrisCpm {
//    [userDefault setObject:@"NO" forKey:@"mustLogin"];
//    [userDefault setObject:@"YES" forKey:@"hasLogin"];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"openfromtab":@"yes"}];
    NSString *menuIdQrisCpm = @"00198"; // 00198 code for QRIS CPM
    NSArray *selectedMenu = [dataManager getJsonDataWithMenuID:menuIdQrisCpm andList:dataManager.listMenu];
    [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
//    [super openNextTemplate];
    [self openTemplateNextStep];
}

#pragma mark featIsTimer timer selector
- (void) featIsTimer:(NSNotification *) notification {
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    if ([notification.name isEqualToString:@"featIsTimer"]){
        NSDictionary* userInfo = notification.userInfo;
        NSString *actionTimer = [userInfo valueForKey:@"actionTimer"];
        if ([actionTimer isEqualToString:@"START_TIMER"]) {
            if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
                [self startIdleTimer];
            } else {
                [self refreshIdleTimer];
            }
        }else if ([actionTimer isEqualToString:@"STOP_TIMER"]){
            if ([isExist isEqualToString:@"YES"]) {
                [self stopIdleTimer];
            }
        }else if([actionTimer isEqualToString:@"TIME_OUT"]){
            if ([isExist isEqualToString:@"YES"]) {
                [self stopIdleTimer];
                [self showPopupLogout];
            }
        }
        else{
            [self refreshIdleTimer];
        }
    }
}

-(void)startIdleTimer {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    
    if ((cid == nil) || ([cid isEqualToString:@""])) {
        NSLog(@"Don't start auto logout timer");
    } else {
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"YES"]) {
                if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
                    [self resetCountDown];
                    
                    [idleTimer invalidate];
                    idleTimer = nil;
                    idleTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                 target:self
                                                               selector:@selector(countDown)
                                                               userInfo:nil
                                                                repeats:YES];
                    
                    [[NSRunLoop mainRunLoop] addTimer:idleTimer forMode:NSRunLoopCommonModes];
                    [userDefault setValue:@"YES" forKey:@"isExist"];
                    [userDefault synchronize];
                }
            }
        }
    }
}

-(void)stopIdleTimer {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"NO" forKey:@"isExist"];
    [userDefault synchronize];
    [idleTimer invalidate];
    idleTimer = nil;
    [self resetCountDown];
}

-(void)refreshIdleTimer {
    [self resetCountDown];
}

-(void)countDown {
    NSLog(@"%@", [NSString stringWithFormat:@"Countdown %d",maxIdle]);
    if (maxIdle == 0) {
        [idleTimer invalidate];
        idleTimer = nil;
        [self showPopupLogout];
        
    } else {
        maxIdle = maxIdle - 1;
    }
}

-(void) timerReset{
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
        [self startIdleTimer];
    } else {
        [self refreshIdleTimer];
    }
}

-(void) showPopupLogout{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *alertTitle = @"";
    NSString *alertMessage = @"";
    if ([lang isEqualToString:@"en"]) {
        alertTitle = @"Session has ended";
        alertMessage = @"Your session has ended. Please log in to continue using BSI Mobile";
    } else {
        alertTitle = @"Sesi Berakhir";
        alertMessage = @"Sesi Anda telah berakhir. Silahkan login ulang untuk melanjutkan penggunaan BSI Mobile";
    }
    
    [self disposePenyesuaianPopupFiltering];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 505;
    [alert show];
}

-(void)resetCountDown{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSInteger nMaxIdle = [[userDefault objectForKey:@"config_timeout_session"]integerValue] / 1000;
    if (nMaxIdle == 0) {
        maxIdle = MAX_IDLE;
    }else{
        maxIdle = (int)nMaxIdle;
    }
}

-(void) disposePenyesuaianPopupFiltering{
    @try {
        UINavigationController *nav = (UINavigationController*)self.presentingViewController;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            [nav popToRootViewControllerAnimated:YES];
        }];
        
        //        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"JuzAmmaFilterVC"];
        //        [viewCont dismissViewControllerAnimated:YES completion:nil];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}



- (void) goToQRPayment {
        [[DataManager sharedManager]resetObjectData];
        NSArray *tempMain = [[DataManager sharedManager] getJSONData:3];
        if(tempMain != nil){
            NSDictionary *objectMain = [tempMain objectAtIndex:1];
            int ind = 2;
            NSArray *temp = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
            NSDictionary *object = [temp objectAtIndex:1];
            DataManager *dataManager = [DataManager sharedManager];
            [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
//            dataManager.currentPosition = 0;
            dataManager.currentPosition++;
            NSDictionary *tempData = [dataManager getObjectData];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"ic_qrpay_header.png" forKey:@"imgIcon"];
            self.lblTittle.text = [tempData valueForKey:@"title"];
            self.lblTittle.textAlignment = const_textalignment_title;
            self.lblTittle.textColor = UIColorFromRGB(const_color_title);

            if([[tempData valueForKey:@"activation"]boolValue]){
//                if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
                if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                    NSString *alertTitle = @"";
                    NSString *alertMessage = @"";
                    if ([lang isEqualToString:@"en"]) {
                        alertTitle = @"Information";
                        alertMessage = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
                    } else {
                        alertTitle = @"Informasi";
                        alertMessage = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
                    }
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
}


-(void) makeFrameScanner{
    
    UILabel *lblTitle = [Utility lblTitleQRIS];
    
    CGRect bounds = self.vwScan.layer.bounds;
    CGRect frmLblTitle = lblTitle.frame;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 32;
    frmLblTitle.size.width = bounds.size.width - (frmLblTitle.origin.x * 2);
    
    lblTitle.frame = frmLblTitle;
    
//    [self.vwScan.layer addSublayer:[self frameScanner:bounds]];
    [self.vwScan addSubview:[self borderScanner:bounds]];
    [self.vwScan addSubview:lblTitle];
    [self.vwScan bringSubviewToFront:self.vwButton];
    [self.vwScan bringSubviewToFront:self.vwQrisOption];
    
}

-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTittle.frame;
    CGRect frmVwScan = self.vwScan.frame;
    CGRect frmVwButton = self.vwButton.frame;
    
    CGRect frmVwBtnFlash = self.vwBtnFlash.frame;
    CGRect frmVwBtnPhotos = self.vwBtnPhotos.frame;
    
    CGRect frmLblFlash = self.lblFlash.frame;
    CGRect frmImgFlash = self.imgFlash.frame;
    CGRect frmBtnFlash = self.btnFlash.frame;
    
    CGRect frmLblOpnPhotos= self.lblOpnPhotos.frame;
    CGRect frmImgOpnPhotos = self.imgOpnPhotos.frame;
    CGRect frmBtnOpnPhotos = self.btnOpnPhotos.frame;
    
    CGRect frmVwQRISLogo = self.vwQrisLogo.frame;
    CGRect frmLblQRISLogo = self.lblQrisLogo.frame;
    CGRect frmImgQRISLogo = self.imgQrisLogo.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width =SCREEN_WIDTH;
    
    frmVwScan.origin.x = frmVwTitle.origin.x;
    frmVwScan.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmVwScan.size.width = SCREEN_WIDTH;
    frmVwScan.size.height = SCREEN_HEIGHT - frmVwScan.origin.y - BOTTOM_NAV_DEF + 19;
    if ([Utility isDeviceHaveNotch]) {
        frmVwScan.size.height = SCREEN_HEIGHT - frmVwScan.origin.y - BOTTOM_NAV_NOTCH + 11;
    }
    
    frmVwButton.origin.x = 0;
    frmVwButton.size.width = frmVwScan.size.width;
    
    frmVwBtnFlash.origin.x = 8;
    frmVwBtnFlash.origin.y = 0;
    
    frmLblFlash.origin.x = 3;
    frmLblFlash.origin.y = frmImgFlash.origin.y + frmImgFlash.size.height + 5;
    
    frmVwBtnFlash.size.height = frmLblFlash.origin.y +frmLblFlash.size.height + 5;
    frmVwBtnFlash.size.width = frmLblFlash.origin.x + frmLblFlash.size.width + 5;
    
    frmImgFlash.origin.x = frmVwBtnFlash.size.width/2 - frmImgFlash.size.width/2;
    
    frmBtnFlash.origin.x = 0;
    frmBtnFlash.origin.y = 0;
    frmBtnFlash.size.width = frmVwBtnFlash.size.width;
    frmBtnFlash.size.height = frmVwBtnFlash.size.height;
    
    frmVwBtnPhotos.origin.x = frmVwBtnFlash.origin.x + frmVwBtnFlash.size.width + 8;
    frmVwBtnPhotos.origin.y = 0;
    
    frmLblOpnPhotos.origin.x = 3;
    frmLblOpnPhotos.origin.y = frmImgOpnPhotos.origin.y + frmImgOpnPhotos.size.height + 5;
    
    frmVwBtnPhotos.size.height = frmVwBtnFlash.size.height;
    frmVwBtnPhotos.size.width = frmLblOpnPhotos.origin.x + frmLblOpnPhotos.size.width + 5;
    
    frmImgOpnPhotos.origin.x = frmVwBtnPhotos.size.width/2 - frmImgOpnPhotos.size.width/2;
    
    frmBtnOpnPhotos.origin.x = 0;
    frmBtnOpnPhotos.origin.y = 0;
    frmBtnOpnPhotos.size.width = frmVwBtnPhotos.size.width;
    frmBtnOpnPhotos.size.height = frmVwBtnPhotos.size.height;
    
    frmVwButton.size.height = frmVwBtnFlash.origin.y + frmVwBtnFlash.size.height + 8;
    frmVwButton.origin.y = frmVwScan.size.height - frmVwButton.size.height;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.height = frmVwTitle.size.height;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    
    frmVwQRISLogo.origin.y = 0;
    frmVwQRISLogo.size.width = frmVwBtnFlash.size.width + frmVwBtnPhotos.size.width;
    frmVwQRISLogo.origin.x = frmVwButton.size.width - (frmVwQRISLogo.size.width + 8);
    frmVwQRISLogo.size.height = frmVwQRISLogo.size.height;
    
    frmLblQRISLogo.origin.x = 0;
    frmLblQRISLogo.origin.y = 0;
    frmLblQRISLogo.size.width = frmVwQRISLogo.size.width;
    frmLblQRISLogo.size.height = frmVwQRISLogo.size.height/4;
    
    frmImgQRISLogo.origin.x = 0;
    frmImgQRISLogo.size.width = frmVwQRISLogo.size.width;
    frmImgQRISLogo.origin.y = frmLblQRISLogo.size.height;
    frmImgQRISLogo.size.height = frmVwQRISLogo.size.height - frmLblQRISLogo.size.height;
    
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTittle.frame = frmLblTitle;
    
    self.vwScan.frame = frmVwScan;
    self.vwButton.frame = frmVwButton;
       
    self.vwBtnFlash.frame = frmVwBtnFlash;
    self.vwBtnPhotos.frame = frmVwBtnPhotos;
       
    self.lblFlash.frame = frmLblFlash;
    self.imgFlash.frame = frmImgFlash;
    self.btnFlash.frame = frmBtnFlash;
       
    self.lblOpnPhotos.frame = frmLblOpnPhotos;
    self.imgOpnPhotos.frame = frmImgOpnPhotos;
    self.btnOpnPhotos.frame = frmBtnOpnPhotos;
    
    self.vwQrisLogo.frame = frmVwQRISLogo;
    self.lblQrisLogo.frame = frmLblQRISLogo;
    self.imgQrisLogo.frame = frmImgQRISLogo;

    [self.vwTitle setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    self.imgQrisLogo.image = [UIImage imageNamed:@"ic_qris_logo.png"];
    
    
}

- (CAShapeLayer *) frameScanner : (CGRect ) vwScanner {
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:vwScanner];
    
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = (vwScanner.size.height - transparantWidth) * 0.5;
    
    [overlayPath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(transparantX,
                                                                        transparantY,
                                                                        transparantWidth, transparantWidth)]];
    
    overlayPath.usesEvenOddFillRule = true;
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [[[UIColor blackColor] colorWithAlphaComponent:0.5]CGColor];
    
    return fillLayer;
    
    
}

-(UIView *) borderScanner : (CGRect )vwScanner{
    
    
//    CGFloat transparantWidth = vwScanner.size.width;
//    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
//    CGFloat transparantY = (vwScanner.size.height - transparantWidth) * 0.5;
//
//    UIView *vwKotakScanner = [[UIView alloc]initWithFrame:CGRectMake(transparantX,
//                                                                     transparantY,
//                                                                     transparantWidth, transparantWidth)];
    
    CGFloat transparantWidth = vwScanner.size.width;
    CGFloat transparentHeight = vwScanner.size.height - 40;
    CGFloat transparantX = 0;
    CGFloat transparantY = 0;
    
    UIView *vwKotakScanner = [[UIView alloc]initWithFrame:CGRectMake(transparantX,
                                                                     transparantY,
                                                                     transparantWidth, transparentHeight)];
    
    CGFloat height = transparantWidth +4;
    CGFloat width = transparantWidth +4;
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(1, 25)];
    [path addLineToPoint:CGPointMake(1, 1)];
    [path addLineToPoint:CGPointMake(25, 1)];
    
    [path moveToPoint:CGPointMake(width - 30, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 25)];
    
    [path moveToPoint:CGPointMake(1, height - 30)];
    [path addLineToPoint:CGPointMake(1, height - 5)];
    [path addLineToPoint:CGPointMake(25, height - 5)];
    
    [path moveToPoint:CGPointMake(width - 30, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 30)];
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor = [UIColorFromRGB(const_color_primary) CGColor];
    pathLayer.lineWidth = 3.0f;
    pathLayer.fillColor = nil;
    
    UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, vwKotakScanner.frame.size.width, 1)];
    [vwLine setBackgroundColor:[UIColor yellowColor]];
    
    CGPoint start = CGPointMake(vwKotakScanner.frame.size.width/2,0);
    CGPoint end = CGPointMake(vwKotakScanner.frame.size.width/2, vwKotakScanner.frame.size.height+4);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.delegate = self;
    animation.fromValue = [NSValue valueWithCGPoint:start];
    animation.toValue = [NSValue valueWithCGPoint:end];
    animation.duration = 5;
    
    animation.repeatCount = HUGE_VALF;
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
    
//    [vwKotakScanner.layer addSublayer:pathLayer];
    [vwKotakScanner addSubview:vwLine];
    [vwLine.layer addAnimation:animation forKey:@"position"];
    return vwKotakScanner;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewDidLayoutSubviews {
    //[self setupLayout];
    UILabel *lblTitle = [Utility lblTitleQRIS];
    
    CGRect bounds = self.vwScan.layer.bounds;
    CGRect frmLblTitle = lblTitle.frame;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 32;
    frmLblTitle.size.width = bounds.size.width - (frmLblTitle.origin.x * 2);
    
    lblTitle.frame = frmLblTitle;
    
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    previewLayer.bounds=bounds;
    previewLayer.position=CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
//    [self.vwScan.layer addSublayer:[self frameScanner:bounds]];
//    [self.vwScan addSubview:[self borderScanner:bounds]];
//    [self.vwScan addSubview:lblTitle];
//    [self.vwScan bringSubviewToFront:self.vwButton];
//    [self.vwScan.layer addSublayer:[self borderScanner:bounds]];
}

-(void)actionSelectedTochDown : (UIButton *) sender
                        event : (UIEvent *) event{
   UITouch *touch = [[event touchesForView:sender] anyObject];
    CGPoint origin = [touch locationInView:sender];
    float radius = sender.frame.size.height;
    float duration = 0.5f;
    float fadeAfter = duration * 0.75f;
    
    [sender rippleStartingAt:origin withColor:[UIColor colorWithWhite:0.0f alpha:0.20f] duration:duration radius:radius fadeAfter:fadeAfter];
    
}

-(CGRect) converRectOfInterest : (CGRect)vwScanner{
    CGFloat transparantWidth = vwScanner.size.width - 10;
    CGFloat transparentHeight = vwScanner.size.height - 10;
    
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = (vwScanner.size.height - transparentHeight) * 0.5;
    return CGRectMake(transparantX,transparantY,transparantWidth, transparentHeight);
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection{
    NSString *QRCode = nil;
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            QRCode = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            break;
        }
    }
    if (QRCode) {
        [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": QRCode}];
        [session stopRunning];
        //        [super openNextTemplate];
        [self openTemplateNextStep];
    }
    
    NSLog(@"QR Code: %@", QRCode);
}

-(void)actionFlashOnOff : (UIButton *) sender{
    if (stateFlash) {
        if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] &&
            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOn)
        {
            stateFlash = NO;
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOff];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
        }
    }else{
        if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] &&
            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff)
        {
            stateFlash = YES;
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOn];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
        }
    }
}

-(void) actionBrowsePhotos : (UIButton *) sender{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select File option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Choose File",nil];
    
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma  mark- Opne Action Sheet for Options

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    imagePicker = [[UIImagePickerController alloc] init];
                    
                    imagePicker.delegate = self;
                    
                    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    
                    [self presentViewController:imagePicker animated:YES completion:nil];
                    
                    break;
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark- Open Image Picker Delegate to select image from Gallery or Camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    CIImage *ciimage = [CIImage imageWithData:UIImageJPEGRepresentation(myImage, 1.0f)];
    
    NSDictionary *detectorOptions = @{ CIDetectorAccuracy : CIDetectorAccuracyHigh };
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:detectorOptions];
    
    NSArray *features = [faceDetector featuresInImage:ciimage];
    CIQRCodeFeature *faceFeature;
    NSString *strQR = @"";
    for(faceFeature in features) {
        strQR = [NSString stringWithFormat:@"%@",faceFeature.messageString];
        NSLog(@"Found feature: %@", strQR);
        if (([strQR isEqualToString:@""]) || (strQR == nil)) {
            
        } else {
            [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": strQR}];
        }
        //break;
    }
    if (([strQR isEqualToString:@""]) || (strQR == nil)) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"This is not a valid QR Code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        [alert show];
    } else {
        
        
//        [super openNextTemplate];
        [self openTemplateNextStep];
        
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


-(void) openTemplateNextStep{
    dataManager.currentPosition++;
    NSDictionary *temp = [dataManager getObjectData];
    BOOL canOpenTemplate = true;
    if([[temp valueForKey:@"activation"]boolValue]){
//        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
        if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
            canOpenTemplate = false;
        }
    }
    if(canOpenTemplate){
        NSLog(@"JSON DATA TEMPLATE 3: %@", temp);
        @try {
            TemplateViewController *templateView = [self routeTemplateController:[temp valueForKey:@"template"]];
    //        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier: [temp valueForKey:@"template"]];
            [templateView setJsonData:temp];
            
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            double delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                // Put your code here
                [currentVC pushViewController:templateView animated:YES];
            });
        } @catch (NSException *exception) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",exception.description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alert show];
        }
    }else{
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        NSString *valTitle;
        NSString *valMsg;
        
        if([lang isEqualToString:@"id"]){
            valTitle = @"Informasi";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Lakukan Aktivasi Terlebih Dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            //valCancel = @"Batal";
            //valAgree = @"Lanjut";
        } else {
            valTitle = @"Information";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            //valCancel = @"Cancel";
            //valAgree = @"Next";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:valTitle message:valMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 100;
        [alert show];
        
        /*UIAlertView *alert = [[UIAlertView alloc]
         initWithTitle:valTitle
         message:valMsg
         delegate:nil
         cancelButtonTitle:valCancel
         otherButtonTitles:valAgree, nil];
         
         alert.tag = 229;
         [alert show];*/
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        if (UIApplicationOpenSettingsURLString)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }else if (alertView.tag == 505) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault synchronize];
        
        if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
            [self stopIdleTimer];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
//            [self openStatic:@"NLoginVC"];
        } else {
            exit(0);
        }
    }else if(alertView.tag == 404){
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
        
    }
}

#pragma mark- Protocol for Login Popup
- (void)loginDisappear:(BOOL)loginDone{
    if (loginDone) {
        [self setButtonQrisStyle:tagIdQrisCpm];
    } else {
        [self setButtonQrisStyle:tagIdQrisMpm];
    }
}

- (void)loginDoneState:(NSString *)identifier{
    [self gotoQrisCpm];
}

@end
