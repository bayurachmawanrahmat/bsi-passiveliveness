//
//  FeatureIslamicViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 9/13/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "FeatureIslamicViewController.h"
#import "Utility.h"
#import "ListSurahViewController.h"
#import "Connection.h"
#import "LiveStreamMakkahViewController.h"
#import "JuzAmmaViewController.h"
#import "AsmaulHusnaViewController.h"
#import "TausiahViewController.h"
#import "ListZakatViewController.h"
//#import "PanduanHajiUmrohViewController.h"

@interface FeatureIslamicViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, ConnectionDelegate>{
    NSString *lang;
    NSUserDefaults *userDefault;
    NSArray *dataFeature;
    NSTimer *idleTimer;
    int maxIdle;
    BOOL fromMenu;
    NSArray *listData;
}
@property (weak, nonatomic) IBOutlet UITableView *tblFeatureIslamic;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;


@end

@implementation FeatureIslamicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    fromMenu = NO;
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if ([lang isEqualToString:@"id"]) {
        self.lblTitle.text = @"Layanan Islami";
    }else{
        self.lblTitle.text = @"Islamic Services";
    }
     
    [self setupLayout];
    
    self.tblFeatureIslamic.delegate = self;
    self.tblFeatureIslamic.dataSource = self;
    
    [self.tblFeatureIslamic setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];

}


-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmTblFeature = self.tblFeatureIslamic.frame;
    
    frmVwTitle.origin.x = 0 ;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.height = 40;
    frmLblTitle.size.width = frmVwTitle.size.width - 32;
    
    frmTblFeature.origin.x = 0;
    frmTblFeature.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmTblFeature.size.width = SCREEN_WIDTH;
    frmTblFeature.size.height = SCREEN_HEIGHT - frmTblFeature.origin.y - BOTTOM_NAV_DEF;
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveBottomPadding]) {
        frmTblFeature.size.height = SCREEN_HEIGHT - frmTblFeature.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.tblFeatureIslamic.frame = frmTblFeature;
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{


    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
       UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];

           NSDictionary *data = [listData objectAtIndex:indexPath.row];
    if ([lang isEqualToString:@"id"]) {
        [label setText:[data valueForKey:@"label_id"]];
    }else{
        [label setText:[data valueForKey:@"label_en"]];
    }
       
       CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
       CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
       CGFloat height = MAX(size.height, 33.0);
       CGRect frame = label.frame;
       frame.size.height = height;
       [label setFrame:frame];
       return cell;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 84.0;
    return 58.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dataDict = [listData objectAtIndex:indexPath.row];
    NSString *template = @"";
    if([[dataDict valueForKey:@"code"] isEqualToString: @"1"]) {
        template = @"JUZAMMAVC";
        fromMenu = NO;
    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"2"]){
        template = @"LISTSURAHVC";
        fromMenu = NO;
    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"3"]){
        template = @"ASMHUSVC";
        fromMenu = NO;
    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"4"]){
        template = @"TausiahVC";
        fromMenu = NO;
    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"5"]){
        template = @"LSMAKKAHVC";
        fromMenu = NO;
    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"6"]){
        template = @"LSTZAKAT";
        fromMenu = NO;
    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"7"]){
        template = @"LV12";
        fromMenu = NO;
    }
    
    @try {
        TemplateViewController *templateViews = [self.storyboard instantiateViewControllerWithIdentifier:template];
        
         if ([lang isEqualToString:@"id"]) {
                if ([[dataDict valueForKey:@"code"] isEqualToString: @"1"]) {
                    JuzAmmaViewController *mJuzVc = (JuzAmmaViewController *) templateViews;
                    [mJuzVc setTitleMenu:[dataDict valueForKey:@"label_id"]];
                }else if ([[dataDict valueForKey:@"code"] isEqualToString: @"2"]) {
                    ListSurahViewController *mListSurahVc = (ListSurahViewController *) templateViews;
                    [mListSurahVc setTitleMenu:[dataDict valueForKey:@"label_id"]];
                }
                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"3"]) {
                    AsmaulHusnaViewController *mAsmaulHusnahVc = (AsmaulHusnaViewController *) templateViews;
                    [mAsmaulHusnahVc setTitleMenu:[dataDict valueForKey:@"label_id"]];
                }
                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"4"]) {
                    TausiahViewController *mTausiahVc = (TausiahViewController *) templateViews;
                    [mTausiahVc setTitleMenu:[dataDict valueForKey:@"label_id"]];
                }
                else if([[dataDict valueForKey:@"code"]isEqualToString:@"5"]){
                    LiveStreamMakkahViewController *mLiveVc = (LiveStreamMakkahViewController *) templateViews;
                    [mLiveVc setTitleMenu:[dataDict valueForKey:@"label_id"]];
                }
                else if([[dataDict valueForKey:@"code"] isEqualToString:@"6"]){
                    ListZakatViewController *mLiztZakat = (ListZakatViewController *) templateViews;
                    [mLiztZakat setTitleMenu:[dataDict valueForKey:@"label_id"]];
                }
                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"7"]) {
                    [dataManager setMenuId:@"00201"];
                    fromMenu = YES;
                    [self openTemplateByMenuID:@"00201"];
                }
             
//                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"7"]) {
//                    PanduanHajiUmrohViewController *mPanHajUmVc = (PanduanHajiUmrohViewController *) templateViews;
//                    [mPanHajUmVc setTitleMenu:[dataDict valueForKey:@"label_id"]];
//                }
            }else{
                if ([[dataDict valueForKey:@"code"] isEqualToString: @"1"]) {
                    JuzAmmaViewController *mJuzVc = (JuzAmmaViewController *) templateViews;
                    [mJuzVc setTitleMenu:[dataDict valueForKey:@"label_en"]];
                }
                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"2"]) {
                    ListSurahViewController *mListSurahVc = (ListSurahViewController *) templateViews;
                    [mListSurahVc setTitleMenu:[dataDict valueForKey:@"label_en"]];
                }
                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"3"]) {
                    AsmaulHusnaViewController *mAsmaulHusnahVc = (AsmaulHusnaViewController *) templateViews;
                    [mAsmaulHusnahVc setTitleMenu:[dataDict valueForKey:@"label_en"]];
                }
                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"4"]) {
                    TausiahViewController *mTausiahVc = (TausiahViewController *) templateViews;
                    [mTausiahVc setTitleMenu:[dataDict valueForKey:@"label_en"]];
                }
                
                else if([[dataDict valueForKey:@"code"]isEqualToString:@"5"]){
                    LiveStreamMakkahViewController *mLiveVc = (LiveStreamMakkahViewController *) templateViews;
                    [mLiveVc setTitleMenu:[dataDict valueForKey:@"label_en"]];
                }
                
                else if([[dataDict valueForKey:@"code"] isEqualToString:@"6"]){
                    ListZakatViewController *mLiztZakat = (ListZakatViewController *) templateViews;
                    [mLiztZakat setTitleMenu:[dataDict valueForKey:@"label_en"]];
                }
                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"7"]) {
                    [dataManager setMenuId:@"00201"];
                    fromMenu = YES;
                    [self openTemplateByMenuID:@"00201"];
                    
                }
//                else if ([[dataDict valueForKey:@"code"] isEqualToString: @"8"]) {
//                    PanduanHajiUmrohViewController *mPanHajUmVc = (PanduanHajiUmrohViewController *) templateViews;
//                    [mPanHajUmVc setTitleMenu:[dataDict valueForKey:@"label_en"]];
//                }
            }
        
            if(!fromMenu){
                [self.tabBarController setSelectedIndex:0];
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateViews animated:YES];
            }
            
//            [self.tabBarController setSelectedIndex:0];
//            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
//            [currentVC pushViewController:templateViews animated:YES];
        
        NSDictionary* userInfo = @{@"position": @(1118)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated{
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *isExist = [userDefault objectForKey:@"isExist"];
    bool stateAcepted = [Utility vcNotifConnection: @"FEISLAMIC"];
    if (stateAcepted) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 404;
        [alert show];
    }else{
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=setting,prefix=islamic"] needLoading:true encrypted:false banking:false favorite:false];
    }
    
}


- (void)openStatic:(NSString *)templateName{
    RootViewController *templateView =  [self.storyboard instantiateViewControllerWithIdentifier:templateName];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}


-(void) disposePenyesuaianPopupFiltering{
    @try {
        UINavigationController *nav = (UINavigationController*)self.presentingViewController;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            [nav popToRootViewControllerAnimated:YES];
        }];

    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    [self timerReset];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if (![requestType isEqualToString:@"check_notif"]) {
        NSDictionary *data = (NSDictionary * )jsonObject;
//        NSArray *arVMap = [[data valueForKey:@"islamic.menu.vmap"] componentsSeparatedByString:@"|"];
//        NSString *strVersionMap = @"v1";
//        for(NSString *strVmap in arVMap){
//            NSArray *arCVmap = [strVmap componentsSeparatedByString:@"="];
//            if ([[arCVmap objectAtIndex:0] isEqualToString:@VERSION_VALUE]) {
//                strVersionMap =[arCVmap objectAtIndex:1];
//                break;
//            }
//        }
         NSArray *arVMap = [[data valueForKey:@"islamic.menu.vmap"] componentsSeparatedByString:@"|"];
         NSString *strVersionMap = @"";
         for(NSString *strVmap in arVMap){
             NSArray *arCVmap = [strVmap componentsSeparatedByString:@"="];
             if ([[arCVmap objectAtIndex:0] isEqualToString:@VERSION_VALUE]) {
                 strVersionMap =[arCVmap objectAtIndex:1];
                 break;
             }
         }
         
         if([strVersionMap isEqualToString:@""]){
             NSArray *arCVmap = [arVMap.lastObject componentsSeparatedByString:@"="];
             strVersionMap =[arCVmap objectAtIndex:1];
         }
        
        NSString *menuIslamic = [data valueForKey:[NSString stringWithFormat:@"islamic.menu.%@", strVersionMap]];
        NSError *error;
        listData = [NSJSONSerialization JSONObjectWithData:[menuIslamic dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error != nil) {
            listData = [[NSArray alloc]init];
        }
        [self.tblFeatureIslamic reloadData];
        [userDefault setValue:[data valueForKey:@"islamic.youtube.id"] forKey:@"islamic.youtube.id"];
        [userDefault setValue:[data valueForKey:@"islamic.gold.price"] forKey:@"islamic.gold.price"];
        [userDefault setValue:[data valueForKey:@"islamic.rice.price"] forKey:@"islamic.rice.price"];

        
    }

}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 404;
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
}

@end
