//
//  LocationViewController.m
//  BSM-Mobile
//
//  Created by lds on 7/7/14.
//  Copyright (c) 2014 lds. All rights reserved.
//

#import "LocationViewController.h"
#import "CellTableData.h"
#import "DetailLocationViewController.h"
#import "HomeViewController.h"
#import "RootViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Utility.h"
#import "Styles.h"

@interface LocationViewController ()<ConnectionDelegate,CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    NSArray *listData;
    NSArray *listAtm;
    NSArray *listBrach;
    NSDictionary *data;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;
    NSString *typeLoc;
    
    #pragma mark - init timer standalone
    NSTimer *idleTimer;
    int maxIdle;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UITableView *tableData;
@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UIView *lineAtm;
@property (weak, nonatomic) IBOutlet UIView *lineBranch;
@property (weak, nonatomic) IBOutlet UIButton *btnAtm;
@property (weak, nonatomic) IBOutlet UIButton *btnBranch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@property (weak, nonatomic) IBOutlet UIView *vwLblTitle;


@end

@implementation LocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.vwLblTitle setHidden:YES];
    
    [Styles setTopConstant:_topConstant];
    //[self.vwLblTitle removeFromSuperview];

    self.btnAtm.backgroundColor = [UIColor whiteColor];
    self.btnBranch.backgroundColor = [UIColor whiteColor];
    [self.btnAtm setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    self.btnAtm.titleLabel.font = [UIFont fontWithName:const_font_name3 size:15];
    [self.btnBranch setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    self.btnBranch.titleLabel.font = [UIFont fontWithName:const_font_name3 size:15];
    self.lineAtm.backgroundColor = UIColorFromRGB(const_color_secondary);
    self.lineBranch.backgroundColor = UIColorFromRGB(const_color_secondary);
    
    [_lineAtm setHidden:NO];
    [_lineBranch setHidden:YES];
    typeLoc = @"ATM";
    
    self.tableData.dataSource = self;
    self.tableData.delegate = self;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]) {
        _lbl.text = @"Lokasi ATM";
        [_btnBranch setTitle:@"CABANG" forState:UIControlStateNormal];
    } else {
        _lbl.text = @"ATM Location";
        [_btnBranch setTitle:@"BRANCH" forState:UIControlStateNormal];
    }
    
    if([[DataManager sharedManager].dataExtra objectForKey:@"atmbranchloc"]){
        if([[[DataManager sharedManager].dataExtra valueForKey:@"atmbranchloc"] isEqualToString:@"0"]){
            [self actionAtm:self];
        }else{
            [self actionBrach:self];
        }
    }
    [self.tableData registerNib:[UINib nibWithNibName:@"CellTableData" bundle:nil] forCellReuseIdentifier:@"LOCATIONCELL"];
}



- (void)viewWillAppear:(BOOL)animated{
    if([[DataManager sharedManager].dataExtra objectForKey:@"atmbranchloc"]){
        if([[[DataManager sharedManager].dataExtra valueForKey:@"atmbranchloc"] isEqualToString:@"0"]){
            [self actionAtm:self];
        }else{
            [self actionBrach:self];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated{
    bool stateAcepted = [Utility vcNotifConnection: @"ATMVC"];
    [self actionAtm:self];
    if (stateAcepted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    
        }]];
        [self presentViewController:alert animated:TRUE completion:nil];
        
    }else{
         [self currentLocationIdentifier];
    }
    
}

-(void)currentLocationIdentifier
{
    @try{
        if ([CLLocationManager locationServicesEnabled]){
            locationManager = nil;
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
            {
                [locationManager requestWhenInUseAuthorization];
            }
            
            [locationManager startUpdatingLocation];
        } else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"LOCATION_SERVICE_STATE") message:lang(@"LOCATION_SERVICE_MESSAGE") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self.tabBarController setSelectedIndex:0];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }@catch (NSException *exception){
        NSLog(@"%@", exception);
    }
    
}

-(double) distanceCallculator:(double)lat1 long1:(double)lng1 lat2:(double)lat2 long2:(double)lng2 {
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:lat1 longitude:lng1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:lat2 longitude:lng2];
    double dMeters = [loc1 distanceFromLocation:loc2];
    return dMeters;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];

    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    
    NSString *paramData = [NSString stringWithFormat:@"request_type=list_atm_branch,latitude=%f,longitude=%f",  lat, lng];
    NSLog(@"kirim location: %@", [NSString stringWithFormat:@"request_type=list_atm_branch,latitude=%f,longitude=%f", lat, lng]);
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellTableData *cell = (CellTableData*)[tableView dequeueReusableCellWithIdentifier:@"LOCATIONCELL"];
    
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    
    if ([typeLoc isEqualToString:@"ATM"]) {
        cell.imgIcon.image = [UIImage imageNamed:@"ic_list_atm"];
    } else if ([typeLoc isEqualToString:@"BRANCH"]) {
        cell.imgIcon.image = [UIImage imageNamed:@"ic_list_cabang"];
    }
    
    cell.name.text = [data objectForKey:@"name"];
    cell.address.text = [data objectForKey:@"address"];
    double distance =  [[data objectForKey:@"distance"] doubleValue];
    if (distance < 1000) {
         cell.distance.text = [NSString stringWithFormat:@"%.2f m", distance];
    } else {
         cell.distance.text = [NSString stringWithFormat:@"%.2f km", (distance / 1000)];
    }
    cell.distance.textColor = UIColorFromRGB(const_color_primary);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    data = [listData objectAtIndex:indexPath.row];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:data forKey:@"passingData"];
    @try {
         //[self performSegueWithIdentifier:@"detailLocation" sender:self];
//        RootViewController *templateView =  [self.storyboard instantiateViewControllerWithIdentifier:@"DetailLocation"];
//        [self.tabBarController setSelectedIndex:0];
//        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
//        [currentVC pushViewController:templateView animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:true];
        data = [listData objectAtIndex:indexPath.row];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:data forKey:@"passingData"];
        [self performSegueWithIdentifier:@"branchDetailLocation" sender:self];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
}
    
    
#pragma mark - Connection
-(void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    NSDictionary* userInfo = @{@"position": @(1118)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            NSArray *atm = [jsonObject objectForKey:@"atm"];
            NSMutableArray *temp = [[NSMutableArray alloc] init];
            for (NSDictionary *data in atm){
                NSMutableDictionary *dictX = [NSMutableDictionary new];
                [dictX setValue:[data objectForKey:@"atm_address"] forKey:@"address"];
                [dictX setValue:[data objectForKey:@"atm_name"] forKey:@"name"];
                [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
                [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
                [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
                double distance = [self distanceCallculator:lat long1:lng lat2:[[data objectForKey:@"latitude"] doubleValue] long2:[[data objectForKey:@"longitude"] doubleValue]];
                [dictX setValue:[NSNumber numberWithDouble: distance] forKey:@"distance"];
                [temp addObject:dictX];
            }
            
            NSArray *brach = [jsonObject objectForKey:@"branch"];
            NSMutableArray *tempBranch = [[NSMutableArray alloc] init];
            for (NSDictionary *data in brach){
                NSMutableDictionary *dictX = [NSMutableDictionary new];
                if ([data objectForKey:@"branch_address"]){
                    [dictX setValue:[data objectForKey:@"branch_address"] forKey:@"address"];
                } else {
                    [dictX setValue: @"" forKey:@"address"];
                }
                [dictX setValue:[data objectForKey:@"branch_name"] forKey:@"name"];
                [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
                [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
                [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
                double distance = [self distanceCallculator:lat long1:lng lat2:[[data objectForKey:@"latitude"] doubleValue] long2:[[data objectForKey:@"longitude"] doubleValue]];
                [dictX setValue:[NSNumber numberWithDouble: distance] forKey:@"distance"];
                [tempBranch addObject:dictX];
            }
            
            
            if ([temp count] > 0 || [tempBranch count] > 0) {
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
                listAtm = [temp sortedArrayUsingDescriptors:@[descriptor]];
                listBrach = [tempBranch sortedArrayUsingDescriptors:@[descriptor]];
                listData = listAtm;
                [self.tableData reloadData];
            }
            else {

//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NO_ATM_MESSAGE") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
                
                [Utility showMessage:lang(@"NO_ATM_MESSAGE") instance:self];
                
            }
        } else if ([jsonObject isKindOfClass:[NSArray class]]){
            NSArray *atm = jsonObject;
            NSMutableArray *temp = [[NSMutableArray alloc] init];
            for (NSDictionary *data in atm){
                
                NSLog(@"--- %@", data);
                NSMutableDictionary *dictX = [NSMutableDictionary new];
                [dictX setValue:[data objectForKey:@"atm_address"] forKey:@"address"];
                [dictX setValue:[data objectForKey:@"atm_name"] forKey:@"name"];
                [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
                [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
                [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
                double distance = [self distanceCallculator:lat long1:lng lat2:[[data objectForKey:@"latitude"] doubleValue] long2:[[data objectForKey:@"longitude"] doubleValue]];
                [dictX setValue:[NSNumber numberWithDouble: distance] forKey:@"distance"];
                [temp addObject:dictX];
            }
            
            if ([temp count] > 0) {
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
                listAtm = [temp sortedArrayUsingDescriptors:@[descriptor]];
                listBrach = [[NSArray alloc] init];
                listData = listAtm;
                [self.tableData reloadData];
            } else {
//                NSString *msg = @"";
//                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//
//                if([lang isEqualToString:@"id"]) {
//                    msg = @"Maaf tidak ada ATM di sekitar anda";
//                } else {
//                    msg = @"Sorry there are no ATMs around you";
//                }
//
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
                
                [Utility showMessage:lang(@"NO_ATM_MESSAGE") instance:self];
            }
        } else {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            [Utility showMessage:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] instance:self];
            
        }
    }
}

-(void)errorLoadData:(NSError *)error{
   // [self startIdleTimer];
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    [Utility showMessage:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] instance:self];

}

- (void)reloadApp{
    DataManager *dataManager;
    [dataManager resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

- (IBAction)actionAtm:(id)sender {
    
    typeLoc = @"ATM";
    [_lineAtm setHidden:NO];
    [_lineBranch setHidden:YES];
    [_btnAtm setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    [_btnBranch setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
    listData = listAtm;
    [self.tableData reloadData];
}

- (IBAction)actionBrach:(id)sender {
    
    typeLoc = @"BRANCH";
    [_lineAtm setHidden:YES];
    [_lineBranch setHidden:NO];
    [_btnAtm setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
    [_btnBranch setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    listData = listBrach;
    [self.tableData reloadData];
}

- (void)openStatic:(NSString *)templateName{
    RootViewController *templateView =  [self.storyboard instantiateViewControllerWithIdentifier:templateName];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}


@end
