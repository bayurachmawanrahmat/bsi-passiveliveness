//
//  HelpDeskViewController.m
//  BSM-Mobile
//
//  Created by ARS on 26/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "HelpDeskViewController.h"
#import "Utility.h"
#import "Connection.h"
//#import "FormBantuanViewController.h"
#import "PopUpLoginViewController.h"
#import "ComplainViewController.h"
#import "WebviewViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"


@interface HelpDeskViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, ConnectionDelegate>{
    NSString *lang;
    NSUserDefaults *userDefault;
    NSArray *listData;
    NSTimer *idleTimer;
    int maxIdle;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblHelpdesk;

@end

@implementation HelpDeskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(helpDeskBC:) name:@"helpDesk" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(chatAisyahBC:) name:@"chatAisyah" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(complainHandlerBC:) name:@"complainHandler" object:nil];
    
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if ([lang isEqualToString:@"id"]) {
        self.lblTitle.text = [userDefault valueForKey:@"config.helpdesk.title.id"];
    }else{
        self.lblTitle.text = [userDefault valueForKey:@"config.helpdesk.title.en"];
    }
    
    //update styles
    self.imgTitle.hidden = YES;
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);

     
    [self setupLayout];
    
    self.tblHelpdesk.delegate = self;
    self.tblHelpdesk.dataSource = self;
    
    [self.tblHelpdesk setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(featIsTimer:)
                                                 name:@"featIsTimer"
                                               object:nil];
    
}

- (void)chatAisyahBC:(NSNotification *) notification{
    if([notification.name isEqualToString:@"chatAisyah"])
    {
        NSDictionary *userInfo = notification.userInfo;
        
        if([[userInfo valueForKey:@"logged_in"] isEqualToString:@"YES"]){
            [self chatAisyah];
        } else {
            [self.navigationController popToRootViewControllerAnimated:true];
        }
        
    }
}
- (void)complainHandlerBC:(NSNotification *) notification{
    if([notification.name isEqualToString:@"complainHandler"])
    {
        NSDictionary *userInfo = notification.userInfo;
        
        if([[userInfo valueForKey:@"logged_in"] isEqualToString:@"YES"]){
            [self gotocomplain];
        } else {
            [self.navigationController popToRootViewControllerAnimated:true];
        }
        
    }
}
- (void)helpDeskBC:(NSNotification *) notification{
    if([notification.name isEqualToString:@"helpDesk"])
    {
        NSDictionary *userInfo = notification.userInfo;
        
        if([[userInfo valueForKey:@"logged_in"] isEqualToString:@"YES"]){
//            loginDone = true;
//            [self.tblHelpdesk reloadData];
        } else {
            [self.navigationController popToRootViewControllerAnimated:true];
        }
        
    }
}

-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmTblFeature = self.tblHelpdesk.frame;
    
    frmVwTitle.origin.x = 0 ;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.size.width = frmVwTitle.size.width - (frmLblTitle.origin.x*2);
    frmLblTitle.size.height = frmVwTitle.size.height;
    frmLblTitle.origin.y = 0;
    
    frmTblFeature.origin.x = 0;
    frmTblFeature.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmTblFeature.size.width = SCREEN_WIDTH;
    frmTblFeature.size.height = SCREEN_HEIGHT - frmTblFeature.origin.y - BOTTOM_NAV_DEF;
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveBottomPadding]) {
        frmTblFeature.size.height = SCREEN_HEIGHT - frmTblFeature.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.tblHelpdesk.frame = frmTblFeature;
}


- (void)viewWillDisappear:(BOOL)animated {

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    if ([isExist isEqualToString:@"YES"]) {
        [self stopIdleTimer];
    }

}

- (void)viewDidAppear:(BOOL)animated{
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
        [self startIdleTimer];
    } else {
        [self refreshIdleTimer];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{


    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"helpDeskCell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
           NSDictionary *data = [listData objectAtIndex:indexPath.row];
    if ([lang isEqualToString:@"id"]) {
        [label setText:[data valueForKey:@"label_id"]];
    }else{
        [label setText:[data valueForKey:@"label_en"]];
    }
       
       CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
       CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
       CGFloat height = MAX(size.height, 33.0);
       CGRect frame = label.frame;
       frame.size.height = height;
       [label setFrame:frame];
       return cell;
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 84.0;
    return 58.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dataDict = [listData objectAtIndex:indexPath.row];
    
    NSString *custId = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    //       NSLog(@"%@",[selectedMenu objectAtIndex:0]);
    NSString *template = @"";
    if([[dataDict valueForKey:@"code"] isEqualToString: @"1"]) {
         NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
              NSString *msg = @"";
              NSString *msgCancel = @"";

              if([lang isEqualToString:@"id"]) {
                  msg = @"Anda ingin menghubungi Call Center ?";
                  msgCancel = @"Batal";
              } else {
                  msg = @"Do you want to contact Call Center ?";
                  msgCancel = @"Cancel";
              }

              UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:msgCancel otherButtonTitles:@"Ok", nil];
              alert.delegate = self;
              alert.tag = 3312;
              [alert show];
    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"2"]){
//        if ([userDefault objectForKey:@"customer_id"]){
        if (custId){
            if([[userDefault valueForKey:@"hasLogin"] isEqualToString:@"NO"]){
                [self checkState];
                PopUpLoginViewController *popLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupLogin"];
                [popLogin setMFeat:@"complainHandler"];
                [self presentViewController:popLogin animated:YES completion:nil];
            }else{
                [self gotocomplain];
            }
        }else{
            [self activationFirst];
        }

    }else if([[dataDict valueForKey:@"code"] isEqualToString: @"3"]){
        if(custId){
            if([[userDefault valueForKey:@"hasLogin"] isEqualToString:@"NO"]){
                [self checkState];
                PopUpLoginViewController *popLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupLogin"];
                [popLogin setMFeat:@"chatAisyah"];
                [self presentViewController:popLogin animated:YES completion:nil];
            }else{
                [self chatAisyah];
            }
        }else{
            [self activationFirst];
        }
    }
}

- (void) gotocomplain{
     ComplainViewController *popBantuan = [self.storyboard instantiateViewControllerWithIdentifier:@"COMPLAIN"];

     [self.tabBarController setSelectedIndex:0];
     UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
     [currentVC pushViewController:popBantuan animated:YES];

    NSDictionary* userInfo = @{@"position": @(1118)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void) activationFirst{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_REQUEST") preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
        PopupOnboardingViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [self presentViewController:viewCont animated:YES completion:nil];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void) checkState{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    bool stateAcepted = [Utility vcNotifConnection: @"FEISLAMIC"];
    
    if(stateAcepted){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
        }]];
    }
    
    if(([isExist isEqualToString:@"NO"])||(isExist == nil)){
        NSDictionary* userInfo = @{@"position": @(1112)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }else{
        NSDictionary* userInfo = @{@"position": @(1114)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
}

#pragma mark featIsTimer timer selector
- (void) featIsTimer:(NSNotification *) notification {
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    if ([notification.name isEqualToString:@"featIsTimer"]){
        NSDictionary* userInfo = notification.userInfo;
        NSString *actionTimer = [userInfo valueForKey:@"actionTimer"];
        if ([actionTimer isEqualToString:@"START_TIMER"]) {
            if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
                [self startIdleTimer];
            } else {
                [self refreshIdleTimer];
            }
        }else if ([actionTimer isEqualToString:@"STOP_TIMER"]){
            if ([isExist isEqualToString:@"YES"]) {
                [self stopIdleTimer];
            }
        }else if([actionTimer isEqualToString:@"TIME_OUT"]){
            if ([isExist isEqualToString:@"YES"]) {
                [self stopIdleTimer];
//                [self showPopupLogout];
                [self backToR];
            }
        }
        else{
            [self refreshIdleTimer];
        }
    }
}

-(void)startIdleTimer {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    
    if ((cid == nil) || ([cid isEqualToString:@""])) {
        NSLog(@"Don't start auto logout timer");
    } else {
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"YES"]) {
                if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
                    [self resetCountDown];
                    
                    [idleTimer invalidate];
                    idleTimer = nil;
                    idleTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                 target:self
                                                               selector:@selector(countDown)
                                                               userInfo:nil
                                                                repeats:YES];
                    
                    [[NSRunLoop mainRunLoop] addTimer:idleTimer forMode:NSRunLoopCommonModes];
                    [userDefault setValue:@"YES" forKey:@"isExist"];
                    [userDefault synchronize];
                }
            }
        }
    }
}

-(void)stopIdleTimer {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"NO" forKey:@"isExist"];
    [userDefault synchronize];
    [idleTimer invalidate];
    idleTimer = nil;
    [self resetCountDown];
}

-(void)refreshIdleTimer {
    [self resetCountDown];
}

-(void)countDown {
    NSLog(@"%@", [NSString stringWithFormat:@"Countdown %d",maxIdle]);
    if (maxIdle == 0) {
        [idleTimer invalidate];
        idleTimer = nil;
//        [self showPopupLogout];
        [self backToR];
    } else {
        maxIdle = maxIdle - 1;
    }
}

-(void) showPopupLogout{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *alertTitle = @"";
    NSString *alertMessage = @"";
    if ([lang isEqualToString:@"en"]) {
        alertTitle = @"Session has ended";
        alertMessage = @"Your session has ended. Please log in to continue using BSI Mobile";
    } else {
        alertTitle = @"Sesi Berakhir";
        alertMessage = @"Sesi Anda telah berakhir. Silahkan login ulang untuk melanjutkan penggunaan BSI Mobile";
    }
    
    [self disposePenyesuaianPopupFiltering];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 505;
    [alert show];
}

-(void)resetCountDown{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSInteger nMaxIdle = [[userDefault objectForKey:@"config_timeout_session"]integerValue] / 1000;
    if (nMaxIdle == 0) {
        maxIdle = MAX_IDLE;
    }else{
        maxIdle = (int)nMaxIdle;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    bool stateAcepted = [Utility vcNotifConnection: @"FEISLAMIC"];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=setting,prefix=helpdesk"] needLoading:true encrypted:false banking:false favorite:false];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 228) {
        
         if (buttonIndex == 0) {
                   //Back to Home
               NSDictionary* userInfo = @{@"position": @(313)};
               NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
               [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
           } else if (buttonIndex == 1) {
               NSDictionary* userInfo = @{@"position": @(313)};
               NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
               [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
               
               NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
               if (![[userDefault valueForKey:@"config_onboarding"] boolValue]){
                              
                   [userDefault setObject:@"NO" forKey:@"fromSliding"];
                   [userDefault synchronize];
                   
                   UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
                   self.slidingViewController.topViewController = viewCont;
               }else{

                   NSDictionary* userInfos = @{@"position": @(1119)};
                              NSNotificationCenter* ncs = [NSNotificationCenter defaultCenter];
                              [ncs postNotificationName:@"listenerNavigate" object:self userInfo:userInfos];
               }

           }
    }else if(alertView.tag == 404){
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    }else if (alertView.tag == 3312) {
        if(buttonIndex == 1){
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://14040"]];
        }
    }
}

- (void)openStatic:(NSString *)templateName{
    RootViewController *templateView =  [self.storyboard instantiateViewControllerWithIdentifier:templateName];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}

-(void) disposePenyesuaianPopupFiltering{
    @try {
        UINavigationController *nav = (UINavigationController*)self.presentingViewController;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            [nav popToRootViewControllerAnimated:YES];
        }];

    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self timerReset];
}

-(void) timerReset{
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
        [self startIdleTimer];
    } else {
        [self refreshIdleTimer];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    NSDictionary* userInfo = @{@"position": @(1118)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if (![requestType isEqualToString:@"check_notif"]) {
        NSDictionary *data = (NSDictionary * )jsonObject;
        NSArray *arVMap = [[data valueForKey:@"helpdesk.menu.vmap"] componentsSeparatedByString:@"|"];
        NSString *strVersionMap = @"v1";
        for(NSString *strVmap in arVMap){
            NSArray *arCVmap = [strVmap componentsSeparatedByString:@"="];
            if ([[arCVmap objectAtIndex:0] isEqualToString:@VERSION_VALUE]) {
                strVersionMap =[arCVmap objectAtIndex:1];
                break;
            }
        }
        
        
        if([strVersionMap isEqualToString:@""]){
            NSArray *arCVmap = [arVMap.lastObject componentsSeparatedByString:@"="];
            strVersionMap =[arCVmap objectAtIndex:1];
        }
        
        NSString *menuIslamic = [data valueForKey:[NSString stringWithFormat:@"helpdesk.menu.%@", strVersionMap]];
        NSError *error;
        listData = [NSJSONSerialization JSONObjectWithData:[menuIslamic dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
        if (error != nil) {
            listData = [[NSArray alloc]init];
        }
        [self.tblHelpdesk reloadData];
    }
    
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 404;
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
}

- (void) chatAisyah{
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.syariahmandiri.co.id/"]];
//    if ([userDefault objectForKey:@"customer_id"]){
    if ([NSUserdefaultsAes getValueForKey:@"customer_id"]){
//         if([[userDefault objectForKey:@"email"]isEqualToString:@""] || [userDefault objectForKey:@"email"] == nil){
         if([[NSUserdefaultsAes getValueForKey:@"email"] isEqualToString:@""] || [NSUserdefaultsAes getValueForKey:@"email"] == nil){
             NSString *chatMessage = @"";
             if([lang isEqualToString:@"id"]){
                 chatMessage = @"Email tidak boleh kosong, harap isi email di menu pengaturan.";
             }else{
                 chatMessage = @"Email cannot be empty, please fill your email in setting menu.";
             }
             
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:chatMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             alert.delegate = self;
             alert.tag = 404;
             [alert show];
             
         }else{
             WebviewViewController *webViewAisa = [self.storyboard instantiateViewControllerWithIdentifier:@"webviewIdentifier"];
              [self.tabBarController setSelectedIndex:0];
              UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
              [currentVC pushViewController:webViewAisa animated:YES];
                 
         }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_REQUEST") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.delegate = self;
        alert.tag = 404;
        [alert show];
    }
}

@end
