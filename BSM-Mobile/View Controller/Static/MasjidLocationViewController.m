//
//  MasjidLocationViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/18/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "MasjidLocationViewController.h"
#import "DetailMasjidLocationViewController.h"
#import "HomeViewController.h"
#import "RootViewController.h"
#import "DLAVAlertView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Utility.h"
#import "Styles.h"
#import "CellTableData.h"

@interface MasjidLocationViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    NSArray *listData;
    NSArray *listMasjid;
    NSDictionary *data;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;
    DLAVAlertView *loadingAlert;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UITableView *tableData;
@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;

@end

@implementation MasjidLocationViewController

NSArray *listName;
NSArray *listAddress;
NSArray *listRange;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self currentLocationIdentifier];
}

- (void)fillData {
    listName = @[@"MASJID AL-IHSAN", @"MASJID JAMI' AL HIDAYAT"];
    listAddress = @[@"Jl. MH Thamrin No 5", @"Jl. Kebon Sirih"];
    listRange = @[@"5 meter", @"10 meter"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.imgIcon setHidden:YES];
    
    [Styles setTopConstant:_topSpace];
    
    // Do any additional setup after loading the view.
    self.tableData.dataSource = self;
    self.tableData.delegate = self;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]) {
        self.lbl.text = @"Lokasi Masjid";
        self.lblDesc.text = @"Masjid terdekat di lokasi anda";
    } else {
        self.lbl.text = @"Mosque Location";
        self.lblDesc.text = @"List of Mosque near you";
    }
    
    //update styles
    self.lbl.textColor = UIColorFromRGB(const_color_title);
    self.lbl.textAlignment = const_textalignment_title;
    self.lbl.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwDesc.backgroundColor = UIColorFromRGB(const_color_primary);
    
    //[self fillData];

    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    [self.tableData registerNib:[UINib nibWithNibName:@"CellTableData" bundle:nil] forCellReuseIdentifier:@"LOCATIONCELL"];
    [self.tableData setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)currentLocationIdentifier
{
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else{
        
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"Continue",nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert setDelegate:self];
    }
}

-(double) distanceCallculator:(double)lat1 long1:(double)lng1 lat2:(double)lat2 long2:(double)lng2 {
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:lat1 longitude:lng1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:lat2 longitude:lng2];
    double dMeters = [loc1 distanceFromLocation:loc2];
    return dMeters;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    
    // Lokasi Wisma Mandiri I
    //lat = -6.183712;
    //lng = 106.823641;
    
    [self searchNearbyMosque];
}

- (void)showLoading:(NSString *)message{
    DLog(@"create loading");
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    loadingAlert = [[DLAVAlertView alloc]initWithTitle:lang(@"WAIT") message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    
    UIView *viewLoading = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 37.0, 37.0)];
    [viewLoading setBackgroundColor:[UIColor clearColor]];
    UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [loadingView setColor:[UIColor blackColor]];
    [loadingView startAnimating];
    [viewLoading addSubview:loadingView];
    
    [loadingAlert setContentView:viewLoading];
    [loadingAlert show];
}

- (void)dismissLoading{
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    [loadingAlert dismissWithClickedButtonIndex:0 animated:YES];
    loadingAlert = nil;
}

- (void)searchNearbyMosque{
    if(!loadingAlert){
        [self showLoading:@""];
    }

    NSString *url = [GMAP_URL stringByAppendingString:[NSString stringWithFormat:@"location=%f,%f&radius=5000&types=mosque&sensor=true&key=%@", lat, lng, API_KEY_SERVER]];
    //older code gmaps for requested
    NSURL *googleRequestURL = [NSURL URLWithString:url];
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
//        [self.tableData reloadData];
    });
    
//    @try {
//        NSURL *googleRequestURL = [NSURL URLWithString:url];
//        dispatch_async(kBgQueue, ^{
//            NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
//            [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
//
//        });
//
//    } @catch (NSException *exception) {
//        NSLog(@"%@", exception);
//        NSDictionary* userInfo = @{@"position": @(1112)};
//        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
//    } @finally {
//        NSDictionary* userInfo = @{@"position": @(1112)};
//        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
//         [self.tableData reloadData];
//    }
}

-(void)fetchedData:(NSData *)responseData {
    
//    NSDictionary* userInfo = @{@"position": @(1113)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    //parse out the json data
    NSError* error;
    if(responseData != nil){
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:responseData
                              options:kNilOptions
                              error:&error];
        
        //The results from Google will be an array obtained from the NSDictionary object with the key "results".
        //NSArray* places = [json objectForKey:@"results"];
        NSArray *arrMasjid = [json objectForKey:@"results"];
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        NSMutableDictionary *tempLoc = [[NSMutableDictionary alloc] init];
        for (NSDictionary *data in arrMasjid){
            NSMutableDictionary *dictX = [NSMutableDictionary new];
            [dictX setValue:[data objectForKey:@"vicinity"] forKey:@"vicinity"];
            [dictX setValue:[data objectForKey:@"name"] forKey:@"name"];
            [dictX setValue:[data objectForKey:@"geometry"] forKey:@"geometry"];
            tempLoc = [[data objectForKey:@"geometry"] valueForKey:@"location"];
            [dictX setValue:[data objectForKey:@"lat"] forKey:@"lat"];
            [dictX setValue:[data objectForKey:@"lng"] forKey:@"lng"];
            double distance = [self distanceCallculator:lat long1:lng lat2:[[tempLoc objectForKey:@"lat"] doubleValue] long2:[[tempLoc objectForKey:@"lng"] doubleValue]];
            [dictX setValue:[NSNumber numberWithDouble: distance] forKey:@"distance"];
            [temp addObject:dictX];
        }
        
        NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        listMasjid = [temp sortedArrayUsingDescriptors:@[descriptor]];
        
        //Write out the data to the console.
        NSLog(@"Google Data: %@", listMasjid);
        [self.tableData reloadData];
    }else{
        [Utility showMessage:@"Failed to Fetch Data" instance:self];
    }
    
    [self dismissLoading];
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listMasjid.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellTableData *cell = (CellTableData*)[tableView dequeueReusableCellWithIdentifier:@"LOCATIONCELL"];
    NSDictionary *data = [listMasjid objectAtIndex:indexPath.row];
    
    cell.imgIcon.image = [UIImage imageNamed:@"ic_list_masjidterdekat"];
    cell.name.text = [data objectForKey:@"name"];
    cell.address.text = [data objectForKey:@"vicinity"];
    
    NSMutableDictionary *temp = [data objectForKey:@"geometry"];
    NSMutableDictionary *tempLoc = [temp objectForKey:@"location"];
    NSString *latMasjid = [tempLoc objectForKey:@"lat"];
    NSString *lngMasjid = [tempLoc objectForKey:@"lng"];
    double jarak = [self distanceCallculator:lat long1:lng lat2:[latMasjid doubleValue] long2:[lngMasjid doubleValue]];
    if (jarak < 1000) {
        cell.distance.text = [NSString stringWithFormat:@"%.02f m",jarak];
    }
    else {
        cell.distance.text = [NSString stringWithFormat:@"%.02f km",(jarak / 1000)];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        [self dismissLoading];
    }
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    data = [listMasjid objectAtIndex:indexPath.row];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:data forKey:@"passingData"];
    [self performSegueWithIdentifier:@"detailMasjidLocation" sender:self];
}


@end
