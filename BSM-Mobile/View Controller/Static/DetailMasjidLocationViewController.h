//
//  DetailMasjidLocationViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/18/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "RootViewController.h"

@interface DetailMasjidLocationViewController : RootViewController
@property (nonatomic, strong) NSDictionary *data;
@end
