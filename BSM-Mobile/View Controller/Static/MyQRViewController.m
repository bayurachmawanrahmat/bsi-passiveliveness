//
//  MyQRViewController.m
//  BSM-Mobile
//
//  Created by BSM on 2/4/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "MyQRViewController.h"
#import "RootViewController.h"
#import "Connection.h"
#import "Utility.h"

@interface MyQRViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate>{
    NSArray *listData;
    NSString *defaultAccount;
    UIView *viewBgRootQR;
    UILabel *titleContinerRootQR;
    UIView *viewContinerRootQR;
    UIImageView *imgShowRootQR;
    NSUserDefaults *userDefault;
    NSInteger *closeOption;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgQR;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
@property (weak, nonatomic) IBOutlet UILabel *lblQR;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet CustomBtn *btnShare;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSave;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet UIView *vwPopupQR;
@property (weak, nonatomic) IBOutlet UIView *vwContainerQR;
@property (weak, nonatomic) IBOutlet UIImageView *imgMyQR;
@property (weak, nonatomic) IBOutlet UILabel *txtPopupQRTitle;
@property (weak, nonatomic) IBOutlet UILabel *txtQRAcquirer;
@property (weak, nonatomic) IBOutlet UILabel *txtQRAccName;
@property (weak, nonatomic) IBOutlet UILabel *txtQRAccNo;
@property (weak, nonatomic) IBOutlet UILabel *txtQRFootnote;
@property (weak, nonatomic) IBOutlet UIButton *btnQrClose;
@property (weak, nonatomic) IBOutlet UIButton *btnQrOk;
@property (weak, nonatomic) IBOutlet UIButton *btnQrShare;

- (IBAction)actionOK:(id)sender;
- (IBAction)actionShare:(id)sender;
- (IBAction)actionSave:(id)sender;
@end

@implementation MyQRViewController

- (void)viewWillAppear:(BOOL)animated {
    /*Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=list_account,menu_id,device,device_type,ip_address,language,date_local,customer_id" needLoading:true encrypted:true banking:true favorite:nil];*/
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setHidden:YES];
    [self.lblQR setHidden:YES];
    [self.btnOK setHidden:YES];
    [self.imgQR setHidden:YES];
    [self.vwButton setHidden:YES];
    [self.btnSave setHidden:YES];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        //self.lblTitle.text = @"QR Code Rekening";
        self.lblTitle.text = @"Pilih Nomor Rekening";
    } else {
        //self.lblTitle.text = @"My QR Code";
        self.lblTitle.text = @"Select Account No.";
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    CGFloat sizeQR = 0.0;
    
    if (screenWidth <= screenHeight) {
        sizeQR = 0.75 * screenWidth;
    } else {
        sizeQR = 0.75 * screenHeight;
    }
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmImage = self.imgQR.frame;
    CGRect frmButton = self.btnOK.frame;
    CGRect frmLabel = self.lblQR.frame;
    CGRect frmTable = self.tableView.frame;
    CGRect frmButton2 = self.vwButton.frame;
    CGRect frmBtnShare = self.btnShare.frame;
    
    frmTitle.origin.x = 0;
    frmTitle.origin.y = TOP_NAV;
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 50;
    
    frmTable.origin.y = frmTitle.origin.y + frmTitle.size.height;
    frmTable.origin.x = 0;
    frmTable.size.width = screenWidth;
    frmTable.size.height = screenHeight - (frmTable.origin.y + BOTTOM_NAV);
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.width = screenWidth - 16;
    frmLblTitle.size.height = frmTitle.size.height;
    
    frmButton.size.width = screenWidth - 20;
    frmImage.origin.x = screenWidth/2 - sizeQR/2;
    frmImage.origin.y = frmImage.origin.y - 10;
    frmImage.size.width = sizeQR;
    frmImage.size.height = sizeQR;
    frmLabel.size.width = 200;
    frmLabel.origin.x = screenWidth/2 - frmLabel.size.width/2;
    frmLabel.origin.y = frmImage.origin.y + frmImage.size.height + 10;
    frmTable.size.width = screenWidth;
    frmButton2.origin.x = screenWidth/2 - frmButton2.size.width/2;
    frmButton2.origin.y = frmLabel.origin.y + frmLabel.size.height + 10;
    frmBtnShare.origin.x = 0;
    frmBtnShare.size.width = frmButton2.size.width;
    
    self.vwTitle.frame = frmTitle;
    self.lblTitle.frame = frmLblTitle;
    self.imgQR.frame = frmImage;
    self.btnOK.frame = frmButton;
    self.lblQR.frame = frmLabel;
    self.tableView.frame = frmTable;
    self.vwButton.frame = frmButton2;
    self.btnShare.frame = frmBtnShare;
    
    //amanah styles
    [self.imgTitle setHidden:YES];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnSave setColorSet:PRIMARYCOLORSET];
    [self.btnShare setColorSet:SECONDARYCOLORSET];
    
    // QR Popup custom
    [_vwPopupQR setHidden:YES];
    self.txtPopupQRTitle.text = lang(@"MYQR_TITLE");
    self.txtQRAcquirer.text = lang(@"MYQR_ACQUIRER");
    self.txtQRFootnote.text = lang(@"MYQR_FOOTNOTE");
    [_btnQrOk.layer setBorderWidth:1.0f];
    [_btnQrOk.layer setBorderColor:[UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1].CGColor];
    [_btnQrShare setTitle:lang(@"SHARE") forState:UIControlStateNormal];
    
    // Reload account list
    [self getListAccountFromPersitance:@"" strMenuId:@""];
    
}

- (IBAction)actionOK:(id)sender {
    [super backToR];
}

- (IBAction)actionShare:(id)sender {
    [self prosesShareImage];
}

- (IBAction)actionQrOK:(id)sender {
    [super backToR];
}

- (IBAction)actionQrShare:(id)sender {
    [self prosesShareQRImage];
}

- (IBAction)actionQrClose:(id)sender {
    [UIView animateWithDuration:1.0 animations:^{
        [self.vwPopupQR setHidden:YES];
        [self.view sendSubviewToBack:self->_vwPopupQR];
        if(closeOption == 0){
            [super backToR];
        }
    }];
}

- (IBAction)actionSave:(id)sender {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *fileName = [NSString stringWithFormat:@"%@.png",defaultAccount];
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
    
    // Save image.
    [UIImagePNGRepresentation(_imgQR.image) writeToFile:filePath atomically:YES];
}

-(void)prosesShareQRImage{
    [_btnQrClose setHidden:YES];
    UIGraphicsBeginImageContextWithOptions(_vwContainerQR.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_vwContainerQR.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
    [_btnQrClose setHidden:NO];
}


-(void)prosesShareImage{
    UIGraphicsBeginImageContextWithOptions(_imgQR.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_imgQR.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

-(void)prosesShareImagePopup{
    UIGraphicsBeginImageContextWithOptions(imgShowRootQR.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [imgShowRootQR.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)generateQRImage:(NSString *)encStr {
    NSData *stringData = [encStr dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = _imgQR.frame.size.width / qrImage.extent.size.width;
    float scaleY = _imgQR.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    _imgQR.image = [UIImage imageWithCIImage:qrImage
                                              scale:[UIScreen mainScreen].scale
                                        orientation:UIImageOrientationUp];
}

- (void)generateMyQRImagePopup:(NSString *)encStr {
    NSData *stringData = [encStr dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = _imgMyQR.frame.size.width / qrImage.extent.size.width;
    float scaleY = _imgMyQR.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    _imgMyQR.image = [UIImage imageWithCIImage:qrImage
                                       scale:[UIScreen mainScreen].scale
                                 orientation:UIImageOrientationUp];
}

- (void)generateQRImagePopup:(NSString *)encStr {
    NSData *stringData = [encStr dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = imgShowRootQR.frame.size.width / qrImage.extent.size.width;
    float scaleY = imgShowRootQR.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    imgShowRootQR.image = [UIImage imageWithCIImage:qrImage
                                       scale:[UIScreen mainScreen].scale
                                 orientation:UIImageOrientationUp];
}

- (void)showMyQR {
    _lblQR.text = defaultAccount;
    [self generateQRImage:defaultAccount];
    
    [self.tableView setHidden:YES];
    [self.lblQR setHidden:NO];
    //[self.btnOK setHidden:NO];
    [self.imgQR setHidden:NO];
    [self.vwButton setHidden:NO];
}

- (void)showAccList {
    [self.tableView reloadData];
    [self.tableView setHidden:NO];
    [self.lblQR setHidden:YES];
    //[self.btnOK setHidden:YES];
    [self.imgQR setHidden:YES];
    [self.vwButton setHidden:YES];
}

- (void)popupQR {
    [UIView animateWithDuration:1.0 animations:^{
        [self.vwPopupQR setHidden:NO];
        [self.view bringSubviewToFront:self->_vwPopupQR];
        // Setup data
        self.txtQRAccNo.text = defaultAccount;
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        self.txtQRAccName.text = [[[userDefault objectForKey:@"customer_name"] lowercaseString] capitalizedString];
        [self generateMyQRImagePopup:defaultAccount];
        
    }];
}

- (void)popupQR2 {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat sizeQR = 0.0;
     
    if (width <= height) {
        sizeQR = 0.8 * width;
    } else {
        sizeQR = 0.8 * height;
    }
     
    viewBgRootQR = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    viewBgRootQR.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [viewContinerRootQR setBackgroundColor:[UIColor redColor]];
    viewContinerRootQR = [[UIView alloc]initWithFrame:CGRectMake(width/2 - sizeQR/2, height/2 - (sizeQR+100)/2, sizeQR+10, sizeQR+130)];
    viewContinerRootQR.backgroundColor = [UIColor whiteColor];
    viewContinerRootQR.layer.cornerRadius = 10;
    
    UILabel *titleContinerRootQR = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, viewContinerRootQR.frame.size.width-20, 60)];
    titleContinerRootQR.textAlignment = NSTextAlignmentCenter;
    titleContinerRootQR.numberOfLines = 0;
    titleContinerRootQR.text = [NSString stringWithFormat:@"PT. Bank Syariah Indonesia Tbk \nNo Rekening : %@",defaultAccount];

    imgShowRootQR = [[UIImageView alloc] initWithFrame:CGRectMake(5, titleContinerRootQR.frame.size.height, sizeQR, sizeQR)];
    [self generateQRImagePopup:defaultAccount];
    
    imgShowRootQR = [[UIImageView alloc] initWithFrame:CGRectMake(5, titleContinerRootQR.frame.size.height, sizeQR, sizeQR)];
    [self generateQRImagePopup:defaultAccount];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];

    CustomBtn *btnOKConfirm = [[CustomBtn alloc]initWithFrame:CGRectMake(10, imgShowRootQR.frame.size.height+70, viewContinerRootQR.frame.size.width/2 - 15, 40)];

    [btnOKConfirm setTitle:@"OK" forState:UIControlStateNormal];
    [btnOKConfirm addTarget:self action:@selector(actionOke) forControlEvents:UIControlEventTouchUpInside];


    CustomBtn *btnBagikan = [[CustomBtn alloc]initWithFrame:CGRectMake(viewContinerRootQR.frame.size.width/2 + 5, imgShowRootQR.frame.size.height+70, viewContinerRootQR.frame.size.width/2 - 15, 40)];
    
    if([lang isEqualToString:@"id"]){
        [btnBagikan setTitle:@"Bagikan" forState:UIControlStateNormal];
    }else{
        [btnBagikan setTitle:@"Share" forState:UIControlStateNormal];
    }
    
    [btnBagikan addTarget:self action:@selector(actionBagikan) forControlEvents:UIControlEventTouchUpInside];
    
    [btnOKConfirm setColorSet:SECONDARYCOLORSET];
    [btnBagikan setColorSet:PRIMARYCOLORSET];
    
    [viewContinerRootQR addSubview:titleContinerRootQR];
    [viewContinerRootQR addSubview:btnOKConfirm];
    [viewContinerRootQR addSubview:btnBagikan];
    [viewContinerRootQR addSubview:imgShowRootQR];
    [viewBgRootQR addSubview:viewContinerRootQR];
    [self.view addSubview:viewBgRootQR];
}

- (void)actionOke {
    [viewBgRootQR removeFromSuperview];
}

- (void)actionBagikan {
    [self prosesShareImagePopup];
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV01Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
//    NSString *selAcc = [data objectForKey:@"id_account"];
    NSString *selAcc = @"";
    if([data objectForKey:@"altname"] != nil && [data objectForKey:@"type"] != nil){
        if(![[data valueForKey:@"altname"] isEqualToString:@""]){
            selAcc = [NSString stringWithFormat:@"%@ - %@ - %@",[data valueForKey:@"name"], [data valueForKey:@"type"], [data valueForKey:@"altname"]];
        }else if(![[data valueForKey:@"type"] isEqualToString:@""]){
            selAcc = [NSString stringWithFormat:@"%@ - %@",[data valueForKey:@"name"], [data valueForKey:@"type"]];
        }else{
            selAcc = [NSString stringWithFormat:@"%@",[data valueForKey:@"name"]];
        }
    }
    else{
        selAcc = [NSString stringWithFormat:@"%@",[data valueForKey:@"name"]];
    }
    [label setText:selAcc];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    
    UIView *line = (UIView *)[cell viewWithTag:2];
    CGRect frmLine = line.frame;
    frmLine.size.width = width - 20;
    line.frame = frmLine;
    
    /*if (indexPath.row == 0) {
        [userDefault setObject:label.text forKey:@"valNoRek"];
        [userDefault synchronize];
    }*/
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    NSString *text = [[listData objectAtIndex:indexPath.row] objectForKey:@"id_account"];
    NSDictionary *temp = [listData objectAtIndex:indexPath.row];
    NSString *titl = @"";
    
       if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
           if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
               titl = [NSString stringWithFormat:@"%@ - %@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"], [temp valueForKey:@"altname"]];
           }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
               titl = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
           }else{
               titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
           }
       }
       else{
           titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
       }
    
    NSString *text = titl;
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+20.0;
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    defaultAccount = [[listData objectAtIndex:indexPath.row] objectForKey:@"id_account"];
    [self popupQR];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
//    if([requestType isEqualToString:@"list_account1"]){
    if([requestType isEqualToString:@"list_account2"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            [self getListAccountFromPersitance:@"" strMenuId:@""];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
    }
    
    if (![requestType isEqualToString:@"check_notif"]) {
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        
    } else {
        if([requestType isEqualToString:@"list_account"]){
            listData = (NSArray *)jsonObject;
            //[userDefault setObject:listData forKey:@"myqrlist"];
            //[userDefault synchronize];
            NSUInteger jmlData = [listData count];
            int idx = 0;
            for(NSDictionary *temp in listData){
                if (idx == 0) {
                    defaultAccount = [temp valueForKey:@"id_account"];
                }
                idx = idx + 1;
            }

            if (jmlData == 1) {
                closeOption = 0;
                [self popupQR];
//                [self showMyQR];
            } else if (jmlData > 1) {
                closeOption = 1;
                [self showAccList];
            }
        }
    }
        
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [BSMDelegate reloadApp];
}

-(void) getListAccountFromPersitance : (NSString *) urlParam
                       strMenuId : (NSString *) menuId{

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct = nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
        }
    }
    
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];
            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct.count != 0 || listAcct != nil) {
        listData = (NSArray *)listAcct;
        //[userDefault setObject:listData forKey:@"myqrlist"];
        //[userDefault synchronize];
        NSUInteger jmlData = [listData count];
        int idx = 0;
        for(NSDictionary *temp in listData){
            if (idx == 0) {
                defaultAccount = [temp valueForKey:@"id_account"];
            }
            idx = idx + 1;
        }

        if (jmlData == 1) {
//            [self showMyQR];
            closeOption = 0;
            [self popupQR];
        } else if (jmlData > 1) {
            closeOption = 1;
            [self showAccList];
        }
    }else{
//        Connection *conn = [[Connection alloc]initWithDelegate:self];
//        [conn sendPostParmUrl:@"request_type=list_account,menu_id,device,device_type,ip_address,language,date_local,customer_id" needLoading:true encrypted:true banking:true favorite:false];
//        NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account1,customer_id"];
        NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
    }
    
}


@end
