//
//  ShalatGPSViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/20/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "ShalatGPSViewController.h"
#import "PrayerTime.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>

@interface ShalatHelper2: NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) int value;
+ (instancetype)create:(NSString *)title andValue:(int)value;
@end

@implementation ShalatHelper2

+ (instancetype)create:(NSString *)title andValue:(int)value{
    ShalatHelper2 *shalatHelper = [[ShalatHelper2 alloc]init];
    [shalatHelper setValue:value];
    [shalatHelper setTitle:title];
    return shalatHelper;
}

@end

@interface ShalatGPSViewController ()<UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate> {
    NSArray *listData;
    NSMutableArray *listNames;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;
    NSDate *curDate;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwSchedule;
@property (weak, nonatomic) IBOutlet UILabel *lblSchedule;
@property (weak, nonatomic) IBOutlet UITableView *tblShalat;
@property (weak, nonatomic) IBOutlet UILabel *labelDay;

@end

@implementation ShalatGPSViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    curDate = [NSDate date];
    [self currentLocationIdentifier];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *currentDate = [dateFormat stringFromDate:date];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Waktu Sholat";
        self.lblSchedule.text = @"Waktu Sholat";
    } else {
        self.lblTitle.text = @"Sholat Time";
        self.lblSchedule.text = @"Sholat Time";
    }
    
    self.tblShalat.dataSource = self;
    self.tblShalat.delegate = self;
    [self.labelDay setText:currentDate];
    //[self.tblShalat reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)currentLocationIdentifier
{
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else{
        
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"Continue",nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert setDelegate:self];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    
    //lat = -6.183712;
    //lng = 106.823641;
    
    PrayTime *prayerTime = [[PrayTime alloc] initWithJuristic:JuristicMethodShafii
                                               andCalculation:CalculationMethodMWL];
    [prayerTime setTimeFormat:TimeFormat12Hour];
    listNames = [prayerTime getTimeNames];
    
    NSMutableArray *times2 = [prayerTime prayerTimesDate:curDate
                                                latitude:lat
                                               longitude:lng
                                             andTimezone:[prayerTime getTimeZone]];
    listData = times2;
    NSLog(@"%@", times2);
    [self.tblShalat reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SholatGPSCell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    UILabel *labelTime = (UILabel *)[cell viewWithTag:2];
    
    label.text = [listNames objectAtIndex:indexPath.row];
    labelTime.text = [listData objectAtIndex:indexPath.row];
    
    return cell;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
