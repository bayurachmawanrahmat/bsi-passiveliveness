//
//  PopupListViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PopupListViewController : UIViewController{
    id delegate;
}
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *searchImg;
@property (weak, nonatomic) IBOutlet UITableView *listView;

- (void) setDelegate:(id)newDelagate;
- (void) setList:(NSArray*)list;
- (void) setPlaceholder: (NSString*)placeholder;

@end

@protocol PopupListViewDelegate

@required

- (void) selectedRow : (NSInteger) indexRow withList:(NSArray*)arrData;

@end

NS_ASSUME_NONNULL_END
