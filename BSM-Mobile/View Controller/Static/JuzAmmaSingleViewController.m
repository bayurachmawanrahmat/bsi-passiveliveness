//
//  JuzAmmaSingleViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 9/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "JuzAmmaSingleViewController.h"
#import "Utility.h"
#import "UIView+SimpleRipple.h"
#import "QuranHelper.h"
#import "CellJuzAmmaSingle.h"
#import <QuartzCore/QuartzCore.h>

@interface JuzAmmaSingleViewController ()<UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate>{
    NSString *lang;
    NSUserDefaults *userDefault;
    
    NSDictionary *dataBundle;
    NSArray *arDataAyat;
    QuranHelper *mQuranHelper;
    
    CGFloat sizeFont;
    
    NSMutableArray *arTextFont;

    NSInteger rowSelected;
    UIToolbar *toolBar;
    UIPickerView *objPickerView;
    NSString *textToolbar;
    
    bool isPickerView;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIView *vwHeaderArab;
@property (weak, nonatomic) IBOutlet UILabel *lblBacaanAwal;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UITableView *tblSurah;
@property (weak, nonatomic) IBOutlet UIView *vwDropDown;
@property (weak, nonatomic) IBOutlet UILabel *lblUkuran;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropDown;
@property (weak, nonatomic) IBOutlet UIButton *btnDropDown;

@end

@implementation JuzAmmaSingleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    arDataAyat = [[NSArray alloc]init];
    mQuranHelper = [[QuranHelper alloc]init];
    arTextFont = [[NSMutableArray alloc]init];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *strTitleUkuran;
    
    if ([lang isEqualToString:@"id"]) {
        textToolbar= @"Selesai";
        strTitleUkuran = @"Ukuran Huruf";
    }else{
        textToolbar = @"Done";
        strTitleUkuran = @"Font Size";
    }
    
    if ([userDefault valueForKey:@"current_size_font"]) {
        sizeFont = [[[userDefault valueForKey:@"current_size_font"]stringByReplacingOccurrencesOfString:@"px" withString:@""]floatValue];
        strTitleUkuran = [userDefault valueForKey:@"current_size_font"];
    }else{
        sizeFont = 16.0f;
    }
    
    isPickerView = false;
    
    NSString *strBismillah = @"بِسْمِ اللّٰهِ الرَّحْمٰنِ الرَّحِيْمِ";
    [arTextFont addObject:@"Ukuran Huruf"];
    [arTextFont addObject:@"12px"];
    [arTextFont addObject:@"16px"];
    [arTextFont addObject:@"18px"];
    [arTextFont addObject:@"20px"];
    [arTextFont addObject:@"24px"];
    [arTextFont addObject:@"28px"];
    
    [self.lblBacaanAwal setText:strBismillah];
    [self.lblBacaanAwal setFont:[UIFont fontWithName:const_font_name3 size:sizeFont]];
    [self.lblBacaanAwal sizeToFit];
    
    
    if (dataBundle != nil) {
        [self.lblTitle setText:[dataBundle valueForKey:@"surah_name"]];
        [self.lblTitle sizeToFit];
        arDataAyat = [mQuranHelper readListAyat:[dataBundle valueForKey:@"id"]];
        
    }
    
    self.tblSurah.delegate = self;
    self.tblSurah.dataSource = self;
    [self.tblSurah setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.tblSurah beginUpdates];
    [self.tblSurah endUpdates];
    
//    [self.lblUkuran setText:strTitleUkuran];
    [self.lblUkuran setText:@""];
    [self.lblUkuran setFont:[UIFont fontWithName:const_font_name1 size:15]];
    [self.lblUkuran sizeToFit];
    
    [self setupLayout];
    
    [self.btnBack addTarget:self action:@selector(actionSelectedTochDown: event:) forControlEvents:UIControlEventTouchDown];
    [self.btnDropDown addTarget:self action:@selector(actionSelectedTochDown: event:) forControlEvents:UIControlEventTouchDown];
    
    [self.btnBack addTarget:self action:@selector(actionBackNav:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnDropDown addTarget:self action:@selector(actionOpenUkuran:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.vwHeaderArab setBackgroundColor:UIColorFromRGB(const_highlight_primary)];
}

-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmVwHeaderArab = self.vwHeaderArab.frame;
    CGRect frmVwLine = self.vwLine.frame;
    CGRect frmTblSurah = self.tblSurah.frame;
    
    CGRect frmBtnBack = self.btnBack.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    CGRect frmLblBacaanAwal = self.lblBacaanAwal.frame;
    
    CGRect frmVwDropDown = self.vwDropDown.frame;
    CGRect frmLblDropDown = self.lblUkuran.frame;
    CGRect frmImgDropDown = self.imgDropDown.frame;
    CGRect frmBtnDropDown = self.btnDropDown.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmVwHeaderArab.origin.x = 0;
    frmVwHeaderArab.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmVwHeaderArab.size.width = SCREEN_WIDTH;
    
    frmLblBacaanAwal.origin.x = 0;
    frmLblBacaanAwal.origin.y = 16;
    frmLblBacaanAwal.size.width = frmVwHeaderArab.size.width;
    
    frmVwHeaderArab.size.height = frmLblBacaanAwal.size.height + (frmLblBacaanAwal.origin.y *2);
    
    frmVwLine.origin.x = 0;
    frmVwLine.origin.y = frmVwHeaderArab.origin.y + frmVwHeaderArab.size.height;
    frmVwLine.size.width = SCREEN_WIDTH;
    
    frmTblSurah.origin.x = 0;
    frmTblSurah.size.width = SCREEN_WIDTH;
    if ([[dataBundle valueForKey:@"id"] intValue] == 1) {
        [self.vwHeaderArab setHidden:YES];
        [self.vwLine setHidden:YES];
        frmTblSurah.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    }else{
        [self.vwHeaderArab setHidden:NO];
        [self.vwLine setHidden:NO];
        frmTblSurah.origin.y = frmVwLine.origin.y + frmVwLine.size.height;
    }
    frmTblSurah.size.height = SCREEN_HEIGHT - frmTblSurah.origin.y - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveBottomPadding]) {
        frmTblSurah.size.height = SCREEN_HEIGHT - frmTblSurah.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    frmBtnBack.origin.x = 0;
    frmBtnBack.origin.y = 0;
    frmBtnBack.size.height = frmVwTitle.size.height;
    
    frmVwDropDown.origin.y = 0;
    frmVwDropDown.size.height = frmVwTitle.size.height;
    
    frmLblDropDown.origin.x = 0;
    frmLblDropDown.origin.y = frmVwDropDown.size.height/2 - frmLblDropDown.size.height/2;
    
    frmImgDropDown.origin.x = frmLblDropDown.origin.x + frmLblDropDown.size.width + 8;
    frmImgDropDown.size.height = 35;
    frmImgDropDown.origin.y = frmVwDropDown.size.height/2 - frmImgDropDown.size.height/2;
    frmImgDropDown.size.width = 35;
    
    frmVwDropDown.size.width = frmImgDropDown.origin.x + frmImgDropDown.size.width + 8;
    frmVwDropDown.origin.x = frmVwTitle.size.width - frmVwDropDown.size.width - 8;
    
    frmBtnDropDown.origin.x = 0;
    frmBtnDropDown.origin.y = 0;
    frmBtnDropDown.size.width = frmVwDropDown.size.width;
    frmBtnDropDown.size.height = frmVwDropDown.size.height;
    
    frmLblTitle.origin.x = frmBtnBack.origin.x + frmBtnBack.size.width;
    frmLblTitle.origin.y = frmVwTitle.size.height/2 - frmLblTitle.size.height/2;
    frmLblTitle.size.width = frmVwDropDown.origin.x - frmBtnBack.size.width - 8;
    
    self.vwTitle.frame = frmVwTitle;
    self.vwHeaderArab.frame = frmVwHeaderArab;
    self.vwLine.frame = frmVwLine;
    self.tblSurah.frame = frmTblSurah;
    
    self.btnBack.frame = frmBtnBack;
    self.lblTitle.frame = frmLblTitle;
    self.vwDropDown.frame = frmVwDropDown;
    self.btnDropDown.frame = frmBtnDropDown;
    
    self.lblUkuran.frame = frmLblDropDown;
    self.imgDropDown.frame = frmImgDropDown;
    
    self.lblBacaanAwal.frame = frmLblBacaanAwal;
    
}

- (void) sendData : (NSDictionary *) data{
    dataBundle = data;
}

-(void)actionSelectedTochDown : (UIButton *) sender
                        event : (UIEvent *) event{
   UITouch *touch = [[event touchesForView:sender] anyObject];
    CGPoint origin = [touch locationInView:sender];
    float radius = sender.frame.size.height;
    float duration = 0.5f;
    float fadeAfter = duration * 0.75f;
    
    [sender rippleStartingAt:origin withColor:[UIColor colorWithWhite:0.0f alpha:0.20f] duration:duration radius:radius fadeAfter:fadeAfter];
    
}

-(void)actionBackNav : (UIButton *)sender{
    UINavigationController *navigationController = self.navigationController;
    [navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arDataAyat.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellJuzAmmaSingle *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if(cell == nil){
        cell = [[CellJuzAmmaSingle alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSDictionary *dataAyat = [arDataAyat objectAtIndex:indexPath.row];
    [cell.lblNoAyat setText:[dataAyat valueForKey:@"ayah"]];
    [cell.lblNoAyat sizeToFit];
    
    [cell.lblAyat setText:[dataAyat valueForKey:@"arabic"]];
    [cell.lblAyat setFont:[UIFont fontWithName:const_font_name3 size:sizeFont]];
    cell.lblAyat.attributedText = [Utility setLineSpasing:[dataAyat valueForKey:@"arabic"] mSpaseSize:10];
    
    [cell.lblTranslater setText:[dataAyat valueForKey:@"translate"]];
    [cell.lblTranslater setFont:[UIFont fontWithName:const_font_name1 size:sizeFont]];
    
    
    [cell.lblAyat sizeToFit];
    cell.lblAyat.lineBreakMode = NSLineBreakByWordWrapping;
    cell.lblAyat.numberOfLines = 0;
    
    
    [cell.lblTranslater sizeToFit];
    cell.lblTranslater.lineBreakMode = NSLineBreakByWordWrapping;
    cell.lblTranslater.numberOfLines = 0;
    
    
    CGRect frmImgBorder = cell.imgBorder.frame;
    CGRect frmLblNoAyat = cell.lblNoAyat.frame;
    CGRect frmLblAyat = cell.lblAyat.frame;
    CGRect frmTranslater = cell.lblTranslater.frame;
    
    frmImgBorder.origin.x = 8;
    frmImgBorder.origin.y = 8;
    
    frmLblNoAyat.origin.x = (frmImgBorder.origin.x + frmImgBorder.size.width/2) - frmLblNoAyat.size.width/2;
    frmLblNoAyat.origin.y = (frmImgBorder.origin.y + frmImgBorder.size.height/2) - frmLblNoAyat.size.height/2;
    
    frmLblAyat.origin.y = frmImgBorder.origin.y + 8;
    frmLblAyat.origin.x = frmImgBorder.origin.x + frmImgBorder.size.width + 5;
    frmLblAyat.size.width = cell.frame.size.width - frmLblAyat.origin.x - 16;
    
    frmTranslater.origin.y = (frmLblAyat.origin.y *2) + frmLblAyat.size.height;
    frmTranslater.origin.x = frmLblAyat.origin.x;
    frmTranslater.size.width = cell.frame.size.width - frmTranslater.origin.x - 16;
    
    cell.imgBorder.frame = frmImgBorder;
    cell.lblNoAyat.frame = frmLblNoAyat;
    cell.lblAyat.frame = frmLblAyat;
    cell.lblTranslater.frame = frmTranslater;
    
    if (indexPath.row % 2 == 0) {
        [cell setBackgroundColor:UIColorFromRGB(const_color_basecolor)];
    }else{
        [cell setBackgroundColor:[UIColor whiteColor]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dataDict = [arDataAyat objectAtIndex:indexPath.row];
    CGFloat paddingTop = 16;
    CGFloat paddingLeft = 8 + 63 + 5;
    CGFloat paddingRight = 16;
    CGFloat paddingBottom = 16;
    CGFloat mScreenWidth = SCREEN_WIDTH - paddingLeft - paddingRight;
    
    
    CGSize mSzTextArab = [[dataDict valueForKey:@"arabic"] sizeWithAttributes:
                               @{NSFontAttributeName: [UIFont fontWithName:const_font_name3 size:sizeFont]}];
    CGSize mSzTextTranslater = [[dataDict valueForKey:@"translate"] sizeWithAttributes:
        @{NSFontAttributeName: [UIFont fontWithName:const_font_name1 size:sizeFont]}];
    
    CGFloat constrainHeight = 2000 + mSzTextArab.height +mSzTextTranslater.height;
    
    CGSize constraint = CGSizeMake(mScreenWidth,constrainHeight);

    CGSize sizeTextArab = [[dataDict valueForKey:@"arabic"] sizeWithFont:[UIFont fontWithName:const_font_name3 size:sizeFont] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize sizeTextTranslater = [[dataDict valueForKey:@"translate"] sizeWithFont:[UIFont fontWithName:const_font_name1 size:sizeFont] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    
    CGFloat mTotalHeight = paddingTop + ceil(sizeTextArab.height) + 10 + paddingBottom + ceil(sizeTextTranslater.height) + paddingBottom + 5;
    
    CGFloat mHeight = MAX(mTotalHeight, 50);
    
    return mHeight;
}

-(void)changeTextFont : (id) sender{
    [toolBar removeFromSuperview];
    [objPickerView removeFromSuperview];
    isPickerView = false;
    if (rowSelected > 0) {
        sizeFont = [[[arTextFont objectAtIndex:rowSelected]stringByReplacingOccurrencesOfString:@"px" withString:@""]floatValue];
        [self changeSizeText];
        [userDefault setValue:[arTextFont objectAtIndex:rowSelected] forKey:@"current_size_font"];
        [userDefault synchronize];
    }
    
    
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arTextFont.count;
}
- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arTextFont objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    [self.lblUkuran setText:[arTextFont objectAtIndex:row]];
    rowSelected = row;
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}

-(void)changeSizeText {
    [self.lblBacaanAwal setFont:[UIFont fontWithName:const_font_name3 size:sizeFont]];
    self.lblBacaanAwal.numberOfLines = 0;
    [self.lblBacaanAwal sizeToFit];
    self.lblBacaanAwal.lineBreakMode = NSLineBreakByWordWrapping;
    
    CGRect frmVwHeader = self.vwHeaderArab.frame;
    CGRect frmLblBacaanAwal = self.lblBacaanAwal.frame;
    CGRect frmVwLine = self.vwLine.frame;
    CGRect frmTblSurah = self.tblSurah.frame;
    
    frmLblBacaanAwal.origin.x = 0;
    frmLblBacaanAwal.size.width = frmVwHeader.size.width;
    
    frmVwHeader.size.height = (frmLblBacaanAwal.origin.y * 2) + frmLblBacaanAwal.size.height;
    
    frmVwLine.origin.y = frmVwHeader.origin.y + frmVwHeader.size.height;
    if ([[dataBundle valueForKey:@"id"] intValue] == 1){
        frmTblSurah.origin.y = self.vwTitle.frame.origin.y + self.vwTitle.frame.size.height;
    }else{
        frmTblSurah.origin.y = frmVwLine.origin.y + frmVwLine.size.height;
    }
    
    frmTblSurah.size.height = SCREEN_HEIGHT - frmTblSurah.origin.y - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveBottomPadding]) {
        frmTblSurah.size.height = SCREEN_HEIGHT - frmTblSurah.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    self.vwHeaderArab.frame = frmVwHeader;
    self.lblBacaanAwal.frame = frmLblBacaanAwal;
    self.vwLine.frame = frmVwLine;
    self.tblSurah.frame = frmTblSurah;
    
    [self.tblSurah reloadData];
    [self.tblSurah beginUpdates];
    [self.tblSurah endUpdates];
}

-(void)actionOpenUkuran : (UIButton *) sender{
    if (!isPickerView) {
        toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,44)];
        [toolBar setBarStyle:UIBarStyleDefault];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:textToolbar
                                                                          style:UIBarButtonItemStyleDone target:self action:@selector(changeTextFont:)];
        toolBar.items = @[barButtonDone];
        barButtonDone.tintColor=[UIColor blackColor];
        
        objPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        objPickerView.showsSelectionIndicator = YES;
        [objPickerView setBackgroundColor:UIColorFromRGB(const_color_gray)];
        objPickerView.delegate = self; // Also, can be done from IB, if you're using
        objPickerView.dataSource = self;
        
        
        CGRect frmObjPicker = objPickerView.frame;
        CGRect frmToolbar = toolBar.frame;
        
        frmObjPicker.origin.y = SCREEN_HEIGHT - frmObjPicker.size.height - BOTTOM_NAV_DEF;
        
        if ([Utility isDeviceHaveBottomPadding]) {
             frmObjPicker.origin.y = SCREEN_HEIGHT - frmObjPicker.size.height - BOTTOM_NAV_NOTCH;
        }
        
        frmToolbar.origin.y = frmObjPicker.origin.y - frmToolbar.size.height;
        
        objPickerView.frame = frmObjPicker;
        toolBar.frame = frmToolbar;
        
        [self.view addSubview:objPickerView];
        [self.view addSubview:toolBar];
        isPickerView = true;
//        [self handleTimer:@"START_TIMER"];
        NSDictionary* userInfo = @{@"position": @(1112)};
           NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
           [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void)handleTimer : (NSString *) modeStr{
    NSDictionary* userInfo = @{@"actionTimer": modeStr};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

@end
