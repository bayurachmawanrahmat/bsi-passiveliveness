//
//  BottomModalViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 26/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "BottomModalViewController.h"
#import "Utility.h"

NS_ENUM(NSInteger, PANGESTUR){
    PANFULL = 0,
    PANHALF = 1,
    PANDISMISS = 2
};

@interface BottomModalViewController ()<UIAlertViewDelegate>{
    NSInteger panFlag;
    NSDictionary *mBundle;
    NSUserDefaults *userDefault;
    NSString *lang;
}
@property (weak, nonatomic) IBOutlet UIView *vwMainScreen;
@property (weak, nonatomic) IBOutlet UIView *vwContentScreen;
@property (weak, nonatomic) IBOutlet UIView *vwContentContainer;
@property (weak, nonatomic) IBOutlet UIView *vwLineGrab;

@end

@implementation BottomModalViewController

- (void)loadView{
    [super loadView];
    CGRect frmVwMainScreen =self.vwMainScreen.frame;
    CGRect frmVwContentScreen = self.vwContentScreen.frame;
    CGRect frmVwLineGrab = self.vwLineGrab.frame;
    CGRect frmVwContentContainer = self.vwContentContainer.frame;
    
    CGFloat fullPostY = TOP_NAV/3;
    
    frmVwMainScreen.origin.x = 0;
    frmVwMainScreen.origin.y = 0;
    frmVwMainScreen.size.width = SCREEN_WIDTH;
    frmVwMainScreen.size.height = SCREEN_HEIGHT;
    
    frmVwContentScreen.origin.x = 0;
    frmVwContentScreen.origin.y = SCREEN_HEIGHT/2;
    frmVwContentScreen.size.height = SCREEN_HEIGHT - fullPostY;
    frmVwContentScreen.size.width = SCREEN_WIDTH;
//    if (IPHONE_X || IPHONE_XS_MAX) {
//        frmVwContentScreen.size.height = SCREEN_HEIGHT - fullPostY - BOTTOM_NAV_NOTCH + 16;
//    }
    
    frmVwLineGrab.origin.y = 16;
    frmVwLineGrab.size.width = frmVwContentScreen.size.width/9;
    frmVwLineGrab.origin.x = frmVwContentScreen.size.width/2 - frmVwLineGrab.size.width/2;
    
    frmVwContentContainer.origin.x = 0;
    frmVwContentContainer.origin.y = frmVwLineGrab.origin.y + frmVwLineGrab.size.height + 16;
    frmVwContentContainer.size.width = frmVwContentScreen.size.width;
    frmVwContentContainer.size.height = frmVwContentScreen.size.height - frmVwContentContainer.origin.y;
    
    self.vwMainScreen.frame = frmVwMainScreen;
    self.vwContentScreen.frame = frmVwContentScreen;
    self.vwLineGrab.frame = frmVwLineGrab;
    self.vwContentContainer.frame = frmVwContentContainer;
    
    self.vwContentScreen.layer.mask = [Utility makeARoundedCornerTopLeftAndRight:self.vwContentScreen];
    self.vwMainScreen.backgroundColor = [[UIColor grayColor]colorWithAlphaComponent:0.15];
    self.vwLineGrab.layer.cornerRadius = 3;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    panFlag = 1;
    UIPanGestureRecognizer *panGestur = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(actionSwipe:)];
    panGestur.minimumNumberOfTouches = 1;
    panGestur.maximumNumberOfTouches = 1;
    [self.vwContentScreen addGestureRecognizer:panGestur];
    
    if (mBundle != nil) {
        NSString *strType = [mBundle objectForKey:@"bottom_type"];
        if ([strType isEqualToString:@"detail_scheduler"]) {
            [self.vwContentContainer addSubview:[self vwDetailScheduler:[mBundle objectForKey:@"data"]]];
        }
    }
}

-(void) actionSwipe: (UIPanGestureRecognizer *) sender{
    [Utility resetTimer];
    CGPoint translatedPoint = [sender translationInView:sender.view.superview];
//    NSLog(@"translate point : %f,%f", translatedPoint.x, translatedPoint.y);
    
    
//    if (sender.state == UIGestureRecognizerStateBegan) {
//        [sender.view setFrame:CGRectMake(0, translatedPoint.y, SCREEN_WIDTH, self.vwContentScreen.frame.size.height)];
//    }
    
    
    CGFloat halfPostY = SCREEN_HEIGHT/2;
    CGFloat fullPostY = TOP_NAV/3;
    CGFloat dismissPostY = halfPostY - BOTTOM_NAV_DEF - TOP_NAV;
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveNotch]) {
        dismissPostY = halfPostY - BOTTOM_NAV_NOTCH - TOP_NAV;
    }
    
    if (sender.state == UIGestureRecognizerStateChanged) {
        CGRect frmVwContentScreen = self.vwContentScreen.frame;
        frmVwContentScreen.origin.x = 0;
        frmVwContentScreen.size.width = SCREEN_WIDTH;
        
        if (translatedPoint.y > fullPostY) {
            panFlag = PANHALF;
            if (translatedPoint.y > dismissPostY - 16) {
                panFlag = PANDISMISS;
            }
        }
        else if (translatedPoint.y < halfPostY) {
            panFlag = PANFULL;
        }
        
        switch (panFlag) {
            case PANFULL:
                frmVwContentScreen.origin.y = fullPostY;
                break;
            case PANHALF:
                frmVwContentScreen.origin.y = halfPostY;
                break;
            case PANDISMISS:
                [self dismissViewControllerAnimated:YES completion:nil];
                break;
            default:
                break;
        }
        
        [UIView transitionWithView:sender.view duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
            
            self.vwContentScreen.frame = frmVwContentScreen;
            sender.view.frame = frmVwContentScreen;
        }completion:nil];
    }
}

-(void) setBundleExtras : (NSDictionary *) dataSet{
    mBundle = dataSet;
}


#pragma mark view detail scheduler
-(UIView *) vwDetailScheduler : (NSDictionary *) dataBundle{
    UIView *vwScheduler = [[UIView alloc]init];
    
    
//--------init view for display---------
    
    UIScrollView *vwScroll = [[UIScrollView alloc]init];
    UIView *vwContentScroll = [[UIView alloc]init];
    vwContentScroll.backgroundColor = [UIColor lightGrayColor];
    
    
    //data transfer
    UIView *vwDataTransfer = [[UIView alloc]init];
    UILabel *lblTitleTransfer = [[UILabel alloc]init];
    UILabel *lblTtlTrfId = [[UILabel alloc]init];
    UILabel *lblTtlTrfName = [[UILabel alloc]init];
    UILabel *lblTtlTrfSrc = [[UILabel alloc]init];
    
    //data taranskasi
    UIView *vwDataTransact = [[UIView alloc] init];
    UILabel *lblTtlTrnsctNext = [[UILabel alloc]init];
    UILabel *lblValTrnsctNext = [[UILabel alloc]init];
    UILabel *lblTtlTrnsctStart = [[UILabel alloc]init];
    UILabel *lblValTrnsctStart = [[UILabel alloc]init];
    UILabel *lblTtlTrnsctEnd = [[UILabel alloc]init];
    UILabel *lblValTrnsctEnd = [[UILabel alloc]init];
    
    //data rekening tujuan
    UIView *vwRekningTujuan = [[UIView alloc]init];
    UILabel *lblTtlRekTujuan = [[UILabel alloc]init];
    UIView *vwNoRek = [self addingViewRemarkAble:1];
    UIView *vwNominal = [self addingViewRemarkAble:2];
    UIView *vwCostTransct = [self addingViewRemarkAble:2];
    UIView *vwFrekeuensi = [self addingViewRemarkAble:2];
    
    
    //view button
    UIView *vwButton = [[UIView alloc]init];
    UIButton *btnHapus = [[UIButton alloc] init];
    
    
    
//--------end view for display---------
    
// --------init property----------------
    //data transfer property
    vwDataTransfer.backgroundColor = [UIColor whiteColor];
    lblTitleTransfer = [self lblTitleGray:lblTitleTransfer];
    lblTtlTrfId = [self lblValGren:lblTtlTrfId];
    lblTtlTrfName = [self lblTtlStyle:lblTtlTrfName mFont:[UIFont fontWithName:const_font_name3 size:15]];
    lblTtlTrfSrc = [self lblTitleGray:lblTtlTrfSrc];
    
    vwDataTransact.backgroundColor = [UIColor whiteColor];
    lblTtlTrnsctNext = [self lblTtlStyle:lblTtlTrnsctNext mFont:[UIFont fontWithName:const_font_name3 size:15]];
    lblValTrnsctNext = [self lblValGren:lblValTrnsctNext];
    lblTtlTrnsctStart = [self lblTtlStyle:lblTtlTrnsctStart mFont:[UIFont fontWithName:const_font_name3 size:15]];
    lblValTrnsctStart = [self lblTitleGray:lblValTrnsctStart];
    lblTtlTrnsctEnd = [self lblTtlStyle:lblTtlTrnsctEnd mFont:[UIFont fontWithName:const_font_name3 size:15]];
    lblValTrnsctEnd = [self lblTitleGray:lblValTrnsctEnd];
    
    vwRekningTujuan.backgroundColor = [UIColor whiteColor];
    lblTtlRekTujuan = [self lblTitleGray:lblTtlRekTujuan];
    
    if (IPHONE_5) {
        lblTtlTrfName = [self lblTtlStyle:lblTtlTrfName mFont:[UIFont fontWithName:const_font_name3 size:11]];
        lblTtlTrnsctNext = [self lblTtlStyle:lblTtlTrnsctNext mFont:[UIFont fontWithName:const_font_name3 size:11]];
        lblTtlTrnsctStart = [self lblTtlStyle:lblTtlTrnsctStart mFont:[UIFont fontWithName:const_font_name3 size:11]];
        lblTtlTrnsctEnd = [self lblTtlStyle:lblTtlTrnsctEnd mFont:[UIFont fontWithName:const_font_name3 size:11]];
    }
    
    UILabel *lblTtlNorek = (UILabel *) [vwNoRek.subviews objectAtIndex:0];
    UILabel *lblValNorek = (UILabel *) [vwNoRek.subviews objectAtIndex:1];
    UILabel *lblValOther = (UILabel *) [vwNoRek.subviews objectAtIndex:2];
    
    UILabel *lblTtlNominal = (UILabel *) [vwNominal.subviews objectAtIndex:0];
    UILabel *lblValNominal = (UILabel *) [vwNominal.subviews objectAtIndex:1];
    
    UILabel *lblTtlCost = (UILabel *) [vwCostTransct.subviews objectAtIndex:0];
    UILabel *lblValCost = (UILabel *) [vwCostTransct.subviews objectAtIndex:1];
    
    UILabel *lblTtlFrekuensi = (UILabel *) [vwFrekeuensi.subviews objectAtIndex:0];
    UILabel *lblValFrekuensi = (UILabel *) [vwFrekeuensi.subviews objectAtIndex:1];
    
    vwButton.backgroundColor = [UIColor whiteColor];
    
    btnHapus.backgroundColor = UIColorFromRGB(const_color_red);
//    btnHapus.layer.borderWidth = 3;
////    btnHapus.layer.borderColor = [[UIColor yellowColor]CGColor];
    btnHapus.layer.cornerRadius = 20;
    btnHapus.tag = 1;
    [btnHapus addTarget:self action:@selector(actionDeleteScheduler:) forControlEvents:UIControlEventTouchUpInside];
    
    
// --------end property----------------
    
// --------- init value in text----------------
    
    if ([lang isEqualToString:@"id"]) {
        lblTitleTransfer.text = [@"transfer dari" uppercaseString];
        lblTtlTrnsctNext.text = @"Transaksi Berikutnya";
        lblTtlTrnsctStart.text = @"Tanggal Mulai";
        lblTtlTrnsctEnd.text = @"Tanggal Berakhir";
        lblTtlRekTujuan.text = [@"rekening tujuan" uppercaseString];
        lblTtlNorek.text = @"Nomor Rekening";
        lblTtlNominal.text = @"Nominal";
        lblTtlCost.text = @"Biaya Transaksi";
        lblTtlFrekuensi.text = @"Frekuensi";
        [btnHapus setTitle:[@"hapus" uppercaseString] forState:UIControlStateNormal];
    }else{
        lblTitleTransfer.text = [@"transfer from" uppercaseString];
        lblTtlTrnsctNext.text = @"Next Transaction";
        lblTtlTrnsctStart.text = @"Start Date";
        lblTtlTrnsctEnd.text = @"End Date";
        lblTtlRekTujuan.text = [@"destination account" uppercaseString];
        lblTtlNorek.text = @"Account number";
        lblTtlNominal.text = @"Nominal";
        lblTtlCost.text = @"Transaction Fees";
        lblTtlFrekuensi.text = @"Frequency";
        [btnHapus setTitle:[@"delete" uppercaseString] forState:UIControlStateNormal];
    }
    
    
    lblTtlTrfId.text = [dataBundle objectForKey:@"id"];
    lblTtlTrfName.text = [userDefault objectForKey:@"customer_name"];
    lblTtlTrfSrc.text = [dataBundle objectForKey:@"srcacctno"];
    
    lblValTrnsctNext.text = [dataBundle objectForKey:@"nextexecdate"];
    lblValTrnsctStart.text = [dataBundle objectForKey:@"startdate"];
    lblValTrnsctEnd.text = [dataBundle objectForKey:@"enddate"];
    
    lblValNorek.text = [dataBundle objectForKey:@"destaccno"];
    lblValOther.text = [dataBundle objectForKey:@"beneficiary_name"];
    lblValNominal.text = [dataBundle objectForKey:@"amount"];
    lblValCost.text = [dataBundle objectForKey:@"charge"];
    lblValFrekuensi.text = [dataBundle objectForKey:@"frequent"];
    
    
    [lblTitleTransfer sizeToFit];
    [lblTtlTrfId sizeToFit];
    
    lblTtlTrfName.numberOfLines = 2;
    [lblTtlTrfName sizeToFit];
    
    [lblTtlTrfSrc sizeToFit];
    
    [lblTtlTrnsctNext sizeToFit];
    [lblTtlTrnsctStart sizeToFit];
    [lblTtlTrnsctEnd sizeToFit];
    
    [lblValTrnsctNext sizeToFit];
    [lblValTrnsctStart sizeToFit];
    [lblValTrnsctEnd sizeToFit];
    
    [lblTtlRekTujuan sizeToFit];
    [lblTtlNorek sizeToFit];
    [lblTtlNominal sizeToFit];
    [lblTtlCost sizeToFit];
    [lblTtlFrekuensi sizeToFit];
    [lblValNorek sizeToFit];
    
    lblValOther.numberOfLines = 2;
    [lblValOther sizeToFit];
    
    [lblValNominal sizeToFit];
    [lblValCost sizeToFit];
    
    lblValFrekuensi.numberOfLines = 0;
    [lblValFrekuensi sizeToFit];
    
    
    
//----------- end value in text ---------------
    
    CGRect frmVwScheduler = vwScheduler.frame;
    CGRect frmVwScroll = vwScroll.frame;
    
    CGRect frmVwContentScroll = vwContentScroll.frame;
    
    CGRect frmVwDataTransfer = vwDataTransfer.frame;
    CGRect frmLblTitleTransfer = lblTitleTransfer.frame;
    CGRect frmLblTtlTrfId = lblTtlTrfId.frame;
    CGRect frmLblTtlTrfName = lblTtlTrfName.frame;
    CGRect frmLblTtlTrfSrc = lblTtlTrfSrc.frame;
    
    CGRect frmVwDataTransact = vwDataTransact.frame;
    CGRect frmLblTtlTransactNext = lblTtlTrnsctNext.frame;
    CGRect frmLblValTransactNext = lblValTrnsctNext.frame;
    CGRect frmLblTtlTransactStart = lblTtlTrnsctStart.frame;
    CGRect frmLblValTransactStart = lblValTrnsctStart.frame;
    CGRect frmLblTtlTransactEnd = lblTtlTrnsctEnd.frame;
    CGRect frmLblValTransactEnd = lblValTrnsctEnd.frame;
    
    CGRect frmVwRekingTujuan = vwRekningTujuan.frame;
    CGRect frmLblTtlRekTujuan = lblTtlRekTujuan.frame;
    CGRect frmVwNorek = vwNoRek.frame;
    CGRect frmVwNominal = vwNominal.frame;
    CGRect frmVwCostTransact = vwCostTransct.frame;
    CGRect frmVwFrequent = vwFrekeuensi.frame;
    
    CGRect frmLblTtlNorek = lblTtlNorek.frame;
    CGRect frmLblTtlNominal = lblTtlNominal.frame;
    CGRect frmLblTtlCost = lblTtlCost.frame;
    CGRect frmLblTtlFrequesnt = lblTtlFrekuensi.frame;
    
    CGRect frmLblValNorek = lblValNorek.frame;
    CGRect frmLblValOther = lblValOther.frame;
    CGRect frmLblValNominal = lblValNominal.frame;
    CGRect frmLblValCost = lblValCost.frame;
    CGRect frmLblValFrequesnt = lblValFrekuensi.frame;
    
    CGRect frmVwBtn = vwButton.frame;
    CGRect frmBtnHapus = btnHapus.frame;
    
    
    frmVwScheduler.origin.x = 0;
    frmVwScheduler.origin.y = 0;
    frmVwScheduler.size.width = self.vwContentContainer.frame.size.width;
    frmVwScheduler.size.height = self.vwContentContainer.frame.size.height;
    
    frmVwScroll.origin.x = frmVwScheduler.origin.x;
    frmVwScroll.origin.y = frmVwScheduler.origin.y;
    frmVwScroll.size.width = frmVwScheduler.size.width;
    frmVwScroll.size.height = frmVwScheduler.size.height;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = frmVwScroll.size.width;
    
    frmVwDataTransfer.origin.x = frmVwContentScroll.origin.x;
    frmVwDataTransfer.origin.y = frmVwContentScroll.origin.y;
    frmVwDataTransfer.size.width = frmVwContentScroll.size.width;
    
    frmLblTitleTransfer.origin.x = 16;
    frmLblTitleTransfer.origin.y = 16;
    frmLblTitleTransfer.size.width = frmVwDataTransfer.size.width - (frmLblTitleTransfer.origin.x *2);
    
    frmLblTtlTrfId.origin.x = frmLblTitleTransfer.origin.x;
    frmLblTtlTrfId.origin.y = frmLblTitleTransfer.origin.y + frmLblTitleTransfer.size.height + 16;
    frmLblTtlTrfId.size.width = frmLblTitleTransfer.size.width;
    
    frmLblTtlTrfName.origin.x = frmLblTitleTransfer.origin.x;
    frmLblTtlTrfName.origin.y = frmLblTtlTrfId.origin.y + frmLblTtlTrfId.size.height + 8;
    frmLblTtlTrfName.size.width = frmLblTitleTransfer.size.width;
    
    frmLblTtlTrfSrc.origin.x = frmLblTitleTransfer.origin.x;
    frmLblTtlTrfSrc.origin.y = frmLblTtlTrfName.origin.y + frmLblTtlTrfName.size.height + 4;
    frmLblTtlTrfSrc.size.width = frmLblTitleTransfer.size.width;
    
    frmVwDataTransfer.size.height = frmLblTtlTrfSrc.origin.y + frmLblTtlTrfSrc.size.height + 16;
    
    frmVwDataTransact.origin.x = frmVwDataTransfer.origin.x;
    frmVwDataTransact.origin.y = frmVwDataTransfer.origin.y + frmVwDataTransfer.size.height + 8;
    frmVwDataTransact.size.width = frmVwDataTransfer.size.width;
    
    frmLblTtlTransactNext.origin.x = 16;
    frmLblTtlTransactNext.origin.y = 16;
    frmLblTtlTransactNext.size.width = frmVwDataTransact.size.width - (frmLblTtlTransactNext.origin.x *2);
    
    frmLblValTransactNext.origin.x = frmLblTtlTransactNext.origin.x;
    frmLblValTransactNext.origin.y = frmLblTtlTransactNext.origin.y + frmLblTtlTransactNext.size.height + 4;
    frmLblValTransactNext.size.width = frmLblTtlTransactNext.size.width;
    
    frmLblTtlTransactStart.origin.x = frmLblTtlTransactNext.origin.x;
    frmLblTtlTransactStart.origin.y = frmLblValTransactNext.origin.y + frmLblValTransactNext.size.height + 8;
    frmLblTtlTransactStart.size.width = frmVwDataTransact.size.width - (frmLblTtlTransactStart.origin.x *2);
    
    frmLblValTransactStart.origin.x = frmLblTtlTransactStart.origin.x;
    frmLblValTransactStart.origin.y = frmLblTtlTransactStart.origin.y + frmLblTtlTransactStart.size.height + 4;
    frmLblValTransactStart.size.width = frmLblTtlTransactStart.size.width;
    
    frmLblTtlTransactEnd.origin.x = frmLblTtlTransactNext.origin.x;
    frmLblTtlTransactEnd.origin.y = frmLblValTransactStart.origin.y + frmLblValTransactStart.size.height + 8;
    frmLblTtlTransactEnd.size.width = frmVwDataTransact.size.width - (frmLblTtlTransactEnd.origin.x *2);
    
    frmLblValTransactEnd.origin.x = frmLblTtlTransactEnd.origin.x;
    frmLblValTransactEnd.origin.y = frmLblTtlTransactEnd.origin.y + frmLblTtlTransactEnd.size.height + 4;
    frmLblValTransactEnd.size.width = frmLblTtlTransactEnd.size.width;
    
    frmVwDataTransact.size.height = frmLblValTransactEnd.origin.y + frmLblValTransactEnd.size.height + 16;
    
    frmVwRekingTujuan.origin.x = frmVwDataTransfer.origin.x;
    frmVwRekingTujuan.origin.y = frmVwDataTransact.origin.y + frmVwDataTransact.size.height + 8;
    frmVwRekingTujuan.size.width = frmVwDataTransfer.size.width;
    
    frmLblTtlRekTujuan.origin.x = 16;
    frmLblTtlRekTujuan.origin.y = 16;
    frmLblTtlRekTujuan.size.width = frmVwRekingTujuan.size.width - (frmLblTtlRekTujuan.origin.x *2);
    
    frmVwNorek.origin.y = frmLblTtlRekTujuan.origin.y + frmLblTtlRekTujuan.size.height + 8;
    frmVwNorek.origin.x = frmLblTtlRekTujuan.origin.x;
    frmVwNorek.size.width = frmLblTtlRekTujuan.size.width;
    
    frmLblTtlNorek.size.width = frmVwNorek.size.width/2 - 8;
    
    frmLblValNorek.origin.x = frmLblTtlNorek.origin.x + frmLblTtlNorek.size.width + 8;
    frmLblValNorek.size.width = frmLblTtlNorek.size.width;
    
    frmLblValOther.origin.x = frmLblValNorek.origin.x;
    frmLblValOther.origin.y = frmLblValNorek.origin.y + frmLblValNorek.size.height + 4;
    frmLblValOther.size.width = frmLblValNorek.size.width;
    frmLblValOther.size.height = [Utility optimalSizeForLabel:lblValOther inMaxSize:CGSizeMake(frmLblValOther.size.width, FLT_MAX)].height;
    
    frmVwNorek.size.height = frmLblValOther.origin.y + frmLblValOther.size.height;
    
    frmVwNominal.origin.y = frmVwNorek.origin.y + frmVwNorek.size.height + 8;
    frmVwNominal.origin.x = frmLblTtlRekTujuan.origin.x;
    frmVwNominal.size.width = frmLblTtlRekTujuan.size.width;
    
    frmLblTtlNominal.size.width = frmVwNominal.size.width/2 - 8;
    
    frmLblValNominal.origin.x = frmLblTtlNominal.origin.x + frmLblTtlNominal.size.width + 8;
    frmLblValNominal.size.width = frmLblTtlNominal.size.width;
    
    frmVwNominal.size.height = frmLblValNominal.origin.y + frmLblValNominal.size.height;
    
    frmVwCostTransact.origin.y = frmVwNominal.origin.y + frmVwNominal.size.height + 8;
    frmVwCostTransact.origin.x = frmLblTtlRekTujuan.origin.x;
    frmVwCostTransact.size.width = frmLblTtlRekTujuan.size.width;
    
    frmLblTtlCost.size.width = frmVwCostTransact.size.width/2 - 8;
    
    frmLblValCost.origin.x = frmLblTtlCost.origin.x + frmLblTtlCost.size.width + 8;
    frmLblValCost.size.width = frmLblTtlCost.size.width;
    
    frmVwCostTransact.size.height = frmLblValCost.origin.y + frmLblValCost.size.height;
    
    frmVwFrequent.origin.y = frmVwCostTransact.origin.y + frmVwCostTransact.size.height + 8;
    frmVwFrequent.origin.x = frmLblTtlRekTujuan.origin.x;
    frmVwFrequent.size.width = frmLblTtlRekTujuan.size.width;
    
    frmLblTtlFrequesnt.size.width = frmVwFrequent.size.width/2 - 8;
    
    frmLblValFrequesnt.origin.x = frmLblTtlFrequesnt.origin.x + frmLblTtlFrequesnt.size.width + 8;
    frmLblValFrequesnt.size.width = frmLblTtlFrequesnt.size.width;
    frmLblValFrequesnt.size.height = [Utility optimalSizeForLabel:lblValFrekuensi inMaxSize:CGSizeMake(frmLblValFrequesnt.size.width, FLT_MAX)].height;
    
    frmVwFrequent.size.height = frmLblValFrequesnt.origin.y + frmLblValFrequesnt.size.height;
    
    frmVwRekingTujuan.size.height = frmVwFrequent.origin.y + frmVwFrequent.size.height + 16;
    
    frmVwBtn.origin.x = frmVwDataTransfer.origin.x;
    frmVwBtn.origin.y = frmVwRekingTujuan.origin.y + frmVwRekingTujuan.size.height;
    frmVwBtn.size.width = frmVwDataTransfer.size.width;
    
    frmBtnHapus.origin.x = 24;
    frmBtnHapus.origin.y = 24;
    frmBtnHapus.size.width = frmVwBtn.size.width - (frmBtnHapus.origin.x *2);
    frmBtnHapus.size.height = 42;
    
    frmVwBtn.size.height = frmBtnHapus.origin.y + frmBtnHapus.size.height + 16;
    
    
    frmVwContentScroll.size.height = frmVwBtn.origin.y + frmVwBtn.size.height;
    
    vwScheduler.frame = frmVwScheduler;
    vwScroll.frame = frmVwScroll;
    
    vwContentScroll.frame = frmVwContentScroll;
    
    vwDataTransfer.frame = frmVwDataTransfer;
    lblTitleTransfer.frame = frmLblTitleTransfer;
    lblTtlTrfId.frame = frmLblTtlTrfId;
    lblTtlTrfName.frame = frmLblTtlTrfName;
    lblTtlTrfSrc.frame = frmLblTtlTrfSrc;
    
    vwDataTransact.frame = frmVwDataTransact;
    lblTtlTrnsctNext.frame = frmLblTtlTransactNext;
    lblValTrnsctNext.frame = frmLblValTransactNext;
    lblTtlTrnsctStart.frame = frmLblTtlTransactStart;
    lblValTrnsctStart.frame = frmLblValTransactStart;
    lblTtlTrnsctEnd.frame = frmLblTtlTransactEnd;
    lblValTrnsctEnd.frame = frmLblValTransactEnd;
    
    vwRekningTujuan.frame = frmVwRekingTujuan;
    lblTtlRekTujuan.frame = frmLblTtlRekTujuan;
    vwNoRek.frame = frmVwNorek;
    vwNominal.frame = frmVwNominal;
    vwCostTransct.frame = frmVwCostTransact;
    vwFrekeuensi.frame = frmVwFrequent;
    
    lblTtlNorek.frame = frmLblTtlNorek;
    lblTtlNominal.frame = frmLblTtlNominal;
    lblTtlCost.frame = frmLblTtlCost;
    lblTtlFrekuensi.frame = frmLblTtlFrequesnt;
    
    lblValNorek.frame = frmLblValNorek;
    lblValOther.frame = frmLblValOther;
    lblValNominal.frame = frmLblValNominal;
    lblValCost.frame = frmLblValCost;
    lblValFrekuensi.frame = frmLblValFrequesnt;
    
    vwButton.frame = frmVwBtn;
    btnHapus.frame = frmBtnHapus;
    
    [vwDataTransfer addSubview:lblTitleTransfer];
    [vwDataTransfer addSubview:lblTtlTrfId];
    [vwDataTransfer addSubview:lblTtlTrfName];
    [vwDataTransfer addSubview:lblTtlTrfSrc];
    
    [vwDataTransact addSubview:lblTtlTrnsctNext];
    [vwDataTransact addSubview:lblValTrnsctNext];
    [vwDataTransact addSubview:lblTtlTrnsctStart];
    [vwDataTransact addSubview:lblValTrnsctStart];
    [vwDataTransact addSubview:lblTtlTrnsctEnd];
    [vwDataTransact addSubview:lblValTrnsctEnd];
    
    [vwRekningTujuan addSubview:lblTtlRekTujuan];
    [vwRekningTujuan addSubview:vwNoRek];
    [vwRekningTujuan addSubview:vwNominal];
    [vwRekningTujuan addSubview:vwCostTransct];
    [vwRekningTujuan addSubview:vwFrekeuensi];
    
    [vwButton addSubview:btnHapus];
    
    [vwContentScroll addSubview:vwDataTransfer];
    [vwContentScroll addSubview:vwDataTransact];
    [vwContentScroll addSubview:vwRekningTujuan];
    [vwContentScroll addSubview:vwButton];
    
    [vwScroll addSubview:vwContentScroll];
    
    [vwScheduler addSubview:vwScroll];
    
    [vwScroll setContentSize:CGSizeMake(vwContentScroll.frame.size.width, vwContentScroll.frame.size.height)];
    
    
    return vwScheduler;
}


-(UILabel *) lblTitleGray : (UILabel *) lblView{
    UILabel *lblTitle = lblView;
    lblTitle.textColor = [UIColor grayColor];
    lblTitle.font = [UIFont fontWithName:const_font_name1 size:12];
    if (IPHONE_5) {
        lblTitle.font = [UIFont fontWithName:const_font_name1 size:8];
    }
    
    return lblTitle;
}

-(UILabel *) lblValGren : (UILabel *) lblView{
    UILabel *lblTitle = lblView;
    lblTitle.textColor = UIColorFromRGB(const_color_primary);
    lblTitle.font = [UIFont fontWithName:const_font_name3 size:15];
    if (IPHONE_5) {
        lblTitle.font = [UIFont fontWithName:const_font_name1 size:11];
    }
    return lblTitle;
}

-(UILabel *) lblTtlStyle : (UILabel *) lblView
                  mFont : (UIFont *) fontVal {
    UILabel *lblTitle = lblView;
    lblTitle.textColor = [UIColor blackColor];
    lblTitle.font = fontVal;
    return lblTitle;
}

-(UIView *) addingViewRemarkAble : (NSInteger) mType{
    UIView *vwBase = [[UIView alloc]init];
    UILabel *lblTtl = [[UILabel alloc]init];
    UILabel *lblVal = [[UILabel alloc]init];
    UILabel *lblOtherVal = [[UILabel alloc] init];
    
    lblTtl.textColor = [UIColor grayColor];
    lblTtl.font = [UIFont fontWithName:const_font_name1 size:14];
    
    
    lblVal = [self lblTtlStyle:lblVal mFont:[UIFont fontWithName:const_font_name3 size:14]];
    lblVal.textAlignment = NSTextAlignmentRight;
    
    if (IPHONE_5) {
        lblTtl.font = [UIFont fontWithName:const_font_name1 size:10];
        lblVal = [self lblTtlStyle:lblVal mFont:[UIFont fontWithName:const_font_name3 size:10]];
    }
    
    lblOtherVal = [self lblTitleGray:lblOtherVal];
    lblOtherVal.textAlignment = NSTextAlignmentRight;
    
    [vwBase insertSubview:lblTtl atIndex:0];
    [vwBase insertSubview:lblVal atIndex:1];
    if (mType == 1) {
        [vwBase insertSubview:lblOtherVal atIndex:2];
    }
    
    
    return vwBase;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) actionDeleteScheduler : (UIButton *) sender{
    if (sender.tag == 1) {
        NSString *nMessageDel, *btnYes, *btnNo;
        if([lang isEqualToString:@"id"]){
            nMessageDel = @"Apakah anda yakin ingin menghapus data?";
            btnYes = @"Iya";
            btnNo = @"Tidak";
        }else{
            nMessageDel = @"Are you sure want to delete data?";
            btnYes = @"Yes";
            btnNo = @"No";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessageDel delegate:self cancelButtonTitle:btnNo otherButtonTitles:btnYes, nil];
        alert.tag = 1;
        alert.delegate = self;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            [self dismissViewControllerAnimated:YES completion:^{
                NSDictionary *dataBundle = [self->mBundle objectForKey:@"data"];
                NSDictionary* userInfo = @{@"type": @"delete",@"id_schedule" : [dataBundle objectForKey:@"id"]};
                NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                [nc postNotificationName:@"schedulerTransaction" object:self userInfo:userInfo];
            }];
            
            
            
            
        }
    }
}

@end
