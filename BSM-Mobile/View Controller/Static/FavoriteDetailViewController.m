//
//  FavoriteDetailViewController.m
//  BSM-Mobile
//
//  Created by lds on 6/15/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import "FavoriteDetailViewController.h"
#import "HomeViewController.h"
#import "ScrollHomeViewController.h"
#import "Connection.h"
#import "PopUpLoginViewController.h"

#import "UIViewController+ECSlidingViewController.h"
@interface FavoriteDetailViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate>{
    NSArray *listData;
    int indexSelected;
    NSString *idFav;
    NSString *nameFav;
    NSArray *arrTitleFav;
    NSString *defTitleReceipt;
    UIView *vwBgCaption;
    UITextField *txtFieldCaption;
    int node;
    bool deleted;
    
    UIView *vwContinerCaption;
    UIView *vwLineTitleCaption;
    UIView *vwLineTextCaption;
    UILabel *lblTitleCaption;
    UILabel *lblTextCaption;
    CustomBtn *btnSaveCaption;
    CustomBtn *btnCancelCaption;
    UIScrollView *vwPopupScroll;
    
    UIToolbar* keyboardDoneButtonView;
    
    NSUserDefaults *userDefault;
    NSString *lang;
    BOOL loginDone;
    NSString *menuid;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation FavoriteDetailViewController

- (void)setSelectedIndex:(int)index{
    indexSelected = index;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)reloadApp{
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"mustLogin"] isEqualToString:@"YES"] &&
       [[[NSUserDefaults standardUserDefaults] valueForKey:@"hasLogin"] isEqualToString:@"NO"]
       ){
        PopUpLoginViewController *popLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupLogin"];
        [popLogin setMFeat:@"favorite"];
        [self presentViewController:popLogin animated:YES completion:nil];
    } else {
        [self reloadData];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"favoriteDetail"
                                             object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(favoriteDetailBC:) name:@"favoriteDetail" object:nil];
    
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgToolbar;
    if ([lang isEqualToString:@"id"]) {
        msgToolbar = @"Selesai";
        [self.lblTitleMenu setText:@"Favorit"];
    }else{
        msgToolbar = @"Done";
        [self.lblTitleMenu setText:@"Favorite"];
    }
    
    self.lblTitleMenu.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleMenu.textAlignment = const_textalignment_title;
    self.lblTitleMenu.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleMenu.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [Styles setTopConstant:self.topConstraint];
    
    arrTitleFav = [[NSArray alloc]init];
    
    deleted = false;
    [self.view removeGestureRecognizer:self.slidingViewController.panGesture];
    // Do any additional setup after loading the view.
    
    keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    [self registerForKeyboardNotifications];
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [vwPopupScroll setContentSize:CGSizeMake(vwContinerCaption.frame.size.width, vwContinerCaption.frame.size.height + kbSize.height + 20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    vwPopupScroll.contentInset = contentInsets;
    vwPopupScroll.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [vwPopupScroll setContentSize:CGSizeMake(vwContinerCaption.frame.size.width, vwContinerCaption.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    vwPopupScroll.contentInset = contentInsets;
    vwPopupScroll.scrollIndicatorInsets = contentInsets;
}

- (void)favoriteDetailBC:(NSNotification *) notification{
    if([notification.name isEqualToString:@"favoriteDetail"])
    {
        NSDictionary *userInfo = notification.userInfo;
        
        if([[userInfo valueForKey:@"logged_in"] isEqualToString:@"YES"]){
            loginDone = true;
            [self reloadData];
        } else {
            [self.navigationController popToRootViewControllerAnimated:true];
        }
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavCell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
    NSDictionary *data = [listData objectAtIndex:indexPath.row];

    
    [label setText:[data valueForKey:@"name"]];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    [label sizeToFit];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return true;
}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(editingStyle == UITableViewCellEditingStyleDelete){
//        idFav = [[listData objectAtIndex:indexPath.row]valueForKey:@"id_favorite"];
//        NSString *text = @"Anda yakin untuk menghapus?";
//            NSString *language = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]objectAtIndex:0];
//        BOOL isEn =[language isEqualToString:@"en"];
//        if(isEn){
//            text = @"Do you want to delete this favorite?";
//        }
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:text delegate:self cancelButtonTitle:lang(@"CANCEL") otherButtonTitles:@"OK", nil];
//        alert.tag = 133;
//        alert.delegate = self;
//        [alert show];
//    }
//}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *nMessageDel;
    if([lang isEqualToString:@"id"]){
        nMessageDel = @"Apakah anda ingin menghapus favorit ini?";
    }else{
        nMessageDel = @"Do you want to delete this favorite?";
    }
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:lang(@"DELETE") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
        {
            idFav = [[listData objectAtIndex:indexPath.row]valueForKey:@"id_favorite"];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessageDel delegate:self cancelButtonTitle:lang(@"CANCEL") otherButtonTitles:@"OK", nil];
            alert.tag = 133;
            alert.delegate = self;
            [alert show];
        }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:lang(@"EDIT") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
        {
            nameFav = [[listData objectAtIndex:indexPath.row]valueForKey:@"name"];
            arrTitleFav = [nameFav componentsSeparatedByString:@"\n"];
            defTitleReceipt = [arrTitleFav objectAtIndex:0];
            idFav = [[listData objectAtIndex:indexPath.row]valueForKey:@"id_favorite"];
            [self popupCaption];
            
        }];
    editAction.backgroundColor = [UIColor colorWithRed:0x2F/255.0 green:0x83/255.0 blue:0xFB/255.0 alpha:1];
    return @[deleteAction, editAction];
}

#pragma mark - TableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];

    DataManager *dataManager = [DataManager sharedManager];
    [dataManager resetObjectData];
    BOOL valid = false;
    NSString *code = @"";
    NSString *submenu = @"";
    NSString *chid = @"";
    NSString *mid = [data objectForKey:@"menu_id"];
    menuid = mid;
    NSArray *urlParm = [[data objectForKey:@"url_parm"]componentsSeparatedByString:@","];
    for(NSString *temp in urlParm){
        NSArray *value = [temp componentsSeparatedByString:@"="];
        if(value.count > 1){
            [dataManager.dataExtra setObject:[value objectAtIndex:1] forKey:[value objectAtIndex:0]];
            if ([[value objectAtIndex:0] isEqualToString:@"code"]) {
                code = [value objectAtIndex:1];
            }
            if ([[value objectAtIndex:0] isEqualToString:@"submenu"]) {
                submenu = [value objectAtIndex:1];
            }
            if ([[value objectAtIndex:0] isEqualToString:@"chid"]) {
                chid = [value objectAtIndex:1];
            }
            
        }
    }
    
    if([data objectForKey:@"menu_id"]){
        
        [dataManager setMenuId:[data valueForKey:@"menu_id"]];
        valid = true;
    }
    
    if([data objectForKey:@"node"]){
        if ([submenu isEqualToString:@""]) {
            node = [[data valueForKey:@"node"]intValue] - 1;
        }
        else {
            node = [[data valueForKey:@"node"]intValue] - 1;
            
            NSMutableDictionary *temp = [NSMutableDictionary dictionary];
            [temp setValue:code forKey:@"code"];
            [temp setValue:submenu forKey:@"submenu"];
            
            if ([chid isEqualToString:@""]) {
                [temp setValue:mid forKey:@"menuid"];
            }else{
                [temp setValue:chid forKey:@"menuid"];
            }
            [userDefault setObject:temp forKey:@"favD"];
        }
    }
    
    if(valid){
        NSString *lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=action_favorite,menu_id=%@,chid=%@,os_type=2,language=%@", [data valueForKey:@"menu_id"],chid, lang] needLoading:true encrypted:true banking:true];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 133) {
        if(buttonIndex == 1){
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=delete_favorite,id_favorite=%@",idFav] needLoading:true encrypted:true banking:true];
        }
    } else if (alertView.tag == 233) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else if (alertView.tag == 333) {
        [self reloadData];
    }else if (alertView.tag == 01){
        NSLog(@"field is empty");
    }
}


#pragma mark - Connection Delegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    DLog(@"---------------------%@", jsonObject);
    if (![requestType isEqualToString:@"check_notif"]) {
        if(jsonObject && [jsonObject isKindOfClass:[NSArray class]]){
            listData = jsonObject;
            if ([listData count] == 0) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *msg = @"";
                if([lang isEqualToString:@"id"]){
                    msg = @"Daftar favorit kosong";
                } else {
                    msg = @"Favorites list is empty";
                }
                [self.tableView reloadData];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag = 233;
                alert.delegate = self;
                [alert show];
            } else {
                [self.tableView reloadData];
            }
            
        } else if (jsonObject){
            if ([requestType isEqualToString:@"action_favorite"]) {
                //if ([jsonObject objectForKey:@"response"]) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                if([jsonObject count] > 0){
                    [userDefault setObject:jsonObject forKey:@"actionFav"];
                }else{
                    [userDefault setObject:[[[dataManager getJsonDataWithMenuID:menuid andList:dataManager.listMenu]objectAtIndex:1]objectForKey:@"action"]
                                    forKey:@"actionFav"];
                }
                [userDefault synchronize];
                @try {
                    [self showFavorite];
                } @catch (NSException *exception) {
                    NSLog(@"%@", exception.description);
                }
                
                //}
            } else {
                
            }
            if ([jsonObject objectForKey:@"response_code"]) {
                if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
                    
                    [self reloadData];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject objectForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alert.tag = 233;
                    alert.delegate = self;
                    [alert show];
                }
                
            } else {
                [self reloadData];
            }
        }
        
    }
}

- (void)reloadData{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    conn.fav = true;
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_favorite,type=%d",indexSelected+1] needLoading:true encrypted:true banking:false];
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    NSString *text = [data valueForKey:@"name"];
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    
    return height + 20;
    
}

#pragma mark change cancel to next and other else
- (void)popupCaption {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat widthContiner =  width - 40;
    CGFloat heightContiner = 200;
    
    NSString *word1 = @"";
    NSString *word2 = @"";
    NSString *word3 = @"";
    NSString *word4 = @"";
    if([lang isEqualToString:@"id"]){
        word1 = @"Ubah Favorit";
        word2 = @"Judul";
        word3 = @"UBAH";
        word4 = @"BATAL";
    } else {
        word1 = @"Edit Favorite";
        word2 = @"Caption";
        word3 = @"EDIT";
        word4 = @"CANCEL";
    }
    vwBgCaption = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    vwBgCaption.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    vwPopupScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _vwTitle.frame.origin.y + _vwTitle.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT - (_vwTitle.frame.origin.y + _vwTitle.frame.size.height))];
    
    vwContinerCaption = [[UIView alloc]initWithFrame:CGRectMake(20, (vwPopupScroll.frame.size.height/2) - (heightContiner/2) - 40, vwPopupScroll.frame.size.width - 40, heightContiner)];
    vwContinerCaption.backgroundColor = [UIColor whiteColor];
    vwContinerCaption.layer.cornerRadius = 10;
    
    [vwPopupScroll addSubview:vwContinerCaption];
    
    lblTitleCaption = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, widthContiner - 20, 20)];
    [lblTitleCaption setFont:[UIFont fontWithName:const_font_name3 size:19]];
    [lblTitleCaption setTextColor:[UIColor blackColor]];
    [lblTitleCaption setTextAlignment:NSTextAlignmentCenter];
    [lblTitleCaption setText:word1];
    
    vwLineTitleCaption = [[UIView alloc]initWithFrame:CGRectMake(0, 40, widthContiner, 1)];
    vwLineTitleCaption.backgroundColor = [UIColor grayColor];
    
    lblTextCaption = [[UILabel alloc] initWithFrame:CGRectMake(15, 50, widthContiner - 30, 20)];
    [lblTextCaption setFont:[UIFont fontWithName:const_font_name1 size:13]];
    [lblTextCaption setTextColor:[UIColor colorWithRed:(21.0/255.0) green:(124.0/255.0) blue:(104.0/255.0) alpha:1]];
    [lblTextCaption setTextAlignment:NSTextAlignmentLeft];
    [lblTextCaption setText:word2];
    
    txtFieldCaption = [[UITextField alloc] initWithFrame:CGRectMake(15, 70, widthContiner - 30, 40)];
    [txtFieldCaption setFont:[UIFont fontWithName:const_font_name1 size:17]];
    [txtFieldCaption setTextColor:[UIColor blackColor]];
    [txtFieldCaption setText:defTitleReceipt];
    
    txtFieldCaption.inputAccessoryView = keyboardDoneButtonView;
    
    vwLineTextCaption = [[UIView alloc]initWithFrame:CGRectMake(15, 110, widthContiner - 30, 1)];
    vwLineTextCaption.backgroundColor = [UIColor grayColor];
    
    btnCancelCaption = [[CustomBtn alloc] initWithFrame:CGRectMake(widthContiner/2 + 10, heightContiner - 60, widthContiner/2 - 20, 40)];
    [btnCancelCaption setTitle:word3 forState:UIControlStateNormal];
    [btnCancelCaption addTarget:self action:@selector(actionSaveCaption) forControlEvents:UIControlEventTouchUpInside];
    [btnCancelCaption setColorSet:PRIMARYCOLORSET];
    
    btnSaveCaption = [[CustomBtn alloc] initWithFrame:CGRectMake(10, heightContiner - 60, widthContiner/2 - 20, 40)];
    [btnSaveCaption setTitle:word4 forState:UIControlStateNormal];
    [btnSaveCaption addTarget:self action:@selector(actionCancelCaption) forControlEvents:UIControlEventTouchUpInside];
    [btnSaveCaption setColorSet:SECONDARYCOLORSET];
    
    [vwContinerCaption addSubview:lblTitleCaption];
    [vwContinerCaption addSubview:vwLineTitleCaption];
    [vwContinerCaption addSubview:lblTextCaption];
    [vwContinerCaption addSubview:txtFieldCaption];
    [vwContinerCaption addSubview:vwLineTextCaption];
    [vwContinerCaption addSubview:btnSaveCaption];
    [vwContinerCaption addSubview:btnCancelCaption];
    
    [vwPopupScroll setContentSize:CGSizeMake(widthContiner, vwContinerCaption.frame.size.height)];
    
    [vwBgCaption addSubview:vwPopupScroll];
    [self.view addSubview:vwBgCaption];
    
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)actionCancelCaption {
    [vwBgCaption removeFromSuperview];
}
- (void)actionSaveCaption {
    NSString *newValue = txtFieldCaption.text;
    NSArray *arrNameFav = [nameFav componentsSeparatedByString:@"\n"];
    int sizeNameFav = (int)[arrNameFav count];
    //NSString *newTitle = [NSString stringWithFormat:@"%@\n%@\n%@\n%@",newValue,arrNameFav[1],arrNameFav[2],arrNameFav[3]];
    NSString *newTitle = [NSString stringWithFormat:@"%@\n",newValue];
    
    for (int i=1;i<sizeNameFav;i++) {
        if (i == (sizeNameFav-1)) {
            newTitle = [newTitle stringByAppendingString:arrNameFav[i]];
        } else {
            newTitle = [[newTitle stringByAppendingString:arrNameFav[i]] stringByAppendingString:@"\n"];
        }
    }
    
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if ([newValue isEqualToString:@""] || newValue == nil) {
        NSString *text;
        if ([lang isEqualToString:@"id"]) {
            text = @"Masukkan judul";
        }else{
            text = @"Insert caption";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:text delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 01;
        alert.delegate = self;
        [alert show];
    }else{
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=rename_favorite,id_favorite=%@,name=%@",idFav,newTitle] needLoading:true encrypted:true banking:true];
        
        [vwBgCaption removeFromSuperview];
    }
    
    
}

- (void)showFavorite {
    @try {
         UITabBarController *tabCont = self.tabBarController;
           UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
           ScrollHomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
           [homeCont openFavorite:node];
           [tabCont setSelectedIndex:0];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
   
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}




@end
