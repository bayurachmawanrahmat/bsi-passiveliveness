//
//  WebviewViewController.h
//  BSM-Mobile
//
//  Created by ARS on 02/01/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebviewViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
