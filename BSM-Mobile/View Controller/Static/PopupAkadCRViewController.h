//
//  PopupAkadCRViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface PopupAkadCRViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate : (id) newDelegate;
- (void) setData : (NSString*) data;
- (void) setButtonTitle : (NSString*)title;

@end

@protocol AkadCRDelegate

@required

- (void) completionAkadCR : (BOOL) state;

@end

NS_ASSUME_NONNULL_END
