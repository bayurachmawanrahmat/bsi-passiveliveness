//
//  PopupBurroqViewController.h
//  BSM-Mobile
//
//  Created by BSM on 8/14/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface PopupBurroqViewController : UIViewController

- (void) DataMessage :(NSString *) dataText;
- (void) findID : (NSString*) findId;

@end

NS_ASSUME_NONNULL_END
