//
//  FinancialViewController.h
//  BSM-Mobile
//
//  Created by lds on 5/26/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import "RootViewController.h"
#import <WebKit/WebKit.h>

@interface FinancialViewController : RootViewController

@property (strong, nonatomic) IBOutlet WKWebView *WebView;
@end
