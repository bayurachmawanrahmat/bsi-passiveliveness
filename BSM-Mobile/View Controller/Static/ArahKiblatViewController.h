//
//  ArahKiblatViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/19/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "RootViewController.h"
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ArahKiblatViewController : RootViewController<CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *needle;
@property (strong, nonatomic) IBOutlet UIImageView *compass;
@property(strong,nonatomic) CLLocationManager *locationManager;
@end
