//
//  ComplainViewController.m
//  BSM-Mobile
//
//  Created by ARS on 26/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ComplainViewController.h"
#import "Connection.h"
#import "Utility.h"
#import "CellComplain.h"
#import "ComplainCell.h"
#import "PopupDetailComplainViewController.h"
#import "CustomerOnboardingData.h"
#import "PopUpLoginViewController.h"

@interface ComplainViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate,
UIScrollViewDelegate> {
    NSString *prodName;
    NSString* requestType;
    NSArray *listAction;
    NSMutableArray *listHeight;
    
    BOOL mustAddToManager;
    NSDictionary *dataObject;
    CellComplain *cellSbn;
    NSArray *selectedMenu;
    NSArray *redemContent;
    NSNumberFormatter* currencyFormatter;
    NSUserDefaults *userDefault;
    
    UIImageView *imgIcon;
    UILabel *lblJudul;
    UITextField *nominal;
    UIView *viewBg;
    UIView *viewContiner;
    UIScrollView *vwScroll;
    UIView *vwBox;
    NSString *lang;
    double vNominal;
    
    UIView *imgEmpty;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btnComplain;
- (IBAction)complain:(id)sender;


@end

@implementation ComplainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    listHeight = [[NSMutableArray alloc]init];
    
    [userDefault setObject:@"complain_handler.png" forKey:@"imgIcon"];
    
    if ([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Daftar Masalah Transaksi";
        [_btnComplain setTitle:@"Pelaporan Masalah Transaksi" forState:UIControlStateNormal];
    }else{
        [_btnComplain setTitle:@"Report Transaction Problem" forState:UIControlStateNormal];
        self.lblTitle.text = @"List Of Transaction Problem";
    }
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
   
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"ComplainCell" bundle:nil] forCellReuseIdentifier:@"COMPLAINCELL"];

    userDefault = [NSUserDefaults standardUserDefaults];
    NSString* noHp;
//    if([userDefault objectForKey:@"msisdn"]){
//        noHp = [userDefault valueForKey:@"msisdn"];
//    }else if([userDefault objectForKey:@"mobilenumber"]){
//        noHp = [userDefault valueForKey:@"mobilenumber"];
//    }else{
//        noHp = @"";
//    }
    
    if([NSUserdefaultsAes getValueForKey:@"msisdn"]){
        noHp = [NSUserdefaultsAes getValueForKey:@"msisdn"];
    }
//    else if([userDefault objectForKey:@"mobilenumber"]){
//        noHp = [userDefault valueForKey:@"mobilenumber"];
//    }
    else{
        noHp = @"";
    }
    
    [dataManager.dataExtra addEntriesFromDictionary:@{@"msisdn":noHp}];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_complaint,msisdn=%@", noHp] needLoading:true encrypted:false banking:false favorite:false];
    
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmTbView = self.tableView.frame;
    
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = SCREEN_WIDTH - (frmLblTitle.origin.x*2);
    frmLblTitle.size.height = 40;
    
    frmTbView.origin.x = 0;
    frmTbView.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height +16;
    frmTbView.size.width = SCREEN_WIDTH - (frmTbView.origin.x*2);
    frmTbView.size.height = SCREEN_HEIGHT - (BOTTOM_NAV+TOP_NAV+frmVwTitle.size.height+ 80);
    
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveNotch]) {
        frmTbView.size.height = SCREEN_HEIGHT - (BOTTOM_NAV+TOP_NAV+frmVwTitle.size.height);
    }
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.tableView.frame = frmTbView;
   
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    imgEmpty = [Utility showNoData:self.tableView];
    imgEmpty.hidden = YES;
    [self.tableView addSubview:imgEmpty];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listAction.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ComplainCell *cell = (ComplainCell *)[tableView dequeueReusableCellWithIdentifier:@"COMPLAINCELL"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *fullString = [listAction objectAtIndex:indexPath.row];
    NSArray *listCek = [[fullString valueForKey:@"DATE_TIKET"] componentsSeparatedByString:@"-"];
    NSArray *listItems = [[listCek objectAtIndex:2] componentsSeparatedByString:@" "];
    NSArray *bulan = @[@"Jan", @"Feb", @"Mar", @"Apr", @"Mei", @"Jun", @"Jul", @"Agu", @"Sep", @"Okt", @"Nov", @"Des"];
    
    
    cell.lblDate.text = [listItems objectAtIndex:0];
    NSInteger bln = ([[listCek objectAtIndex:1] integerValue])-1;
    cell.lblMonth.text = [bulan objectAtIndex:bln];
    cell.lblYear.text = [listCek objectAtIndex:0];
    
    cell.lblComplain.text = [fullString valueForKey:@"SUBJEK_KELUHAN"];
    cell.lblComplainDetail.text = [NSString stringWithFormat:@"%@\n"
                                   "No Ticket : %@ \n"
                                   "Status : %@",
                                   [fullString valueForKey:@"DESKRIPSI"],
                                   [fullString valueForKey:@"NO_TIKET"],
                                   [fullString valueForKey:@"STATUS"]];
    return cell;
}


#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *mutDataDict = [[NSMutableDictionary alloc]init];
    mutDataDict = [listAction objectAtIndex:indexPath.row];
    
    PopupDetailComplainViewController *popDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"POPCOMPLAIN"];
    [popDetail dataBundle:mutDataDict];
    [self presentViewController:popDetail animated:YES completion:nil];
    
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
        if([requestType isEqualToString:@"list_complaint"]){
            listAction = [jsonObject objectForKey:@"response"];
            listAction = [listAction sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *a, NSDictionary *b) {
               return [a[@"DATE_TIKET"] compare:b[@"DATE_TIKET"]];
            }];
            listAction = [[listAction reverseObjectEnumerator] allObjects];
            NSLog(@"listaction ::: %@",listAction);
            
            if(listAction.count < 1){
                imgEmpty.hidden = NO;
            }else{
                imgEmpty.hidden = YES;
            }
            [self.tableView reloadData];
        }
        
        if([requestType isEqualToString:@"check_chms_operational"]){
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            
            NSDictionary *cobTime = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            if([[cobTime objectForKey:@"iscobtime"]isEqualToString:@"0"]){
                [self alertForCOBTime:[cobTime objectForKey:@"MAX"]
                                       withMin:[cobTime objectForKey:@"MIN"]];
            }else{
                [self gotoFormComplain];
            }
            
        }
     }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
    double newValue = [newText doubleValue];
    
    if ([newText length] == 0 || newValue == 0){
        textField.text = nil;
    }
    else{
        textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
        return NO;
        
    }
    
    return YES;

}
- (IBAction)complain:(id)sender{
    [self newCheckCOBTime];
//    [self checkCOBTime];
}

- (void) gotoFormComplain{
//    NSArray *selectedMenu = [[[[dataManager.listMenu objectAtIndex:0]objectAtIndex:1] objectForKey:@"action"] objectAtIndex:1];
    NSString *menuIdMutasi = @"00009"; // 00009 code for Mutation List
    NSArray *selectedMenu = [dataManager getJsonDataWithMenuID:menuIdMutasi andList:dataManager.listMenu];
    [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
    //    [dataManager.dataExtra addEntriesFromDictionary:@{@"complain":@"1"}];
//    [userDefault setValue:@"YES" forKey:@"complain_state"];
    [dataManager.dataExtra setValue:@"YES" forKey:@"complain_state"];
    
    NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
    
//    if([[userDefault objectForKey:@"email"]isEqualToString:@""] || [userDefault objectForKey:@"email"] == nil){
    if([email isEqualToString:@""] || email == nil){
        NSString *chatMessage = @"";
        if([lang isEqualToString:@"id"]){
            chatMessage = @"Email tidak boleh kosong, harap isi email di menu pengaturan.";
        }else{
            chatMessage = @"Email cannot be empty, please fill your email in setting menu.";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:chatMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.delegate = self;
        alert.tag = 404;
        [alert show];
        
    }else{
        @try {
            [super openNextTemplate];
        
        } @catch (NSException *exception) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alertView.tag = 999; //tag no have menu
            [alertView show];
        }
    }
}

- (void) alertForCOBTime:(NSString *)max withMin:(NSString*) min{
    NSString *chatMessage = @"";
    if([lang isEqualToString:@"id"]){
        chatMessage = [NSString stringWithFormat:@"Maaf Layanan dapat diakses mulai pukul %@ s.d %@", min, max];
    }else{
        chatMessage = [NSString stringWithFormat:@"Sorry this service only can be access start from %@ s.d %@.", min, max];
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:chatMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate = self;
    alert.tag = 404;
    [alert show];
}

- (void) newCheckCOBTime{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=check_chms_operational"] needLoading:true encrypted:false banking:false favorite:false];
}

- (void) checkCOBTime{
    dispatch_group_t dispatchGroup = dispatch_group_create();
    NSMutableDictionary *checkCobResult = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=COB_ENABLED" dispatchGroup:dispatchGroup returnData:checkCobResult];
//    [CustomerOnboardingData getFromEndpoint:@"check_chms_operational" dispatchGroup:dispatchGroup returnData:checkCobResult];

    NSMutableDictionary *checkTimeResult = [[NSMutableDictionary alloc] init];
    NSString *mobversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/setting/getCobTimesNew?mobver=%@", mobversion] dispatchGroup:dispatchGroup returnData:checkTimeResult];

    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        NSLog(@"%@", [checkCobResult objectForKey:@"is_success"]);
        if([[checkCobResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *data;
            if([[checkCobResult objectForKey:@"data"] isKindOfClass:NSArray.class])
                data = [(NSArray *)[checkCobResult objectForKey:@"data"] objectAtIndex:0];
            else
                data = [checkCobResult objectForKey:@"data"];
            BOOL cobEnabled = [[data objectForKey:@"setval"] isEqualToString:@"true"] ? YES : NO;
            if(!cobEnabled || ([[checkTimeResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]] && ![[[checkTimeResult objectForKey:@"data"] objectForKey:@"iscobtime"] isEqualToNumber:[NSNumber numberWithBool:YES]]))
            {
                NSLog(@"%@",[[checkTimeResult objectForKey:@"data"] objectForKey:@"iscobtime"]);
                [self alertForCOBTime:[[checkTimeResult objectForKey:@"data"] objectForKey:@"MAX"]
                              withMin:[[checkTimeResult objectForKey:@"data"] objectForKey:@"MIN"]];
            }else{
                [self gotoFormComplain];
            }
        }
    });
}

@end

