//
//  AboutViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 08/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "AboutViewController.h"
#import "Utility.h"
#import "Styles.h"


@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (@available(iOS 13.0, *)) {
       self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    _viewContent.layer.cornerRadius = 10;
    
    if([Utility isLanguageID]){
        [_labelVersionTitle setText:@"Versi"];
        [_labelHakCipta setText:@"BSI Mobile Hak Cipta Terlindungi"];
        [_labelInfo setText:@"Untuk informasi lebih lanjut, kunjungi https://www.bankbsi.co.id atau hubungi kami di 14040"];
    } else {
        [_labelVersionTitle setText:@"Version"];
        [_labelHakCipta setText:@"BSI Mobile All Right Reserved"];
        [_labelInfo setText:@"For further information, please visit https://www.bankbsi.co.id or contact us at 14040"];
    }
    //    [_labelVersion setText:@VERSION_NAME];

    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *buildNumber = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    if ([AppProperties checkIsProd]) {
        [_labelVersion setText:[NSString stringWithFormat:@"%@", version]];
    } else {
        [_labelVersion setText:[NSString stringWithFormat:@"%@ (%@)", version, buildNumber]];
    }
    [_labelCopyright setText:@"Copyright © 2021 - PT Bank Syariah Indonesia, Tbk."];
    
    [_btnOk addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
}

- (void)actionOK{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidAppear:(BOOL)animated{
    UIView* bgView = [[UIView alloc]initWithFrame:self.view.bounds];
    bgView.backgroundColor = [Styles backgroundPopupColor];
    [self.view insertSubview:bgView atIndex:0];
}
@end
