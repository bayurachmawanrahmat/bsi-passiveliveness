//
//  DetailLocationViewController.h
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 29/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "RootViewController.h"

@interface DetailLocationViewController : RootViewController
    @property (nonatomic, strong) NSDictionary *data;
@end

