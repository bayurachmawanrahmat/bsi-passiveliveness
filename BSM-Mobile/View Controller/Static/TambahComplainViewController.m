//
//  TambahComplainViewController.m
//  BSM-Mobile
//
//  Created by ARS on 23/12/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TambahComplainViewController.h"
#import "ComplainViewController.h"
#import "PopupDetailComplainViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface TambahComplainViewController ()<UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate>{
    NSUserDefaults *userDefault;
    NSString * UploadType;
    NSURL * PDFUrl;
    NSMutableArray *arrimg;
    NSString * ImageName;
    UIImagePickerController *imagePicker;
    UIPickerView *objPickerAccount;
    NSString *upType;
    NSString *upMethod;
    NSString *lang;
//    NSString *subject;
    
    UIToolbar *toolBar;
    NSArray *dataRek4;
    NSArray *listAction;
    int numberOfAttachmentFiles;
    NSNumberFormatter* currencyFormatter;
    NSString *buttonState;

}
@property (weak, nonatomic) IBOutlet UIView *vwImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpload;

@property (strong, nonatomic) IBOutlet UIView *vwRoot;

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScrool;
@property (weak, nonatomic) IBOutlet UIView *vwMenu;

@property (weak, nonatomic) IBOutlet UILabel *lblRef;

//DODO
@property (weak, nonatomic) IBOutlet UIView *vwNasabah;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *lblCsNameTitle;

@property (weak, nonatomic) IBOutlet UIView *vwEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblCsEmailTitle;

@property (weak, nonatomic) IBOutlet UIView *vwNorek;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerAcc;
@property (weak, nonatomic) IBOutlet UILabel *lblCsAccTitle;

@property (weak, nonatomic) IBOutlet UIView *vwNophone;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblCsPhoneTitle;

@property (weak, nonatomic) IBOutlet UIView *vwDesc;
@property (weak, nonatomic) IBOutlet UITextField *txtDeskripsi;
@property (weak, nonatomic) IBOutlet UILabel *lblDescTitle;
@property (weak, nonatomic) IBOutlet UIView *vwDescLine;

@property (weak, nonatomic) IBOutlet UIView *vwNominal;
@property (weak, nonatomic) IBOutlet UILabel *lblNominalTitle;
@property (weak, nonatomic) IBOutlet UITextField *textFieldNominal;
@property (weak, nonatomic) IBOutlet UIView *vwNominalLine;

@property (weak, nonatomic) IBOutlet UIView *vwNoref;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRef;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRefTitle;

@property (weak, nonatomic) IBOutlet UIView *vwNotifAttachment;
@property (weak, nonatomic) IBOutlet UILabel *lblFilename;

@property (weak, nonatomic) IBOutlet UIView *vwUpload;
@property (weak, nonatomic) IBOutlet UIImageView *vwImageUp;
@property (weak, nonatomic) IBOutlet UIButton *vwBtnUplod;
@property (weak, nonatomic) IBOutlet UILabel *lblUpload;

@property (weak, nonatomic) IBOutlet UIButton *btKirim;


@end

@implementation TambahComplainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    numberOfAttachmentFiles = 0;
    arrimg =[[NSMutableArray alloc]init];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    buttonState = @"1";

    self.textFieldNominal.delegate = self;
    self.textFieldNominal.leftViewMode = UITextFieldViewModeAlways;
    [self.textFieldNominal setPlaceholder:@"nominal"];
    [self.textFieldNominal setKeyboardType:UIKeyboardTypeNumberPad];

    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [self setupContent];
    [self labelFontConfig];
    [self setupLayout];
    self.vwNominal.hidden = YES;
    
    NSString *ttlToolbar;
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    
    if ([lang isEqualToString:@"id"]) {
        ttlToolbar = @"Selesai";
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_reason,noref=%@", [[dataManager dataExtra] valueForKey:@"Referensi"]] needLoading:true encrypted:false banking:false favorite:false];
    }else{
        ttlToolbar = @"Done";
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_reason,noref=%@", [[dataManager dataExtra] valueForKey:@"Reference"]] needLoading:true encrypted:false banking:false favorite:false];
    }

//    Connection *conn = [[Connection alloc]initWithDelegate:self];
//    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_reason,noref=%@", [[dataManager dataExtra] valueForKey:@"Referensi"]] needLoading:true encrypted:false banking:false favorite:false];

//    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_reason"] needLoading:true encrypted:false banking:false favorite:false];

    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];

    objPickerAccount = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    objPickerAccount.showsSelectionIndicator = YES;
    objPickerAccount.delegate = self;
    objPickerAccount.dataSource = self;

    
    self.txtDeskripsi.inputView = objPickerAccount;
    self.txtDeskripsi.inputAccessoryView = toolBar;
    [self.txtDeskripsi addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventAllTouchEvents];

    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScrool setContentSize:CGSizeMake(self.vwScrool.frame.size.width, self.vwMenu.frame.origin.y+self.vwMenu.frame.size.height + kbSize.height+20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    self.vwScrool.contentInset = contentInsets;
    self.vwScrool.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScrool setContentSize:CGSizeMake(self.vwScrool.frame.size.width, self.vwMenu.frame.origin.y+self.vwMenu.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScrool.contentInset = contentInsets;
    self.vwScrool.scrollIndicatorInsets = contentInsets;
}

- (IBAction)upload:(id)sender {
    
    if(numberOfAttachmentFiles < 3){
        NSString *select = @"";
        NSString *cancel = @"";
        NSString *takePhoto = @"";
        NSString *choose = @"";
        
        if([lang isEqualToString:@"id"]){
            select = @"Pilih Opsi:";
            cancel = @"Batal";
            takePhoto = @"Ambil Foto";
            choose = @"Pilih File yang tersedia";
        }else{
            select = @"Select Photo Option:";
            cancel = @"Cancel";
            takePhoto = @"Take Photo";
            choose = @"Choose Existing";
        }
        
        
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:select delegate:self cancelButtonTitle:cancel destructiveButtonTitle:nil otherButtonTitles:
                                takePhoto,
                                choose,nil];
        
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }else{
        NSString *alertMessage = @"";
        if([lang isEqualToString:@"id"]){
            alertMessage = @"Max Upload 3 File";
        }else{
            alertMessage = @"3 files Max to Upload";
        }
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                   message:alertMessage
                                   preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {}];

        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

#pragma  mark- Opne Action Sheet for Options
- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    upMethod = @"TAKE";
                    imagePicker = [[UIImagePickerController alloc] init];
                    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                    imagePicker.delegate = self;
                    
                    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    [self presentViewController:imagePicker animated:YES completion:nil];
                    
                    break;
                case 1:
                    upMethod = @"CHOOSE";
                    imagePicker = [[UIImagePickerController alloc] init];
                    imagePicker.delegate = self;
                    
                    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    [self presentViewController:imagePicker animated:YES completion:nil];
                    
                    break;

            }
            break;
        }
        default:
            break;
    }
}

#pragma mark- Open Image Picker Delegate to select image from Gallery or Camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSData *imgData = UIImageJPEGRepresentation(myImage, 1);
    
    NSLog(@" image data %lu", (unsigned long)[imgData length]);
//    NSUInteger size = [imgData length];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [arrimg insertObject:myImage atIndex:numberOfAttachmentFiles];
    numberOfAttachmentFiles = numberOfAttachmentFiles + 1;
    [self addingFiles];
    
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (NSString *) base64StringForImage_objc:(UIImage *)image {
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData base64EncodedStringWithOptions:0];
}

- (UIImage *) imageFromBase64String_objc:(NSString *)string {
    NSData *imageData = [[NSData alloc] initWithBase64EncodedString: string options: 0];
    return [[UIImage alloc] initWithData:imageData];
}

- (NSString *)compressAndEncodeToBase64String:(UIImage *)image {
    float ratio = image.size.width /image.size.height;
    float xFinal = 700;
    float yfinal = xFinal/ratio;
    UIImage *scaledImage = [self imageWithImage:image scaledToSize:CGSizeMake(xFinal, yfinal)];
    
    CGFloat compression = 1.0f;
    CGFloat maxCompression = 0.1f;
       float maxSize = 40*1024; //specified in bytes

    NSData *imageData = UIImageJPEGRepresentation(scaledImage, compression);
    while ([imageData length] > maxSize && compression > maxCompression) {
        compression -= 0.10;
        imageData = UIImageJPEGRepresentation(scaledImage, compression);
//        NSLog(@"Compressed to: %.2f MB with Factor: %.2f",(float)imageData.length/1024.0f/1024.0f, compression);
    }
//    NSLog(@"Final Image Size: %.2f MB",(float)imageData.length/1024.0f/1024.0f);
    NSString *compressedImageString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    return compressedImageString;
}

- (UIImage *)imageWithImage:(UIImage*)originalImage scaledToSize:(CGSize)newSize;
{
    @synchronized(self)
    {
        UIGraphicsBeginImageContext(newSize);
        [originalImage drawInRect:CGRectMake(0,0,newSize.width, newSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    return nil;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.tag == 1){
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
            double newValue = [newText doubleValue];
            
            if ([newText length] == 0 || newValue == 0){
                textField.text = nil;
            }
            else{
                if (textField.tag == 10) {
                    NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
                    textField.text = newText;
                }else{
                    textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
                }
                
        //        [self handlingInputUserText:textField.accessibilityLabel];
                
                return NO;
            }
    }
    return YES;

}

- (IBAction)kirim:(id)sender {
    
    if([buttonState isEqualToString:@"0"]){
        [self backToR];
    }else{

            NSArray *keterangan = [[[dataManager dataExtra] valueForKey:@"ket"]componentsSeparatedByString:@"/"];
            NSString *trans_desc = [keterangan objectAtIndex:0];
            NSString *ammount = [[[dataManager dataExtra] valueForKey:@"debit"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        ammount = [ammount stringByReplacingOccurrencesOfString:@"," withString:@""];
        
            NSString *tanggalConv = @"";
            if([lang isEqualToString:@"id"]){
                tanggalConv = [[dataManager dataExtra] valueForKey:@"Tanggal"];
            }else{
                tanggalConv = [[dataManager dataExtra] valueForKey:@"Date"];
            }
            NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
            [inputFormatter setDateFormat:@"dd MMM yyyy"];
            inputFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"id_ID"];
        
            NSDate *trans_date = [inputFormatter dateFromString:tanggalConv];
            [inputFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *dateString = [inputFormatter stringFromDate:trans_date];
            NSLog(@"%@",dateString);
            NSString *tanggal = @"";
            if(dateString != nil){
                tanggal = dateString;
            }
//            NSUInteger componentFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
//            NSDateComponents *components = [[NSCalendar currentCalendar] components:componentFlags fromDate:trans_date];
//            NSInteger year = [components year];
//            NSInteger month = [components month];
//
//            NSInteger day = [components day];
//            if(trans_date != nil){
//                if(month < 10)
//                   {
//                       if(day <10){
//                           tanggal = [NSString stringWithFormat:@"%ld-0%ld-0%ld", (long)year, (long)month,(long)day];
//                       } else {
//                           tanggal = [NSString stringWithFormat:@"%ld-0%ld-%ld", (long)year, (long)month,(long)day];
//                       }
//                   } else {
//                       if(day <10){
//                           tanggal = [NSString stringWithFormat:@"%ld-%ld-0%ld", (long)year, (long)month,(long)day];
//                       } else {
//                           tanggal = [NSString stringWithFormat:@"%ld-%ld-%ld", (long)year, (long)month,(long)day];
//                       }
//                   }
//            }
        
            
            NSString *msisdn = [[dataManager dataExtra] valueForKey:@"msisdn"];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *cname = [userDefault objectForKey:@"customer_name"];
            //NSString *customerid = [userDefault objectForKey:@"customer_id"];
            NSString *customerid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
//            NSString *email = [userDefault objectForKey:@"email"];
            NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
            NSString *nominal = self.textFieldNominal.text;
            NSString *desc = [NSString stringWithFormat:@"%@ %@", self.txtDeskripsi.text, [nominal stringByReplacingOccurrencesOfString:@"," withString:@""]];
            NSString *username = cname;

            NSMutableArray *attachmentFile = [[NSMutableArray alloc]init];
            NSData *imgData;
            NSString *encodedImage = @"";
            
            for (int i = 0; i < 3; i++){
                if(i < numberOfAttachmentFiles){
                    imgData = UIImageJPEGRepresentation([arrimg objectAtIndex:i], 0);

                    encodedImage = [self compressAndEncodeToBase64String:[arrimg objectAtIndex:i]];
                    encodedImage = [[encodedImage componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]]componentsJoinedByString:@""];
        ;
                    [attachmentFile insertObject:encodedImage atIndex:i];
                }else{
                    encodedImage = @"";
                    [attachmentFile insertObject:encodedImage atIndex:i];
                }
                
            }
            if(![self.vwNominal isHidden] && [self.textFieldNominal.text isEqualToString:@""]){
                if([lang isEqualToString:@"id"]){
                    [self showNotification:@"Form Nominal tidak boleh kosong"];
                }else{
                    [self showNotification:@"Amount Field cannot be empty"];
                }
            }else if([self.txtDeskripsi.text isEqualToString:@""]){
                if([lang isEqualToString:@"id"]){
                    [self showNotification:@"Form tidak boleh kosong"];
                }else{
                    [self showNotification:@"Reason Field cannot be empty"];
                }
                
            }else{
                [dataManager.dataExtra setValue:tanggal forKey:@"trans_date"];
                [dataManager.dataExtra setValue:trans_desc forKey:@"trans_desc"];
                [dataManager.dataExtra setValue:desc forKey:@"desc"];
                [dataManager.dataExtra setValue:ammount forKey:@"amount"];
//                [dataManager.dataExtra setValue:id_desc forKey:@"id_desc"];
//                [dataManager.dataExtra setValue:subject forKey:@"subject"];
//

                
                NSString *data = [NSString stringWithFormat: @"lid,language=%@,customer_id=%@,id_account=%@,msisdn=%@,accno=%@,ID_MEDIA_PELAPORAN=7,customer_name=%@,email=%@,id_desc,subject,username=%@,noref=%@,amount,trans_date,trans_desc,desc,attachment1=%@,attachment2=%@,attachment3=%@",lang, customerid, _lblCustomerAcc.text, self.lblCustomerPhone.text,_lblCustomerAcc.text, cname,email, username, _lblNoRef.text, [attachmentFile objectAtIndex:0], [attachmentFile objectAtIndex:1], [attachmentFile objectAtIndex:2]];
                  
                  Connection *conn = [[Connection alloc]initWithDelegate:self];
                  [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=insert_complaint,%@",data] needLoading:true encrypted:false banking:false favorite:false];
            }
    }
}

- (void)showNotification: (NSString *)message{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                               message:message
                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {}];

    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (UIView *) makeBorder : (UIView *) view{
    view.layer.borderWidth = 1;
    view.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    view.layer.cornerRadius = 8;
    return view;
}

- (void) labelFontConfig{
    [self.lblTitle setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
    [self.lblCsNameTitle setFont:[UIFont fontWithName:const_font_name1 size:12]];
    [self.lblCsEmailTitle setFont:[UIFont fontWithName:const_font_name1 size:12]];
    [self.lblCsAccTitle setFont:[UIFont fontWithName:const_font_name1 size:12]];
    [self.lblCsPhoneTitle setFont:[UIFont fontWithName:const_font_name1 size:12]];
    [self.lblRef setFont:[UIFont fontWithName:const_font_name1 size:12]];
    [self.lblDescTitle setFont:[UIFont fontWithName:const_font_name1 size:12]];
    [self.lblNominalTitle setFont:[UIFont fontWithName:const_font_name1 size:12]];

}

- (void) addingFiles{
    
    int i = 0;
    for(i = 0; i < numberOfAttachmentFiles; i ++){
        
        UIView *attachView = [[UIView alloc] init];
        UILabel *attachFile = [[UILabel alloc] init];
        UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeSystem];
        
        CGRect frmAttachView = attachView.frame;
        CGRect frmAttachFile = attachFile.frame;
        CGRect frmBtnCancelation = btnCancel.frame;
        CGRect frmVwAttach = self.vwNotifAttachment.frame;
        
        CGFloat frmWidth = self.vwNotifAttachment.frame.size.width;
        CGFloat frmHeight = 50;
        
        frmAttachView.origin.x = 0;
        frmAttachView.origin.y = i * 51;
        frmAttachView.size.width = frmWidth;
        frmAttachView.size.height = frmHeight;
        
        frmBtnCancelation.origin.x = frmWidth - 45;
        frmBtnCancelation.origin.y = 0;
        frmBtnCancelation.size.height = frmHeight;
        frmBtnCancelation.size.width = 45;
        
        frmAttachFile.origin.x = 0;
        frmAttachFile.origin.y = 0;
        frmAttachFile.size.width = frmWidth - 45;
        frmAttachFile.size.height = frmHeight;
        
        frmVwAttach.size.height = frmHeight + i*frmHeight;
//        frmVwAttach.origin.x = sel
        
        attachView.frame = frmAttachView;
        btnCancel.frame = frmBtnCancelation;
        attachFile.frame = frmAttachFile;
        self.vwNotifAttachment.frame = frmVwAttach;
        
        [attachView setBackgroundColor:UIColorFromRGB(0xf0dd71)];
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//         NSString *upFile = @"";
        
//         upFile = [NSString stringWithFormat:@"Attachment_%@.jpg",[dateFormatter stringFromDate:[NSDate date]]];
        attachFile.tag = i;
        [attachFile setText:[NSString stringWithFormat:@"Attachment_%d_%@.jpg", i+1, [dateFormatter stringFromDate:[NSDate date]]]];
        [attachFile setTextColor:[UIColor blackColor]];
        [attachFile setTextAlignment:NSTextAlignmentCenter];
        [attachFile setFont:[UIFont fontWithName:const_font_name1 size:15]];
        UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openImage:)];
        [attachFile addGestureRecognizer:tap];
        [attachFile setUserInteractionEnabled:YES];
        
        btnCancel.tag = i;
        btnCancel.accessibilityLabel = [NSString stringWithFormat:@"edt_%d", i];
        [btnCancel addTarget:self action:@selector(actionDeleteFileAttach:) forControlEvents:UIControlEventTouchUpInside];
        [btnCancel setTitle:@"x" forState:UIControlStateNormal];
        [btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnCancel.titleLabel setFont:[UIFont systemFontOfSize:28]];
        
        [attachView addSubview:attachFile];
        [attachView addSubview:btnCancel];
        
        [self.vwNotifAttachment addSubview:attachView];
    }
    [self newRelocation];
}


- (void) addNewFiles:(NSString*) file{
    numberOfAttachmentFiles = numberOfAttachmentFiles + 1;
    
    UIView *attachView = [[UIView alloc]init];
    UILabel *attachFile = [[UILabel alloc]init];
    
    CGRect frmAttachView = attachView.frame;
    CGRect frmAttachFile = attachFile.frame;
    
    frmAttachView.origin.x = 0;
    frmAttachView.origin.y = numberOfAttachmentFiles * 51;
    frmAttachView.size.width = self.vwNotifAttachment.frame.size.width;
    frmAttachView.size.height = 50;
    
    frmAttachFile.origin.x = 0;
    frmAttachFile.origin.y = 0;
    frmAttachFile.size.width = frmAttachView.size.width;
    frmAttachFile.size.height = 50;
        
    attachView.frame = frmAttachView;
    [attachView setBackgroundColor:UIColorFromRGB(0xf0dd71)];
    
    attachFile.frame = frmAttachFile;
    [attachFile setText:file];
    attachFile.tag = numberOfAttachmentFiles;
    [attachFile setTextColor:[UIColor blackColor]];
    [attachFile setTextAlignment:NSTextAlignmentCenter];
    [attachFile setFont:[UIFont fontWithName:const_font_name1 size:15]];
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openImage:)];
    [attachFile addGestureRecognizer:tap];
    [attachFile setUserInteractionEnabled:YES];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeSystem];;
    [btnCancel setFrame:CGRectMake(frmAttachView.size.width - 50, 0, 45, 45)];
    [btnCancel addTarget:self action:@selector(actionDeleteFileAttach:) forControlEvents:UIControlEventTouchUpInside];
    btnCancel.tag = numberOfAttachmentFiles;
    btnCancel.accessibilityLabel = [NSString stringWithFormat:@"edt_%d", numberOfAttachmentFiles];
    [btnCancel setTitle:@"x" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnCancel.titleLabel setFont:[UIFont systemFontOfSize:28]];
    [btnCancel setUserInteractionEnabled:YES];

    [attachView addSubview:attachFile];
    [attachView addSubview:btnCancel];

    [self.vwNotifAttachment addSubview:attachView];
    
    [self newRelocation];
}

- (void)openImage:(UIGestureRecognizer*)recognizer
{
    
    UIView *view = recognizer.view;
    
//    NSLog(@"%ld",(long)view.tag);
    
    NSMutableDictionary *mutDataDict = [[NSMutableDictionary alloc]init];
    [mutDataDict setObject:@"YES" forKey:@"popimage"];

    PopupDetailComplainViewController *popDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"POPCOMPLAIN"];
    [popDetail dataBundle:mutDataDict];
    [popDetail setImages:[arrimg objectAtIndex:view.tag]];
    [self presentViewController:popDetail animated:YES completion:nil];
}

- (IBAction) actionDeleteFileAttach:(UIButton *)sender{
//    NSLog(@"%ld", (long)sender.tag);
    
    for (UIView *subUIView in self.vwNotifAttachment.subviews) {
        [subUIView removeFromSuperview];
    }
    
    [arrimg removeObjectAtIndex:sender.tag];
    numberOfAttachmentFiles = numberOfAttachmentFiles - 1;
    [self addingFiles];
}

- (void) newRelocation{
    CGRect frmVwDesc = self.vwDesc.frame;
    CGRect frmVwNominal = self.vwNominal.frame;
    CGRect frmVwAttach = self.vwNotifAttachment.frame;
    CGRect frmVwUpload = self.vwUpload.frame;
    CGRect frmBtnKirim = self.btKirim.frame;
    CGRect frmVwMenu = self.vwMenu.frame;
    
    if(self.vwNominal.hidden == NO){
        frmVwAttach.origin.y = frmVwNominal.origin.y + frmVwNominal.size.height + 1;
    }else{
        frmVwAttach.origin.y = frmVwDesc.origin.y + frmVwDesc.size.height + 1;
    }

//    frmVwAttach.size.height = 51 * numberOfAttachmentFiles;
    
    frmVwUpload.origin.y = frmVwAttach.origin.y + frmVwAttach.size.height + 10;
    frmVwUpload.size.height = self.vwUpload.frame.size.height;

    frmBtnKirim.origin.x = 8;
    frmBtnKirim.origin.y = frmVwUpload.origin.y + frmVwUpload.size.height + 10;
    
    frmVwMenu.size.height = frmBtnKirim.origin.y + frmBtnKirim.size.height + 30;
    
    self.vwNominal.frame = frmVwNominal;
    self.vwNotifAttachment.frame = frmVwAttach;
    
    self.vwUpload.frame = frmVwUpload;
    
    self.btKirim.frame = frmBtnKirim;
    self.vwMenu.frame = frmVwMenu;
    
    [self.vwMenu setUserInteractionEnabled:YES];
    [self.vwNotifAttachment setUserInteractionEnabled:YES];
    [self.vwScrool setUserInteractionEnabled:YES];
    
    [self.vwScrool setContentSize:CGSizeMake(self.vwMenu.frame.size.width, self.vwMenu.frame.size.height)];
    
}

- (void) setupContent{
    
    if([lang isEqualToString:@"id"]){
        _lblTitle.text = @"Form Masalah Transaksi";
        _lblCsNameTitle.text = @"Nama Nasabah";
        _lblCsEmailTitle.text = @"Email";
        _lblCsAccTitle.text = @"No Rekening";
        _lblCsPhoneTitle.text = @"No Telepon";
        _lblRef.text = @"Nomor Referensi (FT)";
        _lblDescTitle.text = @"Alasan Gagal Transaksi";
        _lblNominalTitle.text = @"Jumlah Uang yang Keluar";
        _lblUpload.text = @"Upload File";
        [self.btKirim setTitle:@"KIRIM" forState:UIControlStateNormal];
        _lblNoRef.text = [[dataManager dataExtra] valueForKey:@"Referensi"];

    }else{
        _lblTitle.text = @"Transaction Issue Form";
        _lblCsNameTitle.text = @"Customer Name";
        _lblCsEmailTitle.text = @"Email";
        _lblCsAccTitle.text = @"No Account";
        _lblCsPhoneTitle.text = @"No Phone";
        _lblRef.text = @"Reference Number (FT)";
        _lblDescTitle.text = @"Transaction Problem";
        _lblNominalTitle.text = @"Amount of Received Money";
        _lblUpload.text = @"Upload File";
        [self.btKirim setTitle:@"SEND" forState:UIControlStateNormal];
        _lblNoRef.text = [[dataManager dataExtra] valueForKey:@"Reference"];

    }
    
//    _lblTitle.text = [dataManager.dataExtra valueForKey:@"Tanggal"];
    _lblCustomerName.text = [userDefault objectForKey:@"customer_name"];
    
    self.lblCustomerName.numberOfLines = 0;
    self.lblCustomerName.textAlignment = NSTextAlignmentLeft;
    self.lblCustomerName.lineBreakMode = NSLineBreakByWordWrapping;
    
//    _lblCustomerEmail.text = [userDefault objectForKey:@"email"];
    _lblCustomerEmail.text = [NSUserdefaultsAes getValueForKey:@"email"];
    _lblCustomerAcc.text = [[dataManager dataExtra] valueForKey:@"id_account"];
    
//    if(![[userDefault objectForKey:@"msisdn"]isEqualToString:@""]){
    if(![[NSUserdefaultsAes getValueForKey:@"msisdn"]isEqualToString:@""]){
//        _lblCustomerPhone.text = [userDefault objectForKey:@"msisdn"];
        _lblCustomerPhone.text = [NSUserdefaultsAes getValueForKey:@"msisdn"];
    }else{
        _lblCustomerPhone.text = [userDefault objectForKey:@"mobilenumber"];
    }
//    _lblNoRef.text = [[dataManager dataExtra] valueForKey:@"Referensi"];
}

- (UIView*) getLineVew{
    CGRect frmvwNasabah = self.vwNasabah.frame;
    
    UIView *viewLine = [[UIView alloc]init];
    viewLine.backgroundColor = UIColorFromRGB(const_color_gray);
    
    CGRect frmVwLine = viewLine.frame;
    frmVwLine.origin.x = frmvwNasabah.origin.x;
    frmVwLine.origin.y = frmvwNasabah.origin.y + frmvwNasabah.size.height - 1;
    frmVwLine.size.width = frmvwNasabah.size.width;
    frmVwLine.size.height = 1;
    
    viewLine.frame = frmVwLine;
    
    return viewLine;
}

-(void) setupLayout {
    
    UILabel *ketFile = [[UILabel alloc] init];
    if([lang isEqualToString:@"id"]){
        ketFile.text = @"* File berukuran Maks 2 MB (jpg /png)";
    }else{
        ketFile.text = @"* Max File Size 2 MB (jgp / png)";
    }
    
    CGRect frmvwRoot = self.vwRoot.frame;
    
    CGRect frmvwTitle = self.vwTitle.frame;
    CGRect frmLbTitle = self.lblTitle.frame;
    
    CGRect frmvwScroll = self.vwScrool.frame;
    CGRect frmvwMenu = self.vwMenu.frame;
    
    CGRect frmvwNasabah = self.vwNasabah.frame;
    CGRect frmtitleNasabah = self.lblCsNameTitle.frame;
    CGRect frmlblNasabahName = self.lblCustomerName.frame;
    
    CGRect frmvwEmail = self.vwEmail.frame;
    CGRect frmtitleEmail = self.lblCsEmailTitle.frame;
    CGRect frmlblNasabahEmail = self.lblCustomerEmail.frame;
    
    CGRect frmVwRek = self.vwNorek.frame;
    CGRect frmTitleRek = self.lblCsAccTitle.frame;
    CGRect frmLblRek = self.lblCustomerAcc.frame;
    
    CGRect frmvwPhone = self.vwNophone.frame;
    CGRect frmtitlePhone = self.lblCsPhoneTitle.frame;
    CGRect frmlblPhone = self.lblCustomerPhone.frame;
    
    CGRect frmvwDesc = self.vwDesc.frame;
    CGRect frmtitleDesc = self.lblDescTitle.frame;
    CGRect frmtextDesc = self.txtDeskripsi.frame;
    CGRect frmVwDescLine = self.vwDescLine.frame;
    
    CGRect frmvwNominal = self.vwNominal.frame;
    CGRect frmtitleNominal = self.lblNominalTitle.frame;
    CGRect frmtextNominal = self.textFieldNominal.frame;
    CGRect frmVwNomLine = self.vwNominalLine.frame;

    CGRect frmvwRef = self.vwNoref.frame;
    CGRect frmLblRefTitle = self.lblNoRefTitle.frame;
    CGRect frmLblRef = self.lblNoRef.frame;
    
    CGRect frmvwAttach = self.vwNotifAttachment.frame;
    CGRect frmLblFileAttach = self.lblFilename.frame;
    
    CGRect frmvwUpload = self.vwUpload.frame;
    CGRect frmImgUplod = self.vwImageUp.frame;
    CGRect frmBtnUplod = self.vwBtnUplod.frame;
    CGRect frmLblUpload = self.lblUpload.frame;
    CGRect frmKetUpload = ketFile.frame;
    
    
    CGRect frmBtnKirim = self.btKirim.frame;
    
    frmvwRoot.origin.x = 0;
    frmvwRoot.origin.y = 0;
    frmvwRoot.size.width = SCREEN_WIDTH;
    
    //Title
    frmvwTitle.origin.x = 0;
    frmvwTitle.origin.y = TOP_NAV;
    frmvwTitle.size.width = SCREEN_WIDTH;
    frmvwTitle.size.height = 50;
    
    frmLbTitle.origin.x = 8;
    frmLbTitle.origin.y = 2;
    frmLbTitle.size.width = SCREEN_WIDTH;
    frmLbTitle.size.height = frmvwTitle.size.height;
    
    frmvwScroll.origin.x = 0;
    frmvwScroll.origin.y = frmvwTitle.origin.y + frmvwTitle.size.height;
    frmvwScroll.size.width = SCREEN_WIDTH;
    frmvwScroll.size.height = SCREEN_HEIGHT - frmvwScroll.origin.y - BOTTOM_NAV;
    
    frmvwMenu.origin.x = 8;
    frmvwMenu.origin.y = 0;
    frmvwMenu.size.width = SCREEN_WIDTH - 16;
    
    //nasabah
    frmvwNasabah.origin.x = 0;
    frmvwNasabah.origin.y = 0;
    frmvwNasabah.size.width = frmvwMenu.size.width;
    
    frmtitleNasabah.origin.x = 0;
    frmtitleNasabah.origin.y = 4;
    frmtitleNasabah.size.width = frmvwNasabah.size.width;
    
    frmlblNasabahName.origin.x = frmtitleNasabah.origin.x;
    frmlblNasabahName.origin.y = frmtitleNasabah.size.height + 8;
    frmlblNasabahName.size.width = frmvwNasabah.size.width - 16;
//    frmlblNasabahName.size.height = [self getLabelHeight:_lblCustomerName];
    frmvwNasabah.size.height = frmlblNasabahName.origin.y + frmlblNasabahName.size.height + 8;
    
    
    //email
    frmvwEmail.origin.x = 0;
    frmvwEmail.origin.y = frmvwNasabah.origin.y + frmvwNasabah.size.height +1;
    frmvwEmail.size.width = frmvwMenu.size.width;
    frmtitleEmail.origin.x = 0;
    frmtitleEmail.origin.y = 4;
    frmtitleEmail.size.width = frmvwEmail.size.width;
    
    frmlblNasabahEmail.origin.x = frmtitleEmail.origin.x;
    frmlblNasabahEmail.origin.y = frmtitleEmail.size.height + 8;
    frmlblNasabahEmail.size.width = frmvwEmail.size.width - 16;
    frmvwEmail.size.height = frmlblNasabahEmail.origin.y + frmlblNasabahEmail.size.height + 8;
    
    //rek
    frmVwRek.origin.x = 0;
    frmVwRek.origin.y = frmvwEmail.origin.y + frmvwEmail.size.height +1;
    frmVwRek.size.width = frmvwMenu.size.width;
    
    frmTitleRek.origin.x = 0;
    frmTitleRek.origin.y = 4;
    frmTitleRek.size.width = frmVwRek.size.width;
    
    frmLblRek.origin.x = frmTitleRek.origin.x;
    frmLblRek.origin.y = frmTitleRek.size.height + 8;
    frmLblRek.size.width = frmVwRek.size.width - 16;
    frmVwRek.size.height = frmLblRek.origin.y + frmLblRek.size.height + 8;
    
    //phone
    frmvwPhone.origin.x = 0;
    frmvwPhone.origin.y = frmVwRek.origin.y + frmVwRek.size.height +1;
    frmvwPhone.size.width = frmvwMenu.size.width;
    
    frmtitlePhone.origin.x = 0;
    frmtitlePhone.origin.y = 4;
    frmtitlePhone.size.width = frmvwPhone.size.width;
    
    frmlblPhone.origin.x = frmtitlePhone.origin.x;
    frmlblPhone.origin.y = frmtitlePhone.size.height + 8;
    frmlblPhone.size.width = frmvwPhone.size.width - 16;
    frmvwPhone.size.height = frmlblPhone.origin.y + frmlblPhone.size.height + 8;
    
    //ref
    frmvwRef.origin.x = 0;
    frmvwRef.origin.y = frmvwPhone.origin.y + frmvwPhone.size.height +1;
    frmvwRef.size.width = frmvwMenu.size.width;
    
    frmLblRefTitle.origin.x = 0;
    frmLblRefTitle.origin.y = 4;
    frmLblRefTitle.size.width = frmvwRef.size.width;
    
    frmLblRef.origin.x = frmLblRefTitle.origin.x;
    frmLblRef.origin.y = frmLblRefTitle.size.height + 8;
    frmLblRef.size.width = frmvwRef.size.width - 16;
    frmvwRef.size.height = frmLblRef.origin.y + frmLblRef.size.height + 8;
    
    //des
    frmvwDesc.origin.x = 0;
    frmvwDesc.origin.y = frmvwRef.origin.y + frmvwRef.size.height + 1;
    frmvwDesc.size.width = frmvwMenu.size.width;
    frmtitleDesc.origin.x = 0;
    frmtitleDesc.origin.y = 4;
    frmtitleDesc.size.width = frmvwDesc.size.width;
    
    frmtextDesc.origin.x = frmtitleDesc.origin.x;
    frmtextDesc.origin.y = frmtitleDesc.size.height + 8;
    frmtextDesc.size.width = frmvwDesc.size.width - 16;
   
    frmVwDescLine.origin.x = frmtextDesc.origin.x;
    frmVwDescLine.origin.y = frmtextDesc.origin.y + frmtextDesc.size.height + 1;
    frmVwDescLine.size.width = frmvwDesc.size.width;
    frmVwDescLine.size.height = 1;
    frmvwDesc.size.height = frmVwDescLine.origin.y + frmVwDescLine.size.height + 8;
    
    //nominal
    frmvwNominal.origin.x = 0;
    frmvwNominal.origin.y = frmvwDesc.origin.y + frmvwDesc.size.height + 1;
    frmvwNominal.size.width = frmvwMenu.size.width;
    
    frmtitleNominal.origin.x = 0;
    frmtitleNominal.origin.y = 4;
    frmtitleNominal.size.width = frmvwNominal.size.width;
    
    frmtextNominal.origin.x = frmtitleNominal.origin.x;
    frmtextNominal.origin.y = frmtitleNominal.size.height + 8;
    frmtextNominal.size.width = frmvwNominal.size.width - 16;
    frmVwNomLine.origin.x = frmtextNominal.origin.x;
    frmVwNomLine.origin.y = frmtextNominal.origin.y + frmtextNominal.size.height + 1;
    frmVwNomLine.size.width = frmvwNominal.size.width;
    frmVwNomLine.size.height = 1;
    frmvwNominal.size.height = frmVwNomLine.origin.y + frmVwNomLine.size.height + 8;
    
    // Attachment
    frmvwAttach.origin.x = 0;
    frmvwAttach.origin.y = frmvwDesc.origin.y + frmvwDesc.size.height + 1;
    frmvwAttach.size.width = frmvwMenu.size.width;
    frmLblFileAttach.origin.x = 4;
    frmLblFileAttach.origin.y = 16;
    frmLblFileAttach.size.width = frmvwAttach.size.width - 16;
//    frmvwAttach.size.height = frmLblFileAttach.origin.y + frmLblFileAttach.size.height + 8;
        
    frmvwUpload.origin.x = 0;
    frmvwUpload.origin.y = frmvwAttach.origin.y + frmvwAttach.size.height + 1;
    frmvwUpload.size.width = frmvwMenu.size.width;
    frmBtnUplod.origin.x = 0;
    frmBtnUplod.origin.y = 0;
    frmBtnUplod.size.width = frmvwUpload.size.width;
    
    frmImgUplod.origin.y = 0;
    frmImgUplod.size.height = frmvwUpload.size.height;
    frmImgUplod.size.width = frmvwUpload.size.height;
    frmLblUpload.origin.y = 0;
    frmLblUpload.size.height = frmvwUpload.size.height;
    frmImgUplod.origin.x = (frmvwUpload.size.width - (frmImgUplod.size.width + frmLblUpload.size.width))/2;
    frmvwUpload.size.height = frmImgUplod.size.height;
    frmBtnUplod.size.height = frmvwUpload.size.height;
//    frmKetUpload.origin
    
    frmBtnKirim.origin.x = 0;
    frmBtnKirim.origin.y = frmvwUpload.origin.y + frmvwUpload.size.height + 10;
    frmBtnKirim.size.width = frmvwUpload.size.width;
    
    frmvwMenu.size.height = frmBtnKirim.origin.y + frmBtnKirim.size.height + 30;
    
    self.vwRoot.frame = frmvwRoot;
    
    self.vwTitle.frame = frmvwTitle;
    self.lblTitle.frame = frmLbTitle;
    
    self.vwScrool.frame = frmvwScroll;
    
    self.vwMenu.frame = frmvwMenu;
    self.vwMenu.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    
    self.vwNasabah.frame = frmvwNasabah;
    self.lblCsNameTitle.frame = frmtitleNasabah;
    self.lblCustomerName.frame = frmlblNasabahName;
    [self.vwNasabah addSubview:[self getLineVew]];
    
    self.vwEmail.frame = frmvwEmail;
    self.lblCsEmailTitle.frame = frmtitleEmail;
    self.lblCustomerEmail.frame = frmlblNasabahEmail;
    [self.vwEmail addSubview:[self getLineVew]];
    
    self.vwNorek.frame = frmVwRek;
    self.lblCsAccTitle.frame = frmTitleRek;
    self.lblCustomerAcc.frame = frmLblRek;
    [self.vwNorek addSubview:[self getLineVew]];
    
    self.vwNophone.frame = frmvwPhone;
    self.lblCsPhoneTitle.frame = frmtitlePhone;
    self.lblCustomerPhone.frame = frmlblPhone;
    [self.vwNophone addSubview:[self getLineVew]];
    
    self.vwDesc.frame = frmvwDesc;
    self.lblDescTitle.frame = frmtitleDesc;
    self.txtDeskripsi.frame = frmtextDesc;
    self.vwDescLine.frame = frmVwDescLine;
    
    self.vwNoref.frame = frmvwRef;
    self.lblNoRef.frame = frmLblRef;
    self.lblRef.frame = frmLblRefTitle;
    [self.vwNoref addSubview:[self getLineVew]];
    
    self.vwNominal.frame = frmvwNominal;
    self.lblNominalTitle.frame = frmtitleNominal;
    self.textFieldNominal.frame = frmtextNominal;
    self.vwNominalLine.frame = frmVwNomLine;
    
    self.vwNotifAttachment.frame = frmvwAttach;
    self.lblFilename.frame = frmLblFileAttach;
    
    self.vwUpload.frame = frmvwUpload;
    self.vwUpload.layer.borderColor = [UIColorFromRGB(0xf0dd71)CGColor];
    self.vwUpload.layer.borderWidth = 1;
    
    
    self.vwBtnUplod.frame = frmBtnUplod;
    self.imgUpload.frame = frmImgUplod;
//    self.imgUpload.layer.borderWidth = 1;
    self.lblUpload.frame = frmLblUpload;
    
    self.btKirim.frame = frmBtnKirim;
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.vwScrool setContentSize:CGSizeMake(self.vwMenu.frame.size.width, self.vwMenu.frame.size.height)];
    
}

-(CGFloat)labelHeight:(UILabel *)label withText:(NSString *)text{

    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName:label.font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){label.frame.size.width, CGFLOAT_MAX}
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                           context:nil];

    return ceil(rect.size.height);
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (BOOL) textFieldDidChange:(UITextField *)textField
{
    self.txtDeskripsi.inputView = objPickerAccount;
    self.txtDeskripsi.inputAccessoryView = toolBar;
    
    return YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [listAction count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    
        NSString *hasil = [[NSString alloc]init];
        hasil = [[listAction objectAtIndex:row] valueForKey:@"reason"];
//        subject = [[listAction objectAtIndex:row] valueForKey:@"subject"];
//
    return hasil;
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
        
    NSString *strAccount = [[NSString alloc]init];
    
    strAccount = [[listAction objectAtIndex:row] valueForKey:@"reason"];
//    id_desc = [[listAction objectAtIndex:row] valueForKey:@"id"];
//    subject = [[listAction objectAtIndex:row]valueForKey:@"subject"];
    [dataManager.dataExtra setValue:[[listAction objectAtIndex:0] valueForKey:@"id"] forKey:@"id_desc"];
    [dataManager.dataExtra setValue:[[listAction objectAtIndex:0] valueForKey:@"subject"] forKey:@"subject"];
    
    self.txtDeskripsi.text = strAccount;
    
    if([[[listAction objectAtIndex:row] valueForKey:@"extra_field"] isEqualToString:@"1"] ) {
        self.vwNominal.hidden = NO;
        self.textFieldNominal.inputAccessoryView = toolBar;
    }else{
        self.vwNominal.hidden = YES;
    }
    
    [self newRelocation];

}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"verify_reason"]){
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
            if([[object objectForKey:@"lid"]isEqualToString:@"0"]){
                NSString *messageAlert = @"";
                if([lang isEqualToString:@"id"]){
                    [self.btKirim setTitle:@"Kembali" forState:UIControlStateNormal];
                    messageAlert = @"Tidak ada kategori yang tersedia untuk transaksi ini";
                }else{
                    [self.btKirim setTitle:@"Back" forState:UIControlStateNormal];
                    messageAlert = @"Problem Category not found for this transaction";
                }
                buttonState = @"0";
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")
                                   message:messageAlert
                                   preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                    [self backToR];
                }];

                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                buttonState = @"1";
            }
            
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [dataManager.dataExtra setValue:[object objectForKey:@"lid"] forKey:@"lid"];
            [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_reason,lid"] needLoading:false encrypted:false banking:false favorite:false];

        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    if([requestType isEqualToString:@"list_reason"]){
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            listAction = [jsonObject objectForKey:@"response"];
            if(listAction.count == 1){
                self.txtDeskripsi.text = [[listAction objectAtIndex:0] valueForKey:@"reason"];
//                id_desc = [[listAction objectAtIndex:0] valueForKey:@"id"];
//                subject = [[listAction objectAtIndex:0] valueForKey:@"subject"];
                [dataManager.dataExtra setValue:[[listAction objectAtIndex:0] valueForKey:@"id"] forKey:@"id_desc"];
                [dataManager.dataExtra setValue:[[listAction objectAtIndex:0] valueForKey:@"subject"] forKey:@"subject"];
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    if([requestType isEqualToString:@"insert_complaint"]){
        if([jsonObject isKindOfClass:[NSDictionary class]]){
                NSString *masaj = @"";
                NSString *addon = @"";
                if([lang isEqualToString:@"id"]){
                    masaj = @"Pengaduan anda sudah terdaftar dengan No.";
                    addon = @"dan akan diproses pada hari kerja berikutnya dalam jangka waktu 5 hari kerja";
                }else{
                    masaj = @"Your complaint has been registered with No.";
                    addon = @"and will be processed on the next working day within a period of 5 working days";
                }
            
                if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
                    NSString *message = [NSString stringWithFormat:@"%@ %@ %@",masaj, [jsonObject objectForKey:@"response"], addon];

                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")
                                               message:message
                                               preferredStyle:UIAlertControllerStyleAlert];

                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                        [self gotoListComplain];
                    }];

                    [alert addAction:defaultAction];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                } else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
    }

}

- (void) gotoListComplain{
    ComplainViewController *popBantuan = [self.storyboard instantiateViewControllerWithIdentifier:@"COMPLAIN"];
     //                   [self presentViewController:popBantuan animated:YES completion:nil];
     
     [self.tabBarController setSelectedIndex:0];
     UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
     [currentVC pushViewController:popBantuan animated:YES];
     
    NSDictionary* userInfo = @{@"position": @(1118)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@synthesize description;

@end
