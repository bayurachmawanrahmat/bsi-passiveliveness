//
//  PLAKADViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 23/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

//#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PLAKADViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate : (id)newDelegate;
- (void) setAkadText : (NSString*)akadText;

@property (weak, nonatomic) IBOutlet UILabel *textAkad;
@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollingView;

@end


@protocol PLAKADDelegate

@required

- (void) clickedButtonAgreement : (BOOL) state;

@end

NS_ASSUME_NONNULL_END
