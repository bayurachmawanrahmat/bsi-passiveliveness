//
//  AsmaulHusnaViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/25/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "AsmaulHusnaViewController.h"
#import "CellAsmaulHusna.h"

@interface AsmaulHusnaViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *arrDataAsmaul;
    NSString *lang;
    NSUserDefaults *userDefault;
    NSMutableArray *heightRow;
    NSString *menuTitle;
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblAsmaulHusna;

@end

@implementation AsmaulHusnaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefault = [NSUserDefaults standardUserDefaults];
    arrDataAsmaul = [[NSArray alloc]init];
    heightRow = [[NSMutableArray alloc]init];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if (menuTitle != nil) {
        self.lblTitle.text = menuTitle;
    }else{
        if ([lang isEqualToString:@"id"]) {
            [self.lblTitle setText:@"Asmaul Husna"];
        }else{
            [self.lblTitle setText:@"Asmaul Husna"];
        }
    }

    [self setupLayout];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"asmaul_husna" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSError *error;
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error == nil) {
        arrDataAsmaul = json;
    }
    
    self.tblAsmaulHusna.delegate = self;
    self.tblAsmaulHusna.dataSource = self;
    [self.tblAsmaulHusna setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
//    [self.tblAsmaulHusna reloadData];
    [self.tblAsmaulHusna beginUpdates];
    [self.tblAsmaulHusna endUpdates];
    
//    NSDictionary* userInfo = @{@"position": @(1118)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    
    
    
//
}

-(void) setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmTblAsmaulHusna = self.tblAsmaulHusna.frame;
    CGRect frmImgIconTitle = self.imgIconTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmTblAsmaulHusna.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmTblAsmaulHusna.origin.x = 0;
    frmTblAsmaulHusna.size.width = SCREEN_WIDTH;
    frmTblAsmaulHusna.size.height = SCREEN_HEIGHT - frmTblAsmaulHusna.origin.y - BOTTOM_NAV_DEF;
//    if ([IPHONE_X || IPHONE_XS_MAX]) {
    if ([Utility isDeviceHaveBottomPadding]) {
        frmTblAsmaulHusna.size.height = SCREEN_HEIGHT - frmTblAsmaulHusna.origin.y - BOTTOM_NAV_NOTCH;
    }
    
//    frmLblTitle.origin.x = frmImgIconTitle.origin.x + frmImgIconTitle.size.width + 16;
//    frmLblTitle.size.width = frmVwTitle.size.width - frmLblTitle.origin.x - 16;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.size.height = 40;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmVwTitle.size.width - (frmLblTitle.origin.x*2);
    
    self.vwTitle.frame = frmVwTitle;
    self.tblAsmaulHusna.frame = frmTblAsmaulHusna;
    self.lblTitle.frame = frmLblTitle;
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrDataAsmaul.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellAsmaulHusna *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if(cell == nil){
        cell = [[CellAsmaulHusna alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    
    NSDictionary *dataAsmaulHusna = [arrDataAsmaul objectAtIndex:indexPath.row];
    [cell.lblNoAsmaul setText:[[dataAsmaulHusna objectForKey:@"number"]stringValue]];
    [cell.lblArtiAsmaul setText:[dataAsmaulHusna valueForKey:@"transliteration"]];
    if ([lang isEqualToString:@"id"]) {
        [cell.lblAsmaul setText:[dataAsmaulHusna valueForKey:@"id"]];
    }else{
        [cell.lblAsmaul setText:[dataAsmaulHusna valueForKey:@"en"]];
    }
    [cell.lblArabAsmaul setText:[dataAsmaulHusna valueForKey:@"name"]];
    [cell.lblArabAsmaul setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:20]];
    
    if (indexPath.row == 84) {
        [cell.lblArabAsmaul setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
//        [cell.lblArtiAsmaul setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
//        [cell.lblAsmaul setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    }
    
    if (IPHONE_5) {
        [cell.lblArabAsmaul setFont:[UIFont fontWithName:const_font_name3 size:17]];
            
            if (indexPath.row == 84) {
                [cell.lblArabAsmaul setFont:[UIFont fontWithName:const_font_name3 size:11]];
        //        [cell.lblArtiAsmaul setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
        //        [cell.lblAsmaul setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
            }
    }
   
    [cell.lblNoAsmaul sizeToFit];
    
    [cell.lblAsmaul sizeToFit];
    cell.lblAsmaul.numberOfLines = 0;
    cell.lblAsmaul.lineBreakMode = NSLineBreakByWordWrapping;
    
    [cell.lblArtiAsmaul sizeToFit];
    cell.lblArtiAsmaul.numberOfLines = 2;
    
    [cell.lblArabAsmaul sizeToFit];
    
    CGRect frmImgBorder = cell.imgBorder.frame;
    CGRect frmLblNoAsmaul = cell.lblNoAsmaul.frame;
    CGRect frmLblArab = cell.lblArabAsmaul.frame;
    CGRect frmLblAsmaul = cell.lblAsmaul.frame;
    CGRect frmLblArtiAsmaul = cell.lblArtiAsmaul.frame;
//
    frmLblNoAsmaul.origin.x = ((frmImgBorder.origin.x
                                + frmImgBorder.size.width/2) - frmLblNoAsmaul.size.width/2);
    frmLblNoAsmaul.origin.y = ((frmImgBorder.origin.y
                                + frmImgBorder.size.height/2) - frmLblNoAsmaul.size.height/2);
//
    frmLblArab.origin.x = cell.frame.size.width - frmLblArab.size.width - 8;
    frmLblArab.origin.y = cell.frame.size.height/2 - frmLblArab.size.height/2;
    
    frmLblArtiAsmaul.origin.x = frmImgBorder.origin.x + frmImgBorder.size.width +5;
    frmLblArtiAsmaul.size.width = (frmLblArab.origin.x - frmLblArtiAsmaul.origin.x)- 8;
    
    frmLblAsmaul.origin.x = frmLblArtiAsmaul.origin.x;
    frmLblAsmaul.origin.y = frmLblArtiAsmaul.origin.y + frmLblArtiAsmaul.size.height + 5;
    frmLblAsmaul.size.width = (frmLblArab.origin.x - frmLblAsmaul.origin.x)- 16;
    
    
//
    cell.lblNoAsmaul.frame = frmLblNoAsmaul;
    cell.lblArabAsmaul.frame = frmLblArab;
    cell.lblArtiAsmaul.frame = frmLblArtiAsmaul;
    cell.lblAsmaul.frame = frmLblAsmaul;
    
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSDictionary *dataAsmaulHusna = [arrDataAsmaul objectAtIndex:indexPath.row];
//    NSString *text =@"";
//    if ([lang isEqualToString:@"id"]) {
//        text = [NSString stringWithFormat:@"%@\n%@", [dataAsmaulHusna valueForKey:@"transliteration"], [dataAsmaulHusna valueForKey:@"id"]];
//    }else{
//        text = [NSString stringWithFormat:@"%@\n%@", [dataAsmaulHusna valueForKey:@"transliteration"], [dataAsmaulHusna valueForKey:@"en"]];
//    }
//    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
//    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:19.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
//    CGFloat height = MAX(size.height, 33.0);
//    return height+20.0;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGFloat height = 78;
//    if (heightRow.count > 0) {
//        height = [[heightRow objectAtIndex:indexPath.row-1]floatValue];
//    }
    if (IPHONE_5) {
        return 90;
    }else{
        return 78;
    }
    
}

-(void)handleTimer : (NSString *) modeStr{
    NSDictionary* userInfo = @{@"actionTimer": modeStr};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
//    [self handleTimer:@"STOP_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1113)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void) setTitleMenu : (NSString *) strTitle{
    menuTitle = strTitle;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
