//
//  RedirectCFViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 28/05/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "RedirectCFViewController.h"

@interface RedirectCFViewController (){
    BOOL isMenuID;
}

- (IBAction)ok:(id)sender;
@property (weak, nonatomic) NSString *redirecTo;
@property (weak, nonatomic) NSString *code;
@property (weak, nonatomic) NSString *confText;
@property (weak, nonatomic) NSString *titleText;
@property (weak, nonatomic) NSString *titleButton;
@property (weak, nonatomic) IBOutlet UIButton *buttonOk;
@property (weak, nonatomic) IBOutlet UITextView *textViewConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation RedirectCFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.lblTitle setText:_titleText];
    [Styles setTopConstant:_topConstraint];

    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.textViewConfirmation setFont:[UIFont fontWithName:const_font_name1 size:15]];
    [self.textViewConfirmation setText:_confText];
    
    [self.buttonOk setTitle:_titleButton forState:UIControlStateNormal];
}

- (IBAction)ok:(id)sender {
    if(isMenuID){
        if(_code != nil && ![_code isEqualToString:@""]){
            [self openTemplateByMenuID:_redirecTo andCodeContent:_code];
        }else{
            [self openTemplateByMenuID:_redirecTo];
        }
    }else{
        [self openStatic:_redirecTo];
    }
}

- (void)redirectTo:(NSString *)redirecto andCode:(NSString *)code isMenuID:(BOOL)ismenuid{
    isMenuID = ismenuid;
    _redirecTo = redirecto;
    _code = code;
}

- (void)setTextConfirmation:(NSString *)text{
    _confText = text;
}

- (void)setTitle:(NSString *)title{
    _titleText = title;
}

- (void)setTitleButton:(NSString *)titleBtn{
    _titleButton = titleBtn;
}



@end
