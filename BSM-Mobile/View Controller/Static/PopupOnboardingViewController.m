//
//  PopupOnboardingViewController.m
//  BSM-Mobile
//
//  Created by BSM on 8/25/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PopupOnboardingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "PopupActivationViewController.h"
#import "CustomBtn.h"

@interface PopupOnboardingViewController (){
    NSUserDefaults *userDefault;
    NSString *lang;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgOnboard;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet CustomBtn *btnOpenRek;
@property (weak, nonatomic) IBOutlet UIButton *btnActivasi;

@property (weak, nonatomic) IBOutlet UIView *vwMainTac;
@property (weak, nonatomic) IBOutlet UIView *vwContentTac;

- (IBAction)actionOpenRek:(id)sender;
- (IBAction)actionGoToActivasi:(id)sender;


@end

@implementation PopupOnboardingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (@available(iOS 13.0, *)) {
           self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [self.lblTitle setText:@"Buat Rekening Bank\n Syariah Indonesia Anda"];
        [self.lblDesc setText:@"Dapatkan rekening Anda dan nikmati\n pengalaman #lebihberkah bersama\n Bank Syariah Indonesia"];
        [self.btnOpenRek setTitle:@"Buka Rekening" forState:UIControlStateNormal];
        [self.btnActivasi setTitle:@"Sudah memiliki rekening" forState:UIControlStateNormal];
    }else{
        [self.lblTitle setText:@"Open Account Bank\nSyariah Indonesia"];
        [self.lblDesc setText:@"Get your account and get the\n #lebihberkah experience with\n Bank Syariah Indonesia"];
        [self.btnOpenRek setTitle:@"Open Account" forState:UIControlStateNormal];
        [self.btnActivasi setTitle:@"Already have an account" forState:UIControlStateNormal];
    }
    
//    [self setupLayout];
    
    [self.vwMainTac addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissPopup)]];
    [self.vwContentTac addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissPopup)]];
    [self.imgOnboard addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissPopup)]];
    
    self.vwContentTac.layer.cornerRadius = 16;
    self.btnOpenRek.layer.cornerRadius = 20;
    
}

-(void) dismissPopup{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:NO completion:^{
        PopupActivationViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupActivationVC"];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
    
}


- (IBAction)actionOpenRek:(id)sender {
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
    
    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"OPNREKVC"];
    vc.modalPresentationStyle = UIModalPresentationFullScreen;
    
    UIViewController *pop =  [self topMostController];
    
    [pop presentViewController:vc animated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^(void){
        });
    }];
}

- (IBAction)actionGoToActivasi:(id)sender {
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
    firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:firstVC animated:NO completion:^{
        [self->userDefault setObject:@"NO" forKey:@"fromSliding"];
        PopupActivationViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupActivationVC"];
        [viewCont setDelegate:[self topViewControllerWithRootViewController:firstVC.topViewController]];
        [firstVC presentViewController:viewCont animated:YES completion:nil];
    }];
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}




@end
