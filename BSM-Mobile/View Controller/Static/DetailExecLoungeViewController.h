//
//  DetailExecLoungeViewController.h
//  BSM-Mobile
//
//  Created by BSM on 12/11/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "RootViewController.h"

@interface DetailExecLoungeViewController : RootViewController
@property (nonatomic, strong) NSDictionary *data;
@end
