//
//  ListSurahViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ListSurahViewController.h"
#import "QuranHelper.h"
#import "Utility.h"
#import "CellListSurah.h"
#import "JuzAmmaSingleViewController.h"


@interface ListSurahViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    NSString *lang;
    NSUserDefaults *userDefault;
    BOOL isFilterSurah;
    
    NSArray *listSurah;
    
    QuranHelper *mQuranHelper;
    
    NSString *menuTitle;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwSearch;
@property (weak, nonatomic) IBOutlet UITableView *tblListSurah;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSurah;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation ListSurahViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if (menuTitle != nil) {
        self.lblTitle.text = menuTitle;
    }
    
    self.tblListSurah.delegate = self;
    self.tblListSurah.dataSource = self;
    
    if ([lang isEqualToString:@"id"]) {
        self.txtFieldSurah.placeholder = @"Cari Ayat";
        [self.lblTitle setText:@"Pilih Ayat"];
        
    }else{
        self.txtFieldSurah.placeholder = @"Find Verse";
        [self.lblTitle setText:@"Choose Verse"];
    }
    [self.lblTitle sizeToFit];
    
    self.txtFieldSurah.delegate = self;
    self.txtFieldSurah.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self.tblListSurah setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    self.tblListSurah.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    isFilterSurah = false;
    listSurah = [[NSArray alloc]init];
    mQuranHelper = [[QuranHelper alloc] init];
    
    listSurah = [mQuranHelper readListSurah:1];
    
    [self setupLayout];
    
//    NSDictionary* userInfo = @{@"position": @(1118)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}

-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmVwSearch = self.vwSearch.frame;
    CGRect frmTblListSurah = self.tblListSurah.frame;
    
    CGRect frmImgIcon = self.imgIcon.frame;
    CGRect frmTxtField = self.txtFieldSurah.frame;
    CGRect frmVwLine = self.vwLine.frame;
    
    CGRect frmLblTitle = self.lblTitle.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmVwTitle.size.width - 32;
    frmLblTitle.size.height = 40;
    
    frmVwSearch.origin.x = 0;
    frmVwSearch.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    frmVwSearch.size.width = SCREEN_WIDTH;
    
    frmTxtField.origin.x = frmImgIcon.origin.x + frmImgIcon.size.width + 8;
    frmTxtField.size.width = frmVwSearch.size.width - frmTxtField.origin.x - 8;
    
    frmVwLine.origin.x = frmImgIcon.origin.x;
    frmVwLine.size.width = frmVwSearch.size.width - frmVwLine.origin.x - 8;
    
    frmTblListSurah.origin.y = frmVwSearch.origin.y + frmVwSearch.size.height + 5;
    frmTblListSurah.origin.x = 0;
    frmTblListSurah.size.width = SCREEN_WIDTH;
    frmTblListSurah.size.height = SCREEN_HEIGHT - frmTblListSurah.origin.y - BOTTOM_NAV_DEF;
    
    if ([Utility isDeviceHaveNotch]) {
        frmTblListSurah.size.height = SCREEN_HEIGHT - frmTblListSurah.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.vwSearch.frame = frmVwSearch;
    self.tblListSurah.frame = frmTblListSurah;
    self.txtFieldSurah.frame = frmTxtField;
    self.vwLine.frame = frmVwLine;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listSurah.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellListSurah *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if(cell == nil){
        cell = [[CellListSurah alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    
    NSDictionary *dataSurah = [listSurah objectAtIndex:indexPath.row];
    [cell.lblSurahName setText:[dataSurah valueForKey:@"surah_name"]];
    [cell.lblTranslateSurah setText:[NSString stringWithFormat:@"%@ Ayat | %@", [dataSurah valueForKey:@"num_ayah"],[dataSurah valueForKey:@"translate"]]];
    
    [cell.lblSurahName setFont:[UIFont fontWithName:const_font_name3 size:16.0f]];
    
    
    [cell.lblSurahName sizeToFit];
    
    [cell.lblTranslateSurah sizeToFit];
    cell.lblTranslateSurah.numberOfLines = 2;
    
    
    [cell.lblSurahName setFont:[UIFont fontWithName:const_font_name3 size:14.0f]];
    [cell.lblTranslateSurah setFont:[UIFont fontWithName:const_font_name1 size:14.0f]];
    
    if (IPHONE_5) {
        [cell.lblSurahName setFont:[UIFont fontWithName:const_font_name3 size:12.0f]];
        [cell.lblTranslateSurah setFont:[UIFont fontWithName:const_font_name1 size:12.0f]];
    }
    
    
    CGRect frmLblSurahName =  cell.lblSurahName.frame;
    CGRect frmLblTransSurah = cell.lblTranslateSurah.frame;
    
    frmLblSurahName.origin.x = 16;
    frmLblSurahName.size.width = cell.frame.size.width - (frmLblSurahName.origin.x *2);
    
    frmLblTransSurah.origin.x = frmLblSurahName.origin.x;
    frmLblTransSurah.origin.y = frmLblSurahName.origin.y + frmLblSurahName.size.height + 5;
    frmLblTransSurah.size.width = frmLblSurahName.size.width;
    
    cell.lblSurahName.frame = frmLblSurahName;
    cell.lblTranslateSurah.frame = frmLblTransSurah;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return 68.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *dataToSend = [listSurah objectAtIndex:indexPath.row];
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"JUZAMSIGLE"];
    JuzAmmaSingleViewController *viewCont = (JuzAmmaSingleViewController *) templateView;
    [viewCont sendData:dataToSend];
    [self.navigationController pushViewController:templateView animated:YES];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *searchText = [NSString stringWithString:textField.text];
    searchText = [searchText
                  stringByReplacingCharactersInRange:range withString:string];
    if ([searchText length] > 0) {
        listSurah = [mQuranHelper readListSurahFilter:searchText type:1];
    }else{
        listSurah = [mQuranHelper readListSurah:1];
    }
    [self.tblListSurah reloadData];
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    textField.text = @"";
    [textField resignFirstResponder];
    listSurah = [mQuranHelper readListSurah:1];
    [self.tblListSurah reloadData];
    return NO;
}

- (void)viewWillDisappear:(BOOL)animated{
//    [self handleTimer:@"STOP_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1113)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void)handleTimer : (NSString *) modeStr{
    NSDictionary* userInfo = @{@"actionTimer": modeStr};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"featIsTimer" object:self userInfo:userInfo];
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
//    [self handleTimer:@"START_TIMER"];
    NSDictionary* userInfo = @{@"position": @(1112)};
       NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
       [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void) setTitleMenu : (NSString *) strTitle{
    menuTitle = strTitle;
}

@end
