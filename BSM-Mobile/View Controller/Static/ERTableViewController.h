//
//  ERTableViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "RootViewController.h"

@interface ERTableViewController : RootViewController

@property (nonatomic,retain) NSArray *kursArray;
@property (nonatomic,retain) NSArray *sellArray;
@property (nonatomic,retain) NSArray *buyArray;

@end
