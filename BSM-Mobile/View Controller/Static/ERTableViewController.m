//
//  ERTableViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "ERTableViewController.h"
#import "KursTableViewCell.h"
#import "RootViewController.h"
#import "Connection.h"

@interface ERTableViewController ()<ConnectionDelegate,UITableViewDataSource,UITableViewDelegate>{
    NSArray *listData;
}
@property (weak, nonatomic) IBOutlet UIView *vwKurs;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblKurs;
@property (weak, nonatomic) IBOutlet UILabel *lblMU;
@property (weak, nonatomic) IBOutlet UILabel *lblJS;
@property (weak, nonatomic) IBOutlet UILabel *lblBB;
@property (weak, nonatomic) IBOutlet UILabel *lblVMU;
@property (weak, nonatomic) IBOutlet UILabel *lblVJS;
@property (weak, nonatomic) IBOutlet UILabel *lblVBB;

@end

@implementation ERTableViewController

- (void)viewWillAppear:(BOOL)animated {
    NSString *paramData = [NSString stringWithFormat:@"request_type=list_er"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Info Kurs";
        self.lblMU.text = @"MATA UANG";
        self.lblJS.text = @"JUAL";
        self.lblBB.text = @"BELI";
    } else {
        self.lblTitle.text = @"Exchange Rate";
        self.lblMU.text = @"CURRENCY";
        self.lblJS.text = @"SELL";
        self.lblBB.text = @"BUY";
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    CGRect frmKurs = self.vwKurs.frame;
    frmKurs.size.width = screenWidth;
    self.vwKurs.frame = frmKurs;
    
    self.tblKurs.dataSource = self;
    self.tblKurs.delegate = self;
    
    CGRect frmTable = self.tblKurs.frame;
    frmTable.size.width = screenWidth;
    self.tblKurs.frame = frmTable;
    
    CGRect frmMU = _lblMU.frame;
    frmMU.origin.x = 8;
    _lblMU.frame = frmMU;
    
    CGRect frmJS = _lblJS.frame;
    frmJS.origin.x = (screenWidth/2) - (frmJS.size.width/2);
    _lblJS.frame = frmJS;
    
    CGRect frmBB = _lblBB.frame;
    frmBB.origin.x = (screenWidth) - (frmBB.size.width) - 8;;
    _lblBB.frame = frmBB;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //KursTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KursCell" forIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"KursCell" forIndexPath:indexPath];
    
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    
    // Configure the cell...
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    UILabel *lblMataUang = (UILabel *)[cell viewWithTag:101];
    lblMataUang.text = [data objectForKey:@"mata_uang"];
    
    CGRect frmVMU = lblMataUang.frame;
    frmVMU.origin.x = 8;
    lblMataUang.frame = frmVMU;
    
    UILabel *lblHargaJual = (UILabel *)[cell viewWithTag:102];
    lblHargaJual.text = [data objectForKey:@"harga_jual"];
    
    CGRect frmVHJ = lblHargaJual.frame;
    frmVHJ.origin.x = (screenWidth/2) - (frmVHJ.size.width/2);
    lblHargaJual.frame = frmVHJ;
    
    UILabel *lblHargaBeli = (UILabel *)[cell viewWithTag:103];
    lblHargaBeli.text = [data objectForKey:@"harga_beli"];

    CGRect frmVHB = lblHargaBeli.frame;
    frmVHB.origin.x = (screenWidth) - (frmVHB.size.width) - 8;
    lblHargaBeli.frame = frmVHB;
    
    return cell;
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
        if (![requestType isEqualToString:@"check_notif"]) {
            if([jsonObject isKindOfClass:[NSDictionary class]]){
                NSArray *er = [jsonObject objectForKey:@"response"];
                NSMutableArray *tempER = [[NSMutableArray alloc] init];
                for (NSDictionary *data in er){
                    NSMutableDictionary *dictX = [NSMutableDictionary new];
                    [dictX setValue:[data objectForKey:@"buyrate"] forKey:@"harga_beli"];
                    [dictX setValue:[data objectForKey:@"midrate"] forKey:@"harga_tengah"];
                    [dictX setValue:[data objectForKey:@"sellrate"] forKey:@"harga_jual"];
                    [dictX setValue:[data objectForKey:@"id"] forKey:@"mata_uang"];
                    [tempER addObject:dictX];
                }
                
                if ([tempER count] > 0) {
                    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
                    listData = [tempER sortedArrayUsingDescriptors:@[descriptor]];
                    [self.tblKurs reloadData];
                }
                else {
                    NSString *msg = @"";
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                    
                    if([lang isEqualToString:@"id"]) {
                        msg = @"Permintaan tidak dapat diproses";
                    } else {
                        msg = @"Request time out";
                    }
                    
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }else{
                listData = (NSArray *)jsonObject;
                [self.tblKurs reloadData];
            }
            
        }
    }
    else {
        /*NSString *msg = @"";
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        
        if([lang isEqualToString:@"id"]) {
            msg = @"Permintaan tidak dapat diproses";
        } else {
            msg = @"Request time out";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];*/
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)reloadApp{
    [BSMDelegate reloadApp];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
