//
//  NLoginViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/29/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "NLoginViewController.h"
#import "TemplateViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "Connection.h"
#import "Biometric.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+MD5.h"
#import <LocalAuthentication/LocalAuthentication.h>
#import "Utility.h"
#import "InboxHelper.h"
#import "ScrollHomeViewController.h"
#import "AppProperties.h"

@interface NLoginViewController()<UIAlertViewDelegate, ConnectionDelegate> {
    Biometric *bm;
    int ctrlog;
    NSString *flagTPLVC;
    NSUserDefaults *userDefault;
    InboxHelper *inboxHelper;
}
@property (weak, nonatomic) IBOutlet UIView *vwLoginBox;
@property (weak, nonatomic) IBOutlet UIImageView *imgBSMLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *bgScreen;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)checkLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEye;
- (IBAction)togglePwd:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UILabel *lblBioLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnBioLogin;
-(IBAction)callBioLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblLupa;
@property (weak, nonatomic) IBOutlet UIButton *btnLupa;
-(IBAction)forgetPwd:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblGreet;
@property (weak, nonatomic) IBOutlet UIButton *btnLock;
@property (weak, nonatomic) IBOutlet UILabel *lblGreeting;
- (IBAction)checkNLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *vwLogin;

@end

@implementation NLoginViewController

- (void)viewWillAppear:(BOOL)animated {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *firstLogin = [userDefault objectForKey:@"firstLogin"];
    
    bm = [Biometric objBiometric];
    if (bm.isFaceIDAvailable == NO) {
        if (bm.isTouchIDAvailable == YES) {
            [self showBiometricLogin];
        }
    } else {
        if ([firstLogin isEqualToString:@"YES"]) {
            [self showBiometricLogin];
        }
    }
    
    //[self openTappedNotification];
}

- (void)viewDidAppear:(BOOL)animated{
//    NSString *strUrl = [NSString stringWithFormat:@"request_type=check_notif,customer_id"];
//    Connection *conn = [[Connection alloc]initWithDelegate:self];
//    [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:false];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    ctrlog = 0;
    self.parentViewController.tabBarController.tabBar.hidden = YES;
    self.tabBarController.tabBar.hidden = YES;
    flagTPLVC = @"HIDE";
    bm = [Biometric objBiometric];
    [self.lblGreet setHidden:YES];
    [self.lblGreeting setHidden:YES];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    inboxHelper = [[InboxHelper alloc]init];
    
    [userDefault setObject:@"LOGIN" forKey:@"VC"];
    [userDefault synchronize];
    
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Kata Sandi";
        self.lblLupa.text = @"Lupa Kata Sandi?";
        self.lblGreeting.text = @"selamat datang";
        [self.txtPassword setPlaceholder:@"Kata Sandi"];
        [self.btnLogin setTitle:@"Masuk" forState:UIControlStateNormal];

        if (bm.isTouchIDAvailable == YES) {
            self.lblBioLogin.text = @"Masuk dengan Touch ID";
        } else if (bm.isFaceIDAvailable == YES) {
            self.lblBioLogin.text = @"Masuk dengan Face ID";
        }
    } else {
        self.lblTitle.text = @"Password";
        self.lblLupa.text = @"Forgot password?";
        self.lblGreeting.text = @"welcome";
        [self.txtPassword setPlaceholder:@"Password"];
        [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
        if (bm.isTouchIDAvailable == YES) {
            self.lblBioLogin.text = @"Login with Touch ID";
        } else if (bm.isFaceIDAvailable == YES) {
            self.lblBioLogin.text = @"Login with Face ID";
        }
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmBox = self.vwLoginBox.frame;
    CGRect frmScreen = self.bgScreen.frame;
    CGRect frmButton = self.btnLogin.frame;
    CGRect frmLogo = self.imgBSMLogo.frame;
    CGRect frmText = self.txtPassword.frame;
    //CGRect frmLabel = self.lblTitle.frame;
    CGRect frmEye = self.btnEye.frame;
    CGRect frmLine = self.vwLine.frame;
    CGRect frmLblBio = self.lblBioLogin.frame;
    CGRect frmBtnBio = self.btnBioLogin.frame;
    CGRect frmLblLupa = self.lblLupa.frame;
    CGRect frmBtnLupa = self.btnLupa.frame;
    CGRect frmLblGreet = self.lblGreet.frame;
    CGRect frmLblGreeting = self.lblGreeting.frame;
    CGRect frmLock = self.btnLock.frame;
    CGRect frmLogin = self.vwLogin.frame;
    
    frmScreen.size.height = screenHeight;
    frmScreen.size.width = screenWidth;
    frmBox.size.width = screenWidth - 32;
    frmBox.origin.y = screenHeight/2 - frmBox.size.height;
    
    frmLogo.origin.x = frmBox.size.width - frmLogo.size.width - 15;
    frmLogo.origin.y = 10;
    
    //frmLblGreet.origin.x = frmBox.size.width/2 - frmLblGreet.size.width/2;
    frmLblGreet.size.width = frmBox.size.width - 16;
    frmLblGreet.origin.y = frmLogo.origin.y + frmLogo.size.height + 10;

    //frmLblGreeting.origin.x = frmBox.size.width/2 - frmLblGreeting.size.width/2;
    frmLblGreeting.size.width = frmBox.size.width - 16;
    frmLblGreeting.origin.y = frmLblGreet.origin.y + frmLblGreet.size.height + 5;
    
    frmText.origin.y = frmLblGreeting.origin.y + frmLblGreeting.size.height + 20;
    frmText.origin.x = frmLock.origin.x + frmLock.size.width + 10;
    frmText.size.width = frmBox.size.width - frmText.origin.x - frmEye.size.width - 15;
    
    //frmLabel.origin.y = frmText.origin.y - frmLabel.size.height;
    //frmLabel.origin.x = 10;
    
    frmEye.origin.x = screenWidth - 55 - frmEye.size.width;
    frmEye.origin.y = frmText.origin.y + 5;
    
    frmLock.origin.y = frmText.origin.y + 5;
    
    frmLine.origin.y = frmText.origin.y + frmText.size.height;
    frmLine.origin.x = 15;
    frmLine.size.width = frmBox.size.width - 30;
    
    //frmButton.origin.y = frmBox.size.height - frmButton.size.height - 20;
    frmLblLupa.origin.y = frmLine.origin.y + 1;
    frmLblLupa.size.width = frmLine.size.width;
    
    frmBtnLupa.origin.y = frmLblLupa.origin.y;
    frmBtnLupa.size.width = frmLblLupa.size.width;
    
    frmButton.origin.y = frmBox.origin.y + frmBox.size.height - frmButton.size.height/2;
    frmButton.origin.x = frmBox.size.width/2 - frmButton.size.width/2 + frmBox.origin.x;
    //frmButton.size.width = frmBox.size.width - 20;
    
    frmBtnBio.origin.x = 10;
    frmBtnBio.origin.y = frmBtnLupa.origin.y + frmBtnLupa.size.height + 25;
    //frmBtnBio.origin.y = frmBox.size.height - frmBtnBio.size.height;
    frmBtnBio.size.width = frmBox.size.width - 20;
    frmLblBio.origin.x = 10;
    frmLblBio.origin.y = frmBtnBio.origin.y;
    frmLblBio.size.width = frmBtnBio.size.width;
    
    frmLogin.size.height = screenHeight;
    
    self.vwLoginBox.frame = frmBox;
    self.bgScreen.frame = frmScreen;
    self.btnLogin.frame = frmButton;
    self.imgBSMLogo.frame = frmLogo;
    self.txtPassword.frame = frmText;
    //self.lblTitle.frame = frmLabel;
    self.btnEye.frame = frmEye;
    self.vwLine.frame = frmLine;
    self.lblLupa.frame = frmLblLupa;
    self.btnLupa.frame = frmBtnLupa;
    self.lblBioLogin.frame = frmLblBio;
    self.btnBioLogin.frame = frmBtnBio;
    self.lblGreet.frame = frmLblGreet;
    self.lblGreeting.frame = frmLblGreeting;
    self.btnLock.frame = frmLock;
    self.vwLogin.frame = frmLogin;
    
    [self.vwLogin bringSubviewToFront:_btnLogin];
    
    self.btnLogin.layer.shadowColor = [[UIColor grayColor] CGColor];
    self.btnLogin.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.btnLogin.layer.shadowOpacity = 1.0f;
    self.btnLogin.layer.shadowRadius = 7.0;
    self.btnLogin.layer.masksToBounds = NO;
    self.btnLogin.layer.cornerRadius = 16.0f;
    
    self.vwLoginBox.layer.shadowColor = [[UIColor grayColor] CGColor];
    self.vwLoginBox.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.vwLoginBox.layer.shadowOpacity = 2.0f;
    self.vwLoginBox.layer.shadowRadius = 7.0;
    self.vwLoginBox.layer.masksToBounds = NO;
    self.vwLoginBox.layer.cornerRadius = 16.0f;
    
    //self.vwLogin.layer.borderColor = [[UIColor blackColor] CGColor];
    //self.vwLogin.layer.borderWidth = 1.0f;
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.txtPassword.inputAccessoryView = keyboardDoneButtonView;
    [self.txtPassword setSecureTextEntry:true];
    
    if (bm.isFaceIDAvailable == NO) {
        if (bm.isTouchIDAvailable == YES) {
            [self.lblBioLogin setHidden:NO];
            [self.btnBioLogin setHidden:NO];
        } else {
            [self.lblBioLogin setHidden:YES];
            [self.btnBioLogin setHidden:YES];
        }
    } else {
        //[self.lblBioLogin setHidden:YES];
        //[self.btnBioLogin setHidden:YES];
        [self.lblBioLogin setHidden:NO];
        [self.btnBioLogin setHidden:NO];
    }
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)callBioLogin:(id)sender {
    [self showBiometricLogin];
}

- (void)showBiometricLogin {
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString = @"";
    
    NSString *langLG = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    bm = [Biometric objBiometric];
    if([langLG isEqualToString:@"id"]){
        if (bm.isTouchIDAvailable == YES) {
            myLocalizedReasonString = @"Masuk dengan Touch ID";
        } else if (bm.isFaceIDAvailable == YES) {
            myLocalizedReasonString = @"Masuk dengan Face ID";
        }
    } else {
        if (bm.isTouchIDAvailable == YES) {
            myLocalizedReasonString = @"Use Touch ID to Continue";
        } else if (bm.isFaceIDAvailable == YES) {
            myLocalizedReasonString = @"Use Face ID to Continue";
        }
    }
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    NSLog(@"Success Login with Touch / Face ID");
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSString *langLG = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                                        NSString *msg1 = @"";
                                        bm = [Biometric objBiometric];
                                        if([langLG isEqualToString:@"id"]){
                                            if (bm.isTouchIDAvailable == YES) {
                                                msg1 = @"Touch ID berhasil.";
                                            } else if (bm.isFaceIDAvailable == YES) {
                                                msg1 = @"Face ID berhasil.";
                                            }
                                        } else {
                                            if (bm.isTouchIDAvailable == YES) {
                                                msg1 = @"Touch ID success.";
                                            } else if (bm.isFaceIDAvailable == YES) {
                                                msg1 = @"Face ID success.";
                                            }
                                        }
                                        
                                        [self loginApp];
                                        /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login" message:msg1 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                         alertView.tag = 212;
                                         [alertView show];*/
                                    });
                                } else {
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        NSString *errMsg = @"";
                                        bm = [Biometric objBiometric];
                                        NSString *langLG = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                                        if([langLG isEqualToString:@"id"]){
                                            switch([error code]) {
                                                case kLAErrorAuthenticationFailed:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"Gagal otentikasi. Touch ID tidak cocok";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"Gagal otentikasi. Face ID tidak cocok";
                                                    }
                                                    break;
                                                case kLAErrorUserCancel:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorUserFallback:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorTouchIDNotEnrolled:
                                                    errMsg = @"Touch ID belum terdaftar";
                                                    break;
                                                case kLAErrorBiometryNotAvailable:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"Device ini tidak mendukung Touch ID";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"Device ini tidak mendukung Face ID";
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        } else {
                                            switch([error code]) {
                                                case kLAErrorAuthenticationFailed:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"Authentication failed\nTouch ID doesn't match.";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"Authentication failed\nFace ID doesn't match.";
                                                    }
                                                    break;
                                                case kLAErrorUserCancel:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorUserFallback:
                                                    //errMsg = @"Face/Touch ID otentikasi dibatalkan";
                                                    break;
                                                case kLAErrorTouchIDNotEnrolled:
                                                    errMsg = @"Touch ID not recognized.";
                                                    break;
                                                case kLAErrorBiometryNotAvailable:
                                                    if (bm.isTouchIDAvailable == YES) {
                                                        errMsg = @"This device doesn't support Touch ID.";
                                                    } else if (bm.isFaceIDAvailable == YES) {
                                                        errMsg = @"This device doesn't support Face ID";
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        
                                        if (![errMsg isEqualToString:@""]) {
                                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:errMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                            [alertView show];
                                            // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
                                        }
                                    });
                                }
                            }];
    } else {
        /*dispatch_async(dispatch_get_main_queue(), ^{
         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
         message:authError.description
         delegate:self
         cancelButtonTitle:@"OK"
         otherButtonTitles:nil, nil];
         [alertView show];
         // Rather than show a UIAlert here, use the error to determine if you should push to a keypad for PIN entry.
         });*/
    }
}

- (IBAction)forgetPwd:(id)sender {
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *whdr = @"";
    NSString *wmsg = @"";
    if([lang isEqualToString:@"id"]){
        whdr = @"Informasi";
        wmsg = @"Mohon lakukan instal ulang aplikasi untuk membuat kata sandi baru.";
    } else {
        whdr = @"Information";
        wmsg = @"Please reinstall the application, to create new password.";
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:whdr message:wmsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

- (IBAction)togglePwd:(id)sender {
    if ([flagTPLVC isEqualToString:@"SHOW"]) {
        flagTPLVC = @"HIDE";
        [self.txtPassword setSecureTextEntry:true];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPLVC isEqualToString:@"HIDE"]) {
        flagTPLVC = @"SHOW";
        [self.txtPassword setSecureTextEntry:false];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)checkNLogin:(id)sender {
    NSString *pwd = self.txtPassword.text;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    NSString *upwd = [userDefault objectForKey:@"password"];
    NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];

    if ([[pwd MD5] isEqualToString:upwd]) {
//        [userDefault setObject:@"YES" forKey:@"hasLogin"];
        [self loginApp];
//        [userDefault setObject:@"NO" forKey:@"firstLogin"];
//        [userDefault synchronize];
//
//        NSDictionary* userInfo = @{@"position": @(1112)};
//        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
//
//        MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
//        [menuVC awakeFromNib];
//
//        UITabBarController *tabCont = self.tabBarController;
//        UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
//        HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
//        [homeCont showGreeting];
//
//        self.parentViewController.tabBarController.tabBar.hidden = NO;
//        [self.slidingViewController resetTopViewAnimated:YES];
//        [self.navigationController popToRootViewControllerAnimated:YES];
//        if(self.parentViewController.tabBarController.selectedIndex != 0){
//            [self.parentViewController.tabBarController setSelectedIndex:0];
//        }
    } else {
        NSString *err_msg = @"";
        NSString *alert_title = @"";
        if([lang isEqualToString:@"id"]){
            err_msg = @"Kata Sandi Salah";
            alert_title = @"Informasi";
        } else {
            err_msg = @"Wrong Password";
            alert_title = @"Information";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alert_title message:err_msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)checkLogin:(id)sender {
    NSString *pwd = self.txtPassword.text;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    NSString *upwd = [userDefault objectForKey:@"password"];
    NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];

    if ([[pwd MD5] isEqualToString:upwd]) {
        
        [self loginApp];
                
    } else {
        
        NSString *err_msg = @"";
        NSString *alert_title = @"";
        if([lang isEqualToString:@"id"]){
            err_msg = @"Kata Sandi Salah";
            alert_title = @"Informasi";
        } else {
            err_msg = @"Wrong Password";
            alert_title = @"Information";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alert_title message:err_msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)loginApp {
    
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    [userDefault setObject:@"user_login" forKey:@"mlogin"];
    
    [userDefault setObject:@"YES" forKey:@"hasLogin"];
    [userDefault setObject:@"NO" forKey:@"firstLogin"];
    [userDefault setObject:@"HOME" forKey:@"tabPos"];
    [userDefault setObject:@"HOME" forKey:@"VC"];
    [userDefault setValue:@"NO" forKey:@"isExist"];
    [userDefault setObject:nil forKey:@"req_login"];
    [userDefault synchronize];
    
    [self goToHome];
}



#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 212) {
        [self loginApp];
    }else if(alertView.tag == 100){
        [self ValidasiActivation];
    }
}
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    else if([requestType isEqualToString:@"login"]){
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
//                [userDefault setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
                [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
                [userDefault setObject:@"YES" forKey:@"hasLogin"];
                [userDefault setObject:@"NO" forKey:@"firstLogin"];
                [userDefault setObject:@"HOME" forKey:@"tabPos"];
                [userDefault setObject:@"HOME" forKey:@"VC"];
                [userDefault setValue:@"NO" forKey:@"isExist"];
                [userDefault synchronize];
                
            }else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"0001"]){
                    alert.tag = 100;
                }
                [alert show];
            }
        }
    }
    
}

-(void) goToHome{
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    [menuVC awakeFromNib];
    
    UITabBarController *tabCont = self.tabBarController;
    UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
    
    ScrollHomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
    [homeCont showGreeting];
    [homeCont loadBannerView];
//    [homeCont loadBannerView2];
    
    self.parentViewController.tabBarController.tabBar.hidden = NO;
    [self.slidingViewController resetTopViewAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.parentViewController.tabBarController.selectedIndex != 0){
        [self.parentViewController.tabBarController setSelectedIndex:0];
    }
}


-(void)ValidasiActivation{
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    
    [dataManager resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.tabBarController setSelectedIndex:0];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"NO" forKey:@"fromSliding"];
//    [userDefault setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
    [userDefault synchronize];
    NSString *configIndicator = [userDefault objectForKey:@"config_indicator"];
    NSInteger mutasiMaxDays = [[userDefault valueForKey:@"config_mutation_range"]integerValue];
    NSInteger sessionTimeOut = [[userDefault valueForKey:@"config_timeout_session"]integerValue];
    if(configIndicator == nil || mutasiMaxDays == 0 || sessionTimeOut == 0){
        [self getConfigApp];
    }
    UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
    self.slidingViewController.topViewController = viewCont;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) getConfigApp{
    NSMutableURLRequest *request = [Utility BSMHeader:[NSString stringWithFormat:@"%@config.html",API_URL]];
    [request setHTTPMethod:@"GET"];
    NSString *menuAcceptRecentlyDef = @"00013,00017,00018,00025,00027,00028,00044,00049,00065,00066";
    @try {
        NSError *error;
        NSURLResponse *urlResponse;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if(error == nil){
            NSDictionary *mJsonObject = [NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&error];
            if(mJsonObject){
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.onboarding"] forKey:@"config_onboarding"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.indicator"] forKey:@"config_indicator"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];

                if ([[mJsonObject valueForKey:@"config.mutation.range"] integerValue] != 0) {
                     [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
                }else{
                    [userDefault setValue:@"15" forKey:@"config_mutation_range"];
                }
                
                if ([[mJsonObject valueForKey:@"config.timeout.session"] integerValue] != 0) {
                    [userDefault setObject:[mJsonObject valueForKey:@"config.timeout.session"] forKey:@"config_timeout_session"];
                }else{
                    [userDefault setValue:@"180" forKey:@"config_timeout_session"];
                }
                [userDefault setObject:[mJsonObject valueForKey:@"config.act.iccid"] forKey:@"config.act.iccid"];
                
                if ([[mJsonObject valueForKey:@"config.allow.recently"] isEqualToString:@""] || [mJsonObject valueForKey:@"config.allow.recently"] == nil) {
                    [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
                }else{
                    [userDefault setObject:[mJsonObject valueForKey:@"config.allow.recently"] forKey:@"config.allow.recently"];
                }
               
                
            }
        }else{
            NSLog(@"%@", error);
            [userDefault setObject:@(15) forKey:@"config_mutation_range"];
            [userDefault setObject:@(180) forKey:@"config_timeout_session"];
            [userDefault setObject:@(1) forKey:@"config.act.iccid"];
            [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
        }
        
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
        [userDefault setObject:@(15) forKey:@"config_mutation_range"];
        [userDefault setObject:@(180) forKey:@"config_timeout_session"];
        [userDefault setObject:@(1) forKey:@"config.act.iccid"];
        [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
    }
     [userDefault synchronize];
}



-(void) openTappedNotification{
    NSString *receiving = [userDefault valueForKey:@"RECEIVING"];
    
    if (receiving != nil) {
        if([receiving isEqualToString:@"1"]||[receiving isEqualToString:@"Instant Payment"]){
            NSLog(@"Instant Payment");
        }else if([receiving isEqualToString:@"2"] || [receiving isEqualToString:@"Website"]){
            NSLog(@"Website");
        }else{
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
            self.slidingViewController.topViewController = viewCont;
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    NSLog(@"%@", [error localizedDescription]);
}
- (void)reloadApp{
    
}
@end
