//
//  PopupAkadCRViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PopupAkadCRViewController.h"
#import "CustomBtn.h"
#import "Utility.h"
#import <WebKit/WebKit.h>

@interface PopupAkadCRViewController ()<UIScrollViewDelegate>{
    NSString *string;
    WKWebView *wkWebView;
    NSString *buttonTitle;
}
@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIView *webviewContainer;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *backButton;

@end

@implementation PopupAkadCRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    [self.btnBack addTarget:self action:@selector(actionAgree) forControlEvents:UIControlEventTouchUpInside];
    [self.btnBack setHidden:YES];
    
    [self.backButton setUserInteractionEnabled:YES];
    [self.backButton addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBack)]];
    
    if([[[userdefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        [self.titleBar setText:@"Akad Pembiayaan"];
        [self.btnBack setTitle:@"Setuju" forState:UIControlStateNormal];
    }else{
        [self.titleBar setText:@"Financing Agreements"];
        [self.btnBack setTitle:@"Agree" forState:UIControlStateNormal];
    }
    
    if(buttonTitle){
        [self.btnBack setTitle:buttonTitle forState:UIControlStateNormal];
    }
    
    [self.viewFrame.layer setCornerRadius:16];
    [self.viewFrame.layer setBorderWidth:1];
    [self.viewFrame.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    [self.viewFrame.layer setMasksToBounds:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    
    wkWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, _webviewContainer.frame.size.width, _webviewContainer.bounds.size.height)];
    [_webviewContainer addSubview:wkWebView];
    
    
    if(string){
        NSData *base = [NSData dataWithBase64EncodedString:string];
        NSString *path = [@"~/Documents/bsi_akad_pembiayaan.pdf" stringByExpandingTildeInPath];
        [base writeToFile:path atomically:YES];
        [[DataManager sharedManager].dataExtra setValue:path forKey:@"filepath"];
        
        NSURL *pdfURL = [[NSURL alloc]initFileURLWithPath:path];
        NSURLRequest *pdfReq = [[NSURLRequest alloc] initWithURL:pdfURL];
        
        [wkWebView loadRequest:pdfReq];
    }

    [wkWebView setUserInteractionEnabled:YES];
    wkWebView.scrollView.delegate = self;
    wkWebView.contentMode = UIViewContentModeScaleAspectFit;
    [wkWebView sizeToFit];
    wkWebView.autoresizesSubviews = YES;
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setData:(NSString *)data{
    string = data;
}

- (void) setButtonTitle:(NSString *) title{
    buttonTitle = title;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.contentOffset.y
       >=
       (scrollView.contentSize.height - scrollView.frame.size.height)){
        [self.btnBack setHidden:NO];
    }
    if(scrollView.contentOffset.y <= 0.0){
        [self.btnBack setHidden:YES];
    }
    
    [Utility resetTimer];
}

- (void) actionBack{
    [delegate completionAkadCR:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) actionAgree{
    [delegate completionAkadCR:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    //bg_logo_light.png
    //bg_base_light.jpg
}

@end
