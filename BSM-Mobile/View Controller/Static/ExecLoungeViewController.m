//
//  ExecLoungeViewController.m
//  BSM-Mobile
//
//  Created by BSM on 12/11/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "ExecLoungeViewController.h"
#import "CellTableData.h"
#import "DetailExecLoungeViewController.h"
#import "RootViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface ExecLoungeViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    NSArray *listData;
    NSArray *listLounge;
    NSDictionary *data;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double lat;
    double lng;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UITableView *tableData;
@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end

@implementation ExecLoungeViewController

NSArray *listNameEL;
NSArray *listAddressEL;
NSArray *listRangeEL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self currentLocationIdentifier];
}

- (void)fillData {
    listNameEL = @[@"Halim Lounge", @"Extreme Lounge"];
    listAddressEL = @[@"Bandara Halim Perdana Kusuma", @"Bandara Sukarno Hatta"];
    listRangeEL = @[@"5000 meter", @"20000 meter"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableData.dataSource = self;
    self.tableData.delegate = self;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]) {
        self.lbl.text = @"Lokasi Lounge Eksekutif";
        self.lblDesc.text = @"Lounge Eksekutif terdekat di lokasi anda";
    } else {
        self.lbl.text = @"Halal Executive Lounge";
        self.lblDesc.text = @"List of Executive Lounge near you";
    }
    //[self fillData];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 40;
    frmTitle.origin.x = 0;
    frmTitle.origin.y = 84;
    self.vwTitle.frame = frmTitle;
    
    CGRect frmDesc = self.vwDesc.frame;
    frmDesc.size.width = screenWidth;
    frmDesc.size.height = 45;
    frmDesc.origin.x = 0;
    frmDesc.origin.y = 124;
    self.vwDesc.frame = frmDesc;
    
    CGRect frmLabelDesc = self.lblDesc.frame;
    frmLabelDesc.size.width = screenWidth;
    frmLabelDesc.size.height = 37;
    frmLabelDesc.origin.x = 0;
    //frmLabelDesc.origin.y = 124;
    self.lblDesc.frame = frmLabelDesc;
    
    CGRect frmTable = self.tableData.frame;
    frmTable.size.width = screenWidth;
    frmTable.size.height = 448;
    frmTable.origin.x = 0;
    frmTable.origin.y = 170;
    self.tableData.frame = frmTable;
    
    self.tableData.dataSource = self;
    self.tableData.delegate = self;
    
    //[self.tableData reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)currentLocationIdentifier
{
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else{
        
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"Continue",nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert setDelegate:self];
    }
}

-(double) distanceCallculator:(double)lat1 long1:(double)lng1 lat2:(double)lat2 long2:(double)lng2 {
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:lat1 longitude:lng1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:lat2 longitude:lng2];
    double dMeters = [loc1 distanceFromLocation:loc2];
    return dMeters;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    
    // Lokasi Wisma Mandiri I
    //lat = -6.183712;
    //lng = 106.823641;
    
    NSString *paramData = [NSString stringWithFormat:@"request_type=list_executive_lounge,latitude=%f,longitude=%f",  lat, lng];
    NSLog(@"kirim location: %@", [NSString stringWithFormat:@"request_type=list_executive_lounge,latitude=%f,longitude=%f", lat, lng]);
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
}

-(void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError* error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    
    //The results from Google will be an array obtained from the NSDictionary object with the key "results".
    listLounge = [json objectForKey:@"results"];
    
    //Write out the data to the console.
    NSLog(@"Google Data: %@", listLounge);
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listLounge.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoungeCell" forIndexPath:indexPath];
    
    NSDictionary *data = [listLounge objectAtIndex:indexPath.row];
    
    UILabel *lblNamaLounge = (UILabel *)[cell viewWithTag:101];
    lblNamaLounge.text = [data objectForKey:@"name"];
    
    UILabel *lblAlamatLounge = (UILabel *)[cell viewWithTag:102];
    lblAlamatLounge.text = [data objectForKey:@"address"];
    
    UILabel *lblJarak = (UILabel *)[cell viewWithTag:103];
    double jarak = [[data objectForKey:@"distance"] doubleValue];
    if (jarak < 1000) {
        lblJarak.text = [NSString stringWithFormat:@"%.02f m",jarak];
    }
    else {
        lblJarak.text = [NSString stringWithFormat:@"%.02f km",(jarak / 1000)];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    data = [listLounge objectAtIndex:indexPath.row];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:data forKey:@"passingDataEL"];
    [self performSegueWithIdentifier:@"detailLoungeLocation" sender:self];
}

#pragma mark - Connection
-(void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            NSArray *lounge = [jsonObject objectForKey:@"lounge"];
            NSMutableArray *temp = [[NSMutableArray alloc] init];
            for (NSDictionary *data in lounge){
                NSMutableDictionary *dictX = [NSMutableDictionary new];
                [dictX setValue:[data objectForKey:@"lounge_address"] forKey:@"address"];
                [dictX setValue:[data objectForKey:@"lounge_name"] forKey:@"name"];
                [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
                [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
                [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
                double distance = [self distanceCallculator:lat long1:lng lat2:[[data objectForKey:@"latitude"] doubleValue] long2:[[data objectForKey:@"longitude"] doubleValue]];
                [dictX setValue:[NSNumber numberWithDouble: distance] forKey:@"distance"];
                [temp addObject:dictX];
            }
            
            if ([temp count] > 0) {
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
                listLounge = [temp sortedArrayUsingDescriptors:@[descriptor]];
                listData = listLounge;
                [self.tableData reloadData];
            } else {
                NSString *msg = @"";
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                
                if([lang isEqualToString:@"id"]) {
                    msg = @"Maaf tidak ada lounge di sekitar anda";
                } else {
                    msg = @"Sorry there are no lounges around you";
                }
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        } else if ([jsonObject isKindOfClass:[NSArray class]]){
            NSArray *lounge = jsonObject;
            NSMutableArray *temp = [[NSMutableArray alloc] init];
            for (NSDictionary *data in lounge){
                NSLog(@"--- %@", data);
                NSMutableDictionary *dictX = [NSMutableDictionary new];
                [dictX setValue:[data objectForKey:@"food_address"] forKey:@"address"];
                [dictX setValue:[data objectForKey:@"food_name"] forKey:@"name"];
                [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
                [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
                [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
                double distance = [self distanceCallculator:lat long1:lng lat2:[[data objectForKey:@"latitude"] doubleValue] long2:[[data objectForKey:@"longitude"] doubleValue]];
                [dictX setValue:[NSNumber numberWithDouble: distance] forKey:@"distance"];
                [temp addObject:dictX];
            }
            
            if ([temp count] > 0) {
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
                listLounge = [temp sortedArrayUsingDescriptors:@[descriptor]];
                listData = listLounge;
                [self.tableData reloadData];
            } else {
                NSString *msg = @"";
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                
                if([lang isEqualToString:@"id"]) {
                    msg = @"Maaf tidak ada lounge di sekitar anda";
                } else {
                    msg = @"Sorry there are no lounges around you";
                }
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
    }
}

-(void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    DataManager *dataManager;
    [dataManager resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

