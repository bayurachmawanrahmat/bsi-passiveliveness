//
//  PopupDetailComplainViewController.h
//  BSM-Mobile
//
//  Created by ARS on 24/01/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PopupDetailComplainViewController : UIViewController

-(void) dataBundle : (NSDictionary *) dataBind;
-(void) setImages : (UIImage *)passingImage;

@end

NS_ASSUME_NONNULL_END
