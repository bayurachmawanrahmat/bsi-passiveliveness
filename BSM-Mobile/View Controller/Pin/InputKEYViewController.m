//
//  InputPINViewController.m
//  BSM-Mobile
//
//  Created by Galih Suryo on 1/10/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "InputKEYViewController.h"
#import "Utility.h"

@interface InputKEYViewController ()<UITextFieldDelegate, UIAlertViewDelegate>
-(void) saveKEY:(NSString *) key_value;
- (IBAction)onSave:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *inputKEy;
@property (weak, nonatomic) IBOutlet UITextField *inpuReKEY;
- (IBAction)onCancel:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

@end

@implementation InputKEYViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _arrowImage.image = [_arrowImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_arrowImage setTintColor:[UIColor blackColor]];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)sendRegistration:(id)sender {
    
}

- (void) saveKEY:(NSString *)key_value {
    NSString *key = @"secret-key";
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:key_value forKey:key];
    
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Key telah tersimpan" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
}

- (IBAction)onSave:(id)sender {
    if (![[_inputKEy text] isEqualToString:@""] && [[_inputKEy text] isEqualToString:[_inpuReKEY text]]){
        [self saveKEY:[_inputKEy text]];
    } else if (![[_inputKEy text] isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Key can't be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Key not match (key and re-type key)" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
@end

