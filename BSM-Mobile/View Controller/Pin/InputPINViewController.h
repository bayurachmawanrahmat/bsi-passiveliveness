//
//  InputPINViewController.h
//  BSM-Mobile
//
//  Created by Galih Suryo on 1/10/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "RootViewController.h"
@protocol InputPINDelegate <NSObject>
- (void)onPINSuccess:(NSString*) pinBlock;
@end;
@interface InputPINViewController : UIViewController
@property (readwrite, weak, nonatomic) id <InputPINDelegate> delegate;
@end

