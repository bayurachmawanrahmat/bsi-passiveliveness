//
//  InputPINViewController.m
//  BSM-Mobile
//
//  Created by Galih Suryo on 1/10/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "InputPINViewController.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import <MessageUI/MessageUI.h>
#import "UIViewController+ECSlidingViewController.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "DESChiper.h"

@interface InputPINViewController ()<UITextFieldDelegate, ConnectionDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textFieldPIN;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

- (IBAction)onPINPressed:(UIButton *)sender;
- (IBAction)onOKPressed:(id)sender;
- (IBAction)onDELPressed:(id)sender;
- (NSString *) setPINBlock:(NSString *)key
               aPin :(NSString *) PIN;
//- (NSString *) padRight:(NSString *) aString
//                     n :(int) n
//                  param:(NSString*) param;
- (int) toByte: (NSString*) hex;
- (IBAction)onBack:(id)sender;

@end

@implementation InputPINViewController
NSString *pin = @"";
DESChiper *desChipper;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    pin = @"";
    _arrowImage.image = [_arrowImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_arrowImage setTintColor:[UIColor blackColor]];
    UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    self.textFieldPIN.inputView = dummyView;
    desChipper = [[DESChiper alloc]init];
}

- (IBAction)onPINPressed:(UIButton *)sender {
    if ([pin length] < 6){
        pin = [pin stringByAppendingString:sender.titleLabel.text];
        self.textFieldPIN.text = pin;
    }
}

- (IBAction)onOKPressed:(id)sender {
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    NSString * key = [userDef objectForKey:@"secret-key"];

    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate onPINSuccess:[self setPINBlock:key aPin:pin]];
    }];
}

- (IBAction)onDELPressed:(id)sender {
    if ([pin length] > 0){
        pin = [pin substringToIndex:[pin length] - 1];
        self.textFieldPIN.text = pin;
    }
}
-(NSString *) setPINBlock:(NSString *)key aPin:(NSString *)PIN{
    if (key == NULL || [key length] == 0){
        return @"";
    }
    NSString* pinBLOCK = [desChipper doPinBlock:PIN key:key];

    return pinBLOCK;
}
//-(NSString *) padRight:(NSString *)aString n:(int)n param:(NSString *)param{
//    //return String.format("%-" + n + "s", s).replace(' ', param);
//
//    NSString* result = [NSMutableString stringWithFormat:@"%d", n];
//    result = [NSMutableString stringWithFormat:[[@"%-" stringByAppendingString:result] stringByAppendingString:@"s"], [pin UTF8String]];
//    result = [result stringByReplacingOccurrencesOfString:@" " withString:param];
//    return result;
//}
- (IBAction)onBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
@end
