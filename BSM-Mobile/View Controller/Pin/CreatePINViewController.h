//
//  InputPINViewController.h
//  BSM-Mobile
//
//  Created by Galih Suryo on 1/10/18.
//  Copyright © 2018 lds. All rights reserved.
//

@protocol CreatePINDelegate <NSObject>
- (void)onSuccessCreatePIN;
- (void)onCancelCreatePIN;
@end;
@interface CreatePINViewController : UIViewController
@property (readwrite, weak, nonatomic) id <CreatePINDelegate> delegate;
@end


