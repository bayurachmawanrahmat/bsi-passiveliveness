//
//  InputPINViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 10/1/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "CreatePINViewController.h"
#import "Connection.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "DESChiper.h"
#import "CustomBtn.h"
#import "Styles.h"
#import "NSUserdefaultsAes.h"

@interface CreatePINViewController ()<UITextFieldDelegate, ConnectionDelegate, UIAlertViewDelegate>
{
    NSString *flagTP1;
    NSString *flagTP2;
}
- (IBAction)onOKPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewCreatePIN;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UITextField *inputPIN;
@property (weak, nonatomic) IBOutlet UITextField *inpuConfirmPIN;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc1;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc2;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet UIButton *btnEye1;
@property (weak, nonatomic) IBOutlet UIButton *btnEye2;

@end

@implementation CreatePINViewController
DESChiper *chipperDES;

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}


#pragma mark change cancel to save and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    flagTP1 = @"HIDE";
    flagTP2 = @"HIDE";
    
    self.lblTitle.text = lang(@"CREATE_NEW_PIN");
    self.lblDesc1.text = lang(@"NEW_PIN");
    self.lblDesc2.text = lang(@"CONFIRM_NEW_PIN");
    [self.btnN setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    
    [_btnN setColorSet:PRIMARYCOLORSET];
    
    [Styles setStyleTextFieldBottomLine:_inputPIN];
    [Styles setStyleTextFieldBottomLine:_inpuConfirmPIN];
    
    chipperDES = [[DESChiper alloc]init];
    
    self.viewCreatePIN.backgroundColor = [UIColor whiteColor];
    self.viewCreatePIN.layer.cornerRadius = 10;
    self.viewCreatePIN.layer.borderWidth = 1;
    self.viewCreatePIN.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    [self.vwScroll setContentSize:CGSizeMake(self.viewCreatePIN.frame.size.width, self.viewCreatePIN.frame.size.height)];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.inputPIN.inputAccessoryView = keyboardDoneButtonView;
    self.inpuConfirmPIN.inputAccessoryView = keyboardDoneButtonView;
    
    [self.inputPIN setKeyboardType:UIKeyboardTypeNumberPad];
    [self.inpuConfirmPIN setKeyboardType:UIKeyboardTypeNumberPad];
    
    [self.inputPIN setSecureTextEntry:true];
    [self.inpuConfirmPIN setSecureTextEntry:true];
    
    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.viewCreatePIN.frame.size.width, self.viewCreatePIN.frame.size.height + kbSize.height + 20)];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.viewCreatePIN.frame.size.width, self.viewCreatePIN.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)onOKPressed:(id)sender {
    [self goToNext];
}

-(void) goToNext{
    if ([self.inputPIN.text length] != 6){
        [Utility showMessage:lang(@"PIN_MUST_6_DIGIT") instance:self];
    } else if([self.inpuConfirmPIN.text isEqualToString:@""]){
        [Utility showMessage:lang(@"NOT_EMPTY") instance:self];
    }else if([self.inputPIN.text isEqualToString:@""]){
        [Utility showMessage:lang(@"NOT_EMPTY") instance:self];
    } else if(![self.inputPIN.text isEqualToString:self.inpuConfirmPIN.text]){
        [Utility showMessage:lang(@"PIN_NOT_SAME") instance:self];
    } else {
        NSString *message = @"";
        NSString *trimmedStringinpuConfirmPIN = [self.inpuConfirmPIN.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        NSString *trimmedStringinputPIN = [self.inputPIN.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedStringinpuConfirmPIN length] || [trimmedStringinputPIN length]) {
            message = lang(@"STR_NOT_NUM");
        }
        
        if([message isEqualToString:@""]){
            NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
            NSString * session = [userDef objectForKey:KEY_SESSION_ID];
            [[DataManager sharedManager]setPinNumber:session];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
            NSString *date_local = [dateFormatter stringFromDate:[NSDate date]];
            
            NSString *parmUrl = [NSString stringWithFormat:@"request_type=create_pin,new_pin=%@,session_id=%@,date_local=%@",[_inputPIN text], session, date_local];
            
            [conn sendPostParmUrl:parmUrl needLoading:true encrypted:true banking:true];
        } else {
            [Utility showMessage:message instance:self];
        }
    }
}

- (IBAction)togglePwd1:(id)sender {
    if ([flagTP1 isEqualToString:@"SHOW"]) {
        flagTP1 = @"HIDE";
        [self.inputPIN setSecureTextEntry:true];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTP1 isEqualToString:@"HIDE"]) {
        flagTP1 = @"SHOW";
        [self.inputPIN setSecureTextEntry:false];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)togglePwd2:(id)sender {
    if ([flagTP2 isEqualToString:@"SHOW"]) {
        flagTP2 = @"HIDE";
        [self.inpuConfirmPIN setSecureTextEntry:true];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTP2 isEqualToString:@"HIDE"]) {
        flagTP2 = @"SHOW";
        [self.inpuConfirmPIN setSecureTextEntry:false];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(NSDictionary *)jsonObject withRequestType:(NSString *)requestType{
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        [self dismissViewControllerAnimated:YES completion:^(){
            [self->_delegate onSuccessCreatePIN];
        }];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [[[self presentingViewController]presentingViewController]dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [[[self presentingViewController]presentingViewController]dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reloadApp{
    [BSMDelegate reloadApp];
}

@end

