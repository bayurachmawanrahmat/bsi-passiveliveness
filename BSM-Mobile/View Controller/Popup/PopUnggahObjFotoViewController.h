//
//  PopUnggahObjFotoViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PopUnggahObjFotoViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;
- (void) setAttachedView : (UIView*) view;
- (void) setAttachedLocation : (NSString*) location;
- (void) setAttachedImage : (NSString*) image;

@end

@protocol UnggahObjectDelegate

- (void) doneWithData : (NSDictionary*) string withView:(UIView*) view;

@end

NS_ASSUME_NONNULL_END
