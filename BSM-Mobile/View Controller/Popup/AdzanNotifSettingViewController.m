//
//  AdzanNotifSettingViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/03/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "AdzanNotifSettingViewController.h"
#import "CellRadioSelection.h"
#import "SholluLib.h"
#import "Utility.h"
#import "Styles.h"

@interface AdzanNotifSettingViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *mode;
    NSArray *modeAdhan;
    NSArray *modeNoAdhan;
    
    NSArray *modeImage;
    NSArray *modeImageAdhan;
    NSArray *modeImageNoAdhan;
    NSArray *time;
    NSArray *arrData;
    NSInteger selectedTime;
    
    NSInteger selectedMode;
    NSInteger selectedAlarm;
    
    NSString *pengingatAdzanTitle;
}

@end

@implementation AdzanNotifSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    time = @[@"30",@"15", @"0"];
    
    [self.tableMode registerNib:[UINib nibWithNibName:@"CellRadioSelection" bundle:nil] forCellReuseIdentifier:@"CELLRADIOSEL"];
    [self.tableTime registerNib:[UINib nibWithNibName:@"CellRadioSelection" bundle:nil] forCellReuseIdentifier:@"CELLRADIOSEL"];
    
    self.tableMode.delegate = self;
    self.tableMode.dataSource = self;
    
    self.tableTime.delegate = self;
    self.tableTime.dataSource = self;
    
    [self.contentView.layer setCornerRadius:16];
    [self.contentView.layer setMasksToBounds:YES];
    
    [self.buttonSet.layer setCornerRadius:20];
    
    [self.tableMode reloadData];
    [self.tableTime reloadData];
    NSDictionary *data = arrData[selectedTime];
    
    [self.labelTitle setText:[data objectForKey:@"title"]];
    
    NSIndexPath * indexPathMode = [NSIndexPath indexPathForRow:[[data objectForKey:@"mode"]intValue] inSection:0];
    NSIndexPath * indexPathAlarm = [NSIndexPath indexPathForRow:[time indexOfObject:[data objectForKey:@"alarm"]] inSection:0];

    
    [self.tableMode selectRowAtIndexPath:indexPathMode animated:YES scrollPosition:UITableViewScrollPositionNone];
    selectedMode = indexPathMode.row;
    [self.tableTime selectRowAtIndexPath:indexPathAlarm animated:YES scrollPosition:UITableViewScrollPositionNone];
    selectedAlarm = indexPathAlarm.row;
    
    [self.tableMode setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableTime setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    self.tableModeHeight.constant = self.tableMode.contentSize.height;
    self.tableTimeHeight.constant = self.tableTime.contentSize.height;
    
    [self.closeImg setUserInteractionEnabled:TRUE];
    [self.closeImg addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionClose)]];
    
    [self.buttonSet addTarget:self action:@selector(actionSet:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonSet setTitle:lang(@"APPLY") forState:UIControlStateNormal];

    self.timeTitle.text = pengingatAdzanTitle;
    [Styles addShadow:self.view];
}

- (void)actionClose{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionSet:(UIButton*)sender{
    NSMutableArray *myArrayMut = [arrData mutableCopy];
    NSMutableDictionary *dict = [arrData[selectedTime] mutableCopy];
    [dict setValue:[NSString stringWithFormat:@"%ld",(long)selectedMode] forKey:@"mode"];
    [dict setValue:[NSString stringWithFormat:@"%@",time[selectedAlarm]] forKey:@"alarm"];
    
    myArrayMut[selectedTime] = dict;

    [[NSUserDefaults standardUserDefaults] setObject:myArrayMut forKey:@"data_adzan_notif"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [SholluLib createNotif:selectedTime withData:dict];
    
    
    [self dismissViewControllerAnimated:YES completion:^(){
        [self->delegate didSettingNotification];
    }];
    
}

- (void)setSelectedTime:(NSInteger)timeSelected{
    selectedTime = timeSelected;
    NSString *pengingatImsak = @"";
    NSString *pengingatTerbit = @"";
    if([Utility isLanguageID]){
        pengingatAdzanTitle = @"Pengingat Sebelum Adzan";
        pengingatImsak = @"Pengingat Sebelum Imsak";
        pengingatTerbit = @"Pengingat Sebelum Terbit";
        modeAdhan = @[@"Adzan",@"Notifikasi",@"Senyapkan",@"Tidak Aktif"];
        modeNoAdhan = @[@"Notifikasi",@"Senyapkan",@"Tidak Aktif"];
    }else{
        pengingatAdzanTitle = @"Reminder Before Adhan";
        pengingatImsak = @"Reminder Before Imsak";
        pengingatTerbit = @"Reminder Before Sunrise";
        modeAdhan = @[@"Adhan",@"Notification",@"Silent",@"Non Active"];
        modeNoAdhan = @[@"Notification",@"Silent",@"Non Active"];
    }
    modeImageAdhan = @[@"ic_ntf_adzan",@"ic_ntf_adzan",@"ic_ntf_senyap",@"ic_ntf_non"];
    modeImageNoAdhan = @[@"ic_ntf_adzan",@"ic_ntf_senyap",@"ic_ntf_non"];
    
    if(selectedTime == 0 || selectedTime == 2){
        modeImage = modeImageNoAdhan;
        mode = modeNoAdhan;
        if(selectedTime == 0){
            pengingatAdzanTitle = pengingatImsak;
        }else if(selectedTime == 2){
            pengingatAdzanTitle = pengingatTerbit;
        }
    }else{
        modeImage = modeImageAdhan;
        mode = modeAdhan;
    }
}

- (void)setData:(NSArray*)arr{
    arrData = arr;
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.tableTime){
        if(selectedMode != mode.count-1){
            selectedAlarm = indexPath.row;
            [self.tableTime setUserInteractionEnabled:YES];
        }else{
            selectedAlarm = time.count-1;
            NSIndexPath * index = [NSIndexPath indexPathForRow:selectedAlarm inSection:0];
            [self.tableTime selectRowAtIndexPath:index animated:YES scrollPosition:UITableViewScrollPositionNone];
            [self.tableTime setUserInteractionEnabled:NO];
            CellRadioSelection *cell = (CellRadioSelection*)[self.tableTime cellForRowAtIndexPath:index];
            [cell.radio setTintColor:UIColorFromRGB(const_color_gray)];
        }
    }else{
        selectedMode = indexPath.row;
        if(selectedMode != mode.count-1){
            [self.tableTime setUserInteractionEnabled:YES];
            NSIndexPath * index = [NSIndexPath indexPathForRow:selectedAlarm inSection:0];
            [self.tableTime selectRowAtIndexPath:index animated:YES scrollPosition:UITableViewScrollPositionNone];
        }else{
            selectedAlarm = time.count-1;
            NSIndexPath * index = [NSIndexPath indexPathForRow:selectedAlarm inSection:0];
            [self.tableTime selectRowAtIndexPath:index animated:YES scrollPosition:UITableViewScrollPositionNone];
            [self.tableTime setUserInteractionEnabled:NO];
            CellRadioSelection *cell = (CellRadioSelection*)[self.tableTime cellForRowAtIndexPath:index];
            [cell.radio setTintColor:UIColorFromRGB(const_color_gray)];
        }

    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.tableTime){
        return time.count;
    }else{
        return mode.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellRadioSelection *cell = (CellRadioSelection*)[tableView dequeueReusableCellWithIdentifier:@"CELLRADIOSEL"];
    
    if(tableView == self.tableTime){
        [cell.image setImage:[UIImage imageNamed:@""]];
        NSString *times = time[indexPath.row];
        if([times doubleValue] != 0){
            if([Utility isLanguageID]){
                [cell.label setText:[NSString stringWithFormat:@"%@ Menit",times]];
            }else{
                [cell.label setText:[NSString stringWithFormat:@"%@ Minutes",times]];
            }
        }else{
            if([Utility isLanguageID]){
                [cell.label setText:@"Tidak ada"];
            }else{
                [cell.label setText:@"None"];
            }
        }
        
    }else{
        if(selectedMode == indexPath.row){
            [cell.image setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_active",modeImage[indexPath.row]]]];
        }else{
            [cell.image setImage:[UIImage imageNamed:modeImage[indexPath.row]]];
        }
        [cell.label setText:mode[indexPath.row]];
    }
    
    UIView *selectionColor = [[UIView alloc] init];
    selectionColor.backgroundColor = UIColorFromRGB(const_color_lightblue);
    cell.selectedBackgroundView = selectionColor;
    
    return cell;
}

@end
