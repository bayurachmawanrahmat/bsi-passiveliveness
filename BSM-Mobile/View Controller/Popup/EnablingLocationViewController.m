//
//  EnablingLocationViewController.m
//  BSM-Mobile
//
//  Created by Amini on 05/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "EnablingLocationViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "CustomBtn.h"
#import "Styles.h"

@interface EnablingLocationViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *closeImg;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonEnable;

@end

@implementation EnablingLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.contentView.layer setCornerRadius:16];
    [self.contentView.layer setMasksToBounds:YES];
    
    [self.closeImg setUserInteractionEnabled:true];
    [self.closeImg addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionClose)]];
    [self.buttonEnable addTarget:self action:@selector(actionEnable) forControlEvents:UIControlEventTouchUpInside];
    
    [Styles addShadow:self.view];
}

- (void) actionClose{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *pop = [self topViewController];
    UIViewController *templateView =  [ui instantiateViewControllerWithIdentifier:@"HomeVC"];
    self.slidingViewController.topViewController = templateView;
    [pop dismissViewControllerAnimated:YES completion:nil];
}

- (void) actionEnable{
    [self actionClose];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    int i = 0;
    while (topController.presentedViewController) {
        if (i==0) {
            topController = topController.presentedViewController;
            break;
        }
        i++;
        
    }
    
    return topController;
}

@end
