//
//  PopUnggahObjFotoViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PopUnggahObjFotoViewController.h"
#import "CustomBtn.h"
#import "GCamOverlayView.h"
#import "CamOverlayController.h"
#import <MapKit/MapKit.h>
#import "Styles.h"
#import "Utility.h"

@interface PopUnggahObjFotoViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, UIImagePickerControllerDelegate, GCamOverlayDelegate, UINavigationControllerDelegate>{
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    
    double latitude;
    double longitude;
    
    UIRefreshControl *refreshControl;
    
    NSString *base64img;
    
    UIImagePickerController *imagePicker;
    
    UIView *attachView;
}
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgClose;
@property (weak, nonatomic) IBOutlet UIView *viewImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgFoto;
@property (weak, nonatomic) IBOutlet UILabel *labelFoto;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleLokasi;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSave;

@end

@implementation PopUnggahObjFotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    if([Utility isLanguageID]){
        [self.labelFoto setText:@"Foto barang/asset tidak boleh terpotong, gambar harus jelas"];
        [self.btnSave setTitle:@"Simpan" forState:UIControlStateNormal];
        [self.labelTitleLokasi setText:@"Lokasi"];
    }else{
        [self.labelFoto setText:@"Make sure the entire object is captured and clear"];
        [self.btnSave setTitle:@"Save" forState:UIControlStateNormal];
        [self.labelTitleLokasi setText:@"Location"];
    }
    
    [Styles addShadow:self.view];
    [self.viewContent.layer setCornerRadius:10];
    [self.viewContent.layer setMasksToBounds:YES];
    
    [self.viewImage.layer setBorderColor:[UIColorFromRGB(const_color_lightgray)CGColor]];
    [self.viewImage.layer setBorderWidth:1];
    [self.viewImage.layer setCornerRadius:10];
    [self.viewImage.layer setMasksToBounds:YES];
    
    UITapGestureRecognizer *tapClose = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionClose)];
    [self.imgClose setUserInteractionEnabled:YES];
    [self.imgClose addGestureRecognizer:tapClose];
    
    [self.btnSave addTarget:self action:@selector(actionSave) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCamera addTarget:self action:@selector(actionTakeFoto) forControlEvents:UIControlEventTouchUpInside];
    [self.btnRefresh addTarget:self action:@selector(actionRefreshFoto) forControlEvents:UIControlEventTouchUpInside];
    
    imagePicker.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
    
    [Utility resetTimer];
}

- (void)viewDidAppear:(BOOL)animated{
    [self currentLocationIdentify];
    if(![base64img isEqualToString:@""] && base64img != nil){
        [self.imgFoto setImage:[self decodeBase64ToImage:base64img]];
        [self showLocation];
        [self.labelFoto setHidden:YES];
    }else{
        base64img = @"";
    }
}

- (void) currentLocationIdentify{
    if([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc]init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            NSString *msgTitle = @"App Permission Denied";
            NSString *msgContent = @"Turn On Location Serivce in Settings";
            if([Utility isLanguageID]){
                msgTitle = @"Izin aplikasi di tolak";
                msgContent = @"Aktifkan Layanan lokasi di pengaturan";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:msgTitle message:msgContent preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            }];
            
            NSString *stringCancel = @"";
            if([Utility isLanguageID]){
                stringCancel = @"Batal";
            }else{
                stringCancel = @"Cancel";
            }
            
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:stringCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            }];
            
            [alert addAction:actionOK];
            [alert addAction:actionCancel];
            [self presentViewController:alert animated:YES completion:nil];

        }
        
        if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    }else{
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
    }
    
    if(refreshControl) [refreshControl endRefreshing];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    longitude = currentLocation.coordinate.longitude;
    latitude = currentLocation.coordinate.latitude;
    
    [self showLocation];
    
//    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
//    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(coordinate, 500, 500)];
//    [self->_mapView setRegion:adjustedRegion animated:true];
}

- (void) showLocation{
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(coordinate, 500, 500)];
    [self->_mapView setRegion:adjustedRegion animated:true];
}

- (void) actionRefreshFoto{
    [self.imgFoto setImage:nil];
    base64img = @"";
    [self.labelFoto setHidden:NO];
    [self.btnCamera setHidden:NO];
    [self.btnRefresh setHidden:NO];
    [self currentLocationIdentify];
}

- (void) actionTakeFoto{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    CamOverlayController *overlayController = [[CamOverlayController alloc] init];

    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    imagePicker.showsCameraControls = false;
    imagePicker.allowsEditing = false;

    GCamOverlayView *cameraView = (GCamOverlayView *)overlayController.view;
    cameraView.frame = imagePicker.view.frame;
    cameraView.imageOverlay.image = [UIImage imageNamed:@""];
    if([Utility isLanguageID]){
        cameraView.titleLabel.text = @"Unggah foto object";
        cameraView.descriptionLabel.text = @"Pastikan barang/asset tidak terpotong";
    }else{
        cameraView.titleLabel.text = @"Upload object photo";
        cameraView.descriptionLabel.text = @"Make sure the entire object is captured and clear";
    }

    cameraView.delegate = self;
    
    imagePicker.cameraOverlayView = cameraView;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)didTakePhoto:(GCamOverlayView *)overlayView{
    [imagePicker takePicture];
}

- (void)didBack:(GCamOverlayView *)overlayView{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.imgFoto setImage:image];
    base64img = [Utility compressAndEncodeToBase64String:image withMaxSizeInKB:40];
    [self.labelFoto setHidden:YES];
    [[DataManager sharedManager].dataExtra setValue:base64img forKey:@"obj_photo"];
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)switchCameraDevice:(GCamOverlayView *)overlayView{
    if(imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceRear)
    {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }else{
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}


- (void) actionClose{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) actionSave{
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        [self currentLocationIdentify];
    }else if([base64img isEqualToString:@""]){
        [Utility showMessage:@"Silahkan ambil foto object" enMessage:@"Please take object photo" instance:self];
    }else{
        NSDictionary *dict = @{ @"image" : base64img,
                                @"location" : [NSString stringWithFormat:@"%f,%f", latitude, longitude]};
        [delegate doneWithData:dict withView:attachView];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setAttachedView:(UIView *)view{
    attachView = view;
}

- (void)setAttachedLocation:(NSString *)location{
    latitude = [[location componentsSeparatedByString:@","][0]doubleValue];
    longitude = [[location componentsSeparatedByString:@","][1]doubleValue];
}

- (void)setAttachedImage:(NSString *)image{
    base64img = image;
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
  NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
  return [UIImage imageWithData:data];
}

@end
