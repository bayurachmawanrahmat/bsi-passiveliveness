//
//  AdzanNotifSettingViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/03/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AdzanNotifSettingViewController : UIViewController
{
    id delegate;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *closeImg;
@property (weak, nonatomic) IBOutlet UITableView *tableMode;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableModeHeight;

@property (weak, nonatomic) IBOutlet UILabel *timeTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableTimeHeight;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *buttonSet;

- (void) setSelectedTime: (NSInteger) timeSelected;
- (void) setData:(NSArray*)arr;
- (void) setDelegate:(id)newDelegate;

@end

@protocol AdzanNotifSettingDelegate

@required

- (void) didSettingNotification;

@end

NS_ASSUME_NONNULL_END
