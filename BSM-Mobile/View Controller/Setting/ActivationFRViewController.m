//
//  ActivationViewFRViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 09/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "ActivationFRViewController.h"

#import "MenuViewController.h"
#import "DesclaimerViewController.h"
#import "CreatePINViewController.h"
#import "ActivationFRResultViewController.h"
#import "GCamOverlayView.h"
#import "CamOverlayController.h"

#import "Connection.h"
#import "Utility.h"
#import <MessageUI/MessageUI.h>
#import "UIViewController+ECSlidingViewController.h"
#import "AESCrypt.h"
#import "InboxHelper.h"
#import "DESChiper.h"
#import "CustomBtn.h"
#import "Styles.h"


@interface ActivationFRViewController ()<UITextFieldDelegate, ConnectionDelegate, GCamOverlayDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate ,CreatePINDelegate, UITextViewDelegate, DesclaimerDelegate, ActivationFRResultDelegate> {
    float sizeTV;
    float constTV;
    BOOL isResize, iccidEnabled;
    NSString *paramICCID, *paramIMEI;
    DESChiper *desChipper3;

    NSUserDefaults *userDefault;
    NSString *fromSliding;
    NSString *receipentsNumber;
    
    NSString *base64img;
    UIImagePickerController *imagePicker;
    
    NSDictionary * jsonResponse;
}

@property (weak, nonatomic) IBOutlet UITextField *textFieldPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldKeyCode;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSelanjutnya;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatals;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblActivation;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation ActivationFRViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    receipentsNumber = @"";
    isResize = YES;
    userDefault = [NSUserDefaults standardUserDefaults];
    fromSliding = [userDefault objectForKey:@"fromSliding"];
    
    iccidEnabled = [[userDefault objectForKey:@"config.act.iccid"]boolValue];
    
    [Styles setTopConstant:_topSpace];
    if([fromSliding isEqualToString:@"YES"]){
        self.bgView.hidden = YES;
    }else{
        self.bgView.hidden = NO;
    }
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldPhoneNumber.frame.size.height);
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldPhoneNumber.leftView = paddingPhone;
    self.textFieldPhoneNumber.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldPhoneNumber.keyboardType = UIKeyboardTypeNumberPad;
    [Styles setStyleTextFieldBottomLine:_textFieldPhoneNumber];
    
    UIView *paddingKey = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldKeyCode.leftView = paddingKey;
    self.textFieldKeyCode.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldKeyCode.keyboardType = UIKeyboardTypeNumberPad;
    [Styles setStyleTextFieldBottomLine:_textFieldKeyCode];
    
    [self.btnSelanjutnya setColorSet:PRIMARYCOLORSET];
    [self.btnBatals setColorSet:SECONDARYCOLORSET];
    
    _lblTitle.text = lang(@"ACTIVATION");
    _lblPhoneNumber.text = lang(@"PHONE_NUMBER");
    _lblActivation.text = lang(@"ACTIVATION_CODE");
    [_btnBatals setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [_btnSelanjutnya setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
        
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.textFieldPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    self.textFieldKeyCode.inputAccessoryView = keyboardDoneButtonView;
    
    [self addVC];
}

- (void)addVC{
    DesclaimerViewController *shoDesclaimer = [self.storyboard instantiateViewControllerWithIdentifier:@"DSCLMRVC"];
    [shoDesclaimer setDelegate:self];
    shoDesclaimer.view.frame = self.view.bounds;
    [self.view addSubview:shoDesclaimer.view];
    [self addChildViewController:shoDesclaimer];
    [shoDesclaimer didMoveToParentViewController:self];
}

- (void)onDesclaimerCancel{
    [self toExit];
}

- (void)onDesclaimerAgree{
//    do nothing
}

- (IBAction)doneClicked:(id)sender
{
//    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField== self.textFieldPhoneNumber){
        [self.textFieldKeyCode becomeFirstResponder];
        return false;
    }
    //do registration
    [self.textFieldKeyCode resignFirstResponder];
    return true;
}


#pragma mark send registrasi change batals
- (IBAction)sendRegistration:(id)sender {
    int currentVersionValue = [[NSString stringWithFormat:@"%s",VERSION_VALUE]intValue];
//    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
//    NSLog(@"%@ - %@", [userDefault valueForKey:@"config_enable_activationfr"], [userDefault valueForKey:@"config_activationfr_versionvalue"]);
    if([[userDefault valueForKey:@"config_enable_activationfr"]intValue] == 1 &&
       currentVersionValue >= [[userDefault valueForKey:@"config_activationfr_versionvalue"]intValue]){
        [self toRegistrasi:@"FR"];
    }else{
        [self toRegistrasi:@"SMS"];
    }
}

#pragma mark goto failed FR message
- (void) gotoFRResult{
    ActivationFRResultViewController *frresult = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationFRResult"];
    [frresult setDelegate:self];
    [self presentViewController:frresult animated:YES completion:nil];
}

#pragma mark FRResult delegate
- (void)onCancelTapped{
    [self toBatal];
}

- (void)onNextTapped{
    [self toRegistrasi:@"SMS"];
}


#pragma mark activation with FR
- (void) activationWithFR{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker = [[UIImagePickerController alloc] init];
        
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate = self;
        
        CamOverlayController *overlayController = [[CamOverlayController alloc] init];

        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        imagePicker.delegate = self;
        imagePicker.showsCameraControls = false;
        imagePicker.allowsEditing = false;
        
        

        GCamOverlayView *cameraView = (GCamOverlayView *)overlayController.view;
        cameraView.frame = imagePicker.view.frame;
        cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_selfie_fr.png"];
        cameraView.titleLabel.text = @"";
        cameraView.descriptionLabel.text = lang(@"FR_REG_CAMERA_NOTES");
        cameraView.backButton.hidden = YES;
        cameraView.switchButton.hidden = YES;

        cameraView.delegate = self;
        
        imagePicker.cameraOverlayView = cameraView;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }else{

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")  message:lang(@"CAMERA_NOT_SUPPORT")  preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self onNextTapped];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)didTakePhoto:(GCamOverlayView *)overlayView{
    [imagePicker takePicture];
}

- (UIImage *)fixOrientation:(UIImage *)image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
    UIImage *fixImage = [self fixOrientation:image];
    if(fixImage.size.width > 2160.0)
    {
        fixImage = [self imageWithImage:fixImage scaledToSize:CGSizeMake(fixImage.size.width / 8, fixImage.size.height / 8)];
    }
    else if(fixImage.size.width > 1080.0)
    {
        fixImage = [self imageWithImage:fixImage scaledToSize:CGSizeMake(fixImage.size.width / 4, fixImage.size.height / 4)];
    }
    NSData *imageData = UIImageJPEGRepresentation(fixImage, 0.8);
    NSString* myString;
    myString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength];
    [[DataManager sharedManager].dataExtra setValue:myString forKey:@"file"];
//    NSLog(@"%@",myString);
    [imagePicker dismissViewControllerAnimated:YES completion:^{
        [self requestActivationWithFR];
    }];
}

- (void)switchCameraDevice:(GCamOverlayView *)overlayView{
}

- (void)didBack:(nonnull GCamOverlayView *)overlayView {
}

#pragma mark
- (void) requestActivationWithFR{
    BDError *error = [[BDError alloc] init];
    BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
    
    BDRSACryptorKeyPair *RSAKeyPair = [RSACryptor generateKeyPairWithKeyIdentifier:BSM_SERVER_KEY
                                                                             error:error];
    
    [NSUserdefaultsAes setObject:RSAKeyPair.privateKey forKey:@"privatekey"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    NSString *parmUrl = [NSString stringWithFormat:@"request_type=activation_fr,msisdn=%@,activation_code=%@,otp=%@,public_key=%@,language,file",self.textFieldPhoneNumber.text,[self genAC:self.textFieldPhoneNumber.text],self.textFieldKeyCode.text,RSAKeyPair.publicKey];
    [conn sendPostParmUrl:parmUrl needLoading:true encrypted:true banking:false];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void) activationWithSMS{
    //generate public key
    BDError *error = [[BDError alloc] init];
    BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
    
    BDRSACryptorKeyPair *RSAKeyPair = [RSACryptor generateKeyPairWithKeyIdentifier:BSM_SERVER_KEY
                                                                             error:error];
    
    [NSUserdefaultsAes setObject:RSAKeyPair.privateKey forKey:@"privatekey"];
    
    [userDefault synchronize];
    
    if (iccidEnabled) {
        
        NSString *parmUrl = [NSString stringWithFormat:@"request_type=activation,msisdn=%@,activation_code=%@,otp=%@,public_key=%@,imei=%@,iccid=%@,language",self.textFieldPhoneNumber.text,[self genAC:self.textFieldPhoneNumber.text],self.textFieldKeyCode.text,RSAKeyPair.publicKey,paramIMEI,paramICCID];
        
//        NSLog(@"%@", parmUrl);
        
        [userDefault removeObjectForKey:@"paramURLPost"];
        [userDefault setObject:parmUrl forKey:@"paramURLPost"];
        
        //new checkin
        if([userDefault objectForKey:@"config_source_activation"] != nil){
            if([[userDefault valueForKey:@"config_source_activation"] isEqualToString:@"1"] || ![self isLocalNumber:self.textFieldPhoneNumber.text]){
                
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                NSString *url = [NSString stringWithFormat:@"request_type=check_smspool_roundrobin2,nohp=%@,language",self.textFieldPhoneNumber.text];
                [conn sendPostParmUrl:url needLoading:true encrypted:false banking:false];
            }else{
                receipentsNumber = @"3339";
                [self verifyActivation];
            }
        }else{
            receipentsNumber = @"3339";
            [self verifyActivation];
        }
        
    }else{
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        NSString *parmUrl = [NSString stringWithFormat:@"request_type=activation,msisdn=%@,activation_code=%@,otp=%@,public_key=%@,language",self.textFieldPhoneNumber.text,[self genAC:self.textFieldPhoneNumber.text],self.textFieldKeyCode.text,RSAKeyPair.publicKey];
        [conn sendPostParmUrl:parmUrl needLoading:true encrypted:true banking:false];
    }
}

- (BOOL) isLocalNumber:(NSString*)nomor{
    return [nomor hasPrefix:@"62"] || [nomor hasPrefix:@"0"];
}

#pragma mark activation with sms
- (void) toRegistrasi : (NSString*)method{
    NSString *message = @"";
    if([self.textFieldPhoneNumber.text isEqualToString:@""]){
        message = lang(@"NOT_EMPTY");
    } else if(![self.textFieldKeyCode.text isEqualToString:@""]){
        NSString *trimmedString = [self.textFieldPhoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedString length]) {
            message = lang(@"STR_NOT_NUM");
        }
    }else if([self.textFieldKeyCode.text isEqualToString:@""]){
        message = lang(@"NOT_EMPTY");
    }
    
    if([message isEqualToString:@""]){
        
        NSString *encrypt = [AESCrypt encrypt:self.textFieldKeyCode.text];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:encrypt forKey:@"activation_code"];
        [userDefault synchronize];
        
        
        NSString *unique_id = [self getUUID];
        paramIMEI = [unique_id substringToIndex:16]; //imei
        paramICCID = [unique_id substringFromIndex:16]; //iccid
        
        [NSUserdefaultsAes removeValueForKey:@"imei"];
        [NSUserdefaultsAes setObject:paramIMEI forKey:@"imei"];
        
        [NSUserdefaultsAes removeValueForKey:@"iccid"];
        [NSUserdefaultsAes setObject:paramICCID forKey:@"iccid"];
        
        [NSUserdefaultsAes removeValueForKey:@"msisdn"];
        [NSUserdefaultsAes setObject:self.textFieldPhoneNumber.text forKey:@"msisdn"];
        
        if([method isEqualToString:@"FR"]){
            [self activationWithFR];
        }else{
            [self activationWithSMS];
        }
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:message preferredStyle:UIAlertControllerStyleAlert];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            [self backToHome];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (NSString*)getUUID {
    NSString *str = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return str;
}

- (void)verifyActivation{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"SMS_VERIFY") preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert addAction:[UIAlertAction actionWithTitle:lang(@"CANCEL") style:UIAlertActionStyleCancel handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:lang(@"NEXT") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *msgToEncrypt = [[self->paramICCID stringByAppendingString:@";"] stringByAppendingString:self->paramIMEI];
        NSString *kode = @"";
        kode = [self genAC:self.textFieldPhoneNumber.text];
//        NSLog(@"%@", kode);
        
        NSString *smsContent = @"";
        self->desChipper3 = [[DESChiper alloc]init];
        smsContent = [self->desChipper3 doDES:msgToEncrypt key:kode];
        
        NSData *dataSMS = [smsContent dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64Encoded = [dataSMS base64EncodedStringWithOptions:0];
        NSString *txtSMS = [@"bsmsmsact " stringByAppendingString:base64Encoded];
        [self showSMS:txtSMS];
//        NSLog(@"Ok, send the sms to UID : %@",self.textFieldPhoneNumber.text);
    }]];
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark - MFMessageValidation
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"SMS_NOT_SUPPORT") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler: nil]];
            [self presentViewController:alert animated:YES completion:nil];
            break;
        }
        case MessageComposeResultSent: {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[userDefault objectForKey:@"paramURLPost"] needLoading:true encrypted:true banking:false];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        default:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
    }
    
}

-(void) toBatal{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
        
    }
}

- (IBAction)batal:(id)sender {
    [self toBatal];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(NSDictionary *)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"check_smspool_roundrobin2"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            
            NSDictionary *dict  = [NSJSONSerialization
                                   JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding]
                                   options:NSJSONReadingAllowFragments
                                   error:nil];
            
            receipentsNumber =[dict valueForKey:@"MSISDN"];
            [self verifyActivation];
        }else{
            [self verifyActivation];
        }
    }
    
    if([requestType isEqualToString:@"activation_fr"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [self activationSuccessResponse:jsonObject];
        }else if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"98"]){
            [self gotoFRResult];
        }else{
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            if (@available(iOS 13.0, *)) {
                [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToHome];
            }]];
            [self presentViewController:alerts animated:YES completion:nil];
        }
    }
    
    if([requestType isEqual:@"activation"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [self activationSuccessResponse:jsonObject];
        }else{
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            if (@available(iOS 13.0, *)) {
                [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToHome];
            }]];
            [self presentViewController:alerts animated:YES completion:nil];
        }
    }
}

- (void) activationSuccessResponse : (id)jsonObject{
    
    jsonResponse = jsonObject;
    [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
    [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"session_id"] forKey:@"session_id"];
    [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"public_key"] forKey:@"publickey"];
    
    [userDefault setValue:nil forKey:OBJ_FINANCE];
    [userDefault setValue:nil forKey:OBJ_SPESIAL];
    [userDefault setValue:nil forKey:OBJ_ALL_ACCT];
    [userDefault setValue:nil forKey:OBJ_ROLE_FINANCE];
    [userDefault setValue:nil forKey:OBJ_ROLE_SPESIAL];
    [userDefault synchronize];
    
    
    NSString * isReactivation = [userDefault objectForKey:KEY_HAS_SET_PIN];
    
    if (isReactivation != nil && [isReactivation isEqualToString:@"0"]){
        // havent create pin
        [userDefault synchronize];
        
        CreatePINViewController *createPINVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePINVC"];
        createPINVC.delegate = self;
        [self presentViewController:createPINVC animated:YES completion:nil];
        
    }
    else {
        // have create pin
        
        [userDefault setObject:@"YES" forKey:@"mustLogin"];
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault setObject:[jsonResponse valueForKey:@"name"] forKey:@"customer_name"];
        [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"customer_id"] forKey:@"customer_id"];
        [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"email"] forKey:@"email"];
        
        [userDefault synchronize];
        
        
        UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:[NSString stringWithFormat:@"%@: %@\nEmail: %@\nCustomer id:%@",lang(@"NAME"), [jsonResponse valueForKey:@"name"],[jsonResponse valueForKey:@"email"],[jsonResponse valueForKey:@"customer_id"]] preferredStyle:UIAlertControllerStyleAlert];
        if (@available(iOS 13.0, *)) {
            [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self onSuccessActivation];
        }]];
        [self presentViewController:alerts animated:YES completion:nil];
    }
}

-(NSString *)genAC:(NSString *)key{
    unsigned int length = (int)[key length];
    NSData * array = [key dataUsingEncoding:NSUTF8StringEncoding];
    const char *dbytes = [array bytes];
    int h = 0;
    for (int i = 0; i < length; ++i) {
//        NSLog(@"Value: %d", dbytes[length - (i+1)] ^ 0b00110011);
        h = 31 * h + (dbytes[length - (i+1)] ^ 0b00110011);
    }
    NSString *generated = [NSString stringWithFormat:@"%d",h];
//    NSLog(@"%@", generated);
    return generated;
}

- (void)errorLoadData:(NSError *)error{
    UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:[NSString stringWithFormat:@"%@\n",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self backToHome];}]];
    [self presentViewController:alerts animated:YES completion:nil];
}

- (void)reloadApp{
    [BSMDelegate reloadApp];
    //  [self backToR];
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    
}

- (void)onSuccessActivation{
    InboxHelper *inboxHelper = [[InboxHelper alloc] init];
    [inboxHelper createTbl];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
    NSDictionary *backUpInbox = [Utility dictForKeychainKey:strCID];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (backUpInbox) {
            InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
            [inboxHeldper insertDataInboxKeychain:backUpInbox];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"changeIcon"
             object:self];});
    });
    
    NSString *configIndicator = [userDefault objectForKey:@"config_indicator"];
    NSInteger mutasiMaxDays = [[userDefault valueForKey:@"config_mutation_range"]integerValue];
    NSInteger sessionTimeOut = [[userDefault valueForKey:@"config_timeout_session"]integerValue];
    if(configIndicator == nil || mutasiMaxDays == 0 || sessionTimeOut == 0){
        [self getConfigApp];
        NSString *configIndicator = [userDefault objectForKey:@"config_indicator"];
        bool stateIndicator = [configIndicator boolValue];
        if (stateIndicator) {
            [userDefault setObject:@"0" forKey:@"state_indicator"];
            [userDefault synchronize];
        }
    }
    
    [self backToHome];
    
}

- (void) backToHome{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
    }
}

- (void)showSMS:(NSString*)data {
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"SMS_NOT_SUPPORT") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler: nil]];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    NSMutableArray *recipents = [[NSMutableArray alloc]init];
    if([receipentsNumber isEqualToString:@""]){
        NSArray *smsLongNumberArrays = [[userDefault valueForKey:@"config_sms_longnumber"] componentsSeparatedByString:@"|"];
        NSUInteger myCount = [smsLongNumberArrays count];
        NSMutableArray *recipentsPrefix = [[NSMutableArray alloc]init];
        
        BOOL hasPrefix = false;
        for(NSString* numbers in smsLongNumberArrays){
            if([numbers hasPrefix:[_textFieldPhoneNumber.text substringToIndex:4]]) {
                [recipentsPrefix addObject:numbers];
                hasPrefix = true;
            }
        }
        
        if(hasPrefix){
            [recipents addObject:[recipentsPrefix objectAtIndex:arc4random_uniform((int)recipentsPrefix.count)]];
        }else{
            [recipents addObject:[smsLongNumberArrays objectAtIndex:arc4random_uniform((int)myCount)]];
        }
    }else{
        [recipents addObject:receipentsNumber];
    }
    
    
    //    NSArray *recipents = @[@"3339"];
    NSString *message = [NSString stringWithFormat:@"%@", data];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

-(void) onSuccessCreatePIN{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"YES" forKey:@"mustLogin"];
    [userDefault setObject:@"NO" forKey:@"hasLogin"];
    //[userDefault setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    [userDefault setObject:[jsonResponse valueForKey:@"name"] forKey:@"customer_name"];
    [NSUserdefaultsAes setObject:[jsonResponse valueForKey:@"customer_id"] forKey:@"customer_id"];
    [NSUserdefaultsAes setObject:[jsonResponse valueForKey:@"email"] forKey:@"email"];
    [userDefault synchronize];
    
    //    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"SUCCESS") message:[NSString stringWithFormat:@"%@\n%@: %@\nEmail: %@\nCustomer ID: %@", lang(@"ACTIVATION_SUCCESS"), lang(@"ACTIVATION"), [jsonResponse valueForKey:@"name"],[jsonResponse valueForKey:@"email"],[jsonResponse valueForKey:@"customer_id"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //    alert.tag = 55;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"SUCCESS") message:[NSString stringWithFormat:@"%@\n%@: %@\nEmail: %@\nCustomer ID: %@", lang(@"ACTIVATION_SUCCESS"), lang(@"ACTIVATION"), [jsonResponse valueForKey:@"name"],[jsonResponse valueForKey:@"email"],[jsonResponse valueForKey:@"customer_id"]] preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self onSuccessActivation];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) onCancelCreatePIN{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    //    [userDefault setObject:nil forKey:@"customer_id"];
    //    [userDefault setObject:nil forKey:@"email"];
    //    [userDefault setObject:nil forKey:@"zpk"];
    //    [userDefault setObject:nil forKey:@"session_id"];
    //    [userDefault setObject:nil forKey:@"publickey"];
    [NSUserdefaultsAes setObject:nil forKey:@"customer_id"];
    [NSUserdefaultsAes setObject:nil forKey:@"email"];
    [NSUserdefaultsAes setObject:nil forKey:@"zpk"];
    [NSUserdefaultsAes setObject:nil forKey:@"session_id"];
    [NSUserdefaultsAes setObject:nil forKey:@"publickey"];
    
    [userDefault synchronize];
    // [self backToR];
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
}


- (IBAction)actionMainBatal:(id)sender {
    // [self backToR];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
    }
}

-(void) toExit{
    // [self backToR];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
        
    }
}

-(void) getConfigApp{
    
    NSMutableURLRequest *request = [Utility BSMHeader:[NSString stringWithFormat:@"%@config.html",API_URL]];
    [request setHTTPMethod:@"GET"];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *menuAcceptRecentlyDef = @"00013,00017,00018,00025,00027,00028,00044,00049,00065,00066";
    @try {
        NSError *error;
        NSURLResponse *urlResponse;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if(error == nil){
            NSDictionary *mJsonObject = [NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&error];
            if(mJsonObject){
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.onboarding"] forKey:@"config_onboarding"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.indicator"] forKey:@"config_indicator"];
                //                [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
                //                [userDefault setObject:[mJsonObject valueForKey:@"config.timeout.session"] forKey:@"config_timeout_session"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];
                
                if ([[mJsonObject valueForKey:@"config.mutation.range"] integerValue] != 0) {
                    [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
                }else{
                    [userDefault setValue:@"15" forKey:@"config_mutation_range"];
                }
                
                if ([[mJsonObject valueForKey:@"config.timeout.session"] integerValue] != 0) {
                    [userDefault setObject:[mJsonObject valueForKey:@"config.timeout.session"] forKey:@"config_timeout_session"];
                }else{
                    [userDefault setValue:@"180" forKey:@"config_timeout_session"];
                }
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.act.iccid"] forKey:@"config.act.iccid"];
                
                if ([[mJsonObject valueForKey:@"config.allow.recently"] isEqualToString:@""] || [mJsonObject valueForKey:@"config.allow.recently"] == nil) {
                    [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
                }else{
                    [userDefault setObject:[mJsonObject valueForKey:@"config.allow.recently"] forKey:@"config.allow.recently"];
                }
                
                if ([mJsonObject objectForKey:@"config.enable.financing"] != 0 || [mJsonObject objectForKey:@"config.enable.financing"] != nil){
                    [userDefault setObject:[mJsonObject valueForKey:@"config.enable.financing"] forKey:@"config.enable.financing"];
                }
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.registration"] forKey:@"config_registration"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.onboarding"] forKey:@"config_onboarding"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.indicator"] forKey:@"config_indicator"];
//                [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
                //config.banner2.url config.banner2.link config.enable.banner
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner.url"] forKey:@"config_banner_url"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.type"] forKey:@"config_banner2_type"];

                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2a.link"] forKey:@"config_banner2a_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2a.url"] forKey:@"config_banner2a_url"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2b.link"] forKey:@"config_banner2b_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2b.url"] forKey:@"config_banner2b_url"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.url"] forKey:@"config_webchat_url"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.link"] forKey:@"config_webchat_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.clientid"] forKey:@"config_webchat_clientid"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.webchat.clientsecret"] forKey:@"config_webchat_clientsecret"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.trx_type.order"] forKey:@"config_trxtype_order"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.sms.longnumber"] forKey:@"config_sms_longnumber"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.source.activation"] forKey:@"config_source_activation"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.source.activation2"] forKey:@"config_source_activation2"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.sfeature.caption.id"] forKey:@"config_sfeature_captionid"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.sfeature.caption.en"] forKey:@"config_sfeature_captionen"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.prefix.otpcard"] forKey:@"config_prefix_otpcard"];
                    
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.activationfr"] forKey:@"config_enable_activationfr"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.activationfr.versionvalue"] forKey:@"config_activationfr_versionvalue"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.helpdesk.title.id"] forKey:@"config.helpdesk.title.id"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.helpdesk.title.en"] forKey:@"config.helpdesk.title.en"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2"] forKey:@"config_banner2"];

                NSString *conf = [userDefault objectForKey:@"config_banner2"];
                if(conf != nil){
                    NSArray *getData = [NSJSONSerialization JSONObjectWithData:[conf dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    
                    double versionNumber = [@VERSION_VALUE doubleValue];
                    NSMutableArray *arrs = [[NSMutableArray alloc]init];
                    for(NSDictionary *data in getData){
                        NSMutableDictionary *newDict = [[NSMutableDictionary alloc]init];
                        [newDict addEntriesFromDictionary:@{@"version":[[data objectForKey:@"version"] stringByReplacingOccurrencesOfString:@"." withString:@""], @"data": data}];
                        [arrs addObject:newDict];
                    }
                    for(int i = (int)arrs.count-1; i > 0 ; i--){
                        if([[[arrs objectAtIndex:i]objectForKey:@"version"]doubleValue] <= versionNumber){
                            NSDictionary *data = [[arrs objectAtIndex:i]objectForKey:@"data"];
                            [userDefault setObject:[data valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
                            [userDefault setObject:[data valueForKey:@"config.banner2.type"] forKey:@"config_banner2_type"];

                            [userDefault setObject:[data valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
                            [userDefault setObject:[data valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];

                            [userDefault setObject:[data valueForKey:@"config.banner2a.link"] forKey:@"config_banner2a_link"];
                            [userDefault setObject:[data valueForKey:@"config.banner2a.url"] forKey:@"config_banner2a_url"];

                            [userDefault setObject:[data valueForKey:@"config.banner2b.link"] forKey:@"config_banner2b_link"];
                            [userDefault setObject:[data valueForKey:@"config.banner2b.url"] forKey:@"config_banner2b_url"];
                            break;
                        }
                    }
                }
                
            }
            
//            NSLog(@"%@", [[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);

        }else{
//            NSLog(@"%@", error);
            [userDefault setObject:@(15) forKey:@"config_mutation_range"];
            [userDefault setObject:@(180) forKey:@"config_timeout_session"];
            [userDefault setObject:@(1) forKey:@"config.act.iccid"];
            [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
            [userDefault setObject:@(0) forKey:@"config_onboarding"];
            [userDefault setObject:@(0) forKey:@"config.enable.financing"];
        }
        
        
    } @catch (NSException *exception) {
//        NSLog(@"%@", exception);
        [userDefault setObject:@(15) forKey:@"config_mutation_range"];
        [userDefault setObject:@(180) forKey:@"config_timeout_session"];
        [userDefault setObject:@(1) forKey:@"config.act.iccid"];
        [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
        [userDefault setObject:@(0) forKey:@"config_onboarding"];
        [userDefault setObject:@(0) forKey:@"config.enable.financing"];
    }
    [userDefault synchronize];
}

@end
