//
//  DesclaimerViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 20/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface DesclaimerViewController : UIViewController{
    id delegate;
}

- (void) setDelegate: (id)newDelegate;


@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *btnClose;
@property (weak, nonatomic) IBOutlet UITextView *textviewContent;
@property (weak, nonatomic) IBOutlet UIImageView *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *labelCheck;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewButton;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@end

@protocol DesclaimerDelegate

@required

- (void) onDesclaimerCancel;
- (void) onDesclaimerAgree;

@end

NS_ASSUME_NONNULL_END
