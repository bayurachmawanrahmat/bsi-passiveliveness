//
//  ChangeEmailViewController.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "ChangeEmailViewController.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TemplateViewController.h"
#import "LV01ViewController.h"
#import "UIAlertController+AlertExtension.h"
#import "Styles.h"

@interface ChangeEmailViewController ()<ConnectionDelegate, UITextFieldDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
- (IBAction)next:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation ChangeEmailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark change batal to selanjutnya and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstraint];
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldEmail.frame.size.height);
    
    UIView *padding1 = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldEmail.leftView = padding1;
    self.textFieldEmail.leftViewMode = UITextFieldViewModeAlways;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
//    if([userDefault objectForKey:@"email"]){
    if(email){
//        self.textFieldEmail.text = [userDefault valueForKey:@"email"];
        self.textFieldEmail.text = email;
    }
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        [_btnB setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnN setTitle:@"Batal" forState:UIControlStateNormal];
        
        _lblEmail.text = @"Email";
        _lblDesc.text = @"Alamat Email";
    } else {
        
        [_btnB setTitle:@"Next" forState:UIControlStateNormal];
        [_btnN setTitle:@"Cancel" forState:UIControlStateNormal];
        
         _lblEmail.text = @"Email";
        _lblDesc.text = @"Email Address";
    }
    
    [_lblDesc setFont:[UIFont fontWithName:const_font_name3 size:14]];
    [_textFieldEmail setFont:[UIFont fontWithName:const_font_name3 size:14]];
    
    self.lblEmail.textColor = UIColorFromRGB(const_color_title);
    self.lblEmail.textAlignment = const_textalignment_title;
    self.lblEmail.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblEmail.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnB setColorSet:PRIMARYCOLORSET];
    [self.btnN setColorSet:SECONDARYCOLORSET];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    if([self.textFieldEmail.text isEqualToString:@""]){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"EMAIL_NOT_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alert addActionWithTitle:@"OK" actionWithStyle:UIAlertActionStyleDefault withTag:0 instanceOf:self];
        [self presentViewController:alert animated:YES completion:nil];

    }else if(![self validateEmailWithString:self.textFieldEmail.text]){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"INVALID_EMAIL") preferredStyle:UIAlertControllerStyleAlert];
        [alert addActionWithTitle:@"OK" actionWithStyle:UIAlertActionStyleDefault withTag:0 instanceOf:self];
        [self presentViewController:alert animated:YES completion:nil];

    }else{
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        NSString *parmUrl = [NSString stringWithFormat:@"request_type=change_email,email=%@,language",self.textFieldEmail.text];
        [conn sendPostParmUrl:parmUrl needLoading:true encrypted:true banking:true];
    }
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


#pragma mark change Cancel to goToCancel
- (IBAction)cancel:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"-" forKey:@"last"];
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self next:nil];
    return true;
}
#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(NSDictionary *)jsonObject withRequestType:(NSString *)requestType{
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        
        if (![requestType isEqualToString:@"check_notif"]) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addActionWithTitle:@"OK" actionWithStyle:UIAlertActionStyleDefault withTag:101 instanceOf:self];
            [self presentViewController:alert animated:YES completion:nil];
//            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//            [userDefault setObject:self.textFieldEmail.text forKey:@"email"];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//            [userDefault setObject:self.textFieldEmail.text forKey:@"email"];
            [NSUserdefaultsAes setObject:self.textFieldEmail.text forKey:@"email"];
            [userDefault synchronize];
        }
       
        
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addActionWithTitle:@"OK" actionWithStyle:UIAlertActionStyleDefault withTag:0 instanceOf:self];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    [alert addActionWithTitle:@"OK" actionWithStyle:UIAlertActionStyleDefault withTag:0 instanceOf:self];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)reloadApp{
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    [BSMDelegate reloadApp];
}

- (void)alertController:(UIAlertController *)alert withTag:(int)tag{
    if(tag == 101){
        NSDictionary* userInfo = @{@"position": @(1112)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        [self backToR];
    }
}

@end
