//
//  ChangePwdRevViewController.m
//  BSM-Mobile
//
//  Created by BSM on 2/27/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ChangePwdRevViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "NSString+MD5.h"
#import "Utility.h"
#import "CustomBtn.h"
#import "SKeychain.h"

@interface ChangePwdRevViewController ()<UIAlertViewDelegate, UITextFieldDelegate, UIScrollViewDelegate> {
    NSString *flagTPChangePwd1;
    NSString *flagTPChangePwd2;
    NSString *flagTPChangePwd3;
}

@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContentScrol;

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwSandiLama;
@property (weak, nonatomic) IBOutlet UIView *vwSandiBaru;
@property (weak, nonatomic) IBOutlet UIView *vwSandiConfirm;



@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOldPass;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPwd;
@property (weak, nonatomic) IBOutlet UIButton *btnEye1;
@property (weak, nonatomic) IBOutlet UIView *vwLine1;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPass;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwd;
@property (weak, nonatomic) IBOutlet UIButton *btnEye2;
@property (weak, nonatomic) IBOutlet UIView *vwLine2;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPassConfirm;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwdConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnEye3;
@property (weak, nonatomic) IBOutlet UIView *vwLine3;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
- (IBAction)next:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)togglePwd1:(id)sender;
- (IBAction)togglePwd2:(id)sender;
- (IBAction)togglePwd3:(id)sender;

@end

@implementation ChangePwdRevViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        NSString *alertTitle = @"";
        NSString *alertMessage = @"";
        if ([lang isEqualToString:@"en"]) {
            alertTitle = @"Information";
            alertMessage = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
        } else {
            alertTitle = @"Informasi";
            alertMessage = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 229;
        [alert show];
    }
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _vwScroll.delegate = self;
    _txtOldPwd.delegate = self;
    _txtNewPwd.delegate = self;
    _txtNewPwdConfirm.delegate = self;
    
    flagTPChangePwd1 = @"HIDE";
    flagTPChangePwd2 = @"HIDE";
    flagTPChangePwd3 = @"HIDE";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgToolbar;
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Ganti Kata Sandi";
        self.lblOldPass.text = @"Kata Sandi Lama";
        self.lblNewPass.text = @"Kata Sandi Baru";
        self.lblNewPassConfirm.text = @"Konfirmasi Kata Sandi Baru";
//        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
//        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnNext setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        msgToolbar = @"Selesai";
    } else {
        self.lblTitle.text = @"Change Password";
        self.lblOldPass.text = @"Old Password";
        self.lblNewPass.text = @"New Password";
        self.lblNewPassConfirm.text = @"Confirmation New Password";
//        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
//        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnNext setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Next" forState:UIControlStateNormal];
        msgToolbar = @"Done";
    }
    
    [_btnCancel setColorSet:PRIMARYCOLORSET];
    [_btnNext setColorSet:SECONDARYCOLORSET];

    [self setupLayout];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.txtOldPwd.inputAccessoryView = keyboardDoneButtonView;
    self.txtNewPwd.inputAccessoryView = keyboardDoneButtonView;
    self.txtNewPwdConfirm.inputAccessoryView = keyboardDoneButtonView;
    
    [self.txtOldPwd setSecureTextEntry:true];
    [self.txtNewPwd setSecureTextEntry:true];
    [self.txtNewPwdConfirm setSecureTextEntry:true];
    
    [self registerForKeyboardNotifications];
    
    
}

-(void)setupLayout{
    //vw parent
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwContentScroll = self.vwContentScrol.frame;
    CGRect frmVwTitle = self.vwTitle.frame;
    
    //vw child one
    CGRect frmVwSandiLama = self.vwSandiLama.frame;
    CGRect frmVwSandiBaru = self.vwSandiBaru.frame;
    CGRect frmVwSandiConfirm = self.vwSandiConfirm.frame;
    CGRect frmButtonNext = self.btnNext.frame;
    CGRect frmButtonCancel = self.btnCancel.frame;
    
    
    //vw child two
    CGRect frmLabelOldPass = self.lblOldPass.frame;
    CGRect frmLabelNewPass = self.lblNewPass.frame;
    CGRect frmLabelNewPassConfirm = self.lblNewPassConfirm.frame;
    CGRect frmTextOldPass = self.txtOldPwd.frame;
    CGRect frmTextNewPass = self.txtNewPwd.frame;
    CGRect frmTextNewPassConfirm = self.txtNewPwdConfirm.frame;
    CGRect frmEye1 = self.btnEye1.frame;
    CGRect frmEye2 = self.btnEye2.frame;
    CGRect frmEye3 = self.btnEye3.frame;
    CGRect frmLine1 = self.vwLine1.frame;
    CGRect frmLine2 = self.vwLine2.frame;
    CGRect frmLine3 = self.vwLine3.frame;
    
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = SCREEN_WIDTH - 32;
    frmLblTitle.size.height = 40;
    
    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = SCREEN_HEIGHT - (frmVwTitle.origin.y + frmVwTitle.size.height) - BOTTOM_NAV_GEN;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = frmVwScroll.size.width;
    
    
    frmVwSandiLama.origin.y = 0;
    frmVwSandiLama.origin.x = 0;
    frmVwSandiLama.size.width = frmVwContentScroll.size.width;
    
    frmVwSandiBaru.origin.y = frmVwSandiLama.origin.y + frmVwSandiLama.size.height + 8;
    frmVwSandiBaru.origin.x = 0;
    frmVwSandiBaru.size.width = frmVwSandiLama.size.width;
    
    frmVwSandiConfirm.origin.y = frmVwSandiBaru.origin.y + frmVwSandiBaru.size.height + 8;
    frmVwSandiConfirm.origin.x = 0;
    frmVwSandiConfirm.size.width = frmVwSandiLama.size.width;
    
    frmButtonNext.origin.x = 16;
    frmButtonNext.origin.y = frmVwSandiConfirm.origin.y + frmVwSandiConfirm.size.height + 16;
    frmButtonNext.size.width = (frmVwContentScroll.size.width/2) - 16;
    
    frmButtonCancel.origin.x = frmButtonNext.origin.x + frmButtonNext.size.width + 8;
    frmButtonCancel.origin.y = frmButtonNext.origin.y;
    frmButtonCancel.size.width = frmButtonNext.size.width - 8;
    
    frmVwContentScroll.size.height = frmButtonNext.origin.y + frmButtonNext.size.height + 16;
    
    frmLabelOldPass.size.width = frmVwSandiLama.size.width - (frmLabelOldPass.origin.x * 2);
    frmLine1.size.width  = frmVwSandiLama.size.width - (frmLine1.origin.x * 2);
    frmEye1.origin.x = frmVwSandiLama.size.width - frmEye1.size.width - 16;
    frmTextOldPass.origin.x = frmLine1.origin.x;
    frmTextOldPass.size.width = frmEye1.origin.x - 10;
    
    
    frmLabelNewPass.size.width = frmLabelOldPass.size.width;
    frmLabelNewPass.origin.y = 4;
    frmLine2.size.width = frmLine1.size.width;
    frmEye2.origin.x  = frmEye1.origin.x;
    frmTextNewPass.origin.x = frmTextOldPass.origin.x;
    frmTextNewPass.size.width = frmTextOldPass.size.width;
    
    frmLabelNewPassConfirm.size.width = frmLabelOldPass.size.width;
    frmLabelNewPassConfirm.origin.y = 4;
    frmLine3.size.width = frmLine1.size.width;
    frmEye3.origin.x = frmEye1.origin.x;
    frmTextNewPassConfirm.origin.x = frmTextOldPass.origin.x;
    frmTextNewPassConfirm.size.width = frmTextOldPass.size.width;
    
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveNotch]) {
        frmVwScroll.size.height = SCREEN_HEIGHT - TOP_NAV - BOTTOM_NAV_X;
    }
    
    //vw parent
    self.vwScroll.frame = frmVwScroll;
    self.vwContentScrol.frame = frmVwContentScroll;
    
    //vw child one
    self.vwTitle.frame = frmVwTitle;
    self.vwSandiLama.frame = frmVwSandiLama;
    self.vwSandiBaru.frame = frmVwSandiBaru;
    self.vwSandiConfirm.frame = frmVwSandiConfirm;
    self.btnNext.frame = frmButtonNext;
    self.btnCancel.frame = frmButtonCancel;
    
    
    //vw child two
    self.lblOldPass.frame = frmLabelOldPass;
    self.lblNewPass.frame = frmLabelNewPass;
    self.lblNewPassConfirm.frame = frmLabelNewPassConfirm;
    self.txtOldPwd.frame = frmTextOldPass;
    self.txtNewPwd.frame = frmTextNewPass;
    self.txtNewPwdConfirm.frame = frmTextNewPassConfirm;
    self.btnEye1.frame = frmEye1;
    self.btnEye2.frame = frmEye2;
    self.btnEye3.frame = frmEye3;
    self.vwLine1.frame = frmLine1;
    self.vwLine2.frame = frmLine2;
    self.vwLine3.frame = frmLine3;
    
    [self.vwScroll setContentSize:CGSizeMake(frmVwContentScroll.size.width, frmVwContentScroll.size.height)];
    
    [self.imgTitle setHidden:YES];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height + kbSize.height + 20)];
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)togglePwd1:(id)sender {
    [Utility animeBounchCb:sender];
    if ([flagTPChangePwd1 isEqualToString:@"SHOW"]) {
        flagTPChangePwd1 = @"HIDE";
        [self.txtOldPwd setSecureTextEntry:true];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPChangePwd1 isEqualToString:@"HIDE"]) {
        flagTPChangePwd1 = @"SHOW";
        [self.txtOldPwd setSecureTextEntry:false];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)togglePwd2:(id)sender {
     [Utility animeBounchCb:sender];
    if ([flagTPChangePwd2 isEqualToString:@"SHOW"]) {
        flagTPChangePwd2 = @"HIDE";
        [self.txtNewPwd setSecureTextEntry:true];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPChangePwd2 isEqualToString:@"HIDE"]) {
        flagTPChangePwd2 = @"SHOW";
        [self.txtNewPwd setSecureTextEntry:false];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)togglePwd3:(id)sender {
     [Utility animeBounchCb:sender];
    if ([flagTPChangePwd3 isEqualToString:@"SHOW"]) {
        flagTPChangePwd3 = @"HIDE";
        [self.txtNewPwdConfirm setSecureTextEntry:true];
        [self.btnEye3 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPChangePwd3 isEqualToString:@"HIDE"]) {
        flagTPChangePwd3 = @"SHOW";
        [self.txtNewPwdConfirm setSecureTextEntry:false];
        [self.btnEye3 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}


#pragma mark change cancel to goToNext
- (IBAction)cancel:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"-" forKey:@"last"];
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    if([self.txtOldPwd.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if([self.txtNewPwd.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if([self.txtNewPwdConfirm.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if(![self.txtNewPwd.text isEqualToString:self.txtNewPwdConfirm.text]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:lang(@"PWD_NOT_SAME") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldRepeatPwd = [self.txtNewPwdConfirm.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        NSString *trimmedStringtextFieldNewPwd = [self.txtNewPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        NSString *trimmedStringtextFieldOldPwd = [self.txtOldPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if (([trimmedStringtextFieldRepeatPwd length] == 0) || ([trimmedStringtextFieldNewPwd length] == 0) || ([trimmedStringtextFieldOldPwd length] == 0)) {
            message = lang(@"PWD_NOT_ALPHANUM");
        }
        
        NSString *trimmedDectextFieldRepeatPwd = [self.txtNewPwdConfirm.text stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        NSString *trimmedDectextFieldNewPwd = [self.txtNewPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        NSString *trimmedDectextFieldOldPwd = [self.txtOldPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
        if (([trimmedDectextFieldRepeatPwd length] == 0) || ([trimmedDectextFieldNewPwd length] == 0) || ([trimmedDectextFieldOldPwd length] == 0)) {
            message = lang(@"PWD_NOT_ALPHANUM");
        }
        
        if (([self.txtOldPwd.text length] < 6) || ([self.txtOldPwd.text length] < 6) || ([self.txtOldPwd.text length] < 6) || ([self.txtOldPwd.text length] > 8) || ([self.txtOldPwd.text length] > 8) || ([self.txtOldPwd.text length] > 8)) {
            message = lang(@"PWD_MUST_6_DIGIT");
        }
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//        NSString *upwd = [userDefault objectForKey:@"password"];
        NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
//        NSString *upwd = [SKeychain loadObjectForKey:@"password"];

        NSString *smsg = @"";
        if (!([[self.txtOldPwd.text MD5] isEqualToString:upwd])) {
            if([lang isEqualToString:@"id"]){
                message = @"Kata Sandi Salah";
            } else {
                message = @"Wrong Password";
            }
        }
        
        if([message isEqualToString:@""]){
            NSString *pwd = self.txtNewPwd.text;
            
//            [userDefault setObject:[pwd MD5] forKey:@"password"];
            [NSUserdefaultsAes setObject:[pwd MD5] forKey:@"password"];
            [userDefault synchronize];

//            [SKeychain saveObject:[pwd MD5] forKey:@"password"];
            
            if([lang isEqualToString:@"id"]){
                smsg = @"Kata sandi berhasil diubah\nSilahkan login kembali.";
            } else {
                smsg = @"Change password success\nPlease login again.";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:smsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 752;
            [alert show];
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 123) {
        //Wrong Password
    } else if (alertView.tag == 456) {
        //Disable Password
        exit(0);
    } else if (alertView.tag == 229) {
        //Not Activated
        NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    } else if (alertView.tag == 752) {
        //Change Password
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault synchronize];
        
        if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
            CGRect screenBound = [[UIScreen mainScreen] bounds];
            CGSize screenSize = screenBound.size;
            CGFloat screenWidth = screenSize.width;
            CGFloat screenHeight = screenSize.height;
            
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            UIViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
            templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else {
            exit(0);
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self resetTimer];
    
    return YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resetTimer];
    
}

-(void) resetTimer{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
