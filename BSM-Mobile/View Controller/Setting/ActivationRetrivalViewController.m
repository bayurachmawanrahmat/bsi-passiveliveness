//
//  ActivationRetrivalViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 06/09/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "ActivationRetrivalViewController.h"
#import <MessageUI/MessageUI.h>
#import "AESCrypt.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface ActivationRetrivalViewController ()<MFMessageComposeViewControllerDelegate, UIAlertViewDelegate, ConnectionDelegate> {
    
    NSString *receipentsNumber;
    NSArray *recipentsArr;
}


@property (weak, nonatomic) IBOutlet UITextField *textFieldInput;
- (IBAction)next:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;
@property (weak, nonatomic) IBOutlet UITextView *txtView;

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UIView *vwButton;



@end


@implementation ActivationRetrivalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    
}

-(void) showAlertMintaAktivasi{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msg = @"";
    NSString *msgCancel = @"";
    NSString *ok = @"";
    if([lang isEqualToString:@"id"]) {
        msg = @"Dengan mengakses menu 'Minta Kode Aktivasi Ulang', Anda yakin untuk melakukan permintaan kode aktivasi?";
        msgCancel = @"Batal";
        ok = @"Lanjut";
    } else {
        msg = @"By accessing the 'Request Reactivation Code', you are sure to request an activation code?";
        msgCancel = @"Cancel";
        ok = @"Next";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:msgCancel otherButtonTitles:ok, nil];
    alert.delegate = self;
    alert.tag = 2525;
    [alert show];
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    receipentsNumber = @"";
    
    if([lang isEqualToString:@"id"]){
//        [_btnB setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnB setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnN setTitle:@"Batal" forState:UIControlStateNormal];
        [self.labelTitle setText:@"Minta Kode Aktivasi Ulang"];
        _txtView.text = @"Kode aktivasi akan dikirim melalui SMS ke nomor ponsel pelanggan yang terdaftar di BSI Mobile. Pastikan di ponsel Anda ada aplikasi BSI mobile, nomor ponsel Anda harus terdaftar di Bank Syariah Indonesia dan dapat digunakan untuk mengirim SMS.";
    } else {
//        [_btnB setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Next" forState:UIControlStateNormal];
        [_btnB setTitle:@"Next" forState:UIControlStateNormal];
        [_btnN setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.labelTitle setText:@"Request Reactivation Code"];
        _txtView.text = @"The activation code will be sent via SMS to the customer's mobile number registered at BSI Mobile. Please make sure on your mobile phone there is a BSI mobile apps, your mobile number should be registered at Bank Syariah Indonesia and can be used to send SMS.";
    }
    
    
    //Syariah Indonesia styles
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [_txtView setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
    [self.btnN setColorSet:SECONDARYCOLORSET];
    [self.btnB setColorSet:PRIMARYCOLORSET];
    
    [self setupLayout];
    
    self.txtView.editable = false;
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldInput.frame.size.height);
    
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldInput.leftView = paddingPhone;
    self.textFieldInput.leftViewMode = UITextFieldViewModeAlways;
    
    [self.textFieldInput setPlaceholder:@"PIN"];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.textFieldInput.inputAccessoryView = keyboardDoneButtonView;
    
    //CGRect screenBound = [[UIScreen mainScreen] bounds];
    //CGSize screenSize = screenBound.size;
    //CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    //CGRect frmTV = self.txtView.frame;
    //frmTV.size.width = screenWidth - 32;
    //self.txtView.frame = frmTV;
    
    [self showAlertMintaAktivasi];
    [self setRequest];
}

- (void) setRequest{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if([userDefault objectForKey:@"config_source_activation2"] != nil){
        if([[userDefault valueForKey:@"config_source_activation2"] isEqualToString:@"1"]){
            
            NSString *nohp = @"";
//            if([userDefault objectForKey:@"msisdn"] != nil){
//                nohp = [userDefault valueForKey:@"msisdn"];
//            }else if([userDefault objectForKey:@"mobilnumber"] != nil){
//                nohp = [userDefault valueForKey:@"mobilnumber"];
//            }
            
            if([NSUserdefaultsAes getValueForKey:@"msisdn"] != nil){
                nohp = [NSUserdefaultsAes getValueForKey:@"msisdn"];
            }
//			else if([userDefault objectForKey:@"mobilnumber"] != nil){
//                nohp = [userDefault valueForKey:@"mobilnumber"];
//            }
            
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            NSString *url = [NSString stringWithFormat:@"request_type=check_smspool_roundrobin2,nohp=%@",nohp];
            [conn sendPostParmUrl:url needLoading:true encrypted:false banking:false];
        }else{
            receipentsNumber = @"3339";
        }
    }else{
        receipentsNumber = @"3339";
    }
}

-(void)setupLayout{
    CGRect frmVwTitle =self.vwTitle.frame;
    CGRect frmTxtView = self.txtView.frame;
    CGRect frmTxtInput = self.textFieldInput.frame;
    CGRect frmVwButton =self.vwButton.frame;
    CGRect frmVwLine  = self.vwLine.frame;
    
    CGRect frmBtnCancel = self.btnN.frame;
    CGRect frmBtnNext = self.btnB.frame;
    
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmTxtView.origin.x = 16;
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveNotch]) {
         frmTxtView.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 32;
    }
    frmTxtView.size.width = SCREEN_WIDTH - (frmTxtView.origin.x*2);
    
    frmTxtInput.origin.y = frmTxtView.origin.y + frmTxtView.size.height + 32;
    frmTxtInput.origin.x = 16;
    frmTxtInput.size.width = SCREEN_WIDTH - (frmTxtInput.origin.x *2);
    
    frmVwLine.origin.y = frmTxtInput.origin.y + frmTxtInput.size.height;
    frmVwLine.origin.x = frmTxtInput.origin.x;
    frmVwLine.size.width = frmTxtInput.size.width;
    
    frmVwButton.origin.y = frmVwLine.origin.y + frmVwLine.size.height + 32;
    frmVwButton.origin.x = 0;
    frmVwButton.size.width = SCREEN_WIDTH;
    
    frmBtnCancel.origin.x = 16;
    frmBtnCancel.size.width = (frmVwButton.size.width/2) - 16;
    frmBtnCancel.origin.y = 0;
    
    frmBtnNext.origin.x = frmBtnCancel.origin.x + frmBtnCancel.size.width + 8;
    frmBtnNext.size.width = frmBtnCancel.size.width - 8;
    frmBtnNext.origin.y = 0;
    
   
    self.vwTitle.frame = frmVwTitle;
    self.txtView.frame = frmTxtView;
    self.textFieldInput.frame = frmTxtInput;
    self.vwLine.frame=  frmVwLine;
    self.vwButton.frame = frmVwButton;
    self.btnN.frame  = frmBtnCancel;
    self.btnB.frame = frmBtnNext;
    
}

- (void)viewDidAppear:(BOOL)animated{
    
}


- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
//    [self backToRoot];
}

-(void) goToNext{
    [self.view endEditing:YES];
    double delayInSeconds = 0.6;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if([self.textFieldInput.text isEqualToString:@""]){
            
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
            [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alerts animated:YES completion:nil];
        }else{
            NSString *message = @"";
            NSString *trimmedStringtextFieldInput = [self.textFieldInput.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
            if ([trimmedStringtextFieldInput length]) {
                message = lang(@"STR_NOT_NUM");
            }
            
            if([message isEqualToString:@""]){
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *msg = @"";
                NSString *msgCancel = @"";
                NSString *ok = @"";
                if([lang isEqualToString:@"id"]) {
                    msg = @"Pulsa anda akan terpotong untuk pengiriman kode aktivasi via SMS, apakah anda ingin melanjutkan?";
                    msgCancel = @"Batal";
                    ok = @"Lanjut";
                } else {
                    msg = @"Your phone credit will be deducted to send the activation code via SMS, do you want to continue?";
                    msgCancel = @"Cancel";
                    ok = @"Next";
                }
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:msgCancel otherButtonTitles:ok, nil];
                alert.delegate = self;
                alert.tag = 3131;
                [alert show];
            } else {
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alerts animated:YES completion:nil];
            }
        }
    });
}

#pragma mark change cancel to goToNext
- (IBAction)cancel:(id)sender {
    [self goToNext];
    
}

-(void) goToCancel{
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
//    if ([openRoot isEqualToString:@"@"]) {
//        [userDefault removeObjectForKey:@"openRoot"];
//        [userDefault setValue:@"-" forKey:@"last"];
//        [[DataManager sharedManager] resetObjectData];
//        [self.navigationController popToRootViewControllerAnimated:YES];
//        if(self.tabBarController.selectedIndex != 0){
//            [self.tabBarController setSelectedIndex:0];
//        }
//    } else {
//        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
//        self.slidingViewController.topViewController = menuVC.homeNavigationController;
//    }
    
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
    
    MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textFieldInput resignFirstResponder];
}


- (void)messageComposeViewController:(nonnull MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    switch (result) {
        case MessageComposeResultCancelled:
        {
            [self dismissViewControllerAnimated:YES completion:^{
                [self backToR];
            }];

        }
            break;
            
        case MessageComposeResultFailed:
        {
            NSString *msgWarning = @"";
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];

            if ([lang isEqualToString:@"id"]){
                msgWarning = @"Ooups, terdapat kesalahan pada saat mengirim SMS!";
            }else{
                msgWarning = @"Ooups, error while sending SMS!";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:msgWarning preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            break;
        }
            
        case MessageComposeResultSent:{
//            [self backMainApp];
            [self dismissViewControllerAnimated:YES completion:^{
                [self backToR];
            }];
            break;
        }
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if (alertView.tag == 3131) {
        if(buttonIndex == 1){
            [self.view endEditing:YES];
           
            NSString *msg = @"";
            NSString *ok = @"";
            if([lang isEqualToString:@"id"]) {
                msg = @"Kode aktivasi Anda akan dikirimkan via SMS. Silahkan cek kotak pesan Anda.";
                ok = @"Ok";
            } else {
                msg = @"Your activation code will be sent via SMS. Please check your message.";
                ok = @"Ok";
            }
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:ok, nil];
            alert.delegate = self;
            alert.tag = 3232;
            [alert show];
        }
    } else if (alertView.tag == 3232) {
        NSString *lg = @"";
        if([lang isEqualToString:@"id"]) {
            lg = @"id";
        } else {
            lg = @"en";
        }
        NSString *plainText = [NSString stringWithFormat:@"%@#%@", self.textFieldInput.text, lg];
        NSString *dt = [AESCrypt encrypt:plainText];
//        NSString *ori = [AESCrypt decrypt:dt];
//        // try to SMS
        if(![MFMessageComposeViewController canSendText]) {
            NSString *msgWarning = @"";
            if([lang isEqualToString:@"id"]){
                msgWarning = @"Perangkat Anda tidak mendukung SMS!";
            }else{
                msgWarning = @"Your device doesn't support SMS!";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:msgWarning preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            
            return;
        }
        
//        NSArray *recipents = @[@"3339"];
        NSMutableArray *recipents = [[NSMutableArray alloc]init];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        if([receipentsNumber isEqualToString:@""]){
            NSArray *smsLongNumberArrays = [[userDefault valueForKey:@"config_sms_longnumber"] componentsSeparatedByString:@"|"];
            NSUInteger myCount = [smsLongNumberArrays count];
            NSMutableArray *recipentsPrefix = [[NSMutableArray alloc]init];
            
            NSString *nohp = @"";
//            if([userDefault objectForKey:@"msisdn"] != nil){
//                nohp = [userDefault valueForKey:@"msisdn"];
//            }else if([userDefault objectForKey:@"mobilnumber"] != nil){
//                nohp = [userDefault valueForKey:@"mobilnumber"];
//            }
//            
            if([NSUserdefaultsAes getValueForKey:@"msisdn"] != nil){
                nohp = [NSUserdefaultsAes getValueForKey:@"msisdn"];
            }
//			else if([userDefault objectForKey:@"mobilnumber"] != nil){
//                nohp = [userDefault valueForKey:@"mobilnumber"];
//            }
            
            BOOL hasPrefix = false;
            for(NSString* numbers in smsLongNumberArrays){
                if([nohp isEqualToString:@""]){
                    [recipentsPrefix addObject:numbers];
                }else{
                    if([numbers hasPrefix:[nohp substringToIndex:4]]) {
                        [recipentsPrefix addObject:numbers];
                        hasPrefix = true;
                    }
                }
            }
            
            if(hasPrefix){
                [recipents addObject:[recipentsPrefix objectAtIndex:arc4random_uniform((int)recipentsPrefix.count)]];
            }else{
                [recipents addObject:[smsLongNumberArrays objectAtIndex:arc4random_uniform((int)myCount)]];
            }
        }else{
            [recipents addObject:receipentsNumber];
        }
        
//        NSArray *recipents = recipentsArr;
        NSString *message = [NSString stringWithFormat:@"acr %@", dt];
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setBody:message];

//        [self backMainApp];

        [self presentViewController:messageController animated:YES completion:nil];


//        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//        [userDefault setValue:@"-" forKey:@"last"];
//        [[DataManager sharedManager] resetObjectData];
//        [self.navigationController popToRootViewControllerAnimated:YES];
//        if(self.tabBarController.selectedIndex != 0){
//            [self.tabBarController setSelectedIndex:0];
//        }
        
    } else if (alertView.tag == 2525) {
        if (buttonIndex == 1) {
            
        }
        else {
            [self goToCancel];
        }
    }
}

-(void) backMainApp{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"-" forKey:@"last"];
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"check_smspool_roundrobin"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            
            NSDictionary *dict  = [NSJSONSerialization
            JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding]
            options:NSJSONReadingAllowFragments
                                   error:nil];
            receipentsNumber =[dict valueForKey:@"MSISDN"];
        }
    }
}

@end

