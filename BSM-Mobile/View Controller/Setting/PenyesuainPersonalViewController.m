//
//  PenyesuainPersonalViewController.m
//  BSM-Mobile
//
//  Created by Macbook Air on 23/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PenyesuainPersonalViewController.h"
#import "Utility.h"
#import "CellSliderPersonal.h"
#import <MessageUI/MessageUI.h>
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface PenyesuainPersonalViewController()<UITableViewDataSource,UITableViewDelegate>{
    float sizeTV;
    float constTV;
    BOOL isResize;
    NSString *lang;
    CGFloat widthContentTac;
    
}

@property (strong, nonatomic) NSArray<NSString *> *secTitleWaktuSholat;
@property (strong, nonatomic) NSArray<NSString *> *secTitleDescWaktuSholat;

@property (weak, nonatomic) IBOutlet UIView *vwMainTac;
@property (weak, nonatomic) IBOutlet UIView *vwContentTac;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleTac;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePenyesuian;

@property (weak, nonatomic) IBOutlet UIButton *btnExit;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;

@property (weak, nonatomic) IBOutlet UIButton *btnDismis;

- (IBAction)dismisListener:(id)sender;


@property (weak, nonatomic) IBOutlet UITableView *tablePersonalSlider;
- (IBAction)exitListener:(id)sender;

- (IBAction)actionSlider:(UISlider *)sender;

- (IBAction)resetListener:(id)sender;


@end

@implementation PenyesuainPersonalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (IBAction)exitListener:(id)sender {
    //[self refreshData];
    [self dismissViewControllerAnimated:YES completion:nil];
    NSDictionary* userInfo = @{@"state": @"done"};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"optimasiJadwal" object:self userInfo:userInfo];
}

-(void) refreshData{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
    }
}

- (IBAction)actionSlider:(UISlider *)sender {
    CellSliderPersonal *cell = (CellSliderPersonal *)[_tablePersonalSlider cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]]; // I assume you have only 1 section
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:[NSString stringWithFormat:@"%d",(int)cell.slideWaktu.value] forKey:[NSString stringWithFormat:@"custo_time_%d",(int) sender.tag + 1]];
    NSString *txtLblWaktu;
    if([lang isEqualToString:@"id"]){
        if ((int)cell.slideWaktu.value > 0) {
            txtLblWaktu = [NSString stringWithFormat:@"Tambah %d %@", (int)cell.slideWaktu.value, @"menit"];
        }else{
            txtLblWaktu = [NSString stringWithFormat:@"%d %@", (int)cell.slideWaktu.value, @"menit"];
        }
        
        cell.lblWaktu.text = [txtLblWaktu stringByReplacingOccurrencesOfString:@"-" withString:@"Kurang -"];
    }else{
        if ((int)cell.slideWaktu.value > 0) {
            txtLblWaktu = [NSString stringWithFormat:@"Increase %d %@", (int)cell.slideWaktu.value, @"minutes"];
        }
        else if((int)cell.slideWaktu.value == 0){
            txtLblWaktu = [NSString stringWithFormat:@"%d %@", (int)cell.slideWaktu.value, @"minute"];
        }else{
            txtLblWaktu = [NSString stringWithFormat:@"%d %@", (int)cell.slideWaktu.value, @"minutes"];
        }
        
        cell.lblWaktu.text = [txtLblWaktu stringByReplacingOccurrencesOfString:@"-" withString:@"Decrease -"];
    }
    
    [cell.lblWaktu sizeToFit];
    CGRect frmContentTac = _vwContentTac.frame;
    CGRect frmWaktu = cell.lblWaktu.frame;
    frmWaktu.origin.x = frmContentTac.size.width - frmWaktu.size.width - 16;
    cell.lblWaktu.frame = frmWaktu;
    
    NSDictionary* userInfo = @{@"actionTimer": @"RESET_TIMER"};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"isViewTimer" object:self userInfo:userInfo];
    
}

- (IBAction)resetListener:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    for (int i=0; i < self.secTitleWaktuSholat.count; i++) {
        [userDefault setObject:@"0" forKey:[NSString stringWithFormat:@"custo_time_%d",(i + 1)]];
    }
    [self.tablePersonalSlider reloadData];
}

- (void)viewDidLoad{
    [self setupLayout];
    
}

-(void)setupLayout{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmMainTc = _vwMainTac.frame;
    CGRect frmBtnDismiss = _btnDismis.frame;
    CGRect frmContentMainTc = _vwContentTac.frame;
    CGRect frmLblTitleTac = _lblTitleTac.frame;
    CGRect frmLblTitlePenyesuaian = _lblTitlePenyesuian.frame;
    CGRect frmTableSlider = _tablePersonalSlider.frame;
    CGRect frmBtnExit = _btnExit.frame;
    CGRect frmBtnRest = _btnReset.frame;
   
    
    frmMainTc.size.height = screenHeight;
    frmMainTc.size.width = screenWidth;
    
    frmBtnDismiss.size.height = screenHeight;
    frmBtnDismiss.size.width = screenWidth;
    
    frmContentMainTc.size.width = screenWidth - 30;
    frmContentMainTc.size.height = screenHeight - frmContentMainTc.origin.y -  75;
    frmContentMainTc.origin.y = 135;
    
    widthContentTac = frmContentMainTc.size.width;
    
    if([Utility isDeviceHaveNotch]){
        frmContentMainTc.size.height = frmContentMainTc.size.height - frmContentMainTc.origin.y - 95;
    }else if(IPHONE_5){
        frmContentMainTc.origin.x = frmContentMainTc.origin.x - 5;
        frmContentMainTc.size.height = frmContentMainTc.size.height - frmBtnRest.size.height - 40;
    }else{
        frmContentMainTc.origin.x = frmContentMainTc.origin.x - 5;
        frmContentMainTc.size.height = frmContentMainTc.size.height - frmBtnRest.size.height - 55;
    }
    
    frmBtnRest.size.width = frmContentMainTc.size.width;
    frmBtnRest.origin.y = frmContentMainTc.size.height - frmBtnRest.size.height;
    
    frmBtnExit.origin.x = frmContentMainTc.size.width - frmBtnExit.size.width - 16;
    
    frmLblTitleTac.origin.y = (frmBtnExit.origin.y + frmBtnExit.size.height)/2;
    
    frmLblTitlePenyesuaian.origin.y = frmLblTitleTac.origin.y + frmLblTitleTac.size.height;
    frmLblTitlePenyesuaian.size.width = screenWidth - 16;
    
    
    frmTableSlider.size.width = frmContentMainTc.size.width;
    frmTableSlider.size.height =  frmContentMainTc.size.height - (frmLblTitlePenyesuaian.origin.y + frmLblTitlePenyesuaian.size.height + frmBtnRest.size.height + 16);
    frmTableSlider.origin.y = frmLblTitlePenyesuaian.origin.y + frmLblTitlePenyesuaian.size.height + 20;
    
    _vwMainTac.frame = frmMainTc;
    _btnDismis.frame = frmBtnDismiss;
    _vwContentTac.frame = frmContentMainTc;
    _vwContentTac.layer.cornerRadius = 10;
    _vwContentTac.layer.borderWidth = 2;
    _vwContentTac.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    _lblTitleTac.frame = frmLblTitleTac;
    _lblTitlePenyesuian.frame = frmLblTitlePenyesuaian;
    _tablePersonalSlider.frame = frmTableSlider;
    _btnExit.frame = frmBtnExit;
    _btnReset.frame = frmBtnRest;
    _btnReset.layer.cornerRadius = frmBtnRest.size.height/2;
    _btnReset.layer.masksToBounds = YES;
    [_btnReset setBackgroundColor:UIColorFromRGB(const_color_primary)];
    
    
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        _lblTitleTac.text = @"Koreksi Manual";
        _lblTitlePenyesuian.text = @"Tambah atau kurangi waktu sholat dari standarnya";
        [_btnReset setTitle:@"Atur Ulang" forState: UIControlStateNormal];
        self.secTitleWaktuSholat = @[@"Subuh", @"Terbit", @"Dzuhur", @"Ashar", @"Maghrib",@"Isya"];
        self.secTitleDescWaktuSholat = @[@"Sesuaikan personal waktu sholat subuh", @"Sesuaikan personal waktu terbit", @"Sesuaikan personal waktu sholat dzuhur", @"Sesuaikan personal waktu sholat ashar", @"Sesuaikan personal waktu sholat maghrib", @"Sesuaikan personal waktu sholat isya"];
    }else{
        _lblTitleTac.text = @"Manual Correction";
        _lblTitlePenyesuian.text = @"Increase or decrease prayer times from the standard";
       [_btnReset setTitle:@"Reset" forState: UIControlStateNormal];
        self.secTitleWaktuSholat = @[@"Fajr", @"Sunrise", @"Dhuhr", @"Asr", @"Maghrib",@"Isha"];
        self.secTitleDescWaktuSholat = @[@"Customize personal prayer times fajr", @"Adjust personal times sunrise", @"Customize personal prayer times dhuhr", @"Customize personal prayer times asr", @"Customize personal prayer times maghrib", @"Customize personal prayer times isha"];
    }
    
    [_lblTitleTac sizeToFit];
    [_lblTitlePenyesuian sizeToFit];
    
    if(IPHONE_5){
        [_lblTitleTac setFont:[UIFont fontWithName:const_font_name1 size:13.0f]];
        [_lblTitlePenyesuian setFont:[UIFont fontWithName:const_font_name1 size:10.0f]];
    }
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.secTitleWaktuSholat.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellSliderPersonal *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSliderPersonal" forIndexPath:indexPath];
    if(cell == nil){
        cell = [[CellSliderPersonal alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellSliderPersonal"];
        
    }

    cell.lblTitleWaktu.text = [_secTitleWaktuSholat objectAtIndex:indexPath.row];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *customTimeSholat = [userDefault valueForKey:[NSString stringWithFormat:@"custo_time_%d",(int)indexPath.row + 1]];
    if(customTimeSholat == nil || [customTimeSholat isEqualToString:@""]){
        customTimeSholat = @"0";
    }
    if([lang isEqualToString:@"id"]){
        if ([customTimeSholat integerValue] == 0) {
            cell.lblWaktu.text = [NSString stringWithFormat:@"%@ menit",customTimeSholat];
        }else if([customTimeSholat integerValue]>0){
            cell.lblWaktu.text = [NSString stringWithFormat:@"Tambah %@ menit",customTimeSholat];
        }
        else{
            cell.lblWaktu.text = [NSString stringWithFormat:@"%@ menit", [customTimeSholat stringByReplacingOccurrencesOfString:@"-" withString:@"Kurang -"]];
        }
        
        
    }else{
        if ([customTimeSholat integerValue] == 0) {
            cell.lblWaktu.text = [NSString stringWithFormat:@"%@ minute", [customTimeSholat stringByReplacingOccurrencesOfString:@"-" withString:@"Decrease -"]];
        }else if([customTimeSholat integerValue]>0){
            cell.lblWaktu.text = [NSString stringWithFormat:@"Increase %@ minutes",customTimeSholat];
        }
        else{
            cell.lblWaktu.text = [NSString stringWithFormat:@"%@ minutes", [customTimeSholat stringByReplacingOccurrencesOfString:@"-" withString:@"Decrease -"]];
        }
        
    }
    
    cell.lblWaktu.tag = indexPath.row;
    
    cell.slideWaktu.tag = indexPath.row;
    cell.slideWaktu.minimumValue = -60;
    cell.slideWaktu.maximumValue = 60;
    cell.slideWaktu.value = [customTimeSholat floatValue];
    cell.slideWaktu.continuous = YES;
    cell.slideWaktu.minimumTrackTintColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
    
    [cell.lblTitleWaktu sizeToFit];
    [cell.lblWaktu sizeToFit];
    
    
    CGRect frmLblTitleWaktu = cell.lblTitleWaktu.frame;
    CGRect frmContentTac = _vwContentTac.frame;
    CGRect frmWaktu = cell.lblWaktu.frame;
    CGRect frmSlider = cell.slideWaktu.frame;
    
    frmWaktu.origin.x = frmContentTac.size.width - frmWaktu.size.width - 16;
    cell.lblWaktu.frame = frmWaktu;
    frmSlider.size.width = widthContentTac - 30;
    
    if(IPHONE_5){
        [cell.lblTitleWaktu setFont:[UIFont fontWithName:const_font_name1 size:10.0f]];
        [cell.lblWaktu setFont:[UIFont fontWithName:const_font_name1 size:8.0f]];
        
    }
    
    cell.lblTitleWaktu.frame = frmLblTitleWaktu;
    cell.lblWaktu.frame = frmWaktu;
    cell.slideWaktu.frame = frmSlider;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66.0;
}

- (IBAction)dismisListener:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSDictionary* userInfo = @{@"actionTimer": @"RESET_TIMER"};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"isViewTimer" object:self userInfo:userInfo];
}

@end
