    //
//  ActivationViewController.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "ActivationViewController.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import <MessageUI/MessageUI.h>
#import "UIViewController+ECSlidingViewController.h"
#import "DesclaimerViewController.h"
#import "CreatePINViewController.h"
#import "AESCrypt.h"
#import "InboxHelper.h"
#import "DESChiper.h"
#import "CustomBtn.h"
#import "Styles.h"

@interface ActivationViewController ()<UITextFieldDelegate, ConnectionDelegate, UIAlertViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate ,CreatePINDelegate, UITextViewDelegate, DesclaimerDelegate> {
    float sizeTV;
    float constTV;
    BOOL isResize, iccidEnabled;
    NSString *paramICCID, *paramIMEI;
    DESChiper *desChipper3;
//    NSString *lang;
    NSUserDefaults *userDefault;
    NSString *fromSliding;
    NSString *receipentsNumber;
}

//@property (weak, nonatomic) IBOutlet UITextField *textFieldRegistration;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *textFieldKeyCode;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSelanjutnya;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatals;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblActivation;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@property (weak, nonatomic) IBOutlet UIView *bgView;

- (IBAction)sendRegistration:(id)sender;
- (IBAction)batal:(id)sender;
- (NSString *) genAC:(NSString *) msisdn;

@end

bool chaked = false;

@implementation ActivationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    receipentsNumber = @"";
    isResize = YES;
    chaked = false;
    userDefault = [NSUserDefaults standardUserDefaults];
    fromSliding = [userDefault objectForKey:@"fromSliding"];
    
    iccidEnabled = [[userDefault objectForKey:@"config.act.iccid"]boolValue];
    
//    _topSpace.constant = 0;
    [Styles setTopConstant:_topSpace];
    if([fromSliding isEqualToString:@"YES"]){
        self.bgView.hidden = YES;
    }else{
        self.bgView.hidden = NO;
    }

    
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldPhoneNumber.frame.size.height);
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldPhoneNumber.leftView = paddingPhone;
    self.textFieldPhoneNumber.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldPhoneNumber.keyboardType = UIKeyboardTypeNumberPad;
    [Styles setStyleTextFieldBottomLine:_textFieldPhoneNumber];
    
    UIView *paddingKey = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldKeyCode.leftView = paddingKey;
    self.textFieldKeyCode.leftViewMode = UITextFieldViewModeAlways;
    self.textFieldKeyCode.keyboardType = UIKeyboardTypeNumberPad;
    [Styles setStyleTextFieldBottomLine:_textFieldKeyCode];
    
    [self.btnSelanjutnya setColorSet:PRIMARYCOLORSET];
    [self.btnBatals setColorSet:SECONDARYCOLORSET];

//    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
#pragma mark chance batals in left, agree in right
    _lblTitle.text = lang(@"ACTIVATION");
    _lblPhoneNumber.text = lang(@"PHONE_NUMBER");
    _lblActivation.text = lang(@"ACTIVATION_CODE");
    [_btnBatals setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [_btnSelanjutnya setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
        
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.textFieldPhoneNumber.inputAccessoryView = keyboardDoneButtonView;
    self.textFieldKeyCode.inputAccessoryView = keyboardDoneButtonView;
    
    [self addVC];
    
}

- (void)addVC{
    DesclaimerViewController *shoDesclaimer = [self.storyboard instantiateViewControllerWithIdentifier:@"DSCLMRVC"];
    [shoDesclaimer setDelegate:self];
    shoDesclaimer.view.frame = self.view.bounds;
    [self.view addSubview:shoDesclaimer.view];
    [self addChildViewController:shoDesclaimer];
    [shoDesclaimer didMoveToParentViewController:self];
}

- (void)onDesclaimerCancel{
    [self toExit];
}

- (void)onDesclaimerAgree{
//    do nothing
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField== self.textFieldPhoneNumber){
        [self.textFieldKeyCode becomeFirstResponder];
        return false;
    }
    //do registration
    [self.textFieldKeyCode resignFirstResponder];
    return true;
}

#pragma mark send registrasi change batals
- (IBAction)sendRegistration:(id)sender {
//    [self toBatal];
    [self toRegistrasi];
}

-(void) toRegistrasi{
    NSString *message = @"";
    if([self.textFieldPhoneNumber.text isEqualToString:@""]){
        message = lang(@"NOT_EMPTY");
    } else if(![self.textFieldKeyCode.text isEqualToString:@""]){
        NSString *trimmedString = [self.textFieldPhoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedString length]) {
            message = lang(@"STR_NOT_NUM");
        }
    }else if([self.textFieldKeyCode.text isEqualToString:@""]){
        message = lang(@"NOT_EMPTY");
    }
    
    if([message isEqualToString:@""]){
        NSString *encrypt = [AESCrypt encrypt:self.textFieldKeyCode.text];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:encrypt forKey:@"activation_code"];
        [userDefault synchronize];
        
        
        NSString *unique_id = [self getUUID];
        paramIMEI = [unique_id substringToIndex:16]; //imei
        paramICCID = [unique_id substringFromIndex:16]; //iccid
        
        
        //Set IMEI
//        [userDefault removeObjectForKey:@"imei"];
//        [userDefault setObject:paramIMEI forKey:@"imei"];
//        //Set ICCID
//        [userDefault removeObjectForKey:@"iccid"];
//        [userDefault setObject:paramICCID forKey:@"iccid"];
        //Set MSISDN
//        [userDefault removeObjectForKey:@"mobilenumber"];
//        [userDefault setObject:self.textFieldPhoneNumber.text forKey:@"mobilenumber"];
        
//        [userDefault removeObjectForKey:@"msisdn"];
//        [userDefault setObject:self.textFieldPhoneNumber.text forKey:@"msisdn"];
        [NSUserdefaultsAes removeValueForKey:@"imei"];
        [NSUserdefaultsAes setObject:paramIMEI forKey:@"imei"];
        
        [NSUserdefaultsAes removeValueForKey:@"iccid"];
        [NSUserdefaultsAes setObject:paramICCID forKey:@"iccid"];
        
        [NSUserdefaultsAes removeValueForKey:@"msisdn"];
        [NSUserdefaultsAes setObject:self.textFieldPhoneNumber.text forKey:@"msisdn"];
        
        //generate public key
        BDError *error = [[BDError alloc] init];
        BDRSACryptor *RSACryptor = [[BDRSACryptor alloc] init];
        
        BDRSACryptorKeyPair *RSAKeyPair = [RSACryptor generateKeyPairWithKeyIdentifier:BSM_SERVER_KEY
                                                                                 error:error];
        
//        [userDefault setObject:RSAKeyPair.privateKey forKey:@"privateKey"];
        [NSUserdefaultsAes setObject:RSAKeyPair.privateKey forKey:@"privatekey"];
        
        [userDefault synchronize];
        
        if (iccidEnabled) {
            
            NSString *parmUrl = [NSString stringWithFormat:@"request_type=activation,msisdn=%@,activation_code=%@,otp=%@,public_key=%@,imei=%@,iccid=%@,language",self.textFieldPhoneNumber.text,[self genAC:self.textFieldPhoneNumber.text],self.textFieldKeyCode.text,RSAKeyPair.publicKey,paramIMEI,paramICCID];
            
            NSLog(@"%@", parmUrl);
            
//            [userDefault setObject:RSAKeyPair.privateKey forKey:@"privateKey"];
//            [userDefault synchronize];
            
            [userDefault removeObjectForKey:@"paramURLPost"];
            [userDefault setObject:parmUrl forKey:@"paramURLPost"];
            
            //new checkin
            if([userDefault objectForKey:@"config_source_activation"] != nil){
                if([[userDefault valueForKey:@"config_source_activation"] isEqualToString:@"1"]){
                    
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    NSString *url = [NSString stringWithFormat:@"request_type=check_smspool_roundrobin2,nohp=%@,language",self.textFieldPhoneNumber.text];
                    [conn sendPostParmUrl:url needLoading:true encrypted:false banking:false];
                }else{
                    receipentsNumber = @"3339";
                    [self verifyActivation];
                }
            }else{
                receipentsNumber = @"3339";
                [self verifyActivation];
            }
            
        }else{
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            NSString *parmUrl = [NSString stringWithFormat:@"request_type=activation,msisdn=%@,activation_code=%@,otp=%@,public_key=%@,language",self.textFieldPhoneNumber.text,[self genAC:self.textFieldPhoneNumber.text],self.textFieldKeyCode.text,RSAKeyPair.publicKey];
            [conn sendPostParmUrl:parmUrl needLoading:true encrypted:true banking:false];
        }
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"ACTIVATION") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        } else {
            // Fallback on earlier versions
        }
        [alert show];
    }
}

- (NSString*)getUUID {
    NSString *str = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    str = [str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    return str;
}

- (void)verifyActivation{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:lang(@"INFO")
                          message:lang(@"SMS_VERIFY")
                          delegate:self
                          cancelButtonTitle:lang(@"CANCEL")
                          otherButtonTitles:lang(@"NEXT"), nil];
    alert.delegate = self;
    alert.tag = 404;
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    } else {
        // Fallback on earlier versions
    }
    [alert show];
}

#pragma mark - MFMessageValidation
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        case MessageComposeResultFailed:{

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"SMS_NOT_SUPPORT") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler: nil]];
            [self presentViewController:alert animated:YES completion:nil];
            break;
        }
        case MessageComposeResultSent: {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[userDefault objectForKey:@"paramURLPost"] needLoading:true encrypted:true banking:false];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
        }
        default:
            [self dismissViewControllerAnimated:YES completion:nil];
            break;
    }
    
}

-(void) toBatal{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;

    }
}

#pragma mark change batal to registration
- (IBAction)batal:(id)sender {
    [self toBatal];
//    [self toRegistrasi];
}


NSDictionary * jsonResponse;
#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(NSDictionary *)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"check_smspool_roundrobin2"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            
            NSDictionary *dict  = [NSJSONSerialization
            JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding]
            options:NSJSONReadingAllowFragments
                                   error:nil];
        
            receipentsNumber =[dict valueForKey:@"MSISDN"];
            [self verifyActivation];
        }else{
            [self verifyActivation];
        }
    }
    
    if([requestType isEqual:@"activation"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
//            [[FIRMessaging messaging] unsubscribeFromTopic:@"news"];
            
            jsonResponse = jsonObject;
//            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//            [userDefault setObject:[jsonObject valueForKey:@"costumer_id"] forKey:@"customer_id"];
//            [userDefault setObject:[jsonObject valueForKey:@"email"] forKey:@"email"];
//            [userDefault setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
//            [userDefault setObject:[jsonObject valueForKey:@"session_id"] forKey:@"session_id"];
//            [userDefault setObject:[jsonObject valueForKey:@"public_key"] forKey:@"publickey"];
            
//            [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"customer_id"] forKey:@"customer_id"];
//            [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"email"] forKey:@"email"];
            [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
            [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"session_id"] forKey:@"session_id"];
            [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"public_key"] forKey:@"publickey"];
            
            [userDefault setValue:nil forKey:OBJ_FINANCE];
            [userDefault setValue:nil forKey:OBJ_SPESIAL];
            [userDefault setValue:nil forKey:OBJ_ALL_ACCT];
            [userDefault setValue:nil forKey:OBJ_ROLE_FINANCE];
            [userDefault setValue:nil forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
                        
            
            NSString * isReactivation = [userDefault objectForKey:KEY_HAS_SET_PIN];
            
             if (isReactivation != nil && [isReactivation isEqualToString:@"0"]){
                 // havent create pin
//                 [userDefault setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
                 [userDefault synchronize];
                 
                 CreatePINViewController *createPINVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePINVC"];
                 createPINVC.delegate = self;
                 [self presentViewController:createPINVC animated:YES completion:nil];
                 
             }
             else {
                 // have create pin
                 
                 [userDefault setObject:@"YES" forKey:@"mustLogin"];
                 [userDefault setObject:@"NO" forKey:@"hasLogin"];
//                 [userDefault setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
                 [userDefault setObject:[jsonResponse valueForKey:@"name"] forKey:@"customer_name"];
                 [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"customer_id"] forKey:@"customer_id"];
                 [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"email"] forKey:@"email"];
                 
                 [userDefault synchronize];
                 
                 
                 UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:[NSString stringWithFormat:@"%@: %@\nEmail: %@\nCustomer id:%@",lang(@"NAME"), [jsonResponse valueForKey:@"name"],[jsonResponse valueForKey:@"email"],[jsonResponse valueForKey:@"customer_id"]] preferredStyle:UIAlertControllerStyleAlert];
                 if (@available(iOS 13.0, *)) {
                     [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                 }
                 [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [self onSuccessActivation];
                 }]];
                 [self presentViewController:alerts animated:YES completion:nil];
             }
            
        }else{
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            if (@available(iOS 13.0, *)) {
                [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToHome];
            }]];
            [self presentViewController:alerts animated:YES completion:nil];
        }
    }
}

-(NSString *)genAC:(NSString *)key{
    unsigned int length = (int)[key length];
    NSData * array = [key dataUsingEncoding:NSUTF8StringEncoding];
    const char *dbytes = [array bytes];
    int h = 0;
    for (int i = 0; i < length; ++i) {
        NSLog(@"Value: %d", dbytes[length - (i+1)] ^ 0b00110011);
        h = 31 * h + (dbytes[length - (i+1)] ^ 0b00110011);
    }
    NSString *generated = [NSString stringWithFormat:@"%d",h];
    NSLog(@"%@", generated);
    return generated;
}

- (void)errorLoadData:(NSError *)error{
    UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"ACTIVATION") message:[NSString stringWithFormat:@"%@\n",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){[self backToHome];}]];
    [self presentViewController:alerts animated:YES completion:nil];
}

- (void)reloadApp{
    [BSMDelegate reloadApp];
   //  [self backToR];
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    
}

- (void)onSuccessActivation{
    InboxHelper *inboxHelper = [[InboxHelper alloc] init];
    [inboxHelper createTbl];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
    NSDictionary *backUpInbox = [Utility dictForKeychainKey:strCID];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (backUpInbox) {
            InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
            [inboxHeldper insertDataInboxKeychain:backUpInbox];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [[NSNotificationCenter defaultCenter]
            postNotificationName:@"changeIcon"
            object:self];});
    });
    
    NSString *configIndicator = [userDefault objectForKey:@"config_indicator"];
    NSInteger mutasiMaxDays = [[userDefault valueForKey:@"config_mutation_range"]integerValue];
    NSInteger sessionTimeOut = [[userDefault valueForKey:@"config_timeout_session"]integerValue];
    if(configIndicator == nil || mutasiMaxDays == 0 || sessionTimeOut == 0){
        [self getConfigApp];
        NSString *configIndicator = [userDefault objectForKey:@"config_indicator"];
        bool stateIndicator = [configIndicator boolValue];
        if (stateIndicator) {
            //        [self isNetworkConnected];
            [userDefault setObject:@"0" forKey:@"state_indicator"];
            [userDefault synchronize];
            
        }
    }
    
//    if([fromSliding isEqualToString:@"YES"]){
//        self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
//        [self.slidingViewController resetTopViewAnimated:YES];
//    }else{
//        [self dismissViewControllerAnimated:YES completion:^(){
//            [self->delegate didActivationSuccess];
//        }];
//    }
    [self backToHome];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 56){
        if (buttonIndex == 1) {
            [self toRegistrasi];
            NSString *msgToEncrypt = [[paramICCID stringByAppendingString:@";"] stringByAppendingString:paramIMEI];
            NSString *kode = @"";
            kode = [self genAC:self.textFieldPhoneNumber.text];
            NSLog(@"%@", kode);
            
            NSString *smsContent = @"";
            desChipper3 = [[DESChiper alloc]init];
            smsContent = [desChipper3 doDES:msgToEncrypt key:kode];
            
            NSData *dataSMS = [smsContent dataUsingEncoding:NSUTF8StringEncoding];
            NSString *base64Encoded = [dataSMS base64EncodedStringWithOptions:0];
            NSString *txtSMS = [@"bsmsmsact " stringByAppendingString:base64Encoded];
            [self showSMS:txtSMS];
            NSLog(@"Ok, send the sms to UID : %@",self.textFieldPhoneNumber.text);
        }
        
    }
    else if (alertView.tag == 404) {
        if (buttonIndex == 1) {
            NSString *msgToEncrypt = [[paramICCID stringByAppendingString:@";"] stringByAppendingString:paramIMEI];
            NSString *kode = @"";
            kode = [self genAC:self.textFieldPhoneNumber.text];
            NSLog(@"%@", kode);
            
            NSString *smsContent = @"";
            desChipper3 = [[DESChiper alloc]init];
            smsContent = [desChipper3 doDES:msgToEncrypt key:kode];
            
            NSData *dataSMS = [smsContent dataUsingEncoding:NSUTF8StringEncoding];
            NSString *base64Encoded = [dataSMS base64EncodedStringWithOptions:0];
            NSString *txtSMS = [@"bsmsmsact " stringByAppendingString:base64Encoded];
            [self showSMS:txtSMS];
            NSLog(@"Ok, send the sms to UID : %@",self.textFieldPhoneNumber.text);
        }
        
    }
    else {
        [self backToHome];
    }
}

- (void) backToHome{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
    }
}

- (void)showSMS:(NSString*)data {
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"SMS_NOT_SUPPORT") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler: nil]];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    NSMutableArray *recipents = [[NSMutableArray alloc]init];
    if([receipentsNumber isEqualToString:@""]){
        NSArray *smsLongNumberArrays = [[userDefault valueForKey:@"config_sms_longnumber"] componentsSeparatedByString:@"|"];
        NSUInteger myCount = [smsLongNumberArrays count];
        NSMutableArray *recipentsPrefix = [[NSMutableArray alloc]init];

        BOOL hasPrefix = false;
        for(NSString* numbers in smsLongNumberArrays){
            if([numbers hasPrefix:[_textFieldPhoneNumber.text substringToIndex:4]]) {
                [recipentsPrefix addObject:numbers];
                hasPrefix = true;
            }
        }
        
        if(hasPrefix){
            [recipents addObject:[recipentsPrefix objectAtIndex:arc4random_uniform((int)recipentsPrefix.count)]];
        }else{
            [recipents addObject:[smsLongNumberArrays objectAtIndex:arc4random_uniform((int)myCount)]];
        }
    }else{
        [recipents addObject:receipentsNumber];
    }

    
//    NSArray *recipents = @[@"3339"];
    NSString *message = [NSString stringWithFormat:@"%@", data];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}

-(void) onSuccessCreatePIN{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"YES" forKey:@"mustLogin"];
    [userDefault setObject:@"NO" forKey:@"hasLogin"];
    //[userDefault setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    [userDefault setObject:[jsonResponse valueForKey:@"name"] forKey:@"customer_name"];
    [NSUserdefaultsAes setObject:[jsonResponse valueForKey:@"customer_id"] forKey:@"customer_id"];
    [NSUserdefaultsAes setObject:[jsonResponse valueForKey:@"email"] forKey:@"email"];
    [userDefault synchronize];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"SUCCESS") message:[NSString stringWithFormat:@"%@\n%@: %@\nEmail: %@\nCustomer ID: %@", lang(@"ACTIVATION_SUCCESS"), lang(@"ACTIVATION"), [jsonResponse valueForKey:@"name"],[jsonResponse valueForKey:@"email"],[jsonResponse valueForKey:@"customer_id"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 55;
    
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    } else {
        // Fallback on earlier versions
    }
    
    [alert show];
}

-(void) onCancelCreatePIN{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    [userDefault setObject:nil forKey:@"customer_id"];
//    [userDefault setObject:nil forKey:@"email"];
//    [userDefault setObject:nil forKey:@"zpk"];
//    [userDefault setObject:nil forKey:@"session_id"];
//    [userDefault setObject:nil forKey:@"publickey"];
    [NSUserdefaultsAes setObject:nil forKey:@"customer_id"];
    [NSUserdefaultsAes setObject:nil forKey:@"email"];
    [NSUserdefaultsAes setObject:nil forKey:@"zpk"];
    [NSUserdefaultsAes setObject:nil forKey:@"session_id"];
    [NSUserdefaultsAes setObject:nil forKey:@"publickey"];
    
    [userDefault synchronize];
    // [self backToR];
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
}


- (IBAction)actionMainBatal:(id)sender {
    // [self backToR];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
    }
}

-(void) toExit{
    // [self backToR];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *openRoot  = [userDefault valueForKey:@"openRoot"];
    if ([openRoot isEqualToString:@"@"]) {
        [userDefault removeObjectForKey:@"openRoot"];
        [userDefault setValue:@"-" forKey:@"last"];
        [[DataManager sharedManager] resetObjectData];
        [self.navigationController popToRootViewControllerAnimated:YES];
        if(self.tabBarController.selectedIndex != 0){
            [self.tabBarController setSelectedIndex:0];
        }
    } else {
        MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
        self.slidingViewController.topViewController = menuVC.homeNavigationController;
        
    }
}


#pragma mark actionAgree change to exit
//- (IBAction)actionAgree:(id)sender {
//    [self toExit];
//}

-(void) getConfigApp{
    
    NSMutableURLRequest *request = [Utility BSMHeader:[NSString stringWithFormat:@"%@config.html",API_URL]];
    [request setHTTPMethod:@"GET"];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *menuAcceptRecentlyDef = @"00013,00017,00018,00025,00027,00028,00044,00049,00065,00066";
    @try {
        NSError *error;
        NSURLResponse *urlResponse;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if(error == nil){
            NSDictionary *mJsonObject = [NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&error];
            if(mJsonObject){
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.onboarding"] forKey:@"config_onboarding"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.indicator"] forKey:@"config_indicator"];
//                [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
//                [userDefault setObject:[mJsonObject valueForKey:@"config.timeout.session"] forKey:@"config_timeout_session"];
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.enable.banner2"] forKey:@"config_banner2_enable"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.link"] forKey:@"config_banner2_link"];
                [userDefault setObject:[mJsonObject valueForKey:@"config.banner2.url"] forKey:@"config_banner2_url"];
                
                if ([[mJsonObject valueForKey:@"config.mutation.range"] integerValue] != 0) {
                     [userDefault setObject:[mJsonObject valueForKey:@"config.mutation.range"] forKey:@"config_mutation_range"];
                }else{
                    [userDefault setValue:@"15" forKey:@"config_mutation_range"];
                }
                
                if ([[mJsonObject valueForKey:@"config.timeout.session"] integerValue] != 0) {
                    [userDefault setObject:[mJsonObject valueForKey:@"config.timeout.session"] forKey:@"config_timeout_session"];
                }else{
                    [userDefault setValue:@"180" forKey:@"config_timeout_session"];
                }
                
                [userDefault setObject:[mJsonObject valueForKey:@"config.act.iccid"] forKey:@"config.act.iccid"];
                
                if ([[mJsonObject valueForKey:@"config.allow.recently"] isEqualToString:@""] || [mJsonObject valueForKey:@"config.allow.recently"] == nil) {
                    [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
                }else{
                    [userDefault setObject:[mJsonObject valueForKey:@"config.allow.recently"] forKey:@"config.allow.recently"];
                }
                
                if ([mJsonObject objectForKey:@"config.enable.financing"] != 0 || [mJsonObject objectForKey:@"config.enable.financing"] != nil){
                    [userDefault setObject:[mJsonObject valueForKey:@"config.enable.financing"] forKey:@"config.enable.financing"];
                }
                
            }
        }else{
            NSLog(@"%@", error);
            [userDefault setObject:@(15) forKey:@"config_mutation_range"];
            [userDefault setObject:@(180) forKey:@"config_timeout_session"];
            [userDefault setObject:@(1) forKey:@"config.act.iccid"];
            [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
			[userDefault setObject:@(0) forKey:@"config_onboarding"];
			[userDefault setObject:@(0) forKey:@"config.enable.financing"];
        }
        
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
        [userDefault setObject:@(15) forKey:@"config_mutation_range"];
        [userDefault setObject:@(180) forKey:@"config_timeout_session"];
        [userDefault setObject:@(1) forKey:@"config.act.iccid"];
        [userDefault setObject:menuAcceptRecentlyDef forKey:@"config.allow.recently"];
		[userDefault setObject:@(0) forKey:@"config_onboarding"];
		[userDefault setObject:@(0) forKey:@"config.enable.financing"];
    }
    [userDefault synchronize];
}

@end
