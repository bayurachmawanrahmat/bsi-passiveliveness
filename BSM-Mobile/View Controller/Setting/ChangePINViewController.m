//
//  ChangePINViewController.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "ChangePINViewController.h"
#import "Connection.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "CustomBtn.h"
#import "Styles.h"

@interface ChangePINViewController ()<ConnectionDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLayout;
@property (weak, nonatomic) IBOutlet UITextField *textFieldOldPin;
@property (weak, nonatomic) IBOutlet UITextField *textFieldNewPin;
@property (weak, nonatomic) IBOutlet UITextField *textFieldRepeatPin;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)cancel:(id)sender;
- (IBAction)next:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPinlama;
@property (weak, nonatomic) IBOutlet UILabel *lblPinBaru;
@property (weak, nonatomic) IBOutlet UILabel *confirmPin;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@end

@implementation ChangePINViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change batal to selanjutnya and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
     NSString *msgToolbar;
    
    [Styles setTopConstant:self.topConstraint];
    
    if([lang isEqualToString:@"id"]){
        _lblTitle.text = @"Ubah Pin";
        _lblPinlama.text = @"PIN Lama";
        _lblPinBaru.text = @"PIN Baru";
        _confirmPin.text = @"Konfirmasi PIN Baru";
        
//        [_btnB setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnB setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnN setTitle:@"Batal" forState:UIControlStateNormal];
        msgToolbar = @"Selesai";
        
    } else {
        _lblTitle.text = @"Change Pin";
        _lblPinlama.text = @"Old PIN";
        _lblPinBaru.text = @"New PIN";
        _confirmPin.text = @"Confirmation New PIN";
        
//        [_btnB setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Next" forState:UIControlStateNormal];
        [_btnB setTitle:@"Next" forState:UIControlStateNormal];
        [_btnN setTitle:@"Cancel" forState:UIControlStateNormal];
        msgToolbar = @"Done";
    }
    
    [_lblPinBaru setFont:[UIFont fontWithName:const_font_name3 size:14]];
    [_lblPinlama setFont:[UIFont fontWithName:const_font_name3 size:14]];
    [_confirmPin setFont:[UIFont fontWithName:const_font_name3 size:14]];
    
    [_textFieldNewPin setFont:[UIFont fontWithName:const_font_name1 size:14]];
    [_textFieldOldPin setFont:[UIFont fontWithName:const_font_name1 size:14]];
    [_textFieldRepeatPin setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.btnB setColorSet:PRIMARYCOLORSET];
    [self.btnN setColorSet:SECONDARYCOLORSET];
    

    // [self.view removeGestureRecognizer:self.slidingViewController.panGesture];
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.textFieldNewPin.inputAccessoryView = keyboardDoneButtonView;
    self.textFieldOldPin.inputAccessoryView = keyboardDoneButtonView;
    self.textFieldRepeatPin.inputAccessoryView = keyboardDoneButtonView;
    
//    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldNewPin.frame.size.height);
//
//    UIView *padding1 = [[UIView alloc] initWithFrame:framePadding];
//    self.textFieldNewPin.leftView = padding1;
//    self.textFieldNewPin.leftViewMode = UITextFieldViewModeAlways;
//
//    UIView *padding2 = [[UIView alloc] initWithFrame:framePadding];
//    self.textFieldOldPin.leftView = padding2;
//    self.textFieldOldPin.leftViewMode = UITextFieldViewModeAlways;
//
//    UIView *padding3 = [[UIView alloc] initWithFrame:framePadding];
//    self.textFieldRepeatPin.leftView = padding3;
//    self.textFieldRepeatPin.leftViewMode = UITextFieldViewModeAlways;
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];;
    
    // Do any additional setup after loading the view.
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)keyboardDidShow:(NSNotification *)sender {
    
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
//    self.bottomLayout.constant = height - 100;
     self.bottomLayout.constant = 200;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillHide:(NSNotification *)sender {
    self.bottomLayout.constant = 10;
    [self.view layoutIfNeeded];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark change Cancel to goToNext
- (IBAction)cancel:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    //    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    //    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    //        [self backToR];
    //    MenuViewController *menuVC = (MenuViewController *)self.revealViewController.rearViewController;
    //    [self.revealViewController pushFrontViewController:menuVC.homeNavigationController animated:YES];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"-" forKey:@"last"];
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

-(void)goToNext{
    if([self.textFieldOldPin.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if([self.textFieldNewPin.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if([self.textFieldRepeatPin.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else if(![self.textFieldNewPin.text isEqualToString:self.textFieldRepeatPin.text]){
        //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"PIN_NOT_SAME") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:lang(@"PIN_NOT_SAME") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldRepeatPin = [self.textFieldRepeatPin.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        NSString *trimmedStringtextFieldNewPin = [self.textFieldNewPin.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        NSString *trimmedStringtextFieldOldPin = [self.textFieldOldPin.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedStringtextFieldRepeatPin length] || [trimmedStringtextFieldNewPin length]  || [trimmedStringtextFieldOldPin length]) {
            message = lang(@"STR_NOT_NUM");
        }
        
        if([message isEqualToString:@""]){
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            
            [[DataManager sharedManager]setPinNumber:self.textFieldOldPin.text];
            NSDate *date = [NSDate date];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"yyyyMMdHHmmss"];
            NSString *currentDate = [dateFormat stringFromDate:date];
            NSString *parmUrl = [NSString stringWithFormat:@"request_type=change_pin,new_pin=%@,pin=%@,date_local=%@,language=%@",self.textFieldNewPin.text, self.textFieldOldPin.text,currentDate,lang];
            NSLog(@"%@", parmUrl);
            [conn sendPostParmUrl:parmUrl needLoading:true encrypted:true banking:true];
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

#pragma mark change Next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}


//- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    if (textField == self.textFieldRepeatPin) {
//        [self.scrollView setContentOffset:CGPointMake(0.0, IPHONE_5?45.0:125.0) animated:true];
//    }else if(textField == self.textFieldNewPin){
//        [self.scrollView setContentOffset:CGPointMake(0.0, IPHONE_5?15.0:45.0) animated:true];
//    }else{
//        [self.scrollView setContentOffset:CGPointMake(0.0, 0.0) animated:true];
//    }
//}
#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(NSDictionary *)jsonObject withRequestType:(NSString *)requestType{
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        
        //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"SUCCESS") message:[jsonObject valueForKey:@"response"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        if (![requestType isEqualToString:@"check_notif"]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Change Pin" message:[jsonObject valueForKey:@"response"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
//    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    
//    MenuViewController *menuVC = (MenuViewController *)self.revealViewController.rearViewController;
//    [self.revealViewController pushFrontViewController:menuVC.homeNavigationController animated:YES];
//        [self backToR];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"-" forKey:@"last"];
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

- (void)reloadApp{
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
//    MenuViewController *menuVC = (MenuViewController *)self.revealViewController.rearViewController;
//    [self.revealViewController pushFrontViewController:menuVC.homeNavigationController animated:YES];
    [BSMDelegate reloadApp];
//        [self backToR];
}

@end
