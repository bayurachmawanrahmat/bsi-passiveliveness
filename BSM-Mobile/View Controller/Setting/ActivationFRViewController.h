//
//  ActivationViewFRViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 09/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "RootViewController.h"
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ActivationFRViewController : RootViewController

@end

NS_ASSUME_NONNULL_END
