//
//  ChangeLanguageViewController.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "ChangeLanguageViewController.h"
#import "CustomBtn.h"
#import "Styles.h"
#import "SKeychain.h"

@interface ChangeLanguageViewController ()<UIAlertViewDelegate> {
    UIAlertView *alert;
}
@property (weak, nonatomic) IBOutlet CustomBtn *buttonIna;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonEn;
- (IBAction)changeToIna:(id)sender;
- (IBAction)changeToEn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDis;
@property (weak, nonatomic) IBOutlet UIButton *btnClear;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
- (IBAction)clearData:(id)sender;

@end

@implementation ChangeLanguageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [self.btnClear setHidden:YES];
    [Styles setTopConstant:self.topConstraint];
    
//    NSArray *listLanguage = [defaults objectForKey:@"AppleLanguages"];
//    [self setButtonTo:[listLanguage objectAtIndex:0]];
    // Do any additional setup after loading the view.
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        _lblTitle.text = @"Ubah Bahasa";
        _lblDis.text = @"Silahkan Pilih Bahasa yang diinginkan";
    } else {
        _lblTitle.text = @"Change Language";
        _lblDis.text = @"Please Select a Language";
    }
    
    [_lblDis setFont:[UIFont fontWithName:const_font_name3 size:15]];
    
    self.lblDis.textColor = UIColorFromRGB(const_color_primary);
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    [self.lblTitle superview].backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.buttonEn setColorSet:PRIMARYCOLORSET];
    [self.buttonIna setColorSet:PRIMARYCOLORSET];
    
//    [self.buttonEn setImage:[self.buttonEn.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
//    [self.buttonEn.imageView setTintColor:UIColorFromRGB(amanah_gold_color)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setButtonTo:(NSString *)language{
    if([language isEqualToString:@"id"]){
        language = @"ina";
    }

    
//    [self.buttonEn setBackgroundImage:[UIImage imageNamed:@"bg-language-selected"] forState:UIControlStateNormal];
//    [self.buttonIna setBackgroundImage:[UIImage imageNamed:@"bg-language-selected"] forState:UIControlStateNormal];

}

- (IBAction)clearData:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    [userDefault removeObjectForKey:@"ICCIDEnabled"];
//    [userDefault removeObjectForKey:@"msisdn"];
//    [userDefault removeObjectForKey:@"imei"];
//    [userDefault removeObjectForKey:@"iccid"];
//    [userDefault removeObjectForKey:@"password"];
    
    [NSUserdefaultsAes removeValueForKey:@"msisdn"];
    [NSUserdefaultsAes removeValueForKey:@"imei"];
    [NSUserdefaultsAes removeValueForKey:@"iccid"];
    [NSUserdefaultsAes removeValueForKey:@"password"];
//    [SKeychain deleteObjectForKey:@"password"];
    
    [userDefault removeObjectForKey:@"mustLogin"];
    [userDefault removeObjectForKey:@"hasLogin"];
    
    alert = [[UIAlertView alloc]initWithTitle:@"CLEAR DATA" message:@"Data MSISDN, Flag ICCID, IMEI, ICCID, Password telah dihapus" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)changeToIna:(id)sender {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
//    [defaults synchronize];
//    [self setButtonTo:@"ina"];

    [self restart:@"Please restart application to apply the language." lang:@"id"];
}

- (IBAction)changeToEn:(id)sender {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
//    [defaults synchronize];
//    [self setButtonTo:@"en"];
    [self restart:@"Silahkan restart aplikasi." lang:@"en"];
}

- (void)restart:(NSString *)message lang:(NSString*)language{
    alert = [[UIAlertView alloc]initWithTitle:lang(@"CONFIRM") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate = self;
    if([language isEqualToString:@"id"]){
        alert.tag = 1313;
    }else{
        alert.tag = 1314;
    }
    [alert show];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1313) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
        [defaults synchronize];
       exit(0);
    }else if (alertView.tag == 1314){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
        [defaults synchronize];
       exit(0);
    }
}

@end
