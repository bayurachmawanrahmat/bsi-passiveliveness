//
//  DesclaimerViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 20/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "DesclaimerViewController.h"
#import "MenuViewController.h"
#import "Utility.h"
#import "Styles.h"

@interface DesclaimerViewController ()<UITextViewDelegate>{
    BOOL checkStatus;
    NSString* html;
}

@end

@implementation DesclaimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _viewContent.layer.cornerRadius = 16;
    _viewContent.layer.masksToBounds = YES;
    [Styles addShadow:self.view];
    
    if([Utility isLanguageID]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *urlString = [NSString stringWithFormat:@"%@disclaimer.html?language=id&vv=%s", API_URL,VERSION_VALUE];
            NSLog(@"url desclaimer : %@",urlString);
            NSURL *url = [NSURL URLWithString:urlString];
            NSError *err = nil;
            self->html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
            if(!err){
                self->_textviewContent.text = [NSString stringWithFormat:@"%@\n\n\n",self->html];
            }else{
                [Utility showMessage:lang(@"ERROR") instance:self];
            }
        });
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *urlString = [NSString stringWithFormat:@"%@disclaimer.html?language=en&vv=%s", API_URL,VERSION_VALUE];
            NSLog(@"url desclaimer : %@",urlString);
            NSURL *url = [NSURL URLWithString:urlString];
            NSError *err = nil;
            self->html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&err];
            if(!err){
                self->_textviewContent.text = [NSString stringWithFormat:@"%@\n\n\n",self->html];
            }else{
                [Utility showMessage:lang(@"ERROR") instance:self];
            }
        });
    }
    [_btnNext setTitle:lang(@"AGREE") forState:UIControlStateNormal];
    [_btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    _labelTitle.text = lang(@"TERMS_CONDITION");
    _labelCheck.text = lang(@"TERMS_CONDITION_AGREEMENT");
    
    _textviewContent.delegate = self;
    [_textviewContent setEditable:NO];
    checkStatus = false;
    [_btnCheck setImage:[UIImage imageNamed:@"blank_check"]];
    
    UITapGestureRecognizer *tapClose = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionDecline)];
    UITapGestureRecognizer *tapCheck = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCheck)];
    
    [_btnCheck setUserInteractionEnabled:YES];
    [_btnCheck addGestureRecognizer:tapCheck];
    
    [_btnClose setUserInteractionEnabled:YES];
    [_btnClose addGestureRecognizer:tapClose];
    
    [_btnCancel addTarget:self action:@selector(actionDecline) forControlEvents:UIControlEventTouchUpInside];
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    
    [_btnNext addTarget:self action:@selector(actionAgree) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext setColorSet:PRIMARYCOLORSET];
    
    [_btnCancel setHidden:YES];
    [_btnNext setHidden:YES];
    _heightViewButton.constant = 0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.y >= scrollView.contentSize.height - scrollView.frame.size.height){
//        _heightViewButton.constant = 64;
//        _btnNext.hidden = NO;
//        _btnCancel.hidden = NO;
    }else{
        _heightViewButton.constant = 0;
        _btnNext.hidden = YES;
        _btnCancel.hidden = YES;
        checkStatus = false;
        [_btnCheck setImage:[UIImage imageNamed:@"blank_check"]];
    }
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void) actionDecline{
    [delegate onDesclaimerCancel];
    [self removeView];
}

- (void) actionAgree{
    [delegate onDesclaimerAgree];
    [self removeView];
}

- (void) actionCheck{
    if(checkStatus){
        checkStatus = false;
        [_btnCheck setImage:[UIImage imageNamed:@"blank_check"]];
        _heightViewButton.constant = 0;
        _btnNext.hidden = YES;
        _btnCancel.hidden = YES;
    }else{
        checkStatus = true;
        [_btnCheck setImage:[UIImage imageNamed:@"checked"]];
        _heightViewButton.constant = 64;
        _btnNext.hidden = NO;
        _btnCancel.hidden = NO;
    }
}

- (void) removeView{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
