//
//  ActivationFRResultViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 09/06/21.
//  Copyright © 2021 lds. All rights reserved.
//
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivationFRResultViewController : UIViewController{
    id delgate;
}

- (void) setDelegate: (id)nDelegate;

@end

@protocol ActivationFRResultDelegate

@required

- (void) onCancelTapped;
- (void) onNextTapped;

@end

NS_ASSUME_NONNULL_END
