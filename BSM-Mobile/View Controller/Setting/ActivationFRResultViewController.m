//
//  ActivationFRResultViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 09/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "ActivationFRResultViewController.h"
#import "Styles.h"

@interface ActivationFRResultViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topconstraint;

@end

@implementation ActivationFRResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.viewContainer.layer.cornerRadius = 16;
    self.buttonNext.layer.cornerRadius = 20;
    self.buttonNext.layer.masksToBounds = true;
    self.buttonNext.titleLabel.font = [UIFont fontWithName:const_font_name3 size:14.0];
    
    self.labelTitle.text = lang(@"ACTIVATION");
    self.labelDescription.text = lang(@"SMS_VERIFY");

    [self.buttonNext setTitle:lang(@"CONTINUE") forState:UIControlStateNormal];
    [self.buttonCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark button actions
- (void) actionNext{
    [self dismissViewControllerAnimated:YES completion:^(){
        [self->delgate onNextTapped];
    }];
}

- (void) actionCancel{
    [self dismissViewControllerAnimated:YES completion:^(){
        [self->delgate onCancelTapped];
    }];
}

#pragma mark delegation setup
- (void)setDelegate:(id)nDelegate{
    delgate = nDelegate;
}

@end
