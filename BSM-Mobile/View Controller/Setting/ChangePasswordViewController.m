//
//  ChangePasswordViewController.m
//  BSM-Mobile
//
//  Created by BSM on 1/17/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Connection.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "NSString+MD5.h"
#import "Utility.h"
#import "SKeychain.h"
#import "NSUserdefaultsAes.h"

@interface ChangePasswordViewController ()<ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIScrollViewDelegate> {
    UIView *vwBgConfirm;
    UIView *vwContinerConfirm;
    UIView *vwLineTitleConfirm;
    UIView *vwLineTextConfirm;
    UILabel *lblTitleConfirm;
    UILabel *lblDescConfirm;
    UILabel *lblTextConfirm;
    UITextField *txtPwdConfirm;
    UIButton *btnEyeConfirm;
    UIButton *btnOKConfirm;
    UIButton *btnCancelConfirm;
    UIScrollView *vwMScroll;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
    NSString *flagOn;
    NSString *flagTPChangePwd1;
    NSString *flagTPChangePwd2;
    NSString *flagTPChangePwd3;
    NSString *flagTogglePwd;
    NSString *flagSetting;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;

@property (weak, nonatomic) IBOutlet UIView *vwContentScrol;

@property (weak, nonatomic) IBOutlet UIView *vwActifkan;
@property (weak, nonatomic) IBOutlet UIView *vwSandiLama;
@property (weak, nonatomic) IBOutlet UIView *vwSandiBaru;
@property (weak, nonatomic) IBOutlet UIView *vwSandiKonfirmasi;



@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAktifkan;
@property (weak, nonatomic) IBOutlet UILabel *lblOldPass;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPass;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPassConfirm;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwdConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UISwitch *switchActivate;
@property (weak, nonatomic) IBOutlet UIButton *btnEye1;
@property (weak, nonatomic) IBOutlet UIButton *btnEye2;
@property (weak, nonatomic) IBOutlet UIButton *btnEye3;
@property (weak, nonatomic) IBOutlet UIView *vwLine1;
@property (weak, nonatomic) IBOutlet UIView *vwLine2;
@property (weak, nonatomic) IBOutlet UIView *vwLine3;

- (IBAction)next:(id)sender;
- (IBAction)batal:(id)sender;
- (IBAction)togglePwd1:(id)sender;
- (IBAction)togglePwd2:(id)sender;
- (IBAction)togglePwd3:(id)sender;

@end

@implementation ChangePasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"id"]){
            valTitle = @"Konfirmasi";
            valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            valCancel = @"Batal";
            valAgree = @"Lanjut";
        } else {
            valTitle = @"Confirmation";
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            valCancel = @"Cancel";
            valAgree = @"Next";
        }
        
        //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:valTitle
                              message:valMsg
                              delegate:self
                              cancelButtonTitle:valCancel
                              otherButtonTitles:valAgree, nil];
        
        alert.tag = 229;
        [alert show];
    }
}


#pragma mark change cancel to selanjutnya and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _vwScroll.delegate = self;
    _txtOldPwd.delegate = self;
    _txtNewPwd.delegate = self;
    _txtNewPwdConfirm.delegate = self;
    
    flagTPChangePwd1 = @"HIDE";
    flagTPChangePwd2 = @"HIDE";
    flagTPChangePwd3 = @"HIDE";

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgToolbar;
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Pengaturan Kata Sandi";
        self.lblAktifkan.text = @"Aktifkan";        
        self.lblOldPass.text = @"Kata Sandi Lama";
        self.lblNewPass.text = @"Kata Sandi Baru";
        self.lblNewPassConfirm.text = @"Konfirmasi Kata Sandi Baru";
//        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
//        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnNext setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        valAgree = @"Lanjut";
        valCancel = @"Batal";
        valTitle = @"Konfirmasi";
        valMsg = @"Kata Sandi akan digunakan, apakah anda ingin melanjutkan ?";
         msgToolbar = @"Selesai";
    } else {
        self.lblTitle.text = @"Setting Password";
        self.lblAktifkan.text = @"Activate";
        self.lblOldPass.text = @"Old Password";
        self.lblNewPass.text = @"New Password";
        self.lblNewPassConfirm.text = @"New Password Confirmation";
//        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
//        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnNext setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Next" forState:UIControlStateNormal];
        valAgree = @"Next";
        valCancel = @"Cancel";
        valTitle = @"Confirmation";
        valMsg = @"Password will be used, do you want to continue";
        msgToolbar = @"Done";
    }
    
    [self setupLayout];
    
    
    [self.switchActivate addTarget:self action:@selector(switchTwisted:) forControlEvents:UIControlEventValueChanged];
    NSString *isLogin = [userDefault objectForKey:@"mustLogin"];
    
    if (([[isLogin uppercaseString] isEqualToString:@"NO"]) || (isLogin == nil)) {
        [self.switchActivate setSelected:NO];
        [self.switchActivate setOn:NO];
        
        [self.txtOldPwd setHidden:YES];
        [self.txtNewPwd setHidden:YES];
        [self.txtNewPwdConfirm setHidden:YES];
        [self.lblOldPass setHidden:YES];
        [self.lblNewPass setHidden:YES];
        [self.lblNewPassConfirm setHidden:YES];
        [self.btnNext setHidden:YES];
        [self.btnCancel setHidden:YES];
        [self.btnEye1 setHidden:YES];
        [self.btnEye2 setHidden:YES];
        [self.btnEye3 setHidden:YES];
        [self.vwLine1 setHidden:YES];
        [self.vwLine2 setHidden:YES];
        [self.vwLine3 setHidden:YES];
        flagOn = @"NO";
        
        //frmButtonNext.origin.y = frmLabelAktifkan.origin.y + 53;
        //frmButtonCancel.origin.y = frmLabelAktifkan.origin.y + 53;
    } else if ([[isLogin uppercaseString] isEqualToString:@"YES"]) {
        [self.switchActivate setSelected:YES];
        [self.switchActivate setOn:YES];

        //frmButtonNext.origin.y = 478;
        //frmButtonCancel.origin.y = 478;
        
        [self.txtOldPwd setHidden:NO];
        [self.txtNewPwd setHidden:NO];
        [self.txtNewPwdConfirm setHidden:NO];
        [self.lblOldPass setHidden:NO];
        [self.lblNewPass setHidden:NO];
        [self.lblNewPassConfirm setHidden:NO];
        [self.btnNext setHidden:NO];
        [self.btnCancel setHidden:NO];
        [self.btnEye1 setHidden:NO];
        [self.btnEye2 setHidden:NO];
        [self.btnEye3 setHidden:NO];
        [self.vwLine1 setHidden:NO];
        [self.vwLine2 setHidden:NO];
        [self.vwLine3 setHidden:NO];
        flagOn = @"YES";
    }
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.txtOldPwd.inputAccessoryView = keyboardDoneButtonView;
    self.txtNewPwd.inputAccessoryView = keyboardDoneButtonView;
    self.txtNewPwdConfirm.inputAccessoryView = keyboardDoneButtonView;
    
    [self.txtOldPwd setSecureTextEntry:true];
    [self.txtNewPwd setSecureTextEntry:true];
    [self.txtNewPwdConfirm setSecureTextEntry:true];
}

-(void) setupLayout{
    //vwParent
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwContent = self.vwContentScrol.frame;
    
    //vwChildOne
    CGRect frmVwActif = self.vwActifkan.frame;
    CGRect frmVwSandiLama = self.vwSandiLama.frame;
    CGRect frmVwSandiBaru = self.vwSandiBaru.frame;
    CGRect frmVwSandiConfirm = self.vwSandiKonfirmasi.frame;
    CGRect frmBtnNext = self.btnNext.frame;
    CGRect frmBtnCancel = self.btnCancel.frame;
    
    //vwChildTwo
    CGRect frmSwitch = self.switchActivate.frame;
    CGRect frmLabelAktifkan = self.lblAktifkan.frame;
    CGRect frmLabelOldPass = self.lblOldPass.frame;
    CGRect frmLabelNewPass = self.lblNewPass.frame;
    CGRect frmLabelNewPassConfirm = self.lblNewPassConfirm.frame;
    CGRect frmTextOldPass = self.txtOldPwd.frame;
    CGRect frmTextNewPass = self.txtNewPwd.frame;
    CGRect frmTextNewPassConfirm = self.txtNewPwdConfirm.frame;
    CGRect frmButtonNext = self.btnNext.frame;
    CGRect frmEye1 = self.btnEye1.frame;
    CGRect frmEye2 = self.btnEye2.frame;
    CGRect frmEye3 = self.btnEye3.frame;
    CGRect frmLine1 = self.vwLine1.frame;
    CGRect frmLine2 = self.vwLine2.frame;
    CGRect frmLine3 = self.vwLine3.frame;
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.origin.x = 0;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = SCREEN_HEIGHT - (frmVwTitle.origin.y + frmVwTitle.size.height) - BOTTOM_NAV_GEN;
    
    frmVwContent.origin.x = 0;
    frmVwContent.origin.y = 0;
    frmVwContent.size.width = frmVwScroll.size.width;
    
    frmVwActif.origin.x = 0;
    frmVwActif.origin.y = 0;
    frmVwActif.size.width = frmVwContent.size.width;
    
    frmVwSandiLama.origin.x = 0;
    frmVwSandiLama.origin.y = frmVwActif.origin.y + frmVwActif.size.height + 4;
    frmVwSandiLama.size.width = frmVwActif.size.width;
    
    frmVwSandiBaru.origin.x = 0;
    frmVwSandiBaru.origin.y = frmVwSandiLama.origin.y + frmVwSandiLama.size.height + 8;
    frmVwSandiBaru.size.width = frmVwActif.size.width;
    
    frmVwSandiConfirm.origin.x = 0;
    frmVwSandiConfirm.origin.y = frmVwSandiBaru.origin.y + frmVwSandiBaru.size.height + 8;
    frmVwSandiConfirm.size.width = frmVwActif.size.width;
    
    frmBtnNext.origin.x = 16;
    frmBtnNext.origin.y = frmVwSandiConfirm.origin.y + frmVwSandiConfirm.size.height + 16;
    frmBtnNext.size.width = (frmVwContent.size.width/2) - 16;
    
    frmBtnCancel.origin.x = frmBtnNext.origin.x + frmBtnNext.size.width + 8;
    frmBtnCancel.origin.y = frmBtnNext.origin.y;
    frmBtnCancel.size.width = frmBtnNext.size.width - 8;
    
    frmVwContent.size.height = frmButtonNext.origin.y + frmButtonNext.size.height + 16;
    
    frmLabelAktifkan.origin.x = 16;
    
    frmSwitch.origin.x = frmVwActif.size.width - frmSwitch.size.width - 16;
    
    frmLabelOldPass.size.width = frmVwSandiLama.size.width - (frmLabelOldPass.origin.x * 2);
    frmLine1.size.width  = frmVwSandiLama.size.width - (frmLine1.origin.x * 2);
    frmEye1.origin.x = frmVwSandiLama.size.width - frmEye1.size.width - 16;
    frmTextOldPass.origin.x = frmLine1.origin.x;
    frmTextOldPass.size.width = frmEye1.origin.x - 10;
    
    frmLabelNewPass.size.width = frmLabelOldPass.size.width;
    frmLabelNewPass.origin.y = 4;
    frmLine2.size.width = frmLine1.size.width;
    frmEye2.origin.x  = frmEye1.origin.x;
    frmTextNewPass.origin.x = frmTextOldPass.origin.x;
    frmTextNewPass.size.width = frmTextOldPass.size.width;
    
    frmLabelNewPassConfirm.size.width = frmLabelOldPass.size.width;
    frmLabelNewPassConfirm.origin.y = 4;
    frmLine3.size.width = frmLine1.size.width;
    frmEye3.origin.x = frmEye1.origin.x;
    frmTextNewPassConfirm.origin.x = frmTextOldPass.origin.x;
    frmTextNewPassConfirm.size.width = frmTextOldPass.size.width;
    
//    if (IPHONE_X || IPHONE_XS_MAX) {
    if ([Utility isDeviceHaveNotch]) {
        frmVwScroll.size.height = SCREEN_HEIGHT - TOP_NAV - BOTTOM_NAV_X;
    }
    
    //vwParent
    self.vwTitle.frame = frmVwTitle;
    self.vwScroll.frame = frmVwScroll;
    self.vwContentScrol.frame = frmVwContent;
    
    //vwChildOne
    self.vwActifkan.frame = frmVwActif;
    self.vwSandiLama.frame = frmVwSandiLama;
    self.vwSandiBaru.frame = frmVwSandiBaru;
    self.vwSandiKonfirmasi.frame = frmVwSandiConfirm;
    self.btnNext.frame = frmBtnNext;
    self.btnCancel.frame = frmBtnCancel;
    
    //vwChildTwo
    self.switchActivate.frame = frmSwitch;
    self.lblAktifkan.frame = frmLabelAktifkan;
    self.lblOldPass.frame = frmLabelOldPass;
    self.lblNewPass.frame = frmLabelNewPass;
    self.lblNewPassConfirm.frame = frmLabelNewPassConfirm;
    self.txtOldPwd.frame = frmTextOldPass;
    self.txtNewPwd.frame = frmTextNewPass;
    self.txtNewPwdConfirm.frame = frmTextNewPassConfirm;
    self.btnNext.frame = frmButtonNext;
    self.btnEye1.frame = frmEye1;
    self.btnEye2.frame = frmEye2;
    self.btnEye3.frame = frmEye3;
    self.vwLine1.frame = frmLine1;
    self.vwLine2.frame = frmLine2;
    self.vwLine3.frame = frmLine3;
    [self.vwScroll setContentSize:CGSizeMake(frmVwContent.size.width, frmVwContent.size.height)];
    
    
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
     [self.vwScroll setContentSize:CGSizeMake(self.vwContentScrol.frame.size.width, self.vwContentScrol.frame.size.height + kbSize.height + 20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwScroll.frame.size.width, self.vwScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

//Handle action
- (void)switchTwisted:(UISwitch *)twistedSwitch
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    CGRect frmButtonNext = self.btnNext.frame;
    CGRect frmButtonCancel = self.btnCancel.frame;
    //CGRect frmLabelAktifkan = self.lblAktifkan.frame;
    
    if ([twistedSwitch isOn] && (![twistedSwitch isSelected]))
    {
        [twistedSwitch setSelected:YES];
        
        //Write code for SwitchON Action
        //frmButtonNext.origin.y = 478;
        //frmButtonCancel.origin.y = 478;
        
        [self.txtOldPwd setHidden:NO];
        [self.txtNewPwd setHidden:NO];
        [self.txtNewPwdConfirm setHidden:NO];
        [self.lblOldPass setHidden:NO];
        [self.lblNewPass setHidden:NO];
        [self.lblNewPassConfirm setHidden:NO];
        [self.btnNext setHidden:NO];
        [self.btnCancel setHidden:NO];
        [self.btnEye1 setHidden:NO];
        [self.btnEye2 setHidden:NO];
        [self.btnEye3 setHidden:NO];
        [self.vwLine1 setHidden:NO];
        [self.vwLine2 setHidden:NO];
        [self.vwLine3 setHidden:NO];
        
        if([lang isEqualToString:@"id"]){
            //valMsg = @"Kata Sandi akan digunakan, apakah anda ingin melanjutkan ?";
            valMsg = @"Kata sandi akan diaktifkan.\nDengan mengaktifkan kata sandi Anda diwajibkan membuat kata sandi baru, ingin melanjutkan?";
        } else {
            //valMsg = @"Password will be used, do you want to continue ?";
            valMsg = @"The password will be activated.\nBy activating your password you are required to create a new password, do you want to continue?";
        }
        flagOn = @"YES";
    }
    else if ((![twistedSwitch isOn]) && [twistedSwitch isSelected])
    {
        [twistedSwitch setSelected:NO];
        
        //Write code for SwitchOFF Action
        [self.txtOldPwd setHidden:YES];
        [self.txtNewPwd setHidden:YES];
        [self.txtNewPwdConfirm setHidden:YES];
        [self.lblOldPass setHidden:YES];
        [self.lblNewPass setHidden:YES];
        [self.lblNewPassConfirm setHidden:YES];
        [self.btnNext setHidden:YES];
        [self.btnCancel setHidden:YES];
        [self.btnEye1 setHidden:YES];
        [self.btnEye2 setHidden:YES];
        [self.btnEye3 setHidden:YES];
        [self.vwLine1 setHidden:YES];
        [self.vwLine2 setHidden:YES];
        [self.vwLine3 setHidden:YES];
        
        //frmButtonNext.origin.y = frmLabelAktifkan.origin.y + 53;
        //frmButtonCancel.origin.y = frmLabelAktifkan.origin.y + 53;
        
        if([lang isEqualToString:@"id"]){
            //valMsg = @"Kata Sandi tidak akan digunakan, apakah anda ingin melanjutkan ?";
            valMsg = @"Kata sandi akan dinonaktifkan, anda ingin melanjutkan?";
        } else {
            //valMsg = @"Password will not be used, do you want to continue ?";
            valMsg = @"Password will be disabled, do you want to continue?";
        }
        flagOn = @"NO";
    }
    
    //self.lblAktifkan.frame = frmLabelAktifkan;
    self.btnNext.frame = frmButtonNext;
    self.btnCancel.frame = frmButtonCancel;
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:valTitle
                          message:valMsg
                          delegate:nil
                          cancelButtonTitle:valCancel
                          otherButtonTitles:valAgree, nil];
    alert.delegate = self;
    alert.tag = 424;
    [alert show];
}

- (IBAction)togglePwd1:(id)sender {
    [Utility animeBounchCb:sender];
    if ([flagTPChangePwd1 isEqualToString:@"SHOW"]) {
        flagTPChangePwd1 = @"HIDE";
        [self.txtOldPwd setSecureTextEntry:true];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPChangePwd1 isEqualToString:@"HIDE"]) {
        flagTPChangePwd1 = @"SHOW";
        [self.txtOldPwd setSecureTextEntry:false];
        [self.btnEye1 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)togglePwd2:(id)sender {
    [Utility animeBounchCb:sender];
    if ([flagTPChangePwd2 isEqualToString:@"SHOW"]) {
        flagTPChangePwd2 = @"HIDE";
        [self.txtNewPwd setSecureTextEntry:true];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPChangePwd2 isEqualToString:@"HIDE"]) {
        flagTPChangePwd2 = @"SHOW";
        [self.txtNewPwd setSecureTextEntry:false];
        [self.btnEye2 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)togglePwd3:(id)sender {
    [Utility animeBounchCb:sender];
    if ([flagTPChangePwd3 isEqualToString:@"SHOW"]) {
        flagTPChangePwd3 = @"HIDE";
        [self.txtNewPwdConfirm setSecureTextEntry:true];
        [self.btnEye3 setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTPChangePwd3 isEqualToString:@"HIDE"]) {
        flagTPChangePwd3 = @"SHOW";
        [self.txtNewPwdConfirm setSecureTextEntry:false];
        [self.btnEye3 setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (void)actionToggle {
    if ([flagTogglePwd isEqualToString:@"SHOW"]) {
        flagTogglePwd = @"HIDE";
        [txtPwdConfirm setSecureTextEntry:true];
        [btnEyeConfirm setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTogglePwd isEqualToString:@"HIDE"]) {
        flagTogglePwd = @"SHOW";
        [txtPwdConfirm setSecureTextEntry:false];
        [btnEyeConfirm setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

#pragma mark change batal to goToNext
- (IBAction)batal:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"-" forKey:@"last"];
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    NSString *mustLogin = [NSString stringWithFormat:@"%@",(self.switchActivate.isOn ? @"YES":@"NO")];
    
    if ([mustLogin isEqualToString:@"YES"]) {
        if([self.txtOldPwd.text isEqualToString:@""]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else if([self.txtNewPwd.text isEqualToString:@""]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else if([self.txtNewPwdConfirm.text isEqualToString:@""]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else if(![self.txtNewPwd.text isEqualToString:self.txtNewPwdConfirm.text]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:lang(@"PWD_NOT_SAME") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
            NSString *message = @"";
            NSString *trimmedStringtextFieldRepeatPwd = [self.txtNewPwdConfirm.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
            NSString *trimmedStringtextFieldNewPwd = [self.txtNewPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
            NSString *trimmedStringtextFieldOldPwd = [self.txtOldPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
            if (([trimmedStringtextFieldRepeatPwd length] == 0) || ([trimmedStringtextFieldNewPwd length] == 0) || ([trimmedStringtextFieldOldPwd length] == 0)) {
                message = lang(@"PWD_NOT_ALPHANUM");
            }
            
            NSString *trimmedDectextFieldRepeatPwd = [self.txtNewPwdConfirm.text stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
            NSString *trimmedDectextFieldNewPwd = [self.txtNewPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
            NSString *trimmedDectextFieldOldPwd = [self.txtOldPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
            if (([trimmedDectextFieldRepeatPwd length] == 0) || ([trimmedDectextFieldNewPwd length] == 0) || ([trimmedDectextFieldOldPwd length] == 0)) {
                message = lang(@"PWD_NOT_ALPHANUM");
            }
            
            if (([self.txtOldPwd.text length] < 6) || ([self.txtOldPwd.text length] < 6) || ([self.txtOldPwd.text length] < 6) || ([self.txtOldPwd.text length] > 8) || ([self.txtOldPwd.text length] > 8) || ([self.txtOldPwd.text length] > 8)) {
                message = lang(@"PWD_MUST_6_DIGIT");
            }
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//            NSString *upwd = [userDefault objectForKey:@"password"];
            NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
            NSString *smsg = @"";
            if (!([[self.txtOldPwd.text MD5] isEqualToString:upwd])) {
                if([lang isEqualToString:@"id"]){
                    message = @"Kata Sandi Salah";
                } else {
                    message = @"Wrong Password";
                }
            }
            
            if([message isEqualToString:@""]){
                NSString *pwd = self.txtNewPwd.text;
                
//                [userDefault setObject:[pwd MD5] forKey:@"password"];
                [NSUserdefaultsAes setObject:[pwd MD5] forKey:@"password"];
                [userDefault synchronize];
                
                //[userDefault removeObjectForKey:@"mustLogin"];
                //[userDefault setObject:[mustLogin uppercaseString] forKey:@"mustLogin"];

                if([lang isEqualToString:@"id"]){
                    smsg = @"Kata sandi berhasil diubah\nSilahkan login kembali.";
                } else {
                    smsg = @"Change password success\nPlease login again.";
                }
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:smsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alert.tag = 752;
                [alert show];
            } else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    } else {
        //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        //[userDefault removeObjectForKey:@"mustLogin"];
        //[userDefault setObject:mustLogin forKey:@"mustLogin"];
        
        //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"LOGIN DIMATIKAN" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //[alert show];
    }
}

- (void)showConfirm {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat widthContiner =  width - 40;
    CGFloat heightContiner = 250;
    flagTogglePwd = @"HIDE";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *word1 = @"";
    NSString *word2 = @"";
    NSString *word3 = @"";
    NSString *word4 = @"";
    NSString *word5 = @"";
    NSString *msgToolbar;
    if([lang isEqualToString:@"id"]){
        word1 = @"Pengaturan Kata Sandi";
        word2 = @"Untuk melanjutkannya silahkan masukkan kata sandi Anda.";
        word3 = @"Kata Sandi";
        word4 = @"BATAL";
        word5 = @"LANJUT";
        msgToolbar = @"Selesai";
    } else {
        word1 = @"Setting Password";
        word2 = @"To continue, please enter your password.";
        word3 = @"Password";
        word4 = @"CANCEL";
        word5 = @"NEXT";
        msgToolbar = @"Done";
    }
    
    vwBgConfirm = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    vwBgConfirm.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    vwMScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(20, (height/2) - (heightContiner/2), widthContiner, heightContiner)];
    
    vwContinerConfirm = [[UIView alloc]initWithFrame:CGRectMake(0, 0, vwMScroll.frame.size.width, vwMScroll.frame.size.height)];
    vwContinerConfirm.backgroundColor = [UIColor whiteColor];
    
    [vwMScroll addSubview:vwContinerConfirm];
    
    lblTitleConfirm = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, widthContiner - 20, 20)];
    [lblTitleConfirm setFont:[UIFont fontWithName:const_font_name3 size:19]];
    [lblTitleConfirm setTextColor:[UIColor blackColor]];
    [lblTitleConfirm setTextAlignment:NSTextAlignmentCenter];
    [lblTitleConfirm setText:word1];
    
    vwLineTitleConfirm = [[UIView alloc]initWithFrame:CGRectMake(0, 40, widthContiner, 1)];
    vwLineTitleConfirm.backgroundColor = [UIColor grayColor];
    
    lblDescConfirm = [[UILabel alloc] initWithFrame:CGRectMake(15, 50, widthContiner - 30, 40)];
    lblDescConfirm.lineBreakMode = NSLineBreakByWordWrapping;
    lblDescConfirm.numberOfLines = 2;
    [lblDescConfirm setFont:[UIFont fontWithName:const_font_name1 size:15]];
    [lblDescConfirm setTextColor:[UIColor blackColor]];
    [lblDescConfirm setTextAlignment:NSTextAlignmentJustified];
    [lblDescConfirm setText:word2];
    
    lblTextConfirm = [[UILabel alloc] initWithFrame:CGRectMake(15, 120, widthContiner - 30, 20)];
    [lblTextConfirm setFont:[UIFont fontWithName:const_font_name1 size:13]];
    [lblTextConfirm setTextColor:[UIColor colorWithRed:(21.0/255.0) green:(124.0/255.0) blue:(104.0/255.0) alpha:1]];
    [lblTextConfirm setTextAlignment:NSTextAlignmentLeft];
    [lblTextConfirm setText:word3];
    
    txtPwdConfirm = [[UITextField alloc] initWithFrame:CGRectMake(15, 140, widthContiner - 30, 40)];
    [txtPwdConfirm setSecureTextEntry:YES];
    [txtPwdConfirm setFont:[UIFont fontWithName:const_font_name1 size:17]];
    [txtPwdConfirm setTextColor:[UIColor blackColor]];

       UIToolbar* mKeyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
        mKeyboardDoneButtonView.barStyle = UIBarStyleDefault;
        mKeyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                         [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    txtPwdConfirm.inputAccessoryView = mKeyboardDoneButtonView;
    
    vwLineTextConfirm = [[UIView alloc]initWithFrame:CGRectMake(15, 180, widthContiner - 30, 1)];
    vwLineTextConfirm.backgroundColor = [UIColor grayColor];

    btnEyeConfirm = [[UIButton alloc] initWithFrame:CGRectMake(widthContiner - 45, 145, 30, 30)];
    [btnEyeConfirm setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    [btnEyeConfirm addTarget:self action:@selector(actionToggle) forControlEvents:UIControlEventTouchUpInside];
    
    //btnCancelConfirm = [[UIButton alloc] initWithFrame:CGRectMake(10, heightContiner - 60, widthContiner/2 - 20, 50)];
    btnCancelConfirm = [[UIButton alloc] initWithFrame:CGRectMake(widthContiner/2 + 10, heightContiner - 60, widthContiner/2 - 20, 50)];
    btnCancelConfirm.layer.cornerRadius = 20;
    btnCancelConfirm.layer.masksToBounds = true;
    btnCancelConfirm.layer.backgroundColor = [[UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1] CGColor];
    btnCancelConfirm.layer.borderWidth = 2;
    btnCancelConfirm.layer.borderColor = [[UIColor yellowColor] CGColor];
    
#pragma mark change btn cancel to btn next
//    [btnCancelConfirm setTitle:word4 forState:UIControlStateNormal];
//    [btnCancelConfirm addTarget:self action:@selector(actionCancelConfirm) forControlEvents:UIControlEventTouchUpInside];
    [btnCancelConfirm setTitle:word5 forState:UIControlStateNormal];
    [btnCancelConfirm addTarget:self action:@selector(actionOKConfirm) forControlEvents:UIControlEventTouchUpInside];
    
    //btnOKConfirm = [[UIButton alloc] initWithFrame:CGRectMake(widthContiner/2 + 10, heightContiner - 60, widthContiner/2 - 20, 50)];
    btnOKConfirm = [[UIButton alloc] initWithFrame:CGRectMake(10, heightContiner - 60, widthContiner/2 - 20, 50)];
    btnOKConfirm.layer.cornerRadius = 20;
    btnOKConfirm.layer.masksToBounds = true;
    btnOKConfirm.layer.backgroundColor = [[UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1] CGColor];
    btnOKConfirm.layer.borderWidth = 2;
    btnOKConfirm.layer.borderColor = [[UIColor yellowColor] CGColor];
    
#pragma mark change btn next to btn cancel
//    [btnOKConfirm setTitle:word5 forState:UIControlStateNormal];
//    [btnOKConfirm addTarget:self action:@selector(actionOKConfirm) forControlEvents:UIControlEventTouchUpInside];
    [btnOKConfirm setTitle:word4 forState:UIControlStateNormal];
    [btnOKConfirm addTarget:self action:@selector(actionCancelConfirm) forControlEvents:UIControlEventTouchUpInside];
    
    [vwContinerConfirm addSubview:lblTitleConfirm];
    [vwContinerConfirm addSubview:vwLineTitleConfirm];
    [vwContinerConfirm addSubview:lblDescConfirm];
    [vwContinerConfirm addSubview:lblTextConfirm];
    [vwContinerConfirm addSubview:txtPwdConfirm];
    [vwContinerConfirm addSubview:btnEyeConfirm];
    [vwContinerConfirm addSubview:vwLineTextConfirm];
    [vwContinerConfirm addSubview:btnOKConfirm];
    [vwContinerConfirm addSubview:btnCancelConfirm];
    
    [vwMScroll setContentSize:CGSizeMake(widthContiner, vwContinerConfirm.frame.size.height)];
    
    [vwBgConfirm addSubview:vwMScroll];
    [self.view addSubview:vwBgConfirm];
}

- (void)confirmDisabled {
    [self showConfirm];
    flagSetting = @"DISABLE";
}

- (void)confirmEnabled {
    [self showConfirm];
    flagSetting = @"ENABLE";
}

- (void)actionOKConfirm {
    NSString *pwd = txtPwdConfirm.text;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    NSString *upwd = [userDefault objectForKey:@"password"];
    NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
    
    if ([[pwd MD5] isEqualToString:upwd]) {
        if ([flagOn isEqualToString:@"YES"]) {
            //[userDefault setObject:@"YES" forKey:@"mustLogin"];
        } else if ([flagOn isEqualToString:@"NO"]) {
            [userDefault setObject:@"NO" forKey:@"mustLogin"];
        }
        [userDefault synchronize];
        
        if ([flagSetting isEqualToString:@"ENABLE"]) {
            //[userDefault setObject:@"" forKey:@"password"];
            [vwBgConfirm removeFromSuperview];
            
            CGFloat width = [UIScreen mainScreen].bounds.size.width;
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            UIViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NCreatePwdVC"];
            templateView.view.frame = CGRectMake(0, 0, width, height);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else if ([flagSetting isEqualToString:@"DISABLE"]) {
            [vwBgConfirm removeFromSuperview];
            
            NSString *tw;
            NSString *mw;
            if([lang isEqualToString:@"id"]){
                tw = @"Informasi";
                mw = @"Silahkan restart aplikasi";
            } else {
                tw = @"Information";
                mw = @"Please restart application";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:tw message:mw delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 456;
            [alert show];
        }
    } else {
        NSString *err_msg = @"";
        if([lang isEqualToString:@"id"]){
            err_msg = @"Kata Sandi Salah";
        } else {
            err_msg = @"Wrong Password";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:err_msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 123;
        [alert show];
    }
}

- (void)actionCancelConfirm {
    [self resetSetting];
    [vwBgConfirm removeFromSuperview];
}

- (void)resetSetting {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    if ([mustLogin isEqualToString:@"YES"]) {
        flagOn = @"YES";
        [self.switchActivate setSelected:YES];
        [self.switchActivate setOn:YES];
        [self.txtOldPwd setHidden:NO];
        [self.txtNewPwd setHidden:NO];
        [self.txtNewPwdConfirm setHidden:NO];
        [self.lblOldPass setHidden:NO];
        [self.lblNewPass setHidden:NO];
        [self.lblNewPassConfirm setHidden:NO];
        [self.btnNext setHidden:NO];
        [self.btnCancel setHidden:NO];
        [self.btnEye1 setHidden:NO];
        [self.btnEye2 setHidden:NO];
        [self.btnEye3 setHidden:NO];
        [self.vwLine1 setHidden:NO];
        [self.vwLine2 setHidden:NO];
        [self.vwLine3 setHidden:NO];
    } else if ([mustLogin isEqualToString:@"NO"]) {
        flagOn = @"NO";
        [self.switchActivate setSelected:NO];
        [self.switchActivate setOn:NO];
        [self.txtOldPwd setHidden:YES];
        [self.txtNewPwd setHidden:YES];
        [self.txtNewPwdConfirm setHidden:YES];
        [self.lblOldPass setHidden:YES];
        [self.lblNewPass setHidden:YES];
        [self.lblNewPassConfirm setHidden:YES];
        [self.btnNext setHidden:YES];
        [self.btnCancel setHidden:YES];
        [self.btnEye1 setHidden:YES];
        [self.btnEye2 setHidden:YES];
        [self.btnEye3 setHidden:YES];
        [self.vwLine1 setHidden:YES];
        [self.vwLine2 setHidden:YES];
        [self.vwLine3 setHidden:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 424) {
        if (buttonIndex == 0) {
            //Cancel Button
            [self resetSetting];
        }
        else if (buttonIndex == 1) {
            //OK Button
            if ([flagOn isEqualToString:@"YES"]) { //Enable Password
                [self confirmEnabled];
            } else if ([flagOn isEqualToString:@"NO"]) { //Disable Password
                [self confirmDisabled];
            }
        }
    } else if (alertView.tag == 123) {
        //Wrong Password
        [self resetSetting];
        [vwBgConfirm removeFromSuperview];
    } else if (alertView.tag == 456) {
        //Disable Password
        exit(0);
    } else if (alertView.tag == 229) {
        //Check activate or not
        if (buttonIndex == 0) {
            //Do Nothing
        } else if (buttonIndex == 1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"id"]){
                valTitle = @"Konfirmasi";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Batal";
                valAgree = @"Lanjut";
            } else {
                valTitle = @"Confirmation";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Cancel";
                valAgree = @"Next";
            }
            
            //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            
            alert.tag = 230;
            [alert show];
        }
        
        /*NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];*/
    } else if (alertView.tag == 230) {
        //Check register or not
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        } else if (buttonIndex == 1) {
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
        }
    } else if (alertView.tag == 752) {
        //Change Password
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault synchronize];
        
        if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
            CGRect screenBound = [[UIScreen mainScreen] bounds];
            CGSize screenSize = screenBound.size;
            CGFloat screenWidth = screenSize.width;
            CGFloat screenHeight = screenSize.height;
            
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            UIViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
            templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else {
            exit(0);
        }
    }
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(NSDictionary *)jsonObject withRequestType:(NSString *)requestType{
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        if (![requestType isEqualToString:@"check_notif"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Change Password" message:[jsonObject valueForKey:@"response"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    [BSMDelegate reloadApp];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self resetTimer];
    
    return YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self resetTimer];
    
}

-(void) resetTimer{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


@end
