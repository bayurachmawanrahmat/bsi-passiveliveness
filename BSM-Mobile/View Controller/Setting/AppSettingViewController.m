//
//  AppSettingViewController.m
//  BSM-Mobile
//
//  Created by BSM on 2/27/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "AppSettingViewController.h"
#import "Connection.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "NSString+MD5.h"
#import "NSUserdefaultsAes.h"
#import "CustomBtn.h"

@interface AppSettingViewController ()<UIAlertViewDelegate> {
    UIView *vwBgConfirm;
    UIView *vwContinerConfirm;
    UIView *vwLineTitleConfirm;
    UIView *vwLineTextConfirm;
    UILabel *lblTitleConfirm;
    UILabel *lblDescConfirm;
    UILabel *lblTextConfirm;
    UITextField *txtPwdConfirm;
    UIButton *btnEyeConfirm;
    CustomBtn *btnOKConfirm;
    CustomBtn *btnCancelConfirm;
    UIScrollView *vwScroll;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
    NSString *flagOn;
    NSString *flagTogglePwd;
    NSString *flagSetting;
    UIToolbar* keyboardDoneButtonView;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAktifkan;
@property (weak, nonatomic) IBOutlet UILabel *lblSetting;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
@property (weak, nonatomic) IBOutlet UISwitch *switchActivate;
- (IBAction)gotoCPR:(id)sender;

@end

@implementation AppSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 229;
        [alert show];*/
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"en"]){
            valTitle = @"Information";
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            valCancel = @"No";
            valAgree = @"Ok";
        } else {
            valTitle = @"Informasi";
            valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            valCancel = @"Tidak";
            valAgree = @"Ok";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:valTitle
                              message:valMsg
                              delegate:self
                              cancelButtonTitle:valCancel
                              otherButtonTitles:valAgree, nil];
        
        alert.tag = 229;
        [alert show];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgToolbar;
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"Pengaturan Kata Sandi";
        self.lblAktifkan.text = @"Aktifkan";
        self.lblSetting.text = @"Ganti Kata Sandi";
        valAgree = @"Lanjut";
        valCancel = @"Batal";
        valTitle = @"Konfirmasi";
        valMsg = @"Kata Sandi akan digunakan, apakah anda ingin melanjutkan ?";
        msgToolbar = @"Selesai";
    } else {
        self.lblTitle.text = @"Setting Password";
        self.lblAktifkan.text = @"Activate";
        self.lblSetting.text = @"Change Password";
        valAgree = @"Next";
        valCancel = @"Cancel";
        valTitle = @"Confirmation";
        valMsg = @"Password will be used, do you want to continue";
        msgToolbar = @"Done";
    }
    
    [self.imgTitle setHidden:YES];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.switchActivate setOnTintColor:UIColorFromRGB(const_color_primary)];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmSwitch = self.switchActivate.frame;
    CGRect frmLabelAktifkan = self.lblAktifkan.frame;
    
    frmTitle.origin.y = TOP_NAV;
    frmTitle.size.width = screenWidth;
    frmTitle.origin.x = 0;
    frmTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.width = screenWidth - 16;
    frmLblTitle.size.height = frmTitle.size.height;
    
    frmSwitch.origin.x = screenWidth - 16 - frmSwitch.size.width;
    frmLabelAktifkan.size.width = screenWidth - 32;
    
    [self.switchActivate addTarget:self action:@selector(switchTwisted:) forControlEvents:UIControlEventValueChanged];
    NSString *isLogin = [userDefault objectForKey:@"mustLogin"];
    
    if (([[isLogin uppercaseString] isEqualToString:@"NO"]) || (isLogin == nil)) {
        [self.switchActivate setSelected:NO];
        [self.switchActivate setOn:NO];
        flagOn = @"NO";
        [self.lblSetting setHidden:YES];
        [self.btnSetting setHidden:YES];
    } else if ([[isLogin uppercaseString] isEqualToString:@"YES"]) {
        [self.switchActivate setSelected:YES];
        [self.switchActivate setOn:YES];
        flagOn = @"YES";
        [self.lblSetting setHidden:NO];
        [self.btnSetting setHidden:NO];
    }
    
    self.vwTitle.frame = frmTitle;
    self.lblTitle.frame = frmLblTitle;
    self.switchActivate.frame = frmSwitch;
    self.lblAktifkan.frame = frmLabelAktifkan;
    
    [_lblAktifkan setFont:[UIFont fontWithName:const_font_name3 size:14]];
    [_lblSetting setFont:[UIFont fontWithName:const_font_name3 size:14]];

    keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    [self registerForKeyboardNotifications];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

//Handle action
- (void)switchTwisted:(UISwitch *)twistedSwitch
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if ([twistedSwitch isOn] && (![twistedSwitch isSelected]))
    {
        [twistedSwitch setSelected:YES];
        
        if([lang isEqualToString:@"id"]){
            valMsg = @"Kata sandi akan diaktifkan.\nDengan mengaktifkan kata sandi Anda diwajibkan membuat kata sandi baru, ingin melanjutkan?";
        } else {
            valMsg = @"The password will be activated.\nBy activating your password you are required to create a new password, do you want to continue?";
        }
        flagOn = @"YES";
    }
    else if ((![twistedSwitch isOn]) && [twistedSwitch isSelected])
    {
        [twistedSwitch setSelected:NO];
        
        //Write code for SwitchOFF Action
        
        if([lang isEqualToString:@"id"]){
            //valMsg = @"Kata Sandi tidak akan digunakan, apakah anda ingin melanjutkan ?";
            valMsg = @"Kata sandi akan dinonaktifkan, anda ingin melanjutkan?";
        } else {
            //valMsg = @"Password will not be used, do you want to continue ?";
            valMsg = @"Password will be disabled, do you want to continue?";
        }
        flagOn = @"NO";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:valTitle
                          message:valMsg
                          delegate:nil
                          cancelButtonTitle:valCancel
                          otherButtonTitles:valAgree, nil];
    alert.delegate = self;
    alert.tag = 424;
    [alert show];
}

- (IBAction)gotoCPR:(id)sender {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    UIViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"CPRVC"];
    templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}

- (void)actionToggle {
    if ([flagTogglePwd isEqualToString:@"SHOW"]) {
        flagTogglePwd = @"HIDE";
        [txtPwdConfirm setSecureTextEntry:true];
        [btnEyeConfirm setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    } else if ([flagTogglePwd isEqualToString:@"HIDE"]) {
        flagTogglePwd = @"SHOW";
        [txtPwdConfirm setSecureTextEntry:false];
        [btnEyeConfirm setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (void)showConfirm {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat widthContiner =  width - 40;
    CGFloat heightContiner = 250;
    flagTogglePwd = @"HIDE";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *word1 = @"";
    NSString *word2 = @"";
    NSString *word3 = @"";
    NSString *word4 = @"";
    NSString *word5 = @"";
    
    if([lang isEqualToString:@"id"]){
        word1 = @"Pengaturan Kata Sandi";
        word2 = @"Untuk melanjutkannya silahkan masukkan kata sandi Anda.";
        word3 = @"Kata Sandi";
        word4 = @"BATAL";
        word5 = @"LANJUT";
        
    } else {
        word1 = @"Setting Password";
        word2 = @"To continue, please enter your password.";
        word3 = @"Password";
        word4 = @"CANCEL";
        word5 = @"NEXT";
        
    }
    
    vwBgConfirm = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    vwBgConfirm.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    vwScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _vwTitle.frame.origin.y + _vwTitle.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT - (_vwTitle.frame.origin.y + _vwTitle.frame.size.height))];
    
    
//    vwContinerConfirm = [[UIView alloc]initWithFrame:CGRectMake(20, (height/2) - (heightContiner/2), widthContiner, heightContiner)];
    vwContinerConfirm = [[UIView alloc]initWithFrame:CGRectMake(20, (vwScroll.frame.size.height/2) - (heightContiner/2) - 40, vwScroll.frame.size.width - 40, heightContiner)];
    vwContinerConfirm.backgroundColor = [UIColor whiteColor];
    
    [vwScroll addSubview:vwContinerConfirm];
    
    lblTitleConfirm = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, widthContiner - 20, 20)];
    [lblTitleConfirm setFont:[UIFont fontWithName:const_font_name3 size:19]];
    [lblTitleConfirm setTextColor:[UIColor blackColor]];
    [lblTitleConfirm setTextAlignment:NSTextAlignmentCenter];
    [lblTitleConfirm setText:word1];
    
    vwLineTitleConfirm = [[UIView alloc]initWithFrame:CGRectMake(0, 40, widthContiner, 1)];
    vwLineTitleConfirm.backgroundColor = [UIColor grayColor];
    
    lblDescConfirm = [[UILabel alloc] initWithFrame:CGRectMake(15, 50, widthContiner - 30, 40)];
    lblDescConfirm.lineBreakMode = NSLineBreakByWordWrapping;
    lblDescConfirm.numberOfLines = 2;
    [lblDescConfirm setFont:[UIFont fontWithName:const_font_name1 size:15]];
    [lblDescConfirm setTextColor:[UIColor blackColor]];
    [lblDescConfirm setTextAlignment:NSTextAlignmentJustified];
    [lblDescConfirm setText:word2];
    
    lblTextConfirm = [[UILabel alloc] initWithFrame:CGRectMake(15, 120, widthContiner - 30, 20)];
    [lblTextConfirm setFont:[UIFont fontWithName:const_font_name1 size:13]];
    [lblTextConfirm setTextColor:[UIColor blackColor]];
    [lblTextConfirm setTextAlignment:NSTextAlignmentLeft];
    [lblTextConfirm setText:word3];
    
    txtPwdConfirm = [[UITextField alloc] initWithFrame:CGRectMake(15, 140, widthContiner - 30, 40)];
    [txtPwdConfirm setSecureTextEntry:YES];
    [txtPwdConfirm setFont:[UIFont fontWithName:const_font_name1 size:17]];
    [txtPwdConfirm setTextColor:[UIColor blackColor]];
    
//    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
//    [keyboardDoneButtonView sizeToFit];
//    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
//                                                                   style:UIBarButtonItemStyleBordered target:self
//                                                                  action:@selector(doneClicked:)];
//    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
//    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
//    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
//    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
//                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    
    txtPwdConfirm.inputAccessoryView = keyboardDoneButtonView;
    
    vwLineTextConfirm = [[UIView alloc]initWithFrame:CGRectMake(15, 180, widthContiner - 30, 1)];
    vwLineTextConfirm.backgroundColor = [UIColor grayColor];
    
    btnEyeConfirm = [[UIButton alloc] initWithFrame:CGRectMake(widthContiner - 45, 145, 30, 30)];
    [btnEyeConfirm setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    [btnEyeConfirm addTarget:self action:@selector(actionToggle) forControlEvents:UIControlEventTouchUpInside];
    
    //btnCancelConfirm = [[UIButton alloc] initWithFrame:CGRectMake(10, heightContiner - 60, widthContiner/2 - 20, 50)];
    btnCancelConfirm = [[CustomBtn alloc] initWithFrame:CGRectMake(widthContiner/2 + 10, heightContiner - 60, widthContiner/2 - 20, 40)];
    
#pragma mark change button cancel to button next
//    [btnCancelConfirm setTitle:word4 forState:UIControlStateNormal];
//    [btnCancelConfirm addTarget:self action:@selector(actionCancelConfirm) forControlEvents:UIControlEventTouchUpInside];
    [btnCancelConfirm setTitle:word5 forState:UIControlStateNormal];
    [btnCancelConfirm addTarget:self action:@selector(actionOKConfirm) forControlEvents:UIControlEventTouchUpInside];
    [btnCancelConfirm setColorSet:PRIMARYCOLORSET];
    
    //btnOKConfirm = [[UIButton alloc] initWithFrame:CGRectMake(widthContiner/2 + 10, heightContiner - 60, widthContiner/2 - 20, 50)];
    btnOKConfirm = [[CustomBtn alloc] initWithFrame:CGRectMake(10, heightContiner - 60, widthContiner/2 - 20, 40)];
    [btnOKConfirm setColorSet:SECONDARYCOLORSET];
#pragma mark change button next to button cancel
//    [btnOKConfirm setTitle:word5 forState:UIControlStateNormal];
//    [btnOKConfirm addTarget:self action:@selector(actionOKConfirm) forControlEvents:UIControlEventTouchUpInside];
    [btnOKConfirm setTitle:word4 forState:UIControlStateNormal];
    [btnOKConfirm addTarget:self action:@selector(actionCancelConfirm) forControlEvents:UIControlEventTouchUpInside];
    
    [vwContinerConfirm addSubview:lblTitleConfirm];
    [vwContinerConfirm addSubview:vwLineTitleConfirm];
    [vwContinerConfirm addSubview:lblDescConfirm];
    [vwContinerConfirm addSubview:lblTextConfirm];
    [vwContinerConfirm addSubview:txtPwdConfirm];
    [vwContinerConfirm addSubview:btnEyeConfirm];
    [vwContinerConfirm addSubview:vwLineTextConfirm];
    [vwContinerConfirm addSubview:btnOKConfirm];
    [vwContinerConfirm addSubview:btnCancelConfirm];
    
    vwContinerConfirm.layer.cornerRadius = 16;
    
    [vwScroll setContentSize:CGSizeMake(widthContiner, vwContinerConfirm.frame.size.height)];
    
    [vwBgConfirm addSubview:vwScroll];
    [self.view addSubview:vwBgConfirm];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [vwScroll setContentSize:CGSizeMake(vwContinerConfirm.frame.size.width, vwContinerConfirm.frame.size.height + kbSize.height + 20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    vwScroll.contentInset = contentInsets;
    vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [vwScroll setContentSize:CGSizeMake(vwContinerConfirm.frame.size.width, vwContinerConfirm.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    vwScroll.contentInset = contentInsets;
    vwScroll.scrollIndicatorInsets = contentInsets;
}


- (void)confirmDisabled {
    [self showConfirm];
    flagSetting = @"DISABLE";
}

- (void)confirmEnabled {
    [self showConfirm];
    flagSetting = @"ENABLE";
}

- (void)actionOKConfirm {
    NSString *pwd = txtPwdConfirm.text;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    NSString *upwd = [userDefault objectForKey:@"password"];
    NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
    
    if ([[pwd MD5] isEqualToString:upwd]) {
        if ([flagOn isEqualToString:@"YES"]) {
            //[userDefault setObject:@"YES" forKey:@"mustLogin"];
        } else if ([flagOn isEqualToString:@"NO"]) {
            [userDefault setObject:@"NO" forKey:@"mustLogin"];
        }
        [userDefault synchronize];
        
        if ([flagSetting isEqualToString:@"ENABLE"]) {
            //[userDefault setObject:@"" forKey:@"password"];
            [vwBgConfirm removeFromSuperview];
            
            CGFloat width = [UIScreen mainScreen].bounds.size.width;
            CGFloat height = [UIScreen mainScreen].bounds.size.height;
            
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            UIViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NCreatePwdVC"];
            templateView.view.frame = CGRectMake(0, 0, width, height);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else if ([flagSetting isEqualToString:@"DISABLE"]) {
            [vwBgConfirm removeFromSuperview];
            
            NSString *tw;
            NSString *mw;
            if([lang isEqualToString:@"id"]){
                tw = @"Informasi";
                mw = @"Silahkan restart aplikasi";
            } else {
                tw = @"Information";
                mw = @"Please restart application";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:tw message:mw delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 456;
            [alert show];
        }
    } else {
        NSString *err_msg = @"";
        if([lang isEqualToString:@"id"]){
            err_msg = @"Kata Sandi Salah";
        } else {
            err_msg = @"Wrong Password";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:err_msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 123;
        [alert show];
    }
}

- (void)actionCancelConfirm {
    [self resetSetting];
    [vwBgConfirm removeFromSuperview];
}

- (void)resetSetting {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    if ([mustLogin isEqualToString:@"YES"]) {
        flagOn = @"YES";
        [self.switchActivate setSelected:YES];
        [self.switchActivate setOn:YES];
        [self.lblSetting setHidden:NO];
        [self.btnSetting setHidden:NO];
    } else if ([mustLogin isEqualToString:@"NO"]) {
        flagOn = @"NO";
        [self.switchActivate setSelected:NO];
        [self.switchActivate setOn:NO];
        [self.lblSetting setHidden:YES];
        [self.btnSetting setHidden:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 424) {
        if (buttonIndex == 0) {
            //Cancel Button
            [self resetSetting];
        }
        else if (buttonIndex == 1) {
            //OK Button
            if ([flagOn isEqualToString:@"YES"]) { //Enable Password
                [self confirmEnabled];
            } else if ([flagOn isEqualToString:@"NO"]) { //Disable Password
                [self confirmDisabled];
            }
        }
    } else if (alertView.tag == 123) {
        //Wrong Password
        [self resetSetting];
        [vwBgConfirm removeFromSuperview];
    } else if (alertView.tag == 456) {
        //Disable Password
        exit(0);
    } else if (alertView.tag == 229) {
        //Not Activated
        /*NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];*/
        
        //Check activate or not
        if (buttonIndex == 0) {
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        } else if (buttonIndex == 1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"NO" forKey:@"fromSliding"];
            [userDefault synchronize];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
            
            /*NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"id"]){
                valTitle = @"Konfirmasi";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Batal";
                valAgree = @"Lanjut";
            } else {
                valTitle = @"Confirmation";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Cancel";
                valAgree = @"Next";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            
            alert.tag = 230;
            [alert show];*/
        }
    } else if (alertView.tag == 230) {
        //Check register or not
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 231;
            [alert show];
        } else if (buttonIndex == 1) {
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
        }
    } else if (alertView.tag == 231) {
        //Back to Home
        NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
}

@end
