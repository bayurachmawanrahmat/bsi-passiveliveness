//
//  MSMRegistrationViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 15/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MobileRegistrationViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
