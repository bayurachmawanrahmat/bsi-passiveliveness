//
//  MSMRegistration03ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "MobileRegistration03ViewController.h"
#import "MobileRegistration04ViewController.h"
#import "Styles.h"
//#import "CounterTime.h"

@interface MobileRegistration03ViewController ()<UITextFieldDelegate>{
//    NSUserDefaults *userDefaults;
//    NSString *language;
    UIToolbar *toolBar;
    bool resendState;
//    CounterTime *timerCounter;
    NSTimer *timer;
//    int currMinute;
    int currSecond;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIButton *btnReg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblRegistrationCode;
@property (weak, nonatomic) IBOutlet UILabel *lblInputCode;
@property (weak, nonatomic) IBOutlet UILabel *lblPhonenumber;
@property (weak, nonatomic) IBOutlet UITextField *tf01;
@property (weak, nonatomic) IBOutlet UITextField *tf02;
@property (weak, nonatomic) IBOutlet UITextField *tf03;
@property (weak, nonatomic) IBOutlet UITextField *tf04;
@property (weak, nonatomic) IBOutlet UITextField *tf05;
@property (weak, nonatomic) IBOutlet UITextField *tf06;
@property (weak, nonatomic) IBOutlet UILabel *lblResendCode;
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;

@end

@implementation MobileRegistration03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    currSecond=59;
    resendState = NO;
    
    self.lblTitleBar.text = lang(@"REGISTRATION_TITLE");
    [self.btnReg setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    self.lblRegistrationCode.text = lang(@"REGISTRATION_CODE");
    self.lblResendCode.text = lang(@"RESEND_REGISTRATION_CODE");
    self.lblInputCode.text = lang(@"ENTER_REGISTRATION_CODE");
    
    NSString *phone = [dataManager.dataExtra valueForKey:@"msisdn"];
    if([phone length] > 5){
        NSString *firstChr = [phone substringToIndex:2];
        NSString *lastChr = [phone substringFromIndex: [phone length] - 3];

        NSMutableString *mask = [[NSMutableString alloc]init];
        [mask appendString:firstChr];
        for (int i=2; i<[phone length]-3; i++) {
            [mask appendString:@"*"];
        }
        [mask appendString:lastChr];
        
        _lblPhonenumber.text = mask;
    }else{
        _lblPhonenumber.text = @"******";
    }
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.tf01.keyboardType = UIKeyboardTypeNumberPad;
    self.tf01.delegate = self;
    self.tf01.tag = 1;
    self.tf01.secureTextEntry = YES;

    self.tf02.keyboardType = UIKeyboardTypeNumberPad;
    self.tf02.delegate = self;
    self.tf02.tag = 2;
    self.tf02.secureTextEntry = YES;

    self.tf03.keyboardType = UIKeyboardTypeNumberPad;
    self.tf03.delegate = self;
    self.tf03.tag = 3;
    self.tf03.secureTextEntry = YES;

    self.tf04.keyboardType = UIKeyboardTypeNumberPad;
    self.tf04.delegate = self;
    self.tf04.tag = 4;
    self.tf04.secureTextEntry = YES;

    self.tf05.keyboardType = UIKeyboardTypeNumberPad;
    self.tf05.delegate = self;
    self.tf05.tag = 5;
    self.tf05.secureTextEntry = YES;

    self.tf06.keyboardType = UIKeyboardTypeNumberPad;
    self.tf06.delegate = self;
    self.tf06.tag = 6;
    self.tf06.secureTextEntry = YES;

    self.tf06.inputAccessoryView = toolBar;
    
    self.btnReg.layer.cornerRadius = 20.0f;
    self.btnReg.layer.masksToBounds = YES;
    
    [self.backBtn setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack)];
    [self.backBtn addGestureRecognizer:tapBack];
    [self.btnReg addTarget:self action:@selector(actionRegistration) forControlEvents:UIControlEventTouchUpInside];

    self.lblResendCode.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapResend =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionResend)];
    [self.lblResendCode addGestureRecognizer:tapResend];
    self.lblResendCode.hidden = YES;
    self.lblResendCode.textColor = UIColorFromRGB(const_color_blue);
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self startTimer];
}

- (void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"registration"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            if(resendState){
                resendState = NO;
            }else{
                MobileRegistration04ViewController *nextScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"MOBREG04"];
                [nextScreen setMessage:[jsonObject valueForKey:@"response"]];
                [self.navigationController pushViewController:nextScreen animated:YES];
            }
        }else{
            [Utility showMessage:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]] instance:self];
        }
    }
}

- (void) actionResend{
    resendState = YES;
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=registration,step=send_otp,msisdn" needLoading:YES encrypted:NO banking:NO favorite:NO];
    currSecond=59;
    [self startTimer];
    self.tf01.text = @"";
    self.tf02.text = @"";
    self.tf03.text = @"";
    self.tf04.text = @"";
    self.tf05.text = @"";
    self.tf06.text = @"";
    [self.tf01 becomeFirstResponder];
    self.lblResendCode.hidden = YES;
    self.lblTimer.hidden = NO;
}

- (void) actionRegistration{
    resendState = NO;
    self.lblTimer.hidden = NO;
    
    NSString *str = [NSString stringWithFormat:@"%@%@%@%@%@%@",_tf01.text,_tf02.text,_tf03.text,_tf04.text,_tf05.text,_tf06.text];
    
    if([str length] != 6){
        [Utility showMessage:@"Kode Registrasi tidak lengkap" enMessage:@"Registration Code is not valid" instance:self];
    }else{
        [dataManager.dataExtra setValue:str forKey:@"otp"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:@"request_type=registration,step=action,language,pin_card_number,card_number,email,nik,dob,msisdn,transaction_id,otp" needLoading:YES encrypted:NO banking:NO favorite:NO];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ((textField.text.length == 0) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        
        if (nextResponder)
        {
            [nextResponder becomeFirstResponder];
            textField.text = string;
            if(textField.tag == 6){
                NSString*string = [NSString stringWithFormat:@"%@%@%@%@%@%@",self.tf01.text,self.tf02.text,self.tf03.text,self.tf04.text,self.tf05.text,self.tf06.text];
                [[DataManager sharedManager].dataExtra setValue:string forKey:@"otp"];
            }
        }
        else
        {
            return YES;
        }
    }else if((textField.text.length == 1) && [string isEqualToString:@""]){
        NSInteger nextTag = textField.tag - 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        
        if (nextResponder)
        {
            [nextResponder becomeFirstResponder];
            textField.text = string;
        }
        else
        {
            return YES;
        }
    }
    
    return NO;
}

- (void)actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}


- (void) startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
}

- (void) timerFired
{
    if(currSecond>0)
    {
        currSecond-=1;
        if(currSecond>-1)
            [_lblTimer setText:[NSString stringWithFormat:@"00:%02d", currSecond]];
    }
    else
    {
        [timer invalidate];
        [_lblTimer setHidden:YES];
        [_lblResendCode setHidden:NO];
    }
}

@end
