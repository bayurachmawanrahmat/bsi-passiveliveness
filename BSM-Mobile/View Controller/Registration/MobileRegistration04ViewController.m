//
//  MSMRegistration04ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "MobileRegistration04ViewController.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Styles.h"

@interface MobileRegistration04ViewController (){
    NSUserDefaults *userDefaults;
//    NSString *language;
    NSString *message;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIButton *btnReg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lblRegistrationDate;
@property (weak, nonatomic) IBOutlet UILabel *lblRegisteredDate;
@property (weak, nonatomic) IBOutlet UILabel *lblActivationCodeDesc;

@end

@implementation MobileRegistration04ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    userDefaults = [NSUserDefaults standardUserDefaults];
//    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
//    if([language isEqualToString:@"id"]){
//        self.lblTitleBar.text = @"Registrasi BSI Mobile";
//        self.lblRegistrationDate.text = @"Tanggal Registrasi";
//        self.lblSuccess.text = @"Registrasi Berhasil";
//        [self.btnReg setTitle:@"Aktivasi" forState:UIControlStateNormal];
    //    }else{
//        self.lblTitleBar.text = @"BSI Mobile Registration";
//        self.lblRegistrationDate.text = @"Registration Date";
//        self.lblSuccess.text = @"Registration Success";
//        [self.btnReg setTitle:@"Activation" forState:UIControlStateNormal];
//    }
    
    self.lblTitleBar.text = lang(@"REGISTRATION_TITLE");
    self.lblRegisteredDate.text = lang(@"REGISTRATION_DATE");
    self.lblSuccess.text = lang(@"REGISTRATION_SUCCESS");
    [self.btnReg setTitle:lang(@"ACTIVATION") forState:UIControlStateNormal];
    
    self.btnReg.layer.cornerRadius = 20.0f;
    self.btnReg.layer.masksToBounds = YES;
    [self.btnReg addTarget:self action:@selector(actionActivation) forControlEvents:UIControlEventTouchUpInside];
    
    [self.backBtn setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack)];
    [self.backBtn addGestureRecognizer:tapBack];
    
    self.lblActivationCodeDesc.text = message;
    self.lblRegisteredDate.text = [Utility dateToday:2];
}

- (void)actionBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) setMessage:(NSString*)newMessage{
    message = newMessage;
}

- (void)actionActivation{
    [self.slidingViewController resetTopViewAnimated:YES];
    [userDefaults setObject:@"YES" forKey:@"fromSliding"];
    [userDefaults setValue:@"@" forKey:@"openRoot"];
    [userDefaults synchronize];
    
    RootViewController *templateView =  [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ActivationVC"];
    [self.tabBarController setSelectedIndex:0];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    int i = 0;
    while (topController.presentedViewController) {
        if (i==0) {
            topController = topController.presentedViewController;
            break;
        }
        i++;
        
    }
    
    return topController;
}
@end
