//
//  MSMRegistration02ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "MobileRegistration02ViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "Validation.h"
#import "DESChiper.h"

@interface MobileRegistration02ViewController ()<UITextFieldDelegate>{
    UIToolbar *toolBar;
    Validation *validate;
    DESChiper *desChipper;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIButton *btnReg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *backBtn;

@property (weak, nonatomic) IBOutlet UIView *vwInput1;
@property (weak, nonatomic) IBOutlet UITextField *tfDebitCardNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleDebitCardNo;

@property (weak, nonatomic) IBOutlet UIView *vwInput2;
@property (weak, nonatomic) IBOutlet UITextField *tfPinAtm;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePINAtm;

@property (weak, nonatomic) IBOutlet UIView *vwInput3;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleEmail;

@property (weak, nonatomic) IBOutlet UIView *vwInput4;
@property (weak, nonatomic) IBOutlet UITextField *tfBirthDate;
@property (weak, nonatomic) IBOutlet UILabel *lblBirthDate;

@property (weak, nonatomic) IBOutlet UIView *vwInput5;
@property (weak, nonatomic) IBOutlet UITextField *tfNIK;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNIK;

@property (weak, nonatomic) IBOutlet UIView *vwINput6;
@property (weak, nonatomic) IBOutlet UITextField *tfNoHp;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNoHP;
@property (weak, nonatomic) IBOutlet UIScrollView *viewScrol;

@end

@implementation MobileRegistration02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }

    validate = [[Validation alloc]initWith:self];
    desChipper = [[DESChiper alloc]init];

    if([Utility isLanguageID]){
        _lblTitleDebitCardNo.text = @"No Kartu Debit";
        _lblTitlePINAtm.text = @"PIN ATM";
        _lblTitleEmail.text = @"Email";
        _lblBirthDate.text = @"Tanggal Lahir";
        _lblTitleNIK.text = @"NIK (NO KTP)";
        _lblTitleNoHP.text = @"No Hp Terdaftar";
    }else{
        _lblTitleDebitCardNo.text = @"No Debit Card";
        _lblTitlePINAtm.text = @"PIN ATM";
        _lblTitleEmail.text = @"Email";
        _lblBirthDate.text = @"Date of Birth";
        _lblTitleNIK.text = @"NIK (NO KTP)";
        _lblTitleNoHP.text = @"Registered Mobile Number";
    }
    
    self.lblTitleBar.text = lang(@"REGISTRATION_TITLE");
    [self.btnReg setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    
    
    self.btnReg.layer.cornerRadius = 20.0f;
    self.btnReg.layer.masksToBounds = YES;
    
    [self.backBtn setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack)];
    [self.backBtn addGestureRecognizer:tapBack];
    
    [self.btnReg addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnReg setEnabled:NO];
    
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.tfDebitCardNo.borderStyle = UITextBorderStyleNone;
    self.tfDebitCardNo.keyboardType = UIKeyboardTypeNumberPad;
    self.tfDebitCardNo.inputAccessoryView = toolBar;
    self.tfDebitCardNo.delegate = self;
    
    self.tfPinAtm.borderStyle = UITextBorderStyleNone;
    self.tfPinAtm.keyboardType = UIKeyboardTypeNumberPad;
    self.tfPinAtm.inputAccessoryView = toolBar;
    self.tfPinAtm.delegate = self;
    
    self.tfEmail.borderStyle = UITextBorderStyleNone;
    self.tfEmail.keyboardType = UIKeyboardTypeEmailAddress;
    self.tfEmail.inputAccessoryView = toolBar;
    self.tfEmail.delegate = self;
    
    self.tfBirthDate.borderStyle = UITextBorderStyleNone;
    [self setDatePicker];
    
    self.tfNIK.borderStyle = UITextBorderStyleNone;
    self.tfNIK.keyboardType = UIKeyboardTypeNumberPad;
    self.tfNIK.inputAccessoryView = toolBar;
    self.tfNIK.delegate = self;
    
    self.tfNoHp.borderStyle = UITextBorderStyleNone;
    self.tfNoHp.keyboardType = UIKeyboardTypePhonePad;
    self.tfNoHp.inputAccessoryView = toolBar;
    self.tfNoHp.delegate = self;
    
    [self setupView:self.vwInput1];
    [self setupView:self.vwInput2];
    [self setupView:self.vwInput3];
    [self setupView:self.vwInput4];
    [self setupView:self.vwInput5];
    [self setupView:self.vwINput6];
    
    [self setSideImage:self.tfBirthDate imageNamed:@"icon-calendar@2x.png"];
    
    [self registerForKeyboardNotifications];
    
}

- (void) setSideImage : (UITextField *)textField imageNamed : (NSString*)imageNamed{
    
    CGFloat height = (textField.frame.size.height)-10;
    
    UIImageView *dropIcon = [[UIImageView alloc] init];
    [dropIcon setFrame:CGRectMake(0, 0, height, height)];
    dropIcon.image = [UIImage imageNamed:imageNamed];
    dropIcon.contentMode = UIViewContentModeScaleToFill;
    
    UIView *sideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, height, height)];
    [sideView addSubview:dropIcon];
    
    textField.rightViewMode = UITextFieldViewModeAlways;
    textField.rightView = sideView;
}

- (void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) setupView : (UIView*) view{
    view.layer.cornerRadius = 10;
    view.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    view.layer.borderWidth = 2;
    view.backgroundColor = [UIColor whiteColor];
}

- (void) setDatePicker{
    UIDatePicker *objDate = [[UIDatePicker alloc]init];
    objDate.locale = [[NSLocale alloc]initWithLocaleIdentifier:@"id_ID"];
    objDate.backgroundColor = [UIColor whiteColor];
    objDate.datePickerMode = UIDatePickerModeDate;
    objDate.calendar = [NSCalendar currentCalendar];
    
    if (@available(iOS 13.4, *)) {
        objDate.preferredDatePickerStyle = UIDatePickerStyleWheels;
    }

    [objDate addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    self.tfBirthDate.inputView = objDate;
    self.tfBirthDate.inputAccessoryView = toolBar;
}

- (void)dateChanged :(UIDatePicker *)datePicker {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *selectedDate = [dateFormat stringFromDate:datePicker.date];
    self.tfBirthDate.text = selectedDate;
    
    [dateFormat setDateFormat:@"yyyyMMdd"];
    [dataManager.dataExtra setValue:[dateFormat stringFromDate:datePicker.date] forKey:@"dob"];
}

- (void)actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)actionNext{
    [dataManager.dataExtra setValue:_tfDebitCardNo.text forKey:@"card_number"];
    NSString* str = [dataManager.dataExtra valueForKey:@"encKey"];
    if(str){
        NSData *data = [[NSData alloc]initWithBase64EncodedString:str options:NSDataBase64DecodingIgnoreUnknownCharacters];
        NSString *trial = [desChipper doDecryptDES:data key:KEY_ENCRYPTED_ZPK];
        [dataManager.dataExtra setValue:[desChipper doPinBlock:_tfPinAtm.text key:trial] forKey:@"pin_card_number"];
    }
    
    [dataManager.dataExtra setValue:_tfEmail.text forKey:@"email"];
    [dataManager.dataExtra setValue:_tfNIK.text forKey:@"nik"];
    [dataManager.dataExtra setValue:_tfNoHp.text forKey:@"msisdn"];
    
    if([self isValid]){
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:@"request_type=registration,step=send_otp,language,msisdn,pin" needLoading:YES encrypted:NO banking:NO favorite:NO];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"registration"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            TemplateViewController *nextScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"MOBREG03"];
            [self.navigationController pushViewController:nextScreen animated:YES];

        }else{
            [Utility showMessage:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]] instance:self];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if(textField == _tfDebitCardNo || textField == _tfNIK){
        int maxLength = 16;
        if(newText.length > maxLength){
            return NO;
        }
    }else if(textField == _tfPinAtm){
        int maxLength = 6;
        if(newText.length > maxLength){
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)isValid{
    
    return [validate validateDebitCard:_tfDebitCardNo.text] &&
    [validate validatePIN:_tfPinAtm.text] &&
    [validate validateBirthDate:_tfBirthDate.text] &&
    [validate validateEmail:_tfEmail.text] &&
    [validate validateKTP:_tfNIK.text] &&
    [validate validatePhoneNumber:_tfNoHp.text];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == _tfNIK){
        [validate validateBirthDate:_tfBirthDate.text];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == _tfDebitCardNo){
        [validate validateDebitCard:_tfDebitCardNo.text];
    }
    if(textField == _tfPinAtm){
        [validate validatePIN:_tfPinAtm.text];
    }
    if(textField == _tfEmail){
        [validate validateEmail:_tfEmail.text];
    }
    if(textField == _tfNIK){
        [validate validateKTP:_tfNIK.text];
    }
    
    if(textField == _tfBirthDate){
        [validate validateBirthDate:_tfBirthDate.text];
    }
    
    if(textField == _tfNoHp){
        [validate validatePhoneNumber:_tfNoHp.text];
    }
    
    if([validate validateEmptyField:@[_tfDebitCardNo.text, _tfPinAtm.text, _tfEmail.text, _tfNIK.text, _tfNoHp.text, _tfBirthDate.text]]){
        [_btnReg setEnabled:YES];
    }else{
        [_btnReg setEnabled:NO];
    }
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.viewScrol.contentInset = contentInsets;
    self.viewScrol.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.viewScrol.contentInset = contentInsets;
    self.viewScrol.scrollIndicatorInsets = contentInsets;
}


@end
