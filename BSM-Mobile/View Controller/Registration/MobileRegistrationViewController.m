//
//  MSMRegistrationViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 15/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "MobileRegistrationViewController.h"
#import "MenuViewController.h"
#import "Styles.h"

@interface MobileRegistrationViewController (){
    NSUserDefaults *userDefaults;
    NSString *language;
    BOOL checkState;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet CustomBtn *btnReg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkedBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblAgreement;
@property (weak, nonatomic) IBOutlet UIView *viewTitle;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@end

@implementation MobileRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    if (@available(iOS 13.0, *)) {
         self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    _viewTitle.layer.cornerRadius = 10;
    _viewTitle.layer.borderWidth = 2;
    _viewTitle.layer.borderColor = [UIColorFromRGB(const_color_primary) CGColor];
    
    _viewContent.layer.cornerRadius = 10;
    _viewContent.layer.borderWidth = 2;
    _viewContent.layer.borderColor = [UIColorFromRGB(const_color_primary) CGColor];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
//    if([language isEqualToString:@"id"]){
//        self.lblTitleBar.text = @"Syarat dan Ketentuan";
////        [self.btnReg setTitle:@"Selanjutnya" forState:UIControlStateNormal];
//        self.lblAgreement.text = @"Saya telah membaca dan setuju dengan syarat dan ketentuan yang berlaku";
//    }else{
//        self.lblTitleBar.text = @"Terms and Conditions";
////        [self.btnReg setTitle:@"Next" forState:UIControlStateNormal];
//        self.lblAgreement.text = @"I agree for terms and condition";
//    }
    
    self.lblTitleBar.text = lang(@"TERMS_CONDITION");
    [self.btnReg setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    self.lblAgreement.text = lang(@"TERMS_CONDITION_AGREEMENT");
    
    [self.checkedBtn setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
    
    self.btnReg.layer.cornerRadius = 20.0f;
    self.btnReg.layer.masksToBounds = YES;
    
    [self.backBtn setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapBack =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionBack)];
    [self.backBtn addGestureRecognizer:tapBack];
    
    [self.btnReg setBackgroundColor:UIColorFromRGB(const_color_gray)];
    [self.btnReg setColorSet:PRIMARYCOLORSET];
    [self.btnReg setEnabled:NO];
    checkState = NO;
    [self.checkedBtn addTarget:self action:@selector(actionCheck) forControlEvents:UIControlEventTouchUpInside];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=registration,step=info,language" needLoading:YES encrypted:NO banking:NO favorite:NO];
}

- (void)actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionCheck{
    if(checkState){
        [self.checkedBtn setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
        checkState = NO;
        [self.btnReg setEnabled:NO];
    }else{
        [self.checkedBtn setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        checkState = YES;
        [self.btnReg setEnabled:YES];
    }
}

- (IBAction)actionCall:(id)sender {
    TemplateViewController *nextScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"MOBREG02"];
    [self.navigationController pushViewController:nextScreen animated:YES];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"registration"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            [self.lblInfo setText:[[dataDict objectForKey:@"info"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n\n"]];
            [dataManager.dataExtra setValue:[dataDict objectForKey:@"encrypted_zpk"] forKey:@"encKey"];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

@end
