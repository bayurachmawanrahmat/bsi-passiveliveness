//
//  MSMRegistration04ViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MobileRegistration04ViewController : TemplateViewController

- (void) setMessage:(NSString*)newMessage;

@end

NS_ASSUME_NONNULL_END
