//
//  RootViewController.h
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSUserdefaultsAes.h"

@interface RootViewController : UIViewController
- (void) gotoInbox;
- (void) backToR;
- (void) backPrevious;
- (void) openTemplate : (NSArray*)temp;
- (void) openTemplateByMenuID : (NSString*)menuID;
- (void) openTemplateByMenuID:(NSString *)menuID withData:(NSDictionary*)data;
- (void) openTemplateByMenuIDWithLogin : (NSString*)menuID;
- (void) openTemplateByMenuID : (NSString*)menuID andCodeContent: (NSString*)code;
- (void) openTemplateByCurrentMenu : (NSString*)menuID;
- (void) gotoGold;
- (void) stopIdleTimer;
- (void) logoutSession;
- (void) setShowMenu : (NSString *) state;
- (void) stateShow : (bool) state;
- (void) openStatic:(NSString *)templateName;
- (void) openStatic:(NSString *)storyboard withTemplateName:(NSString*)templateName;
- (void) gotoPopLogin :(NSString*) mFeat;

- (void) openRedirectTo:(NSString*)menuid
             withMenuID:(BOOL)withmenuid
                andCode:(NSString*)code
               setTitle:(NSString*)title
                andText:(NSString*)text
         andTitleButton:(NSString*)titleButton;
- (void) selectedFeature:(int)index;
- (void) openAppStore;
- (void) openNodes : (NSString *)param;

@end
