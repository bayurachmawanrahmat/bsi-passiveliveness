//
//  GESubmissionPINViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"
#import "CustomBtn.h"
#import "Styles.h"

NS_ASSUME_NONNULL_BEGIN

@interface GESubmissionPINViewController : TemplateViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *titleInput;
@property (weak, nonatomic) IBOutlet UITextField *tfInput;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonBack;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@end

NS_ASSUME_NONNULL_END
