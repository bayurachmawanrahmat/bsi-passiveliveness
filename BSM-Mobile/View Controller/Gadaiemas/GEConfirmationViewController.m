//
//  GEConfirmationViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEConfirmationViewController.h"
#import "GESubmissionPINViewController.h"
#import "Styles.h"
#import "GadaiEmasConnect.h"
#import "Utility.h"

@interface GEConfirmationViewController (){
    BOOL isPickup;
    NSUserDefaults *userDefaults;
    NSDictionary *reservasiGadai;
    NSString *language;
}

@end

@implementation GEConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:self.topConstant];
    
    self.titleBar.text = NSLocalizedString(@"GE_SUBMISSION_TITLE_BAR", @"");
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    reservasiGadai = [userDefaults objectForKey:@"reservasi_gadai"];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    isPickup = [[dataManager.dataExtra valueForKey:@"typeLayanan"]isEqualToString:@"Pickup"];

//    [dataManager.dataExtra setValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"msisdn"] forKey:@"noTelp"];
    [dataManager.dataExtra setValue:[NSUserdefaultsAes getValueForKey:@"msisdn"] forKey:@"noTelp"];
    
    
    
    [self.buttonAgree addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonAgree setColorSet:PRIMARYCOLORSET];
    [self.buttonBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBack setColorSet:SECONDARYCOLORSET];
    
    NSString *agunan = @"";
    if([Utility isLanguageID]){
        agunan = @"Jenis Agunan";
        [self.buttonAgree setTitle:@"Setuju" forState:UIControlStateNormal];
        [self.buttonBack setTitle:@"Sebelumnya" forState:UIControlStateNormal];
        
        self.name.text = [NSString stringWithFormat:@"Nama  : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"customer_name"]];
        self.title1.text = @"Nilai Pengajuan";
        self.title2.text = @"Jangka Waktu";
        self.title3.text = @"Metode Perpanjangan";
        self.title4.text = @"Jenis Promo";
        self.title5.text = @"Cara Pengajuan";
        self.title6.text = @"Kantor Cabang";
        self.title7.text = @"Tanggal Kedatangan";
        self.title8.text = @"Jam Kedatangan";
        self.content5.text = @"Datang ke Bank";
        if(isPickup){
            self.content5.text = @"Layanan Pickup";
            self.title7.text = @"Tanggal Pickup";
            self.title8.text = @"Jam Pickup";
        }
        
        self.labelPickupFee.text = [NSString stringWithFormat:@"Atas pengajuan ini, Anda dikenakan biaya Layanan Pickup sebesar Rp. %@ didebit dari rekening Anda",[Utility formatCurrencyValue:[[[[NSUserDefaults standardUserDefaults]objectForKey:@"reservasi_gadai"]objectForKey:@"ReservasiGadai_BiayaPickup"]doubleValue]]];
    }else{
        agunan = @"Collateral";
        [self.buttonAgree setTitle:@"Agree" forState:UIControlStateNormal];
        [self.buttonBack setTitle:@"Back" forState:UIControlStateNormal];
        
        self.name.text = [NSString stringWithFormat:@"Name  : %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"customer_name"]];

        self.title1.text = @"Submission Value";
        self.title2.text = @"Terms";
        self.title3.text = @"Extension Method";
        self.title4.text = @"Type of Promo";
        self.title5.text = @"Submission Method";
        self.title6.text = @"Branch";
        self.title7.text = @"Arrival Date";
        self.title8.text = @"Arrival Time";
        self.content5.text = @"Go to the Bank";
        if(isPickup){
            self.content5.text = @"Pickup Service";
            self.title7.text = @"Pickup Date";
            self.title8.text = @"Pickup Time";
        }
        
        self.labelPickupFee.text = [NSString stringWithFormat:@"with this submission, you will be charged Rp. %@ from your account for pickup service fee",[Utility formatCurrencyValue:[[[[NSUserDefaults standardUserDefaults]objectForKey:@"reservasi_gadai"]objectForKey:@"ReservasiGadai_BiayaPickup"]doubleValue]]];
    }


    NSArray *dataCollateral = [dataManager.dataExtra objectForKey:@"jsonListObjectGadai"];
    NSMutableString* dataColl = [[NSMutableString alloc]init];
    [dataColl appendString:agunan];
    [dataColl appendString:@"\n"];
    for(NSDictionary *dict in dataCollateral){
        [dataColl appendString:[NSString stringWithFormat:@"- %@ %@K %.2fgr",[dict objectForKey:@"ObjectGadai"],[dict objectForKey:@"Karatase"],[[dict objectForKey:@"BeratEmas"]doubleValue]]];
        [dataColl appendString:@"\n"];
    }
    
    self.collateralType.text = dataColl;
    self.content1.text = [NSString stringWithFormat:@"Rp. %@",[Utility formatCurrencyValue:[[dataManager.dataExtra objectForKey:@"nilaiPegajuanPembiayaan"]doubleValue]]];
    self.content2.text = [NSString stringWithFormat:@"%@ %@",[reservasiGadai objectForKey:@"GadaiJangkaWaktu"],lang(@"MONTHS")];
    self.content3.text = [dataManager.dataExtra objectForKey:@"metodePerpanjangan"];
    if([[dataManager.dataExtra objectForKey:@"promo"]isEqualToString:@""] || [dataManager.dataExtra objectForKey:@"promo"] == nil){
        if([Utility isLanguageID]){
            self.content4.text = @"Tidak Ada";
        }else{
            self.content4.text = @"None";
        }
    }else{
        self.content4.text = [dataManager.dataExtra objectForKey:@"promo"];
    }
    self.content6.text = [dataManager.dataExtra objectForKey:@"namaCabang"];

    if(isPickup){
        self.content7.text = [dataManager.dataExtra objectForKey:@"tanggalPickup"];
        self.content8.text = [dataManager.dataExtra objectForKey:@"jamPickup"];
        self.viewPickupFee.hidden = NO;

    }else{
        self.content7.text = [dataManager.dataExtra objectForKey:@"tanggalkedatangan"];
        self.content8.text = [dataManager.dataExtra objectForKey:@"jamkedatangan"];
        self.viewPickupFee.hidden = YES;
    }
    
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionNext{
    GESubmissionPINViewController *ges03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmissionPIN"];
    
    UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
    [currentVC pushViewController:ges03 animated:YES];
}

@end
