//
//  LVGSubMenuViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GELV02MenuViewController.h"
#import "ListGoldMenuCell.h"
#import "GEInformationViewController.h"
#import "PopUpLoginViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "GadaiEmasConnect.h"
#import "NSUserdefaultsAes.h"

@interface GELV02MenuViewController ()<UITableViewDelegate, UITableViewDataSource, PopupLoginDelegate, GadaiEmasConnectDelegate>{
    
    NSArray *listData;
    NSUserDefaults *userDefaults;
    NSString *language;
    NSDictionary *selectedData;
    
    int retry;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UITableView *listView;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;

@end

@implementation GELV02MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [Utility isLanguageID] ? [self.titleBar setText:@"Gadai Emas"] : [self.titleBar setText:@"Gold Pawn"];
//    [dataManager.dataExtra setValue:@"7095597496" forKeyPath:@"norek"];
//    [self getFirstListAccount];
    
    self.listView.delegate = self;
    self.listView.dataSource = self;
    [self.listView registerNib:[UINib nibWithNibName:@"ListGoldMenuCell" bundle:nil] forCellReuseIdentifier:@"LVGOLDMENU"];

    self.listView.tableFooterView = [[UIView alloc]init];
//    [self getInit];
//    retry = 0;
}

- (void) getInit{
    GadaiEmasConnect *connect = [[GadaiEmasConnect alloc]initWithDelegate:self];
    [connect getDataFromEndPoint:@"InitParamGadaiReservasi" needLoading:YES textLoading:@"" encrypted:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ListGoldMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LVGOLDMENU"];
    cell.labelText.text = [listData[indexPath.row] objectForKey:@"title_id"];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSNumber *code = [listData[indexPath.row] objectForKey:@"code"];
    
    selectedData = listData[indexPath.row];

    if([code isEqual:@1]){
        [self getInit];
        retry = 0;
//        selectedData = listData[indexPath.row];
//        [self cekLogin];
    }else{
        [self cekLogin];
    }
//    else if([code isEqual:@2]){
// open fitur with menu id
//        [self openTemplateByMenuIDWithLogin:@"00195"];
//    }
//    [self cekLogin];
}

- (void)setData:(NSArray *)list{
    listData = list;
    [self.listView reloadData];
}

- (void) cekLogin{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_REQUEST") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        
    } else {
        if ([hasLogin isEqualToString:@"NO"] || hasLogin == nil) {
            [self.tabBarController setSelectedIndex:0];
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PopUpLoginViewController *popLogin = [ui instantiateViewControllerWithIdentifier:@"PopupLogin"];
            [popLogin setDelegate:self];
            [popLogin setIdentfier:[selectedData objectForKey:@"code"]];
            [self presentViewController:popLogin animated:YES completion:nil];
        }
        else {
            [self selectionAction];
        }
    }
}

- (void)loginDoneState:(NSString *)identifier{
    [self selectionAction];
}

- (void)selectionAction{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Gadaiemas" bundle:nil];
    if([[selectedData objectForKey:@"code"]isEqual:@1]){
        GEInformationViewController *geInfo = [ui instantiateViewControllerWithIdentifier:@"GadaiInfo"];
        
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:geInfo animated:YES];
    }
    else if ([[selectedData objectForKey:@"code"]isEqual:@2]){
        [self openTemplateByMenuIDWithLogin:@"00195"];
    }
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if([endPoint isEqualToString:@"InitParamGadaiReservasi"]){
        if(error != nil){
            if(retry < 3){
                retry = retry + 1;
                [self getInit];
            }else{
                
                NSLog(@"%@",[error localizedDescription]);
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ERR_REQUEST", @"") preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            NSLog(@"%@",dict);
            NSDictionary* data = [dict objectForKey:@"response"];
            NSUserDefaults *userd = [NSUserDefaults standardUserDefaults];
            [userd setValue:data forKey:@"reservasi_gadai"];
            
//            selectedData = listData[indexPath.row];
            [self cekLogin];
        }
    }
}

@end
