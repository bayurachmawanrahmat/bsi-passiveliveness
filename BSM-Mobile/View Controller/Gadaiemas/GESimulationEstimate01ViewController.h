//
//  GESimulationEstimateViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface GESimulationEstimate01ViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet CustomBtn *btnPrev;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSimulate;
@property (weak, nonatomic) IBOutlet UITableView *tableDataSimulate;

@end

NS_ASSUME_NONNULL_END
