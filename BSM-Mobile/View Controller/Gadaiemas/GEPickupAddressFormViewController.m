//
//  GEPickupAddressFormViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEPickupAddressFormViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface GEPickupAddressFormViewController (){
    BOOL checkState;
}

@end

@implementation GEPickupAddressFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Styles setTopConstant:self.topConstant];
    
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(actionDone)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    self.twStreet.inputAccessoryView = toolbar;
    self.twStreet.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    self.twStreet.layer.borderWidth = 1;
    self.twStreet.layer.cornerRadius = 4;
    
    self.tfVillage.inputAccessoryView = toolbar;
    self.tfRT.inputAccessoryView = toolbar;
    self.tfRT.keyboardType = UIKeyboardTypeNumberPad;
    self.tfRW.inputAccessoryView = toolbar;
    self.tfRW.keyboardType = UIKeyboardTypeNumberPad;
    self.tfDistrict.inputAccessoryView = toolbar;
    
    self.twLandmarkName.inputAccessoryView = toolbar;
    self.twLandmarkName.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    self.twLandmarkName.layer.borderWidth = 1;
    self.twLandmarkName.layer.cornerRadius = 4;
    self.tfLandmarkNo.inputAccessoryView = toolbar;
    
    
    UITapGestureRecognizer *tapReco = [[UITapGestureRecognizer alloc]init];
    [tapReco addTarget:self action:@selector(actionChecked)];
    [tapReco setNumberOfTapsRequired:1];
    [self.imgCheckbox setUserInteractionEnabled:YES];
    [self.imgCheckbox addGestureRecognizer:tapReco];
    checkState = NO;
    
    self.lblTitleStreet.text = NSLocalizedString(@"GE_STREET", @"");
    self.lblTitleRTRW.text = NSLocalizedString(@"GE_RTRW", @"");
    self.lblTitleVillage.text = NSLocalizedString(@"GE_SUBDISTRICT", @"");
    self.lblTitleDistric.text = NSLocalizedString(@"GE_DISTRICT", @"");
    self.lblTitleLandmark.text = NSLocalizedString(@"GE_LANDMARK", @"");
    
//    self.lblCheckboxInfo.text = NSLocalizedString(@"GE_DESC_PICKUP_SERVICE", @"");
    
    NSMutableAttributedString * stringFirst = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"GE_DESC_PICKUP_SERVICE_HEADER","")];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"GE_DESC_PICKUP_SERVICE", @"")];
    
    NSMutableParagraphStyle *paragraphStyle;
    paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setTabStops:@[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:15 options:nil]]];
    [paragraphStyle setDefaultTabInterval:15];
    [paragraphStyle setFirstLineHeadIndent:0];
    UIFont *font = [UIFont fontWithName:@"Lato" size:16];
    CGFloat indent = [Utility heightWrapWithText:@"1. " fontName:font expectedSize:CGSizeMake(16, 16)].width;
    [paragraphStyle setHeadIndent:indent];

    [string addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0,[string length])];
    
    [stringFirst appendAttributedString:string];
    self.lblCheckboxInfo.attributedText = stringFirst;
    
    self.lblTitleDetailPickup.text = NSLocalizedString(@"GE_PICKUP_LOCATION_DETAIL", @"");
    
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    
    [self.buttonBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBack setTitle:NSLocalizedString(@"BACK", @"") forState:UIControlStateNormal];
    [self.buttonBack setColorSet:SECONDARYCOLORSET];

    [self.buttonNext setEnabled:NO];

    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown : (NSNotification*)notification{
    NSDictionary*info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height/2 + 30, 0);
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) actionChecked{
    if(checkState){
        [self.imgCheckbox setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        checkState = NO;
        [self.buttonNext setEnabled:NO];
    }else{
        [self.imgCheckbox setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        checkState = YES;
        [self.buttonNext setEnabled:YES];
    }
}

- (void) actionDone{
    [self.view endEditing:YES];
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionNext{
    //pickup
    
    [dataManager.dataExtra setValue:self.twStreet.text forKey:@"addressPickup_street"];
    [dataManager.dataExtra setValue:[NSString stringWithFormat:@"rt %@ rw %@",self.tfRT.text, self.tfRW.text] forKey:@"addressPickup_rtrw"];
    [dataManager.dataExtra setValue:self.tfVillage.text forKey:@"addressPickup_kelurahan"];
    [dataManager.dataExtra setValue:self.tfDistrict.text forKey:@"addressPickup_kecamatan"];
    [dataManager.dataExtra setValue:self.twLandmarkName.text forKey:@"addressPickup_landmark1"];
    
    if([self.twStreet.text isEqualToString:@""] ||  [self.tfRT.text isEqualToString:@""] || [self.tfRW.text isEqualToString:@""] || [self.tfVillage.text isEqualToString:@""] || [self.tfDistrict.text isEqualToString:@""]){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ADDRESS_VALID_ALERT", @"") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else if(!checkState){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Tandai Checkmark yang tersedia" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        TemplateViewController *ges03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmissionDataForm"];
        
        UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
        [currentVC pushViewController:ges03 animated:YES];
    }
    
}

@end
