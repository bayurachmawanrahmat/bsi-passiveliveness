//
//  GEReceiptViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEReceiptViewController.h"
#import "GESubmissionDataFormViewController.h"
#import "CustomBtn.h"
#import "Styles.h"
#import "Utility.h"
#import "InboxHelper.h"
#import "GadaiEmasConnect.h"
#import "AESCipher.h"
#import "RecentlyHelper.h"
#import "NSUserdefaultsAes.h"

@interface GEReceiptViewController ()<GadaiEmasConnectDelegate>{
    NSString *dear;
    UIView *viewWhite;
    BOOL isUpdateJamPickup;
    NSString *nomorPengajuan;
    NSString *header;
    NSString *content;
    NSString *footer;
}

@property (nonatomic) AESCipher *aesChiper;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UIScrollView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwTextContent;
@property (weak, nonatomic) IBOutlet UILabel *labelContentHeader;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet UILabel *labelContentFooter;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonOK;
//@property (weak, nonatomic) IBOutlet UIImageView *imageShare;
@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet CustomBtn *btnShare;

@end

@implementation GEReceiptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:self.topConstant];
    self.aesChiper = [[AESCipher alloc] init];

    self.labelTitleBar.text = NSLocalizedString(@"GE_SUBMISSION_TITLE_BAR", @"");
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    dear = @"";
    if([Utility isLanguageID]){

        dear = @"Yth. ";
        nomorPengajuan = @"Nomor Pengajuan";
        header = @"PENGAJUAN GADAI EMAS";
        content = @"Pengajuan Gadai Emas anda telah berhasil dibuat.\nRincian pengajuan akan dikirimkan melalui email Anda.";
        footer = @"Terima kasih telah menggunakan BSI mobile.\nsemoga layanan kami mendapatkan berkah bagi anda";
        [_btnShare setTitle:@"Bagikan" forState:UIControlStateNormal];

    }else
    {
        dear = @"Dear. ";
        nomorPengajuan = @"Submission Number";
        header = @"SUBMISSION COMPLETED";
        content = @"Your gold pawn subsmission was successfully made.\nSubmission details will be sent to your email.";
        footer = @"Thankyou for using BSI mobile services";
        [_btnShare setTitle:@"Share" forState:UIControlStateNormal];
    }
    
    [_btnShare setImage:[UIImage imageNamed:@"ic_bt_share_prm"] forState:UIControlStateNormal];
    _btnShare.layer.borderColor = [UIColorFromRGB(const_color_darkprimary)CGColor];
    _btnShare.backgroundColor = UIColorFromRGB(const_color_basecolor);
    _btnShare.layer.borderWidth = 2;
    [_btnShare setTitleColor:UIColorFromRGB(const_color_primary) forState:UIControlStateNormal];
    [_btnShare setTintColor:UIColorFromRGB(const_color_primary)];
    [_btnShare addTarget:self action:@selector(actionShare:) forControlEvents:UIControlEventTouchUpInside];

    NSString *api = @"";
    NSString *param= @"";
    GadaiEmasConnect *connect = [[GadaiEmasConnect alloc]initWithDelegate:self];

    
    if([dataManager.dataExtra objectForKey:@"updateJamPickup"]){
//        api = @"";
//        param = @"";
        isUpdateJamPickup = YES;
        
        NSString *stringURL = [NSString stringWithFormat:@"UpdateJamPickup?KodeReservasi=%@&typeLayanan=%@&kodeCabang=%@&tanggalPickup=%@&jamPickup=%@",
                               [dataManager.dataExtra objectForKey:@"KodeReservasi"],
                               [dataManager.dataExtra objectForKey:@"typeLayanan"],
                               [dataManager.dataExtra objectForKey:@"kodeCabang"],
                               [dataManager.dataExtra objectForKey:@"tanggalPickup"],
                               [dataManager.dataExtra objectForKey:@"jamPickup"]];
        
        [connect getDataFromEndPoint:stringURL needLoading:YES textLoading:@"" encrypted:NO];
    }else{
        isUpdateJamPickup = NO;
        if([[dataManager.dataExtra valueForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
            api = @"submitReservasiGadaiPickup";
            param = @"norek,language,KodeReservasi,typeLayanan,kodeCabang,lokasicabang,geotagingOrigin,geotagigDestination,cif,nilaiPegajuanPembiayaan,promo,noTelp,addressPickup_street,addressPickup_rtrw,addressPickup_kelurahan,addressPickup_kecamatan,addressPickup_landmark1,addressPickup_landmark2,tanggalPickup,jamPickup,email,kyc_tujuanpembiayaan,kyc_SumberKepemilikan,kyc_SumberPelunasan,rekeningPencairan,metodePerpanjangan,AML1A,AML1ANote,AML1B,AML1BNote,AML2A,AML2ANote,AML2B,AML2BNote,AML2C,AML2CNote,AML3A,AML3ANote,AML3B,AML3BNote,AML3C,AML3CNote,jarakKeLokasi,fotoDiri,fotoKTP,fotoDiriKTP,fotoEmas,pin";
            [connect sendPostData:param endPoint:api needLoading:YES textLoading:@"" encrypted:NO];
        }else{
            api = @"submitReservasiGadaiDatangkeBank";
            param = @"norek,language,KodeReservasi,typeLayanan,kodeCabang,lokasiCabang,geotagingOrigin,geotagigDestination,cif,nilaiPengajuanPembiayaan,nilaiPegajuanPembiayaan,promo,noTelp,tanggalkedatangan,jamkedatangan,email,kyc_tujuanpembiayaan,kyc_SumberKepemilikan,kyc_SumberPelunasan,rekeningPencairan,metodePerpanjangan,AML1A,AML1ANote,AML1B,AML1BNote,AML2A,AML2ANote,AML2B,AML2BNote,AML2C,AML2CNote,AML3A,AML3ANote,AML3B,AML3BNote,AML3C,AML3CNote,jarakKeLokasi,fotoDiri,fotoKTP,fotoDiriKTP,fotoEmas,addressPickup_street,addressPickup_rtrw,lokasicabang,pin";
            [connect sendPostData:param endPoint:api needLoading:YES textLoading:@"" encrypted:NO];
        }
    }
    
    
    
    
    [_buttonOK addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidAppear:(BOOL)animated{
    [self whiteView];
}

- (void)whiteView{
    viewWhite = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.viewFrame.frame.size.width, self.viewFrame.frame.size.height)];
    [viewWhite setBackgroundColor:[UIColor whiteColor]];
    [self.viewFrame addSubview:viewWhite];
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        NSLog(@"Gadai Receipt Log Error_%@",error);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"GE_ERR_REQUEST") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        viewWhite.hidden = YES;
        UIImage *backgroundImage = [UIImage imageNamed:@"bg_receipt.png"];
        [_vwTextContent setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];
        
//        [_imageShare addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionShare:)]];
//        [_imageShare setUserInteractionEnabled:YES];
//
        [_btnShare addTarget:self action:@selector(actionShare:) forControlEvents:UIControlEventTouchUpInside];
        
        isUpdateJamPickup ? [self showDataFromInbox] : [self proceedResponse:dict];
            
    }
}

- (void) actionOK{
    [self backToR];
}

- (void) inserData : (NSDictionary*) dict{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *currentDate = [dateFormat stringFromDate:date];
    
    NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
    [dictList setValue:[dict objectForKey:@"trxref"] forKey:@"trxref"];
    [dictList setValue:[dict objectForKey:@"trxref"] forKey:@"transaction_id"];
    [dictList setValue:[dict objectForKey:@"title"] forKey:@"title"];
    [dictList setValue:[dict objectForKey:@"msg"] forKey:@"msg"];
    [dictList setValue:[dict objectForKey:@"footer_msg"] forKey:@"footer_msg"];
    [dictList setValue:@"GENCF02" forKey:@"template"];
    [dictList setValue:@"General" forKey:@"jenis"];
    [dictList setValue:currentDate forKey:@"time"];
    [dictList setValue:@"0" forKey:@"marked"];
    
    InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
    [inboxHeldper insertDataInbox:dictList];
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *strCID = [NSString stringWithFormat:@"%@", [userDefault valueForKey:@"customer_id"]];
    NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
    
    NSMutableArray *arBackUpInbox = [[NSMutableArray alloc]init];
    
    NSDictionary *backUpInbox = [Utility dictForKeychainKey:strCID];
    NSMutableDictionary *contentBackup = [[NSMutableDictionary alloc]init];
    
    //disini validasi jika sudah ada data dikeychain ambil data timpa ke array baru
    if (backUpInbox) {
        NSArray *arrLastBackup = [backUpInbox valueForKey:@"data"];
        for(NSDictionary *dictLastBackup in arrLastBackup){
            if([dictLastBackup valueForKey:@"content"]!= nil){
                NSDictionary *data = @{@"content" : [dictLastBackup valueForKey:@"content"]};
                [arBackUpInbox addObject:data];
            }
        }
        
    }
    NSDictionary *dataDd = [_aesChiper dictAesEncryptString:dictList];
    
    [contentBackup setObject:dataDd forKey:@"content"];
    [arBackUpInbox addObject:contentBackup];
    
    if (arBackUpInbox.count > 0) {
        NSLog(@"Data Array For Backup : %@", arBackUpInbox);
        NSMutableDictionary *storeBackupInbox = [[NSMutableDictionary alloc]init];
        [storeBackupInbox setObject:arBackUpInbox forKey:@"data"];
        if (storeBackupInbox) {
            NSLog(@"Data Dict For Backup : %@", storeBackupInbox);
            [Utility removeForKeychainKey:strCID];
            [Utility setDict:storeBackupInbox forKey:strCID];
        }
    }
}

-(void)actionShare : (UITapGestureRecognizer *) sender{
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    UIGraphicsBeginImageContextWithOptions(_vwContent.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_vwContent.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
    
}

- (void) showDataFromInbox{
    InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
    NSDictionary *dataSend = [inboxHeldper readDataInbox:[dataManager.dataExtra objectForKey:@"trxID"] trxrf:[dataManager.dataExtra objectForKey:@"KodeReservasi"]];
    
    
    self.labelContentHeader.text = [dataSend objectForKey:@"title"];
    self.labelContent.text = [NSString stringWithFormat:@"%@:\n%@\n\n%@%@\n\n%@",
                              [dataSend objectForKey:@"trxref"],
                              [dataSend objectForKey:@"KodeReservasi"],
                              dear,
                              [dataSend objectForKey:@"namaNasabah"],
                              [dataSend objectForKey:@"msg"]];
    self.labelContentFooter.text = [dataSend objectForKey:@"footer_msg"];
    
}

- (NSString*) createTrxID : (NSString *)reservationCode{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    NSString *newString = [[reservationCode componentsSeparatedByCharactersInSet:
                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                    componentsJoinedByString:@""];
    NSString *trxID = [NSString stringWithFormat:@"%@%@",dateString,newString];
    return trxID;
}

- (void) proceedResponse : (NSDictionary*) dict{
    NSDictionary *resp = [dict objectForKey:@"response"];
    NSString *trxID = [self createTrxID:[resp objectForKey:@"KodeReservasi"]];
    
    if([[resp objectForKey:@"status"] isEqualToString:@"00"]){
        self.labelContentHeader.text = header;
        self.labelContent.text = [NSString stringWithFormat:@"%@:\n%@\n\n%@%@\n\n%@",
                                  nomorPengajuan,
                                  [resp objectForKey:@"KodeReservasi"],
                                  dear,
                                  [resp objectForKey:@"namaNasabah"],
                                  content];
        self.labelContentFooter.text = footer;
        
        NSMutableDictionary *dictData = [[NSMutableDictionary alloc]init];
        
        [dictData setValue:trxID forKey:@"transaction_id"];
        [dictData setValue:[resp objectForKey:@"KodeReservasi"] forKey:@"trxref"];
        [dictData setValue:self.labelContentHeader.text forKey:@"title"];
        [dictData setValue:self.labelContent.text forKey:@"msg"];
        [dictData setValue:self.labelContentFooter.text forKey:@"footer_msg"];
        
        [dataManager.dataExtra setValue:trxID forKey:@"trxID"];
        
        [self inserData:dictData];
    }else if([[resp objectForKey:@"status"] isEqualToString:@"88"]){
        [dataManager.dataExtra setValue:@"YES" forKey:@"updateJamPickup"];
        
        NSMutableDictionary *dictData = [[NSMutableDictionary alloc]init];
        [dictData setValue:trxID forKey:@"transaction_id"];
        [dictData setValue:[resp objectForKey:@"KodeReservasi"] forKey:@"trxref"];
        [dictData setValue:self.labelContentHeader.text forKey:@"title"];
        [dictData setValue:self.labelContent.text forKey:@"msg"];
        [dictData setValue:self.labelContentFooter.text forKey:@"footer_msg"];
        
        [dataManager.dataExtra setValue:trxID forKey:@"trxID"];
        
        GESubmissionDataFormViewController *ges03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmissionDataForm"];
        [ges03 setupData];
        UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
        [currentVC pushViewController:ges03 animated:YES];
        
        [self inserData:dictData];
    }else{
        [Utility showMessage:@"Permintaan tidak dapat di proses" instance:self];
    }
}

@end
