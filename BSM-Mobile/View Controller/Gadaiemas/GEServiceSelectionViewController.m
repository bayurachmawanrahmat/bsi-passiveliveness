//
//  GEServiceSelectionViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEServiceSelectionViewController.h"
#import "GEBranchSelectionViewController.h"

#import "CustomBtn.h"
#import "Styles.h"
#import "Utility.h"

@interface GEServiceSelectionViewController (){
    NSUserDefaults *userDefaults;
    NSString *language;
    
    double minLimitAmountPickup;
    double maxLimitAmountPickup;
    double nilaiPengajuan;
    double pickupFee;
    NSString *radiusPickup;
    
    BOOL selected;
    
    BOOL dayValidation;
    BOOL timeValidation;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;

@property (weak, nonatomic) IBOutlet UIView *viewBank;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckedBank;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBank;
@property (weak, nonatomic) IBOutlet UILabel *labelDescBank;

@property (weak, nonatomic) IBOutlet UIView *viewPickup;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckedPickup;
@property (weak, nonatomic) IBOutlet UILabel *labelTitlePickup;
@property (weak, nonatomic) IBOutlet UILabel *labelDescPickup;

@property (weak, nonatomic) IBOutlet UILabel *labelServiceInformation;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@end

@implementation GEServiceSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:self.topConstant];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    minLimitAmountPickup = [[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"GadaiLimitMinAmountPickup"]doubleValue];
    maxLimitAmountPickup = [[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"GadaiLimitMaxAmountPickup"]doubleValue];
    pickupFee = [[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"ReservasiGadai_BiayaPickup"]doubleValue];
    nilaiPengajuan = [[dataManager.dataExtra objectForKey:@"nilaiPengajuanPembiayaan"]doubleValue];
    radiusPickup = [[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"ReservasiGadai_RadiusPickup"];
    
    selected = NO;
//    if(nilaiPengajuan > maxLimitAmountPickup){
//        [self.viewPickup setHidden:YES];
//    }
//    if(nilaiPengajuan < minLimitAmountPickup){
//        [self.viewPickup setHidden:YES];
//    }
    
    self.labelTitleBar.text = NSLocalizedString(@"GE_SUBMISSION_TITLE_BAR", @"");
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    self.labelTitleBank.text = NSLocalizedString(@"GE_GOTOBANK_TITLE", @"");
    self.labelDescBank.text = NSLocalizedString(@"GE_GOTOBANK_CONTENT", @"");
    
    self.labelTitlePickup.text = NSLocalizedString(@"GE_PICKUP_TITLE", @"");
//    self.labelDescPickup.text = NSLocalizedString(@"GE_PICKUP_CONTENT", @"");
    
    if([Utility isLanguageID]){
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"• Layanan pengambilan & penaksiran emas di tempat anda\n• Lokasi pickup maks %@ Kilometer\n• Biaya Pickup Rp. %@",radiusPickup, [Utility formatCurrencyValue:pickupFee]]];
        
        NSMutableParagraphStyle *paragraphStyle;
        paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [paragraphStyle setTabStops:@[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:15 options:nil]]];
        [paragraphStyle setDefaultTabInterval:15];
        [paragraphStyle setFirstLineHeadIndent:0];
        UIFont *font = [UIFont fontWithName:@"Helvetica" size:16];
        CGFloat indent = [Utility heightWrapWithText:@"• " fontName:font expectedSize:CGSizeMake(16, 16)].width;
        [paragraphStyle setHeadIndent:indent];

        [string addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0,[string length])];
        
        self.labelDescPickup.attributedText = string;
    }else{
        
        NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"• Gold Assessment service at your place\n• Maximum pickup location is %@ Kilometers\n• Pickup Fee Rp.%@",radiusPickup, [Utility formatCurrencyValue:pickupFee]]];
        
        NSMutableParagraphStyle *paragraphStyle;
        paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        [paragraphStyle setTabStops:@[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:15 options:nil]]];
        [paragraphStyle setDefaultTabInterval:15];
        [paragraphStyle setFirstLineHeadIndent:0];
        UIFont *font = [UIFont fontWithName:@"Helvetica" size:16];
        CGFloat indent = [Utility heightWrapWithText:@"• " fontName:font expectedSize:CGSizeMake(16, 16)].width;
        [paragraphStyle setHeadIndent:indent];

        [string addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0,[string length])];
        
        self.labelDescPickup.attributedText = string;
    }
    
    
    self.labelServiceInformation.text = NSLocalizedString(@"GE_TIME_SERVICE", @"");
    self.labelServiceInformation.textColor = UIColorFromRGB(const_color_primary);
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]init];
    [recognizer addTarget:self action:@selector(actionSelect:)];
    [recognizer setNumberOfTapsRequired:1];
    
    UITapGestureRecognizer *recognizerBnk = [[UITapGestureRecognizer alloc]init];
    [recognizerBnk addTarget:self action:@selector(actionSelectBank:)];
    [recognizerBnk setNumberOfTapsRequired:1];
    
    [self.viewBank setUserInteractionEnabled:YES];
    [self.viewBank.layer setBorderColor:[UIColorFromRGB(const_color_primary)CGColor]];
    [self.viewBank.layer setBorderWidth:1];
    [self.viewBank.layer setCornerRadius:10];
    [self.viewBank setTag:0];
    [self.viewBank addGestureRecognizer:recognizerBnk];
    
    [self.viewPickup setUserInteractionEnabled:YES];
    [self.viewPickup.layer setBorderColor:[UIColorFromRGB(const_color_primary)CGColor]];
    [self.viewPickup.layer setBorderWidth:1];
    [self.viewPickup.layer setCornerRadius:10];
    [self.viewPickup setTag:1];
    [self.viewPickup addGestureRecognizer:recognizer];
    
    [self.buttonNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    [self.buttonNext setEnabled:NO];
    [self.buttonCancel setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
    [self.buttonCancel addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
        
}
- (void) actionSelectBank : (UIRotationGestureRecognizer *)sender{
    [self.imgCheckedPickup setImage:[UIImage imageNamed:@"radio_unselect"]];
    [self.imgCheckedBank setImage:[UIImage imageNamed:@"radio_select"]];
    [dataManager.dataExtra setValue:@"DatangKeBank" forKey:@"typeLayanan"];
    selected = YES;
    [self.buttonNext setEnabled:YES];
}

- (void) actionSelect:(UITapGestureRecognizer*) sender{ //pickup
    bool isValid = true;
//    if(nilaiPengajuan > maxLimitAmountPickup){
//        [Utility showMessage:[NSString stringWithFormat:@"Nilai pengajuan pembiayaan anda meleibihi Rp. %@",[Utility formatCurrencyValue:maxLimitAmountPickup]] instance:self];
//        isValid = false;
//    }
    if(nilaiPengajuan > maxLimitAmountPickup){
        [Utility showMessage:[NSString stringWithFormat:@"Nilai pengajuan pembiayaan anda dibawah Rp. %@",[Utility formatCurrencyValue:maxLimitAmountPickup]] instance:self];
        isValid = false;
    }
    if(isValid){
        [self.imgCheckedPickup setImage:[UIImage imageNamed:@"radio_select"]];
        [self.imgCheckedBank setImage:[UIImage imageNamed:@"radio_unselect"]];
        [dataManager.dataExtra setValue:@"Pickup" forKey:@"typeLayanan"];
        selected = YES;
        [self.buttonNext setEnabled:YES];
    }
}

- (void) actionNext{
//    if(!selected){
//        [Utility showMessage:@"Please Pick One Service" instance:self];
//    }else{
    
    [self validateJam];
    
    if(dayValidation){
        if(timeValidation){
            GEBranchSelectionViewController *branc = [self.storyboard instantiateViewControllerWithIdentifier:@"BranchSelection"];
            
            UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
            [currentVC pushViewController:branc animated:YES];
        }else{
            NSString *alertMsgTime = @"";
            
            alertMsgTime = @"Mohon Maaf Jam Layanan Gadai Emas Tidak Tersedia di hari Sabtu dan Minggu serta Hari Libur Nasional. Silahkan Anda membuat pengajuan kembali di Hari Kerja Pada Pukul 08.00 - 15.00";
            
            alertMsgTime = @"Sorry, the service hour didn't available on Saturday, Sunday, and Public Holidays. You can try again in the next office hours.";
            
            if([[dataManager.dataExtra valueForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
                if([Utility isLanguageID]){
                    alertMsgTime = @"Mohon Maaf. Seluruh jam pick up yang Anda pilih telah penuh.  Anda dapat melakukan pengajuan kembali pada Hari Kerja berikutnya Pukul 08.00-15.00 WIB";
                }else{
                    alertMsgTime=@"Sorry, all the Pickup Time are full. You can try again in the next office hours";
                }
            }else{
                if([Utility isLanguageID]){
                    alertMsgTime = @"Mohon Maaf. Seluruh jam kedatangan yang Anda pilih telah penuh.  Anda dapat melakukan pengajuan kembali pada Hari Kerja berikutnya Pukul 08.00-15.00 WIB";
                }else{
                    alertMsgTime=@"Sorry, all the arrived Time are full. You can try again in the next office hours";
                }
            }
            [Utility showMessage:alertMsgTime instance:self];
        }
    }else{
    
        NSString *alertMsgTime = @"";
                
        if([Utility isLanguageID]){
            alertMsgTime = @"Mohon Maaf Jam Layanan Gadai Emas Tidak Tersedia di hari Sabtu dan Minggu serta Hari Libur Nasional. Silahkan Anda membuat pengajuan kembali di Hari Kerja Pada Pukul 08.00 - 15.00";
        }else{
            alertMsgTime = @"Sorry, the service hour didn't available on Saturday, Sunday, and Public Holidays. You can try again in the next office hours.";
        }
        [Utility showMessage:alertMsgTime instance:self];

    }
    
}

- (void) actionBack{
//    UINavigationController *navigationCont = self.navigationController;
//    [navigationCont popViewControllerAnimated:YES];
    [self backToRoot];
}

- (void) validateJam{
    NSString *cek1 = @"08:00-15:00";
    
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    NSString *timeNow = [outputFormatter stringFromDate:now];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger day = [components weekday];
    
    NSArray *timeArrCek1 = [cek1 componentsSeparatedByString:@"-"];
    NSString *lowTimeCek1 = [timeArrCek1[0] stringByAppendingString:@":00"];
    NSString *upTimeCek1 = [timeArrCek1[1] stringByAppendingString:@":00"];
    
    NSArray *arrNow = [timeNow componentsSeparatedByString:@":"];
    
    NSArray *arrLowCek1 = [lowTimeCek1 componentsSeparatedByString:@":"];
    NSArray *arrUpCek1 = [upTimeCek1 componentsSeparatedByString:@":"];
    
    
    int ttimeNow = ([arrNow[0] intValue] * 3600) + ([arrNow[1] intValue] * 60) + [arrNow[2] intValue];
    
    int lowtime1 = ([arrLowCek1[0] intValue] * 3600) + ([arrLowCek1[1] intValue] * 60) + [arrLowCek1[2] intValue];
    int uptime1 = ([arrUpCek1[0] intValue] * 3600) + ([arrUpCek1[1] intValue] * 60) + [arrUpCek1[2] intValue];
    
    if(day != 1 && day != 7){
        dayValidation = YES;
        if(ttimeNow > lowtime1 && ttimeNow < uptime1){
            timeValidation = YES;
        }else{
            timeValidation = NO;
        }
    }else{
        dayValidation = NO;
    }
}

@end
