//
//  GadaiSimulateCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 10/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GadaiSimulateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@end

NS_ASSUME_NONNULL_END
