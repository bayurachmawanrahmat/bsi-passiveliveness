//
//  GEPopupStatementViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface GEPopupStatementViewController : UIViewController{
    id delegate;
}

- (void) setDelegate : (id)newdelegate;
- (void) setData : (NSString*) title andContent : (NSString*) content;
- (void) setTagger : (int) tag;

@end

@protocol GEPopupSttmntDelegate

- (void) didAgreedStatment:(int) tagger;

@end

NS_ASSUME_NONNULL_END
