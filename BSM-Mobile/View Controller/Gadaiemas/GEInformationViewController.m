//
//  GEInformationViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEInformationViewController.h"
#import "GESimulationEstimate01ViewController.h"
#import "Styles.h"
#import "GadaiEmasConnect.h"
#import "Utility.h"

@interface GEInformationViewController ()<GadaiEmasConnectDelegate, ConnectionDelegate>{
    NSMutableDictionary *tncResult;
    
    NSString *langua;
    NSString *firstAccount;
    NSUserDefaults *userDefaults;
    DLAVAlertView *loadingAlert;
    
    int retry;
}

@end

@implementation GEInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    langua = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self setupTextview:@""];

    retry = 0;
    [self getTnc];
    
    [Utility isLanguageID] ? [self.lblTitle setText:@"Gadai Emas"] : [self.lblTitle setText:@"Gold Pawn"];
    

    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", @"") forState:UIControlStateNormal];
    [self.btnCancel addTarget:self action:@selector(actionPrev) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    
}

- (void) getTnc{
    NSString* param = [NSString stringWithFormat:@"gadai-emas/tnc?norek=%@",[dataManager.dataExtra objectForKey:@"norek"]];
    GadaiEmasConnect *connect = [[GadaiEmasConnect alloc]initWithDelegate:self];
    [connect getDataFromEndPoint:param needLoading:YES textLoading:@"" encrypted:NO];
}

- (void) setupTextview:(NSString*)info{
    NSAttributedString *attributedString = [[NSAttributedString alloc]
              initWithData: [info dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName:[UIFont fontWithName:const_font_name1 size:16] }
        documentAttributes: nil
                     error: nil
    ];

    self.textViewInfo.attributedText = attributedString;
    self.textViewInfo.font = [UIFont fontWithName:const_font_name1 size:15];
    self.textViewInfo.editable = NO;
}

- (void) actionNext{
    GESimulationEstimate01ViewController *geInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"SimulateEstimation01"];
    
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:geInfo animated:YES];
}

- (void) actionPrev{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        if(retry < 4){
            retry = retry + 1;
            [self getTnc];
        }else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ERR_REQUEST", @"") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        NSDictionary* data = [[dict objectForKey:@"response"]objectAtIndex:0];
        
        NSString *value = [data objectForKey:@"nilai"];
        if([Utility isLanguageID]){
            value = [data objectForKey:@"nilai"];
        }else{
            value = [data objectForKey:@"nilai_en"];
        }
        NSString *jangkawaktu = [[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"GadaiJangkaWaktu"];
        NSString *ftvperhiasan = [[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"GadaiFTVPerhiasan"];
        NSString *ftvkoinemas = [[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"GadaiFTVKoinEmas"];
        
        value = [value stringByReplacingOccurrencesOfString:@"${jangkawaktu}" withString:jangkawaktu];
        value = [value stringByReplacingOccurrencesOfString:@"${ftvperhiasan}" withString:ftvperhiasan];
        value = [value stringByReplacingOccurrencesOfString:@"${ftvkoinemas}" withString:ftvkoinemas];
        
        [self setupTextview:value];
        [dataManager.dataExtra setValue:[data objectForKey:@"cif"] forKey:@"cif"];
    }
}

@end
