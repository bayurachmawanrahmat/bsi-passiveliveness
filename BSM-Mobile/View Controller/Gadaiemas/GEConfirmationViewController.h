//
//  GEConfirmationViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"
#import "Styles.h"
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface GEConfirmationViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
//@property (weak, nonatomic) IBOutlet UILabel *labelConfirmation;
@property (weak, nonatomic) IBOutlet UIView *viewPickupFee;
@property (weak, nonatomic) IBOutlet UILabel *labelPickupFee;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonAgree;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonBack;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *collateralType;
@property (weak, nonatomic) IBOutlet UILabel *title1;
@property (weak, nonatomic) IBOutlet UILabel *title2;
@property (weak, nonatomic) IBOutlet UILabel *title3;
@property (weak, nonatomic) IBOutlet UILabel *title4;
@property (weak, nonatomic) IBOutlet UILabel *title5;
@property (weak, nonatomic) IBOutlet UILabel *title6;
@property (weak, nonatomic) IBOutlet UILabel *title7;
@property (weak, nonatomic) IBOutlet UILabel *title8;

@property (weak, nonatomic) IBOutlet UILabel *content1;
@property (weak, nonatomic) IBOutlet UILabel *content2;
@property (weak, nonatomic) IBOutlet UILabel *content3;
@property (weak, nonatomic) IBOutlet UILabel *content4;
@property (weak, nonatomic) IBOutlet UILabel *content5;
@property (weak, nonatomic) IBOutlet UILabel *content6;
@property (weak, nonatomic) IBOutlet UILabel *content7;
@property (weak, nonatomic) IBOutlet UILabel *content8;


@end

NS_ASSUME_NONNULL_END
