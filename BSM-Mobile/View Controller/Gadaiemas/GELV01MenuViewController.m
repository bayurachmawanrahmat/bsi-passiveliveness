//
//  LVGMenuViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 03/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GELV01MenuViewController.h"
#import "GELV02MenuViewController.h"
#import "PopUpLoginViewController.h"
#import "ListGoldMenuCell.h"
#import "Styles.h"

@interface GELV01MenuViewController ()<UITableViewDelegate, UITableViewDataSource, PopupLoginDelegate>{
    NSArray *listData;
    UIStoryboard *storyboard;
    NSUserDefaults *userDefaults;

}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;

@end

@implementation GELV01MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self.titleBar setText:@"e-mas"];
    
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [dataManager resetObjectData];
    storyboard = [UIStoryboard storyboardWithName:@"Gadaiemas" bundle:nil];
    
    listData = [[NSArray alloc]init];
    listData = @[
         @{@"title": @"e-mas", @"code": @1},
         @{@"title": @"Gadai Emas", @"code": @2},
//         @{@"title": @"Cicil Emas", @"code": @3}
    ];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"ListGoldMenuCell" bundle:nil] forCellReuseIdentifier:@"LVGOLDMENU"];
    
    [self getFirstListAccount];
    
    self.tableView.tableFooterView = [[UIView alloc]init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ListGoldMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LVGOLDMENU"];
    
    cell.labelText.text = [listData[indexPath.row] objectForKey:@"title"];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[listData[indexPath.row] valueForKey:@"code"] isEqual:@1]){
        NSString *strURL = [NSString stringWithFormat:@"request_type=gold_info,customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
    }
    
    if([[listData[indexPath.row] valueForKey:@"code"] isEqual:@2]){
        GELV02MenuViewController *lgv = [storyboard instantiateViewControllerWithIdentifier:@"GELV02"];

        NSArray *arr = @[
            @{@"title_id": @"Gadai Emas Fisik", @"title_en":@"Gold Physical Pawn", @"code": @1},
            @{@"title_id": @"Top Up & Portofolio Gadai Emas", @"title_en":@"Gold Pawn Top Up & Portofolio", @"code": @2}
        ];

        [lgv setData: arr];
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:lgv animated:YES];
    }
    
    if([[listData[indexPath.row] valueForKey:@"code"] isEqual:@3]){
        //goto cicil
    }
}

- (void) getFirstListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_ALL_ACCT];
        
    if(listAcct.count != 0 || listAcct != nil){
        NSString *firstAccount = [listAcct[0] objectForKey:@"id_account"];
        [dataManager.dataExtra setValue:firstAccount forKey:@"norek"];
    }else{
        
        NSString *strURL = [NSString stringWithFormat:@"request_type=list_account2, customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strURL needLoading:YES encrypted:true banking:true favorite:false];
    }
    
    NSString *msiddn = [NSUserdefaultsAes getValueForKey:@"msisdn"];
    
    if(msiddn == nil || [msiddn isEqualToString:@""]){
        [self getMSISDN];
    }else if([userDefaults objectForKey:@"mobilenumber"] == nil || [[userDefaults valueForKey:@"mobilenumber"] isEqualToString:@""]){
        [self getMSISDN];
    }
}

-(void) getMSISDN{
    NSString *strUrl = [NSString stringWithFormat:@"request_type=get_msisdn,customer_id"];

    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}


- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getFirstListAccount];

        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
    
    if([requestType isEqualToString:@"get_msisdn"]){
        NSDictionary *dict  = [NSJSONSerialization
        JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding]
        options:NSJSONReadingAllowFragments
                               error:nil];
        
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            NSString *msisdn = [dict objectForKey:@"msisdn"];
//            [userDefaults setValue:msisdn forKey:@"msisdn"];
//            [userDefaults setValue:msisdn forKey:@"mobilenumber"];
            [NSUserdefaultsAes setObject:msisdn forKey:@"msisdn"];
        }
    }
    
    if([requestType isEqualToString:@"gold_info"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            [self openTemplateByMenuIDWithLogin:@"00170"];
        }else if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"14"]){
            [self gotoGold];
        }else{

            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];

        }
    }
}

@end
