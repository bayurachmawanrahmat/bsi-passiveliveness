//
//  GEInformationViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface GEInformationViewController : TemplateViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UITextView *textViewInfo;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@end

NS_ASSUME_NONNULL_END
