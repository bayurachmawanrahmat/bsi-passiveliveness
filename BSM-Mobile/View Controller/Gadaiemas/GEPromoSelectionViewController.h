//
//  GEPromoSelectionViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GEPromoSelectionViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;
- (void) setDataPromo:(NSArray*)dataPromo;

@end

@protocol PromoSelectionDelegate

- (void) selectedPromo:(NSDictionary*) promo;

@end

NS_ASSUME_NONNULL_END
