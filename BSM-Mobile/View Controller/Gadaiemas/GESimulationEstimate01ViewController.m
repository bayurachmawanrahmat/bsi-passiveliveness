//
//  GESimulationEstimateViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GESimulationEstimate01ViewController.h"
#import "GESimulationEstimate02ViewController.h"
#import "GESimulateDetailViewController.h"
#import "GadaiSimulateCell.h"
#import "Styles.h"


@interface GESimulationEstimate01ViewController ()<UITableViewDataSource, UITableViewDelegate, SimulateDetailDelegate>{
    NSMutableArray *listSimulateData;
}

@end

@implementation GESimulationEstimate01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Styles setTopConstant:self.topConstant];
    
    listSimulateData = [[NSMutableArray alloc]init];
    
    self.titleBar.text = NSLocalizedString(@"GE_ESTIMATED_SIMULATION_TITLE", @"");
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    
    [self.lblTitleSimulate setText:NSLocalizedString(@"GE_INPUT_GOLD_DATA_TITLE", "")];
    
    [self.tableDataSimulate setDelegate:self];
    [self.tableDataSimulate setDataSource:self];
    [self.tableDataSimulate registerNib:[UINib nibWithNibName:@"GadaiSimulateCell" bundle:nil] forCellReuseIdentifier:@"GESimulateCell"];
    [self.tableDataSimulate setTableFooterView:[[UIView alloc]init]];
    [self.tableDataSimulate setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    [self.btnPrev addTarget:self action:@selector(actionPrev) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.btnPrev setColorSet:SECONDARYCOLORSET];
    
    [self.btnPrev setTitle:NSLocalizedString(@"BACK", @"") forState:UIControlStateNormal];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    
//    GadaiEmasConnect *connect = [[GadaiEmasConnect alloc]initWithDelegate:self];
//    [connect getDataFromEndPoint:@"InitParamGadaiReservasi" needLoading:YES textLoading:@"" encrypted:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listSimulateData.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GadaiSimulateCell *cell = (GadaiSimulateCell*)[tableView dequeueReusableCellWithIdentifier:@"GESimulateCell"];
    
    if(indexPath.row == listSimulateData.count){
        [cell.labelTitle setText:NSLocalizedString(@"GE_GOLD_DATA_TITLE", @"")];
        [cell.btnClose setImage:[UIImage imageNamed:@"baseline_plus_black_24pt"] forState:UIControlStateNormal];
        [cell.btnClose setTag:indexPath.row];
        [cell.btnClose removeTarget:self action:@selector(deleteData:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnClose addTarget:self action:@selector(AddData:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit setHidden:YES];
        
    }else{
        [cell.labelTitle setText:[listSimulateData[indexPath.row] objectForKey:@"ObjectGadai"]]; //baseline_close_black_24pt
        [cell.btnClose setImage:[UIImage imageNamed:@"baseline_close_black_24pt"] forState:UIControlStateNormal];
        [cell.btnClose setTag:indexPath.row];
        [cell.btnClose removeTarget:self action:@selector(AddData:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnClose addTarget:self action:@selector(deleteData:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit setHidden:NO];
        [cell.btnEdit addTarget:self action:@selector(editData:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnEdit setTag:indexPath.row];

    }
    [cell.vwContent.layer setBorderColor:[[UIColor blackColor]CGColor]];
    [cell.vwContent.layer setBorderWidth:1];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void) AddData:(id)sender{
    if(listSimulateData.count == 0){
        GESimulateDetailViewController *simulateDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"SimulateDetail"];
        [simulateDetail setDelegate:self];
        [self presentViewController:simulateDetail animated:YES completion:nil];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ALERT_ADD_OBJ", "") preferredStyle:UIAlertControllerStyleAlert];
        
        
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"NOPE", "") style:UIAlertActionStyleDefault handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"YES", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            GESimulateDetailViewController *simulateDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"SimulateDetail"];
            [simulateDetail setDelegate:self];
            [self presentViewController:simulateDetail animated:YES completion:nil];
        }]];
        
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (void) deleteData:(id)sender{
    UIButton *btn = (UIButton*) sender;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ALERT_DELETE_OBJ", "") preferredStyle:UIAlertControllerStyleAlert];

    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"NOPE", "") style:UIAlertActionStyleDefault handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"YES", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self->listSimulateData removeObjectAtIndex:btn.tag];
        [self.tableDataSimulate reloadData];
    }]];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) editData:(id)sender{
    UIButton *btn = (UIButton*) sender;
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ALERT_CHANGE_OBJ", "") preferredStyle:UIAlertControllerStyleAlert];
   
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"NOPE", "") style:UIAlertActionStyleDefault handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"YES", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        GESimulateDetailViewController *simulateDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"SimulateDetail"];
        [simulateDetail setDelegate:self];
        [simulateDetail setupEditData:self->listSimulateData[btn.tag] withIndex:btn.tag];
        [self presentViewController:simulateDetail animated:YES completion:nil];
    }]];
        
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) actionPrev{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionNext{
    
    if(listSimulateData.count > 0){
        
        [dataManager.dataExtra setValue:listSimulateData forKey:@"jsonListObjectGadai"];

        GESimulationEstimate02ViewController *ges = [self.storyboard instantiateViewControllerWithIdentifier:@"SimulateEstimation02"];

        UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
        [currentVC pushViewController:ges animated:YES];
        
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Belum ada data yang di isi" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)updateData:(NSDictionary *)dict withState:(int)state andIndex:(NSInteger)index0{
     if(state == 0){
        [listSimulateData addObject:dict];
    }else{
        [listSimulateData replaceObjectAtIndex:index0 withObject:dict];
    }
    [self.tableDataSimulate reloadData];
}

//- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
//    if([endPoint isEqualToString:@"InitParamGadaiReservasi"]){
//        if(error != nil){
//            NSLog(@"%@",[error localizedDescription]);
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ERR_REQUEST", @"") preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                [self backToR];
//            }]];
//            [self presentViewController:alert animated:YES completion:nil];
//        }else{
//            NSLog(@"%@",dict);
//            NSDictionary* data = [dict objectForKey:@"response"];
//            NSUserDefaults *userd = [NSUserDefaults standardUserDefaults];
//            [userd setValue:data forKey:@"reservasi_gadai"];
//        }
//    }
//}


@end
