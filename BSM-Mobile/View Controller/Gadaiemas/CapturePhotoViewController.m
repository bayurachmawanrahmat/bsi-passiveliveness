//
//  CapturePhotoViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 05/11/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CapturePhotoViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface CapturePhotoViewController ()<AVCapturePhotoCaptureDelegate>{
    AVCaptureSession *session;
    AVCaptureDevice *device;
    AVCaptureDeviceInput *input;
    AVCapturePhoto *photo;
    AVCapturePhotoOutput *photoOutput;
    AVCaptureMetadataOutput *output;
    AVCaptureVideoPreviewLayer *previewLayer;
    AVCaptureDeviceDiscoverySession *deviceSession;
}
@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (nonatomic) AVCaptureDevice *backCamera;
@property (nonatomic) AVCaptureDeviceInput *backCameraInput;
@property (nonatomic) AVCaptureDevice *teleCamera;
@property (nonatomic) AVCaptureDeviceInput *teleCameraInput;
@end

@implementation CapturePhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSError *error = nil;
    session = [[AVCaptureSession alloc]init];
    device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    photoOutput = [[AVCapturePhotoOutput alloc] init];
            
    
    if([AVCaptureDeviceDiscoverySession class]){
        NSArray *allTypes = @[AVCaptureDeviceTypeBuiltInDualCamera, AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInTelephotoCamera ];
        AVCaptureDeviceDiscoverySession *discoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:allTypes mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront];

        for(AVCaptureDevice *device in discoverySession.devices) {
            if(device.deviceType==AVCaptureDeviceTypeBuiltInWideAngleCamera){
                self.backCamera = device;
                self.backCameraInput = [AVCaptureDeviceInput deviceInputWithDevice:self.backCamera error:&error];
            }

            if(device.deviceType==AVCaptureDeviceTypeBuiltInTelephotoCamera){
                self.teleCamera = device;
                self.teleCameraInput = [AVCaptureDeviceInput deviceInputWithDevice:self.teleCamera error:&error];
            }
        }
    }

    if(!self.backCamera){
        self.backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        self.backCameraInput = [AVCaptureDeviceInput deviceInputWithDevice:self.backCamera error:&error];
    }

    if(_backCameraInput){
        [session addInput:_backCameraInput];
    }else{
        NSLog(@"Error : %@", error);
    }
    
    if(error == nil){
        
        previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        previewLayer.frame = self.viewPreview.frame;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.bounds = self.viewPreview.bounds;
        
        [self.viewPreview.layer addSublayer:previewLayer];
        
        

        if([session canAddOutput:photoOutput]){
            [session addOutput:photoOutput];
        }
        
        [session startRunning];
        
    }
}

-(IBAction) capturePhoto:(id)sender{
//    sessionQueue.async {
//            self.photoOutput.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
//        }
    AVCapturePhotoSettings *photoSettings =
    [AVCapturePhotoSettings photoSettingsWithFormat:@{AVVideoCodecKey : AVVideoCodecTypeJPEG}];
    [photoOutput capturePhotoWithSettings:photoSettings delegate:self];
    [session addOutput:photoOutput];
}

- (void)captureOutput:(AVCapturePhotoOutput *)output willBeginCaptureForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings{
    
}

- (void)captureOutput:(AVCapturePhotoOutput *)output willCapturePhotoForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings{
    
    previewLayer.opacity = 0;
    [UIView animateWithDuration:0.25 animations:^(){
        self->previewLayer.opacity = 1;
    }];
}

- (void)captureOutput:(AVCapturePhotoOutput *)output didCapturePhotoForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings{
    
}

- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishCaptureForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings error:(NSError *)error{
    
}

@end
