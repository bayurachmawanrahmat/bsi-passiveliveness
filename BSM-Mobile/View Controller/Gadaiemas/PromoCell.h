//
//  PromoCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PromoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *buttonUse;
@property (weak, nonatomic) IBOutlet UILabel *labelPromo;
@property (weak, nonatomic) IBOutlet UIView *viewBox;

@end

NS_ASSUME_NONNULL_END
