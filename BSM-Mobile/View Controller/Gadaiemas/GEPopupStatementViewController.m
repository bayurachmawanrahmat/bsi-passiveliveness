//
//  GEPopupStatementViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEPopupStatementViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "CustomBtn.h"

@interface GEPopupStatementViewController (){
    int tagger;
    NSString *stringTitle;
    NSString *stringContent;
}
@property (weak, nonatomic) IBOutlet UIView *viewFrme;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet CustomBtn *btnAgreed;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;

@end

@implementation GEPopupStatementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(@available(iOS 13.0,*)){
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }

    self.viewFrme.layer.cornerRadius = 10;
    self.viewFrme.layer.borderWidth = 1;
    self.viewFrme.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    self.viewFrme.clipsToBounds = YES;
    
    [self.btnBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAgreed addTarget:self action:@selector(actionAgree) forControlEvents:UIControlEventTouchUpInside];
    
//    if([Utility isLanguageID]){
        [self.btnAgreed setTitle:@"Saya Setuju" forState:UIControlStateNormal];
//    }
//    else{
//        [self.btnAgreed setTitle:@"I Agree" forState:UIControlStateNormal];
//    }
    
//    [Styles setStyleButton:self.btnBack];
//    [Styles setStyleButton:self.btnAgreed];
    
    self.labelTitle.text = stringTitle;
    self.labelContent.text = stringContent;
}

- (void)setData:(NSString *)title andContent:(NSString *)content{
    stringTitle = title;
    stringContent = content;
}

- (void)setDelegate:(id)newdelegate{
    delegate = newdelegate;
}

- (void)setTagger:(int)tag{
    tagger = tag;
}

- (void) actionBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) actionAgree{
    [delegate didAgreedStatment:tagger];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
