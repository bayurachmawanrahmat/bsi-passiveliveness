//
//  GESubmissionDataFormViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GESubmissionDataFormViewController.h"
#import "GEPickupAddressFormViewController.h"
#import "GCamOverlayView.h"
#import "CamOverlayController.h"
#import "Styles.h"
#import "GadaiEmasConnect.h"
#import "Utility.h"

@interface GESubmissionDataFormViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, GCamOverlayDelegate, GadaiEmasConnectDelegate>{
    NSArray *listPickupTime;
    
    NSUserDefaults *userDefaults;
    
    UIDatePicker *pickupDate;
    
    UIImagePickerController *imagePicker;
    
    int camState;
    BOOL checkState;
    
    BOOL state88;
    NSString *alertMsgTime;
    NSString *language;
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleBranch;
@property (weak, nonatomic) IBOutlet UITextField *tfBranch;

@property (weak, nonatomic) IBOutlet UILabel *labelTitlePickupDate;
@property (weak, nonatomic) IBOutlet UITextField *tfPickupDate;

@property (weak, nonatomic) IBOutlet UILabel *labelTitlePikcupTime;
@property (weak, nonatomic) IBOutlet UITextField *tfPickupTime;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

@property (weak, nonatomic) IBOutlet UILabel *labelEmailDesc;

@property (weak, nonatomic) IBOutlet UIView *viewKTPBox;
@property (weak, nonatomic) IBOutlet UILabel *labelKtp;
@property (weak, nonatomic) IBOutlet UILabel *labelGoldPhoto;
@property (weak, nonatomic) IBOutlet UIView *viewGoldPhotoBox;
@property (weak, nonatomic) IBOutlet UIView *viewSelfnKtpBox;
@property (weak, nonatomic) IBOutlet UILabel *labelSelfnKtp;

@property (weak, nonatomic) IBOutlet UILabel *labelStatement;
@property (weak, nonatomic) IBOutlet UIImageView *imageCheckedStatement;
@property (weak, nonatomic) IBOutlet UIView *viewStatementBox;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonBack;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;
@property (weak, nonatomic) IBOutlet UIImageView *imgSelfKTP;
@property (weak, nonatomic) IBOutlet UIImageView *imgGold;
@property (weak, nonatomic) IBOutlet UIImageView *imgKtp;

@end

@implementation GESubmissionDataFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    listPickupTime = @[@"09:00 - 10:00",
                       @"10:00 - 11:00",
                       @"11:00 - 12:00",
                       @"13:00 - 14:00",
                       @"14:00 - 15:00"];
    
    if([Utility isLanguageID]){
        alertMsgTime = @"Mohon Maaf. Seluruh jam pick up yang Anda pilih telah penuh.  Anda dapat melakukan pengajuan kembali pada Hari Kerja berikutnya Pukul 08.00-15.00 WIB";
    }else{
        alertMsgTime=@"Sorry, all the Pickup Time are full. You can try again in the next office hours";
    }
    
    if(state88){
        [Utility showMessage:alertMsgTime instance:self];
        if([dataManager.dataExtra objectForKey:@"fotoKTP"]){
            self.imgKtp.image = [self decodeBase64ToImage:[dataManager.dataExtra objectForKey:@"fotoKTP"]];
        }
        if([dataManager.dataExtra objectForKey:@"fotoDiriKTP"]){
            self.imgSelfKTP.image = [self decodeBase64ToImage:[dataManager.dataExtra objectForKey:@"fotoDiriKTP"]];
        }
        if([dataManager.dataExtra objectForKey:@"fotoEmas"]){
            self.imgGold.image = [self decodeBase64ToImage:[dataManager.dataExtra objectForKey:@"fotoEmas"]];
        }
        if([dataManager.dataExtra objectForKey:@"namaCabang"]){
            self.tfBranch.text = [dataManager.dataExtra objectForKey:@"namaCabang"];
        }
        if([dataManager.dataExtra objectForKey:@"email"]){
            self.tfEmail.text = [dataManager.dataExtra objectForKey:@"email"];
        }
    }

    [self createToolbarButton:self.tfBranch];
    [self createToolbarButton:self.tfPickupTime];
    [self createPicker:self.tfPickupTime withTag:1];
    
//    [self createToolbarButton:self.tfPickupDate];
    self.tfPickupDate.enabled = NO;
    [self setDate];
    [self createToolbarButton:self.tfEmail];
//    [self createPickupDatePicker];
    
    self.titleBar.text = lang(@"GE_SUBMISSION_TITLE_BAR");
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    self.labelTitleBranch.text = lang(@"GE_BRANCH");
    
    self.labelEmailDesc.text = lang(@"GE_EMAIL_MESSAGE");
//    self.labelStatement.text = lang(@"GE_FOTO_STATMNT_PICKUP");
    self.labelStatement.text = @"";

    [self.tfBranch setEnabled:NO];
    self.tfBranch.text = [dataManager.dataExtra valueForKey:@"namaCabang"];
    
    UITapGestureRecognizer *tapBoxKTP = [[UITapGestureRecognizer alloc]init];
    [tapBoxKTP addTarget:self action:@selector(actionTakePhoto:)];
    [self.viewKTPBox setUserInteractionEnabled:YES];
    [self.viewKTPBox addGestureRecognizer:tapBoxKTP];
    [self.labelKtp setText:NSLocalizedString(@"GE_TAKE_ID_PICTURE", @"")];

    UITapGestureRecognizer *tapBoxSelfKTP = [[UITapGestureRecognizer alloc]init];
    [tapBoxSelfKTP addTarget:self action:@selector(actionTakePhoto:)];
    [self.viewSelfnKtpBox setUserInteractionEnabled:YES];
    [self.viewSelfnKtpBox addGestureRecognizer:tapBoxSelfKTP];
    [self.labelSelfnKtp setText:NSLocalizedString(@"GE_TAKE_SELF_ID_PICTURE", @"")];

    UITapGestureRecognizer *tapBoxGold = [[UITapGestureRecognizer alloc]init];
    [tapBoxGold addTarget:self action:@selector(actionTakePhoto:)];
    [self.viewGoldPhotoBox setUserInteractionEnabled:YES];
    [self.viewGoldPhotoBox addGestureRecognizer:tapBoxGold];
    [self.labelGoldPhoto setText:NSLocalizedString(@"GE_TAKE_GOLD_PICTURE", @"")];
    
    if([[dataManager.dataExtra valueForKey:@"typeLayanan"] isEqualToString:@"Pickup"]){
        [self.viewGoldPhotoBox setHidden:NO];
        self.labelTitlePickupDate.text = lang(@"GE_PICKUP_DATE");
        self.labelTitlePikcupTime.text = lang(@"GE_PICKUP_TIME");
        self.labelStatement.text = lang(@"GE_FOTO_STATMNT_PICKUP");
    }else{
        [self.viewGoldPhotoBox setHidden:YES];
        self.labelTitlePickupDate.text = lang(@"GE_ARRIVAL_DATE");
        self.labelTitlePikcupTime.text = lang(@"GE_ARRIVAL_TIME");
        self.labelStatement.text = lang(@"GE_FOTO_STATMNT_BANK");

    }
    
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    
    [self.buttonBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBack setTitle:NSLocalizedString(@"BACK", @"") forState:UIControlStateNormal];
    [self.buttonBack setColorSet:SECONDARYCOLORSET];
    [self.buttonNext setEnabled:NO];

    
    [self.imageCheckedStatement setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
    checkState = NO;
    [self.imageCheckedStatement addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionChecked)]];
    [self.imageCheckedStatement setUserInteractionEnabled:YES];

    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString* selectedDate = [dateFormat stringFromDate:[NSDate date]];
    if([[dataManager.dataExtra objectForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
        [self getListPickupTime:selectedDate andTime:@""];
    }
}

- (void)getListPickupTime : (NSString *)date andTime:(NSString*)time{
    GadaiEmasConnect *connect = [[GadaiEmasConnect alloc]initWithDelegate:self];
    
    NSString *stringURL = [NSString stringWithFormat:@"checkSlotPickUp?KodeReservasi=%@&typeLayanan=%@&kodeCabang=%@&tanggalPickup=%@&jamPickup=%@",
                           [dataManager.dataExtra objectForKey:@"KodeReservasi"],
                           [dataManager.dataExtra objectForKey:@"typeLayanan"],
                           [dataManager.dataExtra objectForKey:@"kodeCabang"],
                           date,
                           time];
    
    [connect getDataFromEndPoint:stringURL needLoading:YES textLoading:@"" encrypted:NO];
}

- (void) actionBack{
    if(!state88){
        UINavigationController *navigationCont = self.navigationController;
        [navigationCont popViewControllerAnimated:YES];
    }
}

- (void) actionChecked{
    if(checkState){
        [self.imageCheckedStatement setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        checkState = NO;
        [self.buttonNext setEnabled:NO];
    }else{
        [self.imageCheckedStatement setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        checkState = YES;
        if([self isValid]){
            [self.buttonNext setEnabled:YES];
        }else{
            [Utility showMessage:@"Lengkapi dulu data di atas" instance:self];
            [self actionChecked];
        }
    }
}

- (void)setDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString* selectedDate = [dateFormat stringFromDate:[NSDate date]];
    self.tfPickupDate.text = selectedDate;
    [dataManager.dataExtra setValue:selectedDate forKey:@"tanggalPickup"];
    [dataManager.dataExtra setValue:selectedDate forKey:@"tanggalkedatangan"];
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void) doneAction{
    [self.view endEditing:YES];
}


#pragma mark PickerView
- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listPickupTime.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return listPickupTime[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if([self validateJam:listPickupTime[row]]){
        self.tfPickupTime.text = listPickupTime[row];

        [dataManager.dataExtra setValue:listPickupTime[row] forKey:@"jamPickup"];
        [dataManager.dataExtra setValue:listPickupTime[row] forKeyPath:@"jamkedatangan"];
    }else{
        [Utility showMessage:alertMsgTime instance:self];
    }
}

- (BOOL)isValid{
    BOOL valid = YES;
    
    if([_tfPickupTime.text isEqualToString:@""]){
        if([[dataManager.dataExtra objectForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
            
            [Utility showMessage:NSLocalizedString(@"GE_PICKUP_TIME_EMPTY", @"") instance:self];
        }else{
            if([Utility isLanguageID]){
                [Utility showMessage:@"Pilih jam kedatangan" instance:self];
            }else{
                [Utility showMessage:@"Select Arrived time" instance:self];
            }
        }
        return valid = NO;
    }
    
    if(![_tfEmail.text isEqualToString:@""]){
        if(![Utility validateEmailWithString:_tfEmail.text]){
            [Utility showMessage:NSLocalizedString(@"INVALID_EMAIL", @"") instance:self];
            return valid = NO;
        }
    }else{
        [Utility showMessage:NSLocalizedString(@"EMAIL_NOT_EMPTY", @"") instance:self];
        return valid = NO;
    }
    
    if([[dataManager.dataExtra objectForKey:@"fotoKTP"]isEqualToString:@""] ||
       [dataManager.dataExtra objectForKey:@"fotoKTP"] == nil ||
       self.imgKtp.image == [UIImage imageNamed:@"camera"]){
        [Utility showMessage:NSLocalizedString(@"GE_TAKE_ID_PICTURE", @"") instance:self];
        return valid = NO;
    }
    
    if([[dataManager.dataExtra objectForKey:@"fotoDiriKTP"]isEqualToString:@""] ||
       [dataManager.dataExtra objectForKey:@"fotoDiriKTP"] == nil ||
       self.imgSelfKTP.image == [UIImage imageNamed:@"camera"]){
        [Utility showMessage:NSLocalizedString(@"GE_TAKE_SELF_ID_PICTURE", @"") instance:self];
        return valid = NO;
    }
    
    if([[dataManager.dataExtra valueForKey:@"typeLayanan"] isEqualToString:@"Pickup"]){
        if([[dataManager.dataExtra objectForKey:@"fotoEmas"]isEqualToString:@""] ||
           [dataManager.dataExtra objectForKey:@"fotoEmas"] == nil||
           self.imgGold.image == [UIImage imageNamed:@"camera"]){
            [Utility showMessage:NSLocalizedString(@"GE_TAKE_GOLD_PICTURE", @"") instance:self];
            return valid = NO;
        }
    }
    
    return valid;
}

- (void) actionNext{
    
    [dataManager.dataExtra setValue:self.tfPickupDate.text forKey:@"tanggalPickup"];
    [dataManager.dataExtra setValue:self.tfPickupTime.text forKey:@"jamPickup"];
    [dataManager.dataExtra setValue:_tfEmail.text forKey:@"email"];

    if([self isValid]){
        if(state88){
            TemplateViewController *ges = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmissionConfirmation"];
            
            UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
            [currentVC pushViewController:ges animated:YES];
        }else{
            TemplateViewController *ges = [self.storyboard instantiateViewControllerWithIdentifier:@"StatementForm"];
            
            UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
            [currentVC pushViewController:ges animated:YES];
        }
    }
}

#pragma mark CameraHandler

- (void) actionTakePhoto : (UITapGestureRecognizer*) sender{
    if(!state88){
        NSString *descriptiionLabel = @"";
        NSString *imageName = @"";
        NSString *ttileLabel = @"";
        if(sender.view == self.viewKTPBox){
            camState = 0;
            descriptiionLabel = NSLocalizedString(@"GE_TAKE_ID_PICTURE", @"");
            imageName = @"img_shape_ktp@2x.png";
        }else if(sender.view == self.viewGoldPhotoBox){
            camState = 1;
            descriptiionLabel = NSLocalizedString(@"GE_TAKE_GOLD_PICTURE", @"");
            imageName = @"img_shape_ktp@2x.png";
        }else{
            camState = 2;
            descriptiionLabel = NSLocalizedString(@"GE_TAKE_SELF_ID_PICTURE", @"");
            imageName = @"img_shape_selfie.png";
        }
        ttileLabel = NSLocalizedString(@"TAKE_PHOTO", @"");

        CamOverlayController *overlayController = [[CamOverlayController alloc]init];
        
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.accessibilityLabel = @"";
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate = self;
        imagePicker.showsCameraControls = NO;
        imagePicker.toolbarHidden = YES;
    //    imagePicker.allowsEditing = NO;
        
    //    float cameraAspectRatio = 6.0 / 4.0; //! Note: 4.0 and 4.0 works
    //    float imageWidth = floorf(SCREEN_WIDTH * cameraAspectRatio);
    //    float scale = ceilf((SCREEN_HEIGHT / imageWidth) * 10.0) / 10.0;

    //    imagePicker.cameraViewTransform = CGAffineTransformMakeScale(cameraAspectRatio, cameraAspectRatio);

        GCamOverlayView *cameraView = (GCamOverlayView *)overlayController.view;
        cameraView.frame = imagePicker.view.frame;
        cameraView.descriptionLabel.text = descriptiionLabel;
        cameraView.imageOverlay.image = [UIImage imageNamed:imageName];
        cameraView.titleLabel.text = ttileLabel;
        cameraView.delegate = self;
        
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
        imagePicker.cameraOverlayView = cameraView;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)didTakePhoto:(GCamOverlayView *)overlayView{
    [imagePicker takePicture];
}

- (void) switchCameraDevice:(GCamOverlayView *)overlayView{
    if(imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceRear)
    {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }else{
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}

- (void)didBack:(GCamOverlayView *)overlayView{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)percentEscapeString:(NSString *)string
{
    NSString *result = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                 (CFStringRef)string,
                                                                                 (CFStringRef)@" ",
                                                                                 (CFStringRef)@":/?@!$&'()*+,;=",
                                                                                 kCFStringEncodingUTF8));
    return [result stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *base64Imge = [Utility compressAndEncodeToBase64String:image withMaxSizeInKB:40];
//    NSString *base64Imge = [Utility compressandencodebase64string:image withMaxSizeInKB:40];
    
    base64Imge = [[base64Imge stringByReplacingOccurrencesOfString:@"\r" withString:@""] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    base64Imge = [[[base64Imge stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"] stringByReplacingOccurrencesOfString:@"/" withString:@"%2f"] stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    
    if(camState == 0){
        [dataManager.dataExtra setValue:base64Imge forKey:@"fotoKTP"];
        [self.imgKtp setImage:image];
        [self.labelKtp setText:NSLocalizedString(@"GE_RETAKE_ID_PICTURE", @"")];
    }else if(camState == 1){
        [dataManager.dataExtra setValue:base64Imge forKey:@"fotoEmas"];
        [self.imgGold setImage:image];
        [self.labelGoldPhoto setText:NSLocalizedString(@"GE_RETAKE_GOLD_PICTURE", @"")];
    }else{
        [dataManager.dataExtra setValue:base64Imge forKey:@"fotoDiriKTP"];
        [dataManager.dataExtra setValue:base64Imge forKey:@"fotoDiri"];
        [self.imgSelfKTP setImage:image];
        [self.labelSelfnKtp setText:NSLocalizedString(@"GE_RETAKE_SELF_ID_PICTURE", @"")];
    }
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    
}

- (BOOL) validateJam : (NSString*)cek2{
    NSString *cek1 = @"08:00-15:00";
    
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    NSString *timeNow = [outputFormatter stringFromDate:now];
    
    NSArray *timeArrCek1 = [cek1 componentsSeparatedByString:@"-"];
    NSString *lowTimeCek1 = [timeArrCek1[0] stringByAppendingString:@":00"];
    NSString *upTimeCek1 = [timeArrCek1[1] stringByAppendingString:@":00"];
    
    NSArray *timeArrCek2 = [cek2 componentsSeparatedByString:@"-"];
    NSString *lowTimeCek2 = [timeArrCek2[0] stringByAppendingString:@":00"];
    NSString *upTimeCek2 = [timeArrCek2[1] stringByAppendingString:@":00"];
    
    NSArray *arrNow = [timeNow componentsSeparatedByString:@":"];
    
    NSArray *arrLowCek1 = [lowTimeCek1 componentsSeparatedByString:@":"];
    NSArray *arrUpCek1 = [upTimeCek1 componentsSeparatedByString:@":"];
    
    NSArray *arrLowCek2 = [lowTimeCek2 componentsSeparatedByString:@":"];
    NSArray *arrUpCek2 = [upTimeCek2 componentsSeparatedByString:@":"];
    
    int ttimeNow = ([arrNow[0] intValue] * 3600) + ([arrNow[1] intValue] * 60) + [arrNow[2] intValue];
    
    int lowtime1 = ([arrLowCek1[0] intValue] * 3600) + ([arrLowCek1[1] intValue] * 60) + [arrLowCek1[2] intValue];
    int uptime1 = ([arrUpCek1[0] intValue] * 3600) + ([arrUpCek1[1] intValue] * 60) + [arrUpCek1[2] intValue];
    
    int lowtime2 = ([arrLowCek2[0] intValue] * 3600) + ([arrLowCek2[1] intValue] * 60) + [arrLowCek2[2] intValue];
    int uptime2 = ([arrUpCek2[0] intValue] * 3600) + ([arrUpCek2[1] intValue] * 60) + [arrUpCek2[2] intValue];

    
//    if(ttimeNow > lowtime1 && ttimeNow < uptime1){
    if(ttimeNow < uptime1){

        if(ttimeNow > uptime2){
            return NO;
        }
        if(ttimeNow > lowtime2){
            return YES;
        }
        return YES;
    }
    return NO;
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        NSDictionary *resp = [dict objectForKey:@"response"];
        NSArray *datas = [NSJSONSerialization JSONObjectWithData:[[resp objectForKey:@"listslotJamAvailable"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
        listPickupTime = datas;
        if(datas.count == 0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsgTime preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)setupData{
    DataManager *dataM = [DataManager sharedManager];
    if([dataM.dataExtra objectForKey:@"fotoKTP"]){
        self.imgKtp.image = [self decodeBase64ToImage:[dataM.dataExtra objectForKey:@"fotoKTP"]];
    }
    if([dataM.dataExtra objectForKey:@"fotoDiriKTP"]){
        self.imgSelfKTP.image = [self decodeBase64ToImage:[dataM.dataExtra objectForKey:@"fotoDiriKTP"]];
    }
    if([dataM.dataExtra objectForKey:@"fotoEmas"]){
        self.imgGold.image = [self decodeBase64ToImage:[dataM.dataExtra objectForKey:@"fotoEmas"]];
    }
    if([dataM.dataExtra objectForKey:@"namaCabang"]){
        self.tfBranch.text = [dataM.dataExtra objectForKey:@"namaCabang"];
    }
    if([dataM.dataExtra objectForKey:@"email"]){
        self.tfEmail.text = [dataM.dataExtra objectForKey:@"email"];
        self.tfEmail.enabled = NO;
    }
    
    state88 = YES;
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *iimage = [UIImage imageWithData:data];
    return iimage;
}

@end

