//
//  GESimulationEstimate02ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GESimulationEstimate02ViewController.h"
#import "GEPromoSelectionViewController.h"
#import "GEServiceSelectionViewController.h"
#import "Styles.h"
#import "LibGold.h"
#import "Utility.h"
#import "GadaiEmasConnect.h"

@interface GESimulationEstimate02ViewController ()<PromoSelectionDelegate, GadaiEmasConnectDelegate, UITextFieldDelegate>{
    NSArray *listPromo;
    
    double nilaiTaksiranEmas;
    double minProsentasePembiayaan;
    double perkiraanNilaiPembiayaan;
    double mostLowNilaiPermbiayaan;
    double maxNilaiPembiayaan;
    
    NSNumberFormatter* currencyFormatter;

}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleBox1;
@property (weak, nonatomic) IBOutlet UILabel *labelValueBox1;
@property (weak, nonatomic) IBOutlet UIView *viewBox1;

@property (weak, nonatomic) IBOutlet UIView *viewBox2;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBox2;
@property (weak, nonatomic) IBOutlet UILabel *labelValueBox2;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleInputValue;
@property (weak, nonatomic) IBOutlet UITextField *textFieldInputValue;

@property (weak, nonatomic) IBOutlet UIView *viewBox3;
@property (weak, nonatomic) IBOutlet UILabel *labelValueBox3;

@property (weak, nonatomic) IBOutlet UIView *viewPromoBox;
@property (weak, nonatomic) IBOutlet UIView *viewSelectPromo;
@property (weak, nonatomic) IBOutlet UILabel *labelTitlePromo;
@property (weak, nonatomic) IBOutlet UIImageView *iconPromo;
@property (weak, nonatomic) IBOutlet UILabel *labelValuePromo;
@property (weak, nonatomic) IBOutlet UIImageView *iconOpenPromo;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoSuccessTitle;

@end

@implementation GESimulationEstimate02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    nilaiTaksiranEmas = 0;
    currencyFormatter = [LibGold setCurrency];

    [Styles setTopConstant:self.topConstant];
    
    self.titleBar.text = NSLocalizedString(@"GE_ESTIMATED_SIMULATION_TITLE", @"");
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.labelTitleBox1.text = NSLocalizedString(@"GE_GOLD_ESTIMATED_VALUE", @"");
    self.labelTitleBox2.text = NSLocalizedString(@"GE_FINANCING_ESTIMATED_VALUE", @"");
    self.labelValuePromo.text = NSLocalizedString(@"GE_SELECT_PROMO", @"");
    self.labelTitleInputValue.text = NSLocalizedString(@"GE_SUMBISSION_VALUE_TITLE", @"");
    
//    self.labelValueBox3.text = NSLocalizedString(@"GE_FINANCING_ESTIMATED_DESC", @"");
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"GE_FINANCING_ESTIMATED_DESC", @"")];
    
    NSMutableParagraphStyle *paragraphStyle;
    paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setTabStops:@[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:15 options:nil]]];
    [paragraphStyle setDefaultTabInterval:15];
    [paragraphStyle setFirstLineHeadIndent:0];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:16];
    CGFloat indent = [Utility heightWrapWithText:@"• " fontName:font expectedSize:CGSizeMake(16, 16)].width;
    [paragraphStyle setHeadIndent:indent];

    [string addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0,[string length])];
    
    self.labelValueBox3.attributedText = string;

    [self.viewBox1.layer setBorderWidth:1];
    [self.viewBox1.layer setBorderColor:[[UIColor blackColor]CGColor]];
    
    [self.viewBox2.layer setBorderWidth:1];
    [self.viewBox2.layer setBorderColor:[[UIColor blackColor]CGColor]];
    
    [self.viewBox3.layer setBorderWidth:1];
    [self.viewBox3.layer setBorderColor:[[UIColor blackColor]CGColor]];
        
    [self.viewPromoBox setBackgroundColor:[UIColor whiteColor]];
    [self.viewSelectPromo.layer setBorderWidth:1];
    [self.viewSelectPromo.layer setBorderColor:[[UIColor blackColor]CGColor]];
    
    [self createToolbarButton:self.textFieldInputValue];
    [self.textFieldInputValue setKeyboardType:UIKeyboardTypeNumberPad];
    self.textFieldInputValue.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionPromo)];
    
    [self.viewSelectPromo addGestureRecognizer:tap];

    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    [self.buttonNext setEnabled:NO];
    
    [self.buttonCancel addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel setTitle:lang(@"BACK") forState:UIControlStateNormal];
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];

    GadaiEmasConnect *connect = [[GadaiEmasConnect alloc]initWithDelegate:self];
    [connect sendPostData:@"cif,norek,jsonListObjectGadai" endPoint:@"simulasiTaksiranGadai" needLoading:YES textLoading:@"" encrypted:NO];
}



- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(actionDone)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
        NSString* newText = [[[[[textField.text stringByReplacingCharactersInRange:range withString:string]
                               stringByReplacingOccurrencesOfString:@"Rp." withString:@""]
                               stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""]
                              stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]]
                             stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        
        double newValue = [newText doubleValue];
        if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
        }else{
            if(perkiraanNilaiPembiayaan > 500000000){
//                textField.text = [LibGold getCurrencyForm:newValue];
                textField.text = [NSString stringWithFormat:@"Rp.%@",[LibGold getCurrencyForm:newValue]];
            }else{
                if(newValue > perkiraanNilaiPembiayaan){
                    
                    [Utility showMessage:@"Nilai Pengajuan Pembiayaan tidak boleh lebih dari Perkiraan Nilai Pembiayaan"
                               enMessage:@"The Submission Value can not exceed the Estimated Value of Financing"
                                instance:self];
                    
//                    textField.text = [LibGold getCurrencyForm:perkiraanNilaiPembiayaan];
                    textField.text = [NSString stringWithFormat:@"Rp.%@",[LibGold getCurrencyForm:perkiraanNilaiPembiayaan]];

                }else if(newValue > maxNilaiPembiayaan){

                    //250Jt
                    [Utility showMessage:[NSString stringWithFormat:@"Nilai Pengajuan Pembiayaan tidak boleh lebih dari %@",[LibGold getMillionValue:maxNilaiPembiayaan]]
                               enMessage:[NSString stringWithFormat:@"The submission value should not be more than %@",[LibGold getMillionValue:maxNilaiPembiayaan]]
                                instance:self];
                    
//                    textField.text = [LibGold getCurrencyForm:maxNilaiPembiayaan];
                    textField.text = [NSString stringWithFormat:@"Rp.%@",[LibGold getCurrencyForm:maxNilaiPembiayaan]];

                }else{
//                    textField.text = [LibGold getCurrencyForm:newValue];
                    textField.text = [NSString stringWithFormat:@"Rp.%@",[LibGold getCurrencyForm:newValue]];

                }
            }
            return NO;
        }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(![textField.text isEqualToString:@""]){
        [self.buttonNext setEnabled:YES];
    }
}

- (void)selectedPromo:(NSDictionary *)promo{
    NSString *promoContent = [NSString stringWithFormat:@"%@ %.f",[promo objectForKey:@"TypePromo"],[[promo objectForKey:@"NilaiPromo"]doubleValue]];
    
    [dataManager.dataExtra setValue:promoContent forKey:@"promo"];
//    self.iconPromo.hidden = YES;
    self.labelValuePromo.text = [promo objectForKey:@"PromoKode"];
    if([Utility isLanguageID]){
        self.lblPromoSuccessTitle.text = @"Promo Berhasil Digunakan";
        self.lblPromoSuccess.text = [NSString stringWithFormat:@"\n• Selamat Bapak/Ibu mendapatkan %@\n• Berlaku bila pembiayaan disetujui Bank", promoContent];
    }else{
        self.lblPromoSuccessTitle.text = @"Promo Successfully Used";
        self.lblPromoSuccess.text = [NSString stringWithFormat:@"\n• Congratulation you got %@\n• Applies if submission is approved by the Bank", promoContent];
    }
    
//    self.labelValuePromo.text = promoContent;
    
    self.lblPromoSuccess.hidden = NO;
    self.lblPromoSuccessTitle.hidden = NO;
}

- (void) actionPromo{
    GEPromoSelectionViewController *selectPromo = [self.storyboard instantiateViewControllerWithIdentifier:@"PromoSelection"];
    [selectPromo setDataPromo:listPromo];
    [selectPromo setDelegate:self];
    [self presentViewController:selectPromo animated:YES completion:nil];
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (BOOL) isValid{
    if([self.textFieldInputValue.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_SUBMISSION_VALUE_ALERT", @"") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
        return NO;
    }
    
    double valueSubmission = [[[self.textFieldInputValue.text
                                stringByReplacingOccurrencesOfString:@"Rp." withString:@""]
                               stringByReplacingOccurrencesOfString:@"," withString:@""]doubleValue];
    if(valueSubmission > maxNilaiPembiayaan){
        [Utility showMessage:[NSString stringWithFormat:@"Nilai Pengajuan Pembiayaan tidak boleh lebih dari %@",[LibGold getMillionValue:maxNilaiPembiayaan]]
                   enMessage:[NSString stringWithFormat:@"The submission value should not be more than %@",[LibGold getMillionValue:maxNilaiPembiayaan]]
                    instance:self];
        
        [self.textFieldInputValue setText:@""];
        return NO;
    }else if(valueSubmission < (perkiraanNilaiPembiayaan*0.5)){
        if(perkiraanNilaiPembiayaan > 500000000){
            if(valueSubmission < maxNilaiPembiayaan){
                [Utility showMessage:[NSString stringWithFormat:@"Nilai Minimum Pengajuan Pembiayaan Anda adalah IDR %@",[LibGold getMillionValue:maxNilaiPembiayaan]]
                           enMessage:[NSString stringWithFormat:@"The Minimum your submission value is IDR %@",[LibGold getMillionValue:maxNilaiPembiayaan]]
                            instance:self];
                [self.textFieldInputValue setText:@""];
                return NO;
            }
            return YES;
        }else{
            [Utility showMessage:@"Nilai Pengajuan Pembiayaan tidak boleh di bawah 50% dari Perkiraan Nilai Pembiayaan"
                       enMessage:@"The Submission Value can not  below 50% of the Estimated Value of Financing"
                        instance:self];
            [self.textFieldInputValue setText:@""];
            return NO;
        }
    }else if(valueSubmission < mostLowNilaiPermbiayaan){
        
        [Utility showMessage:[NSString stringWithFormat:@"Nilai Minimum Pengajuan Pembiayaan adalah IDR %@",[LibGold getMillionValue:mostLowNilaiPermbiayaan]]
                   enMessage:[NSString stringWithFormat:@"The Minimum submission value is IDR %@",[LibGold getMillionValue:mostLowNilaiPermbiayaan]]
                    instance:self];
        
        [self.textFieldInputValue setText:@""];
        return NO;
        
    }else if(valueSubmission > perkiraanNilaiPembiayaan){
        
        [Utility showMessage:@"Nilai Pengajuan Pembiayaan tidak boleh lebih dari Perkiraan Nilai Pembiayaan"
                   enMessage:@"The Submission Value can not exceed the Estimated Value of Financing"
                    instance:self];
        
        [self.textFieldInputValue setText:@""];
        return NO;
        
    }
    
    return YES;
}

- (void) actionNext{
    
    if([self isValid]){
        NSString *nilai = [[self.textFieldInputValue.text
                            stringByReplacingOccurrencesOfString:@"Rp." withString:@""]
                           stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        [dataManager.dataExtra setValue:nilai forKey:@"nilaiPegajuanPembiayaan"];
        [dataManager.dataExtra setValue:nilai forKey:@"nilaiPengajuanPembiayaan"];

        GEServiceSelectionViewController *serviceSelection = [self.storyboard instantiateViewControllerWithIdentifier:@"ServiceSelection"];
        UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
        [currentVC pushViewController:serviceSelection animated:YES];
    }
}

- (void) actionDone{
    [self.view endEditing:YES];
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{

    
    if(error){
        NSLog(@"%@", [error localizedDescription]);
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_ERR_REQUEST", @"") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
       [self presentViewController:alert animated:YES completion:nil];
    }else{
        NSDictionary* data = [dict objectForKey:@"response"];
        listPromo = [NSJSONSerialization JSONObjectWithData:[[data objectForKey:@"AvailablePromo"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
        
        [dataManager.dataExtra setValue:[data objectForKey:@"KodeReservasi"] forKey:@"KodeReservasi"];
        
        NSArray *objGadai = [NSJSONSerialization JSONObjectWithData:[[data objectForKey:@"ObjectGadai"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
        [dataManager.dataExtra setValue:objGadai forKey:@"ObjectGadaiResponse"];
        
        maxNilaiPembiayaan = [[data objectForKey:@"MaxNilaiPembiayaan"]doubleValue];
        nilaiTaksiranEmas = [[data objectForKey:@"NilaiTaksiranEmas"]doubleValue];
        minProsentasePembiayaan = [[data objectForKey:@"MinProsentasePembiayaan"]doubleValue];
        perkiraanNilaiPembiayaan = [[data objectForKey:@"PerkiraanNilaiPembiayaan"]doubleValue];
        mostLowNilaiPermbiayaan = [[[[NSUserDefaults standardUserDefaults]objectForKey:@"reservasi_gadai"]objectForKey:@"GadaiLimitMinAmountPickup"]doubleValue];
        
        self.labelValueBox1.text = [NSString stringWithFormat:@"Rp. %@",[LibGold getCurrencyFormNoRound:nilaiTaksiranEmas]];
        self.labelValueBox2.text = [NSString stringWithFormat:@"Rp. %@",[LibGold getCurrencyFormNoRound:perkiraanNilaiPembiayaan]];
        if([Utility isLanguageID]){
            self.textFieldInputValue.placeholder = [NSString stringWithFormat:@"Maximal nilai pengajuan Rp. %@",[LibGold getCurrencyFormNoRound:perkiraanNilaiPembiayaan]];
        }else{
            self.textFieldInputValue.placeholder = [NSString stringWithFormat:@"Maximum Submission Value Rp. %@",[LibGold getCurrencyFormNoRound:perkiraanNilaiPembiayaan]];
        }
    }
}

- (BOOL) validateServiceTime{
    NSString *cek1 = @"08:00-15:00";
    
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    NSString *timeNow = [outputFormatter stringFromDate:now];
    
    NSArray *timeArrCek1 = [cek1 componentsSeparatedByString:@"-"];
    NSString *lowTimeCek1 = [timeArrCek1[0] stringByAppendingString:@":00"];
    NSString *upTimeCek1 = [timeArrCek1[1] stringByAppendingString:@":00"];
    
    NSArray *arrNow = [timeNow componentsSeparatedByString:@":"];
    
    NSArray *arrLowCek1 = [lowTimeCek1 componentsSeparatedByString:@":"];
    NSArray *arrUpCek1 = [upTimeCek1 componentsSeparatedByString:@":"];
    
    int ttimeNow = ([arrNow[0] intValue] * 3600) + ([arrNow[1] intValue] * 60) + [arrNow[2] intValue];
    
    int lowtime1 = ([arrLowCek1[0] intValue] * 3600) + ([arrLowCek1[1] intValue] * 60) + [arrLowCek1[2] intValue];
    int uptime1 = ([arrUpCek1[0] intValue] * 3600) + ([arrUpCek1[1] intValue] * 60) + [arrUpCek1[2] intValue];
    
    
    if(ttimeNow > lowtime1 && ttimeNow < uptime1){
        return YES;
    }
    return NO;
}

@end
