//
//  GESubmissionForm03ViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface GEStatementFormViewController : TemplateViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageBar;

@property (weak, nonatomic) IBOutlet UIView *viewDataPelanggan;
@property (weak, nonatomic) IBOutlet UILabel *lblFinDataTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFinDataNom;
@property (weak, nonatomic) IBOutlet UILabel *lblFinDataPeriod;

@property (weak, nonatomic) IBOutlet UILabel *lblFinDataPurposeTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfPurpose;
@property (weak, nonatomic) IBOutlet UILabel *validatePurpose;

@property (weak, nonatomic) IBOutlet UILabel *lblFinDataSrcitmTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfSourceItem;
@property (weak, nonatomic) IBOutlet UILabel *validateSourceItem;

@property (weak, nonatomic) IBOutlet UILabel *lblFinDataSrcPymnt;
@property (weak, nonatomic) IBOutlet UITextField *tfSourcePayment;
@property (weak, nonatomic) IBOutlet UILabel *validateSourcePayment;

@property (weak, nonatomic) IBOutlet UILabel *lblFinDataAccTitl;
@property (weak, nonatomic) IBOutlet UITextField *tfAccount;
@property (weak, nonatomic) IBOutlet UILabel *validateAccount;

@property (weak, nonatomic) IBOutlet UIView *viewDebetAuthorized;
@property (weak, nonatomic) IBOutlet UILabel *lblKuasaDebitTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblDA1;
@property (weak, nonatomic) IBOutlet UILabel *lblDA2;

@property (weak, nonatomic) IBOutlet UIView *viewMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblMethodTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfMethod;
@property (weak, nonatomic) IBOutlet UILabel *validateMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblMethodInfo;

@property (weak, nonatomic) IBOutlet UIView *viewAML;
@property (weak, nonatomic) IBOutlet UILabel *lblAMLTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ1;
@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ1A;
@property (weak, nonatomic) IBOutlet UIImageView *radio1AON;
@property (weak, nonatomic) IBOutlet UIImageView *radio1AOFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML1ANote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML1Note;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField1A;

@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ1B;
@property (weak, nonatomic) IBOutlet UIImageView *radio1BON;
@property (weak, nonatomic) IBOutlet UIImageView *radio1BOFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML1BNote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML1BNote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField1B;

@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ2;
@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ2A;
@property (weak, nonatomic) IBOutlet UIImageView *radio2AON;
@property (weak, nonatomic) IBOutlet UIImageView *radio2AOFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML2ANote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML2ANote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField2A;

@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ2B;
@property (weak, nonatomic) IBOutlet UIImageView *radio2BON;
@property (weak, nonatomic) IBOutlet UIImageView *radio2BOFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML2BNote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML2BNote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField2B;

@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ2C;
@property (weak, nonatomic) IBOutlet UIImageView *radio2CON;
@property (weak, nonatomic) IBOutlet UIImageView *radio2COFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML2CNote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML2CNote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField2C;

@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ3;
@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ3A;
@property (weak, nonatomic) IBOutlet UIImageView *radio3AON;
@property (weak, nonatomic) IBOutlet UIImageView *radio3AOFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML3ANote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML3ANote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField3A;

@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ3B;
@property (weak, nonatomic) IBOutlet UIImageView *radio3BON;
@property (weak, nonatomic) IBOutlet UIImageView *radio3BOFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML3BNote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML3BNote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField3B;

@property (weak, nonatomic) IBOutlet UILabel *lblAMLQ3C;
@property (weak, nonatomic) IBOutlet UIImageView *radio3CON;
@property (weak, nonatomic) IBOutlet UIImageView *radio3COFF;
@property (weak, nonatomic) IBOutlet UITextField *tfAML3CNote;
@property (weak, nonatomic) IBOutlet UILabel *validateAML3CNote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTextField3C;

@property (weak, nonatomic) IBOutlet UILabel *lblCstmrStmntTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerStatement;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckboxCustomerStatment;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomrStatmnt;

@property (weak, nonatomic) IBOutlet UILabel *lblTermNCond;
@property (weak, nonatomic) IBOutlet UILabel *lblCustmAggremnt;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckboxCustmrAggrmnt;
@property (weak, nonatomic) IBOutlet UILabel *lblTermCondTitle;
@property (weak, nonatomic) IBOutlet UIView *viewStatement;
@property (weak, nonatomic) IBOutlet UIView *viewAggrement;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonBack;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

@end

NS_ASSUME_NONNULL_END
