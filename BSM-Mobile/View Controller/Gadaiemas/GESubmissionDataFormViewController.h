//
//  GESubmissionDataFormViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GESubmissionDataFormViewController : TemplateViewController

- (void) setupData;
@end

NS_ASSUME_NONNULL_END
