//
//  ListGoldMenuCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 03/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ListGoldMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@end

NS_ASSUME_NONNULL_END
