//
//  GESubmissionPINViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GESubmissionPINViewController.h"
#import "GEReceiptViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface GESubmissionPINViewController ()

@end

@implementation GESubmissionPINViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:self.topConstant];
    [Styles setStyleTextFieldBottomLine:self.tfInput];
    
    [self.tfInput setSecureTextEntry:true];
    [self.tfInput setKeyboardType:UIKeyboardTypeNumberPad];
    
    self.titleBar.text = NSLocalizedString(@"GE_SUBMISSION_TITLE_BAR", @"");
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    self.titleInput.text = NSLocalizedString(@"GE_INSERT_PIN", @"");
    
    [self.buttonBack setTitle:NSLocalizedString(@"BACK", @"") forState:UIControlStateNormal];
    [self.buttonNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    [self.buttonBack setColorSet:SECONDARYCOLORSET];
    
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
}

- (void) actionNext{
    
    [dataManager setPinNumber:self.tfInput.text];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=verify_pin,pin" needLoading:YES encrypted:YES banking:YES favorite:NO];
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"verify_pin"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            GEReceiptViewController *ges03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmissionReceipt"];

            UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
            [currentVC pushViewController:ges03 animated:YES];
        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

@end
