//
//  GEPromoSelectionViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEPromoSelectionViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "PromoCell.h"

@interface GEPromoSelectionViewController ()<UITableViewDelegate, UITableViewDataSource>{
    
    NSArray *listPromo;
    
}
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitlePromo;
@property (weak, nonatomic) IBOutlet UIImageView *iconPromo;
@property (weak, nonatomic) IBOutlet UILabel *labelDescPromo;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPromo;

@end

@implementation GEPromoSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(@available(iOS 13.0,*)){
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    self.viewContent.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    self.viewContent.layer.borderWidth = 1;
    self.viewContent.layer.cornerRadius = 10;
    
    if(listPromo.count > 0){
        if([[[[NSUserDefaults standardUserDefaults]objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
            self.labelDescPromo.text = [NSString stringWithFormat:@"\nada %lu jenis promo yang dapat dipilih saat ini",(unsigned long)listPromo.count];
        }else{
            self.labelDescPromo.text = [NSString stringWithFormat:@"\nthere is %lu type of promo you can choose",(unsigned long)listPromo.count];
        }
    }else{
        self.labelDescPromo.text = NSLocalizedString(@"GE_NO_PROMO", @"");
    }
    
    
    UIPanGestureRecognizer *panRecogniser = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecogniser setMaximumNumberOfTouches:1];
    [panRecogniser setMinimumNumberOfTouches:1];
    
    [self.viewContent addGestureRecognizer:panRecogniser];
    
    [self.tableViewPromo registerNib:[UINib nibWithNibName:@"PromoCell" bundle:nil] forCellReuseIdentifier:@"PromoCell"];
    [self.tableViewPromo setTableFooterView:[[UIView alloc]init]];
    [self.tableViewPromo setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableViewPromo setDelegate:self];
    [self.tableViewPromo setDataSource:self];

}

- (void) move : (UIPanGestureRecognizer*) sender{
    CGPoint velocity = [sender velocityInView:self.view];
    if(velocity.y > 0){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [Styles setTopConstant:self.topConstant];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PromoCell *cell = (PromoCell*) [tableView dequeueReusableCellWithIdentifier:@"PromoCell"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    /**
     "[{\"NilaiPromo\":\"25000.00\",\"StartDatePromo\":\"2020-08-26 00:00:13.553\",\"SyaratMinTransaksi\":\"500000.00\",\"TypePromo\":\"CashBack\",\"PromoKode\":\"GADAIEMASDIBSM\",\"FalgMinimumTransaksi\":\"Y\",\"PromoDesc\":\"Promo untuk gadai pertama dapat cashback Rp. 25.000\",\"MaxNilaiPromo\":\"25000.00\",\"EndDatePromo\":\"2020-11-26 00:00:13.553\"}]"
     */
    NSDictionary *promo = listPromo[indexPath.row];
    NSString *promoLabel = @"";
    NSString *promoMulai = @"";
    NSString *promoAkhir = @"";
    if([Utility isLanguageID]){
        promoMulai = @"Promo dimulai";
        promoAkhir = @"Promo Berakhir";
    }else{
        promoMulai = @"Promo Starts";
        promoAkhir = @"Promo End";
    }
    NSArray *startDatePromo = [[promo objectForKey:@"StartDatePromo"]componentsSeparatedByString:@" "];
    NSArray *endDatePromo = [[promo objectForKey:@"EndDatePromo"]componentsSeparatedByString:@" "];
    
    if(startDatePromo.count > 1 && endDatePromo.count > 1){
        promoLabel = [NSString stringWithFormat:@"%@\n%@\n%@ : %@\n%@ :%@",
                               [promo objectForKey:@"TypePromo"],
                               [promo objectForKey:@"NilaiPromo"],
                                promoMulai,
                               startDatePromo[0],
                      promoAkhir,
                               endDatePromo[0]];
    }else{
        promoLabel = [NSString stringWithFormat:@"%@\n%@\n%@ : %@\n%@ :%@",
                               [promo objectForKey:@"TypePromo"],
                               [promo objectForKey:@"NilaiPromo"],
                                promoMulai,
                               [promo objectForKey:@"StartDatePromo"],
                                promoAkhir,
                               [promo objectForKey:@"EndDatePromo"]];
    }
    
    cell.labelPromo.text = promoLabel;
    cell.buttonUse.tag = indexPath.row;
    [cell.buttonUse addTarget:self action:@selector(actionPromoUse:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)actionPromoUse:(UIButton*)sender{
    [delegate selectedPromo:listPromo[sender.tag]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listPromo.count;
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setDataPromo:(NSArray *)dataPromo{
    listPromo = dataPromo;
}

@end
