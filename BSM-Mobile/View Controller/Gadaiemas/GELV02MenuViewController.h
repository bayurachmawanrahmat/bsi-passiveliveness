//
//  LVGSubMenuViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GELV02MenuViewController : TemplateViewController

- (void) setData: (NSArray*) list;

@end

NS_ASSUME_NONNULL_END
