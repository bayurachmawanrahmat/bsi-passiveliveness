//
//  GEBranchSelectionViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEBranchSelectionViewController.h"
#import "GESubmissionDataFormViewController.h"
#import "GEPickupAddressFormViewController.h"

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "NSObject+FormInputCategory.h"
#import "CustomerOnboardingData.h"
#import "GadaiEmasConnect.h"
#import "BSMPinAnnotation.h"
#import "CabangTableViewCell.h"

#import "CustomBtn.h"
#import "Utility.h"
#import "Styles.h"

@interface GEBranchSelectionViewController ()<CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>{
    
    NSArray *listData;
    NSArray *listBranch;
    NSArray *filteredList;
    NSArray<MKMapItem *> *matchingItems;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    
    double latitude;
    double longitude;
    double deviceLatitude;
    double deviceLongitude;
    
    double branchLatitude;
    double branchLongitude;
    double userLatitude;
    double userLongitude;
    
    dispatch_group_t referenceGroup;
    
    NSDictionary *apiResult;
    NSMutableDictionary *listBranchResult;
    
    NSMutableDictionary *branchData;
    NSMutableDictionary *pickupData;
    
    NSString *apiMessage;
    
    UIRefreshControl *refreshControl;
    
    BOOL searchActive;
    BOOL searchLocationActive;
    BOOL pickupLocationValid;
    BOOL branchSelected;
    
    CLLocationCoordinate2D startCoord;
    MKCoordinateRegion adjustedRegion;
    
    NSString *currentLocationName;
    NSString *currentLocationAddress;
    
    NSUserDefaults *userDefaults;
    NSString *language;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;

@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UIButton *buttonCurrentLoc;
@property (weak, nonatomic) IBOutlet UITableView *tableLocation;

@property (weak, nonatomic) IBOutlet UIView *vwSelectedBranch;
@property (weak, nonatomic) IBOutlet UILabel *selectedBranchText;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonBack;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@end

@implementation GEBranchSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Styles setTopConstant:self.topConstant];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    branchData = [[NSMutableDictionary alloc]init];
    pickupData = [[NSMutableDictionary alloc]init];
    
    currentLocationName = @"";
    currentLocationAddress = @"";
    
    searchActive = NO;
    searchLocationActive = NO;
    pickupLocationValid = NO;
    branchSelected = NO;
    
    self.labelTitleBar.text = NSLocalizedString(@"GE_BRANCH_SELECTION_TITLE_BAR", @"");
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.vwSelectedBranch.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
    self.vwSelectedBranch.layer.borderWidth = 1;
    self.vwSelectedBranch.layer.cornerRadius = 8;
    self.vwSelectedBranch.hidden = YES;
    self.tableLocation.hidden = YES;
    self.tableLocation.delegate = self;
    self.tableLocation.dataSource = self;
    
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
    
//    point = [[MKPointAnnotation alloc] init];
        
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    
    [self.buttonBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBack setTitle:NSLocalizedString(@"BACK", @"") forState:UIControlStateNormal];
    [self.buttonBack setColorSet:SECONDARYCOLORSET];
    
    [self.tfSearch setDelegate:self];
    [self.tfSearch setBorderStyle:UITextBorderStyleLine];
    [self.tfSearch setBackgroundColor:[UIColor whiteColor]];
    [self.tfSearch.layer setCornerRadius:10];

    [self createToolbarButton:self.tfSearch];

//    [self currentLocationIdentifier];
//
//    refreshControl = [[UIRefreshControl alloc]init];
//    [refreshControl addTarget:self action:@selector(currentLocationIdentifier) forControlEvents:UIControlEventValueChanged];
//
//    if (@available(iOS 10.0, *)) {
//        [self.tableLocation setRefreshControl:refreshControl];
//    } else {
//        [self.tableLocation addSubview:refreshControl];
//    }
    
    [self.buttonCurrentLoc addTarget:self action:@selector(actionCurrentLoc:) forControlEvents:UIControlEventTouchUpInside];
    
    [self currentLocationIdentifier];
    
    UITapGestureRecognizer *fingerTap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self action:@selector(handleMapFingerTap:)];
    fingerTap.numberOfTapsRequired = 1;
    fingerTap.numberOfTouchesRequired = 1;
    [self.mapView addGestureRecognizer:fingerTap];
}

- (IBAction)actionCurrentLoc:(id)sender{
    [self currentLocationIdentifier];
    
    startCoord = CLLocationCoordinate2DMake(latitude, longitude);
    adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 3000, 3000)];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    MKPointAnnotation *point = [MKPointAnnotation new];
    point.coordinate = coordinate;
    point.title = currentLocationName;
    point.subtitle = currentLocationAddress;


    
    [self.mapView setRegion:adjustedRegion animated:YES];
    [self.mapView addAnnotation:point];
}

- (void) setupPoint:(double)latitude andLongitude:(double)longitude{
    startCoord = CLLocationCoordinate2DMake(latitude, longitude);
    adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 3000, 3000)];

    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    BSMPinAnnotation *point = [BSMPinAnnotation new];
    point.coordinate = coordinate;
    point.title = currentLocationName;
    point.subtitle = currentLocationAddress;

    [self.mapView setRegion:adjustedRegion animated:YES];
    [self.mapView addAnnotation:point];
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void) doneAction{
    [self.view endEditing:YES];
    self.tableLocation.hidden = YES;
}

- (void) currentLocationIdentifier{
    if([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc]init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
//            [self requestCall];
            NSString *msgTitle = @"App Permission Denied";
            NSString *msgContent = @"Turn On Location Serivce in Settings";
            if([Utility isLanguageID]){
                msgTitle = @"Izin aplikasi di tolak";
                msgContent = @"Aktifkan Layanan lokasi di pengaturan";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:msgTitle message:msgContent preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            }];
            
            NSString *stringCancel = @"";
            if([language isEqualToString:@"id"]){
                stringCancel = @"Batal";
            }else{
                stringCancel = @"Cancel";
            }
            
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:stringCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            }];
            
            [alert addAction:actionOK];
            [alert addAction:actionCancel];
            [self presentViewController:alert animated:YES completion:nil];

        }
        
        if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    }else{
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
    }
    
    if(refreshControl) [refreshControl endRefreshing];

}

-(double) distanceCallculator:(double)lat1 long1:(double)lng1 lat2:(double)lat2 long2:(double)lng2 {
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:lat1 longitude:lng1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:lat2 longitude:lng2];
    double dMeters = [loc1 distanceFromLocation:loc2];
    return dMeters;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    longitude = currentLocation.coordinate.longitude;
    latitude = currentLocation.coordinate.latitude;
    deviceLongitude =currentLocation.coordinate.longitude;
    deviceLatitude = currentLocation.coordinate.latitude;
    
    if([[dataManager.dataExtra objectForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
//        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f,%f",latitude,longitude] forKeyPath:@"geotagingOrigin"];
    }else{
//        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f,%f",latitude,longitude] forKeyPath:@"geotagigDestination"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f,%f",latitude,longitude] forKeyPath:@"geotagingOrigin"];
    }
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
    [self->_mapView setRegion:region animated:true];
    [self retrieveBranches];
}

- (void) retrieveBranches
{
    listBranchResult = [[NSMutableDictionary alloc] init];
    dispatch_group_t dispatchBranch = dispatch_group_create();
    
    if([[dataManager.dataExtra objectForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
        [GadaiEmasConnect getFromEndpoint:[NSString stringWithFormat:@"api/master/branch/list?lng=%f&lat=%f&pickup=1", longitude, latitude] dispatchGroup:dispatchBranch returnData:listBranchResult];
    }else{
        [GadaiEmasConnect getFromEndpoint:[NSString stringWithFormat:@"api/master/branch/list?lng=%f&lat=%f&instore=1", longitude, latitude] dispatchGroup:dispatchBranch returnData:listBranchResult];
    }
    
//    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/master/branch/list?lng=%f&lat=%f", longitude, latitude] dispatchGroup:dispatchBranch returnData:listBranchResult];

    dispatch_group_notify(dispatchBranch, dispatch_get_main_queue(), ^{
        NSLog(@"branch result : %@", self->listBranchResult);
        
        if ([[self->listBranchResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]) {
            NSArray *branch = [self->listBranchResult objectForKey:@"data"];
            NSMutableArray *tempBranch = [[NSMutableArray alloc] init];
            for (NSDictionary *data in branch){
                NSMutableDictionary *dictX = [NSMutableDictionary new];
                if ([data objectForKey:@"branch_address"]){
                    [dictX setValue:[data objectForKey:@"branch_address"] forKey:@"address"];
                } else {
                    [dictX setValue: @"" forKey:@"address"];
                }
                [dictX setValue:[data objectForKey:@"alamat"] forKey:@"alamat"];
                [dictX setValue:[data objectForKey:@"isoallowed"] forKey:@"isoallowed"];
                [dictX setValue:[data objectForKey:@"kabkotaid"] forKey:@"kabkotaid"];
                [dictX setValue:[data objectForKey:@"provinsiid"] forKey:@"provinsiid"];
                [dictX setValue:[data objectForKey:@"nama"] forKey:@"nama"];
                [dictX setValue:[data objectForKey:@"kode"] forKey:@"kode"];
                [dictX setValue:[data objectForKey:@"nama"] forKey:@"name"];
                [dictX setValue:[data objectForKey:@"google_map"] forKey:@"google_map"];
                [dictX setValue:[data objectForKey:@"latitude"] forKey:@"latitude"];
                [dictX setValue:[data objectForKey:@"longitude"] forKey:@"longitude"];
                [dictX setValue:[NSNumber numberWithDouble: [[NSString stringWithFormat:@"%@", [data objectForKey:@"distance"]] doubleValue]] forKey:@"distance"];
                [tempBranch addObject:dictX];
                
                BSMPinAnnotation *annotation = [BSMPinAnnotation new];
                annotation.coordinate = CLLocationCoordinate2DMake([[data objectForKey:@"latitude"]doubleValue], [[data objectForKey:@"longitude"]doubleValue]);
                annotation.title = [data objectForKey:@"nama"];
                annotation.subtitle = [NSString stringWithFormat:@"%@ %@",
                                       [data objectForKey:@"alamat"],
                                       [data objectForKey:@"distance"]
                                       ];
                annotation.branchData = data;
                
                [self->_mapView addAnnotation:annotation];
            }
    
            if ([tempBranch count] > 0)
            {
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
                self->listBranch = [tempBranch sortedArrayUsingDescriptors:@[descriptor]];
                self->listData = self->listBranch;

                
                [self.tableLocation reloadData];
            }
            else
            {
//                NSString *msg = NSLocalizedString(@"ERR_CABANG", @"ERR: Failed to retrieve branch nearby");
//                [self showAlertErrorWithTitle:lang(@"INFO") andMessage:msg sender:self];
                self.tableLocation.hidden = YES;
            }
        }
        else {
//            NSString *msg = NSLocalizedString(@"ERR_CABANG", @"ERR: Failed to retrieve branch nearby");
//            [self showAlertErrorWithTitle:@"Ooops" andMessage:msg sender:self];
            self.tableLocation.hidden = YES;
        }
    });
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    double ratio;
    double delta;
    CGPoint offset;
    
    //make a new square size, that is the resized imaged width
    CGSize sz = CGSizeMake(newSize.width, newSize.width);
    
    //figure out if the picture is landscape or portrait, then
    //calculate scale factor and offset
    if (image.size.width > image.size.height) {
        ratio = newSize.width / image.size.width;
        delta = (ratio*image.size.width - ratio*image.size.height);
        offset = CGPointMake(delta/2, 0);
    } else {
        ratio = newSize.width / image.size.height;
        delta = (ratio*image.size.height - ratio*image.size.width);
        offset = CGPointMake(0, delta/2);
    }
    
    //make the final clipping rect based on the calculated values
    CGRect clipRect = CGRectMake(-offset.x, -offset.y,
                                 (ratio * image.size.width) + delta,
                                 (ratio * image.size.height) + delta);
    
    
    //start a new context, with scale factor 0.0 so retina displays get
    //high quality image
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(sz, YES, 0.0);
    } else {
        UIGraphicsBeginImageContext(sz);
    }
    
    UIRectClip(clipRect);
    [image drawInRect:clipRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

#pragma mark - TextFieldSearch
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self currentLocationIdentifier];
    self.tableLocation.hidden = NO;
    self.vwSelectedBranch.hidden = YES;
    [self.tableLocation reloadData];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    searchActive = YES;

    NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(searchLocationActive){
        NSString *searchBarText = newText;
        MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
        request.naturalLanguageQuery = searchBarText;
        request.region = self.mapView.region;
        MKLocalSearch *search = [[MKLocalSearch alloc] initWithRequest:request];
        [search startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
            self->matchingItems = response.mapItems;
            [self.tableLocation reloadData];
        }];
    }else{
        NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"name", newText];
        NSArray *apaAtuh = [listData filteredArrayUsingPredicate:predicateString];
    
        filteredList = apaAtuh;
        if([newText isEqualToString:@""]){
            filteredList = listData;
        }
        [self.tableLocation reloadData];
    }
    
    return YES;
}

- (NSString *)parseAddress:(MKPlacemark *)selectedItem {
    NSString *firstSpace = (selectedItem.subThoroughfare != nil && selectedItem.thoroughfare != nil) ? @" " : @"";
    NSString *comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) && (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? @", " : @"";
    NSString *secondSpace = (selectedItem.subAdministrativeArea != nil && selectedItem.administrativeArea != nil) ? @" " : @"";
    NSString *addressLine = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",
                             (selectedItem.subThoroughfare == nil ? @"" : selectedItem.subThoroughfare),
                             firstSpace,
                             (selectedItem.thoroughfare == nil ? @"" : selectedItem.thoroughfare),
                             comma,
                             (selectedItem.locality == nil ? @"" : selectedItem.locality),
                             secondSpace,
                             (selectedItem.administrativeArea == nil ? @"" : selectedItem.administrativeArea)
                             ];
    return addressLine;
}

#pragma mark - TableViewDataSource
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(searchLocationActive){
        [self dropPinZoomIn:matchingItems[indexPath.row].placemark];
    }else{
        
        double selectedLatitude;
        double selectedLongitude;
        NSString *jarak = @"";
        if(searchActive){
            selectedLatitude = [[filteredList[indexPath.row] objectForKey:@"latitude"]doubleValue];
            selectedLongitude = [[filteredList[indexPath.row] objectForKey:@"longitude"]doubleValue];

            currentLocationName = [filteredList[indexPath.row] objectForKey:@"name"];
            currentLocationAddress = [filteredList[indexPath.row] objectForKey:@"alamat"];

            jarak = [filteredList[indexPath.row] objectForKey:@"distance"];
            [dataManager.dataExtra setValue:[filteredList[indexPath.row] objectForKey:@"kode"] forKey:@"kodeCabang"];
            [dataManager.dataExtra setValue:[NSString stringWithFormat:@"0min|%@km",[filteredList[indexPath.row] objectForKey:@"distance"]] forKeyPath:@"jarakKeLokasi"];

        }else{
            selectedLatitude = [[listData[indexPath.row] objectForKey:@"latitude"]doubleValue];
            selectedLongitude = [[listData[indexPath.row] objectForKey:@"longitude"]doubleValue];

            currentLocationName = [listData[indexPath.row] objectForKey:@"name"];
            currentLocationAddress = [listData[indexPath.row] objectForKey:@"alamat"];

            jarak = [listData[indexPath.row] objectForKey:@"distance"];
            [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"kode"] forKey:@"kodeCabang"];
            [dataManager.dataExtra setValue:[NSString stringWithFormat:@"0min|%@km",[listData[indexPath.row] objectForKey:@"distance"]] forKeyPath:@"jarakKeLokasi"];
        }

        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f,%f",selectedLatitude,selectedLongitude] forKeyPath:@"geotagigDestination"];

        searchActive = NO;
        [self.tableLocation reloadData];
                
        [self setupPoint:selectedLatitude andLongitude:selectedLongitude];

//        "lokasicabang": "KCP JAKARTA CIKINI|Jl. Cikini No. 58 F, Cikini, Menteng, Jakarta Pusat.",
        NSString *lokasiCabang = [NSString stringWithFormat:@"%@|%@",currentLocationName, currentLocationAddress];
        [dataManager.dataExtra setValue:currentLocationName forKey:@"namaCabang"];
        [dataManager.dataExtra setValue:lokasiCabang forKey:@"lokasicabang"];
        [dataManager.dataExtra setValue:lokasiCabang forKey:@"lokasiCabang"];
        
        branchLatitude = selectedLatitude;
        branchLongitude = selectedLongitude;
        
        [self showSelectedBranch:currentLocationName andDsitance:jarak andAddress:currentLocationAddress];

        [branchData setValue:currentLocationName forKey:@"nama"];
        [branchData setValue:currentLocationAddress forKey:@"address"];
        branchSelected = YES;
    }
    
    [self.tableLocation setHidden:YES];
    [self doneAction];
}

- (void) showSelectedBranch:(NSString*) name andDsitance:(NSString*) distance andAddress:(NSString*)addresss{
    [self.vwSelectedBranch setHidden:NO];
    
    NSString *stringJarak = @"";
    if([language isEqualToString:@"id"]){
        stringJarak = [NSString stringWithFormat:@"(%@ km dari lokasi anda)",distance];
    }else{
        stringJarak = [NSString stringWithFormat:@"(%@ km from your location)",distance];
    }
    self.selectedBranchText.text = [NSString stringWithFormat:@"%@\n%@\n%@",name, stringJarak, addresss];
}

- (void) showSelectedPickup{
    [self.vwSelectedBranch setHidden:NO];
    
    NSString *branchLocation = [NSString stringWithFormat:@"Lokasi Cabang\n%@\n%@",[branchData objectForKey:@"nama"],[branchData objectForKey:@"address"]];
    NSString *pickupLocation = [NSString stringWithFormat:@"Lokasi Pickup\n%@ (%@ km)\n%@",[pickupData objectForKey:@"nama"],[pickupData objectForKey:@"distance"],[pickupData objectForKey:@"address"]];
    
    [dataManager.dataExtra setValue:[NSString stringWithFormat:@"0min|%@km",[pickupData objectForKey:@"distance"]] forKeyPath:@"jarakKeLokasi"];
    
    self.selectedBranchText.text = [NSString stringWithFormat:@"%@\n\n%@",branchLocation,pickupLocation];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"cellLocation";
    CabangTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CabangTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }


    if(searchLocationActive){
        MKPlacemark *selectedItem = matchingItems[indexPath.row].placemark;
        cell.name.text = selectedItem.name;
        cell.address.text = [self parseAddress:selectedItem];
        cell.distance.text = @"";

    }else{
        NSDictionary *data;
        if(searchActive){
            data = [filteredList objectAtIndex:indexPath.row];
        }else{
            data = [listData objectAtIndex:indexPath.row];
        }
    
        cell.name.text = [data objectForKey:@"name"];
        cell.address.text = [data objectForKey:@"address"];
        double distance =  [[data objectForKey:@"distance"] doubleValue];
        cell.distance.text = [NSString stringWithFormat:@"%.2f km", distance];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(searchLocationActive){
        return matchingItems.count;
    }else{
        if(searchActive){
            return filteredList.count;
        }else{
            return listData.count;
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionNext{
    
    //layanan Pickup goto pickupaddress
    NSString *template = @"";
    if([[dataManager.dataExtra objectForKey:@"typeLayanan"] isEqualToString:@"Pickup"]){
        if(!searchLocationActive){
            if(branchSelected){
                searchLocationActive = YES;
                self.labelTitleBar.text = @"Lokasi Pickup";
                self.tfSearch.placeholder = @"Pilih Lokasi Pickup Anda (maks 5 km dari Kantor Cabang)";
                [self getCurrentLocation:CLLocationCoordinate2DMake(latitude, longitude) removeAll:YES];
            }else{
                [Utility showMessage:@"Please Pick Branch" instance:self];
            }
        }else{
            [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f,%f",userLatitude,userLongitude] forKeyPath:@"geotagigDestination"];
            if(!pickupLocationValid){
                [Utility showMessage:@"Pickup location is not valid" instance:self];
            }else{
                template = @"PickupAddressForm";
                TemplateViewController *gototo = [self.storyboard instantiateViewControllerWithIdentifier:template];
                UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
                [currentVC pushViewController:gototo animated:YES];
            }
        }
    }else{
        template = @"SubmissionDataForm";
        TemplateViewController *gototo = [self.storyboard instantiateViewControllerWithIdentifier:template];
        UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
        [currentVC pushViewController:gototo animated:YES];
    }
}

- (void)dropPinZoomIn:(MKPlacemark *)placemark
{
    [_mapView removeAnnotations:(_mapView.annotations)];
    [self setupPoint:self->branchLatitude andLongitude:self->branchLongitude];
    MKPointAnnotation *annotation = [MKPointAnnotation new];
    annotation.coordinate = placemark.coordinate;
    annotation.title = placemark.name;
    annotation.subtitle = [NSString stringWithFormat:@"%@ %@",
                           (placemark.locality == nil ? @"" : placemark.locality),
                           (placemark.administrativeArea == nil ? @"" : placemark.administrativeArea)
                           ];
    [_mapView addAnnotation:annotation];
    MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
    MKCoordinateRegion region = MKCoordinateRegionMake(placemark.coordinate, span);
    [_mapView setRegion:region animated:true];
    
    [self getPathDirections:CLLocationCoordinate2DMake(branchLatitude, branchLongitude) withDestination:CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude)];
    
}

-(void)getPathDirections:(CLLocationCoordinate2D)source withDestination:(CLLocationCoordinate2D)destination{

    MKPlacemark *placemarkSrc = [[MKPlacemark alloc] initWithCoordinate:source addressDictionary:nil];
    MKMapItem *mapItemSrc = [[MKMapItem alloc] initWithPlacemark:placemarkSrc];
    MKPlacemark *placemarkDest = [[MKPlacemark alloc] initWithCoordinate:destination addressDictionary:nil];
    MKMapItem *mapItemDest = [[MKMapItem alloc] initWithPlacemark:placemarkDest];

    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:mapItemSrc];
    [request setDestination:mapItemDest];
    [request setTransportType:MKDirectionsTransportTypeAutomobile];
    request.requestsAlternateRoutes = NO;

    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];

    [directions calculateDirectionsWithCompletionHandler:
    ^(MKDirectionsResponse *response, NSError *error) {
      if (error) {
          // Handle Error
      } else {
          [self->_mapView removeOverlays:self->_mapView.overlays];
          [self showRoute:response];
      }
    }];

}

- (void) showRoute:(MKDirectionsResponse*) response{
    for (MKRoute* route in response.routes){
        
        CLLocationDistance distance = route.distance;
        
        //new method
        CLLocation *destin = response.destination.placemark.location;
        CLLocation *currnt = response.source.placemark.location;
        double jarak = [destin distanceFromLocation:currnt];
        
        
        NSLog(@"DISTANCE IN ROUTES::: %f Meters",distance);
        NSLog(@"JARAK ::: %f Meters",jarak);

        double radiusDistanceinMeters = [[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"ReservasiGadai_RadiusPickup"]doubleValue]*1000;
        if([[dataManager.dataExtra objectForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
            if(jarak > radiusDistanceinMeters){
//            if(distance > radiusDistanceinMeters){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Melebihi Batas Maximum Radius Pickup" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    self->pickupLocationValid = NO;
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                pickupLocationValid = YES;
                [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
                [pickupData setValue:[NSString stringWithFormat:@"%.2f", distance/1000] forKey:@"distance"];
                [self showSelectedPickup];
            }
        }else{
            [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
        }
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    
    MKPolylineRenderer *polylineRenderer = [[MKPolylineRenderer alloc]initWithOverlay:overlay];
    if([overlay isKindOfClass:[MKPolyline class]]){
        polylineRenderer.strokeColor = UIColorFromRGB(const_color_primary);
        polylineRenderer.lineWidth = 3.5;
        return polylineRenderer;
    }
    return polylineRenderer;
}

- (void)handleMapFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    if(searchLocationActive){
        if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
            return;
        }

        CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
        CLLocationCoordinate2D touchMapCoordinate =
        [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
        [self getCurrentLocation:touchMapCoordinate removeAll:YES];
    }
}

- (void) getCurrentLocation : (CLLocationCoordinate2D) coordinate removeAll:(BOOL)remove{
    CLGeocoder *geocoder = [[CLGeocoder alloc]init];
    CLLocation *location = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude];

    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray<MKPlacemark*> *placemrk, NSError*err){
        if(err == nil){
            MKPlacemark *placemark = placemrk[0];
            if(remove){
                [self->_mapView removeAnnotations:(self->_mapView.annotations)];
            }
            [self setupPoint:self->branchLatitude andLongitude:self->branchLongitude];

            MKPointAnnotation *annotation = [[MKPointAnnotation alloc]init];
            annotation.coordinate = coordinate;
            annotation.title = placemark.name;
            annotation.subtitle = [NSString stringWithFormat:@"%@ %@",
                                   (placemark.locality == nil ? @"" : placemark.locality),
                                   (placemark.administrativeArea == nil ? @"" : placemark.administrativeArea)
                                   ];
            
            self->userLatitude = coordinate.latitude;
            self->userLongitude = coordinate.longitude;
            
            [self->_mapView addAnnotation:annotation];
            MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
            MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
            [self->_mapView setRegion:region animated:true];
            
            [self->pickupData setValue:annotation.title forKey:@"nama"];
            [self->pickupData setValue:annotation.subtitle forKey:@"address"];
            
            [self getPathDirections:CLLocationCoordinate2DMake(self->branchLatitude, self->branchLongitude) withDestination:CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude)];
            
        }
    }];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if([annotation isKindOfClass:[BSMPinAnnotation class]]){
        BSMPinAnnotation *anno = (BSMPinAnnotation*) annotation;
        return anno.annotationView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    if([view isKindOfClass:[MKMarkerAnnotationView class]]){
        MKMarkerAnnotationView *markerView = (MKMarkerAnnotationView*) view;
        if([markerView.accessibilityLabel isEqualToString:@"bsm_annotation"]){
            BSMPinAnnotation *anno = markerView.annotation;
            NSDictionary *dataCabang = anno.branchData;
            [dataManager.dataExtra setValue:[dataCabang objectForKey:@"kode"] forKey:@"kodeCabang"];
            [dataManager.dataExtra setValue:[NSString stringWithFormat:@"0min|%@km",[dataCabang objectForKey:@"distance"]] forKeyPath:@"jarakKeLokasi"];
            
            NSString *lokasiCabang = [NSString stringWithFormat:@"%@|%@",[dataCabang objectForKey:@"nama"], [dataCabang objectForKey:@"alamat"]];
            [dataManager.dataExtra setValue:[dataCabang objectForKey:@"nama"] forKey:@"namaCabang"];
            [dataManager.dataExtra setValue:lokasiCabang forKey:@"lokasicabang"];
            [dataManager.dataExtra setValue:lokasiCabang forKey:@"lokasiCabang"];
            
            branchLatitude = [[dataCabang objectForKey:@"latitude"]doubleValue];
            branchLongitude = [[dataCabang objectForKey:@"longitude"]doubleValue];
            
            if([[dataManager.dataExtra objectForKey:@"typeLayanan"]isEqualToString:@"Pickup"]){
                [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f,%f",branchLatitude,branchLongitude] forKeyPath:@"geotagingOrigin"];
            }else{
                [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f,%f",branchLatitude,branchLongitude] forKeyPath:@"geotagigDestination"];
                
                [self actionCurrentLoc:self];
                [self getPathDirections:CLLocationCoordinate2DMake(latitude, longitude) withDestination:CLLocationCoordinate2DMake(branchLatitude, branchLongitude)];
            }
            
            [self showSelectedBranch:[dataCabang objectForKey:@"nama"] andDsitance:[dataCabang objectForKey:@"distance"] andAddress:[dataCabang objectForKey:@"alamat"]];
 
            [branchData setValue:[dataCabang objectForKey:@"nama"] forKey:@"nama"];
            [branchData setValue:[dataCabang objectForKey:@"alamat"] forKey:@"address"];
            branchSelected = YES;
        }
    }
}


@end
