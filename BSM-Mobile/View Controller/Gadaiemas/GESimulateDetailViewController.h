//
//  SimulateDetailViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GESimulateDetailViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate : (id) newDelegate;
- (void) setupEditData : (NSDictionary*) dict withIndex:(NSInteger)idx;

@end

@protocol SimulateDetailDelegate

@required

- (void) updateData : (NSDictionary*) dict withState:(int) state andIndex:(NSInteger)index0;

@end


NS_ASSUME_NONNULL_END
