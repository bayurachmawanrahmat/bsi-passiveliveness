//
//  GESubmissionForm03ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GEStatementFormViewController.h"
#import "GEConfirmationViewController.h"
#import "GEPopupStatementViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface GEStatementFormViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, GEPopupSttmntDelegate, ConnectionDelegate>{
    
    NSArray *listTujuanPembiayaan;
    NSArray *listAsalBarang;
    NSArray *listSumberPelunasan;
    NSArray *listRekeningPencairan;
    NSArray *listMethod;
    
    NSString *jangkaWaktu;
    
    BOOL checkKuasaDebetState;
    BOOL checkStatementState;
    BOOL checkAgreementState;
    
    UIImage *imageChecked;
    UIImage *imageUncheckd;
    UIImage *radioSelect;
    UIImage *radioUnselect;
    
    NSUserDefaults *userDefaults;
}

@end

@implementation GEStatementFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.lblTitleBar.text = NSLocalizedString(@"GE_SUBMISSION_TITLE_BAR", @"");
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    
    imageChecked = [UIImage imageNamed:@"ic_checkbox_active.png"];
    imageUncheckd = [UIImage imageNamed:@"ic_checkbox_inactive.png"];
    radioSelect = [UIImage imageNamed:@"radio_select"];
    radioUnselect = [UIImage imageNamed:@"radio_unselect"];
    
    listTujuanPembiayaan = [NSJSONSerialization JSONObjectWithData:[[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"kyc_tujuanpembiayaan"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    listAsalBarang = [NSJSONSerialization JSONObjectWithData:[[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"kyc_SumberKepemilikan"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    listSumberPelunasan = [NSJSONSerialization JSONObjectWithData:[[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"kyc_SumberPelunasan"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
    jangkaWaktu = [[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"GadaiJangkaWaktu"];
    
    listMethod = @[@"Otomatis",@"Manual"];
    
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setTitle:NSLocalizedString(@"NEXT", @"") forState:UIControlStateNormal];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    
    [self.buttonBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBack setTitle:NSLocalizedString(@"BACK", @"") forState:UIControlStateNormal];
    [self.buttonBack setColorSet:SECONDARYCOLORSET];
    
    [self viewDataPembiayaan];
    [self viewKuasaDebet];
    [self viewMetodePerpanjangan];
    [self viewSetupAML];
    [self viewCustomerStatement];
    [self viewTerms];
}


- (void) viewDataPembiayaan{
    
    [self.viewDataPelanggan.layer setBorderWidth:1];
    [self.viewDataPelanggan.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    
    self.lblFinDataTitle.text = @"Data Pembiayaan";
    self.lblFinDataPurposeTitle.text = @"Tujuan Pembiayaan Gadai";
    self.lblFinDataSrcitmTitle.text = @"Asal Kepemilikan Barang";
    self.lblFinDataSrcPymnt.text = @"Sumber Pelunasan";
    self.lblFinDataAccTitl.text = @"Rekening Pencairan";
    
    
    self.lblFinDataNom.text = [NSString stringWithFormat:@"Nominal : Rp. %@", [Utility formatCurrencyValue:[[dataManager.dataExtra objectForKey:@"nilaiPengajuanPembiayaan"]doubleValue]]];

    self.lblFinDataPeriod.text = [NSString stringWithFormat:@"Jangka Waktu : %@ bulan", jangkaWaktu];
    
    
    [self createToolbarButton:self.tfPurpose];
    [self createPicker:self.tfPurpose withTag:0];
    self.tfPurpose.placeholder = @"Pilih Tujuan Pembiayan";
    
    [self createToolbarButton:self.tfSourceItem];
    [self createPicker:self.tfSourceItem withTag:1];
    self.tfSourceItem.placeholder = @"Pilih Asal Kepemilikan Barang";
    
    [self createToolbarButton:self.tfSourcePayment];
    [self createPicker:self.tfSourcePayment withTag:2];
    self.tfSourcePayment.placeholder = @"Pilih Sumber Pelunasan";
    
    [self createToolbarButton:self.tfAccount];
    [self createPicker:self.tfAccount withTag:4];
    self.tfAccount.placeholder = @"Pilih Rekening Pencairan";
    
    [self getListAccount];
}

- (void) viewKuasaDebet{
    self.lblKuasaDebitTitle.text = @"Kuasa Debet";
    [self.viewDebetAuthorized.layer setBorderWidth:1];
    [self.viewDebetAuthorized.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    
//    let bullet = "•  "
//
//    var strings = [String]()
//    strings.append("Payment will be charged to your iTunes account at confirmation of purchase.")
//    strings.append("Your subscription will automatically renew unless auto-renew is turned off at least 24-hours before the end of the current subscription period.")
//    strings.append("Your account will be charged for renewal within 24-hours prior to the end of the current subscription period.")
//    strings.append("Automatic renewals will cost the same price you were originally charged for the subscription.")
//    strings.append("You can manage your subscriptions and turn off auto-renewal by going to your Account Settings on the App Store after purchase.")
//    strings.append("Read our terms of service and privacy policy for more information.")
//    strings = strings.map { return bullet + $0 }
//

    NSString *bullets = @"•";
    NSMutableString *lblDAString = [[NSMutableString alloc]init];
    [lblDAString appendString:@"Biaya Sewa Penyimpanan"];
    [lblDAString appendString:@"Biaya Administrasi Gadai"];
    [lblDAString appendString:@"Selisih Nilai Pembiayaan/Pokok Pinjaman bila terjadi penurunan HDE (saat di perpanjang)"];

    
    self.lblDA1.text = @"Saya setuju dan memberikan kuasa kepada PT. Bank Syariah Indonesia, Tbk. yang tidak dapat dibatalkan secara sepihak oleh Saya untuk mendebet rekening Saya dalam rangka pembayaran biaya-biaya yang timbul atas pengajuan pembiayaan gadai emas Saya.";
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"1. Biaya Sewa Penyimpanan\n2. Biaya Administrasi Gadai\n3. Selisih Nilai Pembiayaan/Pokok Pinjaman bila terjadi penurunan HDE (saat di perpanjang)"];

    NSMutableParagraphStyle *paragraphStyle;
    paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setTabStops:@[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:15 options:nil]]];
    [paragraphStyle setDefaultTabInterval:15];
    [paragraphStyle setFirstLineHeadIndent:0];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:16];
    CGFloat indent = [Utility heightWrapWithText:@"3. " fontName:font expectedSize:CGSizeMake(16, 16)].width;
    [paragraphStyle setHeadIndent:indent];

    [string addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0,[string length])];
//    self.lblDA2.text = @"1. Biaya Sewa Penyimpanan\n2. Biaya Administrasi Gadai\n3. Selisih Nilai Pembiayaan/Pokok Pinjaman bila terjadi penurunan HDE (saat di perpanjang)";
    self.lblDA2.attributedText = string;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(actionKuasaDebetCheck)];
    
    [self.imgCheckbox setUserInteractionEnabled:YES];
    [self.imgCheckbox addGestureRecognizer:tap];
}

- (void) viewMetodePerpanjangan{
    [self.viewMethod.layer setBorderWidth:1];
    [self.viewMethod.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"• Perpanjangan otomatis akan dilakukan bila dana perpanjangan dan biaya-biaya tersedia di rekening nasabah dan tanpa harus datang ke bank \n\n• Perpanjangan Manual maka nasabah datang ke Bank\n\n• Harga Dasar Emas (HDE) sebagaimana yang berlaku di PT. Bank Syariah Indonesia, Tbk. yang mengacu kepada ketentuan Bank Indonesia atau Otoritas Jasa Keuangan"];

    NSMutableParagraphStyle *paragraphStyle;
    paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [paragraphStyle setTabStops:@[[[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentLeft location:15 options:nil]]];
    [paragraphStyle setDefaultTabInterval:15];
    [paragraphStyle setFirstLineHeadIndent:0];
    UIFont *font = [UIFont fontWithName:@"Helvetica" size:16];
    CGFloat indent = [Utility heightWrapWithText:@"• " fontName:font expectedSize:CGSizeMake(16, 16)].width;
    [paragraphStyle setHeadIndent:indent];

    [string addAttributes:@{NSParagraphStyleAttributeName: paragraphStyle} range:NSMakeRange(0,[string length])];

    self.lblMethodTitle.text = @"Metode Perpanjangan";
    self.lblMethodInfo.attributedText = string;
    
    [self createToolbarButton:self.tfMethod];
    self.tfMethod.placeholder = @"Pilih Metode Perpanjangan";
    [self createPicker:self.tfMethod withTag:3];
    
}
- (void) addToolbarToTextField{
    [self createToolbarButton:self.tfAML1ANote];
    [self createToolbarButton:self.tfAML1BNote];
    [self createToolbarButton:self.tfAML2ANote];
    [self createToolbarButton:self.tfAML2BNote];
    [self createToolbarButton:self.tfAML2CNote];
    [self createToolbarButton:self.tfAML3ANote];
    [self createToolbarButton:self.tfAML3BNote];
    [self createToolbarButton:self.tfAML3CNote];
}

- (void) viewSetupAML{
    [self.viewAML.layer setBorderWidth:1];
    [self.viewAML.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    
    [self addToolbarToTextField];
    
    self.lblAMLTitle.text = @"Anti Money Laundering (AML) & Know Your Customer (KYC) Calon Debitur";
    self.lblAMLQ1.text = @"1. Apakah Bapak/Ibu pengurus atau pemilik dari Perusahaan Bapak/Ibu pernah memperoleh fasilitas pembiayaan :";
    self.lblAMLQ1A.text = @"a. Dari PT. Bank Syariah Indonesia, Tbk.";
    self.lblAMLQ1B.text = @"b. Dari Bank/Kreditur lain";
    
    self.lblAMLQ2.text = @"2. Apakah Bapak/Ibu :";
    self.lblAMLQ2A.text = @"a. Partisipan (pendukung aktif) Partai Politik tertentu (Politically Expossed Person/PEP)";
    self.lblAMLQ2B.text = @"b. Memiliki hubungan kekerabatan/bisnis dengan PEP/Pejabat Negara (Pusat/Daerah)";
    self.lblAMLQ2C.text = @"c. Memiliki hubungan kekerabatan/bisnis dengan WNA";
    
    self.lblAMLQ3.text = @"3. Apakah Bapak/Ibu saat ini :";
    self.lblAMLQ3A.text = @"a. Sedang menghadapi permasalahan hukum";
    self.lblAMLQ3B.text = @"b. Ditetapkan sebagai tersangka/terdakwa";
    self.lblAMLQ3C.text = @"c. Memiliki hubungan kekerabatan/bisnis dengan tersangka/terdakwa";
    
    [self.radio1AON setUserInteractionEnabled:YES];
    [self.radio1AON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio1AOFF setUserInteractionEnabled:YES];
    [self.radio1AOFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio1AOFF setAccessibilityLabel:@"AML1A"];
    [self.radio1AON setAccessibilityLabel:@"AML1A"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML1A"];
    [self createToolbarButton:self.tfAML1ANote];
    
    [self.radio1BON setUserInteractionEnabled:YES];
    [self.radio1BON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio1BOFF setUserInteractionEnabled:YES];
    [self.radio1BOFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio1BOFF setAccessibilityLabel:@"AML1B"];
    [self.radio1BON setAccessibilityLabel:@"AML1B"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML1B"];

    
    [self.radio2AON setUserInteractionEnabled:YES];
    [self.radio2AON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio2AOFF setUserInteractionEnabled:YES];
    [self.radio2AOFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio2AON setAccessibilityLabel:@"AML2A"];
    [self.radio2AOFF setAccessibilityLabel:@"AML2A"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML2A"];

    
    [self.radio2BON setUserInteractionEnabled:YES];
    [self.radio2BON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio2BOFF setUserInteractionEnabled:YES];
    [self.radio2BOFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio2BON setAccessibilityLabel:@"AML2B"];
    [self.radio2BOFF setAccessibilityLabel:@"AML2B"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML2B"];

    [self.radio2CON setUserInteractionEnabled:YES];
    [self.radio2CON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio2COFF setUserInteractionEnabled:YES];
    [self.radio2COFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio2CON setAccessibilityLabel:@"AML2C"];
    [self.radio2COFF setAccessibilityLabel:@"AML2C"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML2C"];


    
    [self.radio3AON setUserInteractionEnabled:YES];
    [self.radio3AON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio3AOFF setUserInteractionEnabled:YES];
    [self.radio3AOFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio3AON setAccessibilityLabel:@"AML3A"];
    [self.radio3AOFF setAccessibilityLabel:@"AML3A"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML3A"];

    [self.radio3BON setUserInteractionEnabled:YES];
    [self.radio3BON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio3BOFF setUserInteractionEnabled:YES];
    [self.radio3BOFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio3BON setAccessibilityLabel:@"AML3B"];
    [self.radio3BOFF setAccessibilityLabel:@"AML3B"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML3B"];

    [self.radio3CON setUserInteractionEnabled:YES];
    [self.radio3CON addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioON:)]];
    [self.radio3COFF setUserInteractionEnabled:YES];
    [self.radio3COFF addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRadioOFF:)]];
    [self.radio3CON setAccessibilityLabel:@"AML3C"];
    [self.radio3COFF setAccessibilityLabel:@"AML3C"];
    [dataManager.dataExtra setValue:@"Y" forKey:@"AML3C"];

}

- (void) viewCustomerStatement{
    [self.viewStatement.layer setBorderWidth:1];
    [self.viewStatement.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    
    self.lblCstmrStmntTitle.text = @"Pernyataan Nasabah";
    
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    self.lblCustomerStatement.attributedText = [[NSAttributedString alloc] initWithString:@"Pernyataan Nasabah (Wajib di klik)"
                                                             attributes:underlineAttribute];
//    self.lblCustomerStatement.text = @"Pernyataan Nasabah (Wajib di klik)";
    self.lblCustomrStatmnt.text = @"Saya menyatakan dan menyetujui hal-hal yang tercantum pada Pernyataan Nasabah";
    
    [self.lblCustomerStatement setUserInteractionEnabled:YES];
    [self.lblCustomerStatement addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionOpenPopup:)]];
    
    [self.lblCustomrStatmnt addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCheckStatement)]];
    [self.imgCheckboxCustomerStatment addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCheckStatement)]];
}

- (void) viewTerms{
    [self.viewAggrement.layer setBorderWidth:1];
    [self.viewAggrement.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    
    self.lblTermCondTitle.text = @"Ketentuan Pemberian Pembiayaan Gadai Emas";
//    self.lblTermCondTitle.text = @"Syarat dan Ketentuan Pemberian Pembiayaan Gadai Emas";
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
//    self.lblTermNCond.text = @"Syarat dan Ketentuan (Wajib di klik)";
//    self.lblTermNCond.attributedText = [[NSAttributedString alloc] initWithString:@"Syarat dan Ketentuan (Wajib di klik)"
//                                                             attributes:underlineAttribute];
    self.lblTermNCond.attributedText = [[NSAttributedString alloc] initWithString:@"Syarat dan Ketentuan (Wajib di klik)"
                                                             attributes:underlineAttribute];

    
    self.lblCustmAggremnt.text = @"Saya telah membaca, mengerti/memahami dan setuju dengan ketentuan pengajuan pembiayaan Gadai Emas melalui BSI Mobile";
    
    [self.lblTermNCond setUserInteractionEnabled:YES];
    [self.lblTermNCond addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionOpenPopup:)]];
    
    [self.lblCustmAggremnt addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCheckAgreement)]];
    [self.imgCheckboxCustmrAggrmnt addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCheckAgreement)]];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return listTujuanPembiayaan.count;
    }else if(pickerView.tag == 1){
        return listAsalBarang.count;
    }else if(pickerView.tag == 2){
        return listSumberPelunasan.count;
    }else if (pickerView.tag == 3){
        return listMethod.count;
    }else if (pickerView.tag == 4){
        return listRekeningPencairan.count;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return listTujuanPembiayaan[row];
    }else if(pickerView.tag == 1){
        return listAsalBarang[row];
    }else if(pickerView.tag == 2){
        return listSumberPelunasan[row];
    }else if (pickerView.tag == 3){
        return listMethod[row];
    }else if (pickerView.tag == 4){
        if(row == 0){
            return @"";
        }
        return listRekeningPencairan[row];
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        self.tfPurpose.text = listTujuanPembiayaan[row];
        [dataManager.dataExtra setValue:listTujuanPembiayaan[row] forKey:@"kyc_tujuanpembiayaan"];
    }else if(pickerView.tag == 1){
        self.tfSourceItem.text = listAsalBarang[row];
        [dataManager.dataExtra setValue:listAsalBarang[row] forKey:@"kyc_SumberKepemilikan"];
    }else if(pickerView.tag == 2){
        self.tfSourcePayment.text = listSumberPelunasan[row];
        [dataManager.dataExtra setValue:listSumberPelunasan[row] forKey:@"kyc_SumberPelunasan"];
    }else if (pickerView.tag == 3){
        self.tfMethod.text = listMethod[row];
        [dataManager.dataExtra setValue:listMethod[row] forKey:@"metodePerpanjangan"];
    }else if (pickerView.tag == 4){
        if(row != 0){
            self.tfAccount.text = listRekeningPencairan[row];
            [dataManager.dataExtra setValue:listRekeningPencairan[row] forKey:@"rekeningPencairan"];
        }
    }
}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(actionDone)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void) actionDone{
    [self.view endEditing:YES];
}

- (void) actionNext{

    [dataManager.dataExtra setValue:self.tfAML1ANote.text forKey:@"AML1ANote"];
    [dataManager.dataExtra setValue:self.tfAML1BNote.text forKey:@"AML1BNote"];
    [dataManager.dataExtra setValue:self.tfAML2ANote.text forKey:@"AML2ANote"];
    [dataManager.dataExtra setValue:self.tfAML2BNote.text forKey:@"AML2BNote"];
    [dataManager.dataExtra setValue:self.tfAML2CNote.text forKey:@"AML2CNote"];
    [dataManager.dataExtra setValue:self.tfAML3ANote.text forKey:@"AML3ANote"];
    [dataManager.dataExtra setValue:self.tfAML3BNote.text forKey:@"AML3BNote"];
    [dataManager.dataExtra setValue:self.tfAML3CNote.text forKey:@"AML3CNote"];

    if([self validate]){
        GEConfirmationViewController *ges03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SubmissionConfirmation"];
        
        UINavigationController *currentVC = (UINavigationController*) self.tabBarController.selectedViewController;
        [currentVC pushViewController:ges03 animated:YES];
    }else{
        CGPoint newOffset = self.scrollview.contentOffset;
        newOffset.y = 0;
        [self.scrollview setContentOffset:newOffset animated:YES];
    }
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionKuasaDebetCheck{
    if(checkKuasaDebetState){
        [self.imgCheckbox setImage:imageUncheckd];
        checkKuasaDebetState = NO;
    }else{
        [self.imgCheckbox setImage:imageChecked];
        checkKuasaDebetState = YES;
    }}

- (void) actionRadioON : (UITapGestureRecognizer*) sender{
    CGFloat tfON = 30;
    UIView *viewBox = [sender.view superview];
    
    for(UIView *view in viewBox.subviews){
        if([view isKindOfClass:[UITextField class]]){
            NSLog(@"%@", view.constraints);
            for(NSLayoutConstraint *constraint in view.constraints){
                if([constraint isKindOfClass:[NSLayoutConstraint class]]){
                    if(constraint.firstAttribute == NSLayoutAttributeHeight || constraint.secondAttribute == NSLayoutAttributeHeight){
                        constraint.constant = tfON;
                    }
                }
            }
        }
        
        if([view isKindOfClass:[UIImageView class]]){
            UIImageView *imgView = (UIImageView*) view;
            
            [imgView setImage:radioUnselect];
            
            [dataManager.dataExtra setValue:@"Y" forKey:imgView.accessibilityLabel];
            
            if([imgView isEqual:sender.view]){
                [imgView setImage:radioSelect];
            }
        }
    }
}

- (void) actionRadioOFF : (UITapGestureRecognizer*) sender{
    CGFloat tfOFF = 0;
    UIView *viewBox = [sender.view superview];
    
    for(UIView *view in viewBox.subviews){
        if([view isKindOfClass:[UITextField class]]){
            NSLog(@"%@", view.constraints);
            for(NSLayoutConstraint *constraint in view.constraints){
                if([constraint isKindOfClass:[NSLayoutConstraint class]]){
                    if(constraint.firstAttribute == NSLayoutAttributeHeight || constraint.secondAttribute == NSLayoutAttributeHeight){
                        constraint.constant = tfOFF;
                    }
                }
            }
        }
        
        if([view isKindOfClass:[UIImageView class]]){
            UIImageView *imgView = (UIImageView*) view;
            
            [imgView setImage:radioUnselect];
            
            [dataManager.dataExtra setValue:@"N" forKey:imgView.accessibilityLabel];
            
            if([imgView isEqual:sender.view]){
                [imgView setImage:radioSelect];
            }
        }
    }
}

- (void) actionOpenPopup :(UITapGestureRecognizer*)sender{
    GEPopupStatementViewController *popup = [self.storyboard instantiateViewControllerWithIdentifier:@"GEPOPSTTMNT"];
    [popup setDelegate:self];
    if([sender.view isEqual:self.lblTermNCond]){
        [popup setTagger:1];
        [popup setData:@"Ketentuan Pemberian Pembiayaan Gadai Emas" andContent:@"1. Nilai Pembiayaan Gadai Emas yang disetujui oleh Bank adalah berdasarkan penaksiran emas yang dilakukan oleh petugas Bank dan karenanya nilai Pembiayaan Gadai Emas yang disetujui ole Bank dapat menjadi lebih rendah dari nilai Pembiayaan yang diajukan nasabah\n\n2. Akad yang dipergunakan dalam pembiayaan Gadai Emas adalah Akad Qardh dengan Agunan Gadai Emas dan akad Ijarah (“Akad”). Penandatanganan Akad dilakukan setelah pengajuan Pembiayaan Gadai Emas Nasabah disetujui oleh Bank.\n\n3. Biaya administrasi akan diinformasikan kemudian berdasarkan jumlah pembiayaan yang disetujui oleh Bank Sebelum akad dilakukan.\n\n4. Biaya Sewa Penyimpanan atas agunan dalam rangka Pembiayaan Gadai Emas akan diinformasikan kemudian berdasarkan hasil penaksiran oleh Bank sebelum dilakukan penandatanganan akad. Dalam hal nasabah melakukan pelunasan atas pembiayaan Gadai Emas sebelum tanggal jatuh tempo, maka biaya sewa penyimpanan akan dihitung per 15 (lima belas) hari.\n\n5. Promo pada pengajuan melalui BSI Mobile ini (bila ada), hanya berlaku jika pengajuan permohonan Pembiayaan Gadai Emas nasabah telah disetujui oleh Bank.\n\n6. Bank berhak untuk menolak pengajuan permohonan pembiayaan Gadai Emas basabah berdasarkan hasil penaksiran emas yang dilakukan oleh petugas Bank.\n\n7. Petugas Bank dapat menghubungi nasabah untuk melakukan konfirmasi terkait pengajuan Pembiayaan Gadai Emas nasabah.\n\n8. Apabila pengajuan Pembiayaan Gadai Emas adalah Layanan Pick-up, maka dalam kondisi tertentu Bank dapat membatalkan secara sepihak pengajuan Layanan Pick-up nasabah."];

    }else{
        [popup setTagger:0];
        [popup setData:@"Pernyataan Nasabah" andContent:@"1. Setiap data yang Saya isi pada pengajuan pembiayaan Gadai Emas melalui BSI Mobile ini adalah benar.\n\n2. Data diri yang digunakan untuk pengajuan pembiayaan Gadai Emas melalui BSI Mobile adalah sesuai dengan data nasabah yang saat ini telah tersimpan di Bank\n\n3. Barang yang dijaminkan dalam rangka Pembiayaan Gadai Emas adalah benar hak dan milik pribadi Saya, di peroleh secara sah dan tidak melawan hukum.\n\n4. Sumber dana perpanjangan/pelunasan pembiayaan berasal dari pendapatan yang sah/legal dan tidak terkait pencucian uang (money laundering)\n\n5. Bank berhak melakukan pemeriksaan terhadap kebenaran data yang Saya berikan\n\n6. Saya telah mendapatkan informasi yang cukup mengenai karakteristik produk pembiayaan gadai yang akan Saya gunakan dan Saya telah membaca, mengerti dan memahami segala konsekuensi penggunaan produk pembiayaan yang dimaksud termasuk manfaat, risiko dan biaya yang melekat pada produk pembiayaan gadai emas.\n\n7. Bank atau pihak ketiga yang ditunjuk oleh Bank berhak untuk menggunakan nomor telepon Saya untuk tujuan penawaran produk Bank atau penagihan.\n\n8. Tunduk pada syarat dan ketentuan yang berlaku pada Bank."];

    }
    
    [self presentViewController:popup animated:YES completion:nil];
}

- (void)didAgreedStatment:(int)tagger{
    if(tagger == 1){
        [self.imgCheckboxCustmrAggrmnt setUserInteractionEnabled:YES];
        [self.lblCustmAggremnt setUserInteractionEnabled:YES];
        [self.imgCheckboxCustmrAggrmnt setImage:imageChecked];
        checkAgreementState = YES;
    }else{
        [self.imgCheckboxCustomerStatment setUserInteractionEnabled:YES];
        [self.lblCustomrStatmnt setUserInteractionEnabled:YES];
        [self.imgCheckboxCustomerStatment setImage:imageChecked];
        checkStatementState = YES;
    }
}

- (void) actionCheckStatement{
    if(checkStatementState){
        [self.imgCheckboxCustomerStatment setImage:imageUncheckd];
        checkStatementState = NO;
    }else{
        [self.imgCheckboxCustomerStatment setImage:imageChecked];
        checkStatementState = YES;
    }
}

- (void) actionCheckAgreement{
    if(checkAgreementState){
        [self.imgCheckboxCustmrAggrmnt setImage:imageUncheckd];
        checkAgreementState = NO;
    }else{
        [self.imgCheckboxCustmrAggrmnt setImage:imageChecked];
        checkAgreementState = YES;
    }
}

- (void) getListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
    
    if(listAcct.count != 0 || listAcct != nil){
        NSMutableArray *newData = [[NSMutableArray alloc]init];
        if ([Utility isLanguageID]) {
            [newData addObject:@"Pilih Nomor Rekening"];
        }else{
            [newData addObject:@"Select Account Number"];
        }

        for(NSDictionary *temp in listAcct){
            [newData addObject:[temp valueForKey:@"id_account"]];
        }
        listRekeningPencairan = newData;
    }else{
        
        NSString *strURL = [NSString stringWithFormat:@"request_type=list_account2, customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
    }
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getListAccount];

        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
}

- (BOOL) validate{
    BOOL valid = YES;
    if([self.tfPurpose.text isEqualToString:@""]){
        [self.validatePurpose setHidden:NO];
        valid = NO;
    }else{
        [self.validatePurpose setHidden:YES];
    }
    
    if([self.tfSourceItem.text isEqualToString:@""]){
        [self.validateSourceItem setHidden:NO];
        valid = NO;
    }else{
        [self.validateSourceItem setHidden:YES];
    }
    
    if([self.tfSourcePayment.text isEqualToString:@""]){
        [self.validateSourcePayment setHidden:NO];
        valid = NO;
    }else{
        [self.validateSourcePayment setHidden:YES];
    }
    
    if([self.tfAccount.text isEqualToString:@""]){
        [self.validateAccount setHidden:NO];
        valid = NO;
    }else{
        [self.validateAccount setHidden:YES];
    }
    
    if(!checkKuasaDebetState){
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_KUASA_DEBIT_STATE_ALERT", @"") preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
        valid = NO;
    }
    
    if([self.tfMethod.text isEqualToString:@""]){
        [self.validateMethod setHidden:NO];
        valid = NO;
    }else{
        [self.validateMethod setHidden:YES];
    }
    
    if(!checkAgreementState){
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_CUSTOMER_AGREEMENT_ALERT", @"") preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
        valid = NO;
    }
    if(!checkStatementState){
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:NSLocalizedString(@"GE_CUSTOMER_STATEMENT_ALERT", @"") preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
        valid = NO;
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML1A"] isEqualToString:@"Y"] && [self.tfAML1ANote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML1Note setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML1A"] isEqualToString:@"Y"] && ![self.tfAML1ANote.text isEqualToString:@""]){
        [self.validateAML1Note setHidden:YES];
    }else{
        [self.validateAML1Note setHidden:YES];
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML1B"] isEqualToString:@"Y"] && [self.tfAML1BNote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML1BNote setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML1B"] isEqualToString:@"Y"] && ![self.tfAML1BNote.text isEqualToString:@""]){
        [self.validateAML1BNote setHidden:YES];
    }else{
        [self.validateAML1BNote setHidden:YES];
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML2A"] isEqualToString:@"Y"] && [self.tfAML2ANote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML2ANote setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML2A"] isEqualToString:@"Y"] && ![self.tfAML2ANote.text isEqualToString:@""]){
        [self.validateAML2ANote setHidden:YES];
    }else{
        [self.validateAML2ANote setHidden:YES];
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML2B"] isEqualToString:@"Y"] && [self.tfAML2BNote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML2BNote setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML2B"] isEqualToString:@"Y"] && ![self.tfAML1BNote.text isEqualToString:@""]){
        [self.validateAML2BNote setHidden:YES];
    }else{
        [self.validateAML2BNote setHidden:YES];
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML2C"] isEqualToString:@"Y"] && [self.tfAML2CNote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML2CNote setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML2C"] isEqualToString:@"Y"] && ![self.tfAML2CNote.text isEqualToString:@""]){
        [self.validateAML2CNote setHidden:YES];
    }else{
        [self.validateAML2CNote setHidden:YES];
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML3A"] isEqualToString:@"Y"] && [self.tfAML3ANote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML3ANote setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML3A"] isEqualToString:@"Y"] && ![self.tfAML3ANote.text isEqualToString:@""]){
        [self.validateAML3ANote setHidden:YES];
    }else{
        [self.validateAML3ANote setHidden:YES];
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML3B"] isEqualToString:@"Y"] && [self.tfAML3BNote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML3BNote setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML3B"] isEqualToString:@"Y"] && ![self.tfAML3BNote.text isEqualToString:@""]){
        [self.validateAML3BNote setHidden:YES];
    }else{
        [self.validateAML3BNote setHidden:YES];
    }
    
    if([[dataManager.dataExtra  objectForKey:@"AML3C"] isEqualToString:@"Y"] && [self.tfAML3CNote.text isEqualToString:@""]){
//        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
//        return NO;
        valid = NO;
        [self.validateAML3CNote setHidden:NO];
    }else if([[dataManager.dataExtra  objectForKey:@"AML3C"] isEqualToString:@"Y"] && ![self.tfAML3CNote.text isEqualToString:@""]){
        [self.validateAML3CNote setHidden:YES];
    }else{
        [self.validateAML3CNote setHidden:YES];
    }
    
    if(!valid){
        [Utility showMessage:NSLocalizedString(@"GE_AML_DATA_ALERT", @"") instance:self];
    }
    return valid;
}
@end
