//
//  GEPickupAddressFormViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 16/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface GEPickupAddressFormViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *iconBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;

@property (weak, nonatomic) IBOutlet UIScrollView *viewScroll;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleDetailPickup;
@property (weak, nonatomic) IBOutlet UIView *viewDetailPickup;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleStreet;
@property (weak, nonatomic) IBOutlet UITextView *twStreet;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleRTRW;
@property (weak, nonatomic) IBOutlet UITextField *tfRT;
@property (weak, nonatomic) IBOutlet UITextField *tfRW;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleVillage;
@property (weak, nonatomic) IBOutlet UITextField *tfVillage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleDistric;
@property (weak, nonatomic) IBOutlet UITextField *tfDistrict;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleLandmark;
@property (weak, nonatomic) IBOutlet UIView *viewLandmark;
@property (weak, nonatomic) IBOutlet UITextField *tfLandmarkNo;
@property (weak, nonatomic) IBOutlet UITextView *twLandmarkName;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheckbox;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckboxInfo;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonBack;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@end

NS_ASSUME_NONNULL_END
