//
//  SimulateDetailViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 14/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GESimulateDetailViewController.h"
#import "GadaiEmasConnect.h"
#import "LibGold.h"
#import "Utility.h"
#import "CustomBtn.h"

@interface GESimulateDetailViewController ()<GadaiEmasConnectDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    
    NSArray *listGoldType;
    NSArray *listKaratase;
    NSMutableDictionary *dictData;
    
    NSUserDefaults *userDefaults;
    int state;
    NSInteger idxEdit;
}

@property (weak, nonatomic) IBOutlet UIView *viewSimulate;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleDataDetails;

@property (weak, nonatomic) IBOutlet UIImageView *imageClose;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleGoldType;
@property (weak, nonatomic) IBOutlet UITextField *tfGoldType;
@property (weak, nonatomic) IBOutlet UILabel *goldTypeValidate;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleKaratase;
@property (weak, nonatomic) IBOutlet UITextField *tfKaratase;
@property (weak, nonatomic) IBOutlet UILabel *karataseValidate;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleGoldWeight;
@property (weak, nonatomic) IBOutlet UITextField *tfGoldWeight;
@property (weak, nonatomic) IBOutlet UILabel *goldWeightValidate;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonOK;

@end

@implementation GESimulateDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (@available(iOS 13.0, *)) {
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    self.labelTitleGoldType.text = NSLocalizedString(@"GE_GOLD_TYPE_TITLE", @"");
    self.labelTitleKaratase.text = NSLocalizedString(@"GE_GOLD_KARATASE_TITLE", @"");
    self.labelTitleGoldWeight.text = NSLocalizedString(@"GE_GOLD_WEIGHT_TITLE", @"");
    self.labelTitleDataDetails.text = NSLocalizedString(@"GE_GOLD_DATA_DETAILS", @"");
    
    NSArray *datas = [NSJSONSerialization JSONObjectWithData:[[[userDefaults objectForKey:@"reservasi_gadai"]objectForKey:@"ObjectGadai"] dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];

    listGoldType = datas;
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for(int i = 16; i < 25; i++){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:[NSString stringWithFormat:@"%d Karat",i] forKey:@"title"];
        [dict setValue:[NSString stringWithFormat:@"%d",i] forKey:@"value"];
        [arr addObject:dict];
    }
    listKaratase = arr;
    
    [self.viewSimulate.layer setBorderWidth:1];
    [self.viewSimulate.layer setBorderColor:[[UIColor blackColor]CGColor]];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]init];
    [recognizer addTarget:self action:@selector(actionClose)];
    
    [self.imageClose setUserInteractionEnabled:YES];
    [self.imageClose addGestureRecognizer:recognizer];

    
    [self createPicker:self.tfGoldType withTag:0];
    [self createPicker:self.tfKaratase withTag:1];
    
    [self createToolbarButton:self.tfGoldType];
    [self.goldTypeValidate setText:@""];
    
    [self createToolbarButton:self.tfKaratase];
    [self.karataseValidate setText:@""];
    
    [self createToolbarButton:self.tfGoldWeight];
    [self.goldWeightValidate setText:@""];
    
    self.tfGoldWeight.delegate = self;
    
    [self.tfGoldWeight setKeyboardType:UIKeyboardTypeDecimalPad];
    
    [self.buttonOK addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
    
    if(dictData != nil){
        state = 1;
        [self.tfGoldWeight setText:[dictData objectForKey:@"BeratEmas"]];
        [self.tfGoldType setText:[dictData objectForKey:@"ObjectGadai"]];
        [self.tfKaratase setText:[dictData objectForKey:@"Karatase_title"]];
    }else{
        state = 0;
        dictData = [[NSMutableDictionary alloc]init];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return listGoldType.count;
    }else{
        return listKaratase.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return [listGoldType[row] objectForKey:@"ObjectGadai"];
    }else{
        return [listKaratase[row] objectForKey:@"title"];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        self.tfGoldType.text = [listGoldType[row] objectForKey:@"ObjectGadai"];
        self.goldTypeValidate.text = @"";
        
        [dictData setValue:[listGoldType[row] objectForKey:@"ObjectGadai"] forKey:@"ObjectGadai"];
        [dictData setValue:[listGoldType[row] objectForKey:@"ObjectGadaiCode"] forKey:@"ObjectGadaiCode"];
        
    }else{
        self.tfKaratase.text = [listKaratase[row] objectForKey:@"title"];
        self.karataseValidate.text = @"";
        
        [dictData setValue:[listKaratase[row] objectForKey:@"value"] forKey:@"Karatase"];
        [dictData setValue:[listKaratase[row] objectForKey:@"title"] forKey:@"Karatase_title"];

    }
}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setupEditData:(NSDictionary *)dict withIndex:(NSInteger)idx{
    dictData = (NSMutableDictionary*)dict;
    idxEdit = idx;
}

- (void) actionClose{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)isValid{
    BOOL valid = YES;
    
    if([self.tfGoldType.text isEqualToString:@""]){
        [Utility isLanguageID] ? [self.goldTypeValidate setText:@"Jenis emas harus diisi"] : [self.goldTypeValidate setText:@"Type of Gold cannot be empty"];
        valid = NO;
    }else{
        [self.goldTypeValidate setText:@""];
    }
    
    if([self.tfKaratase.text isEqualToString:@""]){
        [Utility isLanguageID] ? [self.karataseValidate setText:@"Karatase harus diisi"] : [self.karataseValidate setText:@"Karatase cannot be empty"];
        valid = NO;
    }else{
        [self.karataseValidate setText:@""];
    }
    
    if([self.tfGoldWeight.text isEqualToString:@""]){
        [Utility isLanguageID] ? [self.goldWeightValidate setText:@"Berat Emas harus diisi"] : [self.goldWeightValidate setText:@"Weight of Gold cannot be empty"];
        valid = NO;
    }else{
        [self.goldWeightValidate setText:@""];
    }
    
    if(![self.tfGoldWeight.text isEqualToString:@""] && [self.tfGoldWeight.text doubleValue] < 1){
        [Utility isLanguageID] ? [self.goldWeightValidate setText:@"Berat Emas tidak boleh 0 gram"] : [self.goldWeightValidate setText:@"Weight of Gold cannot be 0 gram"];
        valid = NO;
    }
    
    return valid;
}

- (void) actionOK{
    [dictData setValue:@"1" forKey:@"JumlahEmas"];
//    [dictData setValue:self.tfGoldWeight.text forKey:@"BeratEmas"];
    double valueBeratEmas = [[self.tfGoldWeight.text stringByReplacingOccurrencesOfString:@"," withString:@"."]doubleValue];
    [dictData setValue:[NSString stringWithFormat:@"%.2f",valueBeratEmas] forKey:@"BeratEmas"];
    
//    if([self.tfGoldType.text isEqualToString:@""] ||
//       [self.tfKaratase.text isEqualToString:@""] ||
////       [self.tfGoldTotal.text isEqualToString:@""] ||
//       [self.tfGoldWeight.text isEqualToString:@""]){
//
//        [Utility showMessage:NSLocalizedString(@"DATA_IS_NOT_VALID",@"") instance:self];
//
//    }else if([self.tfGoldWeight.text doubleValue] < 1){
//        [Utility isLanguageID] ? [Utility showMessage:@"Berat Emas tidak boleh 0 gram" instance:self] : [Utility showMessage:@"Weight of Gold cannot be 0 gram " instance:self];
//    }
    if([self isValid]){
        [delegate updateData:dictData withState:state andIndex:idxEdit];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void) doneAction{
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.tfGoldWeight){
        NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        double newValue = [newText doubleValue];
        if(newValue > 1000){
            [Utility isLanguageID] ? [Utility showMessage:@"maximum 1000 gram" instance:self] : [Utility showMessage:@"maximum 1000 gram" instance:self];
        }
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.tfGoldWeight){
        if([self.tfGoldWeight.text isEqualToString:@""]){
            [Utility isLanguageID] ? [self.goldWeightValidate setText:@"Berat Emas harus diisi"] : [self.goldWeightValidate setText:@"Weight of Gold cannot be empty"];
        }else{
            [self.goldWeightValidate setText:@""];
        }
        
        if(![self.tfGoldWeight.text isEqualToString:@""] && [self.tfGoldWeight.text doubleValue] < 1){
            [Utility isLanguageID] ? [self.goldWeightValidate setText:@"Berat Emas tidak boleh 0 gram"] : [self.goldWeightValidate setText:@"Weight of Gold cannot be 0 gram"];
        }
        
    }
}
@end
