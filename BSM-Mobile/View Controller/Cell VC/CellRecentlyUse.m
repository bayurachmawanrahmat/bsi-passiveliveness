//
//  CellRecentlyUse.m
//  BSM-Mobile
//
//  Created by Alikhsan on 02/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CellRecentlyUse.h"

@implementation CellRecentlyUse

@synthesize lblContentOne = _lblContentOne;
@synthesize lblContentTwo = _lblContentTwo;
@synthesize imgNext = _imgNext;
@synthesize vwBase = _vwBase;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.vwBase addSubview:self.imgNext];
        [self.vwBase addSubview:self.lblContentOne];
        [self.vwBase addSubview:self.lblContentTwo];
        [self.contentView addSubview:self.vwBase];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UILabel *)lblContentOne
{
    if (_lblContentOne) return _lblContentOne;
    _lblContentOne = [[UILabel alloc]init];
    _lblContentOne.font = [UIFont fontWithName:const_font_name1 size:15];
    _lblContentOne.textColor = [UIColor blackColor];
    
    return _lblContentOne;
}

-(UILabel *)lblContentTwo
{
    if (_lblContentTwo) return _lblContentTwo;
    _lblContentTwo = [[UILabel alloc]init];
    _lblContentTwo.font = [UIFont fontWithName:const_font_name1 size:14];
    _lblContentTwo.textColor = [UIColor grayColor];
    return _lblContentTwo;
}

-(UIImageView *) imgNext{
     if (_imgNext) return _imgNext;
       _imgNext = [[UIImageView alloc]init];
       [_imgNext setImage:[UIImage imageNamed:@"baseline_keyboard_arrow_right_black_24pt"]];
       _imgNext.tintColor = UIColorFromRGB(const_color_primary);
       return _imgNext;
}

-(UIView *)vwBase{
    if (_vwBase) return _vwBase;
    _vwBase = [[UIView alloc]init];
    _vwBase.layer.cornerRadius = 8.0f;
    
    return _vwBase;
}

@end
