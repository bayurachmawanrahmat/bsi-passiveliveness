//
//  CellRadioSelection.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 23/03/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "CellRadioSelection.h"

@implementation CellRadioSelection

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        UIImage *img = [[UIImage imageNamed:@"radio_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.radio setImage:img];
        [self.radio setTintColor:UIColorFromRGB(const_color_secondary)];
    } else {
        [self.radio setImage:[UIImage imageNamed:@"radio_unselect"]];
    }
}

@end
