//
//  CellPortoTwo.m
//  BSM-Mobile
//
//  Created by Alikhsan on 07/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CellPortoTwo.h"

@implementation CellPortoTwo

@synthesize cardView = _cardView;
@synthesize lblTitle = _lblTitle;
@synthesize lblTitleKey1 = _lblTitleKey1;
@synthesize lblTitleKey2 = _lblTitleKey2;
@synthesize lblTitleKey3 = _lblTitleKey3;
@synthesize lblTitleKey4 = _lblTitleKey4;
@synthesize lblCtnVal1 = _lblCtnVal1;
@synthesize lblCtnVal2 = _lblCtnVal2;
@synthesize lblCtnVal3 = _lblCtnVal3;
@synthesize lblCtnVal4 = _lblCtnVal4;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.cardView addSubview:self.lblTitle];
        [self.cardView addSubview:self.lblTitleKey1];
        [self.cardView addSubview:self.lblTitleKey2];
        [self.cardView addSubview:self.lblTitleKey3];
        [self.cardView addSubview:self.lblTitleKey4];
        [self.cardView addSubview:self.lblCtnVal1];
        [self.cardView addSubview:self.lblCtnVal2];
        [self.cardView addSubview:self.lblCtnVal3];
        [self.cardView addSubview:self.lblCtnVal4];
        [self.contentView addSubview:self.cardView];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UIView *)cardView{
    if (_cardView) return _cardView;
    _cardView = [[UIView alloc]init];
    _cardView.layer.cornerRadius = 10.0f;
    _cardView.layer.shadowColor = [[UIColor grayColor]CGColor];
    _cardView.layer.shadowOffset = CGSizeMake(0, 2);
    _cardView.layer.shadowOpacity = 0.5f;
    _cardView.backgroundColor = [UIColor whiteColor];
    
    return _cardView;
}

-(UILabel *)lblTitle
{
    if (_lblTitle) return _lblTitle;
    _lblTitle = [[UILabel alloc]init];
    [_lblTitle setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblTitle.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblTitle;
}

-(UILabel *)lblTitleKey1
{
    if (_lblTitleKey1) return _lblTitleKey1;
    _lblTitleKey1 = [[UILabel alloc]init];
    [_lblTitleKey1 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblTitleKey1.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblTitleKey1;
}

-(UILabel *)lblTitleKey2
{
    if (_lblTitleKey2) return _lblTitleKey2;
    _lblTitleKey2 = [[UILabel alloc]init];
    [_lblTitleKey2 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblTitleKey2.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblTitleKey2;
}

-(UILabel *)lblTitleKey3
{
    if (_lblTitleKey3) return _lblTitleKey3;
    _lblTitleKey3 = [[UILabel alloc]init];
    [_lblTitleKey3 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblTitleKey3.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblTitleKey3;
}

-(UILabel *)lblTitleKey4
{
    if (_lblTitleKey4) return _lblTitleKey4;
    _lblTitleKey4 = [[UILabel alloc]init];
    [_lblTitleKey4 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblTitleKey4.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblTitleKey4;
}


-(UILabel *)lblCtnVal1
{
    if (_lblCtnVal1) return _lblCtnVal1;
    _lblCtnVal1 = [[UILabel alloc]init];
    [_lblCtnVal1 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblCtnVal1.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblCtnVal1;
}

-(UILabel *)lblCtnVal2
{
    if (_lblCtnVal2) return _lblCtnVal2;
    _lblCtnVal2 = [[UILabel alloc]init];
    [_lblCtnVal2 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblCtnVal2.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblCtnVal2;
}

-(UILabel *)lblCtnVal3
{
    if (_lblCtnVal3) return _lblCtnVal3;
    _lblCtnVal3 = [[UILabel alloc]init];
    [_lblCtnVal3 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblCtnVal3.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblCtnVal3;
}

-(UILabel *)lblCtnVal4
{
    if (_lblCtnVal4) return _lblCtnVal4;
    _lblCtnVal4 = [[UILabel alloc]init];
    [_lblCtnVal4 setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblCtnVal4.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    //    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblCtnVal4;
}




@end
