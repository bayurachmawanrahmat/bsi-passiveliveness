//
//  CellComplain.h
//  BSM-Mobile
//
//  Created by ARS on 27/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellComplain : UITableViewCell
@property (nonatomic) UIView *cardView;
@property (nonatomic) UIView *cardViewChild;

@property (nonatomic) UILabel *lblTitle1;
@property (nonatomic) UILabel *lblTitle2;
@property (nonatomic) UILabel *lblTitle3;
@property (nonatomic) UILabel *lblTitle4;
@property (nonatomic) UILabel *lblTitle5;
@property (nonatomic) UILabel *lblTitle6;
@property (nonatomic) UILabel *lblTitle7;
@property (nonatomic) UIImageView *imgIcon;
@property (nonatomic) UIImageView *imgNext;
@property (nonatomic) UIView *circle;

@end

NS_ASSUME_NONNULL_END
