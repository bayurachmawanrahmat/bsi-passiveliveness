//
//  CellPortoOne.h
//  BSM-Mobile
//
//  Created by Alikhsan on 07/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellPortoOne : UITableViewCell
@property (nonatomic)  UILabel *lblContentOne;
@property (nonatomic)  UILabel *lblContentTwo;
@property (nonatomic)  UILabel *lblContentTree;
@property (nonatomic)  UILabel *lblContentFour;
@property (nonatomic)  UILabel *lblContentFive;
@property (nonatomic)  UILabel *lblContentSix;
@property (nonatomic)  UILabel *lblContentSeven;
@property (nonatomic)  UILabel *lblContentEight;
@property (nonatomic)  UILabel *lblContentNine;
@property (nonatomic)  UILabel *lblContentTen;
@property (nonatomic)  UILabel *lblContentEleven;
@property (nonatomic)  UILabel *lblContentTwelve;

@end

NS_ASSUME_NONNULL_END
