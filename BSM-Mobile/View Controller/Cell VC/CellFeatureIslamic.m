//
//  CellFeatureIslamic.m
//  BSM-Mobile
//
//  Created by BSM on 9/27/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CellFeatureIslamic.h"

@implementation CellFeatureIslamic

@synthesize cardView = _cardView;
@synthesize lblTitle = _lblTitle;
//@synthesize imgIcon = _imgIcon;
@synthesize imgNext = _imgNext;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.cardView addSubview:self.lblTitle];
//        [self.cardView addSubview:self.imgIcon];
        [self.cardView addSubview:self.imgNext];
        [self.contentView addSubview:self.cardView];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UIView *)cardView{
    if (_cardView) return _cardView;
    _cardView = [[UIView alloc]init];
    _cardView.layer.cornerRadius = 8.0f;
    _cardView.layer.shadowColor = [[UIColor grayColor]CGColor];
    _cardView.layer.shadowOffset = CGSizeMake(0, 2);
    _cardView.layer.shadowOpacity = 0.5f;
    _cardView.backgroundColor = [UIColor whiteColor];
    
    return _cardView;
}

-(UILabel *)lblTitle
{
    if (_lblTitle) return _lblTitle;
    _lblTitle = [[UILabel alloc]init];
    _lblTitle.font = [UIFont fontWithName:const_font_name1 size:15];
    
    return _lblTitle;
}

//-(UIImageView *)imgIcon
//{
//    if (_imgIcon) return _imgIcon;
//    _imgIcon = [[UIImageView alloc]init];
//    [_imgIcon setImage:[UIImage imageNamed:@"icon_wisdom_header.png"]];
//
//    return _imgIcon;
//}

-(UIImageView *)imgNext
{
    if (_imgNext) return _imgNext;
    _imgNext = [[UIImageView alloc]init];
    [_imgNext setImage:[UIImage imageNamed:@"baseline_keyboard_arrow_right_black_24pt"]];
    _imgNext.tintColor = UIColorFromRGB(const_color_primary);
    return _imgNext;
}

@end
