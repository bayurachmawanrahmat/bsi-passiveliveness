//
//  CellJuzAmmaFilter.h
//  BSM-Mobile
//
//  Created by Macbook Air on 27/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellJuzAmmaFilter : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbNoSurah;
@property (weak, nonatomic) IBOutlet UILabel *lbNamaSurah;
@property (weak, nonatomic) IBOutlet UILabel *lbDescSurah;
@property (weak, nonatomic) IBOutlet UIImageView *imgBorderMoeslem;


@end

