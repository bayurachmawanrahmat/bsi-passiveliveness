//
//  CellAsmaulHusna.h
//  BSM-Mobile
//
//  Created by BSM on 9/25/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellAsmaulHusna : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBorder;
@property (weak, nonatomic) IBOutlet UILabel *lblNoAsmaul;
@property (weak, nonatomic) IBOutlet UIView *vwTxtAsmaul;
@property (weak, nonatomic) IBOutlet UILabel *lblAsmaul;
@property (weak, nonatomic) IBOutlet UILabel *lblArtiAsmaul;
@property (weak, nonatomic) IBOutlet UILabel *lblArabAsmaul;

@end

NS_ASSUME_NONNULL_END
