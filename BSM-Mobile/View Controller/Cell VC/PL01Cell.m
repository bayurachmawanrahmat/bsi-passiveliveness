//
//  PL01Cell.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/12/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PL01Cell.h"

@implementation PL01Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        [self.checkImage setImage:[UIImage imageNamed:@"radio_select"]];
        [self.label1 setTextColor:[UIColor blackColor]];
        [self.label2 setTextColor:[UIColor blackColor]];
    } else {
        [self.checkImage setImage:[UIImage imageNamed:@"radio_unselect"]];
        [self.label1 setTextColor:UIColorFromRGB(const_color_gray)];
        [self.label2 setTextColor:UIColorFromRGB(const_color_gray)];
    }
    // Configure the view for the selected state
}

@end
