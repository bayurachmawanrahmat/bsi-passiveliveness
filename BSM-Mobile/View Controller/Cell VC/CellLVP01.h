//
//  CellLVP01.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 13/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface CellLVP01 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonAction;

@end

NS_ASSUME_NONNULL_END
