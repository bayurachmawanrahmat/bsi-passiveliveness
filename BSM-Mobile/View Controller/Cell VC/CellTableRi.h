//
//  CellTableRi.h
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 29/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellTableRi : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTop;
@property (weak, nonatomic) IBOutlet UILabel *lblBtn;
@property (weak, nonatomic) IBOutlet UIImageView *icon;


@end

