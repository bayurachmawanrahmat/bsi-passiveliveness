//
//  CellListSurah.h
//  BSM-Mobile
//
//  Created by BSM on 9/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellListSurah : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblSurahName;
@property (weak, nonatomic) IBOutlet UILabel *lblTranslateSurah;


@end

NS_ASSUME_NONNULL_END
