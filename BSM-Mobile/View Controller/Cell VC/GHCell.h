//
//  GHCell.h
//  BSM-Mobile
//
//  Created by ARS on 24/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GHCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *col1;
@property (weak, nonatomic) IBOutlet UILabel *Col2;
@property (weak, nonatomic) IBOutlet UILabel *col3;
@property (weak, nonatomic) IBOutlet UILabel *col4;
@property (weak, nonatomic) IBOutlet UILabel *col5;

@end

NS_ASSUME_NONNULL_END
