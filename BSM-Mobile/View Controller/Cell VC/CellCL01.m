//
//  CellCL01.m
//  BSM-Mobile
//
//  Created by ARS on 30/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CellCL01.h"

@implementation CellCL01

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        [self.checkImage setImage:[UIImage imageNamed:@"ic_checkbox_active"]];
    } else {
        [self.checkImage setImage:[UIImage imageNamed:@"ic_checkbox_inactive"]];
    }
    // Configure the view for the selected state
}

@end
