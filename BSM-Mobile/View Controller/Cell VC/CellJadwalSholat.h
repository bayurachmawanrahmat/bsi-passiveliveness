//
//  CellJadwalSholat.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 22/03/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellJadwalSholat : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

NS_ASSUME_NONNULL_END
