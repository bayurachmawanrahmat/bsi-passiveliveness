//
//  ComplainCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ComplainCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *vwContainer;
@property (weak, nonatomic) IBOutlet UIView *vwDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblYear;
@property (weak, nonatomic) IBOutlet UILabel *lblComplain;
@property (weak, nonatomic) IBOutlet UILabel *lblComplainDetail;

@end

NS_ASSUME_NONNULL_END
