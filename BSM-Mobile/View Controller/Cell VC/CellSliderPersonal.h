//
//  CellSliderPersonal.h
//  BSM-Mobile
//
//  Created by Macbook Air on 23/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CellSliderPersonal : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitleWaktu;
//@property (weak, nonatomic) IBOutlet UILabel *lblDescWaktu;
@property (weak, nonatomic) IBOutlet UILabel *lblWaktu;
@property (weak, nonatomic) IBOutlet UISlider *slideWaktu;

@end


