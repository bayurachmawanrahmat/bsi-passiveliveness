//
//  CellTS03.m
//  BSM-Mobile
//
//  Created by ARS on 16/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CellTS03.h"

@implementation CellTS03

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.vwBse.layer.cornerRadius = 10.f;
    self.vwBse.layer.masksToBounds = NO;
    self.vwBse.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    self.vwBse.layer.borderWidth = 0.2;
    self.vwBse.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    self.vwBse.layer.shadowOffset = CGSizeMake(2.f, .2f);
    self.vwBse.layer.shadowRadius = 3.0f;
    self.vwBse.layer.shadowOpacity = 0.8f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
