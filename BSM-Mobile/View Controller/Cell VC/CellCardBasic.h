//
//  CellCardBasic.h
//  BSM-Mobile
//
//  Created by Alikhsan on 21/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellCardBasic : UITableViewCell
@property (nonatomic) UIView *cardView;

@property (nonatomic) UILabel *lblTitle;
@property (nonatomic) UIImageView *imgIcon;
@property (nonatomic) UIImageView *imgNext;

@end

NS_ASSUME_NONNULL_END
