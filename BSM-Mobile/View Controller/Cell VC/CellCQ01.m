//
//  CellLV11Card.m
//  BSM-Mobile
//
//  Created by ARS on 08/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CellCQ01.h"

@implementation CellCQ01

@synthesize imageView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.vwBase.layer.cornerRadius = 10.f;
    self.vwBase.layer.masksToBounds = NO;
    self.vwBase.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    self.vwBase.layer.borderWidth = 0.2;
    self.vwBase.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    self.vwBase.layer.shadowOffset = CGSizeMake(2.f, .2f);
    self.vwBase.layer.shadowRadius = 3.0f;
    self.vwBase.layer.shadowOpacity = 0.8f;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
