//
//  PL01Cell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 04/12/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PL01Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@end

NS_ASSUME_NONNULL_END
