//
//  CellLV11.m
//  BSM-Mobile
//
//  Created by ARS on 07/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CellLV11.h"

@implementation CellLV11{
    NSString* mState;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if(selected && [mState isEqualToString:@"active"]){
        [self.imageView1 setImage:[UIImage imageNamed:@"ic_checkbox_active"]];
    }else{
        [self.imageView1 setImage:[UIImage imageNamed:@"ic_checkbox_inactive"]];
    }
    // Configure the view for the selected state
}

- (void)setState:(NSString *)states{
    mState = states;
}
@end
