//
//  CellLV09.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 08/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellLV09 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgShow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgHeight;

@end

NS_ASSUME_NONNULL_END
