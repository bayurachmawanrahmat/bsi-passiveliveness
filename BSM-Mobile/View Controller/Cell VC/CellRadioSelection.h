//
//  CellRadioSelection.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 23/03/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellRadioSelection : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *radio;

@end

NS_ASSUME_NONNULL_END
