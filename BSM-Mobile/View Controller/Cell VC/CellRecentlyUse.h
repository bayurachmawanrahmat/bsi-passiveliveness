//
//  CellRecentlyUse.h
//  BSM-Mobile
//
//  Created by Alikhsan on 02/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellRecentlyUse : UITableViewCell

@property (nonatomic) UILabel *lblContentOne;
@property (nonatomic) UILabel *lblContentTwo;
@property (nonatomic) UIImageView *imgNext;
@property (nonatomic) UIView *vwBase;

@end

NS_ASSUME_NONNULL_END
