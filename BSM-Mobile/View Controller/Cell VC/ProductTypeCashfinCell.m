//
//  ProductTypeCashfinCell.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 01/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "ProductTypeCashfinCell.h"

@implementation ProductTypeCashfinCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.vwBackground.layer setBorderWidth:1];
    [self.vwBackground.layer setBorderColor:[UIColorFromRGB(const_color_primary)CGColor]];
    [self.vwBackground.layer setCornerRadius:16];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
