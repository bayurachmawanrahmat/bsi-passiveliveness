//
//  ProductTypeCashfinCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 01/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductTypeCashfinCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *vwBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblProductDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblProductSubname;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSubmission;

@end

NS_ASSUME_NONNULL_END
