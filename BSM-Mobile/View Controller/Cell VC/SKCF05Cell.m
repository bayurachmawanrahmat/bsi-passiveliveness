//
//  SKCF05Cell.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "SKCF05Cell.h"

@implementation SKCF05Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.viewContent.layer.borderWidth = 2;
//    self.viewContent.layer.cornerRadius = 10;
//    self.viewContent.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    _viewContent.layer.cornerRadius = 10.0f;
    _viewContent.layer.shadowColor = [[UIColor grayColor]CGColor];
    _viewContent.layer.shadowOffset = CGSizeMake(0, 2);
    _viewContent.layer.shadowOpacity = 0.5f;
    _viewContent.backgroundColor = [UIColor whiteColor];
    _viewContent.layer.shadowRadius = 5.0;
    _viewContent.layer.masksToBounds = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
