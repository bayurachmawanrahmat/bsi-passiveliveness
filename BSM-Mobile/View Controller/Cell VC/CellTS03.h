//
//  CellTS03.h
//  BSM-Mobile
//
//  Created by ARS on 16/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellTS03 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fromAccount;
@property (weak, nonatomic) IBOutlet UILabel *fromAccountTitle;
@property (weak, nonatomic) IBOutlet UILabel *toAccountTItle;
@property (weak, nonatomic) IBOutlet UILabel *toAccountName;
@property (weak, nonatomic) IBOutlet UILabel *toAccount;
@property (weak, nonatomic) IBOutlet UILabel *amountTransfer;
@property (weak, nonatomic) IBOutlet UILabel *descTitle;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UILabel *dateTransaction;
@property (weak, nonatomic) IBOutlet UIView *vwBse;

@end

NS_ASSUME_NONNULL_END
