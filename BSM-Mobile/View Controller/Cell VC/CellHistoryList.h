//
//  CellHistoryList.h
//  BSM-Mobile
//
//  Created by Alikhsan on 02/12/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellHistoryList : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelNama;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UIView *viewColorStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelTanggal;
@property (weak, nonatomic) IBOutlet UILabel *labelNominal;
@property (weak, nonatomic) IBOutlet UILabel *labelNorek;

@end

NS_ASSUME_NONNULL_END
