//
//  CellTS01.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 08/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellTS01 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerAccount;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UILabel *lblNominal;
@property (weak, nonatomic) IBOutlet UIImageView *imgNext;

@end

NS_ASSUME_NONNULL_END
