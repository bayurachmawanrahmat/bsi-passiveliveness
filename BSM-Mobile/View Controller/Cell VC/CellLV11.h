//
//  CellLV11.h
//  BSM-Mobile
//
//  Created by ARS on 07/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellLV11 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIButton *buttonDesc;
- (void) setState : (NSString*) states;

@end

NS_ASSUME_NONNULL_END
