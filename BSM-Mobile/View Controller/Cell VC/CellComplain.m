//
//  CellComplain.m
//  BSM-Mobile
//
//  Created by ARS on 27/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CellComplain.h"

@implementation CellComplain
@synthesize cardView = _cardView;
@synthesize cardViewChild = _cardViewChild;
@synthesize lblTitle1 = _lblTitle1;
@synthesize lblTitle2 = _lblTitle2;
@synthesize lblTitle3 = _lblTitle3;
@synthesize lblTitle4 = _lblTitle4;
@synthesize lblTitle5 = _lblTitle5;
@synthesize lblTitle6 = _lblTitle6;
@synthesize lblTitle7 = _lblTitle7;
@synthesize imgIcon = _imgIcon;
@synthesize imgNext = _imgNext;
@synthesize circle = _circle;



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.cardViewChild addSubview:self.lblTitle1];
        [self.cardViewChild addSubview:self.lblTitle2];
        [self.cardViewChild addSubview:self.lblTitle3];
        [self.cardViewChild addSubview:self.lblTitle4];
        [self.cardViewChild addSubview:self.lblTitle5];
        [self.cardViewChild addSubview:self.lblTitle6];
        [self.cardViewChild addSubview:self.lblTitle7];
//        [self.circle addSubview:self.imgIcon];
        [self.cardView addSubview:self.imgNext];
        [self.cardView addSubview:self.cardViewChild];
//        [self.cardView addSubview:self.circle];
        [self.contentView addSubview:self.cardView];
//        self.contentView.size
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UIView *)cardView{
    if (_cardView) return _cardView;
    _cardView = [[UIView alloc]init];
    _cardView.layer.cornerRadius = 8.0f;
    _cardView.layer.shadowColor = [[UIColor grayColor]CGColor];
    _cardView.layer.shadowOffset = CGSizeMake(0, 2);
    _cardView.layer.shadowOpacity = 0.5f;
    _cardView.backgroundColor = [UIColor whiteColor];
    
    return _cardView;
}

-(UIView *)cardViewChild{
    if (_cardViewChild) return _cardViewChild;
    _cardViewChild = [[UIView alloc]init];
    _cardViewChild.layer.cornerRadius = 5.0f;
//    _cardViewChild.backgroundColor = [UIColor colorWithRed:0.94 green:0.94 blue:0.95 alpha:1.0];;
    _cardViewChild.backgroundColor = UIColorFromRGB(const_highlight_primary);
    
    return _cardViewChild;
}

-(UILabel *)lblTitle1
{
    if (_lblTitle1) return _lblTitle1;
    _lblTitle1 = [[UILabel alloc]init];
    _lblTitle1.font = [UIFont fontWithName:const_font_name3 size:17];
    _lblTitle1.textColor = UIColorFromRGB(const_color_primary);
    return _lblTitle1;
}
-(UILabel *)lblTitle2
{
    if (_lblTitle2) return _lblTitle2;
    _lblTitle2 = [[UILabel alloc]init];
    _lblTitle2.font = [UIFont fontWithName:const_font_name1 size:15];
    _lblTitle2.textColor = UIColorFromRGB(const_color_primary);
    return _lblTitle2;
}
-(UILabel *)lblTitle3
{
    if (_lblTitle3) return _lblTitle3;
    _lblTitle3 = [[UILabel alloc]init];
    _lblTitle3.font = [UIFont fontWithName:const_font_name3 size:17];
    
    return _lblTitle3;
}
-(UILabel *)lblTitle4
{
    if (_lblTitle4) return _lblTitle4;
    _lblTitle4 = [[UILabel alloc]init];
    _lblTitle4.font = [UIFont fontWithName:const_font_name1 size:14];
    
    return _lblTitle4;
}
-(UILabel *)lblTitle5
{
    if (_lblTitle5) return _lblTitle5;
    _lblTitle5 = [[UILabel alloc]init];
    _lblTitle5.font = [UIFont fontWithName:const_font_name1 size:14];
    
    return _lblTitle5;
}

-(UILabel *)lblTitle6
{
    if (_lblTitle6) return _lblTitle6;
    _lblTitle6 = [[UILabel alloc]init];
    _lblTitle6.font = [UIFont fontWithName:const_font_name1 size:14];
    _lblTitle6.textColor = UIColorFromRGB(const_color_primary);
    return _lblTitle6;
}

-(UILabel *)lblTitle7
{
    if (_lblTitle7) return _lblTitle7;
    _lblTitle7 = [[UILabel alloc]init];
    _lblTitle7.font = [UIFont fontWithName:const_font_name1 size:14];
    _lblTitle7.textColor = [UIColor blackColor];
    return _lblTitle7;
}

-(UIImageView *)imgIcon
{
    if (_imgIcon) return _imgIcon;
    _imgIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 17 * 2, 2 * 17)];
    _imgIcon.tintColor = UIColorFromRGB(const_color_primary);
    return _imgIcon;
}

-(UIImageView *)imgNext
{
    if (_imgNext) return _imgNext;
    _imgNext = [[UIImageView alloc]init];
    [_imgNext setImage:[UIImage imageNamed:@"baseline_keyboard_arrow_right_black_24pt"]];
    _imgNext.tintColor = UIColorFromRGB(const_color_primary);
    return _imgNext;
}

-(UIView *)circle
{
    if (_circle) return _circle;
    _circle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 17 * 2, 2 * 17)];
    _circle.backgroundColor = [UIColor yellowColor];
    _circle.layer.cornerRadius = 17;
    _circle.layer.masksToBounds = YES;
    return _circle;
}

@end
