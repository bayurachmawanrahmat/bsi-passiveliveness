//
//  SKCF05Cell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/02/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SKCF05Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *titleKodePemesanan;
@property (weak, nonatomic) IBOutlet UILabel *titleNominalPemesanan;
@property (weak, nonatomic) IBOutlet UILabel *titleAvailableRedeem;
@property (weak, nonatomic) IBOutlet UILabel *kodePemesanan;
@property (weak, nonatomic) IBOutlet UILabel *nominalPemesanan;
@property (weak, nonatomic) IBOutlet UILabel *availableRedeem;

@end

NS_ASSUME_NONNULL_END
