//
//  CellPortoOne.m
//  BSM-Mobile
//
//  Created by Alikhsan on 07/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "CellPortoOne.h"

@implementation CellPortoOne

@synthesize lblContentOne = _lblContentOne;
@synthesize lblContentTwo  = _lblContentTwo;
@synthesize lblContentTree  = _lblContentTree;
@synthesize lblContentFour  = _lblContentFour;
@synthesize lblContentFive  = _lblContentFive;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.lblContentOne];
        [self.contentView addSubview:self.lblContentTwo];
        [self.contentView addSubview:self.lblContentTree];
        [self.contentView addSubview:self.lblContentFour];
        [self.contentView addSubview:self.lblContentFive];
        [self.contentView addSubview:self.lblContentSix];
        [self.contentView addSubview:self.lblContentSeven];
        [self.contentView addSubview:self.lblContentEight];
        [self.contentView addSubview:self.lblContentNine];
        [self.contentView addSubview:self.lblContentTen];
        [self.contentView addSubview:self.lblContentEleven];
        [self.contentView addSubview:self.lblContentTwelve];
        
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UILabel *)lblContentOne
{
    if (_lblContentOne) return _lblContentOne;
    _lblContentOne = [[UILabel alloc]init];
    [_lblContentOne setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentOne.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
//    [_lblContentOne setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentOne;
}

-(UILabel *)lblContentTwo
{
    if (_lblContentTwo) return _lblContentTwo;
    _lblContentTwo = [[UILabel alloc]init];
    [_lblContentTwo setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentTwo.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
//    [_lblContentTwo setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentTwo;
}

-(UILabel *)lblContentTree
{
    if (_lblContentTree) return _lblContentTree;
    _lblContentTree = [[UILabel alloc]init];
    [_lblContentTree setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentTree.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
//    [_lblContentTree setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentTree;
}

-(UILabel *)lblContentFour
{
    if (_lblContentFour) return _lblContentFour;
    _lblContentFour = [[UILabel alloc]init];
    [_lblContentFour setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentFour.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
//    [_lblContentFour setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
//
    return _lblContentFour;
}

-(UILabel *)lblContentFive
{
    if (_lblContentFive) return _lblContentFive;
    _lblContentFive = [[UILabel alloc]init];
    [_lblContentFive setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentFive.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
//    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentFive;
}

-(UILabel *)lblContentSix
{
    if (_lblContentSix) return _lblContentSix;
    _lblContentSix = [[UILabel alloc]init];
    [_lblContentSix setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentSix.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentSix;
}

-(UILabel *)lblContentSeven
{
    if (_lblContentSeven) return _lblContentSeven;
    _lblContentSeven = [[UILabel alloc]init];
    [_lblContentSeven setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentSeven.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentSeven;
}


-(UILabel *)lblContentEight
{
    if (_lblContentEight) return _lblContentEight;
    _lblContentEight = [[UILabel alloc]init];
    [_lblContentEight setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentEight.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentEight;
}

-(UILabel *)lblContentNine
{
    if (_lblContentNine) return _lblContentNine;
    _lblContentNine = [[UILabel alloc]init];
    [_lblContentNine setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentNine.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentNine;
}

-(UILabel *)lblContentTen
{
    if (_lblContentTen) return _lblContentTen;
    _lblContentTen = [[UILabel alloc]init];
    [_lblContentTen setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentTen.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentTen;
}

-(UILabel *)lblContentEleven
{
    if (_lblContentEleven) return _lblContentEleven;
    _lblContentEleven = [[UILabel alloc]init];
    [_lblContentEleven setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentEleven.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentEleven;
}

-(UILabel *)lblContentTwelve
{
    if (_lblContentTwelve) return _lblContentTwelve;
    _lblContentTwelve = [[UILabel alloc]init];
    [_lblContentTwelve setTranslatesAutoresizingMaskIntoConstraints:NO];
    _lblContentTwelve.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    //    [_lblContentFive setContentCompressionResistancePriority:500 forAxis:UILayoutConstraintAxisHorizontal];
    
    return _lblContentTwelve;
}


@end
