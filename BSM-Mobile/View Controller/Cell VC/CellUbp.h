//
//  CellUbp.h
//  BSM-Mobile
//
//  Created by BSM on 9/26/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellUbp : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@end
