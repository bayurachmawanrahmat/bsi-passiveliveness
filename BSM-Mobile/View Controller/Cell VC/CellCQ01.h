//
//  CellLV11Card.h
//  BSM-Mobile
//
//  Created by ARS on 08/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellCQ01 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *title1;
@property (weak, nonatomic) IBOutlet UILabel *smallTitle;
@property (weak, nonatomic) IBOutlet UILabel *biggerSubTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageArrow;
@property (weak, nonatomic) IBOutlet UIView *vwBase;

@end

NS_ASSUME_NONNULL_END
