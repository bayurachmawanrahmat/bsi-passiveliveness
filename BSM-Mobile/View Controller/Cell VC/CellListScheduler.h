//
//  CellListScheduler.h
//  BSM-Mobile
//
//  Created by Alikhsan on 25/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellListScheduler : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNama;
@property (weak, nonatomic) IBOutlet UILabel *lblNorek;
@property (weak, nonatomic) IBOutlet UILabel *lblNominal;
@property (weak, nonatomic) IBOutlet UIImageView *imgNext;
@property (weak, nonatomic) IBOutlet UILabel *lblTypeInterval;

@end

NS_ASSUME_NONNULL_END
