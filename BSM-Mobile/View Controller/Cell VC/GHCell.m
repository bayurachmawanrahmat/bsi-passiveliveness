//
//  GHCell.m
//  BSM-Mobile
//
//  Created by ARS on 24/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GHCell.h"

@implementation GHCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
