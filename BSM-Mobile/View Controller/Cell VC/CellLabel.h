//
//  CellLabel.h
//  BSM-Mobile
//
//  Created by ARS on 11/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellLabel : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@end

NS_ASSUME_NONNULL_END
