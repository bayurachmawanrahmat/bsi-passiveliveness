//
//  CellFeatureIslamic.h
//  BSM-Mobile
//
//  Created by BSM on 9/27/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellFeatureIslamic : UITableViewCell
@property (nonatomic) UIView *cardView;

@property (nonatomic) UILabel *lblTitle;
//@property (nonatomic) UIImageView *imgIcon;
@property (nonatomic) UIImageView *imgNext;
@end

NS_ASSUME_NONNULL_END
