//
//  CellCL02.h
//  BSM-Mobile
//
//  Created by ARS on 30/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellCL02 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;
@property (weak, nonatomic) IBOutlet UILabel *label1;

@end

NS_ASSUME_NONNULL_END
