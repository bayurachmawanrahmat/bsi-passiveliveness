//
//  CellJuzAmmaSingle.h
//  BSM-Mobile
//
//  Created by BSM on 9/26/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellJuzAmmaSingle : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNoAyat;
@property (weak, nonatomic) IBOutlet UIImageView *imgBorder;
@property (weak, nonatomic) IBOutlet UILabel *lblAyat;
@property (weak, nonatomic) IBOutlet UILabel *lblTranslater;

@end

NS_ASSUME_NONNULL_END
