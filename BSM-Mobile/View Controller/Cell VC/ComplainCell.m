//
//  ComplainCell.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "ComplainCell.h"

@implementation ComplainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.vwDate.layer.cornerRadius = 5;
    self.vwDate.layer.backgroundColor = [UIColorFromRGB(const_highlight_primary)CGColor];
    self.lblDate.textColor = UIColorFromRGB(const_color_primary);
    self.lblMonth.textColor = UIColorFromRGB(const_color_primary);
    self.lblYear.textColor = UIColorFromRGB(const_color_primary);
    self.contentView.backgroundColor = UIColorFromRGB(const_color_basecolor);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
