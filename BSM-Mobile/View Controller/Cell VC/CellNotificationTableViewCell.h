//
//  CellNotificationTableViewCell.h
//  BSM-Mobile
//
//  Created by Alikhsan on 10/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface CellNotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconNotif;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNotif;
@property (weak, nonatomic) IBOutlet UILabel *lblDescNotif;
@property (weak, nonatomic) IBOutlet UIButton *btnTblCheck;


@end


