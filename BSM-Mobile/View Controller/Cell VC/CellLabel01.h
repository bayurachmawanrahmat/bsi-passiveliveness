//
//  CellLabel01.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 22/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellLabel01 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UIView *vwBase;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vwBaseHeight;

@end

NS_ASSUME_NONNULL_END
