//
//  CellPortoTwo.h
//  BSM-Mobile
//
//  Created by Alikhsan on 07/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellPortoTwo : UITableViewCell
@property (nonatomic) UIView *cardView;

@property (nonatomic) UILabel *lblTitle;
@property (nonatomic) UILabel *lblTitleKey1;
@property (nonatomic) UILabel *lblTitleKey2;
@property (nonatomic) UILabel *lblTitleKey3;
@property (nonatomic) UILabel *lblTitleKey4;
@property (nonatomic) UILabel *lblCtnVal1;
@property (nonatomic) UILabel *lblCtnVal2;
@property (nonatomic) UILabel *lblCtnVal3;
@property (nonatomic) UILabel *lblCtnVal4;


@end

NS_ASSUME_NONNULL_END
