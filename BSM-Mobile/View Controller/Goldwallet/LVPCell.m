//
//  LVPCell.m
//  BSM-Mobile
//
//  Created by ARS on 21/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "LVPCell.h"

@implementation LVPCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
