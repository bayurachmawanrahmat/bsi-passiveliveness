//
//  GCF01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 28/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GCF01ViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface GCF01ViewController (){
    NSString *mid;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation GCF01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    [self.textView setEditable:NO];
    [self.textView setFont:[UIFont fontWithName:const_font_name1 size:16]];
    
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];

    if([lang isEqualToString:@"id"]){
        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
    } else {
        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    if(self.jsonData !=nil && [self.jsonData objectForKey:@"content"] && ![[[self.jsonData objectForKey:@"content"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]isEqualToString:@""]){
            [self.textView setText:[self.jsonData valueForKey:@"content"]];
        }
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        mid = [self.jsonData valueForKey:@"menu_id"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        NSString *urlParam = [self.jsonData valueForKey:@"url_parm"];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,weight,amount,buy_price,sell_price,input_type,branch_code,email",urlParam] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
      }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
    //adding data extra for minimal pemesanan
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){

            NSError* error;
            NSString* source = [jsonObject valueForKey:@"response"];
//            if([source containsString:@"</font>"]){
            source = [source stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
//            }
            NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
        
            NSMutableAttributedString* str = [[NSMutableAttributedString alloc]
                  initWithData: [source dataUsingEncoding:NSUTF8StringEncoding]
                       options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
            documentAttributes: nil
                         error: &error];
            
            [final appendAttributedString:str];
            [self.textView setAttributedText:final];
            [self.textView setFont:[UIFont fontWithName:const_font_name1 size:15]];
            //<font color='red'>Text</font>
//            [self.textView setText:[jsonObject valueForKey:@"response"]];
            
            [[[DataManager sharedManager]dataExtra]setObject:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
            if([jsonObject valueForKey:@"min_pesan"]){
                [[[DataManager sharedManager]dataExtra]setObject:[jsonObject valueForKey:@"min_pesan"] forKey:@"min_pesan"];
            }
            
            if([jsonObject objectForKey:@"share"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"share":[jsonObject valueForKey:@"share"] }];
            }

        } else {
            if ([jsonObject valueForKey:@"response"]){
                if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
                    NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                        [self actionCancel:self];
                    }]];
                    if (@available(iOS 13.0, *)) {
                        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                    }
                    [self presentViewController:alert animated:YES completion:nil];


                } else {
                    NSString * msg = [jsonObject valueForKey:@"response"];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                        [self actionCancel:self];
                    }]];
                    if (@available(iOS 13.0, *)) {
                        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                    }
                    [self presentViewController:alert animated:YES completion:nil];
                }
            } else {
                NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                    [self actionCancel:self];
                }]];
                if (@available(iOS 13.0, *)) {
                    [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
        }
        DLog(@"%@", jsonObject);
    }
}

- (IBAction)actionCancel:(id)sender {
    if([mid isEqualToString:@"00110"]){
        [self backToR];
    }else{
        [self gotoGold];
    }
}
- (IBAction)actionNext:(id)sender {
    [self openNextTemplate];
}

@end
