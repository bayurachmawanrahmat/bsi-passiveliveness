//
//  GoldUtil.m
//  BSM-Mobile
//
//  Created by ARS on 07/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "LibGold.h"
#import "Utility.h"
#import "DataManager.h"

@implementation LibGold

+ (NSString *) goldToSent: (double)value withPrice: (double) pricePerGram{
    double totalGr = value / pricePerGram;
    NSString *totalGram = [NSString stringWithFormat:@"%0.4f", totalGr];
    totalGram = [totalGram stringByReplacingOccurrencesOfString:@"." withString:@""];
    
    return totalGram;
}

+ (NSString *) goldToSentFromGram: (double)value{
    double total = value * 10000;
    NSString *totalGram = [NSString stringWithFormat:@"%.f", total];
    
    return totalGram;
}

+ (NSString *) amountToSent: (double)value withPrice: (double) pricePerGram{
    double totalPrice = value * pricePerGram;

    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:2];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:totalPrice]];
    numberString = [numberString stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    return numberString;
}

+ (NSString *)calculateToGold : (double)value withPrice:(double)pricePerGram{
    double totalGold = value/pricePerGram;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:4];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:totalGold]];
    numberString = [numberString stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    return numberString;
}

+ (NSString *)calculateToMoney : (double)value withPrice:(double)pricePerGram{
    double totalPrice = value * pricePerGram;
    NSString *price = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:totalPrice]];
    return price;
}

+(NSString *) getCurrencyForm:(double)value{
    
    value = round(value);
    
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    [numberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormat setGroupingSeparator:@","];
    
    return [numberFormat stringFromNumber:[NSNumber numberWithDouble:value]];
}

//+(NSString *) getMillionValue:(double)value{
//    double millionValue = value/1000000;
//    if([[[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
//        return [NSString stringWithFormat:@"%.f Juta",millionValue];
//    }
//    return [NSString stringWithFormat:@"%.f Millions",millionValue];
//}

+(NSString *) getCurrencyForm:(double)value withDigits:(NSInteger) digits{
    
//    value = round(value);
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    [numberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormat setMaximumFractionDigits:digits];
    [numberFormat setMinimumFractionDigits:digits];
    [numberFormat setGroupingSeparator:@"."];
    
    return [numberFormat stringFromNumber:[NSNumber numberWithDouble:value]];
}

+(NSString *) getCurrencyFormNoRound:(double)value{
        
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    [numberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormat setGroupingSeparator:@"."];
    
    return [numberFormat stringFromNumber:[NSNumber numberWithDouble:value]];
}

+(NSNumberFormatter*) setCurrency{
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    [numberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormat setGroupingSeparator:@","];
    
    return numberFormat;
}

+(NSString*) getGramFormOfValue:(double)value{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:4];
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    
    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithDouble:value]];
    numberString = [numberString stringByReplacingOccurrencesOfString:@"," withString:@"."];
        
    return numberString;
}

+(NSString*) getMillionValue:(double)value{
    NSString *lang = [[[NSUserDefaults standardUserDefaults]objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        if((value/10000000) >= 1){
            return [NSString stringWithFormat:@"%.f Juta",value/1000000];
        }else if((value/1000) >= 1){
            return [NSString stringWithFormat:@"%.f Ribu",value/1000];
        }else{
            return [NSString stringWithFormat:@"%@",[Utility formatCurrencyValue:value]];
        }
    }else{
        if((value/10000000) >= 1){
            return [NSString stringWithFormat:@"%.f Millions",value/1000000];
        }else if((value/1000) >= 1){
            return [NSString stringWithFormat:@"%.f Thousands",value/1000];
        }else{
            return [NSString stringWithFormat:@"%@",[Utility formatCurrencyValue:value]];
        }
    }
}

@end
