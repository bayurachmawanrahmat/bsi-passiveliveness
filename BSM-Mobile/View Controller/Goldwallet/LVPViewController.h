//
//  LVPViewController.h
//  BSM-Mobile
//
//  Created by ARS on 21/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LVPViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
