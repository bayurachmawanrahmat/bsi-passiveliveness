//
//  HStackViewCell.m
//  BSM-Mobile
//
//  Created by Amini on 29/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "HStackViewCell.h"

@implementation HStackViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
