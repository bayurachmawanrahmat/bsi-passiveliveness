//
//  TarikEmasLVViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "EmasLVViewController.h"
#import "PopInfoViewController.h"
#import "CellLabel.h"
#import "Utility.h"

@interface EmasLVViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *listTarikEmas;
    NSUserDefaults *userDefaults;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;
@property (weak, nonatomic) IBOutlet UITableView *listView;
@property (weak, nonatomic) IBOutlet UIButton *buttonBackBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topconstant;

@end

@implementation EmasLVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if([Utility isLanguageID]){
        [self.labelTitleBar setText:@"Tarik Fisik Emas"];
    }else{
        [self.labelTitleBar setText:@"Gold Physical Pull"];
    }
    
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.buttonBackBar addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBackBar setImage:[UIImage imageNamed:@"baseline_keyboard_arrow_left_black_48pt"] forState:UIControlStateNormal];
    
    self.listView.delegate = self;
    self.listView.dataSource = self;
    self.listView.tableFooterView = [[UIView alloc]init];
    
    [self.listView registerNib:[UINib nibWithNibName:@"CellLabel" bundle:nil] forCellReuseIdentifier:@"CELLLABEL"];
    
    listTarikEmas = [self.jsonData objectForKey:@"action"];
    [self.labelTitleBar setText:[self.jsonData valueForKey:@"title"]];
    [self.listView reloadData];
    
    
    if(![[userDefaults objectForKey:@"gpop_gc01"] isEqualToString:@"YES"] && ![userDefaults objectForKey:@"gpop_gc01"]){
        PopInfoViewController *popInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"POPINFOGOLD"];
        [popInfo setPosition:2];
        [self presentViewController:popInfo animated:YES completion:nil];
    }
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listTarikEmas.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSArray *selectedMenu = [listTarikEmas objectAtIndex:indexPath.row];
    [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
    
    @try {
        [super openNextTemplate];
    } @catch (NSException *exception) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self actionBack];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellLabel *cell = (CellLabel*)[tableView dequeueReusableCellWithIdentifier:@"CELLLABEL"];
    
    NSDictionary *data = [[listTarikEmas objectAtIndex:indexPath.row]objectAtIndex:1];
    [cell.labelText setText:[data valueForKey:@"title"]];
    
    return cell;
}


@end
