//
//  PFViewController.m
//  BSM-Mobile
//
//  Created by ARS on 21/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PFViewController.h"
#import "LVPViewController.h"
#import "CameraOverlayController.h"
#import "CamOverlayController.h"
#import "GCamOverlayView.h"
#import "CameraOverlayView.h"
#import "LibGold.h"
#import "Utility.h"
#import "Styles.h"

@interface PFViewController ()<GCamOverlayDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSDictionary *nCode;
    UIImagePickerController* imagePicker;
    NSString *titleLabel;
    NSArray *listAccount;
    BOOL listAccountIsRequested;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UITextField *textField1;

@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UITextField *textField2;

@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *subLabel3;

@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation PFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    [Styles setTopConstant:_topConstraint];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self setByCode];
    imagePicker.delegate = self;
    _textField2.delegate = self;
}

- (void)setByCode{
    if([[nCode objectForKey:@"code"]isEqualToString:@"1"]){
        _label1.text = @"Nomor Pokok Wajib Pajak";
        self.view3.hidden = YES;
        self.view2.hidden = NO;
        _textField1.keyboardType = UIKeyboardTypeNumberPad;
        if([language isEqualToString:@"id"]){
            _textField1.placeholder = @"Masukkan Nomor NPWP";
        }else{
            _textField1.placeholder = @"Enter Your NPWP Number";
        }
        
        NSString *state = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"verifiedNPWP"];
//        if(![state isEqualToString:@"N"]){
        if([state isEqualToString:@"Y"] || [state isEqualToString:@"W"]){
            NSString *verified = @"";
            NSString *decline = @"";
            NSString *waiting = @"";
            
            if([language isEqualToString:@"id"]){
                verified = @"Terverifikasi";
                decline = @"Ditolak";
                waiting = @"Menunggu Verifikasi";
            }else{
                verified = @"Verified";
                decline = @"Rejected";
                waiting = @"Waiting for Verification";
            }
            
            if([state isEqualToString:@"Y"]){
                _subLabel3.text = verified;
            }else if([state isEqualToString:@"W"]){
                _subLabel3.text = waiting;
            }else if([state isEqualToString:@"R"]){
                _subLabel3.text = decline;
            }
            
            self.view2.hidden = YES;
            self.view3.hidden = NO;
            _textField1.text = @"xxxxxxxxxxxxxxx";
            _textField1.enabled = NO;
            _textField1.textColor = [UIColor lightGrayColor];
            _button.enabled = NO;
            _button.hidden = YES;
        }
    }else if([[nCode objectForKey:@"code"]isEqualToString:@"2"]){
        self.view2.hidden = YES;
        self.view3.hidden = YES;
        if([language isEqualToString:@"id"]){
            _label1.text = @"Nomor Rekening Pembayaran";
        }else{
            _label1.text = @"Payment Account Number";
        }
        _textField1.text = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"rekeningCBS"];
        [self createPicker:_textField1 withTag:0];
    }else if([[nCode objectForKey:@"code"]isEqualToString:@"3"]){
        _label1.text = @"Nomor Pokok Wajib Pajak";
        self.view3.hidden = YES;
        self.view2.hidden = NO;
        _textField1.keyboardType = UIKeyboardTypeNumberPad;
        if([language isEqualToString:@"id"]){
            _textField1.placeholder = @"Masukkan Nomor NPWP";
        }else{
            _textField1.placeholder = @"Enter Your NPWP Number";
        }
        
//        NSString *state = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"verifiedNPWP"];
//        if(![state isEqualToString:@"N"]){
//            NSString *verified = @"";
//            NSString *decline = @"";
//            NSString *waiting = @"";
//
//            if([language isEqualToString:@"id"]){
//                verified = @"Terverifikasi";
//                decline = @"Ditolak";
//                waiting = @"Menunggu Verifikasi";
//            }else{
//                verified = @"Verified";
//                decline = @"Rejected";
//                waiting = @"Waiting for Verification";
//            }
//
//            if([state isEqualToString:@"Y"]){
//                _subLabel3.text = verified;
//            }else if([state isEqualToString:@"W"]){
//                _subLabel3.text = waiting;
//            }else if([state isEqualToString:@"R"]){
//                _subLabel3.text = decline;
//            }
//
//            self.view2.hidden = YES;
//            self.view3.hidden = NO;
//            _textField1.text = @"xxxxxxxxxxxxxxx";
//            _textField1.enabled = NO;
//            _textField1.textColor = [UIColor lightGrayColor];
//            _button.enabled = NO;
//            _button.hidden = YES;
//        }
    }
    [self createToolbarButton:_textField1];
    [_backBtn addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [_button addTarget:self action:@selector(actionButton) forControlEvents:UIControlEventTouchUpInside];

    
    [Styles setStyleTextFieldBottomLine:_textField1];
    [Styles setStyleTextFieldBottomLineWithRightImage:_textField2 imageNamed:@"ic_shutter.png"];
    [Styles setStyleButton:_button];
//    [Styles setStyleDisabledButtonFor:_button];
//    _button.enabled = NO;
        
//    [_subLabel3.layer setCornerRadius:15];
//    [_subLabel3.layer setMasksToBounds:true];

    [self langChange];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([[nCode objectForKey:@"code"]isEqualToString:@"1"] || [[nCode objectForKey:@"code"]isEqualToString:@"3"]){
        [self actionTakePhoto];
    }
}

- (void) actionTakePhoto{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    CamOverlayController *overlayController = [[CamOverlayController alloc] init];

    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    imagePicker.showsCameraControls = false;
    imagePicker.allowsEditing = false;

    GCamOverlayView *cameraView = (GCamOverlayView *)overlayController.view;
    cameraView.frame = imagePicker.view.frame;
    cameraView.descriptionLabel.text = NSLocalizedString(@"DESC_NPWP_POSITION", @"");
    cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_ktp@2x.png"];
    cameraView.titleLabel.text = titleLabel;
    cameraView.delegate = self;
    
    imagePicker.cameraOverlayView = cameraView;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)didTakePhoto:(GCamOverlayView *)overlayView{
    [imagePicker takePicture];
}

- (void)didBack:(GCamOverlayView *)overlayView{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *base64Imge = [Utility compressAndEncodeToBase64String:image withMaxSizeInKB:40];
    
    [dataManager.dataExtra setValue:base64Imge forKey:@"npwp_photo"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [_textField2 setText:[NSString stringWithFormat:@"NPWP_%@.jpg",[dateFormatter stringFromDate:[NSDate date]]]];
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void) langChange{
    if([language isEqualToString:@"id"]){
//        _label1.text = @"Nomor Pokok Wajib Pajak";
//        _textField1.placeholder = @"Masukkan Nomor NPWP anda";
        _label2.text = @"Foto NPWP";
        _textField2.placeholder = @"Unggah Foto NPWP anda";
        
        _label3.text = @"Status :";
        titleLabel = [nCode objectForKey:@"label_id"];
        _labelTitle.text = titleLabel;
        
        [_button setTitle:@"OK" forState:UIControlStateNormal];
    }else{
//        _textField1.placeholder = @"Input Your NPWP Number";
        _label2.text = @"NPWP Photo";
        _textField2.placeholder = @"Upload Your  NPWP Photo";
        
        _label3.text = @"Status :";
        titleLabel = [nCode objectForKey:@"label_en"];

        _labelTitle.text = titleLabel;
        
        [_button setTitle:@"OK" forState:UIControlStateNormal];

    }
}

- (void)doneEditButtonTapped:(id)done{
    if(_textField1.isFirstResponder && ![_textField1.text isEqualToString:@""]){
        if([[nCode objectForKey:@"code"] isEqualToString:@"1"] || [[nCode objectForKey:@"code"] isEqualToString:@"3"]){ //data npwp
            if(_textField1.text.length == 15){
                if([_textField1.text isEqualToString:@"000000000000000"] ||
                   [_textField1.text doubleValue] == 0){
                    NSString* msg = @"";
                    if([language isEqualToString:@"id"])
                    {
                        msg = @"Nomor NPWP tidak valid";
                    }else{
                        msg = @"NPWP Number is not valid";
                    }
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                        
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }else if([_textField1.text isEqualToString:@"000000000000000"] ||
               [_textField1.text doubleValue] == 0){
                NSString* msg = @"";
                if([language isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP tidak valid";
                }else{
                    msg = @"NPWP Number is not valid";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                NSString *msg = @"";
                if(_textField1.text.length != 15){
                    if([language isEqualToString:@"id"])
                    {
                        msg = @"Nomor NPWP harus 15 digit";
                    }else{
                        msg = @"NPWP Number Should have 15 digits";
                    }
                }

                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
    [self.view endEditing:YES];
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}
- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (void)setCode:(NSDictionary *)code{
    nCode = code;
}

- (void) getData{
    if([[nCode objectForKey:@"code"]isEqualToString:@"1"]){
        
    }else if([[nCode objectForKey:@"code"]isEqualToString:@"2"]){
        [self getListAccount];
    }
}

- (void) actionButton{
    
    if([[nCode objectForKey:@"code"] isEqualToString:@"1"] || [[nCode objectForKey:@"code"] isEqualToString:@"3"]){ //data npwp
        if(_textField1.text.length == 15 && ![_textField2.text isEqualToString:@""]){
            
            if([_textField1.text isEqualToString:@"000000000000000"] || [_textField1.text doubleValue] == 0){
                NSString* msg = @"";
                if([language isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP tidak valid";
                }else{
                    msg = @"NPWP Number is not valid";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                [dataManager.dataExtra setValue:_textField1.text forKey:@"npwp"];
                NSString *strURL = [nCode objectForKey:@"url_param"];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
            }
            
        }else{
            NSString *msg = @"";
            
            if([_textField1.text isEqualToString:@""]){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Anda belum menambahkan nomor NPWP";
                }else{
                    msg = @"You have not fill NPWP Numbers";
                }
            }
            else if(_textField1.text.length != 15){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP harus 15 digit";
                }else{
                    msg = @"NPWP Number Should have 15 digits";
                }
            }else
            if([_textField2.text isEqualToString:@""]){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Ambil Foto NPWP Anda untuk Unggah";
                }else{
                    msg = @"Take Photo NPWP to Upload";
                }
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }

    }else if([[nCode objectForKey:@"code"] isEqualToString:@"2"]){ //data rekening
        
        if([_textField1.text isEqualToString:@""]){
            NSString *msg = @"";
            if([language isEqualToString:@"id"])
            {
                msg = @"Nomor Rekening Tidak Boleh Kosong";
            }else{
                msg = @"Account Number cannot be empty";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            [dataManager.dataExtra setValue:_textField1.text forKey:@"id_account"];
            NSString *strURL = [nCode objectForKey:@"url_param"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
        }

    }
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self getData];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listAccount.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return listAccount[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(row == 0){
        self.textField1.text = @"";
    }else{
        self.textField1.text = [listAccount objectAtIndex:row];
    }
}

- (void) getListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
    
    if(listAcct.count != 0 || listAcct != nil){
        NSMutableArray *newData = [[NSMutableArray alloc]init];
        if ([language isEqualToString:@"id"]) {
            [newData addObject:@"Pilih Nomor Rekening"];
        }else{
            [newData addObject:@"Select Account Number"];
        }
        
        
        for(NSDictionary *temp in listAcct){
            [newData addObject:[temp valueForKey:@"id_account"]];
        }
        listAccount = newData;
    }else{
        if(listAccountIsRequested){
            NSString *msg = @"";
            if([language isEqualToString:@"id"]){
                msg = @"Anda Tidak Memiliki Rekening yang dibutuhkan";
            }else{
                msg = @"you dont have any account for this transaction";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            listAccountIsRequested = YES;
            
            NSString *strURL = [NSString stringWithFormat:@"request_type=list_account1, customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
        }

    }
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account1"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getListAccount];

        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
    
    if([requestType isEqualToString:@"gold_setting"]){
        NSString *response = [jsonObject objectForKey:@"response"];
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
            NSString *msg;
            if([[nCode objectForKey:@"code"]isEqualToString:@"1"] || [[nCode objectForKey:@"code"]isEqualToString:@"3"]){
                if([language isEqualToString:@"id"]){
                    msg  = [NSString stringWithFormat:@"Update NPWP Berhasil"];
                }else{
                     msg = [NSString stringWithFormat:@"Update NPWP is Success"];
                }
                
            }else if([[nCode objectForKey:@"code"]isEqualToString:@"2"]){
                if([language isEqualToString:@"id"]){
                    msg  = [NSString stringWithFormat:@"Akun Rekening Pembayaran Anda Berhasil di Ganti Ke Nomor %@", [dataDict objectForKey:@"rekeningCBS"]];
                }else{
                     msg = [NSString stringWithFormat:@"Your Payment Account have success to change to  %@", [dataDict objectForKey:@"rekeningCBS"]];
                }
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,response] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
    
    
}

@end
