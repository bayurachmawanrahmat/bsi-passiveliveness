//
//  GCamOverlayView.m
//  BSM-Mobile
//
//  Created by ARS on 24/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GCamOverlayView.h"

@implementation GCamOverlayView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)hitBackButton:(id)sender {
    [_delegate didBack:self];
}

- (IBAction)hitSwitchButton:(id)sender {
    [_delegate switchCameraDevice:self];
}

- (IBAction)hitTakePhoto:(id)sender {
    [_delegate didTakePhoto:self];
}
@end
