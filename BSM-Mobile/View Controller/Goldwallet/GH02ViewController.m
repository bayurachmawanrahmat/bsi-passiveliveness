//
//  GH02ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 24/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GH02ViewController.h"
#import "LibGold.h"
#import "Styles.h"
#import "HStackViewCell.h"

@interface GH02ViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *listData;
    NSUserDefaults *userDefaults;

    CGFloat colWidth;
    int colNumber;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;

@property (weak, nonatomic) IBOutlet UIView *vwInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelAccountNo;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelSaldo;
@property (weak, nonatomic) IBOutlet UILabel *labelPeriod;
@property (weak, nonatomic) IBOutlet UILabel *labelDateRange;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewWidth;

@property (weak, nonatomic) IBOutlet UITableView *tablview;

@property (weak, nonatomic) IBOutlet UIButton *bottomButton;

@end

@implementation GH02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];

    colWidth = 85;
    colNumber = 5;

    
    [Styles setStyleButton:_bottomButton];
    [Styles setStyleRoundedView:_vwInfo];
    
    [_labelTitleBar setText:[self.jsonData valueForKey:@"title"]];
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    [_bottomButton setTitle:@"OK" forState:UIControlStateNormal];
    [_bottomButton addTarget:self action:@selector(backtogoldhome) forControlEvents:UIControlEventTouchUpInside];
    
    [_tablview registerNib:[UINib nibWithNibName:@"HStackViewCell" bundle:nil] forCellReuseIdentifier:@"HStackViewCell"];
    [_tablview setDelegate:self];
    [_tablview setDataSource:self];
    [_tablview setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
//
    [_labelAccountNo setText:[[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"]];
    [_labelAccountNo setFont:[UIFont fontWithName:@"Lato-Bold" size:15]];
    
    [_labelName setText:[[userDefaults objectForKey:@"gold_info"]objectForKey:@"nama"]];
    

//    NSString *saldoNAverage = [NSString stringWithFormat:@"Saldo : %@ Gram\n %@ : %@",
//                               [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"],
//                               lang(@"E_EMAS_AVERAGEBUY_LABEL"),
//                               [Utility localFormatCurrencyValue:[[[userDefaults objectForKey:@"gold_info"]valueForKey:@"avgBalanceEmas"]doubleValue] showSymbol:true]];
//    [_labelSaldo setText:saldoNAverage];

    NSString *saldo = [NSString stringWithFormat:@"Saldo : %@ Gram",
                               [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]];
    
    [_labelSaldo setText:saldo];

    [_labelDateRange setText:[[dataManager.dataExtra objectForKey:@"list_trans"] stringByReplacingOccurrencesOfString:@"|" withString:@" s.d "]];
    
    [self requesting];
}

- (void)viewDidAppear:(BOOL)animated{
    _tableViewWidth.constant = colWidth*colNumber;
}

- (void) backtogoldhome{
    [self gotoGold];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, colWidth*colNumber, 40)];
    headerView.backgroundColor = [UIColor whiteColor];

    CGFloat widthLabel = colWidth;
    CGFloat heightLabel = 40;
    
    // enchance
//    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
//        [headerView addSubview:[self createUILabelForTableHeader:@"Tanggal / Waktu" tableWidth:widthLabel height:heightLabel posCol:0]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Kode Transaksi" tableWidth:widthLabel height:heightLabel posCol:1]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Jenis Transaksi" tableWidth:widthLabel height:heightLabel posCol:2]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Harga Per Gram" tableWidth:widthLabel height:heightLabel posCol:3]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Jumlah" tableWidth:widthLabel height:heightLabel posCol:4]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Saldo" tableWidth:widthLabel height:heightLabel posCol:5]];
//    }else{
//        [headerView addSubview:[self createUILabelForTableHeader:@"Date / Time" tableWidth:widthLabel height:heightLabel posCol:0]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Transaction Code" tableWidth:widthLabel height:heightLabel posCol:1]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Type of Transaction" tableWidth:widthLabel height:heightLabel posCol:2]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Price per Gram" tableWidth:widthLabel height:heightLabel posCol:3]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Total" tableWidth:widthLabel height:heightLabel posCol:4]];
//        [headerView addSubview:[self createUILabelForTableHeader:@"Saldo" tableWidth:widthLabel height:heightLabel posCol:5]];
//    }
    
    //before enchance
    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        [headerView addSubview:[self createUILabelForTableHeader:@"Tanggal Transaksi" tableWidth:widthLabel height:heightLabel posCol:0]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Kode Transaksi" tableWidth:widthLabel height:heightLabel posCol:1]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Jenis Transaksi" tableWidth:widthLabel height:heightLabel posCol:2]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Jumlah" tableWidth:widthLabel height:heightLabel posCol:3]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Saldo" tableWidth:widthLabel height:heightLabel posCol:4]];
    }else{
        [headerView addSubview:[self createUILabelForTableHeader:@"Transaction Date" tableWidth:widthLabel height:heightLabel posCol:0]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Transaction Code" tableWidth:widthLabel height:heightLabel posCol:1]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Type of Transaction" tableWidth:widthLabel height:heightLabel posCol:2]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Total" tableWidth:widthLabel height:heightLabel posCol:3]];
        [headerView addSubview:[self createUILabelForTableHeader:@"Saldo" tableWidth:widthLabel height:heightLabel posCol:4]];
    }

    return headerView;
}

- (UILabel*) createUILabelForTableHeader:(NSString *) text tableWidth:(CGFloat)width height: (CGFloat)height posCol: (CGFloat) pos{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(width*pos, 0, width, height)];
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = [UIFont fontWithName:const_font_name3 size:13];
    label.textColor = [UIColor blackColor];

    return label;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HStackViewCell *cell = (HStackViewCell*)[tableView dequeueReusableCellWithIdentifier:@"HStackViewCell"];
        
    cell.stackview.alignment = UIStackViewAlignmentCenter;
    cell.stackview.distribution = UIStackViewDistributionFillEqually;
    
    if(cell.stackview.subviews.count == 0){
        NSInteger idx = indexPath.row;
        
        [self addLabelToStackView:cell.stackview setText:[listData[idx] objectForKey:@"tanggalTransaksi"]];
        [self addLabelToStackView:cell.stackview setText:[listData[idx] objectForKey:@"FTCBS"]];
        [self addLabelToStackView:cell.stackview setText:[listData[idx] objectForKey:@"typeTransaksi"]];

        
//        NSString *hargaPerGram = [Utility localFormatCurrencyValue:[[listData[idx] objectForKey:@"hargaPerGram"]doubleValue] showSymbol:YES];
//
//        [self addLabelToStackView:cell.stackview setText:hargaPerGram];
        [self addLabelToStackView:cell.stackview setText:[listData[idx] objectForKey:@"nominalTransaksi"]];
        
        double saldoTotal = 0;
        double saldoBefore =  [[listData[idx] objectForKey:@"SaldoBefore"] doubleValue];
        double nominalTrx =  [[listData[idx] objectForKey:@"nominalTransaksi"] doubleValue];
    
        if([[listData[idx] objectForKey:@"debetORCredit"] isEqualToString:@"-"]){
            saldoTotal = saldoBefore - nominalTrx;
        }else{
            saldoTotal = saldoBefore + nominalTrx;
        }
        

        [self addLabelToStackView:cell.stackview setText:[NSString stringWithFormat:@"%.4f",saldoTotal]];
    }
    
    return cell;
}

- (void) addLabelToStackView:(UIStackView*)stackView setText:(NSString*)text{
    UILabel *label = [[UILabel alloc]init];
    [label setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [label setText:text];
    [label setNumberOfLines:0];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    [stackView addArrangedSubview:label];
        
    label.translatesAutoresizingMaskIntoConstraints = false;
    [label.widthAnchor constraintEqualToConstant:85].active = YES;
}


- (void) requesting{
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,gold_account",[self.jsonData valueForKey:@"url_parm"]] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"gold_history"]){
        NSString *response = [jsonObject objectForKey:@"response"];
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
            options:NSJSONReadingAllowFragments
              error:&error];
            NSString *data = [dataDict objectForKey:@"data"];
            NSArray *dataArr = [NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
            listData = dataArr;
            
            [_tablview reloadData];

        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,response] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

@end
