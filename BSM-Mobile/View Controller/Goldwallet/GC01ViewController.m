//
//  GC01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 01/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GC01ViewController.h"
#import "GC02ViewController.h"
#import "PopInfoViewController.h"
#import "GSearchBranchViewController.h"
#import "LibGold.h"
#import "Utility.h"
#import "Styles.h"

@interface GC01ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, GSearchBranchDelegate>{
    
    NSArray *goldFractionData;
    NSArray *listAccount;
    NSArray *listBranch;

    
    UIColor *yellowColor;
    
    NSUserDefaults *userDefaults;
    
    NSString *language;
    NSString *alertMsg1;
    NSString *alertMsg2;
    NSString *alertMsg3;
    NSString *alertMsg4;
    NSString *alertMsg5;
    NSString *alertMsg6;
    NSString *alertMsg7;
    
    NSString *gcLimitMax;
    NSString *gcLimitMin;
    NSString *gcPrice;
    
    BOOL getKCState;
    double cetakFee;
}

@property (weak, nonatomic) IBOutlet UIView *viewSaldoEmas;
@property (weak, nonatomic) IBOutlet UILabel *labelSaldoEmas;

@property (weak, nonatomic) IBOutlet UILabel *lblGoldFractTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfGoldFract;

@property (weak, nonatomic) IBOutlet UILabel *lblGoldPiecesTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfGoldPieces;

@property (weak, nonatomic) IBOutlet UILabel *lblSGCTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSGC;
@property (weak, nonatomic) IBOutlet UIView *vwSGC;

@property (weak, nonatomic) IBOutlet UILabel *lblGCFTitle;
@property (weak, nonatomic) IBOutlet UIView *vwGCF;
@property (weak, nonatomic) IBOutlet UILabel *lblGCF;
@property (weak, nonatomic) IBOutlet UILabel *labelBranchTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfBranch;
@property (weak, nonatomic) IBOutlet UILabel *labelEmailTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

@property (weak, nonatomic) IBOutlet UILabel *lblFeeAccTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfFeeAcc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstrantPrintFee;

@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation GC01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    getKCState = false;
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    [_lblTitle setText: [self.jsonData objectForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];

    gcLimitMax = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumCetak"];
    gcLimitMin = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumCetak"];
    gcPrice = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"hargaJualBSM"];
    cetakFee = [[[userDefaults objectForKey:@"gold_info"]objectForKey:@"cetak.fee"]doubleValue];
    
    [self setupLanguage];
    [self setupLayout];
    [self hideFee];
}

- (void) hideFee{
    self.topConstrantPrintFee.constant = 16;
    self.vwGCF.hidden = YES;
    self.lblGCFTitle.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [self requestCall];
}

- (void) setupLayout{
//    [self.lblGCFTitle setHidden:YES];
//    [self.vwGCF setHidden:YES];
    
    [Styles setTopConstant:_topConstraint];
    
    [self.lblSGC setText:@"0"];
    
    [Styles setStyleTextFieldBottomLine:_tfGoldPieces];
    [self createToolbarButton:_tfGoldPieces];
    [self.tfGoldPieces setKeyboardType:UIKeyboardTypeNumberPad];
    [self.tfGoldPieces addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfGoldFract imageNamed:@"baseline_arrow_drop_down_black_24pt"];
    [self createToolbarButton:_tfGoldFract];
    [self createPicker:_tfGoldFract withTag:0];
    
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfBranch imageNamed:@"baseline_room_black_18pt"];
    [self.tfBranch setDelegate:self];
    
    [Styles setStyleTextFieldBottomLine:_tfFeeAcc];
    [self createToolbarButton:_tfFeeAcc];
    [self createPicker:_tfFeeAcc withTag:1];
    
    [self.tfEmail setKeyboardType:UIKeyboardTypeEmailAddress];
//    [self.tfEmail setText:[userDefaults objectForKey:@"email"]];
    [self.tfEmail setText:[NSUserdefaultsAes getValueForKey:@"email"]];
    
    [Styles setStyleRoundedView:self.viewSaldoEmas];
    [Styles setStyleRoundedView:_vwGCF];
    [Styles setStyleRoundedView:_vwSGC];
//    [Styles setStyleButton:_btnNext];
//    [Styles setStyleButton:_btnCancel];
    
    [self.lblGCF setText:@""];
}

-(void) setupLanguage{
    if([language isEqualToString:@"id"]){
        [self.labelSaldoEmas setText:[NSString stringWithFormat:@"Rekening Emas : %@\nSaldo Emas:%@",[[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"],
        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]]];
        
        alertMsg1 = @"Pilih Pecahan Emas";
        alertMsg2 = @"Jumlah Keping Emas belum di isi";
        alertMsg3 = @"Jumlah Emas anda tidak mencukupi untuk penarikan";
        alertMsg4 = @"Harap Isi Rekening Pembayaran Biaya Cetak";
        alertMsg5 = @"Jumlah Emas yang ingin anda jual kurang dari limit minimum";
        alertMsg6 = @"Jumlah Emas yang ingin anda jual melebihi limit maxiumum";
        alertMsg7 = @"Pilih Cabang Pengambilan Emas";
        [_tfFeeAcc setPlaceholder:@"Masukkan No.Rekening"];
        [_tfGoldFract setPlaceholder:@"Pilih Pecahan Emas yang akan dicetak"];
        [_tfGoldPieces setPlaceholder:@"Masukkan jumlah keping"];
        
        _lblGoldFractTitle.text = @"Pilih Pecahan Emas";
        _lblGoldPiecesTitle.text = @"Jumlah Keping";
        _lblGCFTitle.text = @"Biaya Cetak";
        _lblSGCTitle.text = @"Total Emas dicetak";
        _lblFeeAccTitle.text = @"Masukkan Rekening Pembayaran Biaya Cetak";
        
        [self.btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Batal" forState:UIControlStateNormal];

        
    }else{
        [self.labelSaldoEmas setText:[NSString stringWithFormat:@"Gold Account : %@\nGold Saldo:%@",[[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"],
        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]]];
        
        alertMsg1 = @"Please select Gold Pieces that you want to print";
        alertMsg2 = @"Please fill the amount of Gold Pieces";
        alertMsg3 = @"Your total gold isnt enough for this print";
        alertMsg4 = @"Please fill purchasing account";
        alertMsg5 = @"Amount of Gold you want to sell is less than minimum limit";
        alertMsg6 = @"Amount of Gold you want to sell is exceeds minimum limit";
        alertMsg7 = @"Please select branch for gold pickup";
        [_tfFeeAcc setPlaceholder:@"Enter Account Number here"];
        [_tfGoldFract setPlaceholder:@"Select Gold Fraction here"];
        [_tfGoldPieces setPlaceholder:@"Enter Amount of Fraction"];
        
        _lblGoldFractTitle.text = @"Select Gold Shards";
        _lblGoldPiecesTitle.text = @"Number of Pieces";
        _lblGCFTitle.text = @"Printing Fee";
        _lblSGCTitle.text = @"Total Gold Print";
        _lblFeeAccTitle.text = @"Payment Fee Account";
        
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
}

- (void) requestCall{
    NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

- (void) textFieldDidChange : (UITextField *)textField{
    double totalEmas = [_tfGoldPieces.text doubleValue] * [_tfGoldFract.text doubleValue];
    _lblSGC.text = [NSString stringWithFormat:@"%.f", totalEmas];
//    _lblGCF.text = [NSString stringWithFormat:@"Rp. 70.000"];
    self.lblGCF.text = [LibGold getCurrencyForm:totalEmas*cetakFee withDigits:2];
    
    [dataManager.dataExtra setValue:_tfGoldPieces.text forKey:@"number_of_pieces"];
    [dataManager.dataExtra setValue:[LibGold goldToSentFromGram:totalEmas] forKey:@"weight"];
    [dataManager.dataExtra setValue:[LibGold amountToSent:totalEmas withPrice:[gcPrice doubleValue]] forKey:@"amount"];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == self.tfBranch){
//        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
//        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
//        getKCState = true;
//        if(listBranch == nil){
//            NSString *urlData= @"request_type=gold_cetak,step=info2,menu_id,device,device_type,ip_address,language,date_local,customer_id,latitude,longitude";
//            Connection *conn = [[Connection alloc]initWithDelegate:self];
//            [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
//        }
        [self.view endEditing:YES];
        GC02ViewController *gsearch = [self.storyboard instantiateViewControllerWithIdentifier:@"GC02"];
//        [gsearch setList:listBranch];
        [gsearch setDelegate:self];
        [self presentViewController:gsearch animated:YES completion:nil];
//        [self gotoSelectBranch];
    }
}

- (void) gotoSelectBranch{
    GSearchBranchViewController *gsearch = [self.storyboard instantiateViewControllerWithIdentifier:@"GSearchBranch"];
    [gsearch setList:listBranch];
    [gsearch setDelegate:self];
    [self presentViewController:gsearch animated:YES completion:nil];
}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

-(void)doneEditButtonTapped:(id)done{
    [self.view endEditing:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return goldFractionData.count;
    }
    else{
        return listAccount.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return [goldFractionData[row] objectForKey:@"name"];
    }else{
        return listAccount[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        if(row == 0){
            self.tfGoldFract.text = @"";
        }else{
            self.tfGoldFract.text = [goldFractionData[row] objectForKey:@"name"];
            [dataManager.dataExtra setValue:[goldFractionData[row] objectForKey:@"id_denom"] forKey:@"type_of_pieces"];
        }
    }
    else{
        if(row == 0){
            self.tfFeeAcc.text = @"";
        }else{
            self.tfFeeAcc.text = [listAccount objectAtIndex:row];
            [dataManager.dataExtra setValue:[listAccount objectAtIndex:row] forKey:@"id_account"];
        }

    }
}

- (void) getListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
    
    if(listAcct.count != 0 || listAcct != nil){
        NSMutableArray *newData = [[NSMutableArray alloc]init];
        if ([language isEqualToString:@"id"]) {
            [newData addObject:@"Pilih Nomor Rekening"];
        }else{
            [newData addObject:@"Select Account Number"];
        }
        for(NSDictionary *temp in listAcct){
            [newData addObject:[temp valueForKey:@"id_account"]];
        }
        listAccount = newData;
    }else{
        
        NSString *strURL = [NSString stringWithFormat:@"request_type=list_account1, customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
    }
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account1"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getListAccount];

        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
    
    if([requestType isEqualToString:@"gold_cetak"] && !getKCState){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            NSArray *typeOfPieces = [dataDict objectForKey:@"type_of_pieces"];
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];

            if ([language isEqualToString:@"id"]) {
                [tempDict setValue:@"Pilih Pecahan Emas" forKey:@"name"];
            }else{
                [tempDict setValue:@"Select Gold Shards" forKey:@"name"];
            }
            [newData addObject:tempDict];
            for(NSDictionary *temp in typeOfPieces){
                [newData addObject:temp];
            }
            goldFractionData = newData;
                        NSLog(@"%@", [goldFractionData[0] objectForKey:@"name"]);
                        NSLog(@"%@", [goldFractionData[0] objectForKey:@"id_denom"]);
            [self getListAccount];
        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
    
    if([requestType isEqualToString:@"gold_cetak"] && getKCState){
        if([[jsonObject objectForKey:@"response_code"] isEqual:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            [arr addObjectsFromArray:[dataDict objectForKey:@"shipping_location_all"]];
//            [arr addObject:[dataDict valueForKey:@"branch_cif"]];
            listBranch = arr;
            
//            labelArray = [[dataDict valueForKey:@"info"] componentsSeparatedByString:@"|"];
//            [self updateLabel];
//            [self getListAccount];
            [self gotoSelectBranch];
        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
    
}

- (BOOL) validate{
    if([_tfGoldFract.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsg1 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if([_tfGoldPieces.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsg2 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    double totalEmas = [_tfGoldPieces.text doubleValue] * [_tfGoldFract.text doubleValue];
    if(totalEmas > [[[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"] doubleValue]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsg3 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }else if(totalEmas < [gcLimitMin doubleValue]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsg5 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }else if(totalEmas > [gcLimitMax doubleValue]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsg6 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }else
    
    if([_tfFeeAcc.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsg4 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if([_tfBranch.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMsg7 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if([self.tfEmail.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"EMAIL_NOT_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if(![Utility validateEmailWithString:self.tfEmail.text]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"INVALID_EMAIL") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
         [alert addAction:ok];
         [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (IBAction)actionBtnNext:(id)sender {
    if([self validate]){
        [dataManager.dataExtra setValue:self.tfEmail.text forKey:@"email"];
        dataManager.currentPosition++;
        [self openNextTemplate];
    }
}

- (IBAction)actionBtnCancel:(id)sender {
    [self gotoGold];
}


- (void)selectedKC:(NSString *)shippinglocation {
    self.tfBranch.text = shippinglocation;
    
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
