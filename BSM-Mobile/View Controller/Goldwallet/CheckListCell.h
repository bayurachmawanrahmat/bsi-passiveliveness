//
//  CheckListCell.h
//  BSM-Mobile
//
//  Created by ARS on 18/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CheckListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UILabel *methodLabel;

@end

NS_ASSUME_NONNULL_END
