//
//  GACCViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GACCViewController.h"
#import "Styles.h"

@interface GACCViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSArray *listAccount;
    NSDictionary *mCode;
    BOOL listAccountIsRequested;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *customerName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblRekeningEmas;
@property (weak, nonatomic) IBOutlet UITextField *tfRekeningEmas;

@end

@implementation GACCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    [Styles setTopConstant:self.topConstant];

    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.customerName.text = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"nama"];
    
    if([language isEqualToString:@"id"]){
        self.lblTitle.text = @"Nomor Rekening Pembayaran";
        self.lblTitleBar.text = [mCode objectForKey:@"label_id"];
        self.lblRekeningEmas.text = @"Rekening Emas";
        
        [self.btnNext setTitle:@"Simpan" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Ubah" forState:UIControlStateNormal];

    }else{
        self.lblTitleBar.text = [mCode objectForKey:@"label_en"];
        self.lblTitle.text = @"Payment Account Number";
        self.lblRekeningEmas.text = @"Gold Account";
        
        [self.btnNext setTitle:@"Save" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Change" forState:UIControlStateNormal];
    }
    
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.tfAccount.text = [[userDefaults objectForKey:@"gold_info"]valueForKey:@"rekeningCBS"];
    NSString *rekening_emas = [[userDefaults objectForKey:@"gold_info"]valueForKey:@"noRekening"];
    self.tfRekeningEmas.text = rekening_emas;
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEdit)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    self.tfAccount.inputAccessoryView = toolbar;
    [self.tfAccount setEnabled:NO];
    [self.tfRekeningEmas setEnabled:NO];
    
    [Styles setStyleTextFieldBorderWithRightImage:_tfAccount imageNamed:@"baseline_keyboard_arrow_down_black_24pt" andRounded:YES];
    [Styles setStyleTextFieldBorder:_tfRekeningEmas andRounded:YES];
    
    [self.btnNext setEnabled:NO];
    
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    self.tfAccount.inputView = picker;
    
    [self.btnNext addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
    [self.btnBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCancel addTarget:self action:@selector(actionChange) forControlEvents:UIControlEventTouchUpInside];
}

- (void) doneEdit{
    [self.view endEditing:YES];
}

- (void) actionChange{
    [self.tfAccount setEnabled:YES];
    [self.btnNext setEnabled:YES];
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self getListAccount];
}

- (void) getListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
    
    if(listAcct.count != 0 || listAcct != nil){
        NSMutableArray *newData = [[NSMutableArray alloc]init];
        if ([language isEqualToString:@"id"]) {
            [newData addObject:@"Pilih Nomor Rekening"];
        }else{
            [newData addObject:@"Select Account Number"];
        }
        
        
        for(NSDictionary *temp in listAcct){
            [newData addObject:[temp valueForKey:@"id_account"]];
        }
        listAccount = newData;
    }else{
        if(listAccountIsRequested){
            NSString *msg = @"";
            if([language isEqualToString:@"id"]){
                msg = @"Anda Tidak Memiliki Rekening yang dibutuhkan";
            }else{
                msg = @"you dont have any account for this transaction";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            listAccountIsRequested = YES;
            
            NSString *strURL = [NSString stringWithFormat:@"request_type=list_account1, customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
        }
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listAccount.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return listAccount[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(row == 0){
        self.tfAccount.text = @"";
    }else{
        self.tfAccount.text = [listAccount objectAtIndex:row];
    }
}

- (void) actionOK{
    if([self.tfAccount.text isEqualToString:@""]){
        NSString *msg = @"";
        if([language isEqualToString:@"id"])
        {
            msg = @"Nomor Rekening Tidak Boleh Kosong";
        }else{
            msg = @"Account Number cannot be empty";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [dataManager.dataExtra setValue:self.tfAccount.text forKey:@"id_account"];
        NSString *strURL = [mCode objectForKey:@"url_param"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
    }
}

- (void)setCode:(NSDictionary *)code{
    mCode = code;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account1"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getListAccount];

        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
    
    if([requestType isEqualToString:@"gold_setting"]){
        NSString *response = [jsonObject objectForKey:@"response"];
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
            NSString *msg;
            if([language isEqualToString:@"id"]){
                msg  = [NSString stringWithFormat:@"Akun Rekening Pembayaran Anda Berhasil di Ganti Ke Nomor %@", [dataDict objectForKey:@"rekeningCBS"]];
            }else{
                 msg = [NSString stringWithFormat:@"Your Payment Account have success to change to  %@", [dataDict objectForKey:@"rekeningCBS"]];
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,response] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if ([self.tfAccount isFirstResponder]) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
        }];
    }
    return [super canPerformAction:action withSender:sender];
}

@end
