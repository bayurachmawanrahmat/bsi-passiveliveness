//
//  GR01ViewController.h
//  BSM-Mobile
//
//  Created by ARS on 19/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OAG01ViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
