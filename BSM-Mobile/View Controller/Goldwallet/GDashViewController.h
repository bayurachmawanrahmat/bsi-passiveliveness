//
//  GDashViewController.h
//  BSM-Mobile
//
//  Created by ARS on 28/02/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GDashViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
