//
//  GBViewController.m
//  BSM-Mobile
//
//  Created by ARS on 27/02/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GB01ViewController.h"
#import "PopInfoViewController.h"
#import "LibGold.h"
#import "Styles.h"

@interface GB01ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>{
    NSArray *goldAccPickerData;
    NSArray *paymentAccPickerData;

    NSUserDefaults *userDefaults;
    
    NSString *language;
    NSString *alertMessage1;
    NSString *alertMessage2;
    NSString *alertMessage3;
    NSString *labelBuyingTitleWeight;
    NSString *labelBuyingTitlePrice;
    
    NSString *gbPrice;
    NSString *gbLimitMin;
//    NSString *gbLimitMax;
    
    NSNumberFormatter* currencyFormatter;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *vwGoldToday;
@property (weak, nonatomic) IBOutlet UILabel *lblGDTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGDPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblGBasedOn;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segContG;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldBG;
@property (weak, nonatomic) IBOutlet UILabel *lblGram;
@property (weak, nonatomic) IBOutlet UILabel *lblPPH; //underAmount
@property (weak, nonatomic) IBOutlet UILabel *lblPPhGram;

@property (weak, nonatomic) IBOutlet UIView *vwBuyPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyingPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyingPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblPymntAccTitle;

@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@property (weak, nonatomic) IBOutlet UITextField *textFieldGldAcc;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPymnAcc;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraintVwTitle;

@end

@implementation GB01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    currencyFormatter = [LibGold setCurrency];
    [_lblTitle setText: [self.jsonData objectForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];

    gbLimitMin = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumBeli"];
//    gbLimitMax = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumBeli"];
    gbPrice = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"hargaBeliBSM"];
    
    [self setupLayout];
    [self setupLanguage];
}

- (void)viewWillAppear:(BOOL)animated{
    [self getListAccount];
}

- (void) setupLayout{
    
    [Styles setTopConstant:_topConstraintVwTitle];
    [Styles setStyleTextFieldBorder:_textFieldGldAcc andRounded:YES];
    [Styles setStyleRoundedView:_vwGoldToday];
    [Styles setStyleSegmentControl:_segContG];
    [Styles setStyleTextFieldBottomLine:_txtFieldBG];
    [Styles setStyleTextFieldBottomLine:_textFieldPymnAcc];
    [Styles setStyleRoundedView:_vwBuyPrice];
//    [Styles setStyleButton:_btnNext];
//    [Styles setStyleButton:_btnCancel];
    
    [self.textFieldGldAcc setText:[NSString stringWithFormat:@"%@ - %@ - %@ Gram",
                                [[userDefaults objectForKey:@"gold_info"]objectForKey:@"nama"],
                                [[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"],
                                [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]]];
    [self.textFieldGldAcc setEnabled:NO];
    
    [self.lblGDPrice setText:[NSString stringWithFormat:@"Rp. %@",
                              [LibGold getCurrencyForm:[gbPrice doubleValue]]]];

    [self.lblGram setHidden:YES];
    [self.lblPPH setHidden:YES];
    
    [self.txtFieldBG setDelegate:self];
    [self.txtFieldBG setTag:1];

    [self createToolbarButton:_txtFieldBG];
    [self.txtFieldBG setKeyboardType:UIKeyboardTypeNumberPad];
    [self.txtFieldBG addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self.textFieldGldAcc setEnabled:NO];
    [self createPicker:_textFieldGldAcc withTag:0];
    [self createToolbarButton:_textFieldGldAcc];

    [self createPicker:_textFieldPymnAcc withTag:1];
    [self createToolbarButton:_textFieldPymnAcc];
    
    [self.lblBuyingPrice setText:@"Rp. 0"];
    
    if(![[userDefaults objectForKey:@"gpop_gb01"] isEqualToString:@"YES"] && ![userDefaults objectForKey:@"gpop_gb01"]){
        PopInfoViewController *popInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"POPINFOGOLD"];
        [popInfo setPosition:0];
        [self presentViewController:popInfo animated:YES completion:nil];
    }
}

- (void) setupLanguage{
    if([language isEqualToString:@"id"]){
        _txtFieldBG.placeholder = @"Masukkan Jumlah Dana";
        _textFieldPymnAcc.placeholder = @"Masukkan No. Rekening Pembayaran";
        [_btnCancel setTitle:@"BATAL" forState:UIControlStateNormal];
        [_btnNext setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];

        labelBuyingTitlePrice = @"Harga Pembelian";
        labelBuyingTitleWeight = @"Berat Emas";
        alertMessage1 = @"Pembelian Emas anda kurang dari Minimum Pembelian";
        alertMessage2 = @"Harap Masukkan Nomor Rekening Pembayaran";
        alertMessage3 = @"";
        
        _lblGDTitle.text = @"Harga beli emas hari ini :";
        _lblGBasedOn.text = @"Beli Emas Berdasarkan";
        _lblBuyingPriceTitle.text = labelBuyingTitleWeight;
        _lblPymntAccTitle.text = @"Rekening Pembayaran";
        [_segContG setTitle:@"Nominal (Rp)" forSegmentAtIndex:0];
        [_segContG setTitle:@"Berat" forSegmentAtIndex:1];

    }else{
        _txtFieldBG.placeholder = @"Enter Amount of Funds";
        _textFieldPymnAcc.placeholder = @"Enter Payment Account Number";
        [_btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
        
        labelBuyingTitlePrice = @"Buying Purchase";
        labelBuyingTitleWeight = @"Weight Of Gold";
        alertMessage1 = @"Your purchasing gold is less than a minimum purchase";
        alertMessage2 = @"Please Enter your Account for Purchasing";
        alertMessage3 = @"";
        
        _lblGDTitle.text = @"Purchase Price of Gold Today :";
        _lblGBasedOn.text = @"Buy Gold Based On";
        _lblBuyingPriceTitle.text = labelBuyingTitleWeight;
        _lblPymntAccTitle.text = @"Payment Account";
        [_segContG setTitle:@"Nominal (Rp)" forSegmentAtIndex:0];
        [_segContG setTitle:@"Weight" forSegmentAtIndex:1];
    }
}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return goldAccPickerData.count;
    }else{
        return paymentAccPickerData.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return goldAccPickerData[row];
    }else{
        return paymentAccPickerData[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        self.textFieldGldAcc.text = [goldAccPickerData objectAtIndex:row];
    }else if(pickerView.tag == 1){
        if(row == 0){
            self.textFieldPymnAcc.text = @"";
        }else{
            self.textFieldPymnAcc.text = [paymentAccPickerData objectAtIndex:row];
        }
    }
}

-(void)doneEditButtonTapped:(id)done{
    [self.view endEditing:YES];
}

- (void) getListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
    
    if(listAcct.count != 0 || listAcct != nil){
        NSMutableArray *newData = [[NSMutableArray alloc]init];
        if ([language isEqualToString:@"id"]) {
            [newData addObject:@"Pilih Nomor Rekening"];
        }else{
            [newData addObject:@"Select Account Number"];
        }

        for(NSDictionary *temp in listAcct){
            [newData addObject:[temp valueForKey:@"id_account"]];
        }
        paymentAccPickerData = newData;
    }else{
        
        NSString *strURL = [NSString stringWithFormat:@"request_type=list_account1, customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
    }
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account1"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getListAccount];

        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(_lblGram.isHidden == YES){
        if(textField.tag == 1){
            NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
            
            double newValue = [newText doubleValue];
            if ([newText length] == 0 || newValue == 0){
                textField.text = nil;
            }else{
                textField.text = [LibGold getCurrencyForm:newValue];
                _lblBuyingPrice.text = [LibGold calculateToGold:newValue withPrice:[gbPrice doubleValue]];
                
                double res = 0;
                if([[[userDefaults objectForKey:@"gold_info"] objectForKey:@"verifiedNPWP"] isEqualToString:@"Y"]){
                    res = newValue * [[[userDefaults objectForKey:@"gold_info"] valueForKey:@"ProsentasePajakBeli"]doubleValue];
//                    res = newValue * 0.0045;
                }else{
                    res = newValue * [[[userDefaults objectForKey:@"gold_info"] valueForKey:@"ProsentasePajakBeliNONPWP"]doubleValue];
//                    res = newValue * 0.009;
                }
                
//                if([language isEqualToString:@"id"]){
//                    [self.lblPPH setText:[NSString stringWithFormat:@"PPH Pasal 22 sebesar Rp. %@,00", [LibGold getCurrencyForm:res]]];
//                }else{
//                    [self.lblPPH setText:[NSString stringWithFormat:@"Amount of PPH Tax Article 22 Rp. %@,00", [LibGold getCurrencyForm:res]]];
//                }
//                [self.lblPPH setHidden:NO];
//                [self.lblPPhGram setHidden:YES];
                if([language isEqualToString:@"id"]){
                    [self.lblPPhGram setText:[NSString stringWithFormat:@"PPH Pasal 22 sebesar Rp. %@", [LibGold getCurrencyForm:res withDigits:2]]];
                }else{
                    [self.lblPPhGram setText:[NSString stringWithFormat:@"Amount of PPH Tax Article 22 Rp. %@", [LibGold getCurrencyForm:res withDigits:2]]];
                }
                [self.lblPPH setHidden:YES];
                [self.lblPPhGram setHidden:NO];
                
                return NO;
            }
        }
        
    }
    else{
        if(textField.tag == 1){
            NSString *text = textField.text;
            
            if([string isEqualToString:@","] && [text containsString:@","]){
                return NO;
            }
            
            NSArray  *arrayOfString = [text componentsSeparatedByString:@","];
            if([arrayOfString count] == 2){
                if([arrayOfString[1] length] > 3 && ![string isEqualToString:@""]){
                    return NO;
                }
            }
            
            NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            double value = [[newText stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];

            _lblBuyingPrice.text = [LibGold calculateToMoney:value withPrice:[gbPrice doubleValue]];
            
            value = value * [gbPrice doubleValue];
            double res = 0;
            if([[[userDefaults objectForKey:@"gold_info"] objectForKey:@"verifiedNPWP"] isEqualToString:@"Y"]){
                res = value * [[[userDefaults objectForKey:@"gold_info"]valueForKey:@"ProsentasePajakBeli"]doubleValue];
            }else{
                res = value * [[[userDefaults objectForKey:@"gold_info"]valueForKey:@"ProsentasePajakBeliNONPWP"]doubleValue];
            }
            
//            if([language isEqualToString:@"id"]){
//                [self.lblPPhGram setText:[NSString stringWithFormat:@"PPH Pasal 22 sebesar Rp. %@,00", [LibGold getCurrencyForm:res]]];
//            }else{
//                [self.lblPPhGram setText:[NSString stringWithFormat:@"Amount of PPH Tax Article 22 Rp. %@,00", [LibGold getCurrencyForm:res]]];
//            }
//            [self.lblPPH setHidden:YES];
//            [self.lblPPhGram setHidden:NO];
            if([language isEqualToString:@"id"]){
                [self.lblPPH setText:[NSString stringWithFormat:@"PPH Pasal 22 sebesar Rp. %@", [LibGold getCurrencyForm:res withDigits:2]]];
            }else{
                [self.lblPPH setText:[NSString stringWithFormat:@"Amount of PPH Tax Article 22 Rp. %@", [LibGold getCurrencyForm:res withDigits:2]]];
            }
            [self.lblPPH setHidden:NO];
            [self.lblPPhGram setHidden:YES];

            
        }
    }
    
    return YES;
}

- (void) textFieldDidChange : (UITextField *)textField{
    //kalo yg beli PPH npwp = 0.45%/0.0045
    //PPH tdk ada NPWP = 0.9% // 0.009
    
//    if(_lblGram.isHidden == YES){
//        double newValue = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue];
//        _lblBuyingPrice.text = [LibGold calculateToGold:newValue withPrice:[gbPrice doubleValue]];
//
//    }else{
////        NSString *text = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
////        textField.text = text;
//        double value = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
//        _lblBuyingPrice.text = [LibGold calculateToMoney:value withPrice:[gbPrice doubleValue]];
//    }

}

- (IBAction)actionSegmentControl:(UISegmentedControl*)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;

    if (selectedSegment == 0) {
        [self.view endEditing:YES];
        _lblGram.hidden = YES;
        _txtFieldBG.text = @"";
        _txtFieldBG.keyboardType = UIKeyboardTypeNumberPad;
        
        if([language isEqualToString:@"id"]){
            _txtFieldBG.placeholder = @"Masukkan Jumlah Dana";
        }else{
            _txtFieldBG.placeholder = @"Enter Amount of Funds";
        }

        double newValue = [_txtFieldBG.text doubleValue];
                
        _lblBuyingPriceTitle.text = labelBuyingTitleWeight;
        _lblBuyingPrice.text = [LibGold getGramFormOfValue:newValue/[gbPrice doubleValue]];


    }else{
        [self.view endEditing:YES];
        _lblGram.hidden = NO;
        _txtFieldBG.text = @"";
        if([language isEqualToString:@"id"]){
            _txtFieldBG.placeholder = @"Masukkan Berat Emas";
        }else{
            _txtFieldBG.placeholder = @"Enter Gold Weight";
        }
        _txtFieldBG.keyboardType = UIKeyboardTypeDecimalPad;
        double pricePerGram = [gbPrice doubleValue];
        double newValue = [_txtFieldBG.text doubleValue];
        double total = newValue * pricePerGram;

        _lblBuyingPriceTitle.text = labelBuyingTitlePrice;
        _lblBuyingPrice.text = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:total]];
        
    }
}


- (BOOL) validate{
    if(_lblGram.hidden == YES)
    {
        NSString *convertString = [_txtFieldBG.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        double pricePerGram = [gbPrice doubleValue];
        double input = [convertString doubleValue];
        double total = input / pricePerGram;
        
        if(total < [self->gbLimitMin doubleValue]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMessage1 preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.txtFieldBG.text = [NSString stringWithFormat:@"%@", [LibGold getCurrencyForm:[self->gbLimitMin doubleValue]*pricePerGram]];
                        
                self.lblBuyingPriceTitle.text = self->labelBuyingTitleWeight;
                self.lblBuyingPrice.text = [LibGold getGramFormOfValue:[self->gbLimitMin doubleValue]];
            }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            return NO;
        }else{
            if([_textFieldPymnAcc.text isEqualToString:@""]){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMessage2 preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
                return NO;
            }
            return YES;
        }
    }else{
        double newValue = [[_txtFieldBG.text stringByReplacingOccurrencesOfString:@"," withString:@"."]doubleValue];
        double pricePerGram = [gbPrice doubleValue];

        if(newValue < [gbLimitMin doubleValue]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMessage1 preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                self.txtFieldBG.text = self->gbLimitMin;
                
                double pricing = [self.txtFieldBG.text doubleValue]*pricePerGram;
                
                self.lblBuyingPriceTitle.text = self->labelBuyingTitlePrice;
                self.lblBuyingPrice.text = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:pricing]];
            
            }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
            
            return NO;
        }else{
            
            if([_textFieldPymnAcc.text isEqualToString:@""]){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMessage2 preferredStyle:UIAlertControllerStyleAlert];

                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
                
                return NO;
            }
            
            return YES;
        }
    }
    return NO;
}

- (IBAction)actionBtnNext:(id)sender {
    [dataManager.dataExtra setValue:_textFieldPymnAcc.text forKey:@"id_account"];

    if(_lblGram.hidden == YES){
        double newValue = [[_txtFieldBG.text stringByReplacingOccurrencesOfString:@"," withString:@""]doubleValue];
        NSString *totalPrice = [_txtFieldBG.text stringByReplacingOccurrencesOfString:@"," withString:@""];

        [dataManager.dataExtra setValue:totalPrice forKey:@"amount"];
        [dataManager.dataExtra setValue:[LibGold goldToSent:newValue withPrice:[gbPrice doubleValue]] forKey:@"weight"];
        [dataManager.dataExtra setValue:@"RP" forKey:@"input_type"];

    }else{
        double newValue = [[_txtFieldBG.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
        double pricePerGram = [gbPrice doubleValue];
        double totalPrice = newValue * pricePerGram;
        
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%.f",totalPrice] forKey:@"amount"];
        [dataManager.dataExtra setValue:[LibGold goldToSentFromGram:newValue] forKey:@"weight"];
        [dataManager.dataExtra setValue:@"GR" forKey:@"input_type"];

    }

    if([self validate]){
        [self openNextTemplate];
    }
}

- (IBAction)actionBtnCancel:(id)sender {
    [self gotoGold];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end

