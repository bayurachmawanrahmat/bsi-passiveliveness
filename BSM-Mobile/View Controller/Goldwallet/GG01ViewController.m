//
//  GG01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 01/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GG01ViewController.h"
#import "Styles.h"

@interface GG01ViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation GG01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstraint];

    // Do any additional setup after loading the view.
}

@end
