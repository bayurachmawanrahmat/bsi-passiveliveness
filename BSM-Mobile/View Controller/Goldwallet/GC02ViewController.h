//
//  GC02ViewController.h
//  BSM-Mobile
//
//  Created by ARS on 01/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GC02ViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate : (id)newDelegate;

@end

@protocol GC02Delegate <NSObject>

@required

- (void) selectedKC : (NSString*) dictKC;

@end

NS_ASSUME_NONNULL_END
