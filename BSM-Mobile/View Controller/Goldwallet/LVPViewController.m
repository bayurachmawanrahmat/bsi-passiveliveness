//
//  LVPViewController.m
//  BSM-Mobile
//
//  Created by ARS on 21/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "LVPViewController.h"
#import "PFViewController.h"
#import "LibGold.h"
#import "Styles.h"
#import "LVPCell.h"
#import "Connection.h"
#import "Utility.h"
#import "GNPWPViewController.h"
#import "GACCViewController.h"

@interface LVPViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *lisData;
    NSUserDefaults *userDefaults;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation LVPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstraint];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.tableView registerNib:[UINib nibWithNibName:@"LVPCell" bundle:nil]
    forCellReuseIdentifier:@"LVPCELL"];
    
    lisData = [[NSArray alloc]init];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];

}

- (void)viewWillAppear:(BOOL)animated{
    [self getData];
}

- (void)getData{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    //dompetemas.setting.menu.v1
    //dompetemas.setting.menu.vmap
    NSString *request = [NSString stringWithFormat:@"request_type=setting,prefix=dompetemas"];
    [conn sendPostParmUrl:request needLoading:true encrypted:false banking:false favorite:false];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    PFViewController *profile = [self.storyboard instantiateViewControllerWithIdentifier:@"PFV"];
//    [profile setCode:lisData[indexPath.row]];
    
    [self.tabBarController setSelectedIndex:0];

    if([[lisData[indexPath.row] objectForKey:@"code"] isEqualToString:@"1"])
    {
        GNPWPViewController *npwp = [self.storyboard instantiateViewControllerWithIdentifier:@"GNPWP"];
        [npwp setCode:lisData[indexPath.row]];
        [self.navigationController pushViewController:npwp animated:YES];
    }
    else if([[lisData[indexPath.row] objectForKey:@"code"] isEqualToString:@"2"])
    {
        GACCViewController *gacc = [self.storyboard instantiateViewControllerWithIdentifier:@"GACC"];
        [gacc setCode:lisData[indexPath.row]];
        [self.navigationController pushViewController:gacc animated:YES];

    }
    else if([[lisData[indexPath.row] objectForKey:@"code"] isEqualToString:@"3"])
    {
        NSString *weight = [[userDefaults objectForKey:@"gold_info"] valueForKey:@"saldoAvailable"];
        [dataManager.dataExtra setValue:[weight stringByReplacingOccurrencesOfString:@"." withString:@""]  forKey:@"weight"];
        [self openTemplateByMenuID:@"00139"];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return lisData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LVPCell *cell = (LVPCell*)[tableView dequeueReusableCellWithIdentifier:@"LVPCELL"];
    
    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        cell.name.text = [lisData[indexPath.row] objectForKey:@"label_id"];
    }else{
        cell.name.text = [lisData[indexPath.row] objectForKey:@"label_en"];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
     if ([requestType isEqualToString:@"setting"]) {
         NSDictionary *data = (NSDictionary * )jsonObject;
         if(data != nil){
//             NSString *arVMap = [data valueForKey:@"dompetemas.setting.menu.v1"];
             NSArray *arVMap = [[data valueForKey:@"dompetemas.setting.menu.vmap"] componentsSeparatedByString:@"|"];
//             NSString *strVersionMap = @"v1";
             NSString *strVersionMap = @"";
             for(NSString *strVmap in arVMap){
                 NSArray *arCVmap = [strVmap componentsSeparatedByString:@"="];
                 if ([[arCVmap objectAtIndex:0] isEqualToString:@VERSION_VALUE]) {
                     strVersionMap =[arCVmap objectAtIndex:1];
                     break;
                 }
             }
             
             if([strVersionMap isEqualToString:@""]){
                 NSArray *arCVmap = [arVMap.lastObject componentsSeparatedByString:@"="];
                 strVersionMap =[arCVmap objectAtIndex:1];
             }
             
             NSString *menuProfile = [data valueForKey:[NSString stringWithFormat:@"dompetemas.setting.menu.%@", strVersionMap]];
             
             NSError *error;
             NSArray *dataArr = [NSJSONSerialization JSONObjectWithData:[menuProfile dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
             lisData = dataArr;
         }else{
             NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
             UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
             [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                 [self gotoGold];
             }]];
             [self presentViewController:alertCont animated:YES completion:nil];
         }

     }

    [self.tableView reloadData];
}
- (IBAction)backAction:(id)sender {
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];    
}

@end
