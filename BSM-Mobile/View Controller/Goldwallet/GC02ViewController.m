//
//  GC02ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 01/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GC02ViewController.h"
#import "PickUpTableViewCell.h"
#import "CheckListCell.h"
#import "Styles.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface GC02ViewController ()<
CLLocationManagerDelegate,
MKMapViewDelegate,
UISearchBarDelegate,
UITableViewDelegate,
ConnectionDelegate,
GC02Delegate,
UITableViewDataSource>{
    NSUserDefaults *userDefaults;
    
    NSString *language;
    NSString *locPermissionDeniedMsg;
    NSString *locServiceDisabledMsg;
    NSString *labelLocListTitleText;
    NSString *alertMsgMethod;
    NSString *alertLocBranch;
    
    NSString* shippingMethod;
    NSString* shippingLocation;
    
    NSArray* allListKCP;
    NSArray* nearbyListKCP;
    NSArray* displayList;
    NSArray* filteredList;
    NSArray* methodeList;
    
    NSDictionary* listData;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double latitude;
    double longitude;
    
    BOOL isRequested;
    BOOL isSearchActive;
    
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *buttonBack;

@property (weak, nonatomic) IBOutlet UILabel *lblOptionTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBranch;
@property (weak, nonatomic) IBOutlet UILabel *lblAntam;
@property (weak, nonatomic) IBOutlet UIButton *btnBranch;
@property (weak, nonatomic) IBOutlet UIButton *btnAntam;

@property (weak, nonatomic) IBOutlet UILabel *lblLocListTitle;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tblViewLocation;
@property (weak, nonatomic) IBOutlet UITableView *tblViewMethod;
@property (weak, nonatomic) IBOutlet UIView *vwTableMethod;

//@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@property (weak, nonatomic) IBOutlet UIView *vwResult;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleResult;
@property (weak, nonatomic) IBOutlet UILabel *lblKCResult;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressResult;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeResult;

@end

@implementation GC02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if(@available(iOS 13.0, *)){
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    shippingMethod = @"";
    shippingLocation = @"";
    
    isRequested = NO;
    isSearchActive = NO;
    _vwResult.hidden = YES;
    _btnAntam.hidden = YES;
    _btnBranch.hidden = YES;
    _lblAntam.hidden = YES;
    _lblBranch.hidden = YES;
    
    [Styles setTopConstant:_topConstraint];
    [Styles setStyleButton:_btnNext];
    
    self.tblViewMethod.delegate = self;
    self.tblViewMethod.dataSource = self;
    
    self.tblViewLocation.delegate = self;
    self.tblViewLocation.dataSource = self;
    
    self.searchBar.delegate = self;
    [self createToolbarButton];
    
    [self.tblViewLocation registerNib:[UINib nibWithNibName:@"PickUpTableViewCell" bundle:nil]
    forCellReuseIdentifier:@"PICKUPCELL"];
    [self.tblViewMethod registerNib:[UINib nibWithNibName:@"CheckListCell" bundle:nil]
    forCellReuseIdentifier:@"CHECKLISTCELL"];
    
    [self.buttonBack setUserInteractionEnabled:YES];
    [self.buttonBack addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBack)]];
}

- (void)viewDidAppear:(BOOL)animated{
    [self currentLocationIdentifier];
}

- (void) actionBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) createToolbarButton{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    _searchBar.inputAccessoryView = toolbar;
}

-(void)doneEditButtonTapped:(id)done{
    [self.view endEditing:YES];
}

- (void)currentLocationIdentifier{
    if([CLLocationManager locationServicesEnabled]){
        
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"App Permission Denied" message:@"Turn On Location Serivce in Settings" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionOK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            }];
            
            NSString *stringCancel = @"";
            if([language isEqualToString:@"id"]){
                stringCancel = @"Batal";
            }else{
                stringCancel = @"Cancel";
            }
            
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:stringCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self requestCall];
            }];
            
            [alert addAction:actionOK];
            [alert addAction:actionCancel];
            [self presentViewController:alert animated:YES completion:nil];

        }
        
        locationManager = nil;
        locationManager = [[CLLocationManager alloc]init];
        locationManager.delegate = self;
        locationManager.distanceFilter = 100;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
        
        if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
            [locationManager requestAlwaysAuthorization];
        }
        [locationManager startUpdatingLocation];
        
    } else {
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"Location Services Disabled" message:@"You currently have all location services for this device disabled. If you proceed, you will be showing past informations. To enable, Settings->Location->location services->on" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
        }]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
    }
}

- (double) distanceCalcWithLatitude1:(double)latitude1 longitude1:(double)longitude1 latitude2:(double)latitude2 longitude2:(double)longitude2{
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:latitude1 longitude:longitude1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:latitude2 longitude:longitude2];
    double dMeters = [loc1 distanceFromLocation:loc2];
    
    return dMeters/1000;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    longitude = currentLocation.coordinate.longitude;
    latitude = currentLocation.coordinate.latitude;
    
    [[DataManager sharedManager].dataExtra setValue:[NSString stringWithFormat:@"%f",latitude] forKey:@"latitude"];
    [[DataManager sharedManager].dataExtra setValue:[NSString stringWithFormat:@"%f",longitude] forKey:@"longitude"];

    [self requestCall];
}

- (void) requestCall{
    NSString *urlData= @"request_type=gold_cetak,step=info2,menu_id,device,device_type,ip_address,language,date_local,customer_id,latitude,longitude";

    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == _tblViewLocation){
        if([shippingMethod isEqualToString:@"shipping_location_nearby"]){
            displayList = nearbyListKCP;
        }else if([shippingMethod isEqualToString:@"shipping_location_all"]) {
            displayList = allListKCP;
        }
        else{
            return 0;
        }
        
        if(isSearchActive){
            return filteredList.count;
        }else{
            return displayList.count;
        }
    }else{
        return methodeList.count;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == _tblViewLocation){
        PickUpTableViewCell *cell = (PickUpTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PICKUPCELL"];
        NSDictionary* pickupData;
        
        if(isSearchActive){
            pickupData = filteredList[indexPath.row];
        }else{
            pickupData = displayList[indexPath.row];
        }
        
        if([shippingMethod isEqualToString:@"shipping_location_nearby"]){
            
                cell.imageView.image = [UIImage imageNamed:@"ic_obd_cstr_kcabang"];
                cell.kcName.text = [pickupData objectForKey:@"branch_name"];
                cell.kcAddress.text = [NSString stringWithFormat:@"%@", [pickupData objectForKey:@"branch_address"]];
                cell.kcDistance.text = [NSString stringWithFormat:@"%.2f km", [self distanceCalcWithLatitude1:latitude
                                                            longitude1:longitude
                                                             latitude2:[[pickupData objectForKey:@"latitude"]doubleValue]
                                                            longitude2:[[pickupData objectForKey:@"longitude"]doubleValue]]];
        }else {
            
            cell.imageView.image = [UIImage imageNamed:@"ic_obd_cstr_kcabang"];
            cell.kcName.text = [pickupData objectForKey:@"branch_name"];
            cell.kcAddress.text = [NSString stringWithFormat:@"%@", [pickupData objectForKey:@"branch_address"]];
            cell.kcDistance.text = @"";
        }
        
        return cell;
    }else {
        CheckListCell *cell = (CheckListCell *)[tableView dequeueReusableCellWithIdentifier:@"CHECKLISTCELL"];
        NSDictionary *pickupMethod = methodeList[indexPath.row];
        if(nearbyListKCP.count == 0 && [[pickupMethod objectForKey:@"key"] isEqualToString:@"shipping_location_nearby"]){
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.methodLabel setTextColor: UIColorFromRGB(const_color_gray)];
            cell.userInteractionEnabled = NO;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if([language isEqualToString:@"id"]){
            [cell.methodLabel setText:[pickupMethod objectForKey:@"label_id"]];
        }else{
            [cell.methodLabel setText:[pickupMethod objectForKey:@"label_en"]];

        }
        
        return cell;
        
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == _tblViewLocation){
        
        if([language isEqualToString:@"id"]){
            _lblLocListTitle.text = @"Lokasi Pengambilan Emas Terpilih";
        }else{
            _lblLocListTitle.text = @"Selected Gold Pickup Location";
        }
        
        
        _searchBar.hidden = YES;
        _tblViewLocation.hidden = YES;
        _vwResult.hidden = NO;
        
        if(isSearchActive){
            
            shippingLocation = [filteredList[indexPath.row] objectForKey:@"branch_name"];
            _lblKCResult.text = [filteredList[indexPath.row] objectForKey:@"branch_name"];
            _lblAddressResult.text = [filteredList[indexPath.row] objectForKey:@"branch_address"];
            
            [[DataManager sharedManager].dataExtra setValue:[filteredList[indexPath.row] objectForKey:@"branch_code"] forKey:@"branch_code"];
            [_searchBar endEditing:true];
            
        }else{

            shippingLocation = [displayList[indexPath.row] objectForKey:@"branch_name"];
            _lblKCResult.text = [displayList[indexPath.row] objectForKey:@"branch_name"];
            _lblAddressResult.text = [displayList[indexPath.row] objectForKey:@"branch_address"];
            [[DataManager sharedManager].dataExtra setValue:[displayList[indexPath.row] objectForKey:@"branch_code"] forKey:@"branch_code"];

            [_searchBar endEditing:true];
        }
    }else{
        CheckListCell *cell = (CheckListCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary *pickupMethod = methodeList[indexPath.row];
                
        [[DataManager sharedManager].dataExtra setValue:[pickupMethod objectForKey:@"method"] forKey:@"shipping_method"];
        
        shippingLocation = @"";
        shippingMethod = [pickupMethod objectForKey:@"key"];
        isSearchActive = NO;

        [self.tblViewLocation reloadData];
    }

    
}

-(BOOL) validate{
    if([shippingMethod isEqualToString:@""]){
        
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"INFORMASI" message:@"Pilih Metode Pengambilan Terlebih Dahulu" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
        
        return NO;
    }
    if([shippingLocation isEqualToString:@""]){
        UIAlertController *servicesDisabledAlert = [UIAlertController alertControllerWithTitle:@"INFORMASI" message:@"Pilih Lokasi Pengambilan Emas Terlebih Dahulu" preferredStyle:UIAlertControllerStyleAlert];
        [servicesDisabledAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:servicesDisabledAlert animated:YES completion:nil];
        
        return NO;
    }
  
    [[DataManager sharedManager].dataExtra setValue:shippingLocation forKey:@"shipping_location"];
    
    return YES;
}

//- (IBAction)actionCancel:(id)sender {
//    [self gotoGold];
//}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (IBAction)actionNext:(id)sender {
    

    if([self validate]){
//        [[DataManager sharedManager].dataExtra valueForKey:@"branch_code"];
        [delegate selectedKC:[[DataManager sharedManager].dataExtra valueForKey:@"shipping_location"]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)actionChangeResult:(id)sender {
    _vwResult.hidden = YES;
    _searchBar.hidden = NO;
    _tblViewLocation.hidden = NO;
    
    if([language isEqualToString:@"id"]){
        _lblLocListTitle.text = @"Pilih Lokasi Pengambilan Emas";
    }else{
        _lblLocListTitle.text = @"Choose Gold Pickup Location";
    }
    shippingLocation = @"";

}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{

    if([requestType isEqualToString:@"gold_cetak"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
            options:NSJSONReadingAllowFragments
              error:&error];
            
            NSArray *shippingMethod = [dataDict objectForKey:@"shipping_method"];
            NSArray *shippingLocation = [dataDict objectForKey:@"shipping_location_all"];
            NSArray *shippingNearby = [dataDict objectForKey:@"shipping_location_nearby"];
            
            
            NSMutableArray* allKCPList = [[NSMutableArray alloc] init];
            for(int i = 0; i < shippingLocation.count; i++)
            {
                NSString* nameProv = [shippingLocation[i] objectForKey:@"name"];
                NSArray* regencyCity = [shippingLocation[i] objectForKey:@"regency_city"];
                
                for(int j = 0; j < regencyCity.count; j++){
                    
                    NSString* nameRegency = [regencyCity[j] objectForKey:@"name"];
                    NSArray* branch = [regencyCity[j] objectForKey:@"branch"];
                    for (int k = 0; k < branch.count; k++){
                        NSMutableDictionary *dictMut = [[NSMutableDictionary alloc] init];
                        
                        [dictMut setObject:[branch[k] objectForKey:@"branch_name"] forKey:@"branch_name"];
                        
                        [dictMut setObject:[branch[k] objectForKey:@"branch_code"] forKey:@"branch_code"];
                        
                        [dictMut setObject:[NSString stringWithFormat:@"%@, %@", nameRegency, nameProv] forKey:@"branch_address"];

                        [allKCPList addObject:dictMut];
                    }
                }
            }
            allListKCP = allKCPList;
            nearbyListKCP = shippingNearby;
            methodeList = shippingMethod;
            
            [self.tblViewLocation reloadData];
            [self.tblViewMethod reloadData];
            
            [self tableView:self.tblViewMethod didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];

        }else{
            
        }
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    isSearchActive = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if([searchBar.text isEqualToString:@""]){
        isSearchActive = NO;
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    isSearchActive = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    
    NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"branch_name", searchText];

    NSArray *filterArrays = [displayList filteredArrayUsingPredicate:predicateString];
    
    filteredList = filterArrays;
    if([searchText isEqualToString:@""]){
        filteredList = displayList;
    }
    [self.tblViewLocation reloadData];

}

@end
