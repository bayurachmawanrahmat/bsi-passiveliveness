//
//  PopInfoViewController.m
//  BSM-Mobile
//
//  Created by ARS on 20/02/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PopInfoViewController.h"
#import "LibGold.h"

@interface PopInfoViewController (){
    int position;
    NSUserDefaults *userDefaults;
    BOOL state;
    
    NSString *language;
    
    NSString *minLimitBeli;
    NSString *maxLimitBeli;
    NSString *minLimitJual;
    NSString *maxLimitJual;
    NSString *maxLimitFlag;
    NSString *minLimitTrf;
    NSString *maxLimitTrf;
//    NSString *maxLimitJualValue;
}

@property (weak, nonatomic) IBOutlet UIView *vwPops;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titlePop;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UITextView *textVDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelDontShow;

@end

@implementation PopInfoViewController

@synthesize description;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(@available(iOS 13.0, *)){
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    /* getting gold information */
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self.textVDescription setEditable:NO];
    
    minLimitBeli = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumBeli"];
    maxLimitBeli = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumBeli"];
    
    minLimitJual = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumJual"];
    maxLimitFlag = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"jual.max.flag"];
    
//    if([maxLimitFlag isEqualToString:@"1"]){
//        maxLimitJual = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"jual.max"];
////        maxLimitJualValue = @"Rupiah";
//    }else{
//        maxLimitJual = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumJual"];
////        maxLimitJualValue = @"gram";
//    }
    
    minLimitTrf = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumTransfer"];
    maxLimitTrf = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumTransfer"];
    //
    state = 0;
    [self setupLanguages];
    
    _vwPops.layer.cornerRadius = 20;
    _vwPops.layer.shadowColor = [[UIColor grayColor]CGColor];
    _vwPops.layer.shadowOffset = CGSizeMake(0, 5);
    _vwPops.layer.shadowOpacity = 0.5f;
    _vwPops.layer.masksToBounds = NO;

    _okButton.layer.cornerRadius = 20;
    _okButton.layer.masksToBounds = YES;
    [_okButton setBackgroundColor:UIColorFromRGB(const_color_primary)];

    [self.button setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"] forState:UIControlStateNormal];
    [self setupPosition];
}

- (void) setupPosition{
    NSMutableAttributedString *albai = [[NSMutableAttributedString alloc]initWithString:@"AKAD AL BAI'" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name3 size:13], NSForegroundColorAttributeName : UIColorFromRGB(const_color_primary)}];
    NSMutableAttributedString *wakalah = [[NSMutableAttributedString alloc]initWithString:@"AKAD WAKALAH BIL UJROH'" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name3 size:13], NSForegroundColorAttributeName : UIColorFromRGB(const_color_primary)}];
    
    self.textVDescription.textAlignment = NSTextAlignmentCenter;
    switch (position) {
        case 0:{
            [self.imageView setImage:[UIImage imageNamed:@"ic_beli_emas_desc.png"]];
            if([language isEqualToString:@"id"]){
                [self.titlePop setText:@"Beli Emas"];
                
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                NSMutableAttributedString *b1 = [[NSMutableAttributedString alloc]initWithString:@"adalah fitur untuk anda melakukan pembelian emas kepada Bank Syariah Indonesia. menggunakan Akad jual beli" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *b2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" dengan minimal pembelian %@ dan maksimal %@ per hari", minLimitBeli, [LibGold getMillionValue:[maxLimitBeli doubleValue]]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                
                [final appendAttributedString:b1];
//                [final appendAttributedString:albai];
                [final appendAttributedString:b2];
                
                self.textVDescription.attributedText = final;
                
                
            }else{
                [self.titlePop setText:@"Gold Buy"];
                
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                NSMutableAttributedString *b1 = [[NSMutableAttributedString alloc]initWithString:@"is a feature for you to buy golds to Bank Syariah Indonesia using sell and purchase agreement" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *b2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" with a minimum purchase of %@ and the maximum of %@ per day.", minLimitBeli,[LibGold getMillionValue:[maxLimitBeli doubleValue]]] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                
                [final appendAttributedString:b1];
//                [final appendAttributedString:albai];
                [final appendAttributedString:b2];
                
                self.textVDescription.attributedText = final;
            }
            break;
        }
        case 1:{
            [self.imageView setImage:[UIImage imageNamed:@"ic_jual_emas_desc.png"]];
            
            double gsPrice = [[[userDefaults objectForKey:@"gold_info"]objectForKey:@"hargaJualBSM"] doubleValue];
            if([maxLimitFlag isEqualToString:@"1"]){
                maxLimitJual = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"jual.max"];
                double res = [maxLimitJual doubleValue]/gsPrice;
                maxLimitJual = [NSString stringWithFormat:@"%.4f",res];
            }else{
                maxLimitJual = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumJual"];
            }

            
            if([language isEqualToString:@"id"]){
                [self.titlePop setText:@"Jual Emas"];
                
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                NSMutableAttributedString *b1 = [[NSMutableAttributedString alloc]initWithString:@"adalah fitur untuk Anda melakukan penjualan emas yang ada di rekening Emas kepada Bank Syariah Indonesia menggunakan Akad jual beli" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *b2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" dengan minimal penjualan sebesar %@ gram dan maksimal %@ gram", minLimitJual, maxLimitJual] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                
                [final appendAttributedString:b1];
//                [final appendAttributedString:albai];
                [final appendAttributedString:b2];
                
                self.textVDescription.attributedText = final;
                
            }else{
                [self.titlePop setText:@"Gold Sell"];
                
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                NSMutableAttributedString *b1 = [[NSMutableAttributedString alloc]initWithString:@"is a feature for you to sell your golds in your Gold Account to Bank Syariah Indonesia using sale and purchase agreement" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *b2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" with a minimum weight of gold that can be sell for %@ gram to maximum %@ gram", minLimitJual, maxLimitJual] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                
                [final appendAttributedString:b1];
//                [final appendAttributedString:albai];
                [final appendAttributedString:b2];
                
                self.textVDescription.attributedText = final;
            }
            break;
        }
        case 2:{
            [self.imageView setImage:[UIImage imageNamed:@"ic_cetak_emas_desc.png"]];
            if([language isEqualToString:@"id"]){
                [self.titlePop setText:@"Tarik Fisik Emas"];
                [self.textVDescription setText:@"adalah fitur untuk Anda melakukan penarikan fisik emas yang bersertifikat dari saldo rekening e-mas Anda dengan pecahan keping emas yang dapat dipilih sebesar 2, 5, 10, 25, 50 atau 100 gram dan dikenakan biaya cetak emas."];
            }else{
                [self.titlePop setText:@"Gold Physical Whitdrawals"];
                [self.textVDescription setText:@"is a feature for you to make certified physical gold withdrawals from your e-mas account balance with selectable gold pieces of 2, 5, 10, 25, 50 or 100 grams and will be charged a gold printing fee."];
            }
            break;
        }
        case 3:{
            [self.imageView setImage:[UIImage imageNamed:@"ic_transfer_emas_desc.png"]];
            if([language isEqualToString:@"id"]){
                [self.titlePop setText:@"Transfer Emas"];
                
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                NSMutableAttributedString *b1 = [[NSMutableAttributedString alloc]initWithString:@"adalah fitur untuk melakukan transfer emas kepada sesama pengguna aplikasi e-mas menggunakan " attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *b2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" dengan minimal transfer per hari sebesar %@ gram dan maksimal %@ gram.", minLimitTrf, maxLimitTrf] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                
                [final appendAttributedString:b1];
                [final appendAttributedString:wakalah];
                [final appendAttributedString:b2];
                
                self.textVDescription.attributedText = final;
                
            }else{
                [self.titlePop setText:@"Gold Transfer"];
                
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                NSMutableAttributedString *b1 = [[NSMutableAttributedString alloc]initWithString:@"is a feature to make gold transfers to fellow e-mas application users using " attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *b2 = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@" with a minimum transfer per day of %@ gram and maximum of %@ gram.", minLimitTrf, maxLimitTrf] attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                
                [final appendAttributedString:b1];
                [final appendAttributedString:wakalah];
                [final appendAttributedString:b2];
                
                self.textVDescription.attributedText = final;
            }
            break;
        }
        case 4:{
            [self.button setHidden:YES];
            [self.labelDontShow setHidden:YES];
            [self.imageView setImage:[UIImage imageNamed:@"ic_jual_emas_desc.png"]];
            [self.titlePop setText:@""];
            
            NSMutableAttributedString *nishab = [[NSMutableAttributedString alloc]initWithString:@"nishab" attributes:@{NSFontAttributeName : [UIFont italicSystemFontOfSize:13]}];
            NSMutableAttributedString *haul = [[NSMutableAttributedString alloc]initWithString:@"haul" attributes:@{NSFontAttributeName : [UIFont italicSystemFontOfSize:13]}];
            
            if([language isEqualToString:@"id"]){
                
                NSMutableAttributedString *first = [[NSMutableAttributedString alloc]initWithString:@"Emas yang Anda simpan pada aplikasi e-mas ini merupakan bagian dari Emas yang dikenai zakat apabila telah mencapai minimum 85 gram (" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *second = [[NSMutableAttributedString alloc]initWithString:@") dan melewati 1 tahun hijriyah (" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *last = [[NSMutableAttributedString alloc]initWithString:@")" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                [final appendAttributedString:first];
                [final appendAttributedString:nishab];
                [final appendAttributedString:second];
                [final appendAttributedString:haul];
                [final appendAttributedString:last];
                
                self.textVDescription.attributedText = final;
                [self.okButton setTitle:@"Saya Mengerti" forState:UIControlStateNormal];

            }else{
                NSMutableAttributedString *first = [[NSMutableAttributedString alloc]initWithString:@"The gold that you store in this e-mas app is part of the Gold that is subject to zakat if it reaches a minimum of 85 grams (" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *second = [[NSMutableAttributedString alloc]initWithString:@") and past 1 year of hijrah (" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *last = [[NSMutableAttributedString alloc]initWithString:@")" attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13]}];
                NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
                [final appendAttributedString:first];
                [final appendAttributedString:nishab];
                [final appendAttributedString:second];
                [final appendAttributedString:haul];
                [final appendAttributedString:last];
                
                self.textVDescription.attributedText = final;
                [self.okButton setTitle:@"I Understand" forState:UIControlStateNormal];
            }
            
            break;
        }
        default:
            break;
    }
}

- (void) setupLanguages{
    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        _labelDontShow.text = @"Jangan Tampilkan Lagi";
    }else{
        _labelDontShow.text = @"Don't Show Again";
    }
}

- (void)setPosition:(int)pos{
    position = pos;
}

- (IBAction)actionOkBtn:(id)sender {
    if(state == 1){
        switch (position) {
            case 0:
                [userDefaults setValue:@"YES" forKey:@"gpop_gb01"];
                break;
            case 1:
                [userDefaults setValue:@"YES" forKey:@"gpop_gs01"];
                break;
            case 2:
                [userDefaults setValue:@"YES" forKey:@"gpop_gc01"];
                break;
            case 3:
                [userDefaults setValue:@"YES" forKey:@"gpop_gt01"];
                break;
            
            default:
                break;
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionCheckBtn:(id)sender {
    if(state == 0){
        state = 1;
        [self.button setImage:[UIImage imageNamed:@"ic_checkbox_active.png"] forState:UIControlStateNormal];
    }else{
        state = 0;
        [self.button setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"] forState:UIControlStateNormal];
    }
}

@end
