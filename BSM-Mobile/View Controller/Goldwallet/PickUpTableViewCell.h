//
//  PickUpTableViewCell.h
//  BSM-Mobile
//
//  Created by ARS on 02/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PickUpTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *kcName;
@property (weak, nonatomic) IBOutlet UILabel *kcAddress;
@property (weak, nonatomic) IBOutlet UILabel *kcDistance;

@end
NS_ASSUME_NONNULL_END
