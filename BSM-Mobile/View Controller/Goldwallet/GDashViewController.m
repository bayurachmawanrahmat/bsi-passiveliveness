//
//  GDashViewController.m
//  BSM-Mobile
//
//  Created by ARS on 28/02/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GDashViewController.h"
#import "Utility.h"
#import "Styles.h"
#import "LibGold.h"

@interface GDashViewController (){
    UIStoryboard *goldWalletUIS;
    NSUserDefaults *userDefaults;
    
    NSString *language;
    NSString *totalGoldText;
    
    NSNumberFormatter* currencyFormatter;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *vwSetting;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
//@property (weak, nonatomic) IBOutlet UIView *vwRed;

@property (weak, nonatomic) IBOutlet UILabel *lblGoldTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleTotalGold;

@property (weak, nonatomic) IBOutlet UILabel *lblSalePriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSalePrice;

@property (weak, nonatomic) IBOutlet UILabel *lblBuyPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBuyPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblIconTitle1;
@property (weak, nonatomic) IBOutlet UILabel *lblIconTitle2;
@property (weak, nonatomic) IBOutlet UILabel *lblIconTitle3;
@property (weak, nonatomic) IBOutlet UILabel *lblIconTitle4;
@property (weak, nonatomic) IBOutlet UILabel *lblIconTitle5;
//@property (weak, nonatomic) IBOutlet UILabel *lblIconTitle6;

@property (weak, nonatomic) IBOutlet UIButton *btnIcon1;
@property (weak, nonatomic) IBOutlet UIButton *btnIcon2;
@property (weak, nonatomic) IBOutlet UIButton *btnIcon3;
@property (weak, nonatomic) IBOutlet UIButton *btnIcon4;
@property (weak, nonatomic) IBOutlet UIButton *btnIcon5;
//@property (weak, nonatomic) IBOutlet UIButton *btnIcon6;

@property (weak, nonatomic) IBOutlet UIView *vwGoldTotal;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *NSLCOfVwTitle;

@end

@implementation GDashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    currencyFormatter = [LibGold setCurrency];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [Styles setTopConstant:_NSLCOfVwTitle];
//    [Styles setStyleRoundedView:_vwGoldTotal];
    
    [self setupImageButton];
    [self setupLanguageLabel];
    
    [self reloadData];
    
//    [self.vwRed.layer setCornerRadius:self.vwRed.frame.size.height/2];
//    [self.vwRed.layer setMasksToBounds:true];
}

- (void)viewWillAppear:(BOOL)animated{
    [self getData];
}

- (void) reloadData{
    double salesPrice = [[[userDefaults objectForKey:@"gold_info"]objectForKey:@"hargaJualBSM"] doubleValue];
    double buyPrice = [[[userDefaults objectForKey:@"gold_info"]objectForKey:@"hargaBeliBSM"] doubleValue];
    
    self.lblTitleTotalGold.text = totalGoldText;
    self.lblGoldTotal.text = [NSString stringWithFormat:@"%@ Gram", [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]];
//    _lblSalePrice.text = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:salesPrice]];
//    _lblBuyPrice.text = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:buyPrice]];
    
    //posisi dibalik
    _lblSalePrice.text = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:buyPrice]];
    _lblBuyPrice.text = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:salesPrice]];
}

- (void) setupImageButton{
    [_lblIconTitle1 setTextColor:UIColorFromRGB(const_color_title)];
    [_lblIconTitle2 setTextColor:UIColorFromRGB(const_color_title)];
    [_lblIconTitle3 setTextColor:UIColorFromRGB(const_color_title)];
    [_lblIconTitle4 setTextColor:UIColorFromRGB(const_color_title)];
    [_lblIconTitle5 setTextColor:UIColorFromRGB(const_color_title)];
//    [_lblIconTitle6 setTextColor:UIColorFromRGB(bsm_green_color)];
//    [_lblIconTitle5 setHidden:YES];
//    [_lblIconTitle6 setHidden:YES];
    
//    [_btnIcon1 setImage:[UIImage imageNamed:@"ic_beli_emas.png"] forState:UIControlStateNormal];
//    [_btnIcon2 setImage:[UIImage imageNamed:@"ic_jual_emas.png"] forState:UIControlStateNormal];
//    [_btnIcon3 setImage:[UIImage imageNamed:@"ic_transfer_emas.png"] forState:UIControlStateNormal];
//    [_btnIcon4 setImage:[UIImage imageNamed:@"ic_tarik_emas.png"] forState:UIControlStateNormal];
//    [_btnIcon5 setImage:[UIImage imageNamed:@"ic_histori_emas.png"] forState:UIControlStateNormal];
//    --------------------------
//    [_btnIcon1 setImage:[UIImage imageNamed:@"ic_emsmenu_beli_mx_v2"] forState:UIControlStateNormal];
//    [_btnIcon2 setImage:[UIImage imageNamed:@"ic_emsmenu_jual_mx_v2"] forState:UIControlStateNormal];
//    [_btnIcon3 setImage:[UIImage imageNamed:@"ic_emsmenu_transfer_mx_v2"] forState:UIControlStateNormal];
//    [_btnIcon4 setImage:[UIImage imageNamed:@"ic_emsmenu_tarikfisik_mx_v2"] forState:UIControlStateNormal];
//    [_btnIcon5 setImage:[UIImage imageNamed:@"ic_emsmenu_history_mx_v2"] forState:UIControlStateNormal];
//    ---------------------
    [_btnIcon1 setImage:[UIImage imageNamed:@"ic_emsmenu_beli"] forState:UIControlStateNormal];
    [_btnIcon2 setImage:[UIImage imageNamed:@"ic_emsmenu_jual"] forState:UIControlStateNormal];
    [_btnIcon3 setImage:[UIImage imageNamed:@"ic_emsmenu_transfer"] forState:UIControlStateNormal];
    [_btnIcon4 setImage:[UIImage imageNamed:@"ic_emsmenu_tarikfisik"] forState:UIControlStateNormal];
    [_btnIcon5 setImage:[UIImage imageNamed:@"ic_emsmenu_history"] forState:UIControlStateNormal];
    
//    _btnIcon1.imageView.contentMode = UIViewContentModeCenter;
//    _btnIcon2.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    _btnIcon3.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    _btnIcon4.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    _btnIcon5.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [_btnIcon2 setContentMode:UIViewContentModeCenter];
//    [_btnIcon6 setImage:[UIImage imageNamed:@"ic_histori_emas.png"] forState:UIControlStateNormal];
    
//    [_btnIcon5 setHidden:YES];
//    [_btnIcon6 setHidden:YES];
    
    [_btnSetting addTarget:self action:@selector(gotoSetting) forControlEvents:UIControlEventTouchUpInside];
}

- (void) setupLanguageLabel{
    
    if(IPHONE_5){
        [_lblGoldTotal setFont:[_lblGoldTotal.font fontWithSize:12.0f]];
    }
    
    if([language isEqualToString:@"id"]){
        _lblTitle.text = @"e-mas";
//        _lblBuyPriceTitle.text = @"Harga Beli";
//        _lblSalePriceTitle.text = @"Harga Jual";
        _lblSalePriceTitle.text = @"Harga Beli";
        _lblBuyPriceTitle.text = @"Harga Jual";
        
        _lblIconTitle1.text = @"Beli";
        _lblIconTitle2.text = @"Jual";
        _lblIconTitle3.text = @"Transfer";
        _lblIconTitle4.text = @"Tarik Fisik";
        _lblIconTitle5.text = @"History";
//        _lblIconTitle6.text = @"Histori";
        
        totalGoldText = @"Total Emas";
    }else{
        _lblTitle.text = @"e-emas";

//        _lblBuyPriceTitle.text = @"Buy Price";
//        _lblSalePriceTitle.text = @"Sell Price"; //posisi dibalik
        _lblSalePriceTitle.text = @"Buy Price";
        _lblBuyPriceTitle.text = @"Sell Price";
        
        _lblIconTitle1.text = @"Buy";
        _lblIconTitle2.text = @"Sell";
        _lblIconTitle3.text = @"Transfer";
        _lblIconTitle4.text = @"Gold Pull";
        _lblIconTitle5.text = @"History";
//        _lblIconTitle6.text = @"History";
        
        totalGoldText = @"Gold Total";

    }
    
//    [_lblIconTitle1 setHidden:YES];
//    [_lblIconTitle2 setHidden:YES];
//    [_lblIconTitle3 setHidden:YES];
//    [_lblIconTitle4 setHidden:YES];
//    [_lblIconTitle5 setHidden:YES];
}

- (void) gotoSetting{
    TemplateViewController *list = [self.storyboard instantiateViewControllerWithIdentifier:@"LVP"];
    [self.tabBarController setSelectedIndex:0];
    [self.navigationController pushViewController:list animated:YES];
}


- (void) gotoRegistration{
    [self openTemplateByMenuID:@"00110"];
}

- (IBAction)actionBtn1:(id)sender { //buy
    [self openTemplateByMenuID:@"00111"];
}

- (IBAction)actionBtn2:(id)sender { //jual
    [self openTemplateByMenuID:@"00112"];
}

- (IBAction)actionBtn3:(id)sender { //transfer
    [self openTemplateByMenuID:@"00113"];
}

- (IBAction)actionBtn4:(id)sender { //Tarik Fisik
    [self openTemplateByMenuID:@"00159"];
}

- (IBAction)actionBtn5:(id)sender { //Hitory
    [self openTemplateByMenuID:@"00117"];

}

- (void) openTemplate{
    DataManager *dataManager = [DataManager sharedManager];
    NSArray *temp = [dataManager getJsonDataByMenuID];
    
    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        [dataManager setAction:[objectMain objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        [self pushNavTemplate:tempData];
    }else{
        
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        [self presentViewController:alertCont animated:YES completion:nil];
    }
}

-(void)pushNavTemplate : (NSDictionary *) tempData{
    @try {
        NSString *templateName = [tempData valueForKey:@"template"];
        TemplateViewController *templateViews;
        templateViews = [self.storyboard instantiateViewControllerWithIdentifier:templateName];

        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: templateName];
        if (stateAcepted) {
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];

        }else{
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateViews animated:YES];
        }
            
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
}

- (void) getData{
    NSString *strURL = [NSString stringWithFormat:@"request_type=gold_info,customer_id"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"gold_info"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            [[DataManager sharedManager]resetObjectData];
            
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"hargaJualBSM"] forKey:@"sell_price"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"hargaBeliBSM"] forKey:@"buy_price"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"noRekening"] forKey:@"gold_account"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"nama"] forKey:@"gold_account_name"];
            [userDefaults setValue:dataDict forKey:@"gold_info"];
           
            if([[dataDict valueForKey:@"statusRekening"] isEqualToString:@"0"]){
                [dataManager setMenuId:@"00110"];
                [userDefaults setValue:[dataDict valueForKey:@"status"] forKey:@"gold_state"];

                [self openTemplate];
            }
            
            [self reloadData];
            if (error == nil) {

            }
            else{
                //Error Alert
               NSLog(@"%@", [jsonObject objectForKey:@"response"]);
                
            }
        }else if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"14"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            
            [[DataManager sharedManager]resetObjectData];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"hargaJualBSM"] forKey:@"sell_price"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"hargaBeliBSM"] forKey:@"buy_price"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"noRekening"] forKey:@"gold_account"];
            [userDefaults setValue:dataDict forKey:@"gold_info"];
            [dataManager setMenuId:@"00110"];
            [userDefaults setValue:[dataDict valueForKey:@"status"] forKey:@"gold_state"];
            
            [self openTemplate];
        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
}



@end
