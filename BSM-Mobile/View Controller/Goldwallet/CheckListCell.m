//
//  CheckListCell.m
//  BSM-Mobile
//
//  Created by ARS on 18/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CheckListCell.h"

@implementation CheckListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    //ic_checkbox_active.png
    //ic_checkbox_inactive.png
    if (selected) {
        [self.checkButton setImage:[UIImage imageNamed:@"ic_checkbox_active"] forState:UIControlStateNormal];
    } else {
        [self.checkButton setImage:[UIImage imageNamed:@"ic_checkbox_inactive"] forState:UIControlStateNormal];
    }
}

@end
