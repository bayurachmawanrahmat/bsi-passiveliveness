//
//  GNPWPViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GNPWPViewController : TemplateViewController

- (void) setCode:(NSDictionary*) code;

@end

NS_ASSUME_NONNULL_END
