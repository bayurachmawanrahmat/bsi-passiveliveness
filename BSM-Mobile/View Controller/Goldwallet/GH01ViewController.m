//
//  GH01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 24/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GH01ViewController.h"
#import "Utility.h"
#import "LibGold.h"
#import "Styles.h"

NS_ENUM(NSInteger, MUTATIONSTATE){
    HARIAN = 0,
    BULANAN = 1
};

@interface GH01ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>{
    NSInteger mutationState;
    UIToolbar *toolbar;
    NSString *titleToolbar;
    UIDatePicker *datePickerFrom;
    UIDatePicker *datePickerTo;
    NSUserDefaults *userDefault;
    
    NSArray *month;
    NSArray *monthValue;
    NSArray *years;
    NSString *monthFromRangeValue;
    NSString *monthToRangeValue;

    NSDateComponents *componentsDate;
    
    BOOL isIna;

}

@property (weak, nonatomic) IBOutlet UITextField *tfGoldAccount;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleMutasi;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleDate;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;

@property (weak, nonatomic) IBOutlet UIImageView *imageBtn2;
@property (weak, nonatomic) IBOutlet UIImageView *imageBtn1;
@property (weak, nonatomic) IBOutlet UILabel *labelBtn1;
@property (weak, nonatomic) IBOutlet UILabel *labelBtn2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@property (weak, nonatomic) IBOutlet UILabel *labelRange;
@property (weak, nonatomic) IBOutlet UITextField *tfRangeFrom;
@property (weak, nonatomic) IBOutlet UITextField *tfRangeTo;

@end

@implementation GH01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_labelTitleBar setText:[self.jsonData valueForKey:@"title"]];
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self hideMutatinoSelection];
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    componentsDate = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    isIna = [[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"];
    
    if(isIna){
        titleToolbar = @"selesai";
        [_labelRange setText:@"s.d"];
        [_buttonCancel setTitle:@"BATAL" forState:UIControlStateNormal];
        [_buttonNext setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
        [_labelTitleDate setText:@"Pilih Tanggal Transaksi"];
    }else{
        titleToolbar = @"done";
        [_labelRange setText:@"to"];
        [_buttonCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_buttonNext setTitle:@"NEXT" forState:UIControlStateNormal];
        [_labelTitleDate setText:@"Select Transacstion Date"];

    }
    
    [Styles setStyleTextFieldBorder:_tfGoldAccount andRounded:YES];
    [Styles setStyleTextFieldBorderWithRightImage:_tfRangeFrom imageNamed:@"baseline_arrow_drop_down_black_24pt" andRounded:YES];
    [Styles setStyleTextFieldBorderWithRightImage:_tfRangeTo imageNamed:@"baseline_arrow_drop_down_black_24pt" andRounded:YES];

    
    [_tfRangeFrom setTextAlignment:NSTextAlignmentCenter];
    [_tfRangeTo setTextAlignment:NSTextAlignmentCenter];
    
    [_button1 addTarget:self action:@selector(mutationHistoriCheckAction) forControlEvents:UIControlEventTouchUpInside];
    [_button2 addTarget:self action:@selector(mutationHistoriCheckAction) forControlEvents:UIControlEventTouchUpInside];
    
    [_tfGoldAccount setText:[NSString stringWithFormat:@"%@ - %@ - %@ Gram",
                [[userDefault objectForKey:@"gold_info"]objectForKey:@"nama"],
                [[userDefault objectForKey:@"gold_info"]objectForKey:@"noRekening"],
                [[userDefault objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]]];
    [_tfGoldAccount setEnabled:NO];
    
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    
    [self setDatePickerLayout];
}

- (void) hideMutatinoSelection{
    _labelTitleMutasi.hidden = YES;
    _button1.hidden = YES;
    _button2.hidden = YES;
    _labelBtn1.hidden = YES;
    _labelBtn2.hidden = YES;
    _imageBtn1.hidden = YES;
    _imageBtn2.hidden = YES;
}

- (void) mutationStateChanged{
    
    if(mutationState == 0){
        [self setDatePickerLayout];
    }else{
        [self setMonthPicker];
        [self setMonthPickerLayout];
    }
    
    _tfRangeFrom.text = @"";
    _tfRangeTo.text = @"";
    
}

- (void) mutationHistoriCheckAction{
    if(mutationState == 0){
        mutationState = 1;
        [_imageBtn2 setImage:[UIImage imageNamed:@"ic_checkbox_active"]];
        [_imageBtn1 setImage:[UIImage imageNamed:@"ic_checkbox_inactive"]];
    }else if(mutationState == 1){
        mutationState = 0;
        [_imageBtn1 setImage:[UIImage imageNamed:@"ic_checkbox_active"]];
        [_imageBtn2 setImage:[UIImage imageNamed:@"ic_checkbox_inactive"]];
    }
    
    [self mutationStateChanged];
    [self doneClicked];
}

- (void) viewWillAppear:(BOOL)animated{
    [self requesting];
}

- (void) requesting{
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,pin",[self.jsonData valueForKey:@"url_parm"]] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
}

- (void) setMonthPicker{
    
    NSArray *monthsData = [[NSArray alloc]init];
    
    if(isIna){
        monthsData = @[@"Januari", @"Februari", @"Maret", @"April", @"Mei", @"Juni", @"Juli", @"Agustus", @"September", @"Oktober", @"November", @"Desember"];
    }else{
        monthsData = @[@"January", @"February", @"March", @"April", @"May", @"June", @"July", @"August", @"September", @"October", @"November", @"December"];
    }
    
    monthValue = @[@"01", @"02", @"03", @"04", @"05", @"06", @"07", @"08", @"09", @"10", @"11", @"12"];
    

   NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSInteger yearFounded = 1999;
    NSInteger currentYear = [components year];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    int i = 0;
    NSMutableArray *arYears = [[NSMutableArray alloc]init];
    for(i = 0; i <= currentYear - yearFounded; i++){
        [arYears addObject:[NSString stringWithFormat:@"%ld", yearFounded+i]];
    }
    
    month = monthsData;
    years = arYears;
}

-(void) doneClicked{
    [_tfRangeFrom resignFirstResponder];
    [_tfRangeTo resignFirstResponder];
    
}
- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;

    NSCalendar *calendar = [NSCalendar currentCalendar];

    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
        interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
        interval:NULL forDate:toDateTime];

    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
        fromDate:fromDate toDate:toDate options:0];

    return [difference day];
}

- (NSDate*) daysFromDate:(NSInteger) days fromDate:(NSDate*) date{
    
    NSDate *dateReturn = [[NSCalendar currentCalendar] dateByAddingUnit:NSCalendarUnitDay
                                                                  value:days
                                                                 toDate:date
                                                                options:0];
    return dateReturn;
}
- (void)dateChanged:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    NSString* selectedDate = [dateFormat stringFromDate:datePicker.date];
    
    if(datePicker.tag == 1){
        self.tfRangeFrom.text = selectedDate;

        datePickerTo.minimumDate = datePicker.date;
        datePickerTo.maximumDate = [NSDate date];

    }else if(datePicker.tag == 2){
        self.tfRangeTo.text = selectedDate;
        datePickerFrom.maximumDate = datePicker.date;
        datePickerFrom.minimumDate = [self daysFromDate:-30 fromDate:datePicker.date];
        
        NSLog(@"%@",[self daysFromDate:-30 fromDate:datePicker.date]);
        
        NSString *dateString = self.tfRangeFrom.text;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
        
        if ([datePicker.date compare:dateFromString] == NSOrderedAscending) {
            self.tfRangeFrom.text = selectedDate;
        }
        NSLog(@"%ld", (long)[dateFromString compare:datePicker.date]);
    }
}

- (void) setMonthPickerLayout{
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, 50)];
    [toolbar setBarStyle:UIBarStyleDefault];
    [toolbar setItems:@[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:titleToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked)]]];
    
    UIPickerView *pickerView1 = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    pickerView1.showsSelectionIndicator = YES;
    pickerView1.delegate = self;
    pickerView1.dataSource = self;
    pickerView1.tag = 0;
    
    [pickerView1 selectRow:componentsDate.month-1 inComponent:0 animated:YES];
    [pickerView1 selectRow:years.count-1 inComponent:1 animated:YES];

    
    UIPickerView *pickerView2 = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    pickerView2.showsSelectionIndicator = YES;
    pickerView2.delegate = self;
    pickerView2.dataSource = self;
    pickerView2.tag = 1;
    
    [pickerView2 selectRow:componentsDate.month-1 inComponent:0 animated:YES];
    [pickerView2 selectRow:years.count-1 inComponent:1 animated:YES];


    
    _tfRangeFrom.inputView = pickerView1;
    _tfRangeFrom.inputAccessoryView = toolbar;
    
    _tfRangeTo.inputView = pickerView2;
    _tfRangeTo.inputAccessoryView = toolbar;
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(component == 0){
        return month.count;
    }else{
        return years.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if(component == 0){
        return month[row];
    }else{
        return years[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    NSString *sMonth = @"";
    NSString *sYears = @"";
    NSString *sMonthVal = @"";
    
    if(component == 0){
        if(componentsDate.month - 1 < row && [pickerView selectedRowInComponent:1]==years.count-1){
            [pickerView selectRow:componentsDate.month-1 inComponent:0 animated:YES];
            sMonth = month[componentsDate.month-1];
            sMonthVal = monthValue[componentsDate.month-1];
        }else{
            sMonth = month[row];
            sMonthVal = monthValue[row];
        }
        sYears = years[[pickerView selectedRowInComponent:1]];
    }else{
        sMonthVal = monthValue[[pickerView selectedRowInComponent:0]];
        sMonth = month[[pickerView selectedRowInComponent:0]];
        sYears = years[row];
    }
    if(pickerView.tag == 0){
        _tfRangeFrom.text = [NSString stringWithFormat:@"%@-%@", sMonth, sYears];
        monthFromRangeValue = [NSString stringWithFormat:@"01-%@-%@",sMonthVal,sYears];
     }else{
         _tfRangeTo.text = [NSString stringWithFormat:@"%@-%@", sMonth, sYears];
         monthToRangeValue = [NSString stringWithFormat:@"01-%@-%@",sMonthVal,sYears];

    }
    
}

- (void) setDatePickerLayout{
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:titleToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked)]];
    
    datePickerFrom = [[UIDatePicker alloc]init];
    if(@available(iOS 13.4, *)) {
        [datePickerFrom setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePickerFrom setPreferredDatePickerStyle:UIDatePickerStyleWheels];
    }
    datePickerFrom.tag = 1;
    [datePickerFrom setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePickerFrom setBackgroundColor:[UIColor whiteColor]];
    [datePickerFrom setDatePickerMode:UIDatePickerModeDate];
    [datePickerFrom addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    [datePickerFrom setMinimumDate:[self daysFromDate:-30 fromDate:[NSDate date]]];
    [datePickerFrom setMaximumDate:[NSDate date]];
    [_tfRangeFrom setInputAccessoryView:toolbar];
    [_tfRangeFrom setInputView:datePickerFrom];
    
    datePickerTo = [[UIDatePicker alloc]init];
    if(@available(iOS 13.4, *)) {
        [datePickerTo setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePickerTo setPreferredDatePickerStyle:UIDatePickerStyleWheels];
    }
    datePickerTo.tag = 2;
    [datePickerTo setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePickerTo setBackgroundColor:[UIColor whiteColor]];
    [datePickerTo setDatePickerMode:UIDatePickerModeDate];
    [datePickerTo addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    [datePickerTo setMaximumDate:[NSDate date]];
    [_tfRangeTo setInputAccessoryView:toolbar];
    [_tfRangeTo setInputView:datePickerTo];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    NSString* selectedDate = [dateFormat stringFromDate:[NSDate date]];
    _tfRangeFrom.text = selectedDate;
    _tfRangeTo.text = selectedDate;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([[jsonObject objectForKey:@"response_code"]isEqualToString: @"00"]) {
        
    }else{
        
        if ([jsonObject valueForKey:@"response"]){
           if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
               NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
               UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
               [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                   [self gotoGold];
               }]];
               [self presentViewController:alertCont animated:YES completion:nil];

           } else {
               NSString * msg = [jsonObject valueForKey:@"response"];
               UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
               [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                   [self gotoGold];
               }]];
               [self presentViewController:alertCont animated:YES completion:nil];
           }
       } else {
           NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
           UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
           [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
               [self gotoGold];
           }]];
           [self presentViewController:alertCont animated:YES completion:nil];
       }
    }
}

- (IBAction)actionNext:(id)sender {
    if([_tfRangeTo.text isEqualToString:@""] || [_tfRangeFrom.text isEqualToString:@""]){
        NSString *message;
        if(isIna){
            message = @"silahkan pilih waktu yang diinginkan";
        }else{
            message = @"please select date range you want";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%@|%@",_tfRangeFrom.text, _tfRangeTo.text] forKey:@"list_trans"];
        [self openNextTemplate];
    }
}
- (IBAction)actionCancel:(id)sender {
    [self gotoGold];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    
    return [super canPerformAction:action withSender:sender];
}

@end
