//
//  GCamOverlayView.h
//  BSM-Mobile
//
//  Created by ARS on 24/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CameraOverlayView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol GCamOverlayDelegate;

@interface GCamOverlayView : UIView

@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageOverlay;
@property (weak, nonatomic) IBOutlet UIButton *switchButton;


@property (weak, nonatomic) id<GCamOverlayDelegate> delegate;

- (IBAction)hitBackButton:(id)sender;
- (IBAction)hitTakePhoto:(id)sender;
- (IBAction)hitSwitchButton:(id)sender;

@end


@protocol GCamOverlayDelegate <NSObject>

@required

-(void) didTakePhoto:(GCamOverlayView *) overlayView;
-(void) didBack: (GCamOverlayView *) overlayView;
-(void) switchCameraDevice: (GCamOverlayView *) overlayView;


@end

NS_ASSUME_NONNULL_END
