//
//  PopInfoViewController.h
//  BSM-Mobile
//
//  Created by ARS on 20/02/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PopInfoViewController : UIViewController
- (void) setPosition : (int) position;

@end

NS_ASSUME_NONNULL_END
