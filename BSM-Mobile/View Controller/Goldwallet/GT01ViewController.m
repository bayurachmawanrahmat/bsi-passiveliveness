//
//  GT01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 01/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GT01ViewController.h"
#import "PopInfoViewController.h"
#import "LibGold.h"
#import "Styles.h"

@interface GT01ViewController ()<UITextFieldDelegate>{
    UIColor *yellowColor;
    
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *destinationAccPlaceholder;
    NSString *totalGoldTransfPlaceholder;
    NSString *descriptionPlaceholder;
    NSString *alertMessage1;
    NSString *alertMessage2;
    NSString *alertMessage3;
    NSString *alertMessage4;
    NSString *alertMessage5;
    
    NSString *saldoAvailable;
    NSString *gtLimitMin;
    NSString *gtLimitMax;
    
    int maxLengthKeterangan;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *tfOriginGoldAcc;

@property (weak, nonatomic) IBOutlet UILabel *lblAimGoldAcc;
@property (weak, nonatomic) IBOutlet UITextField *tfAimGoldAcc;

@property (weak, nonatomic) IBOutlet UILabel *lblGoldTotalTransfer;
@property (weak, nonatomic) IBOutlet UITextField *tfGoldTotalTransfer;
@property (weak, nonatomic) IBOutlet UILabel *lblGram;

@property (weak, nonatomic) IBOutlet UILabel *lblKet;
@property (weak, nonatomic) IBOutlet UITextField *tfKet;

@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLimitKeterangan;

@end

@implementation GT01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    [_lblTitle setText: [self.jsonData objectForKey:@"title"]];
    
    maxLengthKeterangan = [[[userDefaults objectForKey:@"gold_info"]valueForKey:@"transfer.max.desc.length"]intValue];
    if(maxLengthKeterangan != 0){
        _lblLimitKeterangan.text = [NSString stringWithFormat:@"0 / %d",maxLengthKeterangan];
    }else{
        _lblLimitKeterangan.hidden = YES;
    }
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self setupLanguage];
    [self setupLayout];
    
    saldoAvailable = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"];
    gtLimitMin = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumTransfer"];
    gtLimitMax = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumTransfer"];
}

- (void) setupLayout{
    [Styles setTopConstant:_topConstraint];
    [Styles setStyleTextFieldBorder:_tfOriginGoldAcc andRounded:YES];
    

    [self.tfOriginGoldAcc setEnabled:NO];
    [self.tfOriginGoldAcc setText:[NSString stringWithFormat:@"%@ - %@ - %@ Gram",
        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"nama"],
        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"],
        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]]];
    
    [Styles setStyleTextFieldBottomLine:_tfAimGoldAcc];
    [self createToolbarButton:_tfAimGoldAcc];
    [self.tfAimGoldAcc setKeyboardType:UIKeyboardTypeNumberPad];
    [self.tfAimGoldAcc setPlaceholder:destinationAccPlaceholder];
    
    [Styles setStyleTextFieldBottomLine:_tfGoldTotalTransfer];
    [self.tfGoldTotalTransfer setDelegate:self];
    [self createToolbarButton:_tfGoldTotalTransfer];
    [self.tfGoldTotalTransfer setKeyboardType:UIKeyboardTypeDecimalPad];
    [self.tfGoldTotalTransfer setPlaceholder:totalGoldTransfPlaceholder];
    
    [Styles setStyleTextFieldBottomLine:_tfKet];
    [self createToolbarButton:_tfKet];
    [self.tfKet setPlaceholder:descriptionPlaceholder];
    [self.tfKet setDelegate:self];
    
    
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [self registerForKeyboardNotifications];
    
    if(![[userDefaults objectForKey:@"gpop_gt01"] isEqualToString:@"YES"] && ![userDefaults objectForKey:@"gpop_gt01"]){
        PopInfoViewController *popInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"POPINFOGOLD"];
        [popInfo setPosition:3];
        [self presentViewController:popInfo animated:YES completion:nil];
    }
}

- (void) setupLanguage{
    if([language isEqualToString:@"id"]){
        
        _lblAimGoldAcc.text = @"Masukkan No. Rekening Emas Tujuan";
        _lblGoldTotalTransfer.text = @"Masukkan Jumlah Emas yang di Transfer";
        _lblKet.text = @"Keterangan";
        
        [_btnNext setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"BATAL" forState:UIControlStateNormal];
        
        destinationAccPlaceholder = @"Masukkan No. Rekening Emas Tujuan";
        totalGoldTransfPlaceholder = @"Masukkan Berat Emas";
        descriptionPlaceholder = @"Masukkan Keterangan";
        alertMessage1 = @"Harap Masukkan Nomor Rekening Emas Tujuan";
        alertMessage2 = @"Harap Masukkan Jumlah Emas yang ingin ditransfer";
        alertMessage3 = @"Jumlah Emas yang ingin ditransfer melebihi saldo Emas anda";
        alertMessage4 = @"Jumlah Emas yang ingin ditransfer kurang limit minimum";
        alertMessage5 = @"Jumlah Emas yang ingin ditransfer melebihi limit maximum";
    }else{
        _lblAimGoldAcc.text = @"Enter Destination Gold Account Number";
        _lblGoldTotalTransfer.text = @"Enter the Amount of Gold Transfer";
        _lblKet.text = @"Description";
        
        [_btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
        
        destinationAccPlaceholder = @"Enter Destination Gold Account Number";
        totalGoldTransfPlaceholder = @"Enter weight of gold";
        descriptionPlaceholder = @"Enter Description";
        alertMessage1 = @"Please Enter Destination Gold Account Number";
        alertMessage2 = @"Please Enter Amount of Gold you want to transfer";
        alertMessage3 = @"The amount of Gold that you want to transfer exceeds your Gold balance";
        alertMessage4 = @"The amount of Gold that you want to transfer less than limit minimum";
        alertMessage5 = @"The amount of Gold that you want to transfer exceeds limit maximum";
    }
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

-(void)doneEditButtonTapped:(id)done{
    if([_tfAimGoldAcc isFirstResponder]){
        if([_tfAimGoldAcc.text isEqualToString:[[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"]]){
            _tfAimGoldAcc.text = @"";
            NSString *message = @"";
            if([language isEqualToString:@"id"]){
                message = @"Tidak dapat mengirim ke rekening yang sama";
            }else{
                message = @"cannot transfer to the same account";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [self.view endEditing:YES];
}

- (void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.scrollView.frame.size.height + kbSize.height + 20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, 351)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}



- (BOOL) validate{
    NSString *message = @"";
    if([_tfAimGoldAcc.text isEqualToString:@""]){
        message = alertMessage1;
    }else if([_tfGoldTotalTransfer.text isEqualToString:@""]){
        message = alertMessage2;
    }else if([[_tfGoldTotalTransfer.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue] > [saldoAvailable doubleValue]){
        message = alertMessage3;
    }else if([[_tfGoldTotalTransfer.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue] < [gtLimitMin doubleValue]){
        message = alertMessage4;
    }else if([[_tfGoldTotalTransfer.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue] > [gtLimitMax doubleValue]){
        message = alertMessage5;
    }
    
    if([_tfAimGoldAcc.text isEqualToString:[[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"]]){
        _tfAimGoldAcc.text = @"";
        NSString *message = @"";
        if([language isEqualToString:@"id"]){
            message = @"Tidak dapat mengirim ke rekening yang sama";
        }else{
            message = @"cannot transfer to the same account";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if(![message isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }

    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField != _tfAimGoldAcc && ![self.tfAimGoldAcc.text isEqualToString:@""]){
        if([_tfAimGoldAcc.text isEqualToString:[[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"]]){
            _tfAimGoldAcc.text = @"";
            NSString *message = @"";
            if([language isEqualToString:@"id"]){
                message = @"Tidak dapat mengirim ke rekening yang sama";
            }else{
                message = @"cannot transfer to the same account";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField == _tfGoldTotalTransfer){
        NSString *text = textField.text;
            
        if([string isEqualToString:@","] && [text containsString:@","]){
            return NO;
        }
        
        NSArray  *arrayOfString = [text componentsSeparatedByString:@","];
        if([arrayOfString count] == 2){
            if([arrayOfString[1] length] > 3 && ![string isEqualToString:@""]){
                return NO;
            }
        }
        return YES;
    }
    
    if(textField == _tfKet){
        NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if(newText.length <= maxLengthKeterangan){
            _lblLimitKeterangan.text = [NSString stringWithFormat:@"%lu /%d", (unsigned long)newText.length, maxLengthKeterangan];
            return YES;
        }else{
            return NO;
        }
    }

    return YES;
}

- (IBAction)actionCancel:(id)sender {
    [self gotoGold];
}

- (IBAction)actionNext:(id)sender {
    
    [dataManager.dataExtra setValue:_tfAimGoldAcc.text forKey:@"gold_account_dest"];
    [dataManager.dataExtra setValue:[LibGold goldToSentFromGram:[[_tfGoldTotalTransfer.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue]] forKey:@"weight"];
    [dataManager.dataExtra setValue:_tfKet.text forKey:@"description"];
    
    if([self validate]){
        [self openNextTemplate];
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
