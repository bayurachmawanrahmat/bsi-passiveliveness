//
//  HStackViewCell.h
//  BSM-Mobile
//
//  Created by Amini on 29/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HStackViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIStackView *stackview;

@end

NS_ASSUME_NONNULL_END
