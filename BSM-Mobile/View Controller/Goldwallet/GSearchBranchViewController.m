//
//  GSearchBranchViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GSearchBranchViewController.h"
#import "Styles.h"
//#import "CheckListCell.h"
#import <Mapkit/MapKit.h>
#import "PickUpTableViewCell.h"
//#import <CoreLocation/CoreLocation.h>


@interface GSearchBranchViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, MKMapViewDelegate, GSearchBranchDelegate>{
    
    NSUserDefaults *userDefaults;
    NSString *language;
    
    NSArray* nearbyListKCP;
    NSArray* displayList;
    NSArray* filteredList;
    
    BOOL isSearchActive;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    double latitude;
    double longitude;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GSearchBranchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [Styles setTopConstant:self.topConstraint];
    
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if(@available(iOS 13.0, *)){
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([language isEqualToString:@"id"]){
        [self.lblTitleBar setText:@"Cabang"];
    }else{
        [self.lblTitleBar setText:@"Branch"];
    }
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.searchBar.delegate = self;
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    self.searchBar.inputAccessoryView = toolbar;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PickUpTableViewCell" bundle:nil]
    forCellReuseIdentifier:@"PICKUPCELL"];
    
    [self.backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void) backButtonAction{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)doneEditButtonTapped:(id)done{
    [self.view endEditing:YES];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    displayList = nearbyListKCP;
    
    if(isSearchActive){
        return filteredList.count;
    }else{
        return displayList.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PickUpTableViewCell *cell = (PickUpTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PICKUPCELL"];
    NSDictionary* pickupData;
    
    if(isSearchActive){
        pickupData = filteredList[indexPath.row];
    }else{
        pickupData = displayList[indexPath.row];
    }
        
    cell.imageView.image = [UIImage imageNamed:@"ic_obd_cstr_kcabang"];
    if([pickupData objectForKey:@"branch_name"] != nil){
        cell.kcName.text = [pickupData objectForKey:@"branch_name"];
        cell.kcAddress.text = [NSString stringWithFormat:@"%@", [pickupData objectForKey:@"branch_address"]];
    }
    if([pickupData objectForKey:@"latitude"]){
        cell.kcDistance.text = [NSString stringWithFormat:@"%.2f km", [self distanceCalcWithLatitude1:latitude
                           longitude1:longitude
                            latitude2:[[pickupData objectForKey:@"latitude"]doubleValue]
                           longitude2:[[pickupData objectForKey:@"longitude"]doubleValue]]];
    }else{
        cell.kcDistance.text = @"";
    }
      
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *selectedLocation = @"";
    if(isSearchActive){
        selectedLocation = [filteredList[indexPath.row] objectForKey:@"branch_name"];
        [delegate selectedKC:filteredList[indexPath.row]];
        [self.searchBar endEditing:true];
        [self backButtonAction];
    }else{
        selectedLocation = [displayList[indexPath.row] objectForKey:@"branch_name"];
        [delegate selectedKC:displayList[indexPath.row]];
        [self.searchBar endEditing:true];
        [self backButtonAction];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    isSearchActive = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if([searchBar.text isEqualToString:@""]){
        isSearchActive = NO;
    }else{
        isSearchActive = YES;
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if([searchBar.text isEqualToString:@""]){
        isSearchActive = NO;
    }else{
        isSearchActive = YES;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    
    NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"branch_name", searchText];

    NSArray *filterArrays = [displayList filteredArrayUsingPredicate:predicateString];
    
    filteredList = filterArrays;
    if([searchText isEqualToString:@""]){
        filteredList = displayList;
    }
    [self.tableView reloadData];

}

- (double) distanceCalcWithLatitude1:(double)latitude1 longitude1:(double)longitude1 latitude2:(double)latitude2 longitude2:(double)longitude2{
    CLLocation *loc1 = [[CLLocation alloc]  initWithLatitude:latitude1 longitude:longitude1];
    CLLocation *loc2 = [[CLLocation alloc]  initWithLatitude:latitude2 longitude:longitude2];
    double dMeters = [loc1 distanceFromLocation:loc2];
    
    return dMeters/1000;
}

- (void) setList:(NSArray *)listBranch{
    nearbyListKCP = listBranch;
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

@end
