//
//  GoldUtil.h
//  BSM-Mobile
//
//  Created by ARS on 07/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LibGold : NSObject

+ (NSString *) getCurrencyForm:(double) value;
+ (NSNumberFormatter *) setCurrency;
+ (NSString *) calculateToGold : (double) value withPrice:(double) pricePerGram;
+ (NSString *) calculateToMoney : (double) value withPrice:(double) pricePerGram;
+ (NSString *) amountToSent: (double) value withPrice: (double) pricePerGram;
+ (NSString *) goldToSent: (double) value withPrice: (double) pricePerGram;
+ (NSString *) goldToSentFromGram: (double) value;
+ (NSString *) getGramFormOfValue:(double) value;
+ (NSString *) getCurrencyForm:(double)value withDigits:(NSInteger) digits;
+ (NSString *) getMillionValue:(double)value;
+ (NSString *) getCurrencyFormNoRound:(double)value;

@end
