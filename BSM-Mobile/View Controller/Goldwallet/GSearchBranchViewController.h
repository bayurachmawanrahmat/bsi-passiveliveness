//
//  GSearchBranchViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

NS_ASSUME_NONNULL_BEGIN

@interface GSearchBranchViewController : UIViewController
{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;
- (void) setList: (NSArray *) listBranch;

@end

@protocol GSearchBranchDelegate

@required

- (void) selectedKC : (NSDictionary*) dictKC;

@end


NS_ASSUME_NONNULL_END
