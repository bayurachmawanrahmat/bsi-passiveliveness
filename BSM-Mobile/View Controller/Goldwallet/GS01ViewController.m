//
//  GS01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 01/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GS01ViewController.h"
#import "PopInfoViewController.h"
#import "LibGold.h"
#import "Styles.h"

@interface GS01ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSNumberFormatter* currencyFormatter;
    
    NSArray *listAccount;
    
    NSString *gsPrice;
    NSString *gsLimitMin;
    NSString *gsLimitMax;
    NSString *gsMaxFlag;
    
    NSString *alertMsg1;
    NSString *alertMsg2;
    NSString *alertMsg3;
    NSString *alertMsg4;
    NSString *alertMsg5;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UITextField *tfGoldAcc;

@property (weak, nonatomic) IBOutlet UIView *vwGoldToday;
@property (weak, nonatomic) IBOutlet UILabel *lblGoldTodayTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGoldTodayPrice;

@property (weak, nonatomic) IBOutlet UILabel *lblGoldSaleTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfAmountGoldSale;
@property (weak, nonatomic) IBOutlet UILabel *lblGram;

@property (weak, nonatomic) IBOutlet UILabel *lblSalesResultTitle;
@property (weak, nonatomic) IBOutlet UIView *vwSalesResult;
@property (weak, nonatomic) IBOutlet UIView *vwAmountReceive;
@property (weak, nonatomic) IBOutlet UILabel *lblSalesResult;
@property (weak, nonatomic) IBOutlet UILabel *lblReceiveAmounTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblReceiveAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblPph;

@property (weak, nonatomic) IBOutlet UILabel *lblReceiveAccTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfReceiveAcc;

@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation GS01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    currencyFormatter = [LibGold setCurrency];
    [_lblTitle setText: [self.jsonData objectForKey:@"title"]];

    [self setupLanguage];
    [self setupLayout];
    
    gsPrice = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"hargaJualBSM"];
    gsLimitMin = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumJual"];
    gsMaxFlag = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"jual.max.flag"];
    
    if([gsMaxFlag isEqualToString:@"1"]){
        gsLimitMax = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"jual.max"];
    }else{
        gsLimitMax = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMaximumJual"];
    }

    [self.tfGoldAcc setEnabled:NO];
    [self.tfGoldAcc setText:[NSString stringWithFormat:@"%@ - %@ - %@ Gram",
                        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"nama"],
                        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"noRekening"],
                        [[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"]]];
        
    [self.lblGoldTodayPrice setText:[NSString stringWithFormat:@"Rp. %@", [LibGold getCurrencyForm:[gsPrice doubleValue]]]];
}

- (void)viewWillAppear:(BOOL)animated{
    [self getListAccount];
}

- (void) setupLayout{
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [Styles setTopConstant:_topConstraint];
    [Styles setStyleTextFieldBorder:_tfGoldAcc andRounded:YES];
    [Styles setStyleRoundedView:_vwGoldToday];
    
    [Styles setStyleTextFieldBottomLine:_tfAmountGoldSale];
    [self createToolbarButton:_tfAmountGoldSale];
    [self.tfAmountGoldSale setKeyboardType:UIKeyboardTypeDecimalPad];
    [self.tfAmountGoldSale addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.tfAmountGoldSale setDelegate:self];

    
    [Styles setStyleTextFieldBottomLine:_tfReceiveAcc];
    [self createToolbarButton:_tfReceiveAcc];
    [self createPicker:_tfReceiveAcc withTag:0];
    
    [Styles setStyleRoundedView:_vwSalesResult];
    [Styles setStyleRoundedView:self.vwAmountReceive];
//    [Styles setStyleButton:_btnNext];
//    [Styles setStyleButton:_btnCancel];
    [self.lblPph setHidden:YES];
    
    if(![[userDefaults objectForKey:@"gpop_gs01"] isEqualToString:@"YES"] && ![userDefaults objectForKey:@"gpop_gs01"]){
        PopInfoViewController *popInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"POPINFOGOLD"];
        [popInfo setPosition:1];
        [self presentViewController:popInfo animated:YES completion:nil];
    }
}

- (void) setupLanguage{
    if([language isEqualToString:@"id"]){

        [self.tfAmountGoldSale setPlaceholder:@"Masukkan Berat"];
        [self.tfReceiveAcc setPlaceholder:@"Rekening Penerima"];
        
        alertMsg1 = @"Jumlah Emas yang anda Masukkan melebihi saldo emas anda";
        alertMsg2 = @"Harap Masukkan Nomor Rekening penerimaan penjualan emas";
        alertMsg3 = @"Jumlah emas yang anda masukkan melebihi limit maximum jual";
        alertMsg4 = @"Harap masukkan jumlah emas yang akan di jual";
        alertMsg5 = @"Jumlah emas yang anda masukkan kurang dari limit minimum jual";
        
        _lblGoldTodayTitle.text = @"Harga Emas Hari ini";
        _lblGoldSaleTitle.text = @"Masukkan Jumlah Emas yang akan dijual";
        _lblSalesResultTitle.text = @"Dana Hasil Penjualan";
        _lblReceiveAmounTitle.text = @"Dana yang diterima";
        _lblReceiveAccTitle.text = @"Dana Penjualan Masuk Ke Rekening";
        
        [_btnNext setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"BATAL" forState:UIControlStateNormal];
    }else{
        [self.tfAmountGoldSale setPlaceholder:@"Enter Weight of Gold"];
        [self.tfReceiveAcc setPlaceholder:@"Enter Receiver Account"];
        
        alertMsg1 = @"The amount of gold you enter exceeds your gold balance";
        alertMsg2 = @"Please Enter the Gold Sales Receiver Account Number";
        alertMsg3 = @"The Amount of Gold you enter exceeds sales limit";
        alertMsg4 = @"Please Enter amount of Gold you want to sell";
        alertMsg5 = @"The amount of gold that you enter is less than the minimum selling limit";
        
        _lblGoldTodayTitle.text = @"Gold Price Today";
        _lblGoldSaleTitle.text = @"Enter the Amount of Gold to be sold";
        _lblSalesResultTitle.text = @"Sales results Funds";
        _lblReceiveAmounTitle.text = @"Funds Receive";
        _lblReceiveAccTitle.text = @"Sales Funds Account";
        
        [_btnNext setTitle:@"NEXT" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    }
    

}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listAccount.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return listAccount[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(row == 0){
        self.tfReceiveAcc.text = @"";
    }else{
        self.tfReceiveAcc.text = [listAccount objectAtIndex:row];
    }
}

- (void) textFieldDidChange: (UITextField *)textField{
    double value = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    
    _lblSalesResult.text = [LibGold calculateToMoney:value withPrice:[gsPrice doubleValue]];
    [dataManager.dataExtra setValue:[LibGold amountToSent:value withPrice:[gsPrice doubleValue]] forKey:@"amount"];
    [dataManager.dataExtra setValue:[LibGold goldToSentFromGram:value] forKey:@"weight"];
    
    //ProsentasePajakJualNONPWP
    //ProsentasePajakJual
    double amount = value * [gsPrice doubleValue];
    double res = 0;
    
    if( amount > [[[userDefaults objectForKey:@"gold_info"]valueForKey:@"NominalJualKenaPajak"]doubleValue] ){
        if([[[userDefaults objectForKey:@"gold_info"] objectForKey:@"verifiedNPWP"] isEqualToString:@"Y"]){
            res = amount * [[[userDefaults objectForKey:@"gold_info"]valueForKey:@"ProsentasePajakJual"]doubleValue];
        }else{
            double paj = [[[userDefaults objectForKey:@"gold_info"]valueForKey:@"ProsentasePajakJualNONPWP"]doubleValue];
            res = amount * paj;
        }
        [self.lblPph setHidden:NO];
    }else{
        [self.lblPph setHidden:YES];
    }
    if([language isEqualToString:@"id"]){
        self.lblPph.text = [NSString stringWithFormat:@"PPH atas penjualan emas anda Rp. %@,00", [LibGold getCurrencyForm:res]];
    }else{
        self.lblPph.text = [NSString stringWithFormat:@"PPH Tax on your gold sale Rp. %@,00", [LibGold getCurrencyForm:res]];
    }
    //pph sama funds_received
    [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f",res] forKey:@"pph"];
    [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%f",amount-res] forKey:@"funds_received"];
    
    self.lblReceiveAmount.text = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:amount-res]];
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

-(void)doneEditButtonTapped:(id)done{
    [self.view endEditing:YES];
}

- (void) getListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
    
    if(listAcct.count != 0 || listAcct != nil){
        NSMutableArray *newData = [[NSMutableArray alloc]init];
        
        if ([language isEqualToString:@"id"]) {
            [newData addObject:@"Pilih Nomor Rekening"];
        }else{
            [newData addObject:@"Select Account Number"];
        }
        
        for(NSDictionary *temp in listAcct){
            [newData addObject:[temp valueForKey:@"id_account"]];
        }
        listAccount = newData;
    }else{
        
        NSString *strURL = [NSString stringWithFormat:@"request_type=list_account1, customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField == _tfAmountGoldSale){
        NSString *text = textField.text;
            
        if([string isEqualToString:@","] && [text containsString:@","]){
            return NO;
        }
        
        NSArray  *arrayOfString = [text componentsSeparatedByString:@","];
        if([arrayOfString count] == 2){
            if([arrayOfString[1] length] > 3 && ![string isEqualToString:@""]){
                return NO;
            }
        }
        return YES;
    }

    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account1"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getListAccount];

        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToRoot];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
}

- (BOOL) validate{
    NSString *alertMessage = @"";
    if([gsMaxFlag isEqualToString:@"1"]){
        if([[dataManager.dataExtra objectForKey:@"amount"]doubleValue] > [gsLimitMax doubleValue]){
            alertMessage = alertMsg3;
        }
    }else if([[self.tfAmountGoldSale.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue] > [gsLimitMax doubleValue]){
        alertMessage = alertMsg3;
    }
    if([[self.tfAmountGoldSale.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue] > [[[userDefaults objectForKey:@"gold_info"]objectForKey:@"saldoAvailable"] doubleValue]){
        alertMessage = alertMsg1;
    }
    if([[self.tfAmountGoldSale.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue] < [gsLimitMin doubleValue]){
        alertMessage = alertMsg5;
    }else if([self.tfReceiveAcc.text isEqualToString:@""]){
        alertMessage = alertMsg2;
    }
    
    if([_tfAmountGoldSale.text isEqualToString:@""]){
        alertMessage = alertMsg4;
    }
    
    if(![alertMessage isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (IBAction)actionNext:(id)sender {
    
    /*
     amount = "";
    "buy_price" = "";
    "gold_account" = "";
    "id_account" = "";
    "sell_price" = "";
     weight = "";
     **/

    [dataManager.dataExtra setValue:self.tfReceiveAcc.text forKey:@"id_account"];
    
    if([self validate]){
        [self openNextTemplate];
    }
}
- (IBAction)actionCancel:(id)sender {
    [self gotoGold];
}

@end
