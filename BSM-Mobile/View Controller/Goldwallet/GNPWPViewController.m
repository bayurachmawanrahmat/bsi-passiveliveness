//
//  GNPWPViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 29/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GNPWPViewController.h"
#import "GCamOverlayView.h"
#import "CamOverlayController.h"
#import "Styles.h"
#import "Utility.h"


@interface GNPWPViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, GCamOverlayDelegate, UITextFieldDelegate>{
    NSDictionary *mCode;
    NSUserDefaults *userDefault;
    NSString *language, *titleLabel;
    UIImagePickerController *imagePicker;
    BOOL takeNPWP;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBck;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstsny;
@property (weak, nonatomic) IBOutlet UILabel *lblNPWP;
@property (weak, nonatomic) IBOutlet UITextField *tfNpwp;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoto;
@property (weak, nonatomic) IBOutlet UITextField *tfPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblStats;
@property (weak, nonatomic) IBOutlet UIView *vwStats;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UIView *vwNpwp;
@property (weak, nonatomic) IBOutlet UIView *vwPhoto;
@property (weak, nonatomic) IBOutlet UIView *vwStatus;

@property (weak, nonatomic) IBOutlet UIButton *btnRetakeNpwp;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;

@end

@implementation GNPWPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    [Styles setTopConstant:self.topConstsny];
//    [Styles setStyleButton:self.btnOK];
//    [Styles setStyleButton:self.btnRetakeNpwp];
    takeNPWP = NO;
    
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditing)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace, doneButton]];
    [self.tfNpwp setInputAccessoryView:toolbar];
    
    imagePicker.delegate = self;
    self.tfPhoto.delegate = self;
    self.tfNpwp.keyboardType = UIKeyboardTypeNumberPad;
    
    [self.btnRetakeNpwp addTarget:self action:@selector(actionRetakeNPWP) forControlEvents:UIControlEventTouchUpInside];
    [self.btnOK addTarget:self action:@selector(actionDoneButton) forControlEvents:UIControlEventTouchUpInside];
    [self.btnBck addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self setupView];
    
}

- (void)doneEditing{
    
    if(self.tfNpwp.isFirstResponder && ![self.tfNpwp.text isEqualToString:@""]){
        if(self.tfNpwp.text.length == 15){
            if([self.tfNpwp.text isEqualToString:@"000000000000000"] || [self.tfNpwp.text doubleValue] == 0){
                NSString* msg = @"";
                if([language isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP tidak valid";
                }else{
                    msg = @"NPWP Number is not valid";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else if([self.tfNpwp.text isEqualToString:@"000000000000000"] ||
           [self.tfNpwp.text doubleValue] == 0){
            NSString* msg = @"";
            if([language isEqualToString:@"id"])
            {
                msg = @"Nomor NPWP tidak valid";
            }else{
                msg = @"NPWP Number is not valid";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            NSString *msg = @"";
            if(self.tfNpwp.text.length != 15){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP harus 15 digit";
                }else{
                    msg = @"NPWP Number Should have 15 digits";
                }
            }

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
    [self.view endEditing:YES];
}

- (void) setupView{
    self.lblNPWP.text = @"Nomor Pokok Wajib Pajak";
    self.vwStats.hidden = NO;

    NSString *verified = @"";
    NSString *decline = @"";
    NSString *waiting = @"";
    
    if([language isEqualToString:@"id"]){
        verified = @"Terverifikasi";
        decline = @"Ditolak";
        waiting = @"Menunggu Verifikasi";

        titleLabel = [mCode objectForKey:@"label_id"];
        self.tfNpwp.placeholder = @"Masukkan Nomor NPWP";
        self.lblPhoto.text = @"Foto NPWP";
        self.tfPhoto.placeholder = @"Unggah foto NPWP anda";
        self.lblStats.text = @"Status : ";
        self.lblTitleBar.text = titleLabel;
        
        [self.btnRetakeNpwp setTitle:@"Input NPWP Ulang" forState:UIControlStateNormal];
    }else{
        verified = @"Verified";
        decline = @"Rejected";
        waiting = @"Waiting for verification";

        titleLabel = [mCode objectForKey:@"label_id"];
        self.tfNpwp.placeholder = @"Enter Your NPWP Number";
        self.lblPhoto.text = @"NPWP photo";
        self.tfPhoto.placeholder = @"Upload your NPWP photo";
        self.lblStats.text = @"Status : ";
        self.lblTitleBar.text = titleLabel;
        
        [self.btnRetakeNpwp setTitle:@"Re-Input NPWP" forState:UIControlStateNormal];
    }
    
    self.vwPhoto.hidden = YES;
    self.vwStatus.hidden = NO;
    self.tfNpwp.text = @"xxxxxxxxxxxxxxx";
    self.tfNpwp.enabled = NO;
    self.tfNpwp.textColor = [UIColor lightGrayColor];
    
        NSString *state = [[userDefault objectForKey:@"gold_info"]objectForKey:@"verifiedNPWP"];
//    NSString *state = @"R";
    
    if([state isEqualToString:@"Y"]){
        self.lblStatus.text = verified;
        self.lblStatus.textColor = [UIColor whiteColor];
        self.vwStats.backgroundColor = UIColorFromRGB(const_color_primary);
        self.vwStats.layer.cornerRadius = 8;
    }else if([state isEqualToString:@"W"]){
        self.lblStatus.text = waiting;
        self.lblStatus.textColor = [UIColor whiteColor];
        self.vwStats.backgroundColor = UIColorFromRGB(const_color_primary);
    }else if([state isEqualToString:@"R"]){
        self.lblStatus.text = decline;
        self.lblStatus.textColor = [UIColor whiteColor];
        self.vwStats.backgroundColor = [UIColor redColor];
        self.btnRetakeNpwp.hidden = NO;
    }else{
        [self actionRetakeNPWP];
    }
}

- (void)setCode:(NSDictionary *)code{
    mCode = code;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if(textField == self.tfPhoto){
        [self actionTakePhoto];
    }
}

- (void) actionRetakeNPWP{
    takeNPWP = YES;
    self.vwPhoto.hidden = NO;
    self.vwStatus.hidden = YES;
    self.tfNpwp.text = @"";
    self.tfNpwp.enabled = YES;
    self.tfNpwp.textColor = [UIColor blackColor];
}

- (void) actionDoneButton{
    if(takeNPWP){
        if(self.tfNpwp.text.length == 15 && ![self.tfPhoto.text isEqualToString:@""]){
            
            if([self.tfNpwp.text isEqualToString:@"000000000000000"] || [self.tfNpwp.text doubleValue] == 0){
                NSString* msg = @"";
                if([language isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP tidak valid";
                }else{
                    msg = @"NPWP Number is not valid";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                [dataManager.dataExtra setValue:self.tfNpwp.text forKey:@"npwp"];
                NSString *strURL = [mCode objectForKey:@"url_param"];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
            }
            
        }else{
            NSString *msg = @"";
            
            if([self.tfNpwp.text isEqualToString:@""]){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Anda belum menambahkan nomor NPWP";
                }else{
                    msg = @"You have not fill NPWP Numbers";
                }
            }
            else if(self.tfNpwp.text.length != 15){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP harus 15 digit";
                }else{
                    msg = @"NPWP Number Should have 15 digits";
                }
            }else if([self.lblPhoto.text isEqualToString:@""]){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Ambil Foto NPWP Anda untuk Unggah";
                }else{
                    msg = @"Take Photo NPWP to Upload";
                }
            }else if([self.tfPhoto.text isEqualToString:@""]){
                if([language isEqualToString:@"id"])
                {
                    msg = @"Ambil Foto NPWP Anda untuk Unggah";
                }else{
                    msg = @"Take Photo NPWP to Upload";
                }
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        [self actionBack];
    }
}

- (void) actionTakePhoto{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    CamOverlayController *overlayController = [[CamOverlayController alloc] init];

    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    imagePicker.showsCameraControls = false;
    imagePicker.allowsEditing = false;

    GCamOverlayView *cameraView = (GCamOverlayView *)overlayController.view;
    cameraView.frame = imagePicker.view.frame;
    cameraView.descriptionLabel.text = NSLocalizedString(@"DESC_NPWP_POSITION", @"");
    cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_ktp@2x.png"];
    cameraView.titleLabel.text = titleLabel;
    cameraView.delegate = self;
    
    imagePicker.cameraOverlayView = cameraView;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)didTakePhoto:(GCamOverlayView *)overlayView{
    [imagePicker takePicture];
}

- (void)didBack:(GCamOverlayView *)overlayView{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *base64Imge = [Utility compressAndEncodeToBase64String:image withMaxSizeInKB:40];

    [dataManager.dataExtra setValue:base64Imge forKey:@"npwp_photo"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [self.tfPhoto setText:[NSString stringWithFormat:@"NPWP_%@.jpg",[dateFormatter stringFromDate:[NSDate date]]]];
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)switchCameraDevice:(GCamOverlayView *)overlayView{
    if(imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceRear)
    {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }else{
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"gold_setting"]){
        NSString *response = [jsonObject objectForKey:@"response"];
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            
            NSString *msg;
            if([[mCode objectForKey:@"code"]isEqualToString:@"1"]){
                if([language isEqualToString:@"id"]){
                    msg  = [NSString stringWithFormat:@"Update NPWP Berhasil"];
                }else{
                     msg = [NSString stringWithFormat:@"Update NPWP is Success"];
                }
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,response] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self gotoGold];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
