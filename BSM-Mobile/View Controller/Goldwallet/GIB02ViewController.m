//
//  GIB02ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 28/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "GIB02ViewController.h"
#import "Utility.h"
#import "Styles.h"

@interface GIB02ViewController ()<UITextFieldDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelInput;
@property (weak, nonatomic) IBOutlet UITextField *tfInput;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation GIB02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:_topConstant];
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([language isEqualToString:@"id"]){
        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
    }else{
        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    self.tfInput.delegate = self;
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.tfInput.frame.size.height);
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.tfInput.leftView = paddingPhone;
    self.tfInput.leftViewMode = UITextFieldViewModeAlways;
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    //NSLog([self.jsonData valueForKey:@"content"]);
    if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
        [self.tfInput setSecureTextEntry:true];
        [self.tfInput setPlaceholder:@"pin"];
        [self.tfInput setKeyboardType:UIKeyboardTypeNumberPad];
        [self.labelInput setText:@"PIN"];
    }else{
        [self.tfInput setPlaceholder:[self.jsonData valueForKey:@"content"]];
        [self.labelInput setText:[self.jsonData valueForKey:@"content"]];
    }
    
    [self createToolbarButton:_tfInput];
    
    [Styles setStyleTextFieldBottomLine:_tfInput];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.tfInput resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSRange whiteSpaceRange = [string rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        textField.text = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        return false;
    } else {
        return true;
    }
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClick:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void)doneClick:(id)sender{
    [self.view endEditing:YES];
}

- (IBAction)actionCancel:(id)sender {
    [self gotoGold];
}
- (IBAction)actionNext:(id)sender {
    if([self.tfInput.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldInput = [self.tfInput.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedStringtextFieldInput length]) {
            message = lang(@"STR_NOT_NUM");
        }
        
        if([message isEqualToString:@""]){
            if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
                [dataManager setPinNumber:self.tfInput.text];
                [super openNextTemplate];
            } else {
                if ([[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.tfInput.text}];
                    [super openNextTemplate];
                }
                else if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"pan"]){
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.tfInput.text}];
                    [super openNextTemplate];
                }
                else {
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.tfInput.text}];
                    [super openNextTemplate];
                }
            }
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

@end
