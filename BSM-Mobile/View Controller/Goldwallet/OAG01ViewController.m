//
//  GR01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 19/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "OAG01ViewController.h"
#import "GCamOverlayView.h"
#import "GSearchBranchViewController.h"
#import "CamOverlayController.h"
#import "Utility.h"
#import "Styles.h"
#import "LibGold.h"
#import "Connection.h"


@interface OAG01ViewController ()<GCamOverlayDelegate, GSearchBranchDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    
    NSArray *paymentAccPickerData;
    
    NSString *requestType;
    NSString *urlSyarat;
    
    NSString *descriptionNPWP;
    NSString *cameraTitleLabel;
    
    
    int checkState1;
    int checkState2;
    int checkState3;
    
    
    NSString *msgAlert1,
     *msgAlert2,
     *msgAlert3,
     *msgAlert4,
     *msgAlert5,
     *msgAlert6;
    
    NSString *lang;
    NSUserDefaults *userDefaults;
    NSNumberFormatter *currencyFormatter;
    NSArray *labelArray;
    NSArray *listBranch;
    
    UIImagePickerController *imagePicker;
    BOOL listAccountIsRequested;
    
    NSString *labelBuyingTitleWeight;
    NSString *labelBuyingTitlePrice;
    
    NSString *gbPrice;
    NSString *gbLimitMin;
//    NSString *gbLimitMax;
    
    BOOL isSetoranValid;
}

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNPWP;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNPWPPhoto;
@property (weak, nonatomic) IBOutlet UITextField *tfNPWPNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfNPWPPhoto;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *label4;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn2;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSetoranAwal;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UITextField *tfSetoranAwal;
@property (weak, nonatomic) IBOutlet UILabel *lblHintSetoranAwal;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleAccount;
@property (weak, nonatomic) IBOutlet UITextField *tfAccount;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn3;
@property (weak, nonatomic) IBOutlet UILabel *label5;
@property (weak, nonatomic) IBOutlet UILabel *labelConvertion;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsAndConditions;
@property (weak, nonatomic) IBOutlet UILabel *lblBranch;
@property (weak, nonatomic) IBOutlet UITextField *tfBranch;

@property (weak, nonatomic) IBOutlet UILabel *labelTopTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelBtn;
@property (weak, nonatomic) IBOutlet CustomBtn *nextBtn;

@end

@implementation OAG01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    lang = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    currencyFormatter = [LibGold setCurrency];
    labelArray = [[NSArray alloc]init];
    paymentAccPickerData = [[NSArray alloc]init];
    
    [self.labelTopTitle setText:[self.jsonData valueForKey:@"title"]];

    self.labelTopTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTopTitle.textAlignment = const_textalignment_title;
    self.labelTopTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTopTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    
    gbLimitMin = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"LimitMinimumBeliPerdana"];
    gbPrice = [[userDefaults objectForKey:@"gold_info"]objectForKey:@"hargaBeliBSM"];

    checkState1 = 0;
    checkState2 = 0;
    checkState3 = 0;
    
    [Styles setTopConstant:_topConstraint];
    [Styles setStyleSegmentControl:_segmentControl];
    
    [self.nextBtn setColorSet:PRIMARYCOLORSET];
    [self.cancelBtn setColorSet:SECONDARYCOLORSET];
    
    [self setupTextField];
    
    if([lang isEqualToString:@"id"]){
        [_cancelBtn setTitle:@"BATAL" forState:UIControlStateNormal];
        [_nextBtn setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
        [_btnTermsAndConditions setTitle:@"Syarat dan Ketentuan (wajib di klik)" forState:UIControlStateNormal];
        
        [_tfNPWPNumber setPlaceholder:@"Tuliskan Nomor Pokok Wajib Pajak Anda"];
        [_tfNPWPPhoto setPlaceholder:@"Unggah Foto NPWP Anda"];
        [_tfAccount setPlaceholder:@"Pilih Nomor rekening debit Anda"];
        [_tfBranch setPlaceholder:@"Pilih Kantor Cabang"];

        _lblBranch.text = @"Kantor Cabang";
        _lblTitleNPWP.text = @"Nomor Pokok Wajib Pajak (NPWP)";
        _lblTitleAccount.text = @"Pilih Rekening Pembayaran untuk Setoran Awal dan Autodebet Biaya Sewa Tahunan";
        _lblTitleSetoranAwal.text = @"Setoran awal berdasarkan";
        _lblTitleNPWPPhoto.text = @"Foto NPWP";
        _labelConvertion.text = @"";
        
        descriptionNPWP = @"Posisikan NPWP Anda dalam kotak diatas dan pastikan hasil foto terbaca dengan jelas";
        cameraTitleLabel = @"Registrasi e-mas";
        
        [_segmentControl setTitle:@"Nominal (Rp)" forSegmentAtIndex:0];
        [_segmentControl setTitle:@"Berat Emas" forSegmentAtIndex:1];

    }else{
        [_cancelBtn setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_nextBtn setTitle:@"NEXT" forState:UIControlStateNormal];
        [_btnTermsAndConditions setTitle:@"Terms and Condition (must click)" forState:UIControlStateNormal];
        
        [_tfNPWPNumber setPlaceholder:@"Write your NPWP number here"];
        [_tfNPWPPhoto setPlaceholder:@"Upload your NPWP Photo"];
        [_tfAccount setPlaceholder:@"Select Account Debit"];
        [_tfBranch setPlaceholder:@"Select Branch Office"];
        
        _lblBranch.text = @"Office Branch";
        _lblTitleNPWP.text = @"Nomor Pokok Wajib Pajak (NPWP)";
        _lblTitleAccount.text = @"Select Payment account for initial deposit and Autodebit annual rental fee";
        _lblTitleSetoranAwal.text = @"";
        _lblTitleNPWPPhoto.text = @"NPWP Photo";
        _labelConvertion.text = @"";
        
        descriptionNPWP = @"Put your NPWP Card in the box and make sure the result is clear and can be read";
        cameraTitleLabel = @"e-mas Registration";
        
        [_segmentControl setTitle:@"Nominal (Rp)" forSegmentAtIndex:0];
        [_segmentControl setTitle:@"Gold Weight" forSegmentAtIndex:1];
    }
    
    [self.tfSetoranAwal addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_checkBtn addTarget:self action:@selector(actionCheck:) forControlEvents:UIControlEventTouchUpInside];
    [_checkBtn2 addTarget:self action:@selector(actionCheck:) forControlEvents:UIControlEventTouchUpInside];
    [_checkBtn3 addTarget:self action:@selector(actionCheck:) forControlEvents:UIControlEventTouchUpInside];
    [_checkBtn2 setEnabled:NO];
    
    if([lang isEqualToString:@"id"]){
        msgAlert1 = @"Anda belum memilih rekening debit";
        msgAlert2 = @"Harap Check semua poin";
        msgAlert3 = @"Nomor NPWP harus 15 Digit";
        msgAlert4 = @"Anda belum menambahkan nomor NPWP";
        msgAlert5 = @"Anda belum menambahkan Foto NPWP";
        msgAlert6 = @"Nomor NPWP tidak valid";

    }else{
        msgAlert1 = @"you haven't selected a debit account";
        msgAlert2 = @"please check all points";
        msgAlert3 = @"NPWP Number should have 15 digits";
        msgAlert4 = @"You have not fill NPWP Numbers";
        msgAlert5 = @"You have not fill your NPWP Photo";
        msgAlert6 = @"NPWP Number is not valid";
    }
    
    [self.viewContent setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    listAccountIsRequested = NO;
    [self callRequest];
}

- (void) setupTextField{
    [Styles setStyleTextFieldBottomLine:_tfNPWPNumber];
    [Styles setStyleTextFieldBottomLine:_tfAccount];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfNPWPPhoto imageNamed:@"ic_shutter.png"];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfBranch imageNamed:@"baseline_room_black_18pt"];
    
    [self createToolbarButton:_tfNPWPNumber];
    [self createToolbarButton:_tfSetoranAwal];
    [self createToolbarButton:_tfAccount];
    [self createPicker:_tfAccount withTag:0];
    [self createToolbarButton:_tfBranch];

    [_tfNPWPPhoto setDelegate:self];
    [_tfNPWPNumber setDelegate:self];
    [_tfSetoranAwal setDelegate:self];
    [_tfAccount setDelegate:self];
    [_tfBranch setDelegate:self];

    [_tfNPWPNumber setKeyboardType:UIKeyboardTypeNumberPad];
    [_tfSetoranAwal setKeyboardType:UIKeyboardTypeNumberPad];
    
    [_tfSetoranAwal addTarget:self action:@selector(didEndSetoranAwal) forControlEvents:UIControlEventEditingDidEnd];
    [self hideErrorHintSetoranAwal];
}

- (void) gotoSelectBranch{
    GSearchBranchViewController *gsearch = [self.storyboard instantiateViewControllerWithIdentifier:@"GSearchBranch"];
    [gsearch setList:listBranch];
    [gsearch setDelegate:self];
    [self presentViewController:gsearch animated:YES completion:nil];
}

- (void) gotoCamera{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    CamOverlayController *overlayController = [[CamOverlayController alloc] init];

    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    imagePicker.showsCameraControls = false;
    imagePicker.allowsEditing = false;

    GCamOverlayView *cameraView = (GCamOverlayView *)overlayController.view;
    cameraView.frame = imagePicker.view.frame;
    cameraView.descriptionLabel.text = descriptionNPWP;
    cameraView.imageOverlay.image = [UIImage imageNamed:@"img_shape_ktp@2x.png"];
    cameraView.titleLabel.text = cameraTitleLabel;
    
    cameraView.delegate = self;
    
    imagePicker.cameraOverlayView = cameraView;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *base64Imge = [Utility compressAndEncodeToBase64String:image withMaxSizeInKB:40];
    
    [dataManager.dataExtra setValue:base64Imge forKey:@"npwp_photo"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [_tfNPWPPhoto setText:[NSString stringWithFormat:@"NPWP_%@.jpg", [dateFormatter stringFromDate:[NSDate date]]]];
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didTakePhoto:(GCamOverlayView *)overlayView{
    [imagePicker takePicture];
}

- (void)didBack:(GCamOverlayView *)overlayView{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == self.tfNPWPPhoto){
        [self gotoCamera];
    }
    
    if(textField == self.tfBranch){
        [self gotoSelectBranch];
    }
}

- (void)doneEditButtonTapped:(id)done{
    if(_tfNPWPNumber.isFirstResponder && ![_tfNPWPNumber.text isEqualToString:@""]){
        
        if(_tfNPWPNumber.text.length == 15 && ![_tfNPWPNumber.text isEqualToString:@""]){
            if([_tfNPWPNumber.text isEqualToString:@"000000000000000"] || [_tfNPWPNumber.text doubleValue] == 0){
                NSString* msg = @"";
                if([lang isEqualToString:@"id"])
                {
                    msg = @"Nomor NPWP tidak valid";
                }else{
                    msg = @"NPWP Number is not valid";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else if([_tfNPWPNumber.text isEqualToString:@"000000000000000"] || [_tfNPWPNumber.text doubleValue] == 0){
            NSString* msg = @"";
            if([lang isEqualToString:@"id"])
            {
                msg = @"Nomor NPWP tidak valid";
            }else{
                msg = @"NPWP Number is not valid";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            if(_tfNPWPNumber.text.length != 15){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msgAlert3 preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
    [self.view endEditing:YES];
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (void) callRequest{
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    
    NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

- (IBAction)actionCondition:(id)sender {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:urlSyarat];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            [self->_checkBtn2 setEnabled:YES];
             NSLog(@"Opened url");
        }
    }];
}

- (IBAction)actionCancel:(id)sender {
    [self backToR];
}

- (void) showErrorHintSetoranAwal{
    self.lblHintSetoranAwal.hidden = false;
    
    NSString *errorHintWeight = @"";
    NSString *errorHintNominal = @"";
    if([Utility isLanguageID]){
        errorHintWeight = @"Berat harus lebih besar dari";
        errorHintNominal = @"Nominal harus lebih besar dari";
    }else{
        errorHintWeight = @"Weight must be greater than";
        errorHintNominal = @"Nominal must be greater than";
    }
    
    if(self.segmentControl.selectedSegmentIndex == 0){
        double pricePerGram = [gbPrice doubleValue];
        double pricing = [gbLimitMin doubleValue]*pricePerGram;
        self.lblHintSetoranAwal.text = [NSString stringWithFormat:@"%@ Rp. %@",errorHintNominal,[LibGold getCurrencyForm:pricing]];
    }else{
        self.lblHintSetoranAwal.text = [NSString stringWithFormat:@"%@ %@ gram",errorHintWeight, gbLimitMin];
    }

    self.tfSetoranAwal.borderStyle = UITextBorderStyleNone;
    self.tfSetoranAwal.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    
    self.tfSetoranAwal.layer.masksToBounds = false;
    self.tfSetoranAwal.layer.shadowColor = [[UIColor redColor]CGColor];
    self.tfSetoranAwal.layer.shadowOffset = CGSizeMake(0.0, 0.5);
    self.tfSetoranAwal.layer.shadowOpacity = 1.0;
    self.tfSetoranAwal.layer.shadowRadius = 0.0;
    
    [self shakeAnimation];
}

- (void) hideErrorHintSetoranAwal{
    self.lblHintSetoranAwal.hidden = true;
    self.lblHintSetoranAwal.text = @"";
    
    self.tfSetoranAwal.borderStyle = UITextBorderStyleNone;
    self.tfSetoranAwal.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    
    self.tfSetoranAwal.layer.masksToBounds = false;
    self.tfSetoranAwal.layer.shadowColor = [[UIColor grayColor]CGColor];
    self.tfSetoranAwal.layer.shadowOffset = CGSizeMake(0.0, 0.5);
    self.tfSetoranAwal.layer.shadowOpacity = 1.0;
    self.tfSetoranAwal.layer.shadowRadius = 0.0;
}

- (void) didEndSetoranAwal{
    if(self.segmentControl.selectedSegmentIndex == 0){
        
        NSString *convertString = [self.tfSetoranAwal.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        double pricePerGram = [gbPrice doubleValue];
        double input = [convertString doubleValue];
        double total = input / pricePerGram;
        
        if(total < [self->gbLimitMin doubleValue]){
            [self showErrorHintSetoranAwal];
            isSetoranValid = false;
        }else{
            [self hideErrorHintSetoranAwal];
            isSetoranValid = true;
        }
        
        double newValue = [[self.tfSetoranAwal.text stringByReplacingOccurrencesOfString:@"," withString:@""]doubleValue];
        NSString *totalPrice = [self.tfSetoranAwal.text stringByReplacingOccurrencesOfString:@"," withString:@""];

        [dataManager.dataExtra setValue:totalPrice forKey:@"amount"];
        [dataManager.dataExtra setValue:[LibGold goldToSent:newValue withPrice:[gbPrice doubleValue]] forKey:@"weight"];
        [dataManager.dataExtra setValue:@"RP" forKey:@"input_type"];

    }else{
        double newValue = [[self.tfSetoranAwal.text stringByReplacingOccurrencesOfString:@"," withString:@"."]doubleValue];
        double pricePerGram = [gbPrice doubleValue];
        
        if(newValue < [gbLimitMin doubleValue]){
            [self showErrorHintSetoranAwal];
            isSetoranValid = false;
        }else{
            [self hideErrorHintSetoranAwal];
            isSetoranValid = true;
        }
        
        double totalPrice = newValue * pricePerGram;
        
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%.f",totalPrice] forKey:@"amount"];
        [dataManager.dataExtra setValue:[LibGold goldToSentFromGram:newValue] forKey:@"weight"];
        [dataManager.dataExtra setValue:@"GR" forKey:@"input_type"];
    }
}

- (IBAction)actionNext:(id)sender {
    
//    weight, amount, buy_price, sell_price, input_type, branch_name
    BOOL validatingBranch = TRUE;
    
    if([_tfBranch.text isEqualToString:@""]){
        validatingBranch = FALSE;
    }
    
    [dataManager.dataExtra setValue:_tfAccount.text forKey:@"id_account"];
    NSString* noHp;
    if([NSUserdefaultsAes getValueForKey:@"msisdn"]){
        noHp = [NSUserdefaultsAes getValueForKey:@"msisdn"];
    }
    
    [dataManager.dataExtra setValue:noHp forKey:@"msisdn"];
    [dataManager.dataExtra setValue:_tfNPWPNumber.text forKey:@"npwp"];
    if([_tfNPWPNumber.text isEqualToString:@""]){
        [dataManager.dataExtra setValue:@"null" forKey:@"npwp"];
    }
    if([_tfNPWPPhoto.text isEqualToString:@""]){
        [dataManager.dataExtra setValue:@"null" forKey:@"npwp_photo"];
    }
    [dataManager.dataExtra addEntriesFromDictionary:@{@"init_deposit":self.tfSetoranAwal.text}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"init_gram":self.tfSetoranAwal.text}];
    
    if([self validate] && isSetoranValid && validatingBranch){
        [super openNextTemplate];
    }
    
    if (!isSetoranValid){
        [self shakeAnimation];
    }
    
    if(!validatingBranch){
        NSString *msg = @"";
         if([lang isEqualToString:@"id"]){
             msg = @"Anda belum memilih cabang";
         }else{
             msg = @"You haven't selected branch";
         }
         
         UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void) actionCheck:(id)sender{
    
    if([sender isKindOfClass:UIButton.class]){
        UIButton *btn = (UIButton*)sender;
        if(btn == _checkBtn){
            if(checkState1 == 0){
                checkState1 = 1;
                [btn setImage:[UIImage imageNamed:@"ic_checkbox_active.png"] forState:UIControlStateNormal];
            }else{
                checkState1 = 0;
                [btn setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"] forState:UIControlStateNormal];
            }
        }
        if(btn == _checkBtn2){
            if(checkState2 == 0){
                checkState2 = 1;
                [btn setImage:[UIImage imageNamed:@"ic_checkbox_active.png"] forState:UIControlStateNormal];
            }else{
                checkState2 = 0;
                [btn setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"] forState:UIControlStateNormal];
            }
        }
        if(btn == _checkBtn3){
            if(checkState3 == 0){
                checkState3 = 1;
                [btn setImage:[UIImage imageNamed:@"ic_checkbox_active.png"] forState:UIControlStateNormal];
            }else{
                checkState3 = 0;
                [btn setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"] forState:UIControlStateNormal];
            }
        }
    }
}

- (BOOL) validate{
    int state = checkState1 + checkState2 + checkState3;
    
    if(state == 3 && ![_tfAccount.text isEqualToString:@""]){
        if([_tfNPWPNumber.text isEqualToString:@""] && [_tfNPWPPhoto.text isEqualToString:@""]){
            return YES;
        }
        else if((_tfNPWPNumber.text.length == 15) && ![_tfNPWPPhoto.text isEqualToString:@""]){
            if([_tfNPWPNumber.text isEqualToString:@"000000000000000"]){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msgAlert6 preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
                return NO;
            }
            return YES;
        }else{
            if(![_tfNPWPNumber.text isEqualToString:@""]){
                if(_tfNPWPNumber.text.length != 15){
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msgAlert3 preferredStyle:UIAlertControllerStyleAlert];
                        
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            if([_tfNPWPNumber.text isEqualToString:@""] && ![_tfNPWPPhoto.text isEqualToString:@""]){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msgAlert4 preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
            if([_tfNPWPPhoto.text isEqualToString:@""] && ![_tfNPWPNumber.text isEqualToString:@""]){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msgAlert5 preferredStyle:UIAlertControllerStyleAlert];
                    
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }else{
        if(state != 3){
            NSString *msg = @"";
            if([lang isEqualToString:@"id"]){
                msg = @"Harap tandai semua poin";
            }else{
                msg = @"Please fill all checkmark ";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
        if([_tfAccount.text isEqualToString:@""]){
            NSString *msg = @"";
            if([lang isEqualToString:@"id"]){
                msg = @"Anda belum memilih rekening debit";
            }else{
                msg = @"You haven't selected debit account";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
               
           [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
           [self presentViewController:alert animated:YES completion:nil];
        }

        return NO;
    }
    return NO;
}

- (void) updateLabel{
    _label1.text = [labelArray[0] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    _label2.text = labelArray[3];
    _label5.text = labelArray[5];
    _label3.text = [labelArray[4] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        
    if([lang isEqualToString:@"id"]){
        _label4.text = @"Saya telah membaca, memahami, dan dengan ini setuju atas syarat Syarat Umum Pembukaan Rekening (SUPR) dan Syarat & Ketentuan Khusus e-mas.";
    }else{
        _label4.text = @"I have read, understood, and hereby agree to the terms of the General Terms of Account Opening (SUPR) and the Special Terms & Conditions for e-mas";
    }

    
    urlSyarat = labelArray[1];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return paymentAccPickerData.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return paymentAccPickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(row == 0){
        self.tfAccount.text = @"";
    }else{
        self.tfAccount.text = [paymentAccPickerData objectAtIndex:row];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.tfSetoranAwal){
        if(self.segmentControl.selectedSegmentIndex == 0){
            NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
            
            double newValue = [newText doubleValue];
            if ([newText length] == 0 || newValue == 0){
                textField.text = nil;
            }else{
                textField.text = [LibGold getCurrencyForm:newValue];
                if([lang isEqualToString:@"id"]){
                    self.labelConvertion.text = [NSString stringWithFormat:@"Konversi Berat Emas : %@ Gram",[LibGold calculateToGold:newValue withPrice:[gbPrice doubleValue]]];
                }else{
                    self.labelConvertion.text = [NSString stringWithFormat:@"Gold Weight Convertion : %@ Gram",[LibGold calculateToGold:newValue withPrice:[gbPrice doubleValue]]];
                }

                return NO;
            }
        }
        else{
            NSString *text = textField.text;
            
            if([string isEqualToString:@","] && [text containsString:@","]){
                return NO;
            }
            
            NSArray  *arrayOfString = [text componentsSeparatedByString:@","];
            if([arrayOfString count] == 2){
                if([arrayOfString[1] length] > 3 && ![string isEqualToString:@""]){
                    return NO;
                }
            }
            
            double value = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
            if([lang isEqualToString:@"id"]){
                self.labelConvertion.text = [NSString stringWithFormat:@"Konversi dalam rupiah : Rp. %@",[LibGold calculateToMoney:value withPrice:[gbPrice doubleValue]]];
            }else{
                self.labelConvertion.text = [NSString stringWithFormat:@"Convertion in rupiah : Rp. %@",[LibGold calculateToMoney:value withPrice:[gbPrice doubleValue]]];
            }
            
        }

    }
    
    return YES;
}

- (void) textFieldDidChange : (UITextField *)textField{

    if(self.segmentControl.selectedSegmentIndex == 0){
        double newValue = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue];
        if([lang isEqualToString:@"id"]){
            self.labelConvertion.text = [NSString stringWithFormat:@"Konversi Berat Emas : %@ Gram",[LibGold calculateToGold:newValue withPrice:[gbPrice doubleValue]]];
        }else{
            self.labelConvertion.text = [NSString stringWithFormat:@"Gold Weight Convertion : %@ Gram",[LibGold calculateToGold:newValue withPrice:[gbPrice doubleValue]]];
        }

    }else{
        double value = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
        if([lang isEqualToString:@"id"]){
            self.labelConvertion.text = [NSString stringWithFormat:@"Konversi dalam rupiah : Rp. %@",[LibGold calculateToMoney:value withPrice:[gbPrice doubleValue]]];
        }else{
            self.labelConvertion.text = [NSString stringWithFormat:@"Convertion in rupiah : Rp. %@",[LibGold calculateToMoney:value withPrice:[gbPrice doubleValue]]];
        }
    }

}

- (IBAction)actionSegmentControl:(UISegmentedControl*)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;

    if (selectedSegment == 0) {
        [self.view endEditing:YES];
        self.tfSetoranAwal.keyboardType = UIKeyboardTypeNumberPad;
        
        if([lang isEqualToString:@"id"]){
            self.tfSetoranAwal.placeholder = @"Masukkan Jumlah Dana";
        }else{
            self.tfSetoranAwal.placeholder = @"Enter Amount of Funds";
        }

        double newValue = [self.tfSetoranAwal.text doubleValue];
        NSString* convertion  = [LibGold getGramFormOfValue:newValue/[gbPrice doubleValue]];
        if([lang isEqualToString:@"id"]){
            self.labelConvertion.text = [NSString stringWithFormat:@"Konversi Berat Emas : %@ Gram",convertion];
        }else{
            self.labelConvertion.text = [NSString stringWithFormat:@"Gold Weight Convertion : %@ Gram",convertion];
        }

    }else{
        [self.view endEditing:YES];
        if([lang isEqualToString:@"id"]){
            self.tfSetoranAwal.placeholder = @"Masukkan Berat Emas";
        }else{
            self.tfSetoranAwal.placeholder = @"Enter Gold Weight";
        }
        self.tfSetoranAwal.keyboardType = UIKeyboardTypeDecimalPad;
        double pricePerGram = [gbPrice doubleValue];
        double newValue = [self.tfSetoranAwal.text doubleValue];
        double total = newValue * pricePerGram;

        NSString* convertion = [NSString stringWithFormat:@"Rp. %@,00", [LibGold getCurrencyForm:total]];
        if([lang isEqualToString:@"id"]){
            self.labelConvertion.text = [NSString stringWithFormat:@"Konversi dalam rupiah : %@",convertion];
        }else{
            self.labelConvertion.text = [NSString stringWithFormat:@"Convertion in rupiah : %@",convertion];
        }
    }
    
    [self didEndSetoranAwal];
}

- (void) getListAccount{
    NSArray *listAcct = nil;
    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
    
    if(listAcct.count != 0 || listAcct != nil){
        NSMutableArray *newData = [[NSMutableArray alloc]init];
        if ([lang isEqualToString:@"id"]) {
            [newData addObject:@"Pilih Nomor Rekening"];
        }else{
            [newData addObject:@"Select Account Number"];
        }

        
        for(NSDictionary *temp in listAcct){
            [newData addObject:[temp valueForKey:@"id_account"]];
        }
        paymentAccPickerData = newData;
    }else{
        if(listAccountIsRequested){
            NSString *msgAlert = @"";
            if([lang isEqualToString:@"id"]){
                msgAlert = @"Anda Tidak Memiliki Rekening yang dibutuhkan";
            }else{
                msgAlert = @"You dont have account needed";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"INFO" message:msgAlert preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            listAccountIsRequested = YES;
            
            NSString *strURL = [NSString stringWithFormat:@"request_type=list_account1, customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strURL needLoading:true encrypted:true banking:true favorite:false];
        }

    }
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account1"]){
        if([[jsonObject objectForKey:@"rc"]isEqualToString:@"00"]){
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            [self getListAccount];

        }else{
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
            
        }
    }
    
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if(![requestType isEqualToString:@"check_notif"]){
            if([requestType isEqualToString:@"gold_registration"]){
                if([[jsonObject objectForKey:@"response_code"] isEqual:@"00"]){
                    NSString *response = [jsonObject objectForKey:@"response"];
                    NSError *error;
                    NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                            options:NSJSONReadingAllowFragments
                                                                              error:&error];
                    
                    NSMutableArray *arr = [[NSMutableArray alloc]init];
                    [arr addObject:[dataDict valueForKey:@"branch_cif"]];
                    [self selectBranchDefault:[dataDict valueForKey:@"branch_cif"]];
                    [arr addObjectsFromArray:[dataDict objectForKey:@"branch_nearby"]];
                    NSArray *branchLoc = [dataDict objectForKey:@"branch_all"];

                    NSMutableArray* allKCPList = [[NSMutableArray alloc] init];
                    for(int i = 0; i < branchLoc.count; i++)
                    {
                        NSString* nameProv = [branchLoc[i] objectForKey:@"name"];
                        NSArray* regencyCity = [branchLoc[i] objectForKey:@"regency_city"];
                        
                        for(int j = 0; j < regencyCity.count; j++){
                            
                            NSString* nameRegency = [regencyCity[j] objectForKey:@"name"];
                            NSArray* branch = [regencyCity[j] objectForKey:@"branch"];
                            for (int k = 0; k < branch.count; k++){
                                NSMutableDictionary *dictMut = [[NSMutableDictionary alloc] init];
                                
                                [dictMut setObject:[branch[k] objectForKey:@"branch_name"] forKey:@"branch_name"];
                                [dictMut setObject:[branch[k] objectForKey:@"branch_code"] forKey:@"branch_code"];
                                [dictMut setObject:[NSString stringWithFormat:@"%@, %@", nameRegency, nameProv] forKey:@"branch_address"];

                                [allKCPList addObject:dictMut];
                            }
                        }
                    }
                    [arr addObjectsFromArray:allKCPList];
                    listBranch = arr;
                    
                    labelArray = [[dataDict valueForKey:@"info"] componentsSeparatedByString:@"|"];
                    [self updateLabel];
                    [self getListAccount];
                    [self.viewContent setHidden:false];
                }else{
                    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                    [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self backToR];
                    }]];
                    [self presentViewController:alertCont animated:YES completion:nil];
                }
            }
        }
    }
}

- (void)selectedKC:(NSDictionary *)dictKC{
    self.tfBranch.text = [dictKC valueForKey:@"branch_name"];
    [dataManager.dataExtra setValue:[dictKC objectForKey:@"branch_code"] forKey:@"branch_code"];
    [self.view endEditing:YES];
}

- (void)selectBranchDefault:(NSDictionary*)dictKC{
    self.tfBranch.text = [dictKC valueForKey:@"branch_name"];
    [dataManager.dataExtra setValue:[dictKC objectForKey:@"branch_code"] forKey:@"branch_code"];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void) shakeAnimation{
    CABasicAnimation *animation =
                             [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                   CGPointMake([self.lblHintSetoranAwal center].x - 5.0f, [self.lblHintSetoranAwal center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                   CGPointMake([self.lblHintSetoranAwal center].x + 5.0f, [self.lblHintSetoranAwal center].y)]];
    [[self.lblHintSetoranAwal layer] addAnimation:animation forKey:@"position"];
}
@end
