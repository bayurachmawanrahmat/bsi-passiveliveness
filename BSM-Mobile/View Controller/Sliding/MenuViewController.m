//
//  MenuViewController.m
//  ECSlidingViewController
//
//  Created by Michael Enriquez on 1/23/12.
//  Copyright (c) 2012 EdgeCase. All rights reserved.
//

#import "MenuViewController.h"
#import "Constant.h"
#import "TemplateViewController.h"
#import "LV01ViewController.h"
#import "FinancialViewController.h"
#import "RootViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Utility.h"
#import "TabViewController.h"
#import "ScrollHomeViewController.h"
#import "AboutViewController.h"
#import "ShariapointViewController.h"
#import "ToastView.h"
#import "PopUpLoginViewController.h"

@interface MenuViewController() {
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@property (nonatomic, strong) NSMutableArray *menuItems;
@end

UIImageView *imgIcon;
UILabel *lblVersion;
UILabel *version;
UILabel *lblTerm;
UILabel *term;
UILabel *copyRight;
UIView *viewBg;
UIView *viewContiner;

@implementation MenuViewController
@synthesize menuItems;

- (void)awakeFromNib
{
    [self reloadData];
}

- (void)reloadData{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];

    if([lang isEqualToString:@"id"]){
        valAgree = @"Lanjut";
        valCancel = @"Batal";
        valTitle = @"Konfirmasi";
        valMsg = @"Anda akan Logout dari Aplikasi";
        
        self.menuItems = [NSMutableArray arrayWithObjects:
                          @{@"title":@"Beranda",@"image":@"ic_sdmenu_beranda_v2",@"id":@"HomeVC"}, nil];
        [self.menuItems addObject:@{@"title":@"Info Rekening",@"image":@"ic_sdmenu_inforekening_v2",@"id":@"InfoRek"}];
        [self.menuItems addObject:@{@"title":@"Transfer",@"image":@"ic_sdmenu_transfer_v2",@"id":@"transfer"}];
        [self.menuItems addObject:@{@"title":@"Pembayaran",@"image":@"ic_sdmenu_pembayaran_v2",@"id":@"pembayaran"}];
        [self.menuItems addObject:@{@"title":@"Pembelian",@"image":@"ic_sdmenu_pembelian_v2",@"id":@"pembelian"}];
        [self.menuItems addObject:@{@"title":@"QRIS",@"image":@"ic_sdmenu_qris_v2",@"id":@"qr"}];
        [self.menuItems addObject:@{@"title":@"Buka Rekening",@"image":@"ic_sdmenu_bukarekening_v2",@"id":@"openingAcount"}];
        
        [self.menuItems addObject:@{@"title":@"Kotak Masuk",@"image":@"ic_sdmenu_kotakmasuk_v2",@"id":@"inbox"}];
        [self.menuItems addObject:@{@"title":@"Manajemen Kartu",@"image":@"ic_sdmenu_manajemenkartu_v2",@"id":@"kartuManajemen"}];
        [self.menuItems addObject:@{@"title":@"Info Kurs dan Emas",@"image":@"ic_sdmenu_infokursdanemas_v2",@"id":@"kurs"}];
        [self.menuItems addObject:@{@"title":@"Informasi Limit",@"image":@"ic_sdmenu_infolimit_v2",@"id":@"limit"}];
//        [self.menuItems addObject:@{@"title":@"Media Sosial",@"image":@"ic_sdmenu_medsos_v2",@"id":@"sosmed"}];
//        [self.menuItems addObject:@{@"title":@"BSI Point",@"image":@"logo_bsi_point",@"id":@"bsipoint"}];
        [self.menuItems addObject:@{@"title":@"",@"image":@"",@"id":@"-"}];
        [self.menuItems addObject:@{@"title":@"Aktivasi",@"image":@"ic_sdmenul2_aktivasi_v2",@"id":@"ActivationVC"}];
        [self.menuItems addObject:@{@"title":@"Minta Kode Aktivasi",@"image":@"ic_sdmenul2_mintakodeatvulang_v2",@"id":@"ActivationRetrival"}];
        [self.menuItems addObject:@{@"title":@"Pengaturan Kata Sandi",@"image":@"ic_sdmenul2_pengaturanksandi_v2", @"id":@"ChangePwdVC"}];
        [self.menuItems addObject:@{@"title":@"Ubah PIN",@"image":@"ic_sdmenul2_ubahpin_v2", @"id":@"ChangePINVC"}];
        [self.menuItems addObject:@{@"title":@"Ubah Bahasa",@"image":@"ic_sdmenul2_ubahbahasa_v2", @"id":@"ChangeLngVC"}];
        [self.menuItems addObject:@{@"title":@"Email",@"image":@"ic_sdmenul2_email_v2", @"id":@"ChangeEmailVC"}];
        [self.menuItems addObject:@{@"title":@"Pengaturan MenuKu",@"image":@"ic_sdmenul2_mymenu", @"id":@"MyMenuSettingVC"}];
        [self.menuItems addObject:@{@"title":@"Tentang Aplikasi",@"image":@"ic_sdmenul2_tentangapp_v2", @"id":@"openAbout"}];
        if ([hasLogin isEqualToString:@"YES"]) {
            [self.menuItems addObject:@{@"title":@"Logout",@"image":@"ic_sdmenul2_logout_v2", @"id":@"exit"}];
        }
    }else{
        valAgree = @"Next";
        valCancel = @"Cancel";
        valTitle = @"Confirmation";
        valMsg = @"You will Logout from Application";
        
        self.menuItems = [NSMutableArray arrayWithObjects:
                          @{@"title":@"Home",@"image":@"ic_sdmenu_beranda_v2",@"id":@"HomeVC"}, nil];
        [self.menuItems addObject:@{@"title":@"Account Info",@"image":@"ic_sdmenu_inforekening_v2",@"id":@"InfoRek"}];
        [self.menuItems addObject:@{@"title":@"Transfer",@"image":@"ic_sdmenu_transfer_v2",@"id":@"transfer"}];
        [self.menuItems addObject:@{@"title":@"Payment",@"image":@"ic_sdmenu_pembayaran_v2",@"id":@"pembayaran"}];
        [self.menuItems addObject:@{@"title":@"Purchase",@"image":@"ic_sdmenu_pembelian_v2",@"id":@"pembelian"}];
        [self.menuItems addObject:@{@"title":@"QRIS",@"image":@"ic_sdmenu_qris_v2",@"id":@"qr"}];
        [self.menuItems addObject:@{@"title":@"Open Account",@"image":@"ic_sdmenu_bukarekening_v2",@"id":@"openingAcount"}];
        [self.menuItems addObject:@{@"title":@"Inbox",@"image":@"ic_sdmenu_kotakmasuk_v2",@"id":@"inbox"}];
        [self.menuItems addObject:@{@"title":@"Card Management",@"image":@"ic_sdmenu_manajemenkartu_v2",@"id":@"kartuManajemen"}];
        [self.menuItems addObject:@{@"title":@"Exchange Rate and Gold",@"image":@"ic_sdmenu_infokursdanemas_v2",@"id":@"kurs"}];
        [self.menuItems addObject:@{@"title":@"Limit Information",@"image":@"ic_sdmenu_infolimit_v2",@"id":@"limit"}];
//        [self.menuItems addObject:@{@"title":@"Social Media",@"image":@"ic_sdmenu_medsos_v2",@"id":@"sosmed"}];
//        [self.menuItems addObject:@{@"title":@"BSI Point",@"image":@"logo_bsi_point",@"id":@"bsipoint"}];
        [self.menuItems addObject:@{@"title":@"",@"image":@"",@"id":@"-"}];
        [self.menuItems addObject:@{@"title":@"Activation",@"image":@"ic_sdmenul2_aktivasi_v2",@"id":@"ActivationVC"}];
        [self.menuItems addObject:@{@"title":@"Request Activation Code",@"image":@"ic_sdmenul2_mintakodeatvulang_v2",@"id":@"ActivationRetrival"}];
        [self.menuItems addObject:@{@"title":@"Setting Password",@"image":@"ic_sdmenul2_pengaturanksandi_v2", @"id":@"ChangePwdVC"}];
        [self.menuItems addObject:@{@"title":@"Change PIN",@"image":@"ic_sdmenul2_ubahpin_v2", @"id":@"ChangePINVC"}];
        [self.menuItems addObject:@{@"title":@"Change Language",@"image":@"ic_sdmenul2_ubahbahasa_v2", @"id":@"ChangeLngVC"}];
        [self.menuItems addObject:@{@"title":@"Email",@"image":@"ic_sdmenul2_email_v2", @"id":@"ChangeEmailVC"}];
        [self.menuItems addObject:@{@"title":@"MyMenu Setting",@"image":@"ic_sdmenul2_mymenu", @"id":@"MyMenuSettingVC"}];
        [self.menuItems addObject:@{@"title":@"About",@"image":@"ic_sdmenul2_tentangapp_v2", @"id":@"openAbout"}];
        if ([hasLogin isEqualToString:@"YES"]) {
            [self.menuItems addObject:@{@"title":@"Logout",@"image":@"ic_sdmenul2_logout_v2", @"id":@"exit"}];
        }
    }
    
    [self.tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
    self.homeNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
    self.slidingViewController.anchorLeftPeekAmount = 250.0;
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setSeparatorColor:UIColorFromRGB(0xf5f5f5)];
}

- (void)backRoot {
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.slidingViewController resetTopViewAnimated:NO];
}

- (void)actionOke {
    [viewBg removeFromSuperview];
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (alertView.tag == 12345) {
//        if (buttonIndex == 0) {
//            //CANCEL Button
//        } else {
//            //OK Button
//            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//            [userDefault setObject:@"NO" forKey:@"hasLogin"];
//            [userDefault synchronize];
//
//            MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
//            self.slidingViewController.topViewController = menuVC.homeNavigationController;
////            [ToastView showToastInParentView:menuVC.homeNavigationController.view withText:lang(@"LOGOUT") withDuaration:1];
//
//        }
//    }
//}

//-(void)dismissAlert:(UIAlertView*)alert
//{
//    [alert dismissWithClickedButtonIndex:0 animated:YES];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuItemCell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:2];
    UIView *separator = [(UIView*) cell viewWithTag:3];
    
    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    label.textColor = [UIColor colorWithRed:0.38 green:0.38 blue:0.38 alpha:1];
    label.textColor = [UIColor blackColor];
//    [label setFont:[UIFont systemFontOfSize:14]];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    [label setText:[[self.menuItems objectAtIndex:indexPath.row]valueForKey:@"title"]];
    NSString *image =[[self.menuItems objectAtIndex:indexPath.row]valueForKey:@"image"];
    if([image isEqualToString:@""]){
        [imageView setHidden:true];
    }else{
        [imageView setHidden:false];
        [imageView setImage:[UIImage imageNamed:image]];
    }
    
    [separator setBackgroundColor:UIColorFromRGB(0xF5F5F5)];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [[self.menuItems objectAtIndex:indexPath.row]valueForKey:@"id"];
    if([identifier isEqualToString:@"ActivationVC"]){
//        if([[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
        if([NSUserdefaultsAes getValueForKey:@"customer_id"] != nil){
            identifier = @"HomeVC";
            
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_DONE") preferredStyle:UIAlertControllerStyleAlert];
            if (@available(iOS 13.0, *)) {
                [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alerts animated:YES completion:nil];
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    TabViewController *tabVC = (TabViewController *)self.homeNavigationController;
    [tabVC setSelectedIndex:0];
    UINavigationController *navVC = (UINavigationController *)tabVC.selectedViewController;
    @try {
        [navVC popToRootViewControllerAnimated:NO];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
    ScrollHomeViewController *homeVC = (ScrollHomeViewController *)navVC.topViewController;
    
    if ([identifier isEqualToString:@"openAbout"]){
        
        AboutViewController *about = [self.storyboard instantiateViewControllerWithIdentifier:@"ABOUTVC"];
        [self presentViewController:about animated:YES completion:nil];

        
    } else if ([identifier isEqualToString:@"exit"]){
//        UIAlertView *alert = [[UIAlertView alloc]
//                              initWithTitle:valTitle
//                              message:valMsg
//                              delegate:nil
//                              cancelButtonTitle:valCancel
//                              otherButtonTitles:valAgree, nil];
//        alert.delegate = self;
//        alert.tag = 12345;
//        [alert show];
        
        UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"CONFIRM") message:lang(@"LOGOUT_CONFIRMATION") preferredStyle:UIAlertControllerStyleAlert];
        if (@available(iOS 13.0, *)) {
            [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [alerts addAction:[UIAlertAction actionWithTitle:lang(@"CANCEL") style:UIAlertActionStyleDefault handler:nil]];
        [alerts addAction:[UIAlertAction actionWithTitle:lang(@"CONTINUE") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [homeVC stopIdleTimer];
            [homeVC logoutSession];
            MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
            self.slidingViewController.topViewController = menuVC.homeNavigationController;
            [homeVC gotoPopLogin:@"exit"];
//            [ToastView showToastInParentView:menuVC.homeNavigationController.view withText:lang(@"LOGOUT") withDuaration:1];
        }]];
        [self presentViewController:alerts animated:YES completion:nil];
        
//        [alert show];
//        [self performSelector:@selector(dismissAlert:) withObject:alert afterDelay:2.0];

    /*} else if ([identifier isEqualToString:@"createPwd"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [self openTemplate:112];*/
    } else if ([identifier isEqualToString:@"ChangePwdVC"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [homeVC openStatic:@"ASVC"];
    } else if ([identifier isEqualToString:@"InfoRek"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"ic_info_account_header.png" forKey:@"imgIcon"];
        [homeVC openTemplateWithData:0];
    } else if ([identifier isEqualToString:@"transfer"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"ic_transfer_header.png" forKey:@"imgIcon"];
        [homeVC openTemplateWithData:3];
    } else if ([identifier isEqualToString:@"pembayaran"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"ic_payment_header.png" forKey:@"imgIcon"];
        [homeVC openTemplateWithData:1];
    } else if ([identifier isEqualToString:@"pembelian"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"ic_purchase_header.png" forKey:@"imgIcon"];
        [homeVC openTemplateWithData:2];
    } else if ([identifier isEqualToString:@"qr"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"ic_qrpay_header.png" forKey:@"imgIcon"];
        [homeVC goToQR];
    } else if ([identifier isEqualToString:@"openingAcount"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"ic_open_account_header.png" forKey:@"imgIcon"];
        [homeVC openTemplateWithData:4];
    } else if ([identifier isEqualToString:@"inbox"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [homeVC goToInbox];
    }else if ([identifier isEqualToString:@"kartuManajemen"]){ //adding kartu manajemen
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"icon_blokir_kartu" forKey:@"imgIcon"];
        [homeVC openTemplateWithData:7];
    }else if ([identifier isEqualToString:@"kurs"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"icon_kurs_emas.png" forKey:@"imgIcon"];
        [homeVC openStatic:@"KursGoldVC"];
    }else if ([identifier isEqualToString:@"limit"]){
            [self.slidingViewController resetTopViewAnimated:YES];
            [userDefault setObject:@"ic_info_limitmaster.png" forKey:@"imgIcon"];
            [homeVC openStatic:@"IT01"];
    }else if ([identifier isEqualToString:@"bsipoint"]){
            [self.slidingViewController resetTopViewAnimated:YES];
            [homeVC openStatic:@"Shariapoint" withTemplateName:@"Shariapoint"];
    }
    else if ([identifier isEqualToString:@"sosmed"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"SM.png" forKey:@"imgIcon"];
        [homeVC openStatic:@"SMVC"];
    } else if ([identifier isEqualToString:@"-"]){
    } else if ([identifier isEqualToString:@"HomeVC"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        
    } else if ([identifier isEqualToString:@"ChangePINVC"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [homeVC openStatic:@"ChangePINVC"];
    } else if ([identifier isEqualToString:@"ActivationVC"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [userDefault setObject:@"YES" forKey:@"fromSliding"];
        [userDefault setValue:@"@" forKey:@"openRoot"];
        [userDefault synchronize];

        [homeVC openStatic:@"ActivationVC"];
    } else if ([identifier isEqualToString:@"ChangeLngVC"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [homeVC openStatic:@"ChangeLngVC"];
    } else if ([identifier isEqualToString:@"ChangeEmailVC"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [homeVC openStatic:@"ChangeEmailVC"];
    } else if ([identifier isEqualToString:@"ActivationRetrival"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [homeVC openStatic:@"ActivationRetrival"];
    } else if ([identifier isEqualToString:@"MyMenuSettingVC"]){
        [self.slidingViewController resetTopViewAnimated:YES];
        [homeVC openStatic:@"MyMenuSettingVC"];
    } else {
        self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
        [self.slidingViewController resetTopViewAnimated:YES];
    }
}

- (void)openTemplate:(int)position{    
    NSDictionary* userInfo = @{@"position": @(position)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // Add your Colour.
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *label = [(UILabel*) cell viewWithTag:1];
    label.textColor = UIColorFromRGB(const_color_primary);
    [self setCellColor:UIColorFromRGB(const_highlight_primary) ForCell:cell];
}


- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // Reset Colour.
    UITableViewCell *cell =[tableView cellForRowAtIndexPath:indexPath];
    UILabel *label = (UILabel*)[cell viewWithTag:1];
    label.textColor = [UIColor blackColor];
    [self setCellColor:[UIColor clearColor] ForCell:cell]; //normal color
}

- (void)setCellColor:(UIColor *)color ForCell:(UITableViewCell *)cell {
    cell.contentView.backgroundColor = color;
    cell.backgroundColor = color;
}

@end
