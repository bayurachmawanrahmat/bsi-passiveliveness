//
//  MenuViewController.h
//  ECSlidingViewController
//
//  Created by Michael Enriquez on 1/23/12.
//  Copyright (c) 2012 EdgeCase. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UITableViewDataSource, UITabBarControllerDelegate>
- (void)reloadData;
- (void)backRoot;
@property (nonatomic, strong) UINavigationController *homeNavigationController;
@end
