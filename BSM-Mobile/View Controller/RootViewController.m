//
//  RootViewController.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "RootViewController.h"
#import "Utility.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TemplateViewController.h"
#import "GENCF02ViewController.h"
#import "LV01ViewController.h"
#import "PL01ViewController.h"
#import "DataManager.h"
#import "InboxHelper.h"
#import "DTNTF01ViewController.h"
#import "Connection.h"
#import "PopupOnboardingViewController.h"
#import "PopupActivationViewController.h"
#import "PopupBurroqViewController.h"
#import "PopUpLoginViewController.h"
#import "SearchItemViewController.h"
#import "PBYCF00ViewController.h"
#import "GDashViewController.h"
#import "ListSurahViewController.h"
#import "JuzAmmaViewController.h"
#import "AsmaulHusnaViewController.h"
#import "TausiahViewController.h"
#import "FavoriteDetailViewController.h"
#import "ComplainViewController.h"
#import "WebviewViewController.h"
#import "CircleTransition.h"
#import "UIAlertController+AlertExtension.h"
//#import "AlertServiceViewController.h"
#import "AppProperties.h"
#import "NSUserdefaultsAes.h"
#import "RedirectCFViewController.h"


@interface RootViewController ()<UITabBarControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, ConnectionDelegate, SearchItemDelegate, UITextFieldDelegate, PopupLoginDelegate>{
    UIButton *buttonSearchFeature;
    UIButton *buttonInbox;
    UIButton *buttonMyQR;
    UIButton *buttonLogout;
    UIButton *buttonSearch;
    UITextField *uitf;
    
    UIImageView *imgLogo;
    UIImageView *imgConnection;
    BOOL showMenu;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
    NSTimer *idleTimer;
    int maxIdle;
    //idle countup reach network
    NSTimer *reachTimer;
    InboxHelper *inboxHelper;
    NSString *actionSwipe;
	NSString *dataMessageBurroq;
    NSString *findId;
    NSString *listAcctFrom;
    NSUserDefaults *userDefault;
    
    CircleTransition *transitionCircSearch;
    UIView *viewBgAbout;
    CGFloat iconYPost;
}

@end


@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) addingBroadCastlistenerNavigate{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"listenerNavigate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
       selector:@selector(listenerNavigate:)
           name:@"listenerNavigate"
         object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.delegate = self;
    if (@available(iOS 13.0, *)) {
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
      
    inboxHelper = [[InboxHelper alloc]init];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        valAgree = @"Lanjut";
        valCancel = @"Batal";
        valTitle = @"Konfirmasi";
        valMsg = @"Apakah anda yakin untuk keluar dari aplikasi ?";
    } else {
        valAgree = @"Next";
        valCancel = @"Cancel";
        valTitle = @"Confirmation";
        valMsg = @"Do you want to quit the application ?";
    }

    showMenu = false;
    
    iconYPost = 30;
    CGFloat heightTopView = 84.0;
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [self.view insertSubview:backgroundView atIndex:0];
    
    
    
    UIView *topView = [[UIView alloc] init];
    [topView setFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width,heightTopView)];
    topView.backgroundColor = UIColor.whiteColor;
    [self.view insertSubview:topView aboveSubview:backgroundView];

    imgLogo = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_tb_logo_v2"]];
    [imgLogo setTintColor:[UIColor whiteColor]];
    [imgLogo setContentMode:UIViewContentModeScaleAspectFit];
    
    imgConnection = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ic_tb_sgn_disconnect"]];
    if ([userDefault valueForKey:@"state_indicator"] != nil) {
        if ([[userDefault valueForKey:@"state_indicator"] isEqualToString:@"0"]) {
            [imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_disconnect"]];
        }else if ([[userDefault valueForKey:@"state_indicator"] isEqualToString:@"1"]){
            
            if([[userDefault objectForKey:@"mustLogin"]isEqualToString:@"YES"]){
                if([[userDefault objectForKey:@"hasLogin"]isEqualToString:@"YES"]){
                    [imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_connect"]];
                }else{
                    [imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_unlogged"]];
                }
            }else{
                [imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_connect"]];
            }
            MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
            [menuVC awakeFromNib];
        }else{
            [imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_connecting"]];
        }
    }else{
        [userDefault setObject:@"0" forKey:@"state_indicator"];
        [userDefault synchronize];
    }
    
    
    
    UIButton *buttonLeft = [[UIButton alloc]initWithFrame:CGRectMake(16, iconYPost + 16, 24, 24)];
    [buttonLeft setImage:[UIImage imageNamed:@"ic_tb_sidemenu"] forState:UIControlStateNormal];
    [buttonLeft addTarget:self action:@selector(showMenuLeft) forControlEvents:UIControlEventTouchUpInside];
    [buttonLeft setContentMode:UIViewContentModeScaleAspectFit];
    [self.view insertSubview:buttonLeft aboveSubview:topView];
    
    [imgLogo setFrame:CGRectMake(buttonLeft.frame.origin.x + buttonLeft.frame.size.width + 4, iconYPost+8, 110, 32)];

    CGFloat width = [UIScreen mainScreen].bounds.size.width;

    [imgConnection setFrame:CGRectMake((width - 18)-20, iconYPost + 16, 20, 24)];
    [imgConnection setContentMode:UIViewContentModeScaleAspectFit];
    [self.view insertSubview:imgLogo aboveSubview:topView];
    [self.view insertSubview:imgConnection aboveSubview:topView];
    
    buttonMyQR = [[UIButton alloc]initWithFrame:CGRectMake(imgConnection.frame.origin.x - (imgConnection.frame.size.width + 16), iconYPost + 16, 24, 24)];
    [buttonMyQR setContentMode:UIViewContentModeScaleAspectFit];
    [buttonMyQR setImage:[UIImage imageNamed:@"ic_tb_qrrekening"] forState:UIControlStateNormal];
    [buttonMyQR addTarget:self action:@selector(gotoMYQR) forControlEvents:UIControlEventTouchUpInside];

    [self.view insertSubview:buttonMyQR aboveSubview:buttonLeft];
    [self.view insertSubview:buttonMyQR aboveSubview:topView];
    
    buttonInbox = [[UIButton alloc]initWithFrame:CGRectMake(buttonMyQR.frame.origin.x - (buttonMyQR.frame.size.width + 16), iconYPost + 16, 24, 24)];
    [buttonInbox setContentMode:UIViewContentModeScaleAspectFit];
    if ([userDefault objectForKey:@"listInbox"]){
        if ( [[userDefault objectForKey:@"listInbox"] count] >= 1 ) {
            [buttonInbox setImage:[UIImage imageNamed:@"ic_bt_inbox_fill"] forState:UIControlStateNormal];
        } else {
            [buttonInbox setImage:[UIImage imageNamed:@"ic_bt_inbox"] forState:UIControlStateNormal];
        }
        
    } else {
        [buttonInbox setImage:[UIImage imageNamed:@"ic_bt_inbox"] forState:UIControlStateNormal];
    }
    
    [buttonInbox addTarget:self action:@selector(gotoInbox) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view insertSubview:buttonInbox aboveSubview:buttonLeft];
    [self.view insertSubview:buttonInbox aboveSubview:topView];
    
    buttonSearchFeature = [[UIButton alloc]initWithFrame:CGRectMake(buttonInbox.frame.origin.x - (buttonInbox.frame.size.width + 16), iconYPost + 16, 24, 24)];
    [buttonSearchFeature setContentMode:UIViewContentModeScaleAspectFit];
    [buttonSearchFeature setImage:[UIImage imageNamed:@"ic_search_feature"] forState:UIControlStateNormal];
    [buttonSearchFeature addTarget:self action:@selector(gotoSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.view insertSubview:buttonSearchFeature aboveSubview:buttonLeft];
    [self.view insertSubview:buttonSearchFeature aboveSubview:topView];
    [buttonSearchFeature setHidden:YES];
    
    [super viewDidLoad];
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;

    NSString *configIndicator = [userDefault objectForKey:@"config_indicator"];
    bool stateIndicator = [configIndicator boolValue];
    if (stateIndicator) {
        [imgConnection setHidden:false];
    }else{
        [imgConnection setHidden:true];
    }
    
    UISwipeGestureRecognizer *leftRecognizer;
    leftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [leftRecognizer setDirection: UISwipeGestureRecognizerDirectionLeft];
    [[self view] addGestureRecognizer:leftRecognizer];
    
    
    UISwipeGestureRecognizer *rightRecognizer;
    rightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [rightRecognizer setDirection: UISwipeGestureRecognizerDirectionRight];
    [[self view] addGestureRecognizer:rightRecognizer];
    
  
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveTestNotification:)
                                                     name:@"changeIcon"
                                                   object:nil];
        
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(listenerNavigate:)
                                                     name:@"listenerNavigate"
                                                   object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachListener:)
                                                 name:@"reachListener"
                                               object:nil];
    
    [self changeIconInbox];
}

- (void)openAppStore{
    NSString *message = @"";
    if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
        message = @"Segera update BSI Mobile anda ke versi terkini";
    }else{
        message = @"Update your BSI Mobile to the latest version";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *iTunesLink = @"itms://itunes.apple.com/us/app/apple-store/id1410072458?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink] options:@{} completionHandler:nil];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) openNodes : (NSString *)param{
    NSArray *mNode = [param componentsSeparatedByString:@"/"];
    NSDictionary* userInfo = @{@"position": @(1124),
                               @"node_index" : mNode};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)gotoSearch{
    if([self checkActivationCallback]){
        transitionCircSearch = [[CircleTransition alloc] init];
        SearchItemViewController *showup = [self.storyboard instantiateViewControllerWithIdentifier:@"SRCHITM"];
        [showup setSize:CGRectMake(buttonSearch.frame.origin.x, buttonSearch.frame.origin.y, buttonSearch.frame.size.width, buttonSearch.frame.size.height)];
        [showup setDelegate:self];
        showup.transitioningDelegate = self;
        showup.modalTransitionStyle = UIModalPresentationCustom;
        
        [self presentViewController:showup animated:YES completion:nil];
    }
}

- (void)selectedFeature:(int)index{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    switch (index) {
        case 0:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 1){
                [self.tabBarController setSelectedIndex:1];
            }
        }
            break;
        case 1:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"MasjidVC"];
        }
            break;
        case 2:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
             [self openStatic:@"QiblatVC"];
        }
            break;
        case 4:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            ListSurahViewController *mSurrah = (ListSurahViewController *) [storyboard instantiateViewControllerWithIdentifier:@"LISTSURAHVC"];
            if([language isEqualToString:@"id"]){
                [mSurrah setTitleMenu:@"Pilih Ayat"];
            }else{
                [mSurrah setTitleMenu:@"Choose Verse"];
            }
            [self.navigationController pushViewController:mSurrah animated:YES];
        }
            break;
        case 3:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            JuzAmmaViewController *mJuzamma = (JuzAmmaViewController *) [storyboard instantiateViewControllerWithIdentifier:@"JUZAMMAVC"];
            [mJuzamma setTitleMenu:@"Juz Amma"];
            [self.navigationController pushViewController:mJuzamma animated:YES];
        }
            break;
            
        case 5:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            AsmaulHusnaViewController *mAsmaulHusnahVc = (AsmaulHusnaViewController *) [storyboard instantiateViewControllerWithIdentifier:@"ASMHUSVC"];
            [mAsmaulHusnahVc setTitleMenu:@"Asmaul Husna"];
            [self.navigationController pushViewController:mAsmaulHusnahVc animated:YES];
        }
            break;
            
        case 6:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            TausiahViewController *mTausiah = (TausiahViewController *) [storyboard instantiateViewControllerWithIdentifier:@"TausiahVC"];
            if([language isEqualToString:@"id"]){
                [mTausiah setTitleMenu:@"Hikmah"];
            }else{
                [mTausiah setTitleMenu:@"Quotes"];
            }
            [self.navigationController pushViewController:mTausiah animated:YES];
        }break;
            
        case 7:{//Registrasi Notifikasi SMS // menuid 00128 / 00132
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"YES" forKey:@"skippingcontent"];
            [[DataManager sharedManager].dataExtra setValue:@"00132" forKey:@"code"];
            [self openTemplateByMenuID:@"00128" andCodeContent:@"00132"];
        }break;
        case 8:{//favorite, pembayaran
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            FavoriteDetailViewController *favDetail = (FavoriteDetailViewController *) [storyboard instantiateViewControllerWithIdentifier:@"FVRTDETAIL"];
            [favDetail setSelectedIndex:0];
            [self.navigationController pushViewController:favDetail animated:YES];
        }break;
        case 9:{//favorite, transfer
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            FavoriteDetailViewController *favDetail = (FavoriteDetailViewController *) [storyboard instantiateViewControllerWithIdentifier:@"FVRTDETAIL"];
            [favDetail setSelectedIndex:1];
            [self.navigationController pushViewController:favDetail animated:YES];
        }break;
        case 10:{//favorite, transfer
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            FavoriteDetailViewController *favDetail = (FavoriteDetailViewController *) [storyboard instantiateViewControllerWithIdentifier:@"FVRTDETAIL"];
            [favDetail setSelectedIndex:2];
            [self.navigationController pushViewController:favDetail animated:YES];
        }break;
        case 11:{//Keyboard
            NSString *msg = @"";
            NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
            if([[[userdefaults objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
                msg = @"Fitur belum tersedia";
            }else{
                msg = @"Feature is not available";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }break;
        case 12:{//Blokir Kartu Open Menuid 00087
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openTemplateByMenuID:@"00087"];
        }break;
        case 13:{//Call 14040
            NSString *strBtnYes = @"";
            NSString *strBtnNo = @"";
            NSString *strMsg = @"";
            NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
            if([[[userdefaults objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
                strBtnNo = @"Tidak";
                strBtnYes = @"Ya";
                strMsg = @"Anda akan dihubungkan dengan Call Center untuk pendaftaran BSI Mobile";
            }else{
                strBtnNo = @"No";
                strBtnYes = @"Yes";
                strMsg = @"Your call will be forwarded to Call Center for BSI Mobile Registration";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:strMsg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:strBtnYes style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                UIApplication *application = [UIApplication sharedApplication];
                NSURL *URL = [NSURL URLWithString:@"tel://14040"];
                [application openURL:URL options:@{} completionHandler:^(BOOL success) {}];
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:strBtnNo style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }break;
        case 14:{//Daftar Sanggahan Transaksi
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            ComplainViewController *popBantuan = [storyboard instantiateViewControllerWithIdentifier:@"COMPLAIN"];
            [self.navigationController pushViewController:popBantuan animated:YES];

        }break;
        case 15:{//Aisyah
            NSUserDefaults *userd = [NSUserDefaults standardUserDefaults];
            NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];

//            if([[userd objectForKey:@"email"]isEqualToString:@""] || [userd objectForKey:@"email"] == nil){
            if([email isEqualToString:@""] || email == nil){
                NSString *chatMessage = @"";
                
                if([[[userd objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
                    chatMessage = @"Email tidak boleh kosong, harap isi email di menu pengaturan.";
                }else{
                    chatMessage = @"Email cannot be empty, please fill your email in setting menu.";
                }
                
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:chatMessage preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alerts animated:YES completion:nil];
                
            }else{
                [self.navigationController popToRootViewControllerAnimated:YES];
                if(self.tabBarController.selectedIndex != 0){
                    [self.tabBarController setSelectedIndex:0];
                }
                
                WebviewViewController *webViewAisa = [storyboard instantiateViewControllerWithIdentifier:@"webviewIdentifier"];
                [self.navigationController pushViewController:webViewAisa animated:YES];
                    
            }
            
        }break;
        case 16:{//Lokasi ATM
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"0" forKey:@"atmbranchloc"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 3){
                [self.tabBarController setSelectedIndex:3];
            }
        }break;
        case 17:{//Lokasi Branch
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"1" forKey:@"atmbranchloc"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 3){
                [self.tabBarController setSelectedIndex:3];
            }
        }break;
        case 18:{//Info Kurs
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"0" forKey:@"kursgoldvc"];
            [self openStatic:@"KursGoldVC"];
        }break;
        case 19:{//Info Emas
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"1" forKey:@"kursgoldvc"];
            [self openStatic:@"KursGoldVC"];
        }break;
        case 20:{//Info Limit
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"IT01"];
        }break;
        case 21:{//List Notifikasi
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"1" forKey:@"inboxparam"];
            [self gotoInbox];
        }break;
        case 22:{//List Resi
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"0" forKey:@"inboxparam"];
            [self gotoInbox];
        }break;
        case 23:{//Medsos
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"SMVC"];
        }break;
        case 24:{//Aktivasi
//            if([[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
            if([NSUserdefaultsAes getValueForKey:@"customer_id"]){
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_DONE") preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                if (@available(iOS 13.0, *)) {
                    [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [self presentViewController:alerts animated:YES completion:nil];
            }else{
//                [self goToPopupActivation];

                if([[DataManager sharedManager].dataExtra objectForKey:@"activationFromLink"]){
//                    [[DataManager sharedManager].dataExtra removeObjectForKey:@"activationFromLink"];
//                    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *pop = [self topViewController];
                    UIViewController *viewCont = [storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
                    self.slidingViewController.topViewController = viewCont;
                    [pop dismissViewControllerAnimated:YES completion:nil];
                }else{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    if(self.tabBarController.selectedIndex != 0){
                        [self.tabBarController setSelectedIndex:0];
                    }
                    [self openStatic:@"ActivationVC"];
                }
            }
        }break;
        case 25:{//Aktivasi Ulang
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ActivationRetrival"];
        }break;
        case 26:{//Change Password
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ASVC"];
        }break;
        case 27:{//Change Pin
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ChangePINVC"];
        }break;
        case 28:{//Change Language
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ChangeLngVC"];
        }break;
        case 29:{//Email
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ChangeEmailVC"];
        }break;
        case 30:{//About
            [self showAbout];
        }break;
        default:
            break;
    }
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    [transitionCircSearch setTransitionMode:dismiss];
    [transitionCircSearch setStartingPoint:buttonSearch.center];
    [transitionCircSearch setCircleColor:[UIColor whiteColor]];

    return transitionCircSearch;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    [transitionCircSearch setTransitionMode:present];
    [transitionCircSearch setStartingPoint:buttonSearch.center];
    [transitionCircSearch setCircleColor:[UIColor whiteColor]];

    return transitionCircSearch;
}
-(void) checkInboxNotif{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    bool stateValid = false;
    if (cid != nil || [cid isEqualToString:@""]) {
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"YES"]) {
                stateValid = true;
            }
        }else{
            stateValid = true;
        }
    }
    if (stateValid) {
        NSString *strUrl = [NSString stringWithFormat:@"request_type=check_notif,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:false];
    }
}


-(void) setShowMenu : (NSString *) state{
    actionSwipe = state;
}

-(void) stateShow : (bool) state{
    showMenu = state;
}

-(void)handleSwipeFrom:(UISwipeGestureRecognizer*)gestureRecognizer
{
    actionSwipe = @"ROOT";
    
    if ([actionSwipe isEqualToString:@"ROOT"]) {
        if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionRight) {
            
            NSLog(@"get gesture right");
            if (!showMenu) {
                showMenu = true;
                [self.slidingViewController anchorTopViewToRightAnimated:true];
            }
        }
        if (gestureRecognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
            NSLog(@"get gesture Left");
            if (showMenu) {
                showMenu = false;
                [self.slidingViewController resetTopViewAnimated:true];
            }
        }
    }
}


-(void)startIdleTimer {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    
    if ((cid == nil) || ([cid isEqualToString:@""])) {
        NSLog(@"Don't start auto logout timer");
    } else {
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"YES"]) {
                if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
                    [self resetCountDown];
                    
                    [idleTimer invalidate];
                    idleTimer = nil;
                    idleTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                                 target:self
                                                               selector:@selector(countDown)
                                                               userInfo:nil
                                                                repeats:YES];
                    
                    [[NSRunLoop mainRunLoop] addTimer:idleTimer forMode:UITrackingRunLoopMode];
                    [userDefault setValue:@"YES" forKey:@"isExist"];
                    [userDefault synchronize];
                }
            }
        }
    }
}

-(void)stopIdleTimer {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"NO" forKey:@"isExist"];
    [userDefault synchronize];
    [idleTimer invalidate];
    idleTimer = nil;
    [self resetCountDown];
}

-(void)refreshIdleTimer {
    [self resetCountDown];
}


-(void)countDown {
    NSLog(@"%@", [NSString stringWithFormat:@"Countdown %d",maxIdle]);
    if (maxIdle == 0) {
        [idleTimer invalidate];
        idleTimer = nil;
        [self showPopupAutoLogout];
    } else {
        maxIdle = maxIdle - 1;
       
    }
}

- (void) logoutSession{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    
    [userDefault setObject:@"NO" forKey:@"hasLogin"];
    [userDefault synchronize];
    
    if([[userDefault objectForKey:@"mustLogin"]isEqualToString:@"YES"]){
        [imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_unlogged"]];
    }else{
        [self->imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_connect"]];
    }
    MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
    [menuVC awakeFromNib];
    
    if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
        NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
        userInfo = @{@"position": @(111)};
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    } else {
        exit(0);
    }
    [self backToR];
}

-(void)resetCountDown{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSInteger nMaxIdle = [[userDefault objectForKey:@"config_timeout_session"]integerValue] / 1000;
    if (nMaxIdle == 0) {
        maxIdle = MAX_IDLE;
    }else{
        maxIdle = (int)nMaxIdle;
    }
}

- (UIImage *)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)openTemplateWithDataRoot:(int)position{
    
    NSArray *temp = [[DataManager sharedManager] getJSONData:position];
    bool stateAcepted = false;
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        
        NSString *templateName = [object valueForKey:@"template"];
        TemplateViewController *templateView = [self routeTemplateController:templateName];
        [templateView setJsonData:object];
        if([templateName isEqualToString:@"LV01"]){
            LV01ViewController *viewCont = (LV01ViewController *) templateView;
            [viewCont setFirstLV:true];
        }
        
        stateAcepted = [Utility vcNotifConnection: templateName];
        if (stateAcepted) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)openRedirectTo:(NSString*)menuid
            withMenuID:(BOOL)withmenuid
               andCode:(NSString*)code
              setTitle:(NSString*)title
               andText:(NSString*)text
        andTitleButton:(NSString*)titleButton{
    
    RedirectCFViewController *temV = [self.storyboard instantiateViewControllerWithIdentifier:@"RedirectCF"];
    [temV setTitle:title];
    [temV setTitleButton:titleButton];
    [temV setTextConfirmation:text];
    [temV redirectTo:menuid andCode:code isMenuID:withmenuid];
    
    [self.navigationController pushViewController:temV animated:YES];
}

- (TemplateViewController *)routeTemplateController:(NSString *)templateName {
    NSArray *sbList = [[DataManager sharedManager] getStoryboardList];
    for (NSString* sbName in sbList) {
        @try {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:sbName bundle:nil];
            TemplateViewController *templateView = [storyboard instantiateViewControllerWithIdentifier:templateName];
            return templateView;
        } @catch (NSException *exception) {
            continue;
        }
    }
    return [self.storyboard instantiateViewControllerWithIdentifier:templateName];
}

- (void)openStatic:(NSString *)templateName{
    
    bool stateAcepted = [Utility vcNotifConnection:templateName];
    if (stateAcepted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        TemplateViewController *templateView = [self routeTemplateController:templateName];
//        RootViewController *templateView =  [self.storyboard instantiateViewControllerWithIdentifier:templateName];
        [self.tabBarController setSelectedIndex:0];
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateView animated:YES];
    }
}

- (void)openStatic:(NSString *)storyboard withTemplateName:(NSString*)templateName{
    
    bool stateAcepted = [Utility vcNotifConnection: templateName];
    if (stateAcepted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        UIStoryboard *storyboardUI = [UIStoryboard storyboardWithName:@"Shariapoint" bundle:nil];
        RootViewController *templateView =  [storyboardUI instantiateViewControllerWithIdentifier:templateName];
        [self.tabBarController setSelectedIndex:0];
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateView animated:YES];
    }
}

-(BOOL) checkActivationCallback{
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        if (![[userDefault valueForKey:@"config_onboarding"] boolValue]){
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:lang(@"INFO")
                                  message:lang(@"ACTIVATION_CONFIRM")
                                  delegate:self
                                  cancelButtonTitle:lang(@"NOPE")
                                  otherButtonTitles:@"OK", nil];
            alert.tag = 229;
            [alert show];
            
            return false;
        }else{
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
            firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:firstVC animated:NO completion:^{
                if([[DataManager sharedManager].dataExtra objectForKey:@"activationFromLink"]){
                    [[DataManager sharedManager].dataExtra removeObjectForKey:@"activationFromLink"];
                }else{
                    PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
                    [firstVC presentViewController:viewCont animated:YES completion:nil];
                }
            }];
            return false;
        }
    }
    return true;
}


- (void) listenerNavigate:(NSNotification *) notification {
    if ([notification.name isEqualToString:@"listenerNavigate"]){
        /*UITabBarController *tabCont = self.tabBarController;
        UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
        HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
        [homeCont startIdleTimer];*/
        [self refreshIdleTimer];
        
        NSDictionary* userInfo = notification.userInfo;
        NSNumber* position = (NSNumber*)userInfo[@"position"];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *last = [userDefault stringForKey:@"last"];
        NSString *postLast = [NSString stringWithFormat:@"%i",position.intValue];
        
        if (![last isEqualToString:postLast]) {
            NSLog (@"Successfully received test notification! %i", position.intValue );
            if (position.intValue == 0) {
                [[DataManager sharedManager]resetObjectData];
                [userDefault setObject:@"ic_info_account_header.png" forKey:@"imgIcon"];
                [self openTemplateWithDataRoot:0];
            } else if (position.intValue == 1) {
                [[DataManager sharedManager]resetObjectData];
                [userDefault setObject:@"ic_payment_header.png" forKey:@"imgIcon"];
                [self openTemplateWithDataRoot:1];
            } else if (position.intValue == 2) {
                [[DataManager sharedManager]resetObjectData];
                [userDefault setObject:@"ic_purchase_header.png" forKey:@"imgIcon"];
                [self openTemplateWithDataRoot:2];
            } else if (position.intValue == 3) {
                [[DataManager sharedManager]resetObjectData];
                [userDefault setObject:@"ic_transfer_header.png" forKey:@"imgIcon"];
                [self openTemplateWithDataRoot:3];
            } else if (position.intValue == 4) {
                [[DataManager sharedManager]resetObjectData];
                [userDefault setObject:@"ic_open_account_header.png" forKey:@"imgIcon"];
                [self openTemplateWithDataRoot:4];
            } else if (position.intValue == 5) {
                [[DataManager sharedManager]resetObjectData];
                [userDefault setObject:@"icon_kurs_emas.png" forKey:@"imgIcon"];
//                [self openStatic:@"KursVC"];
                [self openStatic:@"KursGoldVC"];
            } else if (position.intValue == 6) {
                [userDefault setObject:@"SM.png" forKey:@"imgIcon"];
                [self openStatic:@"SMVC"];
            } else if (position.intValue == 7) {
                [self openStatic:@"ATMVC"];
            } else if (position.intValue == 8) {
                [[DataManager sharedManager]resetObjectData];
                [userDefault setObject:@"ic_header_berbagi.png" forKey:@"imgIcon"];
                [self openTemplateWithDataRoot:6];
            } else if (position.intValue == 11) {
                [[DataManager sharedManager]resetObjectData];
                NSArray *tempMain = [[DataManager sharedManager] getJSONData:3];
                if (tempMain !=nil) {
                    NSDictionary *objectMain = [tempMain objectAtIndex:1];
                    //                NSString *templateName = [objectMain valueForKey:@"template"];
                    //                TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:templateName];
                    //                [templateView setJsonData:objectMain];
                    int ind = 0;
                    NSArray *menu = [objectMain valueForKey:@"action"];
                    for (int i =0; i < [menu count]; i++) {
                        NSArray *selMenu = [[objectMain valueForKey:@"action"] objectAtIndex:i];
                        if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
                            ind = i;
                            break;
                        }
                    }
                    NSArray *temp = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
                    NSDictionary *object = [temp objectAtIndex:1];
                    // NSString *templateNames = [object valueForKey:@"template"];
                    DataManager *dataManager = [DataManager sharedManager];
                    [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
                    dataManager.currentPosition++;
                    NSDictionary *tempData = [dataManager getObjectData];
                    TemplateViewController *templateViews = [self routeTemplateController:[tempData valueForKey:@"template"]];
                    [templateViews setJsonData:tempData];
                    [userDefault setObject:@"ic_qrpay_header.png" forKey:@"imgIcon"];
                    //[self.navigationController pushViewController:templateViews animated:YES];
                    bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
                    if (stateAcepted) {
//                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        //            alert.tag = 404;
//                        [alert show];
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                        [self presentViewController:alert animated:YES completion:nil];
                    }else{
                    
                    [self.tabBarController setSelectedIndex:0];
                    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                        [currentVC pushViewController:templateViews animated:YES];
                        
                    }
                }else{
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    //            alert.tag = 404;
//                    [alert show];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            } else if (position.intValue == 12) {
                [self gotoInbox];
            }else if(position.intValue == 13){ //adding kartu manajemen
                [userDefault setObject:@"icon_blokir_kartu" forKey:@"imgIcon"];
                [self openTemplateWithDataRoot:7];
            }else if (position.intValue == 313){
//                [dataManager resetObjectData];
                /*UITabBarController *tabCont = self.tabBarController;
                UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
                HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
                [homeCont stopIdleTimer];*/
                [self stopIdleTimer];
                
                [self.navigationController popToRootViewControllerAnimated:YES];
                if(self.tabBarController.selectedIndex != 0){
                    [self.tabBarController setSelectedIndex:0];
                }
            } else if (position.intValue == 111) {
                //[self openStatic:@"NLoginVC"];
            /*} else if (position.intValue == 112) {
                [self openStatic:@"CreatePwdVC"];*/
            } else if (position.intValue == 113) {
                [self openStatic:@"ASVC"];
            } else if (position.intValue == 413) {
                [self openStatic:@"ChangePINVC"];
            } else if (position.intValue == 513) {
                [userDefault setObject:@"YES" forKey:@"fromSliding"];
                [userDefault setValue:@"@" forKey:@"openRoot"];
                [userDefault synchronize];
                
                [self openStatic:@"ActivationVC"];
            } else if (position.intValue == 613) {
                [self openStatic:@"ChangeLngVC"];
            } else if (position.intValue == 713) {
                [self openStatic:@"ChangeEmailVC"];
            } else if (position.intValue == 714) {
                [userDefault setObject:@"YES" forKey:@"fromSliding"];
                [userDefault setValue:@"@" forKey:@"openRoot"];
                [userDefault synchronize];
                [self openStatic:@"ActivationRetrival"];
            } else if (position.intValue == 777) {
                [self openStatic:@"NCreatePwdVC"];
            } else if (position.intValue == 997) {
                [self openStatic:@"ShalatGPSVC"];
            } else if (position.intValue == 998) {
                [self openStatic:@"MasjidVC"];
            } else if (position.intValue == 999) {
                [self openStatic:@"QiblatVC"];
            } else if (position.intValue == 1000) {
                [self openStatic:@"HalalVC"];
            } else if (position.intValue == 1001) {
                [self openStatic:@"LoungeVC"];
            } else if (position.intValue == 1002) {
                [self openStatic:@"ISVC"];
            } else if (position.intValue == 1111) {
                [self showPopupAutoLogout];
            } else if (position.intValue == 1112) {
                MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
                [menuVC awakeFromNib];
                NSString *isExist = [userDefault objectForKey:@"isExist"];
                if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
                    [self startIdleTimer];
                } else {
                    [self refreshIdleTimer];
                }
            } else if (position.intValue == 1113) {
                NSString *isExist = [userDefault objectForKey:@"isExist"];
                if ([isExist isEqualToString:@"YES"]) {
                    [self stopIdleTimer];
                }
            } else if (position.intValue == 1114) {
                [self refreshIdleTimer];
            }else if(position.intValue == 1115){
//                [self openStatic:@"DTNTF01"];
                DataManager *dataManager = [DataManager sharedManager];
                [dataManager.dataExtra setValue:[userInfo valueForKey:@"mid"] forKey:@"mid"];
                [userDefault setObject:@"RECEIVING" forKey:@"action"];
                [userDefault synchronize];
                TemplateViewController *templateView = [self routeTemplateController:@"DTNTF01"];
                [self.tabBarController setSelectedIndex:0];
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateView animated:YES];
                
//                if ([[userDefault valueForKey:@"hasLogin"]isEqualToString:@"NO"]) {
//                    [userDefault setObject:@"RECEIVING" forKey:@"action"];
//                    [userDefault synchronize];
//                    UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
//                    self.slidingViewController.topViewController = viewCont;
//                }else{
//                    TemplateViewController *templateView =  [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
//                    [self.tabBarController setSelectedIndex:0];
//                    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
//                    [currentVC pushViewController:templateView animated:YES];
//                }
                
            }else if(position.intValue == 1116){
                NSString *token = [userInfo valueForKey:@"token"];
                if (token != nil) {
                    [userDefault setValue:token forKey:@"token"];
                    [userDefault synchronize];
                }
            }else if(position.intValue == 1117){
                [self checkInboxNotif];
            }else if(position.intValue == 1118){
                [self stopIdleTimer];
            }else if(position.intValue == 1119){
                [self goToPopupOnboarding];
            }else if(position.intValue == 1120){
                [self goToPopupActivation];
            }else if(position.intValue == 1121){
                NSDictionary *dataTemp = [userInfo objectForKey:@"data_temp"];
                TemplateViewController *templateViews = [self routeTemplateController:[dataTemp valueForKey:@"template"]];
                [templateViews setJsonData:dataTemp];
                bool stateAcepted = [Utility vcNotifConnection: [dataTemp valueForKey:@"template"] ];
                if (stateAcepted) {
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    //            alert.tag = 404;
//                    [alert show];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    [self.tabBarController setSelectedIndex:0];
                    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                    [currentVC popToRootViewControllerAnimated:NO];
                    [currentVC pushViewController:templateViews animated:YES];
                    
                }
            }else if(position.intValue == 1122){
                listAcctFrom = [userInfo objectForKey:@"GET_LIST_ACCT"];
                if ([[userDefault objectForKey:@"is_jail_break"]boolValue]) {
                    [self getListAccount];
                }
                
            }else if(position.intValue == 1123){
//                [self goToLogin];
            }
            else if (position.intValue == 1124){
                NSMutableArray *arNodeIndex = (NSMutableArray *) [userInfo objectForKey:@"node_index"];
//                [userDefault setObject:[userInfo objectForKey:@"node_index"] forKey:@"deeplink_nodeindex"];
//                NSArray *mNode = [arNodeIndex[[arNodeIndex count]-1] componentsSeparatedByString:@"="];
                NSString *mNode = arNodeIndex[[arNodeIndex count]-1];

                if([mNode containsString:@"base64="]){
                    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[mNode stringByReplacingOccurrencesOfString:@"base64=" withString:@""] options:0];
                    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                    NSLog(@"Decode String Value: %@", decodedString);
                    
                    /**
                     [{"version":"5.19.0","nodeMenu":"1","nodeSubmenu":"1","nodeMerchant":"0","code":"","menuId":"00002"},{"version":"5.20.0","nodeMenu":"1","nodeSubmenu":"1","nodeMerchant":"0","code":"","menuId":"00002"}] -- Deprecated
                     menu_type = 0 --> nodes
                     menu_type = 1 --> menuid
                     menu_type = 2 --> index_menu
                     menu_type =
                     [
                       {
                         "version": "5.21.0",
                         "menu_type": "2",
                         "nodes": "0",
                         "service_code": "",
                         "menuId": "00002",
                         "index_menu": "24"
                       }
                     ]
                     desc . launcher
                     */
                    NSArray *getData = [NSJSONSerialization JSONObjectWithData:[decodedString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    
                    NSArray *ver = [@VERSION_NAME componentsSeparatedByString:@"."];
                    double versionNumber = [ver[1]doubleValue];

                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc]init];
                    for(NSDictionary *data in getData){
                        NSArray *verB = [[data objectForKey:@"version"] componentsSeparatedByString:@"."];
                        [newDict addEntriesFromDictionary:@{verB[1]: data}];
                    }
                    if(versionNumber >= 19){
                        for(int i = versionNumber; i > 0; i--){
                            if([newDict objectForKey:[NSString stringWithFormat:@"%d",i]] != nil){
                                NSDictionary *data = [newDict objectForKey:[NSString stringWithFormat:@"%d",i]];
                                
                                switch ([[data objectForKey:@"menu_type"] intValue]) {
                                    case 0:
                                        //nodes
                                        {
                                            NSString *launcher = [data valueForKey:@"nodes"];
                                            NSArray *mNode = [launcher componentsSeparatedByString:@"/"];
                                            [self goToVcFromDeepLink:mNode];
                                        }
                                        break;
                                    case 1:
                                       //menuid
                                        [self openTemplateByMenuID:[data objectForKey:@"menuId"]];
                                       break;
                                    case 2:{
                                       //index_menu
                                        if([[data objectForKey:@"index_menu"]intValue] == 24){
                                            [[DataManager sharedManager].dataExtra setValue:@"YES" forKey:@"activationFromLink"];
                                        }
                                        [self selectedFeature:[[data objectForKey:@"index_menu"]intValue]];
                                    }
                                       break;

                                    default:
                                        break;
                                }
                                
                                break;
                            }
                        }
                    }else{
                        [userDefault setObject:[userInfo objectForKey:@"node_index"] forKey:@"deeplink_nodeindex"];
                        [self goToVcFromDeepLink:arNodeIndex];
                    }
                    
                }
                else if([mNode containsString:@"="] && [arNodeIndex count] != 4){
                    NSArray *nodeCoded = [arNodeIndex[[arNodeIndex count]-1] componentsSeparatedByString:@"="];
                    if(![nodeCoded[1] isEqualToString:@""]){
//                        [userDefault setObject:[userInfo objectForKey:@"node_index"] forKey:@"deeplink_nodeindex"];
                        [userDefault setValue:nodeCoded[1] forKey:@"deeplink_servicecode"];
                        [self goToVcFromDeepLink:arNodeIndex];
                    }
                }
                else{
                    [userDefault setObject:[userInfo objectForKey:@"node_index"] forKey:@"deeplink_nodeindex"];
                    [self goToVcFromDeepLink:arNodeIndex];
                }
            }else if(position.intValue == 1126){
				[self checkPushMessage];
			}else if(position.intValue == 1125){
                dataMessageBurroq = [userInfo valueForKey:@"dataMessage"];
                findId = [userInfo valueForKey:@"findID"];
                [self goToBurroq];
            }else if (position.intValue == 1127){
                [self gotoGold];
            }
            
            
            [userDefault setValue:postLast forKey:@"last"];
            if (showMenu) {
                showMenu = false;
                [self.slidingViewController resetTopViewAnimated:true];
            }
        }        
    }
}

-(void)checkPushMessage{
    NSString *strUrl = [NSString stringWithFormat:@"request_type=inq_push_message,date_local,language"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:false];
    
}

-(void) pushMessageBurroq{
    UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"POPBURROQ"];
    [self presentViewController:viewCont animated:YES completion:nil];
}

 #pragma mark token FCM and request-type=login
-(void) goToLogin{
        NSString *nImei = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
        NSString *tokenFCM = [inboxHelper readToken:nImei];
        
        if ([tokenFCM isEqualToString:@""] || tokenFCM == nil) {
            tokenFCM = @"";
        }
        
        [[[DataManager sharedManager]dataExtra]setObject:tokenFCM forKey:@"token"];
        
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:@"request_type=login,customer_id,token,notif_type=iphone" needLoading:true textLoading:@"" encrypted:true banking:true continueLoading:false];
}

#pragma mark handle deep link to vc targer
-(void) goToVcFromDeepLink : (NSArray *) nodeIndex{
    NSLog(@"nodeIndex %@", nodeIndex);
    NSString *typeTransfer = [[DataManager sharedManager].dataExtra valueForKey:@"type_transfer"];
    [[DataManager sharedManager]resetObjectData];
//    NSArray *arr = [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys];
    NSArray *temp = nil;
    if([DataManager sharedManager].listMenu.count > [[nodeIndex objectAtIndex:0]intValue]){
        temp = [[DataManager sharedManager] getJSONData:[[nodeIndex objectAtIndex:0]intValue]];
    }
    if(temp != nil){
        if(nodeIndex.count > 1){
            NSDictionary *objectMain = [temp objectAtIndex:1];
            NSArray *nTempAction;
            if([[objectMain objectForKey:@"action"] isKindOfClass:[NSArray class]]){
                nTempAction = [objectMain valueForKey:@"action"];
                if(nTempAction.count > [[nodeIndex objectAtIndex:1]intValue]){
                    NSArray *actionTempe = [nTempAction objectAtIndex:[[nodeIndex objectAtIndex:1]intValue]];
                    if([[[actionTempe objectAtIndex:1]objectForKey:@"action"] isKindOfClass:[NSArray class]]){
            //            NSArray *objects = [[nTempAction objectAtIndex:1] objectForKey:@"action"];
                        NSString *templateName = [[actionTempe objectAtIndex:1] valueForKey:@"template"];
                        TemplateViewController *templateView = [self routeTemplateController:templateName];
                        [templateView setJsonData:[actionTempe objectAtIndex:1]];
                        if([templateName isEqualToString:@"LV01"]){
                            LV01ViewController *viewCont = (LV01ViewController *) templateView;
                            [viewCont setFirstLV:true];
                        }

                        [self.navigationController popToRootViewControllerAnimated:YES];
                        if(self.tabBarController.selectedIndex != 0){
                            [self.tabBarController setSelectedIndex:0];
                        }

                        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                        [currentVC pushViewController:templateView animated:YES];

                    }else{
                        NSDictionary *object = [actionTempe objectAtIndex:1];
                        DataManager *dataManager = [DataManager sharedManager];
                        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[actionTempe objectAtIndex:0]];
                        dataManager.currentPosition++;
                        NSMutableDictionary *tempData = [dataManager getObjectData];
                        if(typeTransfer){
                            [tempData setObject:typeTransfer forKey:@"type_transfer"];
                        }
                        [self pushDeepLinkTemplate:tempData];
                    }
                }
            }
        }
        else if(nodeIndex.count == 1){
            NSString *menuID = [temp objectAtIndex:0];
            [self openTemplateByMenuID:menuID];
        }
        
    }
}

-(void) pushDeepLinkTemplate : (NSDictionary *) tempData{
    @try {
        TemplateViewController *templateViews = [self routeTemplateController:[tempData valueForKey:@"template"]];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            [self stopIdleTimer];
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateViews animated:YES];
            
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
}

-(void)pushNavTemplate : (NSDictionary *) tempData inStoryboard:(NSString*)storyboardName{
    @try {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
//        TemplateViewController *templateViews = [storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"]];
        TemplateViewController *templateViews;
        templateViews = [self routeTemplateController:[tempData valueForKey:@"template"]];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UITabBarController *tabCont = self.tabBarController;
            UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.parentViewController.tabBarController.selectedIndex != 0){
                [self.parentViewController.tabBarController setSelectedIndex:0];
            }
            [navCont pushViewController:templateViews animated:YES];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
}


-(void)pushNavTemplate : (NSDictionary *) tempData{
    @try {
        TemplateViewController *templateViews = [self routeTemplateController:[tempData valueForKey:@"template"]];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UITabBarController *tabCont = self.tabBarController;
            UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.parentViewController.tabBarController.selectedIndex != 0){
                [self.parentViewController.tabBarController setSelectedIndex:0];
            }
            [navCont pushViewController:templateViews animated:YES];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
}

-(void) disposePenyesuaianPopupFiltering{
    @try {
        UINavigationController *nav = (UINavigationController*)self.presentingViewController;
        
        [self dismissViewControllerAnimated:YES completion:^{
            
            [nav popToRootViewControllerAnimated:YES];
        }];
        
        //        UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"JuzAmmaFilterVC"];
        //        [viewCont dismissViewControllerAnimated:YES completion:nil];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}


- (void)backToR{
    DataManager *dataManager = [DataManager sharedManager];
    [dataManager resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

- (void)backPrevious{
    DataManager *dataManager = [DataManager sharedManager];
    if(dataManager.currentPosition > 0){
        dataManager.currentPosition--;
        UINavigationController *navigationCont = self.navigationController;
        [navigationCont popViewControllerAnimated:YES];
    }
}

- (void) receiveTestNotification:(NSNotification *) notification {
    NSDictionary *userInfo = notification.userInfo;
   
    if ([notification.name isEqualToString:@"changeIcon"]){
        [self changeIconInbox];
        if (userInfo) {
            if ([[userInfo valueForKey:@"read_fcm_notif"] isEqualToString:@"new"]) {
                [buttonInbox setImage:[UIImage imageNamed:@"ic_bt_inbox_fill"] forState:UIControlStateNormal];
            }else{
                [buttonInbox setImage:[UIImage imageNamed:@"ic_bt_inbox"] forState:UIControlStateNormal];
            }
        }
        
    }
    
}

-(void)changeIconInbox{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSInteger countInbox, countNotif;
    countInbox = [inboxHelper readMarked];
    countNotif = [[userDefault valueForKey:@"count_notif"]integerValue];
    if (countInbox > 0 || countNotif > 0) {
        [buttonInbox setImage:[UIImage imageNamed:@"ic_bt_inbox_fill"] forState:UIControlStateNormal];

    }else{
        [buttonInbox setImage:[UIImage imageNamed:@"ic_bt_inbox"] forState:UIControlStateNormal];
    }
}

-(void) reachListener:(NSNotification *) notification{
    NSDictionary *userInfo = notification.userInfo;
    if([notification.name isEqualToString:@"reachListener"]){
        if([[userInfo valueForKey:@"reachCallback"] isEqualToString:@"unreach"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_disconnect"]];
            });
            
            
        }else if([[userInfo valueForKey:@"reachCallback"] isEqualToString:@"reach"]){
            
            if ([[userInfo valueForKey:@"code_state"] isEqualToString:@"1"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSUserDefaults *nsu = [NSUserDefaults standardUserDefaults];
                    NSString *hasLogin = [nsu objectForKey:@"hasLogin"];
                    NSString *mustLogin = [nsu objectForKey:@"mustLogin"];
                    if(mustLogin != nil && [mustLogin isEqualToString:@"YES"]){
                        if(hasLogin != nil && [hasLogin isEqualToString:@"YES"]){
                            [self->imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_connect"]];
                        }else{
                            [self->imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_unlogged"]];
                        }
                    }else{
                        [self->imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_connect"]];
                    }
                });
            }else if ([[userInfo valueForKey:@"code_state"]isEqualToString:@"0"]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self->imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_disconnect"]];
                });
            }
        }else if ([[userInfo valueForKey:@"reachCallback"]isEqualToString:@"net_reach"]){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->imgConnection setImage:[UIImage imageNamed:@"ic_tb_sgn_connecting"]];
            });
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showPopupAutoLogout
{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *alertTitle = @"";
    NSString *alertMessage = @"";
    if ([lang isEqualToString:@"en"]) {
        alertTitle = @"Session has ended";
        alertMessage = @"Your session has ended. Please log in to continue using BSI Mobile";
    } else {
        alertTitle = @"Sesi Berakhir";
        alertMessage = @"Sesi Anda telah berakhir. Silahkan login ulang untuk melanjutkan penggunaan BSI Mobile";
    }
    
//    [self listOfVC];
//    [self callGenCF02];
    
    [self disposePenyesuaianPopupFiltering];
    
    if ([[[userDefault valueForKey:@"mustLogin"]uppercaseString]isEqualToString:@"YES"]) {
        if ([[[userDefault valueForKey:@"hasLogin"]uppercaseString]isEqualToString:@"YES"]) {
            [self stopIdleTimer];
            [self logoutSession];
        }
    }
    
   
}

- (void) listOfVC {
    @try {
        NSArray * controllerArray = [[self navigationController] viewControllers];
        NSLog(@"List of View Controller :\n");
        for (UIViewController *controller in controllerArray){
            //Code here.. e.g. print their titles to see the array setup;
            NSLog(@"- %@\n",controller.title);
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    if (alertView.tag == 12345) {
        if (buttonIndex == 0) {
            //CANCEL Button
        } else {
            //OK Button
            [userDefault setObject:@"NO" forKey:@"hasLogin"];
            [userDefault synchronize];
            
            if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
                [self.navigationController popToRootViewControllerAnimated:YES];
                if(self.tabBarController.selectedIndex != 0){
                    [self.tabBarController setSelectedIndex:0];
                }
            } else {
                exit(0);
            }
        }
    } else if (alertView.tag == 505) {
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault synchronize];
        
        if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            
            userInfo = @{@"position": @(111)};
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        } else {
            exit(0);
        }
    }
    else if(alertView.tag == 007){
        [self directToActivation];
    }
}

-(void) directToLogin{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    [userDefault setObject:@"NO" forKey:@"hasLogin"];
    [userDefault synchronize];
    
    if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
        NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
        userInfo = @{@"position": @(111)};
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    } else {
        exit(0);
    }
}

-(void) directToActivation{
    
    NSDictionary* userInfo = @{@"position": @(513)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}

- (void)gotoInbox {
    
//    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:5];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition = 1;
        NSDictionary *tempData = [dataManager getObjectData];
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TemplateViewController *templateViews = [ui instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO" ) message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            if([self checkActivationCallback]){
                
                [self.tabBarController setSelectedIndex:0];
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateViews animated:YES];

            }
            
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO" ) message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)goToBurroq{

    [self getMenuIdx:@"indexFinancing"];
    PBYCF00ViewController *templateViews = [self.storyboard instantiateViewControllerWithIdentifier:@"PBYCF00"];
    [templateViews setFindId:findId];
    [templateViews setDataMessageBurroq:dataMessageBurroq];
    
    bool stateAcepted = [Utility vcNotifConnection: @"PBYCF00"];
    if (stateAcepted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO" ) message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        //            [self.navigationController pushViewController:templateViews animated:YES];
        [self.tabBarController setSelectedIndex:0];
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateViews animated:YES];
    }
}

-(void)getMenuIdx : (NSString *) key {
    NSString *url =[NSString stringWithFormat:@"%@menu_index_ios.html?vn=%@",API_URL,@VERSION_NAME];
    NSLog(@"%@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];
    [request setHTTPMethod:@"GET"];
    @try {
        NSError *error;
        NSURLResponse *urlResponse;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if(error == nil){
            NSDictionary *mJsonObject = [NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&error];
            if(mJsonObject){
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setValue:[mJsonObject valueForKey:key] forKey:key];
                [userDefault synchronize];
            }
            
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", [exception description]);
    }
}


- (void)gotoMYQR {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    
    if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
        [self startIdleTimer];
    } else {
        [self refreshIdleTimer];
    }
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TemplateViewController *templateViews = [ui instantiateViewControllerWithIdentifier:@"MyQRVC"];
    bool stateAcepted = [Utility vcNotifConnection: @"MyQRVC"];
    if (stateAcepted) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        
        NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
        NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"NO"]) {
                [self.tabBarController setSelectedIndex:0];
                PopUpLoginViewController *popLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupLogin"];
                [popLogin setMFeat:@"MyQRVC"];
                [self presentViewController:popLogin animated:YES completion:nil];
                
            }else{
                [self.tabBarController setSelectedIndex:0];
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateViews animated:YES];
            }
            
        }else{
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateViews animated:YES];
        }
        
       
        
    }
}

- (void)gotoPopLogin :(NSString*) mFeat{
    [self.tabBarController setSelectedIndex:0];
    PopUpLoginViewController *popLogin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupLogin"];
    [popLogin setMFeat:mFeat];
    [self presentViewController:popLogin animated:YES completion:nil];
}

- (void)gotoGold{
    [self.tabBarController setSelectedIndex:0];
    if([self checkActivationCallback]){
        NSUserDefaults *usdef = [NSUserDefaults standardUserDefaults];
        if([[usdef objectForKey:@"hasLogin"]isEqualToString:@"YES"]){
            TemplateViewController *gDash = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EMAS01"];
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:gDash animated:YES];
        }else{
            [self gotoPopLogin:@"gold"];
        }

    }
}

- (void)logoutApps {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:valTitle
                          message:valMsg
                          delegate:nil
                          cancelButtonTitle:valCancel
                          otherButtonTitles:valAgree, nil];
    alert.delegate = self;
    alert.tag = 12345;
    [alert show];
    
}

- (void)showMenuLeft{
    /*UITabBarController *tabCont = self.tabBarController;
    UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
    HomeViewController *homeCont = [navCont.viewControllers objectAtIndex:0];
    [homeCont startIdleTimer];*/
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
//    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
    NSString *isExist = [userDefault objectForKey:@"isExist"];
    
    if (([isExist isEqualToString:@"NO"]) || (isExist == nil)) {
        [self startIdleTimer];
    } else {
        [self refreshIdleTimer];
    }
    
    if (showMenu) {
        showMenu = false;
        [self.slidingViewController resetTopViewAnimated:YES];
    } else {
        showMenu = true;
        [self.slidingViewController anchorTopViewToRightAnimated:true];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event

{
    
    UITouch *touch = [[event allTouches] anyObject];
    
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if(touchLocation.x > 0 || touchLocation.y >0){
        NSDictionary* userInfo = @{@"position": @(1112)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
    
    //your logic
    
}


- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([requestType isEqualToString:@"check_notif"]) {
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingAllowFragments
                                                                       error:&error];
            if (error == nil) {
                [userDefault setValue:[dataDict valueForKey:@"count"] forKey:@"count_notif"];
            }
            else{
                NSLog(@"%@", [jsonObject objectForKey:@"response"]);
                [userDefault setValue:@"0" forKey:@"count_notif"];
                
            }
        }else{
            NSLog(@"%@", [jsonObject objectForKey:@"response"]);
            [userDefault setValue:@"0" forKey:@"count_notif"];
            
        }
        [userDefault synchronize];
        [self changeIconInbox];
        
    }
//    else if([requestType isEqualToString:@"list_account1"]){
    else if([requestType isEqualToString:@"list_account2"]){

        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
        }
    }else if([requestType isEqualToString:@"login"]){
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
//                [userDefault setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
                [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
                [userDefault setObject:@"YES" forKey:@"hasLogin"];
                [userDefault setObject:@"NO" forKey:@"firstLogin"];
                [userDefault setObject:@"HOME" forKey:@"tabPos"];
                [userDefault setObject:@"HOME" forKey:@"VC"];
                [userDefault setValue:@"NO" forKey:@"isExist"];
                [userDefault synchronize];
                
            }else{
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
                
                if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"0001"]){
                    
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self directToActivation];
                    }]];
                }else{
                    
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                }
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
    else if([requestType isEqualToString:@"inq_push_message"]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
                
                NSString *mResponse = [jsonObject objectForKey:@"response"];
                NSDictionary *mDictMessage = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                if (mDictMessage) {
                    NSString *dataMessage = @"";
                    dataMessage = [dataMessage stringByAppendingString:[mDictMessage valueForKey:@"title"]];
                    dataMessage = [dataMessage stringByAppendingString:@"\n"];
                    dataMessage = [dataMessage stringByAppendingString:[mDictMessage valueForKey:@"content"]];
                    
                    dataMessageBurroq = dataMessage;
                    findId = [mDictMessage valueForKey:@"id"];
                    
                    NSUserDefaults *userD = [NSUserDefaults standardUserDefaults];
                    
                    if([userD objectForKey:@"show.burroq.enable"] == nil || [[userD valueForKey:@"show.burroq.enable"]isEqualToString:@"YES"]){
                        PopupBurroqViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"POPBURROQ"];
                        [viewCont DataMessage:dataMessage];
                        [self presentViewController:viewCont animated:YES completion:nil];
                    }
                }
                
                
            }else if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"01"]){
                NSLog(@"%@", [jsonObject valueForKey:@"response"]);
//                PopupBurroqViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"POPBURROQ"];
//                [viewCont DataMessage:[jsonObject valueForKey:@"response"]];
//                [self presentViewController:viewCont animated:YES completion:nil];
            }
        }
   
}

- (void)errorLoadData:(NSError *)error {
    NSLog(@"%@", [error localizedDescription]);
    if ([listAcctFrom isEqualToString:@"SPLASH"]) {
        @try {
            AppDelegate *mainClass = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                mainClass.splashImageView.alpha = .0f;
            } completion:^(BOOL finished){
                if (finished) {
                    [mainClass.splashImageView removeFromSuperview];
                }
            }];
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
        }
    }
}



- (void)reloadApp {
    NSLog(@"Reload APP");
    if ([listAcctFrom isEqualToString:@"SPLASH"]) {
        @try {
            AppDelegate *mainClass = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                mainClass.splashImageView.alpha = .0f;
            } completion:^(BOOL finished){
                if (finished) {
                    [mainClass.splashImageView removeFromSuperview];
                }
            }];
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
        }
    }
}

-(void) goToPopupActivation{
    PopupActivationViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupActivationVC"];
    [self presentViewController:viewCont animated:YES completion:nil];
}

-(void) goToPopupOnboarding{
    if([[DataManager sharedManager].dataExtra objectForKey:@"activationFromLink"]){
        [[DataManager sharedManager].dataExtra removeObjectForKey:@"activationFromLink"];
    }else{
        PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
        [self presentViewController:viewCont animated:YES completion:nil];
    }
}

-(void) getListAccount {
    NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:false];
    
}

- (void)loginDoneState:(NSString *)menuID{
    [self openTemplateByMenuID:menuID];
}

- (void) openTemplateByMenuIDWithLogin: (NSString*)menuID{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
//    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_REQUEST") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            PopupOnboardingViewController *viewCont = [storyboard instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
            [self presentViewController:viewCont animated:YES completion:nil];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        if ([hasLogin isEqualToString:@"NO"] || hasLogin == nil) {
            [self.tabBarController setSelectedIndex:0];
            PopUpLoginViewController *popLogin = [storyboard instantiateViewControllerWithIdentifier:@"PopupLogin"];
            [popLogin setDelegate:self];
            [popLogin setMenuID:menuID];
            [self presentViewController:popLogin animated:YES completion:nil];
        }
        else {
            [self openTemplateByMenuID:menuID];
        }
    }
}

- (void) openTemplate : (NSArray *)temp{
    DataManager *dataManager = [DataManager sharedManager];
    
    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        [dataManager setAction:[objectMain objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        [self pushNavigationTemplate:tempData];
    }else{
        
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        [self presentViewController:alertCont animated:YES completion:nil];
    }
}

- (void) openTemplateByCurrentMenu:(NSString *)menuID{
    DataManager *dataManager = [DataManager sharedManager];
    [dataManager setMenuId:menuID];
    NSArray *temp = [dataManager getJsonDataByMenuID];
    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        [dataManager setAction:[objectMain objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        [self pushNavTemplate:tempData inStoryboard:@"Main"];

    }else{
        
        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        [self presentViewController:alertCont animated:YES completion:nil];
    }
}

- (void) openTemplateByMenuID:(NSString*)menuID{
    
    DataManager *dataManager = [DataManager sharedManager];
    NSArray *temp = [dataManager getJsonDataWithMenuID:menuID andList:dataManager.listMenu];

    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        if([[objectMain objectForKey:@"action"] isKindOfClass:[NSArray class]]){
            NSDictionary *object = [temp objectAtIndex:1];
            NSString *templateName = [object valueForKey:@"template"];

            TemplateViewController *templateView = [self routeTemplateController:templateName];
            [templateView setJsonData:object];
            if([templateName isEqualToString:@"LV01"]){
                LV01ViewController *viewCont = (LV01ViewController *) templateView;
                [viewCont setFirstLV:true];
            }
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        }else{
            [dataManager setAction:[objectMain objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
            dataManager.currentPosition++;
            NSDictionary *tempData = [dataManager getObjectData];
            [self pushNavTemplate:tempData inStoryboard:@"Main"];
        }
    }else{

        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        [self presentViewController:alertCont animated:YES completion:nil];
    }
}

- (void) openTemplateByMenuID:(NSString*)menuID andCodeContent:(NSString*)code{
    DataManager *dataManager = [DataManager sharedManager];
    NSArray *temp = [dataManager getJsonDataWithMenuID:menuID andList:dataManager.listMenu];

    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        NSDictionary *objAction = [objectMain objectForKey:@"action"];
        if([objAction objectForKey:@"content"]){
            NSArray *arrContent = [[objAction objectForKey:@"content"] objectAtIndex:0];
            for(NSDictionary *diCont in arrContent){
                if([[diCont objectForKey:@"code"] isEqualToString:code]){
                    [dataManager setAction:[diCont objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
                }
            }
        }
        
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        [dataManager.dataExtra setValue:code forKey:@"code"];
        [self pushNavTemplate:tempData inStoryboard:@"Main"];
        
    }else{

        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        [self presentViewController:alertCont animated:YES completion:nil];
    }
}

- (void) openTemplateByMenuID:(NSString*)menuID withData:(NSDictionary*)data{
    DataManager *dataManager = [DataManager sharedManager];
    NSArray *temp = [dataManager getJsonDataWithMenuID:menuID andList:dataManager.listMenu];

    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        [dataManager setAction:[objectMain objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        NSString *templateName = [tempData valueForKey:@"template"];
        @try {

            TemplateViewController *templateViews = [self routeTemplateController:[tempData valueForKey:@"template"]];
            [templateViews setJsonData:tempData];
            
            if([[tempData valueForKey:@"template"] isEqualToString:@"PL01"]){
                PL01ViewController *viewCont = (PL01ViewController *) templateViews;
                [viewCont setPassingData:data];
            }
            
            bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
            if (stateAcepted) {
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alerts animated:YES completion:nil];
            }else{
                UITabBarController *tabCont = self.tabBarController;
                UINavigationController *navCont = [tabCont.viewControllers objectAtIndex:0];
                [self.navigationController popToRootViewControllerAnimated:YES];
                if(self.parentViewController.tabBarController.selectedIndex != 0){
                    [self.parentViewController.tabBarController setSelectedIndex:0];
                }
                [navCont pushViewController:templateViews animated:YES];
            }
        } @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
        }
    }else{

        UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        [self presentViewController:alertCont animated:YES completion:nil];
    }
}

-(void)pushNavigationTemplate : (NSDictionary *) tempData{
    @try {
        NSString *templateName = [tempData valueForKey:@"template"];

        TemplateViewController *templateViews = [self routeTemplateController:templateName];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: templateName];
        if (stateAcepted) {
            
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];

        }else{
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateViews animated:YES];
        }
            
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
}

-(void) showAbout {
    TemplateViewController *about = [self routeTemplateController:@"ABOUTVC"];
    [self presentViewController:about animated:YES completion:nil];
}

- (void) actionOkeAbout : (UIView*) viewBg {
    [viewBgAbout removeFromSuperview];
}

@end
