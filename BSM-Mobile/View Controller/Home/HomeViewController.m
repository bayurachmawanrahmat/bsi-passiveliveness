//
//  HomeViewController.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "HomeViewController.h"
#import "Constant.h"
#import "TemplateViewController.h"
#import "LV01ViewController.h"
#import "FinancialViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "AppProperties.h"
#import "NSUserdefaultsAes.h"
#import <WebKit/WebKit.h>
//#import "Connection.h"
//
@interface HomeViewController() <UIScrollViewDelegate, UIAlertViewDelegate, WKUIDelegate, WKNavigationDelegate> {//<ConnectionDelegate>{
    NSArray *images;
    UIButton *buttonLogout;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    NSString *cname;
    NSString* filePath;
    NSString* fileName;
    NSString* thisVersion;
    NSString* fileAtPath;
    NSTimer *idleTimer;
    int maxIdle;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *iconInfoAccount;
@property (weak, nonatomic) IBOutlet UIImageView *iconTransfer;
@property (weak, nonatomic) IBOutlet UIImageView *iconPayment;
@property (weak, nonatomic) IBOutlet UIImageView *iconPurchase;
@property (weak, nonatomic) IBOutlet UIImageView *iconQrpay;
@property (weak, nonatomic) IBOutlet UIImageView *iconOpenAccount;
@property (weak, nonatomic) IBOutlet UIImageView *iconAtm;
@property (weak, nonatomic) IBOutlet UIImageView *iconFavorite;
@property (weak, nonatomic) IBOutlet WKWebView *viewContiner;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlace;
@property (weak, nonatomic) IBOutlet UIImageView *iconZiswaf;
@property (weak, nonatomic) IBOutlet UIView *vwBanner;
@property (weak, nonatomic) IBOutlet UIView *vwIcon;

@end

@implementation HomeViewController


BOOL alertWasShown = NO;

- (void)checkActivation {
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"en"]){
            valTitle = @"Information";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            valCancel = @"No";
            valAgree = @"Ok";
        } else {
            valTitle = @"Informasi";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            valCancel = @"Tidak";
            valAgree = @"Ok";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:valTitle
                              message:valMsg
                              delegate:self
                              cancelButtonTitle:valCancel
                              otherButtonTitles:valAgree, nil];
        
        alert.tag = 229;
        [alert show];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[self checkActivation];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    //NSString *langs = (NSString *)[[NSLocale preferredLanguages] objectAtIndex:0];
    
    CGFloat widthScreen = [UIScreen mainScreen].bounds.size.width;
    self.width.constant = widthScreen;
    self.height.constant = widthScreen * 0.7;
    
    if(!IPHONE_5){
        [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    }
    
    [_viewContiner setUserInteractionEnabled:true];
    [_viewContiner setUIDelegate:self];
    [_viewContiner setNavigationDelegate:self];
    [_viewContiner.scrollView setScrollEnabled:NO];
    [_viewContiner.scrollView setBounces:NO];
    
    NSString *urlString = SLIDER_URL;
    NSURL *url = [NSURL URLWithString:urlString];
    //NSString *versionText = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *versionText = @VERSION_NAME;
    
    // Check Banner Version
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    thisVersion = @"0";
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        valAgree = @"Lanjut";
        valCancel = @"Batal";
        valTitle = @"Konfirmasi";
        valMsg = @"Apakah anda yakin untuk keluar dari aplikasi ?";
    } else {
        valAgree = @"Next";
        valCancel = @"Cancel";
        valTitle = @"Confirmation";
        valMsg = @"Do you want to quit the application ?";
    }
    fileName = @"version_banner_ios.txt";
    fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fileAtPath];
    
    if (fileExists) {
        thisVersion = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
        NSLog(@"readStringFromFile: %@", thisVersion);
    }
    else {
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        [[thisVersion dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
    //NSURL *TheUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@version_banner_ios.html",API_URL]];
    NSURL *TheUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@version_banner_ios.html?vn=%@",API_URL,versionText]];
    NSString *response = [NSString stringWithContentsOfURL:TheUrl
                                                  encoding:NSASCIIStringEncoding
                                                     error:nil];
    
    // Setting cache
    NSMutableURLRequest *urlRequest;
    if (response != nil) {
        if (![response isEqualToString:@""]) {
            response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([response isEqualToString:thisVersion]) {
                urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
            }
            else {
                urlRequest = [NSMutableURLRequest requestWithURL:url];
            }
        }
        else {
            urlRequest = [NSMutableURLRequest requestWithURL:url];
        }
    }
    else {
        urlRequest = [NSMutableURLRequest requestWithURL:url];
    }
    
    //NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    //NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:10.0];
    [self.viewContiner loadRequest:urlRequest];
    // Do any additional setup after loading the view, typically from a nib.
    
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString * coba = [userDefault objectForKey:KEY_HAS_SET_PIN];
    NSString * abc = [userDefault objectForKey:KEY_SESSION_ID];
    NSString * def = [userDefault objectForKey:BSM_SECRET_KEY];
    
    if([lang isEqualToString:@"en"]){
        _iconInfoAccount.image=[UIImage imageNamed:@"icon_info_account_en.png"];
        _iconPayment.image=[UIImage imageNamed:@"icon_payment_en.png"];
        _iconPurchase.image=[UIImage imageNamed:@"icon_purchase_en.png"];
        _iconOpenAccount.image=[UIImage imageNamed:@"icon_open_account_en.png"];
        _iconAtm.image=[UIImage imageNamed:@"icon_branch_atm_en.png"];
        _iconFavorite.image=[UIImage imageNamed:@"icon_favorite_en.png"];
    }
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)showGreeting {
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    cname = [userDefault objectForKey:@"customer_name"];
    
    if (IPHONE_5) {
        UIView *viewGreeting = [[UIView alloc]initWithFrame:CGRectMake(0, 80, screenWidth, 40)];
        viewGreeting.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblGreeting = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, screenWidth, 40)];
        [lblGreeting setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        lblGreeting.textColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
        if(cname == nil){
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum"];
        }else{
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum, %@",cname];
        }
        
        lblGreeting.textAlignment = NSTextAlignmentLeft;
        lblGreeting.numberOfLines = 2;
        
        [viewGreeting addSubview:lblGreeting];
        [self.view addSubview:viewGreeting];
        
        CGRect frmGreeting = lblGreeting.frame;
        CGRect frmVC = self.viewContiner.frame;
        CGRect frmImgPlace = self.imgPlace.frame;
        CGRect frmBoxIcon = self.vwIcon.frame;
//        frmVC.origin.y = frmVC.origin.y + 20;
        frmVC.origin.y = frmGreeting.origin.y + frmGreeting.size.height;
        frmVC.size.height = frmVC.size.height - 10;
        frmImgPlace.size.height = frmVC.size.height;
        frmImgPlace.origin.y = frmVC.origin.y;
        frmBoxIcon.origin.y = frmVC.origin.y + frmVC.size.height;
        //frmVC.origin.y = viewGreeting.frame.origin.y + (_vwIcon.frame.origin.y - viewGreeting.frame.origin.y)/2 - frmVC.size.height/2;
        self.viewContiner.frame = frmVC;
        self.imgPlace.frame = frmImgPlace;
        self.vwIcon.frame = frmBoxIcon;
    } else if (IPHONE_8) {
        UIView *viewGreeting = [[UIView alloc]initWithFrame:CGRectMake(0, 80, screenWidth, 60)];
        viewGreeting.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblGreeting = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, screenWidth, 60)];
        [lblGreeting setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        lblGreeting.textColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
        if(cname == nil){
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum"];
        }else{
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum, %@",cname];
        }
        lblGreeting.textAlignment = NSTextAlignmentLeft;
        lblGreeting.numberOfLines = 2;
        
        [viewGreeting addSubview:lblGreeting];
        [self.view addSubview:viewGreeting];
        
        CGRect frmGreeting = lblGreeting.frame;
        CGRect frmVC = self.viewContiner.frame;
        CGRect frmImgPlace = self.imgPlace.frame;
        CGRect frmBoxIcon = self.vwIcon.frame;
        //CGRect frmVwBanner = self.vwBanner.frame;
//        frmVC.origin.y = frmVC.origin.y + 30;
        frmVC.origin.y = frmGreeting.origin.y + frmGreeting.size.height;
        frmImgPlace.size.height = frmVC.size.height;
        //frmVwBanner.size.height = frmVC.size.height;
        frmImgPlace.origin.y = frmVC.origin.y;
        //frmVwBanner.origin.y = frmVC.origin.y;
        frmBoxIcon.origin.y = frmVC.origin.y + frmVC.size.height;
        //frmVC.size.height = frmVC.size.height - 10;
        //frmVC.origin.y = viewGreeting.frame.origin.y + (_vwIcon.frame.origin.y - viewGreeting.frame.origin.y)/2 - frmVC.size.height/2;
        self.viewContiner.frame = frmVC;
        self.imgPlace.frame = frmImgPlace;
        self.vwIcon.frame = frmBoxIcon;
        //self.vwBanner.frame = frmVwBanner;
    } else if (IPHONE_8P) {
        UIView *viewGreeting = [[UIView alloc]initWithFrame:CGRectMake(0, 80, screenWidth, 60)];
        viewGreeting.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblGreeting = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, screenWidth, 60)];
        [lblGreeting setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        lblGreeting.textColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
        if(cname == nil){
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum"];
        }else{
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum, %@",cname];
        }
        lblGreeting.textAlignment = NSTextAlignmentLeft;
        lblGreeting.numberOfLines = 2;
        
        [viewGreeting addSubview:lblGreeting];
        [self.view addSubview:viewGreeting];
        
        CGRect frmGreeting = lblGreeting.frame;
        CGRect frmVC = self.viewContiner.frame;
        CGRect frmImgPlace = self.imgPlace.frame;
        CGRect frmBoxIcon = self.vwIcon.frame;
//        frmVC.origin.y = frmVC.origin.y + 30;
        frmVC.origin.y = frmGreeting.origin.y + frmGreeting.size.height;
        frmImgPlace.size.height = frmVC.size.height;
        frmImgPlace.origin.y = frmVC.origin.y;
        frmBoxIcon.origin.y = frmVC.origin.y + frmVC.size.height;
        
        //frmVC.size.height = frmVC.size.height - 10;
        //frmVC.origin.y = viewGreeting.frame.origin.y + (_vwIcon.frame.origin.y - viewGreeting.frame.origin.y)/2 - frmVC.size.height/2;
        self.viewContiner.frame = frmVC;
        self.imgPlace.frame = frmImgPlace;
        self.vwIcon.frame = frmBoxIcon;
    } else if (IPHONE_X) {
        UIView *viewGreeting = [[UIView alloc]initWithFrame:CGRectMake(0, 80, screenWidth, 60)];
        viewGreeting.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblGreeting = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, screenWidth, 60)];
        [lblGreeting setFont:[UIFont fontWithName:@"Helvetica" size:15.0f]];
        lblGreeting.textColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
        if(cname == nil){
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum"];
        }else{
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum, %@",cname];
        }
        lblGreeting.textAlignment = NSTextAlignmentLeft;
        lblGreeting.numberOfLines = 2;
        
        [viewGreeting addSubview:lblGreeting];
        [self.view addSubview:viewGreeting];
        
        CGRect frmGreeting = lblGreeting.frame;
        CGRect frmVC = self.viewContiner.frame;
        CGRect frmImgPlace = self.imgPlace.frame;
        CGRect frmBoxIcon = self.vwIcon.frame;
        //frmVC.origin.y = frmVC.origin.y - 30;
//        frmVC.origin.y = frmVC.origin.y + 10;
        frmVC.origin.y = frmGreeting.origin.y + frmGreeting.size.height + 10;
        frmImgPlace.size.height = frmVC.size.height;
        frmImgPlace.origin.y = frmVC.origin.y;
        frmBoxIcon.origin.y = frmVC.origin.y + frmVC.size.height;
        //frmVC.origin.y = viewGreeting.frame.origin.y + (_vwIcon.frame.origin.y - viewGreeting.frame.origin.y)/2 - frmVC.size.height/2;
        self.viewContiner.frame = frmVC;
        self.imgPlace.frame = frmImgPlace;
        self.vwIcon.frame = frmBoxIcon;
    } else {
        UIView *viewGreeting = [[UIView alloc]initWithFrame:CGRectMake(0, 80, screenWidth, 60)];
        viewGreeting.backgroundColor = [UIColor whiteColor];
        
        UILabel *lblGreeting = [[UILabel alloc]initWithFrame:CGRectMake(12, 0, screenWidth, 60)];
        [lblGreeting setFont:[UIFont fontWithName:@"Helvetica" size:15.0f]];
        lblGreeting.textColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
        if(cname == nil){
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum"];
        }else{
            lblGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum, %@",cname];
        }
        lblGreeting.textAlignment = NSTextAlignmentLeft;
        lblGreeting.numberOfLines = 2;
        
        [viewGreeting addSubview:lblGreeting];
        [self.view addSubview:viewGreeting];
        
        CGRect frmGreeting = lblGreeting.frame;
        CGRect frmVC = self.viewContiner.frame;
        CGRect frmImgPlace = self.imgPlace.frame;
        CGRect frmBoxIcon = self.vwIcon.frame;
//        CGRect frmVwBanner = self.vwBanner.frame;
//        frmVC.origin.y = frmVC.origin.y - 30;
        frmVC.origin.y = frmGreeting.origin.y + frmGreeting.size.height;
        frmImgPlace.size.height = frmVC.size.height;
//        frmVwBanner.size.height = frmVC.size.height;
        frmImgPlace.origin.y = frmVC.origin.y;
//        frmVwBanner.origin.y = frmVC.origin.y;
        frmBoxIcon.origin.y = frmVC.origin.y + frmVC.size.height;
        //frmVC.origin.y = viewGreeting.frame.origin.y + (_vwIcon.frame.origin.y - viewGreeting.frame.origin.y)/2 - frmVC.size.height/2;
        self.viewContiner.frame = frmVC;
        self.imgPlace.frame = frmImgPlace;
        self.vwIcon.frame = frmBoxIcon;
//        self.vwBanner.frame = frmVwBanner;
    }
    
}

- (void)showButtonLogout {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    
    int posX = 0;
    if (IPHONE_X) {
        posX = screenWidth-60;
    } else {
        posX = screenWidth-40;
    }
    
    buttonLogout = [[UIButton alloc]initWithFrame:CGRectMake(posX, 37, 30, 30)];
    [buttonLogout setImage:[UIImage imageNamed:@"power-button-off"] forState:UIControlStateNormal];
    [buttonLogout addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    //buttonLogout.tag = 14045;
    [self.view addSubview:buttonLogout];
    [buttonLogout bringSubviewToFront:self.view];
    
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
        [buttonLogout setHidden:YES];
    } else {
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"YES"]) {
                [buttonLogout setHidden:NO];
            } else {
                [buttonLogout setHidden:YES];
            }
        } else {
            [buttonLogout setHidden:YES];
        }
    }
}

- (void)logout {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:valTitle
                          message:valMsg
                          delegate:nil
                          cancelButtonTitle:valCancel
                          otherButtonTitles:valAgree, nil];
    alert.delegate = self;
    alert.tag = 12345;
    [alert show];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *requestPath = [[navigationAction.request URL] absoluteString];
    
    if ([requestPath isEqualToString:SLIDER_URL] || [requestPath isEqualToString:[NSString stringWithFormat:@"%@#",SLIDER_URL]]) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:requestPath]];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (!self.imgPlace.hidden) {
        self.imgPlace.hidden = true;
    }
    
    if (alertWasShown == NO) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        [[thisVersion dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];

    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        [self checkActivation];
    } else {
        //[self showGreeting];
        //[self startIdleTimer];
    }
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    /*if (alertWasShown == NO) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil]; // Create an alert.
        [alert show];
        
        alertWasShown = YES;
    }*/
}

- (void)viewDidAppear:(BOOL)animated{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];

    //[self showButtonLogout];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    if ((cid == nil) || ([cid isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
    } else {
//        NSString *upwd = [userDefault objectForKey:@"password"];
        NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
        if ((upwd == nil) || ([upwd isEqualToString:@""])) {
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NCreatePwdVC"];
            templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else {
            if ([mustLogin isEqualToString:@"YES"]) {
                if (![hasLogin isEqualToString:@"YES"]) {
                    self.parentViewController.tabBarController.tabBar.hidden = YES;
                    [self.slidingViewController resetTopViewAnimated:YES];
                    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
                    templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                    [currentVC pushViewController:templateView animated:YES];
                }
            }
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
//    CGFloat pageWidth = self.scrollSlider.frame.size.width;
//    NSInteger offsetLooping = 1;
//    int page = floor((self.scrollSlider.contentOffset.x - pageWidth / 2) / pageWidth) + offsetLooping;
//    self.pageControl.currentPage = page % [images count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openTemplateWithData:(int)position{
    NSArray *temp = [[DataManager sharedManager] getJSONData:position];
    NSDictionary *object = [temp objectAtIndex:1];
    
    NSString *templateName = [object valueForKey:@"template"];
    TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:templateName];
    [templateView setJsonData:object];
    if([templateName isEqualToString:@"LV01"]){
        LV01ViewController *viewCont = (LV01ViewController *) templateView;
        [viewCont setFirstLV:true];
    }
    [self.navigationController pushViewController:templateView animated:YES];
}

- (void)goToSomepage:(int)position{
    NSArray *temp = [[DataManager sharedManager] getJSONData:position];
    NSDictionary *object = [temp objectAtIndex:1];
    
    NSString *templateName = [object valueForKey:@"template"];
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:templateName];
    [templateView setJsonData:object];
    //    if([templateName isEqualToString:@"LV01"]){
    LV01ViewController *viewCont = (LV01ViewController *) templateView;
    [viewCont setFromHome:YES];
    //    }
    [self.navigationController pushViewController:templateView animated:YES];
}

- (void)openFavorite:(int)node{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *fd = [userDefaults objectForKey:@"favD"];

//    if([userDefaults objectForKey:@"customer_id"]){
    if([NSUserdefaultsAes getValueForKey:@"customer_id"]){
        DataManager *dataManager = [DataManager sharedManager];
        NSArray *temp = [dataManager getJSONByCurrentMenu];
        NSDictionary *object = [temp objectAtIndex:1];
        NSLog(@"fav value: %@",  [dataManager.dataExtra objectForKey:@"payment_id"]);
        NSDictionary *ad = [userDefaults objectForKey:@"actD"];

        if ([[fd objectForKey:@"submenu"] isEqualToString:@"true"]) {
            NSDictionary *actChild = [ad objectForKey:@"actChild"];
            //[dataManager setAction:actChild andMenuId:[fd objectForKey:@"code"]];
            [dataManager setAction:actChild andMenuId:nil];
        }
        else {
            [dataManager setAction:[object objectForKey:@"action"] andMenuId:nil];
        }
        
        [dataManager setCurrentPosition:node];
        NSDictionary *nextNode = [dataManager getObjectData];
        
        NSString *templateName = [nextNode valueForKey:@"template"];
        TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:templateName];
        [templateView setJsonData:nextNode];
        [self.navigationController pushViewController:templateView animated:YES];
    }else{
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        NSString *alertTitle = @"";
        NSString *alertMessage = @"";
        if ([lang isEqualToString:@"en"]) {
            alertTitle = @"Information";
            alertMessage = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
        } else {
            alertTitle = @"Informasi";
            alertMessage = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)openInfoRek:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_info_account_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:0];
}
- (IBAction)openPayment:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_payment_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:1];
}
- (IBAction)openPurchase:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_purchase_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:2];
}
- (IBAction)openTransfer:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_transfer_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:3];
}

- (IBAction)openOpeningAccount:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_open_account_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:4];
}

- (IBAction)openZiswaf:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_header_berbagi.png" forKey:@"imgIcon"];
    [self openTemplateWithData:6];
}

- (IBAction)qrPayment:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSArray *tempMain = [[DataManager sharedManager] getJSONData:3];
    NSDictionary *objectMain = [tempMain objectAtIndex:1];
    int ind = 0;
    NSArray *menu = [objectMain valueForKey:@"action"];
    for (int i =0; i < [menu count]; i++) {
        NSArray *selMenu = [[objectMain valueForKey:@"action"] objectAtIndex:i];
        if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
            ind = i;
            break;
        }
    }
    NSArray *temp = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
    NSDictionary *object = [temp objectAtIndex:1];
    // NSString *templateNames = [object valueForKey:@"template"];
    DataManager *dataManager = [DataManager sharedManager];
    [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
    dataManager.currentPosition++;
    NSDictionary *tempData = [dataManager getObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
     [userDefault setObject:@"ic_qrpay_header.png" forKey:@"imgIcon"];
    if([[tempData valueForKey:@"activation"]boolValue]){
//        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
        if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            NSString *alertTitle = @"";
            NSString *alertMessage = @"";
            if ([lang isEqualToString:@"en"]) {
                alertTitle = @"Information";
                alertMessage = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            } else {
                alertTitle = @"Informasi";
                alertMessage = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            }
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
            [templateViews setJsonData:tempData];
            [self.navigationController pushViewController:templateViews animated:YES];
        }
    } else {
        TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
        [templateViews setJsonData:tempData];
        [self.navigationController pushViewController:templateViews animated:YES];
    }
}

- (IBAction)contactBSM:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://14040"]];
}

- (IBAction)openFinancial:(id)sender {
    //  [[DataManager sharedManager]resetObjectData];
    //  [self performSegueWithIdentifier:@"FinancialVC" sender:self];
    
    FinancialViewController *VCB = [[FinancialViewController alloc]init];
    [self.navigationController pushViewController:VCB animated:YES];
    
    // FinancialViewController *frm = [[FinancialViewController alloc] initWithNibName:@"FinancialViewController" bundle:nil];
    //  [self.navigationController pushViewController:frm animated:YES];
}
- (IBAction)openShalat:(id)sender {
//    [[DataManager sharedManager]resetObjectData];
//    [self performSegueWithIdentifier:@"ShalatVC" sender:self];
}

- (IBAction)openTausiah:(id)sender {
//    [[DataManager sharedManager]resetObjectData];
//    [self performSegueWithIdentifier:@"TausiahVC" sender:self];
}

- (IBAction)openLokasiATM:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_branch_atm_header.png" forKey:@"imgIcon"];
    [self performSegueWithIdentifier:@"LocationVC" sender:self];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 229) {
        //UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
        //self.slidingViewController.topViewController = viewCont;
        
        //Check activate or not
        if (buttonIndex == 0) {
            //Back to Home
            //NSDictionary* userInfo = @{@"position": @(313)};
            //NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            //[nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        } else if (buttonIndex == 1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"NO" forKey:@"fromSliding"];
            [userDefault synchronize];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
            
            /*NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"id"]){
                valTitle = @"Konfirmasi";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Batal";
                valAgree = @"Lanjut";
            } else {
                valTitle = @"Confirmation";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Cancel";
                valAgree = @"Next";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            
            alert.tag = 230;
            [alert show];*/
        }
    } else if (alertView.tag == 230) {
        //Check register or not
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 231;
            [alert show];
        } else if (buttonIndex == 1) {
            //Back to Home
            //NSDictionary* userInfo = @{@"position": @(313)};
            //NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            //[nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
        }
    } else if (alertView.tag == 231) {
        //Back to Home
        //NSDictionary* userInfo = @{@"position": @(313)};
        //NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        //[nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    } else if (alertView.tag == 12345) {
        if (buttonIndex == 0) {
            //CANCEL Button
        } else {
            //[self stopIdleTimer];
            
            //OK Button
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
            [userDefault setObject:@"NO" forKey:@"hasLogin"];
            [userDefault synchronize];
            
            if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
                CGRect screenBound = [[UIScreen mainScreen] bounds];
                CGSize screenSize = screenBound.size;
                CGFloat screenWidth = screenSize.width;
                CGFloat screenHeight = screenSize.height;
                
                self.parentViewController.tabBarController.tabBar.hidden = YES;
                [self.slidingViewController resetTopViewAnimated:YES];
                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
                templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateView animated:YES];
            } else {
                exit(0);
            }
        }
    } else if (alertView.tag == 505) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault synchronize];
        
        if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
            CGRect screenBound = [[UIScreen mainScreen] bounds];
            CGSize screenSize = screenBound.size;
            CGFloat screenWidth = screenSize.width;
            CGFloat screenHeight = screenSize.height;
            
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
            templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else {
            exit(0);
        }
    }
}


@end

