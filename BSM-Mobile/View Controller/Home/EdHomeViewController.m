//
//  EdHomeViewController.m
//  BSM-Mobile
//
//  Created by Macbook Air on 28/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "EdHomeViewController.h"
#import "Constant.h"
#import "TemplateViewController.h"
#import "LV01ViewController.h"
#import "FinancialViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Utility.h"
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "OnboardingTemplate01ViewController.h"
#import "TabunganBerhasilController.h"
#import "GagalVerifikasiController.h"
#import "PembukaanAkunAwalController.h"
#import "PopupActivationViewController.h"
#import "PopupOnboardingViewController.h"
#import "AppProperties.h"
#import "NSUserdefaultsAes.h"
#import <WebKit/WebKit.h>

@interface EdHomeViewController ()<UIScrollViewDelegate, UIAlertViewDelegate, WKUIDelegate, WKNavigationDelegate>{
    NSArray *images;
    UIButton *buttonLogout;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    NSString *cname;
    NSString* filePath;
    NSString* fileName;
    NSString* thisVersion;
    NSString* fileAtPath;
    NSTimer *idleTimer;
    int maxIdle;
    NSUserDefaults *userDefault;
    
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *iconInfoAccount;
@property (weak, nonatomic) IBOutlet UIImageView *iconTransfer;
@property (weak, nonatomic) IBOutlet UIImageView *iconPayment;
@property (weak, nonatomic) IBOutlet UIImageView *iconPurchase;
@property (weak, nonatomic) IBOutlet UIImageView *iconQrpay;
@property (weak, nonatomic) IBOutlet UIImageView *iconZiswaf;
@property (weak, nonatomic) IBOutlet UIImageView *iconOpenAccount;
@property (weak, nonatomic) IBOutlet UIImageView *iconFavorite;

@property (weak, nonatomic) IBOutlet WKWebView *viewContiner;
@property (weak, nonatomic) IBOutlet UIImageView *imgPlace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@property (weak, nonatomic) IBOutlet UIView *vwBanner;
@property (weak, nonatomic) IBOutlet UIView *vwIcon;
@property (weak, nonatomic) IBOutlet UIView *vwGreeting;
@property (weak, nonatomic) IBOutlet UILabel *lbGreeting;

- (IBAction)opnFavorite:(id)sender;


@end


@implementation EdHomeViewController

BOOL nAlertWasShown = NO;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    
    [userDefault setObject:@"HOME" forKey:@"VC"];
    [userDefault synchronize];
    
    CGFloat widthScreen = [UIScreen mainScreen].bounds.size.width;
    self.width.constant = widthScreen;
    self.height.constant = widthScreen * 0.7;
    
    if(!IPHONE_5){
        [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    }
    
    [_viewContiner setUserInteractionEnabled:true];
    [_viewContiner setUIDelegate:self];
    [_viewContiner setNavigationDelegate:self];
    [_viewContiner.scrollView setScrollEnabled:NO];
    [_viewContiner.scrollView setBounces:NO];
    
    // Check Banner Version
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    thisVersion = @"0";
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        valAgree = @"Lanjut";
        valCancel = @"Batal";
        valTitle = @"Konfirmasi";
        valMsg = @"Apakah anda yakin untuk keluar dari aplikasi ?";
    } else {
        valAgree = @"Next";
        valCancel = @"Cancel";
        valTitle = @"Confirmation";
        valMsg = @"Do you want to quit the application ?";
    }
    fileName = @"version_banner_ios.txt";
    fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fileAtPath];
    
    if (fileExists) {
        thisVersion = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:fileAtPath] encoding:NSUTF8StringEncoding];
        NSLog(@"readStringFromFile: %@", thisVersion);
    }
    else {
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        [[thisVersion dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
    
    
//    if([lang isEqualToString:@"en"]){
//        _iconInfoAccount.image=[UIImage imageNamed:@"icon_info_account_en.png"];
//        _iconPayment.image=[UIImage imageNamed:@"icon_payment_en.png"];
//        _iconPurchase.image=[UIImage imageNamed:@"icon_purchase_en.png"];
//        _iconOpenAccount.image=[UIImage imageNamed:@"icon_open_account_en.png"];
//        //_iconAtm.image=[UIImage imageNamed:@"icon_branch_atm_en.png"];
//        _iconFavorite.image=[UIImage imageNamed:@"icon_favorite_en.png"];
//    }
    
    [self changeIconImgMenu];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    [self showGreeting];
    
    [self loadBannerView];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    // Adding the swipe gesture on WebView
    [self.viewContiner addGestureRecognizer:swipeLeft];
    [self.viewContiner addGestureRecognizer:swipeRight];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    [self setShowMenu:@"HOME"];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    
}

-(void) loadBannerView{
    NSString *urlString = SLIDER_URL;
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *versionText = @VERSION_NAME;
    
    NSURL *TheUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@version_banner_ios.html?vn=%@",API_URL,versionText]];
    NSString *response = [NSString stringWithContentsOfURL:TheUrl
                                                  encoding:NSASCIIStringEncoding
                                                     error:nil];
    
    // Setting cache
    NSMutableURLRequest *urlRequest;
    if (response != nil) {
        if (![response isEqualToString:@""]) {
            response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([response isEqualToString:thisVersion]) {
                urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
            }
            else {
                urlRequest = [NSMutableURLRequest requestWithURL:url];
            }
        }
        else {
            urlRequest = [NSMutableURLRequest requestWithURL:url];
        }
    }
    else {
        urlRequest = [NSMutableURLRequest requestWithURL:url];
    }
    
    [self.viewContiner loadRequest:urlRequest];
}

- (void)viewWillAppear:(BOOL)animated {
//
    
    [super viewWillAppear:animated];
    [self changeIconImgMenu];
    
    [self callResetTimer];
    
    //checkinbox
    
    
}

-(void)changeIconImgMenu{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if ([lang isEqualToString:@"en"]) {
        _iconInfoAccount.image=[UIImage imageNamed:@"account_info.png"];
        _iconPayment.image=[UIImage imageNamed:@"payment.png"];
        _iconPurchase.image=[UIImage imageNamed:@"purchase.png"];
//        _iconOpenAccount.image=[UIImage imageNamed:@"icon_open_account_en.png"];
        _iconOpenAccount.image=[UIImage imageNamed:@"cash_withdrawal.png"];
        _iconFavorite.image=[UIImage imageNamed:@"favorite.png"];
        _iconQrpay.image=[UIImage imageNamed:@"icon_qrpay2.png"];
        _iconZiswaf.image=[UIImage imageNamed:@"icon_ziswaf_en.png"];
        _iconTransfer.image=[UIImage imageNamed:@"icon_transfer2.png"];
        
    }else{
        _iconInfoAccount.image=[UIImage imageNamed:@"icon_info_account2.png"];
        _iconPayment.image=[UIImage imageNamed:@"icon_payment2.png"];
        _iconPurchase.image=[UIImage imageNamed:@"icon_purchase2.png"];
        _iconOpenAccount.image=[UIImage imageNamed:@"icon_tarik_tunai.png"];
//        _iconOpenAccount.image=[UIImage imageNamed:@"icon_tarik_tunai.png"];
        _iconFavorite.image=[UIImage imageNamed:@"icon_favorite2.png"];
        _iconQrpay.image=[UIImage imageNamed:@"icon_qrpay2.png"];
        _iconZiswaf.image=[UIImage imageNamed:@"icon_ziswaf_id.png"];
        _iconTransfer.image=[UIImage imageNamed:@"icon_transfer2.png"];
    }
    
}



- (void)viewWillLayoutSubviews{
    //[self loadBannerView];
    //[self callResetTimer];
}


- (void)checkActivation {
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        NSString *strHasLogin = [userDefault valueForKey:@"hasLogin"];
        
        if (strHasLogin == nil || [strHasLogin isEqualToString:@"NO"]) {
            if([lang isEqualToString:@"en"]){
                valTitle = @"Information";
                //valMsg = lang(@"ACTIVATION_CONFIRM");
                valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
                valCancel = @"No";
                valAgree = @"Ok";
            } else {
                valTitle = @"Informasi";
                //valMsg = lang(@"ACTIVATION_CONFIRM");
                valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
                valCancel = @"Tidak";
                valAgree = @"Ok";
            }
            
            //        UIAlertView *alert = [[UIAlertView alloc]
            //                              initWithTitle:valTitle
            //                              message:valMsg
            //                              delegate:self
            //                              cancelButtonTitle:valCancel
            //                              otherButtonTitles:valAgree, nil];
            //        alert.tag = 229;
            //        [alert show];
            NSDictionary* userInfo = @{@"position": @(1119)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        }
    }
}


- (void)setPresentationStyleForSelfController:(UIViewController *)selfController presentingController:(UIViewController *)presentingController
{
    if (@available(iOS 8, *))
    {
        presentingController.providesPresentationContextTransitionStyle = YES;
        presentingController.definesPresentationContext = YES;
        
        [presentingController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [selfController.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
    else
    {
        [selfController setModalPresentationStyle:UIModalPresentationCurrentContext];
        [selfController.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self callResetTimer];
}

- (void)showGreeting {
    
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmVwGreeting = self.vwGreeting.frame;
    CGRect frmVwBanner = self.vwBanner.frame;
    CGRect frmVwIcon = self.vwIcon.frame;
    
    CGRect frmImgPlace = self.imgPlace.frame;
    CGRect frmWebView = self.viewContiner.frame;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    cname = [userDefault objectForKey:@"customer_name"];
    if(cname == nil){
        _lbGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum"];
    }else{
        _lbGreeting.text = [NSString stringWithFormat:@"Assalamu'alaikum, %@",cname];
    }
    self.lbGreeting.textColor = [UIColor colorWithRed:0.06 green:0.33 blue:0.24 alpha:1];
    
    _lbGreeting.textAlignment = NSTextAlignmentLeft;
    
    
    frmVwGreeting.origin.y = 85;
    frmVwGreeting.size.width = screenWidth;
    
    /*
     * default
     */
    
    frmVwBanner.origin.y = frmVwGreeting.origin.y + frmVwGreeting.size.height;
    frmVwBanner.size.width = screenWidth;
    
    //size height banner from android
    CGFloat heightBanner = (40*100/60) * frmVwBanner.size.width / 100;
    
//    frmVwBanner.size.height = screenHeight - frmVwGreeting.origin.y - frmVwGreeting.size.height - frmVwIcon.size.height - 20;
    frmVwBanner.size.height = heightBanner;

    
    
    if (IPHONE_5) {
        [self.lbGreeting setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        //changing layer if resolutions is change
        CGFloat heightBanner = frmVwBanner.origin.y + frmVwBanner.size.height;
        if (heightBanner > frmVwIcon.origin.y) {
            frmVwBanner.size.height = screenHeight - frmVwGreeting.origin.y - frmVwGreeting.size.height - frmVwIcon.size.height - 40;
            
        }
        
    }else if(IPHONE_8 || IPHONE_8P){
         [self.lbGreeting setFont:[UIFont fontWithName:@"Helvetica" size:13.0f]];
        CGFloat heightBanner = frmVwBanner.origin.y + frmVwBanner.size.height;
        if (heightBanner > frmVwIcon.origin.y) {
        frmVwBanner.size.height = screenHeight - frmVwGreeting.origin.y - frmVwGreeting.size.height - frmVwIcon.size.height - 60;
        
        }
    }
    else{
        [self.lbGreeting setFont:[UIFont fontWithName:@"Helvetica" size:15.0f]];
        
//        frmVwBanner.origin.y = frmVwGreeting.origin.y + frmVwGreeting.size.height + 30;
//
//        CGFloat heightBanner = frmVwBanner.origin.y + frmVwBanner.size.height;
//        if (heightBanner > frmVwIcon.origin.y) {
//            frmVwBanner.size.height = screenHeight - frmVwBanner.origin.y - frmVwIcon.size.height - (frmVwIcon.size.height/2);
//        }
         CGFloat postBanerY = (SCREEN_HEIGHT - frmVwIcon.size.height - (frmVwGreeting.origin.y + frmVwGreeting.size.height))/2 - frmVwBanner.size.height/2 - BOTTOM_NAV;
        frmVwBanner.origin.y = frmVwGreeting.origin.y + frmVwGreeting.size.height + postBanerY;
        
    }
    
    frmImgPlace.size.height = frmVwBanner.size.height;
    frmImgPlace.size.width = frmVwBanner.size.width;
    
    frmWebView.size.height = frmVwBanner.size.height;
    frmWebView.size.width = frmVwBanner.size.width;
    
    self.vwGreeting.frame = frmVwGreeting;
    self.vwBanner.frame  = frmVwBanner;
    self.imgPlace.frame = frmImgPlace;
    self.viewContiner.frame = frmWebView;
}

- (void)showButtonLogout {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    int posX = 0;
    if (IPHONE_X) {
        posX = screenWidth-60;
    } else {
        posX = screenWidth-40;
    }
    
    buttonLogout = [[UIButton alloc]initWithFrame:CGRectMake(posX, 37, 30, 30)];
    [buttonLogout setImage:[UIImage imageNamed:@"power-button-off"] forState:UIControlStateNormal];
    [buttonLogout addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    //buttonLogout.tag = 14045;
    [self.view addSubview:buttonLogout];
    [buttonLogout bringSubviewToFront:self.view];
    
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
        [buttonLogout setHidden:YES];
    } else {
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"YES"]) {
                [buttonLogout setHidden:NO];
            } else {
                [buttonLogout setHidden:YES];
            }
        } else {
            [buttonLogout setHidden:YES];
        }
    }
}

- (void)logout {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:valTitle
                          message:valMsg
                          delegate:nil
                          cancelButtonTitle:valCancel
                          otherButtonTitles:valAgree, nil];
    alert.delegate = self;
    alert.tag = 12345;
    [alert show];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *requestPath = [[navigationAction.request URL] absoluteString];
    
    if ([requestPath isEqualToString:SLIDER_URL] || [requestPath isEqualToString:[NSString stringWithFormat:@"%@#",SLIDER_URL]]) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:requestPath]];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (!self.imgPlace.hidden) {
        self.imgPlace.hidden = true;
    }
    if (nAlertWasShown == NO) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        [[thisVersion dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
//    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
//        [self checkActivation];
//    }
    
    //check onboarding
    //[CustomerOnboardingData removeForKeychainKey:ONBOARDING_MSMSID_KEY];
    //[CustomerOnboardingData setValue:@"337E302C-898C-4B49-8FB1-F511C68F4AC0" forKey:ONBOARDING_MSMSID_KEY];
    //[[[Encryptor alloc] init] resetFormData];
    
    AppDelegate *mainClass = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        if([userDefault objectForKey:ONBOARDING_NEED_CHECK_KEY])
            [self checkOnboarding];
        else
        {
            [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                mainClass.splashImageView.alpha = .0f;
            } completion:^(BOOL finished){
                if (finished) {
                    [mainClass.splashImageView removeFromSuperview];
                }
            }];
            [self checkActivation];
        }
    } else {
        [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            mainClass.splashImageView.alpha = .0f;
        } completion:^(BOOL finished){
            if (finished) {
                [mainClass.splashImageView removeFromSuperview];
            }
        }];
        [self showGreeting];
    }
    
}
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"%@", [error localizedDescription]);
}

- (void)viewDidAppear:(BOOL)animated{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *mAction = [userDefault objectForKey:@"action"];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    
    
    if (mAction == nil) {
        if ((cid == nil) || ([cid isEqualToString:@""])) {
            NSLog(@"Customer ID belum diaktifkan.");
        } else {
//            NSString *upwd = [userDefault objectForKey:@"password"];
            NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
            if ((upwd == nil) || ([upwd isEqualToString:@""])) {
                self.parentViewController.tabBarController.tabBar.hidden = YES;
                [self.slidingViewController resetTopViewAnimated:YES];
                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NCreatePwdVC"];
                templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateView animated:YES];
            } else {
                if ([mustLogin isEqualToString:@"YES"]) {
                    if (![hasLogin isEqualToString:@"YES"]) {
                        
                        [userDefault setObject:@"LOGIN" forKey:@"VC"];
                        [userDefault synchronize];
                        
                        self.parentViewController.tabBarController.tabBar.hidden = YES;
                        [self.slidingViewController resetTopViewAnimated:YES];
                        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
                        templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                        [currentVC pushViewController:templateView animated:YES];
                    }
                }
            }
        }
    }else{
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager.dataExtra setValue:[userDefault valueForKey:@"mid"] forKey:@"mid"];
        
        self.parentViewController.tabBarController.tabBar.hidden = YES;
        [self.slidingViewController resetTopViewAnimated:YES];
        UIViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
//        templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
        
        //push viewcontroller
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateView animated:YES];
      
    }
    
   
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    [self callResetTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openTemplateWithData:(int)position{
    [self callResetTimer];
    NSArray *temp = [[DataManager sharedManager] getJSONData:position];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        
        NSString *templateName = [object valueForKey:@"template"];
        TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:templateName];
        [templateView setJsonData:object];
        if([templateName isEqualToString:@"LV01"]){
            LV01ViewController *viewCont = (LV01ViewController *) templateView;
            [viewCont setFirstLV:true];
        }
        bool stateAcepted = [Utility vcNotifConnection: templateName];
        if (stateAcepted) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            alert.tag = 404;
            [alert show];
        }else{
            [self.navigationController pushViewController:templateView animated:YES];
            
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"ERROR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //            alert.tag = 404;
        [alert show];
    }
   
}

-(void) goToCardLess{
    [self callResetTimer];
    [self getMenuIdx:@"btnCardless"];
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:3];
    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        int ind = 3;
        if ([userDefault valueForKey:@"btnCardless"]) {
            ind = [[userDefault valueForKey:@"btnCardless"] intValue];
        }
        
//        NSArray *menu = [objectMain valueForKey:@"action"];
//        for (int i =0; i < [menu count]; i++) {
//            NSArray *selMenu = [[objectMain valueForKey:@"action"] objectAtIndex:i];
//            if ([[selMenu objectAtIndex:0] isEqualToString:@"00070"]) {
//                ind = i;
//                break;
//            }
//        }
        NSArray *nTempAction = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [nTempAction objectAtIndex:1];
        
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[nTempAction objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        
       [self pushNavTemplate:tempData];
        
    }else{
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}

- (void)goToSomepage:(int)position{
    [self callResetTimer];
    NSArray *temp = [[DataManager sharedManager] getJSONData:position];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        
        NSString *templateName = [object valueForKey:@"template"];
        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:templateName];
        [templateView setJsonData:object];
        //    if([templateName isEqualToString:@"LV01"]){
        LV01ViewController *viewCont = (LV01ViewController *) templateView;
        [viewCont setFromHome:YES];
        //    }
        bool stateAcepted = [Utility vcNotifConnection: templateName];
        if (stateAcepted) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            //            alert.tag = 404;
            [alert show];
        }else{
            [self.navigationController pushViewController:templateView animated:YES];
            
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //            alert.tag = 404;
        [alert show];
    }
    
}

-(void)goToQR{
    [self callResetTimer];
    [[DataManager sharedManager]resetObjectData];
    NSArray *tempMain = [[DataManager sharedManager] getJSONData:3];
    if (tempMain !=nil) {
        NSDictionary *objectMain = [tempMain objectAtIndex:1];
        int ind = 0;
        NSArray *menu = [objectMain valueForKey:@"action"];
        for (int i =0; i < [menu count]; i++) {
            NSArray *selMenu = [[objectMain valueForKey:@"action"] objectAtIndex:i];
            if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
                ind = i;
                break;
            }
        }
        NSArray *temp = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [temp objectAtIndex:1];
        
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        [self pushNavTemplate:tempData];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)goToInbox{
    [self callResetTimer];
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:5];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition = 0;
        NSDictionary *tempData = [dataManager getObjectData];
        [self pushNavTemplate:tempData];
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
               [alert show];
    }
}


-(void)pushNavTemplate : (NSDictionary *) tempData{
    @try {
        TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateViews animated:YES];
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
}

- (void)openFavorite:(int)node{
    [self callResetTimer];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
//    if([userDefaults objectForKey:@"customer_id"]){
    if([NSUserdefaultsAes getValueForKey:@"customer_id"]){
        DataManager *dataManager = [DataManager sharedManager];
//        NSArray *temp = [dataManager getJSONByCurrentMenu];
        NSLog(@"%@", [userDefaults objectForKey:@"actionFav"]);
        NSDictionary *object = [userDefaults objectForKey:@"actionFav"];
        NSLog(@"fav value: %@",  [dataManager.dataExtra objectForKey:@"payment_id"]);
        
        [dataManager setAction:object andMenuId:nil];
        [dataManager setCurrentPosition:node];
        NSDictionary *nextNode = [dataManager getObjectData];
        
        @try {
            NSString *templateName = [nextNode valueForKey:@"template"];
            TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:templateName];
            [templateView setJsonData:nextNode];
            [self.navigationController pushViewController:templateView animated:YES];
        } @catch (NSException *exception) {
            NSLog(@"Template Not Found");
        }
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(NSDictionary *) getNextNode : (int) node
         jsonObject : (NSDictionary *) data{
   NSMutableDictionary *dataDict = [[NSMutableDictionary alloc]init];
    for(int i =0; i<[[data valueForKey:@"node"]count]; i++){
        if (node == i) {
            [dataDict addEntriesFromDictionary:@{@"content":[[data valueForKey:@"content"]objectAtIndex:i],@"favorite":[[data valueForKey:@"favorite"]objectAtIndex:i],@"social":[[data valueForKey:@"social"]objectAtIndex:i],@"template":[[data valueForKey:@"template"]objectAtIndex:i],@"title":[[data valueForKey:@"title"]objectAtIndex:i],@"url":[[data valueForKey:@"url"]objectAtIndex:i],@"url_parm":[[data valueForKey:@"url_parm"]objectAtIndex:i],@"node":[[data valueForKey:@"node"]objectAtIndex:i],@"activation":[[data valueForKey:@"activation"]objectAtIndex:i],@"field_name":[[data valueForKey:@"field_name"]objectAtIndex:i],@"mandatory":[[data valueForKey:@"mandatory"]objectAtIndex:i]}];
            break;
        }
    }
    return dataDict;
    
}

- (IBAction)openInfoRek:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_info_account_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:0];
}
- (IBAction)openPayment:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_payment_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:1];
}
- (IBAction)openPurchase:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_purchase_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:2];
}
- (IBAction)openTransfer:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_transfer_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:3];
}

- (IBAction)openOpeningAccount:(id)sender {
//    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_tarik_tunai_header.png" forKey:@"imgIcon"];
    [userDefault synchronize];
//    [self openTemplateWithData:4];
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    [self goToCardLess];
}

- (IBAction)openZiswaf:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_header_berbagi.png" forKey:@"imgIcon"];
    [self openTemplateWithData:6];
}

- (IBAction)qrPayment:(id)sender {
    [self getMenuIdx:@"btnQRPay"];
    [[DataManager sharedManager]resetObjectData];
    NSArray *tempMain = [[DataManager sharedManager] getJSONData:3];
    if(tempMain != nil){
        NSDictionary *objectMain = [tempMain objectAtIndex:1];
        int ind = 2;
        //NSArray *menu = [objectMain valueForKey:@"action"];
        if ([userDefault valueForKey:@"btnQRPay"]) {
            ind = [[userDefault valueForKey:@"btnQRPay"] intValue];
        }
//        for (int i =0; i < [menu count]; i++) {
//            NSArray *selMenu = [[objectMain valueForKey:@"action"] objectAtIndex:i];
//            if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
//                ind = i;
//                break;
//            }
//        }
        NSArray *temp = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [temp objectAtIndex:1];
        // NSString *templateNames = [object valueForKey:@"template"];
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:@"ic_qrpay_header.png" forKey:@"imgIcon"];
        if([[tempData valueForKey:@"activation"]boolValue]){
//            if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
            if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *alertTitle = @"";
                NSString *alertMessage = @"";
                if ([lang isEqualToString:@"en"]) {
                    alertTitle = @"Information";
                    alertMessage = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
                } else {
                    alertTitle = @"Informasi";
                    alertMessage = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
                }
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            } else {
                TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
                [templateViews setJsonData:tempData];
                bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"] ];
                if (stateAcepted) {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    //            alert.tag = 404;
                    [alert show];
                }else{
                    [self.navigationController pushViewController:templateViews animated:YES];
                    
                }
            }
        } else {
            TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
            [templateViews setJsonData:tempData];
            bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"] ];
            if (stateAcepted) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //            alert.tag = 404;
                [alert show];
            }else{
                [self.navigationController pushViewController:templateViews animated:YES];
                
            }
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //            alert.tag = 404;
        [alert show];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 229) {
        if (buttonIndex == 0) {
            
          
        }else if (buttonIndex == 1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"NO" forKey:@"fromSliding"];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"id"]){
                [userDefault setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
            }else{
                [userDefault setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
            }
        
            [userDefault synchronize];
            
            if (self.parentViewController.tabBarController.tabBar.hidden) {
                self.parentViewController.tabBarController.tabBar.hidden = NO;
            }
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC popToRootViewControllerAnimated:NO];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
            
        }
    }else if (alertView.tag == 230) {
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 231;
            [alert show];
        } else if (buttonIndex == 1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if (lang == nil) {
                [userDefault setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
            }else{
                [userDefault setObject:[NSArray arrayWithObjects:lang, nil] forKey:@"AppleLanguages"];
            }
//            if([lang isEqualToString:@"id"]){
//                [userDefault setObject:[NSArray arrayWithObjects:@"id", nil] forKey:@"AppleLanguages"];
//            }else{
//                [userDefault setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
//            }
//
            [userDefault synchronize];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
        }
    }else if (alertView.tag == 231) {
        
    }else if (alertView.tag == 12345) {
        if (buttonIndex == 0) {
            //CANCEL Button
        } else {
            //[self stopIdleTimer];
            
            //OK Button
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
            [userDefault setObject:@"NO" forKey:@"hasLogin"];
            [userDefault synchronize];
            
            if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
                CGRect screenBound = [[UIScreen mainScreen] bounds];
                CGSize screenSize = screenBound.size;
                CGFloat screenWidth = screenSize.width;
                CGFloat screenHeight = screenSize.height;
                
                self.parentViewController.tabBarController.tabBar.hidden = YES;
                [self.slidingViewController resetTopViewAnimated:YES];
                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
                templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateView animated:YES];
            } else {
                exit(0);
            }
        }
    }
    else if (alertView.tag == 505) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault synchronize];
        
        if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
            CGRect screenBound = [[UIScreen mainScreen] bounds];
            CGSize screenSize = screenBound.size;
            CGFloat screenWidth = screenSize.width;
            CGFloat screenHeight = screenSize.height;
            
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
            templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else {
            exit(0);
        }
    }
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    NSDictionary* userInfo = @{@"position": @(1112)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
//    
//    [super touchesBegan:touches withEvent:event];
//}

-(void)callResetTimer{
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    userInfo = @{@"position": @(1112)};
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}



- (IBAction)opnFavorite:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_favorite.png" forKey:@"imgIcon"];
    TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:@"FavoriteVC"];
    bool stateAcepted = [Utility vcNotifConnection:@"FavoriteVC"];
    if (stateAcepted) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //            alert.tag = 404;
        [alert show];
    }else{
        [self.navigationController pushViewController:templateView animated:YES];
    }
    
}

-(void)getMenuIdx : (NSString *) key {
    NSString *url =[NSString stringWithFormat:@"%@menu_index_ios.html?vn=%@",API_URL,@VERSION_NAME];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];
    [request setHTTPMethod:@"GET"];
    @try {
        NSError *error;
        NSURLResponse *urlResponse;
        NSData *data=[NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if(error == nil){
            NSDictionary *mJsonObject = [NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&error];
            if(mJsonObject){
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setValue:[mJsonObject valueForKey:key] forKey:key];
                [userDefault synchronize];
            }
            
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", [exception description]);
    }
}

#pragma mark - adding function check onboarding
-(void)checkOnboarding
{
    [CustomerOnboardingData getToken];
    dispatch_group_t dispatchGroup = dispatch_group_create();
    Encryptor *encryptor = [[Encryptor alloc] init];
    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_BEGIN_SCREEN_KEY];
    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_REKENING_INFO_KEY];
    [encryptor removeUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
    [encryptor setUserDefaultsObject:@"Yes" forKey:ONBOARDING_NEED_TO_CREATE_NASABAH_KEY];
    [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
    [CustomerOnboardingData getToken];
    
    // INIT MSMSID
    if(!ONBOARDING_ENABLE_UNIQUE_DEVICE && ![encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY])
    {
        [CustomerOnboardingData removeForKeychainKey:ONBOARDING_MSMSID_KEY];
    }
    
    
    if(![CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY])
    {
        NSUUID *uuid = [NSUUID UUID];
        NSString *stringUUID = [uuid UUIDString];
        
        [encryptor setUserDefaultsObject:stringUUID forKey:ONBOARDING_MSMSID_KEY];
        [CustomerOnboardingData setValue:stringUUID forKey:ONBOARDING_MSMSID_KEY];
    }
    else [encryptor setUserDefaultsObject:[CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY] forKey:ONBOARDING_MSMSID_KEY];
    
    // CHECK VIDEO ASSIST VERSION
//    NSMutableDictionary *checkVersionResult = [[NSMutableDictionary alloc] init];
//    [CustomerOnboardingData getFromEndpoint:@"api/setting/getVideoVer" dispatchGroup:dispatchGroup returnData:checkVersionResult];
    
    // CHECK COB ENABLED
    NSMutableDictionary *checkCobResult = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=COB_ENABLED" dispatchGroup:dispatchGroup returnData:checkCobResult];
    
    // CHECK IF MSMSID DID APPLIED
    NSString *msmsid = [CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY];
    NSMutableDictionary *checkMsmsidResult = [[NSMutableDictionary alloc] init];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:dispatchGroup returnData:checkMsmsidResult];
    
    // CHECK MAX MIN COB
//    NSMutableDictionary *checkMaxResult = [[NSMutableDictionary alloc] init];
//    NSMutableDictionary *checkMinResult = [[NSMutableDictionary alloc] init];
//    [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=MAX_COB" dispatchGroup:dispatchGroup returnData:checkMaxResult];
//    [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=MIN_COB" dispatchGroup:dispatchGroup returnData:checkMinResult];
    
    // CHECK COB TIME
    NSMutableDictionary *checkTimeResult = [[NSMutableDictionary alloc] init];
    NSString *mobversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/setting/getCobTimesNew?mobver=%@", mobversion] dispatchGroup:dispatchGroup returnData:checkTimeResult];
    
    dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
        BOOL isNeedRedirect = YES;
        BOOL isMaintenance = NO;
        if([[checkCobResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
        {
            NSDictionary *data;
            if([[checkCobResult objectForKey:@"data"] isKindOfClass:NSArray.class])
                data = [(NSArray *)[checkCobResult objectForKey:@"data"] objectAtIndex:0];
            else
                data = [checkCobResult objectForKey:@"data"];
            BOOL cobEnabled = [[data objectForKey:@"setval"] isEqualToString:@"true"] ? YES : NO;
            if(!cobEnabled || ([[checkTimeResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]] && ![[[checkTimeResult objectForKey:@"data"] objectForKey:@"iscobtime"] isEqualToNumber:[NSNumber numberWithBool:YES]]))
            {
                // DO NOT REDIRECT
                isMaintenance = YES;
                isNeedRedirect = NO;
            }
//            else
//            {
//                if([[checkMaxResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]]
//                   && [[checkMinResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
//                {
//                    NSString *max;
//                    NSString *min;
//                    if([[checkMaxResult objectForKey:@"data"] isKindOfClass:NSArray.class])
//                        max = [[(NSArray *)[checkMaxResult objectForKey:@"data"] objectAtIndex:0] objectForKey:@"setval"];
//                    else
//                        max = [[checkMaxResult objectForKey:@"data"] objectForKey:@"setval"];
//                    if([[checkMinResult objectForKey:@"data"] isKindOfClass:NSArray.class])
//                        min = [[(NSArray *)[checkMinResult objectForKey:@"data"] objectAtIndex:0] objectForKey:@"setval"];
//                    else
//                        min = [[checkMinResult objectForKey:@"data"] objectForKey:@"setval"];
//
//                    NSDate *now = [NSDate date];
//                    NSDate *first = [CustomerOnboardingData date:now withHour:[[min substringToIndex:2] intValue] minute:[[min substringFromIndex:3] intValue]];
//                    NSDate *last = [CustomerOnboardingData date:now withHour:[[max substringToIndex:2] intValue] minute:[[max substringFromIndex:3] intValue]];
//
//                    if(![CustomerOnboardingData compareDate:now withinFirst:first andLast:last])
//                    {
//                        // DO NOT REDIRECT
//                        isMaintenance = YES;
//                        isNeedRedirect = NO;
//                    }
//                }
//            }
            
            // DOWNLOAD VIDEO ASSIST
//            if([[checkVersionResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
//            {
//                BOOL needUpdateVideoAssist = YES;
//                NSInteger videoAssistVersion = 1;
//                if([encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEASSIST_VERSION])
//                    videoAssistVersion = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEASSIST_VERSION] integerValue];
//
//                if(videoAssistVersion == [[checkVersionResult objectForKey:@"data"] integerValue])
//                    needUpdateVideoAssist = NO;
//                else
//                    videoAssistVersion = [[checkVersionResult objectForKey:@"data"] integerValue];
//
//                if(needUpdateVideoAssist)
//                {
//                    PembukaanAkunAwalController *paac = [[PembukaanAkunAwalController alloc] initWithVideoAssistVersion:videoAssistVersion];
//                    [paac downloadVideoAssistVersion];
//                }
//            }
        }
        
        if(isNeedRedirect)
        {
            if([[checkMsmsidResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
            {
                NSMutableDictionary *nasabah = [checkMsmsidResult objectForKey:@"data"];
                NSString *status = [nasabah objectForKey:@"status"];
                Rekening *rekening = [[Rekening alloc] init];
                [rekening setNamaNasabah:[nasabah objectForKey:@"nama_lgkp"]];
                [rekening setNomorRekening:[nasabah objectForKey:@"norek"]];
                [rekening setJenisTabungan:[nasabah objectForKey:@"jenis_tabungan"]];
                [rekening setJenisKartu:[nasabah objectForKey:@"jenis_kartu"]];
                [rekening setKodeCabang:[nasabah objectForKey:@"kode_cabang"]];
                
                [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
                [CustomerOnboardingData setObject:rekening forKey:ONBOARDING_REKENING_INFO_KEY];
                
                OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                
                if([status isEqualToString:@"APPROVED"])
                {
                    [encryptor removeVideoAssist];
                    
                    // GO TO TabunganBerhasil Screen
                    [encryptor setUserDefaultsObject:@"TabunganBerhasil" forKey:ONBOARDING_BEGIN_SCREEN_KEY];
                    TabunganBerhasilController *vc = [[TabunganBerhasilController alloc] initWithRekening:rekening];
                    [templateVC setChildController:vc];
                    [self presentViewController:templateVC animated:YES completion:nil];
                }
                else if([status isEqualToString:@"REJECTEDVIDEO"])
                {
                    // CHECK IF LOCAL DATA STILL EXISTS
                    if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] && ![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY])
                        [encryptor restoreDataToLocalUsingData:nasabah];
                    
                    // GO TO GagalVerifikasi Screen
                    [encryptor setUserDefaultsObject:@"GagalVerifikasiVideo" forKey:ONBOARDING_BEGIN_SCREEN_KEY];
                    GagalVerifikasiController *vc = [[GagalVerifikasiController alloc] initForRejectVideoWithRekening:rekening];
                    [templateVC setChildController:vc];
                    [self presentViewController:templateVC animated:YES completion:nil];
                }
                else if([status isEqualToString:@"NEW"] && [[encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"DONE"])
                {
                    // CHECK EXPIRY DATE
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *expiryString = [nasabah objectForKey:@"expiry_date"];
                    NSDate *now = [NSDate date];
                    NSDate *expiryDate = [dateFormatter dateFromString: [NSString stringWithFormat:@"%@ 23:59:59", expiryString]];
                    
                    if([expiryDate compare:now] == NSOrderedDescending)
                    {
                        NSString *metodeVerifikasi = [nasabah objectForKey:@"jenis_kyc"];
                        if([metodeVerifikasi isEqualToString:@"CABANG"])
                        {
                            // GO TO NomorTiketScene
                            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"NomorTiketScene"];
                            [self presentViewController:vc animated:YES completion:nil];
                        }
                        else if ([metodeVerifikasi isEqualToString:@"VIDEOCALL"])
                        {
                            // GO TO VerifikasiOnlineSelesaiScene
                            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiOnlineSelesaiScene"];
                            [self presentViewController:vc animated:YES completion:nil];
                        }
                    }
                    else
                    {
                        [encryptor resetFormData];
                        isNeedRedirect = NO;
                    }
                }
                else if ([status isEqualToString:@"REJECTED"])
                {
                    if([[encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"DONE"] || [encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] == nil) {
                        // GO TO GagalVerifikasi Screen
                        [encryptor setUserDefaultsObject:@"GagalVerifikasi" forKey:ONBOARDING_BEGIN_SCREEN_KEY];
                        GagalVerifikasiController *vc = [[GagalVerifikasiController alloc] initForRejectedWithRekening:rekening];
                        [templateVC setChildController:vc];
                        [self presentViewController:templateVC animated:YES completion:nil];
                    }
                    else {
                        isNeedRedirect = NO;
                        [encryptor setUserDefaultsObject:@"Yes" forKey:ONBOARDING_NEED_TO_CREATE_NASABAH_KEY];
                    }
                }
                else
                {
                    isNeedRedirect = NO;
                    [encryptor setUserDefaultsObject:@"No" forKey:ONBOARDING_NEED_TO_CREATE_NASABAH_KEY];
                }
            }
            else isNeedRedirect = NO;
        }
        

        
        AppDelegate *mainClass = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            mainClass.splashImageView.alpha = .0f;
        } completion:^(BOOL finished){
            if (finished) {
                [mainClass.splashImageView removeFromSuperview];
            }
        }];
        
        if(!isNeedRedirect)
        {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//            NSString *flagCID = [userDefault objectForKey:@"customer_id"];
            NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
            if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
                [self checkActivation];
            } else {
                [self showGreeting];
            }
        }
    });
}

@end
