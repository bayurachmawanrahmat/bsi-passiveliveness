//
//  ScrollHomeViewController.h
//  BSM-Mobile
//
//  Created by ARS on 04/12/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "RootViewController.h"
#import "TemplateViewController.h"

@interface ScrollHomeViewController : TemplateViewController
- (void)goToSomepage:(int)position;
- (void)openFavorite:(int)node;
- (void)showGreeting;
- (void)loadBannerView;
- (void)loadBannerView2;
- (TemplateViewController *)routeTemplateController:(NSString *)templateName;
- (void)openTemplateWithData:(int)position;
- (void)goToQR;
- (void)goToInbox;
@end


