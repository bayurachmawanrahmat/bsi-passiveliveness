//
//  ScrollHomeViewController.m
//  BSM-Mobile
//
//  Created by ARS on 04/12/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ScrollHomeViewController.h"
#import "Constant.h"
#import "TemplateViewController.h"
#import "LV01ViewController.h"
#import "FinancialViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Utility.h"
#import "InboxHelper.h"
#import "Connection.h"
#import "UIImageView+AFNetworking.h"
#import "PrayerTime.h"
#import "SholluLib.h"
#import "PopupBurroqViewController.h"
#import "GDashViewController.h"
#import "GELV01MenuViewController.h"
#import "CapturePhotoViewController.h"
#import "SKeychain.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreLocation/CoreLocation.h>

//import class for onboarding
#import "CustomerOnboardingData.h"
#import "Encryptor.h"
#import "OnboardingTemplate01ViewController.h"
#import "TabunganBerhasilController.h"
#import "GagalVerifikasiController.h"
#import "PembukaanAkunAwalController.h"
#import "PopupActivationViewController.h"
#import "PopupOnboardingViewController.h"
#import "CircleTransition.h"
#import "SearchItemViewController.h"
#import "ListSurahViewController.h"
#import "JuzAmmaViewController.h"
#import "AsmaulHusnaViewController.h"
#import "TausiahViewController.h"
#import "FavoriteDetailViewController.h"
#import "ComplainViewController.h"
#import "WebviewViewController.h"
#import "NSUserdefaultsAes.h"
#import "PopUpLoginViewController.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import <WebKit/WebKit.h>
#import "CreatePinOnboardingController.h"

@interface ScrollHomeViewController ()<UIScrollViewDelegate, UIAlertViewDelegate, CLLocationManagerDelegate, UITextFieldDelegate,ConnectionDelegate, SearchItemDelegate, PopupActivationDelegate, WKUIDelegate, WKNavigationDelegate>{
    
    NSArray *images;
    UIButton *buttonLogout;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
    
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    
    NSString *cname;
    NSString *filepath;
    NSString *filename;
    NSString *thisVersion;
    NSString *fileAtPath;
    
    NSTimer *idleTimer;
    int maxIdle;
    
    NSUserDefaults *userDefault;
    InboxHelper *inboxHelper;
    
    NSMutableArray *listData;
    NSMutableArray *listNames;
    NSMutableArray *times;
    NSMutableArray *arrInterval;
    NSArray * arrListTimes;
    CLLocation *currentLocation;
    CLLocationManager *locationManager;
    
    double lat;
    double lng;
    NSDate *curDate;
    int leastInterval;
    BOOL flagMinus;
    NSTimer *myTimer;
    BOOL selectedTime;
    NSString *islamicDateString;
    
    NSString *dataMessageBurroq;
    NSString *findId;
    NSString *listAcctFrom;
    
    UIView *uiIcList1;
    UIView *uiIcList2;
    UIView *uiIcList3;
    UIView *uiIcList4;
    
    UIView *uiVwIcInfoAccount;
    UIView *uiVwIcTransfer;
    UIView *uiVwIcPayment;
    UIView *uiVwIcPurchase;
    UIView *uiVwIcQRPay;
    UIView *uiVwIcIslamicService;
    UIView *uiVwIcZiswaf;
    UIView *uiVwIcCashWithdrawal;
    UIView *uiVwIcOpenAccount;
    UIView *uiVwIcFavorite;
    UIView *uiVwIcBlockCard;
    UIView *uiVwIcTopUp;
    UIView *uiVwIcEcommerce;
    UIView *uiVwIcScheduler;
    UIView *uiVwIcGoldWallet;
    UIView *uiVwIcFinancing;
    
    UIButton *btnInfoAccount;
    UIButton *btnTransfer;
    UIButton *btnPayment;
    UIButton *btnPurchase;
    UIButton *btnQRPay;
    UIButton *btnIslamicService;
    UIButton *btnZiswaf;
    UIButton *btnCashWithdrawal;
    UIButton *btnOpenAccount;
    UIButton *btnFavorite;
    UIButton *btnBlockCard;
    UIButton *btnTopUp;
    UIButton *btnEcommerce;
    UIButton *btnGoldWallet;
    UIButton *btnScheduler;
    UIButton *btnFinancing;
    
    UIImageView *iconInfoAccount;
    UIImageView *iconTransfer;
    UIImageView *iconPayment;
    UIImageView *iconPurchase;
    UIImageView *iconQRPay;
    UIImageView *iconIslamicService;
    UIImageView *iconZiswaf;
    UIImageView *iconCashWithdrawal;
    UIImageView *iconFavorite;
    UIImageView *iconBlockCard;
    UIImageView *iconOpenAccount;
    UIImageView *iconTopup;
    UIImageView *iconEcommerce;
    UIImageView *iconScheduler;
    UIImageView *iconGoldWallet;
    UIImageView *iconFinancing;

    UIView *vwSeparator;
    UIView *vwSyariahPoint;
    UIImageView *imgIconPoint;
    
    UITextField *textFieldSearch;
    CircleTransition *transitionCircSearch;
    UIView *viewBgAbout;
    
    NSString *placemarkLocality;
    SholluLib *sholluLib;
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *uiView;
@property (weak, nonatomic) IBOutlet UIView *vwBanner2;
@property (weak, nonatomic) IBOutlet UIView *vwBanner1;
@property (weak, nonatomic) IBOutlet UIView *vwGreeting;
@property (weak, nonatomic) IBOutlet UIView *vwIcons;
@property (weak, nonatomic) IBOutlet UIView *vwJadwalSholat;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;

@property (weak, nonatomic) IBOutlet WKWebView *webViewBanner;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner1;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner2;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner3;

@property (weak, nonatomic) IBOutlet UILabel *lblGreetings;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblJadwalSholat;

@property (weak, nonatomic) IBOutlet UIView *vwFBMyMenu;
@property (weak, nonatomic) IBOutlet UILabel *lblFBMyMenu;

@end

@implementation ScrollHomeViewController

//static int idxIS;
BOOL nAlertWasShownOnScrollHome = NO;
BOOL nJadwalSholatShow = YES;
BOOL nShowSyariahPoint = NO;

- (void)viewDidLoad{
    [super viewDidLoad];
       
    userDefault = [NSUserDefaults standardUserDefaults];
    inboxHelper = [[InboxHelper alloc]init];
    sholluLib = [[SholluLib alloc]init];
    [sholluLib setShowSyariahpoint:nShowSyariahPoint];
    
    [self initSetup];
    
    [userDefault setObject:@"HOME" forKey:@"VC"];
    [userDefault synchronize];
    
    CGFloat widthScreen = [UIScreen mainScreen].bounds.size.width;
    self.width.constant = widthScreen;
    self.height.constant = widthScreen * 0.7;
    
    if(!IPHONE_5){
        [self.scrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    }
    
    [_webViewBanner setUserInteractionEnabled:true];
    [_webViewBanner setUIDelegate:self];
    [_webViewBanner setNavigationDelegate:self];
    [_webViewBanner.scrollView setScrollEnabled:NO];
    [_webViewBanner.scrollView setBounces:NO];
    
    filepath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    thisVersion = @"0";
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        valAgree = @"Lanjut";
        valCancel = @"Batal";
        valTitle = @"Konfirmasi";
        valMsg = @"Apakah anda yakin untuk keluar dari aplikasi ?";
    }else{
        valAgree = @"Next";
        valCancel = @"Cancel";
        valTitle = @"Confirmation";
        valMsg = @"Do you want to quit the application ?";
    }
    
    filename = @"version_banner_ios.txt";
    fileAtPath = [fileAtPath stringByAppendingString:filename];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fileAtPath];
    
    if(fileExists){
        thisVersion = [[NSString alloc]initWithData:[NSData dataWithContentsOfFile:filepath] encoding:NSUTF8StringEncoding];
        NSLog(@"readStringFromFile: %@", thisVersion);
    } else {
        if(![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]){
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        [[thisVersion dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
    
    [self changeIconImgMenu];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    [self showGreeting];
    [self loadBannerView];
    
    // Setting Swipe for banner slider
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    // Adding the swipe gesture on WebView
    [self.webViewBanner addGestureRecognizer:swipeLeft];
    [self.webViewBanner addGestureRecognizer:swipeRight];
    
    [self setSelectorJadwalSholat];
    
    //Style FB MyMenu
    self.lblFBMyMenu.text = lang(@"MYMENU_TITLE");
    self.vwFBMyMenu.layer.shadowOffset = CGSizeMake(0.0, 2.0);
    self.vwFBMyMenu.layer.shadowOpacity = 0.5;
    self.vwFBMyMenu.layer.shadowRadius = 1.0;
    self.vwFBMyMenu.layer.shadowColor = [UIColor grayColor].CGColor;
    [self.vwFBMyMenu addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnGoMyMenu)]];

}

- (void)actionBtnGoMyMenu{
    TemplateViewController *templateView = [self routeTemplateController:@"MYMENU"];
    [self.navigationController pushViewController:templateView animated:YES];
}

- (void)setSelectorJadwalSholat{
    UITapGestureRecognizer *jadwalSholatTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(selectorJadwalSholat:)];
    
    [self.vwJadwalSholat addGestureRecognizer:jadwalSholatTap];
}

- (void)selectorJadwalSholat:(UITapGestureRecognizer *)recognizer{
    self.tabBarController.selectedIndex = 1;
}

- (void)createSyariahPointView{
    CGRect frmParent = vwSyariahPoint.frame;
    
    [vwSyariahPoint setUserInteractionEnabled:YES];
    [self setSelectorSyariaPoint];
    
    imgIconPoint = [[UIImageView alloc]init];
    
    [imgIconPoint setImage:[UIImage imageNamed:@"ic_banner_bsipoint.png"]];
    [imgIconPoint setContentMode:UIViewContentModeScaleAspectFit];
    
    CGRect frmImgIcon = imgIconPoint.frame;
    
    frmImgIcon.origin.x = 0;
    frmImgIcon.origin.y = 0;
    frmImgIcon.size.height = frmParent.size.height;
    frmImgIcon.size.width = frmParent.size.width;
    
    imgIconPoint.frame = frmImgIcon;
    
    [vwSyariahPoint addSubview:imgIconPoint];
    
    [vwSeparator setBackgroundColor:UIColorFromRGB(const_color_secondary)];
    
}

- (void)setSelectorSyariaPoint{
    UITapGestureRecognizer *syariaPointTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(openShariaPoint:)];
    
    [vwSyariahPoint addGestureRecognizer:syariaPointTap];
}

- (void)openShariaPoint:(UITapGestureRecognizer *)recognizer{
    if([self checkActivationCallback]){
        UIStoryboard *storyboardUI = [UIStoryboard storyboardWithName:@"Shariapoint" bundle:nil];
        TemplateViewController *templateView = [storyboardUI instantiateViewControllerWithIdentifier: @"Shariapoint"];
        [self.navigationController pushViewController:templateView animated:YES];
    }
}

-(void)getCurrentLocation
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:self->locationManager.location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                        NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");

                        if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                           
                        }
                       
                        CLPlacemark *placemark = [placemarks objectAtIndex:0];
                        self->sholluLib.placemarkLocality = placemark.locality;
                        [self->sholluLib setNotificationData];
                   }];
}

//
- (void)checkGPS {
    
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        nJadwalSholatShow = NO;
    }
    
    if ([CLLocationManager locationServicesEnabled]){
        locationManager = nil;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
        
        [locationManager startUpdatingLocation];
    } else{
        nJadwalSholatShow = NO;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    
    lng = currentLocation.coordinate.longitude;
    lat = currentLocation.coordinate.latitude;
    
    sholluLib.latitude = lat;
    sholluLib.longitude = lng;
    
    [sholluLib callPrayerTime];
    
    [self getCurrentLocation];
    
    [userDefault setValue:[NSString stringWithFormat:@"%f",lat] forKey:@"latitude"];
    [userDefault setValue:[NSString stringWithFormat:@"%f",lng] forKey:@"longitude"];
    
    [sholluLib getListData];
    
    if (sholluLib.leastInterval == 0) {
        self.lblJadwalSholat.text = @" ";
    }
    
    self.lblJadwalSholat.attributedText = [sholluLib getAttributedJadwalSholat];
    
    [myTimer invalidate];
    myTimer = nil;
    myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                               target:self
                                             selector:@selector(onTick:)
                                             userInfo:nil repeats:YES];
}

-(void)onTick:(NSTimer *)timer {
    
    [sholluLib proceedTick];
    NSString *timeDiff = [sholluLib getTimeDifference];
    
    if ([timeDiff isEqualToString:@"00:00:00"]) {
        [myTimer invalidate];
        myTimer = nil;
        self.lblJadwalSholat.text = [sholluLib getTimeAdzan];
    } else {
        self.lblJadwalSholat.attributedText = [sholluLib getAttributedJadwalSholat];
    }
}


-(void)handleSwipe :(UISwipeGestureRecognizer *)swipe{
    [self setShowMenu:@"HOME"];
    NSDictionary *userInfo = @{@"position": @(1112)};
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void) loadBannerView{
    NSString *urlString = [userDefault valueForKey:@"config_banner_url"];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *versionText = @VERSION_NAME;
    
    NSURL *TheUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@version_banner_ios.html?vn=%@",API_URL,versionText]];
    NSString *response = [NSString stringWithContentsOfURL:TheUrl
                                                  encoding:NSASCIIStringEncoding
                                                     error:nil];
    
    // Setting cache
    NSMutableURLRequest *urlRequest;
    if (response != nil) {
        if (![response isEqualToString:@""]) {
            response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([response isEqualToString:thisVersion]) {
                urlRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
            }
            else {
                urlRequest = [NSMutableURLRequest requestWithURL:url];
            }
        }
        else {
            urlRequest = [NSMutableURLRequest requestWithURL:url];
        }
    }
    else {
        urlRequest = [NSMutableURLRequest requestWithURL:url];
    }
    
    [self.webViewBanner loadRequest:urlRequest];
}

-(void) loadBannerView2{
    self.imgBanner2.userInteractionEnabled = YES;
    self.imgBanner3.userInteractionEnabled = YES;
    
    NSLog(@"%ld", (long)[userDefault integerForKey:@"config_banner2_type"]);
    UITapGestureRecognizer *banner2Tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(loadLinkBanner2:)];
    UITapGestureRecognizer *banner2aTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(loadLinkBanner2a:)];
    UITapGestureRecognizer *banner2bTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(loadLinkBanner2b:)];
    
    if([userDefault integerForKey:@"config_banner2_type"]  == 2){
        NSLog(@"%@\n%@",[userDefault objectForKey:@"config_banner2a_url"],[userDefault objectForKey:@"config_banner2b_url"]);
        
        [_imgBanner2 setImageWithURL:[NSURL URLWithString:[userDefault objectForKey:@"config_banner2a_url"]] placeholderImage:nil];
        [_imgBanner3 setImageWithURL:[NSURL URLWithString:[userDefault objectForKey:@"config_banner2b_url"]] placeholderImage:nil];
        
        [self.imgBanner2 addGestureRecognizer:banner2aTap];
        [self.imgBanner3 addGestureRecognizer:banner2bTap];

    }else{
        [_imgBanner2 setImageWithURL:[NSURL URLWithString:[userDefault objectForKey:@"config_banner2_url"]] placeholderImage:nil];
        [self.imgBanner2 addGestureRecognizer:banner2Tap];
    }

}
- (void) loadLinkBanner2:(UITapGestureRecognizer *)recognizer{
    NSString *pURL = [userDefault objectForKey:@"config_banner2_link"];
    [self openPURL:pURL];
}
- (void) loadLinkBanner2a:(UITapGestureRecognizer *)recognizer{
    NSString *pURL = [userDefault objectForKey:@"config_banner2a_link"];
    if([pURL isEqualToString:@"myMenu"]){
        TemplateViewController *templateView = [self routeTemplateController:@"MYMENU"];
        [self.navigationController pushViewController:templateView animated:YES];
    } else {
        [self openPURL:pURL];
    }
}
- (void) loadLinkBanner2b:(UITapGestureRecognizer *)recognizer{
    NSString *pURL = [userDefault objectForKey:@"config_banner2b_link"];
    [self openPURL:pURL];
}

- (void) openPURL:(NSString*)pURL{
    if(pURL != nil){
        if( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:pURL]]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pURL] options:@{} completionHandler:nil];
        }else{
            [self openNodes:pURL];
        }
    }
}

- (void) openNodes : (NSString *)param{
    NSArray *mNode = [param componentsSeparatedByString:@"/"];
    NSDictionary* userInfo = @{@"position": @(1124),
                               @"node_index" : mNode};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self changeIconImgMenu];


    curDate = [NSDate date];
    sholluLib.currentDate = curDate;
    [self checkGPS];
    //checkinbox
    [self requestBurekol];

}

-(void) reqLogin{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];

    if ((cid == nil) || ([cid isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
    } else {
        
        if ([userDefault valueForKey:@"req_login"] == nil) {
            [userDefault setObject:@"-" forKey:@"req_login"];
            [userDefault synchronize];
            
            [self connectToReqLogin];

        }
        
    }
}

-(void) connectToReqLogin{
    NSString *nImei = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
    NSString *tokenFCM = [inboxHelper readToken:nImei];
    
    if ([tokenFCM isEqualToString:@""] || tokenFCM == nil) {
        tokenFCM = @"";
    }
    
    [[[DataManager sharedManager]dataExtra]setObject:tokenFCM forKey:@"token"];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=login,customer_id,token,notif_type=iphone,date_local" needLoading:false textLoading:@"" encrypted:true banking:true continueLoading:false];
}

-(void)changeIconImgMenu{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if ([lang isEqualToString:@"en"]) {
        iconGoldWallet.image = [UIImage imageNamed:@"ic_mmenu_eemas_mx_en_v3"];
        iconInfoAccount.image=[UIImage imageNamed:@"ic_mmenu_inforekening_mx_en_v3"];
        iconPayment.image=[UIImage imageNamed:@"ic_mmenu_pembayaran_mx_en_v3"];
        iconPurchase.image=[UIImage imageNamed:@"ic_mmenu_pembelian_mx_en_v3"];
        iconCashWithdrawal.image=[UIImage imageNamed:@"ic_mmenu_tariktunai_mx_en_v3"];
        iconFavorite.image=[UIImage imageNamed:@"ic_mmenu_favorit_mx_en_v3"];
        iconIslamicService.image=[UIImage imageNamed:@"ic_mmenu_layananislami_mx_en_v3"];
        iconZiswaf.image = [UIImage imageNamed:@"ic_mmenu_berbagiziswaf_mx_en_v3"];
        iconTransfer.image=[UIImage imageNamed:@"ic_mmenu_transfer_mx_en_v3"];
        iconBlockCard.image = [UIImage imageNamed:@"ic_mmenu_bukarekening_mx_en_v3"];
        iconTopup.image = [UIImage imageNamed:@"ic_mmenu_topupewallet_mx_en_v3"];
        iconOpenAccount.image = [UIImage imageNamed:@"ic_mmenu_bukarekening_mx_en_v3"];
        iconScheduler.image = [UIImage imageNamed:@"ic_mmenu_transaksiterjadwal_mx_en_v3"];
        iconEcommerce.image = [UIImage imageNamed:@"ic_mmenu_ecommerce_mx_en_v3"];
        iconFinancing.image = [UIImage imageNamed:@"ic_mmenu_pembiayaan_mx_en"];

    }else{
        iconGoldWallet.image = [UIImage imageNamed:@"ic_mmenu_eemas_mx_v3"];
        iconInfoAccount.image=[UIImage imageNamed:@"ic_mmenu_inforekening_mx_v3"];
        iconPayment.image=[UIImage imageNamed:@"ic_mmenu_pembayaran_mx_v3"];
        iconPurchase.image=[UIImage imageNamed:@"ic_mmenu_pembelian_mx_v3"];
        iconCashWithdrawal.image=[UIImage imageNamed:@"ic_mmenu_tariktunai_mx_v3"];
        iconFavorite.image=[UIImage imageNamed:@"ic_mmenu_favorit_mx_v3"];
        iconIslamicService.image=[UIImage imageNamed:@"ic_mmenu_layananislami_mx_v3"];
        iconZiswaf.image = [UIImage imageNamed:@"ic_mmenu_berbagiziswaf_mx_v3"];
        iconTransfer.image=[UIImage imageNamed:@"ic_mmenu_transfer_mx_v3"];
        iconBlockCard.image = [UIImage imageNamed:@"ic_mmenu_bukarekening_mx_v3"];
        iconTopup.image = [UIImage imageNamed:@"ic_mmenu_topupewallet_mx_v3"];
        iconOpenAccount.image = [UIImage imageNamed:@"ic_mmenu_bukarekening_mx_v3"];
        iconScheduler.image = [UIImage imageNamed:@"ic_mmenu_transaksiterjadwal_mx_v3"];
        iconEcommerce.image = [UIImage imageNamed:@"ic_mmenu_ecommerce_mx_v3"];
        iconFinancing.image = [UIImage imageNamed:@"ic_mmenu_pembiayaan_mx"];

    }
}

-(BOOL) checkActivationCallback{
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        
        if([lang isEqualToString:@"en"]){
            valTitle = @"Information";
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            valCancel = @"No";
            valAgree = @"Ok";
        } else {
            valTitle = @"Informasi";
            valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            valCancel = @"Tidak";
            valAgree = @"Ok";
        }
        
        if (![[userDefault valueForKey:@"config_onboarding"] boolValue]){
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:lang(@"INFO")
                                  message:lang(@"ACTIVATION_REQUEST")
                                  delegate:self
                                  cancelButtonTitle:lang(@"NOPE")
                                  otherButtonTitles:@"Ok", nil];
            alert.tag = 229;
            [alert show];
            
            return false;
        }else{
            
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
            firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:firstVC animated:NO completion:^{
                if([self->dataManager.dataExtra objectForKey:@"activationFromLink"]){

                    [[DataManager sharedManager].dataExtra removeObjectForKey:@"activationFromLink"];
                    
                }else{
                    PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
                    [firstVC presentViewController:viewCont animated:YES completion:nil];
                }
            }];
            
            return false;
        }
        
    }
    return true;

}

- (void)gotoTemplate:(NSString *)string{
    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Registrasi" bundle:nil];
    TemplateViewController *templateView = [ui instantiateViewControllerWithIdentifier:string];
    [self.navigationController pushViewController:templateView animated:YES];
}

- (void)checkActivation {
    BOOL canOpenTemplate = true;
//    if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
    if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
        canOpenTemplate = false;
    }
    if(!canOpenTemplate){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];

        if([lang isEqualToString:@"en"]){
            valTitle = @"Information";

            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            valCancel = @"No";
            valAgree = @"Ok";
        } else {
            valTitle = @"Informasi";

            valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            valCancel = @"Tidak";
            valAgree = @"Ok";
        }
        
        if (![[userDefault valueForKey:@"config_onboarding"] boolValue]){
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            alert.tag = 229;
            [alert show];
        }else{
            NSDictionary* userInfo = @{@"position": @(1119)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        }
        
        
        
    }
}

- (void)setPresentationStyleForSelfController:(UIViewController *)selfController presentingController:(UIViewController *)presentingController
{
    if (@available(iOS 8, *))
    {
        presentingController.providesPresentationContextTransitionStyle = YES;
        presentingController.definesPresentationContext = YES;
        
        [presentingController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [selfController.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
    else
    {
        [selfController setModalPresentationStyle:UIModalPresentationCurrentContext];
        [selfController.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self callResetTimer];
}

- (float)getFontSize:(float)size{
    if(IPHONE_5)
    {
        return size-3;
    }else if(IPHONE_8)
    {
        return size-2;
    }else if(IPHONE_8P)
    {
        return size-1;
    }else{
        return size;
    }
}

-(void) castCustomerName{
    _lblGreetings.attributedText = [SholluLib getAttributedSalam];
}


- (void)showGreeting {
    
    _lblGreetings.attributedText = [SholluLib getAttributedSalam];
    _lblLocation.text = @"";
    _lblJadwalSholat.text = @"";
    
    _lblGreetings.textAlignment = NSTextAlignmentCenter;
    _lblLocation.textAlignment = NSTextAlignmentCenter;
    if(nShowSyariahPoint){
        _lblJadwalSholat.textAlignment = NSTextAlignmentLeft;
        _lblJadwalSholat.numberOfLines = 2;
    }else{
        _lblJadwalSholat.textAlignment = NSTextAlignmentCenter;
    }
    
    //Layouting
    screenBound = [[UIScreen mainScreen] bounds];
    
    screenSize = screenBound.size;
    
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmScrollView = self.scrollView.frame;
    
    CGRect frmUIView = self.uiView.frame;
    
    CGRect frmVwSearch = textFieldSearch.frame;
    
    CGRect frmVwGreeting = self.vwGreeting.frame;
    CGRect frmLblGreeting = self.lblGreetings.frame;
    
    //SyariahPoint
    
    vwSeparator = [[UIView alloc]init];
    vwSyariahPoint = [[UIView alloc]init];
    if(nShowSyariahPoint){
        [self.uiView addSubview:vwSeparator];
        [self.uiView addSubview:vwSyariahPoint];
    }
    CGRect frmVwSeparator = vwSeparator.frame;
    CGRect frmVwSyariahPoint = vwSyariahPoint.frame;
    
    
    CGRect frmVwJadwalSholat = self.vwJadwalSholat.frame;
    CGRect frmLblJadwalSholat = self.lblJadwalSholat.frame;
    CGRect frmLblLocation = self.lblLocation.frame;
    
    CGRect frmVwBanner = self.vwBanner1.frame;
    CGRect frmImgPlace = self.imgBanner1.frame;
    CGRect frmWebView = self.webViewBanner.frame;
    
    CGRect frmVwBanner2 = self.vwBanner2.frame;
    CGRect frmImagBanner2 = self.imgBanner2.frame;
    CGRect frmImagBanner3 = self.imgBanner3.frame;
    
    CGRect frmVwIcon = self.vwIcons.frame;
    CGRect frmVwIconInfoAccount = uiVwIcInfoAccount.frame;
    CGRect frmVwIconTransfer = uiVwIcTransfer.frame;
    CGRect frmVwIconPayement = uiVwIcPayment.frame;
    CGRect frmVwIconPurchase = uiVwIcPurchase.frame;
    
    CGRect frmVwIconList1 = uiIcList1.frame;
    CGRect frmVwIconList2 = uiIcList2.frame;
    CGRect frmVwIconList3 = uiIcList3.frame;
    CGRect frmVwIconList4 = uiIcList4.frame;
    
    
    frmScrollView.origin.y = 85;
    frmScrollView.size.width = screenWidth;
    
    frmVwSearch.origin.x = 8;
    frmVwSearch.origin.y = 8;
    frmVwSearch.size.width = screenWidth - 16;
    frmVwSearch.size.height = 36;
    
    if(nShowSyariahPoint){
        frmVwGreeting.origin.y = 0;
        frmVwGreeting.origin.y = frmVwSearch.origin.y + frmVwSearch.size.height + 4;
        frmVwGreeting.size.width = screenWidth;

        frmVwGreeting.size.height = [self getFontSize:10] + [self getLabelHeight:_lblGreetings] + [self getFontSize:16];
        frmLblGreeting.origin.y = [self getFontSize:10];
        frmLblGreeting.size.height = [self getLabelHeight:_lblGreetings];
        
        frmVwJadwalSholat.origin.x = 16;
        frmVwJadwalSholat.origin.y = frmVwGreeting.origin.y + frmVwGreeting.size.height;
        
        frmVwJadwalSholat.size.width = (screenWidth/2)-1;
    }else{
        //default
        frmVwGreeting.origin.x = 16;
        frmVwGreeting.origin.y = frmVwSearch.origin.y + frmVwSearch.size.height + 4;
        frmVwGreeting.size.width = screenWidth-32;
    
        frmVwGreeting.size.height = [self getFontSize:10] + [self getLabelHeight:_lblGreetings];
        frmLblGreeting.origin.x = 0;
        frmLblGreeting.size.width = frmVwGreeting.size.width;
        frmLblGreeting.origin.y = [self getFontSize:5];
        frmLblGreeting.size.height = [self getLabelHeight:_lblGreetings];
        
        frmVwJadwalSholat.origin.x = 16;
        frmVwJadwalSholat.origin.y = frmVwGreeting.origin.y + frmVwGreeting.size.height;
        
        frmVwJadwalSholat.size.width = screenWidth - 32;
    }

    frmLblLocation.size.width = frmVwJadwalSholat.size.width;
    frmLblLocation.origin.x = 0;
    frmLblLocation.origin.y = 0;
//    frmLblLocation.size.height = [self getLabelHeight:_lblLocation];
    frmLblLocation.size.height = 0;
    
    frmLblJadwalSholat.size.width = frmVwJadwalSholat.size.width;
    frmLblJadwalSholat.origin.y = frmLblLocation.origin.y + frmLblLocation.size.height;
    frmLblJadwalSholat.origin.x = 0;
    
    /*default*/
    
    if(nShowSyariahPoint){
        /*with syariapoint*/
         frmLblJadwalSholat.size.height = 50;
         frmVwJadwalSholat.size.height = frmLblJadwalSholat.size.height + frmLblLocation.size.height;
        
        //Syaria Point
        frmVwSeparator.origin.y = frmVwJadwalSholat.origin.y;
        frmVwSeparator.size.width = 2;
        frmVwSeparator.origin.x = frmVwJadwalSholat.size.width + frmVwJadwalSholat.origin.x;
        frmVwSeparator.size.height = frmVwJadwalSholat.size.height;
        
        frmVwSyariahPoint.origin.y = frmVwSeparator.origin.y;
        frmVwSyariahPoint.origin.x = frmVwSeparator.origin.x + frmVwSeparator.size.width;
        frmVwSyariahPoint.size.width = frmVwJadwalSholat.size.width;
        frmVwSyariahPoint.size.height = frmVwSeparator.size.height;
    }else{
        frmLblJadwalSholat.size.height = [self getLabelHeight:_lblJadwalSholat];
        if(nJadwalSholatShow == NO){
            frmVwJadwalSholat.size.height = 0;
        }
    }
    
    
    
    frmVwBanner.origin.y = frmVwJadwalSholat.origin.y + frmVwJadwalSholat.size.height + 16;
    
    frmVwBanner.size.width = screenWidth;
    
    frmVwBanner2.origin.x = 16;
    frmVwBanner2.size.width = screenWidth - (frmVwBanner2.origin.x * 2);
    
    //size height banner from android
    CGFloat heightBanner = (9*100/16) * frmVwBanner.size.width / 100; //NEW
    CGFloat heightBanner2 = (1*100/8) * frmVwBanner2.size.width / 100;
    
    frmVwBanner.size.height = heightBanner;
    
    /* Banner 2*/
    frmVwBanner2.size.height = heightBanner2;
    frmVwBanner2.origin.y = frmVwBanner.origin.y + frmVwBanner.size.height + 16;
    frmImagBanner2.origin.x = 0;
    frmImagBanner2.origin.y = 0;
    frmImagBanner2.size.height = frmVwBanner2.size.height;
    frmImagBanner2.size.width = (frmVwBanner2.size.width-16)/2;
    
    frmImagBanner3.origin.y = frmImagBanner2.origin.y;
    frmImagBanner3.origin.x = frmImagBanner2.origin.x + frmImagBanner2.size.width + 16;
    frmImagBanner3.size.height = frmImagBanner2.size.height;
    frmImagBanner3.size.width = frmImagBanner2.size.width;
    
    
    frmVwIcon.origin.y = frmVwBanner2.origin.y + frmVwBanner2.size.height + 16;
    
    frmImgPlace.origin.y = 0;
    frmImgPlace.origin.x = 0;
    frmImgPlace.size.height = frmVwBanner.size.height;
    frmImgPlace.size.width = frmVwBanner.size.width;
    
    frmWebView.size.height = frmVwBanner.size.height;
    frmWebView.size.width = frmVwBanner.size.width;
    frmWebView.origin.y = 0;
    
    frmVwIcon.size.width = screenWidth;
    
    CGFloat icWidth = ((screenWidth - 50)/4);
    CGFloat icHeight = icWidth;
    
    frmVwIconList1.size.width = frmVwIcon.size.width;
    frmVwIconList2.size.width = frmVwIcon.size.width;
    frmVwIconList3.size.width = frmVwIcon.size.width;
    frmVwIconList4.size.width = frmVwIcon.size.width;
    
    frmVwIconList1.size.height = icHeight;
    frmVwIconList2.size.height = icHeight;
    frmVwIconList3.size.height = icHeight;
    frmVwIconList4.size.height = icHeight;
    
    frmVwIconList1.origin.y = 5;
    frmVwIconList1.origin.x = 0;
    
    frmVwIconList2.origin.y = frmVwIconList1.origin.y + frmVwIconList1.size.height + 10;
    frmVwIconList2.origin.x = 0;
    
    frmVwIconList3.origin.y = frmVwIconList2.origin.y + frmVwIconList2.size.height + 10;
    frmVwIconList3.origin.x = 0;
    
    frmVwIconList4.origin.y = frmVwIconList3.origin.y + frmVwIconList3.size.height + 10;
    frmVwIconList4.origin.x = 0;
    
    frmVwIconInfoAccount.size.width = icWidth;
    frmVwIconInfoAccount.size.height = icHeight;
    frmVwIconInfoAccount.origin.x = 10;
    frmVwIconInfoAccount.origin.y = 0;
    
    frmVwIconTransfer.size.width = icWidth;
    frmVwIconTransfer.size.height = icHeight;
    frmVwIconTransfer.origin.x = 10 + frmVwIconInfoAccount.origin.x + frmVwIconInfoAccount.size.width;
    frmVwIconTransfer.origin.y = 0;
    
    frmVwIconPayement.size.width = icWidth;
    frmVwIconPayement.size.height = icHeight;
    frmVwIconPayement.origin.x = 10 + frmVwIconTransfer.origin.x + frmVwIconTransfer.size.width;
    frmVwIconPayement.origin.y = 0;
    
    frmVwIconPurchase.size.width = icWidth;
    frmVwIconPurchase.size.height = icWidth;
    frmVwIconPurchase.origin.x = 10 + frmVwIconPayement.origin.x + frmVwIconPayement.size.width;
    frmVwIconPurchase.origin.y = 0;
    
    /** icons Sizing here */
    CGRect frameIcon = CGRectMake(0, 0, icWidth, icHeight);
    CGFloat row = 4; //row of icons
    frmVwIcon.size.height = (icHeight + 10)*row; //
    
    if([userDefault integerForKey:@"config_banner2_enable"] == 0){
        
        self.vwBanner2.hidden = YES;
        self.imgBanner2.hidden = YES;
        self.imgBanner3.hidden = YES;
        
        if((frmVwGreeting.size.height + frmVwJadwalSholat.size.height + frmVwBanner.size.height + frmVwIcon.size.height) < (screenHeight - BOTTOM_NAV - TOP_NAV)){
//            if([IPHONE_X || IPHONE_XS_MAX]){
            if([Utility isDeviceHaveNotch]){
                frmVwIcon.origin.y = screenHeight - frmVwIcon.size.height - BOTTOM_NAV_X - TOP_NAV + 40;
            }else{
                frmVwIcon.origin.y = screenHeight - frmVwIcon.size.height - BOTTOM_NAV - TOP_NAV;
            }
            frmVwBanner.origin.y = frmVwJadwalSholat.origin.y + frmVwJadwalSholat.size.height + (frmVwIcon.origin.y - (frmVwJadwalSholat.origin.y + frmVwJadwalSholat.size.height) - frmVwBanner.size.height)/2;
        } else {
            frmVwBanner.origin.y = frmVwJadwalSholat.origin.y + frmVwJadwalSholat.size.height + 16;
            frmVwIcon.origin.y = frmVwBanner.origin.y + frmVwBanner.size.height + 16;
        }
    }else{
        
        if([userDefault integerForKey:@"config_banner2_type"] == 2)
        {
            self.vwBanner2.frame = frmVwBanner2;
            self.imgBanner2.frame = frmImagBanner2;
            self.imgBanner3.frame = frmImagBanner3;
        }else{
            self.vwBanner2.frame = frmVwBanner2;
            CGRect frmOneBanner2 = self.imgBanner2.frame;
            frmOneBanner2.origin.x = 0;
            frmOneBanner2.origin.y = 0;
            frmOneBanner2.size.height = frmVwBanner2.size.height;
            frmOneBanner2.size.width = frmVwBanner2.size.width;
            self.imgBanner2.frame = frmOneBanner2;
            self.imgBanner3.hidden = YES;
        }

    }
    
    _imgBanner2.layer.cornerRadius = 5;
    _imgBanner2.layer.masksToBounds = YES;
    _imgBanner3.layer.cornerRadius = 5;
    _imgBanner3.layer.masksToBounds = YES;

    uiIcList1.frame = frmVwIconList1;
    uiIcList2.frame = frmVwIconList2;
    uiIcList3.frame = frmVwIconList3;
    uiIcList4.frame = frmVwIconList4;
    
    //col1
    uiVwIcInfoAccount.frame = frmVwIconInfoAccount;
    uiVwIcIslamicService.frame = frmVwIconInfoAccount;
    uiVwIcCashWithdrawal.frame = frmVwIconInfoAccount;
    uiVwIcScheduler.frame = frmVwIconInfoAccount;
    
    //col2
    uiVwIcOpenAccount.frame = frmVwIconTransfer;
    uiVwIcTransfer.frame = frmVwIconTransfer;
    uiVwIcZiswaf.frame = frmVwIconTransfer;
    uiVwIcFinancing.frame = frmVwIconTransfer;
    
    //col3
    uiVwIcPayment.frame = frmVwIconPayement;
    uiVwIcGoldWallet.frame = frmVwIconPayement;
    uiVwIcTopUp.frame = frmVwIconPayement;

    //col4
    uiVwIcEcommerce.frame = frmVwIconPurchase;
    uiVwIcPurchase.frame = frmVwIconPurchase;
    uiVwIcFavorite.frame = frmVwIconPurchase;
    

    iconInfoAccount.frame = frameIcon;
    iconQRPay.frame = frameIcon;
    iconOpenAccount.frame = frameIcon;
    iconTransfer.frame = frameIcon;
    iconZiswaf.frame = frameIcon;
    iconBlockCard.frame = frameIcon;
    iconTopup.frame = frameIcon;
    iconPayment.frame = frameIcon;
    iconFavorite.frame = frameIcon;
    iconPurchase.frame = frameIcon;
    iconCashWithdrawal.frame = frameIcon;
    iconEcommerce.frame = frameIcon;
    iconScheduler.frame = frameIcon;
    iconGoldWallet.frame = frameIcon;
    iconIslamicService.frame = frameIcon;
    iconFinancing.frame = frameIcon;
    
    btnInfoAccount.frame = frameIcon;
    btnQRPay.frame = frameIcon;
    btnOpenAccount.frame = frameIcon;
    btnTransfer.frame = frameIcon;
    btnZiswaf.frame = frameIcon;
    btnBlockCard.frame = frameIcon;
    btnTopUp.frame = frameIcon;
    btnPayment.frame = frameIcon;
    btnFavorite.frame = frameIcon;
    btnPurchase.frame = frameIcon;
    btnCashWithdrawal.frame = frameIcon;
    btnEcommerce.frame = frameIcon;
    btnScheduler.frame = frameIcon;
    btnGoldWallet.frame = frameIcon;
    btnIslamicService.frame = frameIcon;
    btnFinancing.frame = frameIcon;
    
    frmUIView.size.width = screenWidth;
    frmUIView.size.height = frmVwIcon.origin.y + frmVwIcon.size.height + 30;
    
    frmScrollView.size.height = screenHeight - frmScrollView.origin.y - BOTTOM_NAV;
    
    self.scrollView.frame = frmScrollView;
    self.uiView.frame = frmUIView;
    
    textFieldSearch.frame = frmVwSearch;
    self.vwGreeting.frame = frmVwGreeting;
    self.lblGreetings.frame = frmLblGreeting;
    self.vwJadwalSholat.frame = frmVwJadwalSholat;
    self.lblJadwalSholat.frame = frmLblJadwalSholat;
    self.lblLocation.frame = frmLblLocation;
    self.vwBanner1.frame  = frmVwBanner;
    self.imgBanner1.contentMode = UIViewContentModeCenter;
    self.imgBanner1.frame = frmImgPlace;
    self.webViewBanner.frame = frmWebView;
    self.vwIcons.frame = frmVwIcon;
   
    if(nShowSyariahPoint){
        vwSeparator.frame = frmVwSeparator;
        vwSyariahPoint.frame = frmVwSyariahPoint;
        [self createSyariahPointView];
    }
	
    self.vwGreeting.backgroundColor = UIColorFromRGB(const_color_basecolor);
    self.lblGreetings.backgroundColor = UIColorFromRGB(const_color_basecolor);
    self.vwJadwalSholat.backgroundColor = UIColorFromRGB(const_color_basecolor);
    self.uiView.backgroundColor = UIColorFromRGB(const_color_basecolor);
    self.vwIcons.backgroundColor = UIColorFromRGB(const_color_basecolor);
    self.vwBanner2.backgroundColor = UIColorFromRGB(const_color_basecolor);
    
    [self.scrollView setContentSize:CGSizeMake(self.uiView.frame.size.width, self.uiView.frame.size.height)];
    
    [self setupHandlerButton];
    
}

- (void) initSetup{
    uiIcList1 = [[UIView alloc]init];
    uiIcList2 = [[UIView alloc]init];
    uiIcList3 = [[UIView alloc]init];
    uiIcList4 = [[UIView alloc]init];
    
    uiVwIcInfoAccount = [[UIView alloc]init];
    uiVwIcQRPay = [[UIView alloc]init];
    uiVwIcOpenAccount = [[UIView alloc]init];
    uiVwIcScheduler = [[UIView alloc]init];
    uiVwIcTransfer = [[UIView alloc]init];
    uiVwIcZiswaf = [[UIView alloc]init];
    uiVwIcBlockCard = [[UIView alloc]init];
    uiVwIcPayment = [[UIView alloc]init];
    uiVwIcOpenAccount = [[UIView alloc]init];
    uiVwIcTopUp = [[UIView alloc]init];
    uiVwIcPurchase = [[UIView alloc]init];
    uiVwIcFavorite = [[UIView alloc]init];
    uiVwIcEcommerce = [[UIView alloc]init];
    uiVwIcScheduler = [[UIView alloc]init];
    uiVwIcIslamicService = [[UIView alloc]init];
    uiVwIcCashWithdrawal = [[UIView alloc]init];
    uiVwIcGoldWallet = [[UIView alloc]init];
	uiVwIcFinancing = [[UIView alloc]init];
    
    iconInfoAccount = [[UIImageView alloc]init];
    iconInfoAccount.contentMode = UIViewContentModeScaleAspectFit;
    iconQRPay = [[UIImageView alloc]init];
    iconQRPay.contentMode = UIViewContentModeScaleAspectFit;
    iconOpenAccount = [[UIImageView alloc]init];
    iconOpenAccount.contentMode = UIViewContentModeScaleAspectFit;
    iconTransfer = [[UIImageView alloc]init];
    iconTransfer.contentMode = UIViewContentModeScaleAspectFit;
    iconZiswaf = [[UIImageView alloc]init];
    iconZiswaf.contentMode = UIViewContentModeScaleAspectFit;
    iconBlockCard = [[UIImageView alloc]init];
    iconBlockCard.contentMode = UIViewContentModeScaleAspectFit;
    iconTopup = [[UIImageView alloc]init];
    iconTopup.contentMode = UIViewContentModeScaleAspectFit;
    iconPayment = [[UIImageView alloc]init];
    iconPayment.contentMode = UIViewContentModeScaleAspectFit;
    iconFavorite = [[UIImageView alloc]init];
    iconFavorite.contentMode = UIViewContentModeScaleAspectFit;
    iconPurchase = [[UIImageView alloc]init];
    iconPurchase.contentMode = UIViewContentModeScaleAspectFit;
    iconCashWithdrawal = [[UIImageView alloc]init];
    iconCashWithdrawal.contentMode = UIViewContentModeScaleAspectFit;
    iconScheduler = [[UIImageView alloc]init];
    iconScheduler.contentMode = UIViewContentModeScaleAspectFit;
    iconEcommerce = [[UIImageView alloc]init];
    iconEcommerce.contentMode = UIViewContentModeScaleAspectFit;
    iconIslamicService = [[UIImageView alloc]init];
    iconIslamicService.contentMode = UIViewContentModeScaleAspectFit;
    iconGoldWallet = [[UIImageView alloc]init];
    iconGoldWallet.contentMode = UIViewContentModeScaleAspectFit;
	iconFinancing = [[UIImageView alloc]init];
    iconFinancing.contentMode = UIViewContentModeScaleAspectFit;
    
    btnScheduler = [UIButton buttonWithType:UIButtonTypeSystem];
    btnEcommerce = [UIButton buttonWithType:UIButtonTypeSystem];
    btnInfoAccount = [UIButton buttonWithType:UIButtonTypeSystem];
    btnQRPay = [UIButton buttonWithType:UIButtonTypeSystem];
    btnOpenAccount = [UIButton buttonWithType:UIButtonTypeSystem];
    btnTransfer = [UIButton buttonWithType:UIButtonTypeSystem];
    btnZiswaf = [UIButton buttonWithType:UIButtonTypeSystem];
    btnBlockCard = [UIButton buttonWithType:UIButtonTypeSystem];
    btnTopUp= [UIButton buttonWithType:UIButtonTypeSystem];
    btnPayment = [UIButton buttonWithType:UIButtonTypeSystem];
    btnFavorite = [UIButton buttonWithType:UIButtonTypeSystem];
    btnPurchase = [UIButton buttonWithType:UIButtonTypeSystem];
    btnCashWithdrawal = [UIButton buttonWithType:UIButtonTypeSystem];
    btnIslamicService = [UIButton buttonWithType:UIButtonTypeSystem];
    btnEcommerce = [UIButton buttonWithType:UIButtonTypeSystem];
    btnScheduler = [UIButton buttonWithType:UIButtonTypeSystem];
    btnGoldWallet = [UIButton buttonWithType:UIButtonTypeSystem];
	btnFinancing = [UIButton buttonWithType:UIButtonTypeSystem];
    
    [uiVwIcInfoAccount addSubview:btnInfoAccount];
    [uiVwIcInfoAccount addSubview:iconInfoAccount];
    [uiVwIcTransfer addSubview:btnTransfer];
    [uiVwIcTransfer addSubview:iconTransfer];
    [uiVwIcPayment addSubview:btnPayment];
    [uiVwIcPayment addSubview:iconPayment];
    [uiVwIcPurchase addSubview:btnPurchase];
    [uiVwIcPurchase addSubview:iconPurchase];
    [uiVwIcFinancing addSubview:btnFinancing];
    [uiVwIcFinancing addSubview:iconFinancing];
    
    [uiVwIcIslamicService addSubview:btnIslamicService];
    [uiVwIcIslamicService addSubview:iconIslamicService];
    [uiVwIcZiswaf addSubview:btnZiswaf];
    [uiVwIcZiswaf addSubview:iconZiswaf];
    [uiVwIcCashWithdrawal addSubview:btnCashWithdrawal];
    [uiVwIcCashWithdrawal addSubview:iconCashWithdrawal];
    [uiVwIcFavorite addSubview:btnFavorite];
    [uiVwIcFavorite addSubview:iconFavorite];
    
    [uiVwIcOpenAccount addSubview:btnOpenAccount];
    [uiVwIcOpenAccount addSubview:iconOpenAccount];
    [uiVwIcBlockCard addSubview:btnBlockCard];
    [uiVwIcBlockCard addSubview:iconBlockCard];
    [uiVwIcTopUp addSubview:btnTopUp];
    [uiVwIcTopUp addSubview:iconTopup];
    [uiVwIcEcommerce addSubview:btnEcommerce];
    [uiVwIcEcommerce addSubview:iconEcommerce];
    [uiVwIcScheduler addSubview:btnScheduler];
    [uiVwIcScheduler addSubview:iconScheduler];
    
    [uiVwIcGoldWallet addSubview:iconGoldWallet];
    [uiVwIcGoldWallet addSubview:btnGoldWallet];

    [uiIcList1 addSubview:uiVwIcInfoAccount];
    [uiIcList1 addSubview:uiVwIcTransfer];
    [uiIcList1 addSubview:uiVwIcPayment];
    [uiIcList1 addSubview:uiVwIcPurchase];
    
    [uiIcList2 addSubview:uiVwIcIslamicService];
    [uiIcList2 addSubview:uiVwIcZiswaf];
    [uiIcList2 addSubview:uiVwIcGoldWallet];
    [uiIcList2 addSubview:uiVwIcFavorite];

    [uiIcList3 addSubview:uiVwIcCashWithdrawal];
    [uiIcList3 addSubview:uiVwIcOpenAccount];
    [uiIcList3 addSubview:uiVwIcTopUp];
    [uiIcList3 addSubview:uiVwIcEcommerce];
    
    [uiIcList4 addSubview:uiVwIcScheduler];
    [uiIcList4 addSubview:uiVwIcFinancing];

    [self.vwIcons addSubview:uiIcList1];
    [self.vwIcons addSubview:uiIcList2];
    [self.vwIcons addSubview:uiIcList3];
    [self.vwIcons addSubview:uiIcList4];


    textFieldSearch = [[UITextField alloc]init];
    [self.uiView addSubview:textFieldSearch];
    [textFieldSearch.layer setCornerRadius:5];
    textFieldSearch.backgroundColor = UIColorFromRGB(const_color_lightgray);
    [textFieldSearch setPlaceholder:@""];
    [textFieldSearch addTarget:self action:@selector(gotoSearch) forControlEvents:UIControlEventEditingDidBegin];
    textFieldSearch.inputView = [[UIView alloc]initWithFrame:CGRectZero];
    
    CGFloat height = 26;
    UIImageView *dropIcon = [[UIImageView alloc]init];
    [dropIcon setFrame:CGRectMake(0, 0, height, height)];
    [dropIcon setImage:[UIImage imageNamed:@"baseline_search_black_24pt"]];
    [dropIcon setTintColor:UIColorFromRGB(0x999999)];
    [dropIcon setContentMode:UIViewContentModeScaleToFill];
    
    UIView *sideView = [[UIView alloc]initWithFrame:CGRectMake(10, 0, height, height)];
    [sideView addSubview:dropIcon];
    [textFieldSearch setLeftViewMode:UITextFieldViewModeAlways];
    [textFieldSearch setLeftView:sideView];
    
    NSString *language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *fsearchCaption = @"";
    if([language isEqualToString:@"id"]){
        if([userDefault objectForKey:@"config_sfeature_captionid"]){
            fsearchCaption = [userDefault valueForKey:@"config_sfeature_captionid"];
        }
    }else{
        if([userDefault objectForKey:@"config_sfeature_captionid"]){
            fsearchCaption = [userDefault valueForKey:@"config_sfeature_captionen"];
        }
    }
    
}


- (void)gotoSearch{
    transitionCircSearch = [[CircleTransition alloc] init];
    SearchItemViewController *showup = [self.storyboard instantiateViewControllerWithIdentifier:@"SRCHITM"];
    [showup setSize:CGRectMake(textFieldSearch.frame.origin.x, textFieldSearch.frame.origin.y, textFieldSearch.frame.size.width, textFieldSearch.frame.size.height)];
    [showup setDelegate:self];
    showup.transitioningDelegate = self;
    showup.modalTransitionStyle = UIModalPresentationCustom;

    [self presentViewController:showup animated:YES completion:nil];
}

- (void) setupHandlerButton{
    [btnInfoAccount addTarget:self action:@selector(openInfoRek:) forControlEvents:UIControlEventTouchUpInside];
    [btnTransfer addTarget:self action:@selector(openTransfer:) forControlEvents:UIControlEventTouchUpInside];
    [btnPayment addTarget:self action:@selector(openPayment:) forControlEvents:UIControlEventTouchUpInside];
    [btnPurchase addTarget:self action:@selector(openPurchase:) forControlEvents:UIControlEventTouchUpInside];
    [btnIslamicService addTarget:self action:@selector(islamicService:) forControlEvents:UIControlEventTouchUpInside];
    [btnZiswaf addTarget:self action:@selector(openZiswaf:) forControlEvents:UIControlEventTouchUpInside];
    [btnOpenAccount addTarget:self action:@selector(bukaRekeningClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnFavorite addTarget:self action:@selector(openFavoriteIcon:) forControlEvents:UIControlEventTouchUpInside];
    [btnCashWithdrawal addTarget:self action:@selector(openCashWithdrwal:) forControlEvents:UIControlEventTouchUpInside];
//    [btnBlockCard addTarget:self action:@selector(openBlockCard:) forControlEvents:UIControlEventTouchUpInside];
    [btnTopUp addTarget:self action:@selector(openTopUp:) forControlEvents:UIControlEventTouchUpInside];
    [btnEcommerce addTarget:self action:@selector(openEcommerce:) forControlEvents:UIControlEventTouchUpInside];
    [btnScheduler addTarget:self action:@selector(openScheduler:) forControlEvents:UIControlEventTouchUpInside];
    [btnFinancing addTarget:self action:@selector(openFinancing:) forControlEvents:UIControlEventTouchUpInside];
    [btnGoldWallet addTarget:self action:@selector(openGold:) forControlEvents:UIControlEventTouchUpInside];
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;

    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    return size.height;
}

- (void)showButtonLogout {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    int posX = 0;
    if ([Utility isDeviceHaveNotch]) {
        posX = screenWidth-60;
    } else {
        posX = screenWidth-40;
    }
    
    buttonLogout = [[UIButton alloc]initWithFrame:CGRectMake(posX, 37, 30, 30)];
    [buttonLogout setImage:[UIImage imageNamed:@"power-button-off"] forState:UIControlStateNormal];
    [buttonLogout addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:buttonLogout];
    [buttonLogout bringSubviewToFront:self.view];
    
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
        [buttonLogout setHidden:YES];
    } else {
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"YES"]) {
                [buttonLogout setHidden:NO];
            } else {
                [buttonLogout setHidden:YES];
            }
        } else {
            [buttonLogout setHidden:YES];
        }
    }
}

- (void)logout {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:valTitle
                          message:valMsg
                          delegate:nil
                          cancelButtonTitle:valCancel
                          otherButtonTitles:valAgree, nil];
    alert.delegate = self;
    alert.tag = 12345;
    [alert show];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString *requestPath = [[navigationAction.request URL] absoluteString];
    
    if ([requestPath isEqualToString:SLIDER_URL] || [requestPath isEqualToString:[NSString stringWithFormat:@"%@#",SLIDER_URL]]) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:requestPath] options:@{} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
}


- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    if (!self.imgBanner1.hidden) {
        self.imgBanner1.hidden = true;
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if (nAlertWasShownOnScrollHome == NO) {
        if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
            [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
        }
        [[thisVersion dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
    }
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    NSLog(@"%@", [error localizedDescription]);
    if (self.imgBanner1.hidden) {
        self.imgBanner1.hidden = false;
    }
}

#pragma mark burekol
-(void) requestBurekol {
    AppDelegate *mainClass = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
//    NSString *flagSmsSent = [userDefault objectForKey:@"sms_sent"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
//        if(flagSmsSent == nil||[flagSmsSent isEqualToString:@""]){
            if ([[userDefault objectForKey:@"is_jail_break"]boolValue]) {
                if (![[userDefault valueForKey:@"config_onboarding"] boolValue]){
                    [UIView animateWithDuration:1.f delay:1.f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                        mainClass.splashImageView.alpha = .0f;
                    } completion:^(BOOL finished){
                        if (finished) {
                            [mainClass.splashImageView removeFromSuperview];
                        }
                    }];
                    [self checkActivation];
                }else{
                    NSLog(@"config onboarding: %@", [userDefault objectForKey:ONBOARDING_NEED_CHECK_KEY]);
                    if([userDefault objectForKey:ONBOARDING_NEED_CHECK_KEY]){
                        [self checkOnboarding];
                    }
                }
            }
//        }
        
    }else{
        [UIView animateWithDuration:1.f delay:1.f options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            mainClass.splashImageView.alpha = .0f;
        } completion:^(BOOL finished){
            if (finished) {
                [mainClass.splashImageView removeFromSuperview];
            }
        }];
        [self showGreeting];
    }
    
    if([userDefault integerForKey:@"config_banner2_enable"] == 1){
        [self loadBannerView2];
    }
}

#pragma mark - adding function check onboarding
-(void)checkOnboarding
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:ONBOARDING_NEED_CHECK_KEY] boolValue]) {
            [CustomerOnboardingData getToken];
            dispatch_group_t dispatchGroup = dispatch_group_create();
            Encryptor *encryptor = [[Encryptor alloc] init];
            [encryptor removeUserDefaultsObjectForKey:ONBOARDING_BEGIN_SCREEN_KEY];
            [encryptor removeUserDefaultsObjectForKey:ONBOARDING_REKENING_INFO_KEY];
            [encryptor removeUserDefaultsObjectForKey:ONBOARDING_TOKEN_KEY];
//            [encryptor setUserDefaultsObject:@"InformasiPribadiScreen" forKey:ONBOARDING_PROGRESS_STEP_KEY];
//            [encryptor removeVideoAssist];
            [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
            [CustomerOnboardingData getToken];

            // INIT MSMSID
            if(!ONBOARDING_ENABLE_UNIQUE_DEVICE && ![encryptor getUserDefaultsObjectForKey:ONBOARDING_MSMSID_KEY])
            {
                [CustomerOnboardingData removeForKeychainKey:ONBOARDING_MSMSID_KEY];
            }


            if(![CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY])
            {
                NSUUID *uuid = [NSUUID UUID];
                NSString *stringUUID = [uuid UUIDString];

                [encryptor setUserDefaultsObject:stringUUID forKey:ONBOARDING_MSMSID_KEY];
                [CustomerOnboardingData setValue:stringUUID forKey:ONBOARDING_MSMSID_KEY];
            }
            else [encryptor setUserDefaultsObject:[CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY] forKey:ONBOARDING_MSMSID_KEY];

            // CHECK VIDEO ASSIST VERSION
            NSMutableDictionary *checkVersionResult = [[NSMutableDictionary alloc] init];
            [CustomerOnboardingData getFromEndpoint:@"api/setting/getVideoVer" dispatchGroup:dispatchGroup returnData:checkVersionResult];

            // CHECK COB ENABLED
            NSMutableDictionary *checkCobResult = [[NSMutableDictionary alloc] init];
            [CustomerOnboardingData getFromEndpoint:@"api/setting/get?key=COB_ENABLED" dispatchGroup:dispatchGroup returnData:checkCobResult];

            // CHECK IF MSMSID DID APPLIED
            NSString *msmsid = [CustomerOnboardingData valueForKeychainKey:ONBOARDING_MSMSID_KEY];
            NSMutableDictionary *checkMsmsidResult = [[NSMutableDictionary alloc] init];
            [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/nasabah/retrieveByUuid?uuid=%@", msmsid] dispatchGroup:dispatchGroup returnData:checkMsmsidResult];

            // CHECK COB TIME
            NSMutableDictionary *checkTimeResult = [[NSMutableDictionary alloc] init];
            NSString *mobversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            [CustomerOnboardingData getFromEndpoint:[NSString stringWithFormat:@"api/setting/getCobTimesNew?mobver=%@", mobversion] dispatchGroup:dispatchGroup returnData:checkTimeResult];

            dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{
                BOOL isNeedRedirect = YES;
                BOOL isMaintenance = NO;
                if([[checkCobResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
                {
                    NSDictionary *data;
                    if([[checkCobResult objectForKey:@"data"] isKindOfClass:NSArray.class])
                        data = [(NSArray *)[checkCobResult objectForKey:@"data"] objectAtIndex:0];
                    else
                        data = [checkCobResult objectForKey:@"data"];
                    BOOL cobEnabled = [[data objectForKey:@"setval"] isEqualToString:@"true"] ? YES : NO;
                    if(!cobEnabled || ([[checkTimeResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]] && ![[[checkTimeResult objectForKey:@"data"] objectForKey:@"iscobtime"] isEqualToNumber:[NSNumber numberWithBool:YES]]))
                    {
                        // DO NOT REDIRECT
                        isMaintenance = YES;
                        isNeedRedirect = NO;
                    }
                }

                // DOWNLOAD VIDEO ASSIST
                if([[checkVersionResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
                {
                    BOOL needUpdateVideoAssist = YES;
                    NSInteger videoAssistVersion = 1;
                    if([encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEASSIST_VERSION])
                        videoAssistVersion = [[encryptor getUserDefaultsObjectForKey:ONBOARDING_VIDEASSIST_VERSION] integerValue];

                    if(videoAssistVersion == [[checkVersionResult objectForKey:@"data"] integerValue])
                        needUpdateVideoAssist = NO;
                    else
                        videoAssistVersion = [[checkVersionResult objectForKey:@"data"] integerValue];

                    if(needUpdateVideoAssist)
                    {
                        PembukaanAkunAwalController *paac = [[PembukaanAkunAwalController alloc] initWithVideoAssistVersion:videoAssistVersion];
                        [paac downloadVideoAssistVersion];
                    }
                }

                if(isNeedRedirect)
                {
                    if([[checkMsmsidResult objectForKey:@"is_success"] isEqual:[NSNumber numberWithBool:YES]])
                    {
                        NSMutableDictionary *nasabah = [checkMsmsidResult objectForKey:@"data"];
                        NSString *status = [nasabah objectForKey:@"status"];
                        Rekening *rekening = [[Rekening alloc] init];
                        [rekening setNamaNasabah:[nasabah objectForKey:@"nama_lgkp"]];
                        [rekening setNomorRekening:[nasabah objectForKey:@"norek"]];
                        [rekening setJenisTabungan:[nasabah objectForKey:@"jenis_tabungan"]];
                        [rekening setJenisKartu:[nasabah objectForKey:@"jenis_kartu"]];
                        [rekening setKodeCabang:[nasabah objectForKey:@"kode_cabang"]];
                        [rekening setJenisKyc:[nasabah objectForKey:@"jenis_kyc"]];

                        [CustomerOnboardingData removeForKeychainKey:ONBOARDING_REKENING_INFO_KEY];
                        [CustomerOnboardingData setObject:rekening forKey:ONBOARDING_REKENING_INFO_KEY];

                        OnboardingTemplate01ViewController *templateVC = [[OnboardingTemplate01ViewController alloc] init];
                        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];

                        if([status isEqualToString:@"APPROVED"])
                        {
                            [encryptor removeVideoAssist];
                            
                            // NSString* step = [encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY];
                            NSDictionary *nasabahData = [checkMsmsidResult objectForKey:@"data"];
                            [encryptor setUserDefaultsObject:nasabahData forKey:ONBOARDING_NASABAH];
                            // GO TO OTP Create PIN
                            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Onboarding" bundle:nil];
                            UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"InformasiNoHandphoneScreen"];
                            vc.modalPresentationStyle = UIModalPresentationFullScreen;
                            [self presentViewController:vc animated:YES completion:nil];
                        }
                        else if([status isEqualToString:@"REJECTEDVIDEO"])
                        {
                            // CHECK IF LOCAL DATA STILL EXISTS
                            if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] && ![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY])
                                [encryptor restoreDataToLocalUsingData:nasabah];

                            // GO TO GagalVerifikasi Screen
                            [encryptor setUserDefaultsObject:@"GagalVerifikasiVideo" forKey:ONBOARDING_BEGIN_SCREEN_KEY];
                            GagalVerifikasiController *vc = [[GagalVerifikasiController alloc] initForRejectVideoWithRekening:rekening];
                            [templateVC setChildController:vc];
                            [self presentViewController:templateVC animated:YES completion:nil];
                        }
                        else if([status isEqualToString:@"NEW"] && ([[encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"DONE"] || ![encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY]))
                        {
                            // CHECK EXPIRY DATE
                            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSString *expiryString = [nasabah objectForKey:@"expiry_date"];
                            NSDate *now = [NSDate date];
                            NSDate *expiryDate = [dateFormatter dateFromString: [NSString stringWithFormat:@"%@ 23:59:59", expiryString]];

                            if([expiryDate compare:now] == NSOrderedDescending)
                            {
                                // CHECK IF LOCAL DATA STILL EXISTS
                                if(![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_EMAIL_KEY] && ![encryptor getUserDefaultsObjectForKey:ONBOARDING_INFORMASIKONTAK_PHONE_KEY] && ![encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY])
                                {
                                    [encryptor restoreDataToLocalUsingData:nasabah];
                                }

                                NSString *metodeVerifikasi = [nasabah objectForKey:@"jenis_kyc"];
                                if([metodeVerifikasi isEqualToString:@"CABANG"])
                                {
                                    // GO TO NomorTiketScene
                                    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"NomorTiketScene"];
                                    [self presentViewController:vc animated:YES completion:nil];
                                }
                                else
                                {
                                    // GO TO VerifikasiOnlineSelesaiScene
                                    UIViewController *vc = [ui instantiateViewControllerWithIdentifier:@"VerifikasiOnlineSelesaiScene"];
                                    [self presentViewController:vc animated:YES completion:nil];
                                }
                            }
                            else
                            {
                                [encryptor resetFormData];
                                isNeedRedirect = NO;
                            }
                        }
                        else if ([status isEqualToString:@"REJECTED"])
                        {
                            if([[encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"DONE"] || [encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] == nil || [[encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"PilihMetodeScreen"] || [[encryptor getUserDefaultsObjectForKey:ONBOARDING_PROGRESS_STEP_KEY] isEqualToString:@"PilihUlangMetodeScreen"]) {
                                // GO TO GagalVerifikasi Screen
                                [encryptor setUserDefaultsObject:@"GagalVerifikasi" forKey:ONBOARDING_BEGIN_SCREEN_KEY];
                                GagalVerifikasiController *vc = [[GagalVerifikasiController alloc] initForRejectedWithRekening:rekening];
                                [templateVC setChildController:vc];
                                [self presentViewController:templateVC animated:YES completion:nil];
                            }
                            else {
                                isNeedRedirect = NO;
                            }
                        }
                        else
                        {
                            isNeedRedirect = NO;
                        }
                    }
                    else isNeedRedirect = NO;
                }



                AppDelegate *mainClass = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                    mainClass.splashImageView.alpha = .0f;
                } completion:^(BOOL finished){
                    DLog(@"ONBOARDING FINISHED");
                    if (finished) {
                        [mainClass.splashImageView removeFromSuperview];
                    }
                }];

                if(!isNeedRedirect)
                {
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//                    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
                    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
                    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
                        [self checkActivation];
                    } else {
                        [self showGreeting];
                    }
                }
            });
    }
    else {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//        NSString *flagCID = [userDefault objectForKey:@"customer_id"];
        NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
            [self checkActivation];
        } else {
            [self showGreeting];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated{
    // Setup FB MyMenu
    [UIView transitionWithView:self.vwFBMyMenu duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        NSString *mmSetting = [self->userDefault objectForKey:@"showMyMenu"];
        if([mmSetting isEqualToString:@"YES"]){
            [self->_vwFBMyMenu setHidden:NO];
        } else {
            [self->_vwFBMyMenu setHidden:YES];
        }
        
    } completion:nil];
    
    
//    [self requestBurekol];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *mAction = [userDefault objectForKey:@"action"];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    
    
    if (mAction == nil) {
        if ((cid == nil) || ([cid isEqualToString:@""])) {
            NSLog(@"Customer ID belum diaktifkan.");
        } else {
//            NSString *upwd = [userDefault objectForKey:@"password"];
            NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
            if ((upwd == nil) || ([upwd isEqualToString:@""])) {
                self.parentViewController.tabBarController.tabBar.hidden = YES;
                [self.slidingViewController resetTopViewAnimated:YES];
                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NCreatePwdVC"];
                templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateView animated:YES];
            } else {
                
                [self castCustomerName];
                
                
                if ([mustLogin isEqualToString:@"YES"]) {
                    if (![hasLogin isEqualToString:@"YES"]) {
                        
                        [userDefault setObject:@"LOGIN" forKey:@"VC"];
                        [userDefault synchronize];
                        
                        self.parentViewController.tabBarController.tabBar.hidden = NO;
                        [self.slidingViewController resetTopViewAnimated:YES];
                        /*TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
                        templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                        [currentVC pushViewController:templateView animated:YES];*/
                    }
                }
                [self reqLogin];
            }
//            [self reqLogin];
        }
        
        
    }else{
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager.dataExtra setValue:[userDefault valueForKey:@"mid"] forKey:@"mid"];
        
        self.parentViewController.tabBarController.tabBar.hidden = YES;
        [self.slidingViewController resetTopViewAnimated:YES];
        UIViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
        //        templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
        
        //push viewcontroller
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateView animated:YES];
        
    }
    
    [self setupSearchFeatureHint];
}

- (void) setupSearchFeatureHint{
    NSString *language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *fsearchCaption = @"";
    if([language isEqualToString:@"id"]){
        if([userDefault objectForKey:@"config_sfeature_captionid"]){
            fsearchCaption = [userDefault valueForKey:@"config_sfeature_captionid"];
        }
    }else{
        if([userDefault objectForKey:@"config_sfeature_captionen"]){
            fsearchCaption = [userDefault valueForKey:@"config_sfeature_captionen"];
        }
    }
    textFieldSearch.attributedPlaceholder = [[NSAttributedString alloc]initWithString:fsearchCaption attributes:@{NSFontAttributeName : [UIFont fontWithName:const_font_name1 size:13], NSForegroundColorAttributeName : UIColorFromRGB(0xA9A9A9)}];
}

-(void) requestBurroq{
    DLog(@"REQ INQ PUSH MESSAGE");
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *cid = [userDefault objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    if ((cid == nil) || ([cid isEqualToString:@""])) {
        NSLog(@"Customer ID belum diaktifkan.");
    } else {
//        NSString *upwd = [userDefault objectForKey:@"password"];
        NSString *upwd = [NSUserdefaultsAes getValueForKey:@"password"];
        if ((upwd != nil) || (![upwd isEqualToString:@""])) {
            if([[userDefault objectForKey:@"financing_message_pop"] boolValue]){
                if ([[userDefault objectForKey:@"config.enable.financing"]boolValue]){
                    [userDefault setObject:@(0) forKey:@"financing_message_pop"];
                    NSDictionary* userInfo = @{@"position": @(1126)};
                    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
                }
                
            }
        }
        
    }
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    [self callResetTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (TemplateViewController *)routeTemplateController:(NSString *)templateName {
    NSArray *sbList = [dataManager getStoryboardList];
    for (NSString* sbName in sbList) {
        @try {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:sbName bundle:nil];
            TemplateViewController *templateView = [storyboard instantiateViewControllerWithIdentifier:templateName];
            return templateView;
        } @catch (NSException *exception) {
            continue;
        }
    }
    return [self.storyboard instantiateViewControllerWithIdentifier:templateName];
}

- (void)openTemplateWithData:(int)position{
    [self callResetTimer];
    NSArray *temp = [[DataManager sharedManager] getJSONData:position];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        NSString *templateName = [object valueForKey:@"template"];
            @try {
//                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:templateName];
                TemplateViewController *templateView = [self routeTemplateController:templateName];
                [templateView setJsonData:object];
                if([templateName isEqualToString:@"LV01"]){
                    LV01ViewController *viewCont = (LV01ViewController *) templateView;
                    [viewCont setFirstLV:true];
                }
                bool stateAcepted = [Utility vcNotifConnection: templateName];
                if (stateAcepted) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    [self.navigationController pushViewController:templateView animated:YES];
                    
                }
            } @catch (NSException *exception) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
    }
    
}

-(void) goToCardLess{
    [self callResetTimer];
    [self getMenuIdx:@"btnCardless"];
    [self getMenuIdx:@"indexTransfer"];
    
    int indTrf = 20;
    if ([userDefault valueForKey:@"indexTransfer"]) {
        indTrf = [[userDefault valueForKey:@"indexTransfer"] intValue];
    }
    
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:indTrf];
    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        int ind = 0;
        if ([userDefault valueForKey:@"btnCardless"]) {
            ind = [[userDefault valueForKey:@"btnCardless"] intValue];
        }

        NSArray *nTempAction = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [nTempAction objectAtIndex:1];
        
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[nTempAction objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        
        [self pushNavTemplate:tempData];
        
    }else{

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)goToSomepage:(int)position{
    [self callResetTimer];
    NSArray *temp = [[DataManager sharedManager] getJSONData:position];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        
        NSString *templateName = [object valueForKey:@"template"];
        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:templateName];
        [templateView setJsonData:object];
        //    if([templateName isEqualToString:@"LV01"]){
        LV01ViewController *viewCont = (LV01ViewController *) templateView;
        [viewCont setFromHome:YES];
        //    }
        bool stateAcepted = [Utility vcNotifConnection: templateName];
        if (stateAcepted) {

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            [self.navigationController pushViewController:templateView animated:YES];
            
        }
    }else{

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

-(void)goToQR{
    [self callResetTimer];
    [[DataManager sharedManager]resetObjectData];
    NSArray *tempMain = [[DataManager sharedManager] getJSONData:3];
    if (tempMain !=nil) {
        NSDictionary *objectMain = [tempMain objectAtIndex:1];
        int ind = 0;
        NSArray *menu = [objectMain valueForKey:@"action"];
        for (int i =0; i < [menu count]; i++) {
            NSArray *selMenu = [[objectMain valueForKey:@"action"] objectAtIndex:i];
            if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
                ind = i;
                break;
            }
        }
        NSArray *temp = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [temp objectAtIndex:1];
        
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        [self pushNavTemplate:tempData];
    }else{

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)goToInbox{
    [self callResetTimer];
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:5];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition = 1;
        NSDictionary *tempData = [dataManager getObjectData];
        TemplateViewController *templateViews = [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            
            if([self checkActivationCallback]){

                [self.tabBarController setSelectedIndex:0];
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateViews animated:YES];

            }
            
        }
    }else{

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(void)pushNavTemplate : (NSDictionary *) tempData{
    @try {
        
        if ([self checkActivationCallback]){
        
        TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateViews animated:YES];
        }
            
        }
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    
}

- (void)openFavorite:(int)node{
    [self callResetTimer];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
//    if([userDefaults objectForKey:@"customer_id"]){
    if([NSUserdefaultsAes getValueForKey:@"customer_id"]){
        DataManager *dataManager = [DataManager sharedManager];
        //        NSArray *temp = [dataManager getJSONByCurrentMenu];
        NSLog(@"%@", [userDefaults objectForKey:@"actionFav"]);
        NSDictionary *object = [userDefaults objectForKey:@"actionFav"];
        NSLog(@"fav value: %@",  [dataManager.dataExtra objectForKey:@"payment_id"]);
        
        [dataManager setAction:object andMenuId:nil];
        [dataManager setCurrentPosition:node];
        NSDictionary *nextNode = [dataManager getObjectData];
        
        @try {
            NSString *templateName = [nextNode valueForKey:@"template"];
            TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:templateName];
            [templateView setJsonData:nextNode];
            [self.navigationController pushViewController:templateView animated:YES];
        } @catch (NSException *exception) {
            NSLog(@"Template Not Found");
        }
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(NSDictionary *) getNextNode : (int) node
                   jsonObject : (NSDictionary *) data{
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc]init];
    for(int i =0; i<[[data valueForKey:@"node"]count]; i++){
        if (node == i) {
            [dataDict addEntriesFromDictionary:@{@"content":[[data valueForKey:@"content"]objectAtIndex:i],@"favorite":[[data valueForKey:@"favorite"]objectAtIndex:i],@"social":[[data valueForKey:@"social"]objectAtIndex:i],@"template":[[data valueForKey:@"template"]objectAtIndex:i],@"title":[[data valueForKey:@"title"]objectAtIndex:i],@"url":[[data valueForKey:@"url"]objectAtIndex:i],@"url_parm":[[data valueForKey:@"url_parm"]objectAtIndex:i],@"node":[[data valueForKey:@"node"]objectAtIndex:i],@"activation":[[data valueForKey:@"activation"]objectAtIndex:i],@"field_name":[[data valueForKey:@"field_name"]objectAtIndex:i],@"mandatory":[[data valueForKey:@"mandatory"]objectAtIndex:i]}];
            break;
        }
    }
    return dataDict;
    
}

- (IBAction)openInfoRek:(id)sender {
    [[DataManager sharedManager]resetObjectData];
//    [userDefault removeObjectForKey:@"complain_state"];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_info_account_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:0];
}
- (IBAction)openPayment:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_payment_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:1];
}
- (IBAction)openPurchase:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_purchase_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:2];
}
- (IBAction)openTransfer:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_transfer_header.png" forKey:@"imgIcon"];
    
    if([self checkActivationCallback]){
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"mustLogin"] isEqualToString:@"YES"] &&
           [[[NSUserDefaults standardUserDefaults] valueForKey:@"hasLogin"] isEqualToString:@"NO"]){
            MenuViewController *menuVC = (MenuViewController *)self.slidingViewController.underLeftViewController;
            self.slidingViewController.topViewController = menuVC.homeNavigationController;
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PopUpLoginViewController *popLogin = [ui instantiateViewControllerWithIdentifier:@"PopupLogin"];
            [popLogin setDelegate:self];
            [popLogin setIdentfier:@"3"];
            [self presentViewController:popLogin animated:YES completion:nil];
        }else{
            [self openTemplateWithData:3];
        }
    }
}

- (IBAction)openCashWithdrwal:(id)sender {
    //    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_tarik_tunai_header.png" forKey:@"imgIcon"];
    [userDefault synchronize];
    //    [self openTemplateWithData:4];
//    NSString *img = [userDefault objectForKey:@"imgIcon"];
    [self goToCardLess];
}

- (IBAction)openZiswaf:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_header_berbagi.png" forKey:@"imgIcon"];
    [self openTemplateWithData:6];
}

- (IBAction)islamicService:(id)sender {
    TemplateViewController *templateView = [self routeTemplateController:@"FEISLAMIC"];
    [self.navigationController pushViewController:templateView animated:YES];
}

- (IBAction)qrPayment:(id)sender {
    [self getMenuIdx:@"btnQRPay"];
    [[DataManager sharedManager]resetObjectData];
    NSArray *tempMain = [[DataManager sharedManager] getJSONData:3];
    if(tempMain != nil){
        NSDictionary *objectMain = [tempMain objectAtIndex:1];
        int ind = 2;
        //NSArray *menu = [objectMain valueForKey:@"action"];
        if ([userDefault valueForKey:@"btnQRPay"]) {
            ind = [[userDefault valueForKey:@"btnQRPay"] intValue];
        }
        
        NSArray *temp = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [temp objectAtIndex:1];
        // NSString *templateNames = [object valueForKey:@"template"];
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:@"ic_qrpay_header.png" forKey:@"imgIcon"];
        if([[tempData valueForKey:@"activation"]boolValue]){
//            if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
            if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *alertTitle = @"";
                NSString *alertMessage = @"";
                if ([lang isEqualToString:@"en"]) {
                    alertTitle = @"Information";
                    alertMessage = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
                } else {
                    alertTitle = @"Informasi";
                    alertMessage = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
                }

                UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            } else {
                TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
                [templateViews setJsonData:tempData];
                bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"] ];
                if (stateAcepted) {

                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    [self.navigationController pushViewController:templateViews animated:YES];
                    
                }
            }
        } else {
            TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
            [templateViews setJsonData:tempData];
            bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"] ];
            if (stateAcepted) {

                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                [self.navigationController pushViewController:templateViews animated:YES];
                
            }
        }
    }else{

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}
- (IBAction)openBlockCard:(id)sender {
    [self callResetTimer];
    //[self callMenuOn:@"CardManajemen" withnode:@"btnBlockCard"];
    
    if([self checkActivationCallback]){
        [[DataManager sharedManager]resetObjectData];
        NSArray *temp = [[DataManager sharedManager] getJSONData:7]; //array menu ke 7
        if(temp != nil){
            NSDictionary *objectMain = [temp objectAtIndex:1];
            int ind = 0; //node ke 0

            NSArray *nTempAction = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
            NSDictionary *object = [nTempAction objectAtIndex:1];

            DataManager *dataManager = [DataManager sharedManager];
            [dataManager setAction:[object objectForKey:@"action"] andMenuId:[nTempAction objectAtIndex:0]];
            dataManager.currentPosition++;
            NSDictionary *tempData = [dataManager getObjectData];

            [self pushNavTemplate:tempData];

        }else{

            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)callMenuOn:(NSString *)menuIdx withnode:(NSString *)nIdx {
    //method definition...
//    [self getMenuIdx:menuIdx];
//    [self getMenuIdx:nIdx];
    [self getMenuIdx2:menuIdx withKey:nIdx];
    
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:(int)[userDefault integerForKey:menuIdx]];
    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        int ind = (int)[userDefault integerForKey:nIdx];
        
        NSArray *nTempAction = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [nTempAction objectAtIndex:1];
        
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[nTempAction objectAtIndex:0]];
        dataManager.currentPosition++;
        NSDictionary *tempData = [dataManager getObjectData];
        [userDefault removeObjectForKey:@"deeplink_nodeindex"];
        [self pushNavTemplate:tempData];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)loginDoneState:(NSString *)identifier{
    [self openTemplateWithData:[identifier intValue]];
}

- (IBAction)openTopUp:(id)sender {
    [self callResetTimer];
    if([self checkActivationCallback]){
        [userDefault setObject:@"ic_payment_header.png" forKey:@"imgIcon"];
        [self callMenuOn:@"indexPurchase" withnode:@"btnTopup"];
    }
}
- (IBAction)bukaRekeningClicked:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_open_account_header.png" forKey:@"imgIcon"];
    [self openTemplateWithData:4];
}

-(IBAction)openEcommerce:(id)sender{
    [[DataManager sharedManager]resetObjectData];
    [dataManager setMenuId:@"00043"];
    [userDefault setObject:@"ic_info_account_header.png" forKey:@"imgIcon"];
    [self openTemplate: [[DataManager sharedManager] getJSONByCurrentMenu]];
}

-(IBAction)openScheduler:(id)sender{
    [[DataManager sharedManager]resetObjectData];
    [dataManager setMenuId:@"00091"];
    [userDefault setObject:@"ic_info_account_header.png" forKey:@"imgIcon"];
    [self openTemplate: [[DataManager sharedManager] getJSONByCurrentMenu]];
}

-(IBAction)openFinancing:(id)sender{
    [[DataManager sharedManager]resetObjectData];
    [self openTemplateByMenuID:@"00174"];
}

- (IBAction)openGold:(id)sender{
    [self callResetTimer];
    
    UIStoryboard *uistor = [UIStoryboard storyboardWithName:@"Gadaiemas" bundle:nil];

    GELV01MenuViewController *openList = [uistor instantiateViewControllerWithIdentifier:@"GELV01"];
    [self.tabBarController setSelectedIndex:0];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:openList animated:YES];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 229) {
        if (buttonIndex == 0) {
            
            
        }else if (buttonIndex == 1) {
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"NO" forKey:@"fromSliding"];

            
            [userDefault synchronize];
            
            if (self.parentViewController.tabBarController.tabBar.hidden) {
                self.parentViewController.tabBarController.tabBar.hidden = NO;
            }
            [self.tabBarController setSelectedIndex:0];
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC popToRootViewControllerAnimated:NO];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
            
        }
    }else if (alertView.tag == 230) {
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 231;
            [alert show];
        } else if (buttonIndex == 1) {
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
        }
    }else if (alertView.tag == 231) {
        
    }else if (alertView.tag == 12345) {
        if (buttonIndex == 0) {
            //CANCEL Button
        } else {
            //[self stopIdleTimer];
            
            //OK Button
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
            [userDefault setObject:@"NO" forKey:@"hasLogin"];
            [userDefault setObject:nil forKey:@"req_login"];
            [userDefault synchronize];
            
            if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
                CGRect screenBound = [[UIScreen mainScreen] bounds];
                CGSize screenSize = screenBound.size;
                CGFloat screenWidth = screenSize.width;
                CGFloat screenHeight = screenSize.height;
                
                self.parentViewController.tabBarController.tabBar.hidden = YES;
                [self.slidingViewController resetTopViewAnimated:YES];
                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
                templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateView animated:YES];
            } else {
                exit(0);
            }
        }
    }
    else if (alertView.tag == 505) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *mustLogin = [userDefault objectForKey:@"mustLogin"];
        [userDefault setObject:@"NO" forKey:@"hasLogin"];
        [userDefault synchronize];
        
        if ([[mustLogin uppercaseString] isEqualToString:@"YES"]) {
            CGRect screenBound = [[UIScreen mainScreen] bounds];
            CGSize screenSize = screenBound.size;
            CGFloat screenWidth = screenSize.width;
            CGFloat screenHeight = screenSize.height;
            
            self.parentViewController.tabBarController.tabBar.hidden = YES;
            [self.slidingViewController resetTopViewAnimated:YES];
            TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"NLoginVC"];
            templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
            UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
            [currentVC pushViewController:templateView animated:YES];
        } else {
            exit(0);
        }
    }
    else if(alertView.tag == 007){
        [self directToActivation];
    }
}

-(void) directToActivation{
    
    NSDictionary* userInfo = @{@"position": @(513)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}

-(void)callResetTimer{
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    userInfo = @{@"position": @(1112)};
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}



- (IBAction)openFavoriteIcon:(id)sender {
    [[DataManager sharedManager]resetObjectData];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"ic_favorite.png" forKey:@"imgIcon"];
    TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:@"FavoriteVC"];
    bool stateAcepted = [Utility vcNotifConnection:@"FavoriteVC"];
    if (stateAcepted) {

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self.navigationController pushViewController:templateView animated:YES];
    }
    
}

-(void)getMenuIdx2 : (NSString *) key1 withKey :(NSString *)key2{
    NSString *url =[NSString stringWithFormat:@"%@menu_index_ios.html?vn=%@",API_URL,@VERSION_NAME];
    NSLog(@"%@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *urlResponse, NSError *error){
            if(error == nil){
                NSDictionary *mJsonObject = [NSJSONSerialization
    //                                         JSONObjectWithData:mData
                                             JSONObjectWithData:data
                                             options:kNilOptions
                                             error:&error];
                
                if(mJsonObject){
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setValue:[mJsonObject valueForKey:key1] forKey:key1];
                    [userDefault setValue:[mJsonObject valueForKey:key2] forKey:key2];
                    [userDefault synchronize];
                }
                
            }
        }]resume];
    
}

-(void)getMenuIdx : (NSString *) key {
    DLog(@"GET MENU IDX IN HOME");
    
    NSString *url =[NSString stringWithFormat:@"%@menu_index_ios.html?vn=%@",API_URL,@VERSION_NAME];
    NSLog(@"%@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:URL_TIME_OUT];
    [request setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            if(error == nil){
                NSDictionary *mJsonObject = [NSJSONSerialization
    //                                         JSONObjectWithData:mData
                                             JSONObjectWithData:data
                                             options:kNilOptions
                                             error:&error];
                
                if(mJsonObject){
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setValue:[mJsonObject valueForKey:key] forKey:key];
                    [userDefault synchronize];
                    DLog(@"GET MENU IDX IN HOME DONE");
                }
                
            }
        }]resume];
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"login"]){
        DLog(@"CONNECT TO REQ LOGIN DONE");
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
//                [userDefault setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
                [NSUserdefaultsAes setObject:[jsonObject valueForKey:@"clearZPK"] forKey:@"zpk"];
                [userDefault setObject:@"HOME" forKey:@"tabPos"];
                [userDefault setObject:@"HOME" forKey:@"VC"];
                [userDefault setValue:@"NO" forKey:@"isExist"];
                [userDefault synchronize];
                
            }else{
                DLog(@"CONNECT TO REQ LOGIN NOT SUCCESS");
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"0001"]){
                    alert.tag = 007;
                    [alert show];
                }else{
                }
                
            }
        }
        if ([[userDefault objectForKey:@"config.enable.financing"]boolValue]){
            NSDictionary* userInfo = @{@"position": @(1126)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        }
    }
    else if([requestType isEqualToString:@"inq_push_message"]){
        DLog(@"CONNECT INQ PUSH MESSAGE DONE");

        if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
            
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *mDictMessage = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            if (mDictMessage) {
                NSString *dataMessage = @"";
                dataMessage = [dataMessage stringByAppendingString:[mDictMessage valueForKey:@"title"]];
                dataMessage = [dataMessage stringByAppendingString:@"\n"];
                dataMessage = [dataMessage stringByAppendingString:[mDictMessage valueForKey:@"content"]];

                dataMessageBurroq = dataMessage;
                findId = [mDictMessage valueForKey:@"id"];

                NSUserDefaults *userD = [NSUserDefaults standardUserDefaults];
                NSLog(@"%@",[userD objectForKey:@"show.burroq.enable"]);
                if([userD objectForKey:@"show.burroq.enable"] == nil || [[userD valueForKey:@"show.burroq.enable"]isEqualToString:@"YES"]){
                    
                    PopupBurroqViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"POPBURROQ"];
                    [viewCont DataMessage:dataMessage];
                    [viewCont findID:findId];
                    [self presentViewController:viewCont animated:YES completion:nil];
                }
                
            }
            
        }

    }
    
}

- (void)selectedFeature:(int)index{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    switch (index) {
        case 0:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 1){
                [self.tabBarController setSelectedIndex:1];
            }
        }
            break;
        case 1:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"MasjidVC"];
        }
            break;
        case 2:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
             [self openStatic:@"QiblatVC"];
        }
            break;
        case 4:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            ListSurahViewController *mSurrah = (ListSurahViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"LISTSURAHVC"];
            if([language isEqualToString:@"id"]){
                [mSurrah setTitleMenu:@"Pilih Ayat"];
            }else{
                [mSurrah setTitleMenu:@"Choose Verse"];
            }
            [self.navigationController pushViewController:mSurrah animated:YES];
        }
            break;
        case 3:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            JuzAmmaViewController *mJuzamma = (JuzAmmaViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"JUZAMMAVC"];
            [mJuzamma setTitleMenu:@"Juz Amma"];
            [self.navigationController pushViewController:mJuzamma animated:YES];
        }
            break;
            
        case 5:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            AsmaulHusnaViewController *mAsmaulHusnahVc = (AsmaulHusnaViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ASMHUSVC"];
            [mAsmaulHusnahVc setTitleMenu:@"Asmaul Husna"];
            [self.navigationController pushViewController:mAsmaulHusnahVc animated:YES];
        }
            break;
            
        case 6:{
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            TausiahViewController *mTausiah = (TausiahViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"TausiahVC"];
            if([language isEqualToString:@"id"]){
                [mTausiah setTitleMenu:@"Hikmah"];
            }else{
                [mTausiah setTitleMenu:@"Quotes"];
            }
            [self.navigationController pushViewController:mTausiah animated:YES];
        }break;
            
        case 7:{//Registrasi Notifikasi SMS // menuid 00128 / 00132
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"YES" forKey:@"skippingcontent"];
            [[DataManager sharedManager].dataExtra setValue:@"00132" forKey:@"code"];
            [self openTemplateByMenuID:@"00128" andCodeContent:@"00132"];
        }break;
        case 8:{//favorite, pembayaran
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            FavoriteDetailViewController *favDetail = (FavoriteDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"FVRTDETAIL"];
            [favDetail setSelectedIndex:0];
            [self.navigationController pushViewController:favDetail animated:YES];
        }break;
        case 9:{//favorite, transfer
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            FavoriteDetailViewController *favDetail = (FavoriteDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"FVRTDETAIL"];
            [favDetail setSelectedIndex:1];
            [self.navigationController pushViewController:favDetail animated:YES];
        }break;
        case 10:{//favorite, transfer
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            FavoriteDetailViewController *favDetail = (FavoriteDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"FVRTDETAIL"];
            [favDetail setSelectedIndex:2];
            [self.navigationController pushViewController:favDetail animated:YES];
        }break;
        case 11:{//Keyboard
            NSString *msg = @"";
            NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
            if([[[userdefaults objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
                msg = @"Fitur belum tersedia";
            }else{
                msg = @"Feature is not available";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }break;
        case 12:{//Blokir Kartu Open Menuid 00087
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openTemplateByMenuID:@"00087"];
        }break;
        case 13:{//Call 14040
            NSString *strBtnYes = @"";
            NSString *strBtnNo = @"";
            NSString *strMsg = @"";
            NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
            if([[[userdefaults objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
                strBtnNo = @"Tidak";
                strBtnYes = @"Ya";
                strMsg = @"Anda akan dihubungkan dengan Call Center untuk pendaftaran BSI Mobile";
            }else{
                strBtnNo = @"No";
                strBtnYes = @"Yes";
                strMsg = @"Your call will be forwarded to Call Center for BSI Mobile Registration";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:strMsg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:strBtnYes style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                UIApplication *application = [UIApplication sharedApplication];
                NSURL *URL = [NSURL URLWithString:@"tel://14040"];
                [application openURL:URL options:@{} completionHandler:^(BOOL success) {}];
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:strBtnNo style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }break;
        case 14:{//Daftar Sanggahan Transaksi
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            ComplainViewController *popBantuan = [self.storyboard instantiateViewControllerWithIdentifier:@"COMPLAIN"];
            [self.navigationController pushViewController:popBantuan animated:YES];

        }break;
        case 15:{//Aisyah
            NSUserDefaults *userd = [NSUserDefaults standardUserDefaults];
            NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];

//            if([[userd objectForKey:@"email"]isEqualToString:@""] || [userd objectForKey:@"email"] == nil){
            if([email isEqualToString:@""] || email == nil){

                NSString *chatMessage = @"";
                
                if([[[userd objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
                    chatMessage = @"Email tidak boleh kosong, harap isi email di menu pengaturan.";
                }else{
                    chatMessage = @"Email cannot be empty, please fill your email in setting menu.";
                }
                
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:chatMessage preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alerts animated:YES completion:nil];
                
            }else{
                [self.navigationController popToRootViewControllerAnimated:YES];
                if(self.tabBarController.selectedIndex != 0){
                    [self.tabBarController setSelectedIndex:0];
                }
                
                WebviewViewController *webViewAisa = [self.storyboard instantiateViewControllerWithIdentifier:@"webviewIdentifier"];
                [self.navigationController pushViewController:webViewAisa animated:YES];
                    
            }
            
        }break;
        case 16:{//Lokasi ATM
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"0" forKey:@"atmbranchloc"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 3){
                [self.tabBarController setSelectedIndex:3];
            }
        }break;
        case 17:{//Lokasi Branch
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"1" forKey:@"atmbranchloc"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 3){
                [self.tabBarController setSelectedIndex:3];
            }
        }break;
        case 18:{//Info Kurs
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"0" forKey:@"kursgoldvc"];
            [self openStatic:@"KursGoldVC"];
        }break;
        case 19:{//Info Emas
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"1" forKey:@"kursgoldvc"];
            [self openStatic:@"KursGoldVC"];
        }break;
        case 20:{//Info Limit
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"IT01"];
        }break;
        case 21:{//List Notifikasi
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"1" forKey:@"inboxparam"];
            [self gotoInbox];
        }break;
        case 22:{//List Resi
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [[DataManager sharedManager].dataExtra setValue:@"0" forKey:@"inboxparam"];
            [self gotoInbox];
        }break;
        case 23:{//Medsos
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"SMVC"];
        }break;
        case 24:{//Aktivasi
//            if([[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
            if([NSUserdefaultsAes getValueForKey:@"customer_id"]){
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_DONE") preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                if (@available(iOS 13.0, *)) {
                    [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                } 
                [self presentViewController:alerts animated:YES completion:nil];
            }else{
//                [self goToPopupActivation];

                if([[DataManager sharedManager].dataExtra objectForKey:@"activationFromLink"]){
//                    [[DataManager sharedManager].dataExtra removeObjectForKey:@"activationFromLink"];
//                    UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UIViewController *pop = [self topViewController];
                    UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
                    self.slidingViewController.topViewController = viewCont;
                    [pop dismissViewControllerAnimated:YES completion:nil];
                }else{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    if(self.tabBarController.selectedIndex != 0){
                        [self.tabBarController setSelectedIndex:0];
                    }
                    [self openStatic:@"ActivationVC"];
                }
            }
        }break;
        case 25:{//Aktivasi Ulang
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ActivationRetrival"];
        }break;
        case 26:{//Change Password
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ASVC"];
        }break;
        case 27:{//Change Pin
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ChangePINVC"];
        }break;
        case 28:{//Change Language
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ChangeLngVC"];
        }break;
        case 29:{//Email
            [self.navigationController popToRootViewControllerAnimated:YES];
            if(self.tabBarController.selectedIndex != 0){
                [self.tabBarController setSelectedIndex:0];
            }
            [self openStatic:@"ChangeEmailVC"];
        }break;
        case 30:{//About
            [self showAbout];
        }break;
        default:
            break;
    }
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    [transitionCircSearch setTransitionMode:dismiss];
    [transitionCircSearch setStartingPoint: textFieldSearch.center];
    [transitionCircSearch setCircleColor:[UIColor clearColor]];

    return transitionCircSearch;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    [transitionCircSearch setTransitionMode:present];
    [transitionCircSearch setStartingPoint: textFieldSearch.center];
    [transitionCircSearch setCircleColor:[UIColor clearColor]];

    return transitionCircSearch;
}

-(void) showAbout {
    TemplateViewController *about = [self.storyboard instantiateViewControllerWithIdentifier:@"ABOUTVC"];
    [self presentViewController:about animated:YES completion:nil];
}

@end
