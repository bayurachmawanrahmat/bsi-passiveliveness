//
//  EdHomeViewController.h
//  BSM-Mobile
//
//  Created by Macbook Air on 28/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "RootViewController.h"

@interface EdHomeViewController : RootViewController
- (void)goToSomepage:(int)position;
- (void)openFavorite:(int)node;
- (void)showGreeting;
- (void)loadBannerView;
- (void)openTemplateWithData:(int)position;
- (void)goToQR;
- (void)goToInbox;
@end


