//
//  HomeViewController.h
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "RootViewController.h"

@interface HomeViewController : RootViewController
- (void)goToSomepage:(int)position;
//@property (weak, nonatomic) IBOutlet UIView *viewScroll;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollSlider;
//@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
- (void)openFavorite:(int)node;
- (void)showGreeting;
@end
