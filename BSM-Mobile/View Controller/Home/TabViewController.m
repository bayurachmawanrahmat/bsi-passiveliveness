//
//  TabViewController.m
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//
#define SEPARATOR_WIDTH 0.4f
#define SEPARATOR_COLOR [UIColor whiteColor]

#import "TabViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "TemplateViewController.h"
#import "LocationViewController.h"
#import "Utility.h"


@interface TabViewController ()<UITabBarDelegate, UITabBarControllerDelegate, UIAlertViewDelegate>

@end

UIView *viewSosmad;
UIView *vwIslamic;

@implementation TabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//- (void) addShape{
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = self.tabMenu.bounds;
//    gradient.colors = @[(id)[self colorFromHexString:@"#eeb919"].CGColor,
//                        (id)[self colorFromHexString:@"#ffdd55"].CGColor,
//                        (id)[self colorFromHexString:@"#f6c52f"].CGColor];
//
//    [self.tabMenu.layer insertSublayer:gradient atIndex:0];
//
//    //    [self.tabMenu setBackgroundColor:[UIColor colorWithPatternImage:[self imageFromLayer:gradient]]];
//    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
//    //    [shapeLayer setPath:[[self createShapeUp] CGPath]];
//    //    shapeLayer.strokeColor = [[UIColor grayColor]CGColor];
//    shapeLayer.fillColor = [[UIColor colorWithPatternImage:[self imageFromLayer:gradient]]CGColor];
//
//    shapeLayer.lineWidth = 1.0;
//
//    [self.tabMenu.layer insertSublayer:shapeLayer atIndex:0];
//}
//
//- (UIBezierPath *)createShapeUp
//{
//    UIBezierPath *path = [UIBezierPath bezierPath];
//    CGFloat height = 20;
//    CGFloat centerWidth = self.tabMenu.frame.size.width / 2;
//
//    [path moveToPoint:CGPointMake(0, 0)];
//    [path addLineToPoint:CGPointMake(centerWidth - height*3, 0)];
//
//    [path addCurveToPoint:CGPointMake(centerWidth, -height)
//      controlPoint1:CGPointMake(centerWidth - 20, 0)
//      controlPoint2:CGPointMake(centerWidth - 25, -height)];
//
//    [path addCurveToPoint:CGPointMake(centerWidth + height*3, 0)
//        controlPoint1:CGPointMake(centerWidth + 25, -height)
//        controlPoint2:CGPointMake(centerWidth + 20, 0)];
//
//    [path addLineToPoint:CGPointMake(self.tabMenu.frame.size.width, 0)];
//    [path addLineToPoint:CGPointMake(self.tabMenu.frame.size.width, self.tabMenu.frame.size.height)];
//    [path addLineToPoint:CGPointMake(0, self.tabMenu.frame.size.height)];
//
//    [[UIColor grayColor] setStroke];
//    [path stroke];
//
//    return path;
//
//}

- (void)viewDidLoad{
    self.delegate = self;

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    NSString *txtIS = @"";
    
    if([lang isEqualToString:@"id"]){
        txtIS = @"Islamic Services";
    } else {
        txtIS = @"Islamic Services";
    }
    
    // add background shape
//    [self addShape];
    
    // make unselected tabBar becomes gray
    if ([[UITabBar appearance] respondsToSelector:@selector(setUnselectedItemTintColor:)]) {
        [[UITabBar appearance] setUnselectedItemTintColor:[UIColor darkGrayColor]];
    }
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.tabMenu.bounds;
//    gradient.colors = @[(id)[self colorFromHexString:@"#eeb919"].CGColor,
//                        (id)[self colorFromHexString:@"#ffdd55"].CGColor,
//                        (id)[self colorFromHexString:@"#f6c52f"].CGColor];
    // [self.tabMenu.layer insertSublayer:gradient atIndex:0];
    
//    [self.tabMenu setBackgroundColor:[UIColor colorWithPatternImage:[self imageFromLayer:gradient]]];
//    [self.tabMenu setBackgroundColor:UIColorFromRGB(const_color_primary)];
    [self.tabMenu setBackgroundColor:UIColorFromRGB(const_color_secondary)];
//    [self.tabMenu setBackgroundColor:[UIColor whiteColor]];

    NSArray *itemsObject = self.tabBar.items;
    for(UITabBarItem *tabItem in itemsObject) {
        UIImage *imaged = tabItem.image;
        UIImage *imagedSelected = tabItem.selectedImage;
        tabItem.selectedImage = [imagedSelected imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabItem.image = [imaged imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    [self setupTabBarSeparators];
//    CGFloat width = 200;
//    CGFloat height = [UIScreen mainScreen].bounds.size.height;
//
//    viewSosmad = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, height - 50)];
//    viewSosmad.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
//    UIView *viewLine1 = [[UIView alloc]initWithFrame:CGRectMake(10, 44, width -20, 1)];
//    viewLine1.backgroundColor = [UIColor grayColor];
//    UIView *viewLine2 = [[UIView alloc]initWithFrame:CGRectMake(10, 44, width -20, 1)];
//    viewLine2.backgroundColor = [UIColor grayColor];
//    UIView *viewLine3 = [[UIView alloc]initWithFrame:CGRectMake(10, 44, width -20, 1)];
//    viewLine3.backgroundColor = [UIColor grayColor];
//    UIView *viewLine4 = [[UIView alloc]initWithFrame:CGRectMake(10, 44, width -20, 1)];
//    viewLine4.backgroundColor = [UIColor grayColor];
//    UIView *viewWhite = [[UIView alloc]initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width) - (width + 20), (height - 50) -220, width, 220)];
//    viewWhite.backgroundColor = [UIColor whiteColor];
//
//
//    UIView *viewFb = [[UIView alloc]initWithFrame:CGRectMake(0, 10, width, 45)];
//    UIButton *btnFb = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, width, 45)];
//    [btnFb addTarget:self action:@selector(actionOpenFb) forControlEvents:UIControlEventTouchUpInside];
//    UIImageView *imgIconFb =[[UIImageView alloc]initWithFrame:CGRectMake(20, 0 , 40, 40)];
//    [imgIconFb setImage:[UIImage imageNamed:@"share_facebook.png"]];
//    UILabel *labelFb = [[UILabel alloc]initWithFrame:CGRectMake(80, 10, width - 100 , 20)];
//    [labelFb setText:@"Facebook"];
//    [viewFb addSubview:viewLine1];
//    [viewFb addSubview:labelFb];
//    [viewFb addSubview:imgIconFb];
//    [viewFb addSubview:btnFb];
//
//    UIView *viewInstagram = [[UIView alloc]initWithFrame:CGRectMake(0, 65, width, 45)];
//    UIButton *btnInstagram = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, width, 45)];
//    [btnInstagram addTarget:self action:@selector(openInstagram) forControlEvents:UIControlEventTouchUpInside];
//    UIImageView *imgInstagram =[[UIImageView alloc]initWithFrame:CGRectMake(20, 0 , 40, 40)];
//    [imgInstagram setImage:[UIImage imageNamed:@"instagram1.png"]];
//    UILabel *labelInstagram = [[UILabel alloc]initWithFrame:CGRectMake(80, 10, width - 100 , 20)];
//    [labelInstagram setText:@"Instagram"];
//    [viewInstagram addSubview:viewLine2];
//    [viewInstagram addSubview:labelInstagram];
//    [viewInstagram addSubview:imgInstagram];
//    [viewInstagram addSubview:btnInstagram];
//
//    UIView *viewTwit = [[UIView alloc]initWithFrame:CGRectMake(0, 120, width, 45)];
//    UIButton *btnTwit = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, width, 45)];
//    [btnTwit addTarget:self action:@selector(openTwitter) forControlEvents:UIControlEventTouchUpInside];
//    UIImageView *imgTwit =[[UIImageView alloc]initWithFrame:CGRectMake(20, 0 , 40, 40)];
//    [imgTwit setImage:[UIImage imageNamed:@"share_twitter.png"]];
//    UILabel *labelTwit = [[UILabel alloc]initWithFrame:CGRectMake(80, 10, width - 100 , 20)];
//    [labelTwit setText:@"Twitter"];
//    [viewTwit addSubview:viewLine3];
//    [viewTwit addSubview:labelTwit];
//    [viewTwit addSubview:imgTwit];
//    [viewTwit addSubview:btnTwit];
//
//    UIView *viewYoutube = [[UIView alloc]initWithFrame:CGRectMake(0, 175, width, 45)];
//    UIButton *btnYoutube = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, width, 45)];
//    [btnYoutube addTarget:self action:@selector(openYoutube) forControlEvents:UIControlEventTouchUpInside];
//    UIImageView *imgYoutube =[[UIImageView alloc]initWithFrame:CGRectMake(20, 0 , 40, 40)];
//    [imgYoutube setImage:[UIImage imageNamed:@"youtube.png"]];
//    UILabel *labelYoutube = [[UILabel alloc]initWithFrame:CGRectMake(80, 10, width - 100 , 20)];
//    [labelYoutube setText:@"Youtube"];
//    [viewYoutube addSubview:viewLine4];
//    [viewYoutube addSubview:labelYoutube];
//    [viewYoutube addSubview:imgYoutube];
//    [viewYoutube addSubview:btnYoutube];
//
//    [viewWhite addSubview:viewFb];
//    [viewWhite addSubview:viewInstagram];
//    [viewWhite addSubview:viewTwit];
//    [viewWhite addSubview:viewYoutube];
//    [viewSosmad addSubview:viewWhite];
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(handleSingleTap:)];
//    [viewSosmad addGestureRecognizer:singleFingerTap];
}

- (UIImage *)imageFromLayer:(CALayer *)layer
{
    UIGraphicsBeginImageContextWithOptions(layer.frame.size, NO, 0);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}


- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer{
    [viewSosmad removeFromSuperview];
}

- (void) setupTabBarSeparators {
//    CGFloat itemWidth = floor(self.tabBar.frame.size.width/self.tabBar.items.count);
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tabBar.frame.size.width, self.tabBar.frame.size.height)];
//    for (int i=0; i<self.tabBar.items.count - 1; i++) {
//        UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(itemWidth * (i +1) - SEPARATOR_WIDTH/2, 0, SEPARATOR_WIDTH, self.tabBar.frame.size.height)];
//        [separator setBackgroundColor:SEPARATOR_COLOR];
//        [bgView addSubview:separator];
//    }
    
    UIGraphicsBeginImageContext(bgView.bounds.size);
    [bgView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *tabBarBackground = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    NSLog(@"%@", item);
    
    if(self.selectedIndex !=3){
        [viewSosmad removeFromSuperview];
    }
    
    /*if(self.selectedIndex !=1){
        [vwIslamic removeFromSuperview];
    }*/
//        // [[self.viewControllers objectAtIndex:0]popToRootViewControllerAnimated:NO];
//    }else if(self.selectedIndex==4){
////
//
//    }
}

- (BOOL)tabBarController:(UITabBarController *)tbController shouldSelectViewController:(UIViewController *)viewController {
    
    [self.slidingViewController resetTopViewAnimated:true];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    
    if (viewController == [tbController.viewControllers objectAtIndex:3] ) {
        //[[UIApplication sharedApplication].keyWindow addSubview:viewSosmad];
        //return NO;
    /*} else if (viewController == [tbController.viewControllers objectAtIndex:1]) {
        [[UIApplication sharedApplication].keyWindow addSubview:vwIslamic];
        return NO;*/
        
        [userDefault setObject:@"ATM" forKey:@"tabPos"];
        NSLog(@"Atm open");
        NSDictionary* userInfo = @{@"position": @(1118)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    } else if (viewController == [tbController.viewControllers objectAtIndex:4] ) {
        
//        PopupBantuanViewController *popBantuan = [self.storyboard instantiateViewControllerWithIdentifier:@"POPBANTUAN"];
////        [popZakat dataBundle:mutDataDict];
//        [self presentViewController:popBantuan animated:YES completion:nil];
        
//        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//        NSString *msg = @"";
//        NSString *msgCancel = @"";
//
//        if([lang isEqualToString:@"id"]) {
//            msg = @"Anda ingin menghubungi Mandiri Syariah call 14040 ?";
//            msgCancel = @"Batal";
//        } else {
//            msg = @"Do you want to contact Mandiri Syariah call 14040 ?";
//            msgCancel = @"Cancel";
//        }
//
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:msgCancel otherButtonTitles:@"Ok", nil];
//        alert.delegate = self;
//        alert.tag = 3312;
//        [alert show];
//        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://14040"]];
//        return NO;
    } else if (viewController == [tbController.viewControllers objectAtIndex:1] ) {
//        MenuViewController *root = [[MenuViewController alloc] init];
//        [root backRoot];
//        return YES;
        [userDefault setObject:@"JADWAL" forKey:@"tabPos"];
        NSLog(@"Jadwal sholat open");
        NSDictionary* userInfo = @{@"position": @(1118)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }else if (viewController == [tbController.viewControllers objectAtIndex:2]){
        [userDefault setObject:@"WISDOM" forKey:@"tabPos"];
        NSLog(@"Juz Amma open");
        NSDictionary* userInfo = @{@"position": @(1118)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
    else if(viewController == [tbController.viewControllers objectAtIndex:0]){
        [userDefault setObject:@"HOME" forKey:@"tabPos"];
        NSDictionary* userInfo = @{@"position": @(1113)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        userInfo = @{@"position": @(1112)};
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
//    else{
//        MenuViewController *root = [[MenuViewController alloc] init];
//        [root backRoot];
//        return YES;
//    }
    [userDefault synchronize];
    return YES;
}

//- (void)actionOpenFb {
//    [viewSosmad removeFromSuperview];
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
//        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/153676778019505"]];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://id-id.facebook.com/syariahmandiri"]];
//    } else {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://web.facebook.com/syariahmandiri"]];
//    }
//}

/*-(void)actionOpenIslamic {
    [vwIslamic removeFromSuperview];
    [self openIslamicServices];
}

-(void)openIslamicServices {
    int position = 313;
    NSDictionary* userInfo = @{@"position": @(position)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    position = 1002;
    userInfo = @{@"position": @(position)};
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}*/

//- (void)openInstagram {
//     [viewSosmad removeFromSuperview];
//    NSURL *instagramURL = [NSURL URLWithString:@"instagram://user?username=banksyariahmandiri"];
//    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
//        [[UIApplication sharedApplication] openURL:instagramURL];
//    } else {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.instagram.com/banksyariahmandiri/?hl=id"]];
//    }
//}
//
//- (void)openTwitter {
//     [viewSosmad removeFromSuperview];
//    NSURL *linkTwiter = [NSURL URLWithString:@"twitter://user?screen_name=syariahmandiri"];
//    NSURL *urlTwiter = [NSURL URLWithString:@"https://twitter.com/syariahmandiri"];
//    if ([[UIApplication sharedApplication] canOpenURL:linkTwiter]) {
//       [[UIApplication sharedApplication] openURL:linkTwiter];
//    } else {
//        [[UIApplication sharedApplication] openURL:urlTwiter];
//    }
//
//}
//
//- (void)openYoutube {
//     [viewSosmad removeFromSuperview];
//    NSString *channelName = @"UCDCCcaZKA85NUb_y49MiUlQ";
//    NSURL *linkToAppURL = [NSURL URLWithString:[NSString stringWithFormat:@"youtube://channel/%@",channelName]];
//    NSURL *linkToWebURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/channel/%@",channelName]];
//
//    if ([[UIApplication sharedApplication] canOpenURL:linkToAppURL]) {
//        [[UIApplication sharedApplication] openURL:linkToAppURL];
//    }
//    else{
//        // Can't open the youtube app URL so launch Safari instead
//        [[UIApplication sharedApplication] openURL:linkToWebURL];
//    }
//}
//
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (alertView.tag == 3312) {
//        if(buttonIndex == 1){
//             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://14040"]];
//        }
//    }
//}
//
//- (UIColor *)colorFromHexString:(NSString *)hexString {
//    unsigned rgbValue = 0;
//    NSScanner *scanner = [NSScanner scannerWithString:hexString];
//    [scanner setScanLocation:1]; // bypass '#' character
//    [scanner scanHexInt:&rgbValue];
//    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
//}


@end
