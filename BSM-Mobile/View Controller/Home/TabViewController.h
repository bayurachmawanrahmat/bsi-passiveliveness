//
//  TabViewController.h
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
//#import "ECSlidingViewController.h"
#import "MenuViewController.h"

@interface TabViewController : UITabBarController

@property (weak, nonatomic) IBOutlet UITabBar *tabMenu;


@end
