//
//  RPViewController.m
//  BSM-Mobile
//
//  Created by lds on 6/13/14.
//  Copyright (c) 2014 pikpun. All rights reserved.
//

#import "RPViewController.h"
#import "TemplateViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "LV01ViewController.h"
#import "NSUserdefaultsAes.h"

@interface RPViewController (){
    BOOL needReload;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
}

@end

@implementation RPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    needReload = true;
//    [templateView openNextTemplate];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    if(needReload){
        
        NSArray *temp = [[DataManager sharedManager] getJSONData:0];
        NSArray *balanceInfo = [[[temp objectAtIndex:1]objectForKey:@"action"]objectAtIndex:0];
        NSDictionary *action = [[balanceInfo objectAtIndex:1]objectForKey:@"action"];
        NSString *menuId = [balanceInfo objectAtIndex:0];
        
        DataManager *dataManager = [DataManager sharedManager];
        
        [dataManager setAction:action andMenuId:menuId];
        dataManager.currentPosition = 0;
        NSDictionary *jsonData = [dataManager getObjectData];
        
        BOOL canOpenTemplate = true;
        if([[jsonData valueForKey:@"activation"]boolValue]){
//            if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"]){
            if(![NSUserdefaultsAes getValueForKey:@"customer_id"]){
                canOpenTemplate = false;
            }
        }
        if(canOpenTemplate){
            NSLog(@"JSON DATA TEMPLATE: %@", jsonData);
            dataManager.currentPosition = 0; //reset again
            TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:[jsonData valueForKey:@"template"]];
            [templateView setJsonData:jsonData];
            
            [self pushViewController:templateView animated:YES];
        }else{
            needReload = false;
            /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];*/
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"en"]){
                valTitle = @"Information";
                valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
                valCancel = @"No";
                valAgree = @"Ok";
            } else {
                valTitle = @"Informasi";
                valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
                valCancel = @"Tidak";
                valAgree = @"Ok";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            
            alert.tag = 229;
            [alert show];
        }
    }else{
        UITabBarController *tabCont = self.tabBarController;
        [tabCont setSelectedIndex:0];
        needReload = true;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    /*UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
    //    [self.revealViewController pushFrontViewController:viewCont animated:YES];
    self.slidingViewController.topViewController = viewCont;*/
    
    if (alertView.tag == 229) {
        //Check activate or not
        if (buttonIndex == 0) {
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        } else if (buttonIndex == 1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:@"NO" forKey:@"fromSliding"];
            [userDefault synchronize];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
            
            /*NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"id"]){
                valTitle = @"Konfirmasi";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Batal";
                valAgree = @"Lanjut";
            } else {
                valTitle = @"Confirmation";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Cancel";
                valAgree = @"Next";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            
            alert.tag = 230;
            [alert show];*/
        }
    } else if (alertView.tag == 230) {
        //Check register or not
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 231;
            [alert show];            
        } else if (buttonIndex == 1) {
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            
            UIViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
            self.slidingViewController.topViewController = viewCont;
        }
    } else if (alertView.tag == 231) {
        //Back to Home
        NSDictionary* userInfo = @{@"position": @(313)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
