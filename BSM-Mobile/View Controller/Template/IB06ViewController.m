//
//  IB06ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 12/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "IB06ViewController.h"

@interface IB06ViewController () <UITextFieldDelegate>{
    NSInteger position;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vwTitleTopConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btomConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leading1Const;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *space41Const;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *space47Const;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spapce7delConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailing3Const;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topVwOTPConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spaceTFOTP;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *btn7;
@property (weak, nonatomic) IBOutlet UIButton *btn8;
@property (weak, nonatomic) IBOutlet UIButton *btn9;
@property (weak, nonatomic) IBOutlet UIButton *btn0;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@property (weak, nonatomic) IBOutlet UIView *vwTF;
@property (weak, nonatomic) IBOutlet UITextField *tf01;
@property (weak, nonatomic) IBOutlet UITextField *tf02;
@property (weak, nonatomic) IBOutlet UITextField *tf03;
@property (weak, nonatomic) IBOutlet UITextField *tf04;
@property (weak, nonatomic) IBOutlet UITextField *tf05;
@property (weak, nonatomic) IBOutlet UITextField *tf06;

@end

@implementation IB06ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    position = 11;
    
    [_lblTitle setText:[self.jsonData objectForKey:@"title"]];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    
    if([Utility isDeviceHaveNotch]){
        _btomConst.constant = 100;
        _leading1Const.constant = 32;
        _space41Const.constant = 32;
        _space47Const.constant = 32;
        _spapce7delConst.constant = 32;
        _trailing3Const.constant = 32;
        _topVwOTPConst.constant = 32;
        _spaceTFOTP.constant = 64;
    }
    
    [Styles setTopConstant:_vwTitleTopConst];
    
    [self.vwTF setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    
    [self.tf01 setTag:11];
    [self.tf02 setTag:12];
    [self.tf03 setTag:13];
    [self.tf04 setTag:14];
    [self.tf05 setTag:15];
    [self.tf06 setTag:16];
    self.tf06.delegate = self;
    
    [self.btn0 setTag:0];
    [self.btn0 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn1 setTag:1];
    [self.btn1 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn2 setTag:2];
    [self.btn2 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn3 setTag:3];
    [self.btn3 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn4 setTag:4];
    [self.btn4 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn5 setTag:5];
    [self.btn5 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn6 setTag:6];
    [self.btn6 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn7 setTag:7];
    [self.btn7 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn8 setTag:8];
    [self.btn8 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn9 setTag:9];
    [self.btn9 addTarget:self action:@selector(actionNumber:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnNext addTarget:self action:@selector(actionNext:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnDelete addTarget:self action:@selector(actionDelete:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self setTextfieldForm];

}

- (void) setTextfieldForm{
    UIView *digInputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];

    for(UIView *current in self.vwTF.subviews){
        if([current isKindOfClass:UITextField.class]){
            UITextField *tf = (UITextField*)current;
            
            [tf setTextAlignment:NSTextAlignmentCenter];
            [tf setDelegate:self];
            [tf setSecureTextEntry:YES];
            [tf setInputView:digInputView];
            [tf becomeFirstResponder];
            [tf setBorderStyle:UITextBorderStyleNone];
            tf.layer.masksToBounds = false;
            tf.layer.shadowColor = [[UIColor blackColor]CGColor];
            tf.layer.shadowOffset = CGSizeMake(0.0, 0.5);
            tf.layer.shadowOpacity = 1.0;
            tf.layer.shadowRadius = 0.0;
            [tf setUserInteractionEnabled:NO];
        }
    }
    
    
}

- (UITextField*) findCurrentField{
    for (UIView *current in self.vwTF.subviews) {
        
        UITextField *tfield = [(UITextField*)current viewWithTag:position];
        if([tfield.text isEqualToString:@""]) return tfield;
    }
    return nil;
}

- (BOOL) validate{
    NSString *inputOTP = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                          self.tf01.text,
                          self.tf02.text,
                          self.tf03.text,
                          self.tf04.text,
                          self.tf05.text,
                          self.tf06.text];
    if(inputOTP.length == 6){
        [dataManager.dataExtra setValue:inputOTP forKey:@"otp"];
        return YES;
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.tf06){
        NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if(newText.length == 1){
            textField.text = newText;
            [self openNextTemplate];
            return NO;
        }
    }
    return YES;
}

- (void)actionNext:(id)sender{
    if([self validate]){
        [self openNextTemplate];
    }

}

- (void) actionNumber:(id)sender{
    if([sender isKindOfClass:UIButton.class]){
        UIButton *btn = (UIButton*)sender;
        if(btn.tag > -1){
            UITextField *activeField = [self findCurrentField];
            if(activeField){
                [activeField setText:[NSString stringWithFormat:@"%ld",(long)btn.tag]];
                position = position + 1;
            }else{
                [self actionNext:sender];
            }
        }
    }
}

- (void) actionDelete:(id)sender{
    if(position > 11){
        position = position - 1;
    }
    for (UIView *current in self.vwTF.subviews) {
        
        UITextField *tfield = [(UITextField*)current viewWithTag:position];
        tfield.text = @"";
    }
}


@end
