//
//  CRAKADViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CRAKADViewController.h"
#import "PopupIsiPasanganViewController.h"
//#import "OTPInputViewController.h"
#import "PopupAkadCRViewController.h"
#import "PopupAgreementCRViewController.h"


#import "Utility.h"
#import "NSString+HTML.h"
#import "Styles.h"
#import "CustomBtn.h"

#import <PDFKit/PDFKit.h>

@interface CRAKADViewController ()<ConnectionDelegate, UIAlertViewDelegate, AkadCRDelegate, AgreementCRDelegate>{
    NSUserDefaults *userDefault;
    UIView* vwDynamicContent;
    NSString *lang;
    NSArray *dataContent;
    NSString *stringURL;
    BOOL isAggre;
    BOOL isChecked1, isChecked2, isChecked3;
    int nMartialState;
    UIView *viewProgress;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgStepper;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;

@property (weak, nonatomic) IBOutlet UIScrollView *vwAgreement;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheck1;
@property (weak, nonatomic) IBOutlet UILabel *lblCheck1;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheck2;
@property (weak, nonatomic) IBOutlet UILabel *lblCheck2;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheck3;
@property (weak, nonatomic) IBOutlet UILabel *lblCheck3;

//@property (weak, nonatomic) IBOutlet UIScrollView *vwConfirmation;
//@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;

@property (weak, nonatomic) IBOutlet CustomBtn *btnBack;
@property (weak, nonatomic) IBOutlet CustomBtn *btnAgreed;

@end

@implementation CRAKADViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Utility resetTimer];
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.titleBar.text = [self.jsonData objectForKey:@"title"];
    self.imgIcon.image = [UIImage imageNamed:[userDefault objectForKey:@"imgIcon"]];
    
    [Styles setTopConstant:_topConstant];
    
    if ([lang isEqualToString:@"id"]) {
        [self.btnBack setTitle:@"Batal" forState:UIControlStateNormal];
        [self.btnAgreed setTitle:@"Menyetujui" forState:UIControlStateNormal];
    }else{
        [self.btnBack setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.btnAgreed setTitle:@"Agree" forState:UIControlStateNormal];
    }
    
    [self.btnBack setColorSet:SECONDARYCOLORSET];
    [self.btnAgreed setColorSet:PRIMARYCOLORSET];
    
    if(self.jsonData){
        bool needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if (needRequest) {
            NSString *urlParam = [self.jsonData valueForKey:@"url_parm"];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,spouse_name,spouse_id,spouse_bod,loan_amount,loan_tenor,insurance_code,asset_type,asset_amount,product_code,product_type,app_no_msm,email,insurance_name",urlParam] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
    }
//
    
//    stepPhase = 3;
//    if([lang isEqualToString:@"id"]){
//        [self.titleBar setText:@"Syarat Ketentuan dan Akad"];
//    }else{
//        [self.titleBar setText:@"Terms of Condition and Agreement"];
//    }
    [self.imgStepper setHidden:YES];
    [self.vwAgreement setHidden:YES];
//    [self.vwConfirmation setHidden:YES];
    
    [self.imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
    isChecked1 = NO;
    [self.imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
    isChecked2 = NO;
    isChecked3 = NO;
    [self.imgCheck3 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];

    [self.imgCheck1 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCheck1)]];
    [self.imgCheck2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCheck2)]];
    [self.imgCheck3 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleCheck3)]];
    
    [self.lblCheck1 setUserInteractionEnabled:YES];
    [self.lblCheck1 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLabelCheck1)]];
    
    [self.lblCheck2 setUserInteractionEnabled:YES];
    [self.lblCheck2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLabelCheck2)]];
    
    [self.lblCheck3 setUserInteractionEnabled:YES];
    [self.lblCheck3 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLabelCheck3)]];
    
    [self.btnBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAgreed addTarget:self action:@selector(actionAgree) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnAgreed setEnabled:NO];
    [self changeBtnNext];
//    [self setupDeummy];
}

- (void) actionBack{
//    dataManager.currentPosition--;
//    [self.navigationController popViewControllerAnimated:YES];
    [self backToR];
}

- (BOOL)isValid{
    if(!isChecked1){
        return NO;
    }
    if(!isChecked2){
        return NO;
    }
    if(!isChecked3){
        return NO;
    }
    return YES;
}

- (void)changeBtnNext{
    if([self isValid]){
        [self.btnAgreed setEnabled:YES];
//        self.btnAgreed.layer.borderColor = [UIColorFromRGB(const_color_secondary) CGColor];
        self.btnAgreed.layer.backgroundColor = [UIColorFromRGB(const_color_primary) CGColor];
    }else{
        self.btnAgreed.layer.borderColor = [UIColorFromRGB(const_color_gray) CGColor];
        self.btnAgreed.layer.backgroundColor = [UIColorFromRGB(const_color_gray) CGColor];
    }
}

- (void) actionAgree{
//    if(isChecked1 && isChecked2 && isChecked3){

        [dataManager.dataExtra setValue:dataContent forKey:@"data_content"];
        [self openNextTemplate];

//    }else{
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Harap tandai semua ketentuan tersedia" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
}

- (void) handleCheck1{
    if(!isChecked1){
        [self.imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        isChecked1 = YES;
    }else{
        [self.imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        isChecked1 = NO;
    }
    [self changeBtnNext];
}

- (void) handleCheck2{
    if(!isChecked2){
        [self.imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        isChecked2 = YES;
    }else{
        [self.imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        isChecked2 = NO;
    }
    [self changeBtnNext];
}

- (void) handleCheck3{
    if(!isChecked3){
        [self.imgCheck3 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        isChecked3 = YES;
    }else{
        [self.imgCheck3 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        isChecked3 = NO;
    }
    [self changeBtnNext];
}

- (void) handleLabelCheck1{
    [self.imgCheck1 setUserInteractionEnabled:YES];
    PopupAgreementCRViewController *popup = [self.storyboard instantiateViewControllerWithIdentifier:@"POPAGREECR"];
    [popup setDelegate:self];
    [popup setData:[dataManager.dataExtra objectForKey:@"agreement_string"]];
    [self presentViewController:popup animated:YES completion:nil];
}

- (void)completionAgreementCR:(BOOL)state{
    if(state){
        [self.imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        isChecked1 = YES;
        [self changeBtnNext];
    }
}

- (void) handleLabelCheck2{
    [self actionOpenURL];
    [self.imgCheck2 setUserInteractionEnabled:YES];
}

- (void) actionOpenURL{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:stringURL];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
//            [self.imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        }
    }];
}

- (void) handleLabelCheck3{
    [self.imgCheck3 setUserInteractionEnabled:YES];
    PopupAkadCRViewController *popup = [self.storyboard instantiateViewControllerWithIdentifier:@"POPAKADCR"];
    [popup setDelegate:self];
    [popup setData:[dataManager.dataExtra objectForKey:@"akad_string"]];
    [self presentViewController:popup animated:YES completion:nil];
}

- (void)completionAkadCR:(BOOL)state{
    if(state){
        [self.imgCheck3 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        isChecked3 = YES;
        [self changeBtnNext];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    CGRect frame = self.imgStepper.frame;
    viewProgress = [[UIView alloc]initWithFrame:frame];
    [self.view addSubview:viewProgress];
//    if(stepPhase == 3){
        [Styles createProgress:viewProgress step:3 from:4];
//    }else{
//        [Styles createProgress:viewProgress step:4 from:4];
//    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [Utility resetTimer];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    [Utility stopTimer];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject objectForKey:@"response"];
                NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                if (dataDict) {
                    NSLog(@"%@", dataDict);
                    nMartialState = [[dataDict valueForKey:@"Marital_Status_Code"]intValue];

                    NSArray *agrement = [[dataDict valueForKey:@"agreement"] componentsSeparatedByString:@"|"];
                    if(agrement.count > 3){
                        
                        
//                        [self.lblCheck1 setText:agrement[0]];
//                        [self.lblCheck2 setText:agrement[1]];
//                        [self.lblCheck3 setText:agrement[3]];
                        stringURL = agrement[2];

                        NSString *aggrement0 = [NSString stringWithFormat:@"<body style='font-family:Lato-Regular;font-size: 15px;'>%@</body>",agrement[0]];
                        NSString *aggrement1 = [NSString stringWithFormat:@"<body style='font-family:Lato-Regular;font-size: 15px;'>%@</body>",agrement[1]];
                        NSString *aggrement2 = [NSString stringWithFormat:@"<body style='font-family:Lato-Regular;font-size: 15px;'>%@</body>",agrement[3]];
                        
                        [self.lblCheck1 setAttributedText:[[NSAttributedString alloc]initWithData:[aggrement0 dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil]];
                        [self.lblCheck2 setAttributedText:[[NSAttributedString alloc]initWithData:[aggrement1 dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil]];
                        [self.lblCheck3 setAttributedText:[[NSAttributedString alloc]initWithData:[aggrement2 dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil]];
                    }
                     
                    dataContent = [NSJSONSerialization JSONObjectWithData:[[dataDict objectForKey:@"content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
//                    NSString *originalString = [dataDict objectForKey:@"akad"];
                    NSString *originalString = [dataDict objectForKey:@"akad"];

                    [dataManager.dataExtra setValue:originalString forKey:@"akad_string"];
                    [dataManager.dataExtra setValue:[dataDict objectForKey:@"info"] forKey:@"agreement_string"];
                    
                    [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
                    [userDefault setValue:[jsonObject objectForKey:@"transaction_id"]  forKey:@"trxidcashfin"];
                    [userDefault setValue:[jsonObject objectForKey:@"transaction_id"]  forKey:@"transaction_id"];

                }
                [self.vwAgreement setHidden:NO];
            }
        }else{
            [Utility showMessage:[jsonObject objectForKey:@"response"] mVwController:self mTag:[[jsonObject objectForKey:@"response_code"]intValue]];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    [Utility showMessage:error.localizedDescription mVwController:self mTag:99];
}

- (void)reloadApp{
    [self backToRoot];
}

@end
