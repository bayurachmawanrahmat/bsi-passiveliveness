//
//  QRP01ViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "QRP01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface QRP01ViewController ()<ConnectionDelegate, UITextFieldDelegate>{
    NSString* transactionId;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTittle;
@property (weak, nonatomic) IBOutlet UITextField *amount;
@property (weak, nonatomic) IBOutlet UITextField *remark;
@property (weak, nonatomic) IBOutlet UITextField *tips;
@property (weak, nonatomic) IBOutlet UITextField *adminFee;
@property (weak, nonatomic) IBOutlet UILabel *lblKeterangan;
@property (weak, nonatomic) IBOutlet UILabel *lblJml;
@property (weak, nonatomic) IBOutlet UILabel *lblTips;
@property (weak, nonatomic) IBOutlet UILabel *lblRemark;
@property (weak, nonatomic) IBOutlet UILabel *lblAdminfee;
@property (weak, nonatomic) IBOutlet UIButton *btnSetuju;
@property (weak, nonatomic) IBOutlet UIButton *btnBatal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomScroll;

@end

@implementation QRP01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.amount.userInteractionEnabled = NO;
    self.tips.userInteractionEnabled = NO;
    self.adminFee.userInteractionEnabled = NO;
    self.remark.userInteractionEnabled = NO;
    [self.lblTittle setText:[self.jsonData valueForKey:@"title"]];
    
    self.remark.delegate = self;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
//        [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnSetuju setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Batal" forState:UIControlStateNormal];
        [_lblJml setText:@"Jumlah"];
        [_lblTips setText:@"Tips"];
        [_lblAdminfee setText:@"Biaya Admin"];
        [_lblRemark setText:@"Keterangan"];
    } else {
//        [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnSetuju setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Cancel" forState:UIControlStateNormal];
        [_lblJml setText:@"Amount"];
        [_lblTips setText:@"Tips"];
        [_lblAdminfee setText:@"Admin fee"];
        [_lblRemark setText:@"Remark"];
    }
    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.amount.inputAccessoryView = keyboardDoneButtonView;
    self.remark.inputAccessoryView = keyboardDoneButtonView;
    self.tips.inputAccessoryView = keyboardDoneButtonView;
    self.adminFee.inputAccessoryView = keyboardDoneButtonView;
}

- (void)keyboardDidShow: (NSNotification *) notif{
    self.bottomScroll.constant = ([notif.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height) - 100;
}

- (void)keyboardDidHide: (NSNotification *) notif{
    self.bottomScroll.constant = 10;
}

#pragma mark change actionBatal to goToNext
- (IBAction)actionBatal:(id)sender {
    [self goToNext];
}

-(void)goToCancel{
    [super backToRoot];
    //    MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
    //    self.slidingViewController.topViewController = menuVC.homeNavigationController;
}

#pragma mark change actionSetuju to goToCancel
- (IBAction)actionSetuju:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    NSString *message = @"";
    if (self.amount.userInteractionEnabled) {
        NSString *trimmedString = [self.amount.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedString length]) {
            message = lang(@"STR_NOT_NUM");
        }
    }
    
    if([message isEqualToString:@""]){
        [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id": transactionId}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"tips": self.tips.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"admfee": self.adminFee.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"amount": self.amount.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"remark": self.remark.text}];
        [super openNextTemplate];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.remark) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if ([[jsonObject objectForKey:@"response_code"] isEqual: @"00"]) {
        if (![requestType isEqualToString:@"check_notif"]) {
            NSString *response = [jsonObject valueForKey:@"response"];
            NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            [self.lblKeterangan setText:[dict objectForKey:@"msg"]];
            if([dict objectForKey:@"amount"]) {
                self.amount.text = [dict objectForKey:@"amount"];
            } else {
                self.amount.userInteractionEnabled = YES;
            }
            if([dict objectForKey:@"remark"]) {
                self.remark.text = [dict objectForKey:@"remark"];
            } else {
                self.remark.userInteractionEnabled = YES;
            }
            
            self.tips.text = [dict objectForKey:@"tips"];
            if ([[dict objectForKey:@"admfee"] isKindOfClass:[NSNumber class]]){
                NSNumber* myint = [dict objectForKey:@"admfee"];
                NSString* myNewString = [NSString stringWithFormat:@"%@", myint];
                self.adminFee.text = myNewString;
            } else {
                self.adminFee.text = [dict objectForKey:@"admfee"];
            }
            transactionId = [jsonObject objectForKey:@"transaction_id"];
            
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else {
        [self backToRoot];
    }
}


@end
