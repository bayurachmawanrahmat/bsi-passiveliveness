//
//  IB03ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "IB04ViewController.h"

@interface IB04ViewController (){
}
@property (weak, nonatomic) IBOutlet UITextField *textFieldDate;
- (IBAction)next:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnN;
@property (weak, nonatomic) IBOutlet UIButton *btnB;
@end

@implementation IB04ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [_btnB setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnN setTitle:@"Selanjutnya" forState:UIControlStateNormal];
    } else {
        [_btnB setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnN setTitle:@"Next" forState:UIControlStateNormal];
    }
    BOOL isIna = [[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"];
    
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldDate.frame.size.height);
    
    UIView *paddingPhone1 = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldDate.leftView = paddingPhone1;
    self.textFieldDate.leftViewMode = UITextFieldViewModeAlways;
    [self.textFieldDate setPlaceholder:isIna?@"Masukkan Tanggal":@"Please Input Date"];
    
    
    UIDatePicker *datePicker2 = [[UIDatePicker alloc] init];
    if (@available(iOS 13.4, *)) {
        [datePicker2 setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePicker2 setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    datePicker2.tag = 1;
    //    NSLocale locale = [[NSLocale currentLocale] displayNameForKey:NSLocaleIdentifier value:language];
    [datePicker2 setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePicker2 setBackgroundColor:[UIColor whiteColor]];
    [datePicker2 setDatePickerMode:UIDatePickerModeDate];
    [datePicker2 addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    self.textFieldDate.inputView = datePicker2;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dateChanged:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *selectedDate = [dateFormat stringFromDate:datePicker.date];
        self.textFieldDate.text = selectedDate;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)next:(id)sender {
    if([self.textFieldDate.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        NSString *newData = [NSString stringWithFormat:@"%@", self.textFieldDate.text];
        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:newData}];
        [super openNextTemplate];
    }
}

- (IBAction)cancel:(id)sender {
    [super backToRoot];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textFieldDate resignFirstResponder];
}

@end
