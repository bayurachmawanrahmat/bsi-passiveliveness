//
//  TS02ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 21/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TS02ViewController.h"
#import "UIView+SimpleRipple.h"
#import "Utility.h"

NS_ENUM(NSInteger, UIPICKERCOMPONENTS){
    UIREKTUJUAN = 0,
    UIJUMNOM = 1,
    UIPICKERTYPE = 3,
    UIPICKERDATESTART = 4,
    UIPICKERINTERVAL = 6,
    UIPICKERDATEEND = 5,
    UIPICKERCUSTOMINTERVAL = 7,
    UIINPUTINTERVAL= 8,
};


@interface TS02ViewController ()<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate>{
    NSArray *arPilihanInterval;
    NSArray *arIntervalCust;
    NSUserDefaults *userDefault;
    NSString *lang;
    UIToolbar *toolBar;
    UIView *vwOnScrollSelected;
    NSInteger rowTypeSelected, rowTypeInterval;
    NSNumberFormatter* currencyFormatter;
    
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSelanjutnya;


@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBackNav;

@property (weak, nonatomic) IBOutlet UIView *vwBaseRek;
@property (weak, nonatomic) IBOutlet UIView *vwBaseRekTujuan;
@property (weak, nonatomic) IBOutlet UIView *vwBaseJumlah;
@property (weak, nonatomic) IBOutlet UIView *vwBaseDeskripsi;

@property (weak, nonatomic) IBOutlet UILabel *lblRekAsal;
@property (weak, nonatomic) IBOutlet UILabel *lblRekTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblJumlah;
@property (weak, nonatomic) IBOutlet UILabel *lblDekripsi;

@property (weak, nonatomic) IBOutlet UIView *vwRek;
@property (weak, nonatomic) IBOutlet UIView *vwRekTujuan;
@property (weak, nonatomic) IBOutlet UIView *vwJumlah;
@property (weak, nonatomic) IBOutlet UIView *vwDeskripsi;

@property (weak, nonatomic) IBOutlet UITextField *txtRek;
@property (weak, nonatomic) IBOutlet UITextField *txtRekTujuan;
@property (weak, nonatomic) IBOutlet UITextField *txtJumlah;
@property (weak, nonatomic) IBOutlet UITextField *txtDeskripsi;


@end

@implementation TS02ViewController

- (void)loadView{
    [super loadView];
    
    [self makeBorderView:_vwRek mState:true];
    [self makeBorderView:_vwRekTujuan mState:false];
    [self makeBorderView:_vwJumlah mState:false];
    [self makeBorderView:_vwDeskripsi mState:false];
    
    self.btnBackNav.hidden = YES;
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmBtnSelanjutnya = self.btnSelanjutnya.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    
    CGRect frmVwContentScroll =self.vwContentScroll.frame;
    CGRect frmBtnBackNav = self.btnBackNav.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    CGRect frmVwBaseRek = self.vwBaseRek.frame;
    CGRect frmVwBaseRekTujuan = self.vwBaseRekTujuan.frame;
    CGRect frmVwBaseJumlah = self.vwBaseJumlah.frame;
    CGRect frmVwBaseDeskripsi = self.vwBaseDeskripsi.frame;
    
    CGRect frmLblRekAsal = self.lblRekAsal.frame;
    CGRect frmLblRekTujuan = self.lblRekTujuan.frame;
    CGRect frmLblJumlah = self.lblJumlah.frame;
    CGRect frmLblDeskripsi = self.lblDekripsi.frame;
    
    CGRect frmVwRek = self.vwRek.frame;
    CGRect frmVwRekTujuan = self.vwRekTujuan.frame;
    CGRect frmVwJumlah = self.vwJumlah.frame;
    CGRect frmVwDeskripsi = self.vwDeskripsi.frame;
    
    CGRect frmTxtRek = self.txtRek.frame;
    CGRect frmTxtRekTujuan = self.txtRekTujuan.frame;
    CGRect frmTxtJumlah = self.txtJumlah.frame;
    CGRect frmTxtDeskripsi = self.txtDeskripsi.frame;
    
    frmVwTitle.origin.x= 0 ;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmBtnSelanjutnya.origin.x = 16;
    frmBtnSelanjutnya.size.width = SCREEN_WIDTH - (frmBtnSelanjutnya.origin.x * 2);
    frmBtnSelanjutnya.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_DEF - frmBtnSelanjutnya.size.height;
    if ([Utility isDeviceHaveNotch]) {
        frmBtnSelanjutnya.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_NOTCH - frmBtnSelanjutnya.size.height;
    }
    frmBtnSelanjutnya.size.height = 40;
    
    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = frmBtnSelanjutnya.origin.y - frmVwScroll.origin.y - 16;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = SCREEN_WIDTH;
    
//    frmLblTitle.origin.x = frmBtnBackNav.origin.x + frmBtnBackNav.size.width + 8;
//    frmLblTitle.origin.y = frmVwTitle.size.height/2 - frmLblTitle.size.height/2;
//    frmLblTitle.size.width = frmVwTitle.size.width - frmLblTitle.origin.x - 16;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmVwTitle.size.width - 32;
    frmLblTitle.size.height = 40;
    
    if(self.btnBackNav.isHidden){
        frmLblTitle.origin.x = 16;
    }
    
    //-----vw base rekening
    frmVwBaseRek.origin.x = 16;
    frmVwBaseRek.origin.y = 0;
    frmVwBaseRek.size.width = frmVwContentScroll.size.width - (frmVwBaseRek.origin.x *2);
    
    frmLblRekAsal.origin.x = 5;
    frmLblRekAsal.origin.y = 0;
    frmLblRekAsal.size.width = frmVwContentScroll.size.width - (frmVwBaseRek.origin.x *2);
    
    frmVwRek.origin.x = 0;
    frmVwRek.origin.y = frmLblRekAsal.origin.x + frmLblRekAsal.size.height;
    frmVwRek.size.width = frmVwBaseRek.size.width;
    
    
    //-----vw base rekening tujuan
    
    frmVwBaseRekTujuan.origin.x = frmVwBaseRek.origin.x;
    frmVwBaseRekTujuan.origin.y = frmVwBaseRek.origin.y + frmVwBaseRek.size.height + 8 + frmLblRekAsal.size.height ;
    frmVwBaseRekTujuan.size.width = frmVwBaseRek.size.width;
    
    frmLblRekTujuan.origin.x = 5;
    frmLblRekTujuan.origin.y = 0;
    
    frmVwRekTujuan.origin.x = 0;
    frmVwRekTujuan.origin.y = 0;
    frmVwRekTujuan.size.width = frmVwBaseRekTujuan.size.width;
    
    frmVwBaseRekTujuan.size.height = frmVwRekTujuan.origin.y + frmVwRekTujuan.size.height;
    
    
    //------vw base jumlah
    
    frmVwBaseJumlah.origin.x = frmVwBaseRekTujuan.origin.x;
    frmVwBaseJumlah.origin.y = frmVwBaseRekTujuan.origin.y + frmVwBaseRekTujuan.size.height + 8;
    frmVwBaseJumlah.size.width = frmVwBaseRekTujuan.size.width;
    
    frmLblJumlah.origin.x = frmLblRekTujuan.origin.x;
    frmLblJumlah.origin.y = 0;
    
    frmVwJumlah.origin.x = 0;
    frmVwJumlah.origin.y = 0;
    frmVwJumlah.size.width = frmVwBaseJumlah.size.width;
    
    frmVwBaseJumlah.size.height = frmVwJumlah.origin.y + frmVwJumlah.size.height;
    
    //-------vw base deskripsi
    
    frmVwBaseDeskripsi.origin.x = frmVwBaseRekTujuan.origin.x;
    frmVwBaseDeskripsi.origin.y = frmVwBaseJumlah.origin.y + frmVwBaseJumlah.size.height + 8;
    frmVwBaseDeskripsi.size.width = frmVwBaseRekTujuan.size.width;
    
    frmLblDeskripsi.origin.x = frmLblRekTujuan.origin.x;
    frmLblDeskripsi.origin.y = 0;
    
    frmVwDeskripsi.origin.x = 0;
    frmVwDeskripsi.origin.y = 0;
    frmVwDeskripsi.size.width = frmVwBaseDeskripsi.size.width;
    
    frmVwBaseDeskripsi.size.height = frmVwDeskripsi.origin.y + frmVwDeskripsi.size.height;
    
    frmTxtRekTujuan.origin.x = 8;
    frmTxtRekTujuan.origin.y = frmVwRekTujuan.size.height/2 - frmTxtRekTujuan.size.height/2;
    frmTxtRekTujuan.size.width = frmVwRekTujuan.size.width - (frmTxtRekTujuan.origin.x*2);
    
    frmTxtJumlah.origin.x = 8;
    frmTxtJumlah.origin.y = frmVwJumlah.size.height/2 - frmTxtJumlah.size.height/2;
    frmTxtJumlah.size.width = frmVwJumlah.size.width - (frmTxtJumlah.origin.x*2);
    
    frmTxtDeskripsi.origin.x = 8;
    frmTxtDeskripsi.origin.y = frmVwDeskripsi.size.height/2 - frmTxtDeskripsi.size.height/2;
    frmTxtDeskripsi.size.width = frmVwDeskripsi.size.width - (frmTxtDeskripsi.origin.x*2);
    
    self.vwTitle.frame = frmVwTitle;
    self.btnSelanjutnya.frame = frmBtnSelanjutnya;
    self.vwScroll.frame = frmVwScroll;
    
    self.vwContentScroll.frame = frmVwContentScroll;
    self.btnBackNav.frame = frmBtnBackNav;
    self.lblTitle.frame = frmLblTitle;
    
    self.vwBaseRek.frame = frmVwBaseRek;
    self.vwRek.frame = frmVwRek;
    self.txtRek.frame = frmTxtRek;
    self.lblRekAsal.frame = frmLblRekAsal;
    
    self.vwBaseRekTujuan.frame = frmVwBaseRekTujuan;
    self.vwBaseJumlah.frame = frmVwBaseJumlah;
    self.vwBaseDeskripsi.frame = frmVwBaseDeskripsi;
    
    self.lblRekTujuan.frame = frmLblRekTujuan;
    self.lblJumlah.frame = frmLblJumlah;
    self.lblDekripsi.frame = frmLblDeskripsi;
    
    self.vwRekTujuan.frame = frmVwRekTujuan;
    self.vwJumlah.frame = frmVwJumlah;
    self.vwDeskripsi.frame = frmVwDeskripsi;
    
    self.txtRekTujuan.frame = frmTxtRekTujuan;
    self.txtJumlah.frame = frmTxtJumlah;
    self.txtDeskripsi.frame = frmTxtDeskripsi;
    
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    vwOnScrollSelected = nil;
    currencyFormatter = [[NSNumberFormatter alloc] init];
    
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    NSString *ttlToolbar, *capRekTuj, *capJum, *capDesc, *whenPlacHolder, *capRekAsal;
    arIntervalCust = [[NSArray alloc]init];
    rowTypeSelected = 0;
    rowTypeInterval = 0;
    if ([lang isEqualToString:@"id"]) {
        arPilihanInterval = @[@"Kapan?",@"Pada Tanggal Tertentu",@"Setiap Minggu", @"Setiap Bulan", @"Setiap N hari/bulan"];
        _lblTitle.text = @"Tambah Transaksi Terjadwal";
        ttlToolbar = @"Selesai";
        capRekAsal = @"Rekening Asal";
        capRekTuj = @"Rekening Tujuan";
        capJum = @"Jumlah Nominal";
        capDesc = @"Deskripsi";
        whenPlacHolder = @"Kapan ?";
        [self.btnSelanjutnya setTitle:@"Selanjutnya" forState:UIControlStateNormal];
    }else{
        arPilihanInterval = @[@"When?",@"On A Certain Date",@"Weekly", @"Monthly", @"Every N days/months"];
        _lblTitle.text = @"Add Scheduler Transaction";
        ttlToolbar = @"Done";
        capRekAsal = @"Source Account";
        capRekTuj = @"Destination Account";
        capJum = @"Nominal Amount";
        capDesc = @"Description";
        whenPlacHolder = @"When ?";
        [self.btnSelanjutnya setTitle:@"Next" forState:UIControlStateNormal];
    }
    
    self.lblRekAsal.text = capRekAsal;
    self.lblRekTujuan.text = capRekTuj;
    self.lblJumlah.text = capJum;
    self.lblDekripsi.text = capDesc;
    
    self.txtRek.text = [dataManager.dataExtra valueForKey:@"id_account"];
    self.txtRek.enabled = NO;
    self.txtRek.textColor = [UIColor grayColor];
//    [self showingLabelOnTextIsChanged:_txtRek.tag mText:capRekAsal];

    
    self.txtRekTujuan.placeholder = capRekTuj;
    self.txtRekTujuan.keyboardType = UIKeyboardTypeNumberPad;
    
    self.txtJumlah.placeholder = capJum;
    self.txtJumlah.keyboardType = UIKeyboardTypeNumberPad;
    
    self.txtDeskripsi.placeholder = capDesc;
    self.txtDeskripsi.keyboardType = UIKeyboardTypeDefault;
    
    self.txtRek.tag = -1;
    self.vwBaseRek.tag = -1;
    self.txtRekTujuan.tag = 0;
    self.vwBaseRekTujuan.tag=0;
    self.txtJumlah.tag = 1;
    self.vwBaseJumlah.tag = 1;
    self.txtDeskripsi.tag = 2;
    self.vwBaseDeskripsi.tag=2;
    
    
    self.txtRekTujuan.delegate = self;
    self.txtJumlah.delegate = self;
    self.txtDeskripsi.delegate = self;
    
    [self.btnBackNav addTarget:self action:@selector(actionSelectedTochDown:event:) forControlEvents:UIControlEventTouchDown];
    [self.btnBackNav addTarget:self action:@selector(actionBackNav:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnSelanjutnya addTarget:self action:@selector(actionNext:) forControlEvents:UIControlEventTouchUpInside];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.txtRekTujuan.inputAccessoryView = toolBar;
    self.txtJumlah.inputAccessoryView = toolBar;
    self.txtDeskripsi.inputAccessoryView = toolBar;
    
    UIView *vwKapan = [self addingView:UIPICKERTYPE mStrPlaceHolder:whenPlacHolder];
    
    [self.vwContentScroll addSubview: vwKapan];
    [self relocationPosition:(int)vwKapan.tag];

    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height + 90 + kbSize.height/2)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height/2 +60, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

-(void)actionSelectedTochDown : (UIButton *) sender
                        event : (UIEvent *) event{
   UITouch *touch = [[event touchesForView:sender] anyObject];
    CGPoint origin = [touch locationInView:sender];
    float radius = sender.frame.size.height;
    float duration = 0.5f;
    float fadeAfter = duration * 0.75f;
    
    [sender rippleStartingAt:origin withColor:[UIColor colorWithWhite:0.0f alpha:0.20f] duration:duration radius:radius fadeAfter:fadeAfter];
    
}

-(void) actionBackNav : (UIButton *) sender{
    [Utility goToBackNavigator:self.navigationController];
}


-(void) makeBorderView  : (UIView *) mView
                 mState : (BOOL) isSelected{
    
    mView.layer.borderWidth = 1;
    mView.layer.cornerRadius = 10;
    mView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    if (isSelected) {
        mView.layer.borderColor = [[UIColor blackColor]CGColor];
    }
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self hightligtedBorder:true mTag:textField.tag];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [self hightligtedBorder:false mTag:textField.tag];
    return YES;
}

-(void)hightligtedBorder : (BOOL) mState
                    mTag : (NSInteger) tag{
    for(UIView *vwBase in self.vwContentScroll.subviews){
        if (vwBase.tag == tag) {
            UIView *vwBorder = (UIView *) [vwBase.subviews objectAtIndex:1];
            [self makeBorderView:vwBorder mState:mState];
            break;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSString *strText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField.tag == 1) {
        NSString* newText = [[[strText stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        double newValue = [newText doubleValue];
        if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
        }
        else{
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
            [self showingLabelOnTextIsChanged:textField.tag mText:strText];
            return NO;
        }
    }else if(textField.tag == UIINPUTINTERVAL){
        if (strText.length > 2) {
            return NO;
        }else{
            return YES;
        }
    }
    
    [self showingLabelOnTextIsChanged:textField.tag mText:strText];
    
    return  YES;
}

-(void) showingLabelOnTextIsChanged : (NSInteger)mTag
                              mText : (NSString *) strText{
    UIView *vwBase = [self getVwBaseOnScroll:mTag];
    UILabel *lblTitle = (UILabel *) [vwBase.subviews objectAtIndex:0];
    UIView *vwBorder = (UIView *) [vwBase.subviews objectAtIndex:1];
    
    CGRect frmVwBase = vwBase.frame;
    CGRect frmLblTitle = lblTitle.frame;
    CGRect frmVWBorder = vwBorder.frame;
    
    frmLblTitle.size.width = frmVwBase.size.width - (frmLblTitle.origin.x *2);
    
    if ([strText isEqualToString:@""]) {
        [lblTitle setHidden:YES];
        frmVWBorder.origin.y = 0;
    }else{
        [lblTitle setHidden:NO];
        frmVWBorder.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height + 6;
    }
    frmVwBase.size.height = frmVWBorder.origin.y + frmVWBorder.size.height;
    
    vwBase.frame = frmVwBase;
    lblTitle.frame = frmLblTitle;
    vwBorder.frame = frmVWBorder;
    
    [self relocationPosition:0];
}

- (NSDate *) getTomorrowDate: (NSDate*) currentDate{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *todayComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:currentDate];
    NSInteger theDay = [todayComponents day];
    NSInteger theMonth = [todayComponents month];
    NSInteger theYear = [todayComponents year];

    // now build a NSDate object for yourDate using these components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:theDay];
    [components setMonth:theMonth];
    [components setYear:theYear];
    NSDate *thisDate = [gregorian dateFromComponents:components];
//    [components release];

    // now build a NSDate object for the next day
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:1];
    NSDate *nextDate = [gregorian dateByAddingComponents:offsetComponents toDate:thisDate options:0];
//    [offsetComponents release];
//    [gregorian release];
    
    return nextDate;
}

-(UIView *) addingView : (NSInteger) mTag
       mStrPlaceHolder : (NSString *) placeHolder{
//    NSInteger nextTag = [self.vwContentScroll.subviews count];
    UIView *vwBase = [[UIView alloc]init];
    vwBase.tag = mTag;
    
    UILabel *lblTitle = [[UILabel alloc]init];
    lblTitle.text = placeHolder;
    lblTitle.font = [UIFont fontWithName:const_font_name1 size:12];
    lblTitle.textColor = [UIColor blackColor];
    [lblTitle sizeToFit];
    lblTitle.tag = mTag;
    lblTitle.hidden = YES;
    
    UIView *vwBorder = [[UIView alloc]init];
    vwBorder.layer.borderWidth = 1;
    vwBorder.layer.cornerRadius = 10;
    vwBorder.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    vwBorder.tag = mTag;
    
    UITextField *txtInput = [[UITextField alloc]init];
    txtInput.font = [UIFont fontWithName:const_font_name1 size:14];
    txtInput.textColor = [UIColor blackColor];
    txtInput.placeholder = placeHolder;
    txtInput.borderStyle = UITextBorderStyleNone;
    txtInput.tag = mTag;
    txtInput.delegate = self;
//    txtInput.keyboardType = UIKeyboardTypeDefault;
    txtInput.keyboardType = UIKeyboardTypeNumberPad;
    [[txtInput valueForKey:@"textInputTraits"] setValue:[UIColor clearColor] forKey:@"insertionPointColor"];
    
    BOOL isIna = [[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"];
    
    if (mTag == UIPICKERTYPE || mTag == UIPICKERINTERVAL || mTag == UIPICKERCUSTOMINTERVAL) {
        UIPickerView *objPickerAccount = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        objPickerAccount.showsSelectionIndicator = YES;
        objPickerAccount.delegate = self;
        objPickerAccount.dataSource = self;
        objPickerAccount.tag = mTag;
        txtInput.inputView = objPickerAccount;
    }else if (mTag == UIPICKERDATESTART || mTag == UIPICKERDATEEND){
        UIDatePicker *objDateStart = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        if (@available(iOS 13.4, *)) {
            [objDateStart setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
            [objDateStart setPreferredDatePickerStyle:UIDatePickerStyleWheels];
         }
        [objDateStart setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
        [objDateStart setBackgroundColor:[UIColor whiteColor]];
        [objDateStart setDatePickerMode:UIDatePickerModeDate];
        [objDateStart addTarget:self action:@selector(dateStartChanged:) forControlEvents:UIControlEventValueChanged];
        objDateStart.tag = mTag;
//        objDateStart.minimumDate = [NSDate date];
        objDateStart.minimumDate = [self getTomorrowDate: [NSDate date]];
        txtInput.inputView = objDateStart;
        
        if(rowTypeSelected == 2 || rowTypeSelected == 3){
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"dd-MM-yyyy"];
            NSString *selectedDate = [dateFormat stringFromDate:[self getTomorrowDate: [NSDate date]]];
            txtInput.text = selectedDate;
        }
       
    }
    
    txtInput.inputAccessoryView = toolBar;
    
    
    [vwBorder addSubview:txtInput];
    [vwBase addSubview:lblTitle];
    [vwBase addSubview:vwBorder];
    
    
    CGRect frmVwBase = vwBase.frame;
    CGRect frmLblTitle = lblTitle.frame;
    CGRect frmVwBorder = vwBorder.frame;
    CGRect frmTxtInput = txtInput.frame;
    
    frmVwBase.origin.x = 16;
    frmVwBase.origin.y = 0;
    frmVwBase.size.width = self.vwContentScroll.frame.size.width - (frmVwBase.origin.x*2);
    
    frmLblTitle.origin.x = 5;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.width = frmVwBase.size.width - (frmLblTitle.origin.x*2);
    
    frmVwBorder.origin.y = 0;
    frmVwBorder.origin.x = 0;
    frmVwBorder.size.width = frmVwBase.size.width;
    frmVwBorder.size.height = 48;
    
    frmTxtInput.origin.x = 8;
    frmTxtInput.size.width = frmVwBorder.size.width - (frmTxtInput.origin.x*2);
    frmTxtInput.size.height = 43;
    frmTxtInput.origin.y = frmVwBorder.size.height/2 - frmTxtInput.size.height/2;
    
    frmVwBase.size.height = frmVwBorder.origin.y + frmVwBorder.size.height;
    
    vwBase.frame = frmVwBase;
    lblTitle.frame = frmLblTitle;
    vwBorder.frame = frmVwBorder;
    txtInput.frame = frmTxtInput;
    
    return vwBase;
}

-(void) relocationPosition : (int) startIndex{
    for (int i = startIndex; i< [self.vwContentScroll.subviews count]; i++) {
        UIView *currentView = nil;
        UIView *vwNext = (UIView *) [self.vwContentScroll.subviews objectAtIndex:i];
        if (i > 0) {
            currentView = (UIView *) [self.vwContentScroll.subviews objectAtIndex:i-1];
            CGRect frmVwCurrent = currentView.frame;
            CGRect frmVwNext = vwNext.frame;
            
            frmVwNext.origin.y = frmVwCurrent.origin.y + frmVwCurrent.size.height + 8;
            vwNext.frame = frmVwNext;
        }else{
            CGRect frmVwNext = vwNext.frame;
            frmVwNext.origin.y = 0;
            vwNext.frame = frmVwNext;
        }
    }
    
    UIView *vwLast = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1];
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    CGRect frmVwLast = vwLast.frame;
    
    frmVwContentScroll.size.height = frmVwLast.origin.y + frmVwLast.size.height + 16;
    self.vwContentScroll.frame = frmVwContentScroll;
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
    if (vwOnScrollSelected != nil) {
        if (vwOnScrollSelected.tag == 3) {
            [self removeLastView];
            if (rowTypeSelected == 1) {
                NSString *strPlaceholder;
                if ([lang isEqualToString:@"id"]) {
                    strPlaceholder = @"Tanggal Mulai";
                }else{
                    strPlaceholder = @"Start Date";
                }
                UIView *vwDateStart = [self addingView:UIPICKERDATESTART mStrPlaceHolder:strPlaceholder];
                [self.vwContentScroll addSubview: vwDateStart];
                [self relocationPosition:4];
            }
            else if(rowTypeSelected == 2 || rowTypeSelected == 3){
                NSString *strPlaceholder, *strTglMulai, *strTglAkhir;
                
                if ([lang isEqualToString:@"id"]) {
                    strTglMulai = @"Tanggal Mulai";
                    strTglAkhir = @"Tanggal Berakhir";
                }else{
                    strTglMulai = @"Start Date";
                    strTglAkhir = @"End Date";
                }
                
                if (rowTypeSelected == 2) {
                    if ([lang isEqualToString:@"id"]) {
                        strPlaceholder = @"Pilih Hari";
                        arIntervalCust = @[@"Minggu",@"Senin", @"Selasa", @"Rabu", @"Kamis", @"Jum'at", @"Sabtu"];
                    }else{
                        strPlaceholder = @"Select Day";
                        arIntervalCust = @[@"Sunday", @"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday"];
                    }
                    
                }else if(rowTypeSelected == 3){
                    
                    NSMutableArray *arTanggal = [[NSMutableArray alloc]init];
                    for(int i=1 ; i<32; i++){
                        [arTanggal addObject:[NSString stringWithFormat:@"%d", i]];
                    }
                    if ([lang isEqualToString:@"id"]) {
                        strPlaceholder = @"Pilih Tanggal";
                        [arTanggal addObject:@"Akhir Bulan"];
                    }else{
                        strPlaceholder = @"Choose Date";
                        [arTanggal addObject:@"End of Month"];
                    }
                    
                    arIntervalCust = (NSArray *) arTanggal;
                    
                }
                UIView *vwInterval = [self addingView:UIPICKERINTERVAL mStrPlaceHolder:strPlaceholder];
                UIView *vwDateStart = [self addingView:UIPICKERDATESTART mStrPlaceHolder:strTglMulai];
                UIView *vwDateEnd = [self addingView:UIPICKERDATEEND mStrPlaceHolder:strTglAkhir];
                [self.vwContentScroll addSubview: vwDateStart];
                [self.vwContentScroll addSubview: vwDateEnd];
                [self.vwContentScroll addSubview: vwInterval];
                [self relocationPosition:4];
            }else if(rowTypeSelected == 4){
                
                NSString *strPlaceholder, *strTglMulai, *strTglAkhir, *strPlcHolderType;
                
                if ([lang isEqualToString:@"id"]) {
                    strPlcHolderType = @"Pilih Hari/Bulan";
                    strPlaceholder = @"Interval";
                    strTglMulai = @"Tanggal Mulai";
                    strTglAkhir = @"Tanggal Berakhir";
                    arIntervalCust = @[@"Hari", @"Bulan"];
                }else{
                    strPlcHolderType = @"Choose Day/Monts";
                    strPlaceholder = @"Interval";
                    strTglMulai = @"Start Date";
                    strTglAkhir = @"End Date";
                    arIntervalCust = @[@"Day", @"Month"];
                }
                
                UIView *vwIntervalType = [self addingView:UIPICKERCUSTOMINTERVAL mStrPlaceHolder:strPlcHolderType];
                UIView *vwInterval = [self addingView:UIINPUTINTERVAL mStrPlaceHolder:strPlaceholder];
                UIView *vwDateStart = [self addingView:UIPICKERDATESTART mStrPlaceHolder:strTglMulai];
                UIView *vwDateEnd = [self addingView:UIPICKERDATEEND mStrPlaceHolder:strTglAkhir];
                [self.vwContentScroll addSubview: vwIntervalType];
                [self.vwContentScroll addSubview: vwDateStart];
                [self.vwContentScroll addSubview: vwDateEnd];
                [self.vwContentScroll addSubview: vwInterval];
                [self relocationPosition:4];
            }
            
        }else if(vwOnScrollSelected.tag == UIPICKERINTERVAL){
            [self relocationPosition:4];
        }
        
        NSLog(@"%ld", vwOnScrollSelected.tag);
        vwOnScrollSelected = nil;
        
    }else{
        if(rowTypeSelected == 2 || rowTypeSelected == 3){
            [self changeInterval];
        }
    }
}


- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (pickerView.tag) {
        case UIPICKERTYPE:
            return [arPilihanInterval count];
        case UIPICKERINTERVAL:
            return [arIntervalCust count];
        case UIPICKERCUSTOMINTERVAL:
            return [arIntervalCust count];
            
        default:
            return 0;
            
    }
    
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    switch (thePickerView.tag) {
        case UIPICKERTYPE:
            return [arPilihanInterval objectAtIndex: row];
        case UIPICKERINTERVAL :
            return [arIntervalCust objectAtIndex:row];
        case UIPICKERCUSTOMINTERVAL:
            return [arIntervalCust objectAtIndex:row];
            
        default:
            return @"";
            
    }
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    UIView *vwGetOnScroll = [self getVwBaseOnScroll:thePickerView.tag];
    vwOnScrollSelected = vwGetOnScroll;
    if (vwGetOnScroll != nil) {
        UILabel *lblTitle = (UILabel *) [vwGetOnScroll.subviews objectAtIndex:0];
        UIView *vwBase = (UIView *) [vwGetOnScroll.subviews objectAtIndex:1];
        UITextField *txtInput = (UITextField *) [vwBase.subviews objectAtIndex:0];
        
        [UIView transitionWithView:vwGetOnScroll duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
            CGRect frmVwInput = vwGetOnScroll.frame;
            CGRect frmLblTitle = lblTitle.frame;
            CGRect frmVwBase = vwBase.frame;
            
            NSString *strText = @"";
            if (vwGetOnScroll.tag == 3) {
                if(self->arPilihanInterval.count > 0){
                    if(row != 0){
                        strText = [self->arPilihanInterval objectAtIndex:row];
                    }
                    self->rowTypeSelected = row;
                }
            }else if(vwGetOnScroll.tag == UIPICKERINTERVAL || vwGetOnScroll.tag == UIPICKERCUSTOMINTERVAL){
                if(self->arIntervalCust.count > 0){
                    strText = [self->arIntervalCust objectAtIndex:row];
                    self->rowTypeInterval = row;
                }
            }
            
            if ([lblTitle.text isEqualToString:strText]) {
                lblTitle.hidden = YES;
                frmVwBase.origin.y = 0;
                strText = @"";
            }else{
                lblTitle.hidden = NO;
                frmVwBase.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height + 6;
            }
            frmVwInput.size.height = frmVwBase.origin.y + frmVwBase.size.height;
            vwBase.frame = frmVwBase;
            vwGetOnScroll.frame = frmVwInput;
            txtInput.text = strText;
            
        }completion:nil];
        [self relocationPosition:4];
        
    }
}

-(void) removeLastView{
    for(UIView *vw in self.vwContentScroll.subviews){
        if (vw.tag == UIPICKERDATESTART) {
            [vw removeFromSuperview];
        }else if(vw.tag == UIPICKERDATEEND){
            [vw removeFromSuperview];
        }else if(vw.tag == UIPICKERINTERVAL){
            [vw removeFromSuperview];
        }else if (vw.tag == UIPICKERCUSTOMINTERVAL){
            [vw removeFromSuperview];
        }else if(vw.tag == UIINPUTINTERVAL){
            [vw removeFromSuperview];
        }
    }
}

- (void)dateStartChanged:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *selectedDate = [dateFormat stringFromDate:datePicker.date];
    UIView *vwGetOnScroll = [self getVwBaseOnScroll:datePicker.tag];
    
    if (vwGetOnScroll != nil) {
        
        UILabel *lblTitle = (UILabel *) [vwGetOnScroll.subviews objectAtIndex:0];
        UIView *vwBase = (UIView *) [vwGetOnScroll.subviews objectAtIndex:1];
        UITextField *txtInput = (UITextField *) [vwBase.subviews objectAtIndex:0];
        txtInput.text = selectedDate;
        
        [lblTitle setHidden:NO];
        
        CGRect frmVwBase = vwGetOnScroll.frame;
        CGRect frmLblTitle = lblTitle.frame;
        CGRect frmVwBorder = vwBase.frame;
        
        frmLblTitle.origin.x = 5;
        frmLblTitle.size.width = frmVwBase.size.width - (frmLblTitle.origin.x*2);
        
        frmVwBorder.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height + 6;
        
        frmVwBase.size.height = frmVwBorder.origin.y + frmVwBorder.size.height;
        
        vwGetOnScroll.frame = frmVwBase;
        lblTitle.frame = frmLblTitle;
        vwBase.frame = frmVwBorder;
        
//        [self changeDayofWeek];
//        rowTypeSelected = 2;
//        [self doneClicked:datePicker];
        
        [self relocationPosition:4];
        
    }
}

-(UIView *) getVwBaseOnScroll : (NSInteger) mTag{
    UIView *vwGetOnScroll = nil;
    for(UIView *vwOnScroll in self.vwContentScroll.subviews){
        if (vwOnScroll.tag == mTag) {
            vwGetOnScroll = (UIView *) vwOnScroll;
            break;
        }
    }
    return vwGetOnScroll;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

-(void) actionNext : (UIButton *) sender{
    BOOL isValid = true;
    BOOL isEndDateValid = true;
    BOOL isIntervalValid = true;
    
    NSInteger mIdx = 0;
    NSMutableArray *valueText = [[NSMutableArray alloc]init];
    for(int i=1; i< [self.vwContentScroll.subviews count]; i++){
        UIView *vwBase = (UIView *)[self.vwContentScroll.subviews objectAtIndex:i];
        UIView *vwBorder = (UIView *) [vwBase.subviews objectAtIndex:1];
        UITextField *txtInput = (UITextField *) [vwBorder.subviews objectAtIndex:0];
        if (i != 3) {
            if ([txtInput.text isEqualToString:@""] || [txtInput.text isEqualToString:@"0"]) {
                mIdx = i;
                isValid = false;
                break;
            }
            
        }
        [valueText addObject:txtInput.text];
    }
    
    if (isValid) {
        NSLog(@"%@", valueText);
        NSString *dateStart = nil, *dateEnd = nil;
        [dataManager.dataExtra setValue:[valueText objectAtIndex:0] forKey:@"destacctno"];
        [dataManager.dataExtra setValue:[Utility stringTrimmer:[[valueText objectAtIndex:1]stringByReplacingOccurrencesOfString:@"," withString:@""]] forKey:@"amount"];
        [dataManager.dataExtra setValue:[Utility stringTrimmer:[valueText objectAtIndex:2]] forKey:@"description"];
        [dataManager.dataExtra setValue:@"0" forKey:@"trxtype"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", rowTypeSelected - 1] forKey:@"recurrence_type"];
        [dataManager.dataExtra setValue:[Utility dateToday:4] forKey:@"datecreated"];
       
        switch (rowTypeSelected) {
            case 1:
                dateStart = [valueText objectAtIndex:4];
                dateEnd = [valueText objectAtIndex:4];
                break;
            case 2:
//                [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", rowTypeInterval+1] forKey:@"dayofweek"];
                [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%d", [self getDay:[valueText objectAtIndex:6]] ] forKey:@"dayofweek"];

                dateStart = [valueText objectAtIndex:4];
                dateEnd = [valueText objectAtIndex:5];
                break;
            case 3:{
                NSString *dayOfMonth;
//                if (rowTypeInterval == arIntervalCust.count - 1) {
                if (rowTypeInterval == 31) {
                    dayOfMonth = @"0";
                }else{
                    dayOfMonth = [valueText objectAtIndex:6];
                }
                [dataManager.dataExtra setValue:dayOfMonth forKey:@"dayofmonth"];
                dateStart = [valueText objectAtIndex:4];
                dateEnd = [valueText objectAtIndex:5];
            }
                break;
            case 4:{
                [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", rowTypeInterval] forKey:@"intervaltype"];
                if(rowTypeInterval == 1){
                    [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", rowTypeSelected] forKey:@"recurrence_type"];
                }
                [dataManager.dataExtra setValue:[valueText objectAtIndex:7] forKey:@"interval"];
                dateStart = [valueText objectAtIndex:5];
                dateEnd = [valueText objectAtIndex:6];
            }
                
            default:
                break;
        }
        
        if (dateStart != nil) {
             [dataManager.dataExtra setValue:[Utility splitDateWithFormat:1 dateString:dateStart] forKey:@"startdaterange"];
        }
        
        if (rowTypeSelected != 1) {
            if (dateEnd != nil) {
                [dataManager.dataExtra setValue:[Utility splitDateWithFormat:1 dateString:dateEnd] forKey:@"enddaterange"];
                if (dateStart != nil) {
                    NSDate *dtStart = [Utility dateStringToDate:dateStart mType:3], *dtEnd = [Utility dateStringToDate:dateEnd mType:3];
                    NSComparisonResult mCompareDate = [dtStart compare:dtEnd];
                    if (mCompareDate == NSOrderedAscending) {
                        isEndDateValid = true;
                    }else{
                        isEndDateValid = false;
                    }
                }
            }
        }
        
        if(isIntervalValid){
            
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            NSDate *dateStartFromString = [dateFormatter dateFromString:dateStart];
            NSDate *dateEndFromString = [dateFormatter dateFromString:dateEnd];


            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                       fromDate:dateStartFromString
                                                         toDate:dateEndFromString
                                                        options:0];

//            NSLog(@"Difference in date components: %i/%i/%i", components.day, components.month, components.year);
            
            if(rowTypeSelected == 2){
                if(components.year == 0 && components.month == 0 && components.day+1 < 7){
                        NSMutableArray *ar = [[NSMutableArray alloc] init];;
                        
                        NSDate *date = [dateFormatter dateFromString:dateStart];
                        for(int i = 0; i<components.day+1; i++){
                            if(i!=0){
                                date = [self getTomorrowDate:date];
                            }
                            NSInteger weekDay = [gregorianCalendar component:NSCalendarUnitWeekday fromDate:date];
                            [ar addObject:[NSNumber numberWithInteger:weekDay]];
                        }
                        
                        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                        f.numberStyle = NSNumberFormatterNoStyle;
                        NSNumber *myNumber = [f numberFromString:[dataManager.dataExtra objectForKey:@"dayofweek"]];
                        
                        bool isTheObjectThere = [ar containsObject:myNumber];
                        isIntervalValid = isTheObjectThere;

                    }
            }
            
            if(rowTypeSelected == 3){
                if(components.year == 0 && components.month == 0){
                    NSInteger day1 = [gregorianCalendar component:NSCalendarUnitDay fromDate:dateStartFromString];
                    NSInteger day2 = [gregorianCalendar component:NSCalendarUnitDay fromDate:dateEndFromString];
                    NSInteger month1 = [gregorianCalendar component:NSCalendarUnitMonth fromDate:dateStartFromString];
                    NSInteger month2 = [gregorianCalendar component:NSCalendarUnitMonth fromDate:dateEndFromString];
                    
                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                    f.numberStyle = NSNumberFormatterNoStyle;
                    NSNumber *myNumber = [f numberFromString:[dataManager.dataExtra objectForKey:@"dayofmonth"]];
                    
                    if(month1 == month2){
                        if(day1 > [myNumber integerValue] && day2 > [myNumber integerValue]){
                            isIntervalValid = false;
                        }else if(day2 < [myNumber integerValue]){
                            isIntervalValid = false;
                        }
                    }
                }
            }
            
            if(rowTypeSelected == 4){
                if([[dataManager.dataExtra objectForKey:@"intervaltype"]integerValue] == 0){
                    if(components.month == 0 && components.year == 0){
                        if(components.day < [[dataManager.dataExtra objectForKey:@"interval"]integerValue]){
                            isIntervalValid = false;
                        }
                    }
                }
                if([[dataManager.dataExtra objectForKey:@"intervaltype"]integerValue] == 1){
                    if(components.year == 0){
                        if(components.month < [[dataManager.dataExtra objectForKey:@"interval"]integerValue]){
                            isIntervalValid = false;
                        }
                    }
                }
            }
        }
        
        NSLog(@"%@", dataManager.dataExtra);
        if (isEndDateValid) {
            if(isIntervalValid){
                [super openNextTemplate];
            }else{
                NSString *nMessage;
                if ([lang isEqualToString:@"id"]) {
                    nMessage = @"Tanggal tidak memenuhi interval";
                }else{
                    nMessage = @"Interval do not match with dates";
                }
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
//            [super openNextTemplate];
        }else{
            NSString *nMessage;
            if ([lang isEqualToString:@"id"]) {
                nMessage = @"Tanggal berakhir harus lebih besar dari tanggal mulai";
            }else{
                nMessage = @"End date must be greater than start date";
            }
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        
        
    }else{
        NSString *nMessage = @"";
        NSString *nMessageStartDate, *nMessageEndDate;
        if ([lang isEqualToString:@"id"]) {
            nMessageStartDate = @"Silahkan isi tanggal mulai terlebih dahulu";
            nMessageEndDate = @"Silahkan isi tanggal berakhir terlebih dahulu";
        }else{
            nMessageStartDate = @"Please fill in the start date first";
            nMessageEndDate = @"Please fill in the end date first";
        }
        
        switch (mIdx) {
            case 1:
                if ([lang isEqualToString:@"id"]) {
                    nMessage = @"Silahkan isi rekening tujuan terlebih dahulu";
                }else{
                    nMessage = @"Please fill in the destination account first";
                }
                
                break;
            case 2:
                if([lang isEqualToString:@"id"]){
                    nMessage = @"Silahkan isi jumlah nominal terlebih dahulu";
                }else{
                    nMessage = @"Please fill in the nominal amount first";
                }
                break;
            case 4:
                if ([lang isEqualToString:@"id"]) {
                    nMessage = @"Silahkan pilih tipe recurrent type";
                }else{
                    nMessage = @"Please select the recurrent type";
                }
                break;
            case 5:{
                if (rowTypeSelected == 1) {
                    if ([lang isEqualToString:@"id"]) {
                        nMessage = @"Silahkan pilih tanggal terlebih dahulu";
                    }else{
                        nMessage = @ "Please select a date first";
                    }
                    
                }else if(rowTypeSelected == 2){
                    if ([lang isEqualToString:@"id"]) {
                        nMessage = @"Silahkan pilih hari terlebih dahulu";
                    }else{
                        nMessage = @"Please choose a day first";
                    }
                }else if (rowTypeSelected == 3){
                    if ([lang isEqualToString:@"id"]) {
                        nMessage = @"Silahkan pilih tanggal terlebih dahulu";
                    }else{
                        nMessage = @ "Please select a date first";
                    }
                }else {
                    if([lang isEqualToString:@"id"]){
                        nMessage = @"Silahkan pilih interval terlebih dahulu";
                    }else{
                        nMessage = @"Please select an interval first";
                    }
                }
            }
                break;
            case 6:
                if(rowTypeSelected == 4){
                    if ([lang isEqualToString:@"id"]) {
                        nMessage = @"Silahkan isi interval terlebih dahulu";
                    }else{
                        nMessage = @"Please select an interval first";
                    }
                }else{
                    nMessage = nMessageStartDate;
                }
                
                break;
            case 7:
                if(rowTypeSelected == 4){
                    nMessage = nMessageStartDate;
                }else{
                    nMessage = nMessageEndDate;
                }
                break;
            case 8:{
                nMessage = nMessageEndDate;
            }
                break;
                
            default:
                nMessage = @"No index";
                break;
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void) changeInterval{
    
    NSMutableArray *valueText = [[NSMutableArray alloc]init];
    
    for(int i=1; i< [self.vwContentScroll.subviews count]; i++){
        UIView *vwBase = (UIView *)[self.vwContentScroll.subviews objectAtIndex:i];
        UIView *vwBorder = (UIView *) [vwBase.subviews objectAtIndex:1];
        UITextField *txtInput = (UITextField *) [vwBorder.subviews objectAtIndex:0];
        
        [valueText addObject:txtInput.text];
    }
    
    NSArray *interval = @[@"Minggu",@"Senin", @"Selasa", @"Rabu", @"Kamis", @"Jum'at", @"Sabtu"];
    if(rowTypeSelected == 3){
        NSMutableArray *arTanggal = [[NSMutableArray alloc]init];
        for(int i=1 ; i<32; i++){
            [arTanggal addObject:[NSString stringWithFormat:@"%d", i]];
        }
        if ([lang isEqualToString:@"id"]) {
            [arTanggal addObject:@"Akhir Bulan"];
        }else{
            [arTanggal addObject:@"End of Month"];
        }
        interval = arTanggal;
    }else{
        if ([lang isEqualToString:@"id"]) {
            interval = @[@"Minggu",@"Senin", @"Selasa", @"Rabu", @"Kamis", @"Jum'at", @"Sabtu"];
        }else{
            interval = @[@"Sunday", @"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday"];
        }
    }

    
    NSString *dateStart = nil, *dateEnd = nil;
    NSUInteger count = [valueText count];
    
    UIView *vwBase = [self getVwBaseOnScroll:UIPICKERINTERVAL];
    UIView *vwBorder = (UIView *) [vwBase.subviews objectAtIndex:1];
    UITextField *txtInput = (UITextField *) [vwBorder.subviews objectAtIndex:0];
    NSLog(@"%@", [vwBorder subviews]);

     UIPickerView *objPickerAccount = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
     objPickerAccount.showsSelectionIndicator = YES;
     objPickerAccount.delegate = self;
     objPickerAccount.dataSource = self;
     objPickerAccount.tag = UIPICKERINTERVAL;
     txtInput.inputView = objPickerAccount;
    
    if (count >= 5){
        dateStart = [valueText objectAtIndex:4];
        dateEnd = [valueText objectAtIndex:5];
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *dateStartFromString = [dateFormatter dateFromString:dateStart];
        NSDate *dateEndFromString = [dateFormatter dateFromString:dateEnd];

        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                                   fromDate:dateStartFromString
                                                     toDate:dateEndFromString
                                                    options:0];
                
//        NSLog(@"Difference in date components: %i/%i/%i", components.day, components.month, components.year);
        
        if(rowTypeSelected == 2){
            if(components.year == 0 && components.month == 0 && components.day+1 < 7){
                NSMutableArray *ar = [[NSMutableArray alloc] init];;
                
                NSDate *date = [dateFormatter dateFromString:dateStart];
                for(int i = 0; i<components.day+1; i++){
                    if(i!=0){
                        date = [self getTomorrowDate:date];
                    }
                    NSInteger weekDay = [gregorianCalendar component:NSCalendarUnitWeekday fromDate:date];
                    [ar addObject:[interval objectAtIndex:weekDay-1]];
                }
                txtInput.text = @"";
                arIntervalCust = ar;
            }else{
                arIntervalCust = interval;
            }
        }else{
            if(components.month == 0){
                NSMutableArray *ar = [[NSMutableArray alloc] init];;

                NSDate *date = [dateFormatter dateFromString:dateStart];
                for(int i = 0; i<components.day+1; i++){
                    if(i!=0){
                        date = [self getTomorrowDate:date];
                    }
                    NSInteger dayOfMonth = [gregorianCalendar component:NSCalendarUnitDay fromDate:date];
                    [ar addObject:[NSString stringWithFormat:@"%ld", (long)dayOfMonth]];
                }
                txtInput.text = @"";
                arIntervalCust = ar;
            }else{
                arIntervalCust = interval;
            }
        }
        
    }else{
        arIntervalCust = interval;
    }
}


- (int) getDay : (NSString*) day{
    NSArray *interval = @[@"Minggu",@"Senin", @"Selasa", @"Rabu", @"Kamis", @"Jum'at", @"Sabtu"];
    if ([lang isEqualToString:@"id"]) {
        interval = @[@"Minggu",@"Senin", @"Selasa", @"Rabu", @"Kamis", @"Jum'at", @"Sabtu"];
    }else{
        interval = @[@"Sunday", @"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday"];
    }
    
    int count = 1;
    for(NSString* days in interval){
        if([days isEqualToString:day]){
            return count;
        }
        count = count + 1;
    }
    return 0;
}



@end
