//
//  CRConfirmViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 06/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CRConfirmViewController.h"
#import "OTPInputViewController.h"

@interface CRConfirmViewController ()<OTPVerifierDelegate, UIScrollViewDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    NSArray *dataContent;
    UIView *viewProgress;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;
@property (weak, nonatomic) IBOutlet UIImageView *imgStepper;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *titlebar;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBack;
@property (weak, nonatomic) IBOutlet CustomBtn *btnAgreed;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation CRConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Utility resetTimer];
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [Styles setTopConstant:_topConstant];
    
    self.imgStepper.hidden = YES;
    
    if(self.jsonData){
        [self.titlebar setText:[self.jsonData valueForKey:@"title"]];
    }
    
    if ([lang isEqualToString:@"id"]) {
        [self.btnBack setTitle:@"Batal" forState:UIControlStateNormal];
        [self.btnAgreed setTitle:@"Konfirmasi" forState:UIControlStateNormal];
    }else{
        [self.btnBack setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.btnAgreed setTitle:@"Confirm" forState:UIControlStateNormal];
    }
    
    [self.btnBack setColorSet:SECONDARYCOLORSET];
    [self.btnAgreed setColorSet:PRIMARYCOLORSET];
    
    dataContent = [dataManager.dataExtra objectForKey:@"data_content"];
    
    NSMutableString *str = [[NSMutableString alloc]init];
    if([lang isEqualToString:@"id"]){
        [str appendString:@"Anda akan mengajukan pembiayaan Online Mitraguna Bank Syariah Indonesia : \n\n"];
    }else{
        [str appendString:@"You will apply for Mitraguna Online Financing Bank Syariah Indonesia : \n\n"];
    }
    for(NSDictionary *dict in dataContent){
        [str appendString:[NSString stringWithFormat:@"%@ : %@\n", [dict objectForKey:@"key"], [dict objectForKey:@"value"]]];
    }
    
    self.lblConfirmation.text = str;
    
    self.scrollView.delegate = self;
    
    [self.btnBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [self.btnAgreed addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidAppear:(BOOL)animated{
    CGRect frame = self.imgStepper.frame;
    viewProgress = [[UIView alloc]initWithFrame:frame];
    [self.view addSubview:viewProgress];
    [Styles createProgress:viewProgress step:4 from:4];
}

- (void)verifiedOTP{
    [self openNextTemplate];
}

- (void) actionNext{
    OTPInputViewController *otpOpen = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPINPUT"];
    [otpOpen setDelegate:self];
    [self presentViewController:otpOpen animated:YES completion:nil];
}

- (void) actionBack{
    [self backToR];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [Utility resetTimer];
}

@end
