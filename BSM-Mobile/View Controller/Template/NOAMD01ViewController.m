//
//  OAMD01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/30/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "NOAMD01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface NOAMD01ViewController()<ConnectionDelegate, UIAlertViewDelegate> {
    NSString* requestType;
    NSString* urlSyarat;
    bool chaked;
    NSString *statA1;
    NSString *statA2;
    NSString *statA3;
    NSString *isZakat;
    bool checkedIsLink;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UITextField *textFiledSaldo;
@property (weak, nonatomic) IBOutlet UIButton *btnBatalMain;
@property (weak, nonatomic) IBOutlet UIButton *btnBatal;
@property (weak, nonatomic) IBOutlet UIButton *btnSetuju;
@property (weak, nonatomic) IBOutlet UIImageView *imgChecked;
@property (weak, nonatomic) IBOutlet UILabel *lblSyarat;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblSaldo;
@property (weak, nonatomic) IBOutlet UIView *line;
@property (weak, nonatomic) IBOutlet UIImageView *imgChecked2;
@property (weak, nonatomic) IBOutlet UIImageView *imgChecked3;
@property (weak, nonatomic) IBOutlet UIView *vwAgree3;
@property (weak, nonatomic) IBOutlet UIView *vwAgree;
@property (weak, nonatomic) IBOutlet UIView *vwAgree2;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree3;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree2;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree2;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree3;
@property (weak, nonatomic) IBOutlet UIView *vwSyarat;
@property (weak, nonatomic) IBOutlet UIButton *btnSyarat;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet UIScrollView *vwBody;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIView *line2;

@end

@implementation NOAMD01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    checkedIsLink = false;
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    [self.textFiledSaldo setPlaceholder:@"0"];
    
    statA1 = @"0";
    statA2 = @"0";
    statA3 = @"0";
    isZakat = @"N";
    
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmContent = self.lblContent.frame;
    CGRect frmBody = self.vwBody.frame;
    
    CGRect frmSyarat = self.vwSyarat.frame;
    //CGRect frmBtnSyarat = self.btnSyarat.frame;
    
    CGRect frmAgree = self.vwAgree.frame;
    CGRect frmAgree2 = self.vwAgree2.frame;
    CGRect frmAgree3 = self.vwAgree3.frame;
    
    CGRect frmLblAgree = self.lblAgree.frame;
    //CGRect frmLblAgree2 = self.lblAgree2.frame;
    CGRect frmLblAgree3 = self.lblAgree3.frame;
    
    //CGRect frmBtnAgree = self.btnAgree.frame;
    //CGRect frmBtnAgree2 = self.btnAgree2.frame;
    //CGRect frmBtnAgree3 = self.btnAgree3.frame;
    
    CGRect frmLblSaldo = self.lblSaldo.frame;
    CGRect frmTFSaldo = self.textFiledSaldo.frame;
    CGRect frmLine = self.line.frame;
    
    CGRect frmLblEmail = self.lblEmail.frame;
    CGRect frmTFEmail = self.txtEmail.frame;
    CGRect frmLine2 = self.line2.frame;
    
    CGRect frmButton = self.vwButton.frame;
    CGRect frmBtnBatal = self.btnBatal.frame;
    CGRect frmBtnBatalMain = self.btnBatalMain.frame;
    CGRect frmBtnSetuju = self.btnSetuju.frame;
    
    if (screenWidth <= 350) {
        [_lblContent setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        [_lblAgree setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        [_lblAgree2 setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        [_lblAgree3 setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        [_lblSaldo setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        [_lblSyarat setFont:[UIFont fontWithName:@"Helvetica" size:10.0f]];
        //[_textFiledSaldo setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
        
        frmContent.origin.y = frmContent.origin.y - 20;
        frmSyarat.origin.y = frmSyarat.origin.y - 20;
        frmAgree3.origin.y = frmAgree3.origin.y - 20;
        frmAgree.origin.y = frmAgree.origin.y - 20;
        
        frmLblAgree3.size.width = frmLblAgree3.size.width - 60;
        frmLblAgree.size.width = frmLblAgree.size.width - 50;
        frmLblSaldo.origin.y = frmLblSaldo.origin.y - 25;
        //frmLblSaldo.size.height = frmLblSaldo.size.height - 5;
        frmTFSaldo.origin.y = frmTFSaldo.origin.y - 25;
        frmLine.origin.y = frmTFSaldo.origin.y + frmTFSaldo.size.height;
        
        frmLblEmail.origin.y = frmLblEmail.origin.y - 25;
        frmTFEmail.origin.y = frmTFEmail.origin.y - 25;
        frmLine2.origin.y = frmTFEmail.origin.y + frmTFEmail.size.height;
        
        frmAgree2.origin.y = frmAgree2.origin.y - 25;
        
        //frmContent.size.height = frmContent.size.height - 30;
        
        //Disclaimer #1
        //frmAgree3.size.height = frmAgree3.size.height - 30;
        //frmAgree3.origin.y = frmContent.origin.y + frmContent.size.height + 10;
        //frmBtnAgree3.size.height = frmAgree3.size.height;
        //frmBtnAgree3.origin.y = frmAgree3.origin.y;
        
        //frmSyarat.size.height = frmSyarat.size.height - 30;
        //frmSyarat.origin.y = frmAgree3.origin.y + frmAgree3.size.height + 30;
        //frmBtnSyarat.size.height = frmSyarat.size.height;
        //frmBtnSyarat.origin.y = frmSyarat.origin.y;
        
        //Agreement Rules
        //frmAgree.size.height = frmAgree.size.height - 30;
        //frmAgree.origin.y = frmSyarat.origin.y + frmSyarat.size.height + 30;
        //frmBtnAgree.size.height = frmAgree.size.height;
        //frmBtnAgree.origin.y = frmAgree.origin.y;
        
        //Saldo Section
        //frmLblSaldo.size.height = frmLblSaldo.size.height - 30;
        //frmLblSaldo.origin.y = frmAgree.origin.y + frmAgree.size.height + 30;
        
        //frmTFSaldo.size.height = frmTFSaldo.size.height - 30;
        //frmTFSaldo.origin.y = frmLblSaldo.origin.y + frmLblSaldo.size.height + 10;
        
        //frmLine.size.height = frmLine.size.height - 30;
        //frmLine.origin.y = frmTFSaldo.origin.y + frmTFSaldo.size.height;
        
        //Agreement Zakat
        //frmAgree2.size.height = frmAgree2.size.height - 30;
        //frmAgree2.origin.y = frmLine.origin.y + frmLine.size.height;
        //frmBtnAgree2.size.height = frmAgree2.size.height;
        //frmBtnAgree2.origin.y = frmAgree2.origin.y;
    }
    [_lblContent setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
    frmButton.size.width = screenWidth;
    frmButton.origin.y = screenHeight - frmButton.size.height - 50;
    if (IPHONE_X) {
        frmButton.origin.y = frmButton.origin.y - 72;
    }else if (IPHONE_XS_MAX){
        frmButton.origin.y = frmButton.origin.y - 60;
    }
    if (screenWidth <= 350) {
        frmBody.size.height = frmButton.origin.y - frmTitle.origin.y - frmTitle.size.height;
    }
    frmBtnBatalMain.size.width = screenWidth - 16;
    frmBtnBatal.size.width = (screenWidth/2) - 16;
    frmBtnBatal.origin.x = (screenWidth/2) + 8;
    frmBtnSetuju.size.width = (screenWidth/2) - 16;
    
    frmTitle.size.width = screenWidth;
    frmBody.size.width = screenWidth;
    frmContent.size.width = screenWidth - 36;
    frmSyarat.size.width = screenWidth - 36;
    //frmBtnSyarat.size.width = screenWidth - 36;
    frmAgree.size.width = screenWidth - 36;
    //frmBtnAgree.size.width = screenWidth - 36;
    frmAgree2.size.width = screenWidth - 36;
    //frmBtnAgree2.size.width = screenWidth - 36;
    frmAgree3.size.width = screenWidth - 36;
    //frmBtnAgree3.size.width = screenWidth - 36;
    frmLblSaldo.size.width = screenWidth - 36;
    frmTFSaldo.size.width = screenWidth - 36;
    frmLine.size.width = screenWidth - 36;
    
    self.vwTitle.frame = frmTitle;
    self.vwBody.frame = frmBody;
    self.lblContent.frame = frmContent;
    self.vwSyarat.frame = frmSyarat;
    //self.btnSyarat.frame = frmBtnSyarat;
    self.vwAgree3.frame = frmAgree3;
    //self.btnAgree3.frame = frmBtnAgree3;
    self.lblAgree3.frame = frmLblAgree3;
    self.vwAgree.frame = frmAgree;
    //self.btnAgree.frame = frmBtnAgree;
    self.lblAgree.frame = frmLblAgree;
    self.lblSaldo.frame = frmLblSaldo;
    self.textFiledSaldo.frame = frmTFSaldo;
    self.line.frame = frmLine;
    self.lblEmail.frame = frmLblEmail;
    self.txtEmail.frame = frmTFEmail;
    self.line2.frame = frmLine2;
    self.vwAgree2.frame = frmAgree2;
    //self.btnAgree2.frame = frmBtnAgree2;
    //self.lblAgree2.frame = frmLblAgree2;
    self.vwButton.frame = frmButton;
    self.btnSetuju.frame = frmBtnSetuju;
    self.btnBatal.frame = frmBtnBatal;
    self.btnBatalMain.frame = frmBtnBatalMain;
    
    urlSyarat = @"https://www.syariahmandiri.co.id/";
    chaked = false;
    [_btnBatalMain setHidden:NO];
    [_btnSetuju setHidden:YES];
    [_btnBatal setHidden:YES];
    [_lblSaldo setHidden:YES];
    [_textFiledSaldo setHidden:YES];
    [_line setHidden:YES];
    [_lblEmail setHidden:YES];
    [_txtEmail setHidden:YES];
    [_line2 setHidden:YES];
    [_vwAgree2 setHidden:YES];
    [_lblAgree setTextColor:[UIColor grayColor]];
    [_btnAgree setEnabled:NO];
    
//     _imgChecked.image = [UIImage imageNamed: @"check_disable.png"];
//
    
    _btnBatalMain.layer.cornerRadius = 20;
    _btnBatalMain.layer.masksToBounds = true;
    _btnBatalMain.layer.borderWidth = 2;
    _btnBatalMain.layer.borderColor = [[UIColor yellowColor] CGColor];
    
    _btnBatal.layer.cornerRadius = 20;
    _btnBatal.layer.masksToBounds = true;
    _btnBatal.layer.borderWidth = 2;
    _btnBatal.layer.borderColor = [[UIColor yellowColor] CGColor];
    
    _btnSetuju.layer.cornerRadius = 20;
    _btnSetuju.layer.masksToBounds = true;
    _btnSetuju.layer.borderWidth = 2;
    _btnSetuju.layer.borderColor = [[UIColor yellowColor] CGColor];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        _lblSyarat.text = @"Syarat dan Ketentuan (Wajib di klik)";
        _lblAgree.text = @"Saya telah membaca Syarat Umum Pembukaan Rekening dan Setuju";
        [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnBatalMain setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Setuju" forState:UIControlStateNormal];
        _lblSaldo.text = @"Setoran Awal";
        _lblEmail.text = @"Email";
        _lblAgree2.text = @"Zakat atas bagi hasil";
    } else {
        _lblSyarat.text = @"Terms and Conditions (Must be clicked)";
        _lblAgree.text = @"I have read the General Terms of Account Opening and Agreement";
        [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnBatalMain setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Agree" forState:UIControlStateNormal];
        _lblSaldo.text = @"Beginning balance";
        _lblEmail.text = @"Email";
        _lblAgree2.text = @"Zakat for profit sharing";
    }
    
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.textFiledSaldo.inputAccessoryView = keyboardDoneButtonView;
    
    NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
//    NSString *email = [userDefault objectForKey:@"email"];
    
    if ((email == nil) || ([email isEqualToString:@""])) {
        NSDictionary* userInfo = @{@"position": @(713)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
        /*userInfo = @{@"position": @(713)};
         [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];*/
    } else {
        self.txtEmail.text = email;
        [self.txtEmail setEnabled:NO];
    }
    
    NSString *url =[NSString stringWithFormat:@"%@,zakat",[self.jsonData valueForKey:@"url_parm"]];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}


- (IBAction)actionOpenWeb:(id)sender {
    checkedIsLink = true;
    //enable button
    [_btnAgree setEnabled:YES];
    [_lblAgree setTextColor:[UIColor blackColor]];
    _imgChecked.image = [UIImage imageNamed: @"blank_check.png"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: urlSyarat]];
}

- (IBAction)actionAgree:(id)sender {
    if(checkedIsLink){
        _imgChecked2.image = [UIImage imageNamed: @"checked.png"];
        if ([statA1 isEqualToString:@"0"]) {
            statA1 = @"1";
            if (([statA1 isEqualToString:@"1"]) && ([statA3 isEqualToString:@"1"])) {
                [_btnBatalMain setHidden:YES];
                [_btnSetuju setHidden:NO];
                [_btnBatal setHidden:NO];
                [_lblSaldo setHidden:NO];
                [_textFiledSaldo setHidden:NO];
                [_line setHidden:NO];
                [_lblEmail setHidden:NO];
                [_txtEmail setHidden:NO];
                [_line2 setHidden:NO];
                [_vwAgree2 setHidden:NO];
                
                
            }
            _imgChecked.image = [UIImage imageNamed: @"checked.png"];
        }
        else {
            statA1 = @"0";
            statA2 = @"0";
            [_btnBatalMain setHidden:NO];
            [_btnSetuju setHidden:YES];
            [_btnBatal setHidden:YES];
            [_lblSaldo setHidden:YES];
            [_textFiledSaldo setHidden:YES];
            [_line setHidden:YES];
            [_lblEmail setHidden:YES];
            [_txtEmail setHidden:YES];
            [_line2 setHidden:YES];
            [_vwAgree2 setHidden:YES];
            _imgChecked.image = [UIImage imageNamed: @"blank_check.png"];
            //load otomatis zakat di checked
//            _imgChecked2.image = [UIImage imageNamed: @"blank_check.png"];
        }
    }
    
    
    /*if (!chaked){
     [_btnBatalMain setHidden:YES];
     [_btnSetuju setHidden:NO];
     [_btnBatal setHidden:NO];
     [_lblSaldo setHidden:NO];
     [_textFiledSaldo setHidden:NO];
     [_line setHidden:NO];
     chaked = true;
     _imgChecked.image = [UIImage imageNamed: @"checked.png"];
     } else {
     chaked = false;
     statA1 = @"0";
     [_btnBatalMain setHidden:NO];
     [_btnSetuju setHidden:YES];
     [_btnBatal setHidden:YES];
     [_lblSaldo setHidden:YES];
     [_textFiledSaldo setHidden:YES];
     [_line setHidden:YES];
     _imgChecked.image = [UIImage imageNamed: @"blank_check.png"];
     }*/
}

- (IBAction)actionAgree2:(id)sender {
    if ([statA2 isEqualToString:@"0"]) {
        statA2 = @"1";
        isZakat = @"Y";
        _imgChecked2.image = [UIImage imageNamed: @"checked.png"];
    }
    else {
        statA2 = @"0";
        isZakat = @"N";
        _imgChecked2.image = [UIImage imageNamed: @"blank_check.png"];
    }
}

- (IBAction)actionAgree3:(id)sender {
    if ([statA3 isEqualToString:@"0"]) {
        statA3 = @"1";
        if (([statA1 isEqualToString:@"1"]) && ([statA3 isEqualToString:@"1"])) {
            [_btnBatalMain setHidden:YES];
            [_btnSetuju setHidden:NO];
            [_btnBatal setHidden:NO];
            [_lblSaldo setHidden:NO];
            [_textFiledSaldo setHidden:NO];
            [_line setHidden:NO];
            [_lblEmail setHidden:NO];
            [_txtEmail setHidden:NO];
            [_line2 setHidden:NO];
            [_vwAgree2 setHidden:NO];
        }
        _imgChecked3.image = [UIImage imageNamed: @"checked.png"];
    }
    else {
        statA2 = @"0";
        statA3 = @"0";
        [_btnBatalMain setHidden:NO];
        [_btnSetuju setHidden:YES];
        [_btnBatal setHidden:YES];
        [_lblSaldo setHidden:YES];
        [_textFiledSaldo setHidden:YES];
        [_line setHidden:YES];
        [_lblEmail setHidden:YES];
        [_txtEmail setHidden:YES];
        [_line2 setHidden:YES];
        [_vwAgree2 setHidden:YES];
        _imgChecked2.image = [UIImage imageNamed: @"blank_check.png"];
        _imgChecked3.image = [UIImage imageNamed: @"blank_check.png"];
    }
}

- (IBAction)doneClicked:(id)sender
{
    DLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (IBAction)actionSetuju:(id)sender {
    if([self.textFiledSaldo.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        ////        if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
        ////            [dataManager setPinNumber:self.textFiledSaldo.text];
        ////        }else{
        //
        //    }
        NSString *inputString = self.textFiledSaldo.text;
        NSString *trimmedString = [inputString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        
        if ([trimmedString length]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"STR_NOT_NUM") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else {
            /*if ([self.textFiledSaldo.text intValue] < 100000) {
             NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
             NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
             
             if([lang isEqualToString:@"id"]){
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Saldo minimal adalah Rp 100.000" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             } else {
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Minimum balance is Rp 100.000" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             }
             } else {*/
            [dataManager.dataExtra addEntriesFromDictionary:@{@"amount":self.textFiledSaldo.text}];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"zakat":isZakat}];
            [super openNextTemplate];
            //}
        }
    }
}
- (IBAction)actionBatal:(id)sender {
    [self backToRoot];
    //    MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
    //    self.slidingViewController.topViewController = menuVC.homeNavigationController;
}

- (IBAction)actionbatalMain:(id)sender {
    //    MenuViewController *menuVC = (MenuViewController*)self.slidingViewController.underLeftViewController;
    //    self.slidingViewController.topViewController = menuVC.homeNavigationController;
    [self backToRoot];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        //        NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[jsonObject dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            NSString *response = [jsonObject valueForKey:@"response"];
            NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            NSString *info = [dict objectForKey:@"info"];
            int amt = 0;
            if ([dict objectForKey:@"amount"] == nil) {
                amt = 0;
            } else {
                amt = [[dict objectForKey:@"amount"] intValue];
            }
            NSArray *stringArray = [info componentsSeparatedByString:@"|"];
            
            if (amt == 100000) {
                self.textFiledSaldo.text = [NSString stringWithFormat:@"%d",amt];
                [self.textFiledSaldo setEnabled:NO];
            } else {
                [self.textFiledSaldo setEnabled:YES];
            }
            
            if (stringArray.count == 1) {
                _lblContent.text = stringArray[0];
                
                urlSyarat = @"";
                _lblAgree3.text = @"";
            }
            else if (stringArray.count == 2) {
                _lblContent.text = stringArray[0];
                urlSyarat = stringArray[1];
                _lblAgree3.text = @"";
            }
            else if (stringArray.count == 3) {
                _lblContent.text = stringArray[0];
                urlSyarat = stringArray[1];
                _lblAgree3.text = @"";
            }
            else if (stringArray.count == 4) {
                _lblContent.text = stringArray[0];
                urlSyarat = stringArray[1];
                _lblAgree3.text = stringArray[3];
            }
            [_lblContent setNumberOfLines:0];
            [_lblContent sizeToFit];
//            CGRect frmContent = _lblContent.frame;
//            CGRect frmAggre = _lblAgree.frame;
//            frmContent.size.height = frmContent.size.height + 10;
//            frmAggre.origin.y = frmContent.origin.y + frmContent.size.height + 5;
            
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else {
        [self backToRoot];
    }
}

@end
