//
//  ED01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 08/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "ED01ViewController.h"
#import "Styles.h"

@interface ED01ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>{
    
    NSArray* pickerList;
    NSUserDefaults *userDefaults;
    NSString *lang;
    UIPickerView *objPickerView;
    UIToolbar *toolbar;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet CustomBtn *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelPicker;
@property (weak, nonatomic) IBOutlet UITextField *tfPicker;
@property (weak, nonatomic) IBOutlet UILabel *labelInformation;
@property (weak, nonatomic) IBOutlet UILabel *txtInput;

@end

@implementation ED01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    lang = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [_labelTitle setText: [self.jsonData objectForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [_iconTitle setImage: [UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    [self.iconTitle setHidden:YES];
    
    self.labelPicker.text = lang(@"DEPOSITO_TITLE_DUE_SETTING");
    self.txtInput.text = lang(@"DEPOSITO_SELECT_DUE_SETTING");
    [self.nextButton setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.cancelButton setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    self.labelPicker.text = @"";
    self.labelInformation.text = @"";
    self.tfPicker.hidden = YES;
    self.txtInput.userInteractionEnabled = YES;
    self.txtInput.textColor = UIColorFromRGB(const_color_gray);
    
    toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace]];

    
    self.tfPicker.inputAccessoryView = toolbar;
    self.tfPicker.inputView = objPickerView;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(pickerViewShow)];
    [self.txtInput addGestureRecognizer:singleFingerTap];
    
    UITapGestureRecognizer *removeTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(doneEditButtonTapped)];
    [self.view addGestureRecognizer:removeTap];
    
    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    [self.nextButton setColorSet:PRIMARYCOLORSET];
    [self.cancelButton addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];

        
    [self doRequest];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if([keyPath isEqualToString:@"change"]){
        
    }
}

- (void) pickerViewShow{
    objPickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT-250, SCREEN_WIDTH, 150)];
    objPickerView.backgroundColor = [UIColor whiteColor];
    objPickerView.showsSelectionIndicator = YES;
    objPickerView.delegate = self;
    objPickerView.dataSource = self;
    [objPickerView addSubview:toolbar];
    [self.view addSubview:objPickerView];
}

-(void)doneEditButtonTapped{
    [objPickerView removeFromSuperview];
}

- (void)doRequest{
    NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return pickerList.count+1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(row == 0){
        self.tfPicker.text = @"";
        self.txtInput.text = lang(@"DEPOSITO_SELECT_DUE_SETTING");
        self.txtInput.textColor = UIColorFromRGB(const_color_gray);

    }else{
        int idx = (int)row-1;
        [dataManager.dataExtra setValue:[pickerList[idx]objectForKey:@"id"] forKey:@"maturity_instr"];
        [dataManager.dataExtra setValue:[pickerList[idx]objectForKey:@"title"] forKey:@"maturity_instr_caption"];
        
        if([[pickerList[idx]objectForKey:@"option"] isEqualToString:@"NONE"]){
            [dataManager.dataExtra setValue:@"" forKey:@"aro_option"];
        }else{
            [dataManager.dataExtra setValue:[pickerList[idx]objectForKey:@"option"] forKey:@"aro_option"];
        }
        
        self.tfPicker.text = [pickerList[idx]valueForKey:@"name"];
        self.txtInput.text = [pickerList[idx]valueForKey:@"name"];
        self.txtInput.textColor = [UIColor blackColor];
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(row == 0){
        return lang(@"DEPOSITO_SELECT_DUE_SETTING");
    }else{
        return [pickerList[row-1]objectForKey:@"name"];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
//    {"response":"{\"setting_list\":[{\"name\":\"Automatic Roll Over (Deposito akan diperpanjang otomatis, dana bagi hasil akan dikreditkan ke rekening tabungan)\",\"option\":\"1\",\"title\":\"Automatic Roll Over\",\"id\":\"AUTOMATIC ROLLOVER\"},{\"name\":\"Automatic Roll Over + Bagi Hasil (Deposito akan diperpanjang otomatis dan bagi hasil akan menambah jumlah dana deposito)\",\"option\":\"2\",\"title\":\"Automatic Roll Over + Bagi Hasil\",\"id\":\"AUTOMATIC ROLLOVER\"},{\"name\":\"Cairkan deposito sekarang\",\"option\":\"\",\"id\":\"BREAK\"}],\"info\":\"Ubah Pengaturan Jatuh Tempo|Informasi:\\\\nPerubahan jenis Deposito (Perpanjangan Otomatis menjadi Otomatis Cair, ataupun sebaliknya) hanya dapat dilakukan pada bulan pertama setiap jangka waktu Deposito\"}","transaction_id":"","response_code":"00"}
    
    if([requestType isEqualToString:@"setting_deposito_info"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            if([object objectForKey:@"setting_list"]){
                pickerList = [object objectForKey:@"setting_list"];
            }
            
            if([object objectForKey:@"info"]){
                NSArray *arrInfo = [[object valueForKey:@"info"] componentsSeparatedByString:@"|"];
                self.labelPicker.text = arrInfo[0];
                self.labelInformation.text = [arrInfo[1] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            }
        }
    }
}

- (void)actionCancel{
    [self backToR];
}

- (void) actionNext{
    if([self.tfPicker.text isEqualToString:@""]){
        NSString *message = @"";
        if([lang isEqualToString:@"id"]){
           message = @"Anda belum melakukan perubahan pada deposito anda";
        }else{
           message = @"You haven't change your deposito setting";
        }
           
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                      handler:nil];

        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
           
    }else{
        [self openNextTemplate];
    }
}

@end
