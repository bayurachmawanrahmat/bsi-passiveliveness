//
//  IB07ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 10/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "IB07ViewController.h"
#import "Validation.h"

@interface IB07ViewController ()<UITextFieldDelegate>{
    NSArray *listData;
    int lastY;
    UIToolbar *toolbar;
    Validation *validate;
}

@end

@implementation IB07ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _titleBar.text = [self.jsonData valueForKey:@"title"];
    
    [_btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [_btnNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    
    [Styles setTopConstant:_topSpace];
    [_btnNext setColorSet:PRIMARYCOLORSET];
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    
    validate = [[Validation alloc]initWith:self];
    
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [self createToolbar];
    lastY = 0;
    
    [self registerForKeyboardNotifications];
}

- (void)actionCancel{
    [self backToR];
}

- (void)actionNext{
    if([self isDataFilled]){
        [self openNextTemplate];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"list_inputchangepin_debitcard"] || [requestType isEqualToString:@"list_aktivasi"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            
            listData = [dataDict objectForKey:@"list_input"];
            [dataManager.dataExtra setValue:[dataDict objectForKey:@"encrypted_zpk"] forKey:@"encKey"];
            
            if(listData.count < 1){
                [Utility showMessage:@"No Data Found" instance:self];
            }else{
                for(int x = 0; x < listData.count; x++){
                    [self createField:x];
                }
                _heightViewContent.constant = lastY;
                DLog(@"%@",[dataManager.dataExtra valueForKey:@"card_no"]);
            }
        }else{
            [Utility showMessage:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]] instance:self];
        }
    }
    
    if([requestType isEqualToString:@"inquiry_pf_hajj"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSDictionary *response = [jsonObject objectForKey:@"response"];

            listData = [response objectForKey:@"list_input"];
            NSDictionary *dataKtp = [response objectForKey:@"data_ktp"];
            NSString *nik = [dataKtp objectForKey:@"legal_id_no"];
          
            if(listData.count < 1){
                [Utility showMessage:@"No Data Found" instance:self];
            }else{
                for(int x = 0; x < listData.count; x++){
                    //[self createField:x of:nik];
                    
                    if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"nik"]){
                        [self createField:x of:nik];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"email"]){
                        [self createField:x of:[NSUserdefaultsAes getValueForKey:@"email"]];
                    }
                } 
                _heightViewContent.constant = lastY;
            }
        }else{
            [Utility showMessage:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]] instance:self];
        }
    }
    
    if([requestType isEqualToString:@"list_inputsetor"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSDictionary *response = [jsonObject objectForKey:@"response"];
    
            listData = [response objectForKey:@"list_input"];
            if(listData.count < 1){
                [Utility showMessage:@"No Data Found" instance:self];
            }else{
                for(int x = 0; x < listData.count; x++){
                    [self createField:x];
                }
                _heightViewContent.constant = lastY;
                DLog(@"%@",[dataManager.dataExtra valueForKey:@"card_no"]);
            }
        }else{
            [Utility showMessage:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]] instance:self];
        }
    }
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) createToolbar{
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
}

- (void)createField:(int)index{
    NSDictionary *dict = [listData objectAtIndex:index];
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, lastY, SCREEN_WIDTH, 60)];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, SCREEN_WIDTH-32, 20)];
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(16, 20, SCREEN_WIDTH-32, 34)];
    textField.tag = index;
    
    [label setText:[dict objectForKey:@"label"]];
    [label setFont:[UIFont fontWithName:const_font_name3 size:14.0]];
    [label setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    [self setTextFieldType:[dict valueForKey:@"data_type"] of:textField];
    [Styles setStyleTextFieldBottomLine:textField];
    [textField addTarget:self action:@selector(endEdit:) forControlEvents:UIControlEventEditingDidEnd];
    [textField setFont:[UIFont fontWithName:const_font_name1 size:14.0]];
    [textField setInputAccessoryView:toolbar];
    [textField setDelegate:self];
   // [textField setText:@""];
    
    [view addSubview:label];
    [view addSubview:textField];
    
    [_viewContent addSubview:view];
    
    lastY = lastY + view.frame.size.height + 10;
}

- (void)createField:(int)index of:(NSString*)nik{
    NSDictionary *dict = [listData objectAtIndex:index];
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, lastY, SCREEN_WIDTH, 60)];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, SCREEN_WIDTH-32, 20)];
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(16, 20, SCREEN_WIDTH-32, 34)];
    textField.tag = index;
    
    [label setText:[dict objectForKey:@"label"]];
    [label setFont:[UIFont fontWithName:const_font_name3 size:14.0]];
    [label setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    [self setTextFieldType:[dict valueForKey:@"data_type"] of:textField];
    [Styles setStyleTextFieldBottomLine:textField];
    [textField addTarget:self action:@selector(endEdit:) forControlEvents:UIControlEventEditingDidEnd];
    [textField setFont:[UIFont fontWithName:const_font_name1 size:14.0]];
    [textField setInputAccessoryView:toolbar];
    [textField setDelegate:self];
    [textField setText: nik];
       
    
    
    [view addSubview:label];
    [view addSubview:textField];
    
    [_viewContent addSubview:view];
    
    lastY = lastY + view.frame.size.height + 10;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *require = [listData[textField.tag] objectForKey:@"required"];
    int maxLength = [[listData[textField.tag] objectForKey:@"maxlength"]intValue];
    
    if([require isEqualToString:@"1"]){
        if(newText.length > maxLength){
            return NO;
        }
    }
    return YES;
}

- (void)endEdit:(UITextField*)sender{
    NSString *require = [listData[sender.tag] objectForKey:@"required"];
    NSString *type = [listData[sender.tag] objectForKey:@"data_type"];

    if([require isEqualToString:@"1"]){
        if([type isEqualToString:@"email"]){
            if(![validate validateEmail:sender.text]){
                sender.text = @"";
            }
        }
    }

    NSString *key = [listData[sender.tag]objectForKey:@"field_name"];
    [dataManager.dataExtra setValue:sender.text forKey:key];
    if([key isEqualToString:@"pin"]){
        [dataManager setPinNumber:sender.text];
    }
    
}


- (void) setTextFieldType:(NSString*) type of:(UITextField*)field{
    if([type isEqual:@"number"]){
        [field setKeyboardType:UIKeyboardTypeNumberPad];
    }else if([type isEqual:@"email"]){
        [field setKeyboardType:UIKeyboardTypeEmailAddress];
    }else if([type isEqual:@"phonenumber"]){
        [field setKeyboardType:UIKeyboardTypePhonePad];
    }else{
        [field setKeyboardType:UIKeyboardTypeDefault];
    }
}

- (BOOL) isDataFilled{
    int count = 0;
    for(UIView *view in _viewContent.subviews){
        NSString *require = [listData[count] objectForKey:@"required"];
        NSString *label = [listData[count] objectForKey:@"label"];
        NSString *type = [listData[count] objectForKey:@"data_type"];
        NSString *fieldname = [listData[count] objectForKey:@"field_name"];

       
        //if([require isEqualToString:@"1"]){
            for(UIView *inview in view.subviews){
                if([inview isKindOfClass:[UITextField class]]){
                    UITextField *textField = (UITextField*)inview;
                    [dataManager.dataExtra setValue:textField.text forKey:fieldname];
                     if([require isEqualToString:@"1"]){
                      if([textField.text isEqualToString:@""]){
                        
                        //Alert
                
                        [Utility showMessage:[NSString stringWithFormat:@"%@ perlu di lengkapi",label] enMessage:[NSString stringWithFormat:@"%@ is required",label] instance:self];
                        return NO;
                        
                    }else if([type isEqualToString:@"email"]){
                        if (![validate validateEmail:textField.text]){
                        textField.text = @"";
                        return NO;
                        }
                    }

                }
            }

        }
        
        count +=1;
    }
    return YES;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{

    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

@end
