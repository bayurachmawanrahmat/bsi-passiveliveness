//
//  SKB01ViewController.h
//  BSM-Mobile
//
//  Created by ARS on 25/09/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"
#import "TemplateViewController.h"

@interface SKB01ViewController : TemplateViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,APIManagerDelegate,UIDocumentPickerDelegate, UIDocumentMenuDelegate>

{
    UIDocumentPickerViewController *docPicker;
    UIImagePickerController *imagePicker;
    NSMutableArray *arrimg;
    
    NSString * UploadType;
    NSURL * PDFUrl;
    NSString * ImageName;
}

- (IBAction)browseFile:(id)sender;

@end
