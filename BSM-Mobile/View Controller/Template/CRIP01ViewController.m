//
//  CRIP01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CRIP01ViewController.h"
#import "PopupCoupleCRViewController.h"
#import "PopUnggahObjFotoViewController.h"
#import "JVFloatLabeledTextField.h"
#import "Utility.h"
#import "Styles.h"
#import "UIView+Info.h"

@interface CRIP01ViewController ()<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate, DataCoupleDelegate, UnggahObjectDelegate, UIScrollViewDelegate>{
    
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *titleToolbar;
    
    UIPickerView *pickLoanView;
    UIPickerView *pickMaskapaiView;
    
    UIToolbar *toolBar;
    UIToolbar *toolbarMaskapai;
    
    NSArray *listAssetType;
    NSArray *listPeriod;
    NSArray *listInsurence;
    
    NSMutableArray *listDataAddAsset;
    NSMutableArray *arDataChoiceAsset;
    NSMutableArray *lastTag;
    
    NSNumberFormatter *currencyFormatter;
    
    NSInteger idxChoiceAsset;
    NSInteger nominalChoiceAsset;
    NSInteger nIdxTenor;
    NSInteger nIdxAsuransi;
    NSInteger editCurrentTag;
    
    NSString *nMaxKesepakatan;
    NSString *textDataPasangan;
    NSString *ttlToolbar;
    NSString *ttlHeaderMaskapai;
    
    UILabel *labelIsiDataPasangan;
    
    int mTag;
    int nMaritalState;
    
    BOOL isEdited;
    BOOL stateFocus;
    BOOL maritalState;
    BOOL maritalStateFilled;
    
    double nMaxDana;
    double nTotalAmount;
    double minTotalAmount;
    double minLoan;
    double nRateTasaksi;
    
    NSArray *validateAsset;
}

@property (weak, nonatomic) IBOutlet UIView *viewTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *viewScroll;
@property (weak, nonatomic) IBOutlet UIImageView *imageStepIndicator;
@property (weak, nonatomic) IBOutlet UIView *viewButton;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageMenu;

@property (weak, nonatomic) IBOutlet UIView *viewContentScroll;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@property (weak, nonatomic) IBOutlet UIView *viewContent1;
@property (weak, nonatomic) IBOutlet UIView *viewContent2;
@property (weak, nonatomic) IBOutlet UIView *viewContent3;
@property (weak, nonatomic) IBOutlet UIView *viewContent4;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleMin;
@property (weak, nonatomic) IBOutlet UIView *viewAddItem;
@property (weak, nonatomic) IBOutlet UILabel *labelNominalTotal;
@property (weak, nonatomic) IBOutlet UIView *viewDataCouple;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleContent1;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfContent1;
@property (weak, nonatomic) IBOutlet UIView *viewLineContent1;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleContent2;
@property (weak, nonatomic) IBOutlet UIView *viewLineContent2;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfContent2;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleContent3;
@property (weak, nonatomic) IBOutlet UIView *viewLineContent3;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfContent3;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleContent4;
@property (weak, nonatomic) IBOutlet UIView *viewLineContent4;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfContent4;


@property (weak, nonatomic) IBOutlet UIButton *buttonExtend;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit;
@property (weak, nonatomic) IBOutlet UILabel *labelItem;

@property (weak, nonatomic) IBOutlet UIView *viewAkadAgreement;
@property (weak, nonatomic) IBOutlet UIButton *buttonChecked;
@property (weak, nonatomic) IBOutlet UILabel *labelChecked;
@property (weak, nonatomic) IBOutlet UIButton *buttonBaseChecked;


@end

@implementation CRIP01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    editCurrentTag = 0;
    listAssetType  = [[NSArray alloc]init];
    listPeriod = [[NSArray alloc]init];
    listInsurence = [[NSArray alloc]init];
    
    listDataAddAsset = [[NSMutableArray alloc]init];
    
    idxChoiceAsset = 0;
    nominalChoiceAsset = 0;
    isEdited = false;
    stateFocus = false;
    maritalStateFilled = false;
    nMaxDana = 0;
    nTotalAmount = 0;
    
    arDataChoiceAsset = [[NSMutableArray alloc]init];
    lastTag = [[NSMutableArray alloc]init];
    
    currencyFormatter = [[NSNumberFormatter alloc]init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self labelSetup];
    
    
    if([[NSUserdefaultsAes getValueForKey:@"email"]isEqualToString:@""] || [NSUserdefaultsAes getValueForKey:@"email"] == nil){
//        self.tfContent4.enabled = YES;
    }else{
//        self.tfContent4.enabled = NO;
        self.tfContent4.text = [NSUserdefaultsAes getValueForKey:@"email"];
    }
    
    mTag = 1;
    
    [lastTag addObject:@(mTag)];
    
    self.viewAddItem.tag = mTag;
    self.labelItem.tag = mTag + 100;

    self.buttonExtend.accessibilityLabel = [NSString stringWithFormat:@"ext_%d", mTag];
    self.buttonEdit.accessibilityLabel = [NSString stringWithFormat:@"edt_%d", mTag];
    
    [self.buttonExtend addTarget:self action:@selector(actionExpandView:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonEdit addTarget:self action:@selector(actionEditView:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.labelChecked setNumberOfLines:3];
    [self.labelChecked sizeToFit];
    
    [self.buttonBaseChecked addTarget:self action:@selector(actionAgreeAkad:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonChecked setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
    [self.buttonChecked setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    
    [self.buttonNext setEnabled:FALSE];
//    [self.buttonNext setBackgroundColor:[UIColor grayColor]];
    
    [self setupLayout];
    
    pickLoanView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    [pickLoanView setShowsSelectionIndicator:YES];
    [pickLoanView setDelegate:self];
    [pickLoanView setDataSource:self];
    [pickLoanView setTag:1];
    
    pickMaskapaiView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    [pickMaskapaiView setShowsSelectionIndicator:YES];
    [pickMaskapaiView setDelegate:self];
    [pickMaskapaiView setDataSource:self];
    [pickMaskapaiView setTag:2];

    
    [self.buttonExtend setTitle:@"+" forState:UIControlStateNormal];
    [self.buttonExtend setTitle:@"x" forState:UIControlStateSelected];
    [self.buttonExtend setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.buttonExtend setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    [toolBar setBarStyle:UIBarStyleDefault];
    [toolBar setItems:@[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]]];
    
    toolbarMaskapai = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    [toolbarMaskapai setBarStyle:UIBarStyleDefault];
    
    UILabel *labelHeaderMaskapai = [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, self.view.frame.size.width, 41.0f)];
    [labelHeaderMaskapai setFont:[UIFont fontWithName:@"Lato-Bold" size:15]];
    [labelHeaderMaskapai setNumberOfLines:2];
    [labelHeaderMaskapai setBackgroundColor:[UIColor clearColor]];
//    [labelHeaderMaskapai setTextColor:[UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0]];
    [labelHeaderMaskapai setTextColor:UIColorFromRGB(const_color_primary)];
    [labelHeaderMaskapai setTextAlignment:NSTextAlignmentLeft];
    [labelHeaderMaskapai setText:ttlHeaderMaskapai];
    
    [toolbarMaskapai setItems:@[[[UIBarButtonItem alloc]initWithCustomView:labelHeaderMaskapai],
    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]]];
    
    [self.tfContent1 setTag:1];
    [self.tfContent1 setDelegate:self];
    [self.tfContent1 setInputAccessoryView:toolBar];
    self.tfContent1.alwaysShowFloatingLabel = TRUE;

    [self.tfContent2 setInputView:pickLoanView];
    [self.tfContent2 setInputAccessoryView:toolBar];
    self.tfContent2.alwaysShowFloatingLabel = TRUE;

    [self.tfContent3 setInputView:pickMaskapaiView];
    [self.tfContent3 setInputAccessoryView:toolbarMaskapai];
//    self.tfContent3.alwaysShowFloatingLabel = TRUE;

    [self.tfContent4 setInputAccessoryView:toolBar];
    [self.tfContent4 setKeyboardType:UIKeyboardTypeEmailAddress];
    self.tfContent4.alwaysShowFloatingLabel = TRUE;
    
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.imageMenu setHidden:YES];
    
    [self doRequest];
    
    [self registerForKeyboardNotifications];
    
    [self martialStateHandler];
    
    [self.buttonCancel addTarget:self action:@selector(actionCancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
    [self.buttonNext addTarget:self action:@selector(actionNext:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.viewScroll setDelegate:self];
}

- (void) martialStateHandler{
    if (nMaritalState != 0){
        [self.viewDataCouple addSubview: [self ViewInputDataCouple]];
    }
}

- (void) doRequest{
    bool needReq = [[self.jsonData valueForKey:@"url"]boolValue];
    if (needReq) {
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}


     
- (void)doneClicked:(id)sender{
    
    [self.tfContent1 resignFirstResponder];
    [self.tfContent2 resignFirstResponder];
    [self.tfContent3 resignFirstResponder];
    [self.tfContent4 resignFirstResponder];
    
    [self.view endEditing:YES];
    
    [self.viewScroll setContentSize:CGSizeMake(self.viewContentScroll.frame.size.width, self.viewContentScroll.frame.size.height)];
}

- (void) labelSetup{
    
    if([language isEqualToString:@"id"]){
        self.labelTitleContent1.text = @"Nominal Pengajuan";
        self.labelTitleContent2.text = @"Jangka Waktu Pembiayaan";
        self.labelTitleContent3.text = @"Pilih Maskapai Asuransi";
        self.labelTitleContent4.text = @"Email";

        self.labelItem.text = @"Object Refinancing";
        self.labelTitleMin.text = @"Harap diisi kolom object refinancing/assets yang Anda miliki dengan total nilai minimal IDR.0";
        self.labelChecked.text = @"Object Refinancing / Asset yang saya isi adalah halal dan benar milik saya/pasangan";
        
        [self.buttonCancel setTitle:@"Batal" forState:UIControlStateNormal];
        [self.buttonNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        
        self.tfContent1.placeholder = @"Maksimal IDR.50,000,000";
        self.tfContent2.placeholder = @"Maksimal 5 Tahun";
        self.tfContent3.placeholder = @"";
//        self.tfContent3.placeholder = @"Bank dapat merubah pilihan asuransi dalam rangka penyebaran resiko";
        self.tfContent4.placeholder = @"Masukkan Email Anda";
        
        ttlToolbar = @"Selesai";
        ttlHeaderMaskapai = @"";
//        ttlHeaderMaskapai = @"Bank dapat merubah pilihan asuransi dalam rangka penyebaran resiko";
    }else{
        self.labelTitleContent1.text = @"Financing Amount";
        self.labelTitleContent2.text = @"Tenor (years)";
        self.labelTitleContent3.text = @"Select Insurance";
        self.labelTitleContent4.text = @"Email";
        
        self.labelItem.text = @"Refinancing Object";
        self.labelTitleMin.text = @"Please fill in the object refinancing/assets column that you have with a minimum total value of IDR.0";
        self.labelChecked.text = @"Refinancing Object / Assets Owned that i have filled in this application are halal and belong to me / spouse";
        
        [self.buttonCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.buttonNext setTitle:@"Next" forState:UIControlStateNormal];
        
        self.tfContent1.placeholder = @"Maximum IDR.50,000,000";
        self.tfContent2.placeholder = @"Maximum 5 Years";
        self.tfContent3.placeholder = @"";
//        self.tfContent3.placeholder = @"Banks can change insurance options in order to spread risk";
        self.tfContent4.placeholder = @"Enter Your Email Address";
        
        ttlToolbar = @"Done";
//        ttlHeaderMaskapai = @"Banks can change insurance options in order to spread risk";
        ttlHeaderMaskapai = @"";
    }
    
}

- (void) setupLayout{
        CGRect frmVwTitle = self.viewTitle.frame;
        CGRect frmImgStepper = self.imageStepIndicator.frame;
        CGRect frmVwScroll = self.viewScroll.frame;
        CGRect frmVwBtn = self.viewButton.frame;
        
        CGRect frmVwContentScroll = self.viewContentScroll.frame;
        CGRect frmVwAkadAgreement = self.viewAkadAgreement.frame;
        CGRect frmBtnBatal = self.buttonCancel.frame;
        CGRect frmBtnNext = self.buttonNext.frame;
        
        CGRect frmVwContent1 = self.viewContent1.frame;
        CGRect frmVwContent2 = self.viewContent2.frame;
        CGRect frmVwContent3 = self.viewContent3.frame;
        CGRect frmVwContent4 = self.viewContent4.frame;
    
        CGRect frmLblTitleMin = self.labelTitleMin.frame;
        CGRect frmVwTambahBarang = self.viewAddItem.frame;
        CGRect frmLblNomTotal = self.labelNominalTotal.frame;
        CGRect frmVwDataPasangan = self.viewDataCouple.frame;
        
        CGRect frmLblTitleCntn1 = self.labelTitleContent1.frame;
        CGRect frmVwEdContn1 = self.tfContent1.frame;
        CGRect frmVwLineCntn1 = self.viewLineContent1.frame;
        
        CGRect frmLblTitleCntn2 = self.labelTitleContent2.frame;
        CGRect frmEdDropDownWaktu = self.tfContent2.frame;
        CGRect frmVwLineCntn2 = self.viewLineContent2.frame;
        
        CGRect frmLblTitleCntn3 = self.labelTitleContent3.frame;
        CGRect frmEdDropDownMaksapai = self.tfContent3.frame;
        CGRect frmVwLineCntn3 = self.viewLineContent3.frame;
    
        CGRect frmLblTitleCntn4 = self.labelTitleContent4.frame;
        CGRect frmTfContent4 = self.tfContent4.frame;
        CGRect frmVwLineCntn4 = self.viewLineContent4.frame;
        
        CGRect frmBtnExtView = self.buttonExtend.frame;
        CGRect frmBtnEditView = self.buttonEdit.frame;
        CGRect frmLblBarang = self.labelItem.frame;
        
        CGRect frmBtnChecked = self.buttonChecked.frame;
        CGRect frmBtnBaseChecked = self.buttonBaseChecked.frame;
        CGRect frmLblChecked = self.labelChecked.frame;
        
        CGFloat sizeWidth = SCREEN_WIDTH - 32;
        
        
        frmVwTitle.origin.x = 0;
        frmVwTitle.origin.y = TOP_NAV;
        frmVwTitle.size.width = SCREEN_WIDTH;
        
        frmImgStepper.origin.x = 16;
        frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
        frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x *2);
        frmImgStepper.size.height = 30;
        
        frmVwBtn.origin.x = 0;
        frmVwBtn.size.width = SCREEN_WIDTH;
        
        frmVwScroll.origin.y = frmImgStepper.origin.y + frmImgStepper.size.height + 8;
        frmVwScroll.origin.x = 0;
        frmVwScroll.size.width = SCREEN_WIDTH;
        
        frmVwContentScroll.origin.x = 0;
        frmVwContentScroll.origin.y = 0;
        frmVwContentScroll.size.width = SCREEN_WIDTH;
        
        frmVwAkadAgreement.origin.y = 0;
        frmVwAkadAgreement.origin.x = 0;
        frmVwAkadAgreement.size.width = frmVwBtn.size.width;
        
        frmBtnChecked.origin.x = 16;
        frmBtnChecked.origin.y = 8;
        
        frmLblChecked.origin.y = frmBtnChecked.origin.y;
        frmLblChecked.origin.x = frmBtnChecked.origin.x + frmBtnChecked.size.width + 16;
        frmLblChecked.size.width = frmVwAkadAgreement.size.width - frmLblChecked.origin.x - 16;
        
        frmBtnBaseChecked.origin.x = 0;
        frmBtnBaseChecked.origin.y = 0;
        frmBtnBaseChecked.size.width = frmVwAkadAgreement.size.width;
        
        frmVwAkadAgreement.size.height = frmLblChecked.origin.y +frmLblChecked.size.height;
        frmBtnBaseChecked.size.height = frmVwAkadAgreement.size.height;
        
        frmBtnBatal.origin.x = 16;
        frmBtnBatal.origin.y = frmVwAkadAgreement.origin.y + frmVwAkadAgreement.size.height + 16;
        frmBtnBatal.size.height = 40;
        frmBtnBatal.size.width = frmVwBtn.size.width / 2 - (frmBtnBatal.origin.x *2);
        
        frmBtnNext.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
        frmBtnNext.origin.y = frmBtnBatal.origin.y;
        frmBtnNext.size.width = frmVwBtn.size.width - frmBtnNext.origin.x -16;
        frmBtnNext.size.height = 40;
        
        frmVwBtn.size.height = frmBtnBatal.origin.y + frmBtnBatal.size.height + 5;
        
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 8;
        
        if (IPHONE_X || IPHONE_XS_MAX) {
            frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
        }
        
        frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;
        
        //Content I
        frmVwContent1.origin.x = 0;
        frmVwContent1.origin.y = 0;
        frmVwContent1.size.width = frmVwContentScroll.size.width;
        
        frmLblTitleCntn1.origin.x = 16;
        frmLblTitleCntn1.origin.y = 0;
        frmLblTitleCntn1.size.height = [Utility heightLabelDynamic:self.labelTitleContent1 sizeWidth:sizeWidth sizeFont:15];
        frmLblTitleCntn1.size.width = frmVwContent1.size.width - (frmLblTitleCntn1.origin.x *2);
        
        frmVwEdContn1.origin.x = frmLblTitleCntn1.origin.x;
        frmVwEdContn1.origin.y = frmLblTitleCntn1.origin.y + frmLblTitleCntn1.size.height + 2;
        frmVwEdContn1.size.width = frmLblTitleCntn1.size.width;
        
        frmVwLineCntn1.origin.y = frmVwEdContn1.origin.y + frmVwEdContn1.size.height;
        frmVwLineCntn1.origin.x = frmVwEdContn1.origin.x;
        frmVwLineCntn1.size.width = frmVwEdContn1.size.width;
        
        frmVwContent1.size.height = frmVwLineCntn1.origin.y + frmVwLineCntn1.size.height;
        
        //Content II
        frmVwContent2.origin.y = frmVwContent1.origin.y + frmVwContent1.size.height + 16;
        frmVwContent2.origin.x = frmVwContent1.origin.x;
        frmVwContent2.size.width = frmVwContent1.size.width;
        
        frmLblTitleCntn2.origin.y = 0;
        frmLblTitleCntn2.origin.x = 16;
        frmLblTitleCntn2.size.height = [Utility heightLabelDynamic:self.labelTitleContent2 sizeWidth:sizeWidth sizeFont:15];
        frmLblTitleCntn2.size.width = frmVwContent2.size.width - (frmLblTitleCntn2.origin.x*2);
        
        frmEdDropDownWaktu.origin.x = frmLblTitleCntn2.origin.x;
        frmEdDropDownWaktu.origin.y = frmLblTitleCntn2.origin.y + frmLblTitleCntn2.size.height + 2;
        frmEdDropDownWaktu.size.width = frmLblTitleCntn2.size.width;
        
        frmVwLineCntn2.origin.y = frmEdDropDownWaktu.origin.y + frmEdDropDownWaktu.size.height;
        frmVwLineCntn2.origin.x = frmEdDropDownWaktu.origin.x;
        frmVwLineCntn2.size.width = frmEdDropDownWaktu.size.width;
        
        frmVwContent2.size.height = frmVwLineCntn2.origin.y + frmVwLineCntn2.size.height;
        
        //Content III
        frmVwContent3.origin.y = frmVwContent2.origin.y + frmVwContent2.size.height + 16;
        frmVwContent3.origin.x = frmVwContent2.origin.x;
        frmVwContent3.size.width = frmVwContent2.size.width;
        
        frmLblTitleCntn3.origin.y = 0;
        frmLblTitleCntn3.origin.x = 16;
        frmLblTitleCntn3.size.height = [Utility heightLabelDynamic:self.labelTitleContent3 sizeWidth:sizeWidth sizeFont:15];
        frmLblTitleCntn3.size.width = frmVwContent3.size.width - (frmLblTitleCntn3.origin.x * 2);
        
        frmEdDropDownMaksapai.origin.y = frmLblTitleCntn3.origin.y + frmLblTitleCntn3.size.height + 2;
        frmEdDropDownMaksapai.origin.x = frmLblTitleCntn3.origin.x;
        frmEdDropDownMaksapai.size.width = frmVwContent3.size.width;
        
        frmVwLineCntn3.origin.y = frmEdDropDownMaksapai.origin.y + frmEdDropDownMaksapai.size.height - 5;
        frmVwLineCntn3.origin.x = frmEdDropDownMaksapai.origin.x;
        frmVwLineCntn3.size.width = frmEdDropDownMaksapai.size.width - (frmVwLineCntn3.origin.x * 2);
        
        frmVwContent3.size.height = frmVwLineCntn3.origin.y + frmVwLineCntn3.size.height;
    
        //Content iv
        frmVwContent4.origin.y = frmVwContent3.origin.y + frmVwContent3.size.height + 16;
        frmVwContent4.origin.x = frmVwContent3.origin.x;
        frmVwContent4.size.width = frmVwContent3.size.width;
        
        frmLblTitleCntn4.origin.y = 0;
        frmLblTitleCntn4.origin.x = 16;
        frmLblTitleCntn4.size.height = [Utility heightLabelDynamic:self.labelTitleContent3 sizeWidth:sizeWidth sizeFont:15];
        frmLblTitleCntn4.size.width = frmVwContent4.size.width - (frmLblTitleCntn4.origin.x * 2);
        
        frmTfContent4.origin.y = frmLblTitleCntn4.origin.y + frmLblTitleCntn4.size.height + 2;
        frmTfContent4.origin.x = frmLblTitleCntn4.origin.x;
        frmTfContent4.size.width = frmVwContent4.size.width;
        
        frmVwLineCntn4.origin.y = frmTfContent4.origin.y + frmTfContent4.size.height - 5;
        frmVwLineCntn4.origin.x = frmTfContent4.origin.x;
        frmVwLineCntn4.size.width = frmTfContent4.size.width - (frmVwLineCntn4.origin.x * 2);
    
        frmVwContent4.size.height = frmVwLineCntn4.origin.y + frmVwLineCntn4.size.height;
        
        frmLblTitleMin.origin.y = frmVwContent4.origin.y + frmVwContent4.size.height + 16;
        frmLblTitleMin.origin.x = 16;
        frmLblTitleMin.size.height = [Utility heightLabelDynamic:self.labelTitleMin sizeWidth:sizeWidth sizeFont:15];
        frmLblTitleMin.size.width = SCREEN_WIDTH - (frmLblTitleMin.origin.x * 2);
        
        frmVwTambahBarang.origin.y = frmLblTitleMin.origin.y + frmLblTitleMin.size.height + 8;
        frmVwTambahBarang.origin.x = 16;
        frmVwTambahBarang.size.width = SCREEN_WIDTH - (frmVwTambahBarang.origin.x *2);
        
        
        frmBtnExtView.origin.x = frmVwTambahBarang.size.width - frmBtnExtView.size.width - 16;
        
        frmLblBarang.origin.x = 8;
        frmLblBarang.origin.y = 11;
        if ([self checkItem]) {
            frmLblBarang.size.height = [Utility heightLabelDynamic:self.labelItem sizeWidth:frmBtnExtView.origin.x - (frmLblBarang.origin.x *2) sizeFont:15];
            frmLblBarang.size.width = frmBtnExtView.origin.x - (frmLblBarang.origin.x *2);
        }else{
            frmBtnEditView.origin.x = frmBtnExtView.origin.x - frmBtnEditView.size.width;
            frmLblBarang.size.height = [Utility heightLabelDynamic:self.labelItem sizeWidth:frmBtnEditView.origin.x - (frmLblBarang.origin.x *2) sizeFont:15];
            frmLblBarang.size.width = frmBtnEditView.origin.x - (frmLblBarang.origin.x *2);
        }
        
        frmVwTambahBarang.size.height = frmLblBarang.origin.y + frmLblBarang.size.height + frmLblBarang.origin.y;
        frmBtnExtView.size.height  = frmVwTambahBarang.size.height;
        frmBtnEditView.size.height = frmVwTambahBarang.size.height;
        
        frmLblNomTotal.origin.y  = frmVwTambahBarang.origin.y + frmVwTambahBarang.size.height + 16;
        frmLblNomTotal.origin.x = frmVwContentScroll.size.width - frmLblNomTotal.size.width - 32;
        
        frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
        frmVwDataPasangan.origin.x = 16;
        frmVwDataPasangan.size.height = 34;
        frmVwDataPasangan.size.width = frmVwContentScroll.size.width - 32;
        
        frmVwContentScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;
    //    frmVwContentScroll.size.height = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;

        UIView* viewStepper = [[UIView alloc]initWithFrame:frmImgStepper];
        [Styles createProgress:viewStepper step:2 from:4];
        
        [self.viewContentScroll addSubview:viewStepper];
        
        self.viewTitle.frame = frmVwTitle;
        self.imageStepIndicator.frame = frmImgStepper;
        self.viewScroll.frame = frmVwScroll;
        self.viewButton.frame = frmVwBtn;
        
        [self.imageStepIndicator setHidden:YES];
        [self.view addSubview:viewStepper];
        
        self.viewContentScroll.frame = frmVwContentScroll;
        self.viewAkadAgreement.frame = frmVwAkadAgreement;
        self.buttonCancel.frame = frmBtnBatal;
        self.buttonNext.frame = frmBtnNext ;
        
        self.viewContent1.frame = frmVwContent1;
        self.viewContent2.frame = frmVwContent2;
        self.viewContent3.frame = frmVwContent3;
        self.viewContent4.frame = frmVwContent4;
    
        self.labelTitleMin.frame = frmLblTitleMin;
        self.viewAddItem.frame = frmVwTambahBarang;
        self.labelNominalTotal.frame = frmLblNomTotal;
        self.viewDataCouple.frame = frmVwDataPasangan;
        
        self.labelTitleContent1.frame = frmLblTitleCntn1;
        self.tfContent1.frame = frmVwEdContn1;
        self.viewLineContent1.frame = frmVwLineCntn1;
        
        self.labelTitleContent2.frame = frmLblTitleCntn2;
        self.tfContent2.frame = frmEdDropDownWaktu;
        self.viewLineContent2.frame = frmVwLineCntn2;
        
        self.labelTitleContent3.frame = frmLblTitleCntn3;
        self.tfContent3.frame = frmEdDropDownMaksapai;
        self.viewLineContent3.frame = frmVwLineCntn3;
    
        self.labelTitleContent4.frame = frmLblTitleCntn4;
        self.tfContent4.frame = frmTfContent4;
        self.viewLineContent4.frame = frmVwLineCntn4;
        
        self.buttonExtend.frame = frmBtnExtView;
        self.labelItem.frame = frmLblBarang;
        self.buttonEdit.frame = frmBtnEditView;
        
        self.buttonBaseChecked.frame = frmBtnBaseChecked;
        self.buttonChecked.frame = frmBtnChecked;
        self.labelChecked.frame = frmLblChecked;
        
        self.viewAddItem = [self makeBorder:self.viewAddItem];
        
        [self.viewScroll setContentSize:CGSizeMake(self.viewContentScroll.frame.size.width, self.viewContentScroll.frame.size.height)];
}

-(UIView *) makeBorder : (UIView *) mVw{
    UIView *vw = mVw;
    vw.layer.borderWidth = 1;
    vw.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    vw.layer.cornerRadius = 8;
    return vw;
}

- (BOOL) checkItem{
    if (
        [self.labelItem.text isEqualToString:@"Object Refinancing"]
        || [self.labelItem.text isEqualToString:@"Add Items"]
        || [self.labelItem.text isEqualToString:@"Tambah Barang"]
        ) {
        [self.labelItem setTextColor:UIColorFromRGB(const_color_gray)];
        [self.buttonEdit setHidden:TRUE];
        return TRUE;
    }else{
        [self.labelItem setTextColor:[UIColor blackColor]];
        [self.buttonEdit setHidden:FALSE];
        return FALSE;
    }
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.viewScroll setContentSize:CGSizeMake(self.viewContentScroll.frame.size.width, self.viewContentScroll.frame.size.height + kbSize.height + 20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.viewScroll setContentSize:CGSizeMake(self.viewScroll.frame.size.width, self.viewContentScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 10, 0);
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
}

- (UIView *)ViewInputDataCouple{
        UIView *vwBase = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.viewContentScroll.frame.size.width, 34)];
        UILabel *lblDataPas = [[UILabel alloc]init];

        labelIsiDataPasangan = [[UILabel alloc]init];
        UIButton *btnShowPop = [[UIButton alloc]init];
        
        if(nMaritalState == 1){
            if ([language isEqualToString:@"id"]) {
                [lblDataPas setText:@"Data Pasangan"];
                [labelIsiDataPasangan setText:@"Isi Data Pasangan"];
            }else{
                [lblDataPas setText:@"Spouse"];
                [labelIsiDataPasangan setText:@"Fill in Spouse Data"];
            }
        }else{
            if ([language isEqualToString:@"id"]) {
                [lblDataPas setText:@"Data Tanggungan"];
                [labelIsiDataPasangan setText:@"Isi Data Tanggungan"];
            }else{
                [lblDataPas setText:@"Dependent Data"];
                [labelIsiDataPasangan setText:@"Fill in Dependent Data"];
            }
        }
        
        [lblDataPas setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [lblDataPas sizeToFit];
        
        [labelIsiDataPasangan setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [labelIsiDataPasangan sizeToFit];
        [labelIsiDataPasangan setTextColor:UIColorFromRGB(const_color_primary)];
        [labelIsiDataPasangan setTextAlignment:NSTextAlignmentRight];
        
        vwBase.layer.cornerRadius = 8;
        vwBase.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
        vwBase.layer.borderWidth = 1;
        
        [btnShowPop setTitle:@"" forState:UIControlStateNormal];
        [btnShowPop addTarget:self action:@selector(showPopupPasangan:) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect frmLblDataPas = lblDataPas.frame;
        CGRect frmLblIsiDataPas = labelIsiDataPasangan.frame;
        CGRect frmVwBase = vwBase.frame;
        CGRect frmBtnShowPop = btnShowPop.frame;
        
        frmVwBase.origin.x = 0;
        frmVwBase.size.width = self.viewDataCouple.frame.size.width;
        
        frmLblDataPas.origin.x = 16;
        frmLblDataPas.origin.y = 10;
        frmLblDataPas.size.width = frmVwBase.size.width/2 - (frmLblDataPas.origin.x *2);
        
        frmLblIsiDataPas.origin.y = frmLblDataPas.origin.y;
        frmLblIsiDataPas.origin.x = frmLblDataPas.origin.x + frmLblDataPas.size.width + 24;
        frmLblIsiDataPas.size.width = frmVwBase.size.width - frmLblIsiDataPas.origin.x - 16;
        
        frmVwBase.size.height = frmLblDataPas.size.height + (frmLblDataPas.origin.y*2);
        
        frmBtnShowPop.origin.x = 0;
        frmBtnShowPop.origin.y = 0;
        frmBtnShowPop.size.width = frmVwBase.size.width;
        frmBtnShowPop.size.height = frmVwBase.size.height;
        
        lblDataPas.frame = frmLblDataPas;
        labelIsiDataPasangan.frame = frmLblIsiDataPas;
        btnShowPop.frame = frmBtnShowPop;
        vwBase.frame = frmVwBase;
        
        
        [vwBase addSubview:lblDataPas];
        [vwBase addSubview:labelIsiDataPasangan];
        [vwBase addSubview:btnShowPop];
    
        return vwBase;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 1){
        return listPeriod.count;
    }
    else if(pickerView.tag == pickerExtraData){
        
        NSArray *arrIdxCode = [pickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [[dictExtraData valueForKey:@"Content"]componentsSeparatedByString:@"|"];

        return arrContent.count;
        } else if (pickerView.tag == pickerExtraDataSub){
        NSArray *arrIdxCode =  [pickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [NSJSONSerialization JSONObjectWithData:[[dictExtraData objectForKey:@"Content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

        NSMutableArray *brandList = [[NSMutableArray alloc]init];
        for(NSDictionary *dict in arrContent){
            NSString *brand = @"";
            brand = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Brand"]];
            [brandList addObject:brand];
        }
        return brandList.count;
            
    } else{
        return listInsurence.count;
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    [Utility resetTimer];
    NSDictionary *data;
    NSString *strPicker;
    
    if (pickerView.tag == 1) {
        data = [listPeriod objectAtIndex:row];
        int mTahun = [[data objectForKey:@"Name"]intValue]/12;
        if ([language isEqualToString:@"id"]) {
            strPicker = [NSString stringWithFormat:@"%d Tahun", mTahun];
        }else{
            strPicker = [NSString stringWithFormat:@"%d Year", mTahun];
        }
    
    }else if (pickerView.tag == pickerExtraData){
        NSArray *arrIdxCode =  [pickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [[dictExtraData valueForKey:@"Content"]componentsSeparatedByString:@"|"];

        strPicker = [NSString stringWithFormat:@"%@", arrContent[row]];
    
    } else if (pickerView.tag == pickerExtraDataSub){
        NSArray *arrIdxCode =  [pickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [NSJSONSerialization JSONObjectWithData:[[dictExtraData objectForKey:@"Content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

        //Brand Types || Eval_Value | Type
        NSMutableArray *brandList = [[NSMutableArray alloc]init];
        for(NSDictionary *dict in arrContent){
            NSString *brand = @"";
            brand = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Brand"]];
            [brandList addObject:brand];
        }
        strPicker = brandList[row];
    }
    else{
        data = [listInsurence objectAtIndex:row];
        strPicker = [data valueForKey:@"Name"];
    }
    return strPicker;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    NSDictionary *data;
        if (pickerView.tag == 1) {
            nIdxTenor = row;
            data = [listPeriod objectAtIndex:row];
            int mTahun = [[data objectForKey:@"Name"]intValue]/12;
            if ([language isEqualToString:@"id"]) {
                 [self.tfContent2 setText: [NSString stringWithFormat:@"%d Tahun", mTahun]];
            }else{
                [self.tfContent2 setText: [NSString stringWithFormat:@"%d Year", mTahun]];
            }
           
        }
        else if (pickerView.tag == pickerExtraData){
            NSArray *arrIdxCode =  [pickerView.accessibilityLabel componentsSeparatedByString:@"/"];
            NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
            NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
            NSArray *arrContent = [[dictExtraData valueForKey:@"Content"]componentsSeparatedByString:@"|"];
            
            UITextField* textField = [self getFieldByAccesibilityLabel:[dictExtraData valueForKey:@"Param_Name"] andTag:[arrIdxCode[0]intValue]];
            
            if(textField != nil){
                textField.accessibilityValue = [NSString stringWithFormat:@"%@", arrContent[row]];
                [textField setText:[NSString stringWithFormat:@"%@", arrContent[row]]];
            }
            
        } else if (pickerView.tag == pickerExtraDataSub){
            NSArray *arrIdxCode =  [pickerView.accessibilityLabel componentsSeparatedByString:@"/"];
            NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
            NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
            NSArray *arrContent = [NSJSONSerialization JSONObjectWithData:[[dictExtraData objectForKey:@"Content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

            //Brand Types || Eval_Value | Type
            NSMutableArray *brandList = [[NSMutableArray alloc]init];
            for(NSDictionary *dict in arrContent){
                NSString *brand = @"";
                brand = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Brand"]];
                    [brandList addObject:brand];
            }
            UITextField* textField = [self getFieldByAccesibilityLabel:[dictExtraData valueForKey:@"Param_Name"] andTag:[arrIdxCode[0]intValue]];
            
                if(brandList.count != 0){
                    [textField setText:[NSString stringWithFormat:@"%@", brandList[row]]];
                }
            
        }
        else{
            nIdxAsuransi = row;
            data = [listInsurence objectAtIndex:row];
            [self.tfContent3 setText:[data objectForKey:@"Name"]];
            self.tfContent3.placeholder = ttlHeaderMaskapai;
        }
}

-(UITextField*) getFieldByAccesibilityLabel:(NSString*)label andTag:(int)tag{
    
    NSInteger idxVwTambah = 4 + tag;
    UIView *vwTambah = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwBase = [vwTambah.subviews objectAtIndex: vwTambah.subviews.count-1];
    UIView *vwField;
    for(UIView* view in vwBase.subviews){
        if([view.accessibilityLabel isEqualToString:@"field"]){
            vwField = view;
        }
    }
    UITextField* textField;

    for(UIView* view in vwField.subviews){
        if([view.accessibilityLabel isEqualToString:label]){
            textField = (UITextField*) view;
            return textField;
        }
    }
    
    return nil;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag <= 3){
        
        if (isEdited) {
            if (editCurrentTag != 0) {
                isEdited = false;
                editCurrentTag = 0;
            }
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [Utility resetTimer];
   if (textField.tag == 1) {
       stateFocus = true;
        
        if (textField.tag == 100 + mTag) {
            NSInteger idxView = 3 + mTag;
            UIView *vwTambahBarang = [self.viewContentScroll.subviews objectAtIndex:idxView];
            UIView *vwAddonAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
            UIButton * btnDone = [vwAddonAsset.subviews objectAtIndex:[vwAddonAsset.subviews count]-2];

            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring
                         stringByReplacingCharactersInRange:range withString:string];

            if (btnDone.tag == mTag) {
                if ([substring isEqualToString:@""] || substring == nil) {
//                    [btnDone setHidden:YES];
                }else{
                    NSString *strNom = [substring stringByReplacingOccurrencesOfString:@"," withString:@""];
                    nominalChoiceAsset = [strNom integerValue];
//                    [btnDone setHidden:NO];
                }

            }
        }
       
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        NSInteger newValue = [newText integerValue];
        
       if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
       }
//       else if ([newText length] > nMaxDana){
//           NSString *strMessageMax = @"";
//           if ([language isEqualToString:@"id"]) {
//               strMessageMax = @"Maksimum Pengajuan pembiayaan ";
//           }else{
//               strMessageMax = @"Maximum Financing Amount is ";
//           }
//            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:nMaxDana]];
//            [Utility showMessage:[NSString stringWithFormat:@"%@ IDR. %@", strMessageMax, [Utility formatCurrencyValue:nMaxDana]] mVwController:self mTag:07];
//       }
       else{
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
//            if(newValue > nMaxDana){
//                if ([language isEqualToString:@"id"]) {
//                    [Utility showMessage:@"Nominal Pengajuan melebihi batas yang di tawarkan" mVwController:self mTag:06];
//                }else{
//                    [Utility showMessage:@"Financing Amount exceeding the limit offered" mVwController:self mTag:06];
//                }
//                textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:nMaxDana]];
//            }
       }
        return NO;
        
    }else{
        NSLog(@"teef access %@", textField.accessibilityLabel);
        NSArray *arAccesbility = [textField.accessibilityLabel componentsSeparatedByString:@"_"];
        NSInteger idxAccesbility = [[arAccesbility objectAtIndex:1]integerValue];
        
        NSInteger idxView = 4 + idxAccesbility;
        UIView *vwTambahBarang = [self.viewContentScroll.subviews objectAtIndex:idxView];
        UIView *vwAddonAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1]; //disni error
        UIButton * btnDone = [vwAddonAsset.subviews objectAtIndex:[vwAddonAsset.subviews count]-3];
        
        NSArray *arrLastTag = (NSArray *) lastTag;
        
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        
        if (arrLastTag.count == idxAccesbility) {
            NSNumber *numIdx = [arrLastTag objectAtIndex:idxAccesbility-1];
            if (idxAccesbility == [numIdx integerValue]) {
                
                if (btnDone.tag == idxAccesbility) {
                    if ([substring isEqualToString:@""] || substring == nil) {
//                        [btnDone setHidden:YES];
                    }else{
                        NSString *strNom = [substring stringByReplacingOccurrencesOfString:@"," withString:@""];
                        nominalChoiceAsset = [strNom integerValue];
//                        [btnDone setHidden:NO];
                    }
                    
                }
            }
        }
        else{
            if (btnDone.tag == idxAccesbility) {
                if ([substring isEqualToString:@""] || substring == nil) {
//                    [btnDone setHidden:YES];
                }else{
                    NSString *strNom = [substring stringByReplacingOccurrencesOfString:@"," withString:@""];
                    nominalChoiceAsset = [strNom integerValue];
//                    [btnDone setHidden:NO];
                }
                
            }
        }
        
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        NSInteger newValue = [newText integerValue];
        
        if ([newText length] == 0 || newValue == 0)
            textField.text = nil;
        else if ([newText length] > [nMaxKesepakatan length])
            textField.text = textField.text;
        else
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
            if(newValue > [nMaxKesepakatan doubleValue]){
                textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[nMaxKesepakatan doubleValue]]];
            }
        return NO;
    }
    
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField.tag == 1) {
        if (stateFocus) {
            double minimumNominal = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@""]doubleValue];
            
            NSString *strTitleMin, *strMessage, *strMessageMax;
            
            if ([language isEqualToString:@"id"]) {
                strTitleMin = @"Harap diisi kolom object refinancing/assets yang Anda miliki dengan total nilai minimal IDR.";
                strMessage = @"Minimal Pengajuan pembiayaan ";
                strMessageMax = @"Maksimum Pengajuan pembiayaan ";
            }else{
                strTitleMin = @"Please fill in the object refinancing/assets column that you have with a minimum total value of IDR.";
//                strMessage = @"Minimum Financing Amount is ";
//                strMessageMax = @"Maximum Financing Amount is ";
                strMessage = @"The Nominal Value Proposed is less than";
                strMessageMax = @"The Nominal Value Proposed is more than";
            }
            
            if (minimumNominal < minLoan) {
                textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:minLoan]];
                [Utility showMessage:[NSString stringWithFormat:@"%@ IDR. %@", strMessage, [Utility formatCurrencyValue:minLoan]] mVwController:self mTag:07];
            }else if (minimumNominal > nMaxDana) {
                textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:nMaxDana]];
                [Utility showMessage:[NSString stringWithFormat:@"%@ IDR. %@", strMessageMax, [Utility formatCurrencyValue:nMaxDana]] mVwController:self mTag:07];
            }else{
                
                double persentaseMinNominal = (minimumNominal * nRateTasaksi)/100;
                
                NSString *strMinNominal = [Utility formatCurrencyValue:minimumNominal + persentaseMinNominal];
                [self.labelTitleMin setText:[NSString stringWithFormat:@"%@%@",strTitleMin,strMinNominal]];
                CGRect frmLbTitleMin = self.labelTitleMin.frame;
                
                minTotalAmount = minimumNominal + persentaseMinNominal;
                
                frmLbTitleMin.origin.x = 16;
                frmLbTitleMin.size.height = [Utility heightLabelDynamic:self.labelTitleMin sizeWidth:SCREEN_WIDTH-32 sizeFont:15];
                frmLbTitleMin.size.width = SCREEN_WIDTH - (frmLbTitleMin.origin.x * 2);
                
                self.labelTitleMin.frame = frmLbTitleMin;
                
                [self rechangePostAxisY];
            }

        }
    }
    return YES;
}

-(void) rechangePostAxisY{
    UILabel *lblMinNom = [self.viewContentScroll.subviews objectAtIndex:4];
    NSLog(@"rechange %@", lblMinNom.text);
    
    CGFloat lblMinNomPosY = lblMinNom.frame.origin.y + lblMinNom.frame.size.height + 16;
    CGFloat lastHight = 0;
    for (int x = 5; x < [self.viewContentScroll.subviews count]-1; x++) {
        UIView *vwTambah = [self.viewContentScroll.subviews objectAtIndex:x];
        CGRect frmVwTambah = vwTambah.frame;
        if (x > 5) {
            lblMinNomPosY = lblMinNomPosY + lastHight + 16;
        }
        frmVwTambah.origin.y = lblMinNomPosY;
        lastHight = frmVwTambah.size.height;
        vwTambah.frame = frmVwTambah;
        NSLog(@"+barang %f", vwTambah.frame.size.height);

    }

    CGRect frmLblNomTotal = self.labelNominalTotal.frame;
    CGRect frmVwDataPasangan = self.viewDataCouple.frame;
    CGRect frmVwContenScroll = self.viewContentScroll.frame;
    
    frmLblNomTotal.origin.y = lblMinNomPosY + lastHight + 16;
    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height;
//    frmVwContenScroll.size.height = frmLblNomTotal.origin.y + frmLblNomTotal.size.height;
    frmVwContenScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height;
    
    self.labelNominalTotal.frame = frmLblNomTotal;
    self.viewDataCouple.frame = frmVwDataPasangan;
    self.viewContentScroll.frame = frmVwContenScroll;
    
    [self.viewScroll setContentSize:CGSizeMake(SCREEN_WIDTH, self.viewContentScroll.frame.size.height)];
   
}

- (UIButton*) getSelectedBtn : (NSString*) accessLbl fromSuperview : (UIView*) supervw{
    UIButton *returnBtn = nil;
    for(id view in [supervw subviews]){
        if([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*) view;
            if([btn.accessibilityLabel isEqualToString:accessLbl]){
                btn.selected = YES;
                returnBtn = btn;
            }else{
                btn.selected = NO;
            }
        }
    }
    
    return returnBtn;
}

- (void) actionRadioBtn : (UIButton *) sender{
    NSArray *arID = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
    int idxVwTambah = (int)sender.tag + 4;
    int idxCode = [arID[1] intValue]-1;
    idxChoiceAsset = [arID[1] intValue];
    [self selectedRadioBtn:sender.accessibilityLabel fromSuperview:[sender superview]];
    
    UIView *vwRadio = sender.superview;
    UIView *vwBase = vwRadio.superview;

    for(UIView* view in vwBase.subviews){
        if(![view.accessibilityLabel isEqualToString:@"radio"]){
            [view removeFromSuperview];
        }
    }
    
    UIView *vwField = [[UIView alloc]init];
    vwField.tag = sender.tag;
    vwField.accessibilityLabel = [NSString stringWithFormat:@"field"];
    
    NSDictionary *dictAsset = [listAssetType objectAtIndex:idxCode];
    NSArray *arrExtraData = [dictAsset objectForKey:@"Extra_Data"];

    CGFloat startY = 0;
    for(int x=0; x < arrExtraData.count; x++){
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(16, startY, vwBase.frame.size.width - 32, 24)];
        UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(16, startY + lblTitle.frame.size.height, lblTitle.frame.size.width, 34)];
        textField.borderStyle = UITextBorderStyleNone;
        textField.keyboardType = UIKeyboardTypeDefault;
        [textField setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
//        [textField setTextColor:UIColorFromRGB(const_color_gray)];
        textField.tag = sender.tag;
//        [textField.layer setBorderWidth:.5];
//        [textField.layer setBorderColor: [[UIColor grayColor]CGColor]];
        
        [lblTitle setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
        [lblTitle setText:[NSString stringWithFormat:@"%@", [[arrExtraData objectAtIndex:x] valueForKey:@"Title"]]];
//        [lblTitle setTextColor:UIColorFromRGB(const_color_darkgray)];
        [lblTitle setTextColor:[UIColor blackColor]];
        
        [vwField addSubview:lblTitle];
        [vwField addSubview:textField];
        startY = textField.frame.size.height + textField.frame.origin.y + 4;
        [Styles setStyleTextFieldBottomLine:textField];

        [self setTypeField:textField withType:[arrExtraData objectAtIndex:x] andCode:idxCode andIdx:x];

    }
    vwField.frame = CGRectMake(0, vwRadio.frame.origin.y + vwRadio.frame.size.height, vwRadio.frame.size.width, startY);
    [vwBase addSubview:vwField];
    vwBase.frame = CGRectMake(vwBase.frame.origin.x, vwBase.frame.origin.y, vwBase.frame.size.width,  vwField.frame.origin.y + vwField.frame.size.height);
    
    
    ///Adding Nominal of asset
    [self addNominal:vwField baseView:vwBase dxCode:[arID[1] intValue]];
    
    UIView *vwTambahBarang = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
    UILabel *lblNomMinimum = [vwParentRbAsset.subviews objectAtIndex:[vwParentRbAsset.subviews count]-1];
    UILabel *lblNomTotal = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-2];
    UIView *vwDataPasangan = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-1];
    
    NSString *strCaption;
    
    if([[dictAsset valueForKey:@"Name"] containsString:@"Tanah"]){
        if ([language isEqualToString:@"id"]) {
//            strCaption = @"Nilai maksimal akseptasi tanah >= 50m2 adalah sebesar IDR.";
            strCaption = @"Nilai maksimal akseptasi tanah >= 50m2 adalah sebesar IDR.";
        }else{
//            strCaption = @"Maximum Calculation Tanah for >= 50m2 is IDR. ";
            strCaption = @"Maximum Calculation Tanah for >= 50m2 is IDR. ";
        }
    }else{
        if ([language isEqualToString:@"id"]) {
            strCaption = @"Maksimum Nilai Kesepakatan IDR.";
        }else{
            strCaption = @"Maximum Calculation Value IDR.";
        }
    }
    
    nMaxKesepakatan = [dictAsset valueForKey:@"Value"];
    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[[dictAsset valueForKey:@"Value"]doubleValue]]]];

    CGRect frmVwTambah = vwTambahBarang.frame;
    CGRect frmVwBase = vwBase.frame;
    CGRect frmLblNom = lblNomMinimum.frame;
    
    if (IPHONE_5) {
        frmLblNom.origin.x = 16;
        frmLblNom.size.height = [Utility heightLabelDynamic:lblNomMinimum sizeWidth:frmVwBase.size.width - (frmLblNom.origin.x *2) sizeFont:10];
        frmLblNom.size.width = frmVwBase.size.width - (frmLblNom.origin.x *2);
       
    }
    frmLblNom.size.height = [Utility heightLabelDynamic:lblNomMinimum sizeWidth:frmVwBase.size.width - (frmLblNom.origin.x *2) sizeFont:15];
    lblNomMinimum.frame = frmLblNom;
    
    if([lblNomTotal isKindOfClass:[UILabel class]]){
        CGRect frmLblNomTotal = lblNomTotal.frame;
        CGRect frmVwDataPasangan = vwDataPasangan.frame;
        
        frmVwTambah.size.height = vwBase.frame.origin.y + vwBase.frame.size.height + 20;
        frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
        frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
        
        vwTambahBarang.frame = frmVwTambah;
        lblNomTotal.frame = frmLblNomTotal;
        vwDataPasangan.frame = frmVwDataPasangan;
    }
    
    [self rechangePostAxisY];
}

- (void) setTypeField:(UITextField*) textField withType:(NSDictionary*) dictType andCode:(int) code andIdx:(int)idxObject{
    
    if([[dictType objectForKey:@"Type"] isEqualToString:@"picklist"]){
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
        [picker setDataSource: self];
        [picker setDelegate: self];
        picker.showsSelectionIndicator = YES;
        picker.tag = pickerExtraData;
        picker.accessibilityLabel = [NSString stringWithFormat:@"%ld/%d/%d",(long)textField.tag,code,idxObject];
       
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Pilih %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Select %@",[dictType objectForKey:@"Title"]];
        }
        textField.inputView = picker;
        textField.inputAccessoryView = toolBar;
        
    }else if([[dictType objectForKey:@"Type"] isEqualToString:@"freetext"] || [[dictType objectForKey:@"Type"] isEqualToString:@"freeteext"] ){
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputAccessoryView = toolBar;
        
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Masukkan %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Enter %@",[dictType objectForKey:@"Title"]];
        }
    }else if([[dictType objectForKey:@"Type"] isEqualToString:@"subpicklist"]){
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
        [picker setDataSource: self];
        [picker setDelegate: self];
        picker.showsSelectionIndicator = YES;
        picker.accessibilityLabel = [NSString stringWithFormat:@"%ld/%d/%d",(long)textField.tag,code,idxObject];
        picker.tag = pickerExtraDataSub;

        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputView = picker;
        textField.inputAccessoryView = toolBar;
        
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Pilih %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Select %@",[dictType objectForKey:@"Title"]];
        }
    }else if([[dictType objectForKey:@"Type"] isEqualToString:@"numbertext"] || [[dictType objectForKey:@"Type"] isEqualToString:@"numberText"]){
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputAccessoryView = toolBar;
                
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Masukkan %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Enter %@",[dictType objectForKey:@"Title"]];
        }
    }else{
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputAccessoryView = toolBar;
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Masukkan %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Enter %@",[dictType objectForKey:@"Title"]];
        }
    }
    
    if([textField.accessibilityLabel isEqualToString:@"Surface_Area"]){
        [textField addTarget:self action:@selector(textFieldDidEndTanahValidation:) forControlEvents:UIControlEventEditingDidEnd];
    }else if([textField.accessibilityLabel isEqualToString:@"Production_Year"] || [textField.accessibilityLabel isEqualToString:@"Purchase_Year"]){
        [textField addTarget:self action:@selector(textFieldPurchaseYearValidation:) forControlEvents:UIControlEventEditingDidEnd];
    }
}



- (void) addNominal: (UIView*)vwField baseView: (UIView*)vwBase dxCode:(int)idxCode{
    UILabel *lblTitle = [[UILabel alloc]init];
    UILabel *lblCurrency = [[UILabel alloc]init];
    JVFloatLabeledTextField *txtNominal = [[JVFloatLabeledTextField alloc]init];
    UIButton *btnDone = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 24, 24)];
    UIView *vwLine = [[UIView alloc]init];
    UILabel *lblNominalMinimum = [[UILabel alloc]init];
    
    UIView *viewUnggah = [[UIView alloc]init];
    viewUnggah.accessibilityIdentifier = @"objFoto";
    viewUnggah.accessibilityValue = [NSString stringWithFormat:@"%ld",(long)vwBase.tag];
    UILabel *unggahFotoLbl = [[UILabel alloc]init];
    UIImageView *imgUnggahFoto = [[UIImageView alloc]init];
    
    
    [imgUnggahFoto setImage:[[UIImage imageNamed:@"ic_fpop_tphoto"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    [imgUnggahFoto setContentMode:UIViewContentModeScaleAspectFit];
    [imgUnggahFoto setTintColor:UIColorFromRGB(const_color_primary)];
    
    if ([language isEqualToString:@"id"]) {
        [lblTitle setText:@"Nilai"];
        unggahFotoLbl.text = @"Unggah Foto dan Lokasi Barang/Asset";
    }else{
        [lblTitle setText:@"Value"];
        unggahFotoLbl.text = @"Upload Photo and Object/Asset Location";
    }
    
    [unggahFotoLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [unggahFotoLbl sizeToFit];
    
    [lblTitle setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [lblTitle sizeToFit];

    [lblCurrency setText:@"Rp "];
    [lblCurrency setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [lblCurrency sizeToFit];

    txtNominal.borderStyle = UITextBorderStyleNone;
    txtNominal.keyboardType = UIKeyboardTypeNumberPad;
    txtNominal.tag = 100 + vwBase.tag;
    txtNominal.accessibilityLabel = [NSString stringWithFormat:@"nom_%ld", vwBase.tag];
    txtNominal.delegate = self;
    txtNominal.inputAccessoryView = toolBar;
    [txtNominal setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];

    [btnDone setTitle:@"OK" forState:UIControlStateNormal];
    [btnDone setTag:vwBase.tag];
    [btnDone addTarget:self action:@selector(saveChoiceAsset:) forControlEvents:UIControlEventTouchUpInside];
    [btnDone setAccessibilityLabel:[NSString stringWithFormat:@"%d",idxCode]];
    [Styles setStyleButton:btnDone];

    [vwLine setBackgroundColor:UIColorFromRGB(const_color_gray)];

    lblNominalMinimum.accessibilityLabel = [NSString stringWithFormat:@"lblMinVal_%ld", vwBase.tag];
    [lblNominalMinimum setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [lblNominalMinimum setTextColor:UIColorFromRGB(const_color_primary)];
    [lblNominalMinimum setText:@""];

    if (IPHONE_5) {
         [lblNominalMinimum setFont:[UIFont fontWithName:@"Lato-Regular" size:10]];
    }
    
    CGRect frmVwUnggah = viewUnggah.frame;
    CGRect frmLblUnggahFoto = unggahFotoLbl.frame;
    CGRect frmImgFoto = imgUnggahFoto.frame;
    CGRect frmVwField = vwField.frame;
    CGRect frmLblTitle = lblTitle.frame;
    CGRect frmLblCurrency = lblCurrency.frame;
    CGRect frmTxtNominal = txtNominal.frame;
    CGRect frmVwLine = vwLine.frame;
    CGRect frmVwBase = vwBase.frame;
    CGRect frmBtn = btnDone.frame;
    CGRect frmLblMinimum = lblNominalMinimum.frame;
    

    frmLblTitle.origin.x = 16;
    frmVwUnggah.origin.x = 16;
    frmVwUnggah.origin.y = frmVwField.origin.y + frmVwField.size.height;
    frmVwUnggah.size.width = vwBase.frame.size.width - 32;
    frmVwUnggah.size.height = 50;
    
    frmLblUnggahFoto.origin.x = 0;
    frmLblUnggahFoto.size.width = frmVwUnggah.size.width - 30;
    frmLblUnggahFoto.size.height = 50;
    frmLblUnggahFoto.origin.y = 0;
    
    frmImgFoto.origin.x = frmLblUnggahFoto.origin.x + frmLblUnggahFoto.size.width + 5;
    frmImgFoto.size.width = 25;
    frmImgFoto.size.height = 50;
    frmImgFoto.origin.y = 0;

    frmLblCurrency.origin.x = frmLblTitle.origin.x + frmLblTitle.size.width + 10;

    frmTxtNominal.origin.x = frmLblCurrency.origin.x + frmLblCurrency.size.width +5;
    frmTxtNominal.origin.y = frmVwField.origin.y + frmVwField.size.height + frmVwUnggah.size.height;
    frmTxtNominal.size.height = 43;

    frmBtn.size.width = 70;
    frmBtn.origin.x = vwBase.frame.size.width - frmBtn.size.width - 16;
    frmBtn.origin.y = frmTxtNominal.origin.y + frmTxtNominal.size.height/3 - 3;

    frmTxtNominal.size.width = vwBase.frame.size.width - frmBtn.size.width - 32;

    frmVwLine.origin.x = frmLblCurrency.origin.x;
    frmVwLine.origin.y = frmTxtNominal.origin.y + frmTxtNominal.size.height;
    frmVwLine.size.height = 1;
    frmVwLine.size.width = vwBase.frame.size.width - frmVwLine.origin.x - frmBtn.size.width - 32;

    frmLblMinimum.origin.x = frmVwLine.origin.x;
    frmLblMinimum.origin.y = frmVwLine.origin.y + frmVwLine.size.height + 8;
    frmLblMinimum.size.height = [Utility heightLabelDynamic:lblNominalMinimum sizeWidth:frmVwLine.size.width sizeFont:12];
    frmLblMinimum.size.width = vwBase.frame.size.width - frmLblMinimum.origin.x - 8;

    frmLblTitle.origin.y = frmTxtNominal.origin.y + frmTxtNominal.size.height/3 - 3;

    frmLblCurrency.origin.y = frmLblTitle.origin.y;

    frmVwBase.size.height = frmLblMinimum.origin.y + frmLblMinimum.size.height + 16;

    viewUnggah.frame = frmVwUnggah;
    unggahFotoLbl.frame = frmLblUnggahFoto;
    imgUnggahFoto.frame = frmImgFoto;
    lblTitle.frame = frmLblTitle;
    lblCurrency.frame = frmLblCurrency;
    txtNominal.frame = frmTxtNominal;
    vwLine.frame = frmVwLine;
    lblNominalMinimum.frame = frmLblMinimum;
    vwBase.frame = frmVwBase;
    btnDone.frame=  frmBtn;

//    [vwBase addSubview:vwRadio];
    [viewUnggah addSubview:unggahFotoLbl];
    [viewUnggah addSubview:imgUnggahFoto];
    [vwBase addSubview:viewUnggah];
    [vwBase addSubview:lblTitle];
    [vwBase addSubview:lblCurrency];
    [vwBase addSubview:txtNominal];
    [vwBase addSubview:btnDone];
    [vwBase addSubview:vwLine];
    [vwBase addSubview:lblNominalMinimum];
    
    [viewUnggah setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapUnggah =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(unggahTapped:)];
    [viewUnggah addGestureRecognizer:tapUnggah];
    
    
//    [btnDone setHidden:YES];
    
    vwBase.frame = CGRectMake(vwBase.frame.origin.x, vwBase.frame.origin.y, vwBase.frame.size.width, lblNominalMinimum.frame.origin.y + lblNominalMinimum.frame.size.height + 10);
}

- (void) gotoUnggah:(UIView*) view{
    PopUnggahObjFotoViewController *popUnggah = [self.storyboard instantiateViewControllerWithIdentifier:@"POPOBJFOTO"];
    [popUnggah setDelegate:self];
    [popUnggah setAttachedView:view];
    if(view.info){
        [popUnggah setAttachedImage:[view.info objectForKey:@"image"]];
        [popUnggah setAttachedLocation:[view.info objectForKey:@"location"]];
    }
    [self presentViewController:popUnggah animated:YES completion:nil];
}

- (void)unggahTapped:(UITapGestureRecognizer *)sender
{
    PopUnggahObjFotoViewController *popUnggah = [self.storyboard instantiateViewControllerWithIdentifier:@"POPOBJFOTO"];
    [popUnggah setDelegate:self];
    [popUnggah setAttachedView:sender.view];
    if(sender.view.info){
        [popUnggah setAttachedImage:[sender.view.info objectForKey:@"image"]];
        [popUnggah setAttachedLocation:[sender.view.info objectForKey:@"location"]];
    }
    [self presentViewController:popUnggah animated:YES completion:nil];
}

- (void) unggahFotoDone:(UIView*)view{
    for(UIView* viewChild in view.subviews){
        if ([viewChild isKindOfClass:[UILabel class]]){
            UILabel* label = (UILabel*) viewChild;
            [label setText:[NSString stringWithFormat:@"Photo_%@.jpg",[Utility dateToday:5]]];
        }
    }
}

- (void)doneWithData:(NSDictionary *)dict withView:(UIView*) view{
    NSInteger idxVwTambah = 4 + [view.accessibilityValue integerValue];
    UIView *vwTambah = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
    
    for(UIView* view in vwRb.subviews){
        if([view.accessibilityIdentifier isEqualToString:@"objFoto"]){
            view.info = dict;
            [self unggahFotoDone:view];
        }
    }
//    for(UIView* view in vwField.subviews){
//        NSLog(@"%@", view);
//        NSLog(@"%@", view.accessibilityLabel);
//        if([view isKindOfClass:[UIView class]]){
//            UIView *viewUnggah = (UILabel*) view;
//
//            if([viewUnggah.accessibilityIdentifier isEqualToString:@"objFoto"]){
//                viewUnggah.accessibilityLabel = string;
//            }
//        }
//    }
}

- (void) changeNilaiKesepakatan : (int) tag withValue:(NSString*) value {
    NSInteger idxVwTambah = 4 + tag;
     UIView *vwTambahBarang = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
     UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
     UILabel *lblNomMinimum = [vwParentRbAsset.subviews objectAtIndex:[vwParentRbAsset.subviews count]-1];
    nMaxKesepakatan = value;
     NSString *strCaption;
     if ([language isEqualToString:@"id"]) {
         strCaption = @"Maksimum Nilai Kesepakatan IDR.";
     }else{
         strCaption = @"Maximum Agreement Value IDR.";
     }
    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[value doubleValue]]]];
}

- (void) selectedRadioBtn : (NSString*) accessLbl fromSuperview : (UIView*) supervw{
    for(id view in [supervw subviews]){
        if([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*) view;
            if([btn.accessibilityLabel isEqualToString:accessLbl]){
                btn.selected = YES;
            }else{
                btn.selected = NO;
            }
        }
    }
}

-(void) actionCheckedAsset : (UIButton *) sender{
    NSInteger idxVwTambah = 4 + sender.tag;
    UIView *vwTambahBarang = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
    UIView *vwChildRbAsset = [vwParentRbAsset.subviews objectAtIndex:0];
    UILabel *lblNomMinimum = [vwParentRbAsset.subviews objectAtIndex:[vwParentRbAsset.subviews count]-1];

    NSString *strCaption;
    if ([language isEqualToString:@"id"]) {
        strCaption = @"Maksimum Nilai Kesepakatan IDR.";
    }else{
        strCaption = @"Maximum Agreement Value IDR.";
    }
    
    for (int i = 0; i<[vwChildRbAsset.subviews count]; i++) {
        NSArray *arID = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
        NSInteger xTag =  [[arID objectAtIndex:1]integerValue];
        UIButton *btn = (UIButton *) [vwChildRbAsset.subviews objectAtIndex:i];
        NSDictionary *dictAsset = [listAssetType objectAtIndex:xTag-1];
        nMaxKesepakatan = [dictAsset valueForKey:@"Value"];
        if([btn isKindOfClass:[UIButton class]]){
            if (i == 0) {
                if (btn.tag == xTag) {
                    [btn setSelected:true];
                    idxChoiceAsset = xTag;
                    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[[dictAsset valueForKey:@"Value"]doubleValue]]]];
                    
                }else{
                    [btn setSelected:false];
                }
            }
            else if (i%3 == 0) {
                if (btn.tag == xTag) {
                    [btn setSelected:true];
                   idxChoiceAsset = xTag;
                    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[[dictAsset valueForKey:@"Value"]doubleValue]]]];
                }else{
                    [btn setSelected:false];
                }
               
            }
        }
       
    }
    
    
    CGRect frmVwBase = vwTambahBarang.frame;
    CGRect frmLblNom = lblNomMinimum.frame;
    if (IPHONE_5) {
        frmLblNom.origin.x = 16;
        frmLblNom.size.height = [Utility heightLabelDynamic:lblNomMinimum sizeWidth:frmVwBase.size.width - (frmLblNom.origin.x *2) sizeFont:10];
        frmLblNom.size.width = frmVwBase.size.width - (frmLblNom.origin.x *2);

    }
     lblNomMinimum.frame = frmLblNom;
    
}

-(void) saveChoiceAsset: (UIButton *) sender{
    NSInteger idx = idxChoiceAsset;
    bool isValid = true;
    bool isDone = true;
    if (idx == 0) {
        isValid = false;
    }
    
    if (isValid) {
        NSInteger idxRb = idx -1;
        NSDictionary *dataAsset = [listAssetType objectAtIndex:idxRb];
        NSMutableDictionary *mutDataChoice = [[NSMutableDictionary alloc]init];
        [mutDataChoice setValue:[dataAsset valueForKey:@"Code"] forKey:@"code"]; //changes this code or add new code
        [mutDataChoice setValue:sender.accessibilityLabel forKey:@"newCode"];
        [mutDataChoice setValue:[dataAsset valueForKey:@"Name"] forKey:@"name"];
        
        NSInteger idxVwTambah = 4 + sender.tag;
        UIView *vwTambah = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
        UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
        
        UIView *vwField;
        for(UIView* view in vwRb.subviews){
            NSLog(@"nyari JVF%@", view);
            if([view.accessibilityLabel isEqualToString:@"field"]){
                vwField = view;
            }
            if([view.accessibilityIdentifier  isEqualToString:@"objFoto"]){
                NSLog(@"dict info >>> %@", view.info);
                [mutDataChoice setValue:[view.info objectForKey:@"image"] forKey:@"image"];
                [mutDataChoice setValue:[view.info objectForKey:@"location"] forKey:@"location"];
//                mutDataChoice setValue:view.accesibbil forKey:<#(nonnull NSString *)#>
                if([mutDataChoice valueForKey:@"image"] == nil || [[mutDataChoice valueForKey:@"image"]isEqualToString:@""]){
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"IMAGE_OBJECT_NOT_ENTERED") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self gotoUnggah:view];
                    }]];
                    if (@available(iOS 13.0, *)) {
                        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                    }
                    [self presentViewController:alert animated:YES completion:nil];
                    isDone = false;
                }
            }
            if([view isKindOfClass:[JVFloatLabeledTextField class]]){
                JVFloatLabeledTextField *textNominal = (JVFloatLabeledTextField *) view;
                NSString *strNom = [textNominal.text stringByReplacingOccurrencesOfString:@"," withString:@""];
                if ([strNom integerValue] != 0 && [nMaxKesepakatan integerValue]!= 0) {
                    if ([strNom doubleValue] > [nMaxKesepakatan doubleValue]) {
                        [mutDataChoice setValue:nMaxKesepakatan forKey:@"value"];
                    }else{
                        [mutDataChoice setValue:strNom forKey:@"value"];
                    }
                }
//                else{
//                    [Utility showMessage:@"Masukkan nilai barang" enMessage:@"Enter object value" instance:self];
//                    isDone = false;
//                }
            }
        }
//        if(isDone){
            UITextField* textField;
            for(UIView* view in vwField.subviews){
                if([view isKindOfClass:[UITextField class]]){
                    textField = (UITextField*) view;
                    NSLog(@"%@", textField.accessibilityValue);
                    NSLog(@"%@", textField.accessibilityLabel);
                    if(textField.accessibilityValue != nil){
                        [mutDataChoice setValue:textField.accessibilityValue forKey:textField.accessibilityLabel];
                    }else{
                        [mutDataChoice setValue:textField.text forKey:textField.accessibilityLabel];
                    }
                    
                    if([[mutDataChoice valueForKey:textField.accessibilityLabel]isEqualToString:@""]){
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"DATA_IS_NOT_VALID") preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                        if (@available(iOS 13.0, *)) {
                            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                        }
                        [self presentViewController:alert animated:YES completion:nil];
                        isDone = false;
                    }
//                    else{
//                        isDone = true;
//                    }
                    
                }
                
    //            if([view isKindOfClass:[UIView class]]){
    //                UIView *viewUnggah = (UILabel*) view;
    //
    //            }
            }
            if([mutDataChoice valueForKey:@"image"] == nil || [[mutDataChoice valueForKey:@"image"]isEqualToString:@""]){
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"IMAGE_OBJECT_NOT_ENTERED") preferredStyle:UIAlertControllerStyleAlert];
//                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//                [self presentViewController:alert animated:YES completion:nil];
                isDone = false;
            }
        
            if([[mutDataChoice valueForKey:@"value"]doubleValue] == 0){
                [Utility showMessage:@"Masukkan nilai barang" enMessage:@"Enter object value" instance:self];
                isDone = false;
            }
//            else{
//                isDone = true;
//            }
        
            if(vwField.subviews.count == 0){
                isDone = true;
            }
//        }
        
        
        

//        JVFloatLabeledTextField *textNominal = (JVFloatLabeledTextField *) [vwRb.subviews objectAtIndex:4];
//        NSString *strNom = [textNominal.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//        if ([strNom integerValue] != 0 && [nMaxKesepakatan integerValue]!= 0) {
//            if ([strNom doubleValue] > [nMaxKesepakatan doubleValue]) {
//                [mutDataChoice setValue:nMaxKesepakatan forKey:@"value"];
//            }else{
//                [mutDataChoice setValue:strNom forKey:@"value"];
//            }
//        }
        
        if(isDone){
            if (isEdited) {
                [arDataChoiceAsset replaceObjectAtIndex:sender.tag-1 withObject:mutDataChoice];
                NSLog(@"%@", arDataChoiceAsset);
                [self summaryTotalAmount];
                [self relocatePosition:sender.tag];
                [self rechangePostAxisY];
                isEdited = false;
            }else{
                [arDataChoiceAsset insertObject:mutDataChoice atIndex:sender.tag-1];
                NSLog(@"%@", arDataChoiceAsset);
                [self summaryTotalAmount];
                [self relocatePosition:sender.tag];
                mTag = mTag + 1;
                [self createView:mTag];
            }
        }
    }
}

-(void)createView : (int) tag{
    [lastTag addObject:@(tag)];
    NSInteger lastIdxVwTambah = 3 + tag;
    NSInteger idexLast = [self.viewContentScroll.subviews count]-2;
    
    UIView *lastVwTambah = (UIView *) [self.viewContentScroll.subviews objectAtIndex:lastIdxVwTambah];
    UIView *nextVwTambah = [[UIView alloc]initWithFrame:CGRectMake(16, lastVwTambah.frame.origin.y + lastVwTambah.frame.size.height + 16,lastVwTambah.frame.size.width , lastVwTambah.frame.size.height)];
    nextVwTambah.tag = tag;
    
    UIButton *btnExpand = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 43)];
    btnExpand.tag = tag;
    btnExpand.accessibilityLabel = [NSString stringWithFormat:@"ext_%d", tag];
    [btnExpand setTitle:@"+" forState:UIControlStateNormal];
    [btnExpand setTitle:@"x" forState:UIControlStateSelected];
    [btnExpand setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnExpand.titleLabel setFont:[UIFont systemFontOfSize:28]];
    [btnExpand addTarget:self action:@selector(actionExpandView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnEdit = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 43)];
    btnEdit.tag = tag;
    btnEdit.accessibilityLabel =[NSString stringWithFormat:@"edt_%d", tag];
    [btnEdit setImage:[UIImage imageNamed:@"baseline_create_black_36pt"] forState:UIControlStateNormal];
    btnEdit.imageEdgeInsets = UIEdgeInsetsMake(8, 4, 8, 4);
    btnEdit.tintColor =UIColorFromRGB(const_color_primary);
    [btnEdit setHidden:YES];
    [btnEdit addTarget:self action:@selector(actionEditView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lblTambahBarang = [[UILabel alloc]initWithFrame:CGRectMake(8, 11, (nextVwTambah.frame.size.width - btnExpand.frame.size.width - btnEdit.frame.size.width - 8), (nextVwTambah.frame.size.height - 22))];
    lblTambahBarang.tag = tag;
    lblTambahBarang.accessibilityLabel = [NSString stringWithFormat:@"lbl_%d", tag];
    [lblTambahBarang setTextColor:UIColorFromRGB(const_color_gray)];
    [lblTambahBarang setFont:[UIFont systemFontOfSize:14]];
    [lblTambahBarang setText:@"Object Refinancing"];


    CGRect frmNextVwTambah = nextVwTambah.frame;
    CGRect frmBtnExpand = btnExpand.frame;
    CGRect frmBtnEdit = btnEdit.frame;
    
    frmBtnExpand.origin.x = frmNextVwTambah.size.width - frmBtnExpand.size.width - 16;
    frmBtnEdit.origin.x = frmBtnExpand.origin.x - frmBtnEdit.size.width - 8;
    
    btnExpand.frame = frmBtnExpand;
    btnEdit.frame = frmBtnEdit;
    
//    if(arDataChoiceAsset.count < 3 &&
//       nTotalAmount < minTotalAmount+1){
    if(arDataChoiceAsset.count < 3){
        if(nTotalAmount < minTotalAmount){
            nextVwTambah = [self makeBorder:nextVwTambah];
            [nextVwTambah addSubview:btnExpand];
            [nextVwTambah addSubview:btnEdit];
            [nextVwTambah addSubview:lblTambahBarang];
        }
    }
    
    [self.viewContentScroll insertSubview:nextVwTambah atIndex:idexLast];
    
    UIView *vwTambah = (UIView *) [self.viewContentScroll.subviews objectAtIndex:idexLast];
    UILabel *lblNomTotal = (UILabel *) [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-2];
    UIView *vwDataCouple = (UILabel *) [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-1];

    CGRect frmContentScroll = self.viewContentScroll.frame;
    CGRect frmVwTambah = vwTambah.frame;
    CGRect frmLblNomTotal = lblNomTotal.frame;
    CGRect frmVwDataPasangan = vwDataCouple.frame;
    
    frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
    frmVwDataPasangan.size.height = 34;
    
    frmContentScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;

    self.viewContentScroll.frame = frmContentScroll;
    lblNomTotal.frame = frmLblNomTotal;
    vwDataCouple.frame = frmVwDataPasangan;
    
    [self.viewScroll setContentSize:CGSizeMake(SCREEN_WIDTH, frmContentScroll.origin.y + frmContentScroll.size.height)];
}

-(void)summaryTotalAmount{
    double mTotal = 0;
    for(NSDictionary *dictAsset in arDataChoiceAsset){
        mTotal = mTotal + [[dictAsset valueForKey:@"value"]doubleValue];
    }
    
    nTotalAmount = mTotal;
    
    [self.labelNominalTotal setText:[NSString stringWithFormat:@"IDR. %@", [Utility formatCurrencyValue:mTotal]]];
    [self.labelNominalTotal setTextColor:[UIColor blackColor]];
    [self.labelNominalTotal sizeToFit];
    self.labelNominalTotal.numberOfLines = 1;
    CGRect frmLblNominalTotal = self.labelNominalTotal.frame;
    frmLblNominalTotal.origin.x = self.viewContentScroll.frame.size.width - frmLblNominalTotal.size.width - 32;
    self.labelNominalTotal.frame = frmLblNominalTotal;
    
//    if(nTotalAmount > minTotalAmount){
//
//    }
}

-(void)relocatePosition : (NSInteger) mIdx{
    NSInteger idxVwTambah = 4 + mIdx;
    NSDictionary *data = [arDataChoiceAsset objectAtIndex:mIdx-1];
    UIView *vwTambahBarang = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
    UILabel *lblNomTotal = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-2];
    UIView *vwDataPasangan = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-1];
    UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
    
    UIButton *btnPlus = (UIButton *) [vwTambahBarang.subviews objectAtIndex:0];
    UIButton *btnEdit = (UIButton *) [vwTambahBarang.subviews objectAtIndex:1];
    [btnEdit setHidden:false];
    
    UILabel *lblBarang = (UILabel *) [vwTambahBarang.subviews objectAtIndex:2];
    [lblBarang setText:[data valueForKey:@"name"]];
    [lblBarang setTextColor:[UIColor blackColor]];
    [lblBarang sizeToFit];
    lblBarang.numberOfLines = 1;
    
    [vwParentRbAsset removeFromSuperview];
    
    CGRect frmVwTambahBarang = vwTambahBarang.frame;
    CGRect frmBtnPlus = btnPlus.frame;
    CGRect frmBtnEdit = btnEdit.frame;
    CGRect frmLblBarang = lblBarang.frame;
    CGRect frmLblNomTotal = lblNomTotal.frame;
    CGRect frmVwDataPasangan = vwDataPasangan.frame;
    
    frmVwTambahBarang.size.height = 43;
    
    frmBtnEdit.origin.x = frmBtnPlus.origin.x - frmBtnEdit.size.width - 8;
    frmBtnEdit.origin.y = frmBtnPlus.origin.y;
    
    frmLblBarang.origin.y = frmVwTambahBarang.size.height/2 - 8;
    frmLblBarang.size.width = frmVwTambahBarang.size.width - frmLblBarang.origin.x - (frmBtnPlus.size.width + frmBtnEdit.size.width) - 8;
    
    frmLblNomTotal.origin.y = frmVwTambahBarang.origin.y + frmVwTambahBarang.size.height + 16;
    
    frmVwDataPasangan.origin.y = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;
    
    btnEdit.frame = frmBtnEdit;
    lblBarang.frame = frmLblBarang;
    vwTambahBarang.frame = frmVwTambahBarang;
    lblNomTotal.frame = frmLblNomTotal;
    vwDataPasangan.frame = frmVwDataPasangan;
}

-(void) actionExpandView:(UIButton *) sender{
    isEdited = false;
    NSArray *xAccesbility = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
    NSInteger xId = [[xAccesbility objectAtIndex:1]integerValue];
    NSInteger idxVwTambah = 4 + xId;
    NSInteger idxNomTot = [self.viewContentScroll.subviews count] - 1;
    
    UIView *vwTambah = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
    UILabel *lblNomTotal = [self.viewContentScroll.subviews objectAtIndex:idxNomTot-1];
    UIView *vwDataPasangan = [self.viewContentScroll.subviews objectAtIndex:idxNomTot];
    UILabel *lblBarang = [vwTambah.subviews objectAtIndex:2];
    
    NSArray *arLastTag = (NSArray *) lastTag;
    
    if (arLastTag.count == xId) {
        NSNumber *iLastTag = [arLastTag objectAtIndex:xId-1];
        if(arDataChoiceAsset.count < 3){
            if (xId == [iLastTag integerValue]) {
                 NSLog(@"%d", [iLastTag intValue]);
                if ([sender isSelected]) {
                    [sender setSelected:false];
                    UIView *rbView = (UIView *) [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
                    if (rbView != nil) {
                        [rbView removeFromSuperview];
                        CGRect frmVwTambah = vwTambah.frame;
                        CGRect frmLblNomTotal = lblNomTotal.frame;
                        CGRect frmVwDataPasangan = vwDataPasangan.frame;
                        
                        frmVwTambah.size.height = 43;
                        frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
                        frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;

                        vwTambah.frame = frmVwTambah;
                        lblNomTotal.frame = frmLblNomTotal;
                        vwDataPasangan.frame = frmVwDataPasangan;
                    }
                }else{
                    if ([lblBarang.text isEqualToString:@"Refinancing Object"] ||
                        [lblBarang.text isEqualToString:@"Object Refinancing"] ){

                        bool isValid = true;
                        NSString *nMessage;
                        if ([self.tfContent1.text isEqualToString:@""] || self.tfContent1.text == nil || [self.tfContent1.text isEqualToString: @"0"]) {
                            isValid = false;
                            if ([language isEqualToString:@"id"]) {
                                 nMessage = @"Masukan Pengajuan terlebih dahulu";
                            }else{
                                 nMessage = @"Enter Financing Amount first";
                            }
                            [Utility showMessage:nMessage mVwController:self mTag:05];
                           
                        }
                        else if ([self.tfContent2.text isEqualToString:@""] || self.tfContent2.text == nil) {
                            isValid = false;
                            if ([language isEqualToString:@"id"]) {
                                nMessage = @"Pilih jangka waktu terlebih dahulu";
                            }else{
                                nMessage = @"Select Tenor first";
                            }
                            [Utility showMessage:nMessage mVwController:self mTag:05];
                        }
                        else if([self.tfContent3.text isEqualToString:@""] || self.tfContent3.text == nil){
                            isValid = false;
                            if ([language isEqualToString:@"id"]) {
                                nMessage = @"Pilih asuransi terlebih dahulu";
                            }else{
                                nMessage = @"Select insurance first";
                            }
                            [Utility showMessage:nMessage mVwController:self mTag:05];
                        }
                        
                        if (isValid) {
                            idxChoiceAsset = 0;
                            nominalChoiceAsset = 0;
                            
                            [sender setSelected:true];
                            [sender setTintColor:[UIColor clearColor]];
    //                        UIView *rbView = [self showVwJaminan:vwTambah];
                            UIView *rbView = [self showAset:vwTambah];
                            [vwTambah addSubview:rbView];
                            
                            CGRect frmVwContentScroll = self.viewContentScroll.frame;
                            CGRect frmVwTambah = vwTambah.frame;
                            CGRect frmVwRb = rbView.frame;
                            CGRect frmLblNomTotal = lblNomTotal.frame;
                            CGRect frmVwDataPasangan = vwDataPasangan.frame;
                            
                            frmVwTambah.size.height = frmVwRb.size.height + 43;
                            frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
                            frmLblNomTotal.origin.x = self.viewContentScroll.frame.size.width - frmLblNomTotal.size.width - 32;
                            frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
                            
    //                        frmVwContentScroll.size.height = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
                            frmVwContentScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;
                            
                            vwTambah.frame =  frmVwTambah;
                            lblNomTotal.frame = frmLblNomTotal;
                            vwDataPasangan.frame = frmVwDataPasangan;
                            self.viewContentScroll.frame = frmVwContentScroll;
                            
                            [self.viewScroll setContentSize:CGSizeMake(SCREEN_WIDTH, frmVwContentScroll.origin.y + frmVwContentScroll.size.height)];
                            
                        }else{
    //                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //                        alert.tag = 02;
    //                        [alert show];
                        }
                    }
                    
                }
                
            }
        }
        else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Maximum 3 Object" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        //make delete
        if ([sender isSelected]) {
            
            CGRect frmVwTambah = vwTambah.frame;
            if (frmVwTambah.size.height == 43) {
                if (mTag > 1) {
                    
                    UIView *lastView = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-3];
                    
                    [lastView removeFromSuperview];
                    
                    //                    [dataChoiceAsset removeObjectForKey:[NSString stringWithFormat:@"%ld",xId]];
                    [arDataChoiceAsset removeObjectAtIndex:xId-1];
                    
                    NSLog(@"%@", self.viewContentScroll.subviews);
                    
                    [lastTag removeAllObjects];
                    
                    mTag = mTag - 1;
                    
                    NSLog(@"%@", arDataChoiceAsset);
                    
                    for (int i=5; i<[self.viewContentScroll.subviews count]-2; i++) {
                        
                        //NSInteger idxView = i + 1;
                        NSInteger newTag = i - 4;
                        
                        [lastTag insertObject:@(newTag) atIndex:newTag-1];
                        
                        UIView *vwTambah = (UIView *) [self.viewContentScroll.subviews objectAtIndex:i];
                        vwTambah.tag = newTag;
                        UIButton *btnExpand =  [vwTambah.subviews objectAtIndex:0];
                        UIButton *btnEdit =  [vwTambah.subviews objectAtIndex:1];
                        UILabel *lblBarang = [vwTambah.subviews objectAtIndex:2];
                        
                        btnExpand.tag = newTag;
                        btnExpand.accessibilityLabel = [NSString stringWithFormat:@"ext_%ld", newTag];
                        
                        btnEdit.tag = newTag;
                        btnExpand.accessibilityLabel = [NSString stringWithFormat:@"edt_%ld", newTag];
                        
                        lblBarang.tag = newTag;
                        lblBarang.accessibilityLabel = [NSString stringWithFormat:@"nom_%ld", newTag];
                        NSInteger lenArDataChoice = arDataChoiceAsset.count;
                        if (newTag <= lenArDataChoice) {
                            NSDictionary *data = (NSDictionary *) [arDataChoiceAsset objectAtIndex:newTag-1];
                            [lblBarang setText:[data valueForKey:@"name"]];
                            [lblBarang setTextColor:[UIColor blackColor]];
                        }else{
                            [lblBarang setText:@"Object Refinancing"];
//                            if ([lang isEqualToString:@"id"]) {
//                                [lblBarang setText:@"Tambah Barang"];
//                            }else{
//                                [lblBarang setText:@"Add Items"];
//                            }
                            [lblBarang setTextColor:UIColorFromRGB(const_color_gray)];
                            [btnExpand setSelected:false];
                            [btnEdit setHidden:YES];
                        }
                        
                        NSLog(@"%@", vwTambah.subviews);
                        
                        
                    }
                    UIView *lstVtambah = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-3];
                    UILabel *lblNominal = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-2];
                    UIView *vwDataPasangan = [self.viewContentScroll.subviews objectAtIndex:[self.viewContentScroll.subviews count]-1];

                    CGRect frmLstVTambah = lstVtambah.frame;
                    CGRect frmLblNominal = lblNominal.frame;
                    CGRect frmVwDataPasangan = vwDataPasangan.frame;
                    
                    frmLblNominal.origin.y = frmLstVTambah.origin.y + frmLstVTambah.size.height + 16;
                    frmVwDataPasangan.origin.y = frmLblNominal.origin.y + frmLblNominal.size.height + 16;
                    
                    lblNominal.frame = frmLblNominal;
                    vwDataPasangan.frame = frmVwDataPasangan;
                    
                    [self summaryTotalAmount];
                    
                }
            }else{
                NSLog(@"%@", vwTambah.subviews);
                UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
                [vwRb removeFromSuperview];
                
                frmVwTambah.size.height = 43;
                vwTambah.frame = frmVwTambah;
                
                [self rechangePostAxisY];
                [sender setSelected:true];
            }
            
        }
    }
}

-(void)actionEditView:(UIButton *) sender{
    isEdited = true;
    NSArray *xAccesbility = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
    NSInteger xId = [[xAccesbility objectAtIndex:1]integerValue];
    NSInteger idxVwTambah = 4 + xId;
    editCurrentTag = idxVwTambah;
    UIView *vwTambah = [self.viewContentScroll.subviews objectAtIndex:idxVwTambah];
//    UIView *vwAddonAsset = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
    
    if (vwTambah.frame.size.height == 43) {
        UIView *rbView = [self showAset:vwTambah];
        [vwTambah addSubview:rbView];
        
        UIView *vwRbChoice = [rbView.subviews objectAtIndex:0];
        NSDictionary *data = [arDataChoiceAsset objectAtIndex:xId-1];
//        NSInteger mCode = [[data valueForKey:@"code"]integerValue];
        NSInteger mCode = [[data valueForKey:@"newCode"]integerValue];
        UIButton *selectedBtn = [self getSelectedBtn:[NSString stringWithFormat:@"b_%ld",(long)mCode] fromSuperview:vwRbChoice];
        if(selectedBtn != nil){
            [self actionRadioBtn:selectedBtn];
        }
    
        UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
        UIView *vwField;
        for(UIView* view in vwRb.subviews){
            if([view.accessibilityLabel isEqualToString:@"field"]){
                vwField = view;
            }
            if([view.accessibilityIdentifier  isEqualToString:@"objFoto"]){
                view.info = @{ @"image" : [data objectForKey:@"image"],
                               @"location" : [data objectForKey:@"location"]};
                [self unggahFotoDone:view];
            }
            
            if([view isKindOfClass:[JVFloatLabeledTextField class]]){
                JVFloatLabeledTextField *txtNominal = (JVFloatLabeledTextField*) view;
                [txtNominal setText:[Utility formatCurrencyValue:[[data valueForKey:@"value"]doubleValue]]];
            }
            
            //buttonDone
            if([view isKindOfClass:[UIButton class]]){
                [view setHidden:NO];
            }
        }
        UITextField* textField;
        for(UIView* view in vwField.subviews){
            if([view isKindOfClass:[UITextField class]]){
                textField = (UITextField*) view;
                
                textField.accessibilityValue = [data objectForKey:textField.accessibilityLabel];
                textField.text = [data objectForKey:textField.accessibilityLabel];
        
            }
        }
        
        NSLog(@"%@", rbView.subviews);
//        JVFloatLabeledTextField *txtNominal = [rbView.subviews objectAtIndex:4];
//        if ([txtNominal isKindOfClass:[JVFloatLabeledTextField class]]) {
//            [txtNominal setText:[Utility formatCurrencyValue:[[data valueForKey:@"value"]doubleValue]]];
//        }
        
//        UIButton *btnDone = [rbView.subviews objectAtIndex:[vwAddonAsset.subviews count]-2];
//        [btnDone setHidden:NO];
//        UIButton *btnSubmit = [rbView.subviews objectAtIndex:4];
//        if ([btnSubmit isKindOfClass:[UIButton class]]) {
//            [btnSubmit setHidden:false];
//        }
        
        CGRect frmVwTambah = vwTambah.frame;
        CGRect frmRbView = rbView.frame;
        
        frmVwTambah.size.height = frmRbView.size.height + 79;
        
        vwTambah.frame = frmVwTambah;
        
        UIView *rbVw = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
        
        NSLog(@"%@", rbVw.subviews);
        
        [self rechangePostAxisY:(int)idxVwTambah+1];
    }
}

-(void) rechangePostAxisY : (int) idSelected{
    UILabel *lblMinNom = [self.viewContentScroll.subviews objectAtIndex:4];
    NSLog(@"rechange %@", lblMinNom.text);
    CGFloat lblMinNomPosY = lblMinNom.frame.origin.y + lblMinNom.frame.size.height + 16;
    CGFloat lastHight = 0;
    for (int x = 5; x < [self.viewContentScroll.subviews count]-1; x++) {
        UIView *vwTambah = [self.viewContentScroll.subviews objectAtIndex:x];
        CGRect frmVwTambah = vwTambah.frame;
        if (x > 5) {
            if (x == idSelected) {
//                lblMinNomPosY = lblMinNomPosY + 43 + 24;
                lblMinNomPosY = lblMinNomPosY + lastHight + 24;
            }else{
                lblMinNomPosY = lblMinNomPosY + 43 + 16;
            }
        }
        frmVwTambah.origin.y = lblMinNomPosY;
        lastHight = frmVwTambah.size.height;
        vwTambah.frame = frmVwTambah;
        
    }
    CGRect frmLblNomTotal = self.labelNominalTotal.frame;
    CGRect frmVwDataPasangan = self.viewDataCouple.frame;
    CGRect frmVwContenScroll = self.viewContentScroll.frame;
    
    frmLblNomTotal.origin.y = lblMinNomPosY + lastHight + 16;
    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height;
    frmVwContenScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height;
    
    self.labelNominalTotal.frame = frmLblNomTotal;
    self.viewDataCouple.frame = frmVwDataPasangan;
    self.viewContentScroll.frame = frmVwContenScroll;
    
    [self.viewScroll setContentSize:CGSizeMake(SCREEN_WIDTH, self.viewContentScroll.frame.size.height)];
    
}

- (UIView *) showAset : (UIView *) parentView{
    UIView *vwBase;
    vwBase = [[UIView alloc]initWithFrame:CGRectMake(8, 43, parentView.frame.size.width - 16, 0)];
    vwBase.tag = parentView.tag;
    vwBase.accessibilityLabel = @"itsVwBase";
    CGFloat lenWidth = vwBase.frame.size.width/3;
    
    UIView *vwRadio = [[UIView alloc]init];
    [vwRadio setAccessibilityLabel:@"radio"];
    
    CGFloat radioRectSize = 24;
    CGFloat lastY = 0;
    
    int i = 0, j = 0, idx = 1;
    for(i = 0; i < listAssetType.count/3; i++){
        CGFloat lastX = 0;
        CGFloat lastYinX = 0;
        for(j = 0; j < 3; j++){
            UIButton *radioBtn = [[UIButton alloc]initWithFrame:CGRectMake(lastX, lastY, radioRectSize, radioRectSize)];
            
            UIImage *unselectImage = [[UIImage imageNamed:@"radio_unselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIImage *selectedImage = [[UIImage imageNamed:@"radio_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            
            [radioBtn setImage:unselectImage forState:UIControlStateNormal];
            [radioBtn setImage:selectedImage forState:UIControlStateSelected];
            [radioBtn setTintColor:UIColorFromRGB(const_color_primary)];
            [radioBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
            [radioBtn setTag:parentView.tag];
            [radioBtn setAccessibilityLabel:[NSString stringWithFormat:@"b_%d",idx]];
            [radioBtn addTarget:self action:@selector(actionRadioBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(lastX + radioBtn.frame.size.width + 4, lastY, lenWidth - radioRectSize, radioRectSize*2)];
            [label setTag:parentView.tag];
            [label setText:[[listAssetType objectAtIndex:idx-1]valueForKey:@"Name"]];
            [label setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
            [label setTextAlignment:NSTextAlignmentLeft];
            [label setLineBreakMode:NSLineBreakByWordWrapping];
            [label setNumberOfLines:2];
                        
            [vwRadio addSubview:radioBtn];
            [vwRadio addSubview:label];
            
            lastX = label.frame.origin.x + label.frame.size.width;
            lastYinX = label.frame.origin.y + label.frame.size.height;
                        
            idx = idx + 1;
        }
        lastY = lastYinX;
    }

    CGFloat lastX = 0;
    CGFloat lastYinX = 0;
    for(j = 0; j < fmodl(listAssetType.count, 3); j++){
        UIButton *radioBtn = [[UIButton alloc]initWithFrame:CGRectMake(lastX, lastY, radioRectSize, radioRectSize)];
        
        [radioBtn setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
        [radioBtn setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateSelected];
        [radioBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [radioBtn setAccessibilityLabel:[NSString stringWithFormat:@"b_%d",idx]];
        [radioBtn setTag:parentView.tag]; //or code ?
        [radioBtn addTarget:self action:@selector(actionRadioBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(lastX + radioBtn.frame.size.width + 4, lastY, lenWidth - radioRectSize, radioRectSize*2)];
        [label setTag:parentView.tag];
        [label setText:[[listAssetType objectAtIndex:idx-1]valueForKey:@"Name"]];
        [label setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
        [label setLineBreakMode:NSLineBreakByWordWrapping];
        [label setNumberOfLines:2];
                    
        [vwRadio addSubview:radioBtn];
        [vwRadio addSubview:label];
                
        lastX = label.frame.origin.x + label.frame.size.width;
        lastYinX = label.frame.origin.y + label.frame.size.height;
        idx = idx + 1;
    }
    if(lastYinX != 0){
        lastY = lastYinX;
    }

    vwRadio.frame = CGRectMake(0, 0,vwBase.frame.size.width,  lastY +10);
    [vwBase addSubview:vwRadio];
        
    vwBase.frame = CGRectMake(vwBase.frame.origin.x, vwBase.frame.origin.y, vwBase.frame.size.width, vwRadio.frame.size.height + vwRadio.frame.origin.y);
    
    return vwBase;
}


- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    [Utility stopTimer];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        if ([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]) {
             if (![requestType isEqualToString:@"check_notif"]) {
                 NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                 if (dataDict) {
                     NSLog(@"%@", dataDict);
                     validateAsset = [NSJSONSerialization JSONObjectWithData:[[dataDict objectForKey:@"Validate_Asset"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                     listAssetType = [dataDict objectForKey:@"ListAssetType"];
                     listInsurence = [dataDict objectForKey:@"ListInsurance"];
                     listPeriod = [dataDict objectForKey:@"ListLoanPeriod"];
                     
                     int mTahun = [[dataDict objectForKey:@"MaxTenor"]intValue]/12;
                     NSString *nMaxLoan = [NSString stringWithFormat:@"%@", [dataDict objectForKey:@"MaxLoan"]];
                     nMaxDana = [[dataDict objectForKey:@"MaxLoan"]doubleValue];
                     minLoan = [[dataDict objectForKey:@"MinLoan"]doubleValue];
                     nRateTasaksi = [[dataDict objectForKey:@"Rate_Taksasi"]doubleValue];
                     nMaritalState = [[dataDict objectForKey:@"Marital_Status_Code"]doubleValue];
                     
                     NSLog(@"double Value : %f", [[dataDict objectForKey:@"MaxLoan"]doubleValue]);
                     if ([language isEqualToString:@"id"]) {
                         self.tfContent1.placeholder = [NSString stringWithFormat:@"Minimum %@ dan Maksimal %@", [Utility formatCurrencyValue:[[dataDict objectForKey:@"MinLoan"]doubleValue]], [Utility formatCurrencyValue:[[dataDict objectForKey:@"MaxLoan"]doubleValue]]];
                         self.tfContent2.placeholder = [NSString stringWithFormat:@"Maksimal %d Tahun (%@ bulan)", mTahun,[dataDict objectForKey:@"MaxTenor"]];
//                         self.tfContent2.placeholder = @"";
                         
                     }else{
                         self.tfContent1.placeholder = [NSString stringWithFormat:@"Minimum %@ and Maximum %@", [Utility formatCurrencyValue:[[dataDict objectForKey:@"MinLoan"]doubleValue]], [Utility formatCurrencyValue:[[dataDict objectForKey:@"MaxLoan"]doubleValue]]];
                         self.tfContent2.placeholder = [NSString stringWithFormat:@"Maximum %d Year (%@ months)", mTahun,[dataDict objectForKey:@"MaxTenor"]];
//                         self.tfContent2.placeholder = @"";
                     }
                     
                     currencyFormatter.maximumSignificantDigits = nMaxLoan.length;
                     [dataManager.dataExtra setValue:[dataDict valueForKey:@"ReferenceCode"] forKey:@"reference_code"];
                     [self martialStateHandler];
                 }
             }
        }else{
            [Utility showMessage:[jsonObject valueForKey:@"response"] mVwController:self mTag:[[jsonObject valueForKey:@"response_code"]intValue]];
        }
    }
}



- (void)errorLoadData:(NSError *)error{
    
}

- (void)reloadApp{
    [self backToRoot];
}

-(void)showPopupPasangan : (UIButton *) sender{
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"POPCOUPCR"];
    PopupCoupleCRViewController *viewCont = (PopupCoupleCRViewController *) templateView;
    [viewCont setDelegate:self];
    [viewCont setMartialState:nMaritalState];
    [self presentViewController:templateView animated:YES completion:nil];
}

- (void) doneState:(BOOL)state{
    maritalStateFilled = state;
}

- (void)namaPasangan:(NSString *)string{
    textDataPasangan = string;
    [labelIsiDataPasangan setText:string];
}

- (void)textFieldDidEndTanahValidation:(UITextField *)textField{
    if([textField.accessibilityLabel isEqualToString:@"Surface_Area"]){
        NSDictionary *minData;
        NSDictionary *maxData;
        for(NSDictionary *validationAsset in validateAsset){
            if([[validationAsset objectForKey:@"param_name"] isEqualToString:textField.accessibilityLabel]){
                minData = [validationAsset objectForKey:@"min"];
                maxData = [validationAsset objectForKey:@"max"];
                break;
            }
        }
        
        NSString *stringMsg = @"";
        if([[maxData valueForKey:@"value"]doubleValue] != 0){
            if([textField.text doubleValue] > [[maxData valueForKey:@"value"]doubleValue]){
                if([Utility isLanguageID]){
                    stringMsg = [[maxData objectForKey:@"msg"]valueForKey:@"in"];
                }else{
                    stringMsg = [[maxData objectForKey:@"msg"]valueForKey:@"en"];
                }
            }
        }

        if([textField.text doubleValue] < [[minData valueForKey:@"value"]doubleValue]){
            if([Utility isLanguageID]){
                stringMsg = [[minData objectForKey:@"msg"]valueForKey:@"in"];
            }else{
                stringMsg = [[minData objectForKey:@"msg"]valueForKey:@"en"];
            }
        }
        
        if(![stringMsg isEqualToString:@""]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:stringMsg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
            textField.text = @"";
        }
    }
}

- (void)textFieldPurchaseYearValidation:(UITextField *)textField{
    
    if([textField.accessibilityLabel isEqualToString:@"Production_Year"] || [textField.accessibilityLabel isEqualToString:@"Purchase_Year"]){
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components

        NSString *stringMsg = @"";
        if(textField.text.length != 4){
            stringMsg = lang(@"DATA_IS_NOT_VALID");
        }
        else if([textField.text doubleValue] > components.year){
            stringMsg = lang(@"DATA_IS_NOT_VALID");
        }
        else if([textField.text doubleValue]+8 <= components.year){
            if([lang(@"LANGUAGE") isEqualToString:@"ID"]){
                stringMsg = [NSString stringWithFormat:@"Tahun harus dibawah 8 Tahun"];
            }else{
                stringMsg = [NSString stringWithFormat:@"Year must below 8 years"];
            }
        }
        if(![stringMsg isEqualToString:@""]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:stringMsg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
            textField.text = @"";
        }
    }
}

- (void)actionCancel:(id)sender{
//    dataManager.currentPosition --;
//    UINavigationController *navigationCont = self.navigationController;
//    [navigationCont popViewControllerAnimated:YES];
    [self backToR];
}

- (BOOL) checkValidation{
    bool isValid = true;
    NSString *strNeedDana = [self.tfContent1.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    if([arDataChoiceAsset count] == 0){
        isValid = false;
        if ([language isEqualToString:@"id"]) {
            [Utility showMessage:@"Pilih object/asset terlebih dahulu" mVwController:self mTag:05];
        }else{
            [Utility showMessage:@"Select object/asset first" mVwController:self mTag:05];
        }
    }else{
        double needDana = [strNeedDana doubleValue];
        if (needDana == 0 || [strNeedDana isEqualToString:@""] || strNeedDana == nil ) {
            if ([language isEqualToString:@"id"]) {
                [Utility showMessage:@"Masukan Pengajuan terlebih dahulu" mVwController:self mTag:05];
            }else{
                [Utility showMessage:@"Enter Financing amount first" mVwController:self mTag:05];
            }

        }else{
            if (needDana > nMaxDana ) {
                isValid  = false;
                if ([language isEqualToString:@"id"]) {
                    [Utility showMessage:@"Nominal Pengajuan melebihi batas yang di tawarkan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Financing Ammount Exceeds the limit offered" mVwController:self mTag:05];
                }
            }else if(needDana < minLoan){
                isValid  = false;
                if ([language isEqualToString:@"id"]) {
                    [Utility showMessage:@"Nominal Pengajuan yang tidak boleh kurang dari minimum loan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Financing Amount should not be less than the loan minimum" mVwController:self mTag:05];
                }
            }
            else{
                if (nTotalAmount < needDana + 1) {
                    isValid = false;
                    if ([language isEqualToString:@"id"]) {
                        [Utility showMessage:@"Mohon Maaf, nilai object/asset yang anda masukan kurang dari akseptasi Bank. Silahkan masukkan tambahan object/asset" mVwController:self mTag:05];
                    }else{
                        [Utility showMessage:@"Sorry, the value of the object you entered is less than the Bank's acceptance. Please enter additional assets" mVwController:self mTag:05];
                    }
                }
            }
        }

        if(nMaritalState != 0 && !maritalStateFilled){
            isValid = false;
            if(nMaritalState == 1){
                if ([language isEqualToString:@"id"]) {
                    [Utility showMessage:@"Anda belum mengisi data pasangan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Please Fill Spouse Data" mVwController:self mTag:05];
                }
            }else{
                if ([language isEqualToString:@"id"]) {
                    [Utility showMessage:@"Anda belum mengisi data tanggungan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Please Fill Dependent Data" mVwController:self mTag:05];
                }
            }
        }
        
        if([self.tfContent4.text isEqualToString:@""]){
            isValid = false;
            if ([language isEqualToString:@"id"]) {
                [Utility showMessage:@"Anda belum mengisi alamat email" mVwController:self mTag:05];
            }else{
                [Utility showMessage:@"Email is required" mVwController:self mTag:05];
            }
        }else if(![Utility validateEmailWithString:self.tfContent4.text]){
            isValid = false;
            if ([language isEqualToString:@"id"]) {
                [Utility showMessage:@"Email Anda Tidak Valid" mVwController:self mTag:05];
            }else{
                [Utility showMessage:@"Your Email is not valid" mVwController:self mTag:05];
            }
        }
    }
    return isValid;
}

- (void)actionNext:(id)sender{
    if ([self checkValidation]) {
        NSString *strNeedDana = [self.tfContent1.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        [dataManager.dataExtra setValue:strNeedDana forKey:@"loan_amount"];
        [dataManager.dataExtra setValue:[[listPeriod objectAtIndex:nIdxTenor]valueForKey:@"Code"]forKey:@"loan_tenor"];
        [dataManager.dataExtra setValue:[[listInsurence objectAtIndex:nIdxAsuransi]valueForKey:@"Code"] forKey:@"insurance_code"];
        [dataManager.dataExtra setValue:[[listInsurence objectAtIndex:nIdxAsuransi]valueForKey:@"Name"] forKey:@"insurance_name"];
        [dataManager.dataExtra setValue:[Utility convertJsonString:arDataChoiceAsset] forKey:@"asset_type"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%d", (int)nTotalAmount] forKey:@"asset_amount"];
        [dataManager.dataExtra setValue:self.tfContent4.text forKey:@"email"];
        
        [Utility resetTimer];
        
        [self openNextTemplate];
    }
}

- (void) actionAgreeAkad : (UIButton*)sender{
    if([self checkValidation]){
        [self.buttonChecked setSelected:!self.buttonChecked.isSelected];
        if ([self.buttonChecked isSelected]) {
            [self.buttonNext setEnabled:true];
        }else{
            [self.buttonNext setEnabled:false];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 05) {
        NSLog(@"Field Empty");
    }else if(alertView.tag == 06){
        [self.tfContent1 setText:@"0"];
    }
    else if (alertView.tag == 07){
        stateFocus = false;
        [self.tfContent1 becomeFirstResponder];
    }
    else{
        [self backToRoot];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [Utility resetTimer];
}
@end
