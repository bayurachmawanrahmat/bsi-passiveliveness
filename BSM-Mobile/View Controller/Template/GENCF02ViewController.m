//
//  GENCF02ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 08/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "GENCF02ViewController.h"
#import "Utility.h"
#import "InboxHelper.h"
#import "LGPlusButtonsView.h"
#import "AESCipher.h"
#import "RecentlyHelper.h"
#import "UIAlertController+AlertExtension.h"
#import "NDHTMLtoPDF.h"

@interface GENCF02ViewController ()<ConnectionDelegate, UIAlertViewDelegate, UIScrollViewDelegate, NDHTMLtoPDFDelegate, UIDocumentInteractionControllerDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    
    BOOL fromRecipt;
    
    CGRect scrBounds;
    CGSize scrSizes;
    CGFloat scrWidths;
    CGFloat scrHeights;
    
    UIButton *btnEtcs;
    UIButton *btnShrs;
    UIButton *btnFavs;
    UILabel *lblEtcs;
    UILabel *lblShrs;
    UILabel *lblFavs;
    NSString *isEtcs;
    NSString *nmEtcs;
    NSString *nmShrs;
    NSString *nmFavs;
    NSString *infaqStatus;
    NSString *goldStatus;
    
    UIView *vwBgCaption;
    UIView *vwContinerCaption;
    UIView *vwLineTitleCaption;
    UIView *vwLineTextCaption;
    UILabel *lblTitleCaption;
    UILabel *lblTextCaption;
    UITextField *txtFieldCaption;
//    UIButton *btnSaveCaption;
//    UIButton *btnCancelCaption;
    CustomBtn *btnSaveCaption;
    CustomBtn *btnCancelCaption;
    NSString *defTitleReceipt;
    UIScrollView *vwPopupScroll;
    
    UIToolbar* keyboardDoneButtonView;
    
    NSDictionary *dataObject;
    
    NSString *idvc;
    
    BOOL frmTrx;
    NSString *fromAkad;
	
    NSString *mid;
    RecentlyHelper *mHelper;
    BOOL favIsActive;
}

@property (nonatomic) AESCipher *aesChiper;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *vwContentScroll;
@property (weak, nonatomic) IBOutlet UIView *vwButton;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgContentHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgContentFooter;
@property (weak, nonatomic) IBOutlet UIImageView *imgReceiptNormal;

@property (weak, nonatomic) IBOutlet UIView *vwResiLogo;
@property (weak, nonatomic) IBOutlet UIView *vwTextContent;
@property (weak, nonatomic) IBOutlet UIImageView *vimgWatermark;
@property (weak, nonatomic) IBOutlet UILabel *lblContentHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblContentFooter;

@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOkShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (strong, nonatomic) IBOutlet UIView *bgView;


- (IBAction)actionMore:(id)sender;
- (IBAction)actionShare:(id)sender;
- (IBAction)actionOkShare:(id)sender;
- (IBAction)actionOk:(id)sender;

@property (strong, nonatomic) LGPlusButtonsView *fbMenuAction;
@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;

@end

@implementation GENCF02ViewController

NSString *menuIDPaylater = @"00181";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.aesChiper = [[AESCipher alloc] init];
    mHelper = [[RecentlyHelper alloc]init];
    
    _lblContentHeader.text = @"";
    [_lblContentHeader setFont:[UIFont fontWithName:const_font_name3 size:14]];
    _lblContent.text = @"";
    [_lblContent setFont:[UIFont fontWithName:const_font_name1 size:14]];
    _lblContentFooter.text = @"";
    [_lblContentFooter setFont:[UIFont fontWithName:const_font_name1 size:13]];
    
    _vwContentScroll.delegate = self;
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgToolbar;
    if ([lang isEqualToString:@"id"]) {
        [_btnShare setTitle:@"Bagikan" forState:UIControlStateNormal];
        msgToolbar = @"Selesai";
    }else{
        [_btnShare setTitle:@"Share" forState:UIControlStateNormal];
        msgToolbar = @"Done";
    }
    [_btnShare setImage:[UIImage imageNamed:@"ic_bt_share_prm"] forState:UIControlStateNormal];
    _btnShare.layer.borderColor = [UIColorFromRGB(const_color_darkprimary)CGColor];
    _btnShare.backgroundColor = UIColorFromRGB(const_color_basecolor);
    _btnShare.layer.borderWidth = 2;
    [_btnShare setTitleColor:UIColorFromRGB(const_color_primary) forState:UIControlStateNormal];
    [_btnShare setTintColor:UIColorFromRGB(const_color_primary)];
    
    if(fromRecipt){
        [_btnMore setHidden:YES];
        [_btnOk setHidden:YES];
        [_btnShare setHidden:NO];
        [_btnOkShare setHidden:NO];
        
        
    }else{
        [_btnMore setHidden:YES];
        [_btnOk setHidden:NO];
        [_btnShare setHidden:YES];
        [_btnOkShare setHidden:YES];
    }
    
    _btnOk.layer.borderWidth = 2;
    _btnOk.layer.backgroundColor = [UIColorFromRGB(0xF8AD3C)CGColor];
    _btnOk.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
    
    
    scrBounds = [[UIScreen mainScreen] bounds];
    scrSizes = scrBounds.size;
    scrWidths = scrSizes.width;
    scrHeights = scrSizes.height;
    isEtcs = @"0";
    
    idvc = [dataManager.dataExtra objectForKey:@"idvc"];
    NSDictionary *dictZakat;
    NSString *urlZakat = @"";
    NSString *urlPost = @"";
    
    favIsActive = false;
    
    
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if (idvc == nil) {
        dictZakat = nil;
        urlZakat = @"";
        urlPost = @"";
        [userDefault removeObjectForKey:@"infaqData"];
    }
    else {
        dictZakat = [userDefault objectForKey:@"infaqData"];
        urlZakat = [dictZakat objectForKey:@"url_parm"];
        urlPost = [dictZakat objectForKey:@"url_post"];
        [userDefault removeObjectForKey:@"infaqData"];
    }
    
    [self setupLayout];

    if(!fromRecipt){
        if(![idvc isEqualToString:@"LD01"]){
//            [self addFBShared];
//            [self addViewFloating];
            
            if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:menuIDPaylater]){
                [self addedButtonUnduh];
            }
            
            [self newFloating];
            favIsActive =[[self.jsonData valueForKey:@"favorite"]boolValue];
            NSLog(@"%@", self.bgView.subviews);
            if (!favIsActive) {
                UIButton *btnShare = [self.bgView.subviews objectAtIndex:self.bgView.subviews.count-1];
                UIButton *btnFav = [self.bgView.subviews objectAtIndex:self.bgView.subviews.count-2];
                    CGRect frmBtnShare = btnShare.frame;
                    frmBtnShare.origin.x = btnFav.frame.origin.x;
                    frmBtnShare.size.width = SCREEN_WIDTH - (btnFav.frame.origin.x*2);
                    btnShare.frame  = frmBtnShare;
            }
            
            
        }
    }
    
    if(!fromRecipt){
        if(![idvc isEqualToString:@"LD01"]){
//            [self addFBShared];
//            [self addViewFloating];
            
            if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:menuIDPaylater]){
                [self addedButtonUnduh];
            }
            
            [self newFloating];
            favIsActive =[[self.jsonData valueForKey:@"favorite"]boolValue];
            NSLog(@"%@", self.bgView.subviews);
            if (!favIsActive) {
                UIButton *btnShare = [self.bgView.subviews objectAtIndex:self.bgView.subviews.count-1];
                UIButton *btnFav = [self.bgView.subviews objectAtIndex:self.bgView.subviews.count-2];
                    CGRect frmBtnShare = btnShare.frame;
                    frmBtnShare.origin.x = btnFav.frame.origin.x;
                    frmBtnShare.size.width = SCREEN_WIDTH - (btnFav.frame.origin.x*2);
                    btnShare.frame  = frmBtnShare;
            }
            
            
        }
    }
    
    if(self.jsonData){
        frmTrx = true;
        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if(needRequest){
            [_vwContent setHidden:YES];
            NSString *url = @"";
            mid = [self.jsonData valueForKey:@"menu_id"];
            
            if ([urlZakat isEqualToString:@""]|| urlZakat == nil) {
                
                if([mid isEqualToString:@"00020"]){
                    url = [NSString stringWithFormat:@"%@,amount,transaction_id,zakat",[self.jsonData valueForKey:@"url_parm"]];
                }else if([mid isEqualToString:@"00041"]){
                    url = [NSString stringWithFormat:@"%@,billerid,billertype,billername,billkey1,billkey1name,bclist,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                }else if([mid isEqualToString:@"00025"]){
                    url = [NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                }else if([mid isEqualToString:@"00060"]){
                    url = [NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                }else if([mid isEqualToString:@"00065"]){
                    url = [NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                }else if([mid isEqualToString:@"00047"]){
                    url = [NSString stringWithFormat:@"%@,is_qrdomestic,code=00101",[self.jsonData valueForKey:@"url_parm"]];
                }
                else{
                    NSString *aditionalInfo = [userDefault objectForKey:@"aditionalInfo"];
                    if (aditionalInfo && ![aditionalInfo  isEqual: @""]) {
                        url = [NSString stringWithFormat:@"%@,%@", [self.jsonData valueForKey:@"url_parm"],aditionalInfo];
                        [userDefault removeObjectForKey:@"aditionalInfo"];
                        [userDefault synchronize];
                    }else{
                        url = [self.jsonData valueForKey:@"url_parm"];
                    }
                }
                
            }else{
                url = urlPost;
                mid = @"00021";
            }
            
            NSRange rangeValue = [url rangeOfString:@"pay_infaq=0" options:NSCaseInsensitiveSearch];
            if (rangeValue.length > 0) {
                infaqStatus = @"0";
            }
            else {
                infaqStatus = @"1";
            }
            
            if([mid isEqualToString:@"00110"] ||
               [mid isEqualToString:@"00111"] ||
               [mid isEqualToString:@"00112"] ||
               [mid isEqualToString:@"00113"] ||
               [mid isEqualToString:@"00115"] ||
               [mid isEqualToString:@"00116"] ||
               [[dictZakat objectForKey:@"gold_status"]isEqualToString:@"1"]){
                goldStatus = @"1";
            }else{
                goldStatus = @"0";
            }
            
            [dataManager setMenuId:mid];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:fav];
            
        }
    }else if (dataObject) {
        if(fromAkad || frmTrx){
            frmTrx = true;
            infaqStatus = [dataObject objectForKey:@"pay_infaq"];
            if([dataObject objectForKey:@"title"]) {
                [self.lblTitle setText:[dataObject objectForKey:@"title"]];
            } else {
                [self.lblTitle setText:[dataObject objectForKey:@"title_menu"]];
            }
        } else {
            frmTrx = false;
            if ([lang isEqualToString:@"id"]) {
                [self.lblTitle setText:@"Kotak Masuk"];
            }else{
                [self.lblTitle setText:@"Inbox"];
            }
        }
            
        _lblContentHeader.text = [[dataObject objectForKey:@"title"]stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
        _lblContent.text = [[dataObject objectForKey:@"msg"]stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
        _lblContentFooter.text = [[dataObject objectForKey:@"footer_msg"]stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
        [self setupLayout];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeIcon" object:self];
    }
    
    keyboardDoneButtonView = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:msgToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    [self registerForKeyboardNotifications];
    
    
}

-(void)addFBShared{
    
    NSInteger count = 0;
    NSMutableArray *arrBtnTitle = [[NSMutableArray alloc]init];
    NSMutableArray *arrDescTitle = [[NSMutableArray alloc]init];
    if ([[self.jsonData objectForKey:@"favorite"]boolValue]) {
        count = 3;
    }else{
        count = 2;
    }
    
    for (int i = 1; i<=count; i++) {
        [arrBtnTitle addObject:@""];
    }
    
    if ([lang isEqualToString:@"id"]) {
        [arrDescTitle addObject:@""];
        [arrDescTitle addObject:@"Bagikan melalui aplikasi lain"];
        if (count == 3) {
           [arrDescTitle addObject: @"Jadikan Favorit"];
        }
    }else{
        [arrDescTitle addObject:@""];
        [arrDescTitle addObject:@"Share via other applications"];
        if (count == 3) {
            [arrDescTitle addObject: @"Make Favorites"];
        }
    }
    
    
    _fbMenuAction = [LGPlusButtonsView plusButtonsViewWithNumberOfButtons:count
                                                  firstButtonIsPlusButton:YES
                                                            showAfterInit:YES
                                                            actionHandler:^(LGPlusButtonsView *plusButtonView, NSString *title, NSString *description, NSUInteger index)
                     {
                         [self actionFBMenu:index];
                         
                     }];
    
    
    _fbMenuAction.observedScrollView = self.vwContentScroll;
    _fbMenuAction.coverColor = [UIColor clearColor];
    _fbMenuAction.position = LGPlusButtonsViewPositionBottomRight;
    _fbMenuAction.plusButtonAnimationType = LGPlusButtonAnimationTypeRotate;
    
    [_fbMenuAction setButtonsTitles:arrBtnTitle forState:UIControlStateNormal];
    
    [_fbMenuAction setDescriptionsTexts:arrDescTitle];
    
    if (count == 3) {
        [_fbMenuAction setButtonsImages:@[[UIImage imageNamed:@"icons8-menu-vertical-24.png"], [UIImage imageNamed:@"icons8-share-24.png"], [UIImage imageNamed:@"icons8-heart-outline-24.png"]]
                               forState:UIControlStateNormal
                         forOrientation:LGPlusButtonsViewOrientationAll];
    }else{
        [_fbMenuAction setButtonsImages:@[[UIImage imageNamed:@"icons8-menu-vertical-24.png"], [UIImage imageNamed:@"icons8-share-24.png"]]
                               forState:UIControlStateNormal
                         forOrientation:LGPlusButtonsViewOrientationAll];
    }
   
    
    [_fbMenuAction setButtonsAdjustsImageWhenHighlighted:NO];
    [_fbMenuAction setButtonsBackgroundColor:UIColorFromRGB(0xFFFFC90D) forState:UIControlStateNormal];
    [_fbMenuAction setButtonsBackgroundColor:UIColorFromRGB(0xFFFFF176) forState:UIControlStateHighlighted];
    [_fbMenuAction setButtonsBackgroundColor:UIColorFromRGB(0xFFFFF176) forState:UIControlStateHighlighted|UIControlStateSelected];
    
    [_fbMenuAction setButtonsSize:CGSizeMake(44.f, 44.f) forOrientation:LGPlusButtonsViewOrientationAll];
    [_fbMenuAction setButtonsLayerCornerRadius:44.f/2.f forOrientation:LGPlusButtonsViewOrientationAll];
    [_fbMenuAction setButtonsTitleFont:[UIFont boldSystemFontOfSize:24.f] forOrientation:LGPlusButtonsViewOrientationAll];
    [_fbMenuAction setButtonsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
    [_fbMenuAction setButtonsLayerShadowOpacity:0.5];
    [_fbMenuAction setButtonsLayerShadowRadius:3.f];
    [_fbMenuAction setButtonsLayerShadowOffset:CGSizeMake(0.f, 2.f)];
    [_fbMenuAction setButtonAtIndex:0 size:CGSizeMake(44.f, 44.f)
                            forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [_fbMenuAction setButtonAtIndex:0 layerCornerRadius:44.f/2.f
                            forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [_fbMenuAction setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:40.f]
                            forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [_fbMenuAction setButtonAtIndex:0 titleOffset:CGPointMake(0.f, -3.f) forOrientation:LGPlusButtonsViewOrientationAll];
    
    
    [_fbMenuAction setDescriptionsBackgroundColor:[UIColor whiteColor]];
    [_fbMenuAction setDescriptionsTextColor:[UIColor blackColor]];
    [_fbMenuAction setDescriptionsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
    [_fbMenuAction setDescriptionsLayerShadowOpacity:0.25];
    [_fbMenuAction setDescriptionsLayerShadowRadius:1.f];
    [_fbMenuAction setDescriptionsLayerShadowOffset:CGSizeMake(0.f, 1.f)];
    [_fbMenuAction setDescriptionsLayerCornerRadius:6.f forOrientation:LGPlusButtonsViewOrientationAll];
    [_fbMenuAction setDescriptionsContentEdgeInsets:UIEdgeInsetsMake(4.f, 8.f, 4.f, 8.f) forOrientation:LGPlusButtonsViewOrientationAll];
    
    for (NSUInteger i=1; i<count; i++)
        [_fbMenuAction setButtonAtIndex:i offset:CGPointMake(0.f, 0.f)
                                forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [_fbMenuAction setButtonAtIndex:0 titleOffset:CGPointMake(0.f, -2.f) forOrientation:LGPlusButtonsViewOrientationLandscape];
        [_fbMenuAction setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:32.f] forOrientation:LGPlusButtonsViewOrientationLandscape];
    }
    
    [_bgView addSubview:_fbMenuAction];

   
}

-(UIButton *) addButton : (NSInteger)nTag
               withTitle: (NSString*)nTitle
                andImage: (UIImage*)nImage{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setTitle:nTitle forState:UIControlStateNormal];
    [button setImage:nImage forState:UIControlStateNormal];
    button.layer.masksToBounds = true;
    button.layer.cornerRadius = 20;
    button.layer.borderWidth = 2;
    button.layer.borderColor = [UIColorFromRGB(const_color_darkprimary)CGColor];
    [button setTitleColor:UIColorFromRGB(const_color_primary) forState:UIControlStateNormal];
    [button setTintColor:UIColorFromRGB(const_color_primary)];
    button.backgroundColor = [UIColor whiteColor];
    button.tag = nTag;
    [button addTarget:self action:@selector(actionMakeChoice:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(void) newFloating{
    NSString *lblFav = @"";
    NSString *lblShr = @"";
    if([lang isEqualToString:@"id"])
    {
        lblFav = @" Favoritkan";
        lblShr = @" Bagikan";
    }else{
        lblFav = @" Favorite";
        lblShr = @" Share";
    }
    
    UIButton *btnFavorite = [self addButton:1 withTitle:lblFav andImage:[UIImage imageNamed:@"ic_bt_favoritkan_prm"]];
    btnFavorite.titleLabel.font = [UIFont fontWithName:const_font_name3 size:14];
    UIButton *btnShare = [self addButton:2 withTitle:lblShr andImage:[UIImage imageNamed:@"ic_bt_share_prm"]];
    btnShare.titleLabel.font = [UIFont fontWithName:const_font_name3 size:14];

    CGRect frmBtnShare = btnShare.frame;
    CGRect frmBtnFav = btnFavorite.frame;
    
    frmBtnFav.origin.x = 16;
    frmBtnFav.size.width = ((SCREEN_WIDTH - (frmBtnFav.origin.x *2))/2) - 8;
    frmBtnFav.size.height = 40;
    frmBtnFav.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_DEF - self.vwButton.frame.size.height - frmBtnFav.size.height - 8;
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    NSLog(@"%f", mainWindow.safeAreaInsets.bottom);
    if (mainWindow.safeAreaInsets.bottom > 0) {
        frmBtnFav.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_NOTCH - self.vwButton.frame.size.height - frmBtnFav.size.height - 16;
    }

    frmBtnShare.size.width = frmBtnFav.size.width;
    frmBtnShare.size.height = frmBtnFav.size.height;
    frmBtnShare.origin.x = frmBtnFav.origin.x + frmBtnFav.size.width + 16;
    frmBtnShare.origin.y = frmBtnFav.origin.y;
    
    btnFavorite.frame = frmBtnFav;
    btnShare.frame = frmBtnShare;
    
    [self.bgView addSubview:btnFavorite];
    [self.bgView addSubview:btnShare];
}

-(void) addViewFloating{
    
    UIButton *btnFavorite = [self addButtomBase:1];
    UIButton *btnShare = [self floatingButton:@"icons8-share-24.png" mTag:2];
    
    CGRect frmBtnShare = btnShare.frame;
    CGRect frmBtnFav = btnFavorite.frame;
    
    frmBtnFav.origin.x = 16;
    frmBtnFav.size.width = SCREEN_WIDTH - (frmBtnFav.origin.x *2);
    frmBtnFav.size.height = 50;
    frmBtnFav.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_DEF - self.vwButton.frame.size.height - frmBtnFav.size.height - 8;
    
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    if (mainWindow.safeAreaInsets.bottom > 0) {
        frmBtnFav.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_NOTCH - self.vwButton.frame.size.height - frmBtnFav.size.height - 8;
    }
    
    frmBtnShare.size.width = 44;
    frmBtnShare.size.height = 44;
    frmBtnShare.origin.x = SCREEN_WIDTH - frmBtnShare.size.width - 16;
    frmBtnShare.origin.y = frmBtnFav.origin.y - frmBtnShare.size.height - 16;
    
    
    btnFavorite.frame = frmBtnFav;
    btnShare.frame = frmBtnShare;
    
    [self.bgView addSubview:btnFavorite];
    [self.bgView addSubview:btnShare];
}

-(UIButton *) floatingButton : (NSString *) imageName
                        mTag : (NSInteger ) nTag{
    UIButton *button  = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.clipsToBounds = YES;
    button.layer.cornerRadius = 44.f/2.f;
    button.layer.shadowColor = [[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]CGColor];
    button.layer.shadowOpacity = 0.25f;
    button.layer.shadowOffset = CGSizeMake(0.f, 2.f);
    button.contentEdgeInsets = UIEdgeInsetsMake(4.f, 8.f, 4.f, 8.f);
    [button setTitle:@"Share" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [button setBackgroundImage:[Utility image1x1WithColor:UIColorFromRGB(0xFFFFC90D)] forState:UIControlStateNormal];
    [button setBackgroundImage:[Utility image1x1WithColor:UIColorFromRGB(0xFFFFF176)] forState:UIControlStateHighlighted | UIControlStateSelected];
    button.tag = nTag;
    button.tintColor = UIColorFromRGB(const_color_primary);
    [button addTarget:self action:@selector(actionMakeChoice:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(UIButton *) addButtomBase : (NSInteger)nTag{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if ([lang isEqualToString:@"id"]) {
        [button setTitle:@"TAMBAHKAN FAVORIT" forState:UIControlStateNormal];
    }else{
        [button setTitle:@"ADD TO FAVORITE" forState:UIControlStateNormal];
    }
    
    button.layer.masksToBounds = true;
    button.layer.cornerRadius = 20;
    button.layer.borderWidth = 2;
    button.layer.borderColor = [[UIColor yellowColor] CGColor];
    button.backgroundColor = UIColorFromRGB(const_color_primary);
    button.tag = nTag;
    [button addTarget:self action:@selector(actionMakeChoice:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(void) actionFBMenu : (NSInteger) index{
    
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    switch (index) {
        case 1:
            [self prosesShareImage];
            break;
        case 2:
            //[self prosesAddFav];
            [self popupCaption];
            break;
        default:
            NSLog(@"%ld", (long)index);
            break;
    }
}

-(void)actionMakeChoice : (UIButton *) sender{
    switch (sender.tag) {
        case 1:
            [self popupCaption];
            break;
        case 2:
            //[self prosesAddFav];
            [self prosesShareImage];
            break;
        default:
            NSLog(@"%ld", (long)index);
            break;
    }
}

-(void) setupLayout{
    
    [_lblContentHeader sizeToFit];
    _lblContentHeader.numberOfLines = 0;
    [_lblContent sizeToFit];
    _lblContent.numberOfLines = 0;
    [_lblContentFooter sizeToFit];
    _lblContentFooter.numberOfLines = 0;
    
//    [_vwContent setBackgroundColor:[UIColor redColor]];
//    [_vwContentScroll setBackgroundColor:[UIColor greenColor]];
//    [_lblContentHeader setBackgroundColor:[UIColor yellowColor]];
//    [_lblContent setBackgroundColor:[UIColor blueColor]];
    
    CGRect frmVwTitle = _vwTitle.frame;
    CGRect frmVwContentScroll = _vwContentScroll.frame;
    CGRect frmVwBtn = _vwButton.frame;
    
    CGRect frmLblTitle = _lblTitle.frame;
    CGRect frmBtnMore = _btnMore.frame;
    
    CGRect frmImgReceiptNormal = _imgReceiptNormal.frame;
    CGRect frmVwContent = _vwContent.frame;
    CGRect frmImgContentHeader = _imgContentHeader.frame;
    CGRect frmVwTextContent = _vwTextContent.frame;
    CGRect frmImgContentFooter = _imgContentFooter.frame;
    
    CGRect frmLblContentHeader = _lblContentHeader.frame;
    CGRect frmLblContent = _lblContent.frame;
    CGRect frmLblContentFooter = _lblContentFooter.frame;
    
    CGRect frmBtnShare = _btnShare.frame;
    CGRect frmBtnOkShare = _btnOkShare.frame;
    CGRect frmBtnOk = _btnOk.frame;
    
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.height = 50;
    
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.origin.x = 0;
    
    frmVwContentScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.size.width  = SCREEN_WIDTH;
    
//    if (IPHONE_X || IPHONE_XS_MAX) {
    UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
    if (mainWindow.safeAreaInsets.bottom > 0) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV_NOTCH);
        if(favIsActive){
             frmVwContentScroll.size.height = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV) - frmVwContentScroll.origin.y - 40 - 50 - 8;
        }else{
             frmVwContentScroll.size.height = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV) - frmVwContentScroll.origin.y - 40;
        }
        
        frmVwContentScroll.size.height = frmVwContentScroll.size.height - 10;
    }else{
        frmVwBtn.origin.y = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV_DEF);
        if (favIsActive) {
            frmVwContentScroll.size.height = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV) - frmVwContentScroll.origin.y - 50 - 8;
        }else{
            frmVwContentScroll.size.height = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV) - frmVwContentScroll.origin.y;
        }
        
        frmVwContentScroll.size.height = frmVwContentScroll.size.height - 30;
    }
    
    
    frmBtnMore.origin.x = SCREEN_WIDTH - frmBtnMore.size.width;
    
    frmLblTitle.size.width = frmVwTitle.size.width - (frmLblTitle.origin.x*2);
    frmLblTitle.size.height = 40;
    frmLblTitle.origin.y = 5;
    
    frmVwContent.origin.x = 0;
    frmVwContent.origin.y = 0;
    frmVwContent.size.width = SCREEN_WIDTH;
    
    frmLblContentHeader.origin.x = 48;
    frmLblContentHeader.origin.y = 90;
    
    frmLblContent.origin.y = frmLblContentHeader.origin.y + frmLblContentHeader.size.height + 28;
    frmLblContentFooter.origin.y = frmLblContent.origin.y + frmLblContent.size.height + 28;
    
    frmVwTextContent.size.height = frmLblContentFooter.origin.y + frmLblContentFooter.size.height;
    frmVwTextContent.origin.x = 8;
    frmVwTextContent.size.width = frmVwContent.size.width - 16;
    
    [_imgContentHeader setHidden:true];
    [_imgContentFooter setHidden:true];
    [_imgReceiptNormal setHidden:true];
    
    frmImgContentHeader.size.width = frmVwContent.size.width;
    frmImgContentFooter.size.width = frmVwContent.size.width;
    frmImgContentFooter.origin.y = frmVwTextContent.origin.y + frmVwTextContent.size.height + 8;
    int contentHeight = frmVwTextContent.size.height+80;
    if(contentHeight < frmVwContentScroll.size.height){
        frmVwContent.size.height = frmVwContentScroll.size.height-16;
    } else {
        frmVwContent.size.height = contentHeight;
    }
    
//        UIImage *backgroundImage = [UIImage imageNamed:@"bsmreceipt_center.png"];
//        UIImage *backgroundImage = [UIImage imageNamed:@"bg_receipt.png"];
    _vimgWatermark.center = self.view.center;
    _vimgWatermark.alpha = 0.111;
    [_vimgWatermark setTransform:CGAffineTransformMakeRotation(4)];
    UIImage *backgroundImage = [UIImage imageNamed:@"ic_strklogo_sgl_iover_v2"];
    [_vwResiLogo setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];
//    [_vwTextContent setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];
    
    frmLblContent.origin.x = frmLblContentHeader.origin.x;
    frmLblContent.size.width = frmVwTextContent.size.width - frmLblContent.origin.x - 8;
    frmLblContentFooter.origin.x = frmLblContentHeader.origin.x;
    frmLblContentFooter.size.width = frmVwTextContent.size.width - (frmLblContentFooter.origin.x *2);
    
    frmLblContentHeader.size.width = frmVwTextContent.size.width - frmLblContentHeader.origin.x;
    frmLblContent.size.width = frmVwTextContent.size.width - frmLblContent.origin.x;
    
    frmBtnOk.origin.x = 16;
    frmBtnOk.size.width = frmVwBtn.size.width - (frmBtnOk.origin.x * 2);
    
    frmBtnShare.origin.x = frmBtnOk.origin.x;
    frmBtnShare.size.width = (frmVwBtn.size.width/2) - 16;
    
    frmBtnOkShare.origin.x = frmBtnShare.origin.x + frmBtnShare.size.width + 8;
    frmBtnOkShare.size.width = frmBtnShare.size.width - 8;
    
    _vwTitle.frame = frmVwTitle;
    _vwContentScroll.frame = frmVwContentScroll;
    _vwButton.frame = frmVwBtn;
    
    _lblTitle.frame = frmLblTitle;
    _btnMore.frame = frmBtnMore;
    
    _vwContent.frame = frmVwContent;
    _imgReceiptNormal.frame = frmImgReceiptNormal;
    _imgContentHeader.frame = frmImgContentHeader;
    _vwTextContent.frame = frmVwTextContent;
    _imgContentFooter.frame = frmImgContentFooter;
    
    _lblContentHeader.frame = frmLblContentHeader;
    _lblContent.frame = frmLblContent;
    _lblContentFooter.frame = frmLblContentFooter;
    
    _btnShare.frame = frmBtnShare;
    _btnOkShare.frame = frmBtnOkShare;
    _btnOk.frame = frmBtnOk;
    
    //update styles
    [self.vwTitle setBackgroundColor:[UIColor whiteColor]];
    [self.lblTitle setTextAlignment:NSTextAlignmentCenter];
    
    [_vwContentScroll setContentSize:CGSizeMake(SCREEN_WIDTH, (_vwContent.frame.origin.y + _vwContent.frame.size.height + 16))];
    
}


    
- (IBAction)actionMore:(id)sender { //Favorite
    NSString *msgCancel = @"";
    NSString *msgFavorite = @"";
    NSString *msgShare = @"";
    if([lang isEqualToString:@"id"]){
        msgCancel = @"Batal";
        msgFavorite = @"Favorit";
        msgShare = @"Bagikan";
    } else {
        msgCancel = @"Cancel";
        msgFavorite = @"Favorite";
        msgShare = @"Share";
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"More" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    if (self.jsonData) {
        if ([[self.jsonData objectForKey:@"favorite"]boolValue]) {
            [actionSheet addAction:[UIAlertAction actionWithTitle:msgFavorite style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self prosesAddFav];
            }]];
        }
    }
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgShare style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self prosesShareImage];
        
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)actionShare:(id)sender { //Share
    [self prosesShareImage];
}

- (IBAction)actionOkShare:(id)sender { //ini OKE
    [self okListener];
}

-(void) okListener{
    NSDictionary *rd = [userDefault objectForKey:@"infaqData"];
    if ([rd count] > 0) {
        if ([[rd valueForKey:@"payinfaq"] isEqualToString:@"0"]) {
            infaqStatus = @"0";
        }
        else if ([[rd valueForKey:@"payinfaq"] isEqualToString:@"1"]) {
            infaqStatus = @"1";
        }
    }
    if ([infaqStatus isEqualToString:@"1"]) {
        NSString *ipaddr = [Utility deviceIPAddress];
    
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
        NSString *currentDate = [dateFormat stringFromDate:date];
        NSString *idacc = [dataManager.dataExtra objectForKey:@"id_account"];
        //NSString *trid =[dataManager.dataExtra objectForKey:@"transaction_id"];
        NSString *trid = [userDefault valueForKey:@"tridtx"];
        NSString *mid = @"00021";
        NSString *code = [dataManager.dataExtra objectForKey:@"code"];
        NSString *pin = [userDefault objectForKey:@"pin"];
//        NSString *cid = [userDefault objectForKey:@"customer_id"];
        NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        NSString *payinfaq = @"0";
        NSString *devType = [UIDevice currentDevice].model;
        NSMutableDictionary *temp = [NSMutableDictionary dictionary];
        
        [temp setValue:payinfaq forKey:@"payinfaq"];
        [temp setValue:idacc forKey:@"id_account"];
        [temp setValue:trid forKey:@"transaction_id"];
        [temp setValue:mid forKey:@"menu_id"];
        [temp setValue:code forKey:@"code"];
        [temp setValue:pin forKey:@"pin"];
        [temp setValue:cid forKey:@"customer_id"];
        [temp setValue:lang forKey:@"language"];
        [temp setValue:@"iphone" forKey:@"device"];
        [temp setValue:devType forKey:@"device_type"];
        [temp setValue:ipaddr forKey:@"ip_address"];
        [temp setValue:currentDate forKey:@"date_local"];
        [temp setValue:infaqStatus forKey:@"infaq_status"];
        [temp setValue:goldStatus forKey:@"gold_status"];
        [userDefault setObject:temp forKey:@"passingData"];
        [userDefault removeObjectForKey:@"tridtx"];
        [super openTemplate:@"LD01" withData:dataManager.getObjectData];
    }
    else {
        
        NSDictionary* userInfo = @{@"position": @(1114)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
        if(frmTrx){
            if([goldStatus isEqualToString:@"1"]){
                [super gotoGold];
            }else{
                [super backToRoot];
            }
        }else{
            UINavigationController *navigationController = self.navigationController;
            [navigationController popViewControllerAnimated:YES];
            //NSLog(@"Views in hierarchy: %@", [navigationController viewControllers]);
        }
        
    }
}

- (IBAction)actionOk:(id)sender {
    [self okListener];
}

- (void)okButtonTapped:(UIButton *)sender {
    
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if (![isEtcs isEqualToString:@"1"]) {
        isEtcs = @"1";
        if ([[self.jsonData objectForKey:@"favorite"]boolValue]) {
            [self addFavsButton];
        }
        else {
            [btnFavs removeFromSuperview];
            [lblFavs removeFromSuperview];
        }
        [self addShrsButton];
    } else {
        isEtcs = @"0";
        if ([[self.jsonData objectForKey:@"favorite"]boolValue]) {
            [btnFavs removeFromSuperview];
            [lblFavs removeFromSuperview];
        }
        
        [btnShrs removeFromSuperview];
        [lblShrs removeFromSuperview];
        DLog(@"Remove Fav Button");
        DLog(@"Remove Share Button");
    }
}

- (void)addFavsButton{
    if (scrHeights >= 812.0f) {
        lblFavs = [[UILabel alloc]initWithFrame:CGRectMake(scrWidths - 130, scrHeights - 350, 50, 50)];
    } else {
        lblFavs = [[UILabel alloc]initWithFrame:CGRectMake(scrWidths - 130, scrHeights - 300, 50, 50)];
    }
    lblFavs.text = nmFavs;
    lblFavs.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblFavs.numberOfLines = 1;
    lblFavs.baselineAdjustment = YES;
    lblFavs.adjustsFontSizeToFitWidth = YES;
    lblFavs.clipsToBounds = YES;
    lblFavs.backgroundColor = [UIColor clearColor];
    lblFavs.textColor = [UIColor blackColor];
    lblFavs.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:lblFavs];
    
    btnFavs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if (scrHeights >= 812.0f) {
        btnFavs.frame = CGRectMake(scrWidths - 70, scrHeights - 340, 50, 50);
    } else {
        btnFavs.frame = CGRectMake(scrWidths - 70, scrHeights - 290, 50, 50);
    }
    btnFavs.backgroundColor = UIColor.yellowColor;
    btnFavs.layer.cornerRadius = btnFavs.frame.size.height / 2;
    btnFavs.layer.masksToBounds = true;
    
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-heart-outline-24.png"];
    [btnFavs setImage:btnFloat forState:UIControlStateNormal];
    [btnFavs addTarget:self action:@selector(favsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnFavs];
    [self.view bringSubviewToFront:btnFavs];
    DLog(@"Add Favorit Button");
}

- (void)addShrsButton{
    if (scrHeights >= 812.0f) {
        lblShrs = [[UILabel alloc]initWithFrame:CGRectMake(scrWidths - 130, scrHeights - 285, 50, 50)];
    } else {
        lblShrs = [[UILabel alloc]initWithFrame:CGRectMake(scrWidths - 130, scrHeights - 235, 50, 50)];
    }
    lblShrs.text = nmShrs;
    lblShrs.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblShrs.numberOfLines = 1;
    lblShrs.baselineAdjustment = YES;
    lblShrs.adjustsFontSizeToFitWidth = YES;
    lblShrs.clipsToBounds = YES;
    lblShrs.backgroundColor = [UIColor clearColor];
    lblShrs.textColor = [UIColor blackColor];
    lblShrs.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:lblShrs];
    
    btnShrs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    if (scrHeights >= 812.0f) {
        btnShrs.frame = CGRectMake(scrWidths - 70, scrHeights - 275, 50, 50);
    } else {
        btnShrs.frame = CGRectMake(scrWidths - 70, scrHeights - 225, 50, 50);
    }
    btnShrs.backgroundColor = UIColor.yellowColor;
    btnShrs.layer.cornerRadius = btnShrs.frame.size.height / 2;
    btnShrs.layer.masksToBounds = true;
    
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-share-24.png"];
    [btnShrs setImage:btnFloat forState:UIControlStateNormal];
    [btnShrs addTarget:self action:@selector(shrsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnShrs];
    [self.view bringSubviewToFront:btnShrs];
    DLog(@"Add Share Button");
    
}

- (void)favsButtonTapped:(UIButton *)sender {
    
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    DLog(@"Favorite button was tapped: dismiss the view controller.");
    if (self.jsonData) {
        if ([[self.jsonData objectForKey:@"favorite"]boolValue]) {
            [self prosesAddFav];
        }
    }
}

-(void) prosesAddFav{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSDictionary *rd = [userDefault objectForKey:@"infaqData"];
    //NSDictionary *lr = [userDefault objectForKey:@"listRecipt"];
    if ([rd count] > 0) {
        if ([[rd valueForKey:@"payinfaq"] isEqualToString:@"0"]) {
            infaqStatus = @"0";
        }
        else if ([[rd valueForKey:@"payinfaq"] isEqualToString:@"1"]) {
            infaqStatus = @"1";
        }
    }
    
    if ([infaqStatus isEqualToString:@"1"]) {
        NSString *ipaddr = [Utility deviceIPAddress];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
        NSString *currentDate = [dateFormat stringFromDate:date];
        NSString *idacc = [dataManager.dataExtra objectForKey:@"id_account"];
        //NSString *trid = [dataManager.dataExtra objectForKey:@"transaction_id"];
        NSString *trid = [userDefault valueForKey:@"tridtx"];
        NSString *mid = @"00021";
        NSString *code = [dataManager.dataExtra objectForKey:@"code"];
        NSString *pin = [userDefault objectForKey:@"pin"];
//        NSString *cid = [userDefault objectForKey:@"customer_id"];
        NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        NSString *payinfaq = @"0";
        NSString *devType = [UIDevice currentDevice].model;
        NSMutableDictionary *temp = [NSMutableDictionary dictionary];
        
        [temp setValue:payinfaq forKey:@"payinfaq"];
        [temp setValue:idacc forKey:@"id_account"];
        [temp setValue:trid forKey:@"transaction_id"];
        [temp setValue:mid forKey:@"menu_id"];
        [temp setValue:code forKey:@"code"];
        [temp setValue:pin forKey:@"pin"];
        [temp setValue:cid forKey:@"customer_id"];
        [temp setValue:lang forKey:@"language"];
        [temp setValue:@"iphone" forKey:@"device"];
        [temp setValue:devType forKey:@"device_type"];
        [temp setValue:ipaddr forKey:@"ip_address"];
        [temp setValue:currentDate forKey:@"date_local"];
        [temp setValue:infaqStatus forKey:@"infaq_status"];
        
        [userDefault setObject:temp forKey:@"passingData"];
    }
    
    [self openTemplate:@"SUCFAV" withData:nil];
}

- (void)setFromRecipt:(BOOL)recipt {
    fromRecipt = recipt;
}

- (void)shrsButtonTapped:(UIButton *)sender {
    DLog(@"Share button was tapped: dismiss the view controller.");
    [self prosesShareImage];
}

-(void)prosesShareImage{
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    UIGraphicsBeginImageContextWithOptions(_vwContent.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_vwContent.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)actionSaveCaption {

    if ([txtFieldCaption.text isEqualToString:@""] || txtFieldCaption.text == nil) {
        NSString *text;
        if ([lang isEqualToString:@"id"]) {
            text = @"Masukkan judul";
        }else{
            text = @"Insert caption";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:text delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 01;
        alert.delegate = self;
        [alert show];
        
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:text preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//        }]];
        
    }else{
        [dataManager.favData setValue:txtFieldCaption.text forKey:@"def_title_fav"];
        [self prosesAddFav];
    }
    
}

- (void)actionCancelCaption {
    [vwBgCaption removeFromSuperview];
}

#pragma mark change cancel to next and other else
- (void)popupCaption {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat widthContiner =  width - 40;
    CGFloat heightContiner = 200;
    
    NSString *word1 = @"";
    NSString *word2 = @"";
    NSString *word3 = @"";
    NSString *word4 = @"";
    if([lang isEqualToString:@"id"]){
        word1 = @"Tambah Favorit";
        word2 = @"Judul";
        word3 = @"TAMBAH";
        word4 = @"BATAL";
    } else {
        word1 = @"Add Favorite";
        word2 = @"Caption";
        word3 = @"SAVE";
        word4 = @"CANCEL";
    }
    vwBgCaption = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    vwBgCaption.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    vwPopupScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _vwTitle.frame.origin.y + _vwTitle.frame.size.height, SCREEN_WIDTH, SCREEN_HEIGHT - (_vwTitle.frame.origin.y + _vwTitle.frame.size.height))];
    
    vwContinerCaption = [[UIView alloc]initWithFrame:CGRectMake(20, (vwPopupScroll.frame.size.height/2) - (heightContiner/2) - 40, vwPopupScroll.frame.size.width - 40, heightContiner)];
    vwContinerCaption.backgroundColor = [UIColor whiteColor];
    vwContinerCaption.layer.cornerRadius = 10;
    vwContinerCaption.layer.masksToBounds = YES;
    
    [vwPopupScroll addSubview:vwContinerCaption];
    
    lblTitleCaption = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, widthContiner - 20, 20)];
    [lblTitleCaption setFont:[UIFont fontWithName:@"Lato-Bold" size:19]];
    [lblTitleCaption setTextColor:[UIColor blackColor]];
    [lblTitleCaption setTextAlignment:NSTextAlignmentCenter];
    [lblTitleCaption setText:word1];

    vwLineTitleCaption = [[UIView alloc]initWithFrame:CGRectMake(0, 40, widthContiner, 1)];
    vwLineTitleCaption.backgroundColor = [UIColor grayColor];
    
    lblTextCaption = [[UILabel alloc] initWithFrame:CGRectMake(15, 50, widthContiner - 30, 20)];
    [lblTextCaption setFont:[UIFont fontWithName:@"Lato" size:13]];
    [lblTextCaption setTextColor:[UIColor colorWithRed:(21.0/255.0) green:(124.0/255.0) blue:(104.0/255.0) alpha:1]];
    [lblTextCaption setTextAlignment:NSTextAlignmentLeft];
    [lblTextCaption setText:word2];
    
    txtFieldCaption = [[UITextField alloc] initWithFrame:CGRectMake(15, 70, widthContiner - 30, 40)];
    [txtFieldCaption setFont:[UIFont fontWithName:@"Lato" size:17]];
    [txtFieldCaption setTextColor:[UIColor blackColor]];
    [txtFieldCaption setText:defTitleReceipt];
    
    txtFieldCaption.inputAccessoryView = keyboardDoneButtonView;
    
    vwLineTextCaption = [[UIView alloc]initWithFrame:CGRectMake(15, 110, widthContiner - 30, 1)];
    vwLineTextCaption.backgroundColor = [UIColor grayColor];
    
    btnCancelCaption = [[CustomBtn alloc] initWithFrame:CGRectMake(widthContiner/2 + 10, heightContiner - 60, widthContiner/2 - 20, 40)];
    [btnCancelCaption setColorSet:PRIMARYCOLORSET];

    [btnCancelCaption setTitle:word3 forState:UIControlStateNormal];
    [btnCancelCaption addTarget:self action:@selector(actionSaveCaption) forControlEvents:UIControlEventTouchUpInside];
    
    btnSaveCaption = [[CustomBtn alloc] initWithFrame:CGRectMake(10, heightContiner - 60, widthContiner/2 - 20, 40)];
    [btnSaveCaption setColorSet:SECONDARYCOLORSET];

    [btnSaveCaption setTitle:word4 forState:UIControlStateNormal];
    [btnSaveCaption addTarget:self action:@selector(actionCancelCaption) forControlEvents:UIControlEventTouchUpInside];
    
    [vwContinerCaption addSubview:lblTitleCaption];
    [vwContinerCaption addSubview:vwLineTitleCaption];
    [vwContinerCaption addSubview:lblTextCaption];
    [vwContinerCaption addSubview:txtFieldCaption];
    [vwContinerCaption addSubview:vwLineTextCaption];
    [vwContinerCaption addSubview:btnSaveCaption];
    [vwContinerCaption addSubview:btnCancelCaption];
    
    [vwPopupScroll setContentSize:CGSizeMake(widthContiner, vwContinerCaption.frame.size.height)];
    
    [vwBgCaption addSubview:vwPopupScroll];
    [self.view addSubview:vwBgCaption];
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [vwPopupScroll setContentSize:CGSizeMake(vwContinerCaption.frame.size.width, vwContinerCaption.frame.size.height + kbSize.height + 20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    vwPopupScroll.contentInset = contentInsets;
    vwPopupScroll.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [vwPopupScroll setContentSize:CGSizeMake(vwContinerCaption.frame.size.width, vwContinerCaption.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    vwPopupScroll.contentInset = contentInsets;
    vwPopupScroll.scrollIndicatorInsets = contentInsets;
}

- (void)setDataLocal:(NSDictionary*) object{
    dataObject = object;
}

- (void)setFromTrx:(BOOL*) trx{
    frmTrx = trx;
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([requestType isEqualToString:@"list_account2"]) {
//    if ([requestType isEqualToString:@"list_account1"]) {
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
        }
    }else{
        if (![requestType isEqualToString:@"check_notif"]) {
            if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]){
                if([requestType isEqualToString:@"insert_favorite"]){
                    [super didFinishLoadData:jsonObject withRequestType:requestType];
                }else{
                    
                    if([userDefault objectForKey:@"event_tracker"] != nil
                       && ![[userDefault valueForKey:@"event_tracker"] isEqualToString:@""]){
                        [Utility setEventTracker:[userDefault objectForKey:@"event_tracker"] withParam:nil];
                        [userDefault removeObjectForKey:@"event_tracker"];
                    }
                    
                    NSString *response = [jsonObject valueForKey:@"response"];
                    NSError *error;
                    NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                    if (error == nil) {
                        [_vwContent setHidden:NO];
                        _lblContentHeader.text = [[dict objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
                        _lblContent.text = [[dict objectForKey:@"msg"] stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
                        _lblContentFooter.text = [[dict objectForKey:@"footer_msg"] stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
                        defTitleReceipt = [dict objectForKey:@"title"];
                        [self setupLayout];
                        
                        NSDate *date = [NSDate date];
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                        [dateFormat setDateFormat:@"dd MMM yyyy"];
                        NSString *currentDate = [dateFormat stringFromDate:date];
                        
                        NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
//                        [dictList setValue:self.lblTitle.text forKey:@"titlebar"];
                        [dictList setValue:[dict objectForKey:@"trxref"] forKey:@"trxref"];
                        [dictList setValue:[dict objectForKey:@"title"] forKey:@"title"];
                        [dictList setValue:[dict objectForKey:@"msg"] forKey:@"msg"];
                        [dictList setValue:[dict objectForKey:@"footer_msg"] forKey:@"footer_msg"];
                        [dictList setValue:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
                        [dictList setValue:@"GENCF02" forKey:@"template"];
                        [dictList setValue:@"General" forKey:@"jenis"];
                        [dictList setValue:currentDate forKey:@"time"];
                        [dictList setValue:@"0" forKey:@"marked"];
                        
                        InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
                        [inboxHeldper insertDataInbox:dictList];
                        
                        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//                        NSString *strCID = [NSString stringWithFormat:@"%@", [userDefault valueForKey:@"customer_id"]];
                        NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
                        
                        NSMutableArray *arBackUpInbox = [[NSMutableArray alloc]init];
                        
                        NSDictionary *backUpInbox = [Utility dictForKeychainKey:strCID];
                        NSMutableDictionary *contentBackup = [[NSMutableDictionary alloc]init];
                        
                        
                        //disini validasi jika sudah ada data dikeychain ambil data timpa ke array baru
                        if (backUpInbox) {
                            NSArray *arrLastBackup = [backUpInbox valueForKey:@"data"];
                            for(NSDictionary *dictLastBackup in arrLastBackup){
                                if([dictLastBackup valueForKey:@"content"]!= nil){
                                    NSDictionary *data = @{@"content" : [dictLastBackup valueForKey:@"content"]};
                                    [arBackUpInbox addObject:data];
                                }
                            }
                            
                        }
                        NSDictionary *dataDd = [_aesChiper dictAesEncryptString:dictList];
                        
                        [contentBackup setObject:dataDd forKey:@"content"];
                        [arBackUpInbox addObject:contentBackup];
                        
                        if (arBackUpInbox.count > 0) {
                            DLog(@"Data Array For Backup : %@", arBackUpInbox);
                            NSMutableDictionary *storeBackupInbox = [[NSMutableDictionary alloc]init];
                            [storeBackupInbox setObject:arBackUpInbox forKey:@"data"];
                            if (storeBackupInbox) {
                                DLog(@"Data Dict For Backup : %@", storeBackupInbox);
                                [Utility removeForKeychainKey:strCID];
                                [Utility setDict:storeBackupInbox forKey:strCID];
                            }
                        }
                        
#pragma mark insert data recently
                        [self saveOnRecentlyUse:dict];
                        
#pragma mark call request list account
                        if ([requestType isEqualToString:@"account_opening"]) {
                            
                            [userDefault setValue:nil forKey:OBJ_FINANCE];
                            [userDefault setValue:nil forKey:OBJ_SPESIAL];
                            [userDefault setValue:nil forKey:OBJ_ALL_ACCT];
                            [userDefault setValue:nil forKey:OBJ_ROLE_FINANCE];
                            [userDefault setValue:nil forKey:OBJ_ROLE_SPESIAL];
                            [userDefault synchronize];
                            
                            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
                            Connection *conn = [[Connection alloc]initWithDelegate:self];
                            [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:false];
                        }
                        
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"changeIcon"
                         object:self];
                    }else{
                        DLog(@"%@", error.localizedDescription);
                    }
                    
                    
                    if([mid isEqualToString:@"00140"] && [dataManager.dataExtra valueForKey:@"pl_akad"] != nil){
                        NSDate *date = [NSDate date];
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                        [dateFormat setDateFormat:@"ddMMyyyyhhmmss"];
                        NSString *currdate = [dateFormat stringFromDate:date];
                        NSString *filepath = [NSString stringWithFormat:@"~/Documents/akad_paylater_%@.pdf",currdate];
                        NSString *strHtml = [dataManager.dataExtra valueForKey:@"pl_akad"];
                        self.PDFCreator = [NDHTMLtoPDF createPDFWithHTML:strHtml pathForPDF:[filepath stringByExpandingTildeInPath] delegate:self pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(10, 5, 10, 5)];
                        NSLog(@"%@", strHtml);
                    }
                    
                    if([mid isEqualToString:menuIDPaylater] && [dataManager.dataExtra valueForKey:@"content_link"] != nil){
//                        [self gotoDownloadWithBase64:[dataManager.dataExtra valueForKey:@"content_link"]];
                    }
                    
                }
            }else if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"PL01"]){
                NSString *response = [jsonObject valueForKey:@"response"];
                NSError *error;
                NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                
                NSMutableDictionary *passingData = [[NSMutableDictionary alloc]init];
                [passingData setValue:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
//                [passingData setValue:self.jsonData forKey:@"payment_objectdata"];
                [passingData setValue:dict forKey:@"paylater_passingdata"];
//                [userDefault setValue:dict forKey:@"paylater_passingdata"];
                [self openTemplateByMenuID:@"00140" withData:passingData];
            }else{
                if ([jsonObject valueForKey:@"response"]){
                    if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
                        NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    } else {
                        NSString * msg = [jsonObject valueForKey:@"response"];
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                } else {
                    NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];

}

- (void)reloadApp{
    if([mid isEqualToString:@"00110"] ||
       [mid isEqualToString:@"00111"] ||
       [mid isEqualToString:@"00112"] ||
       [mid isEqualToString:@"00113"] ||
       [mid isEqualToString:@"00115"] ||
       [mid isEqualToString:@"00116"]){
        [self gotoGold];
    }else{
        [self backToRoot];
    }
    [BSMDelegate reloadApp];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    }
    else if(alertView.tag == 01){
        DLog(@"caption is empty");
    }
    else {
        if([mid isEqualToString:@"00110"] ||
           [mid isEqualToString:@"00111"] ||
           [mid isEqualToString:@"00112"] ||
           [mid isEqualToString:@"00113"] ||
           [mid isEqualToString:@"00115"] ||
           [mid isEqualToString:@"00116"]){
            [self gotoGold];
        }else{
            [self backToRoot];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


- (IBAction)doneClicked:(id)sender
{
    DLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

-(void) saveOnRecentlyUse : (NSDictionary *) contentValue{
    NSMutableDictionary *dataDictRecently = [[NSMutableDictionary alloc]init];
    [dataDictRecently setValue:[dataManager.dataExtra valueForKey:@"payment_id"] forKey:F_ACCT_REK];
    [dataDictRecently setValue:mid forKey:F_MENU_ID];
    
    NSString *strResponse = [[contentValue objectForKey:@"msg"] stringByReplacingOccurrencesOfString:@"\t" withString:@" "];
    NSString *strTitle = [[contentValue objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"\t" withString:@" "];;
    
    NSArray *arDataMsg = [strResponse componentsSeparatedByString:@"\n"];
    NSArray *arrMenuRecentlyUse = [[userDefault valueForKey:@"config.allow.recently"] componentsSeparatedByString:@","];
    if (arrMenuRecentlyUse.count > 0) {
        for(NSString *menuRecentlyUse in arrMenuRecentlyUse){
            if ([menuRecentlyUse isEqualToString:mid]) {
                NSString *nRecipient = @"";
                NSString *nCode = [mid isEqualToString:@"00027"] ? @"451" : [dataManager.dataExtra valueForKey:F_CODE];
                NSString *nBank = @"";
                if ([mid isEqualToString:@"00027"] || [mid isEqualToString:@"00028"]) {
                    if ([dataManager.dataExtra valueForKey:@"beneficiary_name"]){
                        nRecipient = [dataManager.dataExtra valueForKey:@"beneficiary_name"];
                    } else if ([Utility arContain:arDataMsg mString:@"penerima"]) {
                        nRecipient = [Utility findArrConstain:arDataMsg strFind:@"penerima"];
                    } else if([Utility arContain:arDataMsg mString:@"recipient"]){
                        nRecipient = [Utility findArrConstain:arDataMsg strFind:@"recipient"];
                    }
                    [dataDictRecently setValue:nRecipient forKey:F_ACCT_TRSCT_NAME];
                    
                    if ([dataManager.dataExtra valueForKey:@"beneficiary_bank"]){
                        nBank = [dataManager.dataExtra valueForKey:@"beneficiary_bank"];
                    } else if ([Utility arContain:arDataMsg mString:@"bank penerima"]) {
                        nBank = [Utility findArrConstain:arDataMsg strFind:@"bank penerima"];
                    } else if([Utility arContain:arDataMsg mString:@"destination bank"]){
                        nBank = [Utility findArrConstain:arDataMsg strFind:@"destination bank"];
                    }
                    nCode = [NSString stringWithFormat:@"%@|%@",nCode,nBank];
                }else if ([mid isEqualToString:@"00026"]){
                    [dataDictRecently setValue:[arDataMsg objectAtIndex:2] forKey:F_ACCT_TRSCT_NAME];
                }else{
                    NSString *trsactName = nil;
                    if([Utility arContain:arDataMsg mString:@"nama"]){
                        trsactName = [Utility findArrConstain:arDataMsg strFind:@"nama" strMenuId:mid];
                    }else if([Utility arContain:arDataMsg mString:@"name"]){
                        trsactName = [Utility findArrConstain:arDataMsg strFind:@"name" strMenuId:mid];
                    }
                    
                    if ([trsactName isEqualToString:@""] ||trsactName == nil) {
                        [dataDictRecently setValue:strTitle forKey:F_ACCT_TRSCT_NAME];
                    }else{
                       [dataDictRecently setValue:trsactName forKey:F_ACCT_TRSCT_NAME];
                    }
                }
                
                [dataDictRecently setValue:nCode forKey:F_CODE];
                [dataDictRecently setValue:[Utility dateToday:1] forKey:F_DATE];
                
                [mHelper insertRecentlyUse:dataDictRecently];
                
                break;
            }
        }
        
        
    }else{
        DLog(@"No list Menu accepted");
    }
}


- (NSString *) getTrasactPurOrPay : (NSString *) menuId
                      dataMessage : (NSArray *) mDataMessage{
    NSArray *menuIdAccepted = @[@"00066",@"00013",@"00044", @"00017",
                                @"00018",@"00013",@"00058",@"00065"];
    NSArray *arValidasi = @[@"nama", @"name"];
    
    BOOL isSame = false;
    NSString *transactName = @"";
    for(NSString *menuAcct in menuIdAccepted){
        if ([menuAcct isEqualToString:menuId]) {
            isSame = true;
            break;
        }
    }
    if (isSame) {
        for(NSString *validasi in arValidasi){
            if (![[Utility findArrConstain:mDataMessage strFind:validasi]isEqualToString:@""]) {
                transactName = [Utility findArrConstain:mDataMessage strFind:validasi];
                break;
            }
        }
    }
    return transactName;
    
}

-(void)setFromAkad: (NSString *) fromAKad{
    fromAkad = fromAKad;
}

-(int) getIdxMenu : (NSString *) menuId{
    NSArray *menuIdAccepted = @[@"00066",@"00013",@"00044", @"00017",
                                @"00018",@"00013",@"00058",@"00065"];
    NSArray *menuIdAcceptedTransfer = @[@"00027",@"00028"];
    NSArray *menuIdAcceptedIdxLast = @[@"00026"];
    
    for(NSString *menuPurOrPay in menuIdAccepted){
        if ([menuPurOrPay isEqualToString:menuId]) {
            return 1;
        }
    }
    
    for(NSString *menuTransfer in menuIdAcceptedTransfer){
        if ([menuTransfer isEqualToString:menuId]) {
            return 2;
        }
    }
    
    for(NSString *menuOthers in menuIdAcceptedIdxLast){
        if ([menuOthers isEqualToString:menuId]) {
            return 3;
        }
    }
    
    return 0;
    
}


- (void)HTMLtoPDFDidSucceed:(NDHTMLtoPDF *)htmlToPDF{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
    NSLog(@"%@",result);
    
    UIDocumentInteractionController *documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:htmlToPDF.PDFpath]];
    documentController.delegate = self;
    [documentController presentOpenInMenuFromRect:CGRectMake(0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 200) inView:self.view animated:YES];
    if(![documentController presentPreviewAnimated:YES])
    {
        NSString *msg = @"";
        
        if([Utility isLanguageID]){
            msg = @"Failed to open pdf file";
        }else{
            msg = @"Gagal membuka file pdf";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Oops" message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)HTMLtoPDFDidFail:(NDHTMLtoPDF *)htmlToPDF{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
    NSLog(@"%@",result);
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (void) gotoDownloadWithBase64:(NSString*)base64{
    NSData *data = [NSData dataWithBase64EncodedString:base64];
    NSArray* shareItem = @[data];
    UIActivityViewController *avvc = [[UIActivityViewController alloc] initWithActivityItems:shareItem applicationActivities:nil];
    avvc.excludedActivityTypes = @[UIActivityTypeAirDrop];
    if (avvc == nil){
       return;
    }
    [avvc setCompletionWithItemsHandler:^(UIActivityType _Nullable activityType, BOOL completed,
                                          NSArray * _Nullable returnedItems,
                                          NSError * _Nullable activityError) {

        if (activityType == nil)    {
            NSLog(@"UIActivityViewController dismissed without any action.");
            
        } else {
            NSLog(@"completionWithItemsHandler, activityType: %@, completed: %d, returnedItems: %@, activityError: %@",
             activityType, completed, returnedItems, activityError);
        }
    }];
    [self presentViewController:avvc animated:YES completion:nil];
}

- (void) addedButtonUnduh{
    
    CustomBtn *btnUnduh = [[CustomBtn alloc]init];
    [btnUnduh setBackgroundColor:UIColorFromRGB(const_color_primary)];
    NSString *btnTitle = @"";
    if([Utility isLanguageID]){
        btnTitle = @"UNDUH DOKUMEN\nPERJANJIAN PEMBIAYAAN";
    }else{
        btnTitle = @"DOWNLOAD DOKUMEN\nFINANCING AGREEMENT";
    }
    [btnUnduh setTitle:btnTitle forState:UIControlStateNormal];
    [btnUnduh.titleLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
    [btnUnduh.titleLabel setNumberOfLines:2];
    [btnUnduh.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnUnduh.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnUnduh setColorSet:PRIMARYCOLORSET];
    [btnUnduh addTarget:self action:@selector(actionUnduh) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frmVwButton = _vwButton.frame;
    CGRect frmBtnOK = _btnOk.frame;
    CGRect frmBtnUnduh = btnUnduh.frame;
    
    frmBtnUnduh.size.width = frmBtnOK.size.width;
    frmBtnUnduh.size.height = frmBtnOK.size.height;
    
    frmBtnOK.origin.y = 0;
    
    frmBtnUnduh.origin.x = frmBtnOK.origin.x;
    frmBtnUnduh.origin.y = frmBtnOK.origin.y + frmBtnOK.size.height + 4;
    
    frmVwButton.origin.y = frmVwButton.origin.y - (frmBtnUnduh.size.height+4);
    frmVwButton.size.height = (frmBtnOK.size.height*2) + 4;
    
    [_vwButton addSubview:btnUnduh];
    
    _vwButton.frame = frmVwButton;
    _btnOk.frame = frmBtnOK;
    btnUnduh.frame = frmBtnUnduh;
}

- (void) actionUnduh{
    [self gotoDownloadWithBase64:[dataManager.dataExtra valueForKey:@"content_link"]];
}

@end
