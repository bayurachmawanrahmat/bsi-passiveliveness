//
//  CRIB01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CRIB01ViewController.h"
#import "Utility.h"
#import "Styles.h"

@interface CRIB01ViewController ()<UITextFieldDelegate, ConnectionDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    UIToolbar *toolBar;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgStepper;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;
@property (weak, nonatomic) IBOutlet UITextField *txtPin;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSelanjutnya;
@property (weak, nonatomic) IBOutlet UILabel *lblDescalimer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation CRIB01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *ttlToolbar;
    if ([lang isEqualToString:@"id"]) {
        [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnSelanjutnya setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        ttlToolbar = @"Selesai";
    }else{
        [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnSelanjutnya setTitle:@"Next" forState:UIControlStateNormal];
        ttlToolbar = @"Done";
    }
    
    [_btnBatal setColorSet:SECONDARYCOLORSET];
    [_btnSelanjutnya setColorSet:PRIMARYCOLORSET];
    
    [Styles setTopConstant:_topConstant];
    
    self.txtPin.delegate = self;
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.txtPin.frame.size.height);
    
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.txtPin.leftView = paddingPhone;
    self.txtPin.leftViewMode = UITextFieldViewModeAlways;
    [self.txtPin setSecureTextEntry:true];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.txtPin.inputAccessoryView = toolBar;
    
    [self.btnBatal addTarget:self action:@selector(actionBatal:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnSelanjutnya addTarget:self action:@selector(actionSelanjutnya:) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.jsonData){
        [self.lblTitleMenu setText:[self.jsonData valueForKey:@"title"]];
        [self.lblTitleContent setText:[self.jsonData valueForKey:@"field_name"]];
    }
    
    NSString *descalimerText = @"";
    if([lang isEqualToString:@"id"]){
        descalimerText = @"Disclaimer :\nDengan memasukan pin, berarti saya telah menyetujui dan menandatangani secara elektronik terhadap Permohonan Pembiayaan, Syarat Umum Pembiayaan, dan Akad Pembiayaan";
    }else{
        descalimerText = @"Disclaimer :\nBy entering the pin, I have agreed and signed electronically with the Request for Financing, the General Terms of Financing, and the Financing Agreement Contract";
    }
    
    [self.lblDescalimer setText:descalimerText];
    [self.lblDescalimer setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    [self.lblDescalimer setTextColor:UIColorFromRGB(const_color_secondary)];
    [self.lblDescalimer setNumberOfLines:0];
    
    [self setupLayout];
}

-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmImgStepper = self.imgStepper.frame;
    CGRect frmLblTitleContent = self.lblTitleContent.frame;
    CGRect frmTxtPin = self.txtPin.frame;
    CGRect frmVwLine = self.vwLine.frame;
    CGRect frmVwBtn = self.vwBtn.frame;
    
    CGRect frmBtnBtals = self.btnBatal.frame;
    CGRect frmBtnSelanjutnya = self.btnSelanjutnya.frame;
    CGRect frmDesclaimer = self.lblDescalimer.frame;
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
//    frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
//    frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x * 2);
    frmImgStepper.origin.x = 16;
    frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
    frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x *2);
    frmImgStepper.size.height = 30;
    
    frmDesclaimer.origin.x = 16;
    frmDesclaimer.origin.y = frmImgStepper.size.height + frmImgStepper.origin.y + 16;
    frmDesclaimer.size.width = SCREEN_WIDTH - 32;
    frmDesclaimer.size.height = [Utility heightLabelDynamic:self.lblDescalimer sizeWidth:frmDesclaimer.size.width sizeFont:17];
    
    frmLblTitleContent.origin.y = frmDesclaimer.origin.y + frmDesclaimer.size.height + 8;
    frmLblTitleContent.size.width = SCREEN_WIDTH - (frmLblTitleContent.origin.x * 2);
    
    frmTxtPin.origin.x = frmLblTitleContent.origin.x;
    frmTxtPin.origin.y = frmLblTitleContent.origin.y + frmLblTitleContent.size.height  + 8;
    frmTxtPin.size.width = SCREEN_WIDTH - (frmTxtPin.origin.x * 2);
    
    frmVwLine.origin.x = frmTxtPin.origin.x;
    frmVwLine.origin.y = frmTxtPin.origin.y + frmTxtPin.size.height + 5;
    frmVwLine.size.width = frmTxtPin.size.width;
    
    frmVwBtn.origin.y = frmVwLine.origin.y + frmVwLine.size.height + 41;
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.width = SCREEN_WIDTH;
    
    frmBtnBtals.origin.x = 16;
    frmBtnBtals.size.width = frmVwBtn.size.width/2 - (frmBtnBtals.origin.x * 2);
    
    frmBtnSelanjutnya.origin.x = frmBtnBtals.origin.x + frmBtnBtals.size.width + 24;
    frmBtnSelanjutnya.size.width = frmBtnBtals.size.width;
        
    self.vwTitle.frame = frmVwTitle;
    self.imgStepper.frame = frmImgStepper;
    [self.imgStepper setHidden:YES];
    [self.imgStepper setHidden:YES];
    
    self.lblTitleContent.frame = frmLblTitleContent;
    self.txtPin.frame = frmTxtPin;
    self.vwLine.frame = frmVwLine;
    self.vwBtn.frame = frmVwBtn;
    
    self.btnBatal.frame = frmBtnBtals;
    self.btnSelanjutnya.frame = frmBtnSelanjutnya;
    
    self.lblDescalimer.frame = frmDesclaimer;

}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
}

-(void)actionBatal : (UIButton *) sender{
    [self backToR];
}

-(void)actionSelanjutnya : (UIButton *)sender{
    if ([self.txtPin.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]) {
        [Utility showMessage:lang(@"NOT_EMPTY") mVwController:self mTag:01];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldInput = [self.txtPin.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedStringtextFieldInput length]) {
            message = lang(@"STR_NOT_NUM");
        }
        if([message isEqualToString:@""]){
//            if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
                [dataManager setPinNumber:self.txtPin.text];
//                if ([[self.jsonData valueForKey:@"url"]boolValue]) {
//                    Connection *conn = [[Connection alloc]initWithDelegate:self];
//                    [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,pin",[self.jsonData valueForKey:@"url_parm"]] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
//                }else{
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_pin,pin,language"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                    
//                    [self openNextTemplate];
//                }
                
//            }
            
        }else{
            [Utility showMessage:lang(@"INFO") mVwController:self mTag:01];
        }
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    [Utility stopTimer];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if ([[jsonObject objectForKey:@"response_code"]isEqualToString: @"00"]) {
        if (![requestType isEqualToString:@"check_notif"]) {
            
            [self openNextTemplate];
        }
        
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"]  preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)errorLoadData:(NSError *)error{
    [Utility showMessage:error.localizedDescription mVwController:self mTag:99];
}

- (void)reloadApp{
    [self backToRoot];
}

//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    [self backToRoot];
//
//}


@end
