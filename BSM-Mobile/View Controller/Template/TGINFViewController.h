//
//  TGINFViewController.h
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 26/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TGINFViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblConten;
@property (weak, nonatomic) IBOutlet UILabel *lblcheck;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnLink1;
@property (weak, nonatomic) IBOutlet UIButton *btnLink2;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;

@end

NS_ASSUME_NONNULL_END
