//
//  PL01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 03/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PL01ViewController.h"
#import "PLAKADViewController.h"
#import "Styles.h"
#import "PL01Cell.h"
#import "CustomBtn.h"
#import "Utility.h"
#import "NSString+HTML.h"

@interface PL01ViewController ()<UITableViewDelegate, UITableViewDataSource, PLAKADDelegate>{
    NSDictionary *data;
    
    NSArray *periodList;
    NSArray *jatuhTempoList;
    NSArray *merchantTenor;
    NSArray *listAngsuran;
    
    double maxTenor;
    double ujrohPaylater;
    double minLoan;
    double maxLoan;
    double jumlahTagihan;
    double paylaterLimit;
    
    NSString *merchantName;
    NSString *branchCode;
    NSString *customerName;
    NSString *maritalStatus;
    NSString *referenceCode;
    NSString *flagLimit;
    NSString *email;
    NSString *tanggalAngsuran;
    NSString *statusMessage;
    NSString *cellularNo;
    NSString *agreements;
    NSString *akadNO;
    NSString *language;
    NSUserDefaults *userDefaults;
    
    NSNumberFormatter *currencyFormatter;
    
    NSString *titleInvoiceCicil;
    NSString *titleInvoiceTunai;
    NSString *titlePenerimaCicil;
    NSString *titlePenerimaTunai;
    NSString *titleSelection, *titleSelected;
    NSString *ujrohDesc;
    NSString *akadText;
    
    BOOL payLaterState, checkState;
    
    NSInteger selectedRow;
    
    NSDictionary *passingData;
}
@property (weak, nonatomic) IBOutlet UIImageView *iconTitleBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleInvoice;
@property (weak, nonatomic) IBOutlet UILabel *lblInvoiceAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleRincian;
@property (weak, nonatomic) IBOutlet UIImageView *imgPenerima;
@property (weak, nonatomic) IBOutlet UILabel *lblPenerima;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePeriod;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblFeeUjroh;
@property (weak, nonatomic) IBOutlet UIImageView *checkListImage;
@property (weak, nonatomic) IBOutlet UILabel *lblCheck;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCicil;
@property (weak, nonatomic) IBOutlet CustomBtn *btnPurchase;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;

@end

@implementation PL01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
        
    [self dataInisialisation];
    
    [Styles setTopConstant:self.topConstant];
    
    [self.titleBar setText:[self.jsonData objectForKey:@"title"]];
    [self.iconTitleBar setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    
    currencyFormatter = [[NSNumberFormatter alloc]init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [self.tableViewPeriod registerNib:[UINib nibWithNibName:@"PL01Cell" bundle:nil]
    forCellReuseIdentifier:@"PL01CELL"];
    self.tableViewPeriod.delegate = self;
    self.tableViewPeriod.dataSource = self;
    self.tableViewPeriod.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableViewPeriod.rowHeight = UITableViewAutomaticDimension;
    
    [self.labelTitleInvoice setText:titleInvoiceTunai];
    [self.lblTitleRincian setText:titlePenerimaTunai];
    [self.lblTitlePeriod setText:[NSString stringWithFormat:@"%@\n%@",titleSelection, ujrohDesc]];
    
    [self enablePaylater:NO];
    
    [self.btnCicil setEnabled:YES];
    [self.btnCicil setColorSet:TERTIARYCOLORSET];
    [self.btnCicil addTarget:self action:@selector(actionBtnCicil) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPurchase addTarget:self action:@selector(actionBtnPurchase) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPurchase setColorSet:PRIMARYCOLORSET];
    
    [self.lblCheck setHidden:YES];
    [self.checkListImage setHidden:YES];
    
    [self.lblCheck setUserInteractionEnabled:NO];
    [self.lblCheck addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(requestAkad)]];
    
//    [self doRequest];
    
    self.lblTitleRincian.textColor = [UIColor blackColor];
    self.lblCheck.textColor = [UIColor blackColor];
    self.lblFeeUjroh.textColor = [UIColor blackColor];
    self.lblTitlePeriod.textColor = [UIColor blackColor];
    
    [self.tableViewPeriod reloadData];
    self.tableHeight.constant = self.tableViewPeriod.contentSize.height + 20;
    selectedRow = -1;
}

- (void)dataInisialisation{
    
    data = [passingData objectForKey:@"paylater_passingdata"];
    [dataManager.dataExtra setValue:[passingData objectForKey:@"transaction_id"] forKey:@"trx_id_pl"];

    if(data != nil && [data count] > 0){
        maxTenor = [[data objectForKey:@"MaxTenor"]doubleValue];
        ujrohPaylater = [[data objectForKey:@"UjrahPaylater"]doubleValue];
        minLoan = [[data objectForKey:@"MinLoan"]doubleValue];
        maxLoan = [[data objectForKey:@"MaxLoan"]doubleValue];
        jumlahTagihan = [[data objectForKey:@"JumlahTagihan"]doubleValue];
        paylaterLimit = [[data objectForKey:@"PaylaterLimit"]doubleValue];
        
        merchantName = [data objectForKey:@"MerchantName"];
        branchCode = [data objectForKey:@"BranchCode"];
        customerName = [data objectForKey:@"CustomerName"];
        tanggalAngsuran = [data objectForKey:@"TanggalAngsuran"];
        flagLimit = [data objectForKey:@"FlagLimit"];
        maritalStatus = [data objectForKey:@"Marital_Status"];
        referenceCode = [data objectForKey:@"ReferenceCode"];
        email = [data objectForKey:@"Email"];
        tanggalAngsuran = [data objectForKey:@"TanggalAngsuran"];
        cellularNo = [data objectForKey:@"CelullerNo"];
        statusMessage = [data objectForKey:@"StatusMessage"];
        agreements = [data objectForKey:@"Agreement"];
        akadNO = [data valueForKey:@"AkadNo"];
        
        listAngsuran = [[[[data objectForKey:@"ListAngsuran"]componentsSeparatedByString:@","]reverseObjectEnumerator]allObjects];
        merchantTenor = [[[[data objectForKey:@"MerchantTenor"]componentsSeparatedByString:@","]reverseObjectEnumerator]allObjects];
        periodList = [[[[data objectForKey:@"ListPeriode"] componentsSeparatedByString:@","]reverseObjectEnumerator]allObjects];
        jatuhTempoList = [[[[data objectForKey:@"ListJatuhTempo"] componentsSeparatedByString:@","]reverseObjectEnumerator]allObjects];

        
    }else{
        NSString *msg = @"";
        if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
            msg = @"Data tidak ditemukan";
        }else{
            msg = @"Data not found";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"data not found" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
    }
    
    [dataManager.dataExtra setValue:akadNO forKey:@"akad_no"];

    /// put data
    self.lblInvoiceAmount.text = [NSString stringWithFormat:@"Rp. %@,-", [Utility formatCurrencyValue:jumlahTagihan]];
    self.lblPenerima.text = merchantName;
    
    if([language isEqualToString:@"id"]){
        titleInvoiceCicil = @"Bank akan bayar tagihan anda";
        titleInvoiceTunai = @"Anda akan bayar tunai tagihan";
        titlePenerimaCicil = @"Atas tagihan dari :";
        titlePenerimaTunai = @"Dari :";
        titleSelection = @"Pilih Cicilan Bulanan";
        titleSelected = @"Anda bayar cicil tagihan diatas ke Bank";
        ujrohDesc = [NSString stringWithFormat:@"(sudah termasuk ujrah sebesar Rp.%@,-/bulan)",[Utility formatCurrencyValue:ujrohPaylater]];
        
        [self.btnCicil setTitle:@"Cicil Saja" forState:UIControlStateNormal];
        [self.btnPurchase setTitle:@"Bayar Lunas" forState:UIControlStateNormal];
    }else{
        titleInvoiceCicil = @"Bank will pay your bill";
        titleInvoiceTunai = @"You will pay the bill";
        titlePenerimaCicil = @"Invoice from :";
        titlePenerimaTunai = @"From :";
        titleSelection = @"Monthly Installment Option :";
        titleSelected = @"You'll apply Paylater by Choose the installment ";
        ujrohDesc = [NSString stringWithFormat:@"(include ujrah fee Rp.%@,-/month)",[Utility formatCurrencyValue:ujrohPaylater]];
        
        [self.btnCicil setTitle:@"Pay by Installment" forState:UIControlStateNormal];
        [self.btnPurchase setTitle:@"Cash" forState:UIControlStateNormal];
    }
    
    NSString *str = [NSString stringWithFormat:@"<p style='color:red;font-family:HelveticaNeue'>%@</p>",agreements];
    
//    agreements = @"<p style='color:red;font-family:HelveticaNeue'>Saya mengajukan dan telah membaca, memahami, serta menyetujui Informasi Produk Paylater, Akad Hawalah, dan Pernyataan Nasabah <b><font color='red'>(wajib di klik)</font></b></p>";
    [self.lblCheck setAttributedText:
            [[NSAttributedString alloc]
                      initWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                           options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                documentAttributes: nil
                             error: nil]];
//    self.lblCheck setFont:<#(UIFont * _Nullable)#>
//    [self.lblCheck setText:agreements];
    
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url =[NSString stringWithFormat:@"%@,loan_amount,loan_tenor,ujrah,installment_amount,due_date,akad_no",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAngsuran.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PL01Cell *cell = (PL01Cell*)[tableView dequeueReusableCellWithIdentifier:@"PL01CELL"];
    
//    if(!payLaterState){
//        cell.label1.textColor = UIColorFromRGB(bsm_gray_color);
//        cell.label2.textColor = UIColorFromRGB(bsm_gray_color);
//    }
//    else{
//        cell.label1.textColor = [UIColor blackColor];
//        cell.label2.textColor = [UIColor blackColor];
//    }
    
    NSString *label1 = [NSString stringWithFormat:@"%@x Rp. %@,-",merchantTenor[indexPath.row], [Utility formatCurrencyValue:[listAngsuran[indexPath.row]doubleValue]]];
    NSString *label2 = @"";
    
    
//    NSInteger totalRow = [tableView numberOfRowsInSection:indexPath.section];
    if([merchantTenor[indexPath.row]doubleValue] > 1){
        if([language isEqualToString:@"id"]){
//            label2 = [NSString stringWithFormat:@"Tiap Tanggal %@, %@", tanggalAngsuran, periodList[indexPath.row]];
            label2 = [NSString stringWithFormat:@"Tiap Tanggal %@", periodList[indexPath.row]];
        }else{
//            label2 = [NSString stringWithFormat:@"from %@, %@", tanggalAngsuran, periodList[indexPath.row]];
            label2 = [NSString stringWithFormat:@"from %@", periodList[indexPath.row]];
        }
    }else{
        if([language isEqualToString:@"id"]){
//            label2 = [NSString stringWithFormat:@"Tanggal %@, %@", tanggalAngsuran, periodList[indexPath.row]];
            label2 = [NSString stringWithFormat:@"Tanggal %@", periodList[indexPath.row]];
        }else{
//            label2 = [NSString stringWithFormat:@"Due Date %@, %@", tanggalAngsuran, periodList[indexPath.row]];
            label2 = [NSString stringWithFormat:@"Due Date %@", periodList[indexPath.row]];
        }
    }
    
    cell.label1.text = label1;
    cell.label2.text = label2;
    
    [cell layoutSubviews];
    [cell layoutIfNeeded];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(selectedRow == indexPath.row){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self.lblCheck setUserInteractionEnabled:NO];
        [self.lblCheck setHidden:YES];
        [self.checkListImage setHidden:YES];
        [self enablePaylater:NO];
        selectedRow = -1;
    }else{
        
        [self enablePaylater:YES];
        
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%.f",jumlahTagihan] forKey:@"loan_amount"];
        [dataManager.dataExtra setValue:merchantTenor[indexPath.row] forKey:@"loan_tenor"];
        [dataManager.dataExtra setValue:listAngsuran[indexPath.row] forKey:@"installment_amount"];
        [dataManager.dataExtra setValue:jatuhTempoList[indexPath.row] forKey:@"due_date"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%.f",ujrohPaylater] forKey:@"ujrah"];

        [dataManager.dataExtra setValue:referenceCode forKey:@"Reference_Code"];
        
        
        [self.lblTitlePeriod setText:[NSString stringWithFormat:@"%@\n%@",titleSelected, ujrohDesc]];
        
        [self.lblCheck setUserInteractionEnabled:YES];
        [self.lblCheck setHidden:NO];
        [self.checkListImage setHidden:NO];
        selectedRow = indexPath.row;
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"paylater"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            akadText = [object objectForKey:@"Akad_Text"];
            [self actionOpenAkad:akadText];
            
            [dataManager.dataExtra setValue:[object objectForKey:@"LoanAmount"] forKey:@"loan_amount"];
            [dataManager.dataExtra setValue:[object objectForKey:@"LoanTenor"] forKey:@"loan_tenor"];
            [dataManager.dataExtra setValue:[object objectForKey:@"BranchCode"] forKey:@"branch_code"];
//            [dataManager.dataExtra setValue:[object objectForKey:@"Akad_No"] forKey:@"akad_no"];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self backToR];
            }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}



- (void) enablePaylater:(BOOL) state{
    UIColor *enableColorText = [UIColor blackColor];
    UIColor *disableColorText = UIColorFromRGB(const_color_lightgray);
    payLaterState = state;

    if(state){
        self.labelTitleInvoice.textColor = enableColorText;
        self.lblPenerima.textColor = enableColorText;
        
        self.lblInvoiceAmount.textColor = UIColorFromRGB(0x0077be);
        
        self.lblTitleRincian.textColor = enableColorText;
        self.lblCheck.textColor = enableColorText;
        self.lblFeeUjroh.textColor = enableColorText;
        self.lblTitlePeriod.textColor = enableColorText;
        
        [self.btnCicil setColorSet:TERTIARYCOLORSET];
        
        
        if([language isEqualToString:@"id"]){
            [self.btnCicil setTitle:@"Cicil Saja" forState:UIControlStateNormal];
        }else{
            [self.btnCicil setTitle:@"Pay by Installment" forState:UIControlStateNormal];
        }
        
        UITapGestureRecognizer *tapCheck =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(tapCheck)];
        [self.checkListImage addGestureRecognizer:tapCheck];
        checkState = YES;
        [self tapCheck];
        [self.checkListImage setUserInteractionEnabled:NO];
        [self.lblCheck setUserInteractionEnabled:NO];
        [self enablePurchase:NO];
        
        [self.labelTitleInvoice setText:titleInvoiceCicil];
        [self.lblTitleRincian setText:titlePenerimaCicil];
        
    }else{
        [self.labelTitleInvoice setText:titleInvoiceTunai];
        [self.lblTitleRincian setText:titlePenerimaTunai];
        [self.lblTitlePeriod setText:[NSString stringWithFormat:@"%@\n%@",titleSelection, ujrohDesc]];
        
        self.lblInvoiceAmount.textColor = UIColorFromRGB(0XCC8080);
        self.lblCheck.textColor = disableColorText;
        self.lblFeeUjroh.textColor = disableColorText;
        self.lblTitlePeriod.textColor = disableColorText;
        
        [self.btnCicil setColorSet:TERTIARYCOLORSET];

        if([language isEqualToString:@"id"]){
            [self.btnCicil setTitle:@"Cicil Saja" forState:UIControlStateNormal];
        }else{
            [self.btnCicil setTitle:@"Pay by Installment" forState:UIControlStateNormal];
        }
        
        [self.tableViewPeriod reloadData];
        [self.checkListImage setUserInteractionEnabled:NO];
        [self.lblCheck setUserInteractionEnabled:NO];

        [self enablePurchase:YES];
        
        checkState = YES;
        [self tapCheck];
        [self.btnCicil setEnabled:YES];
    }
}

- (void) enablePurchase : (BOOL) state{
    
    if(state){
        self.btnPurchase.backgroundColor = UIColorFromRGB(const_color_primary);
    }else{
        self.btnPurchase.backgroundColor = UIColorFromRGB(const_color_lightgray);
    }
}

- (void) tapCheck{
    if(!checkState){
        checkState = YES;
        [self.checkListImage setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        [self.btnCicil setColorSet:TERTIARYCOLORSET];

    }else{
        checkState = NO;
        [self.checkListImage setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        [self.btnCicil setColorSet:TERTIARYCOLORSET];

    }
}

- (void) actionBtnCicil{
    if(payLaterState){
        //Paylater GO
        if(checkState){
            [dataManager.dataExtra setValue:akadText forKey:@"pl_akad"];
            
            [self openNextTemplate];
        }else{
            NSString *msg = @"";
            if([Utility isLanguageID]){
                msg = @"Harap setujui informasi produk, akad dan pernyataan berikut";
            }else{
                msg = @"Please Agree to the following product information, contract and statement";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        [self enablePaylater:YES];
        
        NSString *msg = @"";
        if([Utility isLanguageID]){
            msg = @"Harap pilih tenor terlebih dahulu";
        }else{
            msg = @"Please select the tenor first";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void) actionBtnPurchase{
    if(payLaterState){
        [self enablePaylater:NO];
    }else{
        //Purchase GO
//        dataManager.currentPosition = [self.jsonData count];
        NSDictionary *dict = [dataManager getArrayofActionMenuID:[self.jsonData objectForKey:@"menu_id"] andList:dataManager.listMenu];
        NSLog(@"%lu", [[dict objectForKey:@"node"]count]-1);
        
        dataManager.currentPosition = (int)[[dict objectForKey:@"node"]count]-2;
        [dataManager.dataExtra setValue:@"1" forKey:@"pending_payment"];
        [self openNextTemplate];
    }
}

- (void) requestAkad{
    [self doRequest];
}

- (void) actionOpenAkad : (NSString*)akadText{
    PLAKADViewController *openAkad = [self.storyboard instantiateViewControllerWithIdentifier:@"PLAKAD"];
    [openAkad setAkadText:akadText];
    [openAkad setDelegate:self];
    [self presentViewController:openAkad animated:YES completion:nil];
    
    [self.checkListImage setUserInteractionEnabled:YES];
}

- (void)clickedButtonAgreement:(BOOL)state{
    if(state){
        [self.checkListImage setUserInteractionEnabled:YES];
        [self tapCheck];
//        [self.btnCicil setEnabled:YES];
    }else{
        [self.checkListImage setUserInteractionEnabled:NO];
    }
}

- (void)setPassingData:(NSDictionary *)object{
    passingData = object;
}


@end
