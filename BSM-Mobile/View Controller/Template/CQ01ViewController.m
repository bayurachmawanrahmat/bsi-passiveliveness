//
//  CQ02ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 20/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CQ01ViewController.h"
#import "CalendarQurbanViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface CQ01ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, CalendarQurbanDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *toolbarTitle;
    UIToolbar *toolbar;
    
    NSArray *listFrekuensi, *listDate;
    UIPickerView *frekuensiPickerView, *dateStartPickerView;
    
    UIDatePicker *datePickerStart;
    
    NSDictionary *data;
    NSArray *dateQurbanList, *arrIntervalFreq;

    NSNumberFormatter *currencyFormatter;
    
    NSDate *targetDate;
    
    double fundTargted;
    double priceUp, priceDown;
}
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
//@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
//@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@property (weak, nonatomic) IBOutlet UILabel *labelContentTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelContentText;

@property (weak, nonatomic) IBOutlet UILabel *labelTarget;
@property (weak, nonatomic) IBOutlet UILabel *labelQurbanPrice;
@property (weak, nonatomic) IBOutlet UITextField *tfTarget;
@property (weak, nonatomic) IBOutlet UILabel *labelTargetDate;
@property (weak, nonatomic) IBOutlet UITextField *tfTargetDate;
@property (weak, nonatomic) IBOutlet UILabel *labelFrekuensi;
@property (weak, nonatomic) IBOutlet UITextField *tfFrekuensi;
@property (weak, nonatomic) IBOutlet UILabel *labelDateStart;
@property (weak, nonatomic) IBOutlet UITextField *tfDateStart;
@property (weak, nonatomic) IBOutlet UILabel *labelSetoran;
@property (weak, nonatomic) IBOutlet UITextField *tfSetoran;
@property (weak, nonatomic) IBOutlet UIView *vwForFrekuensi;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vwFrekuensiHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblInsideFrekuensi;
@property (weak, nonatomic) IBOutlet UITextField *tfInsideFrekuensi;

@end

@implementation CQ01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    listDate = [[NSArray alloc]init];
    currencyFormatter = [[NSNumberFormatter alloc]init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [self.labelTitle setText:[self.jsonData objectForKey:@"title"]];
    
    [self.iconTitle setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    
    [self setRequest];
    
    if([language isEqualToString:@"id"]){
        
        [self.labelContentTitle setText:@"Berencana Qurban ?"];
        [self.labelContentText setText:@"Bank Syariah Indonesia memudahkan anda untuk disiplin menyisihkan tabungan Anda dengan Tabungan Autosave, sehingga rencana kurban anda dapat terwujud tepat waktu"];
        
        [_labelTargetDate setText:@"Target waktu dana terkumpul"];
        [_tfTargetDate setPlaceholder:@"Pilih tanggal"];
        
        [_labelFrekuensi setText:@"Frekuensi setoran"];
        [_tfFrekuensi setPlaceholder:@"Pilih frekuensi"];
        
        [_labelDateStart setText:@"Tanggal mulai"];
        [_tfDateStart setPlaceholder:@"Pilih tanggal"];
        
        [_labelSetoran setText:@"Jumlah setoran rutin per bulan/minggu"];
        [_tfSetoran setPlaceholder:@"Rp."];
        
        toolbarTitle = @"Selesai";
        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];

    }else{
        
        [self.labelContentTitle setText:@"Plan to Qurban ?"];
        [self.labelContentText setText:@"Bank Syariah Indonesia makes easier for you to discipline your savings. So that your dreams to do Qurban can be realized on time"];
        
        [_labelTargetDate setText:@"Target time for fund to be collected"];
        [_tfTargetDate setPlaceholder:@"Select date"];
        
        [_labelFrekuensi setText:@"Frequency of deposits"];
        [_tfFrekuensi setPlaceholder:@"Select frequency"];
        
        [_labelDateStart setText:@"Start date"];
        [_tfDateStart setPlaceholder:@"Select date"];
        
        [_labelSetoran setText:@"Amount of deposits per month/week"];
        [_tfSetoran setPlaceholder:@"Rp."];
        
        toolbarTitle = @"Done";
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
    }
    
    [Styles setTopConstant:_topConstraint];
    
    [Styles setStyleTextFieldBottomLine:_tfTarget];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfTargetDate imageNamed:@"icon-calendar.png"];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfFrekuensi imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfDateStart imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfInsideFrekuensi imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBottomLine:_tfSetoran];
    
//    [Styles setStyleButton:_btnNext];
//    [Styles setStyleButton:_btnCancel];
    
    [self createToolbar];
    
    self.tfTargetDate.delegate = self;
    self.tfDateStart.delegate = self;
    
    if([language isEqualToString:@"id"]){
        listFrekuensi = @[@"Setiap minggu", @"Setiap bulan"];
    }else{
        listFrekuensi = @[@"Weekly", @"Monthly"];
    }
    
    frekuensiPickerView = [[UIPickerView alloc]initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    frekuensiPickerView.showsSelectionIndicator = YES;
    frekuensiPickerView.delegate = self;
    frekuensiPickerView.dataSource = self;
    frekuensiPickerView.tag = 1;
    
    dateStartPickerView = [[UIPickerView alloc]initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    dateStartPickerView.showsSelectionIndicator = YES;
    dateStartPickerView.delegate = self;
    dateStartPickerView.dataSource = self;
    dateStartPickerView.tag = 2;
    
    self.tfTarget.delegate = self;
    self.tfTarget.inputAccessoryView = toolbar;
    self.tfTarget.keyboardType = UIKeyboardTypeNumberPad;
    
    self.tfFrekuensi.delegate = self;
    self.tfFrekuensi.inputView = frekuensiPickerView;
    self.tfFrekuensi.inputAccessoryView = toolbar;
    
    self.tfDateStart.delegate = self;
    self.tfDateStart.inputView = dateStartPickerView;
    self.tfDateStart.inputAccessoryView = toolbar;
    self.tfDateStart.enabled = NO;
    
    self.tfSetoran.enabled = NO;
    
    self.vwFrekuensiHeight.constant = 0;
    [self.vwForFrekuensi setHidden:YES];
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [self.btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) setRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url =[NSString stringWithFormat:@"%@,hewan_qurban,id,language",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (void) createToolbar{
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:toolbarTitle style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == self.tfTargetDate){
        CalendarQurbanViewController *qurban = [self.storyboard instantiateViewControllerWithIdentifier:@"CALENDARQURBAN"];
        [qurban setDelegate:self];
        [qurban setDateList:dateQurbanList];
        [self presentViewController:qurban animated:YES completion:^(){
            [self.view endEditing:YES];
            [self.tfTargetDate resignFirstResponder];
        }];
//        [self.tfFrekuensi becomeFirstResponder];
    }
    
    if(textField == self.tfFrekuensi && [self.tfTargetDate.text isEqualToString:@""]){
        UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"anda belum mengisi target date" preferredStyle:UIAlertControllerStyleAlert];
        [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
            [self.tfFrekuensi resignFirstResponder];
            [self->frekuensiPickerView resignFirstResponder];
            [self.tfTargetDate becomeFirstResponder];
        }]];
        [self presentViewController:alerts animated:YES completion:nil];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == _tfTarget){
        _tfSetoran.text = @"";
        _tfFrekuensi.text = @"";
        _tfTargetDate.text = @"";
        _tfInsideFrekuensi.text = @"";
        _tfDateStart.text = @"";
    }
    
//    double newValue = [[textField.text stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""]doubleValue];
//
//    if(textField == self.tfTarget){
//        if(newValue > priceUp){
//            NSString *msg = @"";
//            if ([language isEqualToString:@"id"]) {
//                msg = @"Nilai target melebihi batas maximum";
//            }else{
//                msg = @"Targeted funds the limit offered";
//            }
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                textField.text = [self->currencyFormatter stringFromNumber:[NSNumber numberWithDouble:self->priceUp]];
//            }]];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//        if(newValue < priceDown){
//            NSString *msg = @"";
//            if ([language isEqualToString:@"id"]) {
//                msg = @"Nilai target kurang dari batas minimum";
//            }else{
//                msg = @"Targeted funds is less than minimum limit";
//            }
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                textField.text = [self->currencyFormatter stringFromNumber:[NSNumber numberWithDouble:self->priceDown]];
//            }]];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    if(pickerView.tag == 1){
        return listFrekuensi[row];
    }else {
        if(listDate.count != 0){
            NSString *str = [NSString stringWithFormat:@"%@",[self formatterNSDate: listDate[listDate.count - (row+1)]]];
            return str;
        }else{
            return @"not found";
        }
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == 1){
        self.tfFrekuensi.text = listFrekuensi[row];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", row+1] forKey:@"frekuensi"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", row+1] forKey:@"recurrence_type"];
        
        [dataManager.dataExtra setValue:@"0" forKey:@"trxtype"];
        NSInteger days, months, years;
                
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents * dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: [NSDate date] toDate:targetDate options:0];

    //        components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit
    //                fromDate: [NSDate date] toDate: targetedDate options: 0];
        days = [dateComponents day];
        months = [dateComponents month];
        years = [dateComponents year];
        BOOL isOK = YES;
        if(row+1 == 1){
            if(days < 7 && years < 1 & months < 1){
                NSString *msg = @"";
                if([language isEqualToString:@"id"]){
                    msg = @"Target waktu minimum 7 hari dari hari ini";
                }else{
                    msg = @"Time target minimum 7 days from today";
                }
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self.tfTargetDate setText:@""];
                    [self.tfFrekuensi setText:@""];
                    [self.view endEditing:YES];
                    [self.tfFrekuensi resignFirstResponder];
                    [self.tfTargetDate becomeFirstResponder];
                }]];
                isOK = NO;
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            if(months < 1 && years < 1){
                NSString *msg = @"";
                if([language isEqualToString:@"id"]){
                    msg = @"Target waktu minimum 1 bulan dari hari ini";
                }else{
                    msg = @"Time target minimum 1 month from today";
                }
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self.tfTargetDate setText:@""];
                    [self.tfFrekuensi setText:@""];
                    [self.view endEditing:YES];
                    [self.tfFrekuensi resignFirstResponder];
                    [self.tfTargetDate becomeFirstResponder];
                }]];
                isOK = NO;
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        if(isOK){
            [self createDateList:targetDate withType:(int)row+1];
            if(targetDate != nil){
                [self.tfDateStart setEnabled:YES];
            }
            [self.tfDateStart setText:@""];
        }
    }else{
        if(listDate.count != 0){
            NSDate *dateStart = listDate[listDate.count - (row+1)];
            NSString *str = [NSString stringWithFormat:@"%@",[self formatterNSDate: dateStart]];
            self.tfDateStart.text = str;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//            [dateFormatter setDateFormat:@"dd-MM-yyyy"];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];

            NSString *dateStartToSent = [dateFormatter stringFromDate:dateStart];
            [dataManager.dataExtra setValue:dateStartToSent forKey:@"target_mulai"];
            
//            double debit = [[data objectForKey:@"harga"]doubleValue]/(listDate.count - row);
            double debit = [[self.tfTarget.text stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue]/(listDate.count - row);
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setMaximumFractionDigits:0];
            [formatter setRoundingMode: NSNumberFormatterRoundUp];
            NSString *debitString = [formatter stringFromNumber:[NSNumber numberWithDouble:debit]];
            
            [dataManager.dataExtra setValue:debitString forKey:@"setoran"];
            
            self.tfSetoran.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[debitString doubleValue]]];
        }
    }
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView.tag == 1){
        return listFrekuensi.count;
    }else{
        if([[dataManager.dataExtra valueForKey:@"frekuensi"] isEqualToString:@"1"]){
            if(listDate.count > 4){
                return 4;
            }else{
                return listDate.count;
            }
        }else{
            if(listDate.count > 2){
                return 2;
            }else{
                return listDate.count;
            }
        }
    }
}


- (BOOL) validate{
    NSString *msg = @"";
    
//    if(self.tfTarget.text dou)
    
    if([self.tfTarget.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            msg = @"Anda belum mengisi target dana terkumpul";
        }else{
            msg = @"Please fill collected fund amount target";
        }
    }else if([self.tfTargetDate.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            msg = @"Anda belum mengisi target waktu dana terkumpul";
        }else{
            msg = @"Please fill collected fund time target";
        }
    }else if([self.tfFrekuensi.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            msg = @"Pilih frekuensi pengumpulan dana terlebih dulu";
        }else{
            msg = @"Select collecting fund frequency";
        }
    }else if([self.tfDateStart.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            msg = @"Pilih tanggal pengumpulan dana dimulai";
        }else{
            msg = @"Please select collecting fund date start";
        }
    }
    
    if(![msg isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (void)actionNext{
    if([self validate]){
        
//        [dataManager.dataExtra setValue:[self.tfSetoran.text stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"setoran"];
//        [dataManager.dataExtra setValue:self.tfDateStart.text forKey:@"target_mulai"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        //    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *dateConverted = [dateFormatter dateFromString:self.tfTargetDate.text];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:dateConverted];
            
        [dataManager.dataExtra setValue:dateString forKey:@"target_waktu"];
//        [dataManager.dataExtra setValue:[data objectForKey:@"harga"] forKey:@"target_harga"];
//        fundTargted = [[data objectForKey:@"harga"]doubleValue];
        [dataManager.dataExtra setValue:[self.tfTarget.text stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"target_harga"];

        //sementara
//        [dataManager.dataExtra setValue:@"6001" forKey:@"type_autosave"];
        [self openNextTemplate];
    }
}

- (void)actionCancel{
    [self backToR];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([requestType isEqualToString:@"ref_qurban"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSArray *listD = (NSArray *)[jsonObject valueForKey:@"response"];
            data = listD[0];
            if(data != nil){
                if([language isEqualToString:@"id"]){
                    [self.labelTarget setText:[NSString stringWithFormat:@"Target harga qurban (%@) :",[data objectForKey:@"label_id"]]];
                }else{
                    [self.labelTarget setText:[NSString stringWithFormat:@"Qurban price target (%@) :",[data objectForKey:@"label_id"]]];
                }

//                [self.tfTarget setText: [NSString stringWithFormat:@"Rp. %@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[data objectForKey:@"harga"] doubleValue]]]]];
                [self.tfTarget setText: [NSString stringWithFormat:@"%@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[data objectForKey:@"harga"] doubleValue]]]]];
                
                priceUp = [[data objectForKey:@"harga"]doubleValue];
                priceDown = [[data objectForKey:@"harga_terendah"]doubleValue];
                
//                self.labelQurbanPrice.text = [NSString stringWithFormat:@"Rp. %@ - Rp. %@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:priceDown]], [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:priceUp]]];
////                self.labelQurbanPrice.textColor = UIColorFromRGB(bsm_green_color);
//                self.labelQurbanPrice.textColor = UIColorFromRGB(bsm_yellow_color);
                self.labelQurbanPrice.text = @"";
                
                [self.tfTarget setEnabled:YES];
                
                NSArray *dateQurban = [[data objectForKey:@"date_qurban"] componentsSeparatedByString:@"|"];
                dateQurbanList = dateQurban;
            }
        }else{
            NSString * msg = [jsonObject valueForKey:@"response"];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)selectedDate:(NSString *)date{
    [self.tfTargetDate setText:date];
    [self.tfFrekuensi setText:@""];
    [self.tfSetoran setText:@""];
    [self.tfDateStart setText:@""];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateConverted = [dateFormatter dateFromString:date];
    
    targetDate = dateConverted;
    
    [self createDateList:targetDate withType:1];
}

- (NSString *) formatterNSDate : (NSDate*) date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE dd-MM-yyyy"];
    if([language isEqualToString:@"id"]){
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"id_ID"]];
    }else{
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_EN"]];
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (void) createDateList : (NSDate *)date withType:(int)type{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
    
    if(type == 1){
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld",(long)dateComponents.weekday] forKey:@"dayofweek"];
        [dataManager.dataExtra setValue:@"" forKey:@"dayofmonth"];

    }else{
        if(dateComponents.day > 28){
            [dateComponents setDay:28];
            date = [calendar dateFromComponents:dateComponents];
        }
        [dataManager.dataExtra setValue:@"" forKey:@"dayofweek"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld",(long)dateComponents.day] forKey:@"dayofmonth"];
    }
    
    NSDate *dateToShow = date;

    NSMutableArray *listDates = [[NSMutableArray alloc]init];
    if([dateToShow compare:[NSDate date]] == NSOrderedDescending){
        while([dateToShow compare:[NSDate date]] == NSOrderedDescending){
            [listDates addObject:dateToShow];
            if(type == 1){
                [dateComponents setDay:(dateComponents.day - 7)];
                
            }else{
                NSInteger day = dateComponents.day;
                [dateComponents setMonth:(dateComponents.month - 1)];
                [dateComponents setDay:day];
            }
            dateToShow  = [calendar dateFromComponents:dateComponents];
            dateComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth fromDate: dateToShow];
        }
    }else{
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Tanggal target dibawah tanggal hari ini";
        }else{
            msg = @"Time target below today's date";
        }
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
            [self.tfTargetDate setText:@""];
            [self.tfFrekuensi setText:@""];
            [self.view endEditing:YES];
            [self.tfTargetDate becomeFirstResponder];
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    listDate = listDates;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.tfTarget){
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
         double newValue = [newText doubleValue];
        
        fundTargted = newValue;
        
        
        if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
        }
        else if ([newText length] > currencyFormatter.maximumSignificantDigits){
             textField.text = textField.text;
        }
        else{
             textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
//            if(newValue > priceUp){
//                NSString *msg = @"";
//                if ([language isEqualToString:@"id"]) {
//                    msg = @"Nilai target melebihi batas maximum";
//                }else{
//                    msg = @"Targeted funds the limit offered";
//                }
//                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
//                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                    textField.text = [self->currencyFormatter stringFromNumber:[NSNumber numberWithDouble:self->priceUp]];
//                }]];
//                [self presentViewController:alert animated:YES completion:nil];
//            }
        }
            
         return NO;
    }
    return YES;
}

@end
