//
//  PBYIP01ViewController.m
//  BSM-Mobile
//
//  Created by ALikhsan on 8/16/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PBYIP01ViewController.h"
#import "Connection.h"
#import "Utility.h"
#import "JVFloatLabeledTextField.h"
#import "Styles.h"
#import "PopupIsiPasanganViewController.h"
#import "UIView+Info.h"
#import "PopUnggahObjFotoViewController.h"

@interface PBYIP01ViewController ()<ConnectionDelegate, UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIAlertViewDelegate, DataPasanganDelegate, UnggahObjectDelegate>{
    NSUserDefaults *userDefaults;
    NSString *lang;
    
    UIPickerView *pickLoanView;
    UIPickerView *pickMaskapaiView;
    
    UIToolbar *toolBar, *toolbarMaskapai;
    
    NSArray *listAssetType, *listPeriod, *listInsurance;
    
    NSMutableArray *listDataAddAsset;
    
    NSNumberFormatter* currencyFormatter;
    int mTag;
    
    UILabel *lblIsiDataPas;
    
    NSInteger idxChoiceAsset;
    NSInteger nominalChoiceAsset;
    NSMutableArray *arDataChoiceAsset;
    NSMutableArray *lastTag;
    BOOL isEdited;
    
    double nMaxDana, nTotalAmnt, minLoan, nRateTasaksi;
    NSInteger nIdxTenor, nIdxAsuransi;
    
    NSString *nMaxKesepakatan, *textDataPasangan;
    
    bool stateFocus, martialState, maritalStateFilled;
    NSInteger editCurrentTag;
    
    int nMartialState;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIImageView *imgStepIndicator;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;

@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;


@property (weak, nonatomic) IBOutlet UIView *vwContent1;
@property (weak, nonatomic) IBOutlet UIView *vwContent2;
@property (weak, nonatomic) IBOutlet UIView *vwContent3;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleMin;
@property (weak, nonatomic) IBOutlet UIView *vwTambahBarang;
@property (weak, nonatomic) IBOutlet UILabel *lblNominalTotal;
@property (weak, nonatomic) IBOutlet UIView *vwDataPasangan;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleCntn1;
@property (weak, nonatomic) IBOutlet UITextField *vwEdCntn1;
@property (weak, nonatomic) IBOutlet UIView *vwLineCntn1;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCntn2;
@property (weak, nonatomic) IBOutlet UIView *vwLineCntn2;
@property (weak, nonatomic) IBOutlet UITextField *edDropDownWaktu;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCntn3;
@property (weak, nonatomic) IBOutlet UIView *vwLineCntn3;
@property (weak, nonatomic) IBOutlet UITextField *edDropDownMaskapai;

@property (weak, nonatomic) IBOutlet UIButton *btnExtendView;
@property (weak, nonatomic) IBOutlet UIButton *btnEditView;
@property (weak, nonatomic) IBOutlet UILabel *lblBarang;
@property (weak, nonatomic) IBOutlet UIView *vwAkadAgreement;
@property (weak, nonatomic) IBOutlet UIButton *btnChecked;
@property (weak, nonatomic) IBOutlet UILabel *lblChecked;
@property (weak, nonatomic) IBOutlet UIButton *btnBaseChecked;

- (IBAction)actionTambah:(id)sender;
- (IBAction)actionEdit:(id)sender;
- (IBAction)actionBatal:(id)sender;
- (IBAction)actionNext:(id)sender;

@end

@implementation PBYIP01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    editCurrentTag = 0;
    listAssetType = [[NSArray alloc]init];
    listPeriod = [[NSArray alloc]init];
    listInsurance = [[NSArray alloc]init];
    
    listDataAddAsset = [[NSMutableArray alloc]init];
    
    idxChoiceAsset = 0;
    nominalChoiceAsset = 0;
    isEdited = false;
    stateFocus = false;
    maritalStateFilled = false;
    nMaxDana = 0;
    nTotalAmnt = 0;
    
    arDataChoiceAsset = [[NSMutableArray alloc]init];
    lastTag = [[NSMutableArray alloc]init];
    
    mTag = 1;
    
    [lastTag addObject:@(mTag)];
    
    self.vwTambahBarang.tag = mTag;
    self.lblBarang.tag = mTag + 100;
    self.btnExtendView.accessibilityLabel = [NSString stringWithFormat:@"ext_%d", mTag];
    self.btnEditView.accessibilityLabel =[NSString stringWithFormat:@"edt_%d", mTag];
    
    [self.btnExtendView addTarget:self action:@selector(actionExpandView:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnEditView addTarget:self action:@selector(actionEditView:) forControlEvents:UIControlEventTouchUpInside];
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    lang = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *ttlToolbar;
    
    if ([lang isEqualToString:@"id"]) {
        [self.lblTitleCntn1 setText:@"Nominal Pengajuan"];
        [self.lblTitleCntn2 setText:@"Jangka Waktu Pembiayaan"];
        [self.lblTitleCntn3 setText:@"Pilih Maskapai Asuransi"];
        [self.lblBarang setText:@"Object Refinancing"];

        [self.lblTitleMin setText:@"Object Refinancing / Assets yang anda miliki dengan total nilai minimal IDR.0"];
        ttlToolbar = @"Selesai";
        [self.btnBatal setTitle:@"Kembali" forState:UIControlStateNormal];
        [self.btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        
        _vwEdCntn1.placeholder = @"Maksimal IDR.50,000,000";
        _edDropDownWaktu.placeholder =  @"Maksimal 5 Tahun";
        _edDropDownMaskapai.placeholder = @"";
        
        [self.lblChecked setText:@"Object Refinancing / asset yang saya isi adalah halal dan benar milik saya/pasangan"];
        
    }else{
        [self.lblTitleCntn1 setText:@"Financing Amount"];
        [self.lblTitleCntn2 setText:@"Tenor (years)"];
        [self.lblTitleCntn3 setText:@"Select Insurance"];
        [self.lblBarang setText:@"Object Refinancing"];
        [self.lblTitleMin setText:@"Refinancing Object / Assets Owned with a minimum total value IDR.0"];

        
        _vwEdCntn1.placeholder = @"Maximum IDR.50,000,000";
        _edDropDownWaktu.placeholder =  @"Maximum 5 Year";
        _edDropDownMaskapai.placeholder = @"";
        ttlToolbar = @"Done";
        
        [self.btnBatal setTitle:@"Back" forState:UIControlStateNormal];
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
        
        [self.lblChecked setText:@"Refinancing Object / Assets Owned that i have filled in this application are halal and belong to me / spouse"];
    }
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.lblChecked.numberOfLines = 2;
    [self.lblChecked sizeToFit];
    
    [self.btnBaseChecked addTarget:self action:@selector(actionAgreeAkad:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnChecked setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
    [self.btnChecked setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    
    [_btnNext setEnabled:false];
//    [_btnNext setBackgroundColor:[UIColor grayColor]];
    
    [self setupLayout];
    //[self.vwEdCntn1 addSubview:[self floatEditText]];
    
    
    pickLoanView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    pickLoanView.showsSelectionIndicator = YES;
    pickLoanView.delegate = self;
    pickLoanView.dataSource = self;
    pickLoanView.tag = 1;
    
    pickMaskapaiView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    pickMaskapaiView.showsSelectionIndicator = YES;
    pickMaskapaiView.delegate = self;
    pickMaskapaiView.dataSource = self;
    pickMaskapaiView.tag = 2;
    
    self.vwEdCntn1.tag = 1;
    self.vwEdCntn1.delegate = self;
    
    [self.btnExtendView setTitle:@"+" forState:UIControlStateNormal];
    [self.btnExtendView setTitle:@"x" forState:UIControlStateSelected];
    [self.btnExtendView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnExtendView setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
  
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                     [[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    toolbarMaskapai = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbarMaskapai.barStyle = UIBarStyleDefault;
    
    UILabel *lblHeaderMaskpai = [[UILabel alloc] initWithFrame:CGRectMake(0.0 , 11.0f, self.view.frame.size.width, 41.0f)];
    [lblHeaderMaskpai setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
    [lblHeaderMaskpai setNumberOfLines:2];
    [lblHeaderMaskpai setBackgroundColor:[UIColor clearColor]];
    [lblHeaderMaskpai setTextColor:[UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0]];
    [lblHeaderMaskpai setTextAlignment:NSTextAlignmentLeft];
    if([lang isEqualToString:@"id"]){
        [lblHeaderMaskpai setText:@"Bank dapat merubah pilihan asuransi dalam rangka penyebaran resiko"];
    }else{
        [lblHeaderMaskpai setText:@"Banks can change insurance options in order to spread risk"];
    }
    
    toolbarMaskapai.items = @[
                                [[UIBarButtonItem alloc]initWithCustomView:lblHeaderMaskpai],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:ttlToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]
                            ];
    
    
    self.vwEdCntn1.inputAccessoryView = toolBar;
    
    self.edDropDownWaktu.inputView = pickLoanView;
    self.edDropDownWaktu.inputAccessoryView = toolBar;
    
    self.edDropDownMaskapai.inputView = pickMaskapaiView;
    self.edDropDownMaskapai.inputAccessoryView = toolbarMaskapai;
    
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.imgMenu setHidden:YES];
    
    bool needReq = [[self.jsonData valueForKey:@"url"]boolValue];
    if (needReq) {
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
    
//    [self setDummy2];
    
    [self.btnBatal setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [self registerForKeyboardNotifications];
    
    [self martialStateHandler];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;  // Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    if (thePickerView.tag == 1) {
        return listPeriod.count;
    }
    else if (thePickerView.tag == pickerExtraData){
        NSArray *arrIdxCode =  [thePickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [[dictExtraData valueForKey:@"Content"]componentsSeparatedByString:@"|"];
        
        return arrContent.count;
    } else if (thePickerView.tag == pickerExtraDataSub){
        NSArray *arrIdxCode =  [thePickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
//        NSArray *arrContent = [dictExtraData valueForKey:@"Content"];
        NSArray *arrContent = [NSJSONSerialization JSONObjectWithData:[[dictExtraData objectForKey:@"Content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

        //Brand Types || Eval_Value | Type
        NSMutableArray *brandList = [[NSMutableArray alloc]init];
        for(NSDictionary *dict in arrContent){
            NSString *brand = @"";
            brand = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Brand"]];
            [brandList addObject:brand];
//            NSArray *dictTypes = [dict objectForKey:@"Types"];
//            for(NSDictionary *dictType in dictTypes){
//                brand = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Brand"], [dictType objectForKey:@"Type"]];
//                [brandList addObject:brand];
//            }
        }
        return brandList.count;
    } else{
        return listInsurance.count;
    }
    
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    [Utility resetTimer];
     NSDictionary *data;
    NSString *strPicker;
    if (thePickerView.tag == 1) {
        data = [listPeriod objectAtIndex:row];
       int mTahun = [[data objectForKey:@"Name"]intValue]/12;
        if ([lang isEqualToString:@"id"]) {
            strPicker = [NSString stringWithFormat:@"%d Tahun", mTahun];
        }else{
            strPicker = [NSString stringWithFormat:@"%d Year", mTahun];
        }
       
    }
    else if (thePickerView.tag == pickerExtraData){
        NSArray *arrIdxCode =  [thePickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [[dictExtraData valueForKey:@"Content"]componentsSeparatedByString:@"|"];

        strPicker = [NSString stringWithFormat:@"%@", arrContent[row]];
    } else if (thePickerView.tag == pickerExtraDataSub){
        NSArray *arrIdxCode =  [thePickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [NSJSONSerialization JSONObjectWithData:[[dictExtraData objectForKey:@"Content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

        //Brand Types || Eval_Value | Type
        NSMutableArray *brandList = [[NSMutableArray alloc]init];
        for(NSDictionary *dict in arrContent){
            NSString *brand = @"";
            brand = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Brand"]];
            [brandList addObject:brand];

//            NSArray *dictTypes = [dict objectForKey:@"Types"];
//            for(NSDictionary *dictType in dictTypes){
//                brand = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Brand"], [dictType objectForKey:@"Type"]];
//                [brandList addObject:brand];
//            }
        }
        strPicker = brandList[row];
    }
    else{
        data = [listInsurance objectAtIndex:row];
        strPicker = [data valueForKey:@"Name"];
    }
    return strPicker;
    
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    NSDictionary *data;
    if (thePickerView.tag == 1) {
        nIdxTenor = row;
        data = [listPeriod objectAtIndex:row];
        int mTahun = [[data objectForKey:@"Name"]intValue]/12;
        if ([lang isEqualToString:@"id"]) {
             [self.edDropDownWaktu setText: [NSString stringWithFormat:@"%d Tahun", mTahun]];
        }else{
            [self.edDropDownWaktu setText: [NSString stringWithFormat:@"%d Year", mTahun]];
        }
       
    }
    else if (thePickerView.tag == pickerExtraData){
        NSArray *arrIdxCode =  [thePickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [[dictExtraData valueForKey:@"Content"]componentsSeparatedByString:@"|"];
        
        UITextField* textField = [self getFieldByAccesibilityLabel:[dictExtraData valueForKey:@"Param_Name"] andTag:[arrIdxCode[0]intValue]];
        
        if(textField != nil){
            textField.accessibilityValue = [NSString stringWithFormat:@"%@", arrContent[row]];
            [textField setText:[NSString stringWithFormat:@"%@", arrContent[row]]];
        }
        
    } else if (thePickerView.tag == pickerExtraDataSub){
        NSArray *arrIdxCode =  [thePickerView.accessibilityLabel componentsSeparatedByString:@"/"];
        NSDictionary *assetType = [listAssetType objectAtIndex:[arrIdxCode[1] intValue]];
        NSDictionary *dictExtraData = [[assetType objectForKey:@"Extra_Data"]objectAtIndex:[arrIdxCode[2]intValue]];
        NSArray *arrContent = [NSJSONSerialization JSONObjectWithData:[[dictExtraData objectForKey:@"Content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

        //Brand Types || Eval_Value | Type
        NSMutableArray *brandList = [[NSMutableArray alloc]init];
//        NSMutableArray *brandListValue = [[NSMutableArray alloc]init];
        for(NSDictionary *dict in arrContent){
            NSString *brand = @"";
            brand = [NSString stringWithFormat:@"%@",[dict objectForKey:@"Brand"]];
//            NSString *brandValue = @"";
//            NSArray *dictTypes = [dict objectForKey:@"Types"];
//            for(NSDictionary *dictType in dictTypes){
//                brand = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"Brand"], [dictType objectForKey:@"Type"]];
//                brandValue = [NSString stringWithFormat:@"%@|%@|%@",[dict objectForKey:@"Brand"], [dictType objectForKey:@"Type"], [dictType objectForKey:@"Eval_Value"]];
                [brandList addObject:brand];
//                [brandListValue addObject:brandValue];
//            }
        }
        UITextField* textField = [self getFieldByAccesibilityLabel:[dictExtraData valueForKey:@"Param_Name"] andTag:[arrIdxCode[0]intValue]];
//        if(textField != nil){
//            NSArray* arrValue = [brandListValue[row] componentsSeparatedByString:@"|"];
//            [self changeNilaiKesepakatan:(int)textField.tag withValue:arrValue[2]];
//            textField.accessibilityValue = [NSString stringWithFormat:@"%@|%@", arrValue[0], arrValue[1]];
//            textField.accessibilityLabel = brandList[row];
            if(brandList.count != 0){
                [textField setText:[NSString stringWithFormat:@"%@", brandList[row]]];
            }
//        }
    }
    else{
        nIdxAsuransi = row;
        data = [listInsurance objectAtIndex:row];
        [self.edDropDownMaskapai setText:[data objectForKey:@"Name"]];
    }
}

-(UITextField*) getFieldByAccesibilityLabel:(NSString*)label andTag:(int)tag{
    
    NSInteger idxVwTambah = 3 + tag;
    UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwBase = [vwTambah.subviews objectAtIndex: vwTambah.subviews.count-1];
    UIView *vwField;
    for(UIView* view in vwBase.subviews){
        if([view.accessibilityLabel isEqualToString:@"field"]){
            vwField = view;
        }
    }
    UITextField* textField;

    for(UIView* view in vwField.subviews){
        NSLog(@"lah loh %@", view.accessibilityLabel);
        if([view.accessibilityLabel isEqualToString:label]){
            textField = (UITextField*) view;
            return textField;
        }
    }
    
    return nil;
}

-(void) doneClicked:(id)sender{
    [self.vwEdCntn1 resignFirstResponder];
    [self.edDropDownWaktu resignFirstResponder];
    [self.edDropDownMaskapai resignFirstResponder];
    
    [self.view endEditing:YES];
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
}


-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmImgStepper = self.imgStepIndicator.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwBtn = self.vwBtn.frame;
    
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    CGRect frmVwAkadAgreement = self.vwAkadAgreement.frame;
    CGRect frmBtnBatal = self.btnBatal.frame;
    CGRect frmBtnNext = self.btnNext.frame;
    
    CGRect frmVwContent1 = self.vwContent1.frame;
    CGRect frmVwContent2 = self.vwContent2.frame;
    CGRect frmVwContent3 = self.vwContent3.frame;
    CGRect frmLblTitleMin = self.lblTitleMin.frame;
    CGRect frmVwTambahBarang = self.vwTambahBarang.frame;
    CGRect frmLblNomTotal = self.lblNominalTotal.frame;
    CGRect frmVwDataPasangan = self.vwDataPasangan.frame;
    
    CGRect frmLblTitleCntn1 = self.lblTitleCntn1.frame;
    CGRect frmVwEdContn1 = self.vwEdCntn1.frame;
    CGRect frmVwLineCntn1 = self.vwLineCntn1.frame;
    
    CGRect frmLblTitleCntn2 = self.lblTitleCntn2.frame;
    CGRect frmEdDropDownWaktu = self.edDropDownWaktu.frame;
    CGRect frmVwLineCntn2 = self.vwLineCntn2.frame;
    
    CGRect frmLblTitleCntn3 = self.lblTitleCntn3.frame;
    CGRect frmEdDropDownMaksapai = self.edDropDownMaskapai.frame;
    CGRect frmVwLineCntn3 = self.vwLineCntn3.frame;
    
    CGRect frmBtnExtView = self.btnExtendView.frame;
    CGRect frmBtnEditView = self.btnEditView.frame;
    CGRect frmLblBarang = self.lblBarang.frame;
    
    CGRect frmBtnChecked = self.btnChecked.frame;
    CGRect frmBtnBaseChecked = self.btnBaseChecked.frame;
    CGRect frmLblChecked = self.lblChecked.frame;
    
    CGFloat sizeWidth = SCREEN_WIDTH - 32;
    
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmImgStepper.origin.x = 16;
    frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
    frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x *2);
    frmImgStepper.size.height = 30;
    
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.size.height = 40;
    
    frmVwScroll.origin.y = frmImgStepper.origin.y + frmImgStepper.size.height + 8;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = SCREEN_WIDTH;
    
    frmVwAkadAgreement.origin.y = 0;
    frmVwAkadAgreement.origin.x = 0;
    frmVwAkadAgreement.size.width = frmVwBtn.size.width;
    
    frmBtnChecked.origin.x = 16;
    frmBtnChecked.origin.y = 8;
    
    frmLblChecked.origin.y = frmBtnChecked.origin.y;
    frmLblChecked.origin.x = frmBtnChecked.origin.x + frmBtnChecked.size.width + 16;
    frmLblChecked.size.width = frmVwAkadAgreement.size.width - frmLblChecked.origin.x - 16;
    
    frmBtnBaseChecked.origin.x = 0;
    frmBtnBaseChecked.origin.y = 0;
    frmBtnBaseChecked.size.width = frmVwAkadAgreement.size.width;
    
    frmVwAkadAgreement.size.height = frmLblChecked.origin.y +frmLblChecked.size.height;
    frmBtnBaseChecked.size.height = frmVwAkadAgreement.size.height;
    
    frmBtnBatal.origin.x = 16;
    frmBtnBatal.origin.y = frmVwAkadAgreement.origin.y + frmVwAkadAgreement.size.height + 16;
    frmBtnBatal.size.height = 40;
    frmBtnBatal.size.width = frmVwBtn.size.width / 2 - (frmBtnBatal.origin.x *2);
    
    frmBtnNext.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
    frmBtnNext.origin.y = frmBtnBatal.origin.y;
    frmBtnNext.size.width = frmVwBtn.size.width - frmBtnNext.origin.x -16;
    frmBtnNext.size.height = 40;
    
    frmVwBtn.size.height = frmBtnBatal.origin.y + frmBtnBatal.size.height + 5;
    
    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 8;
    
    if ([Utility isDeviceHaveNotch]) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
    }
    
    frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;
    
    //Content I
    frmVwContent1.origin.x = 0;
    frmVwContent1.origin.y = 0;
    frmVwContent1.size.width = frmVwContentScroll.size.width;
    
    frmLblTitleCntn1.origin.x = 16;
    frmLblTitleCntn1.origin.y = 0;
    frmLblTitleCntn1.size.height = [Utility heightLabelDynamic:self.lblTitleCntn1 sizeWidth:sizeWidth sizeFont:15];
    frmLblTitleCntn1.size.width = frmVwContent1.size.width - (frmLblTitleCntn1.origin.x *2);
    
    frmVwEdContn1.origin.x = frmLblTitleCntn1.origin.x;
    frmVwEdContn1.origin.y = frmLblTitleCntn1.origin.y + frmLblTitleCntn1.size.height + 2;
    frmVwEdContn1.size.width = frmLblTitleCntn1.size.width;
    
    frmVwLineCntn1.origin.y = frmVwEdContn1.origin.y + frmVwEdContn1.size.height;
    frmVwLineCntn1.origin.x = frmVwEdContn1.origin.x;
    frmVwLineCntn1.size.width = frmVwEdContn1.size.width;
    
    frmVwContent1.size.height = frmVwLineCntn1.origin.y + frmVwLineCntn1.size.height;
    
    //Content II
    frmVwContent2.origin.y = frmVwContent1.origin.y + frmVwContent1.size.height + 16;
    frmVwContent2.origin.x = frmVwContent1.origin.x;
    frmVwContent2.size.width = frmVwContent1.size.width;
    
    frmLblTitleCntn2.origin.y = 0;
    frmLblTitleCntn2.origin.x = 16;
    frmLblTitleCntn2.size.height = [Utility heightLabelDynamic:self.lblTitleCntn2 sizeWidth:sizeWidth sizeFont:15];
    frmLblTitleCntn2.size.width = frmVwContent2.size.width - (frmLblTitleCntn2.origin.x*2);
    
    frmEdDropDownWaktu.origin.x = frmLblTitleCntn2.origin.x;
    frmEdDropDownWaktu.origin.y = frmLblTitleCntn2.origin.y + frmLblTitleCntn2.size.height + 2;
    frmEdDropDownWaktu.size.width = frmLblTitleCntn2.size.width;
    
    frmVwLineCntn2.origin.y = frmEdDropDownWaktu.origin.y + frmEdDropDownWaktu.size.height;
    frmVwLineCntn2.origin.x = frmEdDropDownWaktu.origin.x;
    frmVwLineCntn2.size.width = frmEdDropDownWaktu.size.width;
    
    frmVwContent2.size.height = frmVwLineCntn2.origin.y + frmVwLineCntn2.size.height;
    
    //Content III
    frmVwContent3.origin.y = frmVwContent2.origin.y + frmVwContent2.size.height + 16;
    frmVwContent3.origin.x = frmVwContent2.origin.x;
    frmVwContent3.size.width = frmVwContent2.size.width;
    
    frmLblTitleCntn3.origin.y = 0;
    frmLblTitleCntn3.origin.x = 16;
    frmLblTitleCntn3.size.height = [Utility heightLabelDynamic:self.lblTitleCntn3 sizeWidth:sizeWidth sizeFont:15];
    frmLblTitleCntn3.size.width = frmVwContent3.size.width - (frmLblTitleCntn3.origin.x * 2);
    
    frmEdDropDownMaksapai.origin.y = frmLblTitleCntn3.origin.y + frmLblTitleCntn3.size.height + 2;
    frmEdDropDownMaksapai.origin.x = frmLblTitleCntn3.origin.x;
    frmEdDropDownMaksapai.size.width = frmVwContent3.size.width;
    
    frmVwLineCntn3.origin.y = frmEdDropDownMaksapai.origin.y + frmEdDropDownMaksapai.size.height - 5;
    frmVwLineCntn3.origin.x = frmEdDropDownMaksapai.origin.x;
    frmVwLineCntn3.size.width = frmEdDropDownMaksapai.size.width - (frmVwLineCntn3.origin.x * 2);
    
    frmVwContent3.size.height = frmVwLineCntn3.origin.y + frmVwLineCntn3.size.height;
    
    frmLblTitleMin.origin.y = frmVwContent3.origin.y + frmVwContent3.size.height + 16;
    frmLblTitleMin.origin.x = 16;
    frmLblTitleMin.size.height = [Utility heightLabelDynamic:self.lblTitleMin sizeWidth:sizeWidth sizeFont:15];
    frmLblTitleMin.size.width = SCREEN_WIDTH - (frmLblTitleMin.origin.x * 2);
    
    frmVwTambahBarang.origin.y = frmLblTitleMin.origin.y + frmLblTitleMin.size.height + 8;
    frmVwTambahBarang.origin.x = 16;
    frmVwTambahBarang.size.width = SCREEN_WIDTH - (frmVwTambahBarang.origin.x *2);
    
    
    frmBtnExtView.origin.x = frmVwTambahBarang.size.width - frmBtnExtView.size.width - 16;
    
    frmLblBarang.origin.x = 8;
    frmLblBarang.origin.y = 11;
    if ([self checkBarang]) {
        frmLblBarang.size.height = [Utility heightLabelDynamic:self.lblBarang sizeWidth:frmBtnExtView.origin.x - (frmLblBarang.origin.x *2) sizeFont:15];
        frmLblBarang.size.width = frmBtnExtView.origin.x - (frmLblBarang.origin.x *2);
    }else{
        frmBtnEditView.origin.x = frmBtnExtView.origin.x - frmBtnEditView.size.width;
        frmLblBarang.size.height = [Utility heightLabelDynamic:self.lblBarang sizeWidth:frmBtnEditView.origin.x - (frmLblBarang.origin.x *2) sizeFont:15];
        frmLblBarang.size.width = frmBtnEditView.origin.x - (frmLblBarang.origin.x *2);
    }
    
    frmVwTambahBarang.size.height = frmLblBarang.origin.y + frmLblBarang.size.height + frmLblBarang.origin.y;
    frmBtnExtView.size.height  = frmVwTambahBarang.size.height;
    frmBtnEditView.size.height = frmVwTambahBarang.size.height;
    
    frmLblNomTotal.origin.y  = frmVwTambahBarang.origin.y + frmVwTambahBarang.size.height + 16;
    frmLblNomTotal.origin.x = frmVwContentScroll.size.width - frmLblNomTotal.size.width - 32;
    
    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
    frmVwDataPasangan.origin.x = 0;
    frmVwDataPasangan.size.height = 34;
    frmVwDataPasangan.size.width = frmVwContentScroll.size.width;
    
    frmVwContentScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;
//    frmVwContentScroll.size.height = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;

    UIView* viewStepper = [[UIView alloc]initWithFrame:frmImgStepper];
    [Styles createProgress:viewStepper step:2 from:4];
    
    [self.vwContentScroll addSubview:viewStepper];
    
    self.vwTitle.frame = frmVwTitle;
    self.imgStepIndicator.frame = frmImgStepper;
    self.vwScroll.frame = frmVwScroll;
    self.vwBtn.frame = frmVwBtn;
    
    [self.imgStepIndicator setHidden:YES];
    [self.view addSubview:viewStepper];
    
    self.vwContentScroll.frame = frmVwContentScroll;
    self.vwAkadAgreement.frame = frmVwAkadAgreement;
    self.btnBatal.frame = frmBtnBatal;
    self.btnNext.frame = frmBtnNext ;
    
    self.vwContent1.frame = frmVwContent1;
    self.vwContent2.frame = frmVwContent2;
    self.vwContent3.frame = frmVwContent3;
    self.lblTitleMin.frame = frmLblTitleMin;
    self.vwTambahBarang.frame = frmVwTambahBarang;
    self.lblNominalTotal.frame = frmLblNomTotal;
    self.vwDataPasangan.frame = frmVwDataPasangan;
    
    self.lblTitleCntn1.frame = frmLblTitleCntn1;
    self.vwEdCntn1.frame = frmVwEdContn1;
    self.vwLineCntn1.frame = frmVwLineCntn1;
    
    self.lblTitleCntn2.frame = frmLblTitleCntn2;
    self.edDropDownWaktu.frame = frmEdDropDownWaktu;
    self.vwLineCntn2.frame = frmVwLineCntn2;
    
    self.lblTitleCntn3.frame = frmLblTitleCntn3;
    self.edDropDownMaskapai.frame = frmEdDropDownMaksapai;
    self.vwLineCntn3.frame = frmVwLineCntn3;
    
    self.btnExtendView.frame = frmBtnExtView;
    self.lblBarang.frame = frmLblBarang;
    self.btnEditView.frame = frmBtnEditView;
    
    self.btnBaseChecked.frame = frmBtnBaseChecked;
    self.btnChecked.frame = frmBtnChecked;
    self.lblChecked.frame = frmLblChecked;
    
    self.vwTambahBarang = [self makeBorder:self.vwTambahBarang];
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height + kbSize.height + 20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 10, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}


-(UIView *) makeBorder : (UIView *) mVw{
    UIView *vw = mVw;
    vw.layer.borderWidth = 1;
    vw.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    vw.layer.cornerRadius = 8;
    return vw;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    [Utility stopTimer];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        if ([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]) {
             if (![requestType isEqualToString:@"check_notif"]) {
                 NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                 if (dataDict) {
                     NSLog(@"%@", dataDict);
                     listAssetType = [dataDict objectForKey:@"ListAssetType"];
                     listInsurance = [dataDict objectForKey:@"ListInsurance"];
                     listPeriod = [dataDict objectForKey:@"ListLoanPeriod"];
                     
                     int mTahun = [[dataDict objectForKey:@"MaxTenor"]intValue]/12;
                     NSString *nMaxLoan = [NSString stringWithFormat:@"%@", [dataDict objectForKey:@"MaxLoan"]];
                     nMaxDana = [[dataDict objectForKey:@"MaxLoan"]doubleValue];
                     minLoan = [[dataDict objectForKey:@"MinLoan"]doubleValue];
                     nRateTasaksi = [[dataDict objectForKey:@"Rate_Taksasi"]doubleValue];
                     nMartialState = [[dataDict objectForKey:@"Marital_Status_Code"]doubleValue];
                     
                     NSLog(@"double Value : %f", [[dataDict objectForKey:@"MaxLoan"]doubleValue]);
                     if ([lang isEqualToString:@"id"]) {
                         self.vwEdCntn1.placeholder = [NSString stringWithFormat:@"Minimum %@ dan Maksimal %@", [Utility formatCurrencyValue:[[dataDict objectForKey:@"MinLoan"]doubleValue]], [Utility formatCurrencyValue:[[dataDict objectForKey:@"MaxLoan"]doubleValue]]];
                         self.edDropDownWaktu.placeholder = [NSString stringWithFormat:@"Maksimal %d Tahun", mTahun];
                         
                     }else{
                         self.vwEdCntn1.placeholder = [NSString stringWithFormat:@"Minimum %@ and Maximum %@", [Utility formatCurrencyValue:[[dataDict objectForKey:@"MinLoan"]doubleValue]], [Utility formatCurrencyValue:[[dataDict objectForKey:@"MaxLoan"]doubleValue]]];
                         self.edDropDownWaktu.placeholder = [NSString stringWithFormat:@"Maximum %d Year", mTahun];
                     }
                     
                     currencyFormatter.maximumSignificantDigits = nMaxLoan.length;
                     [dataManager.dataExtra setValue:[dataDict valueForKey:@"ReferenceCode"] forKey:@"reference_code"];
                     [self martialStateHandler];
                 }
             }
        }else{
            [Utility showMessage:[jsonObject valueForKey:@"response"] mVwController:self mTag:[[jsonObject valueForKey:@"response_code"]intValue]];
//            [self setDummy2];
//            [self martialStateHandler];
        }
    }
}
    
- (void)errorLoadData:(NSError *)error{
    
}

- (void)reloadApp{
    [self backToRoot];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag <= 3){
        
        if (isEdited) {
            if (editCurrentTag != 0) {
                isEdited = false;
                editCurrentTag = 0;
            }
        }
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [Utility resetTimer];
   if (textField.tag == 1) {
       stateFocus = true;
        
        if (textField.tag == 100 + mTag) {
            NSInteger idxView = 3 + mTag;
            UIView *vwTambahBarang = [_vwContentScroll.subviews objectAtIndex:idxView];
            UIView *vwAddonAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
            UIButton * btnDone = [vwAddonAsset.subviews objectAtIndex:[vwAddonAsset.subviews count]-2];

            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring
                         stringByReplacingCharactersInRange:range withString:string];

            if (btnDone.tag == mTag) {
                if ([substring isEqualToString:@""] || substring == nil) {
                    [btnDone setHidden:YES];
                }else{
                    NSString *strNom = [substring stringByReplacingOccurrencesOfString:@"," withString:@""];
                    nominalChoiceAsset = [strNom integerValue];
                    [btnDone setHidden:NO];
                }

            }
        }
       
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        NSInteger newValue = [newText integerValue];
        
        if ([newText length] == 0 || newValue == 0)
            textField.text = nil;
        else if ([newText length] > currencyFormatter.maximumSignificantDigits)
            textField.text = textField.text;
        else
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
            if(newValue > nMaxDana){
                if ([lang isEqualToString:@"id"]) {
                    [Utility showMessage:@"Nominal Pengajuan melebihi batas yang di tawarkan" mVwController:self mTag:06];
                }else{
                    [Utility showMessage:@"Financing Amount exceeding the limit offered" mVwController:self mTag:06];
                }
            }
       
        return NO;
        
    }else{
        NSLog(@"teef access %@", textField.accessibilityLabel);
        NSArray *arAccesbility = [textField.accessibilityLabel componentsSeparatedByString:@"_"];
        NSInteger idxAccesbility = [[arAccesbility objectAtIndex:1]integerValue];
        
        NSInteger idxView = 3 + idxAccesbility;
        UIView *vwTambahBarang = [_vwContentScroll.subviews objectAtIndex:idxView];
        UIView *vwAddonAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1]; //disni error
        UIButton * btnDone = [vwAddonAsset.subviews objectAtIndex:[vwAddonAsset.subviews count]-3];
        
        NSArray *arrLastTag = (NSArray *) lastTag;
        
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        
        if (arrLastTag.count == idxAccesbility) {
            NSNumber *numIdx = [arrLastTag objectAtIndex:idxAccesbility-1];
            if (idxAccesbility == [numIdx integerValue]) {
                
                if (btnDone.tag == idxAccesbility) {
                    if ([substring isEqualToString:@""] || substring == nil) {
                        [btnDone setHidden:YES];
                    }else{
                        NSString *strNom = [substring stringByReplacingOccurrencesOfString:@"," withString:@""];
                        nominalChoiceAsset = [strNom integerValue];
                        [btnDone setHidden:NO];
                    }
                    
                }
            }
        }
        else{
            if (btnDone.tag == idxAccesbility) {
                if ([substring isEqualToString:@""] || substring == nil) {
                    [btnDone setHidden:YES];
                }else{
                    NSString *strNom = [substring stringByReplacingOccurrencesOfString:@"," withString:@""];
                    nominalChoiceAsset = [strNom integerValue];
                    [btnDone setHidden:NO];
                }
                
            }
        }
        
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        NSInteger newValue = [newText integerValue];
        
        if ([newText length] == 0 || newValue == 0)
            textField.text = nil;
        else if ([newText length] > [nMaxKesepakatan length])
            textField.text = textField.text;
        else
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
            if(newValue > [nMaxKesepakatan doubleValue]){
//                if ([lang isEqualToString:@"id"]) {
//                    [Utility showMessage:@"Nominal Pengajuan melebihi batas yang di tawarkan" mVwController:self mTag:05];
//                }else{
//                    [Utility showMessage:@"Financing Amount exceeds the limit offered" mVwController:self mTag:05];
//                }
//                textField.text = @"";
                textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[nMaxKesepakatan doubleValue]]];
//                if (btnDone.tag == idxAccesbility) {
//                    [btnDone setHidden:YES];
//                }
            }
        return NO;
    }
    
    
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if (textField.tag == 1) {
        if (stateFocus) {
            double minimumNominal = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@""]doubleValue];
            
            NSString *strTitleMin, *strMessage;
            
            if ([lang isEqualToString:@"id"]) {
                strTitleMin = @"Object Refinancing / Assets yang anda miliki dengan total nilai minimal IDR.";
                strMessage = @"Minimal Pengajuan pembiayaan ";
            }else{
                strTitleMin = @"Refinancing Object / Assets Owned with a minimum total value IDR.";
                strMessage = @"Minimum Financing Amount is ";
            }
            
            if (minimumNominal < minLoan) {
                [Utility showMessage:[NSString stringWithFormat:@"%@ IDR. %@", strMessage, [Utility formatCurrencyValue:minLoan]] mVwController:self mTag:07];
            }else{
                
                double persentaseMinNominal = (minimumNominal * nRateTasaksi)/100;
                
                NSString *strMinNominal = [Utility formatCurrencyValue:minimumNominal + persentaseMinNominal];
                [self.lblTitleMin setText:[NSString stringWithFormat:@"%@%@",strTitleMin,strMinNominal]];
                CGRect frmLbTitleMin = self.lblTitleMin.frame;
                
                frmLbTitleMin.origin.x = 16;
                frmLbTitleMin.size.height = [Utility heightLabelDynamic:self.lblTitleMin sizeWidth:SCREEN_WIDTH-32 sizeFont:15];
                frmLbTitleMin.size.width = SCREEN_WIDTH - (frmLbTitleMin.origin.x * 2);
                
                self.lblTitleMin.frame = frmLbTitleMin;
                
                [self rechangePostAxisY];
            }

        }
    }
    return YES;
}

-(BOOL) checkBarang{
    if (
        [self.lblBarang.text isEqualToString:@"Object Refinancing"]
        || [self.lblBarang.text isEqualToString:@"Add Items"]
        || [self.lblBarang.text isEqualToString:@"Tambah Barang"]
        ) {
        [self.lblBarang setTextColor:UIColorFromRGB(const_color_gray)];
        [self.btnEditView setHidden:TRUE];
        return TRUE;
    }else{
        [self.lblBarang setTextColor:[UIColor blackColor]];
        [self.btnEditView setHidden:FALSE];
        return FALSE;
    }
    
}

-(void)createView : (int) tag{
    [lastTag addObject:@(tag)];
    NSInteger lastIdxVwTambah = 2 + tag;
    NSInteger idexLast = [self.vwContentScroll.subviews count]-2;
    
    UIView *lastVwTambah = (UIView *) [self.vwContentScroll.subviews objectAtIndex:lastIdxVwTambah];
    UIView *nextVwTambah = [[UIView alloc]initWithFrame:CGRectMake(16, lastVwTambah.frame.origin.y + lastVwTambah.frame.size.height + 16,lastVwTambah.frame.size.width , lastVwTambah.frame.size.height)];
    nextVwTambah.tag = tag;
    nextVwTambah = [self makeBorder:nextVwTambah];
    
    UIButton *btnExpand = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 43)];
    btnExpand.tag = tag;
    btnExpand.accessibilityLabel = [NSString stringWithFormat:@"ext_%d", tag];
    [btnExpand setTitle:@"+" forState:UIControlStateNormal];
    [btnExpand setTitle:@"x" forState:UIControlStateSelected];
    [btnExpand setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btnExpand.titleLabel setFont:[UIFont systemFontOfSize:28]];
    [btnExpand addTarget:self action:@selector(actionExpandView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnEdit = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 43)];
    btnEdit.tag = tag;
    btnEdit.accessibilityLabel =[NSString stringWithFormat:@"edt_%d", tag];
    [btnEdit setImage:[UIImage imageNamed:@"baseline_create_black_36pt"] forState:UIControlStateNormal];
    btnEdit.imageEdgeInsets = UIEdgeInsetsMake(8, 4, 8, 4);
    btnEdit.tintColor =UIColorFromRGB(const_color_primary);
    [btnEdit setHidden:YES];
    [btnEdit addTarget:self action:@selector(actionEditView:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lblTambahBarang = [[UILabel alloc]initWithFrame:CGRectMake(8, 11, (nextVwTambah.frame.size.width - btnExpand.frame.size.width - btnEdit.frame.size.width - 8), (nextVwTambah.frame.size.height - 22))];
    lblTambahBarang.tag = tag;
    lblTambahBarang.accessibilityLabel = [NSString stringWithFormat:@"lbl_%d", tag];
    [lblTambahBarang setTextColor:UIColorFromRGB(const_color_gray)];
    [lblTambahBarang setFont:[UIFont systemFontOfSize:14]];
    [lblTambahBarang setText:@"Object Refinancing"];


    CGRect frmNextVwTambah = nextVwTambah.frame;
    CGRect frmBtnExpand = btnExpand.frame;
    CGRect frmBtnEdit = btnEdit.frame;
    
    frmBtnExpand.origin.x = frmNextVwTambah.size.width - frmBtnExpand.size.width - 16;
    frmBtnEdit.origin.x = frmBtnExpand.origin.x - frmBtnEdit.size.width - 8;
    
    btnExpand.frame = frmBtnExpand;
    btnEdit.frame = frmBtnEdit;
    
    [nextVwTambah addSubview:btnExpand];
    [nextVwTambah addSubview:btnEdit];
    [nextVwTambah addSubview:lblTambahBarang];
    
    [self.vwContentScroll insertSubview:nextVwTambah atIndex:idexLast];
    
    UIView *vwTambah = (UIView *) [self.vwContentScroll.subviews objectAtIndex:idexLast];
    UILabel *lblNomTotal = (UILabel *) [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-2];
    UIView *vwDataCouple = (UILabel *) [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1];

    CGRect frmContentScroll = self.vwContentScroll.frame;
    CGRect frmVwTambah = vwTambah.frame;
    CGRect frmLblNomTotal = lblNomTotal.frame;
    CGRect frmVwDataPasangan = vwDataCouple.frame;
    
    frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
    frmVwDataPasangan.size.height = 34;
    
    frmContentScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;

    self.vwContentScroll.frame = frmContentScroll;
    lblNomTotal.frame = frmLblNomTotal;
    vwDataCouple.frame = frmVwDataPasangan;
    
    [self.vwScroll setContentSize:CGSizeMake(SCREEN_WIDTH, frmContentScroll.origin.y + frmContentScroll.size.height)];
}

- (UIView *) showAset : (UIView *) parentView{
    UIView *vwBase;
    vwBase = [[UIView alloc]initWithFrame:CGRectMake(8, 43, parentView.frame.size.width - 16, 0)];
    vwBase.tag = parentView.tag;
    vwBase.accessibilityLabel = @"itsVwBase";
    CGFloat lenWidth = vwBase.frame.size.width/3;
    
    UIView *vwRadio = [[UIView alloc]init];
    [vwRadio setAccessibilityLabel:@"radio"];
    
    CGFloat radioRectSize = 24;
    CGFloat lastY = 0;
    
    int i = 0, j = 0, idx = 1;
    for(i = 0; i < listAssetType.count/3; i++){
        CGFloat lastX = 0;
        CGFloat lastYinX = 0;
        for(j = 0; j < 3; j++){
            UIButton *radioBtn = [[UIButton alloc]initWithFrame:CGRectMake(lastX, lastY, radioRectSize, radioRectSize)];
            
            [radioBtn setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
            [radioBtn setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateSelected];
            [radioBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
            [radioBtn setTag:parentView.tag];
            [radioBtn setAccessibilityLabel:[NSString stringWithFormat:@"b_%d",idx]];
            [radioBtn addTarget:self action:@selector(actionRadioBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(lastX + radioBtn.frame.size.width + 4, lastY, lenWidth - radioRectSize, radioRectSize*2)];
            [label setTag:parentView.tag];
            [label setText:[[listAssetType objectAtIndex:idx-1]valueForKey:@"Name"]];
            [label setFont:[UIFont fontWithName:@"Helvetica" size:13]];
            [label setTextAlignment:NSTextAlignmentLeft];
            [label setLineBreakMode:NSLineBreakByWordWrapping];
            [label setNumberOfLines:2];
                        
            [vwRadio addSubview:radioBtn];
            [vwRadio addSubview:label];
            
            lastX = label.frame.origin.x + label.frame.size.width;
            lastYinX = label.frame.origin.y + label.frame.size.height;
                        
            idx = idx + 1;
        }
        lastY = lastYinX;
    }

    CGFloat lastX = 0;
    CGFloat lastYinX = 0;
    for(j = 0; j < fmodl(listAssetType.count, 3); j++){
        UIButton *radioBtn = [[UIButton alloc]initWithFrame:CGRectMake(lastX, lastY, radioRectSize, radioRectSize)];
        
        [radioBtn setImage:[UIImage imageNamed:@"radio_unselect"] forState:UIControlStateNormal];
        [radioBtn setImage:[UIImage imageNamed:@"radio_select"] forState:UIControlStateSelected];
        [radioBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        [radioBtn setAccessibilityLabel:[NSString stringWithFormat:@"b_%d",idx]];
        [radioBtn setTag:parentView.tag]; //or code ?
        [radioBtn addTarget:self action:@selector(actionRadioBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(lastX + radioBtn.frame.size.width + 4, lastY, lenWidth - radioRectSize, radioRectSize*2)];
        [label setTag:parentView.tag];
        [label setText:[[listAssetType objectAtIndex:idx-1]valueForKey:@"Name"]];
        [label setFont:[UIFont fontWithName:@"Helvetica" size:13]];
        [label setLineBreakMode:NSLineBreakByWordWrapping];
        [label setNumberOfLines:2];
                    
        [vwRadio addSubview:radioBtn];
        [vwRadio addSubview:label];
                
        lastX = label.frame.origin.x + label.frame.size.width;
        lastYinX = label.frame.origin.y + label.frame.size.height;
        idx = idx + 1;
    }
    if(lastYinX != 0){
        lastY = lastYinX;
    }

    vwRadio.frame = CGRectMake(0, 0,vwBase.frame.size.width,  lastY +10);
    [vwBase addSubview:vwRadio];
        
    vwBase.frame = CGRectMake(vwBase.frame.origin.x, vwBase.frame.origin.y, vwBase.frame.size.width, vwRadio.frame.size.height + vwRadio.frame.origin.y);
    
    return vwBase;
}

- (void) selectedRadioBtn : (NSString*) accessLbl fromSuperview : (UIView*) supervw{
    for(id view in [supervw subviews]){
        if([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*) view;
            if([btn.accessibilityLabel isEqualToString:accessLbl]){
                btn.selected = YES;
            }else{
                btn.selected = NO;
            }
        }
    }
}

- (UIButton*) getSelectedBtn : (NSString*) accessLbl fromSuperview : (UIView*) supervw{
    UIButton *returnBtn = nil;
    for(id view in [supervw subviews]){
        if([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton*) view;
            if([btn.accessibilityLabel isEqualToString:accessLbl]){
                btn.selected = YES;
                returnBtn = btn;
            }else{
                btn.selected = NO;
            }
        }
    }
    
    return returnBtn;
}

- (void) actionRadioBtn : (UIButton *) sender{
    NSArray *arID = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
    int idxVwTambah = (int)sender.tag + 3;
    int idxCode = [arID[1] intValue]-1;
    idxChoiceAsset = [arID[1] intValue];
    [self selectedRadioBtn:sender.accessibilityLabel fromSuperview:[sender superview]];
    
    UIView *vwRadio = sender.superview;
    UIView *vwBase = vwRadio.superview;

    for(UIView* view in vwBase.subviews){
        if(![view.accessibilityLabel isEqualToString:@"radio"]){
            [view removeFromSuperview];
        }
    }
    
    UIView *vwField = [[UIView alloc]init];
    vwField.tag = sender.tag;
    vwField.accessibilityLabel = [NSString stringWithFormat:@"field"];
    
    NSDictionary *dictAsset = [listAssetType objectAtIndex:idxCode];
    NSArray *arrExtraData = [dictAsset objectForKey:@"Extra_Data"];

    CGFloat startY = 0;
    for(int x=0; x < arrExtraData.count; x++){
        UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(16, startY, vwBase.frame.size.width - 32, 24)];
        UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(16, startY + lblTitle.frame.size.height, lblTitle.frame.size.width, 34)];
        textField.borderStyle = UITextBorderStyleNone;
        textField.keyboardType = UIKeyboardTypeDefault;
        [textField setFont:[UIFont fontWithName:@"Helvetica" size:14]];
        textField.tag = sender.tag;
        [textField.layer setBorderWidth:.5];
        [textField.layer setBorderColor: [[UIColor grayColor]CGColor]];
        
        [lblTitle setFont:[UIFont fontWithName:@"Helvetica" size:14]];
        [lblTitle setText:[NSString stringWithFormat:@"%@", [[arrExtraData objectAtIndex:x] valueForKey:@"Title"]]];
        
        [lblTitle setTextColor:[UIColor blackColor]];
        
        [vwField addSubview:lblTitle];
        [vwField addSubview:textField];
        startY = textField.frame.size.height + textField.frame.origin.y + 4;

        [self setTypeField:textField withType:[arrExtraData objectAtIndex:x] andCode:idxCode andIdx:x];

    }
    vwField.frame = CGRectMake(0, vwRadio.frame.origin.y + vwRadio.frame.size.height, vwRadio.frame.size.width, startY);
    [vwBase addSubview:vwField];
    vwBase.frame = CGRectMake(vwBase.frame.origin.x, vwBase.frame.origin.y, vwBase.frame.size.width,  vwField.frame.origin.y + vwField.frame.size.height);
    
    ///Adding Nominal of asset
    [self addNominal:vwField baseView:vwBase dxCode:[arID[1] intValue]];
    
    UIView *vwTambahBarang = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
    UILabel *lblNomMinimum = [vwParentRbAsset.subviews objectAtIndex:[vwParentRbAsset.subviews count]-1];
    UILabel *lblNomTotal = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-2];
    UIView *vwDataPasangan = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1];
    
    NSString *strCaption;
    
    if([[dictAsset valueForKey:@"Name"] containsString:@"Tanah"]){
        if ([lang isEqualToString:@"id"]) {
            strCaption = @"Nilai maksimal akseptasi tanah >= 50m2 adalah sebesar IDR.";
        }else{
            strCaption = @"Maximum Calculation Tanah for >= 50m2 is IDR. ";
        }
    }else{
        if ([lang isEqualToString:@"id"]) {
            strCaption = @"Maksimum Nilai Kesepakatan IDR.";
        }else{
            strCaption = @"Maximum Calculation Value IDR.";
        }
    }
    
    nMaxKesepakatan = [dictAsset valueForKey:@"Value"];
    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[[dictAsset valueForKey:@"Value"]doubleValue]]]];

    CGRect frmVwTambah = vwTambahBarang.frame;
    CGRect frmVwBase = vwBase.frame;
    CGRect frmLblNom = lblNomMinimum.frame;
    
    if (IPHONE_5) {
        frmLblNom.origin.x = 16;
        frmLblNom.size.height = [Utility heightLabelDynamic:lblNomMinimum sizeWidth:frmVwBase.size.width - (frmLblNom.origin.x *2) sizeFont:10];
        frmLblNom.size.width = frmVwBase.size.width - (frmLblNom.origin.x *2);
       
    }
    frmLblNom.size.height = [Utility heightLabelDynamic:lblNomMinimum sizeWidth:frmVwBase.size.width - (frmLblNom.origin.x *2) sizeFont:15];
    lblNomMinimum.frame = frmLblNom;
    
    if([lblNomTotal isKindOfClass:[UILabel class]]){
        CGRect frmLblNomTotal = lblNomTotal.frame;
        CGRect frmVwDataPasangan = vwDataPasangan.frame;
        
        frmVwTambah.size.height = vwBase.frame.origin.y + vwBase.frame.size.height + 20;
        frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
        frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
        
        vwTambahBarang.frame = frmVwTambah;
        lblNomTotal.frame = frmLblNomTotal;
        vwDataPasangan.frame = frmVwDataPasangan;
    }
    
    [self rechangePostAxisY];
}

- (void) setTypeField:(UITextField*) textField withType:(NSDictionary*) dictType andCode:(int) code andIdx:(int)idxObject{
    
    if([[dictType objectForKey:@"Type"] isEqualToString:@"picklist"]){
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
        [picker setDataSource: self];
        [picker setDelegate: self];
        picker.showsSelectionIndicator = YES;
        picker.tag = pickerExtraData;
        picker.accessibilityLabel = [NSString stringWithFormat:@"%ld/%d/%d",(long)textField.tag,code,idxObject];
       
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Pilih %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Select %@",[dictType objectForKey:@"Title"]];
        }
        textField.inputView = picker;
        textField.inputAccessoryView = toolBar;
        
    }else if([[dictType objectForKey:@"Type"] isEqualToString:@"freetext"] || [[dictType objectForKey:@"Type"] isEqualToString:@"freeteext"] ){
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputAccessoryView = toolBar;
        
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Masukkan %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Enter %@",[dictType objectForKey:@"Title"]];
        }
    }else if([[dictType objectForKey:@"Type"] isEqualToString:@"subpicklist"]){
        UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
        [picker setDataSource: self];
        [picker setDelegate: self];
        picker.showsSelectionIndicator = YES;
        picker.accessibilityLabel = [NSString stringWithFormat:@"%ld/%d/%d",(long)textField.tag,code,idxObject];
        picker.tag = pickerExtraDataSub;

        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputView = picker;
        textField.inputAccessoryView = toolBar;
        
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Pilih %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Select %@",[dictType objectForKey:@"Title"]];
        }
    }else if([[dictType objectForKey:@"Type"] isEqualToString:@"numbertext"] || [[dictType objectForKey:@"Type"] isEqualToString:@"numberText"]){
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputAccessoryView = toolBar;
        
        [textField addTarget:self action:@selector(textFieldPurchaseYearValidation:) forControlEvents:UIControlEventEditingDidEnd];
        
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Masukkan %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Enter %@",[dictType objectForKey:@"Title"]];
        }
    }else{
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.accessibilityLabel = [NSString stringWithFormat:@"%@",[dictType objectForKey:@"Param_Name"]];
        textField.inputAccessoryView = toolBar;
        if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
            textField.placeholder = [NSString stringWithFormat:@"Masukkan %@",[dictType objectForKey:@"Title"]];
        }else{
            textField.placeholder = [NSString stringWithFormat:@"Enter %@",[dictType objectForKey:@"Title"]];
        }
    }
}

- (void) addNominal: (UIView*)vwField baseView: (UIView*)vwBase dxCode:(int)idxCode{
    UILabel *lblTitle = [[UILabel alloc]init];
    UILabel *lblCurrency = [[UILabel alloc]init];
    JVFloatLabeledTextField *txtNominal = [[JVFloatLabeledTextField alloc]init];
    UIButton *btnDone = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 24, 24)];
    UIView *vwLine = [[UIView alloc]init];
    UILabel *lblNominalMinimum = [[UILabel alloc]init];
    
    UIView *viewUnggah = [[UIView alloc]init];
    viewUnggah.accessibilityIdentifier = @"objFoto";
    viewUnggah.accessibilityValue = [NSString stringWithFormat:@"%ld",(long)vwBase.tag];
    UILabel *unggahFotoLbl = [[UILabel alloc]init];
    UIImageView *imgUnggahFoto = [[UIImageView alloc]init];
    
    
    [imgUnggahFoto setImage:[[UIImage imageNamed:@"ic_fpop_tphoto"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    [imgUnggahFoto setContentMode:UIViewContentModeScaleAspectFit];
    [imgUnggahFoto setTintColor:UIColorFromRGB(const_color_primary)];

    if ([lang isEqualToString:@"id"]) {
        [lblTitle setText:@"Nilai"];
        unggahFotoLbl.text = @"Unggah Foto dan Lokasi Barang/Asset";
    }else{
        [lblTitle setText:@"Value"];
        unggahFotoLbl.text = @"Upload Photo and Location Object/Asset";
    }
    
    [unggahFotoLbl setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [unggahFotoLbl sizeToFit];
    
    [lblTitle setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [lblTitle sizeToFit];

    [lblCurrency setText:@"Rp "];
    [lblCurrency setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [lblCurrency sizeToFit];

    txtNominal.borderStyle = UITextBorderStyleNone;
    txtNominal.keyboardType = UIKeyboardTypeNumberPad;
    txtNominal.tag = 100 + vwBase.tag;
    txtNominal.accessibilityLabel = [NSString stringWithFormat:@"nom_%ld", vwBase.tag];
    txtNominal.delegate = self;
    txtNominal.inputAccessoryView = toolBar;
    [txtNominal setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];

    [btnDone setTitle:@"OK" forState:UIControlStateNormal];
    [btnDone setTag:vwBase.tag];
    [btnDone addTarget:self action:@selector(saveChoiceAsset:) forControlEvents:UIControlEventTouchUpInside];
    [btnDone setAccessibilityLabel:[NSString stringWithFormat:@"%d",idxCode]];
    [Styles setStyleButton:btnDone];

    [vwLine setBackgroundColor:UIColorFromRGB(const_color_gray)];

    lblNominalMinimum.accessibilityLabel = [NSString stringWithFormat:@"lblMinVal_%ld", vwBase.tag];
    [lblNominalMinimum setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [lblNominalMinimum setTextColor:UIColorFromRGB(const_color_primary)];
    [lblNominalMinimum setText:@""];

    if (IPHONE_5) {
         [lblNominalMinimum setFont:[UIFont fontWithName:@"Lato-Regular" size:10]];
    }

    CGRect frmVwUnggah = viewUnggah.frame;
    CGRect frmLblUnggahFoto = unggahFotoLbl.frame;
    CGRect frmImgFoto = imgUnggahFoto.frame;
    CGRect frmVwField = vwField.frame;
    CGRect frmLblTitle = lblTitle.frame;
    CGRect frmLblCurrency = lblCurrency.frame;
    CGRect frmTxtNominal = txtNominal.frame;
    CGRect frmVwLine = vwLine.frame;
    CGRect frmVwBase = vwBase.frame;
    CGRect frmBtn = btnDone.frame;
    CGRect frmLblMinimum = lblNominalMinimum.frame;

    frmLblTitle.origin.x = 16;
    frmVwUnggah.origin.x = 16;
    frmVwUnggah.origin.y = frmVwField.origin.y + frmVwField.size.height;
    frmVwUnggah.size.width = vwBase.frame.size.width - 32;
    frmVwUnggah.size.height = 50;
    
    frmLblUnggahFoto.origin.x = 0;
    frmLblUnggahFoto.size.width = frmVwUnggah.size.width - 30;
    frmLblUnggahFoto.size.height = 50;
    frmLblUnggahFoto.origin.y = 0;
    
    frmImgFoto.origin.x = frmLblUnggahFoto.origin.x + frmLblUnggahFoto.size.width + 5;
    frmImgFoto.size.width = 25;
    frmImgFoto.size.height = 50;
    frmImgFoto.origin.y = 0;

    frmLblCurrency.origin.x = frmLblTitle.origin.x + frmLblTitle.size.width + 10;

    frmTxtNominal.origin.x = frmLblCurrency.origin.x + frmLblCurrency.size.width +5;
    frmTxtNominal.origin.y = frmVwField.origin.y + frmVwField.size.height + frmVwUnggah.size.height;
    frmTxtNominal.size.height = 43;

    frmBtn.size.width = 70;
    frmBtn.origin.x = vwBase.frame.size.width - frmBtn.size.width - 16;
    frmBtn.origin.y = frmTxtNominal.origin.y + frmTxtNominal.size.height/3 - 3;

    frmTxtNominal.size.width = vwBase.frame.size.width - frmBtn.size.width - 32;

    frmVwLine.origin.x = frmLblCurrency.origin.x;
    frmVwLine.origin.y = frmTxtNominal.origin.y + frmTxtNominal.size.height;
    frmVwLine.size.height = 1;
    frmVwLine.size.width = vwBase.frame.size.width - frmVwLine.origin.x - frmBtn.size.width - 32;

    frmLblMinimum.origin.x = frmVwLine.origin.x;
    frmLblMinimum.origin.y = frmVwLine.origin.y + frmVwLine.size.height + 8;
    frmLblMinimum.size.height = [Utility heightLabelDynamic:lblNominalMinimum sizeWidth:frmVwLine.size.width sizeFont:12];
    frmLblMinimum.size.width = vwBase.frame.size.width - frmLblMinimum.origin.x - 8;

    frmLblTitle.origin.y = frmTxtNominal.origin.y + frmTxtNominal.size.height/3 - 3;

    frmLblCurrency.origin.y = frmLblTitle.origin.y;

    frmVwBase.size.height = frmLblMinimum.origin.y + frmLblMinimum.size.height + 16;

    viewUnggah.frame = frmVwUnggah;
    unggahFotoLbl.frame = frmLblUnggahFoto;
    imgUnggahFoto.frame = frmImgFoto;
    lblTitle.frame = frmLblTitle;
    lblCurrency.frame = frmLblCurrency;
    txtNominal.frame = frmTxtNominal;
    vwLine.frame = frmVwLine;
    lblNominalMinimum.frame = frmLblMinimum;
    vwBase.frame = frmVwBase;
    btnDone.frame=  frmBtn;

//    [vwBase addSubview:vwRadio];
    [viewUnggah addSubview:unggahFotoLbl];
    [viewUnggah addSubview:imgUnggahFoto];
    [vwBase addSubview:viewUnggah];
    [vwBase addSubview:lblTitle];
    [vwBase addSubview:lblCurrency];
    [vwBase addSubview:txtNominal];
    [vwBase addSubview:btnDone];
    [vwBase addSubview:vwLine];
    [vwBase addSubview:lblNominalMinimum];
    
    [viewUnggah setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapUnggah =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(unggahTapped:)];
    [viewUnggah addGestureRecognizer:tapUnggah];
    
    [btnDone setHidden:YES];
    
    vwBase.frame = CGRectMake(vwBase.frame.origin.x, vwBase.frame.origin.y, vwBase.frame.size.width, lblNominalMinimum.frame.origin.y + lblNominalMinimum.frame.size.height + 10);
}

- (void)unggahTapped:(UITapGestureRecognizer *)sender
{
    PopUnggahObjFotoViewController *popUnggah = [self.storyboard instantiateViewControllerWithIdentifier:@"POPOBJFOTO"];
//    [popZakat dataBundle:mutDataDict];
//    [popZakat setDelegate:self];
    [popUnggah setDelegate:self];
    [popUnggah setAttachedView:sender.view];
    if(sender.view.info){
        [popUnggah setAttachedImage:[sender.view.info objectForKey:@"image"]];
        [popUnggah setAttachedLocation:[sender.view.info objectForKey:@"location"]];
    }
    [self presentViewController:popUnggah animated:YES completion:nil];
}

- (void) unggahFotoDone:(UIView*)view{
    for(UIView* viewChild in view.subviews){
        if ([viewChild isKindOfClass:[UILabel class]]){
            UILabel* label = (UILabel*) viewChild;
            [label setText:@"Foto Already Added"];
        }
    }
}

- (void)doneWithData:(NSDictionary *)dict withView:(UIView*) view{
    NSInteger idxVwTambah = 3 + [view.accessibilityValue integerValue];
    NSLog(@"%@", self.vwContentScroll.subviews);
    UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
    
    for(UIView* view in vwRb.subviews){
        if([view.accessibilityIdentifier isEqualToString:@"objFoto"]){
            view.info = dict;
            [self unggahFotoDone:view];
        }
    }
//    for(UIView* view in vwField.subviews){
//        NSLog(@"%@", view);
//        NSLog(@"%@", view.accessibilityLabel);
//        if([view isKindOfClass:[UIView class]]){
//            UIView *viewUnggah = (UILabel*) view;
//
//            if([viewUnggah.accessibilityIdentifier isEqualToString:@"objFoto"]){
//                viewUnggah.accessibilityLabel = string;
//            }
//        }
//    }
}

- (void) changeNilaiKesepakatan : (int) tag withValue:(NSString*) value {
    NSInteger idxVwTambah = 3 + tag;
     UIView *vwTambahBarang = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
     UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
     UILabel *lblNomMinimum = [vwParentRbAsset.subviews objectAtIndex:[vwParentRbAsset.subviews count]-1];
    nMaxKesepakatan = value;
     NSString *strCaption;
     if ([lang isEqualToString:@"id"]) {
         strCaption = @"Maksimum Nilai Kesepakatan IDR.";
     }else{
         strCaption = @"Maximum Agreement Value IDR.";
     }
    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[value doubleValue]]]];
}


-(void) actionCheckedAsset : (UIButton *) sender{
    NSInteger idxVwTambah = 3 + sender.tag;
    UIView *vwTambahBarang = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
    UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
    UIView *vwChildRbAsset = [vwParentRbAsset.subviews objectAtIndex:0];
    UILabel *lblNomMinimum = [vwParentRbAsset.subviews objectAtIndex:[vwParentRbAsset.subviews count]-1];

    NSString *strCaption;
    if ([lang isEqualToString:@"id"]) {
        strCaption = @"Maksimum Nilai Kesepakatan IDR.";
    }else{
        strCaption = @"Maximum Agreement Value IDR.";
    }
    
    for (int i = 0; i<[vwChildRbAsset.subviews count]; i++) {
        NSArray *arID = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
        NSInteger xTag =  [[arID objectAtIndex:1]integerValue];
        UIButton *btn = (UIButton *) [vwChildRbAsset.subviews objectAtIndex:i];
        NSDictionary *dictAsset = [listAssetType objectAtIndex:xTag-1];
        nMaxKesepakatan = [dictAsset valueForKey:@"Value"];
        if([btn isKindOfClass:[UIButton class]]){
            if (i == 0) {
                if (btn.tag == xTag) {
                    [btn setSelected:true];
                    idxChoiceAsset = xTag;
                    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[[dictAsset valueForKey:@"Value"]doubleValue]]]];
                    
                }else{
                    [btn setSelected:false];
                }
            }
            else if (i%3 == 0) {
                if (btn.tag == xTag) {
                    [btn setSelected:true];
                   idxChoiceAsset = xTag;
                    [lblNomMinimum setText:[NSString stringWithFormat:@"%@ %@", strCaption,[Utility formatCurrencyValue:[[dictAsset valueForKey:@"Value"]doubleValue]]]];
                }else{
                    [btn setSelected:false];
                }
               
            }
        }
       
    }
    
    
    CGRect frmVwBase = vwTambahBarang.frame;
    CGRect frmLblNom = lblNomMinimum.frame;
    if (IPHONE_5) {
        frmLblNom.origin.x = 16;
        frmLblNom.size.height = [Utility heightLabelDynamic:lblNomMinimum sizeWidth:frmVwBase.size.width - (frmLblNom.origin.x *2) sizeFont:10];
        frmLblNom.size.width = frmVwBase.size.width - (frmLblNom.origin.x *2);
       
    }
     lblNomMinimum.frame = frmLblNom;
    
}

-(void) saveChoiceAsset: (UIButton *) sender{
    NSInteger idx = idxChoiceAsset;
    bool isValid = true;
    bool isDone = false;
    if (idx == 0) {
        isValid = false;
    }
    
    if (isValid) {
        NSInteger idxRb = idx -1;
        NSDictionary *dataAsset = [listAssetType objectAtIndex:idxRb];
        NSMutableDictionary *mutDataChoice = [[NSMutableDictionary alloc]init];
        [mutDataChoice setValue:[dataAsset valueForKey:@"Code"] forKey:@"code"]; //changes this code or add new code
        [mutDataChoice setValue:sender.accessibilityLabel forKey:@"newCode"];
        [mutDataChoice setValue:[dataAsset valueForKey:@"Name"] forKey:@"name"];
        
        NSInteger idxVwTambah = 3 + sender.tag;
        UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
        UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
        
        UIView *vwField;
        for(UIView* view in vwRb.subviews){
            NSLog(@"nyari JVF%@", view);
            if([view.accessibilityLabel isEqualToString:@"field"]){
                vwField = view;
            }
            if([view.accessibilityIdentifier  isEqualToString:@"objFoto"]){
                NSLog(@"dict info >>> %@", view.info);
                [mutDataChoice setValue:[view.info objectForKey:@"image"] forKey:@"image"];
                [mutDataChoice setValue:[view.info objectForKey:@"location"] forKey:@"location"];
//                mutDataChoice setValue:view.accesibbil forKey:<#(nonnull NSString *)#>
            }
            if([view isKindOfClass:[JVFloatLabeledTextField class]]){
                JVFloatLabeledTextField *textNominal = (JVFloatLabeledTextField *) view;
                NSString *strNom = [textNominal.text stringByReplacingOccurrencesOfString:@"," withString:@""];
                if ([strNom integerValue] != 0 && [nMaxKesepakatan integerValue]!= 0) {
                    if ([strNom doubleValue] > [nMaxKesepakatan doubleValue]) {
                        [mutDataChoice setValue:nMaxKesepakatan forKey:@"value"];
                    }else{
                        [mutDataChoice setValue:strNom forKey:@"value"];
                    }
                }
            }
        }
        UITextField* textField;
        for(UIView* view in vwField.subviews){
            if([view isKindOfClass:[UITextField class]]){
                textField = (UITextField*) view;
                NSLog(@"accessibility value :?? %@", textField.accessibilityValue);
                NSLog(@"accessibility label :?? %@", textField.accessibilityLabel);
                if(textField.accessibilityValue != nil){
                    [mutDataChoice setValue:textField.accessibilityValue forKey:textField.accessibilityLabel];
                }else{
                    [mutDataChoice setValue:textField.text forKey:textField.accessibilityLabel];
                }
                
                if([[mutDataChoice valueForKey:textField.accessibilityLabel]isEqualToString:@""]){
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"DATA_IS_NOT_VALID") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                    isDone = false;
                }else{
                    isDone = true;
                }
            }
        }
        
        if(vwField.subviews.count == 0){
            isDone = true;
        }

//        JVFloatLabeledTextField *textNominal = (JVFloatLabeledTextField *) [vwRb.subviews objectAtIndex:4];
//        NSString *strNom = [textNominal.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//        if ([strNom integerValue] != 0 && [nMaxKesepakatan integerValue]!= 0) {
//            if ([strNom doubleValue] > [nMaxKesepakatan doubleValue]) {
//                [mutDataChoice setValue:nMaxKesepakatan forKey:@"value"];
//            }else{
//                [mutDataChoice setValue:strNom forKey:@"value"];
//            }
//        }
        
        if(isDone){
            if (isEdited) {
                [arDataChoiceAsset replaceObjectAtIndex:sender.tag-1 withObject:mutDataChoice];
                NSLog(@"%@", arDataChoiceAsset);
                [self summaryTotalAmount];
                [self relocatePosition:sender.tag];
                [self rechangePostAxisY];
                isEdited = false;
            }else{
                [arDataChoiceAsset insertObject:mutDataChoice atIndex:sender.tag-1];
                NSLog(@"%@", arDataChoiceAsset);
                [self summaryTotalAmount];
                [self relocatePosition:sender.tag];
                mTag = mTag + 1;
                [self createView:mTag];
            }
        }
    }
}

-(void)summaryTotalAmount{
    double mTotal = 0;
    for(NSDictionary *dictAsset in arDataChoiceAsset){
        mTotal = mTotal + [[dictAsset valueForKey:@"value"]doubleValue];
    }
    
    nTotalAmnt = mTotal;
    
    [ _lblNominalTotal setText:[NSString stringWithFormat:@"IDR. %@", [Utility formatCurrencyValue:mTotal]]];
    [_lblNominalTotal setTextColor:[UIColor blackColor]];
    [_lblNominalTotal sizeToFit];
    _lblNominalTotal.numberOfLines = 1;
    CGRect frmLblNominalTotal = _lblNominalTotal.frame;
    frmLblNominalTotal.origin.x = _vwContentScroll.frame.size.width - frmLblNominalTotal.size.width - 32;
    _lblNominalTotal.frame = frmLblNominalTotal;
}

-(void)relocatePosition : (NSInteger) mIdx{
    NSInteger idxVwTambah = 3 + mIdx;
    NSDictionary *data = [arDataChoiceAsset objectAtIndex:mIdx-1];
    UIView *vwTambahBarang = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
    UILabel *lblNomTotal = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-2];
    UIView *vwDataPasangan = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1];
    UIView *vwParentRbAsset = [vwTambahBarang.subviews objectAtIndex:[vwTambahBarang.subviews count]-1];
    
    UIButton *btnPlus = (UIButton *) [vwTambahBarang.subviews objectAtIndex:0];
    UIButton *btnEdit = (UIButton *) [vwTambahBarang.subviews objectAtIndex:1];
    [btnEdit setHidden:false];
    
    UILabel *lblBarang = (UILabel *) [vwTambahBarang.subviews objectAtIndex:2];
    [lblBarang setText:[data valueForKey:@"name"]];
    [lblBarang setTextColor:[UIColor blackColor]];
    [lblBarang sizeToFit];
    lblBarang.numberOfLines = 1;
    
    [vwParentRbAsset removeFromSuperview];
    
    CGRect frmVwTambahBarang = vwTambahBarang.frame;
    CGRect frmBtnPlus = btnPlus.frame;
    CGRect frmBtnEdit = btnEdit.frame;
    CGRect frmLblBarang = lblBarang.frame;
    CGRect frmLblNomTotal = lblNomTotal.frame;
    CGRect frmVwDataPasangan = vwDataPasangan.frame;
    
    frmVwTambahBarang.size.height = 43;
    
    frmBtnEdit.origin.x = frmBtnPlus.origin.x - frmBtnEdit.size.width - 8;
    frmBtnEdit.origin.y = frmBtnPlus.origin.y;
    
    frmLblBarang.origin.y = frmVwTambahBarang.size.height/2 - 8;
    frmLblBarang.size.width = frmVwTambahBarang.size.width - frmLblBarang.origin.x - (frmBtnPlus.size.width + frmBtnEdit.size.width) - 8;
    
    frmLblNomTotal.origin.y = frmVwTambahBarang.origin.y + frmVwTambahBarang.size.height + 16;
    
    frmVwDataPasangan.origin.y = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;
    
    btnEdit.frame = frmBtnEdit;
    lblBarang.frame = frmLblBarang;
    vwTambahBarang.frame = frmVwTambahBarang;
    lblNomTotal.frame = frmLblNomTotal;
    vwDataPasangan.frame = frmVwDataPasangan;
}

-(void) actionExpandView:(UIButton *) sender{
    isEdited = false;
    NSArray *xAccesbility = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
    NSInteger xId = [[xAccesbility objectAtIndex:1]integerValue];
    NSInteger idxVwTambah = 3 + xId;
    NSInteger idxNomTot = [self.vwContentScroll.subviews count] - 1;
    
    UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
    UILabel *lblNomTotal = [self.vwContentScroll.subviews objectAtIndex:idxNomTot-1];
    UIView *vwDataPasangan = [self.vwContentScroll.subviews objectAtIndex:idxNomTot];
    UILabel *lblBarang = [vwTambah.subviews objectAtIndex:2];
    
    NSArray *arLastTag = (NSArray *) lastTag;
    
    if (arLastTag.count == xId) {
        NSNumber *iLastTag = [arLastTag objectAtIndex:xId-1];
        if (xId == [iLastTag integerValue]) {
             NSLog(@"%d", [iLastTag intValue]);
            if ([sender isSelected]) {
                [sender setSelected:false];
                UIView *rbView = (UIView *) [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
                if (rbView != nil) {
                    [rbView removeFromSuperview];
                    CGRect frmVwTambah = vwTambah.frame;
                    CGRect frmLblNomTotal = lblNomTotal.frame;
                    CGRect frmVwDataPasangan = vwDataPasangan.frame;
                    
                    frmVwTambah.size.height = 43;
                    frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
                    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;

                    vwTambah.frame = frmVwTambah;
                    lblNomTotal.frame = frmLblNomTotal;
                    vwDataPasangan.frame = frmVwDataPasangan;
                }
            }else{
                if ([lblBarang.text isEqualToString:@"Object Refinancing"]) {

                    bool isValid = true;
                    NSString *nMessage;
                    if ([self.vwEdCntn1.text isEqualToString:@""] || self.vwEdCntn1.text == nil || [self.vwEdCntn1.text isEqualToString: @"0"]) {
                        isValid = false;
                        if ([lang isEqualToString:@"id"]) {
                             nMessage = @"Masukan Pengajuan terlebih dahulu";
                        }else{
                             nMessage = @"Enter Financing Amount first";
                        }
                        [Utility showMessage:nMessage mVwController:self mTag:05];
                       
                    }
                    else if ([self.edDropDownWaktu.text isEqualToString:@""] || self.edDropDownWaktu.text == nil) {
                        isValid = false;
                        if ([lang isEqualToString:@"id"]) {
                            nMessage = @"Pilih jangka waktu terlebih dahulu";
                        }else{
                            nMessage = @"Select Tenor first";
                        }
                        [Utility showMessage:nMessage mVwController:self mTag:05];
                    }
                    else if([self.edDropDownMaskapai.text isEqualToString:@""] || self.edDropDownMaskapai.text == nil){
                        isValid = false;
                        if ([lang isEqualToString:@"id"]) {
                            nMessage = @"Pilih asuransi terlebih dahulu";
                        }else{
                            nMessage = @"Select insurance first";
                        }
                        [Utility showMessage:nMessage mVwController:self mTag:05];
                    }
                    
                    if (isValid) {
                        idxChoiceAsset = 0;
                        nominalChoiceAsset = 0;
                        
                        [sender setSelected:true];
                        [sender setTintColor:[UIColor clearColor]];
//                        UIView *rbView = [self showVwJaminan:vwTambah];
                        UIView *rbView = [self showAset:vwTambah];
                        [vwTambah addSubview:rbView];
                        
                        CGRect frmVwContentScroll = self.vwContentScroll.frame;
                        CGRect frmVwTambah = vwTambah.frame;
                        CGRect frmVwRb = rbView.frame;
                        CGRect frmLblNomTotal = lblNomTotal.frame;
                        CGRect frmVwDataPasangan = vwDataPasangan.frame;
                        
                        frmVwTambah.size.height = frmVwRb.size.height + 43;
                        frmLblNomTotal.origin.y = frmVwTambah.origin.y + frmVwTambah.size.height + 16;
                        frmLblNomTotal.origin.x = self.vwContentScroll.frame.size.width - frmLblNomTotal.size.width - 32;
                        frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
                        
//                        frmVwContentScroll.size.height = frmLblNomTotal.origin.y + frmLblNomTotal.size.height + 16;
                        frmVwContentScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height + 16;
                        
                        vwTambah.frame =  frmVwTambah;
                        lblNomTotal.frame = frmLblNomTotal;
                        vwDataPasangan.frame = frmVwDataPasangan;
                        self.vwContentScroll.frame = frmVwContentScroll;
                        
                        [self.vwScroll setContentSize:CGSizeMake(SCREEN_WIDTH, frmVwContentScroll.origin.y + frmVwContentScroll.size.height)];
                        
                    }else{
//                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                        alert.tag = 02;
//                        [alert show];
                    }
                }
                
            }
            
        }
    }else{
        //make delete
        if ([sender isSelected]) {
            
            CGRect frmVwTambah = vwTambah.frame;
            if (frmVwTambah.size.height == 43) {
                if (mTag > 1) {
                    
                    UIView *lastView = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-3];
                    
                    [lastView removeFromSuperview];
                    
                    //                    [dataChoiceAsset removeObjectForKey:[NSString stringWithFormat:@"%ld",xId]];
                    [arDataChoiceAsset removeObjectAtIndex:xId-1];
                    
                    NSLog(@"%@", self.vwContentScroll.subviews);
                    
                    [lastTag removeAllObjects];
                    
                    mTag = mTag - 1;
                    
                    NSLog(@"%@", arDataChoiceAsset);
                    
                    for (int i=4; i<[self.vwContentScroll.subviews count]-2; i++) {
                        
                        //NSInteger idxView = i + 1;
                        NSInteger newTag = i - 3;
                        
                        [lastTag insertObject:@(newTag) atIndex:newTag-1];
                        
                        UIView *vwTambah = (UIView *) [self.vwContentScroll.subviews objectAtIndex:i];
                        vwTambah.tag = newTag;
                        UIButton *btnExpand =  [vwTambah.subviews objectAtIndex:0];
                        UIButton *btnEdit =  [vwTambah.subviews objectAtIndex:1];
                        UILabel *lblBarang = [vwTambah.subviews objectAtIndex:2];
                        
                        btnExpand.tag = newTag;
                        btnExpand.accessibilityLabel = [NSString stringWithFormat:@"ext_%ld", newTag];
                        
                        btnEdit.tag = newTag;
                        btnExpand.accessibilityLabel = [NSString stringWithFormat:@"edt_%ld", newTag];
                        
                        lblBarang.tag = newTag;
                        lblBarang.accessibilityLabel = [NSString stringWithFormat:@"nom_%ld", newTag];
                        NSInteger lenArDataChoice = arDataChoiceAsset.count;
                        if (newTag <= lenArDataChoice) {
                            NSDictionary *data = (NSDictionary *) [arDataChoiceAsset objectAtIndex:newTag-1];
                            [lblBarang setText:[data valueForKey:@"name"]];
                            [lblBarang setTextColor:[UIColor blackColor]];
                        }else{
                            [lblBarang setText:@"Object Refinancing"];
//                            if ([lang isEqualToString:@"id"]) {
//                                [lblBarang setText:@"Tambah Barang"];
//                            }else{
//                                [lblBarang setText:@"Add Items"];
//                            }
                            [lblBarang setTextColor:UIColorFromRGB(const_color_gray)];
                            [btnExpand setSelected:false];
                            [btnEdit setHidden:YES];
                        }
                        
                        NSLog(@"%@", vwTambah.subviews);
                        
                        
                    }
                    UIView *lstVtambah = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-3];
                    UILabel *lblNominal = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-2];
                    UIView *vwDataPasangan = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1];

                    CGRect frmLstVTambah = lstVtambah.frame;
                    CGRect frmLblNominal = lblNominal.frame;
                    CGRect frmVwDataPasangan = vwDataPasangan.frame;
                    
                    frmLblNominal.origin.y = frmLstVTambah.origin.y + frmLstVTambah.size.height + 16;
                    frmVwDataPasangan.origin.y = frmLblNominal.origin.y + frmLblNominal.size.height + 16;
                    
                    lblNominal.frame = frmLblNominal;
                    vwDataPasangan.frame = frmVwDataPasangan;
                    
                    [self summaryTotalAmount];
                    
                }
            }else{
                NSLog(@"%@", vwTambah.subviews);
                UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
                [vwRb removeFromSuperview];
                
                frmVwTambah.size.height = 43;
                vwTambah.frame = frmVwTambah;
                
                [self rechangePostAxisY];
                [sender setSelected:true];
            }
            
        }
    }
}

-(void)actionEditView:(UIButton *) sender{
    isEdited = true;
    NSArray *xAccesbility = [sender.accessibilityLabel componentsSeparatedByString:@"_"];
    NSInteger xId = [[xAccesbility objectAtIndex:1]integerValue];
    NSInteger idxVwTambah = 3 + xId;
    editCurrentTag = idxVwTambah;
    UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:idxVwTambah];
   
    
    if (vwTambah.frame.size.height == 43) {
        UIView *rbView = [self showAset:vwTambah];
        [vwTambah addSubview:rbView];
        
        UIView *vwRbChoice = [rbView.subviews objectAtIndex:0];
        NSDictionary *data = [arDataChoiceAsset objectAtIndex:xId-1];
//        NSInteger mCode = [[data valueForKey:@"code"]integerValue];
        NSInteger mCode = [[data valueForKey:@"newCode"]integerValue];
        UIButton *selectedBtn = [self getSelectedBtn:[NSString stringWithFormat:@"b_%ld",(long)mCode] fromSuperview:vwRbChoice];
        if(selectedBtn != nil){
            [self actionRadioBtn:selectedBtn];
        }
    
        UIView *vwRb = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
        UIView *vwField;
        for(UIView* view in vwRb.subviews){
            if([view.accessibilityLabel isEqualToString:@"field"]){
                vwField = view;
            }
            if([view.accessibilityIdentifier  isEqualToString:@"objFoto"]){
                view.info = @{ @"image" : [data objectForKey:@"image"],
                               @"location" : [data objectForKey:@"location"]};
                [self unggahFotoDone:view];
            }
            
            if([view isKindOfClass:[JVFloatLabeledTextField class]]){
                JVFloatLabeledTextField *txtNominal = (JVFloatLabeledTextField*) view;
                [txtNominal setText:[Utility formatCurrencyValue:[[data valueForKey:@"value"]doubleValue]]];
            }
            
            //buttonDone
            if([view isKindOfClass:[UIButton class]]){
                [view setHidden:NO];
            }
        }
        UITextField* textField;
        for(UIView* view in vwField.subviews){
            if([view isKindOfClass:[UITextField class]]){
                textField = (UITextField*) view;
                
                textField.accessibilityValue = [data objectForKey:textField.accessibilityLabel];
//                textField.accessibilityValue = [NSString stringWithFormat:@"%ld",(long)xId];
                textField.text = [data objectForKey:textField.accessibilityLabel];
        
            }
        }
        
        NSLog(@"%@", rbView.subviews);
//        JVFloatLabeledTextField *txtNominal = [rbView.subviews objectAtIndex:4];
//        if ([txtNominal isKindOfClass:[JVFloatLabeledTextField class]]) {
//            [txtNominal setText:[Utility formatCurrencyValue:[[data valueForKey:@"value"]doubleValue]]];
//        }
        
//        UIButton * btnDone = [rbView.subviews objectAtIndex:[vwAddonAsset.subviews count]-2];
        
//        UIButton *btnSubmit = [rbView.subviews objectAtIndex:4];
//        if ([btnSubmit isKindOfClass:[UIButton class]]) {
//            [btnSubmit setHidden:false];
//        }
        
        CGRect frmVwTambah = vwTambah.frame;
        CGRect frmRbView = rbView.frame;
        
        frmVwTambah.size.height = frmRbView.size.height + 79;
        
        vwTambah.frame = frmVwTambah;
        
        UIView *rbVw = [vwTambah.subviews objectAtIndex:[vwTambah.subviews count]-1];
        
        NSLog(@"%@", rbVw.subviews);
        
        [self rechangePostAxisY:(int)idxVwTambah+1];
    }
}

-(void) rechangePostAxisY{
    UILabel *lblMinNom = [self.vwContentScroll.subviews objectAtIndex:3];
    NSLog(@"rechange %@", lblMinNom.text);
    
    CGFloat lblMinNomPosY = lblMinNom.frame.origin.y + lblMinNom.frame.size.height + 16;
    CGFloat lastHight = 0;
    for (int x = 4; x < [self.vwContentScroll.subviews count]-1; x++) {
        UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:x];
        CGRect frmVwTambah = vwTambah.frame;
        if (x > 4) {
            lblMinNomPosY = lblMinNomPosY + lastHight + 16;
        }
        frmVwTambah.origin.y = lblMinNomPosY;
        lastHight = frmVwTambah.size.height;
        vwTambah.frame = frmVwTambah;
        NSLog(@"+barang %f", vwTambah.frame.size.height);

    }

    CGRect frmLblNomTotal = self.lblNominalTotal.frame;
    CGRect frmVwDataPasangan = self.vwDataPasangan.frame;
    CGRect frmVwContenScroll = self.vwContentScroll.frame;
    
    frmLblNomTotal.origin.y = lblMinNomPosY + lastHight + 16;
    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height;
//    frmVwContenScroll.size.height = frmLblNomTotal.origin.y + frmLblNomTotal.size.height;
    frmVwContenScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height;
    
    self.lblNominalTotal.frame = frmLblNomTotal;
    self.vwDataPasangan.frame = frmVwDataPasangan;
    self.vwContentScroll.frame = frmVwContenScroll;
    
    [self.vwScroll setContentSize:CGSizeMake(SCREEN_WIDTH, self.vwContentScroll.frame.size.height)];
   
}

-(void) rechangePostAxisY : (int) idSelected{
    UILabel *lblMinNom = [self.vwContentScroll.subviews objectAtIndex:3];
    NSLog(@"rechange %@", lblMinNom.text);
    CGFloat lblMinNomPosY = lblMinNom.frame.origin.y + lblMinNom.frame.size.height + 16;
    CGFloat lastHight = 0;
    for (int x = 4; x < [self.vwContentScroll.subviews count]-1; x++) {
        UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:x];
        CGRect frmVwTambah = vwTambah.frame;
        if (x > 4) {
            if (x == idSelected) {
//                lblMinNomPosY = lblMinNomPosY + 43 + 24;
                lblMinNomPosY = lblMinNomPosY + lastHight + 24;
            }else{
                lblMinNomPosY = lblMinNomPosY + 43 + 16;
            }
        }
        frmVwTambah.origin.y = lblMinNomPosY;
        lastHight = frmVwTambah.size.height;
        vwTambah.frame = frmVwTambah;
        
    }
    CGRect frmLblNomTotal = self.lblNominalTotal.frame;
    CGRect frmVwDataPasangan = self.vwDataPasangan.frame;
    CGRect frmVwContenScroll = self.vwContentScroll.frame;
    
    frmLblNomTotal.origin.y = lblMinNomPosY + lastHight + 16;
    frmVwDataPasangan.origin.y = frmLblNomTotal.origin.y + frmLblNomTotal.size.height;
    frmVwContenScroll.size.height = frmVwDataPasangan.origin.y + frmVwDataPasangan.size.height;
    
    self.lblNominalTotal.frame = frmLblNomTotal;
    self.vwDataPasangan.frame = frmVwDataPasangan;
    self.vwContentScroll.frame = frmVwContenScroll;
    
    [self.vwScroll setContentSize:CGSizeMake(SCREEN_WIDTH, self.vwContentScroll.frame.size.height)];
    
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 05) {
        NSLog(@"Field Empty");
    }else if(alertView.tag == 06){
        [self.vwEdCntn1 setText:@"0"];
    }
    else if (alertView.tag == 07){
        stateFocus = false;
        [self.vwEdCntn1 becomeFirstResponder];
    }
    else{
        [self backToRoot];
    }
    
}

- (IBAction)actionBatal:(id)sender {
//    [self backToRoot];
//    [self backToRoot:[[self.jsonData valueForKey:@"back"]boolValue]];
//    [self backToR];
    dataManager.currentPosition --;
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];}

- (IBAction)actionNext:(id)sender {
//    [self openNextTemplate];

    bool isValid = true;
    NSString *strNeedDana = [self.vwEdCntn1.text stringByReplacingOccurrencesOfString:@"," withString:@""];
    if([arDataChoiceAsset count] == 0){
        isValid = false;
        if ([lang isEqualToString:@"id"]) {
            [Utility showMessage:@"Pilih object/asset terlebih dahulu" mVwController:self mTag:05];
        }else{
            [Utility showMessage:@"Select object/asset first" mVwController:self mTag:05];
        }
    }else{
        double needDana = [strNeedDana doubleValue];
        if (needDana == 0 || [strNeedDana isEqualToString:@""] || strNeedDana == nil ) {
            if ([lang isEqualToString:@"id"]) {
                [Utility showMessage:@"Masukan Pengajuan terlebih dahulu" mVwController:self mTag:05];
            }else{
                [Utility showMessage:@"Enter Financing amount first" mVwController:self mTag:05];
            }

        }else{
            if (needDana > nMaxDana ) {
                isValid  = false;
                if ([lang isEqualToString:@"id"]) {
                    [Utility showMessage:@"Nominal Pengajuan melebihi batas yang di tawarkan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Financing Ammount Exceeds the limit offered" mVwController:self mTag:05];
                }
            }else if(needDana < minLoan){
                isValid  = false;
                if ([lang isEqualToString:@"id"]) {
                    [Utility showMessage:@"Nominal Pengajuan yang tidak boleh kurang dari minimum loan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Financing Amount should not be less than the loan minimum" mVwController:self mTag:05];
                }
            }
            else{
                if (nTotalAmnt < needDana + 1) {
                    isValid = false;
                    if ([lang isEqualToString:@"id"]) {
//                        [Utility showMessage:@"Nilai barang kurang dari nilai dana yang dibutuhkan" mVwController:self mTag:05];
                        [Utility showMessage:@"Mohon Maaf, nilai object/asset yang anda masukan kurang dari akseptasi Bank. Silahkan masukkan tambahan object/asset" mVwController:self mTag:05];
                    }else{
                        ;
//                        [Utility showMessage:@"Value of items is less than the value of funds needed" mVwController:self mTag:05];
                        [Utility showMessage:@"Sorry, the value of the object you entered is less than the Bank's acceptance. Please enter additional assets" mVwController:self mTag:05];
                    }
                }
            }
        }

        if(nMartialState != 0 && !maritalStateFilled){
            isValid = false;
            if(nMartialState == 1){
                if ([lang isEqualToString:@"id"]) {
                    [Utility showMessage:@"Anda belum mengisi data pasangan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Please Fill Spouse Data" mVwController:self mTag:05];
                }
            }else{
                if ([lang isEqualToString:@"id"]) {
                    [Utility showMessage:@"Anda belum mengisi data tanggungan" mVwController:self mTag:05];
                }else{
                    [Utility showMessage:@"Please Fill Dependent Data" mVwController:self mTag:05];
                }
            }
            
        }
    }

    if (isValid) {
        [dataManager.dataExtra setValue:strNeedDana forKey:@"loan_amount"];
        [dataManager.dataExtra setValue:[[listPeriod objectAtIndex:nIdxTenor]valueForKey:@"Code"]forKey:@"loan_tenor"];
        [dataManager.dataExtra setValue:[[listInsurance objectAtIndex:nIdxAsuransi]valueForKey:@"Code"] forKey:@"insurance_code"];
        [dataManager.dataExtra setValue:[Utility convertJsonString:arDataChoiceAsset] forKey:@"asset_type"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%d", (int)nTotalAmnt] forKey:@"asset_amount"];

         [self openNextTemplate];
    }
    
}

-(void)actionAgreeAkad : (UIButton *) sender{
    [self.btnChecked setSelected:!self.btnChecked.isSelected];
    if ([_btnChecked isSelected]) {
        [_btnNext setEnabled:true];
//        [_btnNext setBackgroundColor:UIColorFromRGB(amanah_primary_color)];
    }else{
        [_btnNext setEnabled:false];
//        [_btnNext setBackgroundColor:[UIColor grayColor]];
    }
    
}

- (UIView *) vwInputDataPasangan2{
//    UIView *vwTambah = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1];
    UIView *vwBase = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.vwContentScroll.frame.size.width, 34)];
    UILabel *lblDataPas = [[UILabel alloc]init];
//    UILabel *lblIsiDataPas = [[UILabel alloc]init];
    lblIsiDataPas = [[UILabel alloc]init];
    UIButton *btnShowPop = [[UIButton alloc]init];
    
    if(nMartialState == 1){
        if ([lang isEqualToString:@"id"]) {
            [lblDataPas setText:@"Data Pasangan"];
            [lblIsiDataPas setText:@"Isi Data Pasangan"];
        }else{
            [lblDataPas setText:@"Spouse"];
            [lblIsiDataPas setText:@"Fill in Spouse Data"];
        }
    }else{
        if ([lang isEqualToString:@"id"]) {
            [lblDataPas setText:@"Data Tanggungan"];
            [lblIsiDataPas setText:@"Isi Data Tanggungan"];
        }else{
            [lblDataPas setText:@"Dependent Data"];
            [lblIsiDataPas setText:@"Fill in Dependent Data"];
        }
    }
    
    [lblDataPas setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    [lblDataPas sizeToFit];
    
    [lblIsiDataPas setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    [lblIsiDataPas sizeToFit];
    [lblIsiDataPas setTextColor:UIColorFromRGB(const_color_primary)];
    [lblIsiDataPas setTextAlignment:NSTextAlignmentRight];
    
    vwBase.layer.cornerRadius = 8;
    vwBase.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
    vwBase.layer.borderWidth = 1;
    
    [btnShowPop setTitle:@"" forState:UIControlStateNormal];
    [btnShowPop addTarget:self action:@selector(showPopupPasangan:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frmLblDataPas = lblDataPas.frame;
    CGRect frmLblIsiDataPas = lblIsiDataPas.frame;
    CGRect frmVwBase = vwBase.frame;
    CGRect frmBtnShowPop = btnShowPop.frame;
    
    frmVwBase.origin.x = 16;
    frmVwBase.size.width = self.vwContentScroll.frame.size.width - (frmVwBase.origin.x *2);
    
    frmLblDataPas.origin.x = 16;
    frmLblDataPas.origin.y = 10;
    frmLblDataPas.size.width = frmVwBase.size.width/2 - (frmLblDataPas.origin.x *2);
    
    frmLblIsiDataPas.origin.y = frmLblDataPas.origin.y;
    frmLblIsiDataPas.origin.x = frmLblDataPas.origin.x + frmLblDataPas.size.width + 24;
    frmLblIsiDataPas.size.width = frmVwBase.size.width - frmLblIsiDataPas.origin.x - 16;
    
    frmVwBase.size.height = frmLblDataPas.size.height + (frmLblDataPas.origin.y*2);
    
    frmBtnShowPop.origin.x = 0;
    frmBtnShowPop.origin.y = 0;
    frmBtnShowPop.size.width = frmVwBase.size.width;
    frmBtnShowPop.size.height = frmVwBase.size.height;
    
    lblDataPas.frame = frmLblDataPas;
    lblIsiDataPas.frame = frmLblIsiDataPas;
    btnShowPop.frame = frmBtnShowPop;
    vwBase.frame = frmVwBase;
    
    
    [vwBase addSubview:lblDataPas];
    [vwBase addSubview:lblIsiDataPas];
    [vwBase addSubview:btnShowPop];
    return vwBase;
}

-(UIView *) vwInputDataPasangan{
    UIView *vwBase = [[UIView alloc]initWithFrame:CGRectMake(0, self.vwAkadAgreement.frame.origin.y + self.vwAkadAgreement.frame.size.height + 16, self.vwContentScroll.frame.size.width, 40)];
    
    UILabel *lblDataPas = [[UILabel alloc]init];
    lblIsiDataPas = [[UILabel alloc]init];

    UIButton *btnShowPop = [[UIButton alloc]init];
    if ([lang isEqualToString:@"id"]) {
        [lblDataPas setText:@"Data Pasangan"];
        [lblIsiDataPas setText:@"Isi Data Pasangan"];
    }else{
        [lblDataPas setText:@"Data Pasangan"];
        [lblIsiDataPas setText:@"Isi Data Pasangan"];
    }
    [lblDataPas setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    [lblDataPas sizeToFit];
    
    [lblIsiDataPas setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    [lblIsiDataPas sizeToFit];
    [lblIsiDataPas setTextColor:UIColorFromRGB(const_color_primary)];
    [lblIsiDataPas setTextAlignment:NSTextAlignmentRight];
    
    vwBase.layer.cornerRadius = 8;
    vwBase.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
    vwBase.layer.borderWidth = 1;
    
    [btnShowPop setTitle:@"" forState:UIControlStateNormal];
    [btnShowPop addTarget:self action:@selector(showPopupPasangan:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect frmLblDataPas = lblDataPas.frame;
    CGRect frmLblIsiDataPas = lblIsiDataPas.frame;
    CGRect frmVwBase = vwBase.frame;
    CGRect frmBtnShowPop = btnShowPop.frame;
    
    frmVwBase.origin.x = 16;
    frmVwBase.size.width = self.vwContentScroll.frame.size.width - (frmVwBase.origin.x *2);
    
    frmLblDataPas.origin.x = 16;
    frmLblDataPas.origin.y = 10;
    frmLblDataPas.size.width = frmVwBase.size.width/2 - (frmLblDataPas.origin.x *2);
    
    frmLblIsiDataPas.origin.y = frmLblDataPas.origin.y;
    frmLblIsiDataPas.origin.x = frmLblDataPas.origin.x + frmLblDataPas.size.width + 24;
    frmLblIsiDataPas.size.width = frmVwBase.size.width - frmLblIsiDataPas.origin.x - 16;
    
    frmVwBase.size.height = frmLblDataPas.size.height + (frmLblDataPas.origin.y*2);
    
    frmBtnShowPop.origin.x = 0;
    frmBtnShowPop.origin.y = 0;
    frmBtnShowPop.size.width = frmVwBase.size.width;
    frmBtnShowPop.size.height = frmVwBase.size.height;
    
    lblDataPas.frame = frmLblDataPas;
    lblIsiDataPas.frame = frmLblIsiDataPas;
    btnShowPop.frame = frmBtnShowPop;
    vwBase.frame = frmVwBase;
    
    
    [vwBase addSubview:lblDataPas];
    [vwBase addSubview:lblIsiDataPas];
    [vwBase addSubview:btnShowPop];
    
    return vwBase;
}

- (void) martialStateHandler{
    if (nMartialState != 0){
        [self.vwDataPasangan addSubview: [self vwInputDataPasangan2]];
    }
}

-(void)showPopupPasangan : (UIButton *) sender{
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"POPISIPAS"];
    PopupIsiPasanganViewController *viewCont = (PopupIsiPasanganViewController *) templateView;
    [viewCont setDelegate:self];
    [viewCont setMartialState:nMartialState];
    [self presentViewController:templateView animated:YES completion:nil];
}

- (void) doneState:(BOOL)state{
    maritalStateFilled = state;
}

- (void)namaPasangan:(NSString *)string{
    textDataPasangan = string;
    [lblIsiDataPas setText:string];
}

- (void)textFieldPurchaseYearValidation:(UITextField *)textField{
    
    if([textField.accessibilityLabel isEqualToString:@"Production_Year"] || [textField.accessibilityLabel isEqualToString:@"Purchase_Year"]){
        NSDate *currentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components

        NSString *stringMsg = @"";
        if(textField.text.length != 4){
            stringMsg = lang(@"DATA_IS_NOT_VALID");
        }
        else if([textField.text doubleValue] > components.year){
            stringMsg = lang(@"DATA_IS_NOT_VALID");
        }
        else if([textField.text doubleValue]+8 <= components.year){
            if([lang(@"LANGUAGE") isEqualToString:@"ID"]){
                stringMsg = [NSString stringWithFormat:@"Tahun harus dibawah 8 Tahun"];
            }else{
                stringMsg = [NSString stringWithFormat:@"Year must below 8 years"];
            }
        }
        if(![stringMsg isEqualToString:@""]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:stringMsg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
            textField.text = @"";
        }
    }
}

@end
