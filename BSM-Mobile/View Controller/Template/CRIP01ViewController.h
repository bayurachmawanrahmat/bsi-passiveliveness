//
//  CRIP01ViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CRIP01ViewController : TemplateViewController

@end

typedef NS_ENUM(NSInteger, pickerEnum){
    pickerNull = 0,
    pcikerLoan = 1,
    pickerMaskapai = 2,
    pickerExtraData = 3,
    pickerExtraDataSub = 4
};

NS_ASSUME_NONNULL_END
