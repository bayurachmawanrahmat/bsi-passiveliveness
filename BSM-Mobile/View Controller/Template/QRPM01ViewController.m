//
//  QRPM01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 3/5/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "QRPM01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>

@interface QRPM01ViewController ()<ConnectionDelegate, UITextFieldDelegate>{
    NSString* transactionId;
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnScanQR;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadQR;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

- (IBAction)scanQR:(id)sender;
- (IBAction)uploadQR:(id)sender;
- (IBAction)batal:(id)sender;

@end

@implementation QRPM01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblTitle.text = @"QR Pay";
        [self.btnScanQR setTitle:@"Pindai QR Code" forState:UIControlStateNormal];
        [self.btnUploadQR setTitle:@"Pilih File QR Code" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
    } else {
        self.lblTitle.text = @"QR Pay";
        [self.btnScanQR setTitle:@"Scan QR Code" forState:UIControlStateNormal];
        [self.btnUploadQR setTitle:@"Choose File QR Code" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmBtnScan = self.btnScanQR.frame;
    CGRect frmBtnUpload = self.btnUploadQR.frame;
    CGRect frmBtnBatal = self.btnCancel.frame;
    
    frmTitle.size.width = screenWidth;
    frmBtnUpload.origin.y = (screenHeight/2) - (frmBtnUpload.size.height/2);
    frmBtnUpload.size.width = screenWidth - 32;
    frmBtnScan.origin.y = frmBtnUpload.origin.y - frmBtnScan.size.height - 10;
    frmBtnScan.size.width = screenWidth - 32;
    frmBtnBatal.origin.y = frmBtnUpload.origin.y + frmBtnUpload.size.height + 10;
    frmBtnBatal.size.width = screenWidth - 32;
    
    self.vwTitle.frame = frmTitle;
    self.btnScanQR.frame = frmBtnScan;
    self.btnUploadQR.frame = frmBtnUpload;
    self.btnCancel.frame = frmBtnBatal;
}

- (NSString *) base64StringForImage_objc:(UIImage *)image {
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData base64EncodedStringWithOptions:0];
}

- (UIImage *) imageFromBase64String_objc:(NSString *)string {
    NSData *imageData = [[NSData alloc] initWithBase64EncodedString: string options: 0];
    return [[UIImage alloc] initWithData:imageData];
}

- (IBAction)scanQR:(id)sender {
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"QRPSCAN"];
    templateView.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:templateView animated:YES];
}

- (IBAction)uploadQR:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select File option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Choose File",nil];
    
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)batal:(id)sender {
    [super backToRoot];
}

#pragma  mark- Opne Action Sheet for Options

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    imagePicker = [[UIImagePickerController alloc] init];
                    
                    imagePicker.delegate = self;
                    
                    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    
                    [self presentViewController:imagePicker animated:YES completion:nil];
                    
                    break;
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark- Open Document Picker(Delegate) for PDF, DOC Slection from iCloud

- (void)showDocumentPickerInMode:(UIDocumentPickerMode)mode
{
    
    UIDocumentMenuViewController *picker =  [[UIDocumentMenuViewController alloc] initWithDocumentTypes:@[@"com.adobe.pdf"] inMode:UIDocumentPickerModeImport];
    
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker
{
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    PDFUrl= url;
    UploadType=@"PDF";
    [arrimg removeAllObjects];
    [arrimg addObject:url];
}

#pragma mark- Open Image Picker Delegate to select image from Gallery or Camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UploadType=@"Image";
    [arrimg removeAllObjects];
    [arrimg addObject:myImage];
    
    //UIImage *image = [UIImage imageNamed:@"qr3"];
    CIImage *ciimage = [CIImage imageWithData:UIImageJPEGRepresentation(myImage, 1.0f)];
    
    NSDictionary *detectorOptions = @{ CIDetectorAccuracy : CIDetectorAccuracyHigh };
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:detectorOptions];
    
    NSArray *features = [faceDetector featuresInImage:ciimage];
    CIQRCodeFeature *faceFeature;
    NSString *strQR = @"";
    for(faceFeature in features) {
        strQR = [NSString stringWithFormat:@"%@",faceFeature.messageString];
        NSLog(@"Found feature: %@", strQR);
        if (([strQR isEqualToString:@""]) || (strQR == nil)) {
            
        } else {
            [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": strQR}];
        }
        //break;
    }
    if (([strQR isEqualToString:@""]) || (strQR == nil)) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"This is not a valid QR Code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        } else {
            // Fallback on earlier versions
        }
        [alert show];
    } else {
        [super openNextTemplate];

    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Upload Image, PDF or Doc to Server

/*- (IBAction)submit:(id)sender
 {
 if ([UploadType isEqualToString:@"PDF"])
 {
 [self uploadpdf];
 }
 
 else
 {
 [self uploadimage];
 }
 }*/

#pragma mark - Upload Image

-(void)uploadimage
{
    //loader start
    APIManager *obj =[[APIManager alloc]init];
    [obj setDelegate:(id)self];
    NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
    [dict setValue:@"SOME PARAMETER" forKey:@"type"];
    [dict setValue:@"SOME PARAMETER" forKey:@"title"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",@"API URL"];
    NSURL *url =[NSURL URLWithString:urlString];
    [obj startRequestForImageUploadingWithURL:url withRequestType:(kAPIManagerRequestTypePOST) withDataDictionary:dict arrImage:arrimg CalledforMethod:imageupload index:0 isMultiple:NO str_imagetype:@"image"];
}

#pragma mark - Upload PDF

-(void)uploadpdf
{
    APIManager *obj =[[APIManager alloc]init];
    [obj setDelegate:(id)self];
    NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
    [dict setValue:@"SOME PARAMETER" forKey:@"type"];
    [dict setValue:@"SOME PARAMETER" forKey:@"title"];
    NSString *urlString = [NSString stringWithFormat:@"%@",@"API URL"];
    NSURL *url =[NSURL URLWithString:urlString];
    [obj startRequestForImageUploadingWithURL:url withRequestType:(kAPIManagerRequestTypePOST) withDataDictionary:dict arrImage:arrimg CalledforMethod:imageupload index:0 isMultiple:NO str_imagetype:@"pdf"];
}

#pragma mark-API Manager Delegate Method for Succes or Failure

-(void)APIManagerDidFinishRequestWithData:(id)responseData withRequestType:(APIManagerRequestType)requestType CalledforMethod:(APIManagerCalledForMethodName)APImethodName tag:(NSInteger)tag
{
    if (APImethodName ==imageupload) {
        NSDictionary *responsedata=(NSDictionary*)responseData;
        NSLog(@"data==%@",responsedata);
        
        NSString * Success= [responsedata valueForKey:@"success"];
        NSString * Message= [responseData valueForKey:@"message"];
        
        BOOL SuccessBool= [Success boolValue];
        
        if (SuccessBool)
        {
            
            //[self.navigationController popViewControllerAnimated:YES];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Server Error" message:Message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                                  }];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)APIManagerDidFinishRequestWithError:(NSError *)error withRequestType:(APIManagerRequestType)requestType CalledforMethod:(APIManagerCalledForMethodName)APImethodName tag:(NSInteger)tag
{
    if (APImethodName ==imageupload) {
        NSLog(@"image didfailedupload");
    }
}

@end
