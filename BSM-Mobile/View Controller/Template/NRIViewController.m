//
//  RIViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "NRIViewController.h"
#import "QRPCF02ViewController.h"
#import "GENCF02ViewController.h"
#import "AKSCF01ViewController.h"
#import "Utility.h"
#import "CellTableRi.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"

@interface NRIViewController ()<ConnectionDelegate,UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    NSArray *listData;
    NSArray *listInbox;
    int indexDelete;
    NSDictionary *data;
    NSDictionary *delData;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTittle;
@property (weak, nonatomic) IBOutlet UITableView *tabelRecipt;

@end

@implementation NRIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view removeGestureRecognizer:self.slidingViewController.panGesture];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *arr = [[userDefault objectForKey:@"listInbox"] mutableCopy];
    
    listInbox = [arr copy];
    
    [self.tabelRecipt reloadData];
    
    [self.lblTittle setText:[self.jsonData valueForKey:@"title"]];
    self.tabelRecipt.dataSource = self;
    self.tabelRecipt.delegate = self;
    
    NSString *url =[NSString stringWithFormat:@"%@,pin",[self.jsonData valueForKey:@"url_parm"]];
    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"cellRi";
    CellTableRi *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CellTableRi alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    NSString *response = [data valueForKey:@"response"];
    NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    BOOL isTheObjectThere = [listInbox containsObject:  [data objectForKey:@"transaction_id"]];
    if (isTheObjectThere) {
        cell.lblTop.textColor = [UIColor blackColor];
        cell.lblBtn.textColor = [UIColor blackColor];
    }
  
    
    cell.lblTop.text = [dict objectForKey:@"title"];
    if([dict objectForKey:@"messageType"]!=nil){
        cell.lblBtn.text = [NSString stringWithFormat:@"%@ Nomor Referensi: %@", [data objectForKey:@"time"], [dict objectForKey:@"referenceNumber"]];
    }else{
        cell.lblBtn.text = [NSString stringWithFormat:@"%@ Nomor Referensi: %@", [data objectForKey:@"time"], [dict objectForKey:@"trxref"]];
    }
    
    //if ([data objectForKey:@"icon"]){
    //cell.icon.image = [UIImage imageNamed:[data objectForKey:@"icon"]];
    cell.icon.image = [UIImage imageNamed:@"ic_inbox.png"];
    //}
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return true;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        delData = [listData objectAtIndex:indexPath.row];
        NSString *text = @"Anda yakin untuk menghapus?";
        NSString *language = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]objectAtIndex:0];
        BOOL isEn =[language isEqualToString:@"en"];
        if(isEn){
            text = @"Do you want to delete?";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:text delegate:self cancelButtonTitle:lang(@"CANCEL") otherButtonTitles:@"OK", nil];
        alert.tag = 413;
        alert.delegate = self;
        [alert show];
    }
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    data = [listData objectAtIndex:indexPath.row];
    if ([[data objectForKey:@"template"] isEqualToString:@"QRPCF02"]) {
        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"QRPCF02"];
        QRPCF02ViewController *viewCont = (QRPCF02ViewController *) templateView;
        [viewCont setDataLocal:data];
        [viewCont setFromRecipt:YES];
        [self.navigationController pushViewController:templateView animated:YES];
    } else if ([[data objectForKey:@"template"] isEqualToString:@"GENCF02"]) {
        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"GENCF02"];
        GENCF02ViewController *viewCont = (GENCF02ViewController *) templateView;
        [viewCont setDataLocal:data];
        [viewCont setFromRecipt:YES];
        [self.navigationController pushViewController:templateView animated:YES];
    } else if ([[data objectForKey:@"template"] isEqualToString:@"AKSCF01"]){
        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:[data objectForKey:@"template"]];
        AKSCF01ViewController *viewCont = (AKSCF01ViewController *) templateView;
        [viewCont setDataLocal:data];
        [viewCont setFromRecipt:YES];
        [self.navigationController pushViewController:templateView animated:YES];
    }
//    [self openTemplate:@"aa" withData:data];
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
//            NSString *response = [jsonObject valueForKey:@"response"];
//            TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivationVC"];
//            [self.navigationController pushViewController:templateView animated:YES];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSArray *arr = [[userDefault objectForKey:@"listRecipt"] mutableCopy];
            NSArray *reversedArray = [[arr reverseObjectEnumerator] allObjects];
            listData = [reversedArray copy];
            [self.tabelRecipt reloadData];
            if ([listData count] == 0) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *msg = @"";
                if([lang isEqualToString:@"id"]){
                    msg = @"Daftar kotak masuk kosong";
                } else {
                    msg = @"Inbox list is empty";
                }
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } else {
        
    }
    
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else if(alertView.tag == 413){
        if(buttonIndex == 1){
            int index = (int) [listData indexOfObject:delData];
            int lengthData = (int) listData.count - 1;
            int newIndex =  lengthData - index;
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSMutableArray *arr = [[userDefault objectForKey:@"listRecipt"] mutableCopy];
            [arr removeObjectAtIndex:newIndex];
            
            NSMutableArray *arrlistInbox = [userDefault objectForKey:@"listInbox"];
            NSMutableArray *temp = [[NSMutableArray alloc] init];
            for (id object in arrlistInbox) {
                if (![object isEqual:[delData valueForKey:@"transaction_id"]]) {
                    [temp addObject:object];
                }
            }
            [userDefault setObject:temp forKey:@"listInbox"];
            [userDefault setObject:arr forKey:@"listRecipt"];
            NSArray *reversedArray = [[arr reverseObjectEnumerator] allObjects];
            listData = [reversedArray copy];
            [self.tabelRecipt reloadData];
            
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"changeIcon"
             object:self];
            
            if ([listData count] == 0) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *msg = @"";
                if([lang isEqualToString:@"id"]){
                    msg = @"Daftar kotak masuk kosong";
                } else {
                    msg = @"Inbox list is empty";
                }
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    } else {
        [self backToRoot];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
