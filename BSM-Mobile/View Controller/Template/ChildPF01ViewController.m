//
//  ChildPF01ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 06/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ChildPF01ViewController.h"
#import "XLButtonBarViewCell.h"
#import "PF02ViewController.h"
#import "PF03ViewController.h"
#import "Utility.h"

@interface ChildPF01ViewController ()
@property (weak, nonatomic) IBOutlet UIView *vwBg;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation ChildPF01ViewController {
    NSArray *SAVArray;
    NSArray *SECArray;
    NSArray *DEPArray;
    NSArray *LOANArray;
    NSArray *CURArray;
    NSArray *ZISWAFArray;
    BOOL _isReload;
    DataManager *dataManager;
    NSUserDefaults *userDefault;
    NSString *lang;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataManager = [DataManager sharedManager];
    NSString *listTrans = [dataManager.dataExtra valueForKey:@"list_trans"];
    NSString *nameTrans = [dataManager.dataExtra valueForKey:@"name"];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    SAVArray  = [NSArray new];
    SECArray = [NSArray new];
    DEPArray = [NSArray new];
    LOANArray = [NSArray new];
    CURArray = [NSArray new];
    ZISWAFArray = [NSArray new];
    
     self.isProgressiveIndicator = YES;
    [self.buttonBarView setBackgroundColor:UIColorFromRGB(const_color_topbar)];

    if([listTrans isEqualToString:@"0"]){
         [self.buttonBarView.selectedBar setBackgroundColor:UIColorFromRGB(const_color_secondary)];
    }else{
        [self.buttonBarView.selectedBar setBackgroundColor:UIColorFromRGB(const_color_secondary)];
    }
    
    [self.buttonBarView removeFromSuperview];
    if([listTrans isEqualToString:@"0"]){
         [self.view addSubview:self.buttonBarView];
    }else{
       [self.image setImage:[UIImage imageNamed:@"ic_info_account_header.png"]];
        [self setupLayout];
 
//        [self.label setFont:[UIFont fontWithName:const_font_name3 size:15]];
//        [self.label setTextColor:[UIColor blackColor]];
        self.label.textColor = UIColorFromRGB(const_color_title);
        self.label.textAlignment = const_textalignment_title;
        self.label.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
        self.label.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

        [self.label setText:nameTrans];
        
        
    }
   
    
    self.changeCurrentIndexProgressiveBlock = ^void(XLButtonBarViewCell *oldCell, XLButtonBarViewCell *newCell, CGFloat progressPercentage, BOOL changeCurrentIndex, BOOL animated){
        if (changeCurrentIndex) {
            if([listTrans isEqualToString:@"0"]){
                [oldCell.label setFont:[UIFont fontWithName:const_font_name3 size:15]];
                [oldCell.label setTextColor:[UIColor blackColor]];
                [newCell.label setFont:[UIFont fontWithName:const_font_name3 size:15]];
                [newCell.label setTextColor:UIColorFromRGB (const_color_secondary)];
            }else{

                [newCell.label setFont:[UIFont fontWithName:const_font_name3 size:15]];
                [newCell.label setTextColor:[UIColor blackColor]];
                
            }
             [self resetTimer];
        }
    };
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                        selector:@selector(portofolioNavigate:)
                                                 name:@"portofolioNavigate"
                                               object:nil];
   
}


#pragma mark - XLPagerTabStripViewControllerDataSource

-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{

    dataManager = [DataManager sharedManager];
    NSString *cek = [dataManager.dataExtra valueForKey:@"list_trans"];
    
    NSMutableArray *childViewControllers =[[NSMutableArray alloc]init];
    
    if([cek isEqualToString:@"0"]){
        PF02ViewController *child_1 = [[PF02ViewController alloc]initWithStyle:UITableViewStylePlain mData:SAVArray mIdx:0 nRow:[cek intValue]];
        PF02ViewController *child_2 = [[PF02ViewController alloc]initWithStyle:UITableViewStylePlain mData:CURArray mIdx:1 nRow:[cek intValue]];
        PF02ViewController *child_3 = [[PF02ViewController alloc]initWithStyle:UITableViewStylePlain mData:DEPArray mIdx:2 nRow:[cek intValue]];
        if (!_isReload){
            return @[child_1,child_2,child_3
                     
                     ];
        }
        childViewControllers = [NSMutableArray arrayWithObjects:child_1,child_2,child_3,
                                                 nil];
        
    }
    if([cek isEqualToString:@"1"]){
        PF02ViewController *child_4 = [[PF02ViewController alloc]initWithStyle:UITableViewStylePlain mData:LOANArray mIdx:3 nRow:[cek intValue]];
        if (!_isReload){
            return @[
                     child_4
                     ];
        }
        childViewControllers = [NSMutableArray arrayWithObjects:child_4,
                                nil];
        
    }
    if([cek isEqualToString:@"2"]){
        PF02ViewController *child_5 = [[PF02ViewController alloc]initWithStyle:UITableViewStylePlain mData:SECArray mIdx:4 nRow:[cek intValue]];
        if (!_isReload){
            return @[
                     child_5
                     ];
        }
        childViewControllers = [NSMutableArray arrayWithObjects:child_5,
                                nil];
        
    }
    if([cek isEqualToString:@"3"]){
        
            PF03ViewController *child_6 = [[PF03ViewController alloc]initWithStyle:UITableViewStylePlain mData:ZISWAFArray];
        if (!_isReload){
            return @[
                     child_6
                     ];
        }
        childViewControllers = [NSMutableArray arrayWithObjects:child_6,
                                nil];
        
    }
    
    
    
    
    NSUInteger count = [childViewControllers count];
    for (NSUInteger i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        NSUInteger nElements = count - i;
        NSUInteger n = (arc4random() % nElements) + i;
        [childViewControllers exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    NSUInteger nItems = 1 + (rand() % 6);
    NSArray *arrChildVc = [[NSArray alloc]init];
    @try {
        arrChildVc = [childViewControllers subarrayWithRange:NSMakeRange(0, nItems)];
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
    }
    return arrChildVc;
}

- (void) portofolioNavigate:(NSNotification *) notification {
    if ([notification.name isEqualToString:@"portofolioNavigate"]) {
        NSDictionary *data = notification.userInfo;
        
        
        if ([[data valueForKey:@"SAV_ARRAY"] count] == 0) {
            SAVArray = [[NSArray alloc] initWithObjects:@"",nil];
        }else{
            if(![[[data valueForKey:@"SAV_ARRAY"]objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                SAVArray = [[NSArray alloc] initWithObjects:@"",nil];
            }else{
                NSMutableArray *tempSAV = [NSMutableArray new];
                for (int i=0; i < [[data valueForKey:@"SAV_ARRAY"] count]; i++) {
                    NSDictionary *dataDict = [[data valueForKey:@"SAV_ARRAY"]objectAtIndex:i];
                    NSString *tempString = [dataDict objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b31"]];
                    tempString = [tempString stringByAppendingString:@" - "];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b32"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                    tempString = [tempString stringByAppendingString:@" "];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b42"]];
                    if ([dataDict objectForKey:@"b52"]) {
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                        tempString = [tempString stringByAppendingString:@" "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b52"]];
                        
                    }
                    
                    if ([[dataDict objectForKey:@"b31"] isEqualToString:@"6015"] || [[dataDict objectForKey:@"b31"] isEqualToString:@"6014"]) {
                        if ([dataDict objectForKey:@"b61"]) {
                            tempString = [tempString stringByAppendingString:@"\n"];
                            tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b61"]];
                            tempString = [tempString stringByAppendingString:@"\n"];
                            tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b71"]];
                            tempString = [tempString stringByAppendingString:@"\n"];
                            tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b81"]];
                            tempString = [tempString stringByAppendingString:@"\n"];
                            tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b91"]];
                        }
                       
                    }
                    
                    if ([dataDict objectForKey:@"b101"]) {
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                        tempString = [tempString stringByAppendingString:@" "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b101"]];
                    }
                    
                    if ([dataDict objectForKey:@"b102"]) {
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b102"]];
                    }
                    
                    
                    [tempSAV addObject:tempString];
                    SAVArray = tempSAV;
                }
            }
        }
        
        if ([[data valueForKey:@"CUR_ARRAY"] count] == 0) {
            CURArray = [[NSArray alloc] initWithObjects:@"",nil];
        }else{
            if(![[[data valueForKey:@"CUR_ARRAY"]objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                CURArray = [[NSArray alloc] initWithObjects:@"",nil];
            }else{
                NSMutableArray *tempCur = [NSMutableArray new];
                for (int i=0; i < [[data valueForKey:@"CUR_ARRAY"] count]; i++) {
                    NSDictionary *dataDict = [[data valueForKey:@"CUR_ARRAY"]objectAtIndex:i];
                    NSString *tempString = [dataDict objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b31"]];
                    tempString = [tempString stringByAppendingString:@" - "];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b32"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                    tempString = [tempString stringByAppendingString:@" "];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b42"]];
                    if ([dataDict objectForKey:@"b52"]) {
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                        tempString = [tempString stringByAppendingString:@" "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b52"]];
                        
                    }
                    if ([dataDict objectForKey:@"b61"]) {
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                        tempString = [tempString stringByAppendingString:@" "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b61"]];
                    }
                    [tempCur addObject:tempString];
                    CURArray = tempCur;
                }
            }
        }
        
        if ([[data valueForKey:@"DEP_ARRAY"] count] == 0) {
            DEPArray = [[NSArray alloc] initWithObjects:@"",nil];
        }else{
            if(![[[data valueForKey:@"DEP_ARRAY"]objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                DEPArray = [[NSArray alloc] initWithObjects:@"",nil];
            }else{
                NSMutableArray *tempDEP = [NSMutableArray new];
                for (int i=0; i < [[data valueForKey:@"DEP_ARRAY"] count]; i++) {
                    NSDictionary *dataDict = [[data valueForKey:@"DEP_ARRAY"]objectAtIndex:i];
                    NSString *tempString = [dataDict objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b31"]];
                    tempString = [tempString stringByAppendingString:@" - "];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b32"]];
                    if ([dataDict objectForKey:@"b51"]) {
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b51"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                        tempString = [tempString stringByAppendingString:@" "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b61"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                        tempString = [tempString stringByAppendingString:@" "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b42"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b71"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b81"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b91"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b101"]];
                        
                        if([dataDict objectForKey:@"b111"]){
                            tempString = [tempString stringByAppendingString:@"\n"];
                            tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b111"]];
                            tempString = [tempString stringByAppendingString:@"\n"];
                            tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b121"]];
                        }

                    }else{
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b41"]];
                        tempString = [tempString stringByAppendingString:@" "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b42"]];
                    }
                    
                    [tempDEP addObject:tempString];
                    DEPArray = tempDEP;
                }
            }
        }
        
        if ([[data valueForKey:@"LOAN_ARRAY"] count] == 0) {
            LOANArray = [[NSArray alloc] initWithObjects:@"",nil];
        }else{
            if(![[[data valueForKey:@"LOAN_ARRAY"]objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                LOANArray = [[NSArray alloc] initWithObjects:@"",nil];
            }else{
                NSMutableArray *tempLOAN = [NSMutableArray new];
                for (int i=0; i < [[data valueForKey:@"LOAN_ARRAY"] count]; i++) {
                    NSDictionary *dataDict = [[data valueForKey:@"LOAN_ARRAY"]objectAtIndex:i];
                    NSString *tempString = [dataDict objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b3"]];
                    if ([dataDict objectForKey:@"b51"]) {
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b51"]];
                        tempString = [tempString stringByAppendingString:@" - "];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b52"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b4"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b6"]];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b7"]];
                    }else{
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:@"\n"];
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b4"]];
                    }
                    
                    
                    [tempLOAN addObject:tempString];
                    LOANArray = tempLOAN;
                }
            }
        }
        
        if ([[data valueForKey:@"SEC_ARRAY"] count] == 0) {
            SECArray = [[NSArray alloc] initWithObjects:@"",nil];
        }else{
            if(![[[data valueForKey:@"SEC_ARRAY"]objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                SECArray = [[NSArray alloc] initWithObjects:@"",nil];
            }else{
                NSMutableArray *tempSec = [NSMutableArray new];
                for (int i=0; i < [[data valueForKey:@"SEC_ARRAY"] count]; i++) {
                    NSDictionary *dataDict = [[data valueForKey:@"SEC_ARRAY"]objectAtIndex:i];
                    NSString *tempString = [dataDict objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"b3"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    [tempSec addObject:tempString];
                    SECArray = tempSec;
                }
            }
        }
        
        if ([[data valueForKey:@"ZISWAF_ARRAY"] count] == 0) {
            ZISWAFArray = [[NSArray alloc] initWithObjects:@"",nil];
        }else{
            if(![[[data valueForKey:@"ZISWAF_ARRAY"]objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                ZISWAFArray = [[NSArray alloc] initWithObjects:@"",nil];
            }else{
                NSMutableArray *tempZisaf = [NSMutableArray new];
                for (int i=0; i < [[data valueForKey:@"ZISWAF_ARRAY"] count]; i++) {
                    NSDictionary *dataDict = [[data valueForKey:@"ZISWAF_ARRAY"]objectAtIndex:i];
                    
                    NSString *tempString;
                    if ([[dataDict objectForKey:@"key"]isEqualToString:@"Infaq"]) {
                        tempString = [NSString stringWithFormat:@"%@ / Shadaqah", [dataDict objectForKey:@"key"]];
                    }else{
                        tempString = [dataDict objectForKey:@"key"];
                    }
                    
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"key_0"]];
                    tempString = [tempString stringByAppendingString:@"<tab>"];
                    tempString = [tempString stringByAppendingString:[NSString stringWithFormat:@"Rp. %@",[Utility formatCurrencyValue:[[dataDict objectForKey:@"val_0"]doubleValue]]]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"key_1"]];
                    tempString = [tempString stringByAppendingString:@"<tab>"];
                    tempString = [tempString stringByAppendingString:[NSString stringWithFormat:@"Rp. %@",[Utility formatCurrencyValue:[[dataDict objectForKey:@"val_1"]doubleValue]]]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"key_2"]];
                    tempString = [tempString stringByAppendingString:@"<tab>"];
                    tempString = [tempString stringByAppendingString:[NSString stringWithFormat:@"Rp. %@",[Utility formatCurrencyValue:[[dataDict objectForKey:@"val_2"]doubleValue]]]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    
                    if ([[dataDict objectForKey:@"key_3"] isEqualToString:@"totamt"]){
                        tempString = [tempString stringByAppendingString:@"Total"];
                    }else{
                        tempString = [tempString stringByAppendingString:[dataDict objectForKey:@"key_3"]];
                    }
                    
                    tempString = [tempString stringByAppendingString:@"<tab>"];
                    tempString = [tempString stringByAppendingString:[NSString stringWithFormat:@"Rp. %@",[Utility formatCurrencyValue:[[dataDict objectForKey:@"val_3"]doubleValue]]]];
                    [tempZisaf addObject:tempString];
                    
                    if (tempZisaf.count>0) {
                         ZISWAFArray = tempZisaf;
                    }else{
                        ZISWAFArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                   
                   
                }
            }
        }
        
        [super reloadPagerTabStripView];
       
    }
}

-(void)reloadPagerTabStripView
{
    _isReload = YES;
    self.isProgressiveIndicator = (rand() % 2 == 0);
    self.isElasticIndicatorLimit = (rand() % 2 == 0);
    [super reloadPagerTabStripView];
}

-(void) resetTimer{
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


-(void) setupLayout{
    
    CGRect frmImage = _image.frame;
    CGRect frmLblTitle = _label.frame;
    CGRect frmVwBg = _vwBg.frame;
    
    frmVwBg.size.width = SCREEN_WIDTH;
    frmVwBg.origin.x = 0;
    frmVwBg.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.height = 40;
    frmLblTitle.size.width = frmVwBg.size.width - 16;
    
    
    frmImage.origin.y = ((frmVwBg.size.height/2)-(frmImage.size.height/2))/2;
    
    _image.frame = frmImage;
    [_image setHidden:YES];
    
    _vwBg.frame = frmVwBg;
    _label.frame = frmLblTitle;
    
}



@end
