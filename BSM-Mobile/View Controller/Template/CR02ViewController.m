//
//  CR02ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CR02ViewController.h"
#import "ProductTypeCashfinCell.h"
#import "Styles.h"

@interface CR02ViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSArray *productList;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@property (weak, nonatomic) IBOutlet UIScrollView *vwReject;
@property (weak, nonatomic) IBOutlet UIScrollView *vwAccept;
@property (weak, nonatomic) IBOutlet UIView *vwAcceptTable;

@property (weak, nonatomic) IBOutlet UIImageView *imgReject;
@property (weak, nonatomic) IBOutlet UILabel *lblReject;

@property (weak, nonatomic) IBOutlet UIImageView *imgAccept;
@property (weak, nonatomic) IBOutlet UIView *vwContentAccepted;
@property (weak, nonatomic) IBOutlet UILabel *lblAcceptContent;
@property (weak, nonatomic) IBOutlet UILabel *lblAcceptTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAcceptSubtitle;
@property (weak, nonatomic) IBOutlet CustomBtn *btnRequest;
@property (weak, nonatomic) IBOutlet UITableView *productTable;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productTableHeight;

@end

@implementation CR02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Styles setTopConstant:self.topConstant];
    
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.titleBar.text = [self.jsonData objectForKey:@"title"];
    self.imgIcon.image = [UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]];
    
    [self.vwReject setHidden:YES];
    [self.vwAccept setHidden:YES];
    [self.vwAcceptTable setHidden:YES];
    
    [self.vwContentAccepted.layer setBorderColor:[[UIColor blackColor]CGColor]];
    [self.vwContentAccepted.layer setBorderWidth:1];
    [self.vwContentAccepted.layer setCornerRadius:10];
    
    [self.productTable setDelegate:self];
    [self.productTable setDataSource:self];
    [self.productTable registerNib:[UINib nibWithNibName:@"ProductTypeCashfinCell" bundle:nil] forCellReuseIdentifier:@"PRODCASHFINTYPE"];
    [self.productTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [Styles setStyleButton:self.btnRequest];
    
    if([language isEqualToString:@"id"]){
        [self.lblAcceptTitle setText:@"Mitraguna"];
        [self.lblAcceptSubtitle setText:@"Pembiayaan Kebutuhan Multiguna"];
        [self.btnRequest setTitle:@"Ajukan Mitraguna" forState:UIControlStateNormal];
    }else{
        [self.lblAcceptTitle setText:@"Mitraguna"];
        [self.lblAcceptSubtitle setText:@"Mutli Purpose Financing"];
        [self.btnRequest setTitle:@"Submit Mitraguna" forState:UIControlStateNormal];
    }
    
    [self.btnRequest addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self doRequest];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
//    [self setupDummy];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return productList.count;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [dataManager.dataExtra setValue:[productList[indexPath.row] objectForKey:@"product_name"] forKey:@"product_name"];
//    [dataManager.dataExtra setValue:[productList[indexPath.row] objectForKey:@"product_desc"] forKey:@"product_desc"];
//    [dataManager.dataExtra setValue:[productList[indexPath.row] objectForKey:@"product_type"] forKey:@"product_type"];
//    [dataManager.dataExtra setValue:[productList[indexPath.row] objectForKey:@"product_code"] forKey:@"product_code"];
//
//    [self openNextTemplate];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProductTypeCashfinCell *cell = (ProductTypeCashfinCell *)[tableView dequeueReusableCellWithIdentifier:@"PRODCASHFINTYPE"];
    
    cell.lblProductName.text = [productList[indexPath.row] objectForKey:@"product_name"];
    cell.lblProductSubname.text = [productList[indexPath.row] objectForKey:@"product_subname"];
    cell.lblProductDesc.text = [[productList[indexPath.row] objectForKey:@"product_desc"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    cell.btnSubmission.tag = indexPath.row;
    if([language isEqualToString:@"id"]){
        [cell.btnSubmission setTitle:@"AJUKAN MITRAGUNA" forState:UIControlStateNormal];
    }else{
        [cell.btnSubmission setTitle:@"SUBMIT MITRAGUNA" forState:UIControlStateNormal];
    }
    [cell.btnSubmission addTarget:self action:@selector(actionSubmission:) forControlEvents:UIControlEventTouchUpInside];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void) actionSubmission : (UIButton *) sender{
    [dataManager.dataExtra setValue:[productList[sender.tag] objectForKey:@"product_name"] forKey:@"product_name"];
    [dataManager.dataExtra setValue:[productList[sender.tag] objectForKey:@"product_desc"] forKey:@"product_desc"];
    [dataManager.dataExtra setValue:[productList[sender.tag] objectForKey:@"product_type"] forKey:@"product_type"];
    [dataManager.dataExtra setValue:[productList[sender.tag] objectForKey:@"product_code"] forKey:@"product_code"];

    [self openNextTemplate];
}

- (void) actionNext{
    [self openNextTemplate];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"cashfin_requested"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            
            /**
                {"response":[
             {"product_desc":"Pembiayaan Kebutuhan Multiguna Dapat digunakan untuk:<br>- Pendidikan<br>- Perjalanan Wisata/Rohani<br>- Pembelian Bahan Bangunan/Renovasi<br>- Kesehatan<br>- atau Kebutuhan Konsumtif Lainnya<br>","product_name":"Mitraguna","product_subname":"Pembiayaan Multiguna online dan sebagainya apalaha apalah itu","product_type":"IA","product_code":"BSMMG"},
             {"product_desc":"Lorem Ipsum is simply dummy text of the printing:<br>- paragraphs<br>- words<br>- bytes<br>- lists<br>","product_name":"Lorem Ipsum","product_subname":"Pembiayaan Multiguna online dan sebagainya apalaha apalah itu","product_type":"LI","product_code":"BSMTEST"}],"transaction_id":"","response_code":"00"}
             */
            
//            self.lblAcceptContent.text = [[jsonObject valueForKey:@"response"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            
//            NSString *mResponse = [jsonObject objectForKey:@"response"];
            
            productList = [jsonObject objectForKey:@"response"];
            [self.productTable reloadData];
//            self.productTableHeight.constant = self.productTable.contentSize.height;
            
            [UIView transitionWithView:self.vwAccept
              duration:1
               options:UIViewAnimationOptionShowHideTransitionViews
            animations:^{
//                self.vwAccept.hidden = NO;
                self.vwAcceptTable.hidden = NO;
            }
            completion:NULL];
        }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"01"]){
            
            self.lblReject.text = [[jsonObject valueForKey:@"response"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            
            [UIView transitionWithView:self.vwReject
              duration:1
               options:UIViewAnimationOptionShowHideTransitionViews
            animations:^{
                self.vwReject.hidden = NO;
            }
            completion:NULL];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)reloadApp{
    [self backToRoot];
}

@end
