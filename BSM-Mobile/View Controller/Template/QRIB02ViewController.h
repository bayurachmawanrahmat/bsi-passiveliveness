//
//  QRIB02ViewController.h
//  BSM-Mobile
//
//  Created by BSM on 1/23/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"
#import "TemplateViewController.h"

@interface QRIB02ViewController : TemplateViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,APIManagerDelegate,UIDocumentPickerDelegate, UIDocumentMenuDelegate>

{
    UIImagePickerController *imagePicker;
    NSMutableArray *arrimg;
    
    NSString * UploadType;
    NSString * ImageName;
}

@end
