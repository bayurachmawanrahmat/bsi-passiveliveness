//
//  EMSCANViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 30/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "EMSCANViewController.h"
#import "Utility.h"
#import <CoreNFC/CoreNFC.h>
#import "NSData+Hex.h"
#import "NSString+MD5.h"

@interface EMSCANViewController ()<NFCTagReaderSessionDelegate, EMSCANDelegate>{
    NSString *cardUID, *cardInfo, *lastBalance, *cardAttribute, *nBalance, *cardNumber, *cardType, *resultCommand, *lastTransactionTime, *cardSelected;
    NSString *lastMessage, *apiVersion, *jwt, *balance, *mErrorMessage, *mOldTransactionId;
    NSString *language, *resMsgError;
    
    NSString *responseState, *titlBar;
    
    NSUserDefaults *userDefaults;
    
    NFCTagReaderSession* mSession;
    id<NFCISO7816Tag> mTag;
    
    int seq, sendCommandCount;
    
    BOOL isResponseReversal;
    
    NSNumberFormatter* currencyFormatter;
    
    NSDictionary* cardInformations;
//    NFCTagReaderSession *sessionReader;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNominal;
@property (weak, nonatomic) IBOutlet UIImageView *imageTap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstBar;

@end

@implementation EMSCANViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    [dataManager setMenuId:@"00060"];
    isResponseReversal = NO;
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [Styles setTopConstant:self.heightConstBar];
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if(self.jsonData){
        self.lblTitleBar.text = [self.jsonData objectForKey:@"title"];
    }else{
        self.lblTitleBar.text = titlBar;
    }
    
    if([[dataManager.dataExtra valueForKey:@"code"] isEqualToString:@"00061"]){
        if([language isEqualToString:@"id"]){
            self.lblTitleBar.text = @"Informasil Saldo";
        }else{
            self.lblTitleBar.text = @"Balance Information";
        }
    }
    
    NSLog(@"%@", self.jsonData);
    
    if([language isEqualToString:@"id"]){
        self.lblTitle.text = @"Letakkan kartu e-money anda dekat dengan kamera";
    }else{
        self.lblTitle.text = @"Put your e-money card nearby camera";
    }
    
    [self.imgIconBar setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    
    if (@available(iOS 13.0, *)) {
        if(NFCReaderSession.readingAvailable){
            NSLog(@"NFC - Avail");
            NFCTagReaderSession *sessionTagReader;
            sessionTagReader = [[NFCTagReaderSession alloc]initWithPollingOption:NFCPollingISO14443 delegate:self queue:dispatch_get_main_queue()];

            if([language isEqualToString:@"id"]){
                [sessionTagReader setAlertMessage:@"Tempelkan kartu Bank Syariah Indonesia e-money pada bagian belakang dekat kamera handphone anda."];
                resMsgError = @"Terjadi kesalahan dalam pembacaan kartu, silahkan dicoba kembali";
            }else{
                [sessionTagReader setAlertMessage:@"Place your Bank Syariah Indonesia e-money card in the back near your phone camera."];
                resMsgError = @"There's a problem while reading card, please try again";
            }
            [sessionTagReader beginSession];
            NSLog(@"NFC - Begin Session");
        }else{
            NSString* msg=@"";
            if ([language isEqualToString:@"id"]) {
                msg = @"Mohon maaf, fitur tidak tersedia";
            }else{
                msg = @"Sorry, feature not available";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];

        }
    } else {
        NSString* msg=@"";
        if ([language isEqualToString:@"id"]) {
            msg = @"Mohon maaf fitur ini tersedia untuk iOS 13+, silahkan update iOS anda";
        }else{
            msg = @"Sorry this feature available for iOS 13+, please update your iOS";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        [self presentViewController:alert animated:YES completion:nil];

    }
    
}


- (void)tagReaderSession:(NFCTagReaderSession *)session didDetectTags:(NSArray<__kindof id<NFCTag>> *)tags API_AVAILABLE(ios(13.0)){
    NSLog(@"NFC - didDetect %@", tags.firstObject);
    if (tags.count > 1){
        if([language isEqualToString:@"id"]){
            [session setAlertMessage:@"Terdeteksi Lebih dari 1 kartu. Silahkan di coba kembali."];
        }else{
            [session setAlertMessage:@"More than 1 card is detected. Please remove all tags and try again."];
        }
        [session restartPolling];
        return;
    }

    [session connectToTag:tags.firstObject completionHandler:^(NSError *error){
        self->mSession = session;
        if (error != nil){
            if([self->language isEqualToString:@"id"]){
                [session invalidateSessionWithErrorMessage:@"maaf gagal deteksi kartu"];
            }else{
                [session invalidateSessionWithErrorMessage:@"Sorry failed to scan e-money"];
            }
            return;
        }
        
        if([[self->dataManager.dataExtra valueForKey:@"code"] isEqualToString:@"00063"]){
            [self->userDefaults setValue:@"" forKey:@"transaction_id"];
            if([self->language isEqualToString:@"id"]){
                [session setAlertMessage:@"Harap Tunggu..."];
            }else{
                [session setAlertMessage:@"Please Wait..."];
            }
            [self reqUpdateCardEmoney:session andTag:tags.firstObject];
        }else if([[self->dataManager.dataExtra valueForKey:@"code"] isEqualToString:@"00062"]){
            [self reqGetCardEmoney:session withTag:tags.firstObject completionHandler:^(NSString* result, NSError *err){
                if(err == nil){
                    [self setCardInformation];
                }else{
                    self->mErrorMessage = self->resMsgError;
                    [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                    [self setCardInformation];
                }
            }];
        }else{
            [self reqGetCardEmoney:session withTag:tags.firstObject completionHandler:^(NSString* result, NSError *err){

                if(err == nil){
                    [self updateView];
                }else{
                    self->mErrorMessage = self->resMsgError;
                    [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                    [self setCardInformation];
                }
            }];
        }
        
    }];
    
}

- (void)tagReaderSessionDidBecomeActive:(NFCTagReaderSession *)session API_AVAILABLE(ios(13.0)){
    NSLog(@"NFC - become Active");
}

- (void)tagReaderSession:(NFCTagReaderSession *)session didInvalidateWithError:(NSError *)error API_AVAILABLE(ios(13.0)){
    NSLog(@"NFC - invalid %@", error.description);
//    NSLog(@"NFC - code invalid %ld", (long)error.code);
    if(error.code == 201){
        if([language isEqualToString:@"id"]){
            self->mErrorMessage = @"Pendeteksian kehabisan waktu. silahkan di coba kembali";
            [self setCardInformation];
        }else{
            self->mErrorMessage = @"Detecting timout. please try again";
            [self setCardInformation];
        }
    }
}

- (IBAction)ok:(id)sender {
    [super backToRoot];
}

- (void) getCardInfo:(id<NFCISO7816Tag>) tag completionHandler:(completion_lb) completion API_AVAILABLE(ios(13.0)){
    NSData *data = [[NSData alloc]init];
    if (@available(iOS 13.0, *)) {
        NFCISO7816APDU *apdu = [[NFCISO7816APDU alloc]initWithInstructionClass:0x00
                                                               instructionCode:0xB3
                                                                   p1Parameter:0x00
                                                                   p2Parameter:0x00
                                                                          data:data
                                                        expectedResponseLength:63];

        [tag.asNFCISO7816Tag sendCommandAPDU:apdu completionHandler:^(NSData *responseData, uint8_t sw1, uint8_t sw2, NSError *error){
            
            if (error != nil){
                NSLog(@"GET CARD INFO error : %@", error.description);
                completion(@"fail",error);
            }else{
                NSString* statWord = [self getStatusWord:sw1 and:sw2];
                NSString* resp = [responseData hexRepresentationWithSpaces_AS:NO];
                resp = [NSString stringWithFormat:@"%@%@",resp,statWord];
//                NSLog(@"GET CARD INFO : %@", resp);
                
                self->cardInfo = resp;
                self->cardType = [self getCardType:resp];
                
//                NSLog(@"GET CARD TYPE : %@", self->cardType);

                NSString *formatCardNumber = @"";
                NSString *arrCardInfo = [resp substringWithRange:NSMakeRange(0, 16)];
                                
                if(arrCardInfo.length > 0){
                    for (int i = 0; i < arrCardInfo.length; i++){
                        formatCardNumber = [NSString stringWithFormat:@"%@%c",formatCardNumber,[arrCardInfo characterAtIndex:i]];

                        if((i+1)%4 == 0 && i+1 != arrCardInfo.length){
                            formatCardNumber = [NSString stringWithFormat:@"%@-",formatCardNumber];
                        }
                    }
                }
                self->cardNumber = formatCardNumber;
                self->cardUID = [tag.identifier hexRepresentationWithSpaces_AS:NO];
                completion(@"success",error);
//                NSLog(@"GET CARD INFO DONE : %@", self->cardUID);
            }
        }];
        
    }
}

- (void) getLastBalance:(id<NFCISO7816Tag>) tag completionHandler:(completion_lb) completion API_AVAILABLE(ios(13.0)){
    
    NSData *data = [[NSData alloc]init];
    uint32_t exptResLengt = 0x0A;

    if (@available(iOS 13.0, *)) {
        NFCISO7816APDU *apdu = [[NFCISO7816APDU alloc]initWithInstructionClass:0x00
                                                               instructionCode:0xB5
                                                                   p1Parameter:0x00
                                                                   p2Parameter:0x00
                                                                          data:data
                                                        expectedResponseLength:(NSInteger)exptResLengt];
        
        [tag.asNFCISO7816Tag sendCommandAPDU:apdu completionHandler:^(NSData *responseData, uint8_t sw1, uint8_t sw2, NSError *error){
            
            if (error != nil){
                NSLog(@"erro sendCommapnAPDU : %@", error.description);
                completion(@"",error);
            }else{
                NSString* statWord = [self getStatusWord:sw1 and:sw2];
                NSString* resp = [responseData hexRepresentationWithSpaces_AS:NO];
                resp = [NSString stringWithFormat:@"%@%@",resp,statWord];
//                NSLog(@"NFC - BALANCE %@",resp);
                
                NSString* hexBalance = [resp substringToIndex:8];
//                NSLog(@"NFC - LAST BALANCE RESP %@",hexBalance);
                                
                NSString *resultBalance = [NSString stringWithFormat:@"%d",[self littleEndian:hexBalance]];
//                NSLog(@"NFC - RESULT BALANCE IDR %@", resultBalance);
                
                self->lastBalance = resultBalance;
                self->nBalance = [NSString stringWithFormat:@"IDR %@",resultBalance];
                
                if(resp.length >= 19){
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                    [dateFormatter setDateFormat:@"ddMMyyHHmmss"];
                    NSDate *date1 = [dateFormatter dateFromString:[resp substringWithRange:NSMakeRange(8, 12)]];

                    [dateFormatter setDateFormat:@"dd MMMM yyyy HH:mm:ss"];
                    NSString *dateLastTransaction = [dateFormatter stringFromDate:date1];
                    self->lastTransactionTime = dateLastTransaction;
                }else{
                    self->lastTransactionTime = @"";
                }
                completion(self->lastTransactionTime,error);
            }
        }];
        
    }
}

- (void) getCardCommand:(id<NFCISO7816Tag>) tag andCommand:(NSString*) command completionHandler:(completion_lb)completion API_AVAILABLE(ios(13.0)){
    NSData *data = [self dataFromHexString:command];
        
    NFCISO7816APDU *apdi = [[NFCISO7816APDU alloc]initWithData:data];
    [tag.asNFCISO7816Tag sendCommandAPDU:apdi completionHandler:^(NSData *responseData, uint8_t sw1, uint8_t sw2, NSError *error){
        
        if (error != nil){
            NSLog(@"erro sendCommapnAPDU : %@", error.description);
            completion(@"",error);
            return;
        }else{
            NSString* statusWord = [self getStatusWord:sw1 and:sw2];
            NSString* resp = [responseData hexRepresentationWithSpaces_AS:NO];
            
            if([resp isEqualToString:@""]){
                completion(statusWord, error);
            }else{
                NSString* res = [NSString stringWithFormat:@"%@%@",resp,statusWord];
//                NSLog(@"NFC - CARD COMMAND HEX + STATUS WORD %@",res);
                completion(res, error);
            }
//            NSLog(@"NFC - CARD COMMAND STATUS WORD %@",resp);
        }
    }];
            
}

- (void) getCardAttribute:(id<NFCISO7816Tag>) tag completionHandler:(completion_lb)completion API_AVAILABLE(ios(13.0)){
    NSData *data = [[NSData alloc]init];
    if (@available(iOS 13.0, *)) {
        NFCISO7816APDU *apdu = [[NFCISO7816APDU alloc]initWithInstructionClass:0x00
                                                               instructionCode:0xF2
                                                                   p1Parameter:0x10
                                                                   p2Parameter:0x00
                                                                          data:data
                                                        expectedResponseLength:13];
        
        [tag.asNFCISO7816Tag sendCommandAPDU:apdu completionHandler:^(NSData *responseData, uint8_t sw1, uint8_t sw2, NSError *error){
            
            if (error != nil){
                NSLog(@"erro sendCommapnAPDU : %@", error.description);
                completion(@"", error);
            }else{
                NSString* statWord = [self getStatusWord:sw1 and:sw2];
                NSString* resp = [responseData hexRepresentationWithSpaces_AS:NO];
                self->cardAttribute = [NSString stringWithFormat:@"%@%@",resp,statWord];
//                NSLog(@"NFC - CARD ATTR %@",self->cardAttribute);
                completion(self->cardAttribute,error);
            }
        }];
        
    }
}

- (void) getCardSelect:(id<NFCISO7816Tag>) tag completionHandler:(completion_lb)completion  API_AVAILABLE(ios(13.0)){
    NSData *data = [self dataFromHexString:@"00A40400080000000000000001"];
    
        if (@available(iOS 13.0, *)) {
            NFCISO7816APDU *apdu = [[NFCISO7816APDU alloc]initWithData:data];
            
            [tag.asNFCISO7816Tag sendCommandAPDU:apdu completionHandler:^(NSData *responseData, uint8_t sw1, uint8_t sw2, NSError *error){
    
                if (error != nil){
                    NSLog(@"erro sendCommapnAPDU : %@", error.description);
                    completion(@"",error);
                }else{
                    NSString* resp = [responseData hexRepresentationWithSpaces_AS:NO];
//                    NSLog(@"NFC - CARD SELECT %@",resp);
                    NSString* statusWord = [self getStatusWord:sw1 and:sw2];
//                    NSLog(@"NFC - CARD SELECT Status Word %@",statusWord);
                    self->cardSelected = statusWord;
                    completion(statusWord,error);
                }
            }];
        }
}

- (int) littleEndian : (NSString *) number {
    
    NSMutableString *formatted = [NSMutableString stringWithString:number];
    
    if(number.length <= 16){
        int diff = 16 - (int)number.length;
        int i = 0;
        while(i < diff){
            [formatted appendString:@"0"];
            i++;
        }
    }else{
        return 0;
    }
    
    uint64_t result = (UInt64)strtoull([formatted UTF8String], NULL, 16);
    NSLog(@"The required Length is %llu", result);

    uint64_t z = OSSwapInt64(result);
    return (int)z;
}

- (NSString*) getCardType : (NSString*) cardInfo{
    if([cardInfo isEqualToString:@""])
    {
        return @"";
    }
    
    int32_t number = [self littleEndian:[cardInfo substringWithRange:NSMakeRange(36, 2)]];
    NSLog(@"get Card Type :::: %d",number);
    if((number & INT32_C(0x80)) == INT32_C(0x80)){
        return cardTypeNewApplet;
    }else{
        return cardTypeOldApplet;
    }
}

- (void) getDataNewApplet:(id<NFCISO7816Tag>) tag andAmount:(NSString*)amount andSession:(NSString*)session completionHandler:(completion_lb)complete API_AVAILABLE(ios(13.0)){
    NSString *command = @"00E50000";
    NSString *length = @"46";
    NSString *data = @"";
    
    NSDateFormatter *dt = [[NSDateFormatter alloc]init];
    [dt setDateFormat:@"ddMMyyHHmmss"];
    NSString *date = [dt stringFromDate:[NSDate date]];
    
    NSString *counterCard = @"0000000000000000";
    NSString *pin = @"000000000000";
    NSString *reff = session;
    NSString *institutionReff = @"0000000000000000";
    NSString *sourceOfAcc = @"00000000000000000000";

    NSString *merchantData = @"0000000000000000000000000000000000000000";
    
    
    data = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",date,counterCard,pin,reff,institutionReff,sourceOfAcc,amount,merchantData];
    length = [NSString stringWithFormat:@"%02x",(int)data.length/2];
    command = [NSString stringWithFormat:@"%@%@%@",command,length,data];
    
    [self getCardCommand:tag andCommand:command completionHandler:^(NSString* string, NSError* err){
        NSString* result = [self getResponseValue:string];
//        NSLog(@"get card command new applet -> %@ ",result);
        complete(result,err);
    }];
}

- (void) getCertificateNewApplet :(id<NFCISO7816Tag>) tag completionHandler:(completion_lb)completion  API_AVAILABLE(ios(13.0)){
    NSString *command = @"00E0000000";
    [self getCardCommand:tag andCommand:command completionHandler:^(NSString* string, NSError* err){
        NSString* result = [self getResponseValue:string];
//        NSLog(@"get ceritificate new applet -> %@ ",result);
        completion(result, err);
    }];
}

-(NSString*) getResponseValue:(NSString*)message
{
    if(message == nil || message.length <= 4 || [message isEqualToString:@""]){
        return @"";
    }
    
    NSString *result = [message substringWithRange:NSMakeRange(0, message.length-4)];
    return result;
}

- (void) setFlagReversal:(NSString*) trxID{
    NSString *cardNoHash = [cardNumber MD5];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString* date = [dateFormatter stringFromDate:[NSDate date]];
    NSString* key = [NSString stringWithFormat:@"%@%@",keyFlagReversal,cardNoHash];
    [userDefaults setValue:[NSString stringWithFormat:@"%@|%@",trxID,date] forKey:key];
    [userDefaults synchronize];
    NSLog(@"%@", [userDefaults valueForKey:key]);
}

- (void) removeReversal{
    NSString *cardNoHash = [cardNumber MD5];
    [userDefaults removeObjectForKey:[NSString stringWithFormat:@"%@%@",keyFlagReversal,cardNoHash]];
    [userDefaults synchronize];
}

- (void) updateCardEmoney : (id<NFCISO7816Tag>) tag   API_AVAILABLE(ios(13.0)){
    
//    NSLog(@"Update Card Emoney :::::: >>>> ");
    
    NSString *cardNoHash = [cardNumber MD5];
    
    NSString* flagReversal = [userDefaults valueForKey:[NSString stringWithFormat:@"%@%@",keyFlagReversal,cardNoHash]];
    NSString* trxId = nil;
    
    if(flagReversal != nil && ![flagReversal isEqualToString:@""]){

        NSArray *dataReversal = [flagReversal componentsSeparatedByString:@"|"];
//        NSString *dateReversalString = dataReversal[1];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        NSDate *date1 = [dateFormatter dateFromString:dataReversal[1]];
        NSDate *date2 = [NSDate date];
        
//        NSLog(@"value flag reversal --------> %@", flagReversal);
//        NSLog(@"date 1 --------> %@", date1);
//        NSLog(@"date 2 --------> %@", date2);

        
        if([date2 compare:date1] == NSOrderedDescending){

            NSTimeInterval secs = [date2 timeIntervalSinceDate:date1];
//            NSLog(@"Seconds --------> %f", secs);
            if(secs > 3600){
                [self removeReversal];
            }else{
                trxId = dataReversal[0];
            }
        }else{
            [self removeReversal];
        }
    }
    
    if(trxId != nil && ![trxId isEqualToString:@""]){
        mOldTransactionId = trxId;
        if([[self getCardType:cardInfo] isEqualToString:cardTypeOldApplet]){
            NSLog(@"DO REVERSAL OLD %@ - %@",mOldTransactionId, trxId);
            [self doPostReversalOld];
        }else{
            NSLog(@"DO REVERSAL NEW");
            [self doPostReversalNew];
        }
    }else{
        NSLog(@"DO UPDATE");
        [self doUpdateEmoney];
    }
    
}

- (void) doUpdateEmoney{
    self->responseState = resStatUpdateBalance;
    
    [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%d",seq] forKey:@"seq"];
    [dataManager.dataExtra setValue:cardAttribute forKey:@"cardAttr"];
    [dataManager.dataExtra setValue:lastBalance forKey:@"lastBalance"];
    [dataManager.dataExtra setValue:cardInfo forKey:@"cardInfo"];
    [dataManager.dataExtra setValue:cardUID forKey:@"cardUUID"];

    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=update_card_emoney,menu_id,device,device_type,ip_address,language,date_local,id_account,pin,customer_id,seq,cardUUID,cardInfo,lastBalance,cardAttr,transaction_id,dataToSam,message,session,jwt,apiVersion,code" needLoading:false textLoading:@"" encrypted:false banking:false continueLoading:false];
}

- (void) doPostReversalOld{
    self->responseState = resStatReversalOldApplet;
    mErrorMessage = @"";

    if(!isResponseReversal){
        seq = -1;
        [dataManager.dataExtra setValue:lastMessage forKey:@"message"];
        [dataManager.dataExtra setValue:cardAttribute forKey:@"cardAttr"];
        [dataManager.dataExtra setValue:lastBalance forKey:@"lastBalance"];
        [dataManager.dataExtra setValue:cardInfo forKey:@"cardInfo"];
        [dataManager.dataExtra setValue:cardUID forKey:@"cardUUID"];
//        [dataManager.dataExtra setValue:mOldTransactionId forKey:@"transaction_id"];
//        NSLog(@"INI LOH TRANSACTION ID NYAAA... >>>>>>>>> %@",mOldTransactionId);
//        [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id": mOldTransactionId}];
        [userDefaults setValue:mOldTransactionId forKey:@"transaction_id"];

    }
    [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%d",seq] forKey:@"seq"];

    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=update_card_emoney,transaction_id,menu_id,device,device_type,ip_address,language,date_local,id_account,pin,customer_id,seq,cardUUID,cardInfo,lastBalance,cardAttr,dataToSam,message,session,jwt,apiVersion,code" needLoading:false textLoading:@"" encrypted:false banking:false continueLoading:false];

}

- (void) doPostReversalNew API_AVAILABLE(ios(13.0)){
    self->responseState = resStatReversalNewApplet;
    mErrorMessage = @"";
    
//    NFCTagReaderSession* session = self->mSession;
    id<NFCISO7816Tag> nfciso7816Tag = self->mTag;
    
    __block NSString *data = @"";
    __block NSString *cert = @"";
    seq = -1;
    
    dispatch_semaphore_t semapore = dispatch_semaphore_create(0);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^(){
        
        [self getCardCommand:nfciso7816Tag andCommand:@"00E70000" completionHandler:^(NSString* result, NSError* err){
            if(![self isResponseOk:result]){
                self->mErrorMessage = self->resMsgError;
               [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                [self setCardInformation];
                dispatch_suspend(semapore);
            }else{
                data = [self getResponseValue:result];
                dispatch_semaphore_signal(semapore);
            }
        }];
            
        dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
        
        [self getCertificateNewApplet:nfciso7816Tag completionHandler:^(NSString* result, NSError* err){
            cert = result;
            dispatch_semaphore_signal(semapore);
        }];
        
        dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
        
        dispatch_async(dispatch_get_main_queue(), ^(){
            
            NSString* msg = [NSString stringWithFormat:@"%@%@",data,cert];
            NSLog(@"message data + cert :: ->>>> %@",msg);
            
            [self->dataManager.dataExtra setValue:[NSString stringWithFormat:@"%d",self->seq] forKey:@"seq"];
            [self->dataManager.dataExtra setValue:msg forKey:@"message"];
            [self->dataManager.dataExtra setValue:self->cardAttribute forKey:@"cardAttr"];
            [self->dataManager.dataExtra setValue:self->lastBalance forKey:@"lastBalance"];
            [self->dataManager.dataExtra setValue:self->cardInfo forKey:@"cardInfo"];
            [self->dataManager.dataExtra setValue:self->cardUID forKey:@"cardUUID"];
            [self->dataManager.dataExtra setValue:self->cardType forKey:@"apiVersion"];
//            [self->dataManager.dataExtra setValue:self->mOldTransactionId forKey:@"transaction_id"];
//            [self->dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id": self->mOldTransactionId}];
            [self->userDefaults setValue:self->mOldTransactionId forKey:@"transaction_id"];

            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:@"request_type=update_card_emoney,menu_id,device,device_type,ip_address,language,date_local,id_account,pin,customer_id,seq,cardUUID,cardInfo,lastBalance,cardAttr,transaction_id,dataToSam,message,session,jwt,apiVersion,code" needLoading:false textLoading:@"" encrypted:false banking:false continueLoading:false];
        });
    });
}

- (NSData *)dataFromHexString :(NSString*) hexString {
    const char *chars = [hexString UTF8String];
    int i = 0, len = (int)hexString.length;

    NSMutableData *data = [NSMutableData dataWithCapacity:len / 2];
    char byteChars[3] = {'\0','\0','\0'};
    unsigned long wholeByte;

    while (i < len) {
        byteChars[0] = chars[i++];
        byteChars[1] = chars[i++];
        wholeByte = strtoul(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }

    return data;
}

- (BOOL) checkEmoneyCard : (NSString*) select{
    if(![select isEqualToString:responseCommandOK]){
        if([lang(@"ID")isEqualToString:@"ID"]){
//            NSLog(@"NFC - Kartu tidak dikenali");
            self->resMsgError = @"Kartu tidak dikenali";
        }else{
            self->resMsgError = @"Unknown Card";
//            NSLog(@"NFC - UNKNOWN CARD");
        }
        return NO;
    }
    return YES;
}

- (NSString*) getStatusWord : (UInt8) sw1 and:(UInt8) sw2 {
    
    NSString *d1 = [NSString stringWithFormat:@"%02x",sw1];
    NSString *d2 = [NSString stringWithFormat:@"%02x",sw2];
    NSString *statusCode = [NSString stringWithFormat:@"%@%@",d1,d2];
    if([statusCode isEqualToString:responseSwOK]){
        return responseCommandOK;
    }
    return statusCode;
}

- (void) resetEmoneyData{
    cardUID = @"";
    cardInfo = @"";
    lastBalance = @"";
    cardAttribute = @"";
    nBalance = @"";
    cardNumber = @"";
    cardType = @"";
    lastTransactionTime = @"";
}

-(NSString*) padLeft:(NSString*) s intN:(int)n param:(NSString*)param{

    NSString *paddingString = s;
    int i = 0;
    for(i = 0; i < n-s.length; i++){
        paddingString = [NSString stringWithFormat:@" %@",paddingString];
    }

    return [paddingString stringByReplacingOccurrencesOfString:@" " withString:param];
}

- (void) reqGetCardEmoney:(NFCTagReaderSession*) session withTag:(id<NFCISO7816Tag>) tag completionHandler:(completion_lb)completion  API_AVAILABLE(ios(13.0)){
    mSession = session;
    mTag = tag;
    __block NSError* err;
    [self resetEmoneyData];
    
    dispatch_semaphore_t semapore = dispatch_semaphore_create(0);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^(){

        [self getCardSelect:tag completionHandler:^(NSString *result, NSError *error){
            if(error != nil){
                dispatch_suspend(semapore);
            }else{
                dispatch_semaphore_signal(semapore);
            }
            err = error;
        }];
        
        dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
        
        [self getLastBalance:tag completionHandler:^(NSString *result, NSError *error){
            if(error != nil){
                dispatch_suspend(semapore);
            }else{
                dispatch_semaphore_signal(semapore);
            }
            err = error;
        }];
        dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
        [self getCardAttribute:tag completionHandler:^(NSString *result, NSError *error){
            if(error != nil){
                dispatch_suspend(semapore);
            }else{
                dispatch_semaphore_signal(semapore);
            }
            err = error;
        }];
        dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
        
        [self getCardInfo:tag completionHandler:^(NSString* result, NSError* error){
            if(err != nil){
                dispatch_suspend(semapore);
            }else{
                dispatch_semaphore_signal(semapore);
            }
            err = error;
        }];
        dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
        
        NSLog(@"GET MAIN QUEUE");
        dispatch_async(dispatch_get_main_queue(), ^(){
            completion(@"",err);
        });
    });
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"update_card_emoney"]){
            
            if (@available(iOS 13.0, *)) {
                if([responseState isEqualToString:resStatUpdateBalance]){
                    [self doResponseUpdateBalance:jsonObject];
                }else if([responseState isEqualToString:resStatReversalNewApplet]){
                    [self doResponseReversalNewApplet:jsonObject];
                }else if([responseState isEqualToString:resStatReversalOldApplet]){
                    [self doResponseReversalOldApplet:jsonObject];
                }
            }
//        }
    }
}

- (void) doResponseUpdateBalance : (id)jsonObject API_AVAILABLE(ios(13.0)){
    NFCTagReaderSession* session = self->mSession;
    id<NFCISO7816Tag> nfciso7816Tag = self->mTag;
    
    if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
        NSString* trxID = [jsonObject objectForKey:@"transaction_id"];
        NSLog(@"transaction_id %@",trxID);
        NSString *mResponse = [jsonObject objectForKey:@"response"];
        NSDictionary *mDictMessage = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        switch (seq) {
            case 0:

                self->apiVersion = [mDictMessage objectForKey:@"apiVersion"];
                [dataManager.dataExtra setValue:self->apiVersion forKey:@"apiVersion"];
                [dataManager.dataExtra setValue:[mDictMessage objectForKey:@"jwt"] forKey:@"jwt"];
                [dataManager.dataExtra setValue:trxID forKey:@"transaction_id"];
                [userDefaults setValue:trxID forKey:@"transaction_id"];
                
                if([self->apiVersion isEqualToString:cardTypeOldApplet]){
                    [dataManager.dataExtra setValue:self->cardInfo forKey:@"message"];
                    
                    self->lastMessage = self->cardInfo;
                    self->seq += 1;
                    self->sendCommandCount = 0;
                    [self doUpdateEmoney];
                }
                else if([self->apiVersion isEqualToString:cardTypeNewApplet]){
                    NSDictionary *responseBalance = [mDictMessage objectForKey:@"responseBalance"];
                    if(responseBalance){
                        NSString* pendingTopUp = [responseBalance objectForKey:@"pendingTopup"];
                        int intPendingTopup = [pendingTopUp intValue];
                        NSString* amount = [NSString stringWithFormat:@"%2x",intPendingTopup];
                        NSString* amounPadding = [self padLeft:amount intN:8 param:@"0"];
                        
                        NSString* responseBalanaceSession = [responseBalance objectForKey:@"session"];
                        __block NSString* data = @"";
                        __block NSString* dataToSam = @"";
                        
                        dispatch_semaphore_t semapore = dispatch_semaphore_create(0);
                        
                        dispatch_async(dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0), ^(){
                            

                            [self getDataNewApplet:nfciso7816Tag andAmount:amounPadding andSession:responseBalanaceSession completionHandler:^(NSString* result, NSError* err){
                                if(err != nil){
                                    self->mErrorMessage = self->resMsgError;
                                    [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
//                                    [self->mSession invalidateSession];
                                    [self setCardInformation];
                                }
                                data = result;
//                                NSLog(@"get data new applet ::::: %@", data);
                                dispatch_semaphore_signal(semapore);
                            }];
                            
                            
                            dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
                            
                            [self getCertificateNewApplet:nfciso7816Tag completionHandler:^(NSString* result, NSError* err){
                                if(err != nil){
                                    self->mErrorMessage = self->resMsgError;
                                    [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
//                                    [self->mSession invalidateSession];
                                    [self setCardInformation];
                                }
                                dataToSam = [NSString stringWithFormat:@"%@%@",data,result];
//                                NSLog(@"ceritificate applet %@", result);
//                                NSLog(@"get data cert applet ::::: %@", data);
//                                NSLog(@"get data cert datatosam ::::: %@", dataToSam);

                                dispatch_semaphore_signal(semapore);
                            }];
                            
                            dispatch_semaphore_wait(semapore, DISPATCH_TIME_FOREVER);
                            
                            dispatch_async(dispatch_get_main_queue(), ^(){
                                [self->dataManager.dataExtra setValue:responseBalanaceSession forKey:@"session"];
                                [self->dataManager.dataExtra setValue:self->cardInfo forKey:@"cardInfo"];
                                [self->dataManager.dataExtra setValue:dataToSam forKey:@"dataToSam"];
                                
                                self->lastMessage = self->cardInfo;
                                self->seq += 1;
                                self->sendCommandCount = 0;
                                [self doUpdateEmoney];
                            });
                        });
                    }
                }
                
                break;
            case 1:
//                NSLog(@"SEND COMMAND COUNT %d",sendCommandCount);
                if(sendCommandCount == 0){
                    [self setFlagReversal:trxID];
                }
                
                if([self->apiVersion isEqualToString:cardTypeOldApplet]){
                    __block NSString* msg = @"";
                    NSString* command = [jsonObject objectForKey:@"response"];
                    self->lastMessage = command;
                    
                    if([self->lastMessage hasPrefix:@"Balance"]){
                        self->seq +=1;
                        
                        NSRange range = [command rangeOfString:@"_"];
                        if(range.location != NSNotFound)
                        {
                            NSString *result = [command substringWithRange:NSMakeRange(0, range.location)];
                            self->balance = [NSString stringWithFormat:@"%@",[self->lastMessage substringWithRange:NSMakeRange(7, result.length-1)]];
                            
//                            NSLog(@"LAST MESSAGE -%@",self->lastMessage);
//                            NSLog(@"RESULT MESSAGE -%@",result);
//                            NSLog(@"BALANCE -%@",self->balance);
                        }
                        
                        [self removeReversal];
                        
                        msg = responseCommandOK;
                        [dataManager.dataExtra setValue:msg forKey:@"message"];
                        [self doUpdateEmoney];
                    }
                    else{
//                        NSLog(@"delay start");
//                        [session setAlertMessage:@"remove card now"];
//                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10* NSEC_PER_SEC), dispatch_get_main_queue(), ^(){

                        [self getCardCommand:nfciso7816Tag andCommand:command completionHandler:^(NSString* result, NSError* err){
                            if(![self isResponseOk:result]){
                                self->mErrorMessage = self->resMsgError;
                                [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                            }else{
                                msg = result;
                                [self->dataManager.dataExtra setValue:msg forKey:@"message"];
                                [self removeReversal];
                                [self doUpdateEmoney];
                            }
                        }];
//                    });

                    }
                }
                else if([self->apiVersion isEqualToString:cardTypeNewApplet]){
                    NSString* command = [jsonObject objectForKey:@"response"];
//                    NSLog(@"delay start");
//                    [session setAlertMessage:@"remove card now"];
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10* NSEC_PER_SEC), dispatch_get_main_queue(), ^(){

                    [self getCardCommand:nfciso7816Tag andCommand:command completionHandler:^(NSString* result, NSError* err){
                        if(![self isResponseOk:result]){
                            self->mErrorMessage = self->resMsgError;
                            [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                        }else{
                            
                            NSString *lastB = [result substringWithRange:NSMakeRange(60, 8)];
                            NSString *random = [result substringWithRange:NSMakeRange(100, 8)];
                            
                            self->balance = [NSString stringWithFormat:@"%d",[self littleEndian:lastB] ];
                            [self removeReversal];
                            
                            [self->dataManager.dataExtra setValue:random forKey:@"random"];
                            [self->dataManager.dataExtra setValue:responseCommandOK forKey:@"message"];
                            self->seq += 1;
                            [self doUpdateEmoney];
                        }
                    }];
//                    });
                }
                break;
                
            case 2:
                self->mErrorMessage = @"";
                [session setAlertMessage:[jsonObject objectForKey:@"response"]];
                [session invalidateSession];
                [self setCardInformation];
                break;
            default:
                break;
        }
    }else{
        self->mErrorMessage = [jsonObject objectForKey:@"response"];
        [session invalidateSessionWithErrorMessage:[jsonObject objectForKey:@"response"]];
        [self setCardInformation];
    }
    
}

- (void)doResponseReversalOldApplet:(id)jsonObject API_AVAILABLE(ios(13.0)){
    NFCTagReaderSession* session = self->mSession;
    id<NFCISO7816Tag> nfciso7816Tag = self->mTag;
        
    if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
        NSString* trxID = [jsonObject objectForKey:@"transaction_id"];
        NSString *mResponse = [jsonObject objectForKey:@"response"];
        NSDictionary *mDictMessage = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        switch (seq) {
            case -1:
            {
                [dataManager.dataExtra setValue:[mDictMessage objectForKey:@"jwt"] forKey:@"jwt"];
                [dataManager.dataExtra setValue:self->cardType forKey:@"apiVersion"];
                [dataManager.dataExtra setValue:trxID forKey:@"transaction_id"];
                [userDefaults setValue:trxID forKey:@"transaction_id"];
                
                dispatch_group_t localDispatcher = dispatch_group_create();
                dispatch_group_enter(localDispatcher);
                [self getCardCommand:nfciso7816Tag andCommand:[mDictMessage objectForKey:@"response"] completionHandler:^(NSString* result, NSError* err){
                        [self->dataManager.dataExtra setValue:result forKey:@"message"];
                        dispatch_group_leave(localDispatcher);
                }];
                
                dispatch_group_notify(localDispatcher, dispatch_get_main_queue(), ^(){
                    self->seq = 1;
                    self->isResponseReversal = YES;
                    [self doPostReversalOld];
                });
                
                break;
            }
            case 1:
            {
                NSString* command = mResponse;
                if(![command hasPrefix:@"done"]){
//                    NSLog(@"DONE---------------------------%@",command);
                    [self getCardCommand:nfciso7816Tag andCommand:command completionHandler:^(NSString* result, NSError* err){
                        [self->dataManager.dataExtra setValue:result forKey:@"message"];
                        [self doPostReversalOld];
                    }];
                    return;
                    
                }else{

//                    NSLog(@"DONE_00---------------------------%@",command);
                    if([command isEqualToString:@"done_00"]){
                        seq = 0;
                        [self removeReversal];
                        [dataManager.dataExtra setValue:@"" forKey:@"jwt"];
                        [dataManager.dataExtra setValue:@"" forKey:@"apiVersion"];
                        [dataManager.dataExtra setValue:@"" forKey:@"transaction_id"];
                        [userDefaults setValue:@"" forKey:@"transaction_id"];
                        [dataManager.dataExtra setValue:@"" forKey:@"message"];
                        
                        [self reqGetCardEmoney:session withTag:mTag completionHandler:^(NSString* result, NSError* err){
                            if(err != nil){
                                self->mErrorMessage = self->resMsgError;
                                [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                                [self setCardInformation];
                            }else{
                                [self doUpdateEmoney];
                            }
                        }];
//                        [self updateCardEmoney:nfciso7816Tag];
                        
                    }else{
                        self->mErrorMessage = self->resMsgError;
                        [self removeReversal];
                        [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                        [self setCardInformation];
                    }
                }
                break;
            }
            default:
                break;
        }
    }else{
        self->mErrorMessage = [jsonObject objectForKey:@"response"];
        [session invalidateSessionWithErrorMessage:[jsonObject objectForKey:@"response"]];
        [self setCardInformation];
    }
}

- (void) doResponseReversalNewApplet : (id)jsonObject API_AVAILABLE(ios(13)){
    NFCTagReaderSession* session = self->mSession;
    id<NFCISO7816Tag> nfciso7816Tag = self->mTag;
        
    if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
        NSString* trxID = [jsonObject objectForKey:@"transaction_id"];
        NSString *mResponse = [jsonObject objectForKey:@"response"];
        NSDictionary *mDictMessage = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        
        seq = 0;
        [self removeReversal];
        self->apiVersion = [mDictMessage objectForKey:@"apiVersion"];
        [dataManager.dataExtra setValue:self->apiVersion forKey:@"apiVersion"];
        [dataManager.dataExtra setValue:[mDictMessage objectForKey:@"jwt"] forKey:@"jwt"];
        [dataManager.dataExtra setValue:trxID forKey:@"transaction_id"];
        [userDefaults setValue:trxID forKey:@"transaction_id"];
        
        [self reqGetCardEmoney:session withTag:nfciso7816Tag completionHandler:^(NSString* result, NSError* err){
            if(err != nil){
                self->mErrorMessage = self->resMsgError;
                [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
                [self removeReversal];
                [self setCardInformation];
            }else{
                [self doUpdateEmoney];
            }
        }];

    }else{
        self->mErrorMessage = [jsonObject objectForKey:@"response"];
        [session invalidateSessionWithErrorMessage:[jsonObject objectForKey:@"response"]];
        [self setCardInformation];
    }
}

- (BOOL) isResponseOk : (NSString*) message {
    NSString *respCode = [self getResponseCodeCommand:message];
    if([respCode isEqualToString:@""]){
        return NO;
    }
    
    if([respCode isEqualToString:responseCommandOK]){
        return YES;
    }else{
        return NO;
    }
}

- (NSString*) getResponseCodeCommand : (NSString*) message{
    if([message isEqualToString:@""] || message.length < 4){
        return @"";
    }
    
    NSString *result = [message substringFromIndex:message.length - 4];
//    NSLog(@"NFC - response code command : %@",result);
    if(![result isEqualToString:@"9000"]){
//        NSLog(@"NFC - response code command SUCCESS");
    }
    return result;
}

- (void) reqUpdateCardEmoney : (NFCTagReaderSession*) session andTag : (id<NFCISO7816Tag>) tag API_AVAILABLE(ios(13.0)){
    mSession = session;
    mTag = tag;
    
    [self reqGetCardEmoney:session withTag:tag completionHandler:^(NSString* result, NSError* err){
        if(err != nil){
            self->mErrorMessage = self->resMsgError;
            [self->mSession invalidateSessionWithErrorMessage:self->resMsgError];
            [self setCardInformation];
        }else{
            [self updateCardEmoney:tag];
        }
    }];
}

- (void)updateView{
    if([language isEqualToString:@"id"]){
        [mSession setAlertMessage:@"Scan e-money selesai"];
    }else{
        [mSession setAlertMessage:@"Scan e-money done"];
    }
    [mSession invalidateSession];
    
    NSString* icn = @"";
    NSString* iltt = @"";
    if([language isEqualToString:@"id"]){
        icn = [NSString stringWithFormat:@"Nomor Kartu : %@",cardNumber];
        iltt = [NSString stringWithFormat:@"Transaksi Terakhir : %@",lastTransactionTime];
    }else{
        icn = [NSString stringWithFormat:@"Nomor Kartu : %@",cardNumber];
        iltt = [NSString stringWithFormat:@"Transaksi Terakhir : %@",lastTransactionTime];
    }
    NSString *info = @"";
    if(lastTransactionTime == nil || [lastTransactionTime isEqualToString:@""]){
        info = [NSString stringWithFormat:@"%@",icn];
    }else{
        info = [NSString stringWithFormat:@"%@ \n%@",icn,iltt];
    }
    
    [self.lblTitle setText:info];
    [self.lblTitle setTextAlignment:NSTextAlignmentLeft];
    [self.lblNominal setText:[NSString stringWithFormat:@"Rp. %@",[currencyFormatter stringFromNumber:[NSNumber numberWithInteger:[lastBalance integerValue]]]]];
    [self.imageTap setHidden:YES];
    
}

- (void)setCardInformation{
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc]init];
    
    [newDict setValue:mErrorMessage forKey:@"error"];
    [newDict setValue:cardUID forKey:@"carduid"];
    [newDict setValue:cardInfo forKey:@"cardinfo"];
    [newDict setValue:cardType forKey:@"cardtype"];
    [newDict setValue:cardNumber forKey:@"cardnumber"];
    [newDict setValue:cardAttribute forKey:@"cardattr"];
    [newDict setValue:cardSelected forKey:@"cardselect"];
    [newDict setValue:lastBalance forKey:@"lastbalance"];
    [newDict setValue:lastTransactionTime forKey:@"lasttrxtime"];
    [newDict setValue:balance forKey:@"newbalance"];
    
//    NSLog(@"setCardInformation ::: >> %@",mErrorMessage);
    if([mErrorMessage isEqualToString:@""] || mErrorMessage == nil){
        if([[dataManager.dataExtra valueForKey:@"code"] isEqualToString:@"00063"]){
            if([language isEqualToString:@"id"]){
                [mSession setAlertMessage:@"Update Kartu selesai"];
            }else{
                [mSession setAlertMessage:@"Updating card done"];
            }
        }else{
            if([language isEqualToString:@"id"]){
                [mSession setAlertMessage:@"Deteksi kartu selesai"];
            }else{
                [mSession setAlertMessage:@"Detecting card done"];
            }
        }
        [mSession invalidateSession];
    }
    cardInformations = newDict;
    [delegate cardInformation:cardInformations];
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
    
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setTitleBar:(NSString *)newTitle{
    titlBar = newTitle;
}


@end
