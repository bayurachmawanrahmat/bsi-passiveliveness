//
//  LV05ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "LV05ViewController.h"
#import "Connection.h"
#import "Utility.h"
#import "TambahComplainViewController.h"



@interface LV05ViewController ()<ConnectionDelegate, UIAlertViewDelegate, UITableViewDataSource>{
    NSArray *listData;
    NSUserDefaults *userDefault;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgHeaderLv5;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)ok:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;

@end

@implementation LV05ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    [self setupLayout];
    _imgHeaderLv5.image = [UIImage imageNamed:img];
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    self.labelTitle.text = [self.jsonData valueForKey:@"title"];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if([[self.jsonData valueForKey:@"url"]boolValue]){
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"]  needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
    
//    if([[userDefault valueForKey:@"complain_state"] isEqualToString:@"YES"]){
    if([dataManager.dataExtra objectForKey:@"complain_state"]){
        if([[dataManager.dataExtra valueForKey:@"complain_state"] isEqualToString:@"YES"]){
            if([lang(@"LANGUAGE") isEqualToString:@"ID"]){
                [self.labelTitle setText:@"Pilih Transaksi yang Disanggah"];
            }else{
                [self.labelTitle setText:@"Select Transaction Complaint"];
            }
        }
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */
-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.labelTitle.frame;
    CGRect frmTbl = self.tableView.frame;
    CGRect frmBtn = self.btnOk.frame;
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.origin.x = 0;
    frmVwTitle.size.height = 50;;
    
    frmLblTitle.origin.y = 5;
    frmLblTitle.origin.x = 8;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmTbl.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    
    frmBtn.origin.x = 16;
    frmBtn.size.width = SCREEN_WIDTH - (frmBtn.origin.x*2);
    frmBtn.size.height = 42;
    frmBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtn.size.height - 16;
    
    
    if ([Utility isDeviceHaveNotch]) {
        frmBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtn.size.height - 40;
    }
    
    frmTbl.size.width = SCREEN_WIDTH;
    frmTbl.size.height = frmBtn.origin.y - frmTbl.origin.y - 8;
    
    if([dataManager.dataExtra objectForKey:@"complain_state"]){
        if([[dataManager.dataExtra valueForKey:@"complain_state"] isEqualToString:@"YES"]){
            self.btnOk.hidden = YES;
            frmTbl.size.height = SCREEN_HEIGHT - (BOTTOM_NAV+TOP_NAV);
            
        }
    }
    
    self.tableView.frame = frmTbl;
    self.btnOk.frame = frmBtn;
    self.vwTitle.frame = frmVwTitle;
    self.labelTitle.frame = frmLblTitle;
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV05Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    
    NSArray *arr = [listData objectAtIndex:[indexPath row]];
    NSString *ar1 = [arr objectAtIndex:0];
    NSString *ar2 = [arr objectAtIndex:1];
    NSString *ar3 = [arr objectAtIndex:2];
    NSString *ar4 = [arr objectAtIndex:3];
    NSString *ar5 = [arr objectAtIndex:4];

    NSString *text =[NSString stringWithFormat:@"%@ \n%@ \n%@ \n%@ \n%@", ar1, ar2, ar3, ar4, ar5];
    
    [label setText:text];
    
//    CGSize constraint = CGSizeMake(300.0, 20000.0f);
////    CGSize size = text soze
    CGSize size = [text sizeWithAttributes:
        @{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]}];
//    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    [label sizeToFit];
    


    // Values are fractional -- you should take the ceilf to get equivalent values
//    CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    NSString *text = [listData objectAtIndex:[indexPath row]];
    NSArray *arr = [listData objectAtIndex:[indexPath row]];
    NSString *ar1 = [arr objectAtIndex:0];
    NSString *ar2 = [arr objectAtIndex:1];
    NSString *ar3 = [arr objectAtIndex:2];
    NSString *ar4 = [arr objectAtIndex:3];
    NSString *ar5 = [arr objectAtIndex:4];



    NSString *text =[NSString stringWithFormat:@"%@ \n%@ \n%@ \n%@ \n%@", ar1, ar2, ar3, ar4, ar5];
//    CGSize constraint = CGSizeMake(300.0, 20000.0f);
    CGSize size = [text sizeWithAttributes:
        @{NSFontAttributeName: [UIFont systemFontOfSize:17.0f]}];
//    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+10.0;
}
#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSArray *selectedMenu = [listData objectAtIndex:indexPath.row];
//    if([[userDefault valueForKey:@"complain_state"] isEqualToString:@"YES"]) {
//    if([dataManager.dataExtra objectForKey:@"complain_state"]){
    if([[dataManager.dataExtra valueForKey:@"complain_state"] isEqualToString:@"YES"]){
        
        if([lang(@"LANGUAGE") isEqualToString:@"ID"]){
            [self.labelTitle setText:@"Pilih Transaksi yang disanggah"];
        }else{
            [self.labelTitle setText:@"Select Transaction Complaint"];
        }
        
//        NSArray *sliceListData=[[listData objectAtIndex:indexPath.row] componentsSeparatedByString:@"\n"];
//
//        NSLog(@"COMPLAIN %@", sliceListData);
//
//        NSArray *tanggal=[[sliceListData objectAtIndex:0] componentsSeparatedByString:@": "];
//        [dataManager.dataExtra addEntriesFromDictionary:@{[tanggal objectAtIndex:0]:[tanggal objectAtIndex:1]}];
//
//        NSLog(@"%@", [sliceListData objectAtIndex:0]);
//
//        NSArray *jam=[[sliceListData objectAtIndex:1] componentsSeparatedByString:@": "];
//        [dataManager.dataExtra addEntriesFromDictionary:@{[jam objectAtIndex:0]:[jam objectAtIndex:1]}];
//
//        NSArray *debit=[[sliceListData objectAtIndex:2] componentsSeparatedByString:@"."];
//        [dataManager.dataExtra addEntriesFromDictionary:@{@"debit":[debit objectAtIndex:1]}];
//
//        NSArray *ket=[[sliceListData objectAtIndex:3] componentsSeparatedByString:@": "];
//        [dataManager.dataExtra addEntriesFromDictionary:@{@"ket":[ket objectAtIndex:1]}];
//
//        NSArray *ref=[[sliceListData objectAtIndex:4] componentsSeparatedByString:@": "];
//        NSLog(@"ref %@", ref);
//        [dataManager.dataExtra addEntriesFromDictionary:@{[ref objectAtIndex:0]:[ref objectAtIndex:1]}];
        
        NSArray *sliceData = [listData objectAtIndex:indexPath.row];
        
        NSArray *tanggal = [[sliceData objectAtIndex:0] componentsSeparatedByString:@": "];
        [dataManager.dataExtra addEntriesFromDictionary:@{[tanggal objectAtIndex:0]:[tanggal objectAtIndex:1]}];
        
        NSArray *jam = [[sliceData objectAtIndex:1] componentsSeparatedByString:@": "];
        [dataManager.dataExtra addEntriesFromDictionary:@{[jam objectAtIndex:0]:[jam objectAtIndex:1]}];

        NSArray *debit = [[sliceData objectAtIndex:2] componentsSeparatedByString:@"."];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"debit":[debit objectAtIndex:1]}];

        NSArray *ket = [[sliceData objectAtIndex:3] componentsSeparatedByString:@": "];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"ket":[ket objectAtIndex:1]}];

        NSArray *ref = [[sliceData objectAtIndex:4] componentsSeparatedByString:@": "];
        [dataManager.dataExtra addEntriesFromDictionary:@{[ref objectAtIndex:0]:[ref objectAtIndex:1]}];



//        NSArray *temp = [Utility changeToArray:[listData objectAtIndex:indexPath.row]];
            TambahComplainViewController *ajukanComplain = [self.storyboard instantiateViewControllerWithIdentifier:@"AJUKANCOMPLAIN"];
//                                      [self presentViewController:popBantuan animated:YES completion:nil];
        
                   [self.tabBarController setSelectedIndex:0];
                   UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                   [currentVC pushViewController:ajukanComplain animated:YES];
        
                  NSDictionary* userInfo = @{@"position": @(1118)};
                  NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                  [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
//        [userDefault removeObjectForKey:@"complain_state"];
    }
}

#pragma mark - Action

- (IBAction)ok:(id)sender {
    [super backToRoot];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        if (![requestType isEqualToString:@"check_notif"]) {
            id response = [jsonObject objectForKey:@"response"];
//            NSString *stringResponse;
            if([response isKindOfClass:[NSArray class]]){
                NSArray *temp = response;
//                NSArray *tempArray = [Utility changeToArray:temp combinedSub:(![dataManager.menuId isEqualToString:@"00009"]&&![dataManager.menuId isEqualToString:@"00010"])];
                //        NSMutableArray *tempMutArray = [[NSMutableArray alloc]init];
                //        for(NSString *tempData in tempArray){
                //            [tempMutArray addObject:[tempData componentsSeparatedByString:@"\n"]];
                //        }
//                if ([tempArray count] > 0) {
//                    listData = tempArray;
//                    [self.tableView reloadData];
//                }
                if ([temp count] > 0) {
                    listData = temp;
                    [self.tableView reloadData];
                }
                else {
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
                    UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                    [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self backToRoot];
                    }]];
                    [self presentViewController:alerts animated:YES completion:nil];
                }
            } else {
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToRoot];
                }]];
                [self presentViewController:alerts animated:YES completion:nil];
            }
            
            if([jsonObject objectForKey:@"transaction_id"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"] }];
            }
            if([jsonObject objectForKey:@"share"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"share":[jsonObject valueForKey:@"share"] }];
            }
            
        }
    }else{
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
        UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
        [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToRoot];
        }]];
        [self presentViewController:alerts animated:YES completion:nil];
    }
}

- (void)errorLoadData:(NSError *)error{
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
//
    UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToRoot];
    }]];
    [self presentViewController:alerts animated:YES completion:nil];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(alertView.tag == 100)
//        [super openActivation];
//    else
//        [self backToRoot];
//}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
