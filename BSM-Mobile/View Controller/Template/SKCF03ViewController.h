//
//  SKCF03ViewController.h
//  BSM-Mobile
//
//  Created by ARS on 26/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKCF03ViewController : TemplateViewController

- (void) setMessage : (NSString*) msg;
- (void) setButtonTitle : (NSString*) btnTitle;

@end

NS_ASSUME_NONNULL_END
