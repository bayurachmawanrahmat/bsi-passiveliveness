//
//  OAM01ViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 12/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "OAM01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"

@interface OAM01ViewController()<ConnectionDelegate, UIAlertViewDelegate> {
    NSString* requestType;
    NSString* urlSyarat;
    bool chaked;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UITextField *textFiledSaldo;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatalMain;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSetuju;
@property (weak, nonatomic) IBOutlet UIImageView *imgChecked;
@property (weak, nonatomic) IBOutlet UILabel *lblSyarat;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree;
@property (weak, nonatomic) IBOutlet UILabel *lblSaldo;
@property (weak, nonatomic) IBOutlet UIView *line;
@property (weak, nonatomic) IBOutlet UIImageView *imgChecked2;
@property (weak, nonatomic) IBOutlet UIImageView *imgChecked3;
@property (weak, nonatomic) IBOutlet UIView *vwAgree3;
@property (weak, nonatomic) IBOutlet UIView *vwAgree;
@property (weak, nonatomic) IBOutlet UIView *vwAgree2;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree3;
@property (weak, nonatomic) IBOutlet UILabel *lblAgree2;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree2;
@property (weak, nonatomic) IBOutlet UIButton *btnAgree3;
@property (weak, nonatomic) IBOutlet UIView *vwSyarat;
@property (weak, nonatomic) IBOutlet UIButton *btnSyarat;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet UIScrollView *vwBody;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation OAM01ViewController

NSString *statA1;
NSString *statA2;
NSString *statA3;
NSString *isZakat;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to next and other else

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [Styles setTopConstant:_topConstant];
    
    statA1 = @"0";
    statA2 = @"0";
    statA3 = @"0";
    isZakat = @"N";
    
    urlSyarat = @"https://www.syariahmandiri.co.id/";
    chaked = false;
    [_btnBatalMain setHidden:NO];
    [_btnSetuju setHidden:YES];
    [_btnBatal setHidden:YES];
    [_lblSaldo setHidden:YES];
    [_textFiledSaldo setHidden:YES];
    [_line setHidden:YES];
    [_vwAgree2 setHidden:YES];
    
    [_btnBatalMain setColorSet:PRIMARYCOLORSET];
    [_btnBatal setColorSet:PRIMARYCOLORSET];
    [_btnSetuju setColorSet:SECONDARYCOLORSET];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        _lblSyarat.text = @"Syarat dan Ketentuan";
        _lblAgree.text = @"Saya telah membaca Syarat Umum Pembukaan Rekening dan Setuju";
        
        [_btnBatal setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Batal" forState:UIControlStateNormal];
        
        [_btnBatalMain setTitle:@"Batal" forState:UIControlStateNormal];
        
        _lblSaldo.text = @"Saldo Awal";
        _lblAgree2.text = @"Zakat atas bagi hasil";
    } else {
        _lblSyarat.text = @"Terms and Conditions";
        _lblAgree.text = @"I have read the General Terms of Account Opening and Agreement";
        
        [_btnBatal setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [_btnBatalMain setTitle:@"Cancel" forState:UIControlStateNormal];
        
        _lblSaldo.text = @"Beginning balance";
        _lblAgree2.text = @"Zakat for profit sharing";
    }
    

    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.textFiledSaldo.inputAccessoryView = keyboardDoneButtonView;

    
    NSString *url =[NSString stringWithFormat:@"%@,zakat",[self.jsonData valueForKey:@"url_parm"]];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
    [self registerForKeyboardNotifications];
}


- (IBAction)actionOpenWeb:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlSyarat] options:@{} completionHandler:nil];
    
}

- (IBAction)actionAgree:(id)sender {
    if ([statA1 isEqual:@"0"]) {
        statA1 = @"1";
        if (([statA1 isEqual:@"1"]) && ([statA3 isEqual:@"1"])) {
            [_btnBatalMain setHidden:YES];
            [_btnSetuju setHidden:NO];
            [_btnBatal setHidden:NO];
            [_lblSaldo setHidden:NO];
            [_textFiledSaldo setHidden:NO];
            [_line setHidden:NO];
            [_vwAgree2 setHidden:NO];
        }
        _imgChecked.image = [UIImage imageNamed: @"checked.png"];
    }
    else {
        statA1 = @"0";
        statA2 = @"0";
        [_btnBatalMain setHidden:NO];
        [_btnSetuju setHidden:YES];
        [_btnBatal setHidden:YES];
        [_lblSaldo setHidden:YES];
        [_textFiledSaldo setHidden:YES];
        [_line setHidden:YES];
        [_vwAgree2 setHidden:YES];
        _imgChecked.image = [UIImage imageNamed: @"blank_check.png"];
        _imgChecked2.image = [UIImage imageNamed: @"blank_check.png"];
    }
    
}

- (IBAction)actionAgree2:(id)sender {
    if ([statA2 isEqual:@"0"]) {
        statA2 = @"1";
        isZakat = @"Y";
        _imgChecked2.image = [UIImage imageNamed: @"checked.png"];
    }
    else {
        statA2 = @"0";
        isZakat = @"N";
        _imgChecked2.image = [UIImage imageNamed: @"blank_check.png"];
    }
}

- (IBAction)actionAgree3:(id)sender {
    if ([statA3 isEqual:@"0"]) {
        statA3 = @"1";
        if (([statA1 isEqual:@"1"]) && ([statA3 isEqual:@"1"])) {
            [_btnBatalMain setHidden:YES];
            [_btnSetuju setHidden:NO];
            [_btnBatal setHidden:NO];
            [_lblSaldo setHidden:NO];
            [_textFiledSaldo setHidden:NO];
            [_line setHidden:NO];
            [_vwAgree2 setHidden:NO];
        }
        _imgChecked3.image = [UIImage imageNamed: @"checked.png"];
    }
    else {
        statA2 = @"0";
        statA3 = @"0";
        [_btnBatalMain setHidden:NO];
        [_btnSetuju setHidden:YES];
        [_btnBatal setHidden:YES];
        [_lblSaldo setHidden:YES];
        [_textFiledSaldo setHidden:YES];
        [_line setHidden:YES];
        [_vwAgree2 setHidden:YES];
        _imgChecked2.image = [UIImage imageNamed: @"blank_check.png"];
        _imgChecked3.image = [UIImage imageNamed: @"blank_check.png"];
    }
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

#pragma mark chance actionSetuju to goToCancel
- (IBAction)actionSetuju:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    if([self.textFiledSaldo.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }else{

        NSString *inputString = self.textFiledSaldo.text;
        NSString *trimmedString = [inputString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        
        if ([trimmedString length]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"STR_NOT_NUM") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else {
            if ([self.textFiledSaldo.text intValue] < 100000) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                
                if([lang isEqualToString:@"id"]){
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Saldo minimal adalah Rp 100.000" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Minimum balance is Rp 100.000" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            } else {
                [dataManager.dataExtra addEntriesFromDictionary:@{@"amount":self.textFiledSaldo.text}];
                //[dataManager.dataExtra addEntriesFromDictionary:@{@"zakat":@"Y"}];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"zakat":isZakat}];
                //[dataManager.dataExtra addEntriesFromDictionary:@{@"branch_code":@"ID0010175"}];
                //[dataManager.dataExtra addEntriesFromDictionary:@{@"branch_name":@"KCP JKT KLENDER"}];
                [super openNextTemplate];
            }
        }
    }
}

#pragma mark change actionBatal to goToNext
- (IBAction)actionBatal:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    [self backToRoot];
}
- (IBAction)actionbatalMain:(id)sender {
    [self backToRoot];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
//        NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[jsonObject dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject valueForKey:@"response"];
                NSArray *stringArray = [response componentsSeparatedByString:@"|"];
                
                if (stringArray.count == 1) {
                    _lblContent.text = stringArray[0];
                    urlSyarat = @"";
                    _lblAgree3.text = @"";
                }
                else if (stringArray.count == 2) {
                    _lblContent.text = stringArray[0];
                    urlSyarat = stringArray[1];
                    _lblAgree3.text = @"";
                }
                else if (stringArray.count == 3) {
                    _lblContent.text = stringArray[0];
                    urlSyarat = stringArray[1];
                    _lblAgree3.text = @"";
                }
                else if (stringArray.count == 4) {
                    _lblContent.text = stringArray[0];
                    urlSyarat = stringArray[1];
                    _lblAgree3.text = stringArray[3];
                }
                
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }

    }
    
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else {
        [self backToRoot];
    }
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.vwBody.contentInset = contentInsets;
    self.vwBody.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwBody.contentInset = contentInsets;
    self.vwBody.scrollIndicatorInsets = contentInsets;
}
@end
