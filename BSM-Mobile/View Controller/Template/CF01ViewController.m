//
//  CF01ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/13/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "CF01ViewController.h"

@interface CF01ViewController ()<ConnectionDelegate, UIAlertViewDelegate>
//@property (weak, nonatomic) IBOutlet UILabel *labelConfirmation;
@property (weak, nonatomic) IBOutlet UITextView *textViewConfirmation;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblExclamation;
@property (weak, nonatomic) IBOutlet UIView *viewExclamation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

- (IBAction)next:(id)sender;
- (IBAction)batal:(id)sender;


@end

@implementation CF01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad {
    
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    
    [Styles setTopConstant:_topConstant];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        [self.lblExclamation setText:@"Apabila anda setuju, silahkan tekan 'Selanjutnya'"];
    } else {
        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.lblExclamation setText:@"If you agree, press 'Next'"];
    }
    
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    [super viewDidLoad];
    
//    [self setupLayout];
    self.viewExclamation.layer.cornerRadius = 10;
    self.viewExclamation.layer.masksToBounds = YES;

    [self.textViewConfirmation setFont:[UIFont fontWithName:const_font_name1 size:15]];
    if(self.jsonData !=nil && [self.jsonData objectForKey:@"content"] && ![[[self.jsonData objectForKey:@"content"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]isEqualToString:@""]){
        [self.textViewConfirmation setText:[self.jsonData valueForKey:@"content"]];
//        [self.labelConfirmation setTextAlignment:NSTextAlignmentJustified];
//        [self.labelConfirmation sizeToFit];
    }
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00028"]) {
            NSString *url =[NSString stringWithFormat:@"%@,type_transfer",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        } else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00047"]) {
            NSString *url =[NSString stringWithFormat:@"%@,tips,admfee,amount,remark,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        } else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00065"]) {
            NSString *url =[NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        } else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00021"]) {
            NSString *url =[NSString stringWithFormat:@"%@,amount,id_account",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        } else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00020"]) {
            NSString *url =[NSString stringWithFormat:@"%@,amount,id_account,pin,zakat",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        } else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00070"]) {
           NSString *url =[NSString stringWithFormat:@"%@,txt1,txt2,txt3",[self.jsonData valueForKey:@"url_parm"]];
           [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
           Connection *conn = [[Connection alloc]initWithDelegate:self];
           [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
        else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00201"]) {
            NSString *url =[NSString stringWithFormat:@"%@,menu_id,id_account,customer_id,code,id,hewan_qurban,target_harga,target_waktu,frekuensi,target_mulai,setoran,dayofweek,dayofmonth,type_autosave,account_title",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
        else if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00135"] && [[dataManager.dataExtra valueForKey:@"code"] isEqualToString:@"00138"]){
            NSString *url =[NSString stringWithFormat:@"%@,menu_id,id_account,customer_id,code,id,hewan_qurban,target_harga,target_waktu,frekuensi,target_mulai,setoran,dayofweek,dayofmonth,type_autosave,account_title",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
        else if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00135"] && [[dataManager.dataExtra valueForKey:@"code"] isEqualToString:@"00137"]){
            NSString *url =[NSString stringWithFormat:@"%@,destacctno,availbal,srcacctno,descroprodext,workingbalance,date_local,menu_id,pin",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
        else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00136"]) {
            NSString *url =[NSString stringWithFormat:@"%@,msisdn,price,pilihan,desc_amil",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
        else{
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
  
    }
    // Do any additional setup after loading the view.

    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [_lblExclamation setHidden:YES];
    [_viewExclamation setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmTextConfrim = self.textViewConfirmation.frame;
    CGRect frmBtnB = self.btnCancel.frame;
    CGRect frmBtnN = self.btnNext.frame;
    
    CGRect frmImgIcon = self.imgIcon.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmBtnN.size.height = 42;
    frmBtnN.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtnN.size.height - 8;
    
    if ([Utility isDeviceHaveNotch]) {
        frmBtnN.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_X;
    }
    
    frmBtnN.origin.x = 16;
    frmBtnN.size.width = SCREEN_WIDTH/2 - (frmBtnN.origin.x *2);
    
    
    frmBtnB.origin.y = frmBtnN.origin.y;
    frmBtnB.origin.x = frmBtnN.origin.x + frmBtnN.size.width + 24;
    frmBtnB.size.width = SCREEN_WIDTH - frmBtnB.origin.x - 16;
    frmBtnB.size.height = frmBtnN.size.height;
    
    frmTextConfrim.origin.x = 16;
    frmTextConfrim.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    frmTextConfrim.size.height = frmBtnN.origin.y - frmTextConfrim.origin.y - 8;
    frmTextConfrim.size.width = SCREEN_WIDTH - (frmTextConfrim.origin.x*2);
    
    frmLblTitle.origin.x = frmImgIcon.origin.x + frmImgIcon.size.width + 8;
    frmLblTitle.origin.y = frmVwTitle.size.height/2 - frmLblTitle.size.height/2;
    frmLblTitle.size.width = frmVwTitle.size.width - frmLblTitle.origin.x - 16;
    
    self.vwTitle.frame = frmVwTitle;
    self.vwTitle.backgroundColor = [UIColor whiteColor];
    self.textViewConfirmation.frame = frmTextConfrim;
    self.btnCancel.frame = frmBtnB;
    self.btnNext.frame = frmBtnN;
    self.lblTitle.frame = frmLblTitle;
    [self.lblTitle setTextAlignment:NSTextAlignmentCenter];
    
}

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

#pragma mark change batal to goToNext;
- (IBAction)batal:(id)sender {
//    [self goToNext];
    [self backToRoot];
}

-(void) goToNext{
    [super openNextTemplate];
}

-(void) goToCancel{
//    [super backToRoot];
    [self goToNext];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
    //adding data extra for minimal pemesanan
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *text = [[jsonObject valueForKey:@"response"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            [self.textViewConfirmation setText:text];
            
            [[[DataManager sharedManager]dataExtra]setObject:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
            if([jsonObject valueForKey:@"min_pesan"]){
                [[[DataManager sharedManager]dataExtra]setObject:[jsonObject valueForKey:@"min_pesan"] forKey:@"min_pesan"];
                [[[DataManager sharedManager]dataExtra]setObject:[jsonObject valueForKey:@"multipleOrder"] forKey:@"multiple_order"];
            }
            
            if([jsonObject objectForKey:@"share"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"share":[jsonObject valueForKey:@"share"] }];
            }
            
            if([[jsonObject objectForKey:@"jenisSukuk"] isEqual: @"1"]){
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:@"Informasi" message:[jsonObject objectForKey:@"popupMsg"] preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alerts animated:YES completion:nil];
            }
//            [self setupLayout];
        } else {
            if ([jsonObject valueForKey:@"response"]){
                if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
//                    NSString * msg = [self generateMessage:[jsonObject valueForKey:@"response_code"] :[jsonObject valueForKey:@"response"]];
                    NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                    
                    UIAlertController *aler = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
                    [aler addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self backToRoot];
                    }]];
                    [self presentViewController:aler animated:YES completion:nil];
                } else {
                    NSString * msg = [jsonObject valueForKey:@"response"];
                    UIAlertController *aler = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
                    [aler addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self backToRoot];
                    }]];
                    [self presentViewController:aler animated:YES completion:nil];
                }
            } else {
//                NSString * msg = [self generateMessage:[jsonObject valueForKey:@"response_code"] :[jsonObject valueForKey:@"response"]];
                NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                UIAlertController *aler = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
                [aler addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToRoot];
                }]];
                [self presentViewController:aler animated:YES completion:nil];
            }
            
            
        }
        DLog(@"%@", jsonObject);
        
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertController *aler = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    [aler addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToRoot];
    }]];
    [self presentViewController:aler animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

@end
