//
//  PF01ViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 05/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PF01ViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
