//
//  SKCF05ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/16/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SKCF05ViewController.h"
#import "Connection.h"
#import "Utility.h"
#import "CellSbn.h"
#import "SKCF05Cell.h"
//#import "PopupRedemtionViewController.h"

@interface SKCF05ViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate,
UIScrollViewDelegate> {
    NSString *prodName;
    NSString* requestType;
    NSArray *listAction;
    BOOL mustAddToManager;
    NSDictionary *dataObject;
    CellSbn *cellSbn;
    NSArray *selectedMenu;
    NSArray *redemContent;
    NSNumberFormatter* currencyFormatter;
    
    UIImageView *imgIcon;
    UILabel *lblJudul;
    UITextField *nominal;
    UIView *viewBg;
    UIView *viewContiner;
    UIScrollView *vwScroll;
    UIView *vwBox;
    double vNominal;
    double vAvaRedeem;
    double vMinRedeem;
    double vMultiple;
    UIToolbar *toolbar;
    NSString *titleToolbar;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *vwProduk;
@property (weak, nonatomic) IBOutlet UILabel *lblProduk;
@property (weak, nonatomic) IBOutlet UILabel *lblSeries;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation SKCF05ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)doneAction:(id)sender {
    [nominal endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.imgTitle.hidden = YES;
    [Styles setTopConstant:_topConstant];
    self.lblTitle.text = [self.jsonData valueForKey:@"title"];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.lblProduk.text = [dataManager.dataExtra valueForKey:@"title"];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@"."];
        
    
    toolbar = [[UIToolbar alloc] init];
    [toolbar sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneAction:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [toolbar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
//    [self.tableView registerClass:[CellSbn class] forCellReuseIdentifier:@"SKCF05cell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SKCF05Cell" bundle:nil] forCellReuseIdentifier:@"SKCF05CELL"];
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    NSString *urlParm = [NSString stringWithFormat:@"%@,id_series",[self.jsonData valueForKey:@"url_parm"]];
    [conn sendPostParmUrl:urlParm needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
    id data =  [dataObject objectForKey:@"content"];
    if([data isKindOfClass:[NSString class]]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        mustAddToManager = true;
        listAction = data;
//        [self.tableview reloadData];
    }
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmVwProduk = self.vwProduk.frame;
    CGRect frmTbView = self.tableView.frame;
    
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.height = 40;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    
    frmVwProduk.origin.x = 16;
    frmVwProduk.origin.y = frmVwTitle.origin.y +frmVwTitle.size.height+16;
    frmVwProduk.size.width = SCREEN_WIDTH - (frmVwProduk.origin.x*2);
    frmTbView.origin.x = 16;
    frmTbView.origin.y = frmVwProduk.origin.y +frmVwProduk.size.height+16;
    frmTbView.size.width = SCREEN_WIDTH - (frmTbView.origin.x*2);
    
    if ([Utility isDeviceHaveNotch]) {
        frmTbView.size.height = SCREEN_HEIGHT - (BOTTOM_NAV+TOP_NAV+frmVwTitle.size.height+frmVwProduk.size.height);
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
   
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listAction.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SKCF05Cell *cell = (SKCF05Cell*)[tableView dequeueReusableCellWithIdentifier:@"SKCF05CELL"];
    NSString *fullString = [listAction objectAtIndex:indexPath.row];
    
    if([Utility isLanguageID]){
        cell.titleKodePemesanan.text = @"Kode Pemesanan";
        cell.titleAvailableRedeem.text = @"Available Redemption";
        cell.titleNominalPemesanan.text = @"Nominal Pemesanan";
    }else{
        cell.titleKodePemesanan.text = @"Order Code";
        cell.titleAvailableRedeem.text = @"Available Redemption";
        cell.titleNominalPemesanan.text = @"Nominal Order";
    }

    
    cell.availableRedeem.text = [Utility currencyVal:[[fullString valueForKey:@"maks_redeem"]doubleValue]];
    cell.kodePemesanan.text = [fullString valueForKey:@"kode_pemesanan"];
    cell.nominalPemesanan.text = [Utility currencyVal:[[fullString valueForKey:@"nominal_pemesanan"]doubleValue]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
      selectedMenu = [listAction objectAtIndex:indexPath.row];
    

    if([[selectedMenu valueForKey:@"maks_redeem"]doubleValue] == 0){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"NOT_AVAILABLE_REDEEM") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            if(self->listAction.count == 1){
                [self backToR];
            }
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self showAbout];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated {

     [viewBg removeFromSuperview];
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject objectForKey:@"response"];
                NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                prodName = [dict objectForKey:@"product"];
                self.lblProduk.text = prodName;
                listAction = [dict objectForKey:@"data"];
                
                [self.tableView reloadData];
            }
           
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }
    } else {

    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

-(void) showAbout {
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat widthContiner =  width - 40;
    
    vwScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    viewBg = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    viewBg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
//    vwScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(20, (height/2) - (210/2), widthContiner, 210)];
    viewContiner = [[UIView alloc]initWithFrame:CGRectMake(20, (height/2) - (210/2), widthContiner, 290)];
    viewContiner.backgroundColor = [UIColor whiteColor];
    viewContiner.layer.borderWidth = 2;
    viewContiner.layer.cornerRadius = 10;
    viewContiner.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    
    lblJudul = [[UILabel alloc] initWithFrame:CGRectMake((widthContiner - 148) / 2, 0, 148, 40)];
    [lblJudul setFont:[UIFont fontWithName:@"Lato-Regular" size:17]];
    [lblJudul setTextColor:[UIColor blackColor]];
    
    UIView *viewLn = [[UIView alloc]initWithFrame:CGRectMake(0, lblJudul.frame.origin.y+ lblJudul.frame.size.height, widthContiner, 1)];
    viewLn.backgroundColor = [UIColor grayColor];
    
    vwBox = [[UIView alloc]initWithFrame:CGRectMake(5, viewLn.frame.origin.y+10, widthContiner - 10, 140)];
    vwBox.layer.cornerRadius = 10.0f;
    vwBox.layer.shadowColor = [[UIColor grayColor]CGColor];
    vwBox.layer.shadowOffset = CGSizeMake(0, 2);
    vwBox.layer.shadowOpacity = 0.5f;
    vwBox.backgroundColor = [UIColor whiteColor];
    vwBox.layer.shadowRadius = 5.0;
    vwBox.layer.masksToBounds = NO;
    
    UILabel *lblKdPesan = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, vwBox.frame.size.width/2, 20)];
    [lblKdPesan setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [lblKdPesan setTextColor:[UIColor grayColor]];
    
    UILabel *lblNmPesan = [[UILabel alloc]initWithFrame:CGRectMake(lblKdPesan.frame.origin.x+(vwBox.frame.size.width/2), 10, vwBox.frame.size.width/2, 20)];
    [lblNmPesan setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [lblNmPesan setTextColor:[UIColor grayColor]];
    
    UILabel *lblValueKdPesan = [[UILabel alloc]initWithFrame:CGRectMake(10,lblKdPesan.frame.origin.y+lblKdPesan.frame.size.height+5, vwBox.frame.size.width/2, 20)];
   [lblValueKdPesan setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
   [lblValueKdPesan setTextColor:[UIColor blackColor]];
    
    UILabel *lblRedeem = [[UILabel alloc]initWithFrame:CGRectMake(10,lblValueKdPesan.frame.origin.y+lblValueKdPesan.frame.size.height+5, vwBox.frame.size.width/2, 20)];
    [lblRedeem setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [lblRedeem setTextColor:[UIColor grayColor]];
    
    UILabel *lblValueRedeem = [[UILabel alloc]initWithFrame:CGRectMake(10,lblRedeem.frame.origin.y+lblRedeem.frame.size.height+5, vwBox.frame.size.width/2, 20)];
    [lblValueRedeem setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
    [lblValueRedeem setTextColor:[UIColor blackColor]];
    
    UILabel *lblValueNmPesan = [[UILabel alloc]initWithFrame:CGRectMake(lblNmPesan.frame.origin.x,lblValueKdPesan.frame.origin.y, vwBox.frame.size.width/2, 20)];
    [lblValueNmPesan setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
    [lblValueNmPesan setTextColor:[UIColor blackColor]];
    
    lblRedeem.text = @"Available Redemption";
    lblValueKdPesan.text = [selectedMenu valueForKey:@"kode_pemesanan"];
    vNominal = [[selectedMenu valueForKey:@"nominal_pemesanan"] doubleValue];
    lblValueNmPesan.text = [Utility currencyVal:vNominal];
    lblValueRedeem.text = [Utility currencyVal:[[selectedMenu valueForKey:@"maks_redeem"] doubleValue]];
    vAvaRedeem = [[selectedMenu valueForKey:@"maks_redeem"]doubleValue];
    vMultiple = [[selectedMenu valueForKey:@"multipleRedeem"]doubleValue];
    vMinRedeem = [[selectedMenu valueForKey:@"min_redeem"]doubleValue];
    
    nominal = [[UITextField alloc] initWithFrame:CGRectMake(10, vwBox.frame.origin.y+vwBox.frame.size.height+ 20, widthContiner - 20, 20)];
    [nominal setTextColor:[UIColor blackColor]];
    nominal.inputAccessoryView = toolbar;
    UIView *viewLn2 = [[UIView alloc]initWithFrame:CGRectMake(10, nominal.frame.origin.y+ nominal.frame.size.height, widthContiner-20, 1)];
       viewLn2.backgroundColor = [UIColor grayColor];
   
    CustomBtn *btnBatal = [[CustomBtn alloc]initWithFrame:CGRectMake(10, viewLn2.frame.origin.y+15, (widthContiner/2)-10, 40)];
    [btnBatal setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [btnBatal addTarget:self action:@selector(actionBatal) forControlEvents:UIControlEventTouchUpInside];

    CustomBtn *btnLanjut = [[CustomBtn alloc]initWithFrame:CGRectMake((widthContiner/2)+5,btnBatal.frame.origin.y, (widthContiner/2)-10, 40)];
   [btnLanjut setTitle:lang(@"NEXT") forState:UIControlStateNormal];
   [btnLanjut addTarget:self action:@selector(actionLanjut) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [lblJudul setText:@"Early Redemption"];
        titleToolbar = @"selesai";
        self.lblSeries.text = @"Seri :";
        lblKdPesan.text = @"Kode Pemesanan";
        lblNmPesan.text = @"Nominal Pemesanan";
        nominal.placeholder = @"Jumlah Nominal";
    } else {
        [btnLanjut setTitle:@"NEXT" forState:UIControlStateNormal];
        [btnBatal setTitle:@"CANCEL" forState:UIControlStateNormal];
        titleToolbar = @"done";
        nominal.placeholder = @"Nominal Amount";
        self.lblSeries.text = @"Series :";
        lblKdPesan.text = @"Order Code";
        lblNmPesan.text = @"Nominal Order";
        nominal.placeholder = @"Nominal Amount";
    }
    
    [btnLanjut setColorSet:PRIMARYCOLORSET];
    [btnBatal setColorSet:SECONDARYCOLORSET];

    [vwBox addSubview:lblKdPesan];
    [vwBox addSubview:lblNmPesan];
    [vwBox addSubview:lblValueKdPesan];
    [vwBox addSubview:lblValueNmPesan];
    [vwBox addSubview:lblRedeem];
    [vwBox addSubview:lblValueRedeem];
    
    [viewContiner addSubview:lblJudul];
    [viewContiner addSubview:nominal];
    [viewContiner addSubview:viewLn];
    [viewContiner addSubview:viewLn2];
    [viewContiner addSubview:btnLanjut];
    [viewContiner addSubview:btnBatal];
    [viewContiner addSubview:vwBox];
    [vwScroll addSubview:viewContiner];
    [viewBg addSubview:viewContiner];
    [vwScroll addSubview:viewBg];
    
    vwScroll.contentSize = CGSizeMake(viewBg.frame.size.width, viewBg.frame.size.height);
    [[UIApplication sharedApplication].keyWindow addSubview:vwScroll];
    nominal.delegate = self;
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [vwScroll setContentSize:CGSizeMake(vwScroll.frame.size.width, vwScroll.frame.size.height + kbSize.height+20)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
    vwScroll.contentInset = contentInsets;
    vwScroll.scrollIndicatorInsets = contentInsets;

}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    vwScroll.contentInset = contentInsets;
    vwScroll.scrollIndicatorInsets = contentInsets;
}

- (void)actionBatal {
//    [viewBg removeFromSuperview];
    [vwScroll removeFromSuperview];
}

- (void)actionLanjut {
//    Connection *conn2 = [[Connection alloc]initWithDelegate:self];
  
//    NSString *url = [NSString stringWithFormat:@"%@,id_series=%@,kode_pemesanan=%@,amount=%@",[self.jsonData valueForKey:@"url_parm"],[dataManager.dataExtra valueForKey:@"id_series"],[selectedMenu valueForKey:@"kode_pemesanan"],nominal.text];
//    [conn2 sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    NSString *conNominal = nominal.text;
    NSString *stringWithoutCurrency = [conNominal stringByReplacingOccurrencesOfString:@"."   withString:@""];
    NSUInteger valNominal = [stringWithoutCurrency doubleValue];
    
//    double compare = valNominal % vMultiple;
    double xValid = fmod(valNominal, vMultiple);
    if (xValid != 0){
//       UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",@"Nominal Kelipatan harus lebih dari Rp. 1.000.000"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//              [alert show];
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Nominal Kelipatan harus lebih dari Rp. 1.000.000" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@ Rp %@",lang(@"NOMINAL_MULTIPLE"), [Utility formatCurrencyValue:vMultiple]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
//    else if(valNominal > vNominal*0.5){
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Redeem tidak boleh lebih dari 50% kepemilikan per kode pemesanan" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
    else if (valNominal == 0){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"NOT_AVAILABLE_REDEEM") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(valNominal < vMinRedeem){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MINIMUM_TRANSACTION_REDEEM") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(valNominal > vAvaRedeem){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MAXIMUM_TRANSACTION_REDEEM") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        [dataManager.dataExtra addEntriesFromDictionary:@{@"jumlah": [NSString stringWithFormat:@"%ld",valNominal]}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"amount": [NSString stringWithFormat:@"%ld",valNominal]}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"kode_pemesanan":[selectedMenu valueForKey:@"kode_pemesanan"]}];
        [super openNextTemplate];
    }

    [vwScroll removeFromSuperview];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
    double newValue = [newText doubleValue];
    
    if ([newText length] == 0 || newValue == 0){
        textField.text = nil;
    }
    else{
        textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
//        [self handlingInputUserText:textField.accessibilityLabel];
        return NO;
    }
    return YES;
}




@end
