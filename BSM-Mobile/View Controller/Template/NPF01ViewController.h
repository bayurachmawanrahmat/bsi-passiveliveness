//
//  PF01ViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

@interface NPF01ViewController : TemplateViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic,retain) NSArray *SAVArray;
@property (nonatomic,retain) NSArray *SAVSTRUCTArray;
@property (nonatomic,retain) NSArray *SECArray;
@property (nonatomic,retain) NSArray *SECSTRUCTArray;
@property (nonatomic,retain) NSArray *DEPArray;
@property (nonatomic,retain) NSArray *DEPSTRUCTArray;
@property (nonatomic,retain) NSArray *LOANArray;
@property (nonatomic,retain) NSArray *LOANSTRUCTArray;
@property (nonatomic,retain) NSArray *CURArray;
@property (nonatomic,retain) NSArray *CURSTRUCTArray;

//adding by alikhsan
@property (nonatomic, retain) NSArray *DATAZISWAF;

@end
