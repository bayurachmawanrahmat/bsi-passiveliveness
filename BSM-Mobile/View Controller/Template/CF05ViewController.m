//
//  CF05ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 26/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CF05ViewController.h"
#import "PFViewController.h"
#import "GNPWPViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface CF05ViewController (){
    NSUserDefaults *userDefaults;
}
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet CustomBtn *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *label3;

@end

@implementation CF05ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefaults = [NSUserDefaults standardUserDefaults];
    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
        [_cancelButton setTitle:@"BATAL" forState:UIControlStateNormal];
        [_nextButton setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
    }else{
        
        [_cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_nextButton setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    [_lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    [Styles setTopConstant:_topConstraint];

    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    [self.nextButton setColorSet:PRIMARYCOLORSET];
    
//    [_label2 setHidden:YES];
    [_label2 setTextColor: [UIColor redColor]];
    [_label3 setTextColor: [UIColor redColor]];
    [_label2 setHidden:YES];
    [_label3 setHidden:YES];
    
    [_label3 setUserInteractionEnabled:TRUE];
    UITapGestureRecognizer *label3Tap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionLabel)];
    [_label3 addGestureRecognizer:label3Tap];

    [_cancelButton addTarget:self action:@selector(actionCancel:) forControlEvents:UIControlEventTouchUpInside];
    [_nextButton addTarget:self action:@selector(actionNext:) forControlEvents:UIControlEventTouchUpInside];
    
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        [self requesting];
    }
}

- (void) requesting{
    NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];

    if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00111"]){
        NSString *urlParam = [NSString stringWithFormat:@"%@,input_type",urlData];
        [conn sendPostParmUrl:urlParam needLoading:true encrypted:true banking:true favorite:nil];
    }else{
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
    
}

- (IBAction)actionCancel:(id)sender{
    [self gotoGold];
}

- (IBAction)actionNext:(id)sender{
    [self openNextTemplate];
}

- (void)getData{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    NSString *request = [NSString stringWithFormat:@"request_type=setting,prefix=dompetemas"];
    [conn sendPostParmUrl:request needLoading:true encrypted:false banking:false favorite:false];
}


- (void) actionLabel{
    [self getData];
}


- (void) didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    NSLog(@"%@", jsonObject);
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"setting"]){
        NSDictionary *data = (NSDictionary * )jsonObject;
         if(data != nil){
             NSArray *arVMap = [[data valueForKey:@"dompetemas.setting.menu.vmap"] componentsSeparatedByString:@"|"];
             NSString *strVersionMap = @"";
             for(NSString *strVmap in arVMap){
                 NSArray *arCVmap = [strVmap componentsSeparatedByString:@"="];
                 if ([[arCVmap objectAtIndex:0] isEqualToString:@VERSION_VALUE]) {
                     strVersionMap =[arCVmap objectAtIndex:1];
                     break;
                 }
             }
             
             if([strVersionMap isEqualToString:@""]){
                 NSArray *arCVmap = [arVMap.lastObject componentsSeparatedByString:@"="];
                 strVersionMap =[arCVmap objectAtIndex:1];
             }
             
             NSString *menuProfile = [data valueForKey:[NSString stringWithFormat:@"dompetemas.setting.menu.%@", strVersionMap]];
             
             NSError *error;
             NSArray *dataArr = [NSJSONSerialization JSONObjectWithData:[menuProfile dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
             
             GNPWPViewController *gnpwp = [self.storyboard instantiateViewControllerWithIdentifier:@"GNPWP"];
             
             for(NSDictionary *data in dataArr){
                 if([[data objectForKey:@"code"]isEqualToString:@"1"]){
                     [gnpwp setCode:data];
                     [self.navigationController pushViewController:gnpwp animated:YES];
                 }
             }

             
         }else{
             NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
             UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
             [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                 [self gotoGold];
             }]];
             [self presentViewController:alertCont animated:YES completion:nil];
         }
    }
    
    if(![requestType isEqualToString:@"check_notif"] && ![requestType isEqualToString:@"setting"]){
        if( [[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"] ){
            
            NSString * response = [jsonObject objectForKey:@"response"];
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
            

            [_label1 setText:[dict valueForKey:@"msg"]];
            
            if([[dataManager.dataExtra objectForKey:@"amount"]doubleValue] > [[[userDefaults objectForKey:@"gold_info"]objectForKey:@"NominalJualKenaPajak"]doubleValue] &&
               [[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00112"]){
                
                if(![[[userDefaults objectForKey:@"gold_info"]objectForKey:@"verifiedNPWP"] isEqualToString:@"Y"]){
                    [_label2 setText:[dict valueForKey:@"footer_msg_unverified_npwp"]];
                    [_label2 setHidden:NO];
                    [_label3 setHidden:NO];
                    [_label3 setText:[dict valueForKey:@"footer_msg_action"]];
                }else{
                    [_label2 setText:[dict valueForKey:@"footer_msg_verified_npwp"]];
                    [_label2 setHidden:NO];
                    [_label3 setHidden:YES];
                }
                
            }else if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00111"]){
                if(![[[userDefaults objectForKey:@"gold_info"]objectForKey:@"verifiedNPWP"] isEqualToString:@"Y"]){
                    [_label2 setText:[dict valueForKey:@"footer_msg_unverified_npwp"]];
                    [_label2 setHidden:NO];
                    [_label3 setHidden:NO];
                    [_label3 setText:[dict valueForKey:@"footer_msg_action"]];
                }else{
//                    [_label2 setText:[dict valueForKey:@"footer_msg_verified_npwp"]];
                    [_label2 setHidden:YES];
                    [_label3 setHidden:YES];
                }
            }
            
            [dataManager.dataExtra setObject:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
            
        } else {
            
            if ([jsonObject valueForKey:@"response"]){
               if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
                   NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                   
                   
                   UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
                   [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                       [self gotoGold];
                   }]];
                   [self presentViewController:alertCont animated:YES completion:nil];

               } else {
                   NSString * msg = [jsonObject valueForKey:@"response"];
                   UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
                   [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                       [self gotoGold];
                   }]];
                   [self presentViewController:alertCont animated:YES completion:nil];
               }
           } else {
               NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
               
               UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
               [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                   [self gotoGold];
               }]];
               [self presentViewController:alertCont animated:YES completion:nil];
           }
            
        }
        
    }
}

@end
