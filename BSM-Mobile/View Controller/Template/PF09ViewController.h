//
//  PF03ViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 06/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PF09ViewController : UIViewController

-(instancetype)initWithData : (NSArray *) mDataSource;

@end

NS_ASSUME_NONNULL_END
