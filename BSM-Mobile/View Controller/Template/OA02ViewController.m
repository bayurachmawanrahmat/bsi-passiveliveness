//
//  TAS00ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 29/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "OA02ViewController.h"
#import "Styles.h"

@interface OA02ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>{
    int checkState1, checkState2;
    NSUserDefaults *userDefault;
    NSString *language, *urlSyarat, *toolBarTitle;
    UIToolbar *toolbar;
    
    NSArray *listAutosave;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblChooseAutosave;
@property (weak, nonatomic) IBOutlet UITextField *tfChooseAutosave;
@property (weak, nonatomic) IBOutlet UILabel *lblInformation;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIView *vwCheck1;
@property (weak, nonatomic) IBOutlet UIView *vwTerms;
@property (weak, nonatomic) IBOutlet UIView *vwCheck2;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck1;
@property (weak, nonatomic) IBOutlet UILabel *labelCheck1;
@property (weak, nonatomic) IBOutlet UILabel *labelSyarat;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck2;
@property (weak, nonatomic) IBOutlet UILabel *labelCheck2;

@end

@implementation OA02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self.lblTitle setText:[self.jsonData objectForKey:@"title"]];
    [self.iconTitle setImage:[UIImage imageNamed:[userDefault objectForKey:@"imgIcon"]]];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    
    [self setupStyles];
    [self setupLanguage];
    [self hideDescription];
    [self aggrementInteraction];
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:toolBarTitle style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    UIPickerView *pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    pickerView.showsSelectionIndicator = YES;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.tag = 1;
    self.tfChooseAutosave.inputView = pickerView;
    self.tfChooseAutosave.inputAccessoryView = toolbar;
    
    [self.btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [self setupData];
    
    
//    [dataManager.dataExtra setValue:@"6001" forKey:@"code"];
    [dataManager.dataExtra setValue:@"6001" forKey:@"type_autosave"];
    
    [self setRequest];
}

- (void) aggrementInteraction{
    
    [self.vwTerms setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapSyarat =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(openURLTerms)];
    [self.vwTerms addGestureRecognizer:tapSyarat];
    
    UITapGestureRecognizer *tapCheck1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionCheck1)];
    
    [self.vwCheck1 setUserInteractionEnabled:YES];
    [self.vwCheck1 addGestureRecognizer:tapCheck1];
}

- (void) hideDescription{
    [self.lblDesc setHidden:YES];
    [self.vwCheck1 setHidden:YES];
    [self.vwTerms setHidden:YES];
    [self.vwCheck2 setHidden:YES];
}

- (void) showDescription{
    
    [self showWithAnimation:self.lblDesc];
    [self showWithAnimation:self.vwCheck1];
    [self showWithAnimation:self.vwTerms];
    [self showWithAnimation:self.vwCheck2];
//    [self.lblDesc setHidden:NO];
//    [self.vwCheck1 setHidden:NO];
//    [self.vwTerms setHidden:NO];
//    [self.vwCheck2 setHidden:NO];
}

- (void) showWithAnimation : (UIView*) view{
    [UIView transitionWithView:view
      duration:1
       options:UIViewAnimationOptionTransitionCrossDissolve
    animations:^{
                    view.hidden = NO;
                }
    completion:NULL];

}

- (void) setupStyles{
    [Styles setTopConstant:self.topConstant];
    [Styles setStyleTextFieldBorderWithRightImage:self.tfChooseAutosave imageNamed:@"baseline_keyboard_arrow_down_black_24pt" andRounded:NO];
    
}


- (void) setupLanguage{
    if([language isEqualToString:@"id"]){
        [self.lblChooseAutosave setText:@"Jenis Tabungan"];
        [self.lblInformation setText:@"Informasi : "];
        [self.tfChooseAutosave setPlaceholder:@"pilih jenis tabungan"];
        [self.btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        toolBarTitle = @"selesai";
        [self.labelSyarat setText:@"Syarat dan Ketentuan Umum (Wajib di klik)"];
    }else{
        [self.lblInformation setText:@"Information : "];
        [self.lblChooseAutosave setText:@"Type of Savings"];
        [self.tfChooseAutosave setPlaceholder:@"select type of saving"];
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        toolBarTitle = @"done";
        [self.labelSyarat setText:@"Condition dan General Terms (Must be clicked)"];
    }
}

- (void) openURLTerms{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:urlSyarat];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            [self->_labelCheck2 setTextColor:[UIColor blackColor]];
            [self->_vwCheck2 setUserInteractionEnabled:YES];
            UITapGestureRecognizer *tapCheck2 =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(actionCheck2)];
            [self->_vwCheck2 addGestureRecognizer:tapCheck2];
        }
    }];
}

- (void) actionCheck1{
    if(checkState1 == 0){
        checkState1 = 1;
        [_imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
    }else{
        checkState1 = 0;
        [_imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
    }
}

- (void) actionCheck2{
    if(checkState2 == 0){
        checkState2 = 1;
        [_imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
    }else{
        checkState2 = 0;
        [_imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
    }
}

- (void) setRequest{

    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url =[NSString stringWithFormat:@"%@,id,hewan_qurban,target_harga,target_waktu,frekuensi,target_mulai,setoran,dayofweek,dayofmonth,id_account,type_autosave",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"opening_info_qurban"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *response = [jsonObject valueForKey:@"response"];
            
            NSArray *resp = [response componentsSeparatedByString:@"|"];
            
            [self.lblDesc setText:resp[0]];
            [self.labelCheck1 setText:resp[3]];
            urlSyarat = resp[1];
            
        }else{
            NSString * msg = [jsonObject valueForKey:@"response"];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [listAutosave[row] objectForKey:@"title"];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(listAutosave > 0){
//        NSArray *resp = [[listAutosave[row] valueForKey:@"desc"] componentsSeparatedByString:@"|"];
        if([language isEqualToString:@"id"]){
            if([[listAutosave[row] objectForKey:@"monthly_fee"]doubleValue] == 0){
                [self.lblInformation setText:[NSString stringWithFormat: @"Informasi : \nBebas Biaya Bulanan"]];
            }else{
                [self.lblInformation setText:[NSString stringWithFormat: @"Informasi : \nBiaya Administrasi %@ Rp. %@/bulan", [listAutosave[row] objectForKey:@"title"], [listAutosave[row] objectForKey:@"monthly_fee"]]];
            }
            
            [self.labelCheck2 setText:@"Saya telah membaca syarat khusus pembukaan rekening dan setuju"];

        }else{
            if([[listAutosave[row] objectForKey:@"monthly_fee"]doubleValue] == 0){
                [self.lblInformation setText:[NSString stringWithFormat: @"Information : \nFree Monthly Fee"]];
            }else{
                [self.lblInformation setText:[NSString stringWithFormat: @"Information : \nAdministration Fee %@ Rp.%@/month", [listAutosave[row] objectForKey:@"title"], [listAutosave[row] objectForKey:@"monthly_fee"]]];
            }
            
            [self.labelCheck2 setText:@"I have read the specific terms of opening an account and agree"];
        }
//        [self.lblDesc setText:resp[0]];
//        [self.labelCheck1 setText:resp[3]];
//        urlSyarat = resp[1];
        [self.tfChooseAutosave setText:[listAutosave[row] objectForKey:@"title"]];
        
//        [dataManager.dataExtra setValue:[listAutosave[row] objectForKey:@"code"] forKey:@"code"];
        [dataManager.dataExtra setValue:[listAutosave[row] objectForKey:@"code"] forKey:@"type_autosave"];
        
        [self setRequest];
        
        [self showDescription];
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listAutosave.count;
}

- (void) actionCancel{
    [self backToR];
}

- (void) actionNext{
    NSString *message = @"";
    if([self.tfChooseAutosave.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            message = @"Pilih Jenis Tabungan terlebih dulu";
        }else{
            message = @"Please Select Type of Saving";
        }
    }else
    if(checkState1 == 1 && checkState2 == 1){
        [self openNextTemplate];
        
    }else{
        if([language isEqualToString:@"id"]){
            message = @"Anda wajib untuk checklis point diatas";
        }else{
            message = @"You have to check the point above";
        }
    }
    
    if(![message isEqualToString:@""]){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")
                                   message:message
                                   preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                       handler:nil];

        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void) setupData{
    /*
    {"response":"Tabungan Autosave adalah simpanan yang dilengkapi fitru autodebet terjadwal dalam mata uang rupiah berdasarkan prinsip syariah yaitu Wadiah Yad Dhamanah. Untuk membuaka Tabungan Autosave Anda diwajibkan membaca Syarat Khusus Pembukaan Rekening, Klik syarat dan ketentuan berikut.|https://www.mandirisyariah.co.id/layanan-nasabah/Syarat-syarat-Khusus-Deposito||Dengan ini nasabah menyatakan &amp; menyetujui bahwa data diri beserta contoh tanda tangan nasabah yang digunakan untuk pembukaan Tabungan Autosave melalui mandiri syaraiah mobile adalah sesuai dengan data yang saat ini telah berjalan di Bank","transaction_id":"","response_code":"00"}
     6012 - Mabrur
     6010 - Mudharabah
    **/
    NSString *response = @"{\"type_saving_list\":[{\"title\":\"Tabungan Wadiah\",\"code\":\"6001\",\"monthly_fee\":\"0\",\"desc\":\"\"},{\"title\":\"Tabungan Mudharabah\",\"code\":\"6010\",\"monthly_fee\":\"10000\",\"desc\":\"\"}]}";
    
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    listAutosave = [dict objectForKey:@"type_saving_list"];
}
@end
