//
//  PBYIP01ViewController.h
//  BSM-Mobile
//
//  Created by BSM on 8/16/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PBYIP01ViewController : TemplateViewController

@end

typedef NS_ENUM(NSInteger, pickerEnum) {
    pickerNull = 0,
    pickerLoan = 1,
    pickerMaskapai = 2,
    pickerExtraData = 3,
    pickerExtraDataSub = 4
};

NS_ASSUME_NONNULL_END
