//
//  AKSCF01ViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 10/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"



@interface AKSCF01ViewController : TemplateViewController
- (void)setDataLocal:(NSDictionary*)object;
- (void)setFromRecipt:(BOOL)recipt;
@end


