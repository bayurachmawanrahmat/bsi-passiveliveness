//
//  CF06ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 04/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "IT01ViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "GHCell.h"
#import "PopUpLoginViewController.h"

@interface IT01ViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *listData;
    NSUserDefaults *userDefaults;
    NSString *masterName;
    CGFloat widthColumn;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UITableView *vwTable;
@property (weak, nonatomic) IBOutlet CustomBtn *btnOk;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vwTableWidth;

@end

@implementation IT01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(limitTrxBC:) name:@"limitTrx" object:nil];

    userDefaults = [NSUserDefaults standardUserDefaults];
    masterName = @"";

    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        [self.titleLabel setText:@"Informasi Limit"];
    }else{
        [self.titleLabel setText:@"Limit Information"];
    }
    
    
    [self.imgIcon setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    
    [Styles setTopConstant:_topConstant];
    
    [self.vwTable registerNib:[UINib nibWithNibName:@"GHCell" bundle:nil] forCellReuseIdentifier:@"GHCELL"];
    [self.vwTable setDelegate:self];
    [self.vwTable setDataSource:self];
    [self.vwTable setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    widthColumn = 150;
    [self.vwTableWidth setConstant:(widthColumn*5)+16];
    
    [self.btnOk addTarget:self action:@selector(actionOK:) forControlEvents:UIControlEventTouchUpInside];
    

    self.titleLabel.textColor = UIColorFromRGB(const_color_title);
    self.titleLabel.textAlignment = const_textalignment_title;
    self.titleLabel.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleLabel.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.imgIcon setHidden:YES];
    [self.btnOk setColorSet:PRIMARYCOLORSET];
    
    [self setupRequest];
}

- (void)limitTrxBC:(NSNotification *) notification{
    if([notification.name isEqualToString:@"limitTrx"])
    {
        NSDictionary *userInfo = notification.userInfo;
        
        if([[userInfo valueForKey:@"logged_in"] isEqualToString:@"YES"]){
            [self setupRequest];
        } else {
            [self.navigationController popToRootViewControllerAnimated:true];
        }
        
    }
    
}

- (void)setupRequest{
//    if([userDefaults objectForKey:@"customer_id"]){
    if([NSUserdefaultsAes getValueForKey:@"customer_id"]){
        NSString *mustLogin = [userDefaults objectForKey:@"mustLogin"];
        NSString *hasLogin = [userDefaults objectForKey:@"hasLogin"];
        
        if ([mustLogin isEqualToString:@"YES"]) {
            if ([hasLogin isEqualToString:@"NO"]) {
                [self.tabBarController setSelectedIndex:0];
                PopUpLoginViewController *popLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupLogin"];
                [popLogin setMFeat:@"limitTrx"];
                [self presentViewController:popLogin animated:YES completion:nil];
            }else{
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                NSString *urlParam = @"request_type=limit_info,menu_id,device,device_type,ip_address,language,date_local,id_account,pin,customer_id";
                [conn sendPostParmUrl:urlParam needLoading:true encrypted:true banking:true favorite:nil];
            }
        }
        else{
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            NSString *urlParam = @"request_type=limit_info,menu_id,device,device_type,ip_address,language,date_local,id_account,pin,customer_id";
            [conn sendPostParmUrl:urlParam needLoading:true encrypted:true banking:true favorite:nil];
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        
        [self presentViewController:alert animated:YES completion:^{
            NSDictionary* userInfo = @{@"position": @(513)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        }];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"zpk"]){
            [super didFinishLoadData:jsonObject withRequestType:requestType];
            return;
        }
    if([requestType isEqualToString:@"limit_info"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
//            id response = [jsonObject objectForKey:@"response"];
            NSString *stringResponse;
            stringResponse = [jsonObject valueForKey:@"response"];
            NSLog(@"%@",[jsonObject valueForKey:@"response"]);
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[stringResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            masterName = [dict objectForKey:@"limitmastername"];
            listData = [dict objectForKey:@"limit"];
            [self reSortList];
            [self.vwTable reloadData];
            
            if([jsonObject objectForKey:@"transaction_id"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"] }];
            }
                
        }else{
           UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        }
    }
        
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    CGFloat heightLabel = 40.0;
    
    UIView* parentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, (5*widthColumn)+17, heightLabel+30)];
    [parentView setBackgroundColor:[UIColor whiteColor]];
    UILabel *labelMasterName = [[UILabel alloc]initWithFrame:CGRectMake(8, 0, parentView.frame.size.width, 30)];
    [labelMasterName setFont:[UIFont fontWithName:const_font_name3 size:17.0]];

    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, 30, parentView.frame.size.width, heightLabel)];
    [view setBackgroundColor:UIColorFromRGB(const_color_primary)];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, widthColumn+8, heightLabel)];
    [label1 setFont:[UIFont fontWithName:const_font_name1 size:14.0]];

    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(label1.frame.origin.x + label1.frame.size.width, 0, widthColumn, heightLabel)];
    [label2 setFont:[UIFont fontWithName:const_font_name1 size:14.0]];

    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(label2.frame.origin.x + label2.frame.size.width, 0, widthColumn, heightLabel)];
    [label3 setFont:[UIFont fontWithName:const_font_name1 size:14.0]];

    UILabel *label4 = [[UILabel alloc]initWithFrame:CGRectMake(label3.frame.origin.x + label3.frame.size.width, 0, widthColumn, heightLabel)];
    [label4 setFont:[UIFont fontWithName:const_font_name1 size:14.0]];

    UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(label4.frame.origin.x + label4.frame.size.width, 0, widthColumn+8, heightLabel)];
    [label5 setFont:[UIFont fontWithName:const_font_name1 size:14.0]];

    
    [label1 setTextColor:[UIColor whiteColor]];
    [label1 setTextAlignment:NSTextAlignmentCenter];
    [label1.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [label1.layer setBorderWidth:.5];
    [label1 setNumberOfLines:0];
    
    [label2 setTextColor:[UIColor whiteColor]];
    [label2 setTextAlignment:NSTextAlignmentCenter];
    [label2.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [label2.layer setBorderWidth:.5];
    [label2 setNumberOfLines:0];


    [label3 setTextColor:[UIColor whiteColor]];
    [label3 setTextAlignment:NSTextAlignmentCenter];
    [label3.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [label3.layer setBorderWidth:.5];
    [label3 setNumberOfLines:0];

    [label4 setTextColor:[UIColor whiteColor]];
    [label4 setTextAlignment:NSTextAlignmentCenter];
    [label4.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [label4.layer setBorderWidth:.5];
    [label4 setNumberOfLines:0];

    [label5 setTextColor:[UIColor whiteColor]];
    [label5 setTextAlignment:NSTextAlignmentCenter];
    [label5 setAdjustsFontSizeToFitWidth:YES];
    [label5.layer setBorderColor:[[UIColor whiteColor]CGColor]];
    [label5.layer setBorderWidth:.5];
    [label5 setNumberOfLines:0];

    
    if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
        [labelMasterName setText:[NSString stringWithFormat:@"Jenis Limit : %@", masterName]];
        [label1 setText:@"Jenis Transaksi"];
        [label2 setText:@"Limit Transaksi"];
        [label3 setText:@"Limit Harian"];
        [label4 setText:@"Limit Harian yang digunakan"];
        [label5 setText:@"Sisa Limit Harian"];
    }else{
        [labelMasterName setText:[NSString stringWithFormat:@"Limit Type : %@", masterName]];
        [label1 setText:@"Transaction Type"];
        [label2 setText:@"Transaction Limit"];
        [label3 setText:@"Daily Limit"];
        [label4 setText:@"Used Daily Limit"];
        [label5 setText:@"Remaining Daily Limit"];
    }
    
    
    [view addSubview:label1];
    [view addSubview:label2];
    [view addSubview:label3];
    [view addSubview:label4];
    [view addSubview:label5];

    [parentView addSubview:labelMasterName];
    [parentView addSubview:view];
    
    return parentView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 70.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GHCell *cell = (GHCell*)[tableView dequeueReusableCellWithIdentifier:@"GHCELL"];
    
    
    [cell.col1 setText:[listData[indexPath.row] objectForKey:@"trxtype"]];
    [cell.col1 setTextAlignment:NSTextAlignmentLeft];
    
    [cell.Col2 setText:[NSString stringWithFormat:@"Rp. %@",[listData[indexPath.row] objectForKey:@"trxlimit"]]];
    [cell.Col2 setTextAlignment:NSTextAlignmentRight];

    [cell.col3 setText:[NSString stringWithFormat:@"Rp. %@",[listData[indexPath.row] objectForKey:@"dailylimit"]]];
    [cell.col3 setTextAlignment:NSTextAlignmentRight];
    
    [cell.col4 setText:[NSString stringWithFormat:@"Rp. %@",[listData[indexPath.row] objectForKey:@"limitspent"]]];
    [cell.col4 setTextAlignment:NSTextAlignmentRight];
    
    [cell.col5 setText:[NSString stringWithFormat:@"Rp. %@",[listData[indexPath.row] objectForKey:@"limitbal"]]];
    [cell.col5 setTextAlignment:NSTextAlignmentRight];
    
    return cell;
    
}

- (IBAction)actionOK:(id)sender{
    [super backToRoot];
}

- (void) reSortList{
    NSMutableArray *arr = [[NSMutableArray alloc]init];;
    for(NSDictionary *d in listData){
        [arr addObject:d];
    }
    if([userDefaults objectForKey:@"config.trxtype.order"]){
        NSArray *order = [[userDefaults valueForKey:@"config.trxtype.order"] componentsSeparatedByString:@","];
        for(NSString *orderNumber in order){
            for(NSDictionary *dict in listData){
                if([[dict objectForKey:@"id"] isEqual:orderNumber]){
                    [arr removeObjectAtIndex:[orderNumber intValue]];
                    [arr insertObject:dict atIndex:[orderNumber intValue]];
                }
            }
        }
    }
    listData = arr;
}

@end
