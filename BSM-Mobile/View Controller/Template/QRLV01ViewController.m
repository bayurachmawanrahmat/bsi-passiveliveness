//
//  QRLV01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 1/23/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "QRLV01ViewController.h"
#import "Utility.h"
#import "Connection.h"

@interface QRLV01ViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate >{
    NSArray *listAction;
    NSArray *listOrigin;
    NSDictionary *dataObject;
    NSDictionary *dataLocal;
    BOOL first;
    BOOL fromHome;
    BOOL spesial;
    BOOL mustAddToManager;
    NSString *key;
    UIToolbar *toolBar;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;

@property (weak, nonatomic) IBOutlet UIImageView *iconHeaderLV01;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgScanQR;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightImage; // 15
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSeacrh; // 30
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightLine; // 1
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightView; // 40
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomTable;

@property (weak, nonatomic) IBOutlet UIImageView *imgQR;
@property (weak, nonatomic) IBOutlet UIButton *btnQR;

- (IBAction)showQR:(id)sender;
@end

@implementation QRLV01ViewController

NSString* requestTypeQR;
UIView *viewBgQR;
UIView *viewContinerQR;
UIImageView *imgShowQR;
extern NSString *lv_selected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)setFirstLV:(BOOL)aFirstLV{
    first = aFirstLV;
}

- (void)setFromHome:(BOOL)afromHome{
    fromHome = afromHome;
}

- (void)setSpecial:(BOOL)aspecial{
    spesial = aspecial;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imgScanQR.hidden = true;
    self.imgQR.hidden = true;
    self.btnQR.hidden = true;
    
    [self setDisplay];
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *dt = [userDefault objectForKey:@"indexMenu"];
    if (dt) {
        [[DataManager sharedManager]resetObjectData];
        NSArray *temp = [[DataManager sharedManager] getJSONData:[dt intValue]];
        NSDictionary *object = [temp objectAtIndex:1];
        [self setJsonData:object];
        [userDefault removeObjectForKey:@"indexMenu"];
    }
    self.tableView.tableFooterView = [[UIView alloc] init];
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    _iconHeaderLV01.image = [UIImage imageNamed:img];
    _iconHeaderLV01.hidden = YES;
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestTypeQR = [dataParam valueForKey:@"request_type"];
    if(first){
        listAction = [self.jsonData objectForKey:@"action"];
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        [self.tableView reloadData];
    } else if (fromHome){
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        [dataManager setAction:[self.jsonData valueForKey:@"action"] andMenuId:@"00007"];
        [super openNextTemplate];
    } else if (spesial){
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        int ind = 0;
        NSArray *menu = [self.jsonData valueForKey:@"action"];
        for (int i =0; i < [menu count]; i++) {
            NSArray *selMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:i];
            if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
                ind = i;
                break;
            }
        }
        NSArray *selectedMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:ind];
        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
        [super openNextTemplate];
    } else {
        dataObject = [dataManager getObjectData];
        [self.labelTitle setText:[dataObject valueForKey:@"title"]];
        if([[dataObject valueForKey:@"url"]boolValue]){
            if([requestTypeQR isEqualToString:@"list_merchant"] || [requestTypeQR isEqualToString:@"list_bank"] || [requestTypeQR isEqualToString:@"list_account"] || [requestTypeQR isEqualToString:@"list_denom"] || [requestTypeQR isEqualToString:@"list_denom2"] || [requestTypeQR isEqualToString:@"list_nazhir"]){
                
                if([requestTypeQR isEqualToString:@"list_merchant"] ){
                    NSString *id_merchant = @"";
                    if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00013"]){// TELEPON
                        id_merchant = @"1";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00025"]) { // VOUCHER HP
                        id_merchant = @"2";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00020"]) { // RETAILER
                        id_merchant = @"3";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00016"]) { // ANGSURAN
                        id_merchant = @"4";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00017"]) { // PEMBAYARAN TIKET
                        id_merchant = @"5";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00015"]) { // TV KABEL
                        id_merchant = @"6";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00014"]) { // ACADEMIC
                        id_merchant = @"7";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00018"]) { // ASURANSI
                        id_merchant = @"8";
                    }
                    
                }
                
                if ([requestTypeQR isEqualToString:@"list_denom2"]) {
                    NSString *url =[NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }
                
                if([requestTypeQR isEqualToString:@"list_account"]){
                    [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                }
                
                else {
                    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }
            }
        } else{
            id data =  [dataObject objectForKey:@"content"];
            if([data isKindOfClass:[NSString class]]){
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }else{
                mustAddToManager = true;
                listAction = data;
                [self.tableView reloadData];
            }
        }
    }
    
}

- (void)setQRButton {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *arrAccount = [userDefault objectForKey:@"arrAccount"];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    for(int i=0;i<([arrAccount count]);i++){
        UIButton *myButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        if (i==0) {
            myButton.frame = CGRectMake(screenWidth-44, 134+(i*44),44,44);
        } else {
            myButton.frame = CGRectMake(screenWidth-44, 144+(i*44),44,44);
        }
        myButton.tag = i;
        myButton.backgroundColor = [UIColor whiteColor];
        [myButton setTitle:@"" forState:UIControlStateNormal];
        [myButton addTarget:self action:@selector(myaction:)
           forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:myButton];

        int range;
        if (i==0) {
            range = 0;
        } else {
            range = 10;
        }
        
        UIImageView *myImage = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth-44, 134+(i*44)+range, 44, 44)];
        [myImage setImage:[UIImage imageNamed:@"qr_code"]];
        [self.view addSubview:myImage];
    }
}

-(void)myaction:(UIButton *)sender{
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    //NSArray *arrAccount = [userDefault objectForKey:@"arrAccount"];
    
    //for(int i=0;i<([arrAccount count]);i++){
    [self loadQR:sender.tag];
    //}
}

-(void)loadQR:(NSUInteger)idx {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *arrAccount = [userDefault objectForKey:@"arrAccount"];
    NSDictionary *dictAccount = [arrAccount objectAtIndex:idx];
    NSString *norek = [dictAccount objectForKey:@"id_account"];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat sizeQR = 0.0;
    
    if (width <= height) {
        sizeQR = 0.75 * width;
    } else {
        sizeQR = 0.75 * height;
    }
    
    viewBgQR = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    viewBgQR.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    viewContinerQR = [[UIView alloc]initWithFrame:CGRectMake(width/2 - sizeQR/2, height/2 - (sizeQR+40)/2, sizeQR, (sizeQR+40))];
    viewContinerQR.backgroundColor = [UIColor whiteColor];
    
    imgShowQR = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, sizeQR, sizeQR)];
    
    [self generateQRImage:norek];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, imgShowQR.frame.size.height, sizeQR, 40)];
    [btn setTitle:@"Ok" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(actionOke) forControlEvents:UIControlEventTouchUpInside];
    
    [viewContinerQR addSubview:btn];
    [viewContinerQR addSubview:imgShowQR];
    [viewBgQR addSubview:viewContinerQR];
    [[UIApplication sharedApplication].keyWindow addSubview:viewBgQR];
}

/*- (IBAction)showQR:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *norek = [userDefault objectForKey:@"valNoRek"];
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    viewBgQR = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    viewBgQR.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    viewContinerQR = [[UIView alloc]initWithFrame:CGRectMake(width/2 - 150/2, height/2 - 190/2, 150,190)];
    viewContinerQR.backgroundColor = [UIColor whiteColor];
    
    imgShowQR = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    
    [self generateQRImage:norek];

    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, imgShowQR.frame.size.height, 150, 40)];
    [btn setTitle:@"Ok" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(actionOke) forControlEvents:UIControlEventTouchUpInside];
    
    [viewContinerQR addSubview:btn];
    [viewContinerQR addSubview:imgShowQR];
    [viewBgQR addSubview:viewContinerQR];
    [[UIApplication sharedApplication].keyWindow addSubview:viewBgQR];
    
    if (self.imgScanQR.isHidden == true) {
        self.imgScanQR.hidden = false;
    } else {
        self.imgScanQR.hidden = true;
    }
}*/

- (void)actionOke {
    [viewBgQR removeFromSuperview];
}

- (void)setDisplay {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmTable = self.tableView.frame;
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.labelTitle.frame;
    CGRect frmImgQR = self.imgQR.frame;
    CGRect frmBtnQR = self.btnQR.frame;
    CGRect frmImgScanQR = self.imgScanQR.frame;
    
    frmTitle.origin.x = 0;
    frmTitle.origin.y = TOP_NAV;
    frmTitle.size.height = 50;
    frmTitle.size.width = screenWidth;
    
    frmTable.origin.x = 0;
    frmTable.origin.y = frmTitle.origin.y + frmTitle.size.height;
    frmTable.size.width = screenWidth;
    frmTable.size.height = screenHeight - frmTable.origin.y - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveNotch]) {
        frmTable.size.height = screenHeight - frmTable.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    frmLblTitle.size.height = 40;
//    frmTableView.size.height = screenHeight - (TOP_NAV + frmTitle.size.height + BOTTOM_NAV);
    //frmImgQR.origin.x = screenWidth - frmImgQR.size.width;
    //frmBtnQR.origin.x = screenWidth - frmBtnQR.size.width;
    frmImgScanQR.origin.x = screenWidth/2 - frmImgScanQR.size.width/2;
    frmImgScanQR.origin.y = screenHeight/2 - frmImgScanQR.size.height/2;
    
    self.tableView.frame = frmTable;
    self.vwTitle.frame = frmTitle;
    self.labelTitle.frame = frmLblTitle;
    self.imgQR.frame = frmImgQR;
    self.btnQR.frame = frmBtnQR;
    self.imgScanQR.frame = frmImgScanQR;
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
}

- (void)generateQRImage:(NSString *)encStr {
    NSData *stringData = [encStr dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = imgShowQR.frame.size.width / qrImage.extent.size.width;
    float scaleY = imgShowQR.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    imgShowQR.image = [UIImage imageWithCIImage:qrImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
}

- (void)doneAction:(id)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAction.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV01Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];

    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        [label setText:[data valueForKey:@"title"]];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            [label setText:[data valueForKey:@"title"]];
            
        }else if([data objectForKey:@"name"]){
            [label setText:[data valueForKey:@"name"]];
            
        }
    }

    if (indexPath.row == 0) {
        //[userDefault removeObjectForKey:@"valNoRek"];
        [userDefault setObject:label.text forKey:@"valNoRek"];
        [userDefault synchronize];
    }
    
    CGRect frmLbl = label.frame;
   // CGRect frmVwLine = vwLine.frame;
    
    frmLbl.size.width = cell.frame.size.width - (frmLbl.origin.x *2);
    //frmVwLine.size.width = cell.frame.size.width - (frmVwLine.origin.x * 2);
    
    label.frame = frmLbl;
    //vwLine.frame = frmVwLine;
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = @"";
    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        text = [data valueForKey:@"title"];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            text = [data valueForKey:@"title"];
            
        }else if([data objectForKey:@"name"]){
            text = [data valueForKey:@"name"];
        }
    }
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+20.0;
}
#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
    if(first){
        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
    }
    if(mustAddToManager){
        //        NSString *keyTemp = @"";
        NSString *value = @"";
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        BOOL haveAction = false;
        for(NSString *a in data.allKeys){
            
            if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00028"] && [a isEqualToString:@"type_transfer"]) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setObject:[data valueForKey:a] forKey:@"typeTransfer"];
            }
            
            if([a isEqualToString:@"action"]){
                haveAction = true;
            }else if(![a isEqualToString:@"title"] && ![a isEqualToString:@"name"]){
                key = a;
                value = [data valueForKey:key];
                [userDefault setObject:value forKey:@"valNoRek"];
                [userDefault synchronize];
                break;
            }
            
        }
        
        lv_selected = value;
        [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
        if(haveAction){
            [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
            dataManager.currentPosition=-1;
        }
    }
    
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *last = [userDefault objectForKey:@"last"];
    if (![last isEqualToString:@"-"]) {
        [userDefault setValue:@"-" forKey:@"last"];
    }
    
    [super openNextTemplate];
    
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
//    if([requestType isEqualToString:@"list_account1"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
    }
    
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
                if ([requestType isEqualToString:@"list_denom2"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"response"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listData){
                        [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                    }
                    key = @"id_denom";
                    mustAddToManager = true;
                    listAction = newData;
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
                    [self.tableView reloadData];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else {
                if(![requestType isEqualToString:@"list_account2"]){
//                if(![requestType isEqualToString:@"list_account1"]){
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    
                }
            }
        }else{
            if([requestType isEqualToString:@"list_account"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_account":[temp valueForKey:@"id_account"]}];
                }
                //mustAddToManager = true;
                //key = @"id_account";
                //listAction = newData;
                
                if ([newData count] == 1) {
                    NSDictionary *idAccount = [newData objectAtIndex:0];
                    lv_selected = [idAccount valueForKey:@"id_account"];
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":[idAccount valueForKey:@"id_account"]}];
                    [super openNextTemplate];
                }else{
                    mustAddToManager = true;
                    key = @"id_account";
                    listAction = newData;
                    [self.tableView reloadData];
                }
                
                [userDefault setObject:newData forKey:@"arrAccount"];
                [userDefault synchronize];
                
                //[self.tableView reloadData];
                
                //[self setQRButton];
            } else if([requestType isEqualToString:@"list_merchant"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
                }
                if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00028"]){
                    key = @"id_merchant";
                }else if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00025"]){
                    key = @"id_denom";
                }else{
                    key = @"code";
                }
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_bank"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
                }
                key = @"code";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_nazhir"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"name":[temp valueForKey:@"name"],@"nazhir_id":[temp valueForKey:@"nazhir_id"]}];
                }
                key = @"nazhir_id";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_denom"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                }
                key = @"id_denom";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_denom2"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                }
                key = @"id_denom";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 657) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == 657) {
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        
        if ([substring length] > 0) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for (NSDictionary *temp in listOrigin){
                NSRange substringRange = [[[temp objectForKey:@"title"] lowercaseString] rangeOfString:[substring lowercaseString]];
                if (substringRange.length != 0) {
                    [newData addObject:temp];
                }
            }
            
            listAction = (NSArray *)newData;
            [self.tableView reloadData];
        } else {
            listAction = listOrigin;
            [self.tableView reloadData];
        }
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        self.constraintBottomTable.constant = 216;
        [self.view layoutIfNeeded];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        self.constraintBottomTable.constant = 0;
        [self.view layoutIfNeeded];
    }
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

#pragma mark getListAccountOneHit

-(void) getListAccountFromPersitance : (NSString *) urlParam
                           strMenuId : (NSString *) menuId{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance =[[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct =nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
            if ([dataManager.dataExtra objectForKey:@"code"]) {
                if ([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
               
        }
    }
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];

            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct != nil) {
        if(listAcct.count != 0){
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in listAcct){
//                [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_account":[temp valueForKey:@"id_account"]}];
                
                NSString *titl = @"";
                if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
                    if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                        titl = [NSString stringWithFormat:@"%@ - %@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"], [temp valueForKey:@"altname"]];
                    }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                        titl = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
                    }else{
                        titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
                    }
                }
                else{
                    titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
                }
//                NSString *titl = [NSString stringWithFormat:@"%@ - %@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"], [temp valueForKey:@"altname"]];
                [newData addObject:@{@"title":titl,@"id_account":[temp valueForKey:@"id_account"]}];
            }
            
            if ([newData count] == 1) {
                NSDictionary *idAccount = [newData objectAtIndex:0];
                lv_selected = [idAccount valueForKey:@"id_account"];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":[idAccount valueForKey:@"id_account"]}];
                [super openNextTemplate];
            }else{
                mustAddToManager = true;
                key = @"id_account";
                listAction = newData;
                [self.tableView reloadData];
            }
            [userDefault setObject:newData forKey:@"arrAccount"];
            [userDefault synchronize];
        }else{
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki nomor rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account number for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                       message:mes
                                       preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                [self backToR];
            }];

            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
        
    }else{
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
//        Connection *conn = [[Connection alloc]initWithDelegate:self];
//        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
//        NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account1,customer_id"];
        NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
    }
    
    
    
}

@end
