//
//  CF02ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "CF02ViewController.h"
#import "Connection.h"
#import "Utility.h"

@interface CF02ViewController ()<ConnectionDelegate, UIAlertViewDelegate>
- (IBAction)ok:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonOk;
@property (weak, nonatomic) IBOutlet UIButton *buttonFav;
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;
@property (weak, nonatomic) IBOutlet UITextView *textViewConfirmation;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation CF02ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    [Styles setTopConstant:_topConstraint];

    //update to amanah styles
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.automaticallyAdjustsScrollViewInsets = false;
//    [self.textViewConfirmation setFont:[UIFont systemFontOfSize:17.0]];
    [self.textViewConfirmation setFont:[UIFont fontWithName:const_font_name1 size:15]];
    [self setupLayout];
    if(self.jsonData){
        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
        if(fav){
//            [self.buttonOk setHidden:true];
            [self.buttonFav setHidden:false];
            [self.buttonShare setHidden:false];
        }
        
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if(needRequest){
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:fav];
        }
//        else if([self.jsonData objectForKey:@"content"]){
//            [self.labelConfirmation setText:[self.jsonData valueForKey:@"content"]];
//            [self.labelConfirmation sizeToFit];
//
//        }
    }    // Do any additional setup after loading the view.
//    [self.labelConfirmation addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [self.labelConfirmation removeObserver:self forKeyPath:@"contentSize"];
}

- (void)viewDidDisappear:(BOOL)animated{
//
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmTextConfirm  = self.textViewConfirmation.frame;
    CGRect frmBtnOk  = self.buttonOk.frame;
    CGRect frmImgIcon = self.imgIcon.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmBtnOk.size.height = 42;
    frmBtnOk.origin.x = 16;
    frmBtnOk.size.width = SCREEN_WIDTH - (frmBtnOk.origin.x *2);
    frmBtnOk.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtnOk.size.height - 16;
    
    if ([Utility isDeviceHaveNotch]) {
        frmBtnOk.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtnOk.size.height - 40;
    }
    
    frmTextConfirm.origin.x = 16;
    frmTextConfirm.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    frmTextConfirm.size.height = frmBtnOk.origin.y - frmTextConfirm.origin.y;
    frmTextConfirm.size.width = SCREEN_WIDTH - (frmTextConfirm.origin.x*2);

    frmLblTitle.origin.x = frmImgIcon.origin.x + frmImgIcon.size.width + 8;
    frmLblTitle.origin.y = frmVwTitle.size.height/2 - frmLblTitle.size.height/2;
    frmLblTitle.size.width =  frmVwTitle.size.width - frmLblTitle.origin.x - 16;
    
    self.vwTitle.frame = frmVwTitle;
    self.textViewConfirmation.frame = frmTextConfirm;
    self.buttonOk.frame = frmBtnOk;
    self.lblTitle.frame = frmLblTitle;
    
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        if([requestType isEqualToString:@"insert_favorite"]){
            [super didFinishLoadData:jsonObject withRequestType:requestType];
        }else{
            if (![requestType isEqualToString:@"check_notif"]) {
                id response = [jsonObject objectForKey:@"response"];
                NSString *stringResponse;
                if([response isKindOfClass:[NSArray class]]){
                    NSArray *responseArray = response;
                    stringResponse = [response objectAtIndex:0];
                    if(responseArray.count > 1){
                        if([[response objectAtIndex:1]isKindOfClass:[NSArray class]]){
                            NSArray *temp = [Utility changeToArray:[response objectAtIndex:1]];
                            stringResponse = [NSString stringWithFormat:@"%@\n%@",stringResponse,[temp  componentsJoinedByString:@"\n"]];
                        }else{
                            stringResponse = [NSString stringWithFormat:@"%@\n%@",stringResponse,[response objectAtIndex:1]];
                        }
                    }
                }else{
                    stringResponse = [jsonObject valueForKey:@"response"];
                    NSLog(@"%@",[jsonObject valueForKey:@"response"]);
                }
                
                if([jsonObject objectForKey:@"transaction_id"]){
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"] }];
                }
                if([jsonObject objectForKey:@"share"]){
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"share":[jsonObject valueForKey:@"share"] }];
                }
                [self.textViewConfirmation setText:stringResponse];
                [self.textViewConfirmation setSelectable:YES];
                [self.textViewConfirmation setDataDetectorTypes:UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber];
                [self setupLayout];
            }
            
        }
//        [self.labelConfirmation sizeToFit];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
//        [self.labelConfirmation setText:];
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
- (IBAction)makeFavorite:(id)sender {
//    data addObject:@{@"name":[self.jsonData valueForKey:@"title"]}
}

- (IBAction)ok:(id)sender {
    [super backToRoot];
}
- (IBAction)share:(id)sender {
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}
@end
