//
//  RCPTViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/21/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "RCPTViewController.h"

@interface RCPTViewController ()<ConnectionDelegate, UIAlertViewDelegate> {
    NSDictionary *dataObject;
    BOOL fromRecipt;
    BOOL frmTrx;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTittle;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOkShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIView *viewSS;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UIImageView *imgRecipt;
@property (strong, nonatomic) IBOutlet UIView *bgView;

@end

@implementation RCPTViewController

CGRect scrBoundsRCPT;
CGSize scrSizesRCPT;
CGFloat scrWidthsRCPT;
CGFloat scrHeightsRCPT;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _lblHeader.text = @"";
    _lblContent.text = @"";
    _lblFooter.text = @"";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [_btnShare setTitle:@"Bagikan" forState:UIControlStateNormal];
    } else {
        [_btnShare setTitle:@"Share" forState:UIControlStateNormal];
    }
    
    if (fromRecipt) {
        [_btnMore setHidden:YES];
        [_btnOk setHidden:YES];
        [_btnShare setHidden:YES];
        [_btnOkShare setHidden:YES];
    } else {
        [_btnMore setHidden:YES];
        [_btnOk setHidden:NO];
        [_btnShare setHidden:YES];
        [_btnOkShare setHidden:YES];
    }
    
    scrBoundsRCPT = [[UIScreen mainScreen] bounds];
    scrSizesRCPT = scrBoundsRCPT.size;
    scrWidthsRCPT = scrSizesRCPT.width;
    scrHeightsRCPT = scrSizesRCPT.height;
    
    [self.lblTittle setText:[self.jsonData valueForKey:@"title"]];
    
    if(self.jsonData){
        frmTrx = true;
        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
        
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if(needRequest){
            _imgRecipt.hidden = YES;
            if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00020"]) {
                NSString *url =[NSString stringWithFormat:@"%@,amount,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:fav];
            } else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00025"]) {
                NSString *url =[NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:fav];
            } else if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00060"]) {
                NSString *url =[NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:fav];
            } else {
                [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:fav];
            }
        }
    } else if (dataObject) {
        frmTrx = false;
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"id"]){
            [self.lblTittle setText:@"Kotak Masuk"];
        } else {
            [self.lblTittle setText:@"Inbox"];
        }
        NSString *response = [dataObject valueForKey:@"response"];
        NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        
        _lblHeader.text = [dict objectForKey:@"title"];
        _lblContent.text = [dict objectForKey:@"msg"];
        _lblFooter.text = [dict objectForKey:@"footer_msg"];
        
        NSMutableArray *arrlistInbox = [userDefault objectForKey:@"listInbox"];
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        for (id object in arrlistInbox) {
            if (![object isEqual:[dataObject valueForKey:@"transaction_id"]]) {
                [temp addObject:object];
            }
        }
        
        [userDefault setObject:temp forKey:@"listInbox"];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"changeIcon"
         object:self];
    }
}

- (void)setFromRecipt:(BOOL)recipt {
    fromRecipt = recipt;
}

- (IBAction)actionMore:(id)sender {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgCancel = @"";
    NSString *msgFavorite = @"";
    NSString *msgShare = @"";
    if([lang isEqualToString:@"id"]){
        msgCancel = @"Batal";
        msgFavorite = @"Favorit";
        msgShare = @"Bagikan";
    } else {
        msgCancel = @"Cancel";
        msgFavorite = @"Favorite";
        msgShare = @"Share";
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"More" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    if (self.jsonData) {
        if ([[self.jsonData objectForKey:@"favorite"]boolValue]) {
            [actionSheet addAction:[UIAlertAction actionWithTitle:msgFavorite style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self prosesAddFav];
            }]];
        }
    }
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgShare style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self prosesShareImage];
        //        [self dismissViewControllerAnimated:YES completion:^{
        //
        //        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)actionShare:(id)sender {
    [self prosesShareImage];
    
}

- (IBAction)actionOk:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"changeIcon"
     object:self];
    if(frmTrx){
        [super backToRoot];
    }else{
        UINavigationController *navigationController = self.navigationController;
        [navigationController popViewControllerAnimated:YES];
        NSLog(@"Views in hierarchy: %@", [navigationController viewControllers]);
    }
}

- (void)setDataLocal:(NSDictionary*) object{
    dataObject =object;
}

-(void)prosesAddFav {
    [self openTemplate:@"SUCFAV" withData:nil];
    //    Connection *conn = [[Connection alloc]initWithDelegate:self];
    //    conn.fav = true;
    //    [conn sendPost:[[DataManager sharedManager]favData] needLoading:true textLoading:lang(@"ADD_FAV") encrypted:true];
    
    //    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    //    NSString * session = [userDef objectForKey:KEY_SESSION_ID];
    //    [[DataManager sharedManager]setPinNumber:session];
    ////    [DataManager sharedManager]
    //    Connection *conn = [[Connection alloc]initWithDelegate:self];
    //    NSString *parmUrl = [NSString stringWithFormat:@"request_type=create_pin,new_pin=%@,session_id=%@,date_local=%@"];
    //
    //    [conn sendPostParmUrl:parmUrl needLoading:true encrypted:true banking:true];
}


-(void)prosesShareImage{
    UIGraphicsBeginImageContextWithOptions(_viewSS.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_viewSS.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        
    }
    if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]){
        if([requestType isEqualToString:@"insert_favorite"]){
            
            [super didFinishLoadData:jsonObject withRequestType:requestType];
        }else{
            if (![requestType isEqualToString:@"check_notif"]) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *response = [jsonObject valueForKey:@"response"];
                NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                if (dict) {
                    _imgRecipt.hidden = NO;
                    _lblHeader.text = [dict objectForKey:@"title"];
                    _lblContent.text = [dict objectForKey:@"msg"];
                    _lblFooter.text = [dict objectForKey:@"footer_msg"];
                    
                    NSMutableDictionary *dictList = [[NSMutableDictionary alloc] init];
                    [dictList setValue:response forKey:@"response"];
                    [dictList setValue:[jsonObject valueForKey:@"transaction_id"]
                                forKey:@"transaction_id"];
                    [dictList setValue:@"GENCF02" forKey:@"template"];
                    [dictList setValue:jsonObject forKey:@"apiResponse"];
                    [dictList setValue:@"General" forKey:@"jenis"];
                    NSString *img = [userDefault objectForKey:@"imgIcon"];
                    [dictList setValue:img forKey:@"icon"];
                    NSDate *date = [NSDate date];
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                    [dateFormat setDateFormat:@"dd MMM yyyy"];
                    NSString *currentDate = [dateFormat stringFromDate:date];
                    [dictList setValue:currentDate forKey:@"time"];
                    
                    if ([userDefault objectForKey:@"listRecipt"]){
                        NSMutableArray *arr = [[userDefault objectForKey:@"listRecipt"] mutableCopy];
                        [arr addObject:dictList];
                        [userDefault setObject:arr forKey:@"listRecipt"];
                    } else {
                        NSMutableArray *arr = [[NSMutableArray alloc] init];
                        [arr addObject:dictList];
                        [userDefault setObject:arr forKey:@"listRecipt"];
                    }
                    
                    if ([userDefault objectForKey:@"listInbox"]){
                        NSMutableArray *arrTransaction = [[userDefault objectForKey:@"listInbox"] mutableCopy];
                        [arrTransaction addObject:[jsonObject valueForKey:@"transaction_id"]];
                        [userDefault setObject:arrTransaction forKey:@"listInbox"];
                    } else {
                        NSMutableArray *arrTransaction = [[NSMutableArray alloc] init];
                        [arrTransaction addObject:[jsonObject valueForKey:@"transaction_id"]];
                        [userDefault setObject:arrTransaction forKey:@"listInbox"];
                    }
                    
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"changeIcon"
                     object:self];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,response] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            
        }
    }else{
        if ([jsonObject valueForKey:@"response"]){
            if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
//                NSString * msg = [self generateMessage:[jsonObject valueForKey:@"response_code"] :[jsonObject valueForKey:@"response"]];
                NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            } else {
                NSString * msg = [jsonObject valueForKey:@"response"];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        } else {
//            NSString * msg = [self generateMessage:[jsonObject valueForKey:@"response_code"] :[jsonObject valueForKey:@"response"]];
            NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}



//- (NSString *) generateMessage:(NSString *) rc:(NSString *) defaultValue{
//    NSString *msg = defaultValue;
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    if([lang isEqualToString:@"id"]){
//        if ([rc isEqualToString:@"03"]) {
//            msg = @"Maaf, transaksi anda gagal karena kesalahan pada jumlah pembayaran.";
//        } else   if ([rc isEqualToString:@"05"]) {
////            msg = @"Transaksi gagal, kesalahan sistem";
//            msg = @"Transaksi gagal, silahkan ulangi kembali";
//        } else   if ([rc isEqualToString:@"08"]) {
//            msg = @"Transaksi dalam proses. Periksa transaksi anda melalui menu 'Status Tunai'";
//        } else   if ([rc isEqualToString:@"09"]) {
//            msg = @"Transaksi gagal, pembatalan dalam proses.";
//        } else   if ([rc isEqualToString:@"13"]) {
//            msg = @"MAAF, UNTUK SEMENTARA TIDAK DAPAT MENJALANKAN TRANSAKSI YANG DIMINTA. SILAKAN HUBUNGI LAYANAN CALL CENTER.";
//        } else   if ([rc isEqualToString:@"14"]) {
//            msg = @"Maaf, nomor anda tidak ditemukan.";
//        } else   if ([rc isEqualToString:@"15"]) {
//            msg = @"Rekening tidak terdaftar";
//        } else   if ([rc isEqualToString:@"31"]) {
//            msg = @"Silakan hubungi cabang anda untuk menggunakan layanan ini.";
//        } else   if ([rc isEqualToString:@"33"]) {
//            msg = @"Maaf, kartu ATM anda yang terdaftar untuk Syariah Indonesia MBG sudah tidak aktif.";
//        } else   if ([rc isEqualToString:@"38"]) {
//            msg = @"PIN anda diblokir, hubungi Syariah Indonesia Customer Service atau Call Center";
//        } else   if ([rc isEqualToString:@"40"]) {
//            msg = @"Maaf, layanan ini untuk sementara tidak bisa digunakan.";
//        } else   if ([rc isEqualToString:@"51"]) {
//            msg = @"Maaf, jumlah saldo anda tidak mencukupi.";
//        } else   if ([rc isEqualToString:@"52"]) {
//            msg = @"Rekening tujuan tidak dikenal.";
//        } else   if ([rc isEqualToString:@"53"]) {
//            msg = @"Rekening tujuan tidak dikenal.";
//        } else   if ([rc isEqualToString:@"54"]) {
//            msg = @"Maaf, kartu ATM yang terdaftar untuk Syariah Indonesia MBG sudah kadaluwarsa.";
//        } else   if ([rc isEqualToString:@"55"]) {
//            msg = @"PIN anda salah.";
//        } else   if ([rc isEqualToString:@"56"]) {
//            msg = @"Maaf, kartu ATM anda belum terdaftar untuk layanan Syariah Indonesia MBG.";
//        } else   if ([rc isEqualToString:@"57"]) {
//            msg = @"Layanan ini tidak tersedia untuk jenis kartu anda.";
//        } else   if ([rc isEqualToString:@"61"]) {
//            msg = @"Maaf, anda telah melebihi batas maksimum jumlah transaksi untuk hari ini.";
//        } else   if ([rc isEqualToString:@"62"]) {
//            msg = @"Maaf, kartu anda diblokir";
//        } else   if ([rc isEqualToString:@"65"]) {
//            msg = @"Maaf, anda telah melebihi batas maksimum frekuensi transaksi untuk hari ini.";
//        } else   if ([rc isEqualToString:@"68"]) {
//            msg = @"Transaksi Anda kehabisan waktu. Silahkan cek saldo/mutasi sebelum melakukan transaksi lagi";
//        } else   if ([rc isEqualToString:@"75"]) {
//            msg = @"Maaf, PIN anda diblokir. Silakan hubungi Syariah Indonesia Customer Service atau BSI Call Center di (021) 5299 7755.";
//        } else   if ([rc isEqualToString:@"76"]) {
//            msg = @"Rekening tujuan tidak dikenal.";
//        } else   if ([rc isEqualToString:@"77"]) {
//            msg = @"Transaksi gagal, nomor rekening salah";
//        } else   if ([rc isEqualToString:@"82"]) {
//            msg = @"Tagihan bulan ini belum tersedia";
//        } else   if ([rc isEqualToString:@"88"]) {
//            msg = @"Tagihan anda telah terbayar";
//        } else   if ([rc isEqualToString:@"89"]) {
//            msg = @"Maaf, jaringan terputus, untuk sementara transaksi anda tidak dapat dilayani. ";
//        } else   if ([rc isEqualToString:@"91"]) {
//            msg = @"Maaf, untuk sementara transaksi anda tidak dapat dilayani. ";
//        } else   if ([rc isEqualToString:@"92"]) {
//            msg = @"Maaf, institusi tujuan sedang tidak dapat melayani transaksi. Cobalah beberapa saat lagi.";
//        } else   if ([rc isEqualToString:@"96"]) {
//            msg = @"Maaf, untuk sementara transaksi anda tidak dapat dilayani. ";
//        } else   if ([rc isEqualToString:@"99"]) {
//            msg = @"Jumlah saldo anda tidak mencukupi.";
//        }
//    } else {
//        if ([rc isEqualToString:@"03"]) {
//            msg = @"Sorry, we can not process your transaction at this moment. Please try again later.";
//        } else   if ([rc isEqualToString:@"05"]) {
//            msg = @"Transaction fail, system error";
//        } else   if ([rc isEqualToString:@"08"]) {
//            msg = @"Transaction in progress, check your transaction using 'Tunai Status' menu under Infomation menu";
//        } else   if ([rc isEqualToString:@"09"]) {
//            msg = @"Transaction fail,  reversal in progress.";
//        } else   if ([rc isEqualToString:@"13"]) {
//            msg = @"SORRY, THIS PAYMENT IS TEMPORARILY UNAVAILABLE. PLEASE CONTACT CALL CENTER.";
//        } else   if ([rc isEqualToString:@"14"]) {
//            msg = @"Sorry, your identifier not found.";
//        } else   if ([rc isEqualToString:@"15"]) {
//            msg = @"The account number is not registered.";
//        } else   if ([rc isEqualToString:@"31"]) {
//            msg = @"Please contact your Syariah Indonesia branch office for this service.";
//        } else   if ([rc isEqualToString:@"33"]) {
//            msg = @"Sorry, your registered ATM card for Syariah Indonesia MBG is no longer active.";
//        } else   if ([rc isEqualToString:@"38"]) {
//            msg = @"Your PIN has been blocked. Please contact Syariah Indonesia Customer Service or Call Center";
//        } else   if ([rc isEqualToString:@"40"]) {
//            msg = @"Sorry, this service is not available at this moment.  Please try again later.";
//        } else   if ([rc isEqualToString:@"51"]) {
//            msg = @"Your available balance is not sufficient for this transaction.";
//        } else   if ([rc isEqualToString:@"52"]) {
//            msg = @"Beneficiary account number is not recognized.";
//        } else   if ([rc isEqualToString:@"53"]) {
//            msg = @"Beneficiary account number is not recognized.";
//        } else   if ([rc isEqualToString:@"54"]) {
//            msg = @"Sorry, your registered ATM card for Syariah Indonesia MBG has expired.";
//        } else   if ([rc isEqualToString:@"55"]) {
//            msg = @"Wrong PIN.";
//        } else   if ([rc isEqualToString:@"56"]) {
//            msg = @"Your ATM card has not been registered for Syariah Indonesia MBG service.";
//        } else   if ([rc isEqualToString:@"57"]) {
//            msg = @"The service is not available for your card.";
//        } else   if ([rc isEqualToString:@"61"]) {
//            msg = @"You have achieved maximum amount of transaction within a day.";
//        } else   if ([rc isEqualToString:@"62"]) {
//            msg = @"Your card is blocked.";
//        } else   if ([rc isEqualToString:@"65"]) {
//            msg = @"You have achieved maximum number of transactions within a day.";
//        } else   if ([rc isEqualToString:@"68"]) {
//            msg = @"Your transaction timed out. Please check your balance/history prior to performing other transaction";
//        } else   if ([rc isEqualToString:@"75"]) {
//            msg = @"Your PIN has been blocked. Please contact Syariah Indonesia Customer Service or Call Center.";
//        } else   if ([rc isEqualToString:@"76"]) {
//            msg = @"Beneficiary account number is not recognized. Please try again.";
//        } else   if ([rc isEqualToString:@"77"]) {
//            msg = @"Transaction fail, invalid account";
//        } else   if ([rc isEqualToString:@"82"]) {
//            msg = @"Your bill is not yet available";
//        } else   if ([rc isEqualToString:@"88"]) {
//            msg = @"Your bill already paid";
//        } else   if ([rc isEqualToString:@"89"]) {
//            msg = @"Network problem, we can not process your transaction at this moment. Please try again later.";
//        } else   if ([rc isEqualToString:@"91"]) {
//            msg = @"Sorry, we can not process your transaction at this moment. Please try again later.";
//        } else   if ([rc isEqualToString:@"92"]) {
//            msg = @"Sorry, beneficiary institution not available right now. Please try again later.";
//        } else   if ([rc isEqualToString:@"96"]) {
//            msg = @"Sorry, we can not process your transaction at this moment. Please try again later.";
//        } else   if ([rc isEqualToString:@"99"]) {
//            msg = @"Your available balance is not sufficient for this transaction.";
//        }
//    }
//    
//    return msg;
//}

@end
