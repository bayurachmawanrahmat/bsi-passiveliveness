//
//  ChildPF01ViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 06/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "ViewController.h"
#import "XLButtonBarPagerTabStripViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChildPF01ViewController : XLButtonBarPagerTabStripViewController

@end

NS_ASSUME_NONNULL_END
