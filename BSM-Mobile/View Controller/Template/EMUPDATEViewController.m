//
//  EMUPDATEViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 01/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "EMUPDATEViewController.h"
#import "EMSCANViewController.h"
#import "CustomBtn.h"

@interface EMUPDATEViewController ()<EMSCANDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSDictionary *info;
    NSNumberFormatter *currencyFormatter;
}

@property (weak, nonatomic) IBOutlet CustomBtn *doneBtn;

@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@property (weak, nonatomic) IBOutlet UIView *vwAfter;
@property (weak, nonatomic) IBOutlet UIView *vwBefore;

@property (weak, nonatomic) IBOutlet UILabel *lblSuccessUpdate;
@property (weak, nonatomic) IBOutlet UILabel *lblAddedNominal;
@property (weak, nonatomic) IBOutlet UILabel *lblCardNumber;

@property (weak, nonatomic) IBOutlet UIView *viewBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleLastBal;
@property (weak, nonatomic) IBOutlet UILabel *lblLastBal;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNewBal;
@property (weak, nonatomic) IBOutlet UILabel *lblNewBal;

@property (weak, nonatomic) IBOutlet UILabel *lblGuideText;

@end

@implementation EMUPDATEViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    currencyFormatter = [[NSNumberFormatter alloc]init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [Styles setTopConstant:self.topConstant];
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.viewBalance.layer.cornerRadius = 10;
    self.viewBalance.layer.borderColor = [UIColorFromRGB(0xEEEEEE)CGColor];
    self.viewBalance.layer.borderWidth = 1;
    
    if(self.jsonData){
        self.lblTitleBar.text = [self.jsonData objectForKey:@"title"];
    }
    
    [self.imgIconBar setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    
    if([language isEqualToString:@"id"])
    {
        self.lblSuccessUpdate.text = @"UPDATE SALDO BERHASIL!";
        self.lblTitleLastBal.text = @"Saldo Awal";
        self.lblTitleNewBal.text = @"Saldo Akhir";
        [self.doneBtn setTitle:@"Selesai" forState:UIControlStateNormal];
    }else{
        self.lblSuccessUpdate.text = @"UPDATE BALANCE SUCCESS!";
        self.lblTitleLastBal.text = @"Last Balance";
        self.lblTitleNewBal.text = @"New Balance";
        [self.doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    }
    
    [self.vwAfter setHidden:YES];
    
    [self.doneBtn addTarget:self action:@selector(actionDone) forControlEvents:UIControlEventTouchUpInside];
    
    [self gotoscan];
}

- (void) gotoscan{
    if (@available(iOS 13.0, *)) {
        EMSCANViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"EMSCAN"];
        [templateView setDelegate:self];
        if([language isEqualToString:@"id"]){
            [templateView setTitleBar:@"Update Saldo"];
        }else{
            [templateView setTitleBar:@"Update Balance"];
        }
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateView animated:YES];
    }else{
        NSString *msg = @"";
        if ([language isEqualToString:@""]) {
            msg = @"Mohon maaf fitur ini tersedia untuk iOS 13+, silahkan update iOS anda";
        }else{
            msg = @"I'm sorry this feature available for iOS 13+, please update your iOS";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)cardInformation:(NSDictionary *)data{
    info = data;
//    [newDict setValue:mErrorMessage forKey:@"error"];
//    [newDict setValue:cardUID forKey:@"carduid"];
//    [newDict setValue:cardInfo forKey:@"cardinfo"];
//    [newDict setValue:cardType forKey:@"cardtype"];
//    [newDict setValue:cardNumber forKey:@"cardnumber"];
//    [newDict setValue:cardAttribute forKey:@"cardattr"];
//    [newDict setValue:cardSelected forKey:@"cardselect"];
//    [newDict setValue:lastBalance forKey:@"lastbalance"];
//    [newDict setValue:lastTransactionTime forKey:@"lasttrxtime"];
//    [newDict setValue:nBalance forKey:@"newbalance"];
    
    if([[info valueForKey:@"error"] isEqualToString:@""]){
        
        int nominal = [[info valueForKey:@"newbalance"]intValue] - [[info valueForKey:@"lastbalance"]intValue];
        if([language isEqualToString:@"id"]){
            self.lblCardNumber.text = [NSString stringWithFormat:@"Nomor Kartu : %@",[info valueForKey:@"cardnumber"]];
            self.lblAddedNominal.text = [NSString stringWithFormat:@"Nominal Rp. %@ berhasil di tambahkan", [currencyFormatter stringFromNumber:[NSNumber numberWithInt:nominal]]];
        }else{
            self.lblCardNumber.text = [NSString stringWithFormat:@"Card Number : %@",[info valueForKey:@"cardnumber"]];
            self.lblAddedNominal.text = [NSString stringWithFormat:@"Nominal Rp. %@ successfully added", [currencyFormatter stringFromNumber:[NSNumber numberWithInt:nominal]]];

        }
//        [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[data[0] objectForKey:@"target_amount"] doubleValue]]]
//        self.lblNewBal.text = [info valueForKey:@"newbalance"];
        self.lblNewBal.text = [NSString stringWithFormat:@"Rp. %@",[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[info valueForKey:@"newbalance"] doubleValue]]]];
//        self.lblLastBal.text = [info valueForKey:@"lastbalance"];
        self.lblLastBal.text = [NSString stringWithFormat:@"Rp. %@",[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[info valueForKey:@"lastbalance"] doubleValue]]]];


        [self.vwBefore setHidden:YES];
        [self.vwAfter setHidden:NO];
    }else{
        [self.vwBefore setHidden:YES];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[info valueForKey:@"error"]  preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)actionDone{
    [self backToR];
}

@end
