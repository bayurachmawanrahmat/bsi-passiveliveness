//
//  HJO02ViewController.h
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 09/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HJO02ViewController : TemplateViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewContent;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblDisclm;

@end

NS_ASSUME_NONNULL_END
