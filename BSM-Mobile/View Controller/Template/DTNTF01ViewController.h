//
//  DTNTF01ViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 11/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DTNTF01ViewController : TemplateViewController\

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

-(void) paramAction : (NSString *) action;

@end

NS_ASSUME_NONNULL_END
