//
//  CF04ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 26/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CF04ViewController.h"
#import "PopInfoViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface CF04ViewController (){
    UIView *whiteBlankView;
    NSUserDefaults *userDefaults;
    NSString *language;
}
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *label4;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation CF04ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstraint];
    [self.labelTitle setText:[self.jsonData objectForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
        
    [self.cancelButton addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    
    if([language isEqualToString:@"id"]){
        self.label3.text = @"Nomor Rekening";
    }else{
        self.label3.text = @"Account Number";
    }
    
    [self requesting];
    [self.contentView setHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [self blankView];
}

- (void) blankView{
    whiteBlankView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,self.contentView.frame.size.width, self.contentView.frame.size.height)];
    [whiteBlankView setBackgroundColor:[UIColor whiteColor]];
    [self.contentView addSubview:whiteBlankView];
}

- (void)requesting{
    
//    {"iccid":"db893c4a174e46f6","imei":"b6f2bda1080723a8","version_name":"5.18.0","version_value":"5180","request_type":"generate_qurban","id":"1","hewan_qurban":"Sapi","customer_id":"1006","target_harga":"18625001","target_waktu":"2021-03-01","frekuensi":"2","target_mulai":"2020-09-05","setoran":"3104166","dayofweek":"","dayofmonth":"4","id_account":"7004711585","type_autosave":"6001","pin":"123456","transaction_id":"20200605093332380625"}
    
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00201"] ||
           [[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00135"]){
            NSString *url =[NSString stringWithFormat:@"%@,id,hewan_qurban,target_harga,target_waktu,frekuensi,target_mulai,setoran,dayofweek,dayofmonth,id_account,type_autosave,customer_id,pin,account_title",[self.jsonData valueForKey:@"url_parm"]];
//            NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:nil];
        }else{
            NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
        }
    }

}

- (void) actionCancel{
//    [self gotoGold];
    [self goBack];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
//    NSLog(@"%@", jsonObject);
    
       NSLog(@"%@", jsonObject);
        if([requestType isEqualToString:@"zpk"]){
            [super didFinishLoadData:jsonObject withRequestType:requestType];
            return;
        }
        
        if(![requestType isEqualToString:@"check_notif"]){
            
            if( [[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"] ){
                [whiteBlankView removeFromSuperview];
				if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00110"]){
                    PopInfoViewController *popInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"POPINFOGOLD"];
                    [popInfo setPosition:4];
                    [self presentViewController:popInfo animated:YES completion:nil];
                }
                [self.contentView setHidden:NO];
                
                NSString *response = [jsonObject objectForKey:@"response"];
                NSError *error;
                NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                        options:NSJSONReadingAllowFragments
                                                                          error:&error];
                if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00201"] || [[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00135"]){
                    [self.accountLabel setText:[jsonObject objectForKey:@"accno"]];
                    if([language isEqualToString:@"id"]){
                        [self.label4 setText:@"Terima kasih telah menggunakan BSI mobile.\nSemoga layanan kami mendatangkan berkah bagi anda"];
                        [self.label2 setText:@"Tabungan Anda Berhasil Dibuat"];
                    }else{
                        [self.label4 setText:@"Thanks for using BSI mobile.\nHopefully our services can bring bless for you"];
                        [self.label2 setText:@"Your Savings Has Successfully Created"];
                    }
//
                }else{
                    if([dataDict valueForKey:@"accno"] != nil){
                        [self.accountLabel setText:[dataDict valueForKey:@"accno"]];
                    }else{
                        [self.accountLabel setText:[jsonObject objectForKey:@"accno"]];
                    }
                    [self.label4 setText:[dataDict valueForKey:@"footer_msg"]];
                    [self.label2 setText:[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"title"]]];
                }
                
                [dataManager.dataExtra setObject:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
                
            } else if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"99"]){
                NSString * msg = [jsonObject valueForKey:@"response"];
                UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
                [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                [self presentViewController:alertCont animated:YES completion:nil];
                
            } else {
                if ([jsonObject valueForKey:@"response"]){
                   if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
                       NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
       
                       UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
                       [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                           [self backToR];
                       }]];
                       if (@available(iOS 13.0, *)) {
                           [alertCont setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                       }
                       [self presentViewController:alertCont animated:YES completion:nil];

                   } else {
                       NSString * msg = [jsonObject valueForKey:@"response"];
                       UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
                       [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                           [self backToR];
                       }]];
                       if (@available(iOS 13.0, *)) {
                           [alertCont setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                       }
                       [self presentViewController:alertCont animated:YES completion:nil];
                   }
               } else {
                   NSString *msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
                   
                   UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER, msg] preferredStyle:UIAlertControllerStyleAlert];
                   [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                       [self backToR];
                   }]];
                   if (@available(iOS 13.0, *)) {
                       [alertCont setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                   }
                   [self presentViewController:alertCont animated:YES completion:nil];
               }
                
            }
            
        }
}

- (void) goBack{
    if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00110"]){
        [self gotoGold];
    }else{
        [self backToR];
    }
}

@end
