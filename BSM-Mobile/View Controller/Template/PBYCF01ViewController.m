//
//  PBYCF01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 8/15/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PBYCF01ViewController.h"
#import "Utility.h"

@interface PBYCF01ViewController ()<ConnectionDelegate, UIAlertViewDelegate>{
    NSUserDefaults *userDefaults;
    NSString *lang;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet CustomBtn *btnAjukan;
@property (weak, nonatomic) IBOutlet UILabel *lblMenuTitle;

- (IBAction)actionBatal:(id)sender;
- (IBAction)actionAjukan:(id)sender;


@end

@implementation PBYCF01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    lang = [[userDefaults valueForKey:@"AppleLanguages"]objectAtIndex:0];
    [self.lblMenuTitle setText:[self.jsonData valueForKey:@"title"]];
    
    self.lblMenuTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblMenuTitle.textAlignment = const_textalignment_title;
    self.lblMenuTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblMenuTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    NSString *strFulContent, *strContentTrigger;
    if ([lang isEqualToString:@"id"]) {
        [self.btnBatal setTitle:@"Belum" forState:UIControlStateNormal];
        [self.btnAjukan setTitle:@"Ajukan Pembiayaan" forState:UIControlStateNormal];
        strFulContent = @"Jika anda tertarik, Klik tombol \"Ajukan Pembiyaan\" untuk masuk ke tahap berikutnya";
        strContentTrigger = @"\"Ajukan Pembiyaan\"";
        
    }else{
        [self.btnBatal setTitle:@"Not Yet" forState:UIControlStateNormal];
        [self.btnAjukan setTitle:@"Submit Financing" forState:UIControlStateNormal];
        [self.lblContent setText:@"If you are interested, click the \"Submit Financing\" button to enter the next step"];
        strFulContent = @"If you are interested, click the \"Submit Financing\" button to enter the next step";
        strContentTrigger = @"\"Submit Financing\"";
    }
    NSString *dataBurroqMsg = [dataManager.dataExtra valueForKey:@"data_burroq_msg"];
    NSLog(@"ini fin_id >> %@", [dataManager.dataExtra valueForKey:@"fin_id"]);
    NSArray *dataMessage = [dataBurroqMsg componentsSeparatedByString:@"\n"];
    NSString *mDesc = [[dataMessage objectAtIndex:1]stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
    [self.lblTitle setText:mDesc];
    [self.lblTitle sizeToFit];
//    NSLog(@"lbl title text : %@", self.lblTitle.text);
    
    NSMutableAttributedString *strContent = [[NSMutableAttributedString alloc] initWithString:strFulContent];
    [strContent addAttribute:NSFontAttributeName
                       value: [UIFont fontWithName:@"Lato-Bold" size:16]
                       range:[strFulContent rangeOfString:strContentTrigger]];
    [self.lblContent setAttributedText:strContent];
    [self.lblContent sizeToFit];
    
//    if (IPHONE_5) {
//        [self.btnBatal.titleLabel setFont:[UIFont fontWithName:@"Lato" size:12]];
//        [self.btnAjukan.titleLabel setFont:[UIFont fontWithName:@"Lato" size:12]];
//    }
    
    [self.btnBatal setColorSet:SECONDARYCOLORSET];
    [self.btnAjukan setColorSet:PRIMARYCOLORSET];
    
//    [self setupLayout];
    
    
}

-(void) setupLayout{
    CGRect frmVwTittle = self.vwTitle.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwBtn = self.vwBtn.frame;
    
    CGRect frmLblMenuTitle = self.lblMenuTitle.frame;
    
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmLblContent = self.lblContent.frame;
    
    CGRect frmBtnBatal = self.btnBatal.frame;
    CGRect frmBtnAjukan = self.btnAjukan.frame;
    
    frmVwTittle.origin.x = 0;
    frmVwTittle.size.width = SCREEN_WIDTH;
    frmVwTittle.origin.y = TOP_NAV;
    
    frmLblMenuTitle.size.width = frmVwTittle.size.width - (frmLblMenuTitle.origin.x * 2);
    
    frmVwScroll.origin.x = frmVwTittle.origin.x;
    frmVwScroll.origin.y = frmVwTittle.origin.y + frmVwTittle.size.height;
    frmVwScroll.size.width = SCREEN_WIDTH;
    
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.size.height = 40;
    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 16;
    
    if ([Utility isDeviceHaveNotch]) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
    }
    
    frmVwScroll.size.height = frmVwBtn.origin.y - 8;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = SCREEN_WIDTH;
    
    frmLblTitle.origin.x = 24;
    frmLblTitle.origin.y = 16;
    frmLblTitle.size.width = SCREEN_WIDTH - (frmLblTitle.origin.x * 2);
    frmLblTitle.size.height = [Utility optimalSizeForLabel:_lblTitle inMaxSize:CGSizeMake(frmLblTitle.size.width, FLT_MAX)].height;
   
    
    frmLblContent.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height;
    frmLblContent.origin.x = frmLblTitle.origin.x;
   // frmLblContent.size.height = [Utility heightLabelDynamic:self.lblContent sizeFont:15];
    frmLblContent.size.width = SCREEN_WIDTH - (frmLblContent.origin.x *2);
//    frmLblContent.size.height =  [Utility dynamicLabelHeight:_lblContent];
    
    frmVwContentScroll.size.height = frmLblContent.origin.y + frmLblContent.size.height + 16;
    
    frmBtnBatal.origin.x = 16;
    frmBtnBatal.origin.y = 0;
    frmBtnBatal.size.height = frmVwBtn.size.height;
    frmBtnBatal.size.width = frmVwBtn.size.width/2 - (frmBtnBatal.origin.x *2);
    
    frmBtnAjukan.origin.y = frmBtnBatal.origin.y;
    frmBtnAjukan.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
    frmBtnAjukan.size.height = frmBtnBatal.size.height;
    frmBtnAjukan.size.width = frmVwBtn.size.width - frmBtnAjukan.origin.x - 16;
    
    
    self.vwTitle.frame = frmVwTittle;
    self.vwScroll.frame = frmVwScroll;
    self.vwBtn.frame = frmVwBtn;
    
    self.lblMenuTitle.frame = frmLblMenuTitle;
    
    self.vwContentScroll.frame = frmVwContentScroll;
    
    self.lblTitle.frame = frmLblTitle;
    self.lblContent.frame = frmLblContent;
    
    self.btnBatal.frame = frmBtnBatal;
    self.btnAjukan.frame = frmBtnAjukan;

}

- (IBAction)actionBatal:(id)sender {
//    NSDictionary* userInfo = @{@"position": @(1118)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
//    [self backToR];
}

- (IBAction)actionAjukan:(id)sender {
    [self openNextTemplate];
}
@end
