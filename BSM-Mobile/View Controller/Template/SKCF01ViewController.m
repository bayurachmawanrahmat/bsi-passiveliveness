//
//  SKCF01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 2/7/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SKCF01ViewController.h"
#import "Utility.h"

@interface SKCF01ViewController ()<ConnectionDelegate, UIAlertViewDelegate, UITextViewDelegate, UIScrollViewDelegate>{
    NSString *sCodeID;
    NSString *txtConfirmSKCF01;
    NSArray *arrStrConfirm;
    NSMutableArray *arrValChk;
    NSMutableArray *arrValChkLink;
    NSUInteger lengthArrayConfirm;
    CGFloat maxButtonPosition;
    NSString *cekAktif;
    NSUserDefaults *userDefault;
    
    int numberoflink;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtViewConfirm;
//@property (weak, nonatomic) IBOutlet UIButton *btnN;
//@property (weak, nonatomic) IBOutlet UIButton *btnB;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;

- (IBAction)next:(id)sender;
- (IBAction)batal:(id)sender;

@end

@implementation SKCF01ViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
   
    numberoflink = 0;
   
    if([lang isEqualToString:@"id"]){
          [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
          [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
      } else {
          [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
          [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
      }
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    [_btnBatal setColorSet:SECONDARYCOLORSET];
    [_btnNext setColorSet:PRIMARYCOLORSET];
    [_imgTitle setHidden:YES];
    
    self.txtViewConfirm.editable = NO;
    [self.txtViewConfirm setScrollEnabled:NO];
    self.txtViewConfirm.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    arrStrConfirm = [[NSArray alloc] init];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmTVC = self.txtViewConfirm.frame;
    CGRect frmScroll = self.vwScroll.frame;
    CGRect frmVwButton = self.vwButton.frame;
    CGRect frmBtNext = self.btnBatal.frame;
    CGRect frmBtBatal = self.btnNext.frame;
    
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmScroll.size.width = screenWidth;
//    frmScroll.size.height = screenHeight;
    frmScroll.origin.y = frmTitle.origin.y + frmTitle.size.height;
    frmTVC.size.width = screenWidth - 32;
    frmTVC.size.height = screenHeight/3;

    frmVwButton.size.height = 40;
    frmVwButton.origin.x = 0;
    frmVwButton.origin.y = screenHeight - (BOTTOM_NAV+frmVwButton.size.height+16);
    frmVwButton.size.width = screenWidth;
    frmVwButton.size.height = BOTTOM_NAV+16;
    
    frmBtNext.origin.x = 16;
    frmBtNext.origin.y = 4;
    frmBtNext.size.width = (frmVwButton.size.width/2)-32;
    frmBtNext.size.height = 40;
    frmBtBatal.origin.x = (frmVwButton.size.width/2)+16;
    frmBtBatal.origin.y = 4;
    frmBtBatal.size.width = frmBtNext.size.width;
    frmBtBatal.size.height = 40;
    
    if ([Utility isDeviceHaveNotch]) {
         frmVwButton.origin.y = screenHeight - (BOTTOM_NAV+frmVwButton.size.height+50);
     }
    frmTVC.size.height = frmVwButton.origin.y-frmTitle.origin.y;

    
    sCodeID = [dataManager.dataExtra valueForKey:@"code"];
    cekAktif= [dataManager.dataExtra valueForKey:@"status"];
    
    CGFloat bottomPadding = 0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.windows.firstObject;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    frmScroll.size.height = screenHeight-(bottomPadding+BOTTOM_NAV+frmScroll.origin.y);

    
    self.vwTitle.frame = frmTitle;
    self.lblTitle.frame = frmLblTitle;
    self.txtViewConfirm.frame = frmTVC;
    self.vwScroll.frame = frmScroll;
    self.vwButton.frame = frmVwButton;
    self.btnNext.frame = frmBtBatal;
    self.btnBatal.frame =  frmBtNext;
    
    [self showConfirmation];
    
    [self.vwScroll setContentSize:CGSizeMake(screenWidth, maxButtonPosition + 150)];

    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        if( [sCodeID isEqualToString:@"00079"] ){
            Connection *conn2 = [[Connection alloc]initWithDelegate:self];

            NSString *url = [NSString stringWithFormat:@"%@,id_series=%@,kode_pemesanan=%@,amount=%@",[self.jsonData valueForKey:@"url_parm"],[dataManager.dataExtra valueForKey:@"id_series"],[dataManager.dataExtra valueForKey:@"kode_pemesanan"], [dataManager.dataExtra valueForKey:@"jumlah"] ];
            [conn2 sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
            
           
        } else if( [sCodeID isEqualToString:@"00077"] ){
           Connection *conn2 = [[Connection alloc]initWithDelegate:self];
            if([dataManager.dataExtra valueForKey:@"hp"]){
                [dataManager.dataExtra setValue:[dataManager.dataExtra valueForKey:@"hp"] forKey:@"mobile_no"];
            }
            
            NSString *status = @"";
            if([dataManager.dataExtra objectForKey:@"status"]){
                status = [dataManager.dataExtra valueForKey:@"status"];
            }
            
            NSString *url = [NSString stringWithFormat:@"%@,file_name_ktp=%@,file_name_npwp=%@,mobile_no=%@,status=%@,npwp,ktp",[self.jsonData valueForKey:@"url_parm"],[dataManager.dataExtra valueForKey:@"file_name_ktp"],[dataManager.dataExtra valueForKey:@"file_name_npwp"],[dataManager.dataExtra valueForKey:@"mobile_no"],status];
           [conn2 sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                   
                  
               }
        else{
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
              Connection *conn = [[Connection alloc]initWithDelegate:self];
              [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
            
        }
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

- (void)textViewDidChange:(UITextView *)textView {
    CGSize contentSize = [textView sizeThatFits:textView.bounds.size];
    CGRect frame = textView.frame;
    frame.size.height = contentSize.height;
    textView.frame = frame;
    NSLayoutConstraint *aspectRatioTextViewConstraint = [NSLayoutConstraint constraintWithItem:textView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeWidth multiplier:textView.bounds.size.height/textView.bounds.size.width constant:1.0];
    [textView addConstraint:aspectRatioTextViewConstraint];
}

- (IBAction)next:(id)sender {
    BOOL validChecked = false;
    for (int i=0; i<[arrValChk count]; i++){
        if ([[arrValChk objectAtIndex:i]isEqualToString:@"NO"]) {
            validChecked = false;
            break;
        }else{
            validChecked = true;
        }
    }
    if (validChecked) {
        [super openNextTemplate];
    }else{
        NSString *msg=@"";
        if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
            msg = @"Pastikan seluruh pernyataan terceklis";
        }else{
            msg = @"Make sure all statements are checked";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];

        [self presentViewController:alert animated:YES completion:nil];
//
    }
}

- (IBAction)batal:(id)sender {
    [super backToR];
}

- (void)showConfirmation {
    
    arrValChk = [[NSMutableArray alloc] init];
    arrValChkLink = [[NSMutableArray alloc] init];
    if ([sCodeID isEqualToString:@"00077"]){
        NSString *txtFilter = [txtConfirmSKCF01 stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\r\n"];
        arrStrConfirm = [txtFilter componentsSeparatedByString:@"|"];
    }else if([sCodeID isEqualToString:@"00078"]){
        NSString *txtFilterFontTag = [txtConfirmSKCF01 stringByReplacingOccurrencesOfString:@"<font color='blue'>" withString:@"<font>"];
//        NSString *txtFilterFontCloseTag = [txtFilterFontTag stringByReplacingOccurrencesOfString:@"</font>" withString:@""];
        NSString *txtFilterFontCloseTag = [txtFilterFontTag stringByReplacingOccurrencesOfString:@"" withString:@""];
        arrStrConfirm = [txtFilterFontCloseTag componentsSeparatedByString:@"|"];
    }else if ([sCodeID isEqualToString:@"00079"]){
       NSString *txtFilter = [txtConfirmSKCF01 stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\r\n"];
       arrStrConfirm = [txtFilter componentsSeparatedByString:@"|"];
   }
    
    self.lblTitle.text = [self.jsonData valueForKey:@"title"];
    if ([arrStrConfirm count] > 0 || [arrStrConfirm count]!=0){
        self.txtViewConfirm.text = [arrStrConfirm objectAtIndex:0];
        [self textViewDidChange:self.txtViewConfirm];
        
        [self fillValueChk];
        
        lengthArrayConfirm = [arrStrConfirm count];
        for (int x=1; x<lengthArrayConfirm; x++) {
            [self createCheckBox:x];
        }
    }
    
}

-(void)fillValueChk {
    for (int x=1; x<[arrStrConfirm count]; x++) {
        [arrValChk addObject:@"NO"];
    }
}

-(void)createCheckBox:(int)idx {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
   
    [myButton setImage:[UIImage imageNamed:@"blank_check.png"] forState:UIControlStateNormal];
    myButton.tag = idx;
        myButton.backgroundColor = [UIColor whiteColor];
    [myButton addTarget:self action:@selector(chkAction:) forControlEvents:UIControlEventTouchUpInside];
    
    myButton.frame = CGRectMake(self.txtViewConfirm.frame.origin.x, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35)+10, 25, 25);
    
    UITextView *myLabel = [[UITextView alloc] init];
    
    if([sCodeID isEqualToString:@"00078"]){
        if(idx == 1){
            
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*80), screenWidth - 62, 400);
        }else if(idx == 2 || idx == 3){
        
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 150);
            CGRect frmButton = myButton.frame;
            CGRect frmLabel = myLabel.frame;
            
            if(idx == 2 || idx == 3){
                frmButton.origin.y = frmButton.origin.y + 45;
                frmLabel.origin.y = frmButton.origin.y-10;
            }
            
            myButton.frame = frmButton;
            myLabel.frame = frmLabel;
        
        }else if (idx == 4){
            
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*80), screenWidth - 62, 150);
            CGRect frmButton = myButton.frame;
            CGRect frmLabel = myLabel.frame;
            
            if(idx == 4){
                frmButton.origin.y = frmButton.origin.y + 45;
                frmLabel.origin.y = frmButton.origin.y-10;
            }
            
            myButton.frame = frmButton;
            myLabel.frame = frmLabel;
            
        } else if(idx == 5){
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 150);
            CGRect frmButton = myButton.frame;
            CGRect frmLabel = myLabel.frame;

            if(idx == 5){
                frmButton.origin.y = frmButton.origin.y + 90;
                frmLabel.origin.y = frmButton.origin.y-10;
            }

            myButton.frame = frmButton;
            myLabel.frame = frmLabel;

        }else if(idx == 6){
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*80), screenWidth - 62, 150);
            CGRect frmButton = myButton.frame;
            CGRect frmLabel = myLabel.frame;

            if(idx == 6){
                frmButton.origin.y = frmButton.origin.y + 90;
                frmLabel.origin.y = frmButton.origin.y-10;
            }

            myButton.frame = frmButton;
            myLabel.frame = frmLabel;

        }else if(idx >= 7){
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*90), screenWidth - 62, 150);
            CGRect frmButton = myButton.frame;
            CGRect frmLabel = myLabel.frame;

            if(idx >= 7){
                frmButton.origin.y = frmButton.origin.y + 160;
                frmLabel.origin.y = frmButton.origin.y-10;
            }

            myButton.frame = frmButton;
            myLabel.frame = frmLabel;
            
            myLabel.backgroundColor = [UIColor clearColor];

        }else{
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 150);
        }
        
        //        if(idx >2){
        //            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 200);
        //            CGRect frmButton = myButton.frame;
        //            CGRect frmLabel = myLabel.frame;
        //
        //            if(idx >= 4){
        //                frmButton.origin.y = frmButton.origin.y + 70;
        //                frmLabel.origin.y = frmButton.origin.y;
        //            }
        //            myButton.frame = frmButton;
        //            myLabel.frame = frmLabel;
        //        }else{
        //            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 150);
        //        }
    }else if([sCodeID isEqualToString:@"00077"]){

        if(idx >=1){
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 90);
            CGRect frmButton = myButton.frame;
            CGRect frmLabel = myLabel.frame;
            
            if(idx > 3){
                frmButton.origin.y = frmButton.origin.y + 20;
                frmLabel.origin.y = frmButton.origin.y;
            }
            myButton.frame = frmButton;
            myLabel.frame = frmLabel;
        }else{
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 150);
        }
    }else if([sCodeID isEqualToString:@"00079"]){
        if(idx >1){
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35), screenWidth - 62, 90);
            CGRect frmButton = myButton.frame;
            CGRect frmLabel = myLabel.frame;
            
            myButton.frame = frmButton;
            myLabel.frame = frmLabel;
        }else{
            myLabel.frame = CGRectMake(myButton.frame.origin.x + myButton.frame.size.width + 10, self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35)+10, screenWidth - 62, 50);
        }
    }
    
    [myLabel setFont:[UIFont fontWithName:@"Lato-Regular" size:12]];
    myLabel.textContainer.maximumNumberOfLines = 10;
    myLabel.textContainer.lineBreakMode = NSLineBreakByWordWrapping;
    myLabel.textAlignment = NSTextAlignmentJustified;
    
    [myLabel setEditable:NO];
    [myLabel setScrollEnabled:NO];
    myLabel.scrollIndicatorInsets = UIEdgeInsetsZero;
    
    NSString *valStr = [[arrStrConfirm objectAtIndex:idx] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray *arrStrConfirmURL = [valStr componentsSeparatedByString:@"<link>"];
    
    if([arrStrConfirmURL count] > 1 ){
        myLabel.delegate = self;
        [myLabel setSelectable:YES];
        [myLabel setUserInteractionEnabled:YES];
        [myLabel setDataDetectorTypes:UIDataDetectorTypeLink];
        [myLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        myLabel.tag = idx;
        
//        NSArray *arrStrFont = [[arrStrConfirmURL objectAtIndex:1]componentsSeparatedByString:@"<font>"];
        
        NSArray *arrStrFont = [[NSArray alloc]init];
        //new handle at 27112020 i dont know why i should do this, i'm sorry
        if([[arrStrConfirmURL objectAtIndex:1] containsString:@"<font color='blue'>"]){
            arrStrFont = [[arrStrConfirmURL objectAtIndex:1]componentsSeparatedByString:@"<font color='blue'>"];
        }else if([[arrStrConfirmURL objectAtIndex:1] containsString:@"<font>"]){
            arrStrFont = [[arrStrConfirmURL objectAtIndex:1]componentsSeparatedByString:@"<font>"];
        }
        
        NSString *strLinkText = [[arrStrConfirmURL objectAtIndex:2] stringByReplacingOccurrencesOfString:@"'" withString:@""];
        NSString *strTriggerText = @"";
        NSString *strFullText =@"";
        
        //new handle at 27112020 i dont know why i should do this, i'm sorry
        NSArray *arrNewText = [[NSArray alloc]init];
        arrNewText = [strLinkText componentsSeparatedByString:@"\n"];

        if ([arrStrFont count] !=0){
            strTriggerText = [arrStrFont objectAtIndex:1];
            //new one
            if([strTriggerText containsString:@"</font>"]){
                NSArray *arrlnkteks = [strTriggerText componentsSeparatedByString:@"</font>"];
                strTriggerText = [arrlnkteks objectAtIndex:0];
//                strTriggerText = [strTriggerText stringByReplacingOccurrencesOfString:@"</font>" withString:@""];
            }
            
            NSString *newarrStrfont = [[arrStrFont objectAtIndex:1] stringByReplacingOccurrencesOfString:@"</font>" withString:@""];
            
            if([strLinkText containsString:@"\n"]){
                
                //new one
                strFullText = [NSString stringWithFormat:@"%@ %@ %@", [arrStrFont objectAtIndex:0], newarrStrfont, arrNewText[1]];
            }else{
                
                //old one
                strFullText = [NSString stringWithFormat:@"%@ %@", [arrStrFont objectAtIndex:0], newarrStrfont];
            }
        }
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc]initWithString:strFullText attributes:nil];
        
        //new handle at 27112020 i dont know why i should do this, i'm sorry
        if([strLinkText containsString:@"\n"]){
            [mutableString addAttribute:NSLinkAttributeName value:arrNewText[0] range:[strFullText rangeOfString:strTriggerText]];
            [mutableString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Regular" size:12] range:NSMakeRange(0, [strFullText length])];
            
        }else{
            //Add the link attribute across the range of the target text
            [mutableString addAttribute:NSLinkAttributeName value:strLinkText range:[strFullText rangeOfString:strTriggerText]];
            [mutableString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Regular" size:12] range:NSMakeRange(0, [strFullText length])];
        }
        
        
//        //Add the link attribute across the range of the target text
//        [mutableString addAttribute:NSLinkAttributeName value:strLinkText range:[strFullText rangeOfString:strTriggerText]];
        
//        Add any other font or color bling as needed
//        [mutableString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"helvetica" size:12] range:NSMakeRange(0, [strFullText length])];
        
        //Set the mutable text to the textfield
        [myLabel setAttributedText: mutableString];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:@"NO" forKey:@"state"];
        [dict setValue:[NSString stringWithFormat:@"%d",idx] forKey:@"index"];
        [dict setValue:[NSString stringWithFormat:@"%d",numberoflink] forKey:@"linkIndex"];
        
        numberoflink = numberoflink + 1;
        [arrValChkLink addObject:dict];
        
    }else{
        [myLabel setSelectable:NO];
        [myLabel setText:valStr];
    }
    
    CGRect frmLabel = myLabel.frame;
    [_vwScroll addSubview:myButton];
    [_vwScroll addSubview:myLabel];
    
    if (idx == (lengthArrayConfirm-1)) {
        maxButtonPosition =
        self.txtViewConfirm.frame.origin.y+self.txtViewConfirm.frame.size.height+((idx-1)*35)+10+frmLabel.size.height;
    }
}

- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    NSLog(@"%@",textView.text);
    NSInteger no = textView.tag;
//    NSInteger count = [arrValChkLink count];
    int idxx = 0;
    for(NSDictionary *dict in arrValChkLink){
        if([[dict objectForKey:@"index"]intValue] == no){
            if([[dict objectForKey:@"state"]isEqualToString:@"NO"]){
                NSMutableDictionary *dact = [[NSMutableDictionary alloc]init];
                [dact setValue:[dict objectForKey:@"index"] forKey:@"index"];
                [dact setValue:[dict objectForKey:@"linkIndex"] forKey:@"linkIndex"];
                [dact setValue:@"YES" forKey:@"state"];
                
                [arrValChkLink setObject:dact atIndexedSubscript:idxx];
                break;
            }
        }
        idxx = idxx + 1;
    }
    
//    if([sCodeID isEqualToString:@"00078"]){
//        if ([[arrValChkLink objectAtIndex:(count-1)] isEqualToString:@"NO"]){
//            [arrValChkLink replaceObjectAtIndex:(count-1) withObject:[NSString stringWithFormat:@"YES"]];
//        }
//    }else{
//        if ([[arrValChkLink objectAtIndex:(no-(count - numberoflink))] isEqualToString:@"NO"]){
//            [arrValChkLink replaceObjectAtIndex:(no-(count - numberoflink)) withObject:[NSString stringWithFormat:@"YES"]];
//        }
//    }


    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    return NO;
}

-(void)chkAction:(UIButton *)sender{
    [Utility animeBounchCb:sender];
    NSUInteger no = sender.tag;
    if ([[arrValChk objectAtIndex:(no-1)] isEqualToString:@"NO"]) {
        if(arrValChkLink.count > 0){
            int idxx = -1;
            BOOL found = NO;
            for(NSDictionary *dict in arrValChkLink){
                if([[dict objectForKey:@"index"]intValue] == no){
                    if([[dict objectForKey:@"state"]isEqualToString:@"YES"]){
                        UIButton *editButton = (UIButton *) sender;
                        [editButton setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
//                        [arrValChk replaceObjectAtIndex:no withObject:[NSString stringWithFormat:@"YES"]];
                        [arrValChk setObject:@"YES" atIndexedSubscript:no-1];
                        found = YES;
                        break;
                    }
                    idxx = [[dict objectForKey:@"index"]intValue];
                }
            }
            
            if(!found && idxx != no){
                UIButton *editButton = (UIButton *) sender;
                [editButton setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
                [arrValChk replaceObjectAtIndex:(no-1) withObject:[NSString stringWithFormat:@"YES"]];
            }
        }
        else{
            UIButton *editButton = (UIButton *) sender;
            [editButton setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            [arrValChk replaceObjectAtIndex:(no-1) withObject:[NSString stringWithFormat:@"YES"]];
        }
    } else if ([[arrValChk objectAtIndex:(no-1)] isEqualToString:@"YES"]) {
        UIButton *editButton = (UIButton *) sender;
        [editButton setImage:[UIImage imageNamed:@"blank_check.png"] forState:UIControlStateNormal];
        [arrValChk replaceObjectAtIndex:(no-1) withObject:[NSString stringWithFormat:@"NO"]];
    }
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }

        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            if ([requestType isEqualToString:@"list_sukuk"]) {
    
                txtConfirmSKCF01 = [jsonObject objectForKey:@"response"];
                
                if([jsonObject objectForKey:@"email"]){
                    [dataManager.dataExtra setValue:[jsonObject valueForKey:@"email"] forKey:@"email"];
                }
                if([jsonObject objectForKey:@"mobile_no"]){
                    [dataManager.dataExtra setValue:[jsonObject valueForKey:@"mobile_no"] forKey:@"mobile_no"];
                }

                CGRect screenBound = [[UIScreen mainScreen] bounds];
                CGSize screenSize = screenBound.size;
                CGFloat screenWidth = screenSize.width;
                CGFloat screenHeight = screenSize.height;

                CGRect frmTitle = self.vwTitle.frame;
                CGRect frmTVC = self.txtViewConfirm.frame;
                CGRect frmScroll = self.vwScroll.frame;
                CGRect frmVwButton = self.vwButton.frame;


                frmTitle.size.width = screenWidth;
                frmScroll.size.width = screenWidth;
                CGFloat bottomPadding = 0;
                if (@available(iOS 11.0, *)) {
                    UIWindow *window = UIApplication.sharedApplication.windows.firstObject;
                    bottomPadding = window.safeAreaInsets.bottom;
                }
                frmScroll.size.height = screenHeight-(bottomPadding+BOTTOM_NAV+frmScroll.origin.y);
                
                frmScroll.origin.y = frmTitle.origin.y + frmTitle.size.height;
                frmTVC.size.width = screenWidth - 32;
                frmTVC.size.height = screenHeight;

                [self showConfirmation];
                
                if([sCodeID isEqualToString:@"00079"]){
                    if (IPHONE_X || IPHONE_XS_MAX) {
                        frmTVC.size.height = screenHeight/4;
                    }
                }

                if([sCodeID isEqualToString:@"00077"] & [cekAktif isEqualToString:@"ACTIVE"]){
                    CGRect frmBtnBatal = self.btnBatal.frame;
                    CGRect frmVwButton = self.vwButton.frame;
                    frmBtnBatal.origin.x = (frmVwButton.size.width/2) -(frmBtnBatal.size.width/2);
                    
                    [_btnBatal setTitle:@"OK" forState:UIControlStateNormal];
                    _btnNext.hidden = YES;

                }
                frmVwButton.origin.y = maxButtonPosition + 150;
                self.vwTitle.frame = frmTitle;
                self.txtViewConfirm.frame = frmTVC;
                self.vwScroll.frame = frmScroll;
                self.vwButton.frame = frmVwButton;
                                                
                [self.vwScroll setContentSize:CGSizeMake(screenWidth, frmVwButton.origin.y + frmVwButton.size.height + 150)];
            }
        }else if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"99"]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }

}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [BSMDelegate reloadApp];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
