//
//  SKCF03ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 26/05/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SKCF03ViewController.h"
#import "CustomBtn.h"

@interface SKCF03ViewController ()<UITextViewDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *message;
    NSString *activeBtnTitle;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconTitle;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
//@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
//@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;

@end

@implementation SKCF03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self setupLayout];
    
    self.txtView.editable = NO;
    self.txtView.scrollIndicatorInsets = UIEdgeInsetsZero;
    self.txtView.textAlignment = NSTextAlignmentCenter;
    
    [self.titleBar setText:@"Surat Berharga Negara"];
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.imgIconTitle setHidden:YES];
    
    [self.btnNext setTitle:@"OK" forState:UIControlStateNormal];
    [self.btnNext addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
    
    [self.txtView setText:message];
    if(activeBtnTitle){
        [self.btnNext setTitle:activeBtnTitle forState:UIControlStateNormal];
    }
}

- (void) setupLayout{
    CGRect frmVwTitle = self.vwTitleBar.frame;
    CGRect frmIconTitle = self.imgIconTitle.frame;
    CGRect frmTitlBar = self.titleBar.frame;
    CGRect frmTextView = self.txtView.frame;
    CGRect frmBtnNext = self.btnNext.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmTitlBar.size.height = 40;
    frmTitlBar.size.width = frmVwTitle.size.width - 16;
    frmTitlBar.origin.x = 8;
    frmTitlBar.origin.y = 5;

    frmBtnNext.size.height = 42;
    frmBtnNext.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtnNext.size.height - 8;
    
    if ([Utility isDeviceHaveBottomPadding]) {
        frmBtnNext.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_X - 16;
    }
    
    frmBtnNext.size.width = SCREEN_WIDTH/2;
    frmBtnNext.origin.x = frmBtnNext.size.width/2;

    frmTextView.origin.x = 16;
    frmTextView.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    frmTextView.size.height = frmBtnNext.origin.y - frmTextView.origin.y - 8;
    frmTextView.size.width = SCREEN_WIDTH - (frmTextView.origin.x*2);
    
//    frmTitlBar.origin.x = frmIconTitle.origin.x + frmIconTitle.size.width + 8;
//    frmTitlBar.origin.y = frmVwTitle.size.height/2 - frmTitlBar.size.height/2;
//    frmTitlBar.size.width = frmVwTitle.size.width - frmTitlBar.origin.x - 16;
    
    self.vwTitleBar.frame = frmVwTitle;
    self.titleBar.frame = frmTitlBar;
    self.btnNext.frame = frmBtnNext;
    self.txtView.frame = frmTextView;
}


- (void) setMessage:(NSString*) msg{
    message = msg;
}

- (void) setButtonTitle:(NSString *)btnTitle{
    activeBtnTitle = btnTitle;
}

- (void) actionOK{
    if([activeBtnTitle isEqualToString:@"Pemesanan"] || [activeBtnTitle isEqualToString:@"Order"]){
        [dataManager.dataExtra setValue:@"00078" forKey:@"code"];
        [self openTemplateByMenuID:@"00069" andCodeContent:@"00078"];
    }else{
        [self backToR];
    }
}

@end
