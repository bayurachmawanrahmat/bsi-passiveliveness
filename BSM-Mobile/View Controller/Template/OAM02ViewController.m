//
//  OAM02ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/30/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "OAM02ViewController.h"
#import "Utility.h"
#import "CellUbp.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface OAM02ViewController()<ConnectionDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate> {
    NSArray *listData;
    NSString* requestType;
    NSString* urlSyarat;
    NSString *menuId;
    NSString *state;
    bool chaked;
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    int rowSelected;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblGunaTabungan;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

- (IBAction)next:(id)sender;
- (IBAction)batal:(id)sender;
@end

@implementation OAM02ViewController

NSString *menuIdRdi = @"00035";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to selanjutnya and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
//        [_btnN setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
//        [_btnB setTitle:@"BATAL" forState:UIControlStateNormal];
        [_btnN setTitle:@"BATAL" forState:UIControlStateNormal];
        [_btnB setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
    } else {
//        [_btnN setTitle:@"NEXT" forState:UIControlStateNormal];
//        [_btnB setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_btnN setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_btnB setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTItle = self.lblTitle.frame;
    CGRect frmButton = self.vwButton.frame;
    CGRect frmTable = self.tblGunaTabungan.frame;
    CGRect frmBtnN = self.btnN.frame;
    CGRect frmBtnB = self.btnB.frame;
    
    frmTitle.origin.x = 0;
    frmTitle.origin.y = TOP_NAV;
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 50;
    
    frmLblTItle.size.height = 40;
    frmLblTItle.origin.x = 16;
    frmLblTItle.size.width = frmTitle.size.width - 32;
    frmLblTItle.origin.y = 5;
    
    frmBtnB.origin.x = screenWidth - (frmBtnB.size.width) - 25;
    frmBtnN.origin.x = 25;
    frmTable.size.width = screenWidth;
    frmButton.origin.y = screenHeight - frmButton.size.height - 62;
    frmTable.size.height = frmButton.origin.y - frmTable.origin.y;
    frmBtnN.origin.x = 8;
    frmBtnN.size.width = (screenWidth/2) - 12;
    frmBtnB.origin.x = (screenWidth/2) + 4;
    frmBtnB.size.width = (screenWidth/2) - 12;
    frmButton.size.width = screenWidth;
    
    frmButton.size.height = 40;
    frmBtnB.size.height = 40;
    frmBtnN.size.height = 40;
    
    if (IPHONE_X) {
        frmButton.origin.y = frmButton.origin.y - 72;
    }else if (IPHONE_XS_MAX){
        frmButton.origin.y = frmButton.origin.y - 60;
    }
    
    
    self.btnN.frame = frmBtnN;
    self.btnB.frame = frmBtnB;
    self.lblTitle.frame = frmLblTItle;
    self.vwTitle.frame = frmTitle;
    self.vwButton.frame = frmButton;
    self.tblGunaTabungan.frame = frmTable;
    
    [self.btnB setColorSet:PRIMARYCOLORSET];
    [self.btnN setColorSet:SECONDARYCOLORSET];
   
    
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    self.imgTitle.image = [UIImage imageNamed:img];
    self.imgTitle.hidden = YES;
    
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.tblGunaTabungan.dataSource = self;
    self.tblGunaTabungan.delegate = self;
    rowSelected = -1;
    
    menuId = [self.jsonData valueForKey:@"menu_id"];
    if ([menuId isEqualToString:menuIdRdi]){
        state = @"account_opening_purpose";
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:@"request_type=rdi,step=account_opening_purpose,menu_id,device,device_type,ip_address,language,date_local,customer_id" needLoading:true encrypted:true banking:true favorite:nil];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    }else{
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"] boolValue]];
    }
}

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

-(void)goToNext{
    [super openNextTemplate];
}

#pragma mark change batal to goToNext
- (IBAction)batal:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    [self backToRoot];
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"cellUbp";
    CellUbp *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CellUbp alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    //cell.lbl.text = [NSString stringWithFormat:@"%@ %@", [data valueForKey:@"billname"], [data valueForKey:@"billamount"]];
    cell.lbl.text = [data valueForKey:@"title"];
    cell.lbl.font = [UIFont fontWithName:const_font_name1 size:14];
    cell.lbl.numberOfLines = 0;
    [cell.lbl sizeToFit];
    
    if(rowSelected == indexPath.row){
        cell.icon.image = [UIImage imageNamed:@"radio_select"];
        cell.icon.image = [cell.icon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.icon.tintColor = UIColorFromRGB(const_color_primary);
    } else {
        cell.icon.image = [UIImage imageNamed:@"radio_unselect"];
    }
        
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGRect frmLabel = cell.lbl.frame;
    CGRect frmIcon = cell.icon.frame;
    frmIcon.origin.y = frmLabel.origin.y;
    
    cell.icon.frame = frmIcon;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewAutomaticDimension;
    NSString *text = @"";
    
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    text = [data valueForKey:@"title"];
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+10.0;
}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 70.0;
//}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    rowSelected = indexPath.row;
    [dataManager.dataExtra addEntriesFromDictionary:@{@"purpose":[data objectForKey:@"id"]}];
    [self.tblGunaTabungan reloadData];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
                NSString *response = [jsonObject valueForKey:@"response"];
                NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                listData = [dict objectForKey:@"list"];
              //  [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id": [jsonObject objectForKey:@"transaction_id"]}];
                [self.tblGunaTabungan reloadData];
            } else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }if([requestType isEqualToString:@"rdi"]){
            if([state isEqualToString:@"account_opening_purpose"]){
                if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                    NSString *responses = [jsonObject objectForKey:@"response"];
                    NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[responses dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    listData = [dataDict objectForKey:@"list"];
                 //   [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id": [jsonObject objectForKey:@"transaction_id"]}];
                    [self.tblGunaTabungan reloadData];
                }
            }else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self backToR];
                }]];
                if (@available(iOS 13.0, *)) {
                    [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
