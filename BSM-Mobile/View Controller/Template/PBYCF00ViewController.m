//
//  PBYCF02ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 17/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PBYCF00ViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface PBYCF00ViewController (){
    NSUserDefaults *userDefaults;
    NSString *language;
    
    NSArray *listContent;
    NSString *dataMessageBurroq;
    NSString *findId;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleContent;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBefore;

@end

@implementation PBYCF00ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([language isEqualToString:@"id"]){
        [self.titleBar setText:@"Pembiayaan"];
        [self.labelTitleContent setText:@"Ajukan Pembiayaan"];
        
        self.labelContent.text = @"Fasilitas untuk memenuhi kebutuhan konsumtif anda tanpa agunan / jaminan tanpa perlu datang ke bank";
        
        listContent = @[@"Akad Musyarakah Mutaniqshah",
        @"Angsuran tetap hingga jatuh tempo",
        @"Sesuai Syariah",
        @"Isi form dalam aplikasi dan bisa dapatkan fasilitas pembiayaannya"];
        
        [self.btnBefore setTitle:@"Belum" forState:UIControlStateNormal];
        [self.btnNext setTitle:@"Lanjut" forState:UIControlStateNormal];
    }else{

        [self.titleBar setText:@"Financing"];
        [self.labelTitleContent setText:@"Application"];
        
        self.labelContent.text = @"Facility to fulfill your Consumptive needs without collateral/guarantee and No Need to come to the bank";

        listContent = @[@"Musyarakah Mutaniqshah Contract",
        @"Fixed Installment",
        @"Shariah Compliant",
        @"Fill in the form in the application and get the financing facility"];
        
        [self.btnBefore setTitle:@"Not Yet" forState:UIControlStateNormal];
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
    }
    
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    
    [self.imgTop setImage:[UIImage imageNamed:@"ic_home_kirim_uang2.png"]];
    
    [Styles setTopConstant:_topConstraint];
    
    [self.labelTitleContent setTextColor:UIColorFromRGB(const_color_secondary)];
    [self.labelTitleContent setFont: [UIFont fontWithName:@"Lato-Bold" size:17.0]];
    
    [self createList];
    
    [self.btnBefore addTarget:self action:@selector(actionBefore) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext addTarget:self action:@selector(goToNext) forControlEvents:UIControlEventTouchUpInside];
    
    [self.btnBefore setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
}

- (void) createList{
    int lastY = 4;
    for(NSString *strContent in listContent){
        
        UIImageView *imageRadio  = [[UIImageView alloc]initWithFrame:CGRectMake(4, lastY, 20, 20)];
        [imageRadio setImage: [UIImage imageNamed:@"ic_checkbox_active.png"]];
        UILabel *labelRadio = [[UILabel alloc]initWithFrame:CGRectMake(28, lastY, (_contentView.frame.size.width - 32), 0)];
        [labelRadio setText:strContent];
        [labelRadio setNumberOfLines:0];
        [labelRadio sizeToFit];
        [labelRadio setFont:[UIFont fontWithName:@"Lato" size:15]];
        [labelRadio setTextColor:[UIColor blackColor]];
        
        if(lastY < 5){
            UITapGestureRecognizer *gotoURL =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(loadURL:)];
            [labelRadio addGestureRecognizer:gotoURL];
            [labelRadio setUserInteractionEnabled:YES];
            [labelRadio setTextColor:[UIColor blueColor]];

        }
        
        [self.contentView addSubview:imageRadio];
        [self.contentView addSubview:labelRadio];
        
        lastY =  lastY + [Utility heightLabelDynamic:labelRadio sizeWidth:(_contentView.frame.size.width - 32) sizeFont:15] + 4;
    }
    
    _contentViewHeight.constant = lastY;
    
}

- (void) loadURL:(UITapGestureRecognizer *)recognizer{
    NSString *pURL = @"https://www.bankbsi.co.id/consumer-banking/pembiayaan-konsumen/pembiayaan-online-mitraguna";
    NSURL *url = [NSURL URLWithString:pURL];
    if( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:pURL]]){
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}

- (void) setDataMessageBurroq:(NSString*) string{
    dataMessageBurroq = string;
}

- (void) setFindId:(NSString*) string{
    findId = string;
}

- (void) actionBefore{
//    UINavigationController *navigationCont = self.navigationController;
//    [navigationCont popViewControllerAnimated:YES];
    [self backToR];
}

- (void) goToNext{
//    [self startIdleTimer];
//    [self getMenuIdx:@"indexFinancing"];
    int idx = 8;
    if ([userDefaults objectForKey:@"indexFinancing"]) {
        idx = [[userDefaults objectForKey:@"indexFinancing"] intValue];
    }
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:idx];
    if (temp != nil || temp.count > 0) {
        NSDictionary *dataTemp = [temp objectAtIndex:1];
        if (dataTemp) {
            DataManager *dataManager = [DataManager sharedManager];
            if (dataMessageBurroq != nil) {
                [dataManager.dataExtra setValue:dataMessageBurroq forKey:@"data_burroq_msg"];
                [dataManager.dataExtra setValue:findId forKey:@"fin_id"];
            }
            [dataManager setAction:[dataTemp objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
            dataManager.currentPosition = 0;

            NSDictionary *tempData = [dataManager getObjectData];
            TemplateViewController *templateViews = [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
            [templateViews setJsonData:tempData];

            bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
            if (stateAcepted) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }else{
                [self.tabBarController setSelectedIndex:0];
                UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                [currentVC pushViewController:templateViews animated:YES];
            }
        }else{
            NSLog(@"Data Temp Nil" );
        }
    }else{
        NSLog(@"Temp Nil");
    }
}

@end
