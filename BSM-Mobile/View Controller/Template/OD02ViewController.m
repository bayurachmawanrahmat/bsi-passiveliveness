//
//  OD02ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 25/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "OD02ViewController.h"
#import "CellCL01.h"
#import "CellCL02.h"
#import "Styles.h"
#import "Utility.h"

@interface OD02ViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    NSArray *tenorList;
    NSArray *maturityList;
    NSUserDefaults *userDefaults;
    NSString *lang;
    
    NSNumberFormatter* currencyFormatter;
    NSString *limitMin, *limitMax;
    
//    BOOL checkMarkZkat;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelNominal;
@property (weak, nonatomic) IBOutlet UILabel *subLabelNominal;
@property (weak, nonatomic) IBOutlet UITextField *tfNominal;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet CustomBtn *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleTenor;
@property (weak, nonatomic) IBOutlet UITableView *tableviewTenor;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleJenisDeposito;
@property (weak, nonatomic) IBOutlet UITableView *tableViewJenisDeposito;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *table2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *table1Height;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UITextField *tfName;
@property (weak, nonatomic) IBOutlet UILabel *lblZakat;
@property (weak, nonatomic) IBOutlet UIButton *checkMarkZakat;
@property (weak, nonatomic) IBOutlet UIImageView *checkMarkZakatImg;

@end

@implementation OD02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    lang = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    [_labelTitle setText: [self.jsonData objectForKey:@"title"]];
    [_iconTitle setImage: [UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

//    checkMarkZkat = NO;
//    [dataManager.dataExtra setValue:@"N" forKey:@"zakat"];

    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@"."];
    
    
    [self.tableViewJenisDeposito registerNib:[UINib nibWithNibName:@"CellCL01" bundle:nil]
    forCellReuseIdentifier:@"CELLCL01"];
    [self.tableviewTenor registerNib:[UINib nibWithNibName:@"CellCL02" bundle:nil]
    forCellReuseIdentifier:@"CELLCL02"];
            
    _tableviewTenor.delegate = self;
    _tableviewTenor.dataSource = self;
    _tableviewTenor.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _tableViewJenisDeposito.delegate = self;
    _tableViewJenisDeposito.dataSource = self;
    _tableViewJenisDeposito.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableViewJenisDeposito.estimatedRowHeight = 70.0;
    _tableViewJenisDeposito.rowHeight = UITableViewAutomaticDimension;
    
    [self createToolbarButton:_tfNominal];
    _tfNominal.keyboardType = UIKeyboardTypeNumberPad;
    _tfNominal.delegate = self;
    
    _tfName.keyboardType = UIKeyboardTypeDefault;
    _tfName.delegate = self;
    [self createToolbarButton:_tfName];
    
    [Styles setTopConstant:_topConstraint];
    [Styles setStyleTextFieldBottomLine:_tfName];
    [Styles setStyleTextFieldBottomLine:_tfNominal];

    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    [self.nextButton setColorSet:PRIMARYCOLORSET];
    
//    [self.checkMarkZakat addTarget:self action:@selector(actionZakat) forControlEvents:UIControlEventTouchUpInside];
}

//- (void) actionZakat{
//    if(checkMarkZkat){
//        checkMarkZkat = NO;
//        [self.checkMarkZakat setImage:[UIImage imageNamed:@"ic_checkbox_inactive"] forState:UIControlStateNormal];
//        [self.checkMarkZakatImg setImage:[UIImage imageNamed:@"ic_checkbox_inactive"]];
//        [dataManager.dataExtra setValue:@"N" forKey:@"zakat"];
//    }else{
//        checkMarkZkat = YES;
//        [self.checkMarkZakat setImage:[UIImage imageNamed:@"ic_checkbox_active"] forState:UIControlStateNormal];
//        [self.checkMarkZakatImg setImage:[UIImage imageNamed:@"ic_checkbox_active"]];
//        [dataManager.dataExtra setValue:@"Y" forKey:@"zakat"];
//    }
//}

- (void) reloadLabel{
    if([lang isEqualToString:@"id"]){
//        [_tfNominal setPlaceholder:@"Masukkan Nominal disini"];
        [_tfName setPlaceholder:@""];
        [_lblName setText:@"Peruntukan/Tujuan Deposito"];
        [_lblZakat setText:@"Zakat Bagi Hasil"];
        [_cancelButton setTitle:@"Batal" forState:UIControlStateNormal];
        [_nextButton setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_labelNominal setText:@"Masukkan Nominal"];
        [_labelTitleTenor setText:@"Pilihan Jangka Waktu"];
        [_labelTitleJenisDeposito setText:@"Jenis Deposito"];
        if([limitMax doubleValue] != 0){
            [_subLabelNominal setText:[NSString stringWithFormat:@"(Minimum Rp. %@ Maximum Rp. %@)", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[limitMin doubleValue]]], [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[limitMax doubleValue]]]]];
        }else{
            [_subLabelNominal setText:[NSString stringWithFormat:@"(Minimum Rp. %@)", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[limitMin doubleValue]]]]];
        }

    }else{
//        [_tfNominal setPlaceholder:@"Input Nominal Here"];
        [_tfName setPlaceholder:@""];
        [_lblName setText:@"Purpose of Deposit"];
        [_lblZakat setText:@"Profit Sharing Zakat"];
        [_labelNominal setText:@"Input Nominal"];
        [_cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [_nextButton setTitle:@"Next" forState:UIControlStateNormal];
        [_labelTitleTenor setText:@"Select Time of Period"];
        [_labelTitleJenisDeposito setText:@"Type of Deposito"];
        if([limitMax doubleValue] != 0){
            [_subLabelNominal setText:[NSString stringWithFormat:@"(Minimum Rp. %@ Maximum Rp. %@)", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[limitMin doubleValue]]], [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[limitMax doubleValue]]]]];
        }else{
            [_subLabelNominal setText:[NSString stringWithFormat:@"(Minimum Rp. %@)", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[limitMin doubleValue]]]]];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
    
    if (textField == self.tfName) {
        if(textField.text.length < 35){
            NSCharacterSet *allowedSet = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
            if ([newText length] == 0){
                textField.text = nil;
            }else{
                if(![string isEqualToString:@" "]){
                    BOOL result = ([string rangeOfCharacterFromSet:allowedSet].location == NSNotFound);
                    if(!result){
                        NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        textField.text = newText;
                        return NO;
                    }
                }
            }
        }else{
            NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:@""];
            textField.text = newText;
            return NO;
        }
    }
    
    if(textField == self.tfNominal){
        double newValue = [newText doubleValue];
        if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
        }
        else{
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
            return NO;
        }
    }
    
    return YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [self doRequest];

}

- (void) doRequest{
    NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];

}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

-(void)doneEditButtonTapped:(id)done{
    [self.view endEditing:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == _tableViewJenisDeposito){
        return maturityList.count;
    }else{
        return tenorList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == _tableViewJenisDeposito){
        CellCL01 *cell = (CellCL01*)[tableView dequeueReusableCellWithIdentifier:@"CELLCL01"];
        
        NSLog(@"%@", [maturityList[indexPath.row]objectForKey:@"title_id"]);
        NSLog(@"%@", [maturityList[indexPath.row]objectForKey:@"desc_id"]);

        if([lang isEqualToString:@"id"]){
            cell.label1.text = [maturityList[indexPath.row]objectForKey:@"title_id"];
            cell.label2.text = [maturityList[indexPath.row]objectForKey:@"desc_id"];
        }else{
            cell.label1.text = [maturityList[indexPath.row]objectForKey:@"title_en"];
            cell.label2.text = [maturityList[indexPath.row]objectForKey:@"desc_en"];
        }
        [cell layoutSubviews];
        [cell layoutIfNeeded];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }else{
        CellCL02 *cell = (CellCL02*)[tableView dequeueReusableCellWithIdentifier:@"CELLCL02"];
        NSLog(@"%@", [tenorList[indexPath.row]objectForKey:@"desc_id"]);

        if([lang isEqualToString:@"id"]){
            cell.label1.text = [tenorList[indexPath.row]objectForKey:@"title_id"];
        }else{
            cell.label1.text = [tenorList[indexPath.row]objectForKey:@"title_en"];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    if(tableView == _tableViewJenisDeposito){
        [dataManager.dataExtra setValue:[maturityList[indexPath.row]objectForKey:@"id"] forKey:@"maturity_instr"];
        [dataManager.dataExtra setValue:[maturityList[indexPath.row]objectForKey:@"title_id"] forKey:@"maturity_instr_caption"];
        if([[maturityList[indexPath.row]objectForKey:@"option"] isEqualToString:@"NONE"]){
            [dataManager.dataExtra setValue:@"" forKey:@"aro_option"];
        }else{
            [dataManager.dataExtra setValue:[maturityList[indexPath.row]objectForKey:@"option"] forKey:@"aro_option"];
        }
    }else{
        [dataManager.dataExtra setValue:[tenorList[indexPath.row]objectForKey:@"id"] forKey:@"tenor"];
    }
}

- (IBAction)actionCancel:(id)sender {
    [self backToR];
}
- (IBAction)actionNext:(id)sender {
    
    [dataManager.dataExtra setValue:[_tfNominal.text stringByReplacingOccurrencesOfString:@"." withString:@""] forKey:@"amount"];
    [dataManager.dataExtra setValue:self.tfName.text forKey:@"desc_prod_ext"];
    
    if([self validate]){
        [self openNextTemplate];
    }
}

- (BOOL) validate{
    NSString *message = @"";
    if([_tfName.text isEqualToString:@""]){
        if([lang isEqualToString:@"id"]){
            message = @"Masukkan Peruntukkan/tujuan Deposito";
        }else{
            message = @"Please Enter Purpose of Deposito";
        }
    }
    else if([_tfNominal.text isEqualToString:@""]){
        if([lang isEqualToString:@"id"]){
            message = @"Masukkan Nominal Terlebih dahulu";
        }else{
            message = @"Please Enter Nominal first";
        }
    }
    
    else if([[_tfNominal.text stringByReplacingOccurrencesOfString:@"." withString:@""] doubleValue] < [limitMin doubleValue]){
        if([lang isEqualToString:@"id"]){
            message = @"Nominal yang anda masukkan kurang dari limit minimum";
        }else{
            message = @"Your entered nominal is less than minimum limit";
        }
    }
    
    else if([limitMax doubleValue] != 0 && [[_tfNominal.text stringByReplacingOccurrencesOfString:@"." withString:@""] doubleValue] > [limitMax doubleValue]){
       
        if([lang isEqualToString:@"id"]){
            message = @"Nominal yang anda masukkan melebihi limit maximum";
        }else{
            message = @"Your entered nominal is exceeded maximum limit";
        }
    }
    
    else if([dataManager.dataExtra objectForKey:@"tenor"] == nil || [[dataManager.dataExtra objectForKey:@"tenor"]isEqualToString:@""]){
        if([lang isEqualToString:@"id"]){
            message = @"Pilih Tenor Terlebih Dahulu";
        }else{
            message = @"Please Choose Time of Period";
        }

    }
    
    else if([dataManager.dataExtra objectForKey:@"maturity_instr"] == nil || [[dataManager.dataExtra objectForKey:@"maturity_instr"]isEqualToString:@""]){
        if([lang isEqualToString:@"id"]){
            message = @"Pilih Jenis Deposito Terlebih Dahulu";
        }else{
            message = @"Please Choose Type of Deposit";
        }
    }
    
    
    
    if(![message isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"account_opening_choice"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
                
                NSString *mResponse = [jsonObject objectForKey:@"response"];
                NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
                if([object objectForKey:@"tenor_list"]){
                    tenorList = [object objectForKey:@"tenor_list"];
                }

                if([object objectForKey:@"maturity_list"]){
                    maturityList = [object objectForKey:@"maturity_list"];
                }
                limitMin = [object objectForKey:@"min_deposit"];
                limitMax = [object objectForKey:@"max_deposit"];

                [self.tableviewTenor reloadData];
                [self.tableViewJenisDeposito reloadData];
                [self reloadLabel];

                self.table2Height.constant = self.tableViewJenisDeposito.contentSize.height;
                self.table1Height.constant = self.tableviewTenor.contentSize.height;
        }else{
            [Utility showMessage:[jsonObject objectForKey:@"response"] mVwController:self mTag:[[jsonObject objectForKey:@"response_code"]intValue]];
        }
    }
    
}



@end
