//
//  HJO03ViewController.h
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 20/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HJO03ViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblAkad;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll ;

@end

NS_ASSUME_NONNULL_END
