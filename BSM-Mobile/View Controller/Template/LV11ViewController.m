//
//  LV11ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 07/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "LV11ViewController.h"
#import "EditInfoCardOTPViewController.h"
#import "CellLV11.h"
#import "Styles.h"
#import "Utility.h"


@interface LV11ViewController ()<UITableViewDelegate, UITableViewDataSource, EditInfoCardOTPDelegate>{
    NSUserDefaults *userDefaults;
    NSArray* listAction;
    NSString* menuID;
    NSString* language;
}
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet CustomBtn *okButton;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIView *uiview;
@property (weak, nonatomic) IBOutlet UILabel *labelAgreement;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *okBtnHeight;

@end

@implementation LV11ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [Styles setTopConstant:_topConstraint];
    
    self.labelTitle.text = [self.jsonData objectForKey:@"title"];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    _iconTitle = [userDefaults objectForKey:@"imgIcon"];
    
    menuID = [self.jsonData objectForKey:@"menu_id"];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CellLV11" bundle:nil] forCellReuseIdentifier:@"LV11CELL"];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    if([menuID isEqualToString:@"00155"]){
        [self.tableView setAllowsSelectionDuringEditing:YES];
        [self.tableView setAllowsMultipleSelection:YES];
    }

//    [_lblInfo setBackgroundColor:[[UIColor systemBlueColor]colorWithAlphaComponent:0.5]];
    
//    [Styles setStyleButton:_okButton];
//    [Styles setStyleButton:_cancelButton];
    [self.okButton setColorSet:PRIMARYCOLORSET];
    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    
    [self showAgreements:false withText:@""];
    
    if([language isEqualToString:@"id"]){
        _label1.text = @"Nomor Kartu anda yang terdaftar adalah :";
        _lblInfo.text = @"Nomor Kartu yang di daftarkan maksimal 5 (lima) Kartu Debit OTP yang aktif";
    }else{
        _label1.text = @"Your Registered Card Number :";
        _lblInfo.text = @"The Card Number registered is a maximum of 5 (five) active Debit Cards OTP";
    }
    
    [self setRequest];
}

- (void)showAgreements:(BOOL) enabled withText:(NSString*) text{
    
    if([language isEqualToString:@"id"]){
        [_cancelButton setTitle:@"BATAL" forState:UIControlStateNormal];
        [_okButton setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
        
    }else{
        [_cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_okButton setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    if(enabled){
        _labelAgreement.text = text;
        _cancelBtnHeight.constant = 40;
        _okBtnHeight.constant = 40;
    }else{
        _labelAgreement.text = @"";
        _cancelBtnHeight.constant = 0;
        _okBtnHeight.constant = 0;
    }
}

- (void) showDaftar{
    [_uiview setHidden:YES];
    [_cancelButton setHidden:YES];
    [_okButton setHidden:YES];
        
    UIButton* btnRegister = [[UIButton alloc]initWithFrame:CGRectMake(
                                                                      32,
                                                                      SCREEN_HEIGHT - BOTTOM_NAV - TOP_NAV,
                                                                      SCREEN_WIDTH - 64,
                                                                      40)];
    [Styles setStyleButton:btnRegister];
    
    if([language isEqualToString:@"id"]){
        [btnRegister setTitle:@"Daftar Kartu" forState:UIControlStateNormal];
        [_label1 setText:@"Mohon untuk mendaftarkan Kartu Debit OTP anda"];
    }else{
        [btnRegister setTitle:@"Register Card" forState:UIControlStateNormal];
        [_label1 setText:@"Please register your Debit Card OTP"];
    }
    
    [self.view addSubview:btnRegister];
    [btnRegister addTarget:self action:@selector(gotoDaftarCardOTP) forControlEvents:UIControlEventTouchUpInside];
}

- (void) gotoDaftarCardOTP{
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:7]; //array menu ke 7
    if(temp != nil){
        NSDictionary *objectMain = [temp objectAtIndex:1];
        int ind = 1; //node ke 0

        NSArray *nTempAction = [[objectMain valueForKey:@"action"] objectAtIndex:ind];
        NSDictionary *object = [nTempAction objectAtIndex:1];

        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[nTempAction objectAtIndex:0]];
        dataManager.currentPosition++;
        
        NSDictionary *tempData = [dataManager getObjectData];
        [dataManager setAction:[[[tempData objectForKey:@"content"]objectAtIndex:1]objectForKey:@"action"] andMenuId:[nTempAction objectAtIndex:0]];
        dataManager.currentPosition++;
        tempData = [dataManager getObjectData];
        [self pushNavTemplate:tempData];

    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
    
}

-(void)pushNavTemplate : (NSDictionary *) tempData{
       
    TemplateViewController *templateViews =   [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
    [templateViews setJsonData:tempData];
    bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
    if (stateAcepted) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        [self.tabBarController setSelectedIndex:0];
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateViews animated:YES];
    }
    
}

- (void)setRequest{
    if([[self.jsonData valueForKey:@"url"]boolValue]){;
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"]  needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];

    }
}

- (void) finishState:(BOOL)state{
    if(state){
        [self setRequest];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if (![requestType isEqualToString:@"check_notif"]) {
        if ([requestType isEqualToString:@"list_otpcard"]) {
            if ([[jsonObject valueForKey:@"responsecode"] isEqualToString:@"00"]) {
                NSArray *listData = (NSArray *)[jsonObject valueForKey:@"info"];
                listAction = listData;
                
                if(listAction.count == 0){
                    [self showDaftar];
                }
                [self.tableView reloadData];
                                
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self backToR];
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        if ([requestType isEqualToString:@"update_otpcard"]) {
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
               NSError *error;
               NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[[jsonObject valueForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding]
               options:NSJSONReadingAllowFragments
                 error:&error];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[dataDict objectForKey:@"msg"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self setRequest];
                }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }else{
        
       UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self backToR];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CellLV11 *cell = (CellLV11*) [tableView dequeueReusableCellWithIdentifier:@"LV11CELL"];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if([menuID isEqualToString:@"00155"]){
        
        cell.title.text = [listAction[indexPath.row] objectForKey:@"card_no"];
        cell.buttonDesc.tag = indexPath.row;
        [cell.buttonDesc addTarget:self action:@selector(openDesc:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[listAction[indexPath.row]valueForKey:@"isactive"] boolValue] == true) {
            [self.tableView selectRowAtIndexPath:indexPath
                  animated:NO
            scrollPosition:UITableViewScrollPositionNone];
            [cell setState:@"active"];
        }else{
            [cell setState:@"inactive"];
        }
        
    }else if ([menuID isEqualToString:@"00127"]){
        if ([[listAction[indexPath.row]valueForKey:@"isactive"] boolValue] == true) {
            cell.title.text = [listAction[indexPath.row] objectForKey:@"card_no"];
            cell.buttonDesc.tag = indexPath.row;
            [cell.buttonDesc addTarget:self action:@selector(openDesc:) forControlEvents:UIControlEventTouchUpInside];
            [cell setState:@"active"];
        }
    }
    
    return cell;
}

- (IBAction)openDesc:(id)sender{
    UIButton *btn = (UIButton*) sender;
    
//    PopCardInfoViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"POPCARDINFO"];
//    [view DataMessage:listAction[btn.tag]];
//    [self presentViewController:view animated:YES completion:nil];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 20, 20)];
    
    [image setImage:[UIImage imageNamed:@"exclamation"]];
    [image setTintColor:UIColorFromRGB(const_color_primary)];
    
    NSString *stringTitleInfo = @"";
    if([language isEqualToString:@"id"]){
        stringTitleInfo = @"Info Kartu Debit OTP";
    }else{
        stringTitleInfo = @"Debit Card OTP Info";
    }
    
    NSString* stringMsg = @"";
    if(![[listAction[btn.tag] objectForKey:@"isactive"]doubleValue]){
    //        NSString *string =  anda telah me nonaktifkan no kartu 6034941234567877 pada tanggal 15/04/2020
        NSArray* arString = [[listAction[btn.tag] objectForKey:@"date_inactive"] componentsSeparatedByString:@" "];
        if([[[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
            stringMsg = [NSString stringWithFormat:@"Anda telah menonaktifkan no kartu %@ pada tanggal %@ %@ %@", [listAction[btn.tag] objectForKey:@"card_no"], arString[0], arString[1], arString[2]];
        }else{
            stringMsg = [NSString stringWithFormat:@"You have disactivate card number %@ pada tanggal %@ %@ %@", [listAction[btn.tag] objectForKey:@"card_no"], arString[0], arString[1], arString[2]];
        }
    }else{
        stringMsg = [listAction[btn.tag] objectForKey:@"desc"];
    }
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:stringTitleInfo
                                                                       message:stringMsg
                                                                preferredStyle:UIAlertControllerStyleAlert];
    

    if (@available(iOS 13.0, *)) {
        [alertCont setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alertCont.view addSubview:image];
    
    if([[listAction[btn.tag] objectForKey:@"isactive"] boolValue] == true){
        [alertCont addAction: [UIAlertAction actionWithTitle:@"Edit Info" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            EditInfoCardOTPViewController *view = [self.storyboard instantiateViewControllerWithIdentifier:@"EDINFCOTP"];
            [view setDelegate:self];
            [view DataMessage:self->listAction[btn.tag]];
            [self presentViewController:view animated:YES completion:nil];
        }]];
    }
    [alertCont addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertCont animated:YES completion:nil];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listAction.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if([menuID isEqualToString:@"00155"]){
        if ([[listAction[indexPath.row]valueForKey:@"isactive"] boolValue] == false) {
            UIButton* btn = [[UIButton alloc]init];
            btn.tag = indexPath.row;
            [self openDesc:btn];
        }
        
    }else if([menuID isEqualToString:@"00127"]){
        [self showAgreements:false withText:@""];
        
        NSDictionary *selectedList = [listAction objectAtIndex:indexPath.row];
        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:[selectedList objectForKey:@"card_no"]}];
        [dataManager.dataExtra setValue:[selectedList objectForKey:@"desc"] forKey:@"txt1"];
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = [NSString stringWithFormat:@"Anda akan Mentransaksikan Kartu %@ %@. Apakah Anda yakin ?", [selectedList objectForKey:@"card_no"], [selectedList objectForKey:@"desc"]];
        }else{
            msg = [NSString stringWithFormat:@"You will transact this card %@ %@. Are You sure ?", [selectedList objectForKey:@"card_no"], [selectedList objectForKey:@"desc"]];
        }
        [self showAgreements:true withText:msg];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([menuID isEqualToString:@"00155"]){
        if ([[listAction[indexPath.row]valueForKey:@"isactive"] boolValue] == true) {
            NSString *msg = @"";
            if([language isEqualToString:@"id"]){
                msg = @"Kartu yang dinonaktifkan tidak dapat diaktifkan kembali. Apakah Anda yakin ?";
            }else{
                msg = @"Disactivated Card cannot be reactivated . Are You sure ?";
            }
            NSDictionary *selectedList = [listAction objectAtIndex:indexPath.row];
            [dataManager.dataExtra setValue:[selectedList objectForKey:@"desc"] forKey:@"txt2"];
            [dataManager.dataExtra setValue:[selectedList objectForKey:@"card_no"] forKey:@"card_no"];
            [self showAgreements:true withText:msg];
//            [_tableView setUserInteractionEnabled:NO];
        }else{
            UIButton* btn = [[UIButton alloc]init];
            btn.tag = indexPath.row;
            [self openDesc:btn];
//            [_tableView setUserInteractionEnabled:YES];
        }
    }
}

- (IBAction)actionCancel:(id)sender {
    [self backToR];
}
- (IBAction)actionNext:(id)sender {
    if([menuID isEqualToString:@"00155"]){
        [self deactivateCard];
    }
    else if([menuID isEqualToString:@"00127"]){
        [self openNextTemplate];
    }
    [self showAgreements:NO withText:@""];
}

- (void) deactivateCard{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=update_otpcard,txt1=1,txt2,card_no"]  needLoading:true encrypted:true banking:true favorite:nil];
}

@end
