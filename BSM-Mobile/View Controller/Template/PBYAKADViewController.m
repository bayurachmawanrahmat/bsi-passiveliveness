//
//  PBYAKADViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/2/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PBYAKADViewController.h"
#import "Utility.h"
#import "NSString+HTML.h"
#import "PopupIsiPasanganViewController.h"
#import "Styles.h"

@interface PBYAKADViewController ()<ConnectionDelegate, UIAlertViewDelegate, UIScrollViewDelegate>{
    NSUserDefaults *userDefault;
    UIView* vwDynamicContent;
    NSString *lang;
    NSArray *dataContent;
    NSString *stringURL;
    BOOL isAggre;
    int nMartialState, stepPhase;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgStepper;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;

@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;
@property (weak, nonatomic) IBOutlet UIView *vwAkadInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblAkadInfo;
@property (weak, nonatomic) IBOutlet UIView *vwAkadBase;

@property (weak, nonatomic) IBOutlet UILabel *lblAkadTitle;

@property (weak, nonatomic) IBOutlet UILabel *termTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTermsTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblAkadKetentuan;
@property (weak, nonatomic) IBOutlet UIView *vwAkadAgreement;
@property (weak, nonatomic) IBOutlet UIView *vwRead;
@property (weak, nonatomic) IBOutlet UIView *vwAgrremenClick;

@property (weak, nonatomic) IBOutlet UILabel *lblAkdInfoKey;
@property (weak, nonatomic) IBOutlet UILabel *lblAkdInfoVal;
@property (weak, nonatomic) IBOutlet UIView *vwLine;

@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSelanjutnya;

@property (weak, nonatomic) IBOutlet UIButton *btnChecked;
@property (weak, nonatomic) IBOutlet UIButton *btnBaseChecked;
@property (weak, nonatomic) IBOutlet UILabel *lblChecked;

@property (weak, nonatomic) IBOutlet UIButton *btnChecked2;
@property (weak, nonatomic) IBOutlet UILabel *lblChecked2;
@property (weak, nonatomic) IBOutlet UIButton *btnBaseChecked2;

@property (weak, nonatomic) IBOutlet UIButton *btnChecked3;
@property (weak, nonatomic) IBOutlet UILabel *lblChecked3;
@property (weak, nonatomic) IBOutlet UIButton *btnBaseChecked3;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;


- (IBAction)actionBatal:(id)sender;
- (IBAction)actionSelanjutnya:(id)sender;



@end

@implementation PBYAKADViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Utility resetTimer];
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    isAggre = false;
    stepPhase = 0;
    
    [self.vwScroll setDelegate:self];
    
    if ([lang isEqualToString:@"id"]) {
        [self.btnBatal setTitle:@"SEBELUMNYA" forState:UIControlStateNormal];
        [self.btnSelanjutnya setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
    }else{
        [self.btnBatal setTitle:@"PREVIOUS" forState:UIControlStateNormal];
        [self.btnSelanjutnya setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    [self.btnBatal setColorSet:SECONDARYCOLORSET];
    [self.btnSelanjutnya setColorSet:PRIMARYCOLORSET];
    
    if (IPHONE_5) {
        [self.btnBatal.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [self.btnSelanjutnya.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
        [self.lblAkdInfoKey setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        [self.lblAkdInfoVal setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    }
    
    self.lblTitleMenu.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleMenu.textAlignment = const_textalignment_title;
    self.lblTitleMenu.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleMenu.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnSelanjutnya setEnabled:false];
    self.btnSelanjutnya.layer.backgroundColor = [[UIColor grayColor]CGColor];
    
    [self.btnBaseChecked addTarget:self action:@selector(actionAgreeAkad:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnChecked setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
    [self.btnChecked setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    
    [self.btnBaseChecked2 addTarget:self action:@selector(actionAgreeAkad:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnChecked2 setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
    [self.btnChecked2 setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    
    [self.btnBaseChecked3 addTarget:self action:@selector(actionOpenURL) forControlEvents:UIControlEventTouchUpInside];
    [self.btnChecked3 setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
    [self.btnChecked3 setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    [self.btnChecked3 setEnabled:NO];
    [self.btnChecked3 addTarget:self action:@selector(actionAgreeAkad:) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.jsonData){
        [self.lblTitleMenu setText:[self.jsonData valueForKey:@"title"]];
        bool needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if (needRequest) {
            [self.vwContentScroll setHidden:YES];
            NSString *urlParam = [self.jsonData valueForKey:@"url_parm"];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,loan_amount,loan_tenor,insurance_code,asset_type,asset_amount",urlParam] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
    }
    
    [self showWhiteLoad];
    [self setupNewLayoutPhase3a];
}

- (void) popupDataPas:(NSNotification *) notification {
    if ([notification.name isEqualToString:@"popupDataPas"]){
        NSDictionary *userInfo = notification.userInfo;
        bool isNotEmpty = [[userInfo valueForKey:@"data_state"]boolValue];
        if (isNotEmpty) {
            [_btnSelanjutnya setEnabled:YES];
            [_btnSelanjutnya setBackgroundColor:UIColorFromRGB(const_color_primary)];
        }
    }
}

- (void) actionOpenURL{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:stringURL];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            [self.btnChecked3 setEnabled:YES];
        }
    }];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [Utility resetTimer];
}

//-(void) setupLayout{
//    CGRect frmVwTitle = self.vwTitle.frame;
//    CGRect frmLblTitle = self.lblTitleMenu.frame;
//    CGRect frmImgStepper = self.imgStepper.frame;
//    CGRect frmVwBtn = self.vwBtn.frame;
//
//    CGRect frmVwScroll = self.vwScroll.frame;
//    CGRect frmVwContentScroll = self.vwContentScroll.frame;
//    CGRect frmVwAkadInfo = self.vwAkadInfo.frame;
//    CGRect frmLblAkadInfo = self.lblAkadInfo.frame;
//    CGRect frmVwAkadBase = self.vwAkadBase.frame;
//    CGRect frmLblAkadKetentuan = self.lblAkadKetentuan.frame;
//    CGRect frmVwAkadAggrement = self.vwAkadAgreement.frame;
//
//    CGRect frmVwRead = self.vwRead.frame;
//    CGRect frmVwTerms = self.vwTermsTitle.frame;
//    CGRect frmVwAggreemenClick = self.vwAgrremenClick.frame;
//    CGRect fmrLblTermsTitle = self.termTitle.frame;
//
//    CGRect frmLblAkadInfoKey = self.lblAkdInfoKey.frame;
//    CGRect frmLblAkadInfoVal = self.lblAkdInfoVal.frame;
//    CGRect frmVwLine = self.vwLine.frame;
//
//    CGRect frmLblAkadTitle = self.lblAkadTitle.frame;
//
//    CGRect frmBtnBatal =  self.btnBatal.frame;
//    CGRect frmBtnSelanjutnya = self.btnSelanjutnya.frame;
//
//    CGRect frmBtnChecked = self.btnChecked.frame;
//    CGRect frmLblChecked = self.lblChecked.frame;
//    CGRect frmBtnBaseChecked = self.btnBaseChecked.frame;
//
//    CGRect frmBtnChecked2 = self.btnChecked2.frame;
//    CGRect frmLblChecked2 = self.lblChecked2.frame;
//    CGRect frmBtnBaseChecked2 = self.btnBaseChecked2.frame;
//
//    CGRect frmBtnChecked3 = self.btnChecked3.frame;
//    CGRect frmLblChecked3 = self.lblChecked3.frame;
//    CGRect frmBtnBaseChecked3 = self.btnBaseChecked3.frame;
//
//    frmVwTitle.origin.x = 0;
//    frmVwTitle.origin.y = TOP_NAV;
//    frmVwTitle.size.width = SCREEN_WIDTH;
//    frmVwTitle.size.height = 50;
//
//    frmLblTitle.origin.x = 8;
//    frmLblTitle.origin.y = 5;
//    frmLblTitle.size.width = frmVwTitle.size.width - 16;
//    frmLblTitle.size.height = 40;
//
//    frmImgStepper.origin.x = 16;
//    frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
//    frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x *2);
//    frmImgStepper.size.height = 30;
//
//    frmVwBtn.origin.x = 0;
//    frmVwBtn.size.height = 48;
//    frmVwBtn.size.width = SCREEN_WIDTH;
//    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 16;
//
//    if ([Utility isDeviceHaveNotch]) {
//        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
//    }
//
//    frmVwScroll.origin.y = frmImgStepper.origin.y + frmImgStepper.size.height + 8;
//    frmVwScroll.origin.x = 0;
//    frmVwScroll.size.width = SCREEN_WIDTH;
//    frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;
//
//    frmVwContentScroll.origin.x = 0;
//    frmVwContentScroll.origin.y = 0;
//    frmVwContentScroll.size.width = SCREEN_WIDTH;
//
//    frmBtnBatal.origin.x = 16;
//    frmBtnBatal.origin.y = 0;
//    frmBtnBatal.size.height = frmVwBtn.size.height;
//    frmBtnBatal.size.width = frmVwBtn.size.width / 2 - (frmBtnBatal.origin.x *2);
//
//    frmBtnSelanjutnya.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
//    frmBtnSelanjutnya.origin.y = 0;
//    frmBtnSelanjutnya.size.width = frmVwBtn.size.width - frmBtnSelanjutnya.origin.x -16;
//    frmBtnSelanjutnya.size.height = frmBtnBatal.size.height;
//
//    frmVwAkadInfo.origin.x = 0;
//    frmVwAkadInfo.origin.y = 0 ;
//    frmVwAkadInfo.size.width = frmVwContentScroll.size.width;
//
//    frmLblAkadInfo.origin.y = frmVwAkadInfo.origin.y + frmVwAkadInfo.size.height + 16;
//    frmLblAkadInfo.origin.x = 16;
//    frmLblAkadInfo.size.height = [Utility heightLabelDynamic:self.lblAkadInfo sizeWidth:frmVwContentScroll.size.width sizeFont:15];
//    frmLblAkadInfo.size.width = frmVwContentScroll.size.width - (frmLblAkadInfo.origin.x * 2);
//
//    //adding point clikable
//    frmVwRead.origin.x = 0;
//    frmVwRead.origin.y = frmLblAkadInfo.origin.y + frmLblAkadInfo.size.height + 16;
//    frmVwRead.size.width = frmVwContentScroll.size.width;
//
//    frmVwTerms.origin.x = 0;
//    frmVwTerms.origin.y = frmVwRead.origin.y + frmVwRead.size.height + 16;
//    frmVwTerms.size.width = frmVwContentScroll.size.width;
//
//    frmVwAggreemenClick.origin.x = 0;
//    frmVwAggreemenClick.origin.y = frmVwTerms.origin.y + frmVwTerms.size.height + 16;
//    frmVwAggreemenClick.size.width = frmVwContentScroll.size.width;
//    //// till here
//
//    frmVwAkadBase.origin.x = 0;
//    frmVwAkadBase.origin.y = frmVwAggreemenClick.origin.y + frmVwAggreemenClick.size.height + 16;
//    frmVwAkadBase.size.width = frmVwContentScroll.size.width;
//
//    frmLblAkadKetentuan.origin.y = frmVwAkadBase.origin.y + frmVwAkadBase.size.height + 16;
//    frmLblAkadKetentuan.origin.x = 16;
//    frmLblAkadKetentuan.size.height = [Utility heightLabelDynamic:self.lblAkadKetentuan sizeWidth:frmVwContentScroll.size.width sizeFont:15];
//    frmLblAkadKetentuan.size.width = frmVwContentScroll.size.width - (frmLblAkadKetentuan.origin.x * 2);
//
//    frmVwAkadAggrement.origin.x = 0;
//    frmVwAkadAggrement.origin.y = frmLblAkadKetentuan.origin.y + frmLblAkadKetentuan.size.height + 16;
//    frmVwAkadAggrement.size.width = frmVwContentScroll.size.width;
//
//    frmLblAkadInfoKey.origin.x = 16;
//    frmLblAkadInfoKey.origin.y = 8;
//    frmLblAkadInfoKey.size.height = [Utility heightLabelDynamic:self.lblAkdInfoKey sizeWidth:frmVwAkadInfo.size.width sizeFont:17];
//    frmLblAkadInfoKey.size.width= frmVwAkadInfo.size.width/2 - (frmLblAkadInfoKey.origin.x * 2);
//
//    frmLblAkadInfoVal.origin.x = frmLblAkadInfoKey.origin.x + frmLblAkadInfoKey.size.width + 24;
//    frmLblAkadInfoVal.origin.y = frmLblAkadInfoKey.origin.y;
//    frmLblAkadInfoVal.size.height = [Utility heightLabelDynamic:self.lblAkdInfoVal sizeWidth:frmVwAkadInfo.size.width sizeFont:17];
//    frmLblAkadInfoVal.size.width = frmVwAkadInfo.size.width - frmLblAkadInfoVal.origin.x - 16;
//
//    frmVwLine.origin.y = frmLblAkadInfoKey.origin.y + frmLblAkadInfoKey.size.height + frmVwLine.size.height + 8;
//    frmVwLine.origin.x = 16;
//    frmVwLine.size.width = frmVwAkadInfo.size.width - (frmVwLine.origin.x * 2);
//
//    frmVwAkadInfo.size.height = frmVwLine.origin.y + frmVwLine.size.height;
//
//    fmrLblTermsTitle.size.width = frmVwTerms.size.width - (fmrLblTermsTitle.origin.x * 2);
//    frmLblAkadTitle.size.width = frmVwAkadBase.size.width - (frmLblAkadTitle.origin.x * 2);
//
//    frmBtnBaseChecked.origin.x = 0;
//    frmBtnBaseChecked.origin.y = 0;
//    frmBtnBaseChecked.size.width = frmVwAkadAggrement.size.width;
//
//    frmBtnChecked.origin.x = 16;
//    frmBtnChecked.origin.y = 8;
//
//    frmLblChecked.origin.y = 8;
//    frmLblChecked.origin.x = frmBtnChecked.origin.x + frmBtnChecked.size.width + 16;
//    frmLblChecked.size.height = [Utility heightLabelDynamic:self.lblChecked sizeWidth:frmVwAkadAggrement.size.width sizeFont:15];
//    frmLblChecked.size.width = frmVwAkadAggrement.size.width - frmLblChecked.origin.x - 16;
//
//    frmVwAkadAggrement.size.height = (frmLblChecked.origin.y *2) + frmLblChecked.size.height;
//    frmBtnBaseChecked.size.height = frmVwAkadAggrement.size.height;
//
//    frmBtnBaseChecked2.origin.x = 0;
//    frmBtnBaseChecked2.origin.y = 0;
//    frmBtnBaseChecked2.size.width = frmVwRead.size.width;
//
//    frmBtnChecked2.origin.x = 16;
//    frmBtnChecked2.origin.y = 8;
//    frmBtnChecked2.size.width = 40;
//    frmBtnChecked2.size.height = 40;
//
//    frmLblChecked2.origin.y = 8;
//    frmLblChecked2.origin.x = frmBtnChecked2.origin.x + frmBtnChecked2.size.width + 16;
//    frmLblChecked2.size.height = [Utility heightLabelDynamic:self.lblChecked2 sizeWidth:frmVwRead.size.width sizeFont:15];
//    frmLblChecked2.size.width = frmVwRead.size.width - frmLblChecked2.origin.x - 32;
//
//    frmVwRead.size.height = (frmLblChecked2.origin.y *2) + frmLblChecked2.size.height;
//    frmBtnChecked2.size.height = frmVwRead.size.height;
//
//    frmBtnChecked3.origin.x = 16;
//    frmBtnChecked3.origin.y = 8;
//
//    frmLblChecked3.origin.y = 8;
//    frmLblChecked3.origin.x = frmBtnChecked3.origin.x + frmBtnChecked3.size.width + 16;
//    frmLblChecked3.size.height = [Utility heightLabelDynamic:self.lblChecked3 sizeWidth:frmVwAggreemenClick.size.width sizeFont:15];
//    frmLblChecked3.size.width = frmVwAggreemenClick.size.width - (frmLblChecked3.origin.x - 16);
//
//    frmBtnBaseChecked3.origin.x = frmLblChecked3.origin.x;
//    frmBtnBaseChecked3.origin.y = 0;
//    frmBtnBaseChecked3.size.width = frmLblChecked3.size.width;
//
//    frmVwAggreemenClick.size.height = (frmLblChecked3.origin.y *2) + frmLblChecked3.size.height;
//    frmBtnChecked3.size.height = frmVwAggreemenClick.size.height;
//
//
//    frmVwContentScroll.size.height = frmLblChecked.origin.y + frmLblChecked.size.height + 16;
//    if (isAggre) {
//         frmVwContentScroll.size.height = frmVwAkadAggrement.origin.y + frmVwAkadAggrement.size.height + 16;
//    }
//
//
//    self.vwTitle.frame = frmVwTitle;
//    self.imgStepper.frame = frmImgStepper;
//    self.vwBtn.frame = frmVwBtn;
//
//    UIView* viewStepper = [[UIView alloc]initWithFrame:frmImgStepper];
//    [Styles createProgress:viewStepper step:3 from:4];
//
//    [self.imgStepper setHidden:YES];
//    [self.view addSubview:viewStepper];
//
//    self.vwScroll.frame = frmVwScroll;
//    self.vwContentScroll.frame = frmVwContentScroll;
//    self.vwAkadInfo.frame = frmVwAkadInfo;
//    self.lblAkadInfo.frame = frmLblAkadInfo;
//    self.vwAkadBase.frame = frmVwAkadBase;
//    self.lblAkadKetentuan.frame = frmLblAkadKetentuan;
//    self.vwAkadAgreement.frame = frmVwAkadAggrement;
//    self.vwAgrremenClick.frame = frmVwAggreemenClick;
//    self.vwRead.frame = frmVwRead;
//    self.vwTermsTitle.frame = frmVwTerms;
//    self.termTitle.frame = fmrLblTermsTitle;
//
//    self.lblAkdInfoKey.frame = frmLblAkadInfoKey;
//    self.lblAkdInfoVal.frame = frmLblAkadInfoVal;
//    self.vwLine.frame = frmVwLine;
//    self.lblAkadTitle.frame = frmLblAkadTitle;
//
//    self.btnBatal.frame = frmBtnBatal;
//    self.btnSelanjutnya.frame = frmBtnSelanjutnya;
//
//    self.btnChecked.frame = frmBtnChecked;
//    self.lblChecked.frame = frmLblChecked;
//    [self.lblChecked sizeToFit];
//    self.btnBaseChecked.frame = frmBtnBaseChecked;
//
//    self.btnChecked2.frame = frmBtnChecked2;
//    self.lblChecked2.frame = frmLblChecked2;
//    [self.lblChecked2 sizeToFit];
//    self.btnBaseChecked2.frame = frmBtnBaseChecked2;
//
//    self.btnChecked3.frame = frmBtnChecked3;
//    self.lblChecked3.frame = frmLblChecked3;
//    [self.lblChecked3 sizeToFit];
//    self.btnBaseChecked3.frame = frmBtnBaseChecked3;
//
//    self.vwAkadBase.hidden = YES;
//    self.vwAkadInfo.hidden = YES;
//    self.vwAkadAgreement.hidden = YES;
//}


- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    [Utility stopTimer];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject objectForKey:@"response"];
                NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                if (dataDict) {
                    NSLog(@"%@", dataDict);
                    nMartialState = [[dataDict valueForKey:@"Marital_Status_Code"]intValue];

                    NSArray *agrement = [[dataDict valueForKey:@"agreement"] componentsSeparatedByString:@"|"];
                    if(agrement.count > 3){
                        stringURL = agrement[2];

                        [self.lblChecked setText:agrement[3]];
                        [self.lblChecked2 setText:agrement[0]];
                        [self.lblChecked3 setText:agrement[1]];
                        [self.lblChecked3 setNumberOfLines:3];
                    }
                     
                    dataContent = [NSJSONSerialization JSONObjectWithData:[[dataDict objectForKey:@"content"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    
                    NSString *strHtmlAkad = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue; font-size: 13\">%@</span>",[dataDict valueForKey:@"akad"]];
                    NSString *strHtmlAkadInfo = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue; font-size: 13\">%@</span>",[dataDict valueForKey:@"info"]];
                    
                    NSLog(@"%@", strHtmlAkad);
                    
                    NSArray *arrHtmlAkad = [strHtmlAkad componentsSeparatedByString:@"<footer>"];
                    
                    [dataManager.dataExtra setValue:strHtmlAkad forKey:@"str_akad_html"];
                    [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
                    [userDefault setValue:[jsonObject objectForKey:@"transaction_id"]  forKey:@"trxidcashfin"];
                    [userDefault setValue:[jsonObject objectForKey:@"transaction_id"]  forKey:@"transaction_id"];

                    
                    self.lblAkadInfo.attributedText = [Utility htmlAtributString:strHtmlAkadInfo];
                    if([lang(@"LANGUAGE")isEqualToString:@"ID"]){
                        [self.lblAkadTitle setText:@"Akad Pembiayaan"];
                        [self.termTitle setText:@"Syarat Umum Pembiayaan"];
                        
                    }else{
                        [self.lblAkadTitle setText:@"Financing Agreements"];
                        [self.termTitle setText:@"General Terms of Financing"];
                    }
                    
                    self.lblAkadInfo.numberOfLines = 0;
                    [self.lblAkadInfo sizeToFit];
                    
                    self.lblAkadKetentuan.attributedText = [Utility htmlAtributString:[arrHtmlAkad objectAtIndex:0]];
                    self.lblAkadKetentuan.numberOfLines = 0;
                    [self.lblAkadKetentuan sizeToFit];
                    
                    [self clearWhiteView];
                    [self setupNewLayoutPhase3a];
                    
                    NSLog(@"data view : %@", self.vwContentScroll.subviews);
                }
            }
        }else{
            [Utility showMessage:[jsonObject objectForKey:@"response"] mVwController:self mTag:[[jsonObject objectForKey:@"response_code"]intValue]];
//            [self setDummy];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    [Utility showMessage:error.localizedDescription mVwController:self mTag:99];
}

- (void)reloadApp{
    [self backToRoot];
}

- (IBAction)actionBatal:(id)sender {
    if(stepPhase == 2){
        [self showPhase:@"3b"];
        [self setupNewLayoutPhase3b];
        [self.vwScroll setContentOffset:CGPointMake(0, 0)];
        stepPhase = 1;
        if ([lang isEqualToString:@"id"]) {
            [self.btnSelanjutnya setTitle:@"MENYETUJUI AKAD" forState:UIControlStateNormal];
        }else{
            [self.btnSelanjutnya setTitle:@"ACCEPT" forState:UIControlStateNormal];
//            [self.btnSelanjutnya setTitle:@"AGREE TO AKAD" forState:UIControlStateNormal];
        }
        [_btnSelanjutnya setEnabled:true];
        [_btnSelanjutnya setBackgroundColor:UIColorFromRGB(const_color_primary)];
        
        for(UIView *view in self.vwContentScroll.subviews){
            if([view.accessibilityLabel isEqualToString:@"contents_key"]){
                [view removeFromSuperview];
            }
        }
        
    }else if(stepPhase == 1){
        [self.vwScroll setContentOffset:CGPointMake(0, 0)];
        [self setupNewLayoutPhase3a];
        [self showPhase:@"3a"];
        stepPhase = 0;
        if ([lang isEqualToString:@"id"]) {
            [self.btnSelanjutnya setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
        }else{
            [self.btnSelanjutnya setTitle:@"NEXT" forState:UIControlStateNormal];
        }
        [_btnSelanjutnya setEnabled:true];
        [_btnSelanjutnya setBackgroundColor:UIColorFromRGB(const_color_primary)];
    }else if(stepPhase == 0){
        dataManager.currentPosition --;
        UINavigationController *navigationCont = self.navigationController;
        [navigationCont popViewControllerAnimated:YES];
    }
}

- (IBAction)actionSelanjutnya:(id)sender {
    if(stepPhase == 0){
        [self showPhase:@"3b"];
        [self setupNewLayoutPhase3b];
        [self.vwScroll setContentOffset:CGPointMake(0, 0)];
        stepPhase = 1;
        if ([lang isEqualToString:@"id"]) {
            [self.btnSelanjutnya setTitle:@"MENYETUJUI AKAD" forState:UIControlStateNormal];
        }else{
            [self.btnSelanjutnya setTitle:@"ACCEPT" forState:UIControlStateNormal];
//            [self.btnSelanjutnya setTitle:@"AGREE TO AKAD" forState:UIControlStateNormal];
        }
        [_btnSelanjutnya setEnabled:false];
        [_btnSelanjutnya setBackgroundColor:[UIColor grayColor]];
    }else if(stepPhase == 1){
        [self.vwScroll setContentOffset:CGPointMake(0, 0)];
        [self setupNewLayoutPhase4];
        [self showPhase:@"4"];
        stepPhase = 2;
        if ([lang isEqualToString:@"id"]) {
            [self.btnSelanjutnya setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
        }else{
            [self.btnSelanjutnya setTitle:@"NEXT" forState:UIControlStateNormal];
        }
    }else if(stepPhase == 2){
        [self openNextTemplate];
    }
}

-(UIView *) addContentView : (NSString *) strKey
              mStrValue: (NSString *) strVal
                   mTag: (int) nTag{
    UIView *vwBase = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.vwContentScroll.frame.size.width, 57)];
    vwBase.tag = nTag;
    
    UILabel *lblKey = [[UILabel alloc]initWithFrame:CGRectMake(16, 8, self.vwContentScroll.frame.size.width/2 - 32, 24)];
    [lblKey setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
    [lblKey setText:strKey];
    lblKey.numberOfLines = 2;
    [lblKey sizeToFit];
    lblKey.lineBreakMode = NSLineBreakByWordWrapping;
    lblKey.tag = nTag;
    
    UILabel *lblVal = [[UILabel alloc]initWithFrame:CGRectMake(lblKey.frame.origin.x + lblKey.frame.size.width + 24, 8, self.vwContentScroll.frame.size.width/2 - 32, 24)];
    [lblVal setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
    [lblVal setTextAlignment:NSTextAlignmentRight];
    [lblVal setText:strVal];
    lblVal.numberOfLines = 2;
    [lblVal sizeToFit];
    lblVal.lineBreakMode = NSLineBreakByWordWrapping;
    lblVal.tag = nTag;
    
    UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(16, 0, self.vwContentScroll.frame.size.width - 32, 1)];
    [vwLine setBackgroundColor:[UIColor grayColor]];
    vwLine.tag = nTag;
    
    if (IPHONE_5) {
         [lblKey setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
         [lblVal setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    }
    
    CGRect frmLblKey = lblKey.frame;
    CGRect frmLblVal = lblVal.frame;
    CGRect frmVwLine = vwLine.frame;
    CGRect frmVwBase = vwBase.frame;
    
    frmLblKey.origin.x = 16;
    frmLblKey.origin.y = 8;
    if (IPHONE_5) {
        frmLblKey.size.height = [Utility heightLabelDynamic:lblKey sizeWidth:(vwBase.frame.size.width/2) sizeFont:14];
    }
    frmLblKey.size.width= self.vwContentScroll.frame.size.width/2 - (frmLblKey.origin.x * 2);
    
    frmLblVal.origin.y = frmLblKey.origin.y;
    frmLblVal.origin.x = frmLblKey.origin.x + frmLblKey.size.width + 24;
    frmLblVal.size.height = [Utility heightLabelDynamic:lblVal sizeWidth:(vwBase.frame.size.width/2) sizeFont:16];
    frmLblKey.size.height = frmLblVal.size.height;
    frmLblVal.size.width = self.vwContentScroll.frame.size.width - frmLblVal.origin.x - 16;
    
    frmVwLine.origin.y = frmLblKey.origin.y + frmLblKey.size.height + frmVwLine.size.height + 8;
    
    frmVwBase.size.height = frmVwLine.origin.y + frmVwLine.size.height;
    
    lblKey.frame = frmLblKey;
    lblVal.frame = frmLblVal;
    vwLine.frame = frmVwLine;
    vwBase.frame = frmVwBase;
    
    [vwBase addSubview:lblKey];
    [vwBase addSubview:lblVal];
    [vwBase addSubview: vwLine];
    
    
    return vwBase;
    
}

-(void)relocatePosition{
    
    [self.vwContentScroll setHidden:NO];
    
    CGFloat currentPostY = self.vwAkadInfo.frame.origin.y + self.vwAkadInfo.frame.size.height + 8;
    for(int x = 1; x< dataContent.count; x++ ){
        UIView *vwLastContent = [self.vwContentScroll.subviews objectAtIndex:x-1];
        UIView *vwContent = [self.vwContentScroll.subviews objectAtIndex:x];
        
        if (x > 1) {
            currentPostY = currentPostY + vwContent.frame.size.height + 8;
        }
        
        CGRect frmVwLasContent = vwLastContent.frame;
        CGRect frmVwContent = vwContent.frame;
        frmVwContent.origin.y = frmVwLasContent.origin.y + frmVwLasContent.size.height + 8;
        vwContent.frame = frmVwContent;
    }
    CGRect frmLblAkadInfo = self.lblAkadInfo.frame;
    CGRect frmVwRead = self.vwRead.frame;
    CGRect frmVwAgreementClick = self.vwAgrremenClick.frame;
    CGRect frmLblChecked2 = self.lblChecked2.frame;
    CGRect frmBtnChecked2 = self.btnChecked2.frame;
    CGRect frmBtnBaseChecked2 = self.btnBaseChecked2.frame;
    CGRect frmLblChecked3 = self.lblChecked3.frame;
    CGRect frmBtnChecked3 = self.btnChecked3.frame;
    CGRect frmBtnBaseChecked3 = self.btnBaseChecked3.frame;
    
    CGRect frmVwTerms = self.vwTermsTitle.frame;
    CGRect frmVwAkadBase = self.vwAkadBase.frame;
    CGRect frmLblKetentuan = self.lblAkadKetentuan.frame;
    CGRect frmVwChecked = self.vwAkadAgreement.frame;
    CGRect frmLblChecked = self.lblChecked.frame;
    CGRect frmBtnChecked = self.btnChecked.frame;
    CGRect frmBtnBaseChecked = self.btnBaseChecked.frame;
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    
//    frmLblAkadInfo.size.height = [Utility heightLabelDynamic:self.lblAkadInfo sizeWidth:self.vwContentScroll.frame.size.width - (self.lblAkadInfo.frame.origin.x *2) sizeFont:16];
    frmLblAkadInfo.size.width = self.vwContentScroll.frame.size.width - (frmLblAkadInfo.origin.x *2);
    frmLblAkadInfo.origin.y = currentPostY + 32 + 32;
    
    frmVwRead.origin.y = frmLblAkadInfo.origin.y + frmLblAkadInfo.size.height + 16;
    frmVwAgreementClick.origin.y = frmVwRead.origin.y + frmVwRead.size.height + 16;
    ///////////
    frmLblChecked2.origin.y = 0;
    frmLblChecked2.size.height = [Utility heightLabelDynamic:self.lblChecked2 sizeWidth:frmVwRead.size.width - (frmLblChecked2.origin.x + 16) sizeFont:16];
    
    frmBtnChecked2.origin.y = frmLblChecked2.size.height/2 - frmBtnChecked2.size.height/2;
    
    frmVwRead.size.height = frmLblChecked2.origin.y + frmLblChecked2.size.height;
    
    frmBtnBaseChecked2.origin.x = 0;
    frmBtnBaseChecked2.origin.y = 0;
    frmBtnBaseChecked2.size.width = frmVwRead.size.width;
    frmBtnBaseChecked2.size.height = frmVwRead.size.height;
    
    frmLblChecked3.origin.y = 0;
//    frmLblChecked3.size.height = [Utility heightLabelDynamic:self.lblChecked3 sizeWidth:frmVwAgreementClick.size.width - (frmLblChecked3.origin.x + 16) sizeFont:16];
    frmLblChecked3.size.height = 30;

    frmBtnChecked3.origin.y = frmLblChecked3.size.height/2 - frmBtnChecked3.size.height/2;
    
    frmVwTerms.origin.y = frmVwRead.origin.y + frmVwRead.size.height + 16;
    
    frmVwAgreementClick.origin.y = frmVwTerms.origin.y + frmVwTerms.size.height + 16;
    frmVwAgreementClick.size.height = frmLblChecked3.origin.y + frmLblChecked3.size.height;
    
    frmBtnBaseChecked3.origin.y = 0;
    frmBtnBaseChecked3.size.height = frmVwAgreementClick.size.height;
    /////////////////
    frmVwAkadBase.origin.y = frmVwAgreementClick.origin.y + frmVwAgreementClick.size.height + 32;
    
//    frmLblKetentuan.size.height = [Utility heightLabelDynamic:self.lblAkadKetentuan sizeWidth:self.vwContentScroll.frame.size.width - (self.lblAkadKetentuan.frame.origin.x *2) sizeFont:18];
    frmLblKetentuan.size.width = self.vwContentScroll.frame.size.width - (frmLblKetentuan.origin.x *2);
    frmLblKetentuan.origin.y = frmVwAkadBase.origin.y + frmVwAkadBase.size.height + 16;
    
    frmVwChecked.origin.y = frmLblKetentuan.origin.y + frmLblKetentuan.size.height + 16;

    frmLblChecked.origin.y = 0;
    frmLblChecked.size.height = [Utility heightLabelDynamic:self.lblChecked sizeWidth:frmVwChecked.size.width - (frmLblChecked.origin.x + 16) sizeFont:16];
    frmLblChecked.size.width = frmVwChecked.size.width - (frmLblChecked.origin.x + 16);
    
    frmBtnChecked.origin.y = frmLblChecked.size.height/2 - frmBtnChecked.size.height/2;
    
    frmVwChecked.size.height = frmLblChecked.origin.y + frmLblChecked.size.height;
    
    frmBtnBaseChecked.origin.x = 0;
    frmBtnBaseChecked.origin.y = 0;
    frmBtnBaseChecked.size.width = frmVwChecked.size.width;
    frmBtnBaseChecked.size.height = frmVwChecked.size.height;
    
    self.lblAkadInfo.frame  = frmLblAkadInfo;
    
    self.lblChecked3.frame = frmLblChecked3;
    self.btnChecked3.frame = frmBtnChecked3;
    self.btnBaseChecked3.frame = frmBtnBaseChecked3;
    
    self.lblChecked2.frame = frmLblChecked2;
    self.btnChecked2.frame = frmBtnChecked2;
    self.btnBaseChecked2.frame = frmBtnBaseChecked2;
    
    self.vwRead.frame = frmVwRead;
    self.vwTermsTitle.frame = frmVwTerms;
    self.vwAgrremenClick.frame = frmVwAgreementClick;
    
    self.vwAkadBase.frame = frmVwAkadBase;
    self.lblAkadKetentuan.frame =  frmLblKetentuan;
    self.vwAkadAgreement.frame = frmVwChecked;
    self.lblChecked.frame = frmLblChecked;
    self.btnChecked.frame = frmBtnChecked;
    
    self.btnBaseChecked.frame = frmBtnBaseChecked;
    self.vwContentScroll.frame = frmVwContentScroll;
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];

}

-(void)actionAgreeAkad : (UIButton *) sender{
    if(sender == self.btnBaseChecked){
        [self.btnChecked setSelected:!self.btnChecked.isSelected];
    }else if(sender == self.btnBaseChecked2){
        [self.btnChecked2 setSelected:!self.btnChecked2.isSelected];
    }else if(sender == self.btnChecked3){
        [self.btnChecked3 setSelected:!self.btnChecked3.isSelected];
    }
    
    if(stepPhase == 0){
        if (self.btnChecked3.isSelected && self.btnChecked2.isSelected) {
            [_btnSelanjutnya setEnabled:true];
            [_btnSelanjutnya setBackgroundColor:UIColorFromRGB(const_color_primary)];
        }else{
            [_btnSelanjutnya setEnabled:false];
            [_btnSelanjutnya setBackgroundColor:[UIColor grayColor]];
        }
    }else if(stepPhase == 1){
        if (self.btnChecked3.isSelected && self.btnChecked2.isSelected && self.btnChecked.isSelected) {
            [_btnSelanjutnya setEnabled:true];
            [_btnSelanjutnya setBackgroundColor:UIColorFromRGB(const_color_primary)];
        }else{
            [_btnSelanjutnya setEnabled:false];
            [_btnSelanjutnya setBackgroundColor:[UIColor grayColor]];
        }
    }
}

-(void)showPopupPasangan : (UIButton *) sender{
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"POPISIPAS"];
    PopupIsiPasanganViewController *viewCont = (PopupIsiPasanganViewController *) templateView;
    [viewCont setMartialState:nMartialState];
    [self presentViewController:templateView animated:YES completion:nil];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 05) {
        NSLog(@"Field Empty");
    }else{
        [self backToRoot];
    }
    
}

- (void) setupNewLayoutPhase4{
    [self.vwContentScroll setHidden:NO];

    if([lang isEqualToString:@"id"]){
        [self.lblTitleMenu setText:@"Konfirmasi Pengajuan"];
    }else{
        [self.lblTitleMenu setText:@"Financing Confirmation"];
    }
    
    for(UIView *view in self.vwContentScroll.subviews){
        if([view.accessibilityLabel isEqualToString:@"contents_key"]){
            [view removeFromSuperview];
        }
    }
    
    for (int i=0; i<dataContent.count; i++) {
        NSDictionary *content = [dataContent objectAtIndex:i];
        if (i == 0) {
            [self.lblAkdInfoKey setText:[content valueForKey:@"key"]];
            [self.lblAkdInfoVal setText:[content valueForKey:@"value"]];
        }else{
            UIView *vwDynamicContents = [self addContentView:[content valueForKey:@"key"] mStrValue:[content valueForKey:@"value"] mTag:i];
            vwDynamicContents.accessibilityLabel = @"contents_key";
            [self.vwContentScroll insertSubview:vwDynamicContents atIndex:i];
        }
    }
    CGRect frmLblAkadInfoKey = self.lblAkdInfoKey.frame;
    CGRect frmLblAkadInfoVal = self.lblAkdInfoVal.frame;
    CGRect frmVwLine = self.vwLine.frame;
    CGRect frmVwAkadInfo = self.vwAkadInfo.frame;
    
    frmVwAkadInfo.origin.x = 0;
    frmVwAkadInfo.origin.y = 0 ;
    frmVwAkadInfo.size.width = self.vwContentScroll.frame.size.width;
    
    frmLblAkadInfoKey.origin.x = 16;
    frmLblAkadInfoKey.origin.y = 8;
    frmLblAkadInfoKey.size.height = [Utility heightLabelDynamic:self.lblAkdInfoKey sizeWidth:frmVwAkadInfo.size.width sizeFont:17];
    frmLblAkadInfoKey.size.width= frmVwAkadInfo.size.width/2 - (frmLblAkadInfoKey.origin.x * 2);
    
    frmLblAkadInfoVal.origin.x = frmLblAkadInfoKey.origin.x + frmLblAkadInfoKey.size.width + 24;
    frmLblAkadInfoVal.origin.y = frmLblAkadInfoKey.origin.y;
    frmLblAkadInfoVal.size.height = [Utility heightLabelDynamic:self.lblAkdInfoVal sizeWidth:frmVwAkadInfo.size.width sizeFont:17];
    frmLblAkadInfoVal.size.width = frmVwAkadInfo.size.width - frmLblAkadInfoVal.origin.x - 16;
    
    frmVwLine.origin.y = frmLblAkadInfoKey.origin.y + frmLblAkadInfoKey.size.height + frmVwLine.size.height + 8;
    frmVwLine.origin.x = 16;
    frmVwLine.size.width = frmVwAkadInfo.size.width - (frmVwLine.origin.x * 2);
    
    frmVwAkadInfo.size.height = frmVwLine.origin.y + frmVwLine.size.height;
    
    CGFloat currentPostY = self.vwAkadInfo.frame.origin.y + self.vwAkadInfo.frame.size.height + 8;
    for(int x = 1; x< dataContent.count; x++ ){
        UIView *vwLastContent = [self.vwContentScroll.subviews objectAtIndex:x-1];
        UIView *vwContent = [self.vwContentScroll.subviews objectAtIndex:x];
        
        if (x > 1) {
            currentPostY = currentPostY + vwContent.frame.size.height + 8;
        }
        
        CGRect frmVwLasContent = vwLastContent.frame;
        CGRect frmVwContent = vwContent.frame;
        frmVwContent.origin.y = frmVwLasContent.origin.y + frmVwLasContent.size.height + 8;
        vwContent.frame = frmVwContent;
    }
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmImgStepper = self.imgStepper.frame;
    CGRect frmVwBtn = self.vwBtn.frame;
    
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmImgStepper.origin.x = 16;
    frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
    frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x * 2);
    frmImgStepper.size.height = 30;
    
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.height = 48;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 16;
    
    if (IPHONE_X || IPHONE_XS_MAX) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
    }
    
    frmVwScroll.origin.y = frmImgStepper.origin.y + frmImgStepper.size.height + 8;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;

    frmVwContentScroll.size.height = currentPostY + 100;
    
    self.vwTitle.frame = frmVwTitle;
    self.imgStepper.frame = frmImgStepper;
    self.vwBtn.frame = frmVwBtn;
    self.vwAkadInfo.frame = frmVwAkadInfo;
    self.lblAkdInfoKey.frame = frmLblAkadInfoKey;
    self.lblAkdInfoVal.frame = frmLblAkadInfoVal;
    self.vwLine.frame = frmVwLine;
    
    UIView* viewStepper = [[UIView alloc]initWithFrame:frmImgStepper];
    [Styles createProgress:viewStepper step:4 from:4];
    [self.imgStepper setHidden:YES];
    [self.view addSubview:viewStepper];
    
    self.vwScroll.frame = frmVwScroll;

    self.vwContentScroll.frame = frmVwContentScroll;
    
    [self showPhase:@"4"];
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
}

- (void) setupNewLayoutPhase3a{
    [self.vwContentScroll setHidden:NO];
    
//    if([lang isEqualToString:@"id"]){
//        [self.lblTitleMenu setText:@"Persetujuan Akad"];
//    }else{
//        [self.lblTitleMenu setText:@"Contract Agreement"];
//    }
    
    if([lang isEqualToString:@"id"]){
        [self.lblTitleMenu setText:@"Persetujuan Pengajuan"];
    }else{
        [self.lblTitleMenu setText:@"Application Statements"];
    }
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmImgStepper = self.imgStepper.frame;
    CGRect frmVwBtn = self.vwBtn.frame;
    
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    CGRect frmLblAkadInfo = self.lblAkadInfo.frame;
    
    CGRect frmVwRead = self.vwRead.frame;
    CGRect frmVwTerms = self.vwTermsTitle.frame;
    CGRect frmVwAgreementClick = self.vwAgrremenClick.frame;
    CGRect frmLblTermsTitle = self.termTitle.frame;
    
    CGRect frmBtnChecked2 = self.btnChecked2.frame;
    CGRect frmLblChecked2 = self.lblChecked2.frame;
    CGRect frmBtnBaseChecked2 = self.btnBaseChecked2.frame;
    
    CGRect frmBtnChecked3 = self.btnChecked3.frame;
    CGRect frmLblChecked3 = self.lblChecked3.frame;
    CGRect frmBtnBaseChecked3 = self.btnBaseChecked3.frame;
    
    CGRect frmBtnBatal =  self.btnBatal.frame;
    CGRect frmBtnSelanjutnya = self.btnSelanjutnya.frame;
    
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmImgStepper.origin.x = 16;
    frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
    frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x * 2);
    frmImgStepper.size.height = 30;
    
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.height = 48;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 16;
    
    if (IPHONE_X || IPHONE_XS_MAX) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
    }
    
    frmVwScroll.origin.y = frmImgStepper.origin.y + frmImgStepper.size.height + 8;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;
    
    frmLblAkadInfo.origin.y = 0;
    frmLblAkadInfo.origin.x = 16;
    frmLblAkadInfo.size.height = [Utility heightLabelDynamic:self.lblAkadInfo sizeWidth:frmVwContentScroll.size.width sizeFont:15];
    frmLblAkadInfo.size.width = frmVwContentScroll.size.width - (frmLblAkadInfo.origin.x * 2);
    
    frmVwRead.origin.x = 0;
    frmVwRead.origin.y = frmLblAkadInfo.origin.y + frmLblAkadInfo.size.height + 16;
    frmVwRead.size.width = frmVwContentScroll.size.width;
    
    frmVwTerms.origin.x = 0;
    frmVwTerms.origin.y = frmVwRead.origin.y + frmVwRead.size.height + 16;
    frmVwTerms.size.width = frmVwContentScroll.size.width;
    
    frmLblTermsTitle.size.width = frmVwTerms.size.width - (frmLblTermsTitle.origin.x * 2);
    
    frmVwAgreementClick.origin.x = 0;
    frmVwAgreementClick.origin.y = frmVwTerms.origin.y + frmVwTerms.size.height + 16;
    frmVwAgreementClick.size.width = frmVwContentScroll.size.width;
    
    frmBtnBaseChecked2.origin.x = 0;
    frmBtnBaseChecked2.origin.y = 0;
    frmBtnBaseChecked2.size.width = frmVwRead.size.width;
    
    frmBtnChecked2.origin.x = 16;
    frmBtnChecked2.origin.y = 8;
    frmBtnChecked2.size.width = 40;
    frmBtnChecked2.size.height = 40;
    
    frmLblChecked2.origin.y = 8;
    frmLblChecked2.origin.x = frmBtnChecked2.origin.x + frmBtnChecked2.size.width + 16;
//    frmLblChecked2.size.height = [Utility heightLabelDynamic:self.lblChecked2 sizeWidth:frmVwRead.size.width sizeFont:15];
    frmLblChecked2.size.width = frmVwRead.size.width - frmLblChecked2.origin.x - 32;
    
    frmVwRead.size.height = (frmLblChecked2.origin.y *2) + frmLblChecked2.size.height + 40;
    
    frmBtnChecked3.origin.x = 16;
    frmBtnChecked3.origin.y = 8;
    frmBtnChecked3.size.width = 40;
    frmBtnChecked3.size.height = 40;
    
    frmLblChecked3.origin.y = 8;
    frmLblChecked3.origin.x = frmBtnChecked3.origin.x + frmBtnChecked3.size.width + 16;
//    frmLblChecked3.size.height = [Utility heightLabelDynamic:self.lblChecked3 sizeWidth:frmVwAgreementClick.size.width sizeFont:15];
//    frmLblChecked3.size.height = 100;
    frmLblChecked3.size.width = frmVwAgreementClick.size.width - frmLblChecked3.origin.x - 32;
    
    frmBtnBaseChecked3.origin.x = frmLblChecked3.origin.x;
    frmBtnBaseChecked3.origin.y = 0;
    frmBtnBaseChecked3.size.width = frmLblChecked3.size.width;
    frmBtnBaseChecked3.size.height = frmLblChecked3.size.height + 30;
    
    frmVwAgreementClick.size.height = (frmLblChecked3.origin.y *2) + frmLblChecked3.size.height + 50;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = SCREEN_WIDTH;
    
    frmVwContentScroll.size.height = frmVwAgreementClick.origin.y + frmVwAgreementClick.size.height + 16;
    
    frmBtnBatal.origin.x = 16;
    frmBtnBatal.origin.y = 0;
    frmBtnBatal.size.height = frmVwBtn.size.height;
    frmBtnBatal.size.width = frmVwBtn.size.width / 2 - (frmBtnBatal.origin.x *2);
    
    frmBtnSelanjutnya.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
    frmBtnSelanjutnya.origin.y = 0;
    frmBtnSelanjutnya.size.width = frmVwBtn.size.width - frmBtnSelanjutnya.origin.x -16;
    frmBtnSelanjutnya.size.height = frmBtnBatal.size.height;
    
    self.vwTitle.frame = frmVwTitle;
    self.imgStepper.frame = frmImgStepper;
    self.vwBtn.frame = frmVwBtn;
    
    UIView* viewStepper = [[UIView alloc]initWithFrame:frmImgStepper];
    [Styles createProgress:viewStepper step:3 from:4];
    [self.imgStepper setHidden:YES];
    [self.view addSubview:viewStepper];
    
    self.vwScroll.frame = frmVwScroll;
    self.vwContentScroll.frame = frmVwContentScroll;
    self.lblAkadInfo.frame = frmLblAkadInfo;

    self.vwRead.frame = frmVwRead;
    self.vwTermsTitle.frame = frmVwTerms;
    self.vwAgrremenClick.frame = frmVwAgreementClick;
    self.termTitle.frame = frmLblTermsTitle;
    
    self.btnChecked2.frame = frmBtnChecked2;
    self.lblChecked2.frame = frmLblChecked2;
    [self.lblChecked2 setNumberOfLines:0];
    [self.lblChecked2 sizeToFit];
    [self.lblChecked2 setLineBreakMode:NSLineBreakByWordWrapping];
    self.btnBaseChecked2.frame = frmBtnBaseChecked2;
    
    self.btnChecked3.frame = frmBtnChecked3;
    self.lblChecked3.frame = frmLblChecked3;
    [self.lblChecked3 setNumberOfLines:0];
    [self.lblChecked3 sizeToFit];
    [self.lblChecked3 setLineBreakMode:NSLineBreakByWordWrapping];
    
    self.btnBaseChecked3.frame = frmBtnBaseChecked3;
    
    self.btnBatal.frame = frmBtnBatal;
    self.btnSelanjutnya.frame = frmBtnSelanjutnya;
        
    [self showPhase:@"3a"];

    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    
}

- (void) setupNewLayoutPhase3b{
    [self.vwContentScroll setHidden:NO];
    
    if([lang isEqualToString:@"id"]){
        [self.lblTitleMenu setText:@"Persetujuan Pengajuan"];
    }else{
        [self.lblTitleMenu setText:@"Financing Agreement"];
    }
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmImgStepper = self.imgStepper.frame;
    CGRect frmVwBtn = self.vwBtn.frame;
    
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    CGRect frmLblAkadKetentuan = self.lblAkadKetentuan.frame;
    
    CGRect frmVwChecked = self.vwAkadAgreement.frame;
    CGRect frmLblChecked = self.lblChecked.frame;
    CGRect frmBtnChecked = self.btnChecked.frame;
    CGRect frmBtnBaseChecked = self.btnBaseChecked.frame;
    
    CGRect frmBtnBatal =  self.btnBatal.frame;
    CGRect frmBtnSelanjutnya = self.btnSelanjutnya.frame;
    
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmImgStepper.origin.x = 16;
    frmImgStepper.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 5;
    frmImgStepper.size.width = SCREEN_WIDTH - (frmImgStepper.origin.x * 2);
    frmImgStepper.size.height = 30;
    
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.height = 48;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 16;
    
    if (IPHONE_X || IPHONE_XS_MAX) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
    }
    
    frmVwScroll.origin.y = frmImgStepper.origin.y + frmImgStepper.size.height + 8;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = SCREEN_WIDTH;
    
    frmLblAkadKetentuan.origin.y = 16;
    frmLblAkadKetentuan.origin.x = 16;
    frmLblAkadKetentuan.size.width = frmVwContentScroll.size.width - (frmLblAkadKetentuan.origin.x * 2);
    
    self.vwTitle.frame = frmVwTitle;
    self.imgStepper.frame = frmImgStepper;
    self.vwBtn.frame = frmVwBtn;
    UIView* viewStepper = [[UIView alloc]initWithFrame:frmImgStepper];
    [Styles createProgress:viewStepper step:3 from:4];
    [self.imgStepper setHidden:YES];
    [self.view addSubview:viewStepper];
    self.lblAkadKetentuan.frame = frmLblAkadKetentuan;
    self.lblAkadKetentuan.numberOfLines = 0;
    [self.lblAkadKetentuan sizeToFit];
    
    frmVwChecked.origin.x = 0;
    frmVwChecked.origin.y = self.lblAkadKetentuan.frame.size.height + 16;
    frmVwChecked.size.width = self.vwContentScroll.frame.size.width;

    frmBtnChecked.origin.x = 16;
    frmBtnChecked.origin.y = 8;
    frmBtnChecked.size.width = 40;
    frmBtnChecked.size.height = 40;
    
    frmLblChecked.origin.x = frmBtnChecked.origin.x + frmBtnChecked.size.width + 8;
    frmLblChecked.origin.y = 0;
    frmLblChecked.size.height = [Utility heightLabelDynamic:self.lblChecked sizeWidth:frmVwChecked.size.width - (frmLblChecked.origin.x + 16) sizeFont:16];
    frmLblChecked.size.width = frmVwChecked.size.width - (frmLblChecked.origin.x + 16);

    frmBtnChecked.origin.y = frmLblChecked.size.height/2 - frmBtnChecked.size.height/2;

    frmVwChecked.size.height = frmLblChecked.origin.y + frmLblChecked.size.height;

    frmBtnBaseChecked.origin.x = 0;
    frmBtnBaseChecked.origin.y = 0;
    frmBtnBaseChecked.size.width = frmVwChecked.size.width;
    frmBtnBaseChecked.size.height = frmVwChecked.size.height;
    
    frmVwContentScroll.size.height = frmVwChecked.origin.y + frmVwChecked.size.height + 16;
    
    frmBtnBatal.origin.x = 16;
    frmBtnBatal.origin.y = 0;
    frmBtnBatal.size.height = frmVwBtn.size.height;
    frmBtnBatal.size.width = frmVwBtn.size.width / 2 - (frmBtnBatal.origin.x *2);
    
    frmBtnSelanjutnya.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
    frmBtnSelanjutnya.origin.y = 0;
    frmBtnSelanjutnya.size.width = frmVwBtn.size.width - frmBtnSelanjutnya.origin.x -16;
    frmBtnSelanjutnya.size.height = frmBtnBatal.size.height;
    
    
    self.vwScroll.frame = frmVwScroll;
    self.vwContentScroll.frame = frmVwContentScroll;
    self.vwAkadAgreement.frame = frmVwChecked;
    self.lblChecked.frame = frmLblChecked;
    self.btnBaseChecked.frame = frmBtnBaseChecked;
    self.btnChecked.frame = frmBtnChecked;

    self.btnBatal.frame = frmBtnBatal;
    self.btnSelanjutnya.frame = frmBtnSelanjutnya;
        
    [self showPhase:@"3b"];

    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    
}

- (void) showWhiteLoad{
    CGRect frmVwTitle = self.vwTitle.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    self.vwTitle.frame = frmVwTitle;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, frmVwTitle.origin.y + frmVwTitle.size.height, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [view setBackgroundColor:[UIColor whiteColor]];
    view.tag = 999;
    
    [self.view addSubview:view];
}

- (void) clearWhiteView{
    for(UIView *view in self.view.subviews){
        if(view.tag == 999){
            [view removeFromSuperview];
        }
    }
}

- (void) showPhase : (NSString *) phase{
    if([phase isEqualToString:@"3a"]){
        [self.vwAkadInfo setHidden:YES];
        [self.vwAkadBase setHidden:YES];
        
        [self.vwAkadAgreement setHidden:YES];
        [self.lblAkadKetentuan setHidden:YES];
        
        [vwDynamicContent setHidden:YES];
        
        [self.lblAkadTitle setHidden:YES];
        [self.lblAkdInfoKey setHidden:YES];
        [self.lblAkdInfoVal setHidden:YES];
        [self.vwLine setHidden:YES];
        
        [self.lblAkadInfo setHidden:NO];
        [self.vwRead setHidden:NO];
        [self.vwTermsTitle setHidden:NO];
        [self.vwAgrremenClick setHidden:NO];
    }else if([phase isEqualToString:@"3b"]){
        [self.vwAkadInfo setHidden:YES];
        [self.vwAkadBase setHidden:YES];
        
        [self.vwAkadAgreement setHidden:NO];
        [self.lblAkadKetentuan setHidden:NO];
        
        [self.lblAkadTitle setHidden:YES];
        [self.lblAkdInfoKey setHidden:YES];
        [self.lblAkdInfoVal setHidden:YES];
        [self.vwLine setHidden:YES];
        
        [vwDynamicContent setHidden:YES];

        [self.lblAkadInfo setHidden:YES];
        [self.vwRead setHidden:YES];
        [self.vwTermsTitle setHidden:YES];
        [self.vwAgrremenClick setHidden:YES];
    }else if([phase isEqualToString:@"4"]){
        [self.vwAkadInfo setHidden:YES];
        [self.vwAkadBase setHidden:YES];
        
        [vwDynamicContent setHidden:NO];
        
        [self.vwAkadAgreement setHidden:YES];
        [self.lblAkadKetentuan setHidden:YES];
        
        [self.lblAkadTitle setHidden:NO];
        [self.lblAkdInfoKey setHidden:NO];
        [self.lblAkdInfoVal setHidden:NO];
        [self.vwLine setHidden:NO];
        
        [self.lblAkadInfo setHidden:YES];
        [self.vwRead setHidden:YES];
        [self.vwTermsTitle setHidden:YES];
        [self.vwAgrremenClick setHidden:YES];
    }
}
@end
