//
//  PBYCF02ViewController.h
//  BSM-Mobile
//
//  Created by ARS on 17/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PBYCF00ViewController : TemplateViewController

- (void) setDataMessageBurroq:(NSString*) string;
- (void) setFindId:(NSString*) string;

@end

NS_ASSUME_NONNULL_END
