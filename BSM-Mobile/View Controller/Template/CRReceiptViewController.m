//
//  CRReceiptViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CRReceiptViewController.h"
#import "InboxHelper.h"
#import "NDHTMLtoPDF.h"
#import "GENCF02ViewController.h"
#import "Utility.h"

@interface CRReceiptViewController ()<ConnectionDelegate, UIAlertViewDelegate,NDHTMLtoPDFDelegate, UIDocumentInteractionControllerDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    NSMutableDictionary *dataSend;
    bool isFromNext;
    bool alreadyDownload;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@property (weak, nonatomic) IBOutlet UIButton *btnBatals;
@property (weak, nonatomic) IBOutlet UIButton *btnSelanjutnya;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;
@property (weak, nonatomic) IBOutlet UIScrollView *vwContentScroll;

@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;

@end

@implementation CRReceiptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    alreadyDownload = false;
    [self.lblHeader setText:@""];
    userDefault = [NSUserDefaults standardUserDefaults];
    dataSend = [[NSMutableDictionary alloc]init];
    
    [Styles setTopConstant:_topConstant];
    
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
//        [_btnBatals setTitle:@"Kembali" forState:UIControlStateNormal];
//        [_btnSelanjutnya setTitle:@"Lihat Resi & \nUnduh Akad" forState:UIControlStateNormal];
        [_btnBatals setTitle:@"Unduh Akad" forState:UIControlStateNormal];
        [_btnSelanjutnya setTitle:@"OK" forState:UIControlStateNormal];

    }else{
//        [_btnBatals setTitle:@"Back" forState:UIControlStateNormal];
//        [_btnSelanjutnya setTitle:@"See receipts & \ndownload agreements" forState:UIControlStateNormal];
        [_btnBatals setTitle:@"Download Agreements" forState:UIControlStateNormal];
        [_btnSelanjutnya setTitle:@"OK" forState:UIControlStateNormal];
    }
    _btnBatals.titleLabel.numberOfLines=2;
    _btnBatals.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_btnBatals sizeToFit];
    
    _btnSelanjutnya.titleLabel.numberOfLines=2;
    _btnBatals.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_btnSelanjutnya sizeToFit];

    
    [self setupLayout];
    
    [_btnSelanjutnya addTarget:self action:@selector(actionSelanjutnya:) forControlEvents:UIControlEventTouchUpInside];
    [_btnBatals addTarget:self action:@selector(actionBatal:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (self.jsonData) {
        [self.lblTitleMenu setText:[self.jsonData valueForKey:@"title"]];
        NSString *urlParam = [self.jsonData valueForKey:@"url_parm"];
        //[dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        
        [userDefault setValue:[userDefault objectForKey:@"trxidcashfin"] forKey:@"transaction_id"];
        
        [dataManager.dataExtra setValue:[userDefault valueForKey:@"trxidcashfin"] forKey:@"transaction_id"];
        
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,pin,transaction_id,fin_id,spouse_name,spouse_id,spouse_bod,prenup,no_family_dependants,loan_amount,loan_tenor,insurance_code,reference_code,asset_type,asset_amount,product_code,product_type,app_no_msm,email",urlParam] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        
//        [userDefault removeObjectForKey:@"trxidcashfin"];
    }
}


-(void)setupLayout{
    CGRect frmVwTitle = _vwTitle.frame;
    CGRect frmVwBtn = _vwBtn.frame;
    CGRect frmVwScroll = _vwContentScroll.frame;
    CGRect frmLblHeader = _lblHeader.frame;

    CGRect frmBtnBatal = _btnBatals.frame;
    CGRect frmBtnSelanjutnya = _btnSelanjutnya.frame;
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.origin.x =0;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 20;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.size.height = frmBtnBatal.size.height;
    frmVwBtn.origin.x = 0;
    
    if (IPHONE_X || IPHONE_XS_MAX) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
    }
    
    frmVwScroll.origin.x = 0;
    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;
    
    
    frmLblHeader.origin.y = 0;
    frmLblHeader.origin.x = 16;
//    frmLblHeader.size.height = [Utility heightLabelDynamic:_lblHeader sizeWidth:frmVwScroll.size.width - (16*2) sizeFont:16];
    frmLblHeader.size.width = frmVwScroll.size.width - (frmLblHeader.origin.x *2);
    
    frmBtnBatal.origin.x = 16;
    frmBtnBatal.origin.y = 0;
    frmBtnBatal.size.width = frmVwBtn.size.width/2 - (frmBtnBatal.origin.x * 2);
    frmBtnBatal.size.height = frmVwBtn.size.height;
    
    frmBtnSelanjutnya.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
    frmBtnSelanjutnya.origin.y = 0;
    frmBtnSelanjutnya.size.width = frmBtnBatal.size.width;
    frmBtnSelanjutnya.size.height = frmVwBtn.size.height;
    
    
    _vwTitle.frame = frmVwTitle;
    _vwBtn.frame = frmVwBtn;
    _vwContentScroll.frame = frmVwScroll;
    _lblHeader.frame = frmLblHeader;
    _btnBatals.frame = frmBtnBatal;
    _btnSelanjutnya.frame = frmBtnSelanjutnya;
    
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    [Utility stopTimer];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject objectForKey:@"response"];
                NSError *error;
                            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                if (error == nil) {
                    if (dataDict) {
                        
                        NSString *strHtmlAkad = [NSString stringWithFormat:@"<span style=\"font-family: Lato-Regular; font-size: 15\">%@</span>",[dataDict valueForKey:@"info"]];
                        [_lblHeader setAttributedText:[Utility htmlAtributString:strHtmlAkad]];
                        self.lblHeader.numberOfLines = 0;
                        [self.lblHeader sizeToFit];
                        [self setupLayout];
                        [self saveToDB:dataDict strTransactionId:[jsonObject valueForKey:@"transaction_id"]];
                        
                    }
                }else{
                    NSLog(@"%@", error.localizedDescription);
                }
            }
        }else{

            [Utility showMessage:[jsonObject objectForKey:@"response"] mVwController:self mTag:[[jsonObject objectForKey:@"response_code"]intValue]];
        }
        
    }
    
}

-(void)saveToDB : (NSDictionary *) mData
strTransactionId : (NSString *) mTransactionId {
    NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
    [dictList setValue:[mData valueForKey:@"trxref"] forKey:@"trxref"];
    [dictList setValue:[mData valueForKey:@"title"] forKey:@"title"];
    [dictList setValue:[mData valueForKey:@"msg"] forKey:@"msg"];
    [dictList setValue:[mData valueForKey:@"footer_msg"]  forKey:@"footer_msg"];
    [dictList setValue:mTransactionId forKey:@"transaction_id"];
    [dictList setValue:@"GENCF02" forKey:@"template"];
    [dictList setValue:@"General" forKey:@"jenis"];
    [dictList setValue:[Utility dateConvert:[mData valueForKey:@"date_time"] mType:1] forKey:@"time"];
    [dictList setValue:@"0" forKey:@"marked"];
    
    InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
    [inboxHeldper insertDataInbox:dictList];
    
    [dataSend setValue:_lblTitleMenu.text forKey:@"title_menu"];
    [dataSend setValue:[mData valueForKey:@"title"] forKey:@"title"];
    [dataSend setValue:[mData valueForKey:@"msg"] forKey:@"msg"];
    [dataSend setValue:[mData valueForKey:@"footer_msg"] forKey:@"footer_msg"];
    [dataSend setValue:[mData valueForKey:@"trxref"] forKey:@"trxref"];
    [dataSend setValue:[Utility dateConvert:[mData valueForKey:@"date_time"] mType:1]  forKey:@"time"];
    
}

- (void)errorLoadData:(NSError *)error{
    [Utility showMessage:error.localizedDescription mVwController:self mTag:99];
}

- (void)reloadApp{
    [self backToRoot];
}

//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if (alertView.tag == 01) {
//        NSLog(@"Field Empty");
//    }else{
//        [self backToRoot];
//    }
//
//}

-(void)actionSelanjutnya:(UIButton *) sender{
    isFromNext = true;
    if(alreadyDownload){
        [self gotoResi];
    }else{
        [self gotoDownload];
//        [self openFileAkad];
    }
}

- (void) openFileAkad{
    NSURL *resourceToOpen = [NSURL fileURLWithPath:[dataManager.dataExtra objectForKey:@"filepath"]];

    UIDocumentInteractionController *interactionCont = [UIDocumentInteractionController interactionControllerWithURL:resourceToOpen];
    interactionCont.delegate = self;
    [interactionCont presentPreviewAnimated:YES];
}

-(void)actionBatal:(UIButton *) sender{
    isFromNext = false;
    alreadyDownload = true;
    [self gotoDownload];
//    [self openFileAkad];
}


- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller{
    if(alreadyDownload && isFromNext){
        [self gotoResi];
    }
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (void) gotoResi{
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"GENCF02"];
    GENCF02ViewController *viewCont = (GENCF02ViewController *) templateView;
    [viewCont setDataLocal:dataSend];
    [viewCont setFromRecipt:YES];
    [viewCont setFromAkad:@"YES"];
    [self.navigationController pushViewController:templateView animated:YES];
}

- (void) gotoDownload{
    NSURL *resourceToOpen = [NSURL fileURLWithPath:[dataManager.dataExtra objectForKey:@"filepath"]];
    NSArray* shareItem = @[resourceToOpen];
    UIActivityViewController *avvc = [[UIActivityViewController alloc] initWithActivityItems:shareItem applicationActivities:nil];
    avvc.excludedActivityTypes = @[UIActivityTypeAirDrop];
    if (avvc == nil){
       return;
    }
    [avvc setCompletionWithItemsHandler:^(UIActivityType _Nullable activityType, BOOL completed,
                                          NSArray * _Nullable returnedItems,
                                          NSError * _Nullable activityError) {

        if (activityType == nil)    {
            NSLog(@"UIActivityViewController dismissed without any action.");
            if(self->isFromNext){
                [self gotoResi];
            }
        } else {
            NSLog(@"completionWithItemsHandler, activityType: %@, completed: %d, returnedItems: %@, activityError: %@",
             activityType, completed, returnedItems, activityError);
            if(self->isFromNext){
                [self gotoResi];
            }
        }
    }];
    [self presentViewController:avvc animated:YES completion:nil];
}

@end
