//
//  UBP01ViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "UBP01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupListViewController.h"

@interface UBP01ViewController()<ConnectionDelegate, UITextFieldDelegate, UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, PopupListViewDelegate> {
    NSString* requestType;
    NSArray *arrayData;
    NSMutableArray *textFields;
    NSArray *arrayLabel;
    NSArray *listParamData;
    UIPickerView *objPickerView;
    NSString *textStr;
    UIToolbar *toolBar;
    NSInteger *index;
    NSDictionary *data;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLayanan;
@property (weak, nonatomic) IBOutlet UITextField *textPilihLayanan;
// @property (weak, nonatomic) IBOutlet UITableView *tabelLayout;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSetuju;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTable;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollContiner;
@property (weak, nonatomic) IBOutlet UIView *viewInscroll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightView;

@end

@implementation UBP01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [Styles setTopConstant:_heightView];
    
    textFields = [[NSMutableArray alloc] init];
    objPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 220)];
    objPickerView.showsSelectionIndicator = YES;
    objPickerView.delegate = self; // Also, can be done from IB, if you're using
    objPickerView.dataSource = self;// Also, can be done from IB, if you're using
    _textPilihLayanan.delegate = self;
    _textPilihLayanan.tag = 101;
    _textPilihLayanan.tintColor = [UIColor clearColor];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        _lblLayanan.text = @"Penyedia Layanan";
        _textPilihLayanan.placeholder = @"Pilih Penyedia Layanan";
//        [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnSetuju setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Batal" forState:UIControlStateNormal];
    } else {
        _lblLayanan.text = @"Service Provider";
        _textPilihLayanan.placeholder = @"Select Service Provider";
//        [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnSetuju setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [_btnBatal setColorSet:PRIMARYCOLORSET];
    [_btnSetuju setColorSet:SECONDARYCOLORSET];
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(changeDateFromLabel:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
//    _textPilihLayanan.inputView = objPickerView;
//    _textPilihLayanan.inputAccessoryView = toolBar;

    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];;
}

- (void)keyboardDidShow:(NSNotification *)sender {
    
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    self.btnTable.constant = height - 100;
    [self.view layoutIfNeeded];
}

- (void)keyboardWillHide:(NSNotification *)sender {
    self.btnTable.constant = 10;
    [self.view layoutIfNeeded];
}

-(void)showLayout{
    
    for(UIView *subview in [_viewInscroll subviews]) {
        [subview removeFromSuperview];
    }
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    for (int i = 0; i < [listParamData count]; i++) {
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        UIView *cell = [[UIView alloc] init];
        NSDictionary *dt = [listParamData objectAtIndex: i];
        int *typeInput = [[dt valueForKey:@"type"] intValue];
        cell.frame = CGRectMake(0, i * 90, width, 90);
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 5, width - 32, 15)];
        label.text = [dt valueForKey:@"name"];
        label.font = [UIFont fontWithName:const_font_name1 size:14];
    
        UITextField *textInput = [[UITextField alloc] initWithFrame:CGRectMake(16, 25, width - 32, 40)];
        textInput.tag = 50 + i;
        textInput.delegate = self;
        if (typeInput == 1) {
            textInput.keyboardType = UIKeyboardTypeAlphabet;
        } else if (typeInput == 2) {
            textInput.keyboardType = UIKeyboardTypeNumberPad;
        }
        textInput.inputAccessoryView = keyboardDoneButtonView;
        textInput.font = [UIFont fontWithName:const_font_name1 size:14];
        [textFields addObject:textInput];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(16, 66, width - 32, 1)];
        line.backgroundColor = [UIColor grayColor];
        
        [cell addSubview:label];
        [cell addSubview:textInput];
        [cell addSubview:line];
        [_viewInscroll addSubview:cell];
    }
    
    self.heightView.constant = [listParamData count] * 90;
    [self.view layoutIfNeeded];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag - 49 == [listParamData count]) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField.tag == 101) {
//        if ([textField.text isEqual: @""] && [arrayLabel count] > 0){
        if ([arrayLabel count] > 0){
//            textField.text = [arrayLabel objectAtIndex:0];
//            listParamData = [[arrayData objectAtIndex:0] objectForKey:@"listparam"];
//            data = [arrayData objectAtIndex:0];
            PopupListViewController *openList = [self.storyboard instantiateViewControllerWithIdentifier:@"POPUPLV"];
            [openList setDelegate:self];
            [openList setPlaceholder:_textPilihLayanan.placeholder];
            [openList setList:arrayData];
            [textField resignFirstResponder];
            [self presentViewController:openList animated:YES completion:nil];
        }
    }
    
}

- (IBAction)doneClicked:(id)sender
{
    DLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;  // Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return [arrayLabel count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arrayLabel objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    index = &row;
    
    textStr = [arrayLabel objectAtIndex:row];
    _textPilihLayanan.text = [arrayLabel objectAtIndex:row];
    data = [arrayData objectAtIndex:row];
    listParamData = [data objectForKey:@"listparam"];
    
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData{
    index = &indexRow;
    
    textStr = [arrData objectAtIndex:indexRow];
    _textPilihLayanan.text = [[arrData objectAtIndex:indexRow] objectForKey:@"name"];
    data = [arrData objectAtIndex:indexRow];
    listParamData = [data objectForKey:@"listparam"];
    [self changeDateFromLabel:self];

}

- (void)changeDateFromLabel:(id)sender {
    [_textPilihLayanan resignFirstResponder];
    [textFields removeAllObjects];
    [self showLayout];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            arrayData = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in arrayData){
                [newData addObject:[temp objectForKey:@"name"]];
            }
            arrayLabel = (NSArray *)newData;
        }
    }
}


- (IBAction)actionRefresh:(id)sender {
    _textPilihLayanan.text = @"";
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

#pragma mark change actionSetuju to goToCancel
- (IBAction)actionSetuju:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    NSString *msg;
    
    if ([_textPilihLayanan.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        for (int i = 0; i < [listParamData count]; i++) {
            NSDictionary *dt = [listParamData objectAtIndex:i];
            int typeInput = [[dt valueForKey:@"type"] intValue];
            UITextField *textField = [textFields objectAtIndex:i];
            long minlength = [[dt objectForKey:@"minlength"] longLongValue];
            long maxlength = [[dt objectForKey:@"maxlength"] longLongValue];
            NSString *val = textField.text;
            
            if (typeInput == 2) {
                textField.keyboardType = UIKeyboardTypeNumberPad;
                NSString *trimmedStringtextFieldInput = [val stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
                if ([trimmedStringtextFieldInput length]) {
                    msg = lang(@"STR_NOT_NUM");
                }
            } else if (typeInput == 1) {
                textField.keyboardType = UIKeyboardTypeAlphabet;
            }
            
            if (val.length < minlength || val.length > maxlength ){
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                
                if([lang isEqualToString:@"id"]){
                    msg =[NSString stringWithFormat:@"%@\n hanya boleh berisi di antara %ld - %ld",[dt objectForKey:@"name"], minlength, maxlength];
                } else {
                    msg =[NSString stringWithFormat:@"%@\n should only contain between %ld - %ld digit",[dt objectForKey:@"name"], minlength, maxlength];
                }
                break;
            } else {
                
            }
        }
        
        if (msg) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 212;
            [alert show];
        } else {
            NSString *aditionalParam = @"";
            for (int i = 0; i < [listParamData count]; i++) {
                NSDictionary *dt = [listParamData objectAtIndex:i];
                UITextField *textField = [textFields objectAtIndex:i];
                NSString *val = textField.text;
                int j = i +1;
                NSString *key1 = [NSString stringWithFormat:@"billkey%dname",j];
                NSString *key2 = [NSString stringWithFormat:@"billkey%d",j];
                aditionalParam = [NSString stringWithFormat:@"%@%@,%@,", aditionalParam, key1, key2];
                [dataManager.dataExtra addEntriesFromDictionary:@{key1: [dt objectForKey:@"name"]}];
                [dataManager.dataExtra addEntriesFromDictionary:@{key2: val}];
            }
            [dataManager.dataExtra addEntriesFromDictionary:@{@"billerid": [data objectForKey:@"id"]}];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"billertype": [data objectForKey:@"categorylevel1"]}];
            
            [dataManager.dataExtra addEntriesFromDictionary:@{@"billername": [[data objectForKey:@"name"] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ",[data objectForKey:@"id"]] withString:@""]}];
            aditionalParam = [NSString stringWithFormat:@"%@billerid,billertype,billername", aditionalParam];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:aditionalParam forKey:@"aditionalInfo"];
            [super openNextTemplate];
        }
    }
}

#pragma mark chance actionBatal to goToNext
- (IBAction)actionBatal:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    [self backToRoot];
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else if(alertView.tag == 212) {
    } else {
        [self backToRoot];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    return YES;
}

@end

