//
//  IB03ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "IB03ViewController.h"
#import "Styles.h"

@interface IB03ViewController () <UITextFieldDelegate>{
    NSUserDefaults *userDefault;
}
@property (weak, nonatomic) IBOutlet UITextField *textFieldDate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDateSecond;
- (IBAction)next:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;

@property (weak, nonatomic) IBOutlet UIView *vwTglEnd;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation IB03ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self setupLayout];
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [Styles setTopConstant:self.topConstant];
    
    if([lang isEqualToString:@"id"]){
//        [_btnB setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnB setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnN setTitle:@"Batal" forState:UIControlStateNormal];
    } else {
//        [_btnB setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Next" forState:UIControlStateNormal];
        [_btnB setTitle:@"Next" forState:UIControlStateNormal];
        [_btnN setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [_btnN setColorSet:SECONDARYCOLORSET];
    [_btnB setColorSet:PRIMARYCOLORSET];
    BOOL isIna = [[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"];
    
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldDate.frame.size.height);
    
    UIView *paddingPhone1 = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldDate.leftView = paddingPhone1;
    self.textFieldDate.leftViewMode = UITextFieldViewModeAlways;
    [self.textFieldDate setPlaceholder:isIna?@"Masukkan Tanggal Awal":@"Please Input Start Date"];
    _textFieldDate.tag = 1;
    
    
    UIView *paddingPhone2 = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldDateSecond.leftView = paddingPhone2;
    self.textFieldDateSecond.leftViewMode = UITextFieldViewModeAlways;
    [self.textFieldDateSecond setPlaceholder:isIna?@"Masukkan Tanggal Akhir":@"Please Input End Date"];
    _textFieldDateSecond.tag = 2;
    
    UIDatePicker *datePicker2 = [[UIDatePicker alloc] init];
    if (@available(iOS 13.4, *)) {
        [datePicker2 setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePicker2 setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    datePicker2.tag = 1;
//    NSLocale locale = [[NSLocale currentLocale] displayNameForKey:NSLocaleIdentifier value:language];
    [datePicker2 setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePicker2 setBackgroundColor:[UIColor whiteColor]];
    [datePicker2 setDatePickerMode:UIDatePickerModeDate];
    [datePicker2 addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    self.textFieldDate.inputView = datePicker2;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    if (@available(iOS 13.4, *)) {
        [datePicker setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    datePicker.tag = 2;
    [datePicker setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePicker setBackgroundColor:[UIColor whiteColor]];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    self.textFieldDateSecond.inputView = datePicker;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dateChanged:(UIDatePicker *)datePicker{
    [self resetTimer];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *selectedDate = [dateFormat stringFromDate:datePicker.date];
    if(datePicker.tag == 1){
        self.textFieldDate.text = selectedDate;
    }else{
        self.textFieldDateSecond.text = selectedDate;
    }
}

-(void) setupLayout{
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmVwTglEnd = self.vwTglEnd.frame;
    CGRect frmVwBtn = self.vwBtn.frame;
    CGRect frmBtnN = self.btnN.frame;
    CGRect frmBtnB = self.btnB.frame;
    
    frmTitle.size.width = SCREEN_WIDTH - 40;
    frmTitle.origin.y = TOP_NAV;
    frmTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.height = 40;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    
//
    frmVwBtn.origin.y = frmVwTglEnd.origin.y + frmVwTglEnd.size.height + 54;
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.size.height = 42;
    
    frmBtnN.origin.x = 16;
    frmBtnN.size.height = frmVwBtn.size.height;
    frmBtnN.size.width = frmVwBtn.size.width/2 - (frmBtnN.origin.x * 2);
    
    frmBtnB.origin.x = frmBtnN.origin.x + frmBtnN.size.width + 24;
    frmBtnB.size.width = SCREEN_WIDTH - frmBtnB.origin.x - 16;
    frmBtnB.size.height = frmBtnN.size.height;
    
    self.vwTitle.frame = frmTitle;
    self.lblTitle.frame = frmLblTitle;
    self.vwBtn.frame = frmVwBtn;
    self.btnN.frame = frmBtnN;
    self.btnB.frame = frmBtnB;
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    if([self.textFieldDate.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if([self.textFieldDateSecond.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        NSString *d1 = [self.textFieldDate.text stringByAppendingString:@" 00:00:00"];
        NSString *d2 = [self.textFieldDateSecond.text stringByAppendingString:@" 00:00:00"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        NSDate *df1 = [dateFormatter dateFromString:d1];
        NSDate *df2 = [dateFormatter dateFromString:d2];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *sdf1 = [dateFormatter stringFromDate:df1];
        NSString *sdf2 = [dateFormatter stringFromDate:df2];
        
        NSDate *dcf1 = [dateFormatter dateFromString:sdf1];
        NSDate *dcf2 = [dateFormatter dateFromString:sdf2];
        
        NSTimeInterval t1 = [dcf1 timeIntervalSince1970];
        NSTimeInterval t2 = [dcf2 timeIntervalSince1970];
        NSTimeInterval secondsBetween = [dcf2 timeIntervalSinceDate:dcf1];
        int numberOfDays = secondsBetween / 86400;
        
        if (t2 < t1) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            
            if([lang isEqualToString:@"id"]){
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Informasi" message:@"Tanggal awal harus lebih dulu dari pada tanggal akhir" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"Date end must after date start" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else {
            NSInteger mutasiMaxDays = [[userDefault valueForKey:@"config_mutation_range"]integerValue];
            if (mutasiMaxDays == 0) {
                mutasiMaxDays = 15;
            }
            NSInteger reventMaxDays = numberOfDays + 1;
            if (reventMaxDays > mutasiMaxDays) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                
                if([lang isEqualToString:@"id"]){
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Informasi" message:@"Tanggal mutasi maksimal 15 hari" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Information" message:@"Transaction day maximum 15 days" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
            else {
                NSString *newData = [NSString stringWithFormat:@"%@#%@", self.textFieldDate.text, self.textFieldDateSecond.text];
                [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:newData}];
                [super openNextTemplate];
            }
        }
    }
}


#pragma mark change cancel to goToNext
- (IBAction)cancel:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    [super backToRoot];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textFieldDate resignFirstResponder];
    [self.textFieldDateSecond resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    [self resetTimer];
    
    return YES;
}

-(void) resetTimer{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
