//
//  HJO02ViewController.m
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 09/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "HJO02ViewController.h"
#import "Validation.h"

@interface HJO02ViewController ()<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    NSArray *listData;
    NSArray *listGender;
    NSArray *listProvinsi;
    NSArray *listKota;
    NSArray *listPekerjaan;
    NSArray *listPendidikan;
    NSArray *listStatus;
    UIDatePicker *birthDate;
    int lastY;
    UIPickerView *picker;
    UIToolbar *toolbar;
    Validation *validate;
    BOOL stateKota;
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString* selectedDate;
     
}

@end

@implementation HJO02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    //self.vwScroll.contentInset = UIEdgeInsetsMake(0, 0, 2, 0);
    _titleBar.text = [self.jsonData valueForKey:@"title"];
    stateKota = false;
    
    [_btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [_btnNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    
    [_btnCheck setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    [_btnCheck setImage:[UIImage imageNamed:@"blank_check"]  forState:UIControlStateNormal];
    
    [Styles setTopConstant:_topSpace];
    [_btnNext setEnabled:false];
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    
    validate = [[Validation alloc]initWith:self];
    
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [self createToolbar];
    lastY = 0;
    
    [self registerForKeyboardNotifications];
}

- (void)actionCancel{
    [self backToR];
}

- (void)actionNext{
    if([self isDataFilled]){
        [self openNextTemplate];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{

    if(stateKota == false){
    if([requestType isEqualToString:@"inquiry_pf_hajj"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSDictionary *response = [jsonObject objectForKey:@"response"];
            NSString *disclaimer = [response objectForKey:@"disclaimer"];
            
            [self.lblDisclm setText: disclaimer];
            
            listData = [response objectForKey:@"list_input"];
            NSDictionary *nik = [listData objectAtIndex:0];
            [dataManager.dataExtra setValue:[nik objectForKey:@"value"] forKey:@"nik"];
            NSDictionary *nama = [listData objectAtIndex:1];
            [dataManager.dataExtra setValue:[nama objectForKey:@"value"] forKey:@"nama"];
            NSDictionary *lahir = [listData objectAtIndex:2];
            [dataManager.dataExtra setValue:[lahir objectForKey:@"value"] forKey:@"tempat_lahir"];
            NSDictionary *tgl = [listData objectAtIndex:3];
            [dataManager.dataExtra setValue:[tgl objectForKey:@"value"] forKey:@"tgl_lahir"];
            NSDictionary *alamat = [listData objectAtIndex:5];
            [dataManager.dataExtra setValue:[alamat objectForKey:@"value"] forKey:@"alamat"];
            NSDictionary *kodepos = [listData objectAtIndex:6];
            [dataManager.dataExtra setValue:[kodepos objectForKey:@"value"] forKey:@"kode_pos"];
            NSDictionary *desa = [listData objectAtIndex:7];
            [dataManager.dataExtra setValue:[desa objectForKey:@"value"] forKey:@"nama_desa"];
            NSDictionary *kecamatan = [listData objectAtIndex:8];
            [dataManager.dataExtra setValue:[kecamatan objectForKey:@"value"] forKey:@"nama_kecamatan"];
            NSDictionary *ayah = [listData objectAtIndex:14];
            [dataManager.dataExtra setValue:[ayah objectForKey:@"value"] forKey:@"nama_ayah"];
            NSDictionary *email = [listData objectAtIndex:15];
            [dataManager.dataExtra setValue:[email objectForKey:@"value"] forKey:@"email"];
            
            if(listData.count < 1){
                [Utility showMessage:@"No Data Found" instance:self];
            }else{
                for(int x = 0; x < listData.count; x++){
                    if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"nik"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"nama"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"tempat_lahir"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"tgl_lahir"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"jenis_kelamin"]){
                        [self createField:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"alamat"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"kode_pos"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"nama_desa"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"nama_kecamatan"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"provinsi"]){
                        [self createField:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"kota"]){
                        [self createField:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"pekerjaan"]){
                        [self createField:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"pendidikan"]){
                        [self createField:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"stat_nikah"]){
                        [self createField:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"nama_ayah"]){
                        [self createField2:x];
                    }else if([[listData[x]objectForKey:@"field_name"]isEqualToString:@"email"]){
                        [self createField2:x];
                    }
                }
                _heightViewContent.constant = lastY;
            }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
           }
        }
    }else if(stateKota == true){
        if([requestType isEqualToString:@"inquiry_pf_hajj"]){
            if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
                listKota = [jsonObject objectForKey:@"response"];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"Data kosong"] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        
    }
}
    

- (void)errorLoadData:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *acti){
        [self backToR];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) createToolbar{
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
}

- (void)createField2:(int)index{
    NSDictionary *dict = [listData objectAtIndex:index];
    NSString *field = [dict objectForKey:@"field_name"];
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, lastY, SCREEN_WIDTH, 60)];

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, SCREEN_WIDTH-32, 20)];
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(16, 20, SCREEN_WIDTH-32, 34)];
    textField.tag = index;

    [label setText:[dict objectForKey:@"label"]];
    [label setFont:[UIFont fontWithName:const_font_name3 size:14.0]];
    [label setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    if([field isEqualToString:@"tgl_lahir"]){
        [textField setDelegate:self];
        [self setDatePicker:textField withTag:index identifier:@"dob"];
        [textField setInputAccessoryView:toolbar];
        textField.inputView = birthDate;
    }

    [self setTextFieldType:[dict valueForKey:@"data_type"] of:textField];
    [Styles setStyleTextFieldBottomLine:textField];
    [textField addTarget:self action:@selector(endEdit:) forControlEvents:UIControlEventEditingDidEnd];
    [textField setFont:[UIFont fontWithName:const_font_name1 size:14.0]];
    [textField setInputAccessoryView:toolbar];
    [textField setDelegate:self];
    [textField setText:[dict objectForKey:@"value"]];
    [view addSubview:label];
    [view addSubview:textField];
    [_viewContent addSubview:view];

    lastY = lastY + view.frame.size.height + 10;
}

- (void)createField:(int)index{
    NSDictionary *dict = [listData objectAtIndex:index];
    NSDictionary *dict1 = [listData objectAtIndex:4];
    NSDictionary *dict2 = [listData objectAtIndex:9];
    NSDictionary *dict4 = [listData objectAtIndex:11];
    NSDictionary *dict5 = [listData objectAtIndex:12];
    NSDictionary *dict6 = [listData objectAtIndex:13];
    NSString *field = [dict objectForKey:@"field_name"];
    
    listGender = [dict1 objectForKey:@"value1"];
    listProvinsi = [dict2 objectForKey:@"value2"];
    listPekerjaan = [dict4 objectForKey:@"value4"];
    listPendidikan = [dict5 objectForKey:@"value5"];
    listStatus = [dict6 objectForKey:@"value6"];
    
    UIView* view = [[UIView alloc]initWithFrame:CGRectMake(0, lastY, SCREEN_WIDTH, 60)];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(16, 0, SCREEN_WIDTH-32, 20)];
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(16, 20, SCREEN_WIDTH-32, 34)];
    textField.tag = index;
    [label setText:[dict objectForKey:@"label"]];
    [label setFont:[UIFont fontWithName:const_font_name3 size:14.0]];
    [label setMinimumScaleFactor:8.0/[UIFont labelFontSize]];
    
    [self setTextFieldType:[dict valueForKey:@"data_type"] of:textField];
    [Styles setStyleTextFieldBottomLine:textField];
    [textField addTarget:self action:@selector(endEdit:) forControlEvents:UIControlEventEditingDidEnd];
    [textField setFont:[UIFont fontWithName:const_font_name1 size:14.0]];
    [textField setDelegate:self];
    
    if([field isEqualToString:@"jenis_kelamin"]){ //gender
        [self createPicker:textField withTag:index identifier:@"gender"];
        [self createToolbarButton:textField];
        if([language isEqualToString:@"id"]){
            textField.placeholder= @"Pilih Jenis Kelamin";
        }else{
            textField.placeholder= @"Select Gender";
        }
    }if([field isEqualToString:@"provinsi"]){      //provinsi
        [self createPicker:textField withTag:index identifier:@"provinsi"];
        [self createToolbarButton:textField];
        if([language isEqualToString:@"id"]){
            textField.placeholder= @"Pilih Provinsi";
        }else{
            textField.placeholder= @"Select Province";
        }
    }if([field isEqualToString:@"kota"]){          //kota
        [self createPicker:textField withTag:index identifier:@"kota"];
        [self createToolbarButton:textField];
        if([language isEqualToString:@"id"]){
            textField.placeholder= @"Pilih Kota";
        }else{
            textField.placeholder= @"Select City";
        }
    }if([field isEqualToString:@"pekerjaan"]){     //pekerjaan
        [self createPicker:textField withTag:index identifier:@"pekerjaan"];
        [self createToolbarButton:textField];
        if([language isEqualToString:@"id"]){
            textField.placeholder= @"Pilih Pekerjaan";
        }else{
            textField.placeholder= @"Select Job";
        }
    }if([field isEqualToString:@"pendidikan"]){    //pendidikan
        [self createPicker:textField withTag:index identifier:@"pendidikan"];
        [self createToolbarButton:textField];
        if([language isEqualToString:@"id"]){
            textField.placeholder= @"Pilih Pendidikan";
        }else{
            textField.placeholder= @"Select Education";
        }
    }if([field isEqualToString:@"stat_nikah"]){    //status
        [self createPicker:textField withTag:index identifier:@"status"];
        [self createToolbarButton:textField];
        if([language isEqualToString:@"id"]){
            textField.placeholder= @"Pilih Status Nikah";
        }else{
            textField.placeholder= @"Select Martial Status";
        }
    }
    [view addSubview:label];
    [view addSubview:textField];
    [_viewContent addSubview:view];

    lastY = lastY + view.frame.size.height + 10;
}

- (void)actionGetListKota {
    stateKota = true;
    NSString *urlData = @"request_type=inquiry_pf_hajj,option=get_list_kota,code,prov_id";
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if([pickerView.accessibilityIdentifier isEqualToString:@"gender"]){
        return listGender.count;
    }else if([pickerView.accessibilityIdentifier isEqualToString:@"provinsi"]){
        return listProvinsi.count;
    }else if([pickerView.accessibilityIdentifier isEqualToString:@"kota"]){
         return listKota.count;
    }else if ([pickerView.accessibilityIdentifier isEqualToString:@"pekerjaan"]){
        return listPekerjaan.count;
    }else if ([pickerView.accessibilityIdentifier isEqualToString:@"pendidikan"]){
        return listPendidikan.count;
    }else if ([pickerView.accessibilityIdentifier isEqualToString:@"status"]){
        return listStatus.count;
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if([pickerView.accessibilityIdentifier isEqualToString:@"gender"]){
        return [listGender[row] objectForKey:@"name"];
    }else if([pickerView.accessibilityIdentifier isEqualToString:@"provinsi"]){
        return [listProvinsi[row] objectForKey:@"nama_provinsi"];
    }else if([pickerView.accessibilityIdentifier isEqualToString:@"kota"]){
            return [listKota[row] objectForKey:@"nama_kota"];
    }else if([pickerView.accessibilityIdentifier isEqualToString:@"pekerjaan"]){
        return [listPekerjaan[row] objectForKey:@"name"];
    }else if([pickerView.accessibilityIdentifier isEqualToString:@"pendidikan"]){
        return [listPendidikan[row] objectForKey:@"name"];
    }else if([pickerView.accessibilityIdentifier isEqualToString:@"status"]){
        return [listStatus[row] objectForKey:@"name"];
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if([pickerView.accessibilityIdentifier isEqualToString:@"gender"]){
        for (UIView *current in self.viewContent.subviews) {
            for (UIView *field in current.subviews) {
                if([field isKindOfClass:[UITextField class]]){
                    UITextField *tfield = [(UITextField*)current viewWithTag:pickerView.tag];
                    tfield.text = [listGender[row] objectForKey:@"name"];
                }
                
            }
        }
        
    }if([pickerView.accessibilityIdentifier isEqualToString:@"provinsi"]){
        for (UIView *current in self.viewContent.subviews) {
            for (UIView *field in current.subviews) {
                if([field isKindOfClass:[UITextField class]]){
                    UITextField *tfield = [(UITextField*)current viewWithTag:pickerView.tag];
                    tfield.text = [listProvinsi[row] objectForKey:@"nama_provinsi"];
                    [dataManager.dataExtra setValue:[listProvinsi[row] objectForKey:@"prov_id"] forKey:@"prov_id"];
                }
            }
        }
        
    } if([pickerView.accessibilityIdentifier isEqualToString:@"kota"]){
        [self actionGetListKota];
        for (UIView *current in self.viewContent.subviews) {
            for (UIView *field in current.subviews) {
                if([field isKindOfClass:[UITextField class]]){
                    UITextField *tfield = [(UITextField*)current viewWithTag:pickerView.tag];
                    tfield.text = [listKota[row] objectForKey:@"nama_kota"];
                }
                
            }
        }
    }if([pickerView.accessibilityIdentifier isEqualToString:@"pekerjaan"]){
        for (UIView *current in self.viewContent.subviews) {
            for (UIView *field in current.subviews) {
                if([field isKindOfClass:[UITextField class]]){
                    UITextField *tfield = [(UITextField*)current viewWithTag:pickerView.tag];
                    tfield.text = [listPekerjaan[row] objectForKey:@"name"];
                }
            }
        }
    }if([pickerView.accessibilityIdentifier isEqualToString:@"pendidikan"]){
        for (UIView *current in self.viewContent.subviews) {
            for (UIView *field in current.subviews) {
                if([field isKindOfClass:[UITextField class]]){
                    UITextField *tfield = [(UITextField*)current viewWithTag:pickerView.tag];
                    tfield.text = [listPendidikan[row] objectForKey:@"name"];
                }
            }
        }
    }if([pickerView.accessibilityIdentifier isEqualToString:@"status"]){
        for (UIView *current in self.viewContent.subviews) {
            for (UIView *field in current.subviews) {
                if([field isKindOfClass:[UITextField class]]){
                    UITextField *tfield = [(UITextField*)current viewWithTag:pickerView.tag];
                    tfield.text = [listStatus[row] objectForKey:@"name"];
                }
            }
        }
    }
}
       
- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag identifier :(NSString*) identifier{
    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    picker.accessibilityIdentifier = identifier;
    textField.inputView = picker;
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(actionDone)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    textField.inputAccessoryView = toolbar;
}

- (void) setDatePicker: (UITextField*) textField withTag : (NSInteger) tag identifier :(NSString*) identifier{
    NSString *dateString = @"01-01-1900";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateMinimum = [dateFormatter dateFromString:dateString];
    
    birthDate = [[UIDatePicker alloc]init];
    if (@available(iOS 13.4, *)) {
        [birthDate setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [birthDate setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    [birthDate setLocale:[[NSLocale alloc]initWithLocaleIdentifier:[language isEqualToString:@"id"]?@"id":@"en"]];
    [birthDate setBackgroundColor:[UIColor whiteColor]];
    [birthDate setDatePickerMode:UIDatePickerModeDate];
    [birthDate setMinimumDate:dateMinimum];
    [birthDate setMaximumDate:[NSDate date]];
    [birthDate addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    birthDate.tag = tag;
    birthDate.accessibilityIdentifier = identifier;
}

- (void)dateChanged:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    selectedDate = [dateFormat stringFromDate:datePicker.date];
    [dataManager.dataExtra setValue:selectedDate forKey:@"birth_date"];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    selectedDate = [dateFormat stringFromDate:datePicker.date];
    
    if([datePicker.accessibilityIdentifier isEqualToString:@"dob"]){
        for (UIView *current in self.viewContent.subviews) {
            for (UIView *field in current.subviews) {
                if([field isKindOfClass:[UITextField class]]){
                    UITextField *tfield = [(UITextField*)current viewWithTag:datePicker.tag];
                    tfield.text = selectedDate;
                }
            }
        }
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString *require = [listData[textField.tag] objectForKey:@"required"];
    int maxLength = [[listData[textField.tag] objectForKey:@"maxlength"]intValue];
    
    if([require isEqualToString:@"1"]){
        if(newText.length > maxLength){
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSString *edit = [listData[textField.tag] objectForKey:@"editable"];
    if([edit isEqualToString:@"true"]){
        return YES;
    }
    return NO;
}

- (void)endEdit:(UITextField*)sender{
    NSString *require = [listData[sender.tag] objectForKey:@"required"];
    NSString *type = [listData[sender.tag] objectForKey:@"data_type"];

    if([require isEqualToString:@"1"]){
        if([type isEqualToString:@"email"]){
            if(![validate validateEmail:sender.text]){
                sender.text = @"";
            }
        }
    }
    
    NSString *key = [listData[sender.tag]objectForKey:@"field_name"];
    [dataManager.dataExtra setValue:sender.text forKey:key];
    if([key isEqualToString:@"pin"]){
        [dataManager setPinNumber:sender.text];
    }
    
}

- (void) setTextFieldType:() type of:(UITextField*)field{
    if([type isEqual:@"number"]){
        [field setKeyboardType:UIKeyboardTypeNumberPad];
    }else if([type isEqual:@"email"]){
        [field setKeyboardType:UIKeyboardTypeEmailAddress];
    }else if([type isEqual:@"phonenumber"]){
        [field setKeyboardType:UIKeyboardTypePhonePad];
    }else{
        [field setKeyboardType:UIKeyboardTypeDefault];
    }
}

- (BOOL) isDataFilled{
    int count = 0;
    for(UIView *view in _viewContent.subviews){
        NSString *require = [listData[count] objectForKey:@"required"];
        NSString *label = [listData[count] objectForKey:@"label"];
        NSString *type = [listData[count] objectForKey:@"data_type"];
        
        if([require isEqualToString:@"1"]){
            for(UIView *inview in view.subviews){
                if([inview isKindOfClass:[UITextField class]]){
                    UITextField *textField = (UITextField*)inview;
                    if([textField.text isEqualToString:@""]){
                        //Alert
                        [Utility showMessage:[NSString stringWithFormat:@"%@ perlu di lengkapi",label] enMessage:[NSString stringWithFormat:@"%@ is required",label] instance:self];
                        return NO;
                        
                    }
                    else if([type isEqualToString:@"email"]){
                        if (![validate validateEmail:textField.text]){
                        textField.text = @"";
                        return NO;
                        }
                    }
                    
                }
            }
        }
        count +=1;
    }
    return YES;
}

- (void) actionDone{
    [self.view endEditing:YES];
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{

    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (IBAction)btnCheckBox:(id)sender{
    _btnCheck.selected = !_btnCheck.selected;
    if ([_btnCheck isSelected]) {
        [_btnNext setEnabled:true];
    }else{
        [_btnNext setEnabled:false];
    }
}

@end
