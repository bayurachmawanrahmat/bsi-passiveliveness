//
//  LVP01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 13/07/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "LVP01ViewController.h"
#import "CellLVP01.h"
#import "Styles.h"

@interface LVP01ViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray* listData;
    NSUserDefaults *userDefaults;
    NSString *lang;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation LVP01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self doRequest];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    lang = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [Styles setTopConstant:_topConstraint];
    [self.labelTitle setText:[self.jsonData objectForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.iconTitle setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    [self.iconTitle setHidden:YES];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CellLVP01" bundle:nil] forCellReuseIdentifier:@"CELLLVP01"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url =[NSString stringWithFormat:@"%@,language",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellLVP01 *cell = (CellLVP01*) [tableView dequeueReusableCellWithIdentifier:@"CELLLVP01"];

    NSDictionary *data = listData[indexPath.row];
    NSString *line1 = [NSString stringWithFormat:@"%@", [data valueForKey:@"acctno"]];
    NSString *line2 = [NSString stringWithFormat:@"%@", [data valueForKey:@"acctname"]];
    NSString *line3 = [NSString stringWithFormat:@"%@ - %@", [data valueForKey:@"category"], [data valueForKey:@"catdesc"]];
    
    NSString *line4 = @"";
    NSString *line5 = @"";
    NSString *line6 = @"";
    NSString *line7 = @"";
    NSString *line8 = @"";
    NSString *line9 = @"";
    NSString *line10 = @"";
    NSString *line11 = @"";
    NSString *line12 = @"";
    
    NSString *standInsYes = @"";
    NSString *standInsNo = @"";
    NSString *standText = @"";
    
    if([lang isEqualToString:@"id"]){
        line4 = [NSString stringWithFormat:@"Saldo Tersedia : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"availbal"]];
        line5 = [NSString stringWithFormat:@"Saldo Diblokir : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"lockedamt"]];
        line6 = [NSString stringWithFormat:@"Saldo Total : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"totbal"]];
        line7 = [NSString stringWithFormat:@"Tanggal Mulai: %@", [data valueForKey:@"tanggal_mulai"]];
        line8 = [NSString stringWithFormat:@"Tanggal Jatuh Tempo : %@", [data valueForKey:@"jatuh_tempo"]];
        
        if([[data valueForKey:@"recurrencetype"] isEqualToString:@"1"]){
            line9 = [NSString stringWithFormat:@"Setoran Mingguan : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"setoran"]];
            line12 = [NSString stringWithFormat:@"Frekuensi : Mingguan"];
        }else{
            line9 = [NSString stringWithFormat:@"Setoran Bulanan : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"setoran"]];
            line12 = [NSString stringWithFormat:@"Frekuensi : Bulanan"];
        }
        line10 = [NSString stringWithFormat:@"Tanggal Auto Debet : %@", [data valueForKey:@"tanggal_debet"]];
                
        standInsYes = @"Ya";
        standInsNo = @"Tidak";
        standText = @"Standing Instruction Saat Jatuh Tempo";
        
        [cell.buttonAction setTitle:@"Penutupan" forState:UIControlStateNormal];
    }else{
        line4 = [NSString stringWithFormat:@"Available Balance : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"availbal"]];
        line5 = [NSString stringWithFormat:@"Hold Amount : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"lockedamt"]];
        line6 = [NSString stringWithFormat:@"Total Balance : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"totbal"]];
        line7 = [NSString stringWithFormat:@"Opening Date: %@", [data valueForKey:@"tanggal_mulai"]];
        line8 = [NSString stringWithFormat:@"Maturity Date : %@", [data valueForKey:@"jatuh_tempo"]];
        
        if([[data valueForKey:@"recurrencetype"] isEqualToString:@"1"]){
//            line9 = [NSString stringWithFormat:@"Setoran Mingguan : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"setoran"]];
            line12 = [NSString stringWithFormat:@"Frekuensi : Weekly"];
        }else{
//            line9 = [NSString stringWithFormat:@"Setoran Bulanan : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"setoran"]];
            line12 = [NSString stringWithFormat:@"Frequency : Monthly"];
        }
        
        line9 = [NSString stringWithFormat:@"Installment Amount : %@ %@", [data valueForKey:@"currency"], [data valueForKey:@"setoran"]];
        line10 = [NSString stringWithFormat:@"Installment Date : %@", [data valueForKey:@"tanggal_debet"]];
                
        standInsYes = @"Yes";
        standInsNo = @"No";
        standText = @"Standing Instruction at Maturity";
        
        [cell.buttonAction setTitle:@"Break" forState:UIControlStateNormal];
    }
    
    if([[data valueForKey:@"id_qurban"] isEqualToString:@"1"] || [[data valueForKey:@"id_qurban"] isEqualToString:@"2"]){
        if([[data valueForKey:@"is_credit"] isEqualToString:@"1"]){
            line11 = [NSString stringWithFormat:@"\n%@ : %@", standText, standInsYes];
        }else if([[data valueForKey:@"is_credit"] isEqualToString:@"0"]){
            line11 = [NSString stringWithFormat:@"\n%@ : %@", standText, standInsNo];
        }
    }
    
    NSString* dataToShow = [NSString stringWithFormat:@"%@ \n%@ \n%@ \n%@ \n%@ \n%@ \n%@ \n%@ \n%@ \n%@ %@ \n%@", line1, line2, line3, line4, line5, line6, line7, line8, line9, line10, line11, line12];
    [cell.labelTitle setText:dataToShow];
    cell.buttonAction.tag = indexPath.row;
    [cell.buttonAction addTarget:self action:@selector(actionBreak:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

- (void)actionBreak:(UIButton*)sender{
    
    NSDictionary *data = listData[sender.tag];
    [dataManager.dataExtra setValue:[data valueForKey:@"descroprodext"] forKey:@"descroprodext"];
    [dataManager.dataExtra setValue:[data valueForKey:@"availbal"] forKey:@"availbal"];
    [dataManager.dataExtra setValue:[data valueForKey:@"acctno"] forKey:@"srcacctno"];
    [dataManager.dataExtra setValue:[data valueForKey:@"destacctno"] forKey:@"destacctno"];
    [dataManager.dataExtra setValue:[data valueForKey:@"workingbalance"] forKey:@"workingbalance"];

    [self openNextTemplate];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //to the next template

//    NSDictionary *selectedData = listData[indexPath.row];

//    [super openNextTemplate];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"get_fortolio_autosave"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSArray *listD = (NSArray *)[jsonObject valueForKey:@"response"];
            listData = listD;
            [self.tableView reloadData];
        }else{
            NSString * msg = [jsonObject valueForKey:@"response"];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}
@end
