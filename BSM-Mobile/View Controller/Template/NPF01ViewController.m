//
//  PF01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "NPF01ViewController.h"
#import "TrackCell.h"
#import "Connection.h"
#import "Utility.h"

@interface NPF01ViewController() <ConnectionDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate> {
    NSArray *listData;
    NSInteger tabSelected;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSArray *tracks;
@property (strong, nonatomic) NSArray *tracksID;
@property (strong, nonatomic) NSArray *tracksEN;
@property (weak, nonatomic) IBOutlet UIButton *buttonFav;
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;
@end

@implementation NPF01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    /*NSString *paramData = [NSString stringWithFormat:@"request_type=customer_pf,menu_id=%@,device=%@,device_type=%@,ip_address=%@,language=%@,date_local=%@,customer_id=%@,pin=%@", @"00011", @"samsung", @"SM-G935F", @"192.168.0.8", @"id", @"20180830142630", @"617", @"B3A211FD238DA8AC"];
     Connection *conn = [[Connection alloc]initWithDelegate:self];
     [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];*/
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tracksID = @[@"TABUNGAN",@"GIRO",@"DEPOSITO",@"PEMBIAYAAN",@"SURAT BERHARGA", @"BERBAGI"];
    self.tracksEN = @[@"SAVINGS",@"CURRENT",@"DEPOSIT",@"FINANCING",@"SECURITIES", @"SHARING"];
    
    /*self.SAVArray = [[NSArray alloc] initWithObjects:@"7000000072\nTITIS ESTININGTYAS\n6010 - TABUNGAN BSM\nIDR 111,402,733.20",@"7001309395\nTITIS ESTININGTYAS\n6010 - TABUNGAN BSM\nIDR 12,091,348.59",@"7001313503\nTITIS ESTININGTYAS\n6012 - Tabungan Mabrur\nIDR 28,858,807.21",@"7001317158\nT.ESTININGTYAS OR TUNING I\n6010 - TABUNGAN BSM\nIDR 58,480,240.05",@"7001335221\nTITIS E QQ QARDH 1\n6010 - TABUNGAN BSM\nIDR 21,657,773.42",@"7001420989\nTITIS E\n6003 - Tabungan Wadiah Tabungank\nIDR 4,387,773.29",@"7032497041\nTITIS ESTININGTYAS\n6015 - TABUNGAN BERENCANA BSM\nIDR 42,907.65",@"7032571397\nTITIS E QQ TUNING IK\n6015 - TABUNGAN BERENCANA BSM\nIDR 28,447,741.79",@"7036297497\nTITIS E QQ DHUAFA\n6001 - Tabungan Wadiah Simpatik\nIDR 16,040,225.07",@"7095561289\nTITIS ESTININGTYAS\n6012 - Tabungan Mabrur\nIDR 100,000.00",@"7095561297\nTITIS ESTININGTYAS\n6010 - TABUNGAN BSM\nIDR 50,000.00",@"7095561416\nTITIS ESTININGTYAS\n6012 - Tabungan Mabrur\nIDR 0.00",nil];
     self.CURArray = [[NSArray alloc] initWithObjects:@"",nil];
     self.DEPArray = [[NSArray alloc] initWithObjects:@"7000000000004523\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 3,000,000.00",@"7000000000672828\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 25,000,000.00",@"7000000001917956\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 13,941,511.71",@"7000000001926637\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 10,000,000.00",@"7000000001929048\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 10,000,000.00",@"7000000001931037\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 20,000,000.00",@"7000000001931363\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 30,000,000.00",@"7000000001931468\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 30,000,000.00",@"7000000001934435\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 11,858,164.10",@"7000000001935498\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 30,192,725.25",@"7000000002388768\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 1,420,782.10",@"7000000006140868\nTITIS E QQ TUNING IK\n6601 - Deposito Mudharabah\nIDR 25,000,000.00",@"7000000006156406\nTITIS E QQ TUNING IK\n6601 - Deposito Mudharabah\nIDR 25,000,000.00",@"7000000007180637\nTITIS E QQ TUNING IK\n6601 - Deposito Mudharabah\nIDR 25,000,000.00",@"7000000008776673\nTITIS E QQ TUNING IK\n6601 - Deposito Mudharabah\nIDR 25,000,000.00",@"7000000024218567\nTITIS E QQ TUNING IK\n6601 - Deposito Mudharabah\nIDR 33,093,374.69",@"7000000024804978\nTITIS E QQ TUNING IK\n6601 - Deposito Mudharabah\nIDR 110,123,087.34",@"7000000059931177\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 50,000,000.00",@"7000000059931215\nTITIS ESTININGTYAS\n6601 - Deposito Mudharabah\nIDR 50,000,000.00",nil];
     self.LOANArray = [[NSArray alloc] initWithObjects:@"LD1315042036\nTITIS ESTININGTYAS\nMurabahah Financing\nIDR 6,084,387.59",@"LD1517301582\nTITIS ESTININGTYAS\nMurabahah Financing\nIDR 21,860,913.26",nil];
     self.SECArray = [[NSArray alloc] initWithObjects:@"000400-007\nTITIS ESTININGTYAS\nSUKUK RITEL 005\nIDR 20,000,000.00",@"000400-008\nTITIS ESTININGTYAS\nSUKUK RITEL 006\nIDR 20,000,000.00",nil];*/
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.tracks = _tracksID;
    } else {
        self.tracks = _tracksEN;
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    CGRect frmColl = self.collectionView.frame;
    CGRect frmTbl = self.tblView.frame;
    frmColl.size.width = screenWidth;
    self.collectionView.frame = frmColl;
    //listData = self.SAVArray;
    //self.textViewConfirmation.text = @"7120611024\nNURCHOLIS\n6010 - TABUNGAN BSM\nIDR 1,500,000.00";
    
    frmTbl.size.height = screenHeight - (frmColl.origin.x + frmColl.size.height) - BOTTOM_NAV - 100;
    
    if([Utility isDeviceHaveNotch]){
        frmTbl.size.height = screenHeight - (frmColl.origin.x + frmColl.size.height) - BOTTOM_NAV - 150;
    }
    
    
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    
    self.tblView.frame = frmTbl;
    
    if(self.jsonData){
        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
        if(fav){
            [self.buttonFav setHidden:false];
            [self.buttonShare setHidden:false];
        }
        
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if(needRequest){
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:fav];
        }
    }
    
    // Do any additional setup after loading the view.
    //[self.collectionView reloadData];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"";
    if (indexPath.row == 0) {
        cellIdentifier = @"TrackCell1";
    }
    else if (indexPath.row == 1) {
        cellIdentifier = @"TrackCell2";
    }
    else if (indexPath.row == 2) {
        cellIdentifier = @"TrackCell3";
    }
    else if (indexPath.row == 3) {
        cellIdentifier = @"TrackCell4";
    }
    else if (indexPath.row == 4) {
        cellIdentifier = @"TrackCell5";
    }else if(indexPath.row == 5){
        cellIdentifier = @"TrackCell6";
    }
    
    //TrackCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // get the track
    NSString *text = [self.tracks objectAtIndex:indexPath.row];
    
    // populate the cell
    if (indexPath.row == 0) {
        UILabel *lblTitle = (UILabel *)[cell viewWithTag:101];
        lblTitle.text = text;
    }
    else if (indexPath.row == 1) {
        UILabel *lblTitle = (UILabel *)[cell viewWithTag:102];
        lblTitle.text = text;
    }
    else if (indexPath.row == 2) {
        UILabel *lblTitle = (UILabel *)[cell viewWithTag:103];
        lblTitle.text = text;
    }
    else if (indexPath.row == 3) {
        UILabel *lblTitle = (UILabel *)[cell viewWithTag:104];
        lblTitle.text = text;
    }
    else if (indexPath.row == 4) {
        UILabel *lblTitle = (UILabel *)[cell viewWithTag:105];
        lblTitle.text = text;
    }
    else if (indexPath.row == 5) {
        UILabel *lblTitle = (UILabel *)[cell viewWithTag:106];
        lblTitle.text = text;
    }
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.tracks.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(125.0, 50.0);
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    tabSelected = indexPath.row;
    if (indexPath.row == 0) {
        if (self.SAVSTRUCTArray.count == 0) {
            self.SAVArray = [[NSArray alloc] initWithObjects:@"",nil];
        } else {
            if (![[self.SAVSTRUCTArray objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
                self.SAVArray = [[NSArray alloc] initWithObjects:@"",nil];
            }
            else {
                NSMutableArray *tempSAV = [NSMutableArray new];
                for (int i=0; i < self.SAVSTRUCTArray.count; i++) {
                    NSDictionary *data = [self.SAVSTRUCTArray objectAtIndex:i];
                    NSString *tempString = [data objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b31"]];
                    tempString = [tempString stringByAppendingString:@" - "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b32"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b41"]];
                    tempString = [tempString stringByAppendingString:@" "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b42"]];
                    [tempSAV addObject:tempString];
                    self.SAVArray = tempSAV;
                }
            }
        }
        
        listData = _SAVArray;
        //self.textViewConfirmation.text = @" 7120611024\n NURCHOLIS\n 6010 - TABUNGAN BSM\n IDR 1,500,000.00";
    }
    else if (indexPath.row == 1) {
        if (self.CURSTRUCTArray.count == 0) {
            self.CURArray = [[NSArray alloc] initWithObjects:@"",nil];
        } else {
            if (![[self.CURSTRUCTArray objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
                self.CURArray = [[NSArray alloc] initWithObjects:@"",nil];
            }
            else {
                NSMutableArray *tempCUR = [NSMutableArray new];
                for (int i=0; i < self.CURSTRUCTArray.count; i++) {
                    NSDictionary *data = [self.CURSTRUCTArray objectAtIndex:i];
                    NSString *tempString = [data objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b31"]];
                    tempString = [tempString stringByAppendingString:@" - "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b32"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b41"]];
                    tempString = [tempString stringByAppendingString:@" "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b42"]];
                    [tempCUR addObject:tempString];
                    self.CURArray = tempCUR;
                }
            }
        }

        listData = _CURArray;
        //self.textViewConfirmation.text = @" XXXXXXXXXX\n NURCHOLIS\n ???? - GIRO BSM\n IDR 50,000,000.00";
    }
    else if (indexPath.row == 2) {
        if (self.DEPSTRUCTArray.count == 0) {
            self.DEPArray = [[NSArray alloc] initWithObjects:@"",nil];
        } else {
            if (![[self.DEPSTRUCTArray objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
                self.DEPArray = [[NSArray alloc] initWithObjects:@"",nil];
            }
            else {
                NSMutableArray *tempDEP = [NSMutableArray new];
                for (int i=0; i < self.DEPSTRUCTArray.count; i++) {
                    NSDictionary *data = [self.DEPSTRUCTArray objectAtIndex:i];
                    NSString *tempString = [data objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b31"]];
                    tempString = [tempString stringByAppendingString:@" - "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b32"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b41"]];
                    tempString = [tempString stringByAppendingString:@" "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b42"]];
                    [tempDEP addObject:tempString];
                    self.DEPArray = tempDEP;
                }
            }
        }
        
        listData = _DEPArray;
        //self.textViewConfirmation.text = @" XXXXXXXXXX\n NURCHOLIS\n ???? - DEPOSITO BSM\n IDR 100,000,000.00";
    }
    else if (indexPath.row == 3) {
        if (self.LOANSTRUCTArray.count == 0) {
            self.LOANArray = [[NSArray alloc] initWithObjects:@"",nil];
        } else {
            if (![[self.LOANSTRUCTArray objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
                self.LOANArray = [[NSArray alloc] initWithObjects:@"",nil];
            }
            else {
                NSMutableArray *tempLOAN = [NSMutableArray new];
                for (int i=0; i < self.LOANSTRUCTArray.count; i++) {
                    NSDictionary *data = [self.LOANSTRUCTArray objectAtIndex:i];
                    NSString *tempString = [data objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\nIDR "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b3"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    //tempString = [tempString stringByAppendingString:[data objectForKey:@"b4"]];
                    [tempLOAN addObject:tempString];
                    self.LOANArray = tempLOAN;
                }
            }
        }
        
        listData = _LOANArray;
        //self.textViewConfirmation.text = @" XXXXXXXXXX\n NURCHOLIS\n ???? - PINJAMAN BSM\n IDR 10,000,000.00";
    }
    else if (indexPath.row == 4) {
        if (self.SECSTRUCTArray.count == 0) {
            self.SECArray = [[NSArray alloc] initWithObjects:@"",nil];
        } else {
            if (![[self.SECSTRUCTArray objectAtIndex:0] isKindOfClass:[NSDictionary class]]) {
                self.SECArray = [[NSArray alloc] initWithObjects:@"",nil];
            }
            else {
                NSMutableArray *tempSEC = [NSMutableArray new];
                for (int i=0; i < self.SECSTRUCTArray.count; i++) {
                    NSDictionary *data = [self.SECSTRUCTArray objectAtIndex:i];
                    NSString *tempString = [data objectForKey:@"b1"];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b2"]];
                    tempString = [tempString stringByAppendingString:@"\nIDR "];
                    tempString = [tempString stringByAppendingString:[data objectForKey:@"b3"]];
                    tempString = [tempString stringByAppendingString:@"\n"];
                    //tempString = [tempString stringByAppendingString:[data objectForKey:@"b4"]];
                    [tempSEC addObject:tempString];
                    self.SECArray = tempSEC;
                }
            }
        }
        
        listData = _SECArray;
        //self.textViewConfirmation.text = @" XXXXXXXXXX\n NURCHOLIS\n ???? - SURAT BERHARGA\n IDR 250,000,000.00";
    }
    
    //adding by alikhsan
    else if(indexPath.row == 5){
        NSMutableArray *arTempZiswaf = [[NSMutableArray alloc]init];
        if (self.DATAZISWAF.count>0) {
            for (int i=0; i<self.DATAZISWAF.count; i++) {
                NSMutableDictionary *dictTbl = [[NSMutableDictionary alloc]init];
                NSDictionary *dataDict = [self.DATAZISWAF objectAtIndex:i];
                NSArray *dataArr = [dataDict valueForKey:@"value"];
                [dictTbl setValue:[dataDict valueForKey:@"key"] forKey:@"txt1"];
                for(int x= 0; x<dataArr.count; x++){
                    NSDictionary *dictDataArr = [dataArr objectAtIndex:x];
                    for (int y=0; y<dictDataArr.allKeys.count; y++) {
                        NSString *keyDict = [dictDataArr.allKeys objectAtIndex:y];
                        [dictTbl setValue:keyDict forKey:[NSString stringWithFormat:@"key_ctn_%d_%d", x,y]];
                        [dictTbl setValue:[dictDataArr valueForKey:keyDict] forKey:[NSString stringWithFormat:@"val_ctn_%d_%d",x,y]];
                    }
                }
                
                [arTempZiswaf addObject:dictTbl];
            }
        }
        DLog(@"data zisaf : %@", arTempZiswaf);
        listData = arTempZiswaf;
    }
    
    [self.tblView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listData.count;
}


/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 return 200.0f;
 }*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PortCell" forIndexPath:indexPath];
    
    //NSDictionary *data = [listData objectAtIndex:indexPath.row];
    if (listData.count > 0) {
        NSString *fullString = listData[indexPath.row];
        if ([fullString isEqualToString:@""]) {
            UILabel *lblContent1 = (UILabel *)[cell viewWithTag:110];
            lblContent1.text = @"";
            
            UILabel *lblContent2 = (UILabel *)[cell viewWithTag:111];
            lblContent2.text = @"";
            
            UILabel *lblContent3 = (UILabel *)[cell viewWithTag:112];
            lblContent3.text = @"";
            
            UILabel *lblContent4 = (UILabel *)[cell viewWithTag:113];
            lblContent4.text = @"";
        }
        else {
            NSArray *splitArray = [fullString componentsSeparatedByString:@"\n"];
            
            // Configure the cell...
            UILabel *lblContent1 = (UILabel *)[cell viewWithTag:110];
            lblContent1.text = splitArray[0];
            
            UILabel *lblContent2 = (UILabel *)[cell viewWithTag:111];
            lblContent2.text = splitArray[1];
            
            UILabel *lblContent3 = (UILabel *)[cell viewWithTag:112];
            lblContent3.text = splitArray[2];
            
            UILabel *lblContent4 = (UILabel *)[cell viewWithTag:113];
            lblContent4.text = splitArray[3];
        }
    }
    else {
        UILabel *lblContent1 = (UILabel *)[cell viewWithTag:110];
        lblContent1.text = @"";
        
        UILabel *lblContent2 = (UILabel *)[cell viewWithTag:111];
        lblContent2.text = @"";
        
        UILabel *lblContent3 = (UILabel *)[cell viewWithTag:112];
        lblContent3.text = @"";
        
        UILabel *lblContent4 = (UILabel *)[cell viewWithTag:113];
        lblContent4.text = @"";
    }
    
    return cell;
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType {
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
        if (![requestType isEqualToString:@"check_notif"]) {
            if([jsonObject isKindOfClass:[NSDictionary class]]){
                NSString *response = [jsonObject objectForKey:@"response"];
                NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                NSLog(@"%@", dict);
                if(dict) {
                    //SEC Array
                    NSArray *temp1 = (NSArray *) [dict objectForKey:@"sec"];
                    NSMutableArray *tempPF1 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp1){
                        NSMutableDictionary *dictX1 = [NSMutableDictionary new];
                        [dictX1 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        //[dictX1 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX1 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX1 setValue:[data objectForKey:@"closingbalnonom"] forKey:@"b3"];
                        [tempPF1 addObject:dictX1];
                    }
                    
                    if ([tempPF1 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.SECSTRUCTArray = [tempPF1 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.SECSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //LOAN Array
                    NSArray *temp2 = (NSArray *) [dict objectForKey:@"loan"];
                    NSMutableArray *tempPF2 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp2){
                        NSMutableDictionary *dictX2 = [NSMutableDictionary new];
                        [dictX2 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        //[dictX2 setValue:[data objectForKey:@"producttype"] forKey:@"b2"];
                        [dictX2 setValue:[data objectForKey:@"producttype"] forKey:@"b2"];
                        [dictX2 setValue:[data objectForKey:@"outs"] forKey:@"b3"];
                        [tempPF2 addObject:dictX2];
                    }
                    
                    if ([tempPF2 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.LOANSTRUCTArray = [tempPF2 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.LOANSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //CUR Array
                    NSArray *temp3 = (NSArray *) [dict objectForKey:@"current"];
                    NSMutableArray *tempPF3 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp3){
                        NSMutableDictionary *dictX3 = [NSMutableDictionary new];
                        [dictX3 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        [dictX3 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX3 setValue:[data objectForKey:@"category"] forKey:@"b31"];
                        [dictX3 setValue:[data objectForKey:@"catdesc"] forKey:@"b32"];
                        [dictX3 setValue:[data objectForKey:@"currency"] forKey:@"b41"];
                        [dictX3 setValue:[data objectForKey:@"availbal"] forKey:@"b42"];
                        [tempPF3 addObject:dictX3];
                    }
                    
                    if ([tempPF3 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.CURSTRUCTArray = [tempPF3 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.CURSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //SAV Array
                    NSArray *temp4 = (NSArray *) [dict objectForKey:@"saving"];
                    NSMutableArray *tempPF4 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp4){
                        NSMutableDictionary *dictX4 = [NSMutableDictionary new];
                        [dictX4 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        [dictX4 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX4 setValue:[data objectForKey:@"category"] forKey:@"b31"];
                        [dictX4 setValue:[data objectForKey:@"catdesc"] forKey:@"b32"];
                        [dictX4 setValue:[data objectForKey:@"currency"] forKey:@"b41"];
                        [dictX4 setValue:[data objectForKey:@"availbal"] forKey:@"b42"];
                        [tempPF4 addObject:dictX4];
                    }
                    
                    if ([tempPF4 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.SAVSTRUCTArray = [tempPF4 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.SAVSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //DEP Array
                    NSArray *temp5 = (NSArray *) [dict objectForKey:@"td"];
                    NSMutableArray *tempPF5 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp5){
                        NSMutableDictionary *dictX5 = [NSMutableDictionary new];
                        [dictX5 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        [dictX5 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX5 setValue:[data objectForKey:@"category"] forKey:@"b31"];
                        [dictX5 setValue:[data objectForKey:@"catdesc"] forKey:@"b32"];
                        [dictX5 setValue:[data objectForKey:@"currency"] forKey:@"b41"];
                        [dictX5 setValue:[data objectForKey:@"availbal"] forKey:@"b42"];
                        [tempPF5 addObject:dictX5];
                    }
                    
                    if ([tempPF5 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.DEPSTRUCTArray = [tempPF5 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.DEPSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //ZISWAF array
                    NSDictionary *tempZiswaf = [dict objectForKey:@"ziswafhm1"];
                    NSMutableArray *arrTempZiswaf = [[NSMutableArray alloc]init];
                    if (tempZiswaf) {
                        for (NSString *objKeyZiswaf in tempZiswaf.allKeys) {
                            NSMutableDictionary *tempDictZiswaf = [[NSMutableDictionary alloc]init];
                            [tempDictZiswaf setValue:objKeyZiswaf forKey:@"key"];
                            [tempDictZiswaf setValue:(NSArray *)[tempZiswaf objectForKey:objKeyZiswaf] forKey:@"value"];
                            [arrTempZiswaf addObject:tempDictZiswaf];
                        }
                    }
                    NSLog(@"Ziswaf Array : %@", arrTempZiswaf);
                    self.DATAZISWAF = arrTempZiswaf;
                    
                    
                    
                }
            }
            else{
                self.SAVSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                self.CURSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                self.DEPSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                self.LOANSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                self.SECSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
                self.DATAZISWAF = [[NSArray alloc] initWithObjects:@"",nil];
            }
        }
    }
    else {
        self.SAVSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
        self.CURSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
        self.DEPSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
        self.LOANSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
        self.SECSTRUCTArray = [[NSArray alloc] initWithObjects:@"",nil];
        self.DATAZISWAF = [[NSArray alloc] initWithObjects:@"",nil];
        
        /*NSString *msg = @"";
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        
        if([lang isEqualToString:@"id"]) {
            msg = @"Permintaan tidak dapat diproses";
        } else {
            msg = @"Request time out";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];*/
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
        
    NSIndexPath *indexPathForFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.collectionView selectItemAtIndexPath:indexPathForFirstRow animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    [self collectionView:self.collectionView didSelectItemAtIndexPath:indexPathForFirstRow];
    [self.tblView reloadData];
}

- (void)errorLoadData:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp {
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
