//
//  PF03ViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 07/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLPagerTabStripViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PF03ViewController : UITableViewController<XLPagerTabStripChildItem>

- (id)initWithStyle:(UITableViewStyle)style
             mData : (NSArray *) nDataPortofolio;
- (id)initWithStyle:(UITableViewStyle)style
             mData : (NSArray *) nDataPortofolio
               mIdx: (int)nIdx;

@end

NS_ASSUME_NONNULL_END
