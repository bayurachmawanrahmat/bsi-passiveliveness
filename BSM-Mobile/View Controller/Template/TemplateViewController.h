//
//  TemplateViewController.h
//  BSM Mobile
//
//  Created by lds on 5/8/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import <UIKit/UIGestureRecognizerSubclass.h>
#import "UIImageView+AFNetworking.h"
#import "CustomBtn.h"
#import "Styles.h"
#import "Utility.h"


@interface TemplateViewController : RootViewController{
    DataManager *dataManager;
}

@property (nonatomic, retain) id jsonData;

- (void)backToRoot;
- (void)openTemplate:(NSString *)templateName withData:(NSDictionary *)data;
- (TemplateViewController *)routeTemplateController:(NSString *)templateName;
- (void)openNextTemplate;
- (void)openNextTemplateAvoidLogin;
- (void)openActivation;
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType;
@end
