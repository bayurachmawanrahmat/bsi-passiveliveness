//
//  PBYReceiptViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/6/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PBYReceiptViewController.h"
#import "InboxHelper.h"
#import "NDHTMLtoPDF.h"
//#import "GENCF02ViewController.h"
#import "GENCF02ViewController.h"
#import "Utility.h"

@interface PBYReceiptViewController ()<ConnectionDelegate, UIAlertViewDelegate,NDHTMLtoPDFDelegate, UIDocumentInteractionControllerDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    NSMutableDictionary *dataSend;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet CustomBtn *btnBatals;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSelanjutnya;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;
@property (weak, nonatomic) IBOutlet UIScrollView *vwContentScroll;

@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;

@end

@implementation PBYReceiptViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.lblHeader setText:@""];
    userDefault = [NSUserDefaults standardUserDefaults];
    dataSend = [[NSMutableDictionary alloc]init];
    
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
        [_btnBatals setTitle:@"Kembali" forState:UIControlStateNormal];
        [_btnSelanjutnya setTitle:@"Lihat Resi & \nUnduh Akad" forState:UIControlStateNormal];
//        [_btnSelanjutnya setTitle:@"Lihat Resi & \nUnduh Akad" forState:UIControlStateNormal];

    }else{
        [_btnBatals setTitle:@"Back" forState:UIControlStateNormal];
        [_btnSelanjutnya setTitle:@"See receipts & \ndownload agreements" forState:UIControlStateNormal];
//        [_btnSelanjutnya setTitle:@"See receipts & \ndownload agreements" forState:UIControlStateNormal];
    }
    [_btnBatals setColorSet:SECONDARYCOLORSET];
    
    _btnBatals.titleLabel.numberOfLines=2;
    _btnBatals.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_btnBatals sizeToFit];
    
    _btnSelanjutnya.titleLabel.numberOfLines=2;
    _btnBatals.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [_btnSelanjutnya sizeToFit];

    
    [self setupLayout];
    
    [_btnSelanjutnya addTarget:self action:@selector(actionSelanjutnya:) forControlEvents:UIControlEventTouchUpInside];
    [_btnBatals addTarget:self action:@selector(actionBatal:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (self.jsonData) {
        [self.lblTitleMenu setText:[self.jsonData valueForKey:@"title"]];
        
        NSString *urlParam = [self.jsonData valueForKey:@"url_parm"];
        //[dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        
        [userDefault setValue:[userDefault objectForKey:@"trxidcashfin"] forKey:@"transaction_id"];
        
        [dataManager.dataExtra setValue:[userDefault valueForKey:@"trxidcashfin"] forKey:@"transaction_id"];
        
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,pin,transaction_id,fin_id,spouse_name,spouse_id,spouse_bod,prenup,no_family_dependants,loan_amount,loan_tenor,insurance_code,reference_code",urlParam] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        
//        [userDefault removeObjectForKey:@"trxidcashfin"];
    }
    
    self.lblTitleMenu.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleMenu.textAlignment = const_textalignment_title;
    self.lblTitleMenu.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleMenu.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
}


-(void)setupLayout{
    CGRect frmVwTitle = _vwTitle.frame;
    CGRect frmLblTitle = _lblTitleMenu.frame;
    CGRect frmVwBtn = _vwBtn.frame;
    CGRect frmVwScroll = _vwContentScroll.frame;
    CGRect frmLblHeader = _lblHeader.frame;

    CGRect frmBtnBatal = _btnBatals.frame;
    CGRect frmBtnSelanjutnya = _btnSelanjutnya.frame;
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.origin.x =0;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 20;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.size.height = frmBtnSelanjutnya.size.height;
    frmVwBtn.origin.x = 0;
    
    if ([Utility isDeviceHaveNotch]) {
        frmVwBtn.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmVwBtn.size.height - 40;
    }
    
    frmVwScroll.origin.x = 0;
    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = frmVwBtn.origin.y - frmVwScroll.origin.y - 8;
    
    
    frmLblHeader.origin.y = 0;
    frmLblHeader.origin.x = 16;
//    frmLblHeader.size.height = [Utility heightLabelDynamic:_lblHeader sizeWidth:frmVwScroll.size.width - (16*2) sizeFont:16];
    frmLblHeader.size.width = frmVwScroll.size.width - (frmLblHeader.origin.x *2);
    
    frmBtnBatal.origin.x = 16;
    frmBtnBatal.origin.y = 0;
    frmBtnBatal.size.width = frmVwBtn.size.width/2 - (frmBtnBatal.origin.x * 2);
    frmBtnBatal.size.height = frmVwBtn.size.height;
    
    frmBtnSelanjutnya.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 24;
    frmBtnSelanjutnya.origin.y = 0;
    frmBtnSelanjutnya.size.width = frmBtnBatal.size.width;
    frmBtnSelanjutnya.size.height = frmVwBtn.size.height;
    
    
    _vwTitle.frame = frmVwTitle;
    _lblTitleMenu.frame = frmLblTitle;
    _vwBtn.frame = frmVwBtn;
    _vwContentScroll.frame = frmVwScroll;
    _lblHeader.frame = frmLblHeader;
    _btnBatals.frame = frmBtnBatal;
    _btnSelanjutnya.frame = frmBtnSelanjutnya;
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    [Utility stopTimer];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject objectForKey:@"response"];
                NSError *error;
                            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                if (error == nil) {
                    if (dataDict) {
                        NSString *strHtmlAkad = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue; font-size: 15\">%@</span>",[dataDict valueForKey:@"info"]];
                        [_lblHeader setAttributedText:[Utility htmlAtributString:strHtmlAkad]];
                        self.lblHeader.numberOfLines = 0;
                        [self.lblHeader sizeToFit];
                        [self setupLayout];
                        [self saveToDB:dataDict strTransactionId:[jsonObject valueForKey:@"transaction_id"]];
                        
                    }
                }else{
                    NSLog(@"%@", error.localizedDescription);
                }
            }
        }else{
            
            [Utility showMessage:[jsonObject objectForKey:@"response"] mVwController:self mTag:[[jsonObject objectForKey:@"response_code"]intValue]];
            
        }
        
    }
    
}

-(void)saveToDB : (NSDictionary *) mData
strTransactionId : (NSString *) mTransactionId {
    NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
    [dictList setValue:[mData valueForKey:@"trxref"] forKey:@"trxref"];
    [dictList setValue:[mData valueForKey:@"title"] forKey:@"title"];
    [dictList setValue:[mData valueForKey:@"msg"] forKey:@"msg"];
    [dictList setValue:[mData valueForKey:@"footer_msg"]  forKey:@"footer_msg"];
    [dictList setValue:mTransactionId forKey:@"transaction_id"];
    [dictList setValue:@"GENCF02" forKey:@"template"];
    [dictList setValue:@"General" forKey:@"jenis"];
    [dictList setValue:[Utility dateConvert:[mData valueForKey:@"date_time"] mType:1] forKey:@"time"];
    [dictList setValue:@"0" forKey:@"marked"];
    
    InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
    [inboxHeldper insertDataInbox:dictList];
    
    [dataSend setValue:_lblTitleMenu.text forKey:@"title_menu"];
    [dataSend setValue:[mData valueForKey:@"title"] forKey:@"title"];
    [dataSend setValue:[mData valueForKey:@"msg"] forKey:@"msg"];
    [dataSend setValue:[mData valueForKey:@"footer_msg"] forKey:@"footer_msg"];
    [dataSend setValue:[mData valueForKey:@"trxref"] forKey:@"trxref"];
    [dataSend setValue:[Utility dateConvert:[mData valueForKey:@"date_time"] mType:1]  forKey:@"time"];
    
}

- (void)errorLoadData:(NSError *)error{
    [Utility showMessage:error.localizedDescription mVwController:self mTag:99];
}

- (void)reloadApp{
    [self backToRoot];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 01) {
        NSLog(@"Field Empty");
    }else{
        [self backToRoot];
    }
    
}

-(void)actionSelanjutnya:(UIButton *) sender{
    NSString *strHtml = [dataManager.dataExtra valueForKey:@"str_akad_html"];
    self.PDFCreator = [NDHTMLtoPDF createPDFWithHTML:strHtml pathForPDF:[@"~/Documents/ketentuan_akad.pdf" stringByExpandingTildeInPath] delegate:self pageSize:kPaperSizeA4 margins:UIEdgeInsetsMake(10, 5, 10, 5)];
    NSLog(@"%@", strHtml);
}

-(void)actionBatal:(UIButton *) sender{
//   [self backToRoot:[[self.jsonData valueForKey:@"back"]boolValue]];
    [self backToR];
}

#pragma mark NDHTMLtoPDFDelegate

- (void)HTMLtoPDFDidSucceed:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did succeed (%@ / %@)", htmlToPDF, htmlToPDF.PDFpath];
    NSLog(@"%@",result);
    
    UIDocumentInteractionController *documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:htmlToPDF.PDFpath]];
    documentController.delegate = self;
    [documentController presentOpenInMenuFromRect:CGRectMake(0, SCREEN_HEIGHT - 200, SCREEN_WIDTH, 200) inView:self.view animated:YES];
    if(![documentController presentPreviewAnimated:YES])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Gagal membuka PDF FILE" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"GENCF02"];
    GENCF02ViewController *viewCont = (GENCF02ViewController *) templateView;
    [viewCont setDataLocal:dataSend];
    [viewCont setFromRecipt:YES];
    [viewCont setFromAkad:@"YES"];
    [self.navigationController pushViewController:templateView animated:YES];
    
}

- (void)HTMLtoPDFDidFail:(NDHTMLtoPDF*)htmlToPDF
{
    NSString *result = [NSString stringWithFormat:@"HTMLtoPDF did fail (%@)", htmlToPDF];
    NSLog(@"%@",result);
   
}


- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{

    return self;
}

@end
