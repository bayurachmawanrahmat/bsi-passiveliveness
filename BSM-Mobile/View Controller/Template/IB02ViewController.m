//
//  IBO2ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/13/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "IB02ViewController.h"
#import "RecentlyHelper.h"
#import "CellRecentlyUse.h"
//#import "Utility.h"
//#import "Styles.h"
//#import "CustomBtn.h"

@interface IB02ViewController ()<UIAlertViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>{
    NSDictionary *dataObject;
    NSArray *arDataRecently;
    RecentlyHelper *mHelper;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextField *textFieldInput;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet UITableView *tblRecentlyUse;
@property (weak, nonatomic) IBOutlet UIView *vwRecntlyUse;
@property (weak, nonatomic) IBOutlet UILabel *lblTItleRecently;
@property (weak, nonatomic) IBOutlet UILabel *labelTextFieldInput;

- (IBAction)next:(id)sender;
- (IBAction)batal:(id)sender;
@end

@implementation IB02ViewController

NSString *statusMenu;
NSString *menuIdPlnPostpaid = @"00012";
NSString *menuIdPlnPrepaid = @"00026";
NSString *menuIdPlnNonTaglis = @"00193";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    arDataRecently = [[NSArray alloc]init];
    mHelper = [[RecentlyHelper alloc]init];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    //update Syariah Indonesia styles
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
#pragma mark change batal to selanjutya and other else
    if([lang isEqualToString:@"id"]){
        if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00060"] && [[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
            _lblContent.text = @"Untuk handphone yang tidak mendukung fitur NFC, silahkan masukkan nomor kartu e-money Anda untuk melakukan transaksi.\n\nSetelah transaksi berhasil, update saldo kartu e-money Anda di ATM Mandiri atau update menggunakan ponsel yg memiliki fitur NFC dan aplikasi BSI Mobile.";
            [_lblContent setHidden:false];
            statusMenu = @"eMoney";
        }
        else {
            _lblContent.text = @"";
            [_lblContent setHidden:true];
            statusMenu = @"";
        }
        
//        [_btnB setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnB setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnN setTitle:@"Batal" forState:UIControlStateNormal];
        [_lblTItleRecently setText:@"Transaksi Terakhir"];
    } else {
        if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00060"] && [[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
            _lblContent.text = @"Mobile phone that does not support the NFC feature, please enter your e-money card number to make a transaction.\n\nAfter the transaction is successful, please update the balance of your e-money card at Mandiri ATM or update using a mobile phone which has NFC feature and BSI Mobile application.";
            [_lblContent setHidden:false];
            [_lblContent sizeToFit];
            statusMenu = @"eMoney";
        }
        else {
            _lblContent.text = @"";
            [_lblContent setHidden:true];
            statusMenu = @"";
        }
        
//        [_btnB setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Next" forState:UIControlStateNormal];
        [_btnB setTitle:@"Next" forState:UIControlStateNormal];
        [_btnN setTitle:@"Cancel" forState:UIControlStateNormal];
        [_lblTItleRecently setText:@"Last Transaction"];
    }
    
    //set White Skin
    [_btnN setColorSet:SECONDARYCOLORSET];

    self.textFieldInput.delegate = self;
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldInput.frame.size.height);
    
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldInput.leftView = paddingPhone;
    self.textFieldInput.leftViewMode = UITextFieldViewModeAlways;
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    //NSLog([self.jsonData valueForKey:@"content"]);
    if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
        [self.textFieldInput setSecureTextEntry:true];
        [self.textFieldInput setPlaceholder:@"pin"];
        [self.labelTextFieldInput setText:@"PIN"];
    }else{
        [self.labelTextFieldInput setText:[self.jsonData valueForKey:@"content"]];
//        [self.textFieldInput setPlaceholder:[self.jsonData valueForKey:@"content"]];
    }
    [self.lblTItleRecently sizeToFit];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    
    CGRect frmContent = _lblContent.frame;
    CGRect frmLblTextFieldTitle = _labelTextFieldInput.frame;
    CGRect frmTextField = _textFieldInput.frame;
    CGRect frmTitle = _vwTitle.frame;
    CGRect frmLblTitle = _labelTitle.frame;
    CGRect frmLine = _vwLine.frame;
    CGRect frmButton = _vwButton.frame;
    CGRect frmVwRecentlyUse = self.vwRecntlyUse.frame;
    CGRect frmTblRecently = _tblRecentlyUse.frame;
    CGRect frmLblTitleRecently = _lblTItleRecently.frame;
    
    CGRect frmBtnB = _btnB.frame;
    CGRect frmBtnN = _btnN.frame;

//    frmContent.size.width = screenWidth - 40;
    
    frmTitle.size.width = screenWidth;
    frmTitle.origin.y = TOP_NAV;
//    frmTitle.origin.y = [Styles getTopNav];
//    frmTitle.origin.y = TOP_VIEW_HEIGHT;
    frmTitle.size.height = TOP_BAR_HEIGHT;
    frmTitle.origin.x = 0;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.height = frmTitle.size.height;
    frmLblTitle.size.width = frmTitle.size.width - 32;
    
    frmLine.size.width = screenWidth - 40;
    frmButton.size.width = screenWidth;
    frmButton.origin.x = 0;
    frmButton.size.height = 40;
    
    frmBtnN.origin.x = 16;
    frmBtnN.size.width = (frmButton.size.width/2) - (frmBtnN.origin.x * 2);
    frmBtnN.size.height = frmButton.size.height;
    frmBtnB.origin.x = frmBtnN.origin.x + frmBtnN.size.width + 24;
    frmBtnB.size.width = frmBtnN.size.width;
    frmBtnB.size.height = frmButton.size.height;

    
    frmLblTextFieldTitle.origin.x = 16;
    frmLblTextFieldTitle.origin.y = frmTitle.origin.y + frmTitle.size.height + 10;
    frmLblTextFieldTitle.size.width = SCREEN_WIDTH - (frmLblTextFieldTitle.origin.x * 2);
    frmLblTextFieldTitle.size.height = [Utility getLabelHeight:_labelTextFieldInput.text width:frmLblTextFieldTitle.size.width font:_labelTextFieldInput.font];
    
    frmTextField.origin.y = frmLblTextFieldTitle.origin.y + frmLblTextFieldTitle.size.height + 10;
    frmTextField.origin.x = 16;
    frmTextField.size.width = SCREEN_WIDTH - (frmTextField.origin.x * 2);
    
    if (![_lblContent isHidden]) {
        frmContent.origin.y = frmTitle.origin.y + frmTitle.size.height + 16;
        frmContent.origin.x = 16;
        frmContent.size.width = SCREEN_WIDTH - (frmContent.origin.x * 2);
        
        frmTextField.origin.y = frmContent.origin.y + frmContent.size.height + 8;
    }
    
    frmLine.origin.y = frmTextField.origin.y + frmTextField.size.height;
    frmLine.origin.x = frmTextField.origin.x;
    frmLine.size.width = frmTextField.size.width;
    
    frmButton.origin.y = frmLine.origin.y + frmLine.size.height + 41;
    
    if (![_lblContent isHidden]) {
        frmButton.origin.y = frmLine.origin.y + frmLine.size.height + 16;
    }
    
    
    frmVwRecentlyUse.origin.y = frmButton.origin.y + frmButton.size.height + 16;
    frmVwRecentlyUse.origin.x = 0;
    frmVwRecentlyUse.size.width = SCREEN_WIDTH;
    frmVwRecentlyUse.size.height = SCREEN_HEIGHT - frmVwRecentlyUse.origin.y - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveNotch]) {
        frmVwRecentlyUse.size.height = SCREEN_HEIGHT - frmVwRecentlyUse.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    frmLblTitleRecently.origin.x = 16;
    frmLblTitleRecently.origin.y = 16;
    frmLblTitleRecently.size.width = frmVwRecentlyUse.size.width - (frmLblTitleRecently.origin.x*2);
    
    frmTblRecently.origin.x = 0;
    frmTblRecently.origin.y = frmLblTitleRecently.origin.y + frmLblTitleRecently.size.height + 8;
    frmTblRecently.size.width = frmVwRecentlyUse.size.width;
    frmTblRecently.size.height = frmVwRecentlyUse.size.height - frmTblRecently.origin.y;
    
    
    _lblContent.frame = frmContent;
    _labelTextFieldInput.frame = frmLblTextFieldTitle;
    _textFieldInput.frame = frmTextField;
    _vwTitle.frame = frmTitle;
    _labelTitle.frame = frmLblTitle;
    _vwLine.frame = frmLine;
    _vwButton.frame = frmButton;
    _btnB.frame = frmBtnB;
    _btnN.frame = frmBtnN;
    DLog(@"%f",_btnB.frame.size.height);
    _vwRecntlyUse.frame = frmVwRecentlyUse;
    _lblTItleRecently.frame = frmLblTitleRecently;
    _tblRecentlyUse.frame = frmTblRecently;
    
//    if([[[self.jsonData valueForKey:@"content"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]isEqualToString:@""]){
//        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Opps" message:@"No title found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    NSDictionary *dataObject =
    // Do any additional setup after loading the view.
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.textFieldInput.inputAccessoryView = keyboardDoneButtonView;
    self.textFieldInput.restorationIdentifier = [self.jsonData valueForKey:@"menu_id"];
    
    if([[self.jsonData valueForKey:@"menu_id"]isEqualToString: menuIdPlnPrepaid]){
       if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"payment_id"]){
        self.textFieldInput.text = [dataManager.dataExtra valueForKey:@"payment_id"];
       }
    }
    
   /*else if ([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00065"]) {
       if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"transaction_id"]){
           self.textFieldInput.text = [dataManager.dataExtra valueForKey:@"transaction_id"];
       }
   }*/
    [self showingRecentlyUses : [self.jsonData valueForKey:@"menu_id"] mFieldName:[self.jsonData valueForKey:@"field_name"]];
    

}

- (IBAction)doneClicked:(id)sender
{
    DLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToNext{
    if([self.textFieldInput.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldInput = [self.textFieldInput.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedStringtextFieldInput length]) {
            message = lang(@"STR_NOT_NUM");
        }
        
        if([message isEqualToString:@""]){
            if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
                [dataManager setPinNumber:self.textFieldInput.text];
                [super openNextTemplate];
            } else {
                if ([[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.textFieldInput.text}];
                    [super openNextTemplate];
                }
                else if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"pan"]){
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.textFieldInput.text}];
                    //[[[DataManager sharedManager]dataExtra]setObject:self.textFieldInput.text forKey:@"cardNo"];
                    [super openNextTemplate];
                }
                else {
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.textFieldInput.text}];
                    [super openNextTemplate];
                }
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

-(void) goToCancel{
    [super backToRoot];
}

#pragma mark change batal to goToNext
- (IBAction)batal:(id)sender {
    [self goToNext];
}

#pragma mark change next to goToCancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textFieldInput resignFirstResponder];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    // traping max lenght for PLM postpaid & prepaid
    if([[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]){
        if ([textField.restorationIdentifier isEqualToString: menuIdPlnPostpaid] ||
            [textField.restorationIdentifier isEqualToString: menuIdPlnPrepaid]) {
            if (textField.text.length < 12 || string.length == 0) {
                return YES;
            } else {
                return NO;
            }
        }
        
        if ([textField.restorationIdentifier isEqualToString: menuIdPlnNonTaglis]){
            if (textField.text.length < 13 || string.length == 0){
                return YES;
            }else {
                return NO;
            }
        }
    }
    
    NSRange whiteSpaceRange = [string rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        textField.text = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        return false;
    } else {
        return true;
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if([self.textFieldInput isFirstResponder]){
        // traping copy/paste for PLN postpaid & prepaid
        if ([self.textFieldInput.restorationIdentifier isEqualToString: menuIdPlnPostpaid] ||
            [self.textFieldInput.restorationIdentifier isEqualToString: menuIdPlnPrepaid] ||
            [self.textFieldInput.restorationIdentifier isEqualToString: menuIdPlnNonTaglis]) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
            }];
        }
    }
    return [super canPerformAction:action withSender:sender];
}

-(void) showingRecentlyUses : (NSString *) menuId
                 mFieldName : (NSString *) fieldName{
    [self.vwRecntlyUse setHidden:TRUE];
    arDataRecently = [mHelper readListRecentlyUse:menuId mCode:[dataManager.dataExtra valueForKey:F_CODE]];
    if (arDataRecently.count > 0) {
        if ([fieldName isEqualToString:@"payment_id"]) {
            if ([menuId isEqualToString: menuIdPlnPostpaid] ||
                [menuId isEqualToString: menuIdPlnPrepaid] ||
                [menuId isEqualToString: menuIdPlnNonTaglis]){
                [self.vwRecntlyUse setHidden:TRUE];
            } else {
                [self.vwRecntlyUse setHidden:FALSE];
            }
            self.tblRecentlyUse.delegate = self;
            self.tblRecentlyUse.dataSource = self;
            [self.tblRecentlyUse setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
            [self.tblRecentlyUse registerClass:[CellRecentlyUse class] forCellReuseIdentifier:@"cell"];
            [self.tblRecentlyUse beginUpdates];
            [self.tblRecentlyUse endUpdates];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellRecentlyUse *cell = (CellRecentlyUse *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    
    [cell.lblContentOne setText:[data valueForKey:F_ACCT_TRSCT_NAME]];
    [cell.lblContentOne sizeToFit];
    cell.lblContentOne.numberOfLines = 0;

    [cell.lblContentTwo setText:[data valueForKey:F_ACCT_REK]];
    [cell.lblContentTwo sizeToFit];
    cell.lblContentTwo.numberOfLines = 0;

    CGRect frmVwBase = cell.vwBase.frame;
    CGRect frmLblContentOne = cell.lblContentOne.frame;
    CGRect frmLblContentTwo = cell.lblContentOne.frame;
    CGRect frmImgNext = cell.imgNext.frame;

    frmVwBase.origin.x = 16;
    frmVwBase.origin.y = 0;
    frmVwBase.size.width = cell.contentView.frame.size.width - (frmVwBase.origin.x * 2);
    frmVwBase.size.height = cell.contentView.frame.size.height;

    frmImgNext.size.height = 30;
    frmImgNext.size.width = 30;
    frmImgNext.origin.x = frmVwBase.size.width - frmImgNext.size.width;
    frmImgNext.origin.y = frmVwBase.size.height/2 - frmImgNext.size.height/2;

    frmLblContentOne.origin.x = 0;
    frmLblContentOne.origin.y = 16;
    frmLblContentOne.size.width = frmVwBase.size.width - frmImgNext.size.width - 8;

    frmLblContentTwo.origin.x = frmLblContentOne.origin.x;
    frmLblContentTwo.origin.y = frmLblContentOne.origin.y + frmLblContentOne.size.height + 8;
    frmLblContentTwo.size.width = frmLblContentOne.size.width;

    cell.vwBase.frame = frmVwBase;
    cell.imgNext.frame = frmImgNext;
    cell.lblContentOne.frame = frmLblContentOne;
    cell.lblContentTwo.frame = frmLblContentTwo;
    
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat padding = 16;
    CGFloat paddingMiddle = 8;
    CGFloat imgSize = 30;
    CGFloat actualWidth = SCREEN_WIDTH - imgSize - (padding*2) - paddingMiddle;
    
    NSDictionary *dataDict = [arDataRecently objectAtIndex:indexPath.row];
    
    CGSize mSzLblContentOne = [[dataDict valueForKey:F_ACCT_TRSCT_NAME] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15]}];
    CGSize mSzLblContentTwo = [[dataDict valueForKey:F_ACCT_REK] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14]}];
    
    CGFloat constrainHeight = 2000 + mSzLblContentOne.height +mSzLblContentTwo.height;
    
    CGSize constraint = CGSizeMake(actualWidth,constrainHeight);

    CGSize sizeLblCntnOne = [Utility heightWrapWithText:[dataDict valueForKey:F_ACCT_TRSCT_NAME]
                                               fontName:[UIFont fontWithName:@"HelveticaNeue" size:15] expectedSize:constraint];
    CGSize sizeLblCntnTwo = [Utility heightWrapWithText:[dataDict valueForKey:F_ACCT_REK]
                                               fontName:[UIFont fontWithName:@"HelveticaNeue" size:14] expectedSize:constraint];
    
    
    CGFloat mTotalHeight = padding + ceil(sizeLblCntnOne.height) + paddingMiddle + ceil(sizeLblCntnTwo.height) + padding;
    
    CGFloat mHeight = MAX(mTotalHeight, 50);
    return mHeight;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arDataRecently.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    if ([[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:[data valueForKey:F_ACCT_REK]}];
        [super openNextTemplate];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


@end
