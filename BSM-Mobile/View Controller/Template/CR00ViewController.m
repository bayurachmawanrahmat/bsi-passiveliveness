//
//  CR00ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 31/05/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "CR00ViewController.h"
#import "CobTimesViewController.h"

NS_ENUM(NSInteger, CFRSTEPPROCESS){
    CHECK_PROCESS = 0,
    CANCEL_PROCESS = 1
};

@interface CR00ViewController (){
    int stepProcess;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;

@property (weak, nonatomic) IBOutlet UIScrollView *viewRejected;
@property (weak, nonatomic) IBOutlet UILabel *labelRejected;
@property (weak, nonatomic) IBOutlet CustomBtn *rejectedBtnBack;

@property (weak, nonatomic) IBOutlet UIScrollView *viewAccepted;
@property (weak, nonatomic) IBOutlet UILabel *labelAccepted;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@property (weak, nonatomic) IBOutlet UIScrollView *viewLoading;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UILabel *labelDescLoading;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonBackHome;

@end

@implementation CR00ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.labelTitleBar setText:[self.jsonData valueForKey:@"title"]];
    [Styles setTopConstant:_topConstant];
    
    [self.viewAccepted setHidden:YES];
    [self.viewLoading setHidden:YES];
    [self.viewRejected setHidden:YES];
    
    //accepted
    [self.buttonNext setTitle:lang(@"SUBMIT_FINANCING") forState:UIControlStateNormal];
    [self.buttonCancel setTitle:lang(@"NOT_YET") forState:UIControlStateNormal];
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    
    //in process
    [self.buttonBackHome setTitle:lang(@"BACK_TO_HOME") forState:UIControlStateNormal];
    [self.buttonBackHome addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    
    //rejected
    [self.rejectedBtnBack setTitle:lang(@"BACK_TO_HOME") forState:UIControlStateNormal];
    [self.rejectedBtnBack addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self doRequest];
}

- (void) actionCancel{
    stepProcess = CANCEL_PROCESS;
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=cashfin_requested,step=cancel_process,language,app_no_msm,app_no,customer_id,date_local" needLoading:true encrypted:true banking:true favorite:nil];
}

- (void) actionBack{
    [self backToR];
}

- (void) actionNext{
    [self openTemplateByMenuID:@"00177"];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        stepProcess = CHECK_PROCESS;
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,app_no",urlData] needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if(stepProcess == CHECK_PROCESS){
        if([requestType isEqualToString:@"cashfin_requested"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                
                NSString *mResponse = [jsonObject objectForKey:@"response"];
                NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
                [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
                [dataManager.dataExtra setValue:[object objectForKey:@"app_no_msm"] forKey:@"app_no_msm"];
                [dataManager.dataExtra setValue:[object objectForKey:@"app_no_msm"] forKey:@"app_no"];

                
                self.labelAccepted.text = [[object objectForKey:@"info"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
                
                [UIView transitionWithView:self.view
                  duration:1
                   options:UIViewAnimationOptionShowHideTransitionViews
                animations:^{
                    self.viewLoading.hidden = YES;
                    self.viewRejected.hidden = YES;
                    self.viewAccepted.hidden = NO;
                }
                completion:NULL];
                
            }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"01"]){
                //open action cob
                [self actionOpenCOB: [jsonObject objectForKey:@"response"]];
            }
            else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"08"]){
                
                self.labelDescLoading.text = [[jsonObject objectForKey:@"response"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
                
                [self.loader startAnimating];
                [UIView transitionWithView:self.view
                  duration:1
                   options:UIViewAnimationOptionShowHideTransitionViews
                animations:^{
                    self.viewLoading.hidden = NO;
                    self.viewRejected.hidden = YES;
                    self.viewAccepted.hidden = YES;
                }
                completion:NULL];
            }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"14"]){
                [self openTemplateByMenuID:@"00176"];
            }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"96"]){
                
                self.labelRejected.text = [[jsonObject objectForKey:@"response"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
                [UIView transitionWithView:self.view
                  duration:1
                   options:UIViewAnimationOptionShowHideTransitionViews
                animations:^{
                    self.viewLoading.hidden = YES;
                    self.viewAccepted.hidden = YES;
                    self.viewRejected.hidden = NO;
                }
                completion:NULL];
                
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        
    }else if(stepProcess == CANCEL_PROCESS){
        if([requestType isEqualToString:@"cashfin_requested"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                [self backToR];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        
    }
}

- (void) actionOpenCOB : (NSString*)string{
    
    CobTimesViewController *cob = [self.storyboard instantiateViewControllerWithIdentifier:@"COBTIMESVC"];
    [cob setMessage:string];
    
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:cob animated:YES];
    
}


@end
