//
//  QRPSCANViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "QRPSCANViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"

@interface QRPSCANViewController ()<ConnectionDelegate, AVCaptureMetadataOutputObjectsDelegate,CAAnimationDelegate, UIAlertViewDelegate> {
    AVCaptureSession *session;
    AVCaptureDevice *device;
    AVCaptureDeviceInput *input;
    AVCaptureMetadataOutput *output;
    AVCaptureVideoPreviewLayer *previewLayer;
    UIView *kotakScan;
}

@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UIButton *btnBatal;
@property (weak, nonatomic) IBOutlet UILabel *lblDeskripsi;

@end

@implementation QRPSCANViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
        _lblDeskripsi.text = @"Letakkan kode QR pada kotak untuk dipindai.";
    } else {
        [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
         _lblDeskripsi.text = @"Put the QR code on the scanning box below.";
    }

    session = [[AVCaptureSession alloc] init];
    device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (input) {
        [session addInput:input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    if (error == nil) {
        output = [[AVCaptureMetadataOutput alloc] init];
        [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [session addOutput:output];
        
        output.metadataObjectTypes = [output availableMetadataObjectTypes];
        
        previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        previewLayer.frame = self.viewPreview.frame;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.bounds=self.viewPreview.bounds;
        [self.viewPreview.layer addSublayer:previewLayer];
        [self.view bringSubviewToFront:_btnBatal];
        [session startRunning];
        
        [self createRectangle];
//        [self makeFrameScanner];
        
        CGRect rectBorderArea = [previewLayer metadataOutputRectOfInterestForRect:[Utility converRectOfInterest:self.viewPreview.bounds]];
        output.rectOfInterest = rectBorderArea;
        
    }else{
        NSString *strMsg, *strBtn;
        if ([lang isEqualToString:@"id"]) {
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"Sepertinya pengaturan privasi Anda mencegah kami mengakses kamera Anda untuk melakukan pemindaian barcode. Anda dapat memperbaikinya dengan melakukan hal berikut: \n\nSentuh tombol Buka di bawah untuk membuka aplikasi Pengaturan. \n\nHidupkan Kamera. \n\nBuka aplikasi ini dan coba lagi."];
            strBtn = @"Buka";
        }else{
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following: \n\nTouch the Go button below to open the Settings app.\n\nTurn the Camera on.\n\nOpen this app and try again."];
            strBtn = @"Go";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:strBtn otherButtonTitles:nil, nil];
        alert.tag = 1;
        [alert show];
    }
    
    
}

-(void) makeFrameScanner{
    
    UILabel *lblTitle = [Utility lblTitleQR];
    
    CGRect bounds = self.viewPreview.layer.bounds;
    CGRect frmLblTitle = lblTitle.frame;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 32;
    frmLblTitle.size.width = bounds.size.width - (frmLblTitle.origin.x * 2);
    
    lblTitle.frame = frmLblTitle;
    
    [self.viewPreview.layer addSublayer:[self frameScanner:bounds]];
    [self.viewPreview addSubview:[self borderScanner:bounds]];
    [self.viewPreview addSubview:lblTitle];
    [self.viewPreview bringSubviewToFront:self.btnBatal];
}


- (CAShapeLayer *) frameScanner : (CGRect ) vwScanner {
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:vwScanner];
    
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = (vwScanner.size.height - transparantWidth) * 0.5;
    
    [overlayPath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(transparantX,
                                                                        transparantY,
                                                                        transparantWidth, transparantWidth)]];
    
    overlayPath.usesEvenOddFillRule = true;
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [[[UIColor blackColor] colorWithAlphaComponent:0.5]CGColor];
    
    return fillLayer;
    
    
}

-(UIView *) borderScanner : (CGRect )vwScanner{
    
    
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = (vwScanner.size.height - transparantWidth) * 0.5;
    
    UIView *vwKotakScanner = [[UIView alloc]initWithFrame:CGRectMake(transparantX,
                                                                     transparantY,
                                                                     transparantWidth, transparantWidth)];
    
    CGFloat height = transparantWidth +4;
    CGFloat width = transparantWidth +4;
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(1, 25)];
    [path addLineToPoint:CGPointMake(1, 1)];
    [path addLineToPoint:CGPointMake(25, 1)];
    
    [path moveToPoint:CGPointMake(width - 30, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 25)];
    
    [path moveToPoint:CGPointMake(1, height - 30)];
    [path addLineToPoint:CGPointMake(1, height - 5)];
    [path addLineToPoint:CGPointMake(25, height - 5)];
    
    [path moveToPoint:CGPointMake(width - 30, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 30)];
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor = [UIColorFromRGB(const_color_primary) CGColor];
    pathLayer.lineWidth = 3.0f;
    pathLayer.fillColor = nil;
    
    UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, vwKotakScanner.frame.size.width, 1)];
    [vwLine setBackgroundColor:[UIColor yellowColor]];
    
    CGPoint start = CGPointMake(vwKotakScanner.frame.size.width/2,0);
    CGPoint end = CGPointMake(vwKotakScanner.frame.size.width/2, vwKotakScanner.frame.size.height+4);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.delegate = self;
    animation.fromValue = [NSValue valueWithCGPoint:start];
    animation.toValue = [NSValue valueWithCGPoint:end];
    animation.duration = 5;
    
    animation.repeatCount = HUGE_VALF;
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
    
    [vwKotakScanner.layer addSublayer:pathLayer];
    [vwKotakScanner addSubview:vwLine];
    [vwLine.layer addAnimation:animation forKey:@"position"];
    return vwKotakScanner;
}

-(void)createRectangle{
    kotakScan = [[UIView alloc] initWithFrame:CGRectMake(50, 50, self.view.layer.bounds.size.width - 80, self.view.layer.bounds.size.width - 80)];
    kotakScan.center = self.view.center;
    kotakScan.backgroundColor = [UIColor clearColor];
//    [kotakScan setBackgroundColor:[self colorWithHex:@"ffffff" alpha:0]];
//    kotakScan.layer.borderColor = [[self colorWithHex:@"df0925" alpha:1.0] CGColor];
//    kotakScan.layer.borderWidth = 5;
    CGFloat height = kotakScan.frame.size.height+4;
    CGFloat width = kotakScan.frame.size.width+4;

    UIBezierPath* path = [UIBezierPath bezierPath];

    [path moveToPoint:CGPointMake(1, 25)];
    [path addLineToPoint:CGPointMake(1, 1)];
    [path addLineToPoint:CGPointMake(25, 1)];

    [path moveToPoint:CGPointMake(width - 30, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 25)];

    [path moveToPoint:CGPointMake(1, height - 30)];
    [path addLineToPoint:CGPointMake(1, height - 5)];
    [path addLineToPoint:CGPointMake(25, height - 5)];

    [path moveToPoint:CGPointMake(width - 30, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 30)];

    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor = [UIColorFromRGB(const_color_primary) CGColor];
    pathLayer.lineWidth = 5.0f;
    pathLayer.fillColor = nil;

    
    UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kotakScan.frame.size.width, 1)];
    [vwLine setBackgroundColor:[UIColor yellowColor]];
    
    CGPoint start = CGPointMake(kotakScan.frame.size.width/2,0);
    CGPoint end = CGPointMake(kotakScan.frame.size.width/2, kotakScan.frame.size.height+4);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.delegate = self;
    animation.fromValue = [NSValue valueWithCGPoint:start];
    animation.toValue = [NSValue valueWithCGPoint:end];
    animation.duration = 5;
    
    animation.repeatCount = HUGE_VALF;
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
//
    [kotakScan.layer addSublayer:pathLayer];
    [kotakScan addSubview:vwLine];
    [vwLine.layer addAnimation:animation forKey:@"position"];
    
    
    [[self view] addSubview:kotakScan];
    
    //output.rectOfInterest = [previewLayer metadataOutputRectOfInterestForRect:kotakScan.frame];
}

- (IBAction)actionBatal:(id)sender {
    [self backToRoot];
}

- (void)viewDidLayoutSubviews {
    CGRect bounds=self.viewPreview.layer.bounds;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    previewLayer.bounds=bounds;
    previewLayer.position=CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
}


#pragma mark - AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection{
    NSString *QRCode = nil;
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            QRCode = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            break;
        }
    }
    if (QRCode) {
        [session stopRunning];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": QRCode}];
        [super openNextTemplate];
        
    }
    
    NSLog(@"QR Code: %@", QRCode);
}


- (UIColor *)colorWithHex:(NSString *)hex alpha:(float)alpha {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor blackColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return [UIColor blackColor];
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float)r/255.0) green:((float)g/255.0) blue:((float)b)/255.0 alpha:alpha];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        [self backToRoot];
          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [session stopRunning];
}

@end
