//
//  UBPCF01ViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "UBPCF01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"
#import "Styles.h"

@interface UBPCF01ViewController ()<ConnectionDelegate, UIAlertViewDelegate>  {
    NSString* transactionId;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblConten;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSetuju;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet UIView *vwBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;



@end

@implementation UBPCF01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [Styles setTopConstant:self.topConstraint];
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.lblConten setText:@""];
    
    [_btnBatal setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [_btnSetuju setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [_btnSetuju setColorSet:PRIMARYCOLORSET];
    [_btnBatal setColorSet:SECONDARYCOLORSET];
    
//    self.lblConten.text = @"Nasabah Yth. Anda akan melakukan pembayaran\nAngsuran Mandiri Tunas Finance\n\nNO KONTRAK   :   5\nNAMA CUSTOMER   :   1000000000\nANGSURAN KE   :   Sarah Salsabilla\nBIAYA ADMIN   :   Rp 70\n\nKODE TAGIHAN   :   01\nTotalTagihan   :   Rp 50,000\n\nBukti pembayaran akan dikirimkan ke email 'mukhlis_yani@yahoo.com'.\nApabila anda setuju, silahkan tekan 'Selanjutnya'.";
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    NSString *aditionalInfo = [userDefault objectForKey:@"aditionalInfo"];
    NSString *urlData = @"";
    if (aditionalInfo && ![aditionalInfo  isEqual: @""]) {
        urlData = [NSString stringWithFormat:@"%@,%@", [self.jsonData valueForKey:@"url_parm"],aditionalInfo];
        [userDefault removeObjectForKey:@"aditionalInfo"];
        [userDefault synchronize];
    } else {
        urlData =[self.jsonData valueForKey:@"url_parm"];
    }
    
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];

}

#pragma mark change actionBatal to goToNext
- (IBAction)actionBatal:(id)sender {
    [self goToCancel];
}

-(void) goToCancel{
    [self backToRoot];
}

#pragma mark change actionSetuju to goToCancel
- (IBAction)actionSetuju:(id)sender {
    [self goToNext];
}

-(void) goToNext{
    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id": transactionId}];
    [super openNextTemplate];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject valueForKey:@"response"];
                _lblConten.text = response;
                transactionId =[jsonObject valueForKey:@"transaction_id"];
                
            }
        } else {
            if ([jsonObject valueForKey:@"response"]){
                if ([[jsonObject valueForKey:@"response"] isEqualToString:@""]) {
                    NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
                    [self showAlert:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg]];
                } else {
                    NSString * msg = [jsonObject valueForKey:@"response"];
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
                    [self showAlert:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg]];
                }
            } else {
                NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
//                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
                [self showAlert:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg]];

            }

        }
        
    }
    
}

- (void)errorLoadData:(NSError *)error{
    [self showAlert:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]]];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(alertView.tag == 100) {
//        [super openActivation];
//    } else {
//        [self backToRoot];
//    }
//}

- (void) showAlert:(NSString*)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToRoot];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
