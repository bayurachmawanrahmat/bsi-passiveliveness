//
//  TGCNFViewController.h
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 11/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TGCNFViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblRespon;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;

@end

NS_ASSUME_NONNULL_END
