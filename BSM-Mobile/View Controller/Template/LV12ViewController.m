//
//  CQ01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 20/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "LV12ViewController.h"
#import "CellCQ01.h"
#import "UIImageView+AFNetworking.h"
#import "Styles.h"
#import "Utility.h"

@interface LV12ViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    NSArray* listData;
    NSUserDefaults *userDefaults;
    NSString *lang;
    NSNumberFormatter *currencyFormatter;
}
@property (weak, nonatomic) IBOutlet UILabel *labelSelectType;
@property (weak, nonatomic) IBOutlet UILabel *subLabelSelect;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation LV12ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setRequest];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    lang = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    currencyFormatter = [[NSNumberFormatter alloc]init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    if([lang isEqualToString:@"id"]){
        
        
        [self.labelSelectType setText:@"Pilih Jenis Hewan Qurban"];
        [self.subLabelSelect setText:@"Harga Berdasarkan"];
    }else{
        [self.labelSelectType setText:@"Select Type of Qurban Animal"];
        [self.subLabelSelect setText:@"Price Based"];
    }
    
    [Styles setTopConstant:_topConstraint];
    
    [self.labelTitle setText:[self.jsonData objectForKey:@"title"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    [self.iconTitle setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    [self.iconTitle setHidden:YES];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CellCQ01" bundle:nil] forCellReuseIdentifier:@"CELLCQ01"];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    
    [_tableView reloadData];
}

- (void) setRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url =[NSString stringWithFormat:@"%@,language",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellCQ01 *cell = (CellCQ01*) [tableView dequeueReusableCellWithIdentifier:@"CELLCQ01"];

    NSDictionary *selectedData = listData[indexPath.row];

    [cell.imageView setImageWithURL:[NSURL URLWithString:[selectedData objectForKey:@"url_image"]] placeholderImage:nil];
    [cell.title1 setText:[selectedData objectForKey:@"label_id"]];
    if([lang isEqualToString:@"id"]){
        [cell.smallTitle setText:@"Harga"];
    }else{
        [cell.smallTitle setText:@"Price"];
    }
    
//    NSString *priceDown = [NSString stringWithFormat:@"Rp. %@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[selectedData objectForKey:@"harga_terendah"] doubleValue]]]];
    NSString *priceUp = [NSString stringWithFormat:@"Rp. %@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[selectedData objectForKey:@"harga"] doubleValue]]]];
//
//    [cell.biggerSubTitle setText:[NSString stringWithFormat:@"%@ - %@", priceDown, priceUp]];
    [cell.biggerSubTitle setText:[NSString stringWithFormat:@"%@",priceUp]];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //to the next template
//    [dataManager setAction:[[self.jsonData objectAtIndex:1]objectForKey:@"action"] andMenuId:[self.jsonData objectAtIndex:0]];

    NSDictionary *selectedData = listData[indexPath.row];
    //id = code, hewan_qurban = label_id
    [dataManager.dataExtra setValue:[selectedData objectForKey:@"id"] forKey:@"code"];
    [dataManager.dataExtra setValue:[selectedData objectForKey:@"code"] forKey:@"id"];
    [dataManager.dataExtra setValue:[selectedData objectForKey:@"label_id"] forKey:@"hewan_qurban"];

    [super openNextTemplate];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_ref_qurban"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSArray *listD = (NSArray *)[jsonObject valueForKey:@"response"];
            self.subLabelSelect.text = [listD[0] valueForKey:@"ref_harga"];
            listData = listD;
            [self.tableView reloadData];
        }else{
            NSString * msg = [jsonObject valueForKey:@"response"];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}

@end
