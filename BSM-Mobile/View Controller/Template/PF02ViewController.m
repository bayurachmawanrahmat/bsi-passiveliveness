//
//  PF02ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 06/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PF02ViewController.h"
#import "CellPortoOne.h"


@interface PF02ViewController ()<UIAlertViewDelegate>
{
    NSArray *dataPortofolio;
    NSUserDefaults *userDefault;
    NSString *lang;
    NSString *titleTab;
    int _nIdx;
}
@end



@implementation PF02ViewController

//- (id)initWithStyle:(UITableViewStyle)style
//             mData : (NSArray *) nDataPortofolio
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        dataPortofolio = nDataPortofolio;
//        userDefault = [NSUserDefaults standardUserDefaults];
//        lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    }
//    return self;
//}
//- (id)initWithStyle:(UITableViewStyle)style
//             mData : (NSArray *) nDataPortofolio
//               mIdx:(int)nIdx
//{
//    self = [super initWithStyle:style];
//    if (self) {
//        NSArray *arrText;
//        dataPortofolio = nDataPortofolio;
//        userDefault = [NSUserDefaults standardUserDefaults];
//        lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//        if ([lang isEqualToString:@"id"]) {
//            arrText = @[@"TABUNGAN",@"GIRO",@"DEPOSITO",@"PEMBIAYAAN",@"SURAT BERHARGA", @"ZISWAF"];
//        }else{
//            arrText = @[@"SAVINGS",@"CURRENT",@"DEPOSIT",@"FINANCING",@"SECURITIES", @"ZISWAF"];
//        }
//        titleTab = [arrText objectAtIndex:nIdx];
//        _nIdx = nIdx;
//        if (dataPortofolio.count == 1) {
//            if ([dataPortofolio[0] isEqualToString:@""]) {
//                self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//
//                UIAlertView *alert;
//                if ([lang isEqualToString:@"id"]) {
//                    alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Data Kosong" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//
//                }else{
//                    alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Empty Data" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                }
//
//                if ([userDefault valueForKey:@"PEF_STATE"]) {
//                    bool state = [[userDefault valueForKey:@"PEF_STATE"]boolValue];
//                    if (state) {
//                        [userDefault setValue:@"0" forKey:@"PEF_STATE"];
//                        [userDefault synchronize];
//                        [alert show];
//                        //[self backToRoot];
//
//
//                    }
//
//                }
//
//
//
//
//            }
//
//        }
//        [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
//
//    }
//    return self;
//}

- (id)initWithStyle:(UITableViewStyle)style
             mData : (NSArray *) nDataPortofolio
               mIdx: (int)nIdx
              nRow : (int)mRow{
    self = [super initWithStyle:style];
    if (self) {
        NSArray *arrText;
        dataPortofolio = nDataPortofolio;
        userDefault = [NSUserDefaults standardUserDefaults];
        lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if ([lang isEqualToString:@"id"]) {
            arrText = @[@"TABUNGAN",@"GIRO",@"DEPOSITO",@"PEMBIAYAAN",@"SURAT BERHARGA", @"BERBAGI"];
        }else{
            arrText = @[@"SAVINGS",@"CURRENT",@"DEPOSIT",@"FINANCING",@"SECURITIES", @"SHARING"];
        }
        titleTab = [arrText objectAtIndex:nIdx];
        _nIdx = nIdx;
        if (dataPortofolio.count == 1) {
            if ([dataPortofolio[0] isEqualToString:@""]) {
                self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                
                if (mRow != 0) {
                    UIAlertView *alert;
                    if ([lang isEqualToString:@"id"]) {
                        alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Data Kosong" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        
                    }else{
                        alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Empty Data" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    }
                    
                    if ([userDefault valueForKey:@"PEF_STATE"]) {
                        bool state = [[userDefault valueForKey:@"PEF_STATE"]boolValue];
                        if (state) {
                            [userDefault setValue:@"0" forKey:@"PEF_STATE"];
                            [userDefault synchronize];
                            [alert show];
                         
                        }
                    }
                }
            }
            
        }
        [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerClass:[CellPortoOne class] forCellReuseIdentifier:@"PortoCell"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataPortofolio.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CellPortoOne *cell = (CellPortoOne *)[tableView dequeueReusableCellWithIdentifier:@"PortoCell" forIndexPath:indexPath];
    
//    CellPortoOne *cell = [tableView dequeueReusableCellWithIdentifier:@"PortoCell" forIndexPath:indexPath];
//    if(cell == nil){
//        cell = [[CellPortoOne alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PortoCell"];
//
//    }
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *fullString = [dataPortofolio objectAtIndex:indexPath.row];
    if ([fullString isEqualToString:@""]) {
         cell.lblContentOne.text = @"";
         cell.lblContentTwo.text = @"";
         cell.lblContentTree.text = @"";
         cell.lblContentFour.text = @"";
         cell.lblContentFive.text = @"";
         cell.lblContentFive.text = @"";
         cell.lblContentSix.text = @"";
         cell.lblContentSeven.text = @"";
         cell.lblContentEight.text = @"";
         cell.lblContentNine.text = @"";
         cell.lblContentTen.text = @"";
        cell.lblContentEleven.text = @"";
        cell.lblContentTwelve.text = @"";
    }else{
        NSArray *splitArray = [fullString componentsSeparatedByString:@"\n"];
        int maxAr = (int) splitArray.count;
        cell.lblContentOne.text = splitArray[0];
        cell.lblContentTwo.text = splitArray[1];
        cell.lblContentTree.text = splitArray[2];
        cell.lblContentFour.text = @"";
        cell.lblContentFive.text = @"";
        cell.lblContentSix.text = @"";
        cell.lblContentSeven.text = @"";
        cell.lblContentEight.text = @"";
        cell.lblContentNine.text = @"";
        cell.lblContentTen.text = @"";
        cell.lblContentEleven.text = @"";
        cell.lblContentTwelve.text = @"";
        
        if (splitArray.count>3) {
            cell.lblContentFour.text = splitArray[3];
        }
        
        if (splitArray.count > 4) {
            if (_nIdx == 0 || _nIdx == 1) {
                if ([lang isEqualToString:@"id"]) {
                    cell.lblContentFour.text = [NSString stringWithFormat:@"Saldo Tersedia : %@", splitArray[3]];
                    cell.lblContentFive.text = [NSString stringWithFormat:@"Saldo Diblokir : %@", splitArray[4]];
                    
                    if(splitArray.count == 7){
                        cell.lblContentTree.text = [NSString stringWithFormat:@"Peruntukan/Tujuan : %@",splitArray[(maxAr-1)]];
                        cell.lblContentFour.text = [NSString stringWithFormat:@"%@", splitArray[2]];
                        cell.lblContentFive.text = [NSString stringWithFormat:@"Saldo Tersedia : %@", splitArray[3]];
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Saldo Diblokir : %@", splitArray[4]];
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Saldo Total : %@",splitArray[(maxAr-2)]];
                    }else{
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Saldo Total : %@", splitArray[(maxAr - 1)]];
                    }

                }else{
                    cell.lblContentFour.text = [NSString stringWithFormat:@"Available Balance : %@", splitArray[3]];
                    cell.lblContentFive.text = [NSString stringWithFormat:@"Hold Balance : %@", splitArray[4]];
//                    cell.lblContentSix.text = [NSString stringWithFormat:@"Total Balance : %@", splitArray[(maxAr - 2)]];
//                    cell.lblContentSeven.text = [NSString stringWithFormat:@"%@",splitArray[(maxAr-1)]];
                    
                    if(splitArray.count == 7){
                        cell.lblContentTree.text = [NSString stringWithFormat:@"Purpose : %@",splitArray[(maxAr-1)]];
                        cell.lblContentFour.text = [NSString stringWithFormat:@"%@", splitArray[2]];
                        cell.lblContentFive.text = [NSString stringWithFormat:@"Available Balance : %@", splitArray[3]];
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Hold Balance : %@", splitArray[4]];
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Total Balance : %@",splitArray[(maxAr-2)]];
                    }else{
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Total Balance : %@", splitArray[(maxAr - 1)]];
                    }
                }
            }
            
            if (_nIdx == 0) {
                NSString *category = @"";
                if ([splitArray[2]length]>=4) {
                    category = [splitArray[2]substringWithRange:NSMakeRange(0, 4)];
                }
                if ([category isEqualToString:@"6015"] || [category isEqualToString:@"6014"]) {
                    if ([lang isEqualToString:@"id"]) {
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Tanggal Buka : %@", splitArray[5]];
                        cell.lblContentEight.text = [NSString stringWithFormat:@"Tanggal Jatuh Tempo : %@", splitArray[6]];
                        cell.lblContentNine.text = [NSString stringWithFormat:@"Auto Debet (Bulanan) : %@", splitArray[7]];
                        cell.lblContentTen.text = [NSString stringWithFormat:@"Tanggal Auto Debet : %@", splitArray[8]];
                    }else{
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Opening Date : %@", splitArray[5]];
                        cell.lblContentEight.text = [NSString stringWithFormat:@"Maturity Date : %@", splitArray[6]];
                        cell.lblContentNine.text = [NSString stringWithFormat:@"Installment Amount : %@", splitArray[7]];
                        cell.lblContentTen.text = [NSString stringWithFormat:@"Installment Date : %@", splitArray[8]];
                    }
                    
                }
            }else if(_nIdx == 2){
                if (splitArray.count == 10) {
                    if ([lang isEqualToString:@"id"]) {
                        
                        cell.lblContentFour.text = [NSString stringWithFormat:@"Nomor Bilyet : %@", splitArray[3]];
                        cell.lblContentFive.text = [NSString stringWithFormat:@"Investasi Awal : %@", splitArray[4]];
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Jumlah Investasi Saat Ini : %@", splitArray[5]];
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Tanggal Buka : %@", splitArray[6]];
                        cell.lblContentEight.text = [NSString stringWithFormat:@"Tanggal Jatuh Tempo : %@", splitArray[7]];
                        cell.lblContentNine.text = [NSString stringWithFormat:@"Jangka Waktu : %@", splitArray[8]];
                        cell.lblContentTen.text = [NSString stringWithFormat:@"Instruksi Perpanjangan : %@", splitArray[9]];
                        
                    }else{
                        cell.lblContentFour.text = [NSString stringWithFormat:@"Bilyet No : %@", splitArray[3]];
                        cell.lblContentFive.text = [NSString stringWithFormat:@"Initial Investment : %@", splitArray[4]];
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Current Investment Amount : %@", splitArray[5]];
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Open Date : %@", splitArray[6]];
                        cell.lblContentEight.text = [NSString stringWithFormat:@"Due Date : %@", splitArray[7]];
                        cell.lblContentNine.text = [NSString stringWithFormat:@"Time Period : %@", splitArray[8]];
                        cell.lblContentTen.text = [NSString stringWithFormat:@"Extension Instructions : %@", splitArray[9]];
                        
                    }
                }
                else if(splitArray.count > 10){
                    if ([lang isEqualToString:@"id"]) {

                        cell.lblContentFour.text = [NSString stringWithFormat:@"Peruntukkan/Tujuan Deposito: %@", splitArray[11]];
                        cell.lblContentFive.text = [NSString stringWithFormat:@"Nomor Bilyet : %@", splitArray[3]];
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Investasi Awal : %@", splitArray[4]];
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Jumlah Investasi Saat Ini: %@", splitArray[5]];
                        cell.lblContentEight.text = [NSString stringWithFormat:@"Tanggal Buka : %@", splitArray[6]];
                        cell.lblContentNine.text = [NSString stringWithFormat:@"Tanggal Jatuh Tempo: %@", splitArray[7]];
                        
                        NSString *tenor = splitArray[8];
                        if(tenor.length > 2){
                            cell.lblContentTen.text = [NSString stringWithFormat:@"Jangka Waktu: %d Bulan", [[splitArray[8] substringWithRange:NSMakeRange(0, 2)]intValue]];
                        }else{
                            cell.lblContentTen.text = [NSString stringWithFormat:@"Jangka Waktu: %@", tenor];
                        }
                        cell.lblContentEleven.text = [NSString stringWithFormat:@"Instruksi Perpanjangan : %@", splitArray[9]];
                        cell.lblContentTwelve.text = [NSString stringWithFormat:@"Nisbah Bagi Hasil : %@ %%", splitArray[10]];

                    }else{

                        cell.lblContentFour.text = [NSString stringWithFormat:@"Purpose of Deposito: %@", splitArray[11]];
                        cell.lblContentFive.text = [NSString stringWithFormat:@"Bilyet No : %@", splitArray[3]];
                        cell.lblContentSix.text = [NSString stringWithFormat:@"Initial Investment : %@", splitArray[4]];
                        cell.lblContentSeven.text = [NSString stringWithFormat:@"Current Investmen Amount: %@", splitArray[5]];
                        cell.lblContentEight.text = [NSString stringWithFormat:@"Open Date : %@", splitArray[6]];
                        cell.lblContentNine.text = [NSString stringWithFormat:@"Due Date: %@", splitArray[7]];
//                        cell.lblContentTen.text = [NSString stringWithFormat:@"Time Period: %d Months", [[splitArray[8] substringWithRange:NSMakeRange(0, 2)]intValue]];
                        NSString *tenor = splitArray[8];
                        if(tenor.length > 2){
                            cell.lblContentTen.text = [NSString stringWithFormat:@"Time Period: %d Months", [[splitArray[8] substringWithRange:NSMakeRange(0, 2)]intValue]];
                        }else{
                            cell.lblContentTen.text = [NSString stringWithFormat:@"Time Period: %@", tenor];
                        }
                        cell.lblContentEleven.text = [NSString stringWithFormat:@"Extension Instruction : %@", splitArray[9]];
                        cell.lblContentTwelve.text = [NSString stringWithFormat:@"Profit Sharing Rate : %@ %%", splitArray[10]];

                    }
                }
            }else if (_nIdx == 3){
                cell.lblContentFour.text = splitArray[3];
                if ([lang isEqualToString:@"id"]) {
                    cell.lblContentFive.text = [NSString stringWithFormat:@"Saldo Debit : %@", splitArray[4]];
                    cell.lblContentSix.text = [NSString stringWithFormat:@"Saldo Awal : %@", splitArray[5]];
                    cell.lblContentSeven.text =  [NSString stringWithFormat:@"Tanggal Cicilan Berikutnya : %@", splitArray[6]];
                }else{
                    cell.lblContentFive.text = [NSString stringWithFormat:@"Debit Balance : %@", splitArray[4]];
                    cell.lblContentSix.text = [NSString stringWithFormat:@"Initial Balance : %@", splitArray[5]];
                    cell.lblContentSeven.text =  [NSString stringWithFormat:@"Next Installment Date : %@", splitArray[6]];
                }
               
                
            }
        }
        
        
        
        NSLog(@"Data Cell : %@", cell.lblContentFour.text);
    }
    
    
    if (![cell.lblContentOne.text isEqualToString:@""]) {
        [cell.lblContentOne sizeToFit];
    }
    
    if (![cell.lblContentTwo.text isEqualToString:@""]) {
        [cell.lblContentTwo sizeToFit];
    }
    
    if (![cell.lblContentTree.text isEqualToString:@""]) {
        [cell.lblContentTree sizeToFit];
    }
    
    if (![cell.lblContentFour.text isEqualToString:@""]) {
        [cell.lblContentFour sizeToFit];
    }
    
    if (![cell.lblContentFive.text isEqualToString:@""]) {
        [cell.lblContentFive sizeToFit];
    }
    
    if (![cell.lblContentSix.text isEqualToString:@""]) {
        [cell.lblContentSix sizeToFit];
    }
    
    if (![cell.lblContentSeven.text isEqualToString:@""]) {
        [cell.lblContentSeven sizeToFit];
    }
    
    if (![cell.lblContentEight.text isEqualToString:@""]) {
        [cell.lblContentEight sizeToFit];
    }
    
    if (![cell.lblContentNine.text isEqualToString:@""]) {
        [cell.lblContentNine sizeToFit];
    }
    
    if (![cell.lblContentTen.text isEqualToString:@""]) {
        [cell.lblContentTen sizeToFit];
    }
    
    if (![cell.lblContentEleven.text isEqualToString:@""]) {
        [cell.lblContentEleven sizeToFit];
    }
    
    if (![cell.lblContentTwelve.text isEqualToString:@""]) {
        [cell.lblContentTwelve sizeToFit];
    }
    
    
    CGRect frmCtnOne = cell.lblContentOne.frame;
    CGRect frmCtnTwo = cell.lblContentTwo.frame;
    CGRect frmCtnTree = cell.lblContentTree.frame;
    CGRect frmCtnFour = cell.lblContentFour.frame;
    CGRect frmCtnFive = cell.lblContentFive.frame;
    CGRect frmCtnSix = cell.lblContentSix.frame;
    CGRect frmCtnSeven = cell.lblContentSeven.frame;
    CGRect frmCtnEight = cell.lblContentEight.frame;
    CGRect frmCtnNine = cell.lblContentNine.frame;
    CGRect frmCtnTen = cell.lblContentTen.frame;
    CGRect frmCtnEleven = cell.lblContentEleven.frame;
    CGRect frmCtnTweleve = cell.lblContentTwelve.frame;
    
    frmCtnOne.origin.x = 8;
    frmCtnOne.origin.y = 10;
    
    frmCtnTwo.origin.y = frmCtnOne.origin.y + frmCtnOne.size.height + 5;
    frmCtnTwo.origin.x = 8;
    
    frmCtnTree.origin.y = frmCtnTwo.origin.y + frmCtnTwo.size.height + 5;
    frmCtnTree.origin.x = 8;
    
    frmCtnFour.origin.y = frmCtnTree.origin.y + frmCtnTree.size.height + 5;
    frmCtnFour.origin.x = 8;
    
    frmCtnFive.origin.y = frmCtnFour.origin.y + frmCtnFour.size.height + 5;
    frmCtnFive.origin.x = 8;
    
    frmCtnSix.origin.y = frmCtnFive.origin.y + frmCtnFive.size.height + 5;
    frmCtnSix.origin.x = 8;
    
    frmCtnSeven.origin.y = frmCtnSix.origin.y + frmCtnSix.size.height + 5;
    frmCtnSeven.origin.x = 8;
    
    frmCtnEight.origin.y = frmCtnSeven.origin.y + frmCtnSeven.size.height + 5;
    frmCtnEight.origin.x = 8;
    
    frmCtnNine.origin.y = frmCtnEight.origin.y + frmCtnEight.size.height + 5;
    frmCtnNine.origin.x = 8;
    
    frmCtnTen.origin.y = frmCtnNine.origin.y + frmCtnNine.size.height + 5;
    frmCtnTen.origin.x = 8;
    
    frmCtnEleven.origin.y = frmCtnTen.origin.y + frmCtnTen.size.height + 5;
    frmCtnEleven.origin.x = 8;
    
    frmCtnTweleve.origin.y = frmCtnEleven.origin.y + frmCtnEleven.size.height + 5;
    frmCtnTweleve.origin.x = 8;
    
    
    
    cell.lblContentOne.frame = frmCtnOne;
    cell.lblContentTwo.frame = frmCtnTwo;
    cell.lblContentTree.frame = frmCtnTree;
    cell.lblContentFour.frame = frmCtnFour;
    cell.lblContentFive.frame = frmCtnFive;
    cell.lblContentSix.frame = frmCtnSix;
    cell.lblContentSeven.frame = frmCtnSeven;
    cell.lblContentEight.frame = frmCtnEight;
    cell.lblContentNine.frame = frmCtnNine;
    cell.lblContentTen.frame = frmCtnTen;
    cell.lblContentEleven.frame = frmCtnEleven;
    cell.lblContentTwelve.frame = frmCtnTweleve;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *fullString = [dataPortofolio objectAtIndex:indexPath.row];
    fullString = [fullString stringByAppendingString:@"\n"];
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [fullString sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGFloat heightTotal = height;
    if (_nIdx == 2 || _nIdx == 3) {
        heightTotal = height + 15.0;
    }
    return heightTotal;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - XLPagerTabStripViewControllerDelegate

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
//    NSString *strTitleTab;
//    if ([lang isEqualToString:@"id"]) {
//        strTitleTab = @"TABUNGAN";
//    }else{
//        strTitleTab = @"SAVINGS";
//    }
    return titleTab;
}

-(UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return [UIColor blackColor];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self backToRoot];
}

-(void) backToRoot{
    NSDictionary* userInfo = @{@"position": @(313)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
