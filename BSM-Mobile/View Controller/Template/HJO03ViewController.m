//
//  HJO03ViewController.m
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 20/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "HJO03ViewController.h"
#import "NSString+HTML.h"

@interface HJO03ViewController ()

@end

@implementation HJO03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [_btnNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];

    [_btnNext setColorSet:PRIMARYCOLORSET];
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    
    
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
}

- (void)actionCancel{
    [self backToR];
}

- (void)actionNext{
    [self openNextTemplate];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"inquiry_surat_kuasa_hajj"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            
            NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:response options:0];
            NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
            
            self.lblAkad.attributedText = [Utility htmlAtributString:decodedString];
            [self.lblAkad sizeToFit];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}



@end
