//
//  SKCF02ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 4/16/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SKCF02ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "UIViewController+ECSlidingViewController.h"
#import <math.h>

@interface SKCF02ViewController ()<ConnectionDelegate, UIAlertViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource> {
    NSArray *listCabang;
    NSArray *listParamData;
    NSString *valPemesanan;
    NSString *valInvestor;
    NSString *valNasional;
    
    long double valMultipleOrder;
    long double valMinPemesanan;
    
    UIPickerView *objPickerView;
    NSArray *arData;
    NSArray *arLabel;
    NSString* requestType;
    NSMutableArray *textFields;
    UIToolbar *toolBar;
    NSInteger *index;
    //NSString *textStr;
    NSDictionary *data;
    NSNumberFormatter* currencyFormatter;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *lblKuotaNasional;
@property (weak, nonatomic) IBOutlet UILabel *lblKuotaInvestor;
@property (weak, nonatomic) IBOutlet UILabel *lblMinimalPemesanan;
@property (weak, nonatomic) IBOutlet UITextField *txtNominalPemesanan;
@property (weak, nonatomic) IBOutlet UIView *vwLineNP;
@property (weak, nonatomic) IBOutlet UITextField *txtCabangRef;
@property (weak, nonatomic) IBOutlet UIView *vwLineCR;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleKuotaNasional;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleKuotaInvestor;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleMinPemesanan;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNominalPemesanan;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCabangReferal;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleJumlahIdr;

@property (weak, nonatomic) IBOutlet UILabel *lblTitleCabang;

@property (strong, nonatomic) IBOutlet UIView *viewScreen;



- (IBAction)refreshCabang:(id)sender;

- (IBAction)nextOrder:(id)sender;

- (IBAction)cancelOrder:(id)sender;

@end

@implementation SKCF02ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@"."];
    
    valMinPemesanan = [[dataManager.dataExtra valueForKey:@"min_pesan"] longLongValue];
    valMultipleOrder = [[dataManager.dataExtra valueForKey:@"multiple_order"] longLongValue];
    
    NSString *minPemesanan = [NSString stringWithFormat:@"Rp %@",[Utility formatCurrencyValue:valMinPemesanan withSeparator:@"."]];
    self.lblMinimalPemesanan.text = minPemesanan;
    NSDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_param"]];
    requestType = [dataParam valueForKey:@"request_type"];
    
    [self.imgTitle setHidden:YES];
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    textFields = [[NSMutableArray alloc] init];
    objPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 150)];
    objPickerView.showsSelectionIndicator = YES;
    objPickerView.delegate = self; // Also, can be done from IB, if you're using
    objPickerView.dataSource = self;// Also, can be done from IB, if you're using
    
    _txtCabangRef.delegate = self;
    _txtCabangRef.tag = 101;
    _txtCabangRef.tintColor = [UIColor clearColor];
    
    [self setAutoLayout];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        _lblTitleKuotaNasional.text = @"Kuota Nasional";
        _lblTitleKuotaInvestor.text = @"Kuota Investor";
        _lblTitleMinPemesanan.text = @"Minimal Pemesanan";
        _lblTitleNominalPemesanan.text = @"Nominal Pemesanan";
        _lblTitleCabangReferal.text = @"Cabang Referal";
        _lblTitleJumlahIdr.text = @"IDR";
        _lblTitleCabang.text = @"Cabang";
        
        [_btnNext setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        
    }else{
        _lblTitleKuotaNasional.text = @"National Quota";
        _lblTitleKuotaInvestor.text = @"Investor Quota";
        _lblTitleMinPemesanan.text = @"Minimum Order";
        _lblTitleNominalPemesanan.text = @"Order Nominal";
        _lblTitleCabangReferal.text = @"Referral Branch";
        _lblTitleJumlahIdr.text = @"IDR";
        _lblTitleCabang.text = @"Branch";
        
        [_btnNext setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        
    }
    
    [_btnNext setColorSet:PRIMARYCOLORSET];
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(changeBranchFromLabel:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
    _txtCabangRef.inputView = objPickerView;
    _txtCabangRef.inputAccessoryView = toolBar;
    
    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    _txtNominalPemesanan.delegate = self;
    _txtNominalPemesanan.inputAccessoryView = toolBar;
}

-(void)setAutoLayout{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    
    CGRect frmVwScreen = self.viewScreen.frame;
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmVwContent = self.vwContent.frame;
    CGRect frmVwBtn = self.vwButton.frame;
    CGRect frmBtnN = self.btnCancel.frame;
    CGRect frmBtnC = self.btnNext.frame;
    CGRect frmLineNp = self.vwLineNP.frame;
    CGRect frmLineCR = self.vwLineCR.frame;
    CGRect frmTitlNom = self.lblTitleJumlahIdr.frame;
    CGRect frmTitleCabang = self.lblTitleCabang.frame;
    CGRect frmBtnRefresh = self.btnRefresh.frame;
    CGRect frmTxtNominal = self.txtNominalPemesanan.frame;
    CGRect frmTxtCabang = self.txtCabangRef.frame;
    CGRect frmTitleNominal = self.lblTitleNominalPemesanan.frame;
    CGRect frmTitleCabangRef = self.lblTitleCabangReferal.frame;
    
    frmVwScreen.size.height = screenHeight;
    frmVwScreen.size.width = screenWidth;
    
    frmVwTitle.size.width = frmVwScreen.size.width;
    frmVwTitle.origin.y = frmVwScreen.origin.y + frmVwTitle.size.height + 30;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmVwContent.origin.x = 8;
    frmVwContent.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    frmVwContent.size.width = frmVwScreen.size.width;
    frmVwContent.size.height = frmVwScreen.size.height/2;
    
//    frmVwBtn.origin.y = frmVwContent.origin.y + frmVwContent.size.height + 32;
    frmVwBtn.size.height = 40;
    frmVwBtn.origin.y = screenHeight - (BOTTOM_NAV+frmVwBtn.size.height+16);
    frmVwBtn.size.width = frmVwScreen.size.width;
//    frmVwBtn.size.height = frmVwScreen.size.height - frmVwContent.origin.y - frmVwContent.size.height;
    
//    frmBtnN.origin.x = frmTitleCabang.origin.x + frmTitleCabang.size.width/2 + 10;
    frmBtnN.origin.y = 8;
    frmBtnN.origin.x = 16;
    frmBtnN.size.width = (frmVwBtn.size.width/2)-(frmBtnN.origin.x*2);
    frmBtnN.size.height = 40;
    
//    frmBtnC.origin.x = frmVwScreen.size.width - frmBtnC.size.width - 55;
    frmBtnC.origin.y = frmBtnN.origin.y;
    frmBtnC.origin.x = (frmVwBtn.size.width/2)+10;
    frmBtnC.size.width = frmBtnN.size.width;
    frmBtnC.size.height = 40;
    
    [self.lblTitleJumlahIdr sizeToFit];
    frmTitlNom.size.width +=5;
    frmTxtNominal.origin.x = frmTitlNom.origin.x + frmTitlNom.size.width + 24;
    frmTxtNominal.origin.y = frmTitleNominal.origin.y + frmTitleNominal.size.height+2;
    frmTitlNom.origin.y = frmTxtNominal.origin.y + 5;
    frmTxtNominal.size.width = frmVwScreen.size.width - frmTxtNominal.origin.x - 16;
    frmLineNp.origin.x = frmTxtNominal.origin.x ;
    frmLineNp.origin.y = frmTitlNom.origin.y + frmTitlNom.size.height;
    frmLineNp.size.width = frmTxtNominal.size.width;
    
    frmBtnRefresh.origin.x = frmVwScreen.size.width  - frmBtnRefresh.size.width - 24;
    
    frmLineCR.origin.x = frmTitleCabang.origin.x + frmTitleCabang.size.width + 16;
    frmLineCR.origin.y = frmBtnRefresh.origin.y + frmBtnRefresh.size.height+5;
    frmLineCR.size.width = frmBtnRefresh.origin.x - frmBtnRefresh.size.width;
    
    frmTxtCabang.origin.x =frmTitleCabang.origin.x + frmTitleCabang.size.width + 16;
    frmTxtCabang.origin.y = frmTitleCabangRef.origin.y + frmTitleCabangRef.size.height + 2;
    frmTitleCabang.origin.y = frmTxtCabang.origin.y + 5;
    frmTxtCabang.size.width = frmBtnRefresh.origin.x - 8;
    
    if ([Utility isDeviceHaveNotch]) {
//          frmVwBtn.origin.y = SCREEN_HEIGHT - (BOTTOM_NAV+frmVwBtn.size.height);
            frmVwBtn.origin.y = screenHeight -(BOTTOM_NAV+50+frmBtnN.size.height);
             
          }
    
    self.viewScreen.frame = frmVwScreen;
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.vwContent.frame = frmVwContent;
    self.vwButton.frame = frmVwBtn;
    self.btnCancel.frame = frmBtnN;
    self.btnNext.frame = frmBtnC;
    
    self.txtNominalPemesanan.frame = frmTxtNominal;
    
    
    self.lblTitleJumlahIdr.frame = frmTitlNom;
    self.vwLineNP.frame = frmLineNp;
    
    
    self.txtCabangRef.frame = frmTxtCabang;
    self.lblTitleCabang.frame = frmTitleCabang;
    self.btnRefresh.frame = frmBtnRefresh;
    self.vwLineCR.frame = frmLineCR;
}

- (void)keyboardWillHide:(NSNotification *)sender {
    [self.view layoutIfNeeded];
}

- (void)keyboardDidShow:(NSNotification *)sender {
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    [self.view layoutIfNeeded];
}

-(void) changeBranchFromLabel:(id)sender{
    [_txtCabangRef resignFirstResponder];
    [textFields removeAllObjects];
    [_txtNominalPemesanan resignFirstResponder];

}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject objectForKey:@"response"];
                NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                valPemesanan = [dict objectForKey:@"pemesanan"];
                valInvestor = [dict objectForKey:@"investor"];
                valNasional = [dict objectForKey:@"nasional"];
                listCabang = [dict objectForKey:@"cabang"];
                
                
//                _lblKuotaNasional.text= [NSString stringWithFormat:@"Rp %@",valNasional];
//                _lblKuotaInvestor.text = [NSString stringWithFormat:@"Rp %@",valInvestor];
                
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"id-ID"]];
                [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                [numberFormatter setCurrencySymbol:@"Rp "];
                [numberFormatter setGroupingSeparator:@"."];
                
                NSNumber *valInvestorNumber = [NSNumber numberWithLongLong:[valInvestor longLongValue]];
                NSString *formatValInvestor = [numberFormatter stringFromNumber:valInvestorNumber];
                
                NSNumber *valNasionalNumber = [NSNumber numberWithLongLong:[valNasional longLongValue]];
                NSString *formatValNasional = [numberFormatter stringFromNumber:valNasionalNumber];
                
                _lblKuotaNasional.text= formatValNasional;
                _lblKuotaInvestor.text = formatValInvestor;
                
                
                NSMutableArray *cabangName = [[NSMutableArray alloc]init];
                for(NSDictionary *data in listCabang){
                    [cabangName addObject:[data objectForKey:@"name"]];
                }
                arLabel = (NSArray *) cabangName;
            }
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
    } else {
        
    }
}

- (void)errorLoadData:(NSError *)error{
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToR];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [arLabel count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arLabel objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    index = &row;
    
//    textStr = [arLabel objectAtIndex:row];
    _txtCabangRef.text = [arLabel objectAtIndex:row];
    data = [listCabang objectAtIndex:row];
    listParamData = [data objectForKey:@"cabang"];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag - 49 == [listParamData count]) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 101) {
        if ([textField.text isEqual: @""] && [arLabel count] > 0){
            textField.text = [arLabel objectAtIndex:0];
            listParamData = [[listCabang objectAtIndex:0] objectForKey:@"cabang"];
            data = [listCabang objectAtIndex:0];
        }
    }
}


- (IBAction)refreshCabang:(id)sender {
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

- (IBAction)nextOrder:(id)sender {
    if(_lblMinimalPemesanan.text != nil ||![_lblMinimalPemesanan.text isEqual:@""]){
        NSString *minPemesan, *kuotaInvestor, *nominalPemesanan, *kuotaNasional;
        minPemesan = [_lblMinimalPemesanan text];
        kuotaNasional = [_lblKuotaNasional text];
        kuotaInvestor = [_lblKuotaInvestor text];
        
        NSString *conNominal = _txtNominalPemesanan.text;
        NSString *stringWithoutCurrency = [conNominal stringByReplacingOccurrencesOfString:@"."   withString:@""];
        
        nominalPemesanan = stringWithoutCurrency;
        
        NSCharacterSet *xSperator = [NSCharacterSet characterSetWithCharactersInString:@"Rp."];
        minPemesan = [[minPemesan componentsSeparatedByCharactersInSet: xSperator] componentsJoinedByString: @""];
        kuotaInvestor = [[kuotaInvestor componentsSeparatedByCharactersInSet: xSperator] componentsJoinedByString: @""];
        kuotaNasional = [[kuotaNasional componentsSeparatedByCharactersInSet: xSperator] componentsJoinedByString: @""];
        nominalPemesanan = [[nominalPemesanan componentsSeparatedByCharactersInSet: xSperator] componentsJoinedByString: @""];
        
        long double inMinPesanan, inKuotaInvestor, inNomPemesanan, inKuotaNasional;
        inMinPesanan = [minPemesan longLongValue];
        inKuotaInvestor = [kuotaInvestor longLongValue];
        inNomPemesanan = [nominalPemesanan longLongValue];
        inKuotaNasional = [kuotaNasional longLongValue];
        
        if (inMinPesanan != 0 && inNomPemesanan !=0){
            if (inNomPemesanan >= inMinPesanan){
                if(inNomPemesanan <= inKuotaInvestor && inNomPemesanan <= inKuotaNasional){
                    if([data valueForKey:@"code"] != nil){
                        [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"code"] forKey:@"id_branch"];
                    }
                    if([data valueForKey:@"name"] != nil){
                        [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"name"] forKey:@"name_branch"];
                    }
                    [[[DataManager sharedManager]dataExtra]setObject:nominalPemesanan forKey:@"amount"];
                    double xValid = fmod(inNomPemesanan, valMultipleOrder);
                    if(xValid == 0 || xValid == 0.00){
                        [super openNextTemplate];
                    }else{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@ Rp %@",lang(@"NOMINAL_MULTIPLE"), [Utility formatCurrencyValue:valMultipleOrder withSeparator:@"."]] preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
//                }
                }else if(inNomPemesanan <= inKuotaInvestor){
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"KUOTA_NASIONAL_LIMIT") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }else if(inNomPemesanan <= inKuotaNasional){
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"KUOTA_INVESTOR_LIMIT") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"KUOTA_ORDER_LIMIT") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@ Rp %@",lang(@"LIMIT_MIN_ORDER"), [Utility formatCurrencyValue:valMultipleOrder withSeparator:@"."]] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MIN_ORDER_0") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}

- (IBAction)cancelOrder:(id)sender {
//    [self reloadApp];
     [super backToR];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
    long double newValue = [newText longLongValue];
    
    if ([newText length] == 0 || newValue == 0){
        textField.text = nil;
    }
    else{
        textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithLongLong:newValue]];
//        [self handlingInputUserText:textField.accessibilityLabel];
        return NO;
    }
    
    
    return YES;

}

@end
