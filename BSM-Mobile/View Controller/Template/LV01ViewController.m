//
//  LV01ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/8/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "LV01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "ECSlidingViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PopupOnboardingViewController.h"




@interface LV01ViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIScrollViewDelegate>{
    NSArray *listAction;
    NSArray *listOrigin;
    NSDictionary *dataObject;
    NSDictionary *dataLocal;
    BOOL first;
    BOOL fromHome;
    BOOL spesial;
    BOOL mustAddToManager;
    NSString *key;
    UIToolbar *toolBar;
    BOOL isShowing;
    NSUserDefaults *userDefault;
    BOOL isRequested, selectAnArray;
}

@property (weak, nonatomic) IBOutlet UIImageView *iconHeaderLV01;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightImage; // 15
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSeacrh; // 30
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightLine; // 1
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightView; // 40
@property (weak, nonatomic) IBOutlet UITextField *textSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopView;

@end

@implementation LV01ViewController

NSString* requestType;
extern NSString *lv_selected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)setFirstLV:(BOOL)aFirstLV{
    first = aFirstLV;
}

- (void)setFromHome:(BOOL)afromHome{
    fromHome = afromHome;
}

- (void)setSpecial:(BOOL)aspecial{
    spesial = aspecial;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault = [NSUserDefaults standardUserDefaults];
    isShowing = false;
    isRequested = false;
    selectAnArray = false;
    
    [Styles setTopConstant:_constraintTopView];
    
    self.textSearch.hidden = true;
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
    _textSearch.inputAccessoryView = toolBar;
    _textSearch.delegate = self;

    NSString *dt = [userDefault objectForKey:@"indexMenu"];
    if (dt) {
        [[DataManager sharedManager]resetObjectData];
        NSArray *temp = [[DataManager sharedManager] getJSONData:[dt intValue]];
        NSDictionary *object = [temp objectAtIndex:1];
        [self setJsonData:object];
        [userDefault removeObjectForKey:@"indexMenu"];
    }
    self.tableView.tableFooterView = [[UIView alloc] init];
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    _iconHeaderLV01.image = [UIImage imageNamed:img];
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
//    [_iconHeaderLV01 sizeToFit];
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    if(first){
        listAction = [self.jsonData objectForKey:@"action"];
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        [self.tableView reloadData];
    } else if (fromHome){
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        [dataManager setAction:[self.jsonData valueForKey:@"action"] andMenuId:@"00007"];
        [super openNextTemplate];
    } else if (spesial){
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        int ind = 0;
        NSArray *menu = [self.jsonData valueForKey:@"action"];
        for (int i =0; i < [menu count]; i++) {
            NSArray *selMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:i];
            if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
                ind = i;
                break;
            }
        }
        NSArray *selectedMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:ind];
        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
        [super openNextTemplate];

    }
    else {
        dataObject = [dataManager getObjectData];
        [self.labelTitle setText:[dataObject valueForKey:@"title"]];
        if([[dataObject valueForKey:@"url"]boolValue]){
            if([requestType isEqualToString:@"list_merchant"] ||
               [requestType isEqualToString:@"list_merchant2"] ||
               [requestType isEqualToString:@"list_layanan_pdam"] ||
               [requestType isEqualToString:@"list_debitcardtype"] ||
               [requestType isEqualToString:@"list_bank"] ||
               [requestType isEqualToString:@"list_account"] ||
               [requestType isEqualToString:@"list_denom"] ||
               [requestType isEqualToString:@"list_denom2"] ||
               [requestType isEqualToString:@"list_nazhir"] ||
               [requestType isEqualToString:@"list_nazhir2"] ||
               [requestType isEqualToString:@"list_wp_nazhir"] ||
               [requestType isEqualToString:@"list_sukuk"] ||
               [requestType isEqualToString:@"list_pe"] ||
               [requestType isEqualToString:@"list_currency"] ||
               [requestType isEqualToString:@"list_card"] ||
               [requestType isEqualToString:@"list_card2"] ||
               [requestType isEqualToString:@"list_deposito"] ||
               [requestType isEqualToString:@"list_deposito2"] ||
               [requestType isEqualToString:@"list_agent"] ||
               [requestType isEqualToString:@"gold_otp"] ||
               [requestType isEqualToString:@"list_card_otp"] ||
               [requestType isEqualToString:@"list_card2_otp"] ||
               [requestType isEqualToString:@"list_expirytime_otpcard"] ||
               [requestType isEqualToString:@"list_maxusage_otpcard"] ||
               [requestType isEqualToString:@"list_account_hajj"] ||
               [requestType isEqualToString:@"list_mpan"]||
               [requestType isEqualToString:@"paylater_kafalah"]){
                
                if([requestType isEqualToString:@"list_merchant"] || [requestType isEqualToString:@"list_merchant2"] ){
                    NSString *id_merchant = @"";
                    if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00013"]){// TELEPON
                        id_merchant = @"1";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00025"]) { // VOUCHER HP
                        id_merchant = @"2";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00020"]) { // RETAILER
                        id_merchant = @"3";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00016"]) { // ANGSURAN
                        id_merchant = @"4";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00017"]) { // PEMBAYARAN TIKET
                        id_merchant = @"5";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00015"]) { // TV KABEL
                        id_merchant = @"6";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00014"]) { // ACADEMIC
                        id_merchant = @"7";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00018"]) { // ASURANSI
                        id_merchant = @"8";
                    }
                    
                }

                if ([requestType isEqualToString:@"list_denom2"]) {
                    NSString *url =[NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }
                
                if([requestType isEqualToString:@"list_mpan"]){
                    NSString *url =[NSString stringWithFormat:@"%@,id_account",[self.jsonData valueForKey:@"url_parm"]];
                    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }
                
                if([requestType isEqualToString:@"list_account"]){
                    [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                }
                
                else {
                    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    NSString *param = [NSString stringWithFormat:@"%@",[self.jsonData valueForKey:@"url_parm"]];
                    [conn sendPostParmUrl:param needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }
            }
        } else{
            id data =  [dataObject objectForKey:@"content"];
            if([data isKindOfClass:[NSString class]]){
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alerts animated:YES completion:nil];
            }else{
                if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00009"] && [dataManager.dataExtra objectForKey:@"complain_state"]){
                    if([[dataManager.dataExtra valueForKey:@"complain_state"] isEqualToString:@"YES"]){
                        [dataManager.dataExtra addEntriesFromDictionary:@{@"list_trans":@20}];
                            [self openNextTemplate];
                    }
                }else{
                    mustAddToManager = true;
                    listAction = data;
                    [self.tableView reloadData];
                    [self deeplinkHandler];
                }
            }
        }
    }
}

- (void)doneAction:(id)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAction.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV01Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        [label setText:[data valueForKey:@"title"]];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            [label setText:[data valueForKey:@"title"]];
            if ([data valueForKey:@"card_status"]) {
                DLog(@"status");
            }
            
        }else if([data objectForKey:@"name"]){
            [label setText:[data valueForKey:@"name"]];
            
        }
    }
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = @"";
    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        text = [data valueForKey:@"title"];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            text = [data valueForKey:@"title"];
            
        }else if([data objectForKey:@"name"]){
            text = [data valueForKey:@"name"];
        }
    }
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+20.0;
}
#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
    NSString *byMenuID = @"";
    
    if(first){
        if([[[selectedMenu objectAtIndex:1]objectForKey:@"action"] isKindOfClass:[NSArray class]]){
            selectAnArray = true;
            
            NSDictionary *object = [selectedMenu objectAtIndex:1];
            NSString *templateName = [object valueForKey:@"template"];
            NSString *titleMenu = [object valueForKey:@"title"];
            titleMenu = [[[[[titleMenu stringByReplacingOccurrencesOfString:@" " withString:@"_"] stringByReplacingOccurrencesOfString:@")" withString:@"_"]stringByReplacingOccurrencesOfString:@"(" withString:@"_"]stringByReplacingOccurrencesOfString:@"/" withString:@"_"]stringByReplacingOccurrencesOfString:@"-" withString:@"_"];
    
            [userDefault setValue:titleMenu forKey:@"event_tracker"];
            
            TemplateViewController *templateView =   [self.storyboard instantiateViewControllerWithIdentifier:templateName];
            [templateView setJsonData:object];
            if([templateName isEqualToString:@"LV01"]){
                LV01ViewController *viewCont = (LV01ViewController *) templateView;
                [viewCont setFirstLV:true];
            }
            bool stateAcepted = [Utility vcNotifConnection: templateName];
            if (stateAcepted) {
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
                [alerts addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToRoot];
                }]];
                [self presentViewController:alerts animated:YES completion:nil];
            }else{
                [self.navigationController pushViewController:templateView animated:YES];
            }
        }else{
            [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
            DLog(@"%@",[selectedMenu objectAtIndex:0]);
            //Tabungan Mudharabah Dicek saat awal apakah sudah ada email
            if([[selectedMenu objectAtIndex:0]isEqualToString:@"00022"] || [[selectedMenu objectAtIndex:0]isEqualToString:@"00023"]){
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//                NSString *email = [userDefault objectForKey:@"email"];
                NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *alertTitle = @"";
                NSString *alertMessage = @"";
                if([lang isEqualToString:@"id"]){
                    alertTitle = @"Informasi";
                    alertMessage = @"Silahkan mengisi alamat email terlebih dahulu";
                } else {
                    alertTitle = @"Information";
                    alertMessage = @"Please fill in the email address first";
                }
                
                if ((email == nil) || ([email isEqualToString:@""])) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    alertView.tag = 5823;
                    [alertView show];
                }
            }
        }
    }
    if(mustAddToManager){
        //        NSString *keyTemp = @"";
        NSString *mnId = [self.jsonData valueForKey:@"menu_id"];
        NSString *value = @"";
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        BOOL haveAction = false;
        for(NSString *a in data.allKeys){
            
            if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00028"] && [a isEqualToString:@"type_transfer"]) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setObject:[data valueForKey:a] forKey:@"typeTransfer"];
            }
            
            if([a isEqualToString:@"action"]){
                haveAction = true;
            }else if(![a isEqualToString:@"title"] && ![a isEqualToString:@"name"]){
                key = a;
                value = [data valueForKey:key];
//                break;
                [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
                
                if([key isEqualToString:@"id_series"]){
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"series":[data valueForKey:@"title"]}];
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"linkMemo":[data valueForKey:@"linkMemo"]}];
                }
            }
            
        }
        
        lv_selected = value;
        [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
        if(haveAction){
            [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
            dataManager.currentPosition=-1;
        }
        
        if([[data valueForKey:@"code"]isEqualToString:@"00051"] || [[data valueForKey:@"code"]isEqualToString:@"00052"] ||
            [[data valueForKey:@"code"]isEqualToString:@"00053"] || [[data valueForKey:@"code"]isEqualToString:@"00054"]){
            [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"menu_name"] forKey:@"nama_menu"];
        }
        
        if([mnId isEqualToString:@"00087"]){
            if([data valueForKey:@"card_name"]){
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_no"] forKey:@"card_name"];
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_name"] forKey:@"card_name"];
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_type"] forKey:@"card_type"];
                DLog(@"Card Status : %@",[data valueForKey:@"card_status"]);
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_status"] forKey:@"card_status"];
            }
            
        }
        
        if ([mnId isEqualToString:@"00011"]) {
            [dataManager.dataExtra setValue:[data valueForKey:@"name"] forKey:@"name"];
        }
        
        if ([mnId isEqualToString:@"00070"] || [mnId isEqualToString:@"00060"]) {
            if ([data valueForKey:@"id_denom"]) {
                [dataManager.dataExtra setValue:[data valueForKey:@"id_denom"] forKey:@"amount"];
            }
        }
        
        if ([mnId isEqualToString:@"00092"]) {
            if ([data valueForKey:@"type_id"]) {
                [dataManager.dataExtra setValue:[data valueForKey:@"type_id"] forKey:@"type_id"];
                [dataManager.dataExtra setValue:[data valueForKey:@"type_name"] forKey:@"type_name"];
            }
        }
        
        if([mnId isEqualToString:@"00118"]){
            if([data valueForKey:@"aro_option"]){
                [dataManager.dataExtra setValue:[data valueForKey:@"aro_option"] forKey:@"aro_option"];
                [dataManager.dataExtra setValue:[data valueForKey:@"deposit_account"] forKey:@"deposit_account"];
            }
        }
        
        if([mnId isEqualToString:@"00070"]){
            if([data valueForKey:@"txt1"]){
                [dataManager.dataExtra setValue:[data valueForKey:@"txt1"] forKey:@"txt1"];
                [dataManager.dataExtra setValue:[data valueForKey:@"txt2"] forKey:@"txt2"];
                [dataManager.dataExtra setValue:[data valueForKey:@"txt3"] forKey:@"txt3"];
            }
        }
        
        if([mnId isEqualToString:@"00066"] && [[data valueForKey:@"service_provider"]isEqualToString:@"2"] ){
            byMenuID = [data valueForKey:@"merchant_id"];
        }
        
        if([mnId isEqualToString:@"00048"] && [[data valueForKey:@"service_provider"]isEqualToString:@"1"] ){
            byMenuID = [data valueForKey:@"sp_nazhir_id"];
            [dataManager.dataExtra setValue:[data valueForKey:@"nazhir_id"] forKey:@"nazhir_id"];
        }
        
        if([mnId isEqualToString:@"00116"]){
            if ([data valueForKey:@"kodeReservasi"]) {
                [dataManager.dataExtra setValue:[data valueForKey:@"kodeReservasi"] forKey:@"reservation_code"];
            }
        }
		if([mnId isEqualToString:@"00127"]){
            [dataManager.dataExtra setValue:[data valueForKey:@"expiry_time"] forKey:@"expiry_time"];
            [dataManager.dataExtra setValue:[data valueForKey:@"max_usage"] forKey:@"max_usage"];
            [dataManager.dataExtra setValue:[data valueForKey:@"card_no"] forKey:@"card_no"];
        }
        
        if([mnId isEqualToString:@"00184"]){
            [dataManager.dataExtra setValue:[data valueForKey:@"name"] forKey:@"name"];
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *last = [userDefault objectForKey:@"last"];
    if (![last isEqualToString:@"-"]) {
        [userDefault setValue:@"-" forKey:@"last"];
    }
    
    @try {
        DLog(@"Menu ID : %@", [self.jsonData valueForKey:@"menu_id"]);
        if (first){
            if ([[selectedMenu objectAtIndex:0] isEqualToString:@"00047"]){
                [super openNextTemplateAvoidLogin];
            }
            else if (selectAnArray){
                //do NOTHING
            }
            else{
                [super openNextTemplate];
            }
        }else if(![byMenuID isEqualToString:@""]){
            DLog(@"%@", [userDefault valueForKey:@"imgIcon"]);
            [self openTemplateByMenuID:byMenuID];
        }else{
           [super openNextTemplate];
        }
        
        
    } @catch (NSException *exception) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertView.tag = 999; //tag no have menu
        [alertView show];
    } 
    
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
//    if([requestType isEqualToString:@"list_account1"]){
    if([requestType isEqualToString:@"list_account2"]){

        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
            if(!isRequested){
                isRequested = YES;
                [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                
            }
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            if (!isShowing) {
                [alert show];
                isShowing = true;
            }
        }
        
    }
    
    if([requestType isEqualToString:@"list_agent"]){
        //nanti yg dikirim balik ke server paramnya { txt1 = id, txt2 = name, txt3 = adminFee }
        /**{"responsemsg":"Successful","responsecode":"00","info":[{"name":"ATM BSM","adminfee":"0","id":0},{"name":"Indomaret","adminfee":"5000","id":1}]}*/
        if([[jsonObject valueForKey:@"responsecode"] isEqualToString:@"00"]){
            NSArray *listData = (NSArray *)[jsonObject valueForKey:@"info"];
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in listData){
                [newData addObject:@{@"title":[temp valueForKey:@"name"],
                                     @"txt1":[temp valueForKey:@"id"],
                                     @"txt2":[temp valueForKey:@"name"],
                                     @"txt3":[temp valueForKey:@"adminfee"]
                }];
            }
            key = @"txt1";
            mustAddToManager = true;
            listAction = newData;
//            [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
            [self.tableView reloadData];
            return;
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            if (!isShowing) {
                [alert show];
                isShowing = true;
            }
        }
    }
    
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
                if ([requestType isEqualToString:@"list_denom2"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"response"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listData){
                        [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                    }
                    key = @"id_denom";
                    mustAddToManager = true;
                    listAction = newData;
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
                    [self.tableView reloadData];
                } else if ([requestType isEqualToString:@"list_denom"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"response"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listData){
                        [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                    }
                    key = @"id_denom";
                    mustAddToManager = true;
                    listAction = newData;
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
                    [self.tableView reloadData];
                }else if([requestType isEqualToString:@"gold_otp"]){
                    NSArray *listReservation = (NSArray *)[jsonObject valueForKey:@"response"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listReservation){
    //                        [newData addObject:@{@"title" : [NSString stringWithFormat:@"Kode Reservasi: %@\nkepingan: %@ Gram\nTotal Barat: %@ Gram\nTanggal Cetak: %@",[temp valueForKey:@"kodeReservasi"],[temp valueForKey:@"typeCetakan"],[temp valueForKey:@"totalBerat"],[temp valueForKey:@"tanggalRequestCetak"]],
    //                            @"kodeReservasi" : [temp valueForKey:@"kodeReservasi"]
    //                        }];
                        [newData addObject:@{
                            @"title" : [temp valueForKey:@"kodeReservasi"],
                            @"reservation_code" : [temp valueForKey:@"kodeReservasi"]
                        }];
                    }
                    key = @"reservation_code";
                    mustAddToManager = true;
                    listAction = newData;
                    [self.tableView reloadData];
                }
                //            else {
                //                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //                [alert show];
                //            }
            }
            else if([[jsonObject valueForKey:@"responsecode"] isEqualToString:@"00"]){
                if ([requestType isEqualToString:@"list_card_otp"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"info"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listData){
                        if([Utility isLanguageID]){
                            [newData addObject:@{@"title":[NSString stringWithFormat:@"%@\nOTP %@ kali pakai\nMasa berlaku %@ jam",[temp valueForKey:@"card_no"], [temp valueForKey:@"max_usage"],[temp valueForKey:@"expiry_time"]],
                                                 @"card_no":[temp valueForKey:@"card_no"],
                                                 @"expiry_time":[temp valueForKey:@"expiry_time"],
                                                 @"masked_cardno":[temp valueForKey:@"masked_cardno"],
                                                 @"max_usage":[temp valueForKey:@"max_usage"]
                            }];
                        }else{
                            [newData addObject:@{@"title":[NSString stringWithFormat:@"%@\nOTP %@ time use\nValid for %@ hours",[temp valueForKey:@"card_no"], [temp valueForKey:@"max_usage"],[temp valueForKey:@"expiry_time"]],
                                                 @"card_no":[temp valueForKey:@"card_no"],
                                                 @"expiry_time":[temp valueForKey:@"expiry_time"],
                                                 @"masked_cardno":[temp valueForKey:@"masked_cardno"],
                                                 @"max_usage":[temp valueForKey:@"max_usage"]
                            }];
                        }
                        
                    }
                    key = @"card_no";
                    mustAddToManager = true;
                    listAction = newData;
//                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
                    [self.tableView reloadData];
                }else if ([requestType isEqualToString:@"list_card2_otp"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"info"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listData){
                        if([Utility isLanguageID]){
                            @try{
                                [newData addObject:@{@"title":[NSString stringWithFormat:@"%@\n%@\nOTP %@ kali pakai\nMasa berlaku %@ jam",[temp valueForKey:@"info"],[temp valueForKey:@"card_no"], [temp valueForKey:@"max_usage"],[temp valueForKey:@"expiry_time"]],
                                                     @"card_no":[temp valueForKey:@"card_no"],
                                                     @"expiry_time":[temp valueForKey:@"expiry_time"],
                                                     @"masked_cardno":[temp valueForKey:@"masked_cardno"],
                                                     @"max_usage":[temp valueForKey:@"max_usage"],
                                                     @"info":[temp valueForKey:@"info"]
                                }];
                            }@catch (NSException *exception) {
                                NSLog(@"exception : %@", exception);
                            }
                            
                        }else{
                            @try {
                                [newData addObject:@{@"title":[NSString stringWithFormat:@"%@\n%@\nOTP %@ time use\nValid for %@ hours",[temp valueForKey:@"info"],[temp valueForKey:@"card_no"], [temp valueForKey:@"max_usage"],[temp valueForKey:@"expiry_time"]],
                                                     @"card_no":[temp valueForKey:@"card_no"],
                                                     @"expiry_time":[temp valueForKey:@"expiry_time"],
                                                     @"masked_cardno":[temp valueForKey:@"masked_cardno"],
                                                     @"max_usage":[temp valueForKey:@"max_usage"],
                                                     @"info":[temp valueForKey:@"info"]
                                }];
                            } @catch (NSException *exception) {
                                NSLog(@"exception : %@", exception);
                            }
                        }
                        
                    }
                    key = @"card_no";
                    mustAddToManager = true;
                    listAction = newData;
                    [self.tableView reloadData];
                    
                    if(listAction.count < 1 && [[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00127"]){
                        [self openRedirectTo:@"00155"
                                  withMenuID:YES
                                     andCode:@"00121"
                                    setTitle:[self.jsonData objectForKey:@"title"]
                                     andText:lang(@"REQUEST_REGIS_CARD")
                              andTitleButton:lang(@"REGIS_CARD_OTP")];
                    }
                }
            }else{
                if([requestType isEqualToString:@"list_sukuk"]){
                    UIAlertController *alet = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                    if([[jsonObject valueForKey:@"status"] isEqualToString:@"NEW"]){
                        [alet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"REGISTER", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            [self openTemplateByMenuID:@"00069" andCodeContent:@"00077"];
                        }]];
                    }else{
                        [alet addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            [self backToR];
                        }]];
                    }
                    [self presentViewController:alet animated:YES completion:nil];
                }else if([requestType isEqualToString:@"list_account_hajj"]){
                    if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
                        NSArray *listData = (NSArray *)[jsonObject valueForKey:OBJ_SPESIAL];
                        NSMutableArray *newData = [[NSMutableArray alloc]init];
                        newData = [self getListAccountFormatData:listData];
                        key = @"id_account";
                        mustAddToManager = true;
                        listAction = newData;
                        [self.tableView reloadData];
                    }else{
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        if (!isShowing) {
                            [alert show];
                            isShowing = true;
                        }
                    }
                }else if(![requestType isEqualToString:@"list_account2"]){
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    if (!isShowing) {
                        [alert show];
                        isShowing = true;
                    }
                }
                
            }
        }else{
            if([requestType isEqualToString:@"list_account"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                @try {
                    DLog(@"Menu ID : %@", [self.jsonData valueForKey:@"menu_id"]);
                    if (newData){
                        for(NSDictionary *temp in listData){
                            [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_account":[temp valueForKey:@"id_account"]}];
                        }
                    }
                    if ([newData count] == 1) {
                        NSDictionary *idAccount = [newData objectAtIndex:0];
                        lv_selected = [idAccount valueForKey:@"id_account"];
                        [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":[idAccount valueForKey:@"id_account"]}];
                        [super openNextTemplate];
                    }else{
                        mustAddToManager = true;
                        key = @"id_account";
                        listAction = newData;
                        [self.tableView reloadData];
                    }
                    
                } @catch (NSException *exception) {
//                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    alertView.tag = 999; //tag no have menu
//                    [alertView show];
                }
                
            } else if([requestType isEqualToString:@"list_merchant"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
                }
                if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00028"]){
                    key = @"id_merchant";
                }else if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00025"]){
                    key = @"id_denom";
                }else if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00092"]){
                    
                    self.constraintHeightLine.constant = 1;
                    self.constraintHeightView.constant = 40;
                    self.constraintHeightImage.constant = 15;
                    self.constraintHeightSeacrh.constant = 30;
                    self.textSearch.hidden = false;
                    [self.view layoutIfNeeded];
                    
                    key = @"id_merchant";
                }else{
                    key = @"code";
                }
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
                [self deeplinkHandler];
            } else if([requestType isEqualToString:@"list_merchant2"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],
                                         @"code":[temp valueForKey:@"code"],
                                         @"ext_data":[temp valueForKey:@"ext_data"],
                                         @"service_provider":[temp valueForKey:@"service_provider"],
                                         @"merchant_id":[temp valueForKey:@"merchant_id"]

                    }];
                }
                if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00028"]){
                    key = @"id_merchant";
                }else if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00025"]){
                    key = @"id_denom";
                }else if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00092"]){
                    
                    self.constraintHeightLine.constant = 1;
                    self.constraintHeightView.constant = 40;
                    self.constraintHeightImage.constant = 15;
                    self.constraintHeightSeacrh.constant = 30;
                    self.textSearch.hidden = false;
                    [self.view layoutIfNeeded];
                    
                    key = @"id_merchant";
                }else{
                    key = @"code";
                }
                mustAddToManager = true;
                listAction = newData;
                listOrigin = newData;
                [self.tableView reloadData];

                if(listAction.count == 1){
                    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
                }
                [self deeplinkHandler];
                
            }else if([requestType isEqualToString:@"list_bank"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                self.textSearch.hidden = false;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
                }
                key = @"code";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_nazhir"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                self.textSearch.hidden = false;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"name":[temp valueForKey:@"name"],@"nazhir_id":[temp valueForKey:@"nazhir_id"]}];
                }
                key = @"nazhir_id";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            }
            else if([requestType isEqualToString:@"list_nazhir2"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                self.textSearch.hidden = false;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{
                        @"title":[temp valueForKey:@"name"],
                        @"name":[temp valueForKey:@"name"],
                        @"nazhir_id":[temp valueForKey:@"nazhir_id"],
                        @"service_provider":[temp valueForKey:@"service_provider"],
                        @"sp_nazhir_id":[temp valueForKey:@"sp_nazhir_id"]
                    }];
                }
                key = @"nazhir_id";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            }
            else if([requestType isEqualToString:@"list_wp_nazhir"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                self.textSearch.hidden = false;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{
                        @"title":[temp valueForKey:@"name"],
                        @"name":[temp valueForKey:@"name"],
                        @"wp_id":[temp valueForKey:@"wp_id"],
                        @"description":[temp valueForKey:@"description"]
                    }];
                }
                key = @"wp_id";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            }
            else if([requestType isEqualToString:@"list_denom"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                }
                key = @"id_denom";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_denom2"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                }
                key = @"id_denom";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_expirytime_otpcard"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"expiry_time":[temp valueForKey:@"id_choice"]}];
                }
                key = @"expiry_time";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_maxusage_otpcard"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"max_usage":[temp valueForKey:@"id_choice"]}];
                }
                key = @"max_usage";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if ([requestType isEqualToString:@"list_sukuk"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [NSString stringWithFormat:@"%@",[temp valueForKey:@"name_series"]],
                                         @"id_series" : [NSString stringWithFormat:@"%@",[temp valueForKey:@"id_series"]],
                                         @"linkMemo" : [NSString stringWithFormat:@"%@",[temp valueForKey:@"linkMemo"]]
                                         }];
                }
                key = @"id_series";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if ([requestType isEqualToString:@"list_account_hajj"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [temp valueForKey:@"name"],
                                         @"id_account" : [temp valueForKey:@"id_account"]
                                         }];
                }
                key = @"id_account";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_pe"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [temp valueForKey:@"description"],
                                         @"pe_code" : [temp valueForKey:@"pe_code"]
                                         }];
                }
                key = @"pe_code";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_currency"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"currency_code"],[temp valueForKey:@"currency_name"]],
                                         @"currency_code" : [temp valueForKey:@"currency_code"]
                                         }];
                }
                key = @"currency_code";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_card"] || [requestType isEqualToString:@"list_card2"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [NSString stringWithFormat:@"%@\n%@\n%@",[temp valueForKey:@"card_no"],[temp valueForKey:@"card_name"],[temp valueForKey:@"card_type"]],
                                         @"card_no" : [temp valueForKey:@"card_no"],
                                         @"card_name" :[temp valueForKey:@"card_name"],
                                         @"card_type" : [temp valueForKey:@"card_type"],
                                         @"card_status" : [temp valueForKey:@"card_status"]
                                         }];
                }
                key = @"card_no";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }
            else if([requestType isEqualToString:@"list_mpan"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"name" : [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]],
                                         @"code" : [temp valueForKey:@"code"]
                                         }];
                }
                key = @"code";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }
            else if([requestType isEqualToString:@"list_layanan_pdam"] || [requestType isEqualToString:@"list_debitcardtype"]){
                NSArray *listDat = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listDat){
                    [newData addObject:@{@"name" : [NSString stringWithFormat:@"%@",[temp valueForKey:@"type_name"]],
                                         @"type_id" : [temp valueForKey:@"type_id"]
                                         }];
                }
                key = @"type_id";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            }
            else if([requestType isEqualToString:@"list_deposito"]){
               NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"name" : [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]],
                                         @"deposit_account" : [temp valueForKey:@"deposit_account"]
                                         }];
                }
                key = @"deposit_account";
                mustAddToManager = true;
                listAction = newData;
                
                if(listAction.count == 1){
                    NSDictionary *data = [listAction objectAtIndex:0];
                    [dataManager.dataExtra setValue:[data objectForKey:@"deposit_account"] forKey:@"deposit_account"];
//                    double delayInSeconds = 0.5;
//                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    [self openNextTemplate];
//                    });
                }else{
                    [self.tableView reloadData];
                }
            }else if([requestType isEqualToString:@"gold_otp"]){
                NSArray *listReservation = (NSArray *)[jsonObject valueForKey:@"response"];
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listReservation){
                    [newData addObject:@{
                        @"title" : [temp valueForKey:@"kodeReservasi"],
                        @"reservation_code" : [temp valueForKey:@"kodeReservasi"]
                    }];
                }
                key = @"reservation_code";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }
            else if([requestType isEqualToString:@"list_deposito2"]){
               NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"name" : [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]],
                                         @"deposit_account" : [temp valueForKey:@"deposit_account"],
                                         @"aro_option" : [temp valueForKey:@"aro_option"]
                                         }];
                }
                key = @"deposit_account";
                mustAddToManager = true;
                listAction = newData;
                
                if(listAction.count == 1){
                    NSDictionary *data = [listAction objectAtIndex:0];
                    [dataManager.dataExtra setValue:[data objectForKey:@"deposit_account"] forKey:@"deposit_account"];
                    [dataManager.dataExtra setValue:[data objectForKey:@"aro_option"] forKey:@"aro_option"];
//                    double delayInSeconds = 0.5;
//                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    [self openNextTemplate];
//                    });
                }else{
                    [self.tableView reloadData];
                }
            }
            else if([requestType isEqualToString:@"paylater_kafalah"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                newData = [self getListAccountFormatData:listData];
                key = @"id_account";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 657) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == 657) {
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        
        if ([substring length] > 0) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for (NSDictionary *temp in listOrigin){
                NSRange substringRange = [[[temp objectForKey:@"title"] lowercaseString] rangeOfString:[substring lowercaseString]];
                if (substringRange.length != 0) {
                    [newData addObject:temp];
                }
            }

            listAction = (NSArray *)newData;
            [self.tableView reloadData];
        } else {
            listAction = listOrigin;
            [self.tableView reloadData];
        }
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        self.constraintBottomTable.constant = 216;
        [self.view layoutIfNeeded];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        self.constraintBottomTable.constant = 0;
        [self.view layoutIfNeeded];
    }
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
//        [super openActivation];
//        [self backToRoot];
        UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ECSlidingViewController *firstVC = [ui instantiateViewControllerWithIdentifier:@"FirstVC"];
        firstVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:firstVC animated:NO completion:^{
            PopupOnboardingViewController *viewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
            [firstVC presentViewController:viewCont animated:YES completion:nil];
        }];
       
    } else if (alertView.tag == 5823) {
        NSDictionary* userInfo = @{@"position": @(713)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    } else {
        [self backToRoot];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    DLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}

-(void) getListAccountFromPersitance : (NSString *) urlParam
                           strMenuId : (NSString *) menuId{
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct = nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
            if ([dataManager.dataExtra objectForKey:@"code"]) {
                if ([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
               
        }
    }
    
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];

            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct != nil) {
        isRequested = true;
        if(listAcct.count == 0){
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki nomor rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account number for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                       message:mes
                                       preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                [self backToR];
            }];

            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            newData = [self getListAccountFormatData:listAcct];
//            for(NSDictionary *temp in listAcct){
//                NSString *titl = @"";
//                if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
//                    if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
//                        titl = [NSString stringWithFormat:@"%@ - %@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"], [temp valueForKey:@"altname"]];
//                    }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
//                        titl = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
//                    }else{
//                        titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
//                    }
//                }
//                else{
//                    titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
//                }
//                [newData addObject:@{@"title":titl,@"id_account":[temp valueForKey:@"id_account"]}];
//            }
            
            if ([newData count] == 1) {
                NSDictionary *idAccount = [newData objectAtIndex:0];
                lv_selected = [idAccount valueForKey:@"id_account"];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":[idAccount valueForKey:@"id_account"]}];
                
                double delayInSeconds = 0.1;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    [self openNextTemplate];
                });
            }else{
                mustAddToManager = true;
                key = @"id_account";
                listAction = newData;
                [self.tableView reloadData];
            }
        }
        
    }else{
        if (!isRequested) {
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];

            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
        }else{
            
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                       message:mes
                                       preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                [self backToR];
            }];

            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void) deeplinkHandler{
    if([userDefault objectForKey:@"deeplink_nodeindex"]){
        NSArray *arr = [userDefault objectForKey:@"deeplink_nodeindex"];
        
        DLog(@"%lu", (unsigned long)arr.count);
        if(arr.count > 2){
            int idxCode = [[[userDefault objectForKey:@"deeplink_nodeindex"]objectAtIndex:2]intValue];
            [userDefault removeObjectForKey:@"deeplink_nodeindex"];
            [userDefault synchronize];
            
            if(listAction.count > idxCode){
                [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:idxCode inSection:0]];
            }
        }
        [userDefault removeObjectForKey:@"deeplink_nodeindex"];
    }else if([userDefault objectForKey:@"deeplink_servicecode"]){

        int idxCode = 0;
        for(NSDictionary *dict in listAction){
            if([[dict objectForKey:@"code"]isEqualToString:[userDefault objectForKey:@"deeplink_servicecode"]]){
                lv_selected = [dict valueForKey:@"code"];

                [userDefault synchronize];
                [userDefault removeObjectForKey:@"deeplink_servicecode"];
                
                [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:idxCode inSection:0]];
            }
            idxCode = idxCode + 1;
        }
    }
}

- (NSMutableArray*)getListAccountFormatData:(NSArray*)listAcct{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listAcct){
        NSString *titl = @"";
        if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
            if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                titl = [NSString stringWithFormat:@"%@ - %@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"], [temp valueForKey:@"altname"]];
            }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                titl = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else{
                titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
            }
        }
        else{
            titl = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
        }
        [newData addObject:@{@"title":titl,@"id_account":[temp valueForKey:@"id_account"],@"name":[temp valueForKey:@"name"]}];

    }
    
    return newData;
}


@end
