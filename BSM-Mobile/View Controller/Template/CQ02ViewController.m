//
//  CQ02ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 22/06/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CQ02ViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface CQ02ViewController ()<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *toolbarTitle;
    UIToolbar *toolbar;
    
//    NSArray *listOfQurbanType, *listDateQurban;
    NSArray *listDataAmil, *data;
    UIPickerView *amilPickerView;

    NSNumberFormatter *currencyFormatter;

    BOOL stateCheck1, stateCheck2;
}


@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *IconBar;
@property (weak, nonatomic) IBOutlet UILabel *labelAccountNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelTypeOfQurban;
@property (weak, nonatomic) IBOutlet UILabel *labelTargetSaving;
@property (weak, nonatomic) IBOutlet UILabel *labelJatuhTempo;
@property (weak, nonatomic) IBOutlet UILabel *labelAnimalSelect;
@property (weak, nonatomic) IBOutlet UITextField *tfAnimalSelect;
@property (weak, nonatomic) IBOutlet UILabel *labelNamaPengurban;
@property (weak, nonatomic) IBOutlet UITextField *tfNamaPengurban;
@property (weak, nonatomic) IBOutlet UILabel *lblNophone;
@property (weak, nonatomic) IBOutlet UITextField *tfNoPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
//@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
//@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIView *vwAgreement1;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck1;
@property (weak, nonatomic) IBOutlet UILabel *labelAgreement1;
@property (weak, nonatomic) IBOutlet UIView *vwAgreement2;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck2;
@property (weak, nonatomic) IBOutlet UILabel *labelAgreement2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@end

@implementation CQ02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    self.labelTitleBar.text = [self.jsonData objectForKey:@"title"];
    
    currencyFormatter = [[NSNumberFormatter alloc]init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [self setupLanguage];
    [self setupCheckEvent];
    [self setupStyle];
    
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url =[NSString stringWithFormat:@"%@,language",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
    
    if([language isEqualToString:@"id"])
    {
        self.labelAgreement1.text = @"Nasabah menyatakan setuju apabila saat jatuh tempo dana qurban yang terkumpul pada rekening di atas tidak memenuhi harga, maka Nasabah setuju Bank tidak melaksanakan pemesanan hewan qurban.";
        self.labelAgreement2.text = @"Nasabah setuju nomor handphone dan alamat email di atas diberikan kepada penjual/pelaksana hewan qurban.";
    }else{
        self.labelAgreement1.text = @"The customer has agreed that if the qurban funds collected in the above account do not meet the price due, the Customer agrees the Bank does not carry out the ordering of sacrificial animals.";
        self.labelAgreement2.text = @"The customer agrees that the mobile number and email address above are given to the qurban animal seller / organizer.";
    }
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    amilPickerView = [[UIPickerView alloc]initWithFrame: CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    amilPickerView.showsSelectionIndicator = YES;
    amilPickerView.delegate = self;
    amilPickerView.dataSource = self;
    
    self.tfAnimalSelect.delegate = self;
    self.tfAnimalSelect.inputView = amilPickerView;
    self.tfAnimalSelect.inputAccessoryView = toolbar;
    
    self.tfNamaPengurban.delegate = self;
    self.tfNamaPengurban.inputAccessoryView = toolbar;
    
    self.tfEmail.delegate = self;
    self.tfEmail.inputAccessoryView = toolbar;
    
    self.tfNoPhone.delegate = self;
    self.tfNoPhone.inputAccessoryView = toolbar;
    self.tfNoPhone.keyboardType = UIKeyboardTypePhonePad;
    
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.IconBar setHidden:YES];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    [self registerForKeyboardNotifications];

}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.bottomConstraint.constant = 30 + keyboardSize.height - self.vwScroll.safeAreaInsets.bottom;

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height/2 +30, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.bottomConstraint.constant =  30;

    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) setupLanguage{
    if([language isEqualToString:@"id"]){
        _labelAccountNumber.text = @"Nomor Rekening : ";
        _labelTypeOfQurban.text = @"Jenis Qurban : ";
        _labelTargetSaving.text = @"Target Tabungan : ";
        _labelJatuhTempo.text = @"Jatuh Tempo : ";
        
        _labelAnimalSelect.text = @"Pilih hewan qurban yang akan dipesan";
        _tfAnimalSelect.placeholder = @"Pilih jenis hewan qurban yang ingin dipesan";
        
        _labelNamaPengurban.text = @"Qurban atas nama";
        _tfNamaPengurban.placeholder = @"Tuliskan nama pengurban";
        
        _lblNophone.text = @"No. handphone untuk laporan qurban";
        _tfNoPhone.placeholder = @"Tuliskan nomor handphone anda";
        
        _lblEmail.text = @"Email untuk laporan qurban";
        _tfEmail.placeholder = @"Tuliskan alamat email anda";
        
        [self.btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        
    }else{
        
        _labelAccountNumber.text = @"Account Number : ";
        _labelTypeOfQurban.text = @"Type of Qurban : ";
        _labelTargetSaving.text = @"Saving Target : ";
        _labelJatuhTempo.text = @"Due Date: ";
        
        _labelAnimalSelect.text = @"Select qurban you want to order";
        _tfAnimalSelect.placeholder = @"Select type of qurban animal you want to order";
        
        _labelNamaPengurban.text = @"Qurban on behalf of";
        _tfNamaPengurban.placeholder = @"Write qurban on behalf of name";
        
        _lblNophone.text = @"No. handphone for qurban reporting";
        _tfNoPhone.placeholder = @"Write your handphone number";
        
        _lblEmail.text = @"Email for qurban reporting";
        _tfEmail.placeholder = @"Qrite your email address";
        
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
}



- (void) setupCheckEvent{
    UITapGestureRecognizer *tapCheck1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap1:)];
    UITapGestureRecognizer *tapCheck2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap2:)];

    [self.vwAgreement1 setUserInteractionEnabled:YES];
    [self.vwAgreement2 setUserInteractionEnabled:YES];
    
    [self.vwAgreement1 addGestureRecognizer:tapCheck1];
    stateCheck1 = NO;
    [self.vwAgreement2 addGestureRecognizer:tapCheck2];
    stateCheck2 = NO;
}

- (void)handleTap1:(UITapGestureRecognizer *)sender
{
//    UIView *view = (UIView*) sender;
//    if (view.tag == 1)
//    {
        if(!stateCheck1){
            [self.imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_active"]];
            stateCheck1 = YES;
        }else{
            [self.imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_inactive"]];
            stateCheck1 = NO;
        }
//    }
//    if (view.tag == 2)
//    {
//
//    }
//
}

-(void)handleTap2:(UITapGestureRecognizer *)sender{
    if(!stateCheck2){
        [self.imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_active"]];
        stateCheck2 = YES;
    }else{
        [self.imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_inactive"]];
        stateCheck2 = NO;
    }
}

- (void) setupStyle{
    [Styles setTopConstant:_topConstraint];
    
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfAnimalSelect imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBottomLine:_tfNamaPengurban];
    [Styles setStyleTextFieldBottomLine:_tfNoPhone];
    [Styles setStyleTextFieldBottomLine:_tfEmail];
    
    [Styles setStyleButton:_btnCancel];
    [Styles setStyleButton:_btnNext];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self.tfAnimalSelect setText: [listDataAmil[row] objectForKey:@"pilihan"]];
    
    [dataManager.dataExtra setValue:[listDataAmil[row] objectForKey:@"name"] forKey:@"name_amil"];
    [dataManager.dataExtra setValue:[listDataAmil[row] objectForKey:@"id_amil"] forKey:@"id_amil"];
    [dataManager.dataExtra setValue:[listDataAmil[row] objectForKey:@"price"] forKey:@"price"];
    [dataManager.dataExtra setValue:[listDataAmil[row] objectForKey:@"price"] forKey:@"price_amil"];
    [dataManager.dataExtra setValue:[listDataAmil[row] objectForKey:@"pilihan"] forKey:@"pilihan"];
    [dataManager.dataExtra setValue:[listDataAmil[row] objectForKey:@"pilihan"] forKey:@"desc_amil"];

}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listDataAmil.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [listDataAmil[row] objectForKey:@"pilihan"];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (IBAction)actionCancel:(id)sender {
    [self backToR];
//    [self backPrevious];
}
- (IBAction)actionNext:(id)sender {
    /*
    String msisdn = ParseJsonRequest("msisdn", false);
    String email = ParseJsonRequest("email", false);
    String name = ParseJsonRequest("name", false);
     */

    [dataManager.dataExtra setValue:self.tfEmail.text forKey:@"email"];
    [dataManager.dataExtra setValue:self.tfNamaPengurban.text forKey:@"name"];
    [dataManager.dataExtra setValue:self.tfNoPhone.text forKey:@"msisdn"];
    [dataManager.dataExtra setValue:self.tfNoPhone.text forKey:@"misdn"];

    if([self validate] & stateCheck1 & stateCheck2){
        [self openNextTemplate];
    }else{
        if(!stateCheck2 || !stateCheck1){
            NSString *msg = @"";
            if([language isEqualToString:@"id"]){
                msg = @"Checkmark harus diklik";
            }else{
                msg = @"Checkmark must be clicked";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (BOOL)validate{
    NSString *msg = @"";
    if([self.tfAnimalSelect.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            msg = @"Pilih Hewan Qurban terlebih dulu";
        }else{
            msg = @"Please Select Sacrifice Animal first";
        }
    }else if([self.tfNamaPengurban.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            msg = @"Nama Pengurban Belum Diisi";
        }else{
            msg = @"Enter Name";
        }
    }else if([self.tfNoPhone.text isEqualToString:@""] && self.tfNoPhone.text.length < 9){
        if([language isEqualToString:@"id"]){
            msg = @"Lengkapi Nomor handphone yang dapat dihubungi";
        }else{
            msg = @"Fill Phone Number for qurban report";
        }
    }else if([self.tfEmail.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            msg = @"Lengkapi Email untuk pelaporan hewan qurban";
        }else{
            msg = @"Enter Email address for qurban report";
        }
    }
    
    if(![msg isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return NO;
    }
    
    return YES;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"get_amil_qurban"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
//            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[respon dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

            NSArray *listAmilResponse = [object objectForKey:@"list_amil"];
            NSArray *dataResponse = [object objectForKey:@"data"];
            
            if(dataResponse.count > 0){
                listDataAmil = listAmilResponse;
                data = dataResponse;
                
                [dataManager.dataExtra setValue:[data[0] objectForKey:@"jenis_qurban"] forKey:@"jenis_qurban"];
                [dataManager.dataExtra setValue:[data[0] objectForKey:@"srcacctno"] forKey:@"srcacctno"];
                
                self.labelAccountNumber.text = [NSString stringWithFormat:@"%@ %@", self.labelAccountNumber.text, [data[0] objectForKey:@"acctno"]];
                self.labelTypeOfQurban.text = [NSString stringWithFormat:@"%@ %@", self.labelTypeOfQurban.text, [data[0] objectForKey:@"jenis_qurban"]];
                
                self.labelTargetSaving.text = [NSString stringWithFormat:@"%@ Rp. %@", self.labelTargetSaving.text, [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[[data[0] objectForKey:@"target_amount"] doubleValue]]]];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateConverted = [dateFormatter dateFromString:[data[0] objectForKey:@"jatuh_tempo"]];

                [dateFormatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:language?@"id":@"en"]];
                [dateFormatter setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString = [dateFormatter stringFromDate:dateConverted];
                self.labelJatuhTempo.text = [NSString stringWithFormat:@"%@ %@", self.labelJatuhTempo.text, dateString];
            }else{
                NSString * msg = @"";
                if([language isEqualToString:@"id"]){
                    msg = @"Data tidak ditemukan";
                }else{
                    msg = @"No data found";
                }
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
        }else{
            NSString * msg = [jsonObject valueForKey:@"response"];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

@end
