//
//  RCPTViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/21/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "TemplateViewController.h"

@interface RCPTViewController : TemplateViewController
- (void)setDataLocal:(NSDictionary*)object;
- (void)setFromRecipt:(BOOL)recipt;
@end
