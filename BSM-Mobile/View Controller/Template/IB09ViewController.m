//
//  IB09ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 20/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "IB09ViewController.h"

@interface IB09ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;

@end

@implementation IB09ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [Styles setTopConstant:_topSpace];
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    [_btnNext setColorSet:PRIMARYCOLORSET];
    
    [_titleBar setText:[self.jsonData valueForKey:@"title"]];
    
    _titleBar.textColor = UIColorFromRGB(const_color_title);
    _titleBar.textAlignment = const_textalignment_title;
    _titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    _titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_btnNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [_btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];

    [Styles setStyleTextFieldBottomLine:_textField];
    [_lblTitle setText:[self.jsonData valueForKey:@"content"]];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    _textField.inputAccessoryView = keyboardDoneButtonView;
    
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"paylater_kafalah"]){
        NSString *resp_code = [jsonObject valueForKey:@"response_code"];
        if([resp_code isEqualToString:@"00"]){
            
            [dataManager.dataExtra setValue:[jsonObject valueForKey:@"transaction_id_limit"] forKey:@"transaction_id_limit"];
            
        }else if([resp_code isEqualToString:@"02"]){
            
            NSString *menuid = [jsonObject valueForKey:@"response"];
            NSString *code = [jsonObject valueForKey:@"code"];
            [self openTemplateByMenuID:menuid andCodeContent:code];
            
        }else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void) actionNext{
    if([_textField.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        [Utility showMessage:lang(@"NOT_EMPTY") instance:self];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldInput = [_textField.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        
        if ([trimmedStringtextFieldInput length]) {
            message = lang(@"STR_NOT_NUM");
        }
        
        if([message isEqualToString:@""]){
            [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.textField.text}];
            [self openNextTemplate];
        } else {
            [Utility showMessage:message instance:self];
        }
    }}

- (void) actionCancel{
    [self backToR];
}

@end
