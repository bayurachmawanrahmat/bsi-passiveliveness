//
//  SKCF04ViewController.m
//  BSM-Mobile
//
//  Created by Macbook Air on 15/05/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SKCF04ViewController.h"
#import "CustomCollectionViewLayout.h"
#import "CollectionViewCell.h"

@interface SKCF04ViewController() <UICollectionViewDelegate, UICollectionViewDataSource, ConnectionDelegate, UIAlertViewDelegate>
{
    NSMutableArray *listHistory;
    NSMutableArray *listHeader;
    int numSection;
    int numRow;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtConfirmHistory;

@property (weak, nonatomic) IBOutlet UITableView *tblHistory;

@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UICollectionView *customCollection;

@property (weak, nonatomic) IBOutlet UIView *vwTitle;

@end

@implementation SKCF04ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];


    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    listHistory = [[NSMutableArray alloc]init];
    listHeader = [[NSMutableArray alloc]init];
    numSection = 0;
    numRow = 0;
    
    [self.txtConfirmHistory setEditable:NO];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"identifierCell"];

    [[CustomCollectionViewLayout alloc]prepareLayout];
    [self setupLayout];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
                NSString *response = [jsonObject objectForKey:@"response"];
                NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                self.txtConfirmHistory.text = [dict valueForKey:@"text"];
                listHeader = (NSMutableArray *)[dict valueForKey:@"header"];
                NSArray *arTable = (NSArray *) [dict valueForKey:@"table"];
                NSMutableArray *marTable = [NSMutableArray new];
                for(NSDictionary *dictData in arTable){
                    [marTable addObject:[dictData valueForKey:@"data"]];
                }
                
                listHistory = marTable;
                
                if(listHistory.count > 0){
                    numSection = ((int) [listHistory count]) + 1;
                    numRow = ((int) [listHeader count]) + 1;
                    [self.collectionView reloadData];
                }

            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                
                [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            NSLog(@"%@", jsonObject);
        }
         [self setupLayout];
        
    }
    
}

- (void)errorLoadData:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToR];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}

- (void)reloadApp {
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return numSection;
}


- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return numRow;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
        CollectionViewCell *cell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"identifierCell" forIndexPath:indexPath];
        //cell.backgroundColor=[UIColor greenColor];
        cell.lblContent.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor whiteColor];
        if ((indexPath.section % 2) != 0) {
            cell.backgroundColor = [UIColor lightGrayColor];
        }

        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                cell.lblContent.text = @"No";
            } else {
                if (indexPath.row <= [listHeader count]) {
                    cell.lblContent.text = listHeader[(indexPath.row-1)];
                }
            }
        } else {
            if (indexPath.row == 0) {
                cell.lblContent.text = [NSString stringWithFormat:@" %ld", (long) indexPath.section];
            } else {
                if (indexPath.section <= ([listHistory count]+1)) {
                    if (indexPath.row <= [listHeader count]) {
                        NSArray *tempArray = listHistory[(indexPath.section-1)];
                        cell.lblContent.text = tempArray[(indexPath.row-1)];
                    }
                }
            }
        }
    
        return cell;
}





-(void)setupLayout{
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmtxtConfirm = self.txtConfirmHistory.frame;
    CGRect frmVwBtn = self.btnConfirm.frame;
    CGRect frCollection = self.collectionView.frame;

    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.height = 40;
    
    frmtxtConfirm.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmtxtConfirm.origin.x = 16;

    frCollection.origin.y = frmtxtConfirm.origin.y + frmtxtConfirm.size.height ;
    frCollection.origin.x = 16;
    frCollection.size.width = SCREEN_WIDTH - (frCollection.origin.x*2);
    
    frmVwBtn.size.height = 40;
    frmVwBtn.origin.x = 16;
    frmVwBtn.size.width = SCREEN_WIDTH - (frmVwBtn.origin.x*2);
    frmVwBtn.origin.y = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV) - 20;
    
    if ([Utility isDeviceHaveNotch]) {
        frCollection.size.height = SCREEN_HEIGHT - (BOTTOM_NAV+TOP_NAV+frmVwTitle.size.height+frmtxtConfirm.size.height+frmVwBtn.size.height+90);
        frmVwBtn.origin.y = SCREEN_HEIGHT - (frmVwBtn.size.height + BOTTOM_NAV) - 60;

    }
    
    self.vwTitle.frame = frmVwTitle;
    self.lblTitle.frame = frmLblTitle;
    self.txtConfirmHistory.frame = frmtxtConfirm;
    self.btnConfirm.frame = frmVwBtn;
    self.collectionView.frame = frCollection;

}
- (IBAction)btnConfirm:(id)sender {
     [super backToR];
    
}

@end
