//
//  LV09ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 10/10/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "LV09ViewController.h"
#import "CellLV09.h"
#import "Utility.h"
#import "Connection.h"
#import "UIImageView+AFNetworking.h"


@interface LV09ViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate> {
    NSArray *listAction;
    NSArray *listOrigin;
    NSDictionary *dataObject;
    NSDictionary *dataLocal;
    BOOL mustAddToManager;
    NSString *key;
    UIToolbar *toolBar;
    NSString* requestType;
    NSString *lv_selected;
    NSString *urlwp;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *iconHeaderLV09;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation LV09ViewController
//
//extern NSString* requestType;
//extern NSString *lv_selected;
//extern NSString *urlwp;
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *dt = [userDefault objectForKey:@"indexMenu"];
    if (dt) {
        [[DataManager sharedManager]resetObjectData];
        NSArray *temp = [[DataManager sharedManager] getJSONData:[dt intValue]];
        NSDictionary *object = [temp objectAtIndex:1];
        [self setJsonData:object];
        [userDefault removeObjectForKey:@"indexMenu"];
    }

//    NSString *img = [userDefault objectForKey:@"imgIcon"];
//    _iconHeaderLV09.image = [UIImage imageNamed:img];
//    _iconHeaderLV09.contentMode = UIViewContentModeScaleToFill;
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    
    dataObject = [dataManager getObjectData];
    [self.labelTitle setText:[dataObject valueForKey:@"title"]];
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"CellLV09" bundle:nil]
    forCellReuseIdentifier:@"CELL09LV"];
//    self.tableView setauto
//    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedRowHeight = 220; // set to whatever your "average" cell height is

    if([[dataObject valueForKey:@"url"]boolValue]){
        if([requestType isEqualToString:@"list_wp"]) {
            NSString *url = [NSString stringWithFormat:@"%@",[self.jsonData valueForKey:@"url_parm"]];
            NSString *mid = [self.jsonData valueForKey:@"menu_id"];
            [dataManager setMenuId:mid];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
    }
    else {
        id data =  [dataObject objectForKey:@"content"];
        if([data isKindOfClass:[NSString class]]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alert show];
        }
        else {
            mustAddToManager = true;
            listAction = data;
            [self.tableView reloadData];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAction.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellLV09 *cell = (CellLV09*)[tableView dequeueReusableCellWithIdentifier:@"CELL09LV"];

    NSDictionary *data = [listAction objectAtIndex:indexPath.row];
    
//    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
//    singleTap.numberOfTapsRequired = 1;
//    singleTap.numberOfTouchesRequired = 1;
//    [imgView setUserInteractionEnabled:YES];
//    [imgView addGestureRecognizer:singleTap];
                                
    [cell.labelTitle setText:[data valueForKey:@"name"]];
    [cell.imgShow setImageWithURL:[NSURL URLWithString:[data valueForKey:@"imgurl"]]];
    cell.imgShow.layer.cornerRadius = 10;
    cell.imgShow.layer.masksToBounds = YES;
    
    CGFloat width = cell.imgShow.image.size.width;
    CGFloat height = cell.imgShow.image.size.height;
    
    cell.imgHeight.constant = (height/width)*(SCREEN_WIDTH-8);
    
    return cell;
}

-(void)tapDetected:(UITapGestureRecognizer*)sender{
    CGPoint location = [sender locationInView:_tableView];
    NSIndexPath *ipath = [_tableView indexPathForRowAtPoint:location];
    NSDictionary *data = [listAction objectAtIndex:ipath.row];
    NSString *urlweb;
    if (([[data valueForKey:@"website"] isEqualToString:@""]) || ([data valueForKey:@"website"] == nil)){
        urlweb = @"";
    }
    else {
        urlweb = [data valueForKey:@"website"];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlweb]];
    }
}



#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
//    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
    
    if(mustAddToManager){
        NSString *value = @"";
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        BOOL haveAction = false;
        for(NSString *a in data.allKeys){
            if([a isEqualToString:@"action"]){
                haveAction = true;
            }else if(![a isEqualToString:@"name"] && ![a isEqualToString:@"description"] && ![a isEqualToString:@"imgurl"]){
                key = a;
                value = [data valueForKey:key];
                break;
            }
        }
        
        lv_selected = value;
        [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
        if(haveAction){
            [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
            dataManager.currentPosition=-1;
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *last = [userDefault objectForKey:@"last"];
    if (![last isEqualToString:@"-"]) {
        [userDefault setValue:@"-" forKey:@"last"];
    }
    
    [super openNextTemplate];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                if ([requestType isEqualToString:@"list_wp"]) {
                    NSArray *listData = (NSArray *)jsonObject;
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    
                    for(NSDictionary *temp in listData){
                        [newData addObject:@{@"name":[temp valueForKey:@"name"],@"description":[temp valueForKey:@"description"],@"imgurl":[temp valueForKey:@"imgurl"],@"wp_id":[temp valueForKey:@"wp_id"],@"website":[temp valueForKey:@"website"]}];
                    }
                    key = @"wp_id";
                    mustAddToManager = true;
                    listAction = newData;
                    [self.tableView reloadData];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            }
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        if([requestType isEqualToString:@"list_wp"]){
            NSArray *listData = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];

            NSString *webwp;
            for(NSDictionary *temp in listData){
                NSString *nmwp = [temp valueForKey:@"name"];
                NSString *dscwp = [temp valueForKey:@"description"];
                NSString *uwp = [temp valueForKey:@"imgurl"];
                NSString *wid = [temp valueForKey:@"wp_id"];
                if ([temp valueForKey:@"website"] == nil) {
                    webwp = @"";
                }
                else {
                    webwp = [temp valueForKey:@"website"];
                }
                [newData addObject:@{@"name":nmwp,@"description":dscwp,@"imgurl":uwp,@"wp_id":wid,@"website":webwp}];
            }
            key = @"wp_id";
            mustAddToManager = true;
            listAction = newData;
            [self.tableView reloadData];
        }
    }
}
//
//- (void)errorLoadData:(NSError *)error{
//
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
//}
//
//- (void)reloadApp{
//    [self backToRoot];
//    [BSMDelegate reloadApp];
//}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}

//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//
//    NSDictionary* userInfo = @{@"position": @(1112)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
//}

@end
