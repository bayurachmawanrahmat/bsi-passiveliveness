//
//  TGINFViewController.m
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 26/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGINFViewController.h"
#import "NSString+HTML.h"

@interface TGINFViewController (){
    NSString *state;
    NSUserDefaults *userDefault;
    NSString *language;
    int stepPhase;
}

@end

@implementation TGINFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   

    self.titleBar.lineBreakMode = NSLineBreakByWordWrapping;
    self.titleBar.numberOfLines = 0;
    _titleBar.text = [self.jsonData valueForKey:@"title"];
    stepPhase = 0;
    
    _btnLink1.layer.borderWidth = 0.0;
    _btnLink2.layer.borderWidth = 0.0;
    [_btnCancel.layer setCornerRadius:20.0f];
    [_btnCancel.layer setBorderWidth:1.0f];
    [_btnCancel.layer setBorderColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
    [_btnCancel.layer setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor];
    [_btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [_btnNext.layer setCornerRadius:20.0f];
    [_btnNext.layer setBorderWidth:1.0f];
    [_btnNext.layer setBorderColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
    [_btnNext setEnabled:false];
    
    
    _btnLink1.layer.borderColor = [UIColor whiteColor].CGColor;
    

    
    [self.btnLink1 addTarget:self action:@selector(actionAgree:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnLink2 addTarget:self action:@selector(actionAgree:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCheck addTarget:self action:@selector(actionAgree:) forControlEvents:UIControlEventTouchUpInside];
    
    [_btnLink1 setBackgroundImage:[UIImage imageNamed:@"solid_white"] forState:UIControlStateSelected];
    [_btnLink1 setBackgroundImage:[UIImage imageNamed:@"solid_white"] forState:UIControlStateNormal];
    [_btnCheck setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    [_btnCheck setImage:[UIImage imageNamed:@"blank_check"]  forState:UIControlStateNormal];
    
    
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    
    if([language isEqualToString:@"id"]){
        [self.btnNext setTitle:@"SETUJU" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"BELUM" forState:UIControlStateNormal];
    }else{
        [self.btnNext setTitle:@"APPLY" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"NOT YET" forState:UIControlStateNormal];
    }
}

- (void)actionCancel{
    [self backToR];
}

- (void)actionNext{
    [self openNextTemplate];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    state = @"info";
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=topup_griya,step=info,menu_id,device,device_type,ip_address,language,date_local,customer_id,nid,app_no" needLoading:true encrypted:true banking:true favorite:nil];
    }

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"topup_griya"]){
        if([state isEqualToString:@"info"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                NSString *responses = [jsonObject objectForKey:@"response"];
                NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[responses dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                NSString *info = [dataDict objectForKey:@"info"];
                NSString *disclaim = [dataDict objectForKey:@"checkbox"];
                
                self.lblConten.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblConten.numberOfLines = 0;
                
                self.lblConten.attributedText = [Utility htmlAtributString:info];
                [self.lblConten setFont:[UIFont systemFontOfSize:16.0]];
                [self.lblConten sizeToFit];
                
                self.lblcheck.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblcheck.numberOfLines = 0;
                self.lblcheck.attributedText = [Utility htmlAtributString:disclaim];
                self.lblcheck.textAlignment = NSTextAlignmentJustified;
                [self.lblcheck setFont:[UIFont systemFontOfSize:14.0]];
                [self.lblcheck sizeToFit];
                
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self backToR];
                }]];
                if (@available(iOS 13.0, *)) {
                    [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
}

-(void)actionAgree: (UIButton *) sender{
 if(sender == self.btnCheck){
     [self.btnCheck setSelected:!self.btnCheck.isSelected];
 }else if(sender == self.btnLink1){
     [self.btnLink1 setSelected:YES];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.bankbsi.co.id/produk&layanan/digital-banking/1638941312griya-hasanah-online-top-up-melalui-bsi-mobile"]];
 }else if(sender == self.btnLink2){
     [self.btnLink2 setSelected:YES];
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.bankbsi.co.id/produk&layanan/digital-banking/1638941312griya-hasanah-online-top-up-melalui-bsi-mobile"]];
 }
    
    if(stepPhase == 0){
        if (self.btnCheck.isSelected && self.btnLink1.isSelected && self.btnLink2.isSelected) {
            [_btnNext setEnabled:true];
        }else{
            [_btnNext setEnabled:false];
        }
    }
}

@end


