//
//  LV10ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 21/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "LV10ViewController.h"
#import "CellCardBasic.h"

@interface LV10ViewController ()<UITableViewDelegate, UITableViewDataSource, ConnectionDelegate, UIAlertViewDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    NSArray *dataListQR;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UITableView *tblListQR;


@end

@implementation LV10ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataListQR = [[NSArray alloc]init];
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];

    [self setupLayout];
    
    self.tblListQR.delegate = self;
    self.tblListQR.dataSource = self;
    [self.tblListQR setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [self.tblListQR registerClass:[CellCardBasic class] forCellReuseIdentifier:@"cell"];
    self.tblListQR.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSDictionary *dataObject = [dataManager getObjectData];
    [self.lblTitle setText:[dataObject valueForKey:@"title"]];

    //update style to amanah
    [self.lblTitle setTextColor:UIColorFromRGB(const_color_title)];
    [self.vwTitle setBackgroundColor:[UIColor whiteColor]];
    [self.lblTitle sizeToFit];
    
    if (![[dataObject valueForKey:@"url"]boolValue]) {
        NSArray *arrDataContent = (NSArray *) [dataObject valueForKey:@"content"];
        if (arrDataContent.count > 0) {
            dataListQR = arrDataContent;
            [self.tblListQR reloadData];
        }
    }
    
}

-(void) setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmTblList = self.tblListQR.frame;
    
    CGRect frmLblTitle = self.lblTitle.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmTblList.origin.x = frmVwTitle.origin.x;
    frmTblList.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmTblList.size.width = SCREEN_WIDTH;
    frmTblList.size.height = SCREEN_HEIGHT - frmTblList.origin.y - BOTTOM_NAV_DEF - 16;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = frmVwTitle.size.height/2 - frmLblTitle.size.height/2;
    frmLblTitle.size.width = SCREEN_WIDTH - (frmLblTitle.origin.x*2);
    
    self.vwTitle.frame = frmVwTitle;
    self.tblListQR.frame = frmTblList;
    self.lblTitle.frame = frmLblTitle;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellCardBasic *cell = (CellCardBasic *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dataContent = [dataListQR objectAtIndex:indexPath.row];
    [cell.lblTitle setText:[dataContent valueForKey:@"name"]];
    [cell.lblTitle sizeToFit];
    
    if ([[dataContent valueForKey:@"code"] isEqualToString:@"00101"]) {
        [cell.imgIcon setImage:[UIImage imageNamed:@"qr_bulat2"]];
    }else{
        [cell.imgIcon setImage:[UIImage imageNamed:@"qr_bulat"]];
    }
    cell.imgIcon.layer.shadowColor = [[UIColor grayColor]CGColor];
    cell.imgIcon.layer.shadowOffset = CGSizeMake(0, 2);
    cell.imgIcon.layer.shadowOpacity = 0.5f;
    
    CGRect frmCardView = cell.cardView.frame;
    CGRect frmImgIcon = cell.imgIcon.frame;
    CGRect frmLblText = cell.lblTitle.frame;
    CGRect frmImgNext = cell.imgNext.frame;
    
    frmCardView.origin.x = 16;
    frmCardView.origin.y = 0;
    frmCardView.size.width = cell.frame.size.width - (frmCardView.origin.x*2);
    
    frmImgIcon.size.width = 42;
    frmImgIcon.size.height = 42;
    frmImgIcon.origin.x = 8;
    frmImgIcon.origin.y = 8;
    
    frmCardView.size.height = frmImgIcon.size.height + (frmImgIcon.origin.y *2);
    
    frmLblText.origin.x = frmImgIcon.origin.x + frmImgIcon.size.width + 8;
    frmLblText.origin.y = frmCardView.size.height/2 - frmLblText.size.height/2;
    
    frmImgNext.size.height = 24;
    frmImgNext.size.width = 24;
    frmImgNext.origin.x = frmCardView.size.width - frmImgNext.size.width - 16;
    frmImgNext.origin.y = frmCardView.size.height/2 - frmImgNext.size.height/2;
    
    frmLblText.size.width = frmImgNext.origin.x - frmLblText.origin.x - 8;
    
    cell.cardView.frame = frmCardView;
    cell.imgIcon.frame = frmImgIcon;
    cell.lblTitle.frame = frmLblText;
    cell.imgNext.frame = frmImgNext;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataListQR.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 66;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *dataContent = [dataListQR objectAtIndex:indexPath.row];

    [dataManager.dataExtra addEntriesFromDictionary:@{@"code":[dataContent valueForKey:@"code"]}];
    [dataManager setAction:[dataContent objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
    dataManager.currentPosition=-1;
    @try {
        [super openNextTemplate];
    } @catch (NSException *exception) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertView.tag = 999; //tag no have menu
        [alertView show];
    }
    
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self backToRoot];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
