//
//  LD01ViewController.h
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

@interface LD01ViewController : TemplateViewController

- (void)setDataLocal:(NSDictionary*)object;
- (void)setFirstLV:(BOOL)firstLV;
- (void)setFromHome:(BOOL)fromHome;
- (void)setSpecial:(BOOL)special;
@property (nonatomic, retain) NSString * menu_file;
@end
