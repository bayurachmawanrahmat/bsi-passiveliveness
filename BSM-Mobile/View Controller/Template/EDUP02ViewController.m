//
//  EDUP02ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 19/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "EDUP02ViewController.h"
#import "Utility.h"

@interface EDUP02ViewController ()<ConnectionDelegate, UIAlertViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    NSArray *arInstansi;
    NSArray *listDataInstansi;
    NSDictionary *dataDictSelected;
    NSNumberFormatter* currencyFormatter;
    
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBtal;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSetuju;

@end

@implementation EDUP02ViewController

-(void)loadView{
    [super loadView];
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwButton = self.vwButton.frame;
    
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmContentScroll = self.vwContentScroll.frame;
    CGRect frmBtnBatal = self.btnBtal.frame;
    CGRect frmBtnNext = self.btnSetuju.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV_DEF;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmVwButton.origin.x = 0;
    frmVwButton.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_DEF - frmVwButton.size.height;
    frmVwButton.size.width = SCREEN_WIDTH;
    frmVwButton.size.height = 40;
    
    if ([Utility isDeviceHaveNotch]) {
        frmVwTitle.origin.y = TOP_NAV;
        frmVwButton.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_NOTCH - frmVwButton.size.height;
    }
    
    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmVwScroll.origin.x = 0;
    frmVwScroll.size.width = SCREEN_WIDTH;
    frmVwScroll.size.height = frmVwButton.origin.y - frmVwScroll.origin.y;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = frmVwTitle.size.height/2 - frmLblTitle.size.height/2;
    frmLblTitle.size.width = frmVwTitle.size.width - (frmLblTitle.origin.x*2);
    
    frmContentScroll.origin.x = 0;
    frmContentScroll.origin.y = 0;
    frmContentScroll.size.width = frmVwScroll.size.width;
    
    frmBtnBatal.origin.x = 8;
    frmBtnBatal.origin.y = 0;
    frmBtnBatal.size.width = frmVwButton.size.width/2 - (frmBtnBatal.origin.x*2);
    frmBtnBatal.size.height = 40;
    
    frmBtnNext.origin.y = 0;
    frmBtnNext.origin.x = frmBtnBatal.origin.x + frmBtnBatal.size.width + 16;
    frmBtnNext.size.width = frmBtnBatal.size.width;
    frmBtnNext.size.height = 40;
    
    self.vwTitle.frame = frmVwTitle;
    self.vwScroll.frame = frmVwScroll;
    self.vwButton.frame = frmVwButton;
    
    self.lblTitle.frame = frmLblTitle;
    self.vwContentScroll.frame = frmContentScroll;
    self.btnBtal.frame = frmBtnBatal;
    self.btnSetuju.frame = frmBtnNext;
    
    [self.btnBtal setColorSet:SECONDARYCOLORSET];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self setupForm];
    
    [self relocatePosition];
    [[self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1] setHidden:YES];
    
    
    [self.vwContentScroll addSubview:[self addingViewTable]];
    [[self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1] setHidden:YES];
    [self.vwContentScroll bringSubviewToFront:[self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1]];
    
    [self.btnBtal addTarget:self action:@selector(actionBatal:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnSetuju addTarget:self action:@selector(actionNext:) forControlEvents:UIControlEventTouchUpInside];
    
    self.lblTitle.text = [self.jsonData valueForKey:@"title"];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    dataDictSelected = [[NSDictionary alloc]init];
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [self registerForKeyboardNotifications];
    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

- (void) setupForm{

    NSArray *arInput = [[NSArray alloc]init];
    NSString *ttlKeyboard;

    if ([lang isEqualToString:@"id"]) {
        arInput = @[@{@"tag" : @"1",
                      @"name" : @"Nama Institusi",
                      @"placholder" : @"Pilih Institusi"},
                    @{@"tag" : @"2",
                      @"name" : @"Masukan ID Pelanggan/Kode Bayar",
                      @"placholder" : @""},
                    @{@"tag" : @"3",
                      @"name" : @"Jumlah",
                      @"placholder" : @""}];
        ttlKeyboard = @"Selesai";
        [_btnBtal setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Setuju" forState:UIControlStateNormal];
    }else{
        arInput = @[@{@"tag" : @"1",
                      @"name" : @"Institusion Name",
                      @"placholder" : @"Select Institusion"},
                    @{@"tag" : @"2",
                      @"name" : @"Enter Customer ID / Pay Code",
                      @"placholder" : @""},
                    @{@"tag" : @"3",
                      @"name" : @"Amount",
                      @"placholder" : @""}];
        ttlKeyboard = @"Done";
        [_btnBtal setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Agree" forState:UIControlStateNormal];
    }
    
    if([[self.jsonData valueForKey:@"content"]containsString:@"|"]){
        /** Based on Content in menu JSON*/
        NSArray *arrContent = [[self.jsonData valueForKey:@"content"]componentsSeparatedByString:@"|"];
        if(arrContent.count == 2){
            if ([lang isEqualToString:@"id"]) {
                arInput = @[@{@"tag" : @"1",
                              @"name" : arrContent[0],
                              @"placholder" : @""},
                            @{@"tag" : @"2",
                              @"name" : arrContent[1],
                              @"placholder" : @""},
                            @{@"tag" : @"3",
                              @"name" : @"Jumlah",
                              @"placholder" : @""}];
            }else{
                arInput = @[@{@"tag" : @"1",
                              @"name" : arrContent[0],
                              @"placholder" : @""},
                            @{@"tag" : @"2",
                              @"name" : arrContent[1],
                              @"placholder" : @""},
                            @{@"tag" : @"3",
                              @"name" : @"Amount",
                              @"placholder" : @""}];
            }
        }
    }
    
    
    for(int i=0; i<[arInput count]; i++){
        NSDictionary *dataContent = [arInput objectAtIndex:i];
        UIView *vwTextInput = [self vwTxtInput:[[dataContent valueForKey:@"tag"]integerValue] strTitle:[dataContent valueForKey:@"name"] strPlaceHolder:[dataContent valueForKey:@"placholder"] strKeyboardTitle:ttlKeyboard vwContentScroll:self.vwContentScroll];
        [self.vwContentScroll addSubview:vwTextInput];
        
    }
}

-(UIView *) addingViewTable {
    UIView *vwTxtPilihan = (UIView *) [self.vwContentScroll.subviews objectAtIndex:0];
    
    UIView *vwTable = [[UIView alloc]initWithFrame:CGRectMake(0, vwTxtPilihan.frame.origin.y + vwTxtPilihan.frame.size.height, self.vwContentScroll.frame.size.width, 200)];
    UITableView *tblEdu = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, vwTable.frame.size.width, 165)];
    [tblEdu registerClass:[UITableViewCell class]forCellReuseIdentifier:@"cell"];
    tblEdu.delegate = self;
    tblEdu.dataSource = self;
    [tblEdu setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    [vwTable addSubview:tblEdu];
    return vwTable;
}

-(UIView *) vwTxtInput : (NSInteger) tag
              strTitle : (NSString *) mTitle
        strPlaceHolder : (NSString *) mPlaceHolder
      strKeyboardTitle : (NSString *)ttlKeyboard
       vwContentScroll : (UIView *) vwParent{
    UIView *vwBase = [[UIView alloc]init];
    
    UILabel *lblTitleTxtInput = [[UILabel alloc]init];
    UITextField *txtInput = [[UITextField alloc]init];
    UIView *vwLine = [[UIView alloc]init];
    
    lblTitleTxtInput.text = mTitle;
    lblTitleTxtInput.tag = tag;
    [lblTitleTxtInput setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    lblTitleTxtInput.textColor = [UIColor blackColor];
    [lblTitleTxtInput sizeToFit];
    
    txtInput.tag  = tag;
    if (tag == 3){
        txtInput.keyboardType = UIKeyboardTypeNumberPad;
    }else {
        txtInput.keyboardType = UIKeyboardTypeDefault;
    }
    txtInput.placeholder = mPlaceHolder;
    [txtInput setFont:[UIFont fontWithName:@"HelveticaNeue" size:15]];
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:ttlKeyboard style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    txtInput.delegate = self;
    txtInput.inputAccessoryView = toolBar;
    if (tag ==1) {
        NSLog(@"%@", @"down show deleting");
    }else{
        txtInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    
    [vwLine setBackgroundColor:[UIColor lightGrayColor]];
    
    [vwBase addSubview:lblTitleTxtInput];
    [vwBase addSubview:txtInput];
    [vwBase addSubview:vwLine];
    
    CGRect frmVwBase = vwBase.frame;
    CGRect frmLblTitle = lblTitleTxtInput.frame;
    CGRect frmTxtInput = txtInput.frame;
    CGRect frmVwLine = vwLine.frame;
    
    frmVwBase.origin.x = 16;
    frmVwBase.origin.y = 0;
    frmVwBase.size.width = vwParent.frame.size.width - (frmVwBase.origin.x*2);
    
    frmLblTitle.origin.x = 0;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.width = frmVwBase.size.width;
    
    frmTxtInput.origin.x = 0;
    frmTxtInput.origin.y = frmLblTitle.origin.y + frmLblTitle.size.height + 8;
    frmTxtInput.size.height = 40;
    frmTxtInput.size.width = frmVwBase.size.width;
    
    frmVwLine.origin.x = 0;
    frmVwLine.origin.y = frmTxtInput.origin.y + frmTxtInput.size.height + 1;
    frmVwLine.size.height = 1;
    frmVwLine.size.width = frmVwBase.size.width;
    
    frmVwBase.size.height  = frmVwLine.origin.y + frmVwLine.size.height + 8;
    
    vwBase.frame = frmVwBase;
    lblTitleTxtInput.frame = frmLblTitle;
    txtInput.frame = frmTxtInput;
    vwLine.frame = frmVwLine;
    
    
    return vwBase;
}

-(void)relocatePosition{
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    for(int i=0; i < [self.vwContentScroll.subviews count]; i++){
        UIView *currentView = [self.vwContentScroll.subviews objectAtIndex:i];
    
        CGRect frmCurrentView = currentView.frame;
        frmCurrentView.origin.y = 0;
        
        if (i >= 1 ) {
            UIView *lastView = [self.vwContentScroll.subviews objectAtIndex:i-1];
            frmCurrentView.origin.y = lastView.frame.origin.y + lastView.frame.size.height + 8;
        }
        
        currentView.frame = frmCurrentView;
        
        
    }
   
    UIView *vwLast = [self.vwContentScroll.subviews objectAtIndex:[self.vwContentScroll.subviews count]-1];
    frmVwContentScroll.size.height = vwLast.frame.origin.y + vwLast.frame.size.height + 24;
     self.vwContentScroll.frame = frmVwContentScroll;
    
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height + kbSize.height + 30)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height/2, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if ([requestType isEqualToString:@"list_inst2"]) {
        arInstansi = (NSArray *) jsonObject;
    }
    
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(void) doneClicked :  (UIButton *) sender{
    UIView *vwTable = [self.vwContentScroll.subviews objectAtIndex:self.vwContentScroll.subviews.count - 1];
    if (![vwTable isHidden]) {
        [vwTable setHidden:YES];
    }
    [self.view endEditing:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    NSString *txtLabel;
    @try {
         NSDictionary *dataInstansi = [listDataInstansi objectAtIndex:indexPath.row];
         txtLabel = [self strInstitusi:dataInstansi];
    } @catch (NSException *exception) {
        txtLabel = @"";
    }
    
    cell.textLabel.text = txtLabel;
    cell.textLabel.font = [UIFont fontWithName:const_font_name1 size:14];
//    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
    
    return cell;
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listDataInstansi.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 36.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *dataInstansi = [listDataInstansi objectAtIndex:indexPath.row];
    NSString *txtLabel = [self strInstitusi:dataInstansi];
    
    UIView *vwTxtPilihLayanan = [self.vwContentScroll.subviews objectAtIndex:0];
    UIView *vwTable = [self.vwContentScroll.subviews objectAtIndex:self.vwContentScroll.subviews.count - 1];
    
    [vwTable setHidden:YES];
    UITextField *txtPilihLayanan = (UITextField *) [vwTxtPilihLayanan.subviews objectAtIndex:1];
    txtPilihLayanan.text = txtLabel;
    [self makeVisibleAmount:[[dataInstansi valueForKey:@"payflag"]integerValue]];
    [self.view endEditing:YES];
    dataDictSelected = dataInstansi;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 1) {
        UITableView *tblInstansi = [self tblInstansi:1];
        
        if ([textField.text isEqualToString:@""]) {
            listDataInstansi = arInstansi;
        }else{
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in arInstansi){
                NSString *txtLabel = [self strInstitusi:temp];
            
                if ([txtLabel rangeOfString:textField.text].location != NSNotFound) {
                    [newData addObject:temp];
                }
            }
            
            listDataInstansi = (NSArray *)newData;
        }
        
        [tblInstansi reloadData];
        
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if (textField.tag == 1) {
        UITableView *tblInstansi = [self tblInstansi:1];
        
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        if ([substring length] > 0) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for (NSDictionary *temp in arInstansi){
                NSString *txtLabel = [self strInstitusi:temp];
                NSRange substringRange = [[txtLabel lowercaseString] rangeOfString:[substring lowercaseString]];
                if (substringRange.length != 0) {
                    [newData addObject:temp];
                }
                //            if ([temp rangeOfString:substring].location != NSNotFound) {
                //                [newData addObject:temp];
                //            }
            }
            
            listDataInstansi = (NSArray *)newData;
        } else {
            listDataInstansi = arInstansi;
        }
        [tblInstansi reloadData];
    }else if(textField.tag == 3){
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        double newValue = [newText doubleValue];
        
        if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
        }
        else{
            if (textField.tag == 10) {
                NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
                textField.text = newText;
            }else{
                textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
            }
            return NO;
            
        }
    }
    
    
     return YES;
}

-(void)actionBatal : (UIButton *) sender{
    [self backToRoot];
}

-(void)actionNext : (UIButton *) sender{
    BOOL mState = true;
//    if ([[dataDictSelected objectForKey:@"payflag"]integerValue] == 1) {
//        UIView *vwNominal = [self.vwContentScroll.subviews objectAtIndex:2];
//        UITextField *txtNominal = (UITextField *) [vwNominal.subviews objectAtIndex:1];
//        if ([txtNominal.text integerValue] != 0) {
//            NSString *strNominal = [txtNominal.text stringByReplacingOccurrencesOfString:@"," withString:@""];
//            [dataManager.dataExtra setValue:strNominal forKey:@"amount"];
//        }else{
//            mState = false;
//        }
//
//    }
    
    UIView *vwPilihLayanan = [self.vwContentScroll.subviews objectAtIndex:0];
    UIView *vwNomor = [self.vwContentScroll.subviews objectAtIndex:1];
    
    UITextField *tfNomor = [vwNomor.subviews objectAtIndex:1];
    UITextField *textPilihLayanan = [vwPilihLayanan.subviews objectAtIndex:1];
    
    if (![tfNomor.text isEqualToString:@""] && ![textPilihLayanan isEqual:@""]) {
        NSArray *arPrefix = [[Utility stringTrimmer:textPilihLayanan.text] componentsSeparatedByString:@"-"];
        NSString *paymentId = [NSString stringWithFormat:@"%@%@", arPrefix[0], [Utility stringTrimmer:tfNomor.text]];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"payment_id": [paymentId stringByReplacingOccurrencesOfString:@" " withString:@""]}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"merchant_code":[arPrefix[0] stringByReplacingOccurrencesOfString:@" " withString:@""]}];
    }else{
        mState = false;
    }
    
    if (mState) {
        
        if ([[dataDictSelected objectForKey:@"payflag"]integerValue] == 1) {
             [self openNextTemplate];
        }else{
            [dataManager.dataExtra setValue:@"0" forKey:@"amount"];
            dataManager.currentPosition++;
            [self openNextTemplate];
        }
        
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void) makeVisibleAmount : (NSInteger ) payFlag{
    UIView *vwAmount = [self.vwContentScroll.subviews objectAtIndex:2];
    
//    if (payFlag == 1) {
//        [vwAmount setHidden:NO];
//    }else{
//        [vwAmount setHidden:YES];
//    }
     [vwAmount setHidden:YES];
}

-(UITableView *) tblInstansi : (NSInteger) modeVisiblity {
    UIView *vwTable = [self.vwContentScroll.subviews objectAtIndex:self.vwContentScroll.subviews.count - 1];
    UIView *vwEndText = [self.vwContentScroll.subviews objectAtIndex:self.vwContentScroll.subviews.count - 2];
    UITableView *tblInstansi = (UITableView *) [vwTable.subviews objectAtIndex:0];
    
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    if (modeVisiblity == 1) {
         [vwTable setHidden:NO];
        frmVwContentScroll.size.height = vwTable.frame.origin.y + vwTable.frame.size.height + 32;
    }else{
         [vwTable setHidden:YES];
        frmVwContentScroll.size.height = vwEndText.frame.origin.y + vwEndText.frame.size.height + 16;
    }
    self.vwContentScroll.frame = frmVwContentScroll;
    self.vwScroll.contentSize = self.vwContentScroll.frame.size;
    return tblInstansi;
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else {
        [self backToRoot];
    }
}

-(NSString *) strInstitusi : (NSDictionary *) mDictData{
    
    NSString *strNameEdu = [Utility stringTrimmer:[mDictData valueForKey:@"name"]];
    NSString *strPrefix = [Utility stringTrimmer: [mDictData valueForKey:@"prefix"]];
    NSArray *arNameEdu = [strNameEdu componentsSeparatedByString:@"-"];
    NSString *txtLabel = @"";
    
    if (arNameEdu.count > 0) {
        if (arNameEdu.count == 1) {
            txtLabel = [NSString stringWithFormat:@"%@ - %@", strPrefix, [Utility stringTrimmer:arNameEdu[0]]];
        }else if(arNameEdu.count == 2){
            txtLabel = [NSString stringWithFormat:@"%@ - %@", strPrefix, [Utility stringTrimmer:arNameEdu[1]]];
        }else{
            for (int i = 0; i <= [arNameEdu count]; i++) {
                NSMutableString* string1 = [[NSMutableString alloc] init];
                if (i != 0) {
                    [string1 appendString: [arNameEdu[1] stringByReplacingOccurrencesOfString:@"  " withString:@""]];
                }
                txtLabel = [NSString stringWithString:string1];
            }
        }
    }
    return txtLabel;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
