//
//  CR03ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CR03ViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "NSString+HTML.h"
#import <QuartzCore/QuartzCore.h>

@interface CR03ViewController (){
    NSUserDefaults *userDefaults;
    NSString *language;
    NSArray *listData;
    BOOL isValidtoGo;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;

@property (weak, nonatomic) IBOutlet UIImageView *imgIconIfno;
@property (weak, nonatomic) IBOutlet UILabel *titleContent;
//@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIScrollView *vwAccept;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContent;
@property (weak, nonatomic) IBOutlet UIView *vwFrameContent;

@property (weak, nonatomic) IBOutlet UIScrollView *vwRejection;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconReject;
@property (weak, nonatomic) IBOutlet UILabel *lblRejection;

@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@end

@implementation CR03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    listData = [[NSArray alloc]init];
    
    self.vwRejection.hidden = YES;
    self.vwAccept.hidden = YES;
    
    self.titleBar.text = [self.jsonData objectForKey:@"title"];
    self.imgIconBar.image = [UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]];
    
    [self.vwFrameContent.layer setBorderColor:[[UIColor blackColor]CGColor]];
    [self.vwFrameContent.layer setBorderWidth:1];
    [self.vwFrameContent.layer setCornerRadius:10];
    
    [Styles setTopConstant:self.topConstant];
    [Styles setStyleButton:self.btnNext];
    
    if([language isEqualToString:@"id"]){
        [self.btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
    }else{
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
    }
    
//    isValidtoGo = NO;
    [self enabledButton:self.btnNext set:NO];
    
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self doRequest];
}

- (void) actionNext{
//    if(isValidtoGo){
        [self openNextTemplate];
//    }else{
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Cek lis" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void) createList{
    int lastY = 8;
    int i = 0;
    for(NSDictionary *data in listData){
        UIImageView *imageRadio  = [[UIImageView alloc]initWithFrame:CGRectMake(4, lastY, 20, 20)];
        [imageRadio setImage: [UIImage imageNamed:@"ic_checkbox_active.png"]];
        UILabel *labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(28, lastY, (self.vwContent.frame.size.width - 32), 0)];
        [labelTitle setText:[data objectForKey:@"title"]];
        [labelTitle setNumberOfLines:0];
        [labelTitle sizeToFit];
        [labelTitle setFont:[UIFont fontWithName:@"Lato-Bold" size:15]];
        [labelTitle setTextColor:[UIColor blackColor]];
  
        lastY =  lastY + [Utility heightLabelDynamic:labelTitle sizeWidth:(self.vwContent.frame.size.width - 32) sizeFont:15] + 4;

        UILabel *labelDesc = [[UILabel alloc]initWithFrame:CGRectMake(28, lastY, (self.vwContent.frame.size.width - 32), 0)];
        
        NSError* error;
        NSString* source = [data valueForKey:@"info"];
        NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];
        NSMutableAttributedString *desc = [[NSMutableAttributedString alloc]initWithString:[data valueForKey:@"description"] attributes:@{NSFontAttributeName : [UIFont fontWithName:@"Lato-Bold" size:15], NSForegroundColorAttributeName : UIColorFromRGB(0x00BFFF)}];
        NSMutableAttributedString* str = [[NSMutableAttributedString alloc]
              initWithData: [source dataUsingEncoding:NSUTF8StringEncoding]
                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
        documentAttributes: nil
                     error: &error];
        
        [final appendAttributedString:desc];
        [final appendAttributedString:str];
        
        [labelDesc setAttributedText:final];
        [labelDesc setNumberOfLines:0];
        [labelDesc sizeToFit];

        if(![[data objectForKey:@"link"] isEqualToString:@""]){
            UITapGestureRecognizer *gotoURL =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(loadURL:)];
            [labelDesc addGestureRecognizer:gotoURL];
            [labelDesc setTag:i];
            [labelDesc setUserInteractionEnabled:YES];
        }else{
            [labelDesc setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
            [labelDesc setTextColor:[UIColor blackColor]];

        }
        
        [self.vwContent addSubview:imageRadio];
        [self.vwContent addSubview:labelTitle];
        [self.vwContent addSubview:labelDesc];
        
        lastY =  lastY + [Utility heightLabelDynamic:labelDesc sizeWidth:(self.vwContent.frame.size.width - 32) sizeFont:15] + 8;
        i += 1;
    }
    
    self.heightContent.constant = lastY + 20;
}

-(void) loadURL:(UITapGestureRecognizer*) sender{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:[[listData objectAtIndex:sender.view.tag]objectForKey:@"link"]];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
//            self->isValidtoGo = YES;
            [self enabledButton:self.btnNext set:YES];
        }
    }];
}

- (void) enabledButton:(UIButton*)button set:(BOOL)state{
    if(state){
        button.backgroundColor = UIColorFromRGB(const_color_primary);
        button.enabled = true;
    }else{
        button.backgroundColor = UIColorFromRGB(const_color_gray);
        button.enabled = false;
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"cashfin_requested"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            /**
                {"response":"{\"info\":\"{\\\"caption\\\":\\\"Mitraguna\\\",\\\"data\\\":[{\\\"title\\\":\\\"Sesuai Prinsip Syariah\\\",\\\"description\\\":\\\"Akad Musyarakah Mutanaqishah\\\",\\\"link\\\":\\\"https://mandirisyariah.co.id/cashfin-requested\\\"},{\\\"title\\\":\\\"Angsuran Ringan\\\",\\\"description\\\":\\\"Margin dan hanya dikenakan biaya administrasi, materai, dan asuransi\\\",\\\"link\\\":\\\"\\\"},{\\\"title\\\":\\\"Berbagai Pilihan Plafond dan Tenor\\\",\\\"description\\\":\\\"Plafond pengajuan s.d [plafon] dengan jangka waktu hingga [time_period]\\\",\\\"link\\\":\\\"\\\"}]}\",\"app_no_msm\":\"BSMMG20200914110459907244\"}","transaction_id":"20200914110459907244","response_code":"00"}
             */
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            [dataManager.dataExtra setValue:[object valueForKey:@"app_no_msm"] forKey:@"app_no_msm"];
            
            NSDictionary *infoObject = [NSJSONSerialization JSONObjectWithData:[[object valueForKey:@"info"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            self.vwAccept.hidden = NO;
            self.titleContent.text = [infoObject valueForKey:@"caption"];
            listData = [infoObject objectForKey:@"data"];
            [self createList];
        }
        else if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"96"]){

            self.vwRejection.hidden = NO;
            self.lblRejection.text = [[jsonObject objectForKey:@"response"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
//            self.lblRejection setAt
            self.lblRejection.font = [UIFont italicSystemFontOfSize:self.lblRejection.font.pointSize];
        }
        else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)reloadApp{
    [self backToRoot];
}

@end
