//
//  TGINQViewController.h
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 07/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TGINQViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UILabel *lblNik;
@property (weak, nonatomic) IBOutlet UILabel *lblResNik;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *TfEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblTlp;
@property (weak, nonatomic) IBOutlet UITextField *TfTlp1;
@property (weak, nonatomic) IBOutlet UITextField *TfTlp2;
@property (weak, nonatomic) IBOutlet UITextField *TfTlpStrip;
@property (weak, nonatomic) IBOutlet UILabel *lblAset;
@property (weak, nonatomic) IBOutlet UILabel *lblResAset;
@property (weak, nonatomic) IBOutlet UILabel *lblObjek;
@property (weak, nonatomic) IBOutlet UILabel *lblResObjek;
@property (weak, nonatomic) IBOutlet UILabel *lblhishahBank;
@property (weak, nonatomic) IBOutlet UILabel *lblResHishahBank;
@property (weak, nonatomic) IBOutlet UILabel *lblSewa;
@property (weak, nonatomic) IBOutlet UILabel *lblTahunSewa;
@property (weak, nonatomic) IBOutlet UILabel *lblTahunke;
@property (weak, nonatomic) IBOutlet UILabel *lblResSewa;
@property (weak, nonatomic) IBOutlet UILabel *lbljudul1;
@property (weak, nonatomic) IBOutlet UISegmentedControl *optionToggle;
@property (weak, nonatomic) IBOutlet UILabel *lbljudul2;
@property (weak, nonatomic) IBOutlet UILabel *lblHishah;
@property (weak, nonatomic) IBOutlet UITextField *TfHishah;
@property (weak, nonatomic) IBOutlet UILabel *lblJangkaWaktu;
@property (weak, nonatomic) IBOutlet UILabel *lblResJangkaWaktu;
@property (weak, nonatomic) IBOutlet UITextField *TfJangkaWaktu;
@property (weak, nonatomic) IBOutlet UILabel *lblMargin;
@property (weak, nonatomic) IBOutlet UITextField *TfMargin;
@property (weak, nonatomic) IBOutlet CustomBtn *btnHitung;
@property (weak, nonatomic) IBOutlet UILabel *lblSewa2;
@property (weak, nonatomic) IBOutlet UILabel *lblJangkaWaktu2;
@property (weak, nonatomic) IBOutlet UILabel *lblTahun1;
@property (weak, nonatomic) IBOutlet UILabel *lblTahun2;
@property (weak, nonatomic) IBOutlet UILabel *lblTahunke1;
@property (weak, nonatomic) IBOutlet UILabel *lblTahunke2;
@property (weak, nonatomic) IBOutlet UILabel *lblJumlah1;
@property (weak, nonatomic) IBOutlet UILabel *lblHumlah2;
@property (weak, nonatomic) IBOutlet UILabel *lblPdfKtp;
@property (weak, nonatomic) IBOutlet UILabel *lblFormatKtp;
@property (weak, nonatomic) IBOutlet UILabel *lblPdfSK;
@property (weak, nonatomic) IBOutlet UILabel *lblformatSK;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck1;
@property (weak, nonatomic) IBOutlet UILabel *lblcheck1;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck2;
@property (weak, nonatomic) IBOutlet UILabel *lblCheck2;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIView *fvViewHitung;
@property (weak, nonatomic) IBOutlet UIView *fvViewHasil;
@property (weak, nonatomic) IBOutlet UIView *fvViewSegment;
@property (weak, nonatomic) IBOutlet UIView *fvViewHishah;
@property (weak, nonatomic) IBOutlet UIView *fvViewTenor;
@property (weak, nonatomic) IBOutlet UIView *fvViewMargin;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload1;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightVwSegment;


@end

NS_ASSUME_NONNULL_END
