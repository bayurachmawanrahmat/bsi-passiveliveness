//
//  PBYIP02ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 20/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "PBYIP00ViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface PBYIP00ViewController ()<UITextFieldDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *titleToolbar;
    
    UIDatePicker *birthDate;
    
    UIToolbar *toolbar;
}
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIImageView *iconBar;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonBefore;

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *labelKTPNo;
@property (weak, nonatomic) IBOutlet UITextField *tfKTPNo;
@property (weak, nonatomic) IBOutlet UILabel *labelBioMom;
@property (weak, nonatomic) IBOutlet UITextField *tfBioMom;
@property (weak, nonatomic) IBOutlet UILabel *labelBirthDate;
@property (weak, nonatomic) IBOutlet UITextField *tfBirthDate;
//@property (weak, nonatomic) IBOutlet UILabel *labelNoPhone;
//@property (weak, nonatomic) IBOutlet UITextField *tfNoPhone;
@property (weak, nonatomic) IBOutlet UILabel *labelEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;

@property (weak, nonatomic) IBOutlet UIView *vwProgress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstant;

@end

@implementation PBYIP00ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self.buttonNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.buttonBefore setTitle:lang(@"BACK") forState:UIControlStateNormal];
    titleToolbar = lang(@"DONE");

    if([language isEqualToString:@"id"]){
//        titleToolbar = @"selesai";
        self.labelKTPNo.text = @"Nomor KTP Pemohon";
        self.labelBioMom.text = @"Nama Ibu Kandung";
        self.labelBirthDate.text = @"Tanggal Lahir";
        self.labelEmail.text = @"Email";
        
//        [self.buttonNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
//        [self.buttonBefore setTitle:@"Kembali" forState:UIControlStateNormal];
    }else{
//        titleToolbar = @"done";
//        titleToolbar = lang(@"DONE");
        self.labelKTPNo.text = @"ID Card Number";
        self.labelBioMom.text = @"Mother's Maiden Name";
        self.labelBirthDate.text = @"Date of Birth";
        self.labelEmail.text = @"Email";
        
//        [self.buttonNext setTitle:@"Next" forState:UIControlStateNormal];
//        [self.buttonBefore setTitle:@"Back" forState:UIControlStateNormal];
    }
    
    [self.titleBar setText:[self.jsonData objectForKey:@"title"]];
    [self.iconBar setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    [self.buttonBefore setColorSet:SECONDARYCOLORSET];
    
    //    [self setRequest];
    [self setStyles];
    
    [_buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [_buttonBefore addTarget:self action:@selector(actionBefore) forControlEvents:UIControlEventTouchUpInside];
    
    // ADJUST AUTOSCROLL WHEN KEYBOARD APPEARS
    [self registerForKeyboardNotification];
}

- (void) setStyles{
    [Styles setTopConstant:_topConstant];
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:titleToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked)]];
    
    [Styles setStyleTextFieldBottomLine:_tfKTPNo];
    [self.tfKTPNo setDelegate:self];
    [self.tfKTPNo setKeyboardType:UIKeyboardTypeNumberPad];
    [self.tfKTPNo setInputAccessoryView:toolbar];
    
    [Styles setStyleTextFieldBottomLine:_tfBirthDate];
    [self.tfBirthDate setDelegate:self];
    [self setDatePicker];
    
    [Styles setStyleTextFieldBottomLine:_tfBioMom];
    [self.tfBioMom setDelegate:self];
    [self.tfBioMom setInputAccessoryView:toolbar];

//    [Styles setStyleTextFieldBottomLine:_tfNoPhone];
//    [self.tfNoPhone setDelegate:self];
//    [self.tfNoPhone setKeyboardType:UIKeyboardTypePhonePad];
//    [self.tfNoPhone setInputAccessoryView:toolbar];

    [Styles setStyleTextFieldBottomLine:_tfEmail];
    [self.tfEmail setDelegate:self];
    [self.tfEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    [self.tfEmail setInputAccessoryView:toolbar];
    
    UIView *stepper = [[UIView alloc]initWithFrame:CGRectMake(16, 8, SCREEN_WIDTH - (16*2), 30)];
    
    [self.vwContent addSubview:stepper];
    
    [Styles createProgress:stepper step:1 from:4];
    
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [Utility resetTimer];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [Utility resetTimer];
    return YES;
}


- (void) setDatePicker{
    NSString *dateString = @"01-01-1900";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *dateMinimum = [dateFormatter dateFromString:dateString];
    
    birthDate = [[UIDatePicker alloc]init];
    if (@available(iOS 13.4, *)) {
        [birthDate setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [birthDate setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    [birthDate setLocale:[[NSLocale alloc]initWithLocaleIdentifier:[language isEqualToString:@"id"]?@"id":@"en"]];
    [birthDate setBackgroundColor:[UIColor whiteColor]];
    [birthDate setDatePickerMode:UIDatePickerModeDate];
    [birthDate setMinimumDate:dateMinimum];
    [birthDate setMaximumDate:[NSDate date]];
    [birthDate addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    self.tfBirthDate.inputAccessoryView = toolbar;
    self.tfBirthDate.inputView = birthDate;
    
}

- (void)dateChanged:(UIDatePicker *)datePicker{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyyMMdd"];
    NSString* selectedDate = [dateFormat stringFromDate:datePicker.date];
    [dataManager.dataExtra setValue:selectedDate forKey:@"birth_date"];

    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    selectedDate = [dateFormat stringFromDate:datePicker.date];
    [self.tfBirthDate setText:selectedDate];
}

- (void) doneClicked{
    [self.view endEditing:YES];
}

- (void) actionBefore{
    dataManager.currentPosition--;
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionNext{
    if([self validate]){
        [dataManager.dataExtra setValue:self.tfKTPNo.text forKey:@"nik"];
        [dataManager.dataExtra setValue:self.tfBioMom.text forKey:@"mother_name"];
//        [dataManager.dataExtra setValue:self.tfBirthDate.text forKey:@"birth_date"];
        [dataManager.dataExtra setValue:self.tfEmail.text forKey:@"email_verify"];
        [self setRequest];
    }
//    [self openNextTemplate];
}

- (BOOL) validate{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if([self.tfKTPNo.text isEqualToString:@""] ||
       [self.tfBirthDate.text isEqualToString:@""] || [[self.tfBioMom.text stringByTrimmingCharactersInSet: set] length] == 0 ||
       [self.tfBioMom.text isEqualToString:@""] ||
       [self.tfEmail.text isEqualToString:@""]
       ){
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Data yang anda masukan tidak lengkap, periksa kembali data yang Anda masukkan";
        }else{
            msg = @"The data you entered is incomplete, please check your data again";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
         
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if(_tfKTPNo.text.length != 16){
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Data KTP tidak valid, periksa kembali data yang Anda masukkan";
        }else{
            msg = @"KTP data is invalid, double check the data you entered";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
         
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    if(![self validateEmailWithString:_tfEmail.text]){
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Data Email tidak valid, periksa kembali data yang Anda masukkan";
        }else{
            msg = @"Email data is invalid, double check the data you entered";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
         
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return true;
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (void) setRequest{
    bool needReq = [[self.jsonData valueForKey:@"url"]boolValue];
    if (needReq) {
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"verify_financing"]){
        NSLog(@"%@", jsonObject);
        if ([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]) {
            
            [self openNextTemplate];
        }else if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"14"]){
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
             
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
             
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}


- (void)registerForKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
 
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;

    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

@end
