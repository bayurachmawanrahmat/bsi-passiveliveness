//
//  PF03ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 07/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PF03ViewController.h"
#import "CellPortoTwo.h"

@interface PF03ViewController (){
    NSArray *dataPortofolio;
    NSUserDefaults *userDefault;
    NSString *lang;
    CellPortoTwo *cellPortTwo;
}

@end

@implementation PF03ViewController

- (id)initWithStyle:(UITableViewStyle)style
             mData : (NSArray *) nDataPortofolio
{
    self = [super initWithStyle:style];
    if (self) {
        dataPortofolio = nDataPortofolio;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    [self.tableView registerClass:[CellPortoTwo class] forCellReuseIdentifier:@"PortoCell2"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return dataPortofolio.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellPortoTwo *cell = (CellPortoTwo *)[tableView dequeueReusableCellWithIdentifier:@"PortoCell2" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *fullString = [dataPortofolio objectAtIndex:indexPath.row];
    if ([fullString isEqualToString:@""]) {
        [cell.cardView setHidden:YES];
        cell.lblTitle.text = @"";
        cell.lblTitleKey1.text = @"";
        cell.lblTitleKey2.text = @"";
        cell.lblTitleKey3.text = @"";
        cell.lblTitleKey4.text = @"";
        cell.lblCtnVal1.text = @"";
        cell.lblCtnVal2.text = @"";
        cell.lblCtnVal3.text = @"";
        cell.lblCtnVal4.text = @"";
    }else{
        NSArray *splitArray = [fullString componentsSeparatedByString:@"\n"];
        cell.lblTitle.text = splitArray[0];
        
        NSArray *spCtn1 = [splitArray[1] componentsSeparatedByString:@"<tab>"];
        NSArray *spCtn2 = [splitArray[2] componentsSeparatedByString:@"<tab>"];
        NSArray *spCtn3 = [splitArray[3] componentsSeparatedByString:@"<tab>"];
        NSArray *spCtn4 = [splitArray[4] componentsSeparatedByString:@"<tab>"];
        
        cell.lblTitleKey1.text = [spCtn1 objectAtIndex:0];
        cell.lblCtnVal1.text = [spCtn1 objectAtIndex:1];
        
        cell.lblTitleKey2.text = [spCtn2 objectAtIndex:0];
        cell.lblCtnVal2.text = [spCtn2 objectAtIndex:1];
        
        cell.lblTitleKey3.text = [spCtn3 objectAtIndex:0];
        cell.lblCtnVal3.text = [spCtn3 objectAtIndex:1];
        
        cell.lblTitleKey4.text = [spCtn4 objectAtIndex:0];
        cell.lblCtnVal4.text = [spCtn4 objectAtIndex:1];
    
        
        NSLog(@"Data Cell : %@", splitArray);
    }
    
    [cell.lblTitle sizeToFit];
    [cell.lblTitleKey1 sizeToFit];
    [cell.lblCtnVal1 sizeToFit];
    [cell.lblTitleKey2 sizeToFit];
    [cell.lblCtnVal2 sizeToFit];
    [cell.lblTitleKey3 sizeToFit];
    [cell.lblCtnVal3 sizeToFit];
    [cell.lblTitleKey4 sizeToFit];
    [cell.lblCtnVal4 sizeToFit];
    
    CGRect frmCard = cell.cardView.frame;
    CGRect frmTitle = cell.lblTitle.frame;
    CGRect frmTitleKey1 = cell.lblTitleKey1.frame;
    CGRect frmTitleKey2 = cell.lblTitleKey2.frame;
    CGRect frmTitleKey3 = cell.lblTitleKey3.frame;
    CGRect frmTitleKey4 = cell.lblTitleKey4.frame;
    CGRect frmCtnVal1 = cell.lblCtnVal1.frame;
    CGRect frmCtnVal2 = cell.lblCtnVal2.frame;
    CGRect frmCtnVal3 = cell.lblCtnVal3.frame;
    CGRect frmCtnVal4 = cell.lblCtnVal4.frame;
    
    
    frmCard.origin.x = 8;
    frmCard.origin.y = 8;
    if (indexPath.row == 0) {
        frmCard.origin.y = 16;
    }
    frmCard.size.width = cell.contentView.frame.size.width - (frmCard.origin.x * 2);
    
    frmTitle.origin.x = 16;
    frmTitle.origin.y = 16;
    
    frmTitleKey1.origin.x = frmTitle.origin.x;
    frmTitleKey1.origin.y = frmTitle.origin.y + frmTitle.size.height + 8;
    
    frmTitleKey2.origin.x = frmTitle.origin.x;
    frmTitleKey2.origin.y = frmTitleKey1.origin.y + frmTitleKey1.size.height + 8;
    
    frmTitleKey3.origin.x = frmTitle.origin.x;
    frmTitleKey3.origin.y = frmTitleKey2.origin.y + frmTitleKey2.size.height + 8;
    
    frmTitleKey4.origin.x = frmTitle.origin.x;
    frmTitleKey4.origin.y = frmTitleKey3.origin.y + frmTitleKey3.size.height + 8;
    
    frmCtnVal1.origin.x = frmCard.size.width - frmCtnVal1.size.width - 16;
    frmCtnVal1.origin.y = frmTitleKey1.origin.y;
    
    frmCtnVal2.origin.x = frmCard.size.width - frmCtnVal2.size.width - 16;
    frmCtnVal2.origin.y = frmTitleKey2.origin.y;
    
    frmCtnVal3.origin.x = frmCard.size.width - frmCtnVal3.size.width - 16;
    frmCtnVal3.origin.y = frmTitleKey3.origin.y;
    
    frmCtnVal4.origin.x = frmCard.size.width - frmCtnVal4.size.width - 16;
    frmCtnVal4.origin.y = frmTitleKey4.origin.y;
    
    
    
    frmCard.size.height = frmTitleKey4.origin.y + frmTitleKey4.size.height + 16;
    
    cell.cardView.frame = frmCard;
    cell.lblTitle.frame = frmTitle;
    cell.lblTitleKey1.frame = frmTitleKey1;
    cell.lblTitleKey2.frame = frmTitleKey2;
    cell.lblTitleKey3.frame = frmTitleKey3;
    cell.lblTitleKey4.frame = frmTitleKey4;
    cell.lblCtnVal1.frame = frmCtnVal1;
    cell.lblCtnVal2.frame = frmCtnVal2;
    cell.lblCtnVal3.frame = frmCtnVal3;
    cell.lblCtnVal4.frame = frmCtnVal4;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (!cellPortTwo) {
//        cellPortTwo = (CellPortoTwo *)[tableView dequeueReusableCellWithIdentifier:@"PortoCell2" forIndexPath:indexPath];
//    }
    CGFloat mHeight = 50.0;
    NSString *fullString = [dataPortofolio objectAtIndex:indexPath.row];
    if (![fullString isEqualToString:@""]) {
        fullString = [fullString stringByAppendingString:@"\n\n"];
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
        CGSize size = [fullString sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        CGFloat height = MAX(size.height, 33.0);
        //    CGRect frmCardView = cellPortTwo.cardView.frame;
        
        mHeight = height+30.0;
    }
    return mHeight;
   
}

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    NSString *strTitleTab;
    if ([lang isEqualToString:@"id"]) {
        strTitleTab = @"ZISWAF";
    }else{
        strTitleTab = @"ZISWAF";
    }
    return strTitleTab;
}

-(UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController
{
    return [UIColor whiteColor];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
