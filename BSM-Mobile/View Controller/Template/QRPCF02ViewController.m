//
//  QRPCF02ViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "QRPCF02ViewController.h"
#import "Utility.h"
#import "InboxHelper.h"
#import "AESCipher.h"

@interface QRPCF02ViewController ()<ConnectionDelegate, UIAlertViewDelegate> {
    NSDictionary *dataObject;
    BOOL fromRecipt;
    BOOL frmTrx;
    
}

@property (nonatomic) AESCipher *aesChiper;
@property (weak, nonatomic) IBOutlet UILabel *lblTittle;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOkShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIView *viewSS;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UIImageView *imgRecipt;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleToko;
@property (weak, nonatomic) IBOutlet UILabel *lblToko;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleAMOUNT;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFee;
@property (weak, nonatomic) IBOutlet UILabel *lblFee;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleTips;
@property (weak, nonatomic) IBOutlet UILabel *lblTips;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleTotAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTotAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleRemark;
@property (weak, nonatomic) IBOutlet UILabel *lblRemak;
@end

@implementation QRPCF02ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.aesChiper = [[AESCipher alloc] init];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [_btnShare setTitle:@"Bagikan" forState:UIControlStateNormal];
    } else {
        [_btnShare setTitle:@"Share" forState:UIControlStateNormal];
    }
    
    if (fromRecipt) {
        [_btnMore setHidden:YES];
        [_btnOk setHidden:YES];
        [_btnShare setHidden:NO];
        [_btnOkShare setHidden:NO];
    } else {
        [_btnMore setHidden:NO];
        [_btnOk setHidden:NO];
        [_btnShare setHidden:YES];
        [_btnOkShare setHidden:YES];
    }
    
    
    if(self.jsonData){
        frmTrx = true;
        _imgRecipt.hidden = YES;
        [self.lblTittle setText:[self.jsonData valueForKey:@"title"]];
        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if(needRequest){
             NSString *url =[NSString stringWithFormat:@"%@,tips,admfee,amount,remark,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:fav];
        }
    } else if (dataObject) {
        frmTrx = false;
        [self.lblTittle setText:@"QR Pay"];
//        NSString *response = [dataObject valueForKey:@"response"];
        NSString *response = [dataObject valueForKey:@"msg"];
        NSLog(@"%@", response);
        NSError *error;
        
        NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
        NSLog(@"%@", [error localizedDescription]);
        
        NSString *strTime = @"";
        NSString *strRef = @"";
        NSString *strToko = @"";
        NSString *strAmount = @"";
        NSString *strFee = @"";
        NSString *strTips = @"";
        NSString *strTotal = @"";
        NSString *strRemark = @"";
        if([lang isEqualToString:@"id"]){
            strTime = @"Waktu";
            strRef= @"No.Ref";
            strToko= @"Bayar Toko/Merchant";
            strAmount= @"Jumlah";
            strFee= @"Fee";
            strTips= @"Tips";
            strTotal= @"Total";
            strRemark= @"Keterangan";
        } else {
            strTime = @"Time";
            strRef= @"Ref Number";
            strToko= @"Pay Shop/Merchant";
            strAmount= @"amount";
            strFee= @"Fee";
            strTips= @"Tips";
            strTotal= @"Total Amount";
            strRemark= @"Remark";
        }
        
        _lblHeader.text = [NSString stringWithFormat:@"%@ : %@", strRef, [dict objectForKey:@"trxref"]];
        _lblContent.text = [NSString stringWithFormat:@"%@ : %@",strTime ,[dict objectForKey:@"date_time"]];
        _lblTitleToko.text = strToko;
        _lblTitleAMOUNT.text = strAmount;
        _lblTitleFee.text = strFee;
        _lblTitleTips.text = strTips;
        _lblTitleTotAmount.text = strTotal;
        _lblTitleRemark.text = strRemark;
        _lblFooter.text = [dict objectForKey:@"footer_msg"];
        
        _lblToko.text = [NSString stringWithFormat:@"%@ / %@", [dict objectForKey:@"merchant_name"], [dict objectForKey:@"merchant_city"]];
        _lblAmount.text = [dict objectForKey:@"amount"];
        _lblFee.text = [dict objectForKey:@"fee"];
        _lblTips.text = [dict objectForKey:@"tips"];
        _lblTotAmount.text = [dict objectForKey:@"totamt"];
        _lblRemak.text = [dict objectForKey:@"remark"];
        
//        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//        NSMutableArray *arrlistInbox = [userDefault objectForKey:@"listInbox"];
//        NSMutableArray *temp = [[NSMutableArray alloc] init];
//        for (id object in arrlistInbox) {
//            if (![object isEqual:[dataObject valueForKey:@"transaction_id"]]) {
//                [temp addObject:object];
//            }
//        }
        
//        [userDefault setObject:temp forKey:@"listInbox"];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"changeIcon"
         object:self];
    }
}

- (void)setDataLocal:(NSDictionary*) object{
    dataObject =object;
}

- (void)setFromRecipt:(BOOL)recipt {
    fromRecipt = recipt;
}

- (IBAction)actionMore:(id)sender {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgCancel = @"";
    NSString *msgShare = @"";
    if([lang isEqualToString:@"id"]){
        msgCancel = @"Batal";
        msgShare = @"Bagikan";
    } else {
        msgCancel = @"Cancel";
        msgShare = @"Share";
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"More" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgShare style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self prosesShareImage];
        //        [self dismissViewControllerAnimated:YES completion:^{
        //
        //        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)actionShare:(id)sender {
    [self prosesShareImage];
}


-(void)prosesShareImage{
    UIGraphicsBeginImageContextWithOptions(_viewSS.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_viewSS.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}


- (IBAction)actionOk:(id)sender {
    // All instances of TestClass will be notified
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"changeIcon"
     object:self];
    if(frmTrx){
         [super backToRoot];
    }else{
        UINavigationController *navigationController = self.navigationController;
        [navigationController popViewControllerAnimated:YES];
        NSLog(@"Views in hierarchy: %@", [navigationController viewControllers]);
    }
   
    
}



#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        if([requestType isEqualToString:@"insert_favorite"]){
            [super didFinishLoadData:jsonObject withRequestType:requestType];
        }else{
            if (![requestType isEqualToString:@"check_notif"]) {
                _imgRecipt.hidden = NO;
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *response = [jsonObject valueForKey:@"response"];
                NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *strTime = @"";
                NSString *strRef = @"";
                NSString *strToko = @"";
                NSString *strAmount = @"";
                NSString *strFee = @"";
                NSString *strTips = @"";
                NSString *strTotal = @"";
                NSString *strRemark = @"";
                if([lang isEqualToString:@"id"]){
                    strTime = @"Waktu";
                    strRef= @"No.Ref";
                    strToko= @"Bayar Toko/Merchant";
                    strAmount= @"Jumlah";
                    strFee= @"Fee";
                    strTips= @"Tips";
                    strTotal= @"Total";
                    strRemark= @"Keterangan";
                } else {
                    strTime = @"Time";
                    strRef= @"Ref Number";
                    strToko= @"Pay Shop/Merchant";
                    strAmount= @"amount";
                    strFee= @"Fee";
                    strTips= @"Tips";
                    strTotal= @"Total Amount";
                    strRemark= @"Remark";
                }
                
                _lblHeader.text = [NSString stringWithFormat:@"%@ : %@", strRef, [dict objectForKey:@"trxref"]];
                _lblContent.text = [NSString stringWithFormat:@"%@ : %@",strTime ,[dict objectForKey:@"date_time"]];
                _lblTitleToko.text = strToko;
                _lblTitleAMOUNT.text = strAmount;
                _lblTitleFee.text = strFee;
                _lblTitleTips.text = strTips;
                _lblTitleTotAmount.text = strTotal;
                _lblTitleRemark.text = strRemark;
                _lblFooter.text = [dict objectForKey:@"footer_msg"];
                
                _lblToko.text = [NSString stringWithFormat:@"%@ / %@", [dict objectForKey:@"merchant_name"], [dict objectForKey:@"merchant_city"]];
                _lblAmount.text = [dict objectForKey:@"amount"];
                _lblFee.text = [dict objectForKey:@"fee"];
                _lblTips.text = [dict objectForKey:@"tips"];
                _lblTotAmount.text = [dict objectForKey:@"totamt"];
                _lblRemak.text = [dict objectForKey:@"remark"];
                
                //            NSMutableDictionary *dictList = [[NSMutableDictionary alloc] init];
                //            [dictList setValue:response forKey:@"response"];
                //            [dictList setValue:[jsonObject valueForKey:@"transaction_id"]
                //                        forKey:@"transaction_id"];
                //            [dictList setValue:@"QRPCF02" forKey:@"template"];
                //            [dictList setValue:jsonObject forKey:@"apiResponse"];
                //            [dictList setValue:@"Qr Pay" forKey:@"jenis"];
                //            NSString *img = [userDefault objectForKey:@"imgIcon"];
                //            [dictList setValue:img forKey:@"icon"];
                //            NSDate *date = [NSDate date];
                //            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                //            [dateFormat setDateFormat:@"dd MMM yyyy"];
                //            NSString *currentDate = [dateFormat stringFromDate:date];
                //            [dictList setValue:currentDate forKey:@"time"];
                //
                //            if ([userDefault objectForKey:@"listRecipt"]){
                //                NSMutableArray *arr = [[userDefault objectForKey:@"listRecipt"] mutableCopy];
                //                [arr addObject:dictList];
                //                [userDefault setObject:arr forKey:@"listRecipt"];
                //            } else {
                //                NSMutableArray *arr = [[NSMutableArray alloc] init];
                //                [arr addObject:dictList];
                //                [userDefault setObject:arr forKey:@"listRecipt"];
                //            }
                //            if ([userDefault objectForKey:@"listInbox"]){
                //                NSMutableArray *arrTransaction = [[userDefault objectForKey:@"listInbox"] mutableCopy];
                //                [arrTransaction addObject:[jsonObject valueForKey:@"transaction_id"]];
                //                [userDefault setObject:arrTransaction forKey:@"listInbox"];
                //            } else {
                //                NSMutableArray *arrTransaction = [[NSMutableArray alloc] init];
                //                [arrTransaction addObject:[jsonObject valueForKey:@"transaction_id"]];
                //                [userDefault setObject:arrTransaction forKey:@"listInbox"];
                //            }
                
                NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
                [dictList setValue:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
                [dictList setValue:@"QRPCF02" forKey:@"template"];
                [dictList setValue:@"Qr Pay" forKey:@"jenis"];
                [dictList setValue:response forKey:@"msg"];
                [dictList setValue:[Utility dateToday:2] forKey:@"time"];
                [dictList setValue:@"0" forKey:@"marked"];
                [dictList setValue:[dict objectForKey:@"footer_msg"] forKey:@"footer_msg"];
                [dictList setValue:[dict objectForKey:@"title"] forKey:@"title"];
                [dictList setValue:[dict objectForKey:@"trxref"] forKey:@"trxref"];
                
                InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
                [inboxHeldper insertDataInbox:dictList];
                
//                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//                NSString *strCID = [NSString stringWithFormat:@"%@", [userDefault valueForKey:@"customer_id"]];
                NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
//
                NSMutableArray *arBackUpInbox = [[NSMutableArray alloc]init];
                NSDictionary *backUpInbox = [Utility dictForKeychainKey:strCID];
                
                NSMutableDictionary *contentBackup = [[NSMutableDictionary alloc]init];
//                NSDictionary *dataEncrypt = [[NSDictionary alloc]init];
                
                NSDictionary *dataEncrypt = [_aesChiper dictAesEncryptString:dictList];
                
               
                
                //disini validasi jika sudah ada data dikeychain ambil data timpa ke array baru
                if (backUpInbox) {
                    NSArray *arrLastBackup = [backUpInbox valueForKey:@"data"];
                    for(NSDictionary *dictLastBackup in arrLastBackup){
                        if([dictLastBackup valueForKey:@"content"]!= nil){
                            NSDictionary *data = @{@"content" : [dictLastBackup valueForKey:@"content"]};
                            [arBackUpInbox addObject:data];
                        }
                    }
                   
                }
                [contentBackup setObject:dataEncrypt forKey:@"content"];
                [arBackUpInbox addObject:contentBackup];
                
                
                if (arBackUpInbox.count > 0) {
                    NSLog(@"Data Array For Backup : %@", arBackUpInbox);
                    NSMutableDictionary *storeBackupInbox = [[NSMutableDictionary alloc]init];
                    [storeBackupInbox setObject:arBackUpInbox forKey:@"data"];
                    if (storeBackupInbox) {
                        NSLog(@"Data Dict For Backup : %@", storeBackupInbox);
                        [Utility removeForKeychainKey:strCID];
                        [Utility setDict:storeBackupInbox forKey:strCID];
                    }
                }
                
//                                [Utility removeForKeychainKey:strCID];
                
                
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"changeIcon"
                 object:self];
                
            }
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        //        [self.labelConfirmation setText:];
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}

@end

