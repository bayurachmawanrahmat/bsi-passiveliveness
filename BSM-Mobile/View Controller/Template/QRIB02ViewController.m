//
//  QRIB02ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 1/23/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "QRIB02ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>
#import "RecentlyHelper.h"
#import "CellRecentlyUse.h"
#import "Utility.h"

@interface QRIB02ViewController ()<ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate, AVCaptureMetadataOutputObjectsDelegate, UITableViewDelegate,UITableViewDataSource,CAAnimationDelegate,UINavigationControllerDelegate>{
    NSDictionary *dataObject;
    AVCaptureSession *session;
    AVCaptureDevice *device;
    AVCaptureDeviceInput *input;
    AVCaptureMetadataOutput *output;
    AVCaptureVideoPreviewLayer *previewLayer;
    UIView *kotakScan;
    NSString *statusMenuQR;
    UIView *viewPreviewQR;
    UIView *vwListAccount;
    NSMutableArray *arrFA;
    NSArray *arDataRecently;
    RecentlyHelper *mHelper;
    NSString *lang;
    
    UIView *viewFlash;
    UIButton *btnFlash;
    UILabel *lblFlash;
    
    BOOL stateFlash;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextField *textFieldInput;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet UIButton *btnQR;
@property (weak, nonatomic) IBOutlet UIImageView *imgQR;
@property (weak, nonatomic) IBOutlet UIButton *btnBatalScanQR;

@property (weak, nonatomic) IBOutlet UIView *vwRecentllyUse;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleRecently;

@property (weak, nonatomic) IBOutlet UITableView *tblRecentlyUse;

- (IBAction)next:(id)sender;
- (IBAction)batal:(id)sender;
- (IBAction)scanQR:(id)sender;
- (IBAction)batalScan:(id)sender;
@end

@implementation QRIB02ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    /*if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00048"]){
     if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"id_denom"]){
     NSString *valDenom = [dataManager.dataExtra objectForKey:@"id_denom"];
     if (valDenom != nil) {
     self.textFieldInput.text = valDenom;
     if ([valDenom isEqualToString:@"0"]) {
     }
     else {
     [self next:nil];
     }
     }
     }
     }*/
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.btnBatalScanQR.hidden = true;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    arDataRecently = [[NSArray alloc]init];
    mHelper = [[RecentlyHelper alloc]init];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00060"] && [[self.jsonData valueForKey:@"content"] isEqualToString:@"No. Kartu"]) {
            _lblContent.text = @"Untuk handphone yang tidak mendukung fitur NFC, silahkan masukkan nomor kartu e-money Anda untuk melakukan transaksi.\n\nSetelah transaksi berhasil, update saldo kartu e-money Anda di ATM Mandiri atau update menggunakan ponsel yg memiliki fitur NFC dan aplikasi BSI Mobile.";
            [_lblContent setHidden:false];
            statusMenuQR = @"eMoney";
        }
        else {
            _lblContent.text = @"";
            [_lblContent setHidden:true];
            statusMenuQR = @"";
        }
        
        [_btnB setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnN setTitle:@"Batal" forState:UIControlStateNormal];
        
        
        [_lblTitleRecently setText:@"Transaksi Terakhir"];
        
    } else {
        if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00060"] && [[self.jsonData valueForKey:@"content"] isEqualToString:@"Card No."]) {
            _lblContent.text = @"Mobile phone that does not support the NFC feature, please enter your e-money card number to make a transaction.\n\nAfter the transaction is successful, please update the balance of your e-money card at Mandiri ATM or update using a mobile phone which has NFC feature and BSI Mobile application.";
            [_lblContent setHidden:false];
            statusMenuQR = @"eMoney";
        }
        else {
            _lblContent.text = @"";
            [_lblContent setHidden:true];
            statusMenuQR = @"";
        }
        
        [_btnB setTitle:@"Next" forState:UIControlStateNormal];
        [_btnN setTitle:@"Cancel" forState:UIControlStateNormal];
        [_lblTitleRecently setText:@"Last Transaction"];
    }
    
    self.textFieldInput.delegate = self;
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldInput.frame.size.height);
    
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldInput.leftView = paddingPhone;
    self.textFieldInput.leftViewMode = UITextFieldViewModeAlways;
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
        [self.textFieldInput setSecureTextEntry:true];
    }else{
        [self.textFieldInput setPlaceholder:[self.jsonData valueForKey:@"content"]];
    }
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    //CGFloat screenHeight = screenSize.height;
    
    CGRect frmContent = _lblContent.frame;
    CGRect frmTextField = _textFieldInput.frame;
    CGRect frmTitle = _vwTitle.frame;
    CGRect frmLabelTitle = _labelTitle.frame;
    CGRect frmLine = _vwLine.frame;
    CGRect frmButton = _vwButton.frame;
    CGRect frmVwRecently = _vwRecentllyUse.frame;
    CGRect frmLblTitleRecently = _lblTitleRecently.frame;
    CGRect frmTblRecently = _tblRecentlyUse.frame;
    
    CGRect frmBtnB = _btnB.frame;
    CGRect frmBtnN = _btnN.frame;
    
    frmContent.size.width = screenWidth - 40;
    frmTitle.size.width = screenWidth;
    frmTitle.origin.x = 0;
    frmTitle.origin.y = TOP_NAV;
    frmLabelTitle.size.width = screenWidth - 32;
    frmLine.size.width = screenWidth - 40;
    frmButton.size.width = screenWidth;
    frmButton.origin.x = 0;
    frmButton.size.height = 40;
    
    frmBtnN.origin.x = 20;
    frmBtnN.size.width = (screenWidth/2) - 30;
    frmBtnB.origin.x = (screenWidth/2) + 10;
    frmBtnB.size.width = (screenWidth/2) - 30;
    frmBtnN.size.height = 40;
    frmBtnB.size.height = 40;
    
    if ([statusMenuQR isEqualToString:@""]) {
        //frmTextField.origin.y = frmContent.origin.y;
        frmTextField.origin.y = frmTitle.origin.y + frmTitle.size.height + 30;
        frmLine.origin.y = frmTextField.origin.y + 41;
        frmButton.origin.y = frmLine.origin.y + 41;
    }
    
    if (screenWidth <= 350) {
        [_lblContent setFont:[UIFont fontWithName:const_font_name1 size:11.0f]];
        frmContent.size.height = frmContent.size.height + 20;
    }
    else {
        
    }
    
    frmVwRecently.origin.x = 0;
    frmVwRecently.origin.y = frmButton.origin.y + frmButton.size.height + 8;
    frmVwRecently.size.width = SCREEN_WIDTH;
    frmVwRecently.size.height = SCREEN_HEIGHT - frmVwRecently.origin.y - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveNotch]) {
        frmVwRecently.size.height = SCREEN_HEIGHT - frmVwRecently.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    frmLblTitleRecently.origin.x = 16;
    frmLblTitleRecently.origin.y = 16;
    frmLblTitleRecently.size.width = frmVwRecently.size.width - (frmLblTitleRecently.origin.x*2);
    
    frmTblRecently.origin.x = 0;
    frmTblRecently.origin.y = frmLblTitleRecently.origin.y + frmLblTitleRecently.size.height + 8;
    frmTblRecently.size.width = frmVwRecently.size.width;
    frmTblRecently.size.height = frmVwRecently.size.height - frmTblRecently.origin.y;
    
    _lblContent.frame = frmContent;
    _textFieldInput.frame = frmTextField;
    _vwTitle.frame = frmTitle;
    _labelTitle.frame = frmLabelTitle;
    _vwLine.frame = frmLine;
    _vwButton.frame = frmButton;
    _vwRecentllyUse.frame = frmVwRecently;
    _lblTitleRecently.frame = frmLblTitleRecently;
    _tblRecentlyUse.frame = frmTblRecently;
    _btnB.frame = frmBtnB;
    _btnN.frame = frmBtnN;
    
    
    [self.btnB setColorSet:PRIMARYCOLORSET];
    [self.btnN setColorSet:SECONDARYCOLORSET];
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self setDisplay];
    
    //    if([[[self.jsonData valueForKey:@"content"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]isEqualToString:@""]){
    //
    //        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Opps" message:@"No title found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //        [alert show];
    //    }
    //    NSDictionary *dataObject =
    // Do any additional setup after loading the view.
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.textFieldInput.inputAccessoryView = keyboardDoneButtonView;
    
    
    if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00026"]){
        if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"payment_id"]){
            self.textFieldInput.text = [dataManager.dataExtra valueForKey:@"payment_id"];
        }
    }
    
    /*else if ([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00065"]) {
     if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"transaction_id"]){
     self.textFieldInput.text = [dataManager.dataExtra valueForKey:@"transaction_id"];
     }
     }*/
    [self showingRecentlyUses : [self.jsonData valueForKey:@"menu_id"] mFieldName:[self.jsonData valueForKey:@"field_name"]];
    
}

-(void) changeHigthTable {
    CGRect frmTblRecently = self.tblRecentlyUse.frame;
    if (self.btnBatalScanQR.isHidden) {
        frmTblRecently.size.height = SCREEN_HEIGHT - frmTblRecently.origin.y - BOTTOM_NAV_DEF;
        if ([Utility isDeviceHaveNotch]) {
            frmTblRecently.size.height = SCREEN_HEIGHT - frmTblRecently.origin.y - BOTTOM_NAV_NOTCH;
        }
    }else{
        frmTblRecently.size.height = SCREEN_HEIGHT - frmTblRecently.origin.y - BOTTOM_NAV_DEF - self.btnBatalScanQR.frame.size.height;
        if ([Utility isDeviceHaveNotch]) {
            frmTblRecently.size.height = SCREEN_HEIGHT - frmTblRecently.origin.y - BOTTOM_NAV_NOTCH - self.btnBatalScanQR.frame.size.height;
        }
    }
    self.tblRecentlyUse.frame = frmTblRecently;
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDisplay {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGRect frmImgQR = self.imgQR.frame;
    CGRect frmBtnQR = self.btnQR.frame;
    CGRect frmBtnBatalScan = self.btnBatalScanQR.frame;
    
    frmImgQR.origin.x = screenWidth - frmImgQR.size.width - 44 - 10;
    frmImgQR.origin.y = self.textFieldInput.frame.origin.y;
    frmBtnQR.origin.x = frmImgQR.origin.x;
    frmBtnQR.origin.y = frmImgQR.origin.y;
    frmBtnBatalScan.origin.y = screenHeight - 60;
    frmBtnBatalScan.size.width = screenWidth - 20;
    
    self.imgQR.frame = frmImgQR;
    self.btnQR.frame = frmBtnQR;
    self.btnBatalScanQR.frame = frmBtnBatalScan;
    
//    [self.imgQR setImage:[UIImage imageNamed:@"ic_frm_scanqr"]];
    
    [self.btnQR setImage:[UIImage imageNamed:@"ic_frm_scanqr"] forState:UIControlStateNormal];
    
    UIButton *btnMore = [[UIButton alloc]initWithFrame:CGRectMake(frmImgQR.origin.x-44, frmImgQR.origin.y, 44, 44)];
    UIImage *imgMore = [UIImage imageNamed:@"ic_frm_searchqr"];
    //UIImage *imgMore = [UIImage imageNamed:@"Icon Widget QR Code.png"];
    [btnMore setImage:imgMore forState:UIControlStateNormal];
    [btnMore addTarget:self action:@selector(actionMore:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnMore];
    
    UIButton *btnBrowse = [[UIButton alloc]initWithFrame:CGRectMake(frmImgQR.origin.x+frmImgQR.size.width+5, frmImgQR.origin.y, 44, 44)];
    //UIImage *imgBrowse = [UIImage imageNamed:@"open-file-icon.png"];
//    UIImage *imgBrowse = [UIImage imageNamed:@"ic_fpop_uphoto"];
    UIImage *imgBrowse = [UIImage imageNamed:@"ic_frm_searchqr"];
    [btnBrowse setImage:imgBrowse forState:UIControlStateNormal];
    [btnBrowse addTarget:self action:@selector(actionBrowse:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBrowse];
    
    
    [self.imgQR setHidden:YES];
}

- (void)viewDidLayoutSubviews {
    CGRect bounds = viewPreviewQR.layer.bounds;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    previewLayer.bounds=bounds;
    previewLayer.position=CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection{
    NSString *QRCode = nil;
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            QRCode = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            break;
        }
    }
    if (QRCode) {
        [session stopRunning];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": QRCode}];
        [self.textFieldInput setText:QRCode];
        [self closeScanner];
        //[super openNextTemplate];
    }
    
    NSLog(@"QR Code: %@", QRCode);
}

- (UIColor *)colorWithHex:(NSString *)hex alpha:(float)alpha {
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor blackColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return [UIColor blackColor];
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float)r/255.0) green:((float)g/255.0) blue:((float)b)/255.0 alpha:alpha];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)closeScanner {
    [session stopRunning];
    [viewPreviewQR removeFromSuperview];
    [viewFlash removeFromSuperview];
    //[kotakScan removeFromSuperview];
    self.btnBatalScanQR.hidden = true;
    self.tabBarController.tabBar.hidden = NO;
}

- (void)actionMore:(UIButton *)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSArray *arrAccount = [userDefault objectForKey:@"arrAccount"];
    NSString *selectedAccount = [userDefault objectForKey:@"valNoRek"];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    arrFA = [[NSMutableArray alloc]init];
    
    //Filter Nomor Rekening yang sudah dipakai
    for (int j=0;j<([arrAccount count]);j++) {
        NSDictionary *tempDict = [arrAccount objectAtIndex:j];
        NSString *tempString = [tempDict objectForKey:@"id_account"];
        if (![tempString isEqualToString:selectedAccount]) {
            [arrFA addObject:tempString];
        }
    }
    
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat heightBG = [arrFA count]*45;
    CGFloat widthBG = 300;
    
    if ([arrFA count] == 0) {
        NSString *msgAcc = @"";
        if([lang isEqualToString:@"id"]){
            msgAcc = @"Nomer Rekening anda hanya ada 1.";
        } else {
            msgAcc = @"You only have 1 bank account.";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msgAcc delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        vwListAccount = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, height - 50)];
        vwListAccount.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        
        UIView *viewBGWhite = [[UIView alloc]initWithFrame:CGRectMake((width/2) - (widthBG/2), (height/2) - (heightBG/2), widthBG, heightBG)];
        viewBGWhite.backgroundColor = [UIColor whiteColor];
        
        for(int i=0;i<([arrFA count]);i++){
            UIView *vwAccount = [[UIView alloc]initWithFrame:CGRectMake(0, i*45, widthBG, 45)];
            
            UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 44, widthBG, 1)];
            vwLine.backgroundColor = [UIColor grayColor];
            
            UILabel *lblAccount = [[UILabel alloc]initWithFrame:CGRectMake(widthBG/2 - (widthBG - 180)/2, 5, widthBG - 180, 35)];
            
            NSString *norek = [arrFA objectAtIndex:i];
            [lblAccount setText:norek];
            
            UIButton *btnAccount = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, widthBG, 45)];
            btnAccount.tag = i;
            [btnAccount addTarget:self action:@selector(actionOpenAccount:) forControlEvents:UIControlEventTouchUpInside];
            
            [vwAccount addSubview:vwLine];
            [vwAccount addSubview:lblAccount];
            [vwAccount addSubview:btnAccount];
            [viewBGWhite addSubview:vwAccount];
        }
        
        [vwListAccount addSubview:viewBGWhite];
        [self.view addSubview:vwListAccount];
    }
}

- (void)actionBrowse:(UIButton *)sender {
//    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select File option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
//                            @"Choose File",nil];
//
//    popup.tag = 1;
//    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select File option:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Choose File" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        self->imagePicker = [[UIImagePickerController alloc] init];
        self->imagePicker.delegate = self;
        [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        [self presentViewController:self->imagePicker animated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma  mark- Opne Action Sheet for Options

//- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
//    switch (popup.tag) {
//        case 1: {
//            switch (buttonIndex) {
//                case 0:
//                    imagePicker = [[UIImagePickerController alloc] init];
//
//                    imagePicker.delegate = self;
//
//                    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//
//                    [self presentViewController:imagePicker animated:YES completion:nil];
//
//                    break;
//            }
//            break;
//        }
//        default:
//            break;
//    }
//}

#pragma mark- Open Image Picker Delegate to select image from Gallery or Camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UploadType=@"Image";
    [arrimg removeAllObjects];
    [arrimg addObject:myImage];
    
    //UIImage *image = [UIImage imageNamed:@"qr3"];
    CIImage *ciimage = [CIImage imageWithData:UIImageJPEGRepresentation(myImage, 1.0f)];
    
    NSDictionary *detectorOptions = @{ CIDetectorAccuracy : CIDetectorAccuracyHigh };
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:detectorOptions];
    
    NSArray *features = [faceDetector featuresInImage:ciimage];
    CIQRCodeFeature *faceFeature;
    NSString *strQR = @"";
    for(faceFeature in features) {
        strQR = [NSString stringWithFormat:@"%@",faceFeature.messageString];
        NSLog(@"Found feature: %@", strQR);
        if (([strQR isEqualToString:@""]) || (strQR == nil)) {
            
        } else {
            [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": strQR}];
        }
        //break;
    }
    if (([strQR isEqualToString:@""]) || (strQR == nil)) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"This is not a valid QR Code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [alert show];
    } else {
        //[super openNextTemplate];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *selectedAccount = [userDefault objectForKey:@"valNoRek"];
        if ([strQR isEqualToString:selectedAccount]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"You can't transfer to the same account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alert show];
        } else {
            [self.textFieldInput setText:strQR];
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)actionOpenAccount:(UIButton *)sender {
    [self getAccount:sender.tag];
}

- (void)getAccount:(NSUInteger)idx {
    //NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    //NSArray *arrAccount = [userDefault objectForKey:@"arrAccount"];
    //NSDictionary *dictAccount = [arrAccount objectAtIndex:idx];
    NSString *norek = [arrFA objectAtIndex:idx];
    
    [self.textFieldInput setText:norek];
    [vwListAccount removeFromSuperview];
}

- (IBAction)batalScan:(id)sender {
    [self closeScanner];
}

- (IBAction)scanQR:(id)sender {
//    CGFloat width = [UIScreen mainScreen].bounds.size.width;
//    CGFloat height = [UIScreen mainScreen].bounds.size.height;

    self.tabBarController.tabBar.hidden = NO;
    
    
    viewPreviewQR = [[UIView alloc]init];
    
    viewFlash = [[UIView alloc]init];
    [viewFlash setUserInteractionEnabled:YES];
    lblFlash = [[UILabel alloc]init];
    btnFlash = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFlash setImage:[UIImage imageNamed:@"ic_fpop_flashlight"] forState:UIControlStateNormal];
    
    [viewFlash addSubview:lblFlash];
    [viewFlash addSubview:btnFlash];
    [btnFlash addTarget:self action:@selector(actionFlashOnOff:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionFlashOnOff:)];
    [viewFlash addGestureRecognizer:singleFingerTap];

    stateFlash = false;

    
    if([Utility isLanguageID]){
        lblFlash.text = @"Senter";
    }else{
        lblFlash.text = @"Flaslight";
    }
    [lblFlash setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
    [lblFlash setTextColor:[UIColor whiteColor]];
    [lblFlash setTextAlignment:NSTextAlignmentCenter];
    [lblFlash sizeToFit];
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmVwPreviewQr = viewPreviewQR.frame;
    CGRect frmBtnBtl = self.btnBatalScanQR.frame;
    
    CGRect frmVwBtnFlash = viewFlash.frame;
    CGRect frmBtnFlash = btnFlash.frame;
    CGRect frmLblBtnFlash = lblFlash.frame;
    
    frmBtnBtl.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_DEF - frmBtnBtl.size.height/2 - 8;
    
    frmVwBtnFlash.origin.x = 8;
    frmVwBtnFlash.origin.y = frmBtnBtl.origin.y - 100;
    frmVwBtnFlash.size.height = 80;
    frmVwBtnFlash.size.width = 80;
    
    frmBtnFlash.size.width = 100;
    frmBtnFlash.size.height = 40;
    
    frmLblBtnFlash.origin.x = 3;
    frmLblBtnFlash.origin.y = frmBtnFlash.origin.y + frmBtnFlash.size.height;
    frmLblBtnFlash.size.width = frmVwBtnFlash.size.width;
    
    frmBtnFlash.origin.x = frmVwBtnFlash.size.width/2 - frmBtnFlash.size.width/2;
    
    
    frmVwPreviewQr.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmVwPreviewQr.origin.x = 0;
    frmVwPreviewQr.size.width = SCREEN_WIDTH;
    
    frmVwPreviewQr.size.height = SCREEN_HEIGHT - BOTTOM_NAV_DEF - frmVwPreviewQr.origin.y + 20;
    if ([Utility isDeviceHaveNotch]) {
        frmBtnBtl.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_NOTCH - frmBtnBtl.size.height;
        frmVwPreviewQr.size.height = SCREEN_HEIGHT - BOTTOM_NAV_NOTCH - frmVwPreviewQr.origin.y + 16;
    }
    
//    frmVwPreviewQr.size.height = frmBtnBtl.origin.y - frmVwPreviewQr.origin.y - 8;
    
    self.btnBatalScanQR.frame = frmBtnBtl;
    viewPreviewQR.frame = frmVwPreviewQr;
    viewFlash.frame = frmVwBtnFlash;
    lblFlash.frame = frmLblBtnFlash;
    btnFlash.frame = frmBtnFlash;
    
    [self changeHigthTable];
    
    
    session = [[AVCaptureSession alloc] init];
    device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (input) {
        [session addInput:input];
    } else {
        NSLog(@"Error: %@", error);
    }
    if (error == nil) {
        self.btnBatalScanQR.hidden = NO;
        output = [[AVCaptureMetadataOutput alloc] init];
        [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [session addOutput:output];
        
        output.metadataObjectTypes = [output availableMetadataObjectTypes];
        
        previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        previewLayer.frame = viewPreviewQR.frame;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.bounds = viewPreviewQR.bounds;
        [viewPreviewQR.layer addSublayer:previewLayer];
        
        //[[UIApplication sharedApplication].keyWindow addSubview:viewPreviewQR];
        [self.view addSubview:viewPreviewQR];
        [self.view addSubview:viewFlash];
        [self.view bringSubviewToFront:_btnBatalScanQR];
        [session startRunning];
        
//        [self createRectangle];
        [self makeFrameScanner];
        
        CGRect rectBorderArea = [previewLayer metadataOutputRectOfInterestForRect:[Utility converRectOfInterest:viewPreviewQR.bounds]];
        output.rectOfInterest = rectBorderArea;
        
        
    }else{
        
        NSString *strMsg, *strBtn;
        if ([lang isEqualToString:@"id"]) {
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"Sepertinya pengaturan privasi Anda mencegah kami mengakses kamera Anda untuk melakukan pemindaian barcode. Anda dapat memperbaikinya dengan melakukan hal berikut: \n\nSentuh tombol Buka di bawah untuk membuka aplikasi Pengaturan. \n\nHidupkan Kamera. \n\nBuka aplikasi ini dan coba lagi."];
            strBtn = @"Buka";
        }else{
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following: \n\nTouch the Go button below to open the Settings app.\n\nTurn the Camera on.\n\nOpen this app and try again."];
            strBtn = @"Go";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:strBtn otherButtonTitles:nil, nil];
        alert.tag = 1;
        [alert show];
    }
    
    
}

-(void) makeFrameScanner{
    
    UILabel *lblTitle = [Utility lblTitleQR];
    
    CGRect bounds = viewPreviewQR.layer.bounds;
    CGRect frmLblTitle = lblTitle.frame;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 32;
    frmLblTitle.size.width = bounds.size.width - (frmLblTitle.origin.x * 2);
    
    lblTitle.frame = frmLblTitle;
    
    [viewPreviewQR.layer addSublayer:[self frameScanner:bounds]];
    [viewPreviewQR addSubview:[self borderScanner:bounds]];
    [viewPreviewQR addSubview:lblTitle];
    [viewPreviewQR bringSubviewToFront:self.vwButton];
}


- (CAShapeLayer *) frameScanner : (CGRect ) vwScanner {
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:vwScanner];
    
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = (vwScanner.size.height - transparantWidth) * 0.5;
    
    [overlayPath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(transparantX,
                                                                        transparantY,
                                                                        transparantWidth, transparantWidth)]];
    
    overlayPath.usesEvenOddFillRule = true;
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [[[UIColor blackColor] colorWithAlphaComponent:0.5]CGColor];
    
    return fillLayer;
    
    
}

-(UIView *) borderScanner : (CGRect )vwScanner{
    
    
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = (vwScanner.size.height - transparantWidth) * 0.5;
    
    UIView *vwKotakScanner = [[UIView alloc]initWithFrame:CGRectMake(transparantX,
                                                                     transparantY,
                                                                     transparantWidth, transparantWidth)];
    
    CGFloat height = transparantWidth +4;
    CGFloat width = transparantWidth +4;
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(1, 25)];
    [path addLineToPoint:CGPointMake(1, 1)];
    [path addLineToPoint:CGPointMake(25, 1)];
    
    [path moveToPoint:CGPointMake(width - 30, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 25)];
    
    [path moveToPoint:CGPointMake(1, height - 30)];
    [path addLineToPoint:CGPointMake(1, height - 5)];
    [path addLineToPoint:CGPointMake(25, height - 5)];
    
    [path moveToPoint:CGPointMake(width - 30, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 30)];
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor = [UIColorFromRGB(const_color_primary) CGColor];
    pathLayer.lineWidth = 3.0f;
    pathLayer.fillColor = nil;
    
    UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, vwKotakScanner.frame.size.width, 1)];
    [vwLine setBackgroundColor:[UIColor yellowColor]];
    
    CGPoint start = CGPointMake(vwKotakScanner.frame.size.width/2,0);
    CGPoint end = CGPointMake(vwKotakScanner.frame.size.width/2, vwKotakScanner.frame.size.height+4);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.delegate = self;
    animation.fromValue = [NSValue valueWithCGPoint:start];
    animation.toValue = [NSValue valueWithCGPoint:end];
    animation.duration = 5;
    
    animation.repeatCount = HUGE_VALF;
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
    
    [vwKotakScanner.layer addSublayer:pathLayer];
    [vwKotakScanner addSubview:vwLine];
    [vwLine.layer addAnimation:animation forKey:@"position"];
    return vwKotakScanner;
}

-(void)createRectangle{
    
    kotakScan = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.layer.bounds.size.width - 80, self.view.layer.bounds.size.width - 80)];
//    kotakScan.center = viewPreviewQR.center;
//    [kotakScan setBackgroundColor:[self colorWithHex:@"ffffff" alpha:0]];
//    kotakScan.layer.borderColor = [[self colorWithHex:@"df0925" alpha:1.0] CGColor];
//    kotakScan.layer.borderWidth = 5;
//    [viewPreviewQR addSubview:kotakScan];
    
    UILabel *lblTitle = [Utility lblTitleQR];
    
    CGRect frmLblTitleScan = lblTitle.frame;
    CGRect frmVwPreviewQr = viewPreviewQR.frame;
    CGRect frmVwKotakScan = kotakScan.frame;
    
    frmVwKotakScan.origin.x = frmVwPreviewQr.size.width/2 - frmVwKotakScan.size.width/2;
    frmVwKotakScan.origin.y = frmVwPreviewQr.size.height/2 - frmVwKotakScan.size.height/2;
    
    frmLblTitleScan.origin.x = 16;
    frmLblTitleScan.size.width = frmVwPreviewQr.size.width - (frmLblTitleScan.origin.x *2);
    frmLblTitleScan.origin.y = frmVwKotakScan.origin.y - frmLblTitleScan.size.height - 16;
    
    
    kotakScan.frame = frmVwKotakScan;
    lblTitle.frame = frmLblTitleScan;
    
    CGFloat height = kotakScan.frame.size.height+4;
    CGFloat width = kotakScan.frame.size.width+4;
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(1, 25)];
    [path addLineToPoint:CGPointMake(1, 1)];
    [path addLineToPoint:CGPointMake(25, 1)];
    
    [path moveToPoint:CGPointMake(width - 30, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 25)];
    
    [path moveToPoint:CGPointMake(1, height - 30)];
    [path addLineToPoint:CGPointMake(1, height - 5)];
    [path addLineToPoint:CGPointMake(25, height - 5)];
    
    [path moveToPoint:CGPointMake(width - 30, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 30)];
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor = [UIColorFromRGB(const_color_primary) CGColor];
    pathLayer.lineWidth = 3.0f;
    pathLayer.fillColor = nil;
    
    
    UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kotakScan.frame.size.width, 1)];
    [vwLine setBackgroundColor:[UIColor yellowColor]];
    
    CGPoint start = CGPointMake(kotakScan.frame.size.width/2,0);
    CGPoint end = CGPointMake(kotakScan.frame.size.width/2, kotakScan.frame.size.height+4);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.delegate = self;
    animation.fromValue = [NSValue valueWithCGPoint:start];
    animation.toValue = [NSValue valueWithCGPoint:end];
    animation.duration = 5;
    
    animation.repeatCount = HUGE_VALF;
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
    //
    [kotakScan.layer addSublayer:pathLayer];
    [kotakScan addSubview:vwLine];
    [vwLine.layer addAnimation:animation forKey:@"position"];
    
    
    [viewPreviewQR addSubview:kotakScan];
    [viewPreviewQR addSubview:lblTitle];
    //[self.view bringSubviewToFront:kotakScan];
}

#pragma mark change batal to goToNext
- (IBAction)batal:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    [super backToRoot];
}

- (IBAction)next:(id)sender {
    [self goToCancel];
}

-(void)goToNext{
    if([self.textFieldInput.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldInput = [self.textFieldInput.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedStringtextFieldInput length]) {
            message = lang(@"STR_NOT_NUM");
        }
        
        if([message isEqualToString:@""]){
            if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
                [dataManager setPinNumber:self.textFieldInput.text];
                [super openNextTemplate];
            } else {
                if ([[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.textFieldInput.text}];
                    [super openNextTemplate];
                } else {
                    [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.textFieldInput.text}];
                    [super openNextTemplate];
                }
            }
        } else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textFieldInput resignFirstResponder];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSRange whiteSpaceRange = [string rangeOfCharacterFromSet:[NSCharacterSet whitespaceCharacterSet]];
    if (whiteSpaceRange.location != NSNotFound) {
        textField.text = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
        return false;
    } else {
        return true;
    }
    
    //    textField.text = @"4353535";
    //    return false;
}

-(void) showingRecentlyUses : (NSString *) menuId
                 mFieldName : (NSString *) fieldName{
    [self.vwRecentllyUse setHidden:TRUE];
    NSString *codeId = @"";
    
    if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00027"]) {
        codeId = @"451";
    }else if([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00028"]){
        codeId = @"452";
    }
    
    arDataRecently = [mHelper readListRecentlyUse:menuId mCode:codeId];
    if (arDataRecently.count > 0) {
        if ([fieldName isEqualToString:@"payment_id"]) {
            [self.vwRecentllyUse setHidden:FALSE];
            self.tblRecentlyUse.delegate = self;
            self.tblRecentlyUse.dataSource = self;
            [self.tblRecentlyUse setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
            [self.tblRecentlyUse registerClass:[CellRecentlyUse class] forCellReuseIdentifier:@"cell"];
            [self.tblRecentlyUse beginUpdates];
            [self.tblRecentlyUse endUpdates];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellRecentlyUse *cell = (CellRecentlyUse *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    
    [cell.lblContentOne setText:[data valueForKey:F_ACCT_TRSCT_NAME]];
    [cell.lblContentOne sizeToFit];
    cell.lblContentOne.numberOfLines = 0;

    [cell.lblContentTwo setText:[data valueForKey:F_ACCT_REK]];
    [cell.lblContentTwo sizeToFit];
    cell.lblContentTwo.numberOfLines = 0;

    CGRect frmVwBase = cell.vwBase.frame;
    CGRect frmLblContentOne = cell.lblContentOne.frame;
    CGRect frmLblContentTwo = cell.lblContentOne.frame;
    CGRect frmImgNext = cell.imgNext.frame;

    frmVwBase.origin.x = 16;
    frmVwBase.origin.y = 0;
    frmVwBase.size.width = cell.contentView.frame.size.width - (frmVwBase.origin.x * 2);
    frmVwBase.size.height = cell.contentView.frame.size.height;

    frmImgNext.size.height = 30;
    frmImgNext.size.width = 30;
    frmImgNext.origin.x = frmVwBase.size.width - frmImgNext.size.width;
    frmImgNext.origin.y = frmVwBase.size.height/2 - frmImgNext.size.height/2;

    frmLblContentOne.origin.x = 0;
    frmLblContentOne.origin.y = 16;
    frmLblContentOne.size.width = frmVwBase.size.width - frmImgNext.size.width - 8;

    frmLblContentTwo.origin.x = frmLblContentOne.origin.x;
    frmLblContentTwo.origin.y = frmLblContentOne.origin.y + frmLblContentOne.size.height + 8;
    frmLblContentTwo.size.width = frmLblContentOne.size.width;

    cell.vwBase.frame = frmVwBase;
    cell.imgNext.frame = frmImgNext;
    cell.lblContentOne.frame = frmLblContentOne;
    cell.lblContentTwo.frame = frmLblContentTwo;
    
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat padding = 16;
    CGFloat paddingMiddle = 8;
    CGFloat imgSize = 30;
    CGFloat actualWidth = SCREEN_WIDTH - imgSize - (padding*2) - paddingMiddle;
    
    NSDictionary *dataDict = [arDataRecently objectAtIndex:indexPath.row];
    
    CGSize mSzLblContentOne = [[dataDict valueForKey:F_ACCT_TRSCT_NAME] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:const_font_name1 size:15]}];
    CGSize mSzLblContentTwo = [[dataDict valueForKey:F_ACCT_REK] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:const_font_name1 size:14]}];
    
    CGFloat constrainHeight = 2000 + mSzLblContentOne.height +mSzLblContentTwo.height;
    
    CGSize constraint = CGSizeMake(actualWidth,constrainHeight);

    CGSize sizeLblCntnOne = [Utility heightWrapWithText:[dataDict valueForKey:F_ACCT_TRSCT_NAME]
                                               fontName:[UIFont fontWithName:const_font_name1 size:15] expectedSize:constraint];
    CGSize sizeLblCntnTwo = [Utility heightWrapWithText:[dataDict valueForKey:F_ACCT_REK]
                                               fontName:[UIFont fontWithName:const_font_name1 size:14] expectedSize:constraint];
    
    
    CGFloat mTotalHeight = padding + ceil(sizeLblCntnOne.height) + paddingMiddle + ceil(sizeLblCntnTwo.height) + padding;
    
    CGFloat mHeight = MAX(mTotalHeight, 50);
    return mHeight;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arDataRecently.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    if ([[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:[data valueForKey:F_ACCT_REK]}];
        [super openNextTemplate];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        if (UIApplicationOpenSettingsURLString)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }else{
        [self backToRoot];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [session stopRunning];
}

-(void)actionFlashOnOff : (UIButton *) sender{
    if (stateFlash) {
        if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] &&
            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOn)
        {
            stateFlash = NO;
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOff];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
        }
    }else{
        if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] &&
            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff)
        {
            stateFlash = YES;
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOn];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
        }
    }
}


@end

