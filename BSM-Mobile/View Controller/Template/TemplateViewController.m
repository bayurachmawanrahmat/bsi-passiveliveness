//
//  TemplateViewController.m
//  BSM Mobile
//
//  Created by lds on 5/8/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "TemplateViewController.h"
#import "HomeViewController.h"
#import "Utility.h"
#import <Accounts/Accounts.h>
#import "UIViewController+ECSlidingViewController.h"
#import "PopUpLoginViewController.h"
#import <Social/Social.h>
#import "PopupOnboardingViewController.h"
#import "Utility.h"
#import "CustomBtn.h"
#import "UIImageView+AFNetworking.h"
#import "UIImage+AFNetworking.h"


@interface TemplateViewController ()<UIAlertViewDelegate, UIActionSheetDelegate, ConnectionDelegate>{
    int totalButton;
    NSString *valAgree;
    NSString *valCancel;
    NSString *valTitle;
    NSString *valMsg;
    BOOL loginDone;
    
}

@end

@implementation TemplateViewController
- (void)reloadApp{
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    loginDone = false;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(popBC:) name:@"popLogin" object:nil];

    dataManager = [DataManager sharedManager];
    BOOL hasSocial = false;
    if([[self.jsonData valueForKey:@"social"]boolValue]){
        hasSocial = true;
//        UIButton *socialButton = [[UIButton alloc]init];
//        [socialButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
//        socialButton.frame = CGRectMake(280, 39.0, 30.0, 30.0);
//        [socialButton addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:socialButton];
    }
    totalButton = -1;
    BOOL hasFavorite = false;
    if ([[self.jsonData valueForKey:@"favorite"]boolValue]){
        hasFavorite = true;
//        UIButton *favButton = [[UIButton alloc]init];
//        [favButton setImage:[UIImage imageNamed:@"fav"] forState:UIControlStateNormal];
//        favButton.frame = CGRectMake((hasSocial?-35:0)+280, 39.0, 30.0, 30.0);
//        [favButton addTarget:self action:@selector(favorite) forControlEvents:UIControlEventTouchUpInside];
//        [self.view addSubview:favButton];
    }
    
    if(hasFavorite || hasSocial){
        
        UIButton *buttonRight = [[UIButton alloc]initWithFrame:CGRectMake(280.0, 37.0, 30.0, 30.0)];
        [buttonRight setImage:[UIImage imageNamed:@"button-left"] forState:UIControlStateNormal];
        [buttonRight addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:buttonRight];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(popBC:) name:@"popLogin" object:nil];
    loginDone = false;

}

- (void)backToRoot{
    [dataManager resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

- (TemplateViewController *)routeTemplateController:(NSString *)templateName {
    NSArray *sbList = [dataManager getStoryboardList];
    for (NSString* sbName in sbList) {
        @try {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:sbName bundle:nil];
            TemplateViewController *templateView = [storyboard instantiateViewControllerWithIdentifier:templateName];
            return templateView;
        } @catch (NSException *exception) {
            continue;
        }
    }
    return [self.storyboard instantiateViewControllerWithIdentifier:templateName];
}

- (void)openTemplate:(NSString *)templateName withData:(NSDictionary *)data{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
//    TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:templateName];
    TemplateViewController *templateView = [self routeTemplateController:templateName];
    if([templateName isEqualToString:@"LD01"]){
        [data setValue:@"Infaq" forKey:@"title"];
    }
    [templateView setJsonData:data];
    [self.navigationController pushViewController:templateView animated:YES];
}

- (void)openTemplateFromTabbar:(NSString *)templateName withData:(NSDictionary *)data{
    TemplateViewController *templateView = [self routeTemplateController:templateName];
    [templateView setJsonData:data];
    [self.navigationController pushViewController:templateView animated:YES];
    
    [self.tabBarController setSelectedIndex:0];
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // Put your code here
        [currentVC pushViewController:templateView animated:YES];
    });
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"popLogin" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"popLogin" object:nil];
}

- (void) popBC:(NSNotification *) notification{
    if([notification.name isEqualToString:@"popLogin"] && loginDone == false){
        NSDictionary *userInfo = notification.userInfo;
        if([[userInfo valueForKey:@"logged_in"] isEqualToString:@"YES"]){
            loginDone = true;
            @try {
                NSArray *arrSkipFeatType = @[@"QRIS", @"exit"];
                NSString *featType = [userInfo objectForKey:@"feat_type"];
                if(![arrSkipFeatType containsObject: featType]){
                    if([featType isEqualToString:@"MyQRVC"]){
                        TemplateViewController *templateViews = [self.storyboard instantiateViewControllerWithIdentifier:@"MyQRVC"];
                        [self.tabBarController setSelectedIndex:0];
                        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
                        [currentVC pushViewController:templateViews animated:YES];
                    } else {
                        [self openTemplate:[[dataManager getObjectData] valueForKey:@"template"] withData:[dataManager getObjectData]];
                    }
                }
            } @catch (NSException *exception) {
                DLog(@"%@", exception.description);
                [self openTemplate:[[dataManager getObjectData] valueForKey:@"template"] withData:[dataManager getObjectData]];
            }
        }
    }
}

-(void) openNextTemplateAvoidLogin{
    dataManager.currentPosition++;
    NSDictionary *temp = [dataManager getObjectData];
    BOOL canOpenTemplate = true;
    if([[temp valueForKey:@"activation"]boolValue]){
//        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"] ||
//           [[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"] == nil ||
//           [[[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"] isEqualToString:@""]){
//            canOpenTemplate = false;
//        }
        NSString *custid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        if(!custid ||
           custid == nil ||
           [custid isEqualToString:@""]){
            canOpenTemplate = false;
        }
    }
    if(canOpenTemplate){
        DLog(@"JSON DATA TEMPLATE: %@", temp);
        [self openTemplate:[temp valueForKey:@"template"] withData:temp];
    }else{
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"id"]){
            valTitle = @"Informasi";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Lakukan Aktivasi Terlebih Dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            //valCancel = @"Batal";
            //valAgree = @"Lanjut";
        } else {
            valTitle = @"Information";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            //valCancel = @"Cancel";
            //valAgree = @"Next";
        }
        
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:valTitle message:valMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         alert.tag = 100;
         [alert show];
        
        /*UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:valTitle
                              message:valMsg
                              delegate:nil
                              cancelButtonTitle:valCancel
                              otherButtonTitles:valAgree, nil];
        
        alert.tag = 229;
        [alert show];*/
    }
}

- (void)openNextTemplate{
    dataManager.currentPosition++;
    NSDictionary *temp = [dataManager getObjectData];
    BOOL canOpenTemplate = true;
    if([[temp valueForKey:@"activation"]boolValue]){
//        if(![[NSUserDefaults standardUserDefaults]objectForKey:@"customer_id"] ||
//           [[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"] == nil ||
//           [[[NSUserDefaults standardUserDefaults] valueForKey:@"customer_id"] isEqualToString:@""]){
//            canOpenTemplate = false;
//        }
        NSString *custid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        if(!custid ||
           custid == nil ||
           [custid isEqualToString:@""]){
            canOpenTemplate = false;
        }
    }
    if(canOpenTemplate){
        DLog(@"JSON DATA TEMPLATE: %@", temp);
        
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"mustLogin"] isEqualToString:@"YES"] &&
           [[[NSUserDefaults standardUserDefaults] valueForKey:@"hasLogin"] isEqualToString:@"NO"]
           ){
            loginDone = false;
            PopUpLoginViewController *popLogin = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PopupLogin"];
            [self presentViewController:popLogin animated:YES completion:nil];
        }else{
            if([[dataManager.dataExtra valueForKey:@"openfromtab"] isEqualToString:@"yes"]){
                [self openTemplateFromTabbar:[temp valueForKey:@"template"] withData:temp];
            } else {
                [self openTemplate:[temp valueForKey:@"template"] withData:temp];
            }
        }
        
    }else{
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"id"]){
            valTitle = @"Informasi";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Lakukan Aktivasi Terlebih Dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
            //valCancel = @"Batal";
            //valAgree = @"Lanjut";
        } else {
            valTitle = @"Information";
            //valMsg = lang(@"ACTIVATION_CONFIRM");
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
            //valCancel = @"Cancel";
            //valAgree = @"Next";
        }
        
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:valTitle message:valMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         alert.tag = 100;
         [alert show];
        
        /*UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:valTitle
                              message:valMsg
                              delegate:nil
                              cancelButtonTitle:valCancel
                              otherButtonTitles:valAgree, nil];
        
        alert.tag = 229;
        [alert show];*/
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100){
        [self openActivation];
    }
    else{
        [self backToRoot];
        
    }
    
    /*if (alertView.tag == 229) {
        //Check activate or not
        if (buttonIndex == 0) {
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        } else if (buttonIndex == 1) {
            //[self backToRoot];
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            if([lang isEqualToString:@"id"]){
                valTitle = @"Konfirmasi";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Batal";
                valAgree = @"Lanjut";
            } else {
                valTitle = @"Confirmation";
                valMsg = lang(@"REG_CONFIRM");
                valCancel = @"Cancel";
                valAgree = @"Next";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:valTitle
                                  message:valMsg
                                  delegate:self
                                  cancelButtonTitle:valCancel
                                  otherButtonTitles:valAgree, nil];
            
            alert.tag = 230;
            [alert show];
        }
    } else if (alertView.tag == 230) {
        //Check register or not
        if (buttonIndex == 0) {
            //Not registered
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"REG_NEEDED") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            //Back to Home
            NSDictionary* userInfo = @{@"position": @(313)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        } else if (buttonIndex == 1) {
            //Open Activation
            [self openActivation];
        }
    }*/
}

- (void)openActivation{
    [self backToRoot];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:@"NO" forKey:@"fromSliding"];
    [userDefault synchronize];
    UIViewController *viewCont = [self routeTemplateController:@"ActivationVC"];
//    RootViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
////    PopupOnboardingViewController *viewCont = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupOnboardingVC"];
    self.slidingViewController.topViewController = viewCont;
    
//    [self.slidingViewController t:viewCont animated:YES];
}

- (void)showMenu{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Menu" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    totalButton = 0;
    if([[self.jsonData valueForKey:@"favorite"]boolValue]){
        [actionSheet addButtonWithTitle:lang(@"FAVORITE")];
        totalButton++;
    }
    if ([[self.jsonData valueForKey:@"social"]boolValue]){
        [actionSheet addButtonWithTitle:lang(@"SHARE")];
        totalButton++;
    }
    [actionSheet addButtonWithTitle:lang(@"CANCEL")];
    [actionSheet setCancelButtonIndex:totalButton];
    actionSheet.tag = 10;
    [actionSheet showInView:self.view];
}

- (void)favorite{
    
//    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
//    if(![userDef objectForKey:@"favorite"]){
//        NSMutableArray *data = [[NSMutableArray alloc]initWithObjects:[[NSMutableArray alloc]init],[[NSMutableArray alloc]init],[[NSMutableArray alloc]init],nil];
//        [userDef setObject:data forKey:@"favorite"];
//    }
//
//    //add favorite to userdef
//    NSMutableArray *data = [[NSMutableArray alloc]initWithArray:[userDef objectForKey:@"favorite"]];
//    NSMutableDictionary *tempData = [[NSMutableDictionary alloc]init];
//    [tempData addEntriesFromDictionary:dataManager.dataExtra];
//    NSString *title = @"";
//    int index = -1;
//    int node = -1;
//    if(dataManager.menuId){
//        [tempData setObject:dataManager.menuId forKey:@"menu_id"];
//        
//        if([dataManager.menuId isEqualToString:@"00011"]){
//            title = @"PAM";
//            index = 1;
//            node = 2;
//        }else if([dataManager.menuId isEqualToString:@"00012"]){
//            title = @"PLN";
//            index = 1;
//            node = 2;
//        }else if([dataManager.menuId isEqualToString:@"00013"]){
//            title = @"TELEPON/HP";
//            index = 1;
//            node = 3;
//        }else if([dataManager.menuId isEqualToString:@"00014"]){
//            title = @"Pendidikan";
//            index = 1;
//            node = 3;
//        }else if([dataManager.menuId isEqualToString:@"00015"]){
//            title = @"TV Kabel/Internet";
//            index = 1;
//            node = 2;
//        }else if([dataManager.menuId isEqualToString:@"00016"]){
//            title = @"Angsuran/Cicilan";
//            index = 1;
//            node = 3;
//        }else if([dataManager.menuId isEqualToString:@"00017"]){
//            title = @"Tiket (Pesawat & Kereta Api)";
//            index = 1;
//            node = 3;
//        }else if([dataManager.menuId isEqualToString:@"00018"]){
//            title = @"Asuransi";
//            index = 1;
//            node = 2;
//        }else if([dataManager.menuId isEqualToString:@"00019"]){
//            title = @"Iuran Perumahan/Apartment";
//            index = 1;
//            node = 2;
//        }else if([dataManager.menuId isEqualToString:@"00020"]){
//            title = @"Retailer/Distributor";
//            index = 1;
//            node = 2;
//        }else if([dataManager.menuId isEqualToString:@"00021"]){
//            title = @"Zakat/Infaq/Sedekah";
//            index = 1;
//            node = 3;
//        }else if([dataManager.menuId isEqualToString:@"00022"]){
//            title = @"Pajak";
//            index = 1;
//            node = 1;
//        }else if([dataManager.menuId isEqualToString:@"00023"]){
//            title = @"Pinjaman Bank";
//            index = 1;
//            node = 1;
//        }else if([dataManager.menuId isEqualToString:@"00024"]){
//            title = @"Lain-lain";
//            index = 1;
//            node = 4;
//        }
//        
//        else if([dataManager.menuId isEqualToString:@"00025"]){
//            title = @"Voucher HP";
//            index = 0;
//            node = 3;
//        }else if([dataManager.menuId isEqualToString:@"00026"]){
//            title = @"PLN Prepaid";
//            index = 0;
//            node = 2;
//        }
//        
//        
//        else if([dataManager.menuId isEqualToString:@"00027"]){ //transfer
//            title = @"Transfer BSM";
//            index = 2;
//            node = 2;
//        }else if([dataManager.menuId isEqualToString:@"00028"]){ //transfer antar bank
//            title = @"Transfer Non BSM";
//            index = 2;
//            node = 4;
//        }
//        
//    }
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    conn.fav = true;
    [conn sendPost:[[DataManager sharedManager]favData] needLoading:true textLoading:lang(@"ADD_FAV") encrypted:true];
    
    
//    if([title isEqualToString:@""]){
//        title = [self.jsonData valueForKey:@"title"];
//    }
//    [tempData setObject:title forKey:@"title"];
//    
//    if(node != -1){
//        [tempData setObject:[NSNumber numberWithInteger:node] forKey:@"node"];
//    }
//    NSMutableArray *tempSubData = [[NSMutableArray alloc]initWithArray:[data objectAtIndex:index]];
//    [tempSubData addObject:tempData];
//    [data replaceObjectAtIndex:index withObject:tempSubData];
////    [(NSMutableArray *)[data objectAtIndex:index] addObject:tempData];
//    [userDef setObject:data forKey:@"favorite"];
//    [userDef synchronize];
//    NSString *lang = [[userDef objectForKey:@"AppleLanguages"]objectAtIndex:0];
//    BOOL isIna = [lang isEqualToString:@"id"];
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:isIna?@"Berhasil":lang(@"SUCCESS") message:isIna?@"Data berhasil ditambahkan ke dalam favorit":@"Transaction successfully add to favorite" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
}
- (void)share{
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:lang(@"SHARE") delegate:self cancelButtonTitle:lang(@"CANCEL") destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter", nil];
    actionSheet.tag = 20;
    [actionSheet showInView:self.view];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return  UIStatusBarStyleDefault;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 2 || buttonIndex == totalButton){
        return;
    }
    if(actionSheet.tag == 10){
        if(buttonIndex ==0){
            [self favorite];
        }else{
            [self share];
        }
    }else{
        SLComposeViewController *composeController = [SLComposeViewController
                                                      composeViewControllerForServiceType:buttonIndex==0?SLServiceTypeFacebook:SLServiceTypeTwitter];
        
        [composeController setInitialText:[dataManager.dataExtra objectForKey:@"share"]?[dataManager.dataExtra valueForKey:@"share"]:@""];
        [composeController addImage:[UIImage imageNamed:@"icon60"]];
        composeController.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone:{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:lang(@"SUCCESS") message:lang(@"SHARE_SUCCESS") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alertView show];
                    });
                } break;
            }
        };
        
        [self presentViewController:composeController
                           animated:YES completion:nil];
    }
    //    }
}

#pragma mark - Connection Delegate{
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    DLog(@"jsonObject = %@", jsonObject);
    if([requestType isEqualToString:@"zpk"]){
        [self backToRoot];
    }else{
        if (![requestType isEqualToString:@"check_notif"]) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:lang(@"SUCCESS") message:lang(@"FAV_SUCCESS") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
       
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
