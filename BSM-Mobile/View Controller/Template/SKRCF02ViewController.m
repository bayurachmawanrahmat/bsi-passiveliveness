//
//  CF02ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "SKRCF02ViewController.h"
#import "Connection.h"
#import "Utility.h"

@interface SKRCF02ViewController ()<ConnectionDelegate, UIAlertViewDelegate>
- (IBAction)ok:(id)sender;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonOk;
@property (weak, nonatomic) IBOutlet UIButton *buttonFav;
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;
@property (weak, nonatomic) IBOutlet UITextView *textViewConfirmation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@end

@implementation SKRCF02ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.automaticallyAdjustsScrollViewInsets = false;
    [self.textViewConfirmation setFont:[UIFont systemFontOfSize:17.0]];
   
    [Styles setTopConstant:_topConstraint];
      
    [self setupLayout];
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url = [NSString stringWithFormat:@"%@,file_name_ktp=%@,file_name_npwp=%@,hp,email,ktp,npwp,status",[self.jsonData valueForKey:@"url_parm"],[dataManager.dataExtra valueForKey:@"file_name_ktp"],[dataManager.dataExtra valueForKey:@"file_name_npwp"] ];
         Connection *conn = [[Connection alloc]initWithDelegate:self];
          [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        
//        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
//        Connection *conn = [[Connection alloc]initWithDelegate:self];
//        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
    else {
        NSString *status = [dataManager.dataExtra valueForKey:@"status"];
        NSString *code = [dataManager.dataExtra valueForKey:@"code"];
        if([status isEqualToString:@"ACTIVE"] && [code isEqualToString:@"00077"]) {
            NSString *url_parm = @"request_type=list_sukuk,menu_id,device,device_type,ip_address,language,date_local,id_account,customer_id,amount,code,transaction_id,mobile_no,email,hp";
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url_parm needLoading:true encrypted:true banking:true favorite:false];
        }

    }
//    if(self.jsonData){
//        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
//        if(fav){
//            //            [self.buttonOk setHidden:true];
//            [self.buttonFav setHidden:false];
//            [self.buttonShare setHidden:false];
//        }
//
//
//    }    // Do any additional setup after loading the view.
    //    [self.labelConfirmation addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    
//    [self.buttonOk setTitle:@"Pemesanan" forState:UIControlStateNormal];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //    [self.labelConfirmation removeObserver:self forKeyPath:@"contentSize"];
}

- (void)viewDidDisappear:(BOOL)animated{
    //
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void) setupLayout{
    CGRect frmTextConfirm  = self.textViewConfirmation.frame;
    CGRect frmBtnOk  = self.buttonOk.frame;
    CGRect frmTitle = self.lblTitle.frame;
    
    frmBtnOk.size.height = 42;
    frmBtnOk.origin.x = 16;
    frmBtnOk.size.width = SCREEN_WIDTH - (frmBtnOk.origin.x *2);
    frmBtnOk.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtnOk.size.height - 16;
    
//    frmTextConfirm.origin.y = frmTitle.origin.y + frmTitle.size.height+16;
    
    if ([Utility isDeviceHaveNotch]) {
        frmBtnOk.origin.y = SCREEN_HEIGHT - BOTTOM_NAV - frmBtnOk.size.height - 40;
        frmTextConfirm.origin.y = TOP_NAV + 70;
    }
    
    frmTextConfirm.size.height = frmBtnOk.origin.y - frmTextConfirm.origin.y;
    
    self.textViewConfirmation.frame = frmTextConfirm;
   
    self.lblTitle.frame = frmTitle;
    self.buttonOk.frame = frmBtnOk;
    
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        if([requestType isEqualToString:@"insert_favorite"]){
            [super didFinishLoadData:jsonObject withRequestType:requestType];
        }else{
            if (![requestType isEqualToString:@"check_notif"]) {
                id response = [jsonObject objectForKey:@"response"];
                NSString *stringResponse;
                
                if([[dataManager.dataExtra objectForKey:@"status"]isEqualToString:@"ACTIVE"] ||
                   [[dataManager.dataExtra objectForKey:@"status"]isEqualToString:@"REGISTER"]){
                    if([Utility isLanguageID]){
                        [self.buttonOk setTitle:@"Pemesanan" forState:UIControlStateNormal];
                    }else{
                        [self.buttonOk setTitle:@"Order" forState:UIControlStateNormal];
                    }
                }
                
                if([response isKindOfClass:[NSArray class]]){
                    NSArray *responseArray = response;
                    stringResponse = [response objectAtIndex:0];
                    if(responseArray.count > 1){
                        if([[response objectAtIndex:1]isKindOfClass:[NSArray class]]){
                            NSArray *temp = [Utility changeToArray:[response objectAtIndex:1]];
                            stringResponse = [NSString stringWithFormat:@"%@\n%@",stringResponse,[temp  componentsJoinedByString:@"\n"]];
                        }else{
                            stringResponse = [NSString stringWithFormat:@"%@\n%@",stringResponse,[response objectAtIndex:1]];
                        }
                    }
                }else{
                    stringResponse = [jsonObject valueForKey:@"response"];
                    NSLog(@"%@",[jsonObject valueForKey:@"response"]);
                }
                
                if([jsonObject objectForKey:@"transaction_id"]){
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"] }];
                }
                if([jsonObject objectForKey:@"share"]){
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"share":[jsonObject valueForKey:@"share"] }];
                }
                [self.textViewConfirmation setText:stringResponse];
                [self setupLayout];
            }
            
        }
        //        [self.labelConfirmation sizeToFit];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        //        [self.labelConfirmation setText:];
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
- (IBAction)makeFavorite:(id)sender {
    //    data addObject:@{@"name":[self.jsonData valueForKey:@"title"]}
}

- (IBAction)ok:(id)sender {
    if([self.buttonOk.currentTitle isEqualToString:@"Pemesanan"] || [self.buttonOk.currentTitle isEqualToString:@"Order"]){
        [self openTemplateByMenuID:@"00069" andCodeContent:@"00078"];
    }else{
        [super backToRoot];
    }
}
- (IBAction)share:(id)sender {
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}
@end

