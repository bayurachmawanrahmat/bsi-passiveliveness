//
//  SKB01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 25/09/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SKB01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>

@interface SKB01ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;
@property (weak, nonatomic) IBOutlet UILabel *lblFilename;
@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;
- (IBAction)cancel:(id)sender;
- (IBAction)next:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblCaption;

@end

@implementation SKB01ViewController

NSString *upType;
NSString *upMethod;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblCaption.text = @"Pilih File";
    } else {
        self.lblCaption.text = @"Choose File";
    }
    
    if ([[self.jsonData objectForKey:@"field_name"] isEqualToString:@"ktp"]) {
        self.lblTitle.text = @"KTP";
        upType = @"KTP";
    } else if ([[self.jsonData objectForKey:@"field_name"] isEqualToString:@"npwp"]) {
        self.lblTitle.text = @"NPWP";
        upType = @"NPWP";
    }
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.imgTitle setHidden:YES];
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    CGRect frmButton = self.btnUpload.frame;
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmLine = self.vwLine.frame;
    CGRect frmLabel = self.lblFilename.frame;
    CGRect frmButton2 = self.vwButton.frame;
    CGRect frmCaption = self.lblCaption.frame;
    CGRect frmBtnN = self.btnN.frame;
    CGRect frmBtnB = self.btnB.frame;
    
    frmCaption.origin.y = frmTitle.origin.y + frmTitle.size.height + 10;
    frmButton.origin.x = 16;
    frmButton.origin.y = frmCaption.origin.y + frmCaption.size.height + 10;
    
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 50;
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmLine.origin.x = 16;
    frmLine.size.width = screenWidth - 32;
    frmLine.origin.y = frmButton.origin.y + frmButton.size.height + 10;
    frmLabel.origin.x = frmButton.origin.x + frmButton.size.width + 20;
    frmLabel.origin.y = frmLine.origin.y - frmLabel.size.height - 20;
    frmButton2.origin.y = frmLine.origin.y + 20;
    frmButton2.origin.x = 16;
    frmButton2.size.width = screenWidth - 32;
    frmBtnB.origin.x=0;
    frmBtnB.size.width = (frmButton2.size.width/2)-8;
    frmBtnB.size.height = 40;
    frmBtnN.origin.x= frmBtnB.size.width+frmBtnB.origin.x+16;
    frmBtnN.size.width = frmBtnB.size.width;
    frmBtnN.size.height = 40;
    
    self.btnB.frame = frmBtnB;
    self.btnN.frame = frmBtnN;
    self.btnUpload.frame = frmButton;
    self.vwTitle.frame = frmTitle;
    self.lblTitle.frame = frmLblTitle;
    self.vwLine.frame = frmLine;
    self.lblFilename.frame = frmLabel;
    self.vwButton.frame = frmButton2;
    self.lblCaption.frame = frmCaption;
    
    [self.btnB setColorSet:SECONDARYCOLORSET];
    [self.btnN setColorSet:PRIMARYCOLORSET];
    
    [self.btnN setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.btnB setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    arrimg =[[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)cancel:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:@"-" forKey:@"last"];
    [[DataManager sharedManager] resetObjectData];
    [self.navigationController popToRootViewControllerAnimated:YES];
    if(self.tabBarController.selectedIndex != 0){
        [self.tabBarController setSelectedIndex:0];
    }
}

- (IBAction)next:(id)sender {
    
    if ([UploadType isEqualToString:@"PDF"])
    {
        [self uploadpdf];
    }
    else
    {
        if(arrimg.count > 0){
            NSData *imgData;
            if ([upMethod isEqualToString:@"TAKE"]) {
                imgData = UIImageJPEGRepresentation([arrimg objectAtIndex:0], 0.15);
            } else if ([upMethod isEqualToString:@"CHOOSE"]) {
                imgData = UIImageJPEGRepresentation([arrimg objectAtIndex:0], 0.75);
            }
            NSString *encodedImage = [self base64StringForImage_objc:[UIImage imageWithData:imgData]];
            encodedImage = [Utility compressAndEncodeToBase64String:[UIImage imageWithData:imgData] withMaxSizeInKB:500];

            if ([[self.jsonData objectForKey:@"field_name"] isEqualToString:@"ktp"]) {
                [dataManager.dataExtra addEntriesFromDictionary:@{@"ktp":encodedImage}];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"file_name_ktp":self.lblFilename.text}];
                [super openNextTemplate];
            } else if ([[self.jsonData objectForKey:@"field_name"] isEqualToString:@"npwp"]) {
                [dataManager.dataExtra addEntriesFromDictionary:@{@"npwp":encodedImage}];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"file_name_npwp":self.lblFilename.text}];
                [super openNextTemplate];
            }
        }else if([[self.jsonData objectForKey:@"field_name"] isEqualToString:@"npwp"]){
            [dataManager.dataExtra addEntriesFromDictionary:@{@"npwp":@""}];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"file_name_npwp":@""}];
            [super openNextTemplate];
        }else{
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            NSString *msg = @"";

            if([lang isEqualToString:@"id"]){
                msg = [NSString stringWithFormat:@"%@ tidak boleh kosong",[self.jsonData objectForKey:@"field_name"]];
            }else{
                msg = [NSString stringWithFormat:@"%@ cannot empty",[self.jsonData objectForKey:@"field_name"]];
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        

    }
}

- (NSString *) base64StringForImage_objc:(UIImage *)image {
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData base64EncodedStringWithOptions:0];
}

- (UIImage *) imageFromBase64String_objc:(NSString *)string {
    NSData *imageData = [[NSData alloc] initWithBase64EncodedString: string options: 0];
    return [[UIImage alloc] initWithData:imageData];
}

#pragma mark- Choose File or Image
- (IBAction)browseFile:(id)sender
{

    NSString *selectTitle = @"";
    NSString *cancelTitle = @"";
    NSString *takePhoto = @"";
    NSString *chooseExisting = @"";
    
    if([Utility isLanguageID]){
        selectTitle = @"Pilih opsi foto:";
        cancelTitle = @"Batal";
        takePhoto = @"Ambil Foto";
        chooseExisting = @"Pilih di gallery";
    }else{
        selectTitle = @"Select Photo option:";
        cancelTitle = @"Cancel";
        takePhoto = @"Take Photo";
        chooseExisting = @"Choose Existing";
    }
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:selectTitle delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:
                            takePhoto,
                            chooseExisting,
                            nil];
    
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma  mark- Opne Action Sheet for Options

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    upMethod = @"TAKE";
                    imagePicker = [[UIImagePickerController alloc] init];
                    
                    // Set source to the camera
                    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                    
                    // Delegate is self
                    imagePicker.delegate = self;
                    
                    // Allow editing of image ?
                    
                    // Show image picker
                    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    [self presentViewController:imagePicker animated:YES completion:nil];
                    
                    break;
                case 1:
                    upMethod = @"CHOOSE";
                    imagePicker = [[UIImagePickerController alloc] init];
                    
                    imagePicker.delegate = self;
                    
                    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
                    
                    [self presentViewController:imagePicker animated:YES completion:nil];
                    
                    break;
                    
                    /*case 2:
                     [self showDocumentPickerInMode:UIDocumentPickerModeOpen];
                     break;*/
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark- Open Document Picker(Delegate) for PDF, DOC Slection from iCloud


- (void)showDocumentPickerInMode:(UIDocumentPickerMode)mode
{
    
    UIDocumentMenuViewController *picker =  [[UIDocumentMenuViewController alloc] initWithDocumentTypes:@[@"com.adobe.pdf"] inMode:UIDocumentPickerModeImport];
    
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}


-(void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker
{
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller
  didPickDocumentAtURL:(NSURL *)url
{
    PDFUrl= url;
    UploadType=@"PDF";
    [arrimg removeAllObjects];
    [arrimg addObject:url];
    
}

#pragma mark- Open Image Picker Delegate to select image from Gallery or Camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UploadType=@"Image";
    [arrimg removeAllObjects];
    [arrimg addObject:myImage];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *upFile = @"";
    if ([upType isEqualToString:@"KTP"]) {
        upFile = [NSString stringWithFormat:@"KTP_%@.jpg",[dateFormatter stringFromDate:[NSDate date]]];
    } else if ([upType isEqualToString:@"NPWP"]) {
        upFile = [NSString stringWithFormat:@"NPWP_%@.jpg",[dateFormatter stringFromDate:[NSDate date]]];
    }
    [self.lblFilename setText:upFile];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Upload Image, PDF or Doc to Server

#pragma mark - Upload Image

-(void)uploadimage
{
    //loader start
    APIManager *obj =[[APIManager alloc]init];
    [obj setDelegate:(id)self];
    NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
    [dict setValue:@"SOME PARAMETER" forKey:@"type"];
    [dict setValue:@"SOME PARAMETER" forKey:@"title"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@",@"API URL"];
    NSURL *url =[NSURL URLWithString:urlString];
    [obj startRequestForImageUploadingWithURL:url withRequestType:(kAPIManagerRequestTypePOST) withDataDictionary:dict arrImage:arrimg CalledforMethod:imageupload index:0 isMultiple:NO str_imagetype:@"image"];
}

#pragma mark - Upload PDF

-(void)uploadpdf
{
    APIManager *obj =[[APIManager alloc]init];
    [obj setDelegate:(id)self];
    NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
    [dict setValue:@"SOME PARAMETER" forKey:@"type"];
    [dict setValue:@"SOME PARAMETER" forKey:@"title"];
    NSString *urlString = [NSString stringWithFormat:@"%@",@"API URL"];
    NSURL *url =[NSURL URLWithString:urlString];
    [obj startRequestForImageUploadingWithURL:url withRequestType:(kAPIManagerRequestTypePOST) withDataDictionary:dict arrImage:arrimg CalledforMethod:imageupload index:0 isMultiple:NO str_imagetype:@"pdf"];
}

#pragma mark-API Manager Delegate Method for Succes or Failure

-(void)APIManagerDidFinishRequestWithData:(id)responseData withRequestType:(APIManagerRequestType)requestType CalledforMethod:(APIManagerCalledForMethodName)APImethodName tag:(NSInteger)tag
{
    if (APImethodName ==imageupload) {
        NSDictionary *responsedata=(NSDictionary*)responseData;
        NSLog(@"data==%@",responsedata);
        
        NSString * Success= [responsedata valueForKey:@"success"];
        NSString * Message= [responseData valueForKey:@"message"];
        
        BOOL SuccessBool= [Success boolValue];
        
        if (SuccessBool)
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Server Error" message:Message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                                  }];
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void)APIManagerDidFinishRequestWithError:(NSError *)error withRequestType:(APIManagerRequestType)requestType CalledforMethod:(APIManagerCalledForMethodName)APImethodName tag:(NSInteger)tag
{
    if (APImethodName ==imageupload) {
        NSLog(@"image didfailedupload");
    }
}

@end

