//
//  LV01ViewController.h
//  BSM Mobile
//
//  Created by lds on 5/8/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

@interface LV01ViewController : TemplateViewController

- (void)setDataLocal:(NSDictionary*)object;
- (void)setFirstLV:(BOOL)firstLV;
- (void)setFromHome:(BOOL)fromHome;
- (void)setSpecial:(BOOL)special;
@property (nonatomic, retain) NSString * menu_file;
@end
