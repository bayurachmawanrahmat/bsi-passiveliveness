//
//  TGRCPViewController.h
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 12/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TGRCPViewController : TemplateViewController
@property (weak, nonatomic) IBOutlet UILabel *lblRespon;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@end

NS_ASSUME_NONNULL_END
