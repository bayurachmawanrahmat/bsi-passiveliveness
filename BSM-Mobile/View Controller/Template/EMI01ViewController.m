//
//  EMI01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 22/06/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "EMI01ViewController.h"
#import "EMSCANViewController.h"
#import "CustomBtn.h"

@interface EMI01ViewController ()<EMSCANDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    NSDictionary *infoCard;
    
}
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *tfCardNumber;
@property (weak, nonatomic) IBOutlet UIImageView *icEmoney;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation EMI01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if(self.jsonData){
        self.titleBar.text = [self.jsonData objectForKey:@"title"];
    }
    [self.imgIconBar setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    
    [Styles setTopConstant:self.topConstant];
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    NSString *desc = @"";
    if([language isEqualToString:@"id"]){
        desc = @"Untuk handphone yang tidak mendukung fitur NFC, silahkan masukkan nomor kartu e-money anda untuk melakukan transaksi\n\nSetelah transaksi, perbaharui saldo kartu e-money anda di ATM mandiri atau perbaharui menggunakan ponsel yang memiliki fitur NFC dan aplikasi BSI Mobile";
        
        [self.btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
        
        [self.lblCardNumber setText:@"No. Kartu"];
    }else{
        desc = @"Mobile phone that do not support the NFC feature, please enter your e-money card number to make a transaction\n\nAfter the transaction is success, please update balance of your e-money card at Mandiri ATM or Update using a mobile phone which has NFC feature dan BSI Mobile Application";
        
        [self.btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        
        [self.lblCardNumber setText:@"Card No."];
    }
    
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    
    [_lblCardNumber setFont:[UIFont fontWithName:const_font_name3 size:16]];
    
    [self.lblDescription setText:desc];
    
    [self.tfCardNumber setKeyboardType:UIKeyboardTypeNumberPad];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tapScanning =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionScanning:)];
    [self.icEmoney addGestureRecognizer:tapScanning];
    [self.icEmoney setUserInteractionEnabled:YES];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.tfCardNumber.inputAccessoryView = keyboardDoneButtonView;
    
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}


-(void)actionScanning:(UITapGestureRecognizer *)recognizer{
    if (@available(iOS 13.0, *)) {
        EMSCANViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"EMSCAN"];
        [templateView setDelegate:self];
        if([language isEqualToString:@"id"]){
            [templateView setTitleBar:@"Top Up Saldo"];
        }else{
            [templateView setTitleBar:@"Top Up Balance"];
        }
        
        UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
        [currentVC pushViewController:templateView animated:YES];
    }else{
        NSString *msg = @"";
        if ([language isEqualToString:@""]) {
            msg = @"Mohon maaf fitur ini tersedia untuk iOS 13+, silahkan update iOS anda";
        }else{
            msg = @"I'm sorry this feature available for iOS 13+, please update your iOS";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void) actionNext{
    if(![self.tfCardNumber.text isEqualToString:@""]){
        [dataManager.dataExtra setValue:self.tfCardNumber.text forKey:@"payment_id"];
        [self openNextTemplate];
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void) actionCancel{
    [self backToR];
}

- (void)cardInformation:(NSDictionary *)data{
    infoCard = data;
    self.tfCardNumber.text = [[data valueForKey:@"cardnumber"] stringByReplacingOccurrencesOfString:@"-" withString:@""];
}


@end
