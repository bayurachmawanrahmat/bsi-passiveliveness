//
//  DTNTF01ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 11/07/19.
//  Modified by Novriansyah Amini on 30/10/21.
//
//  Copyright © 2019 lds. All rights reserved.
//

#import "DTNTF01ViewController.h"
#import "Utility.h"
#import "InboxHelper.h"
#import "UIImageView+AFNetworking.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MenuViewController.h"
#import "NSString+HTML.h"
#import <QuartzCore/QuartzCore.h>
#import "Styles.h"


@interface DTNTF01ViewController () <ConnectionDelegate, UIScrollViewDelegate, UIAlertViewDelegate>{
    NSString *language;
    NSString *judulNotif;
    NSUserDefaults *userDefault;
    NSString *receivNotif;
    NSString *action;
    NSString *flagButton, *paramAutosave, *labelButton, *selectedMenuID;
    NSDictionary *dataNotif;
    NSMutableArray *listNotification;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScrollContent;

@property (weak, nonatomic) IBOutlet UIView *vwBannerContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnNavBack;

@property (weak, nonatomic) IBOutlet UITextView *lblDesckripsiTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCenter;

@property (weak, nonatomic) IBOutlet UILabel *lblBelaku;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@property (weak, nonatomic) IBOutlet UIView *viewButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;


- (IBAction)actionNavBack:(id)sender;

@end

@implementation DTNTF01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([language isEqualToString:@"id"]){
        [self.btnCancel setTitle:@"Kembali" forState:UIControlStateNormal];
    }else{
        [self.btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    self.vwScrollContent.delegate = self;
    
    [self.lblDesckripsiTitle setText:@""];

    self.lblTitleCenter.numberOfLines = 0;
    [self.lblTitleCenter setText:@""];
    [self setupLayout];
    
    [_btnCancel.layer setCornerRadius:20.0f];
    [_btnCancel.layer setBorderWidth:1.0f];
    [_btnCancel.layer setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor];
    [_btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.vwTitle setBackgroundColor:[UIColor clearColor]];
    self.btnNavBack.layer.cornerRadius = self.btnNavBack.frame.size.height/2;
    [self.btnNavBack setBackgroundColor:UIColorFromRGB(const_color_primary)];
    [self.lblTitle setTextColor:[UIColor clearColor]];
    
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    
    [_viewButton setHidden:true];

}

-(void) viewWillAppear:(BOOL)animated{
    [self openNotification];
}

-(void)openNotification{
    action =[userDefault objectForKey:@"action"];
    if (action != nil) {
        receivNotif = action;
        NSDictionary* userInfo = @{@"position": @(1113)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
    }
    
    NSString *strUrl = [NSString stringWithFormat:@"request_type=detail_notif,customer_id,mid,nid"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

-(void) setupLayout{
    
    UIView *roundedView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.vwBannerContainer.frame.size.width, self.vwBannerContainer.frame.size.height)];
    
    roundedView.layer.cornerRadius = 16.0f;
    roundedView.layer.masksToBounds = YES;
    
    self.vwBannerContainer.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    self.vwBannerContainer.layer.shadowOffset = CGSizeMake(2.f, .2f);
    self.vwBannerContainer.layer.shadowRadius = 3.0f;
    self.vwBannerContainer.layer.shadowOpacity = 0.8f;
    self.vwBannerContainer.layer.cornerRadius = 16.0f;
    

    self.lblDesckripsiTitle.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
    
    if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0] isEqualToString:@"id"]){
        [_lblBelaku setText:@"Tanggal"];
    }else{
        [_lblBelaku setText:@"Date"];
    }
    
}

- (void)actionCancel{
    [self backToR];
}

- (void) checkButtonFlag{

    if([flagButton isEqualToString:@"1"]){
        
        NSArray *paramArr = [paramAutosave componentsSeparatedByString:@"|"];
        @try {
            [dataManager.dataExtra setValue:paramArr[0] forKey:@"destacctno"];
            [dataManager.dataExtra setValue:paramArr[1] forKey:@"id_qurban"];
            selectedMenuID = paramArr[2];
        } @catch (NSException *exception) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")
                                                                           message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                UINavigationController *navigationController = self.navigationController;
                       [navigationController popViewControllerAnimated:YES];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }

        [_actionButton setTitle:labelButton forState:UIControlStateNormal];
        [_actionButton addTarget:self action:@selector(gotoMenu) forControlEvents:UIControlEventTouchUpInside];
        [_viewButton setHidden:false];
        
    }else if([flagButton isEqualToString:@"2"]){

        @try {
            [dataManager.dataExtra addEntriesFromDictionary:[dataNotif mutableCopy]];
            selectedMenuID = [dataNotif valueForKey:@"menuid"];
            if(![[dataNotif valueForKey:@"feop"]isEqualToString:@""] && [dataNotif valueForKey:@"feop"] != nil){
                _lblDate.text = [dataNotif valueForKey:@"feop"];
                _lblBelaku.text = lang(@"END_OF_PERIODE");
            }
            
        } @catch (NSException *exception) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")
                                                                           message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                UINavigationController *navigationController = self.navigationController;
                       [navigationController popViewControllerAnimated:YES];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }

        
        [_actionButton setTitle:labelButton forState:UIControlStateNormal];
        [_actionButton addTarget:self action:@selector(gotoMenu) forControlEvents:UIControlEventTouchUpInside];
        [_viewButton setHidden:false];
    }
}

- (void) gotoMenu{
    if([selectedMenuID isEqualToString:@""] || [labelButton isEqualToString:@"OK"]){
        [self backToR];
    }else{
        [self openTemplateByMenuIDWithLogin:selectedMenuID];
    }
}

- (void) getListUpdateResi{
    NSString *strUrl = [NSString stringWithFormat:@"request_type=list_update_receipt,customer_id"];

    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

- (void) updateResi : (NSString*) jsonObject{
    NSMutableArray *listGid = [[NSMutableArray alloc]init];

    NSArray *dataReceipt = [jsonObject valueForKey:@"response"];
    for(int i = 0; i < dataReceipt.count; i++){
        NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
        [dictList setValue:[dataReceipt[i] objectForKey:@"footer_st"] forKey:@"footer_msg"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"jenis_st"] forKey:@"jenis"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"marked_st"] forKey:@"marked"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"msg_st"] forKey:@"msg"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"template_st"] forKey:@"template"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"time_st"] forKey:@"time"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"title_st"] forKey:@"title"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"trid_st"] forKey:@"transaction_id"];
        [dictList setValue:[dataReceipt[i] objectForKey:@"noref_st"] forKey:@"trxref"];
        [listGid addObject:[dataReceipt[i] objectForKey:@"id_ni"]];
        
        InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
        [inboxHeldper insertDataInbox:dictList];
    }
    
    NSString *strUrl = [NSString stringWithFormat:@"request_type=update_receipt,gid"];
    [dataManager.dataExtra setValue:[listGid componentsJoinedByString:@","] forKey:@"gid"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                if ([requestType isEqualToString:@"update_notif"]) {
                    NSString *strUrl = [NSString stringWithFormat:@"request_type=detail_notif,customer_id,mid,nid"];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }else if([requestType isEqualToString:@"detail_notif"]){
                    
                    NSString *response = [jsonObject objectForKey:@"response"];
                    NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    
                    [self.lblTitle setText:[dict valueForKey:@"title"]];
                    [self.lblTitleCenter setText:[dict valueForKey:@"title"]];
                    [self.lblDate setText:[dict valueForKey:@"msg_date"]];
                    
                    //setup content
                    NSString *strEscapeChar = [[dict valueForKey:@"desc"]stringByDecodingHTMLEntities];
                    NSString *strDescIsoEncode = [NSString stringWithCString:[strEscapeChar cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                    strDescIsoEncode = [[strDescIsoEncode stringByReplacingOccurrencesOfString:@"[CR]" withString:@"<br>"] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
                    strDescIsoEncode = [[strDescIsoEncode stringByReplacingOccurrencesOfString:@"[B]" withString:@"<b>"] stringByReplacingOccurrencesOfString:@"[/B]" withString:@"</b>"];
                    [self setupTextview:strDescIsoEncode];
                    
                    
                    //setup image banner
                    if([[dict valueForKey:@"image"] isEqualToString:@""]){
                        [self.imgBanner setContentMode:UIViewContentModeCenter];
                    }else{
                        [self.imgBanner setContentMode:UIViewContentModeScaleAspectFit];
                    }
                    [self.imgBanner setImageWithURL:[NSURL URLWithString:[dict valueForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"ic_bnload_logo_gscl"]];
                    
                    //handle button
                    flagButton = [dict objectForKey:@"flag_button"];
                    labelButton = [dict objectForKey:@"label_button"];
                    paramAutosave = [dict objectForKey:@"param_autosave"];
                    
                    dataNotif = [dict objectForKey:@"data"];
                    
                    [self setupLayout];
                    [self checkButtonFlag];
                }
            
            }
            
            
            
        }else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject valueForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *acti){
                [self backToR];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    
}


- (void)errorLoadData:(NSError *)error{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *acti){
        [self backToR];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (IBAction)actionNavBack:(id)sender {
    
    UINavigationController *navigationController = self.navigationController;
           [navigationController popViewControllerAnimated:YES];
  
    [userDefault setObject:nil forKey:@"mid"];
    [userDefault setObject:nil forKey:@"nid"];
    [userDefault setObject:nil forKey:@"action"];
    [userDefault synchronize];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat posImgBaner = self.imgBanner.frame.origin.y + self.imgBanner.frame.size.height;
    
    if(scrollView.contentOffset.y > posImgBaner){
        [self.vwTitle setBackgroundColor:UIColorFromRGB(const_color_primary)];
        self.btnNavBack.layer.cornerRadius = 0.0f;
        [self.btnNavBack setBackgroundColor:[UIColor clearColor]];
        [self.lblTitle setTextColor:[UIColor whiteColor]];
    }else{
        [self.vwTitle setBackgroundColor:[UIColor clearColor]];
        self.btnNavBack.layer.cornerRadius = self.btnNavBack.frame.size.height/2;
        [self.btnNavBack setBackgroundColor:UIColorFromRGB(const_color_primary)];
         [self.lblTitle setTextColor:[UIColor clearColor]];
    }
}

-(void) paramAction : (NSString *) action{
    receivNotif = action;
}

- (void)viewDidDisappear:(BOOL)animated{
    [userDefault setObject:nil forKey:@"mid"];
    [userDefault setObject:nil forKey:@"nid"];
    [userDefault setObject:nil forKey:@"action"];
    [userDefault synchronize];
}

- (void) setupTextview:(NSString*)info{
    info = [NSString stringWithFormat:@"<body style='font-family:Lato-Regular;font-size: 15px;'>%@</body>",info];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
              initWithData: [info dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName:[UIFont fontWithName:const_font_name1 size:16] }
        documentAttributes: nil
                     error: nil
    ];

    self.lblDesckripsiTitle.scrollEnabled = NO;
    self.lblDesckripsiTitle.editable = NO;
    self.lblDesckripsiTitle.dataDetectorTypes = UIDataDetectorTypeAll;
    self.lblDesckripsiTitle.attributedText = attributedString;
}
@end
