//
//  SKRLV01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 24/09/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SKRLV01ViewController.h"
#import "Utility.h"
#import "Connection.h"



@interface SKRLV01ViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate,
UIScrollViewDelegate>{
    NSArray *listAction;
    NSArray *listOrigin;
    NSDictionary *dataObject;
    NSDictionary *dataLocal;
    BOOL first;
    BOOL fromHome;
    BOOL spesial;
    BOOL mustAddToManager;
    NSString *key;
    UIToolbar *toolBar;
}

@property (weak, nonatomic) IBOutlet UIImageView *iconHeaderLV01;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightImage; // 15
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSeacrh; // 30
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightLine; // 1
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightView; // 40
@property (weak, nonatomic) IBOutlet UITextField *textSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation SKRLV01ViewController

NSString* requestTypeSKRLV01;
extern NSString *lv_selected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)setFirstLV:(BOOL)aFirstLV{
    first = aFirstLV;
}

- (void)setFromHome:(BOOL)afromHome{
    fromHome = afromHome;
}

- (void)setSpecial:(BOOL)aspecial{
    spesial = aspecial;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [Styles setTopConstant:self.topConstraint];
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.textSearch.hidden = true;
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
    _textSearch.inputAccessoryView = toolBar;
    _textSearch.delegate = self;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *dt = [userDefault objectForKey:@"indexMenu"];
    if (dt) {
        [[DataManager sharedManager]resetObjectData];
        NSArray *temp = [[DataManager sharedManager] getJSONData:[dt intValue]];
        NSDictionary *object = [temp objectAtIndex:1];
        [self setJsonData:object];
        [userDefault removeObjectForKey:@"indexMenu"];
    }
    self.tableView.tableFooterView = [[UIView alloc] init];
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    _iconHeaderLV01.image = [UIImage imageNamed:img];
//    [_iconHeaderLV01 sizeToFit];
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestTypeSKRLV01 = [dataParam valueForKey:@"request_type"];
    if(first){
        listAction = [self.jsonData objectForKey:@"action"];
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        [self.tableView reloadData];
    } else if (fromHome){
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        [dataManager setAction:[self.jsonData valueForKey:@"action"] andMenuId:@"00007"];
        [super openNextTemplate];
    } else if (spesial){
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        int ind = 0;
        NSArray *menu = [self.jsonData valueForKey:@"action"];
        for (int i =0; i < [menu count]; i++) {
            NSArray *selMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:i];
            if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
                ind = i;
                break;
            }
        }
        NSArray *selectedMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:ind];
        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
        [super openNextTemplate];
        //  [dataManager setAction:[[self.jsonData valueForKey:@"listAction"] objectAtIndex:2] andMenuId:@"wq"]
        //        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:3]];
    }
    else {
        dataObject = [dataManager getObjectData];
        [self.labelTitle setText:[dataObject valueForKey:@"title"]];
        if([[dataObject valueForKey:@"url"]boolValue]){
            if([requestTypeSKRLV01 isEqualToString:@"list_merchant"] || [requestTypeSKRLV01 isEqualToString:@"list_bank"] || [requestTypeSKRLV01 isEqualToString:@"list_account"] || [requestTypeSKRLV01 isEqualToString:@"list_denom"] || [requestTypeSKRLV01 isEqualToString:@"list_denom2"] || [requestTypeSKRLV01 isEqualToString:@"list_nazhir"] ||
                [requestTypeSKRLV01 isEqualToString:@"list_sukuk"] || [requestTypeSKRLV01 isEqualToString:@"list_pe"] || [requestTypeSKRLV01 isEqualToString:@"list_currency"] || [requestTypeSKRLV01 isEqualToString:@"list_card"] ){
                
                if([requestTypeSKRLV01 isEqualToString:@"list_merchant"] ){
                    NSString *id_merchant = @"";
                    if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00013"]){// TELEPON
                        id_merchant = @"1";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00025"]) { // VOUCHER HP
                        id_merchant = @"2";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00020"]) { // RETAILER
                        id_merchant = @"3";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00016"]) { // ANGSURAN
                        id_merchant = @"4";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00017"]) { // PEMBAYARAN TIKET
                        id_merchant = @"5";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00015"]) { // TV KABEL
                        id_merchant = @"6";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00014"]) { // ACADEMIC
                        id_merchant = @"7";
                    } else if ([[self.jsonData  valueForKey:@"menu_id"] isEqualToString:@"00018"]) { // ASURANSI
                        id_merchant = @"8";
                    }
                    
                }

                if ([requestTypeSKRLV01 isEqualToString:@"list_denom2"]) {
                    NSString *url =[NSString stringWithFormat:@"%@,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
                    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }
                else {
                    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                    Connection *conn = [[Connection alloc]initWithDelegate:self];
                    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                }
            }
        } else{
            id data =  [dataObject objectForKey:@"content"];
            if([data isKindOfClass:[NSString class]]){
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }else{
                mustAddToManager = true;
                listAction = data;
                [self.tableView reloadData];
            }
        }
    }
}

- (void)doneAction:(id)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAction.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV01Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        [label setText:[data valueForKey:@"title"]];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            [label setText:[data valueForKey:@"title"]];
            if ([data valueForKey:@"card_status"]) {
                NSLog(@"status");
            }
            
        }else if([data objectForKey:@"name"]){
            [label setText:[data valueForKey:@"name"]];
            
        }
    }
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = @"";
    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        text = [data valueForKey:@"title"];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            text = [data valueForKey:@"title"];
            
        }else if([data objectForKey:@"name"]){
            text = [data valueForKey:@"name"];
        }
    }
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+20.0;
}
#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
   
    //Unused karena dipindahkan saat validasi di awal
//    if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00022"]) { // Tabungan Mudharabah
//        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//        NSString *email = [userDefault objectForKey:@"email"];
//        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//        NSString *alertTitle = @"";
//        NSString *alertMessage = @"";
//        if([lang isEqualToString:@"id"]){
//            alertTitle = @"Informasi";
//            alertMessage = @"Silahkan mengisi alamat email terlebih dahulu";
//        } else {
//            alertTitle = @"Information";
//            alertMessage = @"Please fill in the email address first";
//        }
//
//        if ((email == nil) || ([email isEqualToString:@""])) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            alertView.tag = 5823;
//            [alertView show];
//        }
//    }
    
    if(first){
        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
        NSLog(@"%@",[selectedMenu objectAtIndex:0]);
        //Tabungan Mudharabah Dicek saat awal apakah sudah ada email
        if([[selectedMenu objectAtIndex:0]isEqualToString:@"00022"] || [[selectedMenu objectAtIndex:0]isEqualToString:@"00023"]){
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *email = [userDefault objectForKey:@"email"];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            NSString *alertTitle = @"";
            NSString *alertMessage = @"";
            if([lang isEqualToString:@"id"]){
                alertTitle = @"Informasi";
                alertMessage = @"Silahkan mengisi alamat email terlebih dahulu";
            } else {
                alertTitle = @"Information";
                alertMessage = @"Please fill in the email address first";
            }
            
            if ((email == nil) || ([email isEqualToString:@""])) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                alertView.tag = 5823;
                [alertView show];
            }
        }
    }
    if(mustAddToManager){
        //        NSString *keyTemp = @"";
        NSString *mnId = [self.jsonData valueForKey:@"menu_id"];
        NSString *value = @"";
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        BOOL haveAction = false;
        for(NSString *a in data.allKeys){
            
            if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00028"] && [a isEqualToString:@"type_transfer"]) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setObject:[data valueForKey:a] forKey:@"typeTransfer"];
            }
            
            if([a isEqualToString:@"action"]){
                haveAction = true;
            }else if(![a isEqualToString:@"title"] && ![a isEqualToString:@"name"]){
                key = a;
                value = [data valueForKey:key];
                break;
            }
            
        }
        
        lv_selected = value;
        [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
        if(haveAction){
            [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
            dataManager.currentPosition=-1;
        }
       
//              NSString *status = [dataManager.dataExtra valueForKey:@"status"];
//              NSString *code = [dataManager.dataExtra valueForKey:@"code"];
                
//              if([status isEqualToString:@"ACTIVE"] && [code isEqualToString:@"00077"] ){
//                  [super openTemplate:@"SKRCF02" withData:temp];
//                   dataManager.currentPosition=5;
//                  [super openNextTemplate];
//                  UIStoryboard *mStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                  TemplateViewController *tableViewVc = [mStoryboard instantiateViewControllerWithIdentifier:@"SKRCF02"];
//                  [self presentViewController:tableViewVc animated:true completion:nil];
//                  [self.navigationController pushViewController:tableViewVc animated:YES];
//              }
           
        
        
        //        if([key isEqualToString:@"id_account"]){
        //            [dataManager setIdAccount:[[listAction objectAtIndex:indexPath.row]valueForKey:key]];
        //        }else if([key isEqualToString:@"code"]){
        //            [dataManager setCode:[[listAction objectAtIndex:indexPath.row]valueForKey:@"code"]];
        //        }else if([key isEqualToString:@"id_denom"]){
        //            [dataManager setIdDenom:[[listAction objectAtIndex:indexPath.row]valueForKey:@"id_denom"]];
        //        }else if([key isEqualToString:@"id_merchant"]){
        //            [dataManager setIdMerchant:[[listAction objectAtIndex:indexPath.row]valueForKey:@"code"]];
        //        }else if([key isEqualToString:@"type_bank"]){
        //            [dataManager setTypeBank:[[listAction objectAtIndex:indexPath.row]valueForKey:@"code"]];
        //        }
        
        if([[data valueForKey:@"code"]isEqualToString:@"00051"] || [[data valueForKey:@"code"]isEqualToString:@"00052"] ||
            [[data valueForKey:@"code"]isEqualToString:@"00053"] || [[data valueForKey:@"code"]isEqualToString:@"00054"]){
            [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"menu_name"] forKey:@"nama_menu"];
        }
        
        if([mnId isEqualToString:@"00087"]){
            if([data valueForKey:@"card_name"]){
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_no"] forKey:@"card_name"];
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_name"] forKey:@"card_name"];
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_type"] forKey:@"card_type"];
                NSLog(@"Card Status : %@",[data valueForKey:@"card_status"]);
                [[[DataManager sharedManager]dataExtra]setObject:[data valueForKey:@"card_status"] forKey:@"card_status"];
            }
            
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *last = [userDefault objectForKey:@"last"];
    if (![last isEqualToString:@"-"]) {
        [userDefault setValue:@"-" forKey:@"last"];
    }
    
    @try {

        [super openNextTemplate];
    } @catch (NSException *exception) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertView.tag = 999; //tag no have menu
        [alertView show];
    }
    
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
                if ([requestType isEqualToString:@"list_denom2"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"response"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listData){
                        [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                    }
                    key = @"id_denom";
                    mustAddToManager = true;
                    listAction = newData;
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
                    [self.tableView reloadData];
                } else if ([requestType isEqualToString:@"list_denom"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"response"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    for(NSDictionary *temp in listData){
                        [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                    }
                    key = @"id_denom";
                    mustAddToManager = true;
                    listAction = newData;
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
                    [self.tableView reloadData];
                }
                //            else {
                //                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //                [alert show];
                //            }
            }
            else {
                /*NSString *msg = @"";
                 NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                 NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                 
                 if([lang isEqualToString:@"id"]) {
                 msg = @"Permintaan tidak dapat diproses";
                 } else {
                 msg = @"Request time out";
                 }
                 
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];*/
                
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }else{
            if([requestType isEqualToString:@"list_account"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_account":[temp valueForKey:@"id_account"]}];
                    //[newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_account":[temp valueForKey:@"id_account"],@"code":[temp valueForKey:@"code"]}];
                }
                
                if ([newData count] == 1) {
                    NSDictionary *idAccount = [newData objectAtIndex:0];
                    lv_selected = [idAccount valueForKey:@"id_account"];
                    [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":[idAccount valueForKey:@"id_account"]}];
                    [super openNextTemplate];
                }else{
                    mustAddToManager = true;
                    key = @"id_account";
                    listAction = newData;
                    [self.tableView reloadData];
                }
                
                
            } else if([requestType isEqualToString:@"list_merchant"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
                }
                if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00028"]){
                    key = @"id_merchant";
                }else if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00025"]){
                    key = @"id_denom";
                }else{
                    key = @"code";
                }
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_bank"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                self.textSearch.hidden = false;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
                }
                key = @"code";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_nazhir"]){
                self.constraintHeightLine.constant = 1;
                self.constraintHeightView.constant = 40;
                self.constraintHeightImage.constant = 15;
                self.constraintHeightSeacrh.constant = 30;
                self.textSearch.hidden = false;
                [self.view layoutIfNeeded];
                
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"name":[temp valueForKey:@"name"],@"nazhir_id":[temp valueForKey:@"nazhir_id"]}];
                }
                key = @"nazhir_id";
                mustAddToManager = true;
                listOrigin = newData;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_denom"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                }
                key = @"id_denom";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            } else if([requestType isEqualToString:@"list_denom2"]){
                NSArray *listData = (NSArray *)jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc]init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                }
                key = @"id_denom";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if ([requestType isEqualToString:@"list_sukuk"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [temp valueForKey:@"name_series"],
                                         @"id_series" : [temp valueForKey:@"id_series"]
                                         }];
                }
                key = @"id_series";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_pe"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [temp valueForKey:@"description"],
                                         @"pe_code" : [temp valueForKey:@"pe_code"]
                                         }];
                }
                key = @"pe_code";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_currency"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"currency_code"],[temp valueForKey:@"currency_name"]],
                                         @"currency_code" : [temp valueForKey:@"currency_code"]
                                         }];
                }
                key = @"currency_code";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }else if([requestType isEqualToString:@"list_card"]){
                NSArray *listData = (NSArray *) jsonObject;
                NSMutableArray *newData = [[NSMutableArray alloc] init];
                for(NSDictionary *temp in listData){
                    [newData addObject:@{@"title" : [NSString stringWithFormat:@"%@\n%@\n%@",[temp valueForKey:@"card_no"],[temp valueForKey:@"card_name"],[temp valueForKey:@"card_type"]],
                                         @"card_no" : [temp valueForKey:@"card_no"],
                                         @"card_name" :[temp valueForKey:@"card_name"],
                                         @"card_type" : [temp valueForKey:@"card_type"],
                                         @"card_status" : [temp valueForKey:@"card_status"]
                                         }];
                }
                key = @"card_no";
                mustAddToManager = true;
                listAction = newData;
                [self.tableView reloadData];
            }
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 657) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == 657) {
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        
        if ([substring length] > 0) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for (NSDictionary *temp in listOrigin){
                NSRange substringRange = [[[temp objectForKey:@"title"] lowercaseString] rangeOfString:[substring lowercaseString]];
                if (substringRange.length != 0) {
                    [newData addObject:temp];
                }
            }
            
            listAction = (NSArray *)newData;
            [self.tableView reloadData];
        } else {
            listAction = listOrigin;
            [self.tableView reloadData];
        }
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        self.constraintBottomTable.constant = 216;
        [self.view layoutIfNeeded];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        self.constraintBottomTable.constant = 0;
        [self.view layoutIfNeeded];
    }
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else if (alertView.tag == 5823) {
        NSDictionary* userInfo = @{@"position": @(713)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    } else {
        [self backToRoot];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    NSLog(@"Did Scroll");
//}

//-(void)sendEvent:(UIEvent *)event
//{
//    NSSet *allTouches = [event allTouches];
//    if ([allTouches count] > 0)
//    {
//        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
//        if (phase == UITouchPhaseBegan)
//        {
//            NSLog(@"Did Scroll");
//        }
//
//    }
//}

//- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    if(touches.count > 1){
//         NSLog(@"Did Scroll");
//    }
//}


@end
