//
//  CR01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CR01ViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "CobTimesViewController.h"

@interface CR01ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>{
    NSArray *listPeriod;
    NSUserDefaults *userDefaults;
    NSString *language;
    NSString *maxFinancing, *minFinancing;
    int maxAge, minAge;
    NSNumberFormatter *currencyFormatter;
    BOOL stateSimulate;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFinancing;
@property (weak, nonatomic) IBOutlet UITextField *tfFinancing;
@property (weak, nonatomic) IBOutlet UILabel *subTitleFinancing;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePeriod;
@property (weak, nonatomic) IBOutlet UITextField *tfPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblTItleAge;
@property (weak, nonatomic) IBOutlet UITextField *tfAge;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMargin;
@property (weak, nonatomic) IBOutlet UITextField *tfMargin;
@property (weak, nonatomic) IBOutlet UIButton *btnCalculate;
@property (weak, nonatomic) IBOutlet UILabel *lblTItleEstimation;
@property (weak, nonatomic) IBOutlet UILabel *lblEstimation;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitleEstimation;

@end

@implementation CR01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    stateSimulate = false;
    
    if(self.jsonData){
        self.lblTitle.text = [self.jsonData objectForKey:@"title"];
    }
    [self.imgIcon setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    
    if([language isEqualToString:@"id"]){
        self.lblTItleAge.text = @"Usia";
        self.tfAge.placeholder = @"Masukkan Usia";
        
        self.lblTitleMargin.text = @"Margin";
        self.tfMargin.placeholder = @"Masukkan Nilai Margin";
        
        self.lblTitleFinancing.text = @"Pembiayaan Bank";
        self.tfFinancing.placeholder = @"Masukkan Nominal Pengajuan";
        
        self.lblTitlePeriod.text = @"Jangka Waktu Pembiayaan";
        self.tfPeriod.placeholder = @"Pilih Jangka Waktu Pembiayaan";
        
        self.lblTItleEstimation.text = @"Estimasi Kewajiban Ujrah Perbulan";
        self.lblSubTitleEstimation.text = @"Kewajiban ujrah diatas hanya estimasi. Anda bisa melihat kewajiban ujrah sebenarnya di laman konfirmasi";

        [self.btnCalculate setTitle:@"Hitung" forState:UIControlStateNormal];
        [self.btnNext setTitle:@"Minat" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Belum Minat" forState:UIControlStateNormal];
    }else{
        self.lblTItleAge.text = @"Age";
        self.tfAge.placeholder = @"Enter Age";
        
        self.lblTitleMargin.text = @"Margin";
        self.tfMargin.placeholder = @"Enter margin value";
        
        self.lblTitleFinancing.text = @"Bank Financing";
        self.tfFinancing.placeholder = @"Enter the amount you need";
        
        self.lblTitlePeriod.text = @"Tenor (Years)";
        self.tfPeriod.placeholder = @"Maximum 3 year";
        
        self.lblTItleEstimation.text = @"Estimated Monthly Installment";
        self.lblSubTitleEstimation.text = @"The above installment are only estimate. You can see the actual installment in the confirmation page.";

        [self.btnCalculate setTitle:@"Calculate" forState:UIControlStateNormal];
        [self.btnNext setTitle:@"Interested" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"Not Interested" forState:UIControlStateNormal];
    }
    self.subTitleFinancing.text = @"";
    self.lblEstimation.text = @"";
    
    self.lblTitle.text = [self.jsonData objectForKey:@"title"];
    self.imgIcon.image = [UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]];
    
    [Styles setTopConstant:self.topConstant];
    [Styles setStyleButton:self.btnCalculate];
    [Styles setStyleButton:self.btnNext];
    [Styles setStyleButton:self.btnCancel];
    
    UIPickerView *picker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    picker.showsSelectionIndicator = YES;
    picker.delegate = self;
    picker.dataSource = self;
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.tfPeriod.inputView = picker;
    self.tfPeriod.inputAccessoryView = toolbar;
    
    self.tfAge.inputAccessoryView = toolbar;
    self.tfAge.keyboardType = UIKeyboardTypeNumberPad;
    self.tfAge.delegate = self;
    
    self.tfFinancing.inputAccessoryView = toolbar;
    self.tfFinancing.keyboardType = UIKeyboardTypeNumberPad;
    self.tfFinancing.delegate = self;
    
    self.tfMargin.inputAccessoryView = toolbar;
    self.tfMargin.keyboardType = UIKeyboardTypeNumberPad;
    self.tfMargin.delegate = self;
    
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];

    [self.btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [self.btnCalculate addTarget:self action:@selector(actionCalculate) forControlEvents:UIControlEventTouchUpInside];
    
    //[self doRequest];
    
    //hide margin
    [self.lblTitleMargin setHidden:YES];
    [self.tfMargin setHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
    if([email isEqualToString:@""] || email == nil){
        NSString *message = @"";
        if([Utility isLanguageID]){
            message = @"Harap lengkapi email Anda terlebih dahulu";
        }else{
            message = @"Please provide your email first";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self openStatic:@"ChangeEmailVC"];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        [self doRequest];
    }
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void) actionCancel{
    [self backToR];
}

- (BOOL) validate{
    int fillages = 0;
    if([language isEqualToString:@"id"]){
        fillages = [[self.tfAge.text stringByReplacingOccurrencesOfString:@" Tahun" withString:@""]intValue];
    }else{
        fillages = [[self.tfAge.text stringByReplacingOccurrencesOfString:@" Years" withString:@""]intValue];    }
    
    if([self.tfFinancing.text isEqualToString:@""]){
        
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Masukkan Nominal yang diinginkan";
        }else{
            msg = @"Enter the amount you need";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self.tfFinancing becomeFirstResponder];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        return FALSE;

    }else if([[self.tfFinancing.text stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue] > [maxFinancing doubleValue]){
        
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Nominal pengajuan melebihi batas yang di tawarkan";
        }else{
            msg = @"Financing amount exceeding the limit offered";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            self.tfFinancing.text = [self->currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[self->maxFinancing doubleValue]]];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        return FALSE;

    }else if([[self.tfFinancing.text stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue] < [minFinancing doubleValue]){
        
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Nominal pengajuan kurang dari batas yang di tawarkan";
        }else{
            msg = @"Financing amount less than the limit offered";
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            self.tfFinancing.text = [self->currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[self->minFinancing doubleValue]]];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        return FALSE;

    }else if([self.tfPeriod.text isEqualToString:@""]){
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Pilih jangka waktu pembiayaan";
        }else{
            msg = @"Select time of financing period";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        return FALSE;

    }else if([self.tfAge.text isEqualToString:@""]){
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Masukkan Usia";
        }else{
            msg = @"Enter Age";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        return FALSE;

    }else if(fillages < minAge){
        
//        self.tfAge.text = [NSString stringWithFormat:@"%d Tahun", minAge];
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            self.tfAge.text = [NSString stringWithFormat:@"%d Tahun", minAge];
            msg = [NSString stringWithFormat:@"Minimum Usia %d Tahun",minAge];
        }else{
            self.tfAge.text = [NSString stringWithFormat:@"%d Years", minAge];
            msg = [NSString stringWithFormat:@"Minimum Age %d Years",minAge];
        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
        return FALSE;
    }
    
    
//    else if([self.tfMargin.text isEqualToString:@""]){
//        NSString *msg = @"";
//        if([language isEqualToString:@"id"]){
//            msg = @"Masukkan Margin";
//        }else{
//            msg = @"Enter Margin Value";
//        }
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//        [self presentViewController:alert animated:YES completion:nil];
//        return FALSE;
//
//    }
    else{
        return TRUE;
    }
}

- (void) actionNext{
    if([self validate]){
        [dataManager.dataExtra setValue:[self.tfFinancing.text stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"limit"];
        if([language isEqualToString:@"id"]){
            [dataManager.dataExtra setValue:[self.tfAge.text stringByReplacingOccurrencesOfString:@" Tahun" withString:@""] forKey:@"age"];
        }else{
            [dataManager.dataExtra setValue:[self.tfAge.text stringByReplacingOccurrencesOfString:@" Years" withString:@""] forKey:@"age"];
        }
        [dataManager.dataExtra setValue:self.tfMargin.text forKey:@"margin"];

        [self openNextTemplate];
    }
}

- (void)actionCalculate{
    [dataManager.dataExtra setValue:[self.tfFinancing.text stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"limit"];
    if([language isEqualToString:@"id"]){
        [dataManager.dataExtra setValue:[self.tfAge.text stringByReplacingOccurrencesOfString:@" Tahun" withString:@""] forKey:@"age"];
    }else{
        [dataManager.dataExtra setValue:[self.tfAge.text stringByReplacingOccurrencesOfString:@" Years" withString:@""] forKey:@"age"];
    }
    [dataManager.dataExtra setValue:self.tfMargin.text forKey:@"margin"];
    
    if([self validate]){
        stateSimulate = true;
        NSString *urlData = @"request_type=cashfin_requested,step=simulate,limit,loan_period,margin";
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
//        conn finsih
    }

}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listPeriod.count + 1;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(row == 0){
        if([language isEqualToString:@"id"]){
            return @"Pilih Jangka Waktu Pembiayaan";
        }else{
            return @"Select Period of Financing";
        }
    }else{
        if([language isEqualToString:@"id"]){
            return [NSString stringWithFormat:@"%@ bulan", [listPeriod[row-1] objectForKey:@"name"]];
        }else{
            return [NSString stringWithFormat:@"%@ months", [listPeriod[row-1] objectForKey:@"name"]];
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(row == 0){
        self.tfPeriod.text = @"";
    }else{
        [dataManager.dataExtra setValue:[listPeriod[row-1] objectForKey:@"code"] forKey:@"loan_period"];
        if([language isEqualToString:@"id"]){
            self.tfPeriod.text = [NSString stringWithFormat:@"%@ bulan", [listPeriod[row-1] objectForKey:@"name"]];
        }else{
            self.tfPeriod.text = [NSString stringWithFormat:@"%@ months", [listPeriod[row-1] objectForKey:@"name"]];
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField == self.tfAge){
        NSString* newText = @"";
        NSString* yearsTitle = @"";
        if([language isEqualToString:@"id"]){
            yearsTitle = @" Tahun";
        }else{
            yearsTitle = @" Years";
        }
        if([textField.text containsString:yearsTitle]){
            textField.text = [textField.text stringByReplacingOccurrencesOfString:yearsTitle withString:@""];
            NSRange newRange = NSMakeRange(range.location - (yearsTitle.length) , range.length);
            if(yearsTitle.length > range.location){
                newText = @"";
            }else{
                newText = [textField.text stringByReplacingCharactersInRange:newRange withString:string];
                
//                if([newText intValue] < minAge){
//                    newText = [NSString stringWithFormat:@"%d", minAge];
//                }
                if([newText intValue] > maxAge){
                    newText = [NSString stringWithFormat:@"%d", maxAge];
                    NSString *msg = @"";
                    if([language isEqualToString:@"id"]){
                        msg = [NSString stringWithFormat:@"Maksimum Usia %d Tahun",maxAge];
                    }else{
                        msg = [NSString stringWithFormat:@"Maximum Age %d Years",maxAge];
                    }
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
        }else{
            newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        }
        
        if([language isEqualToString:@"id"]){
            textField.text = [NSString stringWithFormat:@"%@ Tahun",newText];
        }else{
            textField.text = [NSString stringWithFormat:@"%@ Years",newText];
        }
        
        if([newText isEqualToString:@""]){
            textField.text = newText;
        }
    }
    
    if(textField == self.tfFinancing){
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        double newValue = [newText doubleValue];
        
        if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
        }
        else{
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
        }
    }
    
    if(textField == self.tfMargin){
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
        double newValue = [newText doubleValue];
        
        if ([newText length] == 0 || newValue == 0){
            textField.text = nil;
        }else if(newValue > 100){
            textField.text = @"100";
        }else{
            textField.text = newText;
        }
    }
    
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.tfFinancing){

        if([[textField.text stringByReplacingOccurrencesOfString:@"," withString:@""]doubleValue] > [maxFinancing doubleValue]){
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[maxFinancing doubleValue]]];
            NSString *msg = @"";
            if([language isEqualToString:@"id"]){
                msg = @"Nominal pengajuan melebihi batas yang di tawarkan";
            }else{
                msg = @"Financing amount exceeding the limit offered";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@ IDR. %@", msg, [Utility formatCurrencyValue:[maxFinancing doubleValue]]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                self.tfFinancing.text = [self->currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[self->maxFinancing doubleValue]]];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
        if([[textField.text stringByReplacingOccurrencesOfString:@"," withString:@""]doubleValue] < [minFinancing doubleValue]){
            textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[minFinancing doubleValue]]];
            NSString *msg = @"";
            if([language isEqualToString:@"id"]){
                msg = @"Nominal pengajuan kurang batas yang di tawarkan";
            }else{
                msg = @"Financing amount less the limit offered";
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@ IDR. %@", msg, [Utility formatCurrencyValue:[minFinancing doubleValue]]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                self.tfFinancing.text = [self->currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[self->minFinancing doubleValue]]];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    /**
     {"response":"{\"max_age\":\"\",\"list_loan_period\":[{\"name\":\"12\",\"code\":\"12\"},{\"name\":\"24\",\"code\":\"24\"},{\"name\":\"36\",\"code\":\"36\"}],\"max_financing\":\"30000000\",\"min_age\":\"\"}","transaction_id":"","response_code":"00"}
     */
    if(stateSimulate){
        if([requestType isEqualToString:@"cashfin_requested"]){
            if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
                stateSimulate = false;
                
                NSString *mResponse = [jsonObject objectForKey:@"response"];
                NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
                NSString *doubleString = [NSString stringWithFormat:@"%.2f", [[object valueForKey:@"installment_amount"]doubleValue]];
                self.lblEstimation.text = [NSString stringWithFormat:@"IDR. %@", [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[doubleString doubleValue]]]];
                
                [self.view endEditing:YES];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }else{
        if([requestType isEqualToString:@"cashfin_requested"]){
            if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
                NSString *mResponse = [jsonObject objectForKey:@"response"];
                NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
                listPeriod = [object objectForKey:@"list_loan_period"];
                maxFinancing = [object valueForKey:@"max_financing"];
                minFinancing = [object valueForKey:@"min_financing"];
                maxAge = [[object valueForKey:@"max_age"]intValue];
                minAge = [[object valueForKey:@"min_age"]intValue];
                
                [self.subTitleFinancing setHidden:YES];
                
//                if([language isEqualToString:@"id"]){
//                    self.subTitleFinancing.text = [NSString stringWithFormat:@"Maksimal pembiayaan yang diberikan adalah Rp. %@",[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[maxFinancing doubleValue]]]];
//                }else{
//                    self.subTitleFinancing.text = [NSString stringWithFormat:@"Maximum Financing Provided Rp. %@",[currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[maxFinancing doubleValue]]]];
//                }
                
            }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"01"]){
                [self actionOpenCOB: [jsonObject objectForKey:@"response"]];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
}

- (void) actionOpenCOB : (NSString*)string{
    
    CobTimesViewController *cob = [self.storyboard instantiateViewControllerWithIdentifier:@"COBTIMESVC"];
    [cob setMessage:string];
    
    UINavigationController *currentVC = (UINavigationController *) self.tabBarController.selectedViewController;
    [currentVC pushViewController:cob animated:YES];
    
//    [self presentViewController:cob animated:YES completion:nil];
}

- (void)reloadApp{
    [self backToRoot];
}

@end
