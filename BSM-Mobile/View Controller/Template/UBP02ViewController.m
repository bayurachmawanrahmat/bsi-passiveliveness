//
//  UBP02ViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 25/09/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "UBP02ViewController.h"
#import "Utility.h"
#import "CellUbp.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "UIViewController+ECSlidingViewController.h"

@interface UBP02ViewController ()<ConnectionDelegate,UITableViewDataSource, UITableViewDelegate>{
    NSArray *listData;
    NSString *paymenttype;
    NSMutableArray *dtSelect;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *lblTittle;
@property (weak, nonatomic) IBOutlet UITableView *tabelRecipt;
@property (weak, nonatomic) IBOutlet CustomBtn *btnOk;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwButton;

@end

@implementation UBP02ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    dtSelect = [[NSMutableArray alloc] init];
    
    [_btnOk setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [_btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    [self.btnOk setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
//    CGRect screenBound = [[UIScreen mainScreen] bounds];
//    CGSize screenSize = screenBound.size;
//    CGFloat screenWidth = screenSize.width;
//    CGFloat screenHeight = screenSize.height;
//
//    CGRect frmTitle = self.vwTitle.frame;
//    CGRect frmLblTitle = self.lblTittle.frame;
//    CGRect frmButton = self.vwButton.frame;
//    CGRect frmTable = self.tabelRecipt.frame;
//    CGRect frmBtnOk = self.btnOk.frame;
//    CGRect frmBtnCancel = self.btnCancel.frame;
//
//    frmTitle.size.width = screenWidth;
//    frmTitle.size.height = 50;
//    frmTitle.origin.x = 0;
//    frmTitle.origin.y = TOP_NAV;
//    frmLblTitle.origin.x = 8;
//    frmLblTitle.origin.y = 5;
//    frmLblTitle.size.width = frmTitle.size.width - 16;
//    frmLblTitle.size.height = 40;
//
//    frmBtnCancel.origin.x = screenWidth - (frmBtnCancel.size.width) - 25;
//    frmBtnOk.origin.x = 25;
//    frmTable.size.width = screenWidth;
//
//    if (screenHeight >= 812.0f) {
//        frmButton.origin.y = screenHeight - frmButton.size.height - 112;
//    } else {
//        frmButton.origin.y = screenHeight - frmButton.size.height - 62;
//    }
//    //frmTable.size.height = screenHeight - frmTitle.size.height - frmTitle.origin.y - frmButton.size.height;
//    if (screenWidth <= 350) {
//        //frmTable.size.height = frmTable.size.height - 125;
//        frmTable.size.height = frmButton.origin.y - frmTable.origin.y;
//        frmBtnOk.origin.x = 8;
//        frmBtnOk.size.width = (screenWidth/2) - 12;
//        frmBtnCancel.origin.x = (screenWidth/2) + 4;
//        frmBtnCancel.size.width = (screenWidth/2) - 12;
//    } else {
//        frmTable.size.height = frmButton.origin.y - frmTable.origin.y;
//        frmBtnOk.origin.x = 8;
//        frmBtnOk.size.width = (screenWidth/2) - 12;
//        frmBtnCancel.origin.x = (screenWidth/2) + 4;
//        frmBtnCancel.size.width = (screenWidth/2) - 12;
//    }
//    frmButton.size.width = screenWidth;
//    frmButton.size.height = 40;
//    frmBtnOk.size.height = 40;
//    frmBtnCancel.size.height = 40;
//
//
//    self.btnOk.frame = frmBtnOk;
//    self.btnCancel.frame = frmBtnCancel;
//    self.vwTitle.frame = frmTitle;
//    self.lblTittle.frame = frmLblTitle;
//    self.vwButton.frame = frmButton;
//    self.tabelRecipt.frame = frmTable;

    [Styles setTopConstant:_topConstant];
    [self.lblTittle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTittle.textColor = UIColorFromRGB(const_color_title);
    self.lblTittle.textAlignment = const_textalignment_title;
    self.lblTittle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTittle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.tabelRecipt.dataSource = self;
    self.tabelRecipt.delegate = self;
   
    [self doRequest];
}

- (void) doRequest{
    if([[self.jsonData valueForKey:@"url"]boolValue]){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *aditionalInfo = [userDefault objectForKey:@"aditionalInfo"];
        NSString *urlData = @"";
        if (aditionalInfo && ![aditionalInfo  isEqual: @""]) {
            urlData = [NSString stringWithFormat:@"%@,%@", [self.jsonData valueForKey:@"url_parm"],aditionalInfo];
        } else {
            urlData =[NSString stringWithFormat:@"%@,billerid,billertype,billername,billkey1,billkey1name",[self.jsonData valueForKey:@"url_parm"]];
        }
        
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"] boolValue]];
    }
    [self.tabelRecipt reloadData];
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"cellUbp";
    CellUbp *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CellUbp alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    cell.lbl.text = [NSString stringWithFormat:@"%@ %@", [data valueForKey:@"billname"], [data valueForKey:@"billamount"]];
//    cell.lbl.text = @"Pengujian Kalibrasi Alat Ukur (63/SP.2KAL/BBPPT/VII/2021) - IDR 1.000.0000.0000";
//    cell.lbl.font = [UIFont fontWithName:const_font_name1 size:14];
    if([paymenttype isEqualToString:@"0"]){
        if ([dtSelect containsObject:[data valueForKey:@"billcode"]]) {
            cell.icon.image = [UIImage imageNamed:@"checked.png"];
        } else {
            cell.icon.image = [UIImage imageNamed:@"blank_check.png"];
        }
    } else if([paymenttype isEqualToString:@"1"]){
        if ([dtSelect containsObject:[data valueForKey:@"billcode"]]) {
            cell.icon.image = [UIImage imageNamed:@"radio_select.png"];
        } else {
            cell.icon.image = [UIImage imageNamed:@"radio_unselect.png"];
        }
    } else {
        cell.lbl.textColor = [UIColor grayColor];
        cell.icon.image = [UIImage imageNamed:@"check_disable.png"];
        if (![dtSelect containsObject:[data valueForKey:@"billcode"]]) {
            [dtSelect addObject: [data valueForKey:@"billcode"]];
        }
    }
    
//    CGRect screenBound = [[UIScreen mainScreen] bounds];
//    CGSize screenSize = screenBound.size;
//    CGFloat screenWidth = screenSize.width;
//
//    UIView *svLine = (UIView *) [cell viewWithTag:11];
//    CGRect frmSVLine = svLine.frame;
//    frmSVLine.size.width = screenWidth;
//    svLine.frame = frmSVLine;
        
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewAutomaticDimension;
//}
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return 30.0;
//}


#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    if([paymenttype isEqualToString:@"0"]){
        if (![dtSelect containsObject:[data valueForKey:@"billcode"]]) {
            [dtSelect addObject: [data valueForKey:@"billcode"]];
            [self.tabelRecipt reloadData];
        } else {
            [dtSelect removeObject:[data valueForKey:@"billcode"]];
            [self.tabelRecipt reloadData];
        }
    } else if([paymenttype isEqualToString:@"1"]){
        if (![dtSelect containsObject:[data valueForKey:@"billcode"]]) {
            [dtSelect removeAllObjects];
            [dtSelect addObject: [data valueForKey:@"billcode"]];
            [self.tabelRecipt reloadData];
        }
    }
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject valueForKey:@"response"];
                NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                // paymenttype = @"0";
                paymenttype = [dict valueForKey:@"paymenttype"];
                listData = [dict valueForKey:@"billitemlist"];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id": [jsonObject objectForKey:@"transaction_id"]}];
                [self.tabelRecipt reloadData];
                
            }
        } else {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToRoot];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } else {
        
    }
    
}

- (void)errorLoadData:(NSError *)error{
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToRoot];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
//#pragma mark - UIAlertViewDelegate
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(alertView.tag == 331) {
//
//    } else {
//        [self backToRoot];
//    }
//}

- (IBAction)actionOk:(id)sender {
    [self goToNext];
}

-(void) goToNext{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if([dtSelect count] == 0){
//        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
//        if([lang isEqualToString:@"id"]){
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Silakan pilih produk!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            alert.delegate = self;
//            alert.tag = 331;
//            [alert show];
//        } else {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"Please choose a product!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            alert.delegate = self;
//            alert.tag = 331;
//            [alert show];
//        }
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"PLEASE_SELECT_PRODUCT") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        NSString *aditionalInfo = [userDefault objectForKey:@"aditionalInfo"];
        NSString *urlData = @"";
        if (aditionalInfo && ![aditionalInfo  isEqual: @""]) {
            urlData = [NSString stringWithFormat:@"%@,bclist,transaction_id",aditionalInfo];
        } else {
            urlData = @"billerid,billertype,billername,billkey1,billkey1name,bclist,transaction_id";
        }
        NSString *bclist= @"";
        for (int i = 0; i < [dtSelect count]; i++) {
            bclist = [NSString stringWithFormat:@"%@%@,",bclist,[dtSelect objectAtIndex:i]];
        }
//        bclist = [bclist substringToIndex:[bclist length] - 1];
        [userDefault setObject:urlData forKey:@"aditionalInfo"];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"bclist": bclist}];
        [super openNextTemplate];
    }
}

- (IBAction)actionCancel:(id)sender {
    [self backToRoot];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
