//
//  CRAKADViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"
#import "Utility.h"

NS_ASSUME_NONNULL_BEGIN

@interface CRAKADViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
