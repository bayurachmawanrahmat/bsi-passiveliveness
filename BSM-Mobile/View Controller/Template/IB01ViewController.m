//
//  IB01ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/18/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "IB01ViewController.h"
#import "CellRecentlyUse.h"
#import "Utility.h"
#import "RecentlyHelper.h"
#import "Styles.h"
#import "CustomBtn.h"

@interface IB01ViewController () <UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource>{
    NSArray *arDataRecently;
    RecentlyHelper *mHelper;
}
@property (weak, nonatomic) IBOutlet UITextField *textFieldInput;
- (IBAction)next:(id)sender;
- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet CustomBtn *btnN;
@property (weak, nonatomic) IBOutlet CustomBtn *btnB;

@property (weak, nonatomic) IBOutlet UIView *vwLine;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;

@property (weak, nonatomic) IBOutlet UITableView *tblRecentlyUse;
@property (weak, nonatomic) IBOutlet UIView *vwRecentlyUse;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleRecently;


@end

@implementation IB01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    arDataRecently = [[NSArray alloc]init];
    mHelper = [[RecentlyHelper alloc]init];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [self setupLayout];
    
#pragma mark change batal to selanjutnya and other else
    
    if([lang isEqualToString:@"id"]){
//        [_btnB setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnB setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnN setTitle:@"Batal" forState:UIControlStateNormal];
        [_lblTitleRecently setText:@"Transaksi Terakhir"];
    } else {
//        [_btnB setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnN setTitle:@"Next" forState:UIControlStateNormal];
        [_btnB setTitle:@"Next" forState:UIControlStateNormal];
        [_btnN setTitle:@"Cancel" forState:UIControlStateNormal];
        [_lblTitleRecently setText:@"Last Transaction"];
    }
    //amanah Styles
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.btnB setColorSet:PRIMARYCOLORSET];
    [self.btnN setColorSet:SECONDARYCOLORSET];
    
    CGRect framePadding = CGRectMake(0.0, 0.0, 5.0, self.textFieldInput.frame.size.height);
    
    UIView *paddingPhone = [[UIView alloc] initWithFrame:framePadding];
    self.textFieldInput.leftView = paddingPhone;
    self.textFieldInput.leftViewMode = UITextFieldViewModeAlways;
    
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    [self.textFieldInput setPlaceholder:[self.jsonData valueForKey:@"content"]];
//    if([[[self.jsonData valueForKey:@"content"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]isEqualToString:@""]){
//        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Opps" message:@"No title found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
    // Do any additional setup after loading the view.
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.textFieldInput.inputAccessoryView = keyboardDoneButtonView;
    self.textFieldInput.delegate = self;
    [self showingRecentlyUses : [self.jsonData valueForKey:@"menu_id"] mFieldName:[self.jsonData valueForKey:@"field_name"]];
}

-(void)setupLayout{
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.labelTitle.frame;
    CGRect frmTextField = self.textFieldInput.frame;
    CGRect frmVwLine = self.vwLine.frame;
    CGRect frmVwBtn = self.vwButton.frame;
    CGRect frmVwRecently = self.vwRecentlyUse.frame;
    CGRect frmLblTitleRecently = self.lblTitleRecently.frame;
    CGRect frmTblRecently = self.tblRecentlyUse.frame;
    
    CGRect frmBtnN = self.btnN.frame;
    CGRect frmBtnB = self.btnB.frame;
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = TOP_BAR_HEIGHT;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.size.height = 40;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    frmLblTitle.origin.y = 5;
    
    frmTextField.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 30;
    frmTextField.size.width = SCREEN_WIDTH - (frmTextField.origin.x *2);
    
    frmVwLine.origin.y = frmTextField.origin.y + frmTextField.size.height;
    frmVwLine.size.width = frmTextField.size.width;
    
    
    frmVwBtn.origin.y = frmVwLine.origin.y + frmVwLine.size.height + 54;
    frmVwBtn.origin.x = 0;
    frmVwBtn.size.width = SCREEN_WIDTH;
    frmVwBtn.size.height = 42;
    
    frmBtnN.origin.x = 16;
    frmBtnN.origin.y = 0;
    frmBtnN.size.width = frmVwBtn.size.width/2 - (frmBtnN.origin.x * 2);
    frmBtnN.size.height = frmVwBtn.size.height;
    
    frmBtnB.origin.x = frmBtnN.origin.x + frmBtnN.size.width + 24;
    frmBtnB.origin.y = 0;
    frmBtnB.size.width = SCREEN_WIDTH - frmBtnB.origin.x - 16;
    frmBtnB.size.height = frmBtnN.size.height;
    
    frmVwRecently.origin.x = 0;
    frmVwRecently.origin.y = frmVwBtn.origin.y + frmVwBtn.size.height + 16;
    frmVwRecently.size.width = SCREEN_WIDTH;
    frmVwRecently.size.height = SCREEN_HEIGHT - frmVwRecently.origin.y - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveNotch]) {
        frmVwRecently.size.height = SCREEN_HEIGHT - frmVwRecently.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    frmLblTitleRecently.origin.x = 16;
    frmLblTitleRecently.origin.y = 16;
    frmLblTitleRecently.size.width = frmVwRecently.size.width - (frmLblTitleRecently.origin.x*2);
    
    frmTblRecently.origin.x = 0;
    frmTblRecently.origin.y = frmLblTitleRecently.origin.y + frmLblTitleRecently.size.height + 8;
    frmTblRecently.size.width = frmVwRecently.size.width;
    frmTblRecently.size.height = frmVwRecently.size.height - frmTblRecently.origin.y;
    
    self.vwTitle.frame = frmVwTitle;
    self.labelTitle.frame = frmLblTitle;
    self.textFieldInput.frame = frmTextField;
    self.vwLine.frame = frmVwLine;
    self.vwButton.frame = frmVwBtn;
    self.vwRecentlyUse.frame = frmVwRecently;
    self.lblTitleRecently.frame = frmLblTitleRecently;
    self.tblRecentlyUse.frame = frmTblRecently;
    
    self.btnN.frame = frmBtnN;
    self.btnB.frame = frmBtnB;
    
}

- (IBAction)doneClicked:(id)sender
{
    DLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark change next to cancel
- (IBAction)next:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    if([self.textFieldInput.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else if([[self.jsonData valueForKey:@"mandatory"]boolValue] && [[self.jsonData valueForKey:@"field_name"]isEqualToString:@"email"] && ![Utility validateEmailWithString:self.textFieldInput.text]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"INVALID_EMAIL") preferredStyle:UIAlertControllerStyleAlert];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
//        NSString *txtKey = [NSString stringWithFormat:@"txt%d",dataManager.incrementText];
//        dataManager.incrementText++;
        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"] :self.textFieldInput.text}];
        [super openNextTemplate];
    }
}

-(void) goToCancel {
    [super backToRoot];
}

#pragma mark change cancel to next
- (IBAction)cancel:(id)sender {
    [self goToNext];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textFieldInput resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    
    return YES;
}

-(void) showingRecentlyUses : (NSString *) menuId
                 mFieldName : (NSString *) fieldName{
    [self.vwRecentlyUse setHidden:TRUE];
    arDataRecently = [mHelper readListRecentlyUse:menuId mCode:[dataManager.dataExtra valueForKey:F_CODE]];
    if (arDataRecently.count > 0) {
        if ([fieldName isEqualToString:@"payment_id"]) {
            [self.vwRecentlyUse setHidden:FALSE];
            self.tblRecentlyUse.delegate = self;
            self.tblRecentlyUse.dataSource = self;
            [self.tblRecentlyUse setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
            [self.tblRecentlyUse registerClass:[CellRecentlyUse class] forCellReuseIdentifier:@"cell"];
            [self.tblRecentlyUse beginUpdates];
            [self.tblRecentlyUse endUpdates];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellRecentlyUse *cell = (CellRecentlyUse *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    
    [cell.lblContentOne setText:[data valueForKey:F_ACCT_TRSCT_NAME]];
    [cell.lblContentOne sizeToFit];
    cell.lblContentOne.numberOfLines = 0;

    [cell.lblContentTwo setText:[data valueForKey:F_ACCT_REK]];
    [cell.lblContentTwo sizeToFit];
    cell.lblContentTwo.numberOfLines = 0;

    CGRect frmVwBase = cell.vwBase.frame;
    CGRect frmLblContentOne = cell.lblContentOne.frame;
    CGRect frmLblContentTwo = cell.lblContentOne.frame;
    CGRect frmImgNext = cell.imgNext.frame;

    frmVwBase.origin.x = 16;
    frmVwBase.origin.y = 0;
    frmVwBase.size.width = cell.contentView.frame.size.width - (frmVwBase.origin.x * 2);
    frmVwBase.size.height = cell.contentView.frame.size.height;

    frmImgNext.size.height = 30;
    frmImgNext.size.width = 30;
    frmImgNext.origin.x = frmVwBase.size.width - frmImgNext.size.width;
    frmImgNext.origin.y = frmVwBase.size.height/2 - frmImgNext.size.height/2;

    frmLblContentOne.origin.x = 0;
    frmLblContentOne.origin.y = 16;
    frmLblContentOne.size.width = frmVwBase.size.width - frmImgNext.size.width - 8;

    frmLblContentTwo.origin.x = frmLblContentOne.origin.x;
    frmLblContentTwo.origin.y = frmLblContentOne.origin.y + frmLblContentOne.size.height + 8;
    frmLblContentTwo.size.width = frmLblContentOne.size.width;

    cell.vwBase.frame = frmVwBase;
    cell.imgNext.frame = frmImgNext;
    cell.lblContentOne.frame = frmLblContentOne;
    cell.lblContentTwo.frame = frmLblContentTwo;
    
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat padding = 16;
    CGFloat paddingMiddle = 8;
    CGFloat imgSize = 30;
    CGFloat actualWidth = SCREEN_WIDTH - imgSize - (padding*2) - paddingMiddle;
    
    NSDictionary *dataDict = [arDataRecently objectAtIndex:indexPath.row];
    
    CGSize mSzLblContentOne = [[dataDict valueForKey:F_ACCT_TRSCT_NAME] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15]}];
    CGSize mSzLblContentTwo = [[dataDict valueForKey:F_ACCT_REK] sizeWithAttributes:
    @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:14]}];
    
    CGFloat constrainHeight = 2000 + mSzLblContentOne.height +mSzLblContentTwo.height;
    
    CGSize constraint = CGSizeMake(actualWidth,constrainHeight);

    CGSize sizeLblCntnOne = [Utility heightWrapWithText:[dataDict valueForKey:F_ACCT_TRSCT_NAME]
                                               fontName:[UIFont fontWithName:@"HelveticaNeue" size:15] expectedSize:constraint];
    CGSize sizeLblCntnTwo = [Utility heightWrapWithText:[dataDict valueForKey:F_ACCT_REK]
                                               fontName:[UIFont fontWithName:@"HelveticaNeue" size:14] expectedSize:constraint];
    
    
    CGFloat mTotalHeight = padding + ceil(sizeLblCntnOne.height) + paddingMiddle + ceil(sizeLblCntnTwo.height) + padding;
    
    CGFloat mHeight = MAX(mTotalHeight, 50);
    return mHeight;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arDataRecently.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    if ([[self.jsonData valueForKey:@"field_name"] isEqualToString:@"payment_id"]) {
        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:[data valueForKey:F_ACCT_REK]}];
        [super openNextTemplate];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


@end
