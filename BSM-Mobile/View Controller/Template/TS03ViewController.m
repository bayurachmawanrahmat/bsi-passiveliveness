//
//  TS03ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 29/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TS03ViewController.h"
#import "CellHistoryList.h"
#import "CellTS03.h"

@interface TS03ViewController ()<ConnectionDelegate,
    UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate>{
        NSUserDefaults *userDefault;
        NSString *lang;
        UIToolbar *toolbar;
        NSArray *arListData;
        UIDatePicker *datePickerFrom;
        UIDatePicker *datePickerTo;
        NSString *titleToolbar;
        NSString *lblStatusSuccess;
        NSString *lblStatusFailed;
        BOOL isIna;
        UIView* imgNoData;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBackNav;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblListHistory;
@property (weak, nonatomic) IBOutlet UIView *vwDate;
@property (weak, nonatomic) IBOutlet UITextField *tfDateFrom;
@property (weak, nonatomic) IBOutlet UITextField *tfDateTo;
@property (weak, nonatomic) IBOutlet UILabel *lblRange;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblRangeFrom;


@end

@implementation TS03ViewController

- (void)loadView{
    [super loadView];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    isIna = [[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"];
    _lblTitle.text = [self.jsonData objectForKey:@"title"];
    
    if(isIna){
        _lblRange.text = @"Sampai :";
        _lblRangeFrom.text = @"Dari :";
        _resultLabel.text = @"Tidak Ada Riwayat";
        titleToolbar = @"Selesai";
        lblStatusSuccess = @"Transaksi Berhasil";
        lblStatusFailed = @"Transaksi Gagal";
    }else{
        _lblRange.text = @"To :";
        _lblRangeFrom.text = @"From :";
        _resultLabel.text = @"No Histories";
        titleToolbar = @"Done";
        lblStatusSuccess = @"Transaction Success";
        lblStatusFailed = @"Transaction Failed";
    }
    
//    self.tfDateTo.textAlignment = NSTextAlignmentCenter;
//    self.tfDateFrom.textAlignment = NSTextAlignmentCenter;
    self.tfDateTo.textAlignment = NSTextAlignmentLeft;
    self.tfDateFrom.textAlignment = NSTextAlignmentLeft;
    
    if (IPHONE_5) {
        [self.lblRange setFont:[UIFont fontWithName:const_font_name1 size:10.0f]];
        [self.lblRangeFrom setFont:[UIFont fontWithName:const_font_name1 size:10.0f]];
        [self.resultLabel setFont:[UIFont fontWithName:const_font_name1 size:10.0f]];
    }else if(IPHONE_8 || IPHONE_8P){
        [self.lblRange setFont:[UIFont fontWithName:const_font_name1 size:12.0f]];
        [self.lblRangeFrom setFont:[UIFont fontWithName:const_font_name1 size:12.0f]];
        [self.resultLabel setFont:[UIFont fontWithName:const_font_name1 size:13.0f]];
    }else{
        [self.lblRange setFont:[UIFont fontWithName:const_font_name1 size:14.0f]];
        [self.lblRangeFrom setFont:[UIFont fontWithName:const_font_name1 size:14.0f]];
        [self.resultLabel setFont:[UIFont fontWithName:const_font_name1 size:15.0f]];
    }
    
    [self.tblListHistory registerNib:[UINib nibWithNibName:@"CellTS03" bundle:nil]
    forCellReuseIdentifier:@"CELLTS03"];
        
    [self setLayout];
    [self setDatePickerLayout];
    
    [self initDate];
    
    [self getHistori];
}

-(void)getHistori{
    [dataManager.dataExtra setValue:self.tfDateFrom.text forKey:@"startdate"];
    [dataManager.dataExtra setValue:self.tfDateTo.text forKey:@"enddate"];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,startdate=%@,enddate=%@,id_account,pin,transaction_id,date_local", [self.jsonData valueForKey:@"url_parm"],self.tfDateFrom.text,self.tfDateTo.text] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

-(void)setLayout{
    self.btnBackNav.hidden = YES;

    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmVwDateRange = self.vwDate.frame;
    CGRect frmTFDateStart = self.tfDateFrom.frame;
    CGRect frmTFDateTo = self.tfDateTo.frame;
    CGRect frmLblRange = self.lblRange.frame;
    CGRect frmLblRangeFrom = self.lblRangeFrom.frame;
    
    CGRect frmTblListHistory = self.tblListHistory.frame;
    CGRect frmBtnBackNav = self.btnBackNav.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    CGRect frmResultLabel = self.resultLabel.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmVwDateRange.origin.x = 0;
    frmVwDateRange.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
    frmVwDateRange.size.width = SCREEN_WIDTH;
    
    frmLblRangeFrom.size.width = (SCREEN_WIDTH - 32) /2;
    frmLblRangeFrom.origin.x = 8;
    frmLblRangeFrom.origin.y = 0;
    frmTFDateStart.size.width = (SCREEN_WIDTH - 32)/2;
    frmTFDateStart.origin.x = 8;
    frmTFDateStart.origin.y = frmLblRange.size.height + 4;
    
    frmLblRange.size.width = (SCREEN_WIDTH - 32) / 2;
    frmLblRange.origin.y = 0;
    frmTFDateTo.size.width = (SCREEN_WIDTH -32)/2;
    frmTFDateTo.origin.x = frmTFDateStart.origin.x + frmTFDateStart.size.width + 16;
    frmLblRange.origin.x = frmTFDateTo.origin.x;
    frmTFDateTo.origin.y = frmLblRange.size.height + 4;
    
    
    frmTblListHistory.origin.x = 0;
    frmTblListHistory.origin.y = frmVwDateRange.origin.y + frmVwDateRange.size.height;
    frmTblListHistory.size.width = SCREEN_WIDTH;
    frmTblListHistory.size.height = SCREEN_HEIGHT - frmTblListHistory.origin.y - BOTTOM_NAV_DEF;
    if ([Utility isDeviceHaveNotch]) {
        frmTblListHistory.size.height = SCREEN_HEIGHT - frmTblListHistory.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    frmResultLabel.size.width = SCREEN_WIDTH;
    frmResultLabel.origin.x = 0;
    frmResultLabel.origin.y = frmTblListHistory.size.height/2;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmVwTitle.size.width - 32;
    frmLblTitle.size.height = 30;
    
    if (self.btnBackNav.isHidden) {
        frmLblTitle.origin.x = 16;
    }
    
    self.resultLabel.frame = frmResultLabel;

    self.vwTitle.frame = frmVwTitle;
    self.vwDate.frame = frmVwDateRange;
    self.tfDateFrom.frame = frmTFDateStart;
    self.tfDateTo.frame = frmTFDateTo;
    self.lblRange.frame = frmLblRange;
    _lblRange.textAlignment = NSTextAlignmentLeft;
    self.lblRangeFrom.frame = frmLblRangeFrom;
    _lblRangeFrom.textAlignment = NSTextAlignmentLeft;
    self.tblListHistory.frame = frmTblListHistory;
    self.btnBackNav.frame = frmBtnBackNav;
    self.lblTitle.frame = frmLblTitle;
    
    _resultLabel.hidden = YES;
    imgNoData = [Utility showNoData:_tblListHistory];
    imgNoData.hidden = YES;
    [self.tblListHistory setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    [self.tblListHistory addSubview:_resultLabel];
    [self.tblListHistory addSubview:imgNoData];
    
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
}

-(void)setDatePickerLayout{
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:titleToolbar style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    datePickerFrom = [[UIDatePicker alloc]init];
    if (@available(iOS 13.4, *)) {
        [datePickerFrom setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePickerFrom setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    datePickerFrom.tag = 1;
    [datePickerFrom setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePickerFrom setBackgroundColor:[UIColor whiteColor]];
    [datePickerFrom setDatePickerMode:UIDatePickerModeDate];
    [datePickerFrom addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    datePickerFrom.minimumDate = [self getDateDifference:-6 withDate:[NSDate date]];
    datePickerFrom.maximumDate = [NSDate date];
    
    self.tfDateFrom.inputAccessoryView = toolbar;
    self.tfDateFrom.inputView = datePickerFrom;
    
    datePickerTo = [[UIDatePicker alloc]init];
    if (@available(iOS 13.4, *)) {
        [datePickerTo setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePickerTo setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    datePickerTo.tag = 2;
    [datePickerTo setLocale:[[NSLocale alloc]initWithLocaleIdentifier:isIna?@"id":@"en"]];
    [datePickerTo setBackgroundColor:[UIColor whiteColor]];
    [datePickerTo setDatePickerMode:UIDatePickerModeDate];
    [datePickerTo addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    datePickerTo.maximumDate = [NSDate date];
    
    self.tfDateTo.inputAccessoryView = toolbar;
    self.tfDateTo.inputView = datePickerTo;
}

-(void)doneClicked:(id)sender{
    [self.tfDateFrom resignFirstResponder];
    [self.tfDateTo resignFirstResponder];
    
    [self getHistori];
}

- (void)viewDidAppear:(BOOL)animated{
    UIImageView* imgCalendar = [[UIImageView alloc]initWithFrame:CGRectMake(_tfDateTo.frame.size.width - _tfDateTo.frame.size.height, 0, _tfDateTo.frame.size.height, _tfDateTo.frame.size.height)];
    [imgCalendar setImage:[UIImage imageNamed:@"ic_frm_calendar"]];
    
    UIImageView* imgCalendar2 = [[UIImageView alloc]initWithFrame:CGRectMake(_tfDateTo.frame.size.width - _tfDateTo.frame.size.height, 0, _tfDateTo.frame.size.height, _tfDateTo.frame.size.height)];
    [imgCalendar2 setImage:[UIImage imageNamed:@"ic_frm_calendar"]];
    
    [_tfDateTo addSubview:imgCalendar];
    [_tfDateFrom addSubview:imgCalendar2];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTitle.text = [self.jsonData objectForKey:@"title"];
    arListData = [[NSArray alloc]init];
    
    [self.tblListHistory setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    self.tblListHistory.delegate = self;
    self.tblListHistory.dataSource = self;
    [self.tblListHistory beginUpdates];
    [self.tblListHistory endUpdates];
    
}

- (NSDate*)getDateDifference:(NSInteger)number withDate:(NSDate*)date{
    NSDate *today = date;
    NSLog(@"%@", date);
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setMonth: +(number)]; // note that I'm setting it to -1
    NSDate *endOfWorldWar3 = [gregorian dateByAddingComponents:offsetComponents toDate:today options:0];
    NSLog(@"%@", endOfWorldWar3);
    
    return endOfWorldWar3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arListData.count;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if ([requestType isEqualToString:@"inq_tfscheduled_history"]) {
        
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            NSLog(@"%@", jsonObject);
            
            NSError *error;
            NSDictionary *dataStateSheduler = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
            
            if (error == nil) {
                arListData = (NSArray *) [dataStateSheduler valueForKey:@"data"];
                [self.tblListHistory reloadData];
                [self.tblListHistory beginUpdates];
                [self.tblListHistory endUpdates];
                
                NSLog(@"Long of list History %ld", [_tblListHistory numberOfRowsInSection:0]);

                if([_tblListHistory numberOfRowsInSection:0] == 0){
//                    _resultLabel.hidden = NO;
                    imgNoData.hidden = NO;
                }else{
//                    _resultLabel.hidden = YES;
                    imgNoData.hidden = YES;
                }
                
                
            }else{
                NSLog(@"%@", error.localizedDescription);
            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show]; // klik alert ke home
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellTS03 *cell = (CellTS03*)[tableView dequeueReusableCellWithIdentifier:@"CELLTS03"];
    
    NSDictionary *dataDictHistory = [arListData objectAtIndex:indexPath.row];
    
    if(isIna){
        cell.fromAccountTitle.text = @"Dari Rekening";
        cell.toAccountTItle.text = @"Ke Rekening";
        cell.descTitle.text = @"Keterangan";
    }else{
        cell.fromAccountTitle.text = @"From Account";
        cell.toAccountTItle.text = @"To Account";
        cell.descTitle.text = @"Description";
    }
    
    cell.toAccountName.text = [dataDictHistory valueForKey:@"beneficiary_name"];
    cell.toAccount.text = [dataDictHistory valueForKey:@"creditacctno"];
    cell.amountTransfer.text = [dataDictHistory valueForKey:@"debitamount"];
    cell.amountTransfer.textColor = UIColorFromRGB(const_color_secondary);
    cell.fromAccount.text = [dataDictHistory valueForKey:@"debitacctno"];
    cell.desc.text = [dataDictHistory valueForKey:@"remark"];
    cell.dateTransaction.text = [dataDictHistory valueForKey:@"datetime"];
    
    if([[dataDictHistory valueForKey:@"status"] isEqualToString:@"SUCCESS"]){
        cell.statusLabel.text = lblStatusSuccess;
        cell.statusLabel.textColor = UIColorFromRGB(0x78B678);
        cell.statusView.backgroundColor = UIColorFromRGB(0xEFFAEA);
    }else{
        cell.statusLabel.text = lblStatusFailed;
        cell.statusLabel.textColor = UIColorFromRGB(0xED4C5C);
        cell.statusView.backgroundColor = UIColorFromRGB(0xFFF6F6);
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


- (void)errorLoadData:(NSError *)error{
    
}
- (void)reloadApp{
    
}
- (void) resetTimer{
    NSDictionary* userinfo = @{@"position": @(1112)};
    NSNotificationCenter* notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter postNotificationName:@"listenerNavigate" object:self userInfo:userinfo];
}

- (void)initDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDate = [dateFormat stringFromDate:[NSDate date]];
    
    self.tfDateFrom.text = currentDate;
    self.tfDateTo.text = currentDate;
    
}

- (void)dateChanged:(UIDatePicker *)datePicker{
    [self resetTimer];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString* selectedDate = [dateFormat stringFromDate:datePicker.date];
    
    if(datePicker.tag == 1){
        self.tfDateFrom.text = selectedDate;
        datePickerTo.maximumDate = [self getDateDifference:3 withDate:datePicker.date];
    }else{
        self.tfDateTo.text = selectedDate;
        datePickerFrom.maximumDate = [NSDate date];
    }
}

@end
