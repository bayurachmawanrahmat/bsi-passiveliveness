//
//  SUCFAVViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 03/06/18.
//  Copyright © 2018 lds. All rights reserved.
//


#import "SUCFAVViewController.h"
#import "Utility.h"

@interface SUCFAVViewController() <ConnectionDelegate> {

}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMesage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpace;

@end

@implementation SUCFAVViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        _lblTitle.text = @"Favorit";
    } else {
        _lblTitle.text = @"Favorite";
    }
    
    [Styles setTopConstant:_topSpace];
    
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    
    NSMutableDictionary *dd = [[[DataManager sharedManager]favData] mutableCopy];
    if ([userDefault objectForKey:@"typeTransfer"]){
        [dd setObject:[userDefault objectForKey:@"typeTransfer"] forKey:@"type_transfer"];
        [userDefault removeObjectForKey:@"typeTransfer"];
    }
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    conn.fav = true;
    [conn sendPost:dd needLoading:true textLoading:lang(@"ADD_FAV") encrypted:true];
 
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated{
   
}

- (void)viewDidDisappear:(BOOL)animated{
}



- (IBAction)ok:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dt = [userDefault objectForKey:@"passingData"];
    NSString *is = [dt objectForKey:@"infaq_status"];
    
    if ([is isEqualToString:@"1"]) {
        [super openTemplate:@"LD01" withData:dataManager.getObjectData];
    }
    else {
        [super backToRoot];
    }
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        if (![requestType isEqualToString:@"check_notif"]) {
            _lblMesage.text = [jsonObject valueForKey:@"response"];
        }
    } else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToRoot];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToRoot];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}



@end

