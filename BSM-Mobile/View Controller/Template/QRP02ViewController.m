//
//  QRP02ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 30/10/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "QRP02ViewController.h"
#import "Utility.h"

@interface QRP02ViewController ()<ConnectionDelegate, UITextFieldDelegate, UIAlertViewDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang, *isQrDomestic;
    double mPercentage;
    BOOL flagVwMpanIsHidden, flagVwTipIsHidden;
    NSNumberFormatter* currencyFormatter;
    UIView *whiteVIew;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMerchant;
@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;
@property (weak, nonatomic) IBOutlet UIView *vwNominal;
@property (weak, nonatomic) IBOutlet UIView *vwMpan;
@property (weak, nonatomic) IBOutlet UIView *vwTip;
@property (weak, nonatomic) IBOutlet UIView *vwInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblNominal;
@property (weak, nonatomic) IBOutlet UITextField *txtNominal;
@property (weak, nonatomic) IBOutlet UIView *vwLineNominal;
@property (weak, nonatomic) IBOutlet UILabel *lblMpan;
@property (weak, nonatomic) IBOutlet UITextField *txtMpan;
@property (weak, nonatomic) IBOutlet UIView *vwLineMpan;
@property (weak, nonatomic) IBOutlet UILabel *lblTip;
@property (weak, nonatomic) IBOutlet UITextField *txtTip;
@property (weak, nonatomic) IBOutlet UIView *vwLineTip;
@property (weak, nonatomic) IBOutlet UIImageView *ivInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSelanjutnya;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatals;

@end

@implementation QRP02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    whiteVIew = [[UIView alloc]init];
    
    //Setup currencyFormatter
    currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setDecimalSeparator:@"."];
    [currencyFormatter setGroupingSeparator:@""];
    [currencyFormatter setMaximumFractionDigits:2];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    self.lblMerchant.text = @"";
    self.lblNominal.text = @"Nominal";
//    self.lblMpan.text = @"No. Rekening Tujuan";
    self.lblMpan.text = @"Bank Acquire ";
    self.lblTip.text = @"Tip";
    [self.btnBatals setTitle:@"Batal" forState:UIControlStateNormal];
    [self.btnSelanjutnya setTitle:@"Selanjutnya" forState:UIControlStateNormal];
    NSString *msgToolbar = @"Selesai";
    
    if ([lang isEqualToString:@"en"]) {
//        self.lblMpan.text = @"No Destination Account";
        [self.btnBatals setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.btnSelanjutnya setTitle:@"Next" forState:UIControlStateNormal];
        msgToolbar = @"Done";
    }
    
    [self.lblNominal sizeToFit];
    self.lblNominal.numberOfLines = 2;
    
    [self.lblMpan sizeToFit];
    self.lblMpan.numberOfLines = 2;
    
    [self.lblTip sizeToFit];
    self.lblTip.numberOfLines = 2;
    
    mPercentage = 0;
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:msgToolbar
                                                                   style:UIBarButtonItemStylePlain  target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    self.txtNominal.inputAccessoryView = keyboardDoneButtonView;
    self.txtNominal.tag = 1;
    self.txtNominal.delegate = self;
    [self.txtNominal setKeyboardType: UIKeyboardTypeDecimalPad];
    
    self.txtMpan.inputAccessoryView = keyboardDoneButtonView;
    self.txtMpan.tag = 2;
    self.txtMpan.delegate = self;
    [self.txtMpan setKeyboardType: UIKeyboardTypeNumberPad];
    
    self.txtTip.inputAccessoryView = keyboardDoneButtonView;
    self.txtTip.tag = 3;
    self.txtTip.delegate = self;
    [self.txtTip setKeyboardType: UIKeyboardTypeDecimalPad];
    
    [self.btnBatals addTarget:self action:@selector(actionBatal:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnBatals setColorSet:SECONDARYCOLORSET];
    
    [self.btnSelanjutnya addTarget:self action:@selector(actionNext:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnSelanjutnya setColorSet:PRIMARYCOLORSET];
    
    self.lblTitle.text = [self.jsonData valueForKey:@"title"];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if (needRequest) {
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"url"]boolValue]];
    }
    [self.view addSubview:whiteVIew];
    [self setupLayout];
    [self registerForKeyboardNotifications];
    
}



-(void) setupLayout{
    CGRect frmVwWhitey = whiteVIew.frame;
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    CGRect frmLblMerchant = self.lblMerchant.frame;
    CGRect frmVwScroll = self.vwScroll.frame;
    CGRect frmVwButton = self.vwButton.frame;
    
    CGRect frmVwContentScroll = self.vwContentScroll.frame;
    CGRect frmVwNominal = self.vwNominal.frame;
    CGRect frmVwMpan = self.vwMpan.frame;
    CGRect frmVwTip = self.vwTip.frame;
    CGRect frmVwInfo = self.vwInfo.frame;
    
    CGRect frmLblNominal = self.lblNominal.frame;
    CGRect frmTxtNominal = self.txtNominal.frame;
    CGRect frmVwLineNominal = self.vwLineNominal.frame;
    
    CGRect frmLblMpan = self.lblMpan.frame;
    CGRect frmTxtMpan = self.txtMpan.frame;
    CGRect frmVwLineMpan =self.vwLineMpan.frame;
    
    CGRect frmLblTip = self.lblTip.frame;
    CGRect frmTxtTip = self.txtTip.frame;
    CGRect frmVwLineTip = self.vwLineTip.frame;
    
    CGRect frmIvInfo = self.ivInfo.frame;
    CGRect frmLblInfo = self.lblInfo.frame;
    
    CGRect frmBtnBatals = self.btnBatals.frame;
    CGRect frmBtnSelanjutnya =self.btnSelanjutnya.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmLblMerchant.origin.x = 16;
    frmLblMerchant.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 16;
    frmLblMerchant.size.width = SCREEN_WIDTH - (frmLblMerchant.origin.x *2);
    
    frmVwScroll.origin.x = frmVwTitle.origin.x;
    frmVwScroll.origin.y = frmLblMerchant.origin.y + frmLblMerchant.size.height + 24;
    frmVwScroll.size.width = SCREEN_WIDTH;
    
    frmVwButton.origin.x = 0;
    frmVwButton.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_DEF - frmVwButton.size.height - 8;
    if ([Utility isDeviceHaveNotch2]) {
        frmVwButton.origin.y = SCREEN_HEIGHT - BOTTOM_NAV_NOTCH - frmVwButton.size.height - 22;
    }
    frmVwButton.size.width = SCREEN_WIDTH;
    
    frmBtnBatals.origin.x = 16;
    frmBtnBatals.origin.y = 0;
    frmBtnBatals.size.width = frmVwButton.size.width/2 - 32;
    
    frmBtnSelanjutnya.origin.x = frmBtnBatals.origin.x + frmBtnBatals.size.width + 32;
    frmBtnSelanjutnya.origin.y = 0;
    frmBtnSelanjutnya.size.width = frmBtnBatals.size.width;
    
    frmVwScroll.size.height = frmVwButton.origin.y - frmVwScroll.origin.y - 16;
    
    frmVwContentScroll.origin.x = 0;
    frmVwContentScroll.origin.y = 0;
    frmVwContentScroll.size.width = frmVwScroll.size.width;
    
    frmVwNominal.origin.x = 0;
    frmVwNominal.origin.y = 0;
    frmVwNominal.size.width = frmVwContentScroll.size.width;
    
    frmLblNominal.origin.x = 16;
    frmLblNominal.origin.y = 0;
    frmLblNominal.size.width = frmVwNominal.size.width - (frmLblNominal.origin.x * 2);
    
    frmTxtNominal.origin.x = frmLblNominal.origin.x;
    frmTxtNominal.origin.y = frmLblNominal.origin.y + frmLblNominal.size.height + 8;
    frmTxtNominal.size.width = frmVwNominal.size.width - (frmTxtNominal.origin.x * 2);
    
    frmVwLineNominal.origin.x = frmLblNominal.origin.x;
    frmVwLineNominal.origin.y = frmTxtNominal.origin.y + frmTxtNominal.size.height + frmVwLineNominal.size.height;
    frmVwLineNominal.size.width = frmVwNominal.size.width - (frmVwLineNominal.origin.x * 2);
    
    frmVwNominal.size.height = frmVwLineNominal.origin.y + frmVwLineNominal.size.height + 8;
    
    frmVwMpan.origin.x = 0;
    frmVwMpan.origin.y = frmVwNominal.origin.y + frmVwNominal.size.height + 8;
    frmVwMpan.size.width = frmVwContentScroll.size.width;
    
    frmLblMpan.origin.x = 16;
    frmLblMpan.origin.y = 0;
    frmLblMpan.size.width = frmVwMpan.size.width - (frmLblMpan.origin.x * 2);
    
    frmTxtMpan.origin.x = frmLblMpan.origin.x;
    frmTxtMpan.origin.y = frmLblMpan.origin.y + frmLblMpan.size.height + 8;
    frmTxtMpan.size.width = frmVwMpan.size.width - (frmTxtMpan.origin.x * 2);
    
    frmVwLineMpan.origin.x = frmLblMpan.origin.x;
    frmVwLineMpan.origin.y = frmTxtMpan.origin.y + frmTxtMpan.size.height + frmVwLineMpan.size.height;
    frmVwLineMpan.size.width = frmVwMpan.size.width - (frmVwLineMpan.origin.x * 2);
    
    if (flagVwMpanIsHidden) {
        frmVwMpan.size.height = 0;
    } else {
        frmVwMpan.size.height = frmVwLineMpan.origin.y + frmVwLineMpan.size.height + 8;
    }
    
    frmVwTip.origin.x = 0;
    frmVwTip.origin.y = frmVwMpan.origin.y + frmVwMpan.size.height + 8;
    frmVwTip.size.width = frmVwContentScroll.size.width;
    
    frmLblTip.origin.x = 16;
    frmLblTip.origin.y = 0;
    frmLblTip.size.width = frmVwTip.size.width - (frmLblTip.origin.x * 2);
    
    frmTxtTip.origin.x = frmLblTip.origin.x;
    frmTxtTip.origin.y = frmLblTip.origin.y + frmLblTip.size.height + 8;
    frmTxtTip.size.width = frmVwTip.size.width - (frmTxtTip.origin.x * 2);
    
    frmVwLineTip.origin.x = frmLblTip.origin.x;
    frmVwLineTip.origin.y = frmTxtTip.origin.y + frmTxtTip.size.height + frmVwLineTip.size.height;
    frmVwLineTip.size.width = frmVwTip.size.width - (frmVwLineTip.origin.x * 2);
    
    if (flagVwTipIsHidden) {
        frmVwTip.size.height = 0;
    } else {
        frmVwTip.size.height = frmVwLineTip.origin.y + frmVwLineTip.size.height + 8;
    }
    
    frmVwInfo.origin.x = 16;
    frmVwInfo.origin.y = frmVwTip.origin.y + frmVwTip.size.height + 8;
    frmVwInfo.size.width = frmVwContentScroll.size.width-32;
    frmVwInfo.size.height = 40;
    
    frmIvInfo.origin.x = 10;
    frmIvInfo.origin.y = 5;
    frmIvInfo.size.width = 16;
    frmIvInfo.size.height = 16;
    
    frmLblInfo.origin.x = 30;
    frmLblInfo.origin.y = 5;
    frmLblInfo.size.width = frmVwInfo.size.width - (frmLblInfo.origin.x * 2);
    
    frmVwInfo.size.height = frmLblInfo.origin.y + frmLblInfo.size.height + 8;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    frmLblTitle.size.height = frmVwTitle.size.height;
    
    self.vwTitle.frame = frmVwTitle;
    self.vwTitle.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.lblTitle.frame = frmLblTitle;
    
    self.lblMerchant.frame = frmLblMerchant;
    self.vwScroll.frame = frmVwScroll;
    self.vwButton.frame = frmVwButton;
    
    self.vwContentScroll.frame = frmVwContentScroll;
    self.vwNominal.frame = frmVwNominal;
    self.vwMpan.frame = frmVwMpan;
    self.vwTip.frame = frmVwTip;
    self.vwInfo.frame = frmVwInfo;
    
    self.lblNominal.frame = frmLblNominal;
    self.txtNominal.frame = frmTxtNominal;
    self.vwLineNominal.frame = frmVwLineNominal;
    
    self.lblMpan.frame = frmLblMpan;
    self.txtMpan.frame = frmTxtMpan;
    self.vwLineMpan.frame = frmVwLineMpan;
    
    self.lblTip.frame = frmLblTip;
    self.txtTip.frame = frmTxtTip;
    self.vwLineTip.frame = frmVwLineTip;
    
    self.btnBatals.frame = frmBtnBatals;
    self.btnSelanjutnya.frame = frmBtnSelanjutnya;
    
    frmVwWhitey.origin.x = 0;
    frmVwWhitey.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmVwWhitey.size.width = SCREEN_WIDTH;
    frmVwWhitey.size.height = SCREEN_HEIGHT - frmVwWhitey.origin.y;
    
    whiteVIew.frame = frmVwWhitey;
    whiteVIew.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    
    self.vwInfo.layer.cornerRadius = 15;
    self.vwInfo.layer.masksToBounds = true;
    self.ivInfo.layer.cornerRadius = 8;
    self.ivInfo.layer.masksToBounds = true;
    self.lblInfo.numberOfLines = 2;
    
    [self.vwScroll setContentSize:self.vwContentScroll.frame.size];
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
        if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[[jsonObject valueForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            if (dataDict) {
                NSLog(@"%@", dataDict);
                self.lblMerchant.text = [dataDict valueForKey:@"msg"];
                [self.lblMerchant sizeToFit];
                self.lblMerchant.numberOfLines = 0;
                
                // traping for qris xborder
                isQrDomestic = [dataDict valueForKey:@"is_qrdomestic"];
                self.vwMpan.hidden = NO;
                flagVwMpanIsHidden = NO;
                self.vwInfo.hidden = YES;
                if ([isQrDomestic isEqualToString:@"false"]) {
                    self.vwMpan.hidden = YES;
                    flagVwMpanIsHidden = YES;
                    self.vwInfo.hidden = NO;
                    self.txtNominal.text = [dataDict valueForKey:@"amount"];
                    self.txtTip.text = [dataDict valueForKey:@"tips"];
                } else {
                    self.txtNominal.text = [Utility formatCurrencyValue:[[dataDict valueForKey:@"amount"]doubleValue]];
                    self.txtTip.text =[Utility formatCurrencyValue:[[dataDict valueForKey:@"tips"]doubleValue]];
                }
                self.txtNominal.enabled = false;
                self.txtTip.enabled = false;
                
                self.txtMpan.text = [dataDict valueForKey:@"mpan"];
                self.txtMpan.enabled = false;
                
                self.vwTip.hidden = NO;
                flagVwTipIsHidden = NO;
                
                if ([dataDict objectForKey:@"percentage"]) {
                    mPercentage = [[dataDict valueForKey:@"percentage"]doubleValue];
                }
                
//                if ([[dataDict objectForKey:@"tips"]isEqualToString:@""] || ![dataDict objectForKey:@"tips"] || [[dataDict objectForKey:@"tips"]isEqualToString:@"0"] ) {
                if ([[dataDict objectForKey:@"tips"]isEqualToString:@"0"]) {
//                    self.txtTip.text = @"";
                    self.txtTip.enabled = false;
                    self.vwTip.hidden = YES;
                    flagVwTipIsHidden = YES;
                } else if (![dataDict objectForKey:@"tips"]){
                    self.txtTip.text = @"";
                    self.txtTip.enabled = true;
                }
                
                if ([[dataDict objectForKey:@"amount"] doubleValue] == 0) {
                    self.txtNominal.text = @"";
                    self.txtNominal.enabled = true;
                }
                
                [dataManager.dataExtra setValue:[dataDict objectForKey:@"admfee"] forKey:@"admfee"];
                [dataManager.dataExtra setValue:[dataDict objectForKey:@"code"] forKey:@"code"];
                
                self.lblInfo.text = [dataDict objectForKey:@"footer"];
                [self.lblInfo sizeToFit];
                self.lblInfo.numberOfLines = 0;
                
                [self setupLayout];
                [whiteVIew removeFromSuperview];
            }
            [dataManager.dataExtra setValue:[jsonObject valueForKey:@"transaction_id"] forKey:@"transaction_id"];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[jsonObject valueForKey:@"response"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

-(void)doneClicked : (id) sender{
    [self.view endEditing:YES];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
//    NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
    NSString* newText = [[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    if ([newText length] == 0){
        textField.text = nil;
    } else{
        NSString* lastChar = [newText substringFromIndex:[newText length]-1];
        NSArray *sep = [newText componentsSeparatedByString:@"."];
        if (([lastChar isEqualToString:@"."] && [sep count]<=2) || [sep.lastObject isEqualToString:@"0"]) {
            textField.text = newText;
            return NO;
        }
        
        double newValue = [newText doubleValue];
        textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
        if (textField.tag == 1) {
            if (mPercentage != 0) {
                double newValue = [newText doubleValue];
                double mNewTips = newValue * mPercentage;
                self.txtTip.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:mNewTips]];
            }
        }
        
        return NO;
        
    }
    return YES;
}

-(void) actionNext : (UIButton *) sender{
    if ([self.txtNominal.text isEqualToString:@""]){
        NSString* msg = [NSString stringWithFormat: @"%@ %@",self.lblNominal.text, lang(@"NOT_EMPTY")];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
//    } else if ([self.txtTip.text isEqualToString:@""]){
//        NSString* msg = [NSString stringWithFormat: @"%@ %@",self.lblTip.text, lang(@"NOT_EMPTY")];
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
    } else {
        NSString *strNominal, *strTips;
        strNominal = [self.txtNominal.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        strTips = [self.txtTip.text stringByReplacingOccurrencesOfString:@"," withString:@""];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%.2lf", [strNominal doubleValue]] forKey:@"amount"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%.2lf", [strTips doubleValue]] forKey:@"tips"];
        [dataManager.dataExtra setValue:self.txtMpan.text forKey:@"mpan"];
        [dataManager.dataExtra setValue:isQrDomestic forKey:@"is_qrdomestic"];
        
        [super openNextTemplate];
    }
}

-(void) actionBatal : (UIButton *) sender{
    [self backToRoot];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self backToRoot];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height + kbSize.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height/2, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwContentScroll.frame.size.width, self.vwContentScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, 10, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
