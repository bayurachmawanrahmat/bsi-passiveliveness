//
//  LD01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 9/17/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "LD01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "AppProperties.h"

@interface LD01ViewController () <ConnectionDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSArray *listData;
    NSArray *listAction;
    NSArray *listOrigin;
    NSDictionary *dataObject;
    NSDictionary *dataLocal;
    BOOL first;
    BOOL fromHome;
    BOOL spesial;
    BOOL mustAddToManager;
    NSString *key;
}
@property (weak, nonatomic) IBOutlet UITableView *tblDenomInfaq;
@property (weak, nonatomic) IBOutlet UILabel *lblDescInfaq;
@property (weak, nonatomic) IBOutlet UIView *vwDenomInfaq;
@property (weak, nonatomic) IBOutlet CustomBtn *btnInfaq;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *bgView;
- (IBAction)batalAction:(id)sender;

@end

@implementation LD01ViewController

NSString *reqType;
extern NSString *lv_selected;
UIButton *btnMore;
UIButton *btnShare;
UIButton *btnFav;
UILabel *lblMore;
UILabel *lblShare;
UILabel *lblFav;
NSString *isClicked;
CGRect screenBound;
CGSize screenSize;
CGFloat screenWidth;
CGFloat screenHeight;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)setFirstLV:(BOOL)aFirstLV{
    first = aFirstLV;
}

- (void)setFromHome:(BOOL)afromHome{
    fromHome = afromHome;
}

- (void)setSpecial:(BOOL)aspecial{
    spesial = aspecial;
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSString *paramData = [NSString stringWithFormat:@"request_type=list_denom,code=%d",8002];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:paramData needLoading:true encrypted:false banking:false];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblDescInfaq.text = @"Berinfaq hari ini untuk Lembaga Amil Zakat Nasional BSM Umat.";
        [self.btnInfaq setTitle:@"Batal" forState:UIControlStateNormal];
    } else {
        self.lblDescInfaq.text = @"Give infaq today for National Amil Zakat BSM Umat Foundation";
        [self.btnInfaq setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmDenom = self.vwDenomInfaq.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    CGRect frmLabelDescInfaq = self.lblDescInfaq.frame;
    CGRect frmButtonInfaq = self.btnInfaq.frame;
    CGRect frmTableDenomInfaq = self.tblDenomInfaq.frame;
    
    frmDenom.origin.y = TOP_NAV;
    frmDenom.size.width = screenWidth;
    frmDenom.size.height = 40;
    frmDenom.origin.x = 0;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 0;
    frmLblTitle.size.width = frmDenom.size.width - 16;
    frmLblTitle.size.height = frmDenom.size.height;
    
    frmLabelDescInfaq.size.width = screenWidth - 30;
    frmButtonInfaq.size.width = screenWidth - 30;
    frmButtonInfaq.origin.y = screenHeight - (frmButtonInfaq.size.height + BOTTOM_NAV) - 20;
    if ([Utility isDeviceHaveNotch]) {
        frmButtonInfaq.origin.y = screenHeight - (frmButtonInfaq.size.height + BOTTOM_NAV) - 80;
    }
    frmTableDenomInfaq.size.width = screenWidth - 25;
    frmTableDenomInfaq.size.height = frmButtonInfaq.origin.y - (frmLabelDescInfaq.origin.y + frmLabelDescInfaq.size.height) - frmButtonInfaq.size.height;
    
    
    
    self.vwDenomInfaq.frame = frmDenom;
    self.lblTitle.frame = frmLblTitle;
    self.lblDescInfaq.frame = frmLabelDescInfaq;
    self.btnInfaq.frame = frmButtonInfaq;
    self.tblDenomInfaq.frame = frmTableDenomInfaq;
    
    //amanah styles
    
    [self.btnInfaq setColorSet:SECONDARYCOLORSET];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.vwDenomInfaq.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    isClicked = @"0";
    //[self addMyButton];
    
    self.tblDenomInfaq.dataSource = self;
    self.tblDenomInfaq.delegate = self;
    [self.tblDenomInfaq setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

- (void)viewWillDisappear:(BOOL)animated {
    /*btnMore.removeFromSuperview;
     btnFav.removeFromSuperview;
     btnShare.removeFromSuperview;
     lblMore.removeFromSuperview;
     lblFav.removeFromSuperview;
     lblShare.removeFromSuperview;*/
}

- (void)addMyButton{    // Method for creating button, with background image and other properties
    UIWindow *window            = [UIApplication sharedApplication].keyWindow;
    _bgView                     = [[UIView alloc] initWithFrame:CGRectMake(screenWidth - 120, screenHeight - 130, 200, 50)];
    _bgView.backgroundColor     = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.0];
    [window addSubview:_bgView];
    
    lblMore = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    lblMore.text = @"MORE";
    lblMore.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblMore.numberOfLines = 1;
    lblMore.baselineAdjustment = YES;
    lblMore.adjustsFontSizeToFitWidth = YES;
    //lblMore.adjustsLetterSpacingToFitWidth = YES;
    //lblMore.size = [lblMore.text sizeWithFont:lblMore.font constrainedToSize:CGSizeMake(380, 20) lineBreakMode:NSLineBreakByTruncatingTail];
    //lblMore.minimumScaleFactor = MIN_SCALE_FACTOR;
    lblMore.clipsToBounds = YES;
    lblMore.backgroundColor = [UIColor clearColor];
    lblMore.textColor = [UIColor blackColor];
    lblMore.textAlignment = NSTextAlignmentLeft;
    [_bgView addSubview:lblMore];
    
    btnMore = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnMore.frame = CGRectMake(50, 0, 50, 50);
    btnMore.backgroundColor = UIColor.yellowColor;
    btnMore.layer.cornerRadius = btnMore.frame.size.height / 2;
    btnMore.layer.masksToBounds = true;
    //btn.isUserInteractionEnabled = true;
    //[btn setTitle:@"VIEW" forState:UIControlStateNormal];
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-menu-vertical-24.png"];
    [btnMore setImage:btnFloat forState:UIControlStateNormal];
    [btnMore addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:btnMore];
}

- (void)addFavButton{
    UIWindow *window            = [UIApplication sharedApplication].keyWindow;
    _bgView                     = [[UIView alloc] initWithFrame:CGRectMake(screenWidth - 130, screenHeight - 260, 50, 50)];
    _bgView.backgroundColor     = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.0];
    [window addSubview:_bgView];
    
    lblFav = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    lblFav.text = @"FAVORITE";
    lblFav.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblFav.numberOfLines = 1;
    lblFav.baselineAdjustment = YES;
    lblFav.adjustsFontSizeToFitWidth = YES;
    lblFav.clipsToBounds = YES;
    lblFav.backgroundColor = [UIColor clearColor];
    lblFav.textColor = [UIColor blackColor];
    lblFav.textAlignment = NSTextAlignmentLeft;
    [_bgView addSubview:lblFav];
    
    btnFav = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnFav.frame = CGRectMake(60, 0, 50, 50);
    btnFav.backgroundColor = UIColor.yellowColor;
    btnFav.layer.cornerRadius = btnFav.frame.size.height / 2;
    btnFav.layer.masksToBounds = true;
    
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-heart-outline-24.png"];
    [btnFav setImage:btnFloat forState:UIControlStateNormal];
    [btnFav addTarget:self action:@selector(favButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:btnFav];
}

- (void)addShareButton{
    UIWindow *window            = [UIApplication sharedApplication].keyWindow;
    _bgView                     = [[UIView alloc] initWithFrame:CGRectMake(screenWidth - 130, screenHeight - 195, 50, 50)];
    _bgView.backgroundColor     = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.0];
    [window addSubview:_bgView];
    
    lblShare = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    lblShare.text = @"SHARE";
    lblShare.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblShare.numberOfLines = 1;
    lblShare.baselineAdjustment = YES;
    lblShare.adjustsFontSizeToFitWidth = YES;
    lblShare.clipsToBounds = YES;
    lblShare.backgroundColor = [UIColor clearColor];
    lblShare.textColor = [UIColor blackColor];
    lblShare.textAlignment = NSTextAlignmentLeft;
    [_bgView addSubview:lblShare];
    
    btnShare = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnShare.frame = CGRectMake(60, 0, 50, 50);
    btnShare.backgroundColor = UIColor.yellowColor;
    btnShare.layer.cornerRadius = btnShare.frame.size.height / 2;
    btnShare.layer.masksToBounds = true;
    
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-share-24.png"];
    [btnShare setImage:btnFloat forState:UIControlStateNormal];
    [btnShare addTarget:self action:@selector(shareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:btnShare];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)batalAction:(id)sender {
    NSDictionary *passData = [[NSUserDefaults standardUserDefaults] objectForKey:@"passingData"];
    if([[passData objectForKey:@"gold_status"] isEqualToString:@"1"]){
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"passingData"];
        [super gotoGold];
    }else{
        [super backToRoot];
    }
}

- (void)okButtonTapped:(UIButton *)sender {
    //NSLog(@"Ok button was tapped: dismiss the view controller.");
    if (![isClicked isEqualToString:@"1"]) {
        isClicked = @"1";
        [self addFavButton];
        [self addShareButton];
    }
    else {
        isClicked = @"0";
        [btnFav removeFromSuperview];
        [btnShare removeFromSuperview];
        [lblFav removeFromSuperview];
        [lblShare removeFromSuperview];
    }
}

- (void)favButtonTapped:(UIButton *)sender {
    DLog(@"Favorite button was tapped: dismiss the view controller.");
}

- (void)shareButtonTapped:(UIButton *)sender {
    DLog(@"Share button was tapped: dismiss the view controller.");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DenomCell" forIndexPath:indexPath];
    
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    
    // Configure the cell...
    UILabel *lblDenom = (UILabel *)[cell viewWithTag:110];
    lblDenom.text = [data objectForKey:@"name"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [listData objectAtIndex:indexPath.row];
    NSString *value = [data objectForKey:@"id_denom"];

    NSURL *TheUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@menu_index_ios.html?vn=%@",API_URL,@VERSION_NAME]];
    NSString *response = [NSString stringWithContentsOfURL:TheUrl
                                                  encoding:NSASCIIStringEncoding
                                                     error:nil];
    
    NSData *dataMenuIndex = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:dataMenuIndex options:0 error:nil];
    
    NSInteger idx = [[json valueForKey:@"indexPayment"] integerValue];
    NSInteger idy = [[json valueForKey:@"indexInfaq"] integerValue];
    NSInteger idz = [[json valueForKey:@"nodeInfaq"] integerValue];
    
    NSArray *arrMenu = [dataManager.listMenu objectAtIndex:idx];
    NSDictionary *dictMenu = arrMenu[1];
    NSArray *actPembayaran = [dictMenu objectForKey:@"action"];
    NSArray *arrPembayaran = [actPembayaran objectAtIndex:idy];
    NSDictionary *dictPembayaran = arrPembayaran[1];
    NSDictionary *dictZakat = [dictPembayaran objectForKey:@"action"];
    NSArray *arrZakat = [dictZakat objectForKey:@"url_parm"];
    NSString *urlZakat = [arrZakat objectAtIndex:idz];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dt = [userDefault objectForKey:@"passingData"];
    NSString *payinfaq = [dt objectForKey:@"payinfaq"];
    NSString *idacc = [dt objectForKey:@"id_account"];
    NSString *trid =[dt objectForKey:@"transaction_id"];
    NSString *mid = [dt objectForKey:@"menu_id"];
    //NSString *code = [dt objectForKey:@"code"];
    NSString *code = @"8002";
    NSString *pin = [dt objectForKey:@"pin"];
//    NSString *cid = [dt objectForKey:@"customer_id"];
    NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *bhs = [dt objectForKey:@"language"];
    NSString *dvc = [dt objectForKey:@"device"];
    NSString *dvct = [dt objectForKey:@"device_type"];
    NSString *ipaddress = [dt objectForKey:@"ip_address"];
    NSString *dl = [dt objectForKey:@"date_local"];
    
    [dataManager.dataExtra addEntriesFromDictionary:@{@"amount":value}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"idvc":@"LD01"}];
    
    NSString *url = [NSString stringWithFormat:@"request_type=payment,transaction_id=%@,menu_id=%@,device=%@,device_type=%@,ip_address=%@,language=%@,date_local=%@,id_account=%@,code=%@,pin=%@,customer_id=%@,amount=%@,pay_infaq=0",trid,mid,dvc,dvct,ipaddress,bhs,dl,idacc,code,pin,cid,value];
    
    NSMutableDictionary *temp = [NSMutableDictionary dictionary];
    [temp setValue:payinfaq forKey:@"payinfaq"];
    [temp setValue:value forKey:@"amount"];
    [temp setValue:urlZakat forKey:@"url_parm"];
    [temp setValue:url forKey:@"url_post"];
    [temp setValue:idacc forKey:@"id_account"];
    [temp setValue:trid forKey:@"transaction_id"];
    [temp setValue:mid forKey:@"menu_id"];
    [temp setValue:code forKey:@"code"];
    [temp setValue:pin forKey:@"pin"];
    [temp setValue:cid forKey:@"customer_id"];
    [temp setValue:bhs forKey:@"language"];
    [temp setValue:dvc forKey:@"device"];
    [temp setValue:dvct forKey:@"device_type"];
    [temp setValue:ipaddress forKey:@"ip_address"];
    [temp setValue:dl forKey:@"date_local"];
    if([dt objectForKey:@"gold_status"]){
        [temp setValue:[dt objectForKey:@"gold_status"] forKey:@"gold_status"];
    }
    [userDefault setObject:temp forKey:@"infaqData"];
    [userDefault removeObjectForKey:@"passingData"];
    
    [super openTemplate:@"GENCF02" withData:[self jsonData]];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            NSArray *listArr = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in listArr){
                [newData addObject:@{@"name":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
            }
            listData = newData;
            
        }else{
            listData = (NSArray *)jsonObject;
        }
    }
    key = @"id_denom";
    mustAddToManager = true;
    [self.tblDenomInfaq reloadData];
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [BSMDelegate reloadApp];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
