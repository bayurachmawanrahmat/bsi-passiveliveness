//
//  RIViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 10/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "RIViewController.h"
#import "CellNotificationTableViewCell.h"
#import "PopUpLoginViewController.h"
#import "InboxHelper.h"
#import "Utility.h"
#import "QRPCF02ViewController.h"
#import "GENCF02ViewController.h"
#import "AKSCF01ViewController.h"
#import "DTNTF01ViewController.h"
#import "Utility.h"
#import "UIView+SimpleRipple.h"
#import "UIView+DCAnimationKit.h"
#import "AESCipher.h"

@interface RIViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, ConnectionDelegate, PopupLoginDelegate>{
    
    NSString *strCheckTab;
    NSUserDefaults *userDefault;
    NSString *lang;
    NSMutableArray *listNotification;
    InboxHelper *inboxHelper;
    NSString *transactId, *trxRef;
    CGFloat cellSizeWidth;
    NSInteger indexRow;
    AESCipher *aesChiper;
    UIButton *btnChecked;
    NSMutableArray *listDelete;
    
    
    bool stateOpnDel;
    bool stateAllMarked;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTab;

@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;

@property (weak, nonatomic) IBOutlet UIView *vwTabOne;
@property (weak, nonatomic) IBOutlet UIView *vwTabTwo;
@property (weak, nonatomic) IBOutlet UIView *vwTabThree;

@property (weak, nonatomic) IBOutlet UIView *vwLineTabOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTabOne;

@property (weak, nonatomic) IBOutlet UIView *vwLineTabTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnTabTwo;

@property (weak, nonatomic) IBOutlet UIView *vwLineTabThree;
@property (weak, nonatomic) IBOutlet UIButton *btnTabThree;

@property (weak, nonatomic) IBOutlet UITableView *tableNotif;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *vwOpnDelete;

@property(nonatomic,strong) UIView *vwMarked;
@property(nonatomic,strong) UIView *vwBtnDelete;




- (IBAction)actionTabOne:(id)sender;
- (IBAction)actionTabTwo:(id)sender;
- (IBAction)actionTabThree:(id)sender;
- (IBAction)actionOpnDelete:(id)sender;
- (IBAction)actiionDwnOpnDel:(id)sender forEvent:(UIEvent *)event;


@end

@implementation RIViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.vwMarked = [[UIView alloc]initWithFrame:CGRectMake(0, self.vwTitle.frame.origin.y + self.vwTitle.frame.size.height, SCREEN_WIDTH, 40)];
    [self.vwMarked setBackgroundColor:UIColorFromRGB(0xEBEBEB)];
    
    if ([Utility isDeviceHaveNotch]) {
         self.vwBtnDelete = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - BOTTOM_NAV - 80 - 16, SCREEN_WIDTH, 50)];
    }else{
      self.vwBtnDelete = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - BOTTOM_NAV - 50, SCREEN_WIDTH, 50)];
    }
    
    [self.view addSubview:self.vwMarked];
    [self.view addSubview:self.vwBtnDelete];
    
    
    [self.vwMarked setHidden:true];
    [self.vwBtnDelete setHidden:true];
    
    [self setupLayout];
    
    stateOpnDel = false;
    stateAllMarked = false;

    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwBtnDelete.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.vwOpnDelete.backgroundColor = UIColorFromRGB(const_color_topbar);
    

    [self.btnTabOne setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    self.btnTabOne.titleLabel.font = [UIFont fontWithName:const_font_name3 size:14];
    self.vwLineTabOne.backgroundColor = UIColorFromRGB(const_color_secondary);
    

    [self.btnTabTwo setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    self.btnTabTwo.titleLabel.font = [UIFont fontWithName:const_font_name3 size:14];
    self.vwLineTabTwo.backgroundColor = UIColorFromRGB(const_color_secondary);
    

    [self.btnTabThree setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
    self.btnTabThree.titleLabel.font = [UIFont fontWithName:const_font_name3 size:14];
    self.vwLineTabThree.backgroundColor = UIColorFromRGB(const_color_secondary);
    
    [self.btnTabOne.superview setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    [self.btnTabTwo.superview setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    [self.btnTabThree.superview setBackgroundColor:UIColorFromRGB(const_color_topbar)];
    
    _tableNotif.delegate = self;
    _tableNotif.dataSource = self;
    [self.tableNotif setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
//   _tableNotif.allowsSelection = NO;
    
    inboxHelper = [[InboxHelper alloc]init];
    aesChiper = [[AESCipher alloc]init];
    listNotification = [[NSMutableArray alloc]init];
    listDelete = [[NSMutableArray alloc]init];
    
    UIButton *btnDeleteAll  = [UIButton buttonWithType:UIButtonTypeSystem];
//    btnDeleteAll.frame = CGRectMake(16, 0, self.vwBtnDelete.frame.size.width - 32, self.vwBtnDelete.frame.size.height);
    btnDeleteAll.frame = CGRectMake(16, 0, self.vwBtnDelete.frame.size.width - 32, 40);

    NSString *strMarker;
    
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault valueForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
        [_btnTabOne setTitle:@"RESI" forState:UIControlStateNormal];
        [_btnTabTwo setTitle:@"INFO" forState:UIControlStateNormal];
        [_btnTabThree setTitle:@"QRIS" forState:UIControlStateNormal];
        
        [btnDeleteAll setTitle:@"Hapus" forState:UIControlStateNormal];
        strMarker = @"Seleksi Semua";
    }else{
        [_btnTabOne setTitle:@"RECEIPT" forState:UIControlStateNormal];
        [_btnTabTwo setTitle:@"INFO" forState:UIControlStateNormal];
        [_btnTabThree setTitle:@"QRIS" forState:UIControlStateNormal];

        [btnDeleteAll setTitle:@"Delete" forState:UIControlStateNormal];
        strMarker = @"All Selection";
    }
    
    [btnDeleteAll addTarget:self action:@selector(actionDelete:) forControlEvents:UIControlEventTouchUpInside];
    btnDeleteAll.layer.cornerRadius = 20;
    btnDeleteAll.layer.masksToBounds = true;
    [btnDeleteAll setTitleColor:UIColorFromRGB(const_color_topbar) forState:UIControlStateNormal];
    [btnDeleteAll setBackgroundColor:UIColorFromRGB(const_color_red)];
    
    btnChecked = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChecked.frame = CGRectMake(8,0,40,40);
    btnChecked.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    [btnChecked setImage:[UIImage imageNamed:@"blank_check.png"] forState:UIControlStateNormal];
    [btnChecked setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
    [btnChecked addTarget:self action:@selector(actionMarked:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lblMarked =  [[UILabel alloc] initWithFrame:CGRectMake(btnChecked.frame.origin.x+btnChecked.frame.size.width,0, self.vwMarked.frame.size.width - 16, self.vwMarked.frame.size.height)];
    [lblMarked setText:strMarker];
    [lblMarked setFont:[UIFont fontWithName:const_font_name1 size:13]];
    
    [self.vwMarked addSubview:btnChecked];
    [self.vwMarked addSubview:lblMarked];
    [self.vwBtnDelete addSubview:btnDeleteAll];
    
    
    if([dataManager.dataExtra objectForKey:@"inboxparam"]){
        [self changeLineIndicator:[dataManager.dataExtra valueForKey:@"inboxparam"]];
        [dataManager.dataExtra removeObjectForKey:@"inboxparam"];
    }else if([[dataManager.dataExtra objectForKey:@"inboxTapped"]isEqualToString:@"tapped"]){
        [self changeLineIndicator:@"0"];
    }else{
        [self changeLineIndicator:@"1"];
    }
}

-(void) getMSISDN{
    NSString *strUrl = [NSString stringWithFormat:@"request_type=get_msisdn,customer_id"];

    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

-(void) setupLayout{
    
    CGRect frmVwTitle = _vwTitle.frame;
    CGRect frmLblTitle = _lblTitle.frame;
    CGRect frmVwOpnDel = _vwOpnDelete.frame;
    
    CGRect frmVwTab = _vwTab.frame;
    CGRect frmTableNotif = _tableNotif.frame;
    
    CGRect frmVwScroll = _vwScroll.frame;
    
    CGRect frmVwTabOne = _vwTabOne.frame;
    CGRect frmVwTabTwo = _vwTabTwo.frame;
    CGRect frmVwTabThree = _vwTabThree.frame;
    
    CGRect frmVwLineOne = _vwLineTabTwo.frame;
    CGRect frmVwLineTwo = _vwLineTabTwo.frame;
    CGRect frmVwLineThree = _vwLineTabThree.frame;
    
    CGRect frmBtnTabOne = _btnTabOne.frame;
    CGRect frmBtnTabTwo = _btnTabTwo.frame;
    CGRect frmBtnTabThree = _btnTabThree.frame;
    
    
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.origin.x = 0;
    frmVwTitle.size.width = SCREEN_WIDTH;
    frmVwTitle.size.height = 50;
    
    frmLblTitle.origin.y = 5;
    frmLblTitle.origin.x = 8;
    frmLblTitle.size.width = frmVwTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmVwOpnDel.origin.y = 0;
    frmVwOpnDel.origin.x = SCREEN_WIDTH - frmVwOpnDel.size.width;
    
    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmVwTab.origin.x = 0;
    frmVwTab.origin.y = 0;
    frmVwScroll.size.height = 50;
//    frmVwTab.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
//    frmVwTab.origin.y = TOP_NAV;
    frmVwScroll.size.width = frmVwTitle.size.width;
    
    frmTableNotif.origin.y = frmVwScroll.origin.y + frmVwScroll.size.height;
    frmTableNotif.origin.x = 0;
    frmTableNotif.size.width = frmVwTitle.size.width;
    frmTableNotif.size.height = SCREEN_HEIGHT - (frmTableNotif.origin.y + BOTTOM_NAV);
    
    if ([Utility isDeviceHaveNotch]) {
        frmTableNotif.size.height = SCREEN_HEIGHT - (frmTableNotif.origin.y + BOTTOM_NAV) - 50;
    }
    
    
    frmVwTabOne.origin.x = 0;
    frmVwTabOne.size.width = 150;
    frmVwTabOne.size.height = 50;
    
    frmVwTabTwo.origin.x = frmVwTabOne.origin.x + frmVwTabOne.size.width;
    frmVwTabTwo.size.width = frmVwTabOne.size.width;
    frmVwTabTwo.size.height = 50;
    
    frmVwTabThree.origin.x = frmVwTabTwo.origin.x + frmVwTabTwo.size.width;
    frmVwTabThree.size.width = frmVwTabTwo.size.width;
    frmVwTabThree.size.height = 50;

    frmBtnTabOne.size.width = frmVwTabOne.size.width;
    frmVwLineOne.size.width = frmVwTabOne.size.width;
    
    frmBtnTabTwo.size.width = frmVwTabTwo.size.width;
    frmVwLineTwo.size.width  = frmVwTabTwo.size.width;
    
    frmBtnTabThree.size.width = frmVwTabThree.size.width;
    frmVwLineThree.size.width = frmVwTabThree.size.width;
    
    frmVwTab.size.width = frmVwTabOne.size.width * 3;
    frmVwTab.size.height = frmVwScroll.size.height;
    
//    frmVwScroll.size.height = frmVwTabOne.size.height;
//    frmVwScroll.origin.x = 0;
//    frmVwScroll.origin.y =
    
    
    _vwTitle.frame = frmVwTitle;
    _lblTitle.frame = frmLblTitle;
    _vwOpnDelete.frame = frmVwOpnDel;
    _vwTab.frame = frmVwTab;
    _tableNotif.frame = frmTableNotif;
    
    _vwTabOne.frame = frmVwTabTwo;
    _vwTabTwo.frame = frmVwTabOne;
    _vwTabThree.frame = frmVwTabThree;
    
    _vwLineTabTwo.frame = frmVwLineOne;
    _vwLineTabOne.frame = frmVwLineTwo;
    _vwLineTabThree.frame = frmVwLineThree;
    
    _btnTabOne.frame = frmBtnTabTwo;
    _btnTabTwo.frame = frmBtnTabOne;
    _btnTabThree.frame = frmBtnTabThree;
    
    
    
    _vwScroll.frame = frmVwScroll;
    [_vwScroll setContentSize:CGSizeMake(frmVwTab.size.width, frmVwTab.size.height)];

//    [_vwScroll setContentSize:frmvw];
    
//    _vwTabOne.frame = frmVwTabOne;
//    _vwTabTwo.frame = frmVwTabTwo;
    
//    _vwLineTabTwo.frame = frmVwLineOne;
//    _vwLineTabTwo.frame = frmVwLineTwo;
    
//    _btnTabOne.frame = frmBtnTabOne;
//    _btnTabTwo.frame = frmBtnTabTwo;
    
}

- (IBAction)actionTabOne:(id)sender {
    [self changeLineIndicator:@"0"];
}

- (IBAction)actionTabTwo:(id)sender {
    [self changeLineIndicator:@"1"];
}

- (IBAction)actionOpnDelete:(id)sender {
    if (listNotification.count >0) {
        [self setVisibleVwDeleteAll];
        [self.vwBtnDelete pulse:NULL];
        [_tableNotif reloadData];
        
    }
    
}

- (IBAction)actionTabThree:(id)sender {
    [self changeLineIndicator:@"2"];
}

-(void) setVisibleVwDeleteAll{
    
        CGRect frmVwTab = self.vwScroll.frame;
        CGRect frmTblNotif = self.tableNotif.frame;
        CGRect frmVwMarked = self.vwMarked.frame;
        CGRect frmVwBtn = self.vwBtnDelete.frame;
        
        if (stateOpnDel) {
            [self.vwMarked setHidden:true];
            [self.vwBtnDelete setHidden:true];
            
            frmTblNotif.origin.y = frmVwTab.origin.y + frmVwTab.size.height;
            frmTblNotif.origin.x = 0;
            frmTblNotif.size.width = SCREEN_WIDTH;
            frmTblNotif.size.height = SCREEN_HEIGHT - (frmTblNotif.origin.y + BOTTOM_NAV);
            
            if ([Utility isDeviceHaveNotch]) {
                frmTblNotif.size.height = SCREEN_HEIGHT - (frmTblNotif.origin.y + BOTTOM_NAV) - 50;
            }
            
            stateOpnDel = false;
            stateAllMarked = false;
            [btnChecked setSelected:false];
        }else{
            [self.vwMarked setHidden:false];
            [self.vwBtnDelete setHidden:false];
            
            frmVwMarked.origin.y = frmVwTab.origin.y + frmVwTab.size.height;
            frmVwMarked.origin.x = 0;
            
            frmTblNotif.origin.y = frmVwMarked.origin.y + frmVwMarked.size.height;
            frmTblNotif.size.height = frmVwBtn.origin.y - frmTblNotif.origin.y;
            
            stateOpnDel = true;
            
        }
        
        self.vwScroll.frame = frmVwTab;
        self.tableNotif.frame = frmTblNotif;
        self.vwMarked.frame = frmVwMarked;
        self.vwBtnDelete.frame = frmVwBtn;
        
}



- (IBAction)actiionDwnOpnDel:(id)sender forEvent:(UIEvent *)event{
    UITouch *touch = [[event touchesForView:sender] anyObject];
    CGPoint origin = [touch locationInView:sender];
    float radius = self.vwOpnDelete.frame.size.height;
    float duration = 1.0f;
    float fadeAfter = duration * 0.75f;
    
    [sender rippleStartingAt:origin withColor:[UIColor colorWithWhite:0.0f alpha:0.20f] duration:duration radius:radius fadeAfter:fadeAfter];
}

-(void) changeLineIndicator: (NSString *) stateTab{
    if ([stateTab isEqualToString:@"0"]) {
        [_vwLineTabTwo setHidden:true];
        [_vwLineTabOne setHidden:false];
        [_vwLineTabThree setHidden:true];
        
        [_btnTabOne setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
        [_btnTabTwo setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
        [_btnTabThree setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];

    }else if([stateTab isEqualToString:@"1"]){
        [_vwLineTabOne setHidden:true];
        [_vwLineTabTwo setHidden:false];
        [_vwLineTabThree setHidden:true];
        
        [_btnTabTwo setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
        [_btnTabOne setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
        [_btnTabThree setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
        
    }else{
        [_vwLineTabOne setHidden:true];
        [_vwLineTabTwo setHidden:true];
        [_vwLineTabThree setHidden:false];
        
        [_btnTabThree setTitleColor:UIColorFromRGB(const_color_secondary) forState:UIControlStateNormal];
        [_btnTabTwo setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
        [_btnTabOne setTitleColor:UIColorFromRGB(const_color_gray) forState:UIControlStateNormal];
    }
    
    [btnChecked setSelected:false];
    stateAllMarked = false;
    strCheckTab = stateTab;
    if (listDelete.count>0) {
        [listDelete removeAllObjects];
    }
    [self callData:strCheckTab];
}

-(void) callData : (NSString *) stateTab{
    NSString *strUrl;
    
    if ([stateTab isEqualToString:@"0"]) {
        [self cekLogin];
//        NSLog(@"%@", dataManager.pinNumber);
//        if(dataManager.pinNumber == nil){
//            [self checkingPin];
//            [dataManager.dataExtra setValue:@"tapped" forKey:@"inboxTapped"];
//        }else{
//            strUrl = [NSString stringWithFormat:@"%@,pin",[self.jsonData valueForKey:@"url_parm"]];
//            Connection *conn = [[Connection alloc]initWithDelegate:self];
//            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
//        }
//        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    }else{
        /** msgtype
            - Qris -> 1 , promo -> 2, sanggahan -> 4, schedule -> 5, autosave -> 6
         */
        if([stateTab isEqualToString:@"2"]){
            [dataManager.dataExtra setValue:@"1" forKey:@"msgtype"];
        }else{
            NSMutableArray *msgTypeLst = [[NSMutableArray alloc]init];
            [msgTypeLst addObject:@"2"];
            [msgTypeLst addObject:@"3"];
            [msgTypeLst addObject:@"4"];
            [msgTypeLst addObject:@"5"];
            [msgTypeLst addObject:@"6"];
            [msgTypeLst addObject:@"7"];
            [msgTypeLst addObject:@"8"];
            [msgTypeLst addObject:@"9"];
            [msgTypeLst addObject:@"10"];
            [dataManager.dataExtra setValue:[msgTypeLst componentsJoinedByString:@","] forKey:@"msgtype"];
        }
        userDefault = [NSUserDefaults standardUserDefaults];
        
//        if([userDefault objectForKey:@"msisdn"] != nil && ![[userDefault valueForKey:@"msisdn"] isEqualToString:@""]){
//            [self requestListNotif:[userDefault valueForKey:@"msisdn"]];
//        }else if([userDefault objectForKey:@"mobilenumber"] != nil && ![[userDefault valueForKey:@"mobilenumber"] isEqualToString:@""]){
//            [self requestListNotif:[userDefault valueForKey:@"mobilenumber"]];
//        }else{
//            [self getMSISDN];
//        }
        
        if([NSUserdefaultsAes getValueForKey:@"msisdn"] != nil && ![[NSUserdefaultsAes getValueForKey:@"msisdn"] isEqualToString:@""]){
            [self requestListNotif:[NSUserdefaultsAes getValueForKey:@"msisdn"]];
        }else if([userDefault objectForKey:@"mobilenumber"] != nil && ![[userDefault valueForKey:@"mobilenumber"] isEqualToString:@""]){
            [self requestListNotif:[userDefault valueForKey:@"mobilenumber"]];
        }else{
            [self getMSISDN];
        }

    }
}

- (void)requestListNotif :(NSString*) msisdn{
    [dataManager.dataExtra setValue:msisdn forKey:@"msisdn"];
    NSString *strUrl = [NSString stringWithFormat:@"request_type=list_notif2,msisdn,msgtype,customer_id"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

- (void) updateResi{
    NSString *strUrl = [NSString stringWithFormat:@"request_type=list_update_receipt,customer_id"];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

- (void) cekLogin{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    NSString *flagCID = [userDefault objectForKey:@"customer_id"];
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *hasLogin = [userDefault objectForKey:@"hasLogin"];
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {
        DLog(@"Customer ID belum diaktifkan.");
        NSString *valMsg = @"";
        if([Utility isLanguageID]){
            valMsg = @"Lakukan aktivasi terlebih dahulu.\nGunakan nomor mobile yang sudah terdaftar dan kode aktivasi yang didapatkan melalui sms.";
        }else{
            valMsg = @"Please activate first.\nUse the registered phone number and activation code obtained via sms.";
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:valMsg preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//            [self backToR];
        }]];
        
    } else {
        if ([hasLogin isEqualToString:@"NO"] || hasLogin == nil) {
            [self.tabBarController setSelectedIndex:0];
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PopUpLoginViewController *popLogin = [ui instantiateViewControllerWithIdentifier:@"PopupLogin"];
            [popLogin setDelegate:self];
            [popLogin setIdentfier:@"Receipt View Inbox"];
            [self presentViewController:popLogin animated:YES completion:nil];
        }
        else {
            DLog(@"%@", dataManager.pinNumber);
            if(dataManager.pinNumber == nil){
                [self checkingPin];
                [dataManager.dataExtra setValue:@"tapped" forKey:@"inboxTapped"];
            }else{
                NSString *strUrl = [NSString stringWithFormat:@"%@,pin",[self.jsonData valueForKey:@"url_parm"]];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
            }
        }
    }
}

- (void)loginDoneState:(NSString *)identifier{
    DLog(@"%@",identifier);
    DLog(@"%@", dataManager.pinNumber);
    if(dataManager.pinNumber == nil){
        [self checkingPin];
        [dataManager.dataExtra setValue:@"tapped" forKey:@"inboxTapped"];
    }else{
        NSString *strUrl = [NSString stringWithFormat:@"%@,pin",[self.jsonData valueForKey:@"url_parm"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (void) checkingPin{
    [[DataManager sharedManager]resetObjectData];
    NSArray *temp = [[DataManager sharedManager] getJSONData:5];
    if(temp != nil){
        NSDictionary *object = [temp objectAtIndex:1];
        DataManager *dataManager = [DataManager sharedManager];
        [dataManager setAction:[object objectForKey:@"action"] andMenuId:[temp objectAtIndex:0]];
        dataManager.currentPosition = 0;
        NSDictionary *tempData = [dataManager getObjectData];
        TemplateViewController *templateViews = [self.storyboard instantiateViewControllerWithIdentifier:[tempData valueForKey:@"template"] ];
        [templateViews setJsonData:tempData];
        bool stateAcepted = [Utility vcNotifConnection: [tempData valueForKey:@"template"]];
        if (stateAcepted) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self changeLineIndicator:self->strCheckTab];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            [self.navigationController pushViewController:templateViews animated:YES];
            
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MESSAGE_INDICATOR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self changeLineIndicator:self->strCheckTab];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"]isEqualToString: @"00"]) {
            if([requestType isEqualToString:@"get_msisdn"]){
                NSDictionary *dict  = [NSJSONSerialization
                JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding]
                options:NSJSONReadingAllowFragments
                                       error:nil];

                NSString *msisdn = [dict objectForKey:@"msisdn"];
//                [userDefault setValue:msisdn forKey:@"msisdn"];
//                [userDefault setValue:msisdn forKey:@"mobilenumber"];

                [NSUserdefaultsAes setObject:msisdn forKey:@"msisdn"];
//                [userDefault setValue:msisdn forKey:@"mobilenumber"];
                [self requestListNotif:msisdn];
            }
            if (![requestType isEqualToString:@"check_notif"]) {
                if([strCheckTab isEqualToString:@"0"]){
                    
                    listNotification = (NSMutableArray *) [inboxHelper listDataInbox];
                    DLog(@"%@", listNotification);
                    
                    if([listNotification count] == 0){
                        NSString *msg;
                        if ([lang isEqualToString:@"id"]) {
                            msg = @"Daftar kotak masuk kosong";
                        }else{
                            msg = @"Inbox list is empty";
                        }
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                    [_tableNotif reloadData];
                }else{
                    if([requestType isEqualToString:@"list_update_receipt"]){
                        DLog(@"%@", jsonObject);
//                        NSString *gid = @"";
                        NSMutableArray *listGid = [[NSMutableArray alloc]init];

                        NSArray *dataReceipt = [jsonObject valueForKey:@"response"];
                        for(int i = 0; i < dataReceipt.count; i++){
                            NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"footer_st"] forKey:@"footer_msg"];
//                            [dictList setValue:[dataReceipt[i] objectForKey:@"id_message"] forKey:@"footer_msg"];
//                            [dictList setValue:[dataReceipt[i] objectForKey:@"id_ni"] forKey:@""];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"jenis_st"] forKey:@"jenis"];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"marked_st"] forKey:@"marked"];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"msg_st"] forKey:@"msg"];
//                            [dictList setValue:[dataReceipt[i] objectForKey:@"msisdn"] forKey:@"footer_msg"];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"template_st"] forKey:@"template"];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"time_st"] forKey:@"time"];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"title_st"] forKey:@"title"];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"trid_st"] forKey:@"transaction_id"];
                            [dictList setValue:[dataReceipt[i] objectForKey:@"noref_st"] forKey:@"trxref"];
                            
                            [listGid addObject:[dataReceipt[i] objectForKey:@"id_ni"]];
                            
//                            if(i == 0){
//                                gid = [NSString stringWithFormat:@"%@", [dataReceipt[i] objectForKey:@"id_ni"]];
//                            }else{
//                                gid = [NSString stringWithFormat:@"%@,%@",gid, [dataReceipt[i] objectForKey:@"id_ni"]];
//                            }
                            
                            InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
                            [inboxHeldper insertDataInbox:dictList];
                        }
                        
                        NSString *strUrl = [NSString stringWithFormat:@"request_type=update_receipt,gid"];
                        [dataManager.dataExtra setValue:[listGid componentsJoinedByString:@","] forKey:@"gid"];

                        Connection *conn = [[Connection alloc]initWithDelegate:self];
                        [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                        
                    }
                    if ([requestType isEqualToString:@"list_notif"]) {
                        NSArray *dataNotif = [jsonObject valueForKey:@"response"];
                        listNotification = [dataNotif copy];
                        [_tableNotif reloadData];
                        [self updateResi];
                        
                    }
                    else if ([requestType isEqualToString:@"list_notif2"]) {
                        NSArray *dataNotif = [jsonObject valueForKey:@"response"];
                        listNotification = [dataNotif copy];
                        [_tableNotif reloadData];
                        [self updateResi];
                        
                    }
                    else if([requestType isEqualToString:@"delete_notif"]){
                        //                    [self callData:strCheckTab];
                         NSMutableArray *copyListNotif = [listNotification mutableCopy];
                         [copyListNotif removeObjectAtIndex:indexRow];
                        listNotification = [copyListNotif mutableCopy];
                        [self.tableNotif reloadData];
                    }else if([requestType isEqualToString:@"update_notif"]){
                        TemplateViewController *templateView = [self routeTemplateController:@"DTNTF01"];
                        [self.navigationController pushViewController:templateView animated:YES];
                        
                    }else if([requestType isEqualToString:@"delete_multinotif"]){
                        
                        for (int i=0; i<listDelete.count; i++) {
                            NSString *strIdMsg = [listDelete objectAtIndex:i];
                            if (strIdMsg != nil) {
                                NSMutableArray *copyListNotif = [listNotification mutableCopy];
                                for (int x=0; x< copyListNotif.count; x++){
                                    NSDictionary *notifDict = [copyListNotif objectAtIndex:x];
                                    if ([strIdMsg isEqualToString:[notifDict valueForKey:@"mid"]] ) {
                                        [copyListNotif removeObjectAtIndex:x];
                                        break;
                                    }
                                }
                                listNotification = [copyListNotif mutableCopy];
                                
                            }
                            
                        }
                        
                        if (listNotification == nil || listNotification.count == 0) {
                            stateOpnDel = true;
                            [self setVisibleVwDeleteAll];
                        }
                        [listDelete removeAllObjects];
                        [btnChecked setSelected:false];
                        [self callData:strCheckTab];
                        [_tableNotif reloadData];
                    }
                }
                
            }
            
        }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"01"]){
            listNotification = [NSMutableArray new];
            [_tableNotif reloadData];
            [self updateResi];

            NSString *msg;
            if ([lang isEqualToString:@"id"]) {
                if([strCheckTab isEqualToString:@"0"]){
                    msg = @"Daftar Resi kosong";
                }else if([strCheckTab isEqualToString:@"1"]){
                    msg = @"Daftar Info kosong";
                }else{
                    msg = @"Daftar QRIS kosong";
                }
            }else{
                if([strCheckTab isEqualToString:@"0"]){
                    msg = @"Receipt list is empty";
                }else if([strCheckTab isEqualToString:@"1"]){
                    msg = @"List info is empty";
                }else{
                    msg = @"List QRIS is empty";
                }
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"02"]){
            //do nothing
        }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"99"]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n",[jsonObject objectForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n",[jsonObject objectForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 100) {
        [super openActivation];
    }else if(alertView.tag == 413){
        
        if (buttonIndex == 1) {
            if ([strCheckTab isEqualToString:@"0"]) {
                if([inboxHelper removeDataInbox:transactId trxRef:trxRef]){
                    [listNotification removeObjectAtIndex:indexRow];
                    [self syncBackup];
                    //[self.tableNotif reloadData];
                }
            }else{
                NSString *strUrl = [NSString stringWithFormat:@"request_type=delete_notif,customer_id,mid,nid"];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
            }
        }
        
        
    }
    else if(alertView.tag == 414){
        NSLog(@"No Data Inbox");
    }
    else if(alertView.tag == 415){
        if (buttonIndex == 1) {
            if ([strCheckTab isEqualToString:@"0"]) {
                NSMutableArray *listTransactId = [[NSMutableArray alloc]init];
                NSMutableArray *listTrxRef = [[NSMutableArray alloc]init];
                for (int i=0; i<listDelete.count; i++) {
                    NSString *strIdMsg = [listDelete objectAtIndex:i];
                    NSArray *dataId = [strIdMsg componentsSeparatedByString:@"|"];
                    if (dataId.count > 0) {
                        [listTransactId addObject:[aesChiper aesEncryptString:[dataId objectAtIndex:0]]];
                        [listTrxRef addObject:[aesChiper aesEncryptString:[dataId objectAtIndex:1]]];
                        for (int x=0; x< listNotification.count; x++){
                            NSDictionary *notifDict = [listNotification objectAtIndex:x];
                            if ([[dataId objectAtIndex:0] isEqualToString:[notifDict valueForKey:@"transaction_id"]] && [[dataId objectAtIndex:1] isEqualToString:[notifDict valueForKey:@"trxref"]] ) {
                                [listNotification removeObjectAtIndex:x];
                                break;
                            }
                        }
                        
                    }
                }
                NSLog(@"%@", listTransactId);
                NSLog(@"%@", listTrxRef);
                
                if([inboxHelper removeMultiDataInbox:listTransactId trxRef:listTrxRef]){
                    //[_tableNotif reloadData];
                    [self syncBackup];
                    if (listNotification == nil || listNotification.count == 0) {
                        stateOpnDel = true;
                        [self setVisibleVwDeleteAll];
                    }
                    [listDelete removeAllObjects];
                    [btnChecked setSelected:false];
                }
                
                
                
            }else{
                NSString *strUrl = [NSString stringWithFormat:@"request_type=delete_multinotif,customer_id,gmid,flag=1"];
                [dataManager.dataExtra setValue:[listDelete componentsJoinedByString:@","] forKey:@"gmid"];
                
               
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
            }
        }
        
    }
    
    else if(alertView.tag == 202){
        NSLog(@"Sukses Networking");
    }
    else{
        [self backToRoot];
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listNotification.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellNotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[CellNotificationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    [cell.iconNotif setImage:[UIImage imageNamed:@"ic_list_kotakmasuk"]];
//    [cell.btnTblCheck setImage:[UIImage imageNamed:@"blank_check.png"] forState:UIControlStateNormal];
//    [cell.btnTblCheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
    
    cell.btnTblCheck.tag = indexPath.row;
    
    NSDictionary *dataDict = [listNotification objectAtIndex:indexPath.row];
    NSString *strMsgId;
    NSString *marked;
    if ([strCheckTab isEqualToString:@"0"]) {
        NSString *nMsg;
        if ([lang isEqualToString:@"id"]){
            nMsg = @"Nomor Referensi:";
        }else{
            nMsg = @"Reference Number:";
        }
        [cell.lblTitleNotif setText: [dataDict valueForKey:@"title"]];
        [cell.lblDescNotif setText:[NSString stringWithFormat:@"%@ %@ %@", [dataDict valueForKey:@"time"], nMsg, [dataDict valueForKey:@"trxref"]]];
        marked = [dataDict valueForKey:@"marked"];
        
        strMsgId = [NSString stringWithFormat:@"%@|%@", [dataDict valueForKey:@"transaction_id"], [dataDict valueForKey:@"trxref"]];
        
    }else{
        [cell.lblTitleNotif setText:[dataDict valueForKey:@"title"]];
        [cell.lblDescNotif setText:[dataDict valueForKey:@"msg_date"]];
        marked = [dataDict valueForKey:@"read_flag"];
        
//        strMsgId = [dataDict valueForKey:@"mid"];
        strMsgId = [dataDict valueForKey:@"nid"];
    }
    
    if ([marked isEqualToString:@"0"]) {
        [cell.lblTitleNotif setTextColor:[UIColor blackColor]];
        [cell.lblDescNotif setTextColor:[UIColor blackColor]];
    }else{
        [cell.lblTitleNotif setTextColor:[UIColor grayColor]];
        [cell.lblDescNotif setTextColor:[UIColor grayColor]];
    }
    
    NSString *textDesc, *textTitle;
    textTitle = cell.lblTitleNotif.text;
    textDesc = cell.lblDescNotif.text;
    
    cell.lblTitleNotif.font = [UIFont fontWithName:const_font_name3 size:14];
    cell.lblDescNotif.font = [UIFont fontWithName:const_font_name1 size:14];

    
    CGRect frmBtnTblCheck = cell.btnTblCheck.frame;
    CGRect frmTitleNotif = cell.lblTitleNotif.frame;
    CGRect frmDescNotif = cell.lblDescNotif.frame;
    CGRect frmImg = cell.iconNotif.frame;
    
    if (stateOpnDel) {
        [cell.btnTblCheck setHidden:false];
        frmImg.origin.x = frmBtnTblCheck.origin.x + frmBtnTblCheck.size.width;
        if (stateAllMarked) {
            [cell.btnTblCheck setSelected:true];
        }else{
             [cell.btnTblCheck setSelected:false];
        }
        
    }else{
        [cell.btnTblCheck setHidden:true];
        frmImg.origin.x = 8;
        if (stateAllMarked) {
            [cell.btnTblCheck setSelected:true];
        }else{
            [cell.btnTblCheck setSelected:false];
        }
    }
    
    if ([listDelete containsObject:strMsgId]) {
        [cell.btnTblCheck setSelected:true];
    }else{
        [cell.btnTblCheck setSelected:false];
    }
    
    
    cellSizeWidth = cell.frame.size.width - frmImg.origin.x - 30;
    CGSize constraint;
    if (IPHONE_5) {
        constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    }else{
        constraint = CGSizeMake(cellSizeWidth, 20000.0f);
        if (IPHONE_8 || IPHONE_8P) {
            constraint = CGSizeMake(cellSizeWidth - 20, 20000.0f);
        }
    }
    CGSize sizeTitle = [textTitle sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGSize sizeDesc = [textDesc sizeWithFont:[UIFont systemFontOfSize:12.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    frmTitleNotif.origin.x = frmImg.origin.x + frmImg.size.width + 8;
    frmTitleNotif.origin.y = frmImg.origin.y;
    frmTitleNotif.size.height = sizeTitle.height;
    frmTitleNotif.size.width = cell.frame.size.width - frmTitleNotif.origin.x - 8;
    
    frmDescNotif.origin.x  = frmTitleNotif.origin.x;
    
    frmDescNotif.origin.y = frmTitleNotif.origin.y + frmTitleNotif.size.height;
    frmDescNotif.size.height = sizeDesc.height;
    frmDescNotif.size.width = frmTitleNotif.size.width;
    
    cell.btnTblCheck.frame = frmBtnTblCheck;
    cell.iconNotif.frame = frmImg;
    cell.lblTitleNotif.frame = frmTitleNotif;
    cell.lblDescNotif.frame = frmDescNotif;
    cell.lblTitleNotif.numberOfLines = 0;
    cell.lblDescNotif.numberOfLines = 0;
   
    [cell.btnTblCheck addTarget:self action:@selector(actionBtnTblChecked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnTblCheck setImage:[UIImage imageNamed:@"blank_check.png"] forState:UIControlStateNormal];
    [cell.btnTblCheck setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (listNotification.count>1 || listNotification != nil) {
        NSString *text = @"";
        NSDictionary *data = [listNotification objectAtIndex:indexPath.row];
         text = [NSString stringWithFormat:@"%@\n%@ %@", [data valueForKey:@"title"],[data valueForKey:@"time"], [data valueForKey:@"trxref"]];
         CGSize constraint;
         if (IPHONE_5) {
             constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
         }else{
             constraint = CGSizeMake(cellSizeWidth, 20000.0f);
         }
         
         CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:15.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
         CGFloat height = MAX(size.height, 33.0);
         CGFloat asumseHeight = height;
         if (stateOpnDel) {
             asumseHeight = height + 10;
         }
         if (IPHONE_5) {
             return asumseHeight + 30;
         }else{
             return asumseHeight + 20;
         }
    }else{
        return 60;
    }
   
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *dataDict = [listNotification objectAtIndex:indexPath.row];
    
    CellNotificationTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell.lblTitleNotif setTextColor:[UIColor grayColor]];
    [cell.lblDescNotif setTextColor:[UIColor grayColor]];
    
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    [newDict addEntriesFromDictionary:dataDict];
    
    if ([strCheckTab isEqualToString:@"0"]) {
        [newDict setObject:@"1" forKey:@"marked"];
        [listNotification replaceObjectAtIndex:indexPath.row withObject:newDict];
        
        if([dataDict valueForKey:@"template"]){
            NSString *template = [dataDict valueForKey:@"template"];
            if ([template isEqualToString:@"GENCF02"]) {
                NSDictionary *dataSend = [inboxHelper readDataInbox:[dataDict valueForKey:@"transaction_id"] trxrf:[dataDict valueForKey:@"trxref"]];
                
//                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
//                //                    DTNTF01ViewController *viewCont = (DTNTF01ViewController *) templateView;
//                [self.navigationController pushViewController:templateView animated:YES];
                
                //minjem this line for detail notification
                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"GENCF02"];
                GENCF02ViewController *viewCont = (GENCF02ViewController *) templateView;
                [viewCont setDataLocal:dataSend];
                [viewCont setFromRecipt:YES];
                [self.navigationController pushViewController:templateView animated:YES];
                
                
            }else if([template isEqualToString:@"QRPCF02"]){
                NSDictionary *dataSend = [inboxHelper readDataInbox:[dataDict valueForKey:@"transaction_id"] trxrf:[dataDict valueForKey:@"trxref"]];
                TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"QRPCF02"];
                QRPCF02ViewController *viewCont = (QRPCF02ViewController *) templateView;
                [viewCont setDataLocal:dataSend];
                [viewCont setFromRecipt:YES];
                [self.navigationController pushViewController:templateView animated:YES];
            }
        }
    }else{
        if ([[newDict valueForKey:@"read_flag"] isEqualToString:@"0"]) {
            NSInteger countNotif = [[userDefault valueForKey:@"count_notif"]integerValue];
            if (countNotif > 0) {
                countNotif = countNotif -1;
            }
            [userDefault setValue:[@(countNotif)stringValue] forKey:@"count_notif"];
            [userDefault synchronize];
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"changeIcon" object:self userInfo:nil];
        }
        [newDict setObject:@"1" forKey:@"read_flag"];
        NSMutableArray *copyListNotif = [listNotification mutableCopy];
        [copyListNotif replaceObjectAtIndex:indexPath.row withObject:newDict];
        listNotification = [copyListNotif mutableCopy];
        
        [dataManager.dataExtra setObject:[dataDict valueForKey:@"mid"] forKey:@"mid"];
        [dataManager.dataExtra setObject:[dataDict valueForKey:@"nid"] forKey:@"nid"];
//        if ([[dataDict valueForKey:@"read_flag"] isEqualToString:@"0"]) {
//            NSString *strUrl = [NSString stringWithFormat:@"request_type=update_notif,customer_id,mid"];
//            Connection *conn = [[Connection alloc]initWithDelegate:self];
//            [conn sendPostParmUrl:strUrl needLoading:false encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
//        }else{
//             TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
//            [self.navigationController pushViewController:templateView animated:YES];
//        }
        
        NSUserDefaults *userNotification = [[NSUserDefaults alloc]initWithSuiteName:@"group.bsm_mobile"];
        NSMutableArray *listNotifMid = [userNotification objectForKey:@"list_mid"];
        [listNotifMid removeObject:[dataDict valueForKey:@"mid"]];
        [userNotification setObject:listNotifMid forKey:@"list_mid"];
        [userNotification synchronize];
        
            NSString *strUrl = [NSString stringWithFormat:@"request_type=update_notif,customer_id,mid,nid"];
//        NSString *strUrl = [NSString stringWithFormat:@"request_type=detail_notif,customer_id,mid,nid"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        
//        TemplateViewController *templateView = [self.storyboard instantiateViewControllerWithIdentifier:@"DTNTF01"];
//        [self.navigationController pushViewController:templateView animated:YES];
       
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(editingStyle == UITableViewCellEditingStyleDelete){
        
        NSDictionary* userInfo = @{@"position": @(1114)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        indexRow = indexPath.row;
        NSDictionary *dataDict = [listNotification objectAtIndex:indexPath.row];
        if([strCheckTab isEqualToString:@"0"]){
            trxRef = [dataDict valueForKey:@"trxref"];
            transactId = [dataDict valueForKey:@"transaction_id"];
        }else{
            [dataManager.dataExtra setObject:[dataDict valueForKey:@"mid"] forKey:@"mid"];
            [dataManager.dataExtra setObject:[dataDict valueForKey:@"nid"] forKey:@"nid"];
        }
        
        NSString *msg;
        if ([lang isEqualToString:@"id"]) {
            msg = @"Anda yakin untuk menghapus?";
        }else{
            msg = @"Do you want to delete?";
        }
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:lang(@"CANCEL") otherButtonTitles:@"OK", nil];
        alert.tag = 413;
        alert.delegate = self;
        [alert show];
    }
}

- (void)actionDelete:(UIButton *)sender {
    if (listDelete.count > 0) {
        NSLog(@"%@", listDelete);
        NSString *strMsg;
        
        if ([lang isEqualToString:@"id"]) {
            if (listDelete.count == listNotification.count) {
                if ([strCheckTab isEqualToString:@"0"]) {
                    strMsg = @"Apa kamu ingin menghapus semua kotak masuk?";
                }else{
                    strMsg = @"Apa kamu ingin menghapus semua pesan?";
                }
            }else{
                if ([strCheckTab isEqualToString:@"0"]) {
                    strMsg = @"Apa kamu ingin menghapus semua kotak masuk yang di seleksi?";
                }else{
                    strMsg = @"Apa kamu ingin menghapus semua pesan yang di seleksi?";
                }
            }
           
        }else{
            if (listDelete.count == listNotification.count) {
                if ([strCheckTab isEqualToString:@"0"]) {
                    strMsg = @"Do you want to delete all inboxs?";
                }else{
                    strMsg = @"Do you want to delete all messages?";
                }
            }else{
                if ([strCheckTab isEqualToString:@"0"]) {
                    strMsg = @"Do you want to delete all selected inboxes?";
                }else{
                    strMsg = @"Do you want to delete all selected messages?";
                }
            }
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:lang(@"CANCEL") otherButtonTitles:@"OK", nil];
        alert.tag = 415;
        alert.delegate = self;
        [alert show];
       
    }
    
}

-(void)actionMarked:(UIButton *)sender{
    [Utility animeBounchCb:sender];
    if (listNotification.count > 0) {
        [sender setSelected: !sender.isSelected];
        if ([sender isSelected]) {
            stateAllMarked = true;
            NSString *strIdMsg;
            for (int i=0; i<listNotification.count; i++) {
                NSDictionary *dataDic = [listNotification objectAtIndex:i];
                if ([strCheckTab isEqualToString:@"0"]) {
                    strIdMsg = [NSString stringWithFormat:@"%@|%@", [dataDic valueForKey:@"transaction_id"], [dataDic valueForKey:@"trxref"]];
                }else{
//                    strIdMsg = [dataDic valueForKey:@"mid"];
                    strIdMsg = [dataDic valueForKey:@"nid"];
                }
                if (![listDelete containsObject:strIdMsg]) {
                    [listDelete addObject:strIdMsg];
                }
            }
        }else{
            stateAllMarked = false;
            if (listDelete.count > 0) {
                [listDelete removeAllObjects];
            }
        }
        [_tableNotif reloadData];
    }
}

-(void)actionBtnTblChecked:(UIButton *) sender{
    [Utility animeBounchCb:sender];
    [sender setSelected: !sender.isSelected];
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableNotif];
    NSIndexPath *indexPath = [self.tableNotif indexPathForRowAtPoint:touchPoint];
    
    NSDictionary *dataDic = [listNotification objectAtIndex:indexPath.row];
    NSString *strIdMsg;
    if ([strCheckTab isEqualToString:@"0"]) {
        strIdMsg = [NSString stringWithFormat:@"%@|%@", [dataDic valueForKey:@"transaction_id"], [dataDic valueForKey:@"trxref"]];
    }else{
//        strIdMsg = [dataDic valueForKey:@"mid"];
        strIdMsg = [dataDic valueForKey:@"nid"];
    }
    
    if ([sender isSelected]) {
        if (![listDelete containsObject:strIdMsg]) {
            [listDelete addObject:strIdMsg];
        }
    }else{
        if ([listDelete containsObject:strIdMsg]) {
            [listDelete removeObject:strIdMsg];
        }
    }
    
}

-(void)syncBackup{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSInteger countInbox = [self->inboxHelper inboxCount];
        DLog(@"%ld",countInbox);
//        NSString *strCID = [NSString stringWithFormat:@"%@", [self->userDefault valueForKey:@"customer_id"]];
        NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
        if(countInbox != 0){
            NSArray *arInbox = [self->inboxHelper listAllDataInbox];
            NSLog(@"%@", arInbox);
            NSMutableArray *arEncryptInbox = [[NSMutableArray alloc]init];
            for(NSDictionary *data in arInbox){
                NSDictionary *dataEncrypt = [self->aesChiper dictAesEncryptString:data];
                NSDictionary *dataContent = @{@"content" : dataEncrypt};
                [arEncryptInbox addObject:dataContent];
            }
            NSDictionary *mData = @{@"data" : arEncryptInbox};
            NSLog(@"ar encrypt : %@", arEncryptInbox);
            [Utility removeForKeychainKey:strCID];
            [Utility setDict:mData forKey:strCID];
            NSDictionary *dataBacup = [Utility dictForKeychainKey:strCID];
            NSLog(@"data berhasil backup : %@", dataBacup);
        }else{
             [Utility removeForKeychainKey:strCID];
        }
    });
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self.tableNotif reloadData];
    });
}


@end
