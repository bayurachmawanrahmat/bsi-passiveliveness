//
//  CRConfirmViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 06/10/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"
#import "Utility.h"
#import "Styles.h"
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

@interface CRConfirmViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
