//
//  QRPCF02ViewController.h
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "TemplateViewController.h"


@interface QRPCF02ViewController : TemplateViewController
- (void)setDataLocal:(NSDictionary*)object;
- (void)setFromRecipt:(BOOL)recipt;
@end

