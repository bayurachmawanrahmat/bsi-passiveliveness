//
//  AKSCF01ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 10/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "AKSCF01ViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include "Utility.h"

@interface AKSCF01ViewController () <ConnectionDelegate, UIAlertViewDelegate>{
    NSDictionary *dataObject;
    BOOL fromRecipt;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
    UIButton *btnEtcs;
    UIButton *btnShrs;
    UIButton *btnFavs;
    UILabel *lblEtcs;
    UILabel *lblShrs;
    UILabel *lblFavs;
    NSString *isEtcs;
    NSString *nmEtcs;
    NSString *nmShrs;
    NSString *nmFavs;
    NSString *infaqStatus;
    
    UIView *vwBgCaption;
    UIView *vwContinerCaption;
    UIView *vwLineTitleCaption;
    UIView *vwLineTextCaption;
    UILabel *lblTitleCaption;
    UILabel *lblTextCaption;
    UITextField *txtFieldCaption;
    UIButton *btnSaveCaption;
    UIButton *btnCancelCaption;
    NSString *defTitleReceipt;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *viewSS;
@property (weak, nonatomic) IBOutlet UIImageView *imgReceipt;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblFooter;

@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOkShare;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;

- (IBAction)actionMoreListener:(id)sender;
- (IBAction)actionShareListener:(id)sender;
- (IBAction)actionOkShareListener:(id)sender;
- (IBAction)actionOkListener:(id)sender;


@end

@implementation AKSCF01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lblHeader.text = @"";
    _lblContent.text = @"";
    _lblFooter.text = @"";
    
    [_btnShare setTitle:lang(@"SHARING") forState:UIControlStateNormal];
    [_lblTitle setText:[self.jsonData valueForKey:@"title"]];
    
    if (fromRecipt) {
        [_btnMore setHidden:YES];
        [_btnOk setHidden:YES];
        [_btnShare setHidden:NO];
        [_btnOkShare setHidden:NO];
    } else {
        [_btnMore setHidden:YES];
        [_btnOk setHidden:NO];
        [_btnShare setHidden:YES];
        [_btnOkShare setHidden:YES];
    }
    
//    NSString *idvc = [dataManager.dataExtra objectForKey:@"idvc"];
//    NSDictionary *dictZakat;
//    NSString *urlZakat = @"";
//    NSString *urlPost = @"";
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//
//    if(!fromRecipt){
//        if ([idvc isEqualToString:@""]) {
//            NSLog(@"check id vc not used");
//        }else{
//            [self addMyButton];
//        }
//    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    screenSize = screenBounds.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
//    if(idvc == nil){
//        dictZakat = nil;
//        urlZakat = @"";
//        urlPost = @"";
//        [userDefault removeObjectForKey:@"infaqData"];
//    }else{
//        dictZakat = [userDefault objectForKey:@"infaqData"];
//        urlZakat = [dictZakat objectForKey:@"url_parm"];
//        urlPost = [dictZakat objectForKey:@"url_post"];
//        [userDefault removeObjectForKey:@"infaqData"];
//    }
    
    if(self.jsonData){
        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
        
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if(needRequest){
            NSDictionary* userInfo = @{@"position": @(1113)};
            NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
            [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
            _imgReceipt.hidden = YES;
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:fav];
        }
    }else if(dataObject){
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if([lang isEqualToString:@"id"]){
            [self.lblTitle setText:@"Kotak Masuk"];
        } else {
            [self.lblTitle setText:@"Inbox"];
        }
        NSString *response = [dataObject valueForKey:@"response"];
        NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        if([dict objectForKey:@"messageType"]!=nil){
            NSString *staticContent = @"Kartu ATM Anda telah terdaftar di AKSes KSEI";
            _lblHeader.text = [dict valueForKey:@"responseDesc"];
            _lblContent.text = [NSString stringWithFormat:@"%@\n\nNomor kartu ATM: %@\nNomor SID: %@",staticContent,[dict valueForKey:@"cardNo"],[dict valueForKey:@"SIDNo"]];
            _lblFooter.text = @"Terima kasih telah menggunakan layanan BSI mobile.\nSemoga layanan kami mendatangkan berkah bagi anda";
        }else{
            _lblHeader.text = [dict objectForKey:@"title"];
            _lblContent.text = [dict objectForKey:@"msg"];
            _lblFooter.text = [dict objectForKey:@"footer_msg"];
        }
        
        
        NSMutableArray *arrlistInbox = [userDefault objectForKey:@"listInbox"];
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        for (id object in arrlistInbox) {
            if (![object isEqual:[dataObject valueForKey:@"transaction_id"]]) {
                [temp addObject:object];
            }
        }
        
        [userDefault setObject:temp forKey:@"listInbox"];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"changeIcon"
         object:self];
    }
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) addMyButton{
    UIWindow *mWindow = [UIApplication sharedApplication].keyWindow;
    CGRect frmBgView = _bgView.frame;
    frmBgView.origin.x = screenWidth - 130;
    frmBgView.size.width = 200;
    frmBgView.size.height = 50;
    if(IPHONE_X){
        frmBgView.origin.y = screenHeight - 210;
    }else{
        frmBgView.origin.y = screenHeight - 160;
    }
    
    _bgView.frame = frmBgView;
    _bgView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.0];
    [mWindow addSubview:_bgView];
    
    lblEtcs = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    lblEtcs.text = nmEtcs;
    lblEtcs.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblEtcs.numberOfLines = 1;
    lblEtcs.baselineAdjustment = YES;
    lblEtcs.adjustsFontSizeToFitWidth = YES;
    lblEtcs.clipsToBounds = YES;
    lblEtcs.backgroundColor = [UIColor clearColor];
    lblEtcs.textColor = [UIColor blackColor];
    lblEtcs.textAlignment = NSTextAlignmentLeft;
    [_bgView addSubview:lblEtcs];
    
    btnEtcs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnEtcs.frame = CGRectMake(60, 0, 50, 50);
    btnEtcs.backgroundColor = UIColor.yellowColor;
    btnEtcs.layer.cornerRadius = btnEtcs.frame.size.height / 2;
    btnEtcs.layer.masksToBounds = true;
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-menu-vertical-24.png"];
    [btnEtcs setImage:btnFloat forState:UIControlStateNormal];
    [btnEtcs addTarget:self action:@selector(okButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:btnEtcs];
    
}

- (IBAction)actionMoreListener:(id)sender {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSString *msgCancel = @"";
    NSString *msgFavorite = @"";
    NSString *msgShare = @"";
    if([lang isEqualToString:@"id"]){
        msgCancel = @"Batal";
        msgFavorite = @"Favorit";
        msgShare = @"Bagikan";
    } else {
        msgCancel = @"Cancel";
        msgFavorite = @"Favorite";
        msgShare = @"Share";
    }
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"More" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    if (self.jsonData) {
        if ([[self.jsonData objectForKey:@"favorite"]boolValue]) {
            [actionSheet addAction:[UIAlertAction actionWithTitle:msgFavorite style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                [self prosesAddFav];
            }]];
        }
    }
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:msgShare style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self prosesShareImage];
        //        [self dismissViewControllerAnimated:YES completion:^{
        //
        //        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (IBAction)actionShareListener:(id)sender {
    [self prosesShareImage];
}

- (IBAction)actionOkShareListener:(id)sender {
    [super backToRoot];
}

- (IBAction)actionOkListener:(id)sender {
    [super backToRoot];
}

-(void)okButtonTapped:(UIButton *) sender{
    if (![isEtcs isEqualToString:@"1"]) {
        isEtcs = @"1";
        if([[self.jsonData objectForKey:@"favorite"]boolValue]){
            
        }else{
            [btnFavs removeFromSuperview];
            [lblFavs removeFromSuperview];
        }
    }
    
}

-(void) addFavsButton{
    btnFavs = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    if(IPHONE_X){
        lblFavs = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 130, screenHeight - 340, 50, 50)];
        btnFavs.frame = CGRectMake(screenWidth - 70, screenHeight - 340, 50, 50);
    }else{
        lblFavs = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth - 130, screenHeight - 290, 50, 50)];
        btnFavs.frame = CGRectMake(screenWidth - 70, screenHeight - 290, 50, 50);
    }
    lblFavs.text = nmFavs;
    lblFavs.font = [UIFont fontWithName:@"HELVETICA" size:15];
    lblFavs.numberOfLines = 1;
    lblFavs.baselineAdjustment = YES;
    lblFavs.adjustsFontSizeToFitWidth = YES;
    lblFavs.clipsToBounds = YES;
    lblFavs.backgroundColor = [UIColor clearColor];
    lblFavs.textColor = [UIColor blackColor];
    lblFavs.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:lblFavs];
    
    btnFavs.backgroundColor = UIColor.yellowColor;
    btnFavs.layer.cornerRadius = btnFavs.frame.size.height / 2;
    btnFavs.layer.masksToBounds = true;
    
    UIImage *btnFloat = [UIImage imageNamed:@"icons8-heart-outline-24.png"];
    [btnFavs setImage:btnFloat forState:UIControlStateNormal];
    [btnFavs addTarget:self action:@selector(favsButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnFavs];
    [self.view bringSubviewToFront:btnFavs];
}

-(void)favsButtonTapped:(UIButton *)sender{
    [self removeMyButton];
    if(self.jsonData){
        if([[self.jsonData objectForKey:@"favorite"]boolValue]){
            [self prosesAddFav];
        }
    }
}

- (void)removeMyButton {
    [btnEtcs removeFromSuperview];
    [btnFavs removeFromSuperview];
    [btnShrs removeFromSuperview];
    [lblEtcs removeFromSuperview];
    [lblFavs removeFromSuperview];
    [lblShrs removeFromSuperview];
}

-(void) prosesAddFav{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    NSDictionary *rd = [userDefault objectForKey:@"infaqData"];
    //NSDictionary *lr = [userDefault objectForKey:@"listRecipt"];
    if ([rd count] > 0) {
        if ([[rd valueForKey:@"payinfaq"] isEqualToString:@"0"]) {
            infaqStatus = @"0";
        }
        else if ([[rd valueForKey:@"payinfaq"] isEqualToString:@"1"]) {
            infaqStatus = @"1";
        }
    }
    
    if ([infaqStatus isEqualToString:@"1"]) {
        NSString *ipaddr = [self deviceIPAddress];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
        NSString *currentDate = [dateFormat stringFromDate:date];
        NSString *idacc = [dataManager.dataExtra objectForKey:@"id_account"];
        //NSString *trid = [dataManager.dataExtra objectForKey:@"transaction_id"];
        NSString *trid = [userDefault valueForKey:@"tridtx"];
        NSString *mid = @"00021";
        NSString *code = [dataManager.dataExtra objectForKey:@"code"];
        NSString *pin = [userDefault objectForKey:@"pin"];
//        NSString *cid = [userDefault objectForKey:@"customer_id"];
        NSString *cid = [NSUserdefaultsAes getValueForKey:@"customer_id"];
        NSString *payinfaq = @"0";
        NSString *devType = [UIDevice currentDevice].model;
        NSMutableDictionary *temp = [NSMutableDictionary dictionary];
        
        [temp setValue:payinfaq forKey:@"payinfaq"];
        [temp setValue:idacc forKey:@"id_account"];
        [temp setValue:trid forKey:@"transaction_id"];
        [temp setValue:mid forKey:@"menu_id"];
        [temp setValue:code forKey:@"code"];
        [temp setValue:pin forKey:@"pin"];
        [temp setValue:cid forKey:@"customer_id"];
        [temp setValue:lang forKey:@"language"];
        [temp setValue:@"iphone" forKey:@"device"];
        [temp setValue:devType forKey:@"device_type"];
        [temp setValue:ipaddr forKey:@"ip_address"];
        [temp setValue:currentDate forKey:@"date_local"];
        [temp setValue:infaqStatus forKey:@"infaq_status"];
        
        [userDefault setObject:temp forKey:@"passingData"];
    }
    [self removeMyButton];
    [self openTemplate:@"SUCFAV" withData:nil];
}

-(NSString *) deviceIPAddress{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    //#if TARGET_IPHONE_SIMULATOR
    //#error Specify network interface for computer(iPhone simulator) below and then remove this line
    //NSString *networkInterface = @"en1";
    //#else
    NSString *networkInterface = @"en0";
    //#endif
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:networkInterface]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
    
    
}
#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
//    NSDictionary* userInfo = @{@"position": @(1112)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]){
            if([requestType isEqualToString:@"insert_favorite"]){
                [super didFinishLoadData:jsonObject withRequestType:requestType];
            }else{
                if (![requestType isEqualToString:@"check_notif"]) {
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    NSString *response = [jsonObject valueForKey:@"response"];
                    NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    if (dict) {
                        _imgReceipt.hidden = NO;
                        
                        NSMutableDictionary *dictList = [[NSMutableDictionary alloc] init];
                        [dictList setValue:response forKey:@"response"];
                        [dictList setValue:jsonObject forKey:@"apiResponse"];
                        [dictList setValue:@"General" forKey:@"jenis"];
                        
                        if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00056"]){
                            NSString *mHeader, *mContent,*mFooter;
                            mHeader = [dict valueForKey:@"responseDesc"];
                            mFooter = @"Terima kasih telah menggunakan layanan BSI mobile.\nSemoga layanan kami mendatangkan berkah bagi anda";
                            [dictList setValue:[jsonObject valueForKey:@"transaction_id"]
                                        forKey:@"transaction_id"];
                            [dictList setValue:@"AKSCF01" forKey:@"template"];
                            if([requestType isEqualToString:@"reg_ksei"]){
                                if ([[dict valueForKey:@"responseCode"] isEqualToString:@"00"]) {
                                    mContent =[NSString stringWithFormat:@"%@\n\nNomor kartu ATM: %@\nNomor SID: %@",[self.jsonData valueForKey:@"content"],[dict valueForKey:@"cardNo"],[dict valueForKey:@"SIDNo"]];
                                    
                                }else{
                                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[dict valueForKey:@"responseDesc"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                    [alert show];
                                }
                            }else if([requestType isEqualToString:@"tarik_ksei"]){
                                if ([[dict valueForKey:@"responseCode"] isEqualToString:@"00"]) {
                                    mContent = [NSString stringWithFormat:@"Anda mengirimkan instruksi penarikan dana\n\nPerusahaan efek : %@\nKode mata uang : %@\nNominal : %@",[dict valueForKey:@"PEName"],[dict valueForKey:@"currencyCode"], [Utility formatCurrencyValue:[[dict valueForKey:@"nominal"]doubleValue]]];
                                }else{
                                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[dict valueForKey:@"responseDesc"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                    [alert show];
                                }
                            }
                            _lblHeader.text = mHeader;
                            _lblFooter.text = mFooter;
                            _lblContent.text = mContent;
                            
                        }else{
                            _lblHeader.text = [dict objectForKey:@"title"];
                            _lblContent.text = [dict objectForKey:@"msg"];
                            _lblFooter.text = [dict objectForKey:@"footer_msg"];
                            [dictList setValue:[jsonObject valueForKey:@"transaction_id"]
                                        forKey:@"transaction_id"];
                            [dictList setValue:@"GENCF02" forKey:@"template"];
                            
                            NSString *img = [userDefault objectForKey:@"imgIcon"];
                            [dictList setValue:img forKey:@"icon"];
                            NSDate *date = [NSDate date];
                            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                            [dateFormat setDateFormat:@"dd MMM yyyy"];
                            NSString *currentDate = [dateFormat stringFromDate:date];
                            [dictList setValue:currentDate forKey:@"time"];
                            
                            if ([userDefault objectForKey:@"listRecipt"]){
                                NSMutableArray *arr = [[userDefault objectForKey:@"listRecipt"] mutableCopy];
                                [arr addObject:dictList];
                                [userDefault setObject:arr forKey:@"listRecipt"];
                            } else {
                                NSMutableArray *arr = [[NSMutableArray alloc] init];
                                [arr addObject:dictList];
                                [userDefault setObject:arr forKey:@"listRecipt"];
                            }
                            
                            if ([userDefault objectForKey:@"listInbox"]){
                                NSMutableArray *arrTransaction = [[userDefault objectForKey:@"listInbox"] mutableCopy];
                                [arrTransaction addObject:[jsonObject valueForKey:@"transaction_id"]];
                                [userDefault setObject:arrTransaction forKey:@"listInbox"];
                            } else {
                                NSMutableArray *arrTransaction = [[NSMutableArray alloc] init];
                                [arrTransaction addObject:[jsonObject valueForKey:@"transaction_id"]];
                                [userDefault setObject:arrTransaction forKey:@"listInbox"];
                            }
                            
                            [[NSNotificationCenter defaultCenter]
                             postNotificationName:@"changeIcon"
                             object:self];
                        }
                        
                    }else {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,response] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }
        }else if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"01"]){
            
            NSString *response = [jsonObject valueForKey:@"response"];
            NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[dict valueForKey:@"responseDesc"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }else{
            NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    
//    NSDictionary* userInfo = @{@"position": @(1112)};
//    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
//    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}


-(void)prosesShareImage{
    UIGraphicsBeginImageContextWithOptions(_viewSS.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_viewSS.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)setDataLocal:(NSDictionary*) object{
    dataObject = object;
}
- (void)setFromRecipt:(BOOL)recipt {
    fromRecipt = recipt;
}

@end
