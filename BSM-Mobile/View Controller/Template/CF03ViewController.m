//
//  CF03ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 12/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CF03ViewController.h"

@interface CF03ViewController (){
}
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *lblExplanation;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label0;

@end

@implementation CF03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.lblTitle setText:@""];
    
    [self.button.layer setCornerRadius:self.button.frame.size.height/2];
    [self.button.layer setMasksToBounds:YES];
    
    [self.viewContent setHidden:YES];
    [self requesting];
}

- (void)requesting{
    NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

- (IBAction)actionButton:(id)sender {
    [self gotoGold];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
        [self.viewContent setHidden:NO];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self gotoGold];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
