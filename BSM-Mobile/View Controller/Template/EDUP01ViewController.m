//
//  EDUP01ViewController.m
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "EDUP01ViewController.h"
#import "Utility.h"
#import "Connection.h"
#import "MenuViewController.h"
#import "Utility.h"
#import "UIViewController+ECSlidingViewController.h"
#import "CellEduPay.h"
#import "Styles.h"

@interface EDUP01ViewController ()<ConnectionDelegate, UITextFieldDelegate, UIAlertViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource> {
    NSArray *arrayData;
    NSArray *arrayLabel;
    UIPickerView *objPickerView;
    NSString *textStr;
    UIToolbar *toolBar;
    NSInteger *index;
    NSDictionary *data;
    NSArray *listData;
    NSArray *orilistData;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLayanan;
@property (weak, nonatomic) IBOutlet UITextField *textPilihLayanan;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSetuju;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTable;
@property (weak, nonatomic) IBOutlet UITextField *tfNomor;
@property (weak, nonatomic) IBOutlet UILabel *lblDisplay;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightAutoComplate;
@property (weak, nonatomic) IBOutlet UITableView *tabelEdu;
@property (weak, nonatomic) IBOutlet UIView *viewCount;
@property (weak, nonatomic) IBOutlet UILabel *lblPilihLayanan;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@end

@implementation EDUP01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to next and other else
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tabelEdu.dataSource = self;
    self.tabelEdu.delegate = self;
    
    [self.lblTitle setText:[self.jsonData valueForKey:@"title"]];
    objPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 220)];
    objPickerView.showsSelectionIndicator = YES;
    objPickerView.delegate = self; // Also, can be done from IB, if you're using
    objPickerView.dataSource = self;// Also, can be done from IB, if you're using
    
    self.textPilihLayanan.delegate = self;
    _textPilihLayanan.tag = 201;
    _textPilihLayanan.autocorrectionType = UITextAutocorrectionTypeNo;
    // _textPilihLayanan.tintColor = [UIColor clearColor];
    
    [Styles setTopConstant:self.topConstraint];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.btnSetuju setColorSet:SECONDARYCOLORSET];
    
    _tfNomor.tag = 91;
    self.tfNomor.delegate = self;
    _tfNomor.autocorrectionType = UITextAutocorrectionTypeNo;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        _lblLayanan.text = @"Nama Institusi";
        _textPilihLayanan.placeholder = @"Pilih Institusi";
        _lblPilihLayanan.text = @"Masukan ID Pelanggan/Kode Bayar";
//        [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnSetuju setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Batal" forState:UIControlStateNormal];
    } else {
        _lblLayanan.text = @"Institusion Name";
        _textPilihLayanan.placeholder = @"Select Institusion";
        _lblPilihLayanan.text = @"Enter Customer ID / Pay Code";
//        [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnSetuju setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnSetuju setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(changeDateFromLabel:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
//    _textPilihLayanan.inputView = objPickerView;
    _textPilihLayanan.inputAccessoryView = toolBar;
    _tfNomor.inputAccessoryView = toolBar;
    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
//    Connection *conn = [[Connection alloc]initWithDelegate:self];
//    [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
}


- (void)viewDidLayoutSubviews{
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    _heightAutoComplate.constant = height - 480;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    [self refreshIdleTimer];
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    if (textField.tag == 201) {
        if ([textField.text isEqualToString:@""]) {
            listData = orilistData;
            [self.tabelEdu reloadData];
        } else {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSString *temp in orilistData){
                if ([temp rangeOfString:textField.text].location != NSNotFound) {
                    [newData addObject:temp];
                }
            }
            
            listData = (NSArray *)newData;
            [self.tabelEdu reloadData];
        }
      
        if ([_viewCount isHidden]) {
            [self.viewCount setHidden:NO];
        }
    }
    
//    if (textField.tag == 201) {
//        if ([textField.text  isEqual: @""] && [arrayLabel count] > 0){
//            textField.text = [arrayLabel objectAtIndex:0];
//            data = [arrayData objectAtIndex:0];
//        }
//    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;  // Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView
numberOfRowsInComponent:(NSInteger)component {
    return [arrayLabel count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [arrayLabel objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    index = &row;
    
    textStr = [arrayLabel objectAtIndex:row];
    _lblDisplay.text = [arrayLabel objectAtIndex:row];
    _textPilihLayanan.text = @" ";
    data = [arrayData objectAtIndex:row];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 91) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
//    [self refreshIdleTimer];
    
    if (textField.tag == 201) {
        
        if ([self.viewCount isHidden]) {
            [self.viewCount setHidden:NO];
        }
        
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        if ([substring length] > 0) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for (NSString *temp in orilistData){
                NSRange substringRange = [[temp lowercaseString] rangeOfString:[substring lowercaseString]];
                if (substringRange.length != 0) {
                    [newData addObject:temp];
                }
                //            if ([temp rangeOfString:substring].location != NSNotFound) {
                //                [newData addObject:temp];
                //            }
            }
            
            listData = (NSArray *)newData;
            [self.tabelEdu reloadData];
        } else {
            listData = orilistData;
            [self.tabelEdu reloadData];
        }
    }
    
    return YES;
}


- (void)changeDateFromLabel:(id)sender {
    if (![self.viewCount isHidden]) {
        [self.viewCount setHidden:YES];
    }
     [self.view endEditing:YES];
//    [_textPilihLayanan resignFirstResponder];
}

#pragma mark change actionSetuju to goToCancel
- (IBAction)actionSetuju:(id)sender {
    [self goToCancel];
}

-(void) goToNext{
    if (![_tfNomor.text isEqualToString:@""] && ![_textPilihLayanan isEqual:@""]) {
        NSString *strPrefix = [_textPilihLayanan.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSArray *dt = [strPrefix componentsSeparatedByString:@"-"];
        if ([dt count] > 0) {
            [dataManager.dataExtra addEntriesFromDictionary:@{@"payment_id": [NSString stringWithFormat:@"%@%@", dt[0], _tfNomor.text]}];
            
            //    [dataManager.dataExtra addEntriesFromDictionary:@{@"code": [[data objectForKey:@"prefix"] stringByReplacingOccurrencesOfString:@" " withString:@""]}];
            //    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            //    [userDefault setObject:aditionalParam forKey:@"aditionalInfo"];
            [super openNextTemplate];
        }
//    } else if ([_textPilihLayanan isEqual:@""]) {
//
//    } else if ([_tfNomor.text isEqualToString:@""]) {
//
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark change actionBatal to goToNext
- (IBAction)actionBatal:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    [self backToRoot];
}

- (IBAction)actionShowPopUp:(id)sender {
    // [_textPilihLayanan becomeFirstResponder];
}


#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if (![requestType isEqualToString:@"check_notif"]) {
        if([jsonObject isKindOfClass:[NSDictionary class]]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        } else {
            arrayData = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in arrayData){
                NSString *edu = [temp objectForKey:@"name"];
                NSString *prefix = [[temp objectForKey:@"prefix"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSArray *dt = [edu componentsSeparatedByString:@"-"];
                NSString *Label = @"";
                if ([dt count] > 0) {
                    if ([dt count] == 1) {
                        Label = [NSString stringWithFormat:@"%@ - %@", prefix, [dt[0] stringByReplacingOccurrencesOfString:@"  " withString:@""]];
                    } else if ([dt count] == 2) {
                        Label = [NSString stringWithFormat:@"%@ - %@",prefix, [dt[1] stringByReplacingOccurrencesOfString:@"  " withString:@""]];
                    } else {
                        for (int i = 0; i <= [dt count]; i++) {
                            NSMutableString* string1 = [[NSMutableString alloc] init];
                            if (i != 0) {
                                [string1 appendString: [dt[1] stringByReplacingOccurrencesOfString:@"  " withString:@""]];
                            }
                            Label = [NSString stringWithString:string1];
                        }
                    }
                    [newData addObject:Label];
                }
                
            }
            //        arrayLabel = (NSArray *)newData;
            orilistData = (NSArray *)newData;
            listData = (NSArray *)newData;
            [self.tabelEdu reloadData];
        }
        
    }
    
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else {
        [self backToRoot];
    }
}




#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"cellEdu";
    CellEduPay *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[CellEduPay alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.lbl.text = [listData objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 36.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    self.textPilihLayanan.text = [listData objectAtIndex:indexPath.row];
    [self.viewCount setHidden:true];
}



@end
