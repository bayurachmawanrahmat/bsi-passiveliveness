//
//  PF01ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 05/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PF01ViewController.h"
#import "TrackCell.h"
#import "PF02ViewController.h"
#import "PF03ViewController.h"
#import "ChildPF01ViewController.h"
#import "Utility.h"

@interface PF01ViewController()<ConnectionDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    NSInteger tabSelected;
    NSString *sendData;
}

@property (weak, nonatomic) IBOutlet UIView *vwContainer;


@property (strong, nonatomic) NSArray *tabText;
@property (nonatomic, retain) NSArray *SAVArray;
@property (nonatomic, retain) NSArray *SECArray;
@property (nonatomic, retain) NSArray *DEPArray;
@property (nonatomic, retain) NSArray *LOANArray;
@property (nonatomic, retain) NSArray *CURArray;
@property (nonatomic, retain) NSArray *ZISWAFArray;


@end

@implementation PF01ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    userDefault = [NSUserDefaults standardUserDefaults];
    tabSelected = 0;
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if ([lang isEqualToString:@"id"]) {
        self.tabText = @[@"TABUNGAN",@"GIRO",@"DEPOSITO",@"PEMBIAYAAN",@"SURAT BERHARGA", @"BERBAGI"];
    }else{
        self.tabText = @[@"SAVINGS",@"CURRENT",@"DEPOSIT",@"FINANCING",@"SECURITIES", @"SHARING"];
    }
    
    [userDefault setValue:@"1" forKey:@"PEF_STATE"];
    [userDefault synchronize];
    
    [self setupLayout];
    
    [self arrayInit];

}

- (void)viewWillAppear:(BOOL)animated{
    if (self.jsonData) {
        BOOL fav = [[self.jsonData objectForKey:@"favorite"]boolValue];
        BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
        if(needRequest){
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:fav];
        }
    }
}

-(void) setupLayout{
    CGRect frmVwContainer = self.vwContainer.frame;
    
    frmVwContainer.origin.y = TOP_NAV + 50;
    frmVwContainer.origin.x = 0;
    frmVwContainer.size.width = SCREEN_WIDTH;
    if ([Utility isDeviceHaveNotch]) {
        frmVwContainer.origin.y = TOP_NAV + 80;
    }
    frmVwContainer.size.height = SCREEN_HEIGHT - frmVwContainer.origin.y;
    
    self.vwContainer.frame = frmVwContainer;
    
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    NSLog(@"OUT OF MEMORY");
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType {
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
        if (![requestType isEqualToString:@"check_notif"]) {
            if([jsonObject isKindOfClass:[NSDictionary class]]){
                NSString *response = [jsonObject objectForKey:@"response"];
                NSMutableDictionary *dict=[NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                NSLog(@"%@", dict);
                if (dict) {
                    //populate data SEC Array
                    NSArray *temp1 = (NSArray *) [dict objectForKey:@"sec"];
                    NSMutableArray *tempPF1 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp1){
                        NSMutableDictionary *dictX1 = [NSMutableDictionary new];
                        [dictX1 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        //[dictX1 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX1 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX1 setValue:[data objectForKey:@"closingbalnonom"] forKey:@"b3"];
                        [tempPF1 addObject:dictX1];
                    }
                    
                    if ([tempPF1 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.SECArray = [tempPF1 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.SECArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //populate data LOAN Array
                    NSArray *temp2 = (NSArray *) [dict objectForKey:@"loan"];
                    NSMutableArray *tempPF2 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp2){
                        NSMutableDictionary *dictX2 = [NSMutableDictionary new];
                        [dictX2 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        [dictX2 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX2 setValue:[data objectForKey:@"producttype"] forKey:@"b3"];
                        [dictX2 setValue:[data objectForKey:@"outs"] forKey:@"b4"];
                        if ([data objectForKey:@"dateopened"]) {
                            [dictX2 setValue:[data objectForKey:@"dateopened"] forKey:@"b51"];
                            [dictX2 setValue:[data objectForKey:@"dateclosed"] forKey:@"b52"];
                            [dictX2 setValue:[data objectForKey:@"initbal"] forKey:@"b6"];
                            [dictX2 setValue:[data objectForKey:@"installmentdate"] forKey:@"b7"];
                            
                        }
                        [tempPF2 addObject:dictX2];
                    }
                    
                    if ([tempPF2 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.LOANArray = [tempPF2 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.LOANArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //populate data CUR Array
                    NSArray *temp3 = (NSArray *) [dict objectForKey:@"current"];
                    NSMutableArray *tempPF3 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp3){
                        NSMutableDictionary *dictX3 = [NSMutableDictionary new];
                        [dictX3 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        [dictX3 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX3 setValue:[data objectForKey:@"category"] forKey:@"b31"];
                        [dictX3 setValue:[data objectForKey:@"catdesc"] forKey:@"b32"];
                        [dictX3 setValue:[data objectForKey:@"currency"] forKey:@"b41"];
                        [dictX3 setValue:[data objectForKey:@"availbal"] forKey:@"b42"];
                        if ([data objectForKey:@"lockedamt"]) {
                             [dictX3 setValue:[data objectForKey:@"lockedamt"] forKey:@"b52"];
                        }
                        if ([data objectForKey:@"totbal"]) {
                            [dictX3 setValue:[data objectForKey:@"totbal"] forKey:@"b61"];
                        }
                        [tempPF3 addObject:dictX3];
                    }
                    
                    if ([tempPF3 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.CURArray = [tempPF3 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.CURArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //populate data SAV Array
                    NSArray *temp4 = (NSArray *) [dict objectForKey:@"saving"];
                    NSMutableArray *tempPF4 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp4){
                        NSMutableDictionary *dictX4 = [NSMutableDictionary new];
                        [dictX4 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        [dictX4 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX4 setValue:[data objectForKey:@"category"] forKey:@"b31"];
                        [dictX4 setValue:[data objectForKey:@"catdesc"] forKey:@"b32"];
                        [dictX4 setValue:[data objectForKey:@"currency"] forKey:@"b41"];
                        [dictX4 setValue:[data objectForKey:@"availbal"] forKey:@"b42"];
                        if ([data objectForKey:@"lockedamt"]) {
                            [dictX4 setValue:[data objectForKey:@"lockedamt"] forKey:@"b52"];
                        }
                        if([[data objectForKey:@"category"] isEqualToString:@"6015"] || [[data objectForKey:@"category"] isEqualToString:@"6014"] ){
                            [dictX4 setValue:[data objectForKey:@"openingdate"] forKey:@"b61"];
                            [dictX4 setValue:[data objectForKey:@"maturitydate"] forKey:@"b71"];
                            [dictX4 setValue:[data objectForKey:@"installmentamt"] forKey:@"b81"];
                            [dictX4 setValue:[data objectForKey:@"installmentdate"] forKey:@"b91"];
                        }
                        if ([data objectForKey:@"totbal"]) {
                            [dictX4 setValue:[data objectForKey:@"totbal"] forKey:@"b101"];
                        }
                        
                        if([data objectForKey:@"descprodext"]){
                            [dictX4 setValue:[data objectForKey:@"descprodext"] forKey:@"b102"];
                        }
                        
                        [tempPF4 addObject:dictX4];
                    }
                    
                    if ([tempPF4 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.SAVArray = [tempPF4 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.SAVArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //populate data DEP Array
                    NSArray *temp5 = (NSArray *) [dict objectForKey:@"td"];
                    NSMutableArray *tempPF5 = [[NSMutableArray alloc] init];
                    for (NSDictionary *data in temp5){
                        NSMutableDictionary *dictX5 = [NSMutableDictionary new];
                        [dictX5 setValue:[data objectForKey:@"acctno"] forKey:@"b1"];
                        [dictX5 setValue:[data objectForKey:@"acctname"] forKey:@"b2"];
                        [dictX5 setValue:[data objectForKey:@"category"] forKey:@"b31"];
                        [dictX5 setValue:[data objectForKey:@"catdesc"] forKey:@"b32"];
                        [dictX5 setValue:[data objectForKey:@"currency"] forKey:@"b41"];
                        [dictX5 setValue:[data objectForKey:@"availbal"] forKey:@"b42"];
                        
                        if ([data objectForKey:@"bilyetno"]) {
                            [dictX5 setValue:[data objectForKey:@"bilyetno"] forKey:@"b51"];
                            [dictX5 setValue:[data objectForKey:@"initbal"] forKey:@"b61"];
                            [dictX5 setValue:[data objectForKey:@"openingdate"] forKey:@"b71"];
                            [dictX5 setValue:[data objectForKey:@"maturitydate"] forKey:@"b81"];
                            [dictX5 setValue:[data objectForKey:@"tenor"] forKey:@"b91"];
                            [dictX5 setValue:[data objectForKey:@"arooption"] forKey:@"b101"];
                        }
                        
                        if([data objectForKey:@"mudnisbah"]){
                            [dictX5 setValue:[data objectForKey:@"mudnisbah"] forKey:@"b111"];
                        }
                        
                        if([data objectForKey:@"descprodext"]){
                            [dictX5 setValue:[data objectForKey:@"descprodext"] forKey:@"b121"];
                        }
                        
                        [tempPF5 addObject:dictX5];
                    }
                    NSLog(@"Data Dep : %@", tempPF5);
                    
                    if ([tempPF5 count] > 0) {
                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"acctno" ascending:YES];
                        self.DEPArray = [tempPF5 sortedArrayUsingDescriptors:@[descriptor]];
                    }
                    else {
                        self.DEPArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    //populate data ZISWAF Array
                    NSDictionary *tempZiswaf = [dict objectForKey:@"ziswafhm1"];
                    NSMutableArray *arrTempZiswaf = [[NSMutableArray alloc]init];
                    if (tempZiswaf) {
                        
                        if ([tempZiswaf valueForKey:@"zakat"]) {
                            NSMutableDictionary *tempDictZiswaf = [[NSMutableDictionary alloc]init];
                            [tempDictZiswaf setValue:@"Zakat" forKey:@"key"];
                            NSArray *dataValue = (NSArray *)[tempZiswaf objectForKey:@"zakat"];
                            if (dataValue.count > 0) {
                                for (int x = 0; x<dataValue.count; x++) {
                                    NSDictionary *dictVal =[dataValue objectAtIndex:x];
                                    [tempDictZiswaf setValue:[dictVal.allKeys objectAtIndex:0] forKey:[NSString stringWithFormat:@"key_%d", x]];
                                    [tempDictZiswaf setValue:[dictVal valueForKey:[dictVal.allKeys objectAtIndex:0]] forKey:[NSString stringWithFormat:@"val_%d",x]];
                                }
                            }
                            [arrTempZiswaf addObject:tempDictZiswaf];
                        }
                        if ([tempZiswaf valueForKey:@"infaq"]) {
                            NSMutableDictionary *tempDictZiswaf = [[NSMutableDictionary alloc]init];
                            [tempDictZiswaf setValue:@"Infaq" forKey:@"key"];
                            NSArray *dataValue = (NSArray *)[tempZiswaf objectForKey:@"infaq"];
                            if (dataValue.count > 0) {
                                for (int x = 0; x<dataValue.count; x++) {
                                    NSDictionary *dictVal =[dataValue objectAtIndex:x];
                                    [tempDictZiswaf setValue:[dictVal.allKeys objectAtIndex:0] forKey:[NSString stringWithFormat:@"key_%d", x]];
                                    [tempDictZiswaf setValue:[dictVal valueForKey:[dictVal.allKeys objectAtIndex:0]] forKey:[NSString stringWithFormat:@"val_%d",x]];
                                }
                            }
                            [arrTempZiswaf addObject:tempDictZiswaf];
                        }
                        if ([tempZiswaf valueForKey:@"wakaf"]) {
                           NSMutableDictionary *tempDictZiswaf = [[NSMutableDictionary alloc]init];
                            [tempDictZiswaf setValue:@"Wakaf" forKey:@"key"];
                            NSArray *dataValue = (NSArray *)[tempZiswaf objectForKey:@"wakaf"];
                            if (dataValue.count > 0) {
                                for (int x = 0; x<dataValue.count; x++) {
                                    NSDictionary *dictVal =[dataValue objectAtIndex:x];
                                    [tempDictZiswaf setValue:[dictVal.allKeys objectAtIndex:0] forKey:[NSString stringWithFormat:@"key_%d", x]];
                                    [tempDictZiswaf setValue:[dictVal valueForKey:[dictVal.allKeys objectAtIndex:0]] forKey:[NSString stringWithFormat:@"val_%d",x]];
                                }
                            }
                            [arrTempZiswaf addObject:tempDictZiswaf];
                        }
                        
                        /*Format get yang laman*/
//                        for (NSString *objKeyZiswaf in tempZiswaf.allKeys) {
//                            NSMutableDictionary *tempDictZiswaf = [[NSMutableDictionary alloc]init];
//                            [tempDictZiswaf setValue:objKeyZiswaf forKey:@"key"];
//                            //[tempDictZiswaf setObject:(NSArray *)[tempZiswaf objectForKey:objKeyZiswaf] forKey:@"value"];
//                            NSArray *dataValue = (NSArray *)[tempZiswaf objectForKey:objKeyZiswaf];
//                            if (dataValue.count > 0) {
//                                for (int x = 0; x<dataValue.count; x++) {
//                                    NSDictionary *dictVal =[dataValue objectAtIndex:x];
//                                    [tempDictZiswaf setValue:[dictVal.allKeys objectAtIndex:0] forKey:[NSString stringWithFormat:@"key_%d", x]];
//                                    [tempDictZiswaf setValue:[dictVal valueForKey:[dictVal.allKeys objectAtIndex:0]] forKey:[NSString stringWithFormat:@"val_%d",x]];
//                                }
//                            }
//                            [arrTempZiswaf addObject:tempDictZiswaf];
//                        }
                    }
                    if (arrTempZiswaf.count > 0) {
                        NSLog(@"Ziswaf Array : %@", arrTempZiswaf);
                        self.ZISWAFArray = arrTempZiswaf;
                    }else{
                        self.ZISWAFArray = [[NSArray alloc] initWithObjects:@"",nil];
                    }
                    
                    
                   
                    
                    NSDictionary* userInfo = @{@"SEC_ARRAY": self.SECArray, @"LOAN_ARRAY" : self.LOANArray, @"CUR_ARRAY" : self.CURArray,
                                               @"SAV_ARRAY" : self.SAVArray, @"DEP_ARRAY" : self.DEPArray, @"ZISWAF_ARRAY" : self.ZISWAFArray
                                               };
                    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
                    [nc postNotificationName:@"portofolioNavigate" object:self userInfo:userInfo];
                }
            }
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)errorLoadData:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp {
    [self backToRoot];
    [BSMDelegate reloadApp];
}

-(void)arrayInit{
    self.SAVArray = [[NSArray alloc] initWithObjects:@"",nil];
    self.CURArray = [[NSArray alloc] initWithObjects:@"",nil];
    self.DEPArray = [[NSArray alloc] initWithObjects:@"",nil];
    self.LOANArray = [[NSArray alloc] initWithObjects:@"",nil];
    self.SECArray = [[NSArray alloc] initWithObjects:@"",nil];
    self.ZISWAFArray = [[NSArray alloc] initWithObjects:@"",nil];
}


-(void) resetTimer{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}




@end
