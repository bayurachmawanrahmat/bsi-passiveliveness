//
//  IB08ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 15/01/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "IB08ViewController.h"
#import "DESChiper.h"
#import "UIAlertController+AlertExtension.h"


@interface IB08ViewController ()<UITextFieldDelegate>{
    DESChiper *desChipper;
}

@end

@implementation IB08ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    desChipper = [[DESChiper alloc]init];
    
    [Styles setTopConstant:_topSpace];
    [_btnCancel setColorSet:SECONDARYCOLORSET];
    [_btnNext setColorSet:PRIMARYCOLORSET];
    
    [_titleBar setText:[self.jsonData valueForKey:@"title"]];
    
    _titleBar.textColor = UIColorFromRGB(const_color_title);
    _titleBar.textAlignment = const_textalignment_title;
    _titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    _titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    
    if([Utility isLanguageID]){
        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Batal" forState:UIControlStateNormal];
    }else{
        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [Styles setStyleTextFieldBottomLine:_textField];
//    [_textField setPlaceholder:[self.jsonData valueForKey:@"content"]];
    [_lblTitle setText:[self.jsonData valueForKey:@"content"]];
    
    if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"oldpin"] ||
       [[self.jsonData valueForKey:@"field_name"]isEqualToString:@"newpin"] ||
       [[self.jsonData valueForKey:@"field_name"]isEqualToString:@"newpin2"]){
        [self.textField setSecureTextEntry:true];
        [self.textField setKeyboardType:UIKeyboardTypeNumberPad];
        [self.textField setDelegate:self];
    }
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    _textField.inputAccessoryView = keyboardDoneButtonView;
    
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"oldpin"] ||
       [[self.jsonData valueForKey:@"field_name"]isEqualToString:@"newpin"] ||
       [[self.jsonData valueForKey:@"field_name"]isEqualToString:@"newpin2"]){
        
        int maxLength = 6;
        
        if(newText.length > maxLength){
            return NO;
        }
    }
    
    return YES;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
}

- (IBAction)doneClicked:(id)sender
{
    DLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void) actionNext{
    if([_textField.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        [Utility showMessage:lang(@"NOT_EMPTY") instance:self];
    }else{
        NSString *message = @"";
        NSString *trimmedStringtextFieldInput = [_textField.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
        if ([trimmedStringtextFieldInput length]) {
            message = lang(@"STR_NOT_NUM");
        }
        
        if([message isEqualToString:@""]){
            if([[self.jsonData valueForKey:@"content"]isEqualToString:@"pin"]){
                [dataManager setPinNumber:_textField.text];
                [super openNextTemplate];
            }else if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"oldpin"] ||
                     [[self.jsonData valueForKey:@"field_name"]isEqualToString:@"newpin"] ||
                     [[self.jsonData valueForKey:@"field_name"]isEqualToString:@"newpin2"]){
                
                if([self.textField.text length] != 6){
                    [Utility showMessage:lang(@"PIN_MUST_6_DIGIT") instance:self];
                }else{
                    
                    NSString* str = [dataManager.dataExtra valueForKey:@"encKey"];
                    if(str){
                        NSData *data = [[NSData alloc]initWithBase64EncodedString:str options:NSDataBase64DecodingIgnoreUnknownCharacters];
                        NSString *trial = [desChipper doDecryptDES:data key:KEY_ENCRYPTED_ZPK];
                    
                        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:[desChipper doPinBlock:_textField.text key:trial]}];
                    }
                    
                    if([[self.jsonData valueForKey:@"field_name"]isEqualToString:@"newpin2"]){
                        if([[dataManager.dataExtra valueForKey:@"newpin"] isEqualToString:[dataManager.dataExtra valueForKey:@"newpin2"]]){
                            [self openNextTemplate];
                        }else{
                            [Utility showMessage:@"PIN baru dan konfirmasi PIN tidak sesuai, silahkan coba lagi" enMessage:@"New PIN and PIN confirmation do not match, please try again" instance:self];
//                            [Utility showMessage:lang(@"PIN_NOT_SAME") instance:self];
                        }
                    }else{
                        [self openNextTemplate];
                    }
                    
                }
            }
        } else {
            [Utility showMessage:message instance:self];
        }
    }}

- (void) actionCancel{
    [self backToR];
}
@end
