//
//  EMSCANViewController.h
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 30/05/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface EMSCANViewController : TemplateViewController
{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;
- (void) setTitleBar:(NSString*)newTitle;


typedef void (^completion_lb) (NSString *result, NSError *error);

@end

static NSString *responseCommandOK = @"9000";
static NSString *responseSwOK = @"900";
static NSString *cardTypeOldApplet = @"OLD";
static NSString *cardTypeNewApplet = @"NEW";
static NSString *keyFlagReversal = @"EM/RVSL/";

static NSString *resStatUpdateBalance = @"UPDATE";
static NSString *resStatReversalOldApplet = @"OLDREV";
static NSString *resStatReversalNewApplet = @"NEWREV";

@protocol EMSCANDelegate

@required

- (void) cardInformation: (NSDictionary*) data;

@end
NS_ASSUME_NONNULL_END
