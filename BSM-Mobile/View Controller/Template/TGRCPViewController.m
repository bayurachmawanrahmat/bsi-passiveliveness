//
//  TGRCPViewController.m
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 12/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGRCPViewController.h"

@interface TGRCPViewController (){
    NSString *state;
}

@end

@implementation TGRCPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_btnNext addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
}


- (void)actionCancel{
    [self backToR];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    state = @"apply";
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=topup_griya,step=apply,menu_id,device,device_type,ip_address,language,date_local,customer_id,pin,app_no,kodeSkemaPrice,tenor,noTelpKantor,angsuran,emailPemohon,imgKtp,imgSlipGaji,limitPlafon,transaction_id" needLoading:true encrypted:true banking:true favorite:nil];
    }

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"topup_griya"]){
        if([state isEqualToString:@"apply"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                NSString *responses = [jsonObject objectForKey:@"response"];
                self.lblRespon.text = responses;
                self.lblRespon.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblRespon.numberOfLines = 0;
                [self.lblRespon setFont:[UIFont systemFontOfSize:16.0]];
                self.lblRespon.textAlignment = NSTextAlignmentCenter;
                [self.lblRespon sizeToFit];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self backToR];
                }]];
                if (@available(iOS 13.0, *)) {
                    [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [self presentViewController:alert animated:YES completion:nil];
            }
                
            }
        }
    }

@end
