//
//  LV03ViewController.m
//  BSM Mobile
//
//  Created by lds on 5/23/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import "LV03ViewController.h"
#import "Connection.h"
#import "Utility.h"

@interface LV03ViewController ()<ConnectionDelegate>{
    NSArray *listData;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgHeaderLv3;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)ok:(id)sender;
- (IBAction)batal:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnN;
@property (weak, nonatomic) IBOutlet UIButton *btnB;

@end

@implementation LV03ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    _imgHeaderLv3.image = [UIImage imageNamed:img];
    
    //updat to amanah styles
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    [self.labelTitle setTextAlignment:NSTextAlignmentCenter];
    [self.labelTitle setTextColor:UIColorFromRGB(const_color_title)];
    
    if([[self.jsonData valueForKey:@"url"]boolValue]){
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"]  needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
    
    [self.textField setPlaceholder:[self.jsonData valueForKey:@"content"]];
    // Do any additional setup after loading the view.
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    self.textField.inputAccessoryView = keyboardDoneButtonView;
    
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        [_btnB setTitle:@"Batal" forState:UIControlStateNormal];
        [_btnN setTitle:@"Selanjutnya" forState:UIControlStateNormal];
    } else {
        [_btnB setTitle:@"Cancel" forState:UIControlStateNormal];
        [_btnN setTitle:@"Next" forState:UIControlStateNormal];
    }
}

- (IBAction)doneClicked:(id)sender
{
    DLog(@"Done Clicked.");
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV02Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    //    if(first){
    //        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
    //        [label setText:[data valueForKey:@"title"]];
    //    }else{
    [label setText:[listData objectAtIndex:indexPath.row]];
    //    }
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [label.text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = [listData objectAtIndex:[indexPath row]];
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+10.0;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)ok:(id)sender {
    if([self.textField.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        [dataManager.dataExtra addEntriesFromDictionary:@{[self.jsonData valueForKey:@"field_name"]:self.textField.text}];
        [super openNextTemplate];
    }
}

- (IBAction)batal:(id)sender {
    [super backToRoot];
}

#pragma mark - Conn
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        if (![requestType isEqualToString:@"check_notif"]) {
            NSArray *temp = [jsonObject objectForKey:@"response"];
            listData = [Utility changeToArray:temp];
            [self.tableView reloadData];
            
            if([jsonObject objectForKey:@"transaction_id"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"] }];
            }
            if([jsonObject objectForKey:@"share"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"share":[jsonObject valueForKey:@"share"] }];
            }
            [self.tableView reloadData];
            
        }
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",ERROR_UNKNOWN] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.scrollView setContentOffset:CGPointMake(0.0, 80.0) animated:true];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self ok:nil];
    return true;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textField resignFirstResponder];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
