//
//  CR04ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "CR04ViewController.h"
#import "Styles.h"

@interface CR04ViewController ()<UIScrollViewDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadIndicator;
@property (weak, nonatomic) IBOutlet UIScrollView *vwAccept;
@property (weak, nonatomic) IBOutlet UILabel *lblCongrats;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;



@end

@implementation CR04ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Styles setTopConstant:self.topConstant];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.lblTitleBar.text = [self.jsonData objectForKey:@"title"];
    
    [self.loadIndicator startAnimating];
    [self.loadIndicator setHidesWhenStopped:NO];
    CGAffineTransform transform = self.loadIndicator.transform;
    transform = CGAffineTransformScale(transform, 2, 2);
    self.loadIndicator.transform = transform;
    [self.loadIndicator setTintColor:UIColorFromRGB(const_color_darkprimary)];
    
    [self.vwAccept setHidden:YES];
    
    [Styles setStyleButton:self.btnNext];
    
    [self.btnNext setTitle:[lang(@"BACK_TO_HOME") uppercaseString] forState:UIControlStateNormal];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [self doRequest];
}

#pragma mark back to home
- (void) actionNext{
    [self backToR];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"cashfin_requested"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"08"]){
            /**
             {"response":"Selamat anda mendapatkan limit indikatif pembiayaan hingga [plafon]<br><br>Anda mendapatkan penawaran pembiayaan dengan jangka waktu maksimal [time_period] dengan syarat ketentuan MMQ wajib terpenuhi<br><br>Jika Anda tertarik, klik \"Ajukan Pembiayaan\" untuk masuk ketahap selanjutnya<br>","transaction_id":"","response_code":"00"}
             */
            NSString *response = [[jsonObject objectForKey:@"response"] stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            NSArray *resArr = [response componentsSeparatedByString:@"\n\n"];
            [self.loadIndicator startAnimating];
            [self.lblCongrats setText:[resArr objectAtIndex:0]];
            [self.lblDesc setText:[response substringFromIndex:self.lblCongrats.text.length]];
            
            [UIView transitionWithView:self.view
              duration:1
               options:UIViewAnimationOptionShowHideTransitionViews
            animations:^{
                self.vwAccept.hidden = NO;
            }
            completion:NULL];
        }
        else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [Utility resetTimer];
}

- (void)reloadApp{
    [self backToR];
}

@end
