//
//  TGCNFViewController.m
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 11/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGCNFViewController.h"

@interface TGCNFViewController (){
    NSString *state;
    NSUserDefaults *userDefault;
    NSString *language;
}

@end

@implementation TGCNFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleBar.text = [self.jsonData valueForKey:@"title"];
    
    [_btnCancel.layer setCornerRadius:20.0f];
    [_btnCancel.layer setBorderWidth:1.0f];
    [_btnCancel.layer setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor];
    [_btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([language isEqualToString:@"id"]){
        [self.btnNext setTitle:@"SETUJU" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"BATAL" forState:UIControlStateNormal];
    }else{
        [self.btnNext setTitle:@"APPLY" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    }

}

- (void)actionCancel{
    [self backToR];
}

- (void)actionNext{
    [self openNextTemplate];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    state = @"confirm";
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=topup_griya,step=confirm,menu_id,device,device_type,ip_address,language,date_local,customer_id,app_no,transaction_id,nama,jenisAset,nilaiObjek,osPembiayaan,hishahBank,hishahNasabah,nisbahBank,nisbahNasabah,limitPlafon,angsuran,noTelpKantor,skemaPrice,emailPemohon,tenor" needLoading:true encrypted:true banking:true favorite:nil];
    }

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"topup_griya"]){
        if([state isEqualToString:@"confirm"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                NSString *responses = [jsonObject objectForKey:@"response"];
                
                self.lblRespon.text = responses;
                self.lblRespon.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblRespon.numberOfLines = 0;
                [self.lblRespon setFont:[UIFont systemFontOfSize:14.0]];
                self.lblRespon.textAlignment = NSTextAlignmentLeft;
                [self.lblRespon sizeToFit];
               
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self backToR];
                }]];
                if (@available(iOS 13.0, *)) {
                    [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
}


@end
