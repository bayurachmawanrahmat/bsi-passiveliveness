//
//  SKLV01ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 2/13/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "SKLV01ViewController.h"
#import "SKCF03ViewController.h"
#import "Utility.h"
#import "Connection.h"

@interface SKLV01ViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate >{
    NSArray *listAction;
    NSArray *listOrigin;
    NSDictionary *dataObject;
    NSDictionary *dataLocal;
    BOOL first;
    BOOL fromHome;
    BOOL spesial;
    BOOL mustAddToManager;
    NSString *key;
    UIToolbar *toolBar;
    NSString *message;
}

@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *iconHeaderLV01;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end

@implementation SKLV01ViewController

NSString* requestTypeSKLV01;
extern NSString *lv_selected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)setFirstLV:(BOOL)aFirstLV{
    first = aFirstLV;
}

- (void)setFromHome:(BOOL)afromHome{
    fromHome = afromHome;
}

- (void)setSpecial:(BOOL)aspecial{
    spesial = aspecial;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setDisplay];
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [self.iconHeaderLV01 setHidden:YES];
    
    //self.textSearch.hidden = true;
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,320,44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    toolBar.items = @[barButtonDone];
    barButtonDone.tintColor=[UIColor blackColor];
    
    //_textSearch.inputAccessoryView = toolBar;
    //_textSearch.delegate = self;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *dt = [userDefault objectForKey:@"indexMenu"];
    if (dt) {
        [[DataManager sharedManager]resetObjectData];
        NSArray *temp = [[DataManager sharedManager] getJSONData:[dt intValue]];
        NSDictionary *object = [temp objectAtIndex:1];
        [self setJsonData:object];
        [userDefault removeObjectForKey:@"indexMenu"];
    }
    self.tableView.tableFooterView = [[UIView alloc] init];
    NSString *img = [userDefault objectForKey:@"imgIcon"];
    _iconHeaderLV01.image = [UIImage imageNamed:img];
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestTypeSKLV01 = [dataParam valueForKey:@"request_type"];
    if(first){
        listAction = [self.jsonData objectForKey:@"action"];
        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
        [self.tableView reloadData];
    }
//    else if (fromHome){
//        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
//        [dataManager setAction:[self.jsonData valueForKey:@"action"] andMenuId:@"00007"];
//        [super openNextTemplate];
//    } else if (spesial){
//        [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
//        int ind = 0;
//        NSArray *menu = [self.jsonData valueForKey:@"action"];
//        for (int i =0; i < [menu count]; i++) {
//            NSArray *selMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:i];
//            if ([[selMenu objectAtIndex:0] isEqualToString:@"00047"]) {
//                ind = i;
//                break;
//            }
//        }
//        NSArray *selectedMenu = [[self.jsonData valueForKey:@"action"] objectAtIndex:ind];
//        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
//        [super openNextTemplate];
        //  [dataManager setAction:[[self.jsonData valueForKey:@"listAction"] objectAtIndex:2] andMenuId:@"wq"]
        //        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:3]];
//    }
    else {
        dataObject = [dataManager getObjectData];
        [self.labelTitle setText:[dataObject valueForKey:@"title"]];
        if([[dataObject valueForKey:@"url"]boolValue]){
            if([requestTypeSKLV01 isEqualToString:@"list_sukuk"]){
                
                [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
                [dataManager.dataExtra setValue:@"00077" forKey:@"code"];
                Connection *conn = [[Connection alloc]initWithDelegate:self];
                [conn sendPostParmUrl:[self.jsonData valueForKey:@"url_parm"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
                
            }

            id data =  [dataObject objectForKey:@"content"];
            mustAddToManager = true;
            listAction = data;
            [self.tableView reloadData];
        } else{
            id data =  [dataObject objectForKey:@"content"];
            if([data isKindOfClass:[NSString class]]){
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }else{
                mustAddToManager = true;
                listAction = data;
                [self.tableView reloadData];
            }
        }
    }
}

- (void)setDisplay {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    CGRect frmTable = self.tableView.frame;
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.labelTitle.frame;
    
    frmTable.size.width = screenWidth;
    frmTitle.size.width = screenWidth;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    frmLblTitle.origin.x = 8;
    frmTitle.size.height = 50;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.height = 40;
    
    self.tableView.frame = frmTable;
    self.vwTitle.frame = frmTitle;
    self.labelTitle.frame = frmLblTitle;
}

- (void)doneAction:(id)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAction.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV01Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        [label setText:[data valueForKey:@"title"]];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            [label setText:[data valueForKey:@"title"]];
            
        }else if([data objectForKey:@"name"]){
            [label setText:[data valueForKey:@"name"]];
            
        }
    }
    
    
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = @"";
    if(first){
        NSDictionary *data = [[listAction objectAtIndex:indexPath.row]objectAtIndex:1];
        text = [data valueForKey:@"title"];
    }else{
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        if([data objectForKey:@"title"]){
            text = [data valueForKey:@"title"];
            
        }else if([data objectForKey:@"name"]){
            text = [data valueForKey:@"name"];
        }
    }
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+20.0;
}
#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
    if(first){
        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
    }
    if(mustAddToManager){
        NSString *value = @"";
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        BOOL haveAction = false;
        for(NSString *a in data.allKeys){
            
//            if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00028"] && [a isEqualToString:@"type_transfer"]) {
//                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//                [userDefault setObject:[data valueForKey:a] forKey:@"typeTransfer"];
//            }
            
            if([a isEqualToString:@"action"]){
                haveAction = true;
            }else if(![a isEqualToString:@"title"] && ![a isEqualToString:@"name"]){
                key = a;
                value = [data valueForKey:key];
                break;
            }
            
        }
        
        lv_selected = value;
        [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
        if(haveAction){
            [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
            dataManager.currentPosition=-1;
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *last = [userDefault objectForKey:@"last"];
    if (![last isEqualToString:@"-"]) {
        [userDefault setValue:@"-" forKey:@"last"];
    }
    
    if(indexPath.row == 0 && [[dataManager.dataExtra valueForKey:@"status"]isEqualToString:@"REGISTER"]){
        dataManager.currentPosition=4;
        [super openNextTemplate];
    }else if([[dataManager.dataExtra valueForKey:@"status"]isEqualToString:@"ACTIVE"] && indexPath.row == 0){
        SKCF03ViewController *skcf03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SKCF03"];
        [skcf03 setMessage:message];
        
        if([Utility isLanguageID]){
            [skcf03 setButtonTitle:@"Pemesanan"];
        }else{
            [skcf03 setButtonTitle:@"Order"];
        }
        
        [self.navigationController pushViewController:skcf03 animated:YES];
    }else{
        [super openNextTemplate];
    }
        
    
}

- (void) gotoNode5{
    NSString *value = @"";
    NSDictionary *data = [listAction objectAtIndex:0];
    BOOL haveAction = false;
    for(NSString *a in data.allKeys){
        
        if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:@"00028"] && [a isEqualToString:@"type_transfer"]) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:[data valueForKey:a] forKey:@"typeTransfer"];
        }
        
        if([a isEqualToString:@"action"]){
            haveAction = true;
        }else if(![a isEqualToString:@"title"] && ![a isEqualToString:@"name"]){
            key = a;
            value = [data valueForKey:key];
            break;
        }
        
    }
    
    lv_selected = value;
    [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
    if(haveAction){
        [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
        dataManager.currentPosition=4;
    }
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *last = [userDefault objectForKey:@"last"];
    if (![last isEqualToString:@"-"]) {
        [userDefault setValue:@"-" forKey:@"last"];
    }
    
    [self openNextTemplate];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_sukuk"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *sts = [jsonObject valueForKey:@"status"];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"status":sts}];
            message = [jsonObject valueForKey:@"response"];
            
//            if([[jsonObject valueForKey:@"status"] isEqualToString:@"ACTIVE"]){
//                SKCF03ViewController *skcf03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SKCF03"];
//                [skcf03 setMessage:[jsonObject valueForKey:@"response"]];
//                [self.navigationController pushViewController:skcf03 animated:YES];
//            }
            
        }else if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"01"]){
            SKCF03ViewController *skcf03 = [self.storyboard instantiateViewControllerWithIdentifier:@"SKCF03"];
            [skcf03 setMessage:[jsonObject valueForKey:@"response"]];
            if([[jsonObject valueForKey:@"status"]isEqualToString:@"PENDING"]){
                [skcf03 setButtonTitle:@"OK"];
            }else{
                if([Utility isLanguageID]){
                    [skcf03 setButtonTitle:@"Pemesanan"];
                }else{
                    [skcf03 setButtonTitle:@"Order"];
                }
            }
            [self.navigationController pushViewController:skcf03 animated:YES];

        }else if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"02"]){
            NSString *sts = [jsonObject valueForKey:@"status"];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"status":sts}];
            [dataManager.dataExtra setValue:[jsonObject valueForKey:@"id_account"] forKey:@"id_account"];
            [dataManager.dataExtra setValue:[jsonObject valueForKey:@"mobile_no"] forKey:@"mobile_no"];
            [dataManager.dataExtra setValue:[jsonObject valueForKey:@"email"] forKey:@"email"];
            
//            [self gotoNode5];
        }else{
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];

            [alerts addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];

            [self presentViewController:alerts animated:YES completion:nil];
        }
    }
    
        
//        if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
//            if ([requestType isEqualToString:@"list_denom2"]) {
//                NSArray *listData = (NSArray *)[jsonObject valueForKey:@"response"];
//                NSMutableArray *newData = [[NSMutableArray alloc]init];
//                for(NSDictionary *temp in listData){
//                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
//                }
//                key = @"id_denom";
//                mustAddToManager = true;
//                listAction = newData;
//                [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject valueForKey:@"transaction_id"]}];
//                [self.tableView reloadData];
//            } else if ([requestType isEqualToString:@"list_sukuk"]) {
//                NSString *sts = [jsonObject valueForKey:@"status"];
//                [dataManager.dataExtra addEntriesFromDictionary:@{@"status":sts}];
//
//            } else {
//                if (![requestType isEqualToString:@"check_notif"]) {
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                    [alert show];
//                }
//
//            }
//        }
//        else {
//
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//        }
//    }else{
//        if([requestType isEqualToString:@"list_account"]){
//            NSArray *listData = (NSArray *)jsonObject;
//            NSMutableArray *newData = [[NSMutableArray alloc]init];
//            for(NSDictionary *temp in listData){
//                [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_account":[temp valueForKey:@"id_account"]}];
//            }
//            mustAddToManager = true;
//            key = @"id_account";
//            listAction = newData;
//            [self.tableView reloadData];
//        } else if([requestType isEqualToString:@"list_merchant"]){
//            NSArray *listData = (NSArray *)jsonObject;
//            NSMutableArray *newData = [[NSMutableArray alloc]init];
//            for(NSDictionary *temp in listData){
//                [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
//            }
//            if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00028"]){
//                key = @"id_merchant";
//            }else if([[self.jsonData valueForKey:@"menu_id"]isEqualToString:@"00025"]){
//                key = @"id_denom";
//            }else{
//                key = @"code";
//            }
//            mustAddToManager = true;
//            listAction = newData;
//            [self.tableView reloadData];
//        } else if([requestType isEqualToString:@"list_bank"]){
//            [self.view layoutIfNeeded];
//
//            NSArray *listData = (NSArray *)jsonObject;
//            NSMutableArray *newData = [[NSMutableArray alloc]init];
//            for(NSDictionary *temp in listData){
//                [newData addObject:@{@"title":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
//            }
//            key = @"code";
//            mustAddToManager = true;
//            listOrigin = newData;
//            listAction = newData;
//            [self.tableView reloadData];
//        } else if([requestType isEqualToString:@"list_nazhir"]){
//            [self.view layoutIfNeeded];
//
//            NSArray *listData = (NSArray *)jsonObject;
//            NSMutableArray *newData = [[NSMutableArray alloc]init];
//            for(NSDictionary *temp in listData){
//                [newData addObject:@{@"title":[temp valueForKey:@"name"],@"name":[temp valueForKey:@"name"],@"nazhir_id":[temp valueForKey:@"nazhir_id"]}];
//            }
//            key = @"nazhir_id";
//            mustAddToManager = true;
//            listOrigin = newData;
//            listAction = newData;
//            [self.tableView reloadData];
//        } else if([requestType isEqualToString:@"list_denom"]){
//            NSArray *listData = (NSArray *)jsonObject;
//            NSMutableArray *newData = [[NSMutableArray alloc]init];
//            for(NSDictionary *temp in listData){
//                [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
//            }
//            key = @"id_denom";
//            mustAddToManager = true;
//            listAction = newData;
//            [self.tableView reloadData];
//        } else if([requestType isEqualToString:@"list_denom2"]){
//            NSArray *listData = (NSArray *)jsonObject;
//            NSMutableArray *newData = [[NSMutableArray alloc]init];
//            for(NSDictionary *temp in listData){
//                [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
//            }
//            key = @"id_denom";
//            mustAddToManager = true;
//            listAction = newData;
//            [self.tableView reloadData];
//        } else if([requestType isEqualToString:@"list_sukuk"]){
//            NSString *sts = [jsonObject valueForKey:@"status"];
//            [dataManager.dataExtra addEntriesFromDictionary:@{@"status":sts}];
//        }
//    }
}

- (void)errorLoadData:(NSError *)error{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 657) {
        [self.view endEditing:YES];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == 657) {
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring
                     stringByReplacingCharactersInRange:range withString:string];
        
        if ([substring length] > 0) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for (NSDictionary *temp in listOrigin){
                NSRange substringRange = [[[temp objectForKey:@"title"] lowercaseString] rangeOfString:[substring lowercaseString]];
                if (substringRange.length != 0) {
                    [newData addObject:temp];
                }
            }
            
            listAction = (NSArray *)newData;
            [self.tableView reloadData];
        } else {
            listAction = listOrigin;
            [self.tableView reloadData];
        }
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        [self.view layoutIfNeeded];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 657) {
        [self.view layoutIfNeeded];
    }
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}
#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


/*
 {"status":"ACTIVE","response":"Alhamdulillah, data Anda telah tercatat di sistem ∂e-SBN Kementerian Keuangan. Berikut ini adalah detail rekening Anda :\n\nNama : RAFIKA SALSABILA\nSID : IDD20200517DC02\nSRE : BSM011030000DUMMY51\nBank Penyimpanan Surat Berharga : Kustodian Bukopin\n\nSelanjutnya anda dapat melanjutkan ke proses pemesanan.","transaction_id":"","response_code":"00"}
 2020-05-26 08:46:52.264963+0700 BSM Mobile Banking[5532:3096224] RESPONSE: {"status":"ACTIVE","response":"Alhamdulillah, data Anda telah tercatat di sistem e-SBN Kementerian Keuangan. Berikut ini adalah detail rekening Anda :\n\nNama : RAFIKA SALSABILA\nSID : IDD20200517DC02\nSRE : BSM011030000DUMMY51\nBank Penyimpanan Surat Berharga : Kustodian Bukopin\n\nSelanjutnya anda dapat melanjutkan ke proses pemesanan.","transaction_id":"","response_code":"00"}
 **/

@end
