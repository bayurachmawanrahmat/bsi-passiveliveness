//
//  GENCF02ViewController.h
//  BSM-Mobile
//
//  Created by Alikhsan on 08/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TemplateViewController.h"

@interface GENCF02ViewController : TemplateViewController

- (void)setDataLocal:(NSDictionary*)object;
- (void)setFromRecipt:(BOOL)recipt;
- (void)setFromAkad: (NSString *) fromAKad;
- (void)setFromTrx:(BOOL)fromTrx;


@end


