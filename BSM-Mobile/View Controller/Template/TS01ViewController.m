//
//  TS01ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 20/11/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "TS01ViewController.h"
#import "UIView+SimpleRipple.h"
#import "CellListScheduler.h"
#import "CellTS01.h"
#import "BottomModalViewController.h"
#import "Utility.h"



@interface TS01ViewController ()<ConnectionDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>{
    NSUserDefaults *userDefault;
    NSString *lang;
    NSArray *arDataScheduler;
    NSString *strSheduleId;
    NSString *strIdBCScheduler;
    BOOL flagBcScheduler;
    UIView *imgEmpty;
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblTransaksiTerjadwal;
@property (weak, nonatomic) IBOutlet UIButton *btnBackNav;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@end

@implementation TS01ViewController

- (void)loadView{
    [super loadView];
    
    CGRect frmVwTitle = self.vwTitle.frame;
    CGRect frmTblTransaksiTerjadwal = self.tblTransaksiTerjadwal.frame;
    
    CGRect frmBtnBack = self.btnBackNav.frame;
    CGRect frmLblTitle = self.lblTitle.frame;
    
    frmVwTitle.origin.x = 0;
    frmVwTitle.origin.y = TOP_NAV;
    frmVwTitle.size.width = SCREEN_WIDTH;
    
    frmTblTransaksiTerjadwal.origin.x = 0;
    frmTblTransaksiTerjadwal.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height;
    frmTblTransaksiTerjadwal.size.width = SCREEN_WIDTH;
    frmTblTransaksiTerjadwal.size.height = SCREEN_HEIGHT - frmTblTransaksiTerjadwal.origin.y - BOTTOM_NAV_DEF;
    if (IPHONE_X | IPHONE_XS_MAX) {
        frmTblTransaksiTerjadwal.size.height = SCREEN_HEIGHT - frmTblTransaksiTerjadwal.origin.y - BOTTOM_NAV_NOTCH;
    }
    
    frmLblTitle.origin.x = frmBtnBack.origin.x + frmBtnBack.size.width + 8;
    frmLblTitle.origin.y = frmVwTitle.size.height/2 - frmLblTitle.size.height/2;
//    frmLblTitle.size.width = frmVwTitle.size.width - frmLblTitle.origin.x - 16;
    frmLblTitle.size.width = frmVwTitle.size.width - ((frmBtnBack.origin.x + frmBtnBack.size.width + 8)*2);
   
    
    self.vwTitle.frame = frmVwTitle;
    self.tblTransaksiTerjadwal.frame = frmTblTransaksiTerjadwal;
    
    self.btnBackNav.frame = frmBtnBack;
    self.lblTitle.frame = frmLblTitle;
    
    [self addPlusButton];
    
    imgEmpty = [Utility showNoData:_tblTransaksiTerjadwal];
    imgEmpty.hidden = YES;
    [_tblTransaksiTerjadwal addSubview:imgEmpty];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    arDataScheduler = [[NSArray alloc]init];
    strSheduleId = nil;
    strIdBCScheduler = @"-";
    flagBcScheduler = true;
    
    self.lblTitle.text = [self.jsonData valueForKey:@"title"];
    self.lblTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblTitle.textAlignment = const_textalignment_title;
    self.lblTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.tblTransaksiTerjadwal registerNib:[UINib nibWithNibName:@"CellTS01" bundle:nil]
    forCellReuseIdentifier:@"CELLTS01"];
    [self.tblTransaksiTerjadwal setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    self.tblTransaksiTerjadwal.delegate = self;
    self.tblTransaksiTerjadwal.dataSource = self;
    [self.tblTransaksiTerjadwal beginUpdates];
    [self.tblTransaksiTerjadwal endUpdates];
    
    [self.btnBackNav addTarget:self action:@selector(actionSelectedTochDown:event:) forControlEvents:UIControlEventTouchDown];
    [self.btnBackNav addTarget:self action:@selector(actionBackNav:) forControlEvents:UIControlEventTouchUpInside];
    
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,id_account,pin", [self.jsonData valueForKey:@"url_parm"]] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(schedulerTransaction:)
        name:@"schedulerTransaction"
      object:nil];
    
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if ([requestType isEqualToString:@"inq_tfscheduled"]) {
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            NSError *error;
            NSDictionary *dataSheduler = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
            if (error == nil) {
                arDataScheduler = (NSArray *) [dataSheduler valueForKey:@"data"];
                [self.tblTransaksiTerjadwal reloadData];
                [self.tblTransaksiTerjadwal beginUpdates];
                [self.tblTransaksiTerjadwal endUpdates];
                
                if([_tblTransaksiTerjadwal numberOfRowsInSection:0] == 0){
                    imgEmpty.hidden = NO;
                }else{
                    imgEmpty.hidden = YES;
                }
            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 22;
            alert.delegate = self;
            [alert show];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    
}

- (void)reloadApp{
    [self backToRoot];
}

-(void) addPlusButton {
    UIButton *btnPlus = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnPlus.frame = CGRectMake(SCREEN_WIDTH - 70, SCREEN_HEIGHT- BOTTOM_NAV_DEF - 50, 50, 50);
    if ([Utility isDeviceHaveNotch]) {
        btnPlus.frame = CGRectMake(SCREEN_WIDTH - 70,SCREEN_HEIGHT -  BOTTOM_NAV_NOTCH - 50, 50, 50);
    }
    [btnPlus setImage:[UIImage imageNamed:@"baseline_add_white_36pt"] forState:UIControlStateNormal];
    btnPlus.backgroundColor = UIColorFromRGB(const_color_primary);
//    btnPlus.backgroundColor = UIColorFromRGB(const_color_secondary);
    btnPlus.layer.cornerRadius = btnPlus.frame.size.height/2;
    btnPlus.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    btnPlus.layer.shadowOffset = CGSizeMake(5, 5);
    btnPlus.layer.masksToBounds = YES;
    btnPlus.tintColor = [UIColor whiteColor];
    [btnPlus addTarget:self action:@selector(actionTambahScheduler:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnPlus];
    [self.view bringSubviewToFront:btnPlus];
    
    
}

-(void) actionTambahScheduler : (UIButton *) sender{
    [self getAction:0];
    [self goToNextVc];
}

-(void) getAction : (NSInteger) index{
    NSDictionary *dataObject = [dataManager getObjectData];
    NSArray *arContent = (NSArray *)[dataObject objectForKey:@"content"];
    NSDictionary *dictAction = [arContent objectAtIndex:index];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"code":[dictAction valueForKey:@"code"]}];
    [dataManager setAction:[dictAction objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
    dataManager.currentPosition=-1;
    
}


-(void) goToNextVc{
    @try {
        flagBcScheduler = false;
        [super openNextTemplate];
    } @catch (NSException *exception) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alertView.tag = 999; //tag no have menu
        [alertView show];
    }
}

-(void)actionSelectedTochDown : (UIButton *) sender
                        event : (UIEvent *) event{
    [self animateRipple:sender event:event];
    
}

-(void) animateRipple : (UIButton * ) sender
                 event: (UIEvent *) event{
    UITouch *touch = [[event touchesForView:sender] anyObject];
    CGPoint origin = [touch locationInView:sender];
    float radius = sender.frame.size.height;
    float duration = 0.5f;
    float fadeAfter = duration * 0.75f;
    
    [sender rippleStartingAt:origin withColor:[UIColor colorWithWhite:0.0f alpha:0.20f] duration:duration radius:radius fadeAfter:fadeAfter];
}


-(void) actionBackNav : (UIButton *) sender{
    [Utility goToBackNavigator:self.navigationController];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    CellListScheduler *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//
//    if(cell == nil){
//        cell = [[CellListScheduler alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    }
    
    CellTS01 *cell = (CellTS01*)[tableView dequeueReusableCellWithIdentifier:@"CELLTS01"];
    
    if(cell == nil){
        cell = [[CellTS01 alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CELLTS01"];
    }

        NSDictionary *dataDictScheduler = [arDataScheduler objectAtIndex:indexPath.row];
        cell.lblCustomerName.text = [dataDictScheduler valueForKey:@"beneficiary_name"];
        cell.lblCustomerAccount.text = [dataDictScheduler valueForKey:@"destaccno"];
        cell.lblCustomerAccount.textColor = UIColorFromRGB(const_color_darkgray);
        cell.lblNominal.text = [dataDictScheduler valueForKey:@"amount"];
        cell.lblNominal.textColor = UIColorFromRGB(const_color_secondary);
        cell.lblType.text = [dataDictScheduler valueForKey:@"frequent"];
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.lblType.textColor = UIColorFromRGB(const_color_darkgray);
    
//    NSDictionary *dataDictScheduler = [arDataScheduler objectAtIndex:indexPath.row];
//    cell.lblNama.text = [dataDictScheduler valueForKey:@"beneficiary_name"];
//    cell.lblNorek.text = [dataDictScheduler valueForKey:@"destaccno"];
//    cell.lblNominal.text = [dataDictScheduler valueForKey:@"amount"];
//    cell.lblNominal.textColor = UIColorFromRGB(const_color_secondary);
//    cell.lblTypeInterval.text = [dataDictScheduler valueForKey:@"frequent"];
//
//    [cell.lblNama sizeToFit];
//    [cell.lblNorek sizeToFit];
//    [cell.lblTypeInterval sizeToFit];
//    [cell.lblNominal sizeToFit];
//
//    CGRect frmLblNama = cell.lblNama.frame;
//    CGRect frmLblNorek = cell.lblNorek.frame;
//    CGRect frmLblInterval = cell.lblTypeInterval.frame;
//    CGRect frmImgNext = cell.imgNext.frame;
//    CGRect frmLblNominal = cell.lblNominal.frame;
//    CGFloat cellWidth = cell.frame.size.width;
//
//    frmLblNama.origin.x = 15;
//    frmLblNama.origin.y = 8;
//    frmLblNama.size.width = cellWidth - 15 - 8;
//
//    frmLblNorek.origin.x = frmLblNama.origin.x;
//    frmLblNorek.origin.y = frmLblNama.origin.y + frmLblNama.size.height + 5;
//    frmLblNorek.size.width = (cellWidth - frmLblNorek.origin.x)/2;
//
//    frmImgNext.origin.x = cellWidth - frmImgNext.size.width - 8;
//
//    frmLblInterval.origin.x = frmLblNorek.origin.x + frmLblNorek.size.width + 5;
//    frmLblInterval.origin.y = frmLblNorek.origin.y;
//    frmLblInterval.size.width = frmLblNorek.size.width - frmImgNext.size.width - 10;
//
//    frmImgNext.origin.y = frmLblInterval.origin.y + frmImgNext.size.height/2;
//
//    frmLblNominal.origin.x = frmLblNama.origin.x;
//    frmLblNominal.origin.y = frmLblNorek.origin.y + frmLblNorek.size.height + 5;
//    frmLblNominal.size.width = cellWidth - (frmLblNominal.origin.x *2);
//
//    cell.lblNama.frame = frmLblNama;
//    cell.lblNorek.frame = frmLblNorek;
//    cell.lblTypeInterval.frame = frmLblInterval;
//    cell.imgNext.frame = frmImgNext;
//    cell.lblNominal.frame = frmLblNominal;
//
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arDataScheduler.count;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 84;
//}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *nMessageDel, *btnYes, *btnNo;
    if([lang isEqualToString:@"id"]){
        nMessageDel = @"Apakah anda yakin ingin menghapus data?";
        btnYes = @"Iya";
        btnNo = @"Tidak";
    }else{
        nMessageDel = @"Are you sure want to delete data?";
        btnYes = @"Yes";
        btnNo = @"No";
    }
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:lang(@"DELETE") handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
        {
        NSDictionary *dataShcduler = [self->arDataScheduler objectAtIndex:indexPath.row];
        self->strSheduleId = [dataShcduler valueForKey:@"id"];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:nMessageDel delegate:self cancelButtonTitle:btnNo otherButtonTitles:btnYes, nil];
        alert.tag = 1;
        alert.delegate = self;
        [alert show];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [Utility resetTimer];
    NSDictionary *dataShcduler = [arDataScheduler objectAtIndex:indexPath.row];
    BottomModalViewController *bottomModalController = [self.storyboard instantiateViewControllerWithIdentifier:@"BTMMDL"];
    [bottomModalController setBundleExtras:@{@"bottom_type" : @"detail_scheduler",
                                             @"data" : dataShcduler
    }];
    [self presentViewController:bottomModalController animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            [self getAction:1];
            if (strSheduleId != nil) {
                [dataManager.dataExtra setValue:strSheduleId forKey:@"id_schedule"];
                [self goToNextVc];
            }
        }
        strSheduleId = nil;
    }else{
        [self backToRoot];
    }
}

- (void) schedulerTransaction:(NSNotification *) notification {
    if ([notification.name isEqualToString:@"schedulerTransaction"]) {
        NSDictionary* userInfo = notification.userInfo;
        NSString *actionType = [userInfo valueForKey:@"type"];
        if ([actionType isEqualToString:@"delete"]) {
            strSheduleId = [userInfo valueForKey:@"id_schedule"];
            if (strSheduleId != nil) {
                if (flagBcScheduler) {
                    [self getAction:1];
                    [dataManager.dataExtra setValue:strSheduleId forKey:@"id_schedule"];
                    strIdBCScheduler = strSheduleId;
                    [self goToNextVc];
                }
//                if (![strIdBCScheduler isEqualToString:strSheduleId]) {
//
//
//                }
//
            }
            
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
