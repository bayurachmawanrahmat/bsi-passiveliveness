//
//  TGINQViewController.m
//  BSM-Mobile
//
//  Created by Naufal Hilmi on 07/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGINQViewController.h"
#import "Validation.h"
#import "GCamOverlayView.h"
#import "CamOverlayController.h"

@interface TGINQViewController ()<GCamOverlayDelegate, UIDocumentPickerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    UIImagePickerController *imagePicker;
    NSString *state;
    NSUserDefaults *userDefault;
    NSString *language;
    Validation *validate;
    UIToolbar *toolbar;
    NSArray *listMargin;
    double hishahBank;
    int stepPhase;
    NSString *hishahBank2;
    NSInteger margin;
    NSString *path;
    NSString *btn;
    NSString *descriptionKTP;
    NSString *cameraTitleLabel;
    UIGestureRecognizer *tapper;
    NSString *alertMsg1;
    NSString *alertMsg2;
    NSString *alertMsg3;
    NSString *alertMsg4;
    NSString *alertMsg5;
    NSString *alertMsg6;
    NSString *alertMsg7;
    NSString *alertMsg8;
    NSString *lblformatKtp;
    NSString *size;
    NSString *code;
    NSString *limitPlafon2;
    NSInteger pdfSize;
    NSArray *listRent1;
    NSArray *listRent2;
    NSArray *listRent3;
    NSString *nisbahBank;
    NSString *nisbahNasabah;
    NSString *nisbahBank1;
    NSString *nisbahNasabah1;
    NSString *nisbahBank2;
    NSString *nisbahNasabah2;
    NSString *nObjek1;
    NSString *nObjek2;
    NSString *nObjek3;
    NSString *hishahNasbah1;
    NSString *hishahNasbah2;
    NSString *hishahNasbah3;
    NSInteger selectedSegment;
}

@end

@implementation TGINQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    stepPhase = 0;
    _titleBar.text = [self.jsonData valueForKey:@"title"];
    validate = [[Validation alloc]initWith:self];
    userDefault = [NSUserDefaults standardUserDefaults];
    language = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([language isEqualToString:@"id"]){
        [self.btnNext setTitle:@"SETUJU" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"BELUM" forState:UIControlStateNormal];
        [self.btnHitung setTitle:@"HITUNG" forState:UIControlStateNormal];
        [self.lblTlp setText:@"No Telpon Kantor"];
        [self.lblAset setText:@"Jenis Aset"];
        [self.lblObjek setText:@"Nilai Objek"];
        [self.lblhishahBank setText:@"Penawaran penjualan hishah kepada Bank"];
        [self.lblJangkaWaktu setText:@"Jangka Waktu"];
        [self.optionToggle setTitle:@"Ya" forSegmentAtIndex:0];
        [self.optionToggle setTitle:@"Tidak" forSegmentAtIndex:1];
        [self.lblJangkaWaktu2 setText:@"Jangka Waktu"];
        [self.lblSewa setText:@"Sewa Perbulan Periode:"];
        [self.lblPdfSK setText:@"Slip Gaji Terkahir & SK Kerja"];
        [self.lblPdfKtp setText:@"KTP"];
        lblformatKtp = @"(status menikah: foto KTP pemohon dan pasangan)";
        [self.lblformatSK setText:@"PDF Format,Maks 1MB"];
        self.TfMargin.placeholder = @"Pilih Margin";
        alertMsg1 = @"Maaf, KTP, Slip Gaji dan Surat Keterangan Kerja wajib di upload";
        alertMsg2 = @"Email wajib diisi";
        alertMsg3 = @"Maaf, nominal yang anda isi lebih dari penawaran atau nominal yang anda isi kurang dari Rp 50.000.000,00";
        alertMsg4 = @"Maaf, nomor telepon kantor harus menggunakan -";
        alertMsg5 = @"Maaf, ukuran PDF maksimal 1MB";
        alertMsg6 = @"Margin belum dipilih";
        alertMsg7 = @"Nomor telepon kantor wajib diisi";
        alertMsg8 = @"Dokumen yang anda masukan bukan pdf";
        descriptionKTP = @"Posisikan KTP Anda dalam kotak diatas dan pastikan hasil foto terbaca dengan jelas";
        cameraTitleLabel = @"KTP";
    }else{
        [self.lblSewa2 setText:@"Rent Per Month Period"];
        [self.lblHishah setText:@"Hishah To Bank"];
        [self.btnNext setTitle:@"APPLY" forState:UIControlStateNormal];
        [self.btnCancel setTitle:@"NOT YET" forState:UIControlStateNormal];
        [self.btnHitung setTitle:@"COUNT" forState:UIControlStateNormal];
        [self.lblTlp setText:@"Office Phone Number"];
        [self.lblAset setText:@"Asset Type"];
        [self.lblObjek setText:@"Object Value"];
        [self.lblhishahBank setText:@"Hishah Sales Offer To Bank"];
        [self.lblJangkaWaktu setText:@"Financing Period"];
        [self.optionToggle setTitle:@"Yes" forSegmentAtIndex:0];
        [self.optionToggle setTitle:@"No" forSegmentAtIndex:1];
        [self.lblJangkaWaktu2 setText:@"Financing Period"];
        [self.lblSewa setText:@"Rent Per Month Period:"];
        [self.lblPdfSK setText:@"Sallary slip & Certificate of Employment"];
        [self.lblPdfKtp setText:@"ID Card"];
        //[self.lblPdfKtp setText:@"ID Card"];
        lblformatKtp = @"(married status: photo of the applicant's and spouse's ID cards)";
        [self.lblformatSK setText:@"PDF Format,Max 1MB"];
        self.TfMargin.placeholder = @"Pilih Tujuan Pembiayan";
        self.TfMargin.placeholder = @"Choose Margin";
        alertMsg1 = @"ID Card, salary slip and certificate of employment must be uploaded";
        alertMsg2 = @"Email is required";
        alertMsg3 = @"the amount you fill is more that the offer or the amount you entered is less than IDR 50.000.000,00";
        alertMsg4 = @"Office telephone number must use -";
        alertMsg5 = @"the maximum PDF size is 1 MB";
        alertMsg6 = @"Margin is required";
        alertMsg7 = @"Office number is required";
        alertMsg8 = @"The document you entered is not a pdf";
        descriptionKTP = @"Put your KTP Card in the box and make sure the result is clear and can be read";
        cameraTitleLabel = @"KTP";
    }
    
    [self createToolbar];
    
    //style
    [self.TfTlp1 setKeyboardType:UIKeyboardTypeNumberPad];
    self.TfTlp1.delegate = self;
    [self.TfTlp2 setKeyboardType:UIKeyboardTypeNumberPad];
    self.TfTlp2.delegate = self;
    [self.TfEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    self.TfEmail.delegate = self;
    [self.TfHishah setKeyboardType:UIKeyboardTypeNumberPad];
    
    [Styles setStyleSegmentControl:_optionToggle];
    
    [_btnNext setEnabled:false];
    [self.btnCheck1 addTarget:self action:@selector(actionAgreeAkad:) forControlEvents:UIControlEventTouchUpInside];
    [_btnCheck1 setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    [_btnCheck1 setImage:[UIImage imageNamed:@"blank_check"]  forState:UIControlStateNormal];
    
    [self.btnCheck2 addTarget:self action:@selector(actionAgreeAkad:) forControlEvents:UIControlEventTouchUpInside];
    [_btnCheck2 setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateSelected];
    [_btnCheck2 setImage:[UIImage imageNamed:@"blank_check"]  forState:UIControlStateNormal];
    
    [_btnCancel.layer setCornerRadius:20.0f];
    [_btnCancel.layer setBorderWidth:1.0f];
    [_btnCancel.layer setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0].CGColor];
    [_btnCancel setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                                                         
    [_optionToggle.layer setCornerRadius:20.0f];
    [_optionToggle.layer setBorderWidth:1.0f];
    [_optionToggle.layer setBorderColor: (__bridge CGColorRef _Nullable)(UIColor.clearColor)];
    
    [self.fvViewHitung.layer setCornerRadius:10.0f];
    [self.fvViewHitung.layer setBorderWidth:1.0f];
    [self.fvViewHitung.layer setBorderColor: (__bridge CGColorRef _Nullable)(UIColor.clearColor)];
    
    [self.fvViewHasil.layer setCornerRadius:10.0f];
    [self.fvViewHasil.layer setBorderWidth:1.0f];
    [self.fvViewHasil.layer setBorderColor: [UIColor colorWithRed:146/255.0 green:146/255.0 blue:146/255.0 alpha:1].CGColor];
    
    listMargin = @[@"",@"Single Price",@"Step Up Price"];
    
    self.lblFormatKtp.lineBreakMode = NSLineBreakByWordWrapping;
    self.lblFormatKtp.numberOfLines = 0;
    [self.lblFormatKtp setText: lblformatKtp];
    [self.lblFormatKtp sizeToFit];
    
    [self createToolbarButton:self.TfMargin];
    [self createPicker:self.TfMargin withTag:0];
    
    
    [self createToolbarButton:self.TfEmail];
    [self createToolbarButton:self.TfTlp1];
    [self createToolbarButton:self.TfTlp2];
    [self createToolbarButton:self.TfHishah];
    [self createToolbarButton:self.TfJangkaWaktu];
    
    [super viewDidLoad];
    tapper = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)]; tapper.cancelsTouchesInView = NO;
        [self.view addGestureRecognizer:tapper];
    
    [self.TfHishah addTarget:self action:@selector(nominalTransactionDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.TfHishah addTarget:self action:@selector(endEdit:) forControlEvents:UIControlEventEditingChanged];

    [self.TfJangkaWaktu setEnabled:NO];
    [self.TfTlpStrip setEnabled:NO];
    [self.TfMargin addTarget:self action:@selector(endEdit:) forControlEvents:UIControlEventEditingDidEnd];
    [_btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [_btnHitung addTarget:self action:@selector(actionHitung) forControlEvents:UIControlEventTouchUpInside];
    [_btnUpload1 addTarget:self action:@selector(gotoCamera) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)actionCancel{
    [self backToR];
}


- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    state = @"inquiry";
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=topup_griya,step=inquiry,menu_id,device,device_type,ip_address,language,date_local,customer_id,app_no,transaction_id" needLoading:true encrypted:true banking:true favorite:nil];
    }

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"topup_griya"]){
        if([state isEqualToString:@"inquiry"]){
            if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                NSString *responses = [jsonObject objectForKey:@"response"];
                NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[responses dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
                NSString *nama = [dataDict objectForKey:@"nama"];
                nisbahBank = [dataDict objectForKey:@"nisbahBank"];
                nisbahNasabah = [dataDict objectForKey:@"nisbahNasabah"];
                NSString *nik = [dataDict objectForKey:@"noKtp"];
                NSString *status = [dataDict objectForKey:@"statusPernihakan"];
                NSString *email = [dataDict objectForKey:@"emailPemohon"];
                NSString *tlpKantor = [dataDict objectForKey:@"noTelpKantor"];
                NSArray *arr = [tlpKantor componentsSeparatedByString:@"-"];
                NSString *noWilayah = [arr objectAtIndex:0];
                NSString *noTlp = [arr objectAtIndex:1];
                
                NSString *aset = [dataDict objectForKey:@"jenisAset"];
                
                double nObjek = [[dataDict objectForKey:@"nilaiObjek"]doubleValue];
                nObjek1 = [dataDict objectForKey:@"nilaiObjek"];
                double osPemb = [[dataDict objectForKey:@"osPembiayaan"]doubleValue];
                NSString *osPemb2 = [dataDict objectForKey:@"osPembiayaan"];
                double hishahNasabah = [[dataDict objectForKey:@"hishahNasabah"]doubleValue];
                hishahNasbah1 = [dataDict objectForKey:@"hishahNasabah"];
                limitPlafon2 = [dataDict objectForKey:@"limitPlafon"];
                double limitPlaf = [[dataDict objectForKey:@"limitPlafon"]doubleValue];
                
                hishahBank = [[dataDict objectForKey:@"hishahBank"]doubleValue];
                hishahBank2 = [dataDict objectForKey:@"hishahBank"];
                NSString *jangkaWaktu = [dataDict objectForKey:@"tenor"];
                NSArray *tenor1 = [jangkaWaktu componentsSeparatedByString:@" "];
                NSString *tenor = [tenor1 objectAtIndex:0];
                
                listRent1 = [dataDict objectForKey:@"angsuran"];
                NSDictionary *listAngsuran = [listRent1 objectAtIndex:0];
                NSString *tahunKe = [listAngsuran objectForKey:@"tahun"];
                NSString *tahun = [listAngsuran objectForKey:@"label"];
                double jmlSewa = [[listAngsuran objectForKey:@"jumlah"]doubleValue];
               
                NSDictionary *textcont = [dataDict objectForKey:@"textContent"];
                NSString *titleSwitch = [textcont objectForKey:@"titleSwitch"];
                NSString *titleSimulate = [textcont objectForKey:@"titleSimulate"];
                NSArray *checkBox = [textcont objectForKey:@"checkbox"];
                NSString *check1 = [checkBox objectAtIndex:0];
                NSString *check2 = [checkBox objectAtIndex:1];
                
                [dataManager.dataExtra setValue:nama forKey:@"nama"];
                [dataManager.dataExtra setValue:aset forKey:@"jenisAset"];
                [dataManager.dataExtra setValue:tenor forKey:@"tenor"];
                [dataManager.dataExtra setValue:osPemb2 forKey:@"osPembiayaan"];
               
                
                
                self.lblResNik.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblResNik.numberOfLines = 0;
                self.lblResNik.attributedText = [Utility htmlAtributString:nik];
                [self.lblResNik setFont:[UIFont systemFontOfSize:14.0]];
                self.lblResNik.textAlignment = NSTextAlignmentRight;
                [self.lblResNik sizeToFit];
                
                self.TfEmail.attributedText = [Utility htmlAtributString:email];
                [self.TfEmail setFont:[UIFont systemFontOfSize:14.0]];
                self.TfEmail.textAlignment = NSTextAlignmentRight;
                
                [self.TfTlp1 setText:noWilayah];
                [self.TfTlp1 setFont:[UIFont systemFontOfSize:13.0]];
                self.TfTlp1.textAlignment = NSTextAlignmentRight;
                
                [self.TfTlpStrip setText:@"-"];
                
                [self.TfTlp2 setText:noTlp];
                [self.TfTlp2 setFont:[UIFont systemFontOfSize:13.0]];
                self.TfTlp2.textAlignment = NSTextAlignmentLeft;
                
                self.lblResAset.attributedText = [Utility htmlAtributString:aset];
                self.lblResAset.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblResAset.numberOfLines = 0;
                [self.lblResAset setFont:[UIFont systemFontOfSize:13.0]];
                self.lblResAset.textAlignment = NSTextAlignmentRight;
                [self.lblResAset sizeToFit];
                
                self.lblResObjek.attributedText = [Utility htmlAtributString:[Utility localFormatCurrencyValue:nObjek showSymbol:true]];
                self.lblResObjek.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblResObjek.numberOfLines = 0;
                [self.lblResObjek setFont:[UIFont systemFontOfSize:14.0]];
                self.lblResObjek.textAlignment = NSTextAlignmentRight;
                [self.lblResObjek sizeToFit];
                
                self.lblResHishahBank.attributedText = [Utility htmlAtributString:[Utility localFormatCurrencyValue:hishahBank showSymbol:true]];
                self.lblResHishahBank.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblResHishahBank.numberOfLines = 0;
                [self.lblResHishahBank setFont:[UIFont systemFontOfSize:14.0]];
                self.lblResHishahBank.textAlignment = NSTextAlignmentRight;
                [self.lblResHishahBank sizeToFit];
                
                self.lblResJangkaWaktu.attributedText = [Utility htmlAtributString:jangkaWaktu];
                [self.TfJangkaWaktu setText: jangkaWaktu];
                self.lblResJangkaWaktu.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblResJangkaWaktu.numberOfLines = 0;
                [self.lblResJangkaWaktu setFont:[UIFont systemFontOfSize:14.0]];
                self.lblResJangkaWaktu.textAlignment = NSTextAlignmentRight;
                [self.lblResJangkaWaktu sizeToFit];
                
                self.lblTahunSewa.attributedText = [Utility htmlAtributString:tahun];
                self.lblTahunSewa.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblTahunSewa.numberOfLines = 0;
                [self.lblTahunSewa setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                [self.lblTahunSewa setFont:[UIFont systemFontOfSize:14.0]];
                self.lblTahunSewa.textAlignment = NSTextAlignmentLeft;
                [self.lblTahunSewa sizeToFit];
                
                self.lblTahunke.attributedText = [Utility htmlAtributString:tahunKe];
                self.lblTahunke.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblTahunke.numberOfLines = 0;
                [self.lblTahunke setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                [self.lblTahunke setFont:[UIFont systemFontOfSize:14.0]];
                self.lblTahunke.textAlignment = NSTextAlignmentLeft;
                [self.lblTahunke sizeToFit];
                
                self.lblResSewa.attributedText = [Utility htmlAtributString:[Utility localFormatCurrencyValue:jmlSewa showSymbol:true]];
                self.lblResSewa.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblResSewa.numberOfLines = 0;
                [self.lblResSewa setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                [self.lblResSewa setFont:[UIFont systemFontOfSize:14.0]];
                self.lblResSewa.textAlignment = NSTextAlignmentLeft;
                [self.lblResSewa sizeToFit];
                
                self.lbljudul1.attributedText = [Utility htmlAtributString:titleSwitch];
                self.lbljudul1.lineBreakMode = NSLineBreakByWordWrapping;
                self.lbljudul1.numberOfLines = 0;
                [self.lbljudul1 setFont:[UIFont systemFontOfSize:15.0]];
                self.lbljudul1.textAlignment = NSTextAlignmentJustified;
                [self.lbljudul1 sizeToFit];
                
                self.lbljudul2.attributedText = [Utility htmlAtributString:titleSimulate];
                self.lbljudul2.lineBreakMode = NSLineBreakByWordWrapping;
                self.lbljudul2.numberOfLines = 0;
                [self.lbljudul2 setFont:[UIFont systemFontOfSize:15.0]];
                self.lbljudul2.textAlignment = NSTextAlignmentJustified;
                [self.lbljudul2 sizeToFit];
                
                self.lblcheck1.attributedText = [Utility htmlAtributString:check1];
                self.lblcheck1.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblcheck1.numberOfLines = 0;
                [self.lblcheck1 setFont:[UIFont systemFontOfSize:15.0]];
                self.lblcheck1.textAlignment = NSTextAlignmentJustified;
                [self.lblcheck1 sizeToFit];
                
                self.lblCheck2.attributedText = [Utility htmlAtributString:check2];
                self.lblCheck2.lineBreakMode = NSLineBreakByWordWrapping;
                self.lblCheck2.numberOfLines = 0;
                [self.lblCheck2 setFont:[UIFont systemFontOfSize:15.0]];
                self.lblCheck2.textAlignment = NSTextAlignmentJustified;
                [self.lblCheck2 sizeToFit];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self backToR];
                }]];
                if (@available(iOS 13.0, *)) {
                    [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [self presentViewController:alert animated:YES completion:nil];
            }
        }if([requestType isEqualToString:@"topup_griya"]){
            if([state isEqualToString:@"simulate"]){
                if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
                    NSString *responses = [jsonObject objectForKey:@"response"];
                    NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[responses dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    if (margin == 1){
                    listRent2 = [dataDict objectForKey:@"angsuran"];
                    NSDictionary *listAngsuran = [listRent2 objectAtIndex:0];
                    NSString *tahunAng = [listAngsuran objectForKey:@"tahun"];
                    NSString *tahunKeAng = [listAngsuran objectForKey:@"label"];
                    nisbahBank1 = [dataDict objectForKey:@"nisbahBank"];
                    nisbahNasabah1 = [dataDict objectForKey:@"nisbahNasabah"];
                    hishahNasbah2 = [dataDict objectForKey:@"hishahNasabah"];
                    nObjek2 = [dataDict objectForKey:@"nilaiObjek"];
                    
                    double jmlSewaAng = [[listAngsuran objectForKey:@"jumlah"]doubleValue];
                        
                        [dataManager.dataExtra setValue:listRent2 forKey:@"angsuran"];
                        self.lblTahun1.attributedText = [Utility htmlAtributString:tahunKeAng];
                        self.lblTahun1.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblTahun1.numberOfLines = 0;
                        [self.lblTahun1 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblTahun1 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblTahun1.textAlignment = NSTextAlignmentLeft;
                        [self.lblTahun1 sizeToFit];
                        
                        self.lblTahunke1.attributedText = [Utility htmlAtributString:tahunAng];
                        self.lblTahunke1.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblTahunke1.numberOfLines = 0;
                        [self.lblTahunke1 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblTahunke1 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblTahunke1.textAlignment = NSTextAlignmentLeft;
                        [self.lblTahunke1 sizeToFit];
                        
                        self.lblJumlah1.attributedText = [Utility htmlAtributString:[Utility localFormatCurrencyValue:jmlSewaAng showSymbol:true]];
                        self.lblJumlah1.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblJumlah1.numberOfLines = 0;
                        [self.lblJumlah1 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblJumlah1 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblJumlah1.textAlignment = NSTextAlignmentLeft;
                        [self.lblJumlah1 sizeToFit];
                        
                        [self.lblTahun2 setText:@""];
                        [self.lblTahunke2 setText:@""];
                        [self.lblHumlah2 setText:@""];
                       
                    }else{
                    listRent3 = [dataDict objectForKey:@"angsuran"];
                    nisbahBank2 = [dataDict objectForKey:@"nisbahBank"];
                    nisbahNasabah2 = [dataDict objectForKey:@"nisbahNasabah"];
                    nObjek3 = [dataDict objectForKey:@"nilaiObjek"];
                    hishahNasbah3 = [dataDict objectForKey:@"hishahNasabah"];
                    NSDictionary *listAngsuran = [listRent3 objectAtIndex:0];
                    NSString *tahunAng = [listAngsuran objectForKey:@"tahun"];
                    NSString *tahunKeAng = [listAngsuran objectForKey:@"label"];
                    double jmlSewaAng = [[listAngsuran objectForKey:@"jumlah"]doubleValue];
                    NSDictionary *listAngsuran2 = [listRent3 objectAtIndex:1];
                    NSString *tahun2 = [listAngsuran2 objectForKey:@"tahun"];
                    NSString *tahunKe2 = [listAngsuran2 objectForKey:@"label"];
                    double jmlSewa2 = [[listAngsuran2 objectForKey:@"jumlah"]doubleValue];
                        
                        [dataManager.dataExtra setValue:listRent3 forKey:@"angsuran"];
                        self.lblTahun1.attributedText = [Utility htmlAtributString:tahunKeAng];
                        self.lblTahun1.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblTahun1.numberOfLines = 0;
                        [self.lblTahun1 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblTahun1 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblTahun1.textAlignment = NSTextAlignmentLeft;
                        [self.lblTahun1 sizeToFit];
                        
                        self.lblTahunke1.attributedText = [Utility htmlAtributString:tahunAng];
                        self.lblTahunke1.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblTahunke1.numberOfLines = 0;
                        [self.lblTahunke1 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblTahunke1 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblTahunke1.textAlignment = NSTextAlignmentLeft;
                        [self.lblTahunke1 sizeToFit];
                        
                        self.lblJumlah1.attributedText = [Utility htmlAtributString:[Utility localFormatCurrencyValue:jmlSewaAng showSymbol:true]];
                        self.lblJumlah1.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblJumlah1.numberOfLines = 0;
                        [self.lblJumlah1 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblJumlah1 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblJumlah1.textAlignment = NSTextAlignmentLeft;
                        [self.lblJumlah1 sizeToFit];
                        
                        self.lblTahun2.attributedText = [Utility htmlAtributString:tahunKe2];
                        self.lblTahun2.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblTahun2.numberOfLines = 0;
                        [self.lblTahun2 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblTahun2 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblTahun2.textAlignment = NSTextAlignmentLeft;
                        [self.lblTahun2 sizeToFit];
                        
                        self.lblTahunke2.attributedText = [Utility htmlAtributString:tahun2];
                        self.lblTahunke2.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblTahunke2.numberOfLines = 0;
                        [self.lblTahunke2 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblTahunke2 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblTahunke2.textAlignment = NSTextAlignmentLeft;
                        [self.lblTahunke2 sizeToFit];
                        
                        self.lblHumlah2.attributedText = [Utility htmlAtributString:[Utility localFormatCurrencyValue:jmlSewa2 showSymbol:true]];
                        self.lblHumlah2.lineBreakMode = NSLineBreakByWordWrapping;
                        self.lblHumlah2.numberOfLines = 0;
                        [self.lblHumlah2 setTextColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
                        [self.lblHumlah2 setFont:[UIFont systemFontOfSize:14.0]];
                        self.lblHumlah2.textAlignment = NSTextAlignmentLeft;
                        [self.lblHumlah2 sizeToFit];
                        
                    }
                   
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                        [self backToR];
                    }]];
                    if (@available(iOS 13.0, *)) {
                        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                    }
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        }
    }
}

-(void)addAccessoryKeypad{
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    _TfHishah.inputAccessoryView = keyboardDoneButtonView;
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) createToolbar{
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,50)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return listMargin.count;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return listMargin[row];
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        self.TfMargin.text = listMargin[row];
    }
}

- (void)showErrorAlert:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert show];
}

- (void)nominalTransactionDidChange:(UITextField *) sender {
    double amountInput = [[self stringNominalReplace:sender.text] doubleValue];
    sender.text = [Utility localFormatCurrencyValue:amountInput showSymbol:true];
}

- (NSString *)stringNominalReplace:(NSString *) txtString{
    NSString *tfR1 = [txtString stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *tfR2 = [tfR1 stringByReplacingOccurrencesOfString:@"Rp " withString:@""];
    return tfR2;
}


- (void)stateSimulate{
        state = @"simulate";
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:@"request_type=topup_griya,step=simulate,app_no,kodeSkemaPrice,tenor,limitPlafon,transaction_id" needLoading:true encrypted:true banking:true favorite:nil];
}

- (void)endEdit:(UITextField*)sender{
    if(sender == _TfMargin){
        if([sender.text isEqualToString:@"Single Price"]){
            [dataManager.dataExtra setValue:@"1" forKey:@"kodeSkemaPrice"];
            [dataManager.dataExtra setValue:sender.text forKey:@"skemaPrice"];
            margin = 1;
        }else{
            margin = 2;
            [dataManager.dataExtra setValue:@"2" forKey:@"kodeSkemaPrice"];
            [dataManager.dataExtra setValue:sender.text forKey:@"skemaPrice"];
        }
    }
    else if (selectedSegment == 0){
        if(sender == _TfHishah){
            [dataManager.dataExtra setValue:[self stringNominalReplace:_TfHishah.text] forKey:@"limitPlafon"];
            [dataManager.dataExtra setValue:[self stringNominalReplace:_TfHishah.text] forKey:@"hishahBank"];
            [dataManager.dataExtra setValue:nisbahNasabah1 forKey:@"nisbahNasabah"];
            [dataManager.dataExtra setValue:nisbahBank1 forKey:@"nisbahBank"];
        }
    }else{
        [dataManager.dataExtra setValue:hishahBank2 forKey:@"hishahBank"];
        [dataManager.dataExtra setValue:limitPlafon2 forKey:@"limitPlafon"];
        [dataManager.dataExtra setValue:nisbahNasabah2 forKey:@"nisbahNasabah"];
        [dataManager.dataExtra setValue:nisbahBank2 forKey:@"nisbahBank"];
    }
}

- (void)actionHitung{
    if([self isFilled]){
        [self stateSimulate];
    }
}

- (void)actionNext{
    if([self isFilled2]){
        NSString *notelpon = [NSString stringWithFormat:@"%@%@%@",_TfTlp1.text,_TfTlpStrip.text,_TfTlp2.text];
        [dataManager.dataExtra setValue:notelpon forKey:@"noTelpKantor"];
        [dataManager.dataExtra setValue:_TfEmail.text forKey:@"emailPemohon"];
        [self openNextTemplate];
    }
}


-(BOOL)isFilled{
    NSString *alertMessage = @"";
    int angka = 50000000;
    NSString *hisha = [self stringNominalReplace:hishahBank2];
    NSInteger hisha2 = [hisha integerValue];
    NSString *tes = [self stringNominalReplace:_TfHishah.text];
    NSInteger tes2 = [tes integerValue];
    if((tes2 > hisha2) || (tes2 < angka)){
        alertMessage = alertMsg3;
    }else if([_TfMargin.text isEqualToString:@""]){
        alertMessage = alertMsg6;
    }
    if(![alertMessage isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
  
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if(textField == _TfTlp1){
        int maxLength = 4;
        if(newText.length > maxLength){
            return NO;
        }
    }else if(textField == _TfTlp2){
        int maxLength = 10;
        if(newText.length > maxLength){
            return NO;
        }
    }else if (textField == _TfEmail){
        int maxLength = 50;
        if(newText.length > maxLength){
            return NO;
        }
    }
    
    return YES;
}


-(BOOL)isFilled2{
    NSString *alertMessage = @"";
    if([_lblPdfKtp.text isEqualToString:@"KTP"] || [_lblPdfKtp.text isEqualToString:@"ID Card"]){
        alertMessage = alertMsg1;
    }
    else if ([_lblPdfSK.text isEqualToString:@"Slip Gaji Terkahir & SK Kerja"] || [_lblPdfSK.text isEqualToString:@"Sallary slip & Certificate of Employment"]){
        alertMessage = alertMsg1;
    }
    else if ([_TfTlp1.text isEqualToString:@""]){
        alertMessage = alertMsg2;
    }
    else if(![validate validateEmail:self.TfEmail.text]){
         [self.TfEmail setText:@""];
         alertMessage = lang(@"INVALID_EMAIL");
    }
    else if(pdfSize > 1000000){
        alertMessage = alertMsg5;
    }
    else if ([_TfTlp2.text isEqualToString:@""]){
        alertMessage = alertMsg2;
    }
    if (!([code isEqualToString:@"pdf"] || [code isEqualToString:@"PDF"])){
        alertMessage = alertMsg8;
    }

    if(![alertMessage isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:alertMessage preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
[self selectedSegment];
 return YES;
}

- (void)selectedSegment{
    if(selectedSegment == 0){
        if(margin == 1){
            [dataManager.dataExtra setValue:nisbahNasabah1 forKey:@"nisbahNasabah"];
            [dataManager.dataExtra setValue:nisbahBank1 forKey:@"nisbahBank"];
            [dataManager.dataExtra setValue:nObjek2 forKey:@"nilaiObjek"];
            [dataManager.dataExtra setValue:hishahNasbah2 forKey:@"hishahNasabah"];
        }else{
            [dataManager.dataExtra setValue:nisbahNasabah2 forKey:@"nisbahNasabah"];
            [dataManager.dataExtra setValue:nisbahBank2 forKey:@"nisbahBank"];
            [dataManager.dataExtra setValue:nObjek3 forKey:@"nilaiObjek"];
            [dataManager.dataExtra setValue:hishahNasbah3 forKey:@"hishahNasabah"];
        }
    }else{
        [dataManager.dataExtra setValue:nisbahNasabah forKey:@"nisbahNasabah"];
        [dataManager.dataExtra setValue:nisbahBank forKey:@"nisbahBank"];
        [dataManager.dataExtra setValue:nObjek1 forKey:@"nilaiObjek"];
        [dataManager.dataExtra setValue:hishahNasbah1 forKey:@"hishahNasabah"];
    }
}



- (IBAction)actionSegmentControl:(UISegmentedControl*)sender {
    selectedSegment = sender.selectedSegmentIndex;
    if(selectedSegment == 0){
        self.TfHishah.enabled = YES;
        self.TfJangkaWaktu.enabled = NO;
        self.TfMargin.enabled = YES;
        self.btnHitung.enabled = YES;
        [self.fvViewHitung setHidden:NO];
        [self.fvViewSegment setHidden:NO];
        [self.fvViewHishah setHidden:NO];
        [self.fvViewTenor setHidden:NO];
        [self.fvViewMargin setHidden:NO];
        self.heightVwSegment.constant = 653;
    }else{
        [self resetFieldHitung];
        self.TfHishah.enabled = NO;
        self.TfJangkaWaktu.enabled = NO;
        self.TfMargin.enabled = NO;
        self.btnHitung.enabled = NO;
        [self.fvViewHitung setHidden:YES];
        [self.fvViewSegment setHidden:NO];
        [self.fvViewHishah setHidden:YES];
        [self.fvViewTenor setHidden:YES];
        [self.fvViewMargin setHidden:YES];
        self.heightVwSegment.constant = 108;
        [dataManager.dataExtra setValue:listRent1 forKey:@"angsuran"];
        [dataManager.dataExtra setValue:nObjek1 forKey:@"nilaiObjek"];
        [dataManager.dataExtra setValue:hishahBank2 forKey:@"hishahBank"];
        [dataManager.dataExtra setValue:@"Single Price" forKey:@"skemaPrice"];
        [dataManager.dataExtra setValue:@"1" forKey:@"kodeSkemaPrice"];
        [dataManager.dataExtra setValue:limitPlafon2 forKey:@"limitPlafon"];
        [dataManager.dataExtra setValue:nisbahNasabah forKey:@"nisbahNasabah"];
        [dataManager.dataExtra setValue:nisbahBank forKey:@"nisbahBank"];
        [dataManager.dataExtra setValue:hishahNasbah1 forKey:@"hishahNasabah"];
        
    }
}

-(void)resetFieldHitung{
    self.TfHishah.text = @"";
    self.TfMargin.text = @"";
}

-(void)actionAgreeAkad: (UIButton *) sender{
 if(sender == self.btnCheck1){
        [self.btnCheck1 setSelected:!self.btnCheck1.isSelected];
    }else if(sender == self.btnCheck2){
        [self.btnCheck2 setSelected:!self.btnCheck2.isSelected];
    }
    
    if(stepPhase == 0){
        if (self.btnCheck1.isSelected && self.btnCheck2.isSelected) {
            [_btnNext setEnabled:true];
        }else{
            [_btnNext setEnabled:false];
        }
    }
}

//importDoc
- (void) gotoCamera {
    btn = @"1";
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    
    CamOverlayController *overlayController = [[CamOverlayController alloc] init];

    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    imagePicker.showsCameraControls = false;
    imagePicker.allowsEditing = false;

    GCamOverlayView *cameraView = (GCamOverlayView *)overlayController.view;
    cameraView.frame = imagePicker.view.frame;
    cameraView.descriptionLabel.text = descriptionKTP;
    cameraView.imageOverlay.image = [UIImage imageNamed:@""];
    cameraView.titleLabel.text = cameraTitleLabel;
    
    cameraView.delegate = self;
    
    imagePicker.cameraOverlayView = cameraView;
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)didTakePhoto:(GCamOverlayView *)overlayView{
    [imagePicker takePicture];
}

- (void)didBack:(GCamOverlayView *)overlayView{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

- (void)switchCameraDevice:(GCamOverlayView *)overlayView{
    if(imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceRear)
    {
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    }else{
        [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceRear];
    }
}
    
- (IBAction)openImportDocumentPicker2:(id)sender {
    btn = @"2";
    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.data"] inMode:UIDocumentPickerModeImport];
    documentPicker.delegate = self;
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    if([btn isEqualToString:@"1"]){
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSString *base64Imge = [Utility compressAndEncodeToBase64String:image withMaxSizeInKB:1000];
    
    [dataManager.dataExtra setValue:base64Imge forKey:@"imgKtp"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    [_lblPdfKtp setText:[NSString stringWithFormat:@"KTP_%@.jpg", [dateFormatter stringFromDate:[NSDate date]]]];
    [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

//HandleDoc
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *) url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        NSData *fileData = [NSData dataWithContentsOfURL:url];
        NSString *name = [url lastPathComponent];
        code = [name substringFromIndex: [name length] - 3];
        NSString *string = [fileData base64EncodedStringWithOptions:kNilOptions];
        NSUInteger len = [fileData length];
        pdfSize = len;
        [dataManager.dataExtra setValue:string forKey:@"imgSlipGaji"];
        self.lblPdfSK.text = [url lastPathComponent];
        [self.TfTlp2 setText:@""];
        [self.TfEmail setText:@""];
    }
}

@end

