//
//  OAMD01ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 18/06/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "OAMD01ViewController.h"
#import "Utility.h"
#import "Connection.h"

@interface OAMD01ViewController () <ConnectionDelegate, UIAlertViewDelegate, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
    NSString *requestType;
    NSString *stateCheckedOne, *stateCheckedTwo, *stateCheckedZakat;
    bool checkedViewLink;
    NSString *mnId;
    NSString *urlSyarat;
    NSArray *listFintech;
    
    
}
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMenuTitle;

@property (weak, nonatomic) IBOutlet UIView *vwButton;
@property (weak, nonatomic) IBOutlet CustomBtn *btnSutuju;
@property (weak, nonatomic) IBOutlet CustomBtn *btnBatal;


@property (weak, nonatomic) IBOutlet UIButton *btnCheckOne;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckZakat;
@property (weak, nonatomic) IBOutlet UIButton *btnOpnWeb;


@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIView *vwInnerScroll;
@property (weak, nonatomic) IBOutlet UILabel *lblText;


@property (weak, nonatomic) IBOutlet UIView *vwCheckOne;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckOne;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckOne;

@property (weak, nonatomic) IBOutlet UIView *vwKetentuan;
@property (weak, nonatomic) IBOutlet UILabel *lblKetentuan;

@property (weak, nonatomic) IBOutlet UIView *vwCheckTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckTwo;
@property (weak, nonatomic) IBOutlet UILabel *lblCheckTwo;

@property (weak, nonatomic) IBOutlet UIView *vwSetoran;
@property (weak, nonatomic) IBOutlet UILabel *lblStoran;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldStoran;
@property (weak, nonatomic) IBOutlet UIView *vwLineStoran;

@property (weak, nonatomic) IBOutlet UIView *vwEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (weak, nonatomic) IBOutlet UIView *vwLineEmail;

@property (weak, nonatomic) IBOutlet UIView *vwZakat;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckZakat;
@property (weak, nonatomic) IBOutlet UILabel *lblZakat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

- (IBAction)actionCheckOne:(id)sender;
- (IBAction)actionCheckTwo:(id)sender;
- (IBAction)actionZakat:(id)sender;
- (IBAction)actionSetuju:(id)sender;
- (IBAction)actionBatal:(id)sender;
- (IBAction)actionOpenWeb:(id)sender;

@end

@implementation OAMD01ViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark change cancel to selanjutnya and other else
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _vwScroll.delegate = self;
    
    stateCheckedOne = @"N";
    stateCheckedTwo = @"N";
    stateCheckedZakat = @"N";
    checkedViewLink = false;
    
    [Styles setTopConstant:_topConstant];
    self.lblCheckOne.numberOfLines = 0;
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    mnId = [self.jsonData valueForKey:@"menu_id"];
    requestType = [dataParam valueForKey:@"request_type"];
    
    [self.lblMenuTitle setText:[self.jsonData valueForKey:@"title"]];
    
    self.lblMenuTitle.textColor = UIColorFromRGB(const_color_title);
    self.lblMenuTitle.textAlignment = const_textalignment_title;
    self.lblMenuTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblMenuTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
   
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    if([lang isEqualToString:@"id"]){
        [_lblKetentuan setText:@"Syarat dan Ketentuan (Wajib di klik)"];
        [_lblCheckTwo setText:@"Saya telah membaca Syarat Umum Pembukaan Rekening dan Setuju"];
//        [_btnBatal setTitle:@"Batal" forState:UIControlStateNormal];
//        [_btnSutuju setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Setuju" forState:UIControlStateNormal];
        [_btnSutuju setTitle:@"Batal" forState:UIControlStateNormal];
        [_lblZakat setText:@"Zakat atas bagi hasil"];
    }else{
        [_lblKetentuan setText:@"Terms and Conditions (Must be clicked)"];
        [_lblCheckTwo setText:@"I have read the General Terms of Account Opening and Agreement"];
//        [_btnBatal setTitle:@"Cancel" forState:UIControlStateNormal];
//        [_btnSutuju setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnBatal setTitle:@"Agree" forState:UIControlStateNormal];
        [_btnSutuju setTitle:@"Cancel" forState:UIControlStateNormal];
        [_lblStoran setText:@"Beginning Balance"];
        [_lblZakat setText:@"Zakat for profit sharing"];
    }
    
    [_btnBatal setColorSet:PRIMARYCOLORSET];
    [_btnSutuju setColorSet:SECONDARYCOLORSET];
    
    
    [self setupLayout];
//    [self viewVisibility];
    [self viewIsEnabled];
    
    if([mnId isEqualToString:@"00023"]){
        [_vwZakat setHidden:true];
    }else{
        stateCheckedZakat = @"Y";
        [_vwZakat setHidden:false];
        _imgCheckZakat.image = [UIImage imageNamed:@"checked.png"];
        _imgCheckZakat.image = [_imgCheckZakat.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _imgCheckZakat.tintColor = UIColorFromRGB(const_color_primary);
    }
    
//    NSString *email = [userDefault objectForKey:@"email"];
    NSString *email = [NSUserdefaultsAes getValueForKey:@"email"];
    if ((email == nil) || ([email isEqualToString:@""])) {
        NSDictionary* userInfo = @{@"position": @(713)};
        NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
        
    }else{
        [self.txtFieldEmail setText:email];
        [self.txtFieldEmail setEnabled:NO];
    }
    
    if(!checkedViewLink){
        [_btnCheckTwo setEnabled:false];
        [_lblCheckTwo setTextColor:[UIColor grayColor]];
    }
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    [self registerForKeyboardNotifications];
    
    self.txtFieldStoran.inputAccessoryView = keyboardDoneButtonView;
    
    if ([mnId isEqualToString:@"00035"]){
        [self createToolbarButton:self.txtFieldStoran];
        [self createPicker:self.txtFieldStoran withTag:0];
        [_lblZakat setHidden: true];
        [_imgCheckZakat setHidden: true];
        if([lang isEqualToString:@"id"]){
            [self.txtFieldStoran setPlaceholder:@"Pilih Fintech"];
            [_lblStoran setText:@"Fintech"];
        }else{
            [_lblStoran setText:@"Fintech"];
            [self.txtFieldStoran setPlaceholder:@"Select Fintech"];
        }
    }else{
        [self.txtFieldStoran setKeyboardType:UIKeyboardTypeNumberPad];
        [_lblStoran setText:@"Setoran Awal"];
        [self.txtFieldStoran setPlaceholder:@"0"];
        [_lblZakat setHidden: false];
        [_imgCheckZakat setHidden: false];
        self.txtFieldStoran.inputAccessoryView = keyboardDoneButtonView;
    }
    
    NSDictionary* userInfo = @{@"position": @(1113)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    NSString *url =[NSString stringWithFormat:@"%@,zakat",[self.jsonData valueForKey:@"url_parm"]];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [self.vwScroll setContentSize:CGSizeMake(self.vwInnerScroll.frame.size.width, self.vwInnerScroll.frame.size.height + kbSize.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height/2, 0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.vwScroll setContentSize:CGSizeMake(self.vwInnerScroll.frame.size.width, self.vwInnerScroll.frame.size.height)];
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setupLayout{
    
//    //view parent
//    CGRect frmVwTitle = self.vwTitle.frame;
//    CGRect frmLblTitle = self.lblMenuTitle.frame;
//    CGRect frmLblTxt = self.lblText.frame;
//    CGRect frmVwScroll = self.vwScroll.frame;
//    CGRect frmVwBtn = self.vwButton.frame;
//
//    //view child
//    CGRect frmVwInner = self.vwInnerScroll.frame;
//    CGRect frmVwCheckOne = self.vwCheckOne.frame;
//    CGRect frmVwKetentuan = self.vwKetentuan.frame;
//    CGRect frmVwCheckTwo = self.vwCheckTwo.frame;
//    CGRect frmVwStoran = self.vwSetoran.frame;
//    CGRect frmVwEmail = self.vwEmail.frame;
//    CGRect frmVwZakat = self.vwZakat.frame;
//    CGRect frmBtnCheckOne = self.btnCheckOne.frame;
//    CGRect frmBtnKetentuan = self.btnOpnWeb.frame;
//    CGRect frmBtnCheckTwo  = self.btnCheckTwo.frame;
//    CGRect frmBtnCheckZakat = self.btnCheckZakat.frame;
//
//    //view inner child
//    CGRect frmLblCheckOne = self.lblCheckOne.frame;
//    CGRect frmLblKetentuan = self.lblKetentuan.frame;
//
//    CGRect frmLblCheckTwo = self.lblCheckTwo.frame;
//    CGRect frmlblStoran = self.lblStoran.frame;
//    CGRect frmTxtStoran = self.txtFieldStoran.frame;
//    CGRect frmVwLineStoran = self.vwLineStoran.frame;
//    CGRect frmLblEmail = self.lblEmail.frame;
//    CGRect frmTxtEmail = self.txtFieldEmail.frame;
//    CGRect frmVwLineEmail = self.vwLineEmail.frame;
//    CGRect frmBtnSetuju = self.btnSutuju.frame;
//    CGRect frmBtnCancel = self.btnBatal.frame;
//
//    frmVwTitle.origin.x = 0;
//    frmVwTitle.origin.y = TOP_NAV;
//    frmVwTitle.size.width = SCREEN_WIDTH;
//    frmVwTitle.size.height = 50;
//
//    frmLblTitle.size.height = 40;
//    frmLblTitle.origin.y = 5;
//    frmLblTitle.origin.x = 8;
//    frmLblTitle.size.width = frmVwTitle.size.width - 16;
//
//    frmVwBtn.origin.y = SCREEN_HEIGHT - (BOTTOM_NAV + frmVwBtn.size.height) - 20;
//    frmVwBtn.origin.x = 0;
//    frmVwBtn.size.width = SCREEN_WIDTH;
//
//    if(IPHONE_X || IPHONE_XS_MAX){
//        frmVwBtn.origin.y = SCREEN_HEIGHT - (BOTTOM_NAV + frmVwBtn.size.height) - 80;
//    }
//
//    frmVwScroll.origin.y = frmVwTitle.origin.y + frmVwTitle.size.height + 8;
//    frmVwScroll.origin.x = 0;
//    frmVwScroll.size.width = SCREEN_WIDTH;
//    frmVwScroll.size.height = frmVwBtn.origin.y - (frmVwTitle.origin.y + frmVwTitle.size.height) - (frmVwBtn.size.height - 20);
//
//    frmVwInner.origin.y = 0;
//    frmVwInner.origin.x = 8;
//    frmVwInner.size.width = frmVwScroll.size.width - (frmVwInner.origin.x + 8);
////    if([stateCheckedOne isEqualToString:@"N"]){
////        frmVwInner.size.height = frmVwCheckTwo.origin.y + frmVwCheckTwo.size.height;
////    }else{
////        if([mnId isEqualToString:@"00022"]){
////            stateCheckedZakat = @"Y";
////            frmVwInner.size.height = frmVwZakat.origin.y + frmVwZakat.size.height + 16;
////        }else{
////            stateCheckedZakat = @"N";
////        }
////
////    }
//    frmLblTxt.origin.y = 0;
//    frmLblTxt.size.width = SCREEN_WIDTH - (frmLblTxt.origin.x + 16);
//
//    frmVwCheckOne.origin.y = frmLblTxt.origin.y + frmLblTxt.size.height + 8;
//    frmVwCheckOne.origin.x = 8;
//    frmVwCheckOne.size.width = frmVwInner.size.width - 16;
//    frmVwCheckOne.size.height = frmLblCheckOne.origin.y + frmLblCheckOne.size.height + 4;
//
//    frmBtnCheckOne.size.height = frmVwCheckOne.size.height;
//    frmBtnCheckOne.size.width = frmVwCheckOne.size.width;
//
//    frmVwKetentuan.origin.y = frmVwCheckOne.origin.y + frmVwCheckOne.size.height;
//    frmVwKetentuan.origin.x = frmVwCheckOne.origin.x;
//    frmVwKetentuan.size.width = frmVwCheckOne.size.width;
//
//    frmBtnKetentuan.size.width = frmVwKetentuan.size.width;
//    frmBtnKetentuan.size.height = frmVwKetentuan.size.height;
//
//
//    frmVwCheckTwo.origin.y = frmVwKetentuan.origin.y + frmVwKetentuan.size.height;
//    frmVwCheckTwo.origin.x = frmVwCheckOne.origin.x;
//    frmVwCheckTwo.size.width = frmVwCheckOne.size.width;
//    frmVwCheckTwo.size.height = frmLblCheckTwo.origin.y + frmLblCheckTwo.size.height + 8;
//
//    frmBtnCheckTwo.size.width  = frmVwCheckTwo.size.width;
//    frmBtnCheckTwo.size.height = frmVwCheckTwo.size.height;
//
//    frmVwStoran.origin.y = frmVwCheckTwo.origin.y + frmVwCheckTwo.size.height;
//    frmVwStoran.origin.x = frmVwCheckOne.origin.x;
//    frmVwStoran.size.width = frmVwCheckOne.size.width;
//
//    frmVwEmail.origin.y = frmVwStoran.origin.y + frmVwStoran.size.height;
//    frmVwEmail.origin.x = frmVwCheckOne.origin.x;
//    frmVwEmail.size.width = frmVwCheckOne.size.width;
//
//    frmVwZakat.origin.y = frmVwEmail.origin.y + frmVwEmail.size.height;
//    frmVwZakat.origin.x = frmVwCheckOne.origin.x;
//    frmVwZakat.size.width = frmVwCheckOne.size.width;
//
//    frmBtnCheckZakat.size.width = frmVwZakat.size.width;
//    frmBtnCheckZakat.size.height = frmVwZakat.size.height;
//
//    frmVwInner.size.height = frmVwZakat.origin.y + frmVwZakat.size.height + 16;
//
//    frmLblCheckOne.size.width = frmVwCheckOne.size.width - (frmLblCheckOne.origin.x + 8);
//    frmLblKetentuan.size.width = frmVwKetentuan.size.width - (frmLblKetentuan.origin.x + 8);
//    frmLblCheckTwo.size.width = frmVwCheckTwo.size.width - (frmLblCheckTwo.origin.x + 8);
//
//    frmlblStoran.size.width = frmVwStoran.size.width - (frmlblStoran.origin.x *2);
//    frmTxtStoran.size.width = frmlblStoran.size.width;
//    frmVwLineStoran.size.width = frmTxtStoran.size.width;
//
//    frmLblEmail.size.width = frmVwEmail.size.width - (frmLblEmail.origin.x *2);
//    frmTxtEmail.size.width  = frmLblEmail.size.width;
//    frmVwLineEmail.size.width = frmTxtEmail.size.width;
//
//
//    frmBtnSetuju.origin.x = 32;
//    frmBtnCancel.origin.x = frmVwBtn.size.width - (frmBtnCancel.size.width - 32);
//
//    frmBtnSetuju.origin.x = 8;
//    frmBtnSetuju.size.width = (frmVwBtn.size.width/2) - 16;
//    frmBtnCancel.origin.x = frmBtnSetuju.origin.x + frmBtnSetuju.size.width + 16;
//    frmBtnCancel.size.width = frmBtnSetuju.size.width;
//
//    frmBtnSetuju.size.height = 40;
//    frmBtnCancel.size.height = 40;
//    frmVwBtn.size.height = 40;
//
//
//
//    //view parent
//    self.vwTitle.frame = frmVwTitle;
//    self.lblMenuTitle.frame = frmLblTitle;
//    self.lblText.frame = frmLblTxt;
//    self.vwScroll.frame = frmVwScroll;
//    self.vwButton.frame = frmVwBtn;
//
//    //view child
//    self.vwInnerScroll.frame = frmVwInner;
//    self.vwCheckOne.frame = frmVwCheckOne;
//    self.vwKetentuan.frame = frmVwKetentuan;
//    self.vwCheckTwo.frame = frmVwCheckTwo;
//    self.vwSetoran.frame = frmVwStoran;
//    self.vwEmail.frame = frmVwEmail;
//    self.vwZakat.frame = frmVwZakat;
//    self.btnCheckOne.frame = frmBtnCheckOne;
//    self.btnOpnWeb.frame = frmBtnKetentuan;
//    self.btnCheckTwo.frame = frmBtnCheckTwo;
//    self.btnCheckZakat.frame = frmBtnCheckZakat;
//
//    //view inner child
//    self.lblCheckOne.frame = frmLblCheckOne;
//    self.lblKetentuan.frame = frmLblKetentuan;
//    self.lblCheckTwo.frame = frmLblCheckTwo;
//    self.lblStoran.frame = frmlblStoran;
//    self.txtFieldStoran.frame = frmTxtStoran;
//    self.vwLineStoran.frame = frmVwLineStoran;
//    self.lblEmail.frame = frmLblEmail;
//    self.txtFieldEmail.frame = frmTxtEmail;
//    self.vwLineEmail.frame = frmVwLineEmail;
//    self.btnBatal.frame = frmBtnCancel;
//    self.btnSutuju.frame = frmBtnSetuju;
//
//    if(IPHONE_5){
//        [self.lblText setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
//        [self.lblCheckOne setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
//        [self.lblCheckTwo setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
//        [self.lblKetentuan setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
//        [self.txtFieldStoran setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
//        [self.txtFieldEmail setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
//        [self.lblEmail setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
//        [self.lblStoran setFont:[UIFont fontWithName:@"Helvetica" size:11.0f]];
//
//    }else{
//         [self.lblText setFont:[UIFont fontWithName:@"Helvetica" size:12.0f]];
//         [self.vwScroll setContentSize:CGSizeMake(SCREEN_WIDTH, (_vwZakat.frame.origin.y + _vwZakat.frame.size.height))];
//    }
    
    if([mnId isEqualToString:@"00023"]){
        [_vwZakat setHidden:true];
        [self.vwScroll setContentSize:CGSizeMake(SCREEN_WIDTH, (_vwEmail.frame.origin.y + _vwEmail.frame.size.height))];
    }else{
        stateCheckedZakat = @"Y";
        [_vwZakat setHidden:false];
        _imgCheckZakat.image = [UIImage imageNamed:@"checked.png"];
        _imgCheckZakat.image = [_imgCheckZakat.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        _imgCheckZakat.tintColor = UIColorFromRGB(const_color_primary);
        [self.vwScroll setContentSize:CGSizeMake(SCREEN_WIDTH, (_vwZakat.frame.origin.y + _vwZakat.frame.size.height))];
    }
    
}

-(void) viewVisibility{
    if([stateCheckedOne isEqualToString:@"N"]){
        [self.vwSetoran setHidden:true];
        [self.vwEmail setHidden:true];
//        [self.btnBatal setHidden:true];
//        [self.btnSutuju setHidden:true];
//        [self.btnRootBatal setHidden:false];
    }else{
        [self.vwSetoran setHidden:false];
        [self.vwEmail setHidden:false];
//        [self.btnBatal setHidden:false];
//        [self.btnSutuju setHidden:false];
//        [self.btnRootBatal setHidden:true];
    }
    
}


-(void)viewIsEnabled{
    
    if([stateCheckedOne isEqualToString:@"N"]){
        [_txtFieldStoran setEnabled:false];
        [_imgCheckOne setImage:[UIImage imageNamed: @"blank_check.png"]];
        //[_txtFieldEmail setEnabled:false];
//        [self.btnBatal setHidden:true];
//        [self.btnSutuju setHidden:true];
        //state normal
        [_btnBatal setEnabled:false];
//        [_btnBatal setBackgroundColor:[UIColor grayColor]];
        
        
        if ([mnId isEqualToString:@"00022"]) {
            [_lblZakat setTextColor:[UIColor grayColor]];
            [_btnCheckZakat setEnabled:false];
        }
        
        //stateCheckedOne = @"Y";
    }
}

- (IBAction)doneClicked:(id)sender
{
    NSLog(@"Done Clicked.");
    [self.view endEditing:YES];
}


- (IBAction)actionCheckOne:(id)sender {
    if([stateCheckedOne isEqualToString:@"N"]){
        [_txtFieldStoran setEnabled:true];
        [_imgCheckOne setImage:[UIImage imageNamed: @"checked.png"]];
        //[_txtFieldEmail setEnabled:false];
//        [self.btnBatal setHidden:false];
//        [self.btnSutuju setHidden:false];
//        [self.btnRootBatal setHidden:true];
        stateCheckedOne = @"Y";
        if ([mnId isEqualToString:@"00022"]) {
            [_lblZakat setTextColor:[UIColor blackColor]];
            [_btnCheckZakat setEnabled:true];
        }
        
    }else{
        [_txtFieldStoran setEnabled:false];
        [_imgCheckOne setImage:[UIImage imageNamed: @"blank_check.png"]];
        //[_txtFieldEmail setEnabled:true];
//        [self.btnBatal setHidden:true];
//        [self.btnSutuju setHidden:true];
//        [self.btnRootBatal setHidden:false];
        stateCheckedOne = @"N";
        if ([mnId isEqualToString:@"00022"]) {
            [_lblZakat setTextColor:[UIColor grayColor]];
            [_btnCheckZakat setEnabled:false];
    
        }
        
    }
}

- (IBAction)actionCheckTwo:(id)sender {
    if([stateCheckedTwo isEqualToString:@"N"]){
        [_imgCheckTwo setImage:[UIImage imageNamed:@"checked.png"]];
//        [self.btnBatal setHidden:false];
//        [self.btnSutuju setHidden:false];
//        [self.btnRootBatal setHidden:true];
        [_btnBatal setEnabled:true];
//        [_btnBatal setBackgroundColor:UIColorFromRGB(amanah_primary_color)];
        stateCheckedTwo = @"Y";
    }else{
        [_imgCheckTwo setImage:[UIImage imageNamed:@"blank_check.png"]];
//        [self.btnBatal setHidden:true];
//        [self.btnSutuju setHidden:true];
//        [self.btnRootBatal setHidden:false];
        [_btnBatal setEnabled:false];
//        [_btnBatal setBackgroundColor:[UIColor grayColor]];
        stateCheckedTwo = @"N";
    }
}

- (IBAction)actionZakat:(id)sender {
    if([stateCheckedZakat isEqualToString:@"N"]){
        [_imgCheckZakat setImage:[UIImage imageNamed:@"checked.png"]];
        stateCheckedZakat = @"Y";
    }else{
        [_imgCheckZakat setImage:[UIImage imageNamed:@"blank_check.png"]];
        stateCheckedZakat = @"N";
    }
}


#pragma mark change actionSetuju to goToCancel
- (IBAction)actionSetuju:(id)sender {
    [self goToCancel];
}

-(void)goToNext{
    if([self.txtFieldStoran.text isEqualToString:@""] && [[self.jsonData valueForKey:@"mandatory"]boolValue]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
//    }else{
//
//        NSString *inputString = self.txtFieldStoran.text;
//        NSString *trimmedString = [inputString stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
//
//        if ([trimmedString length]) {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:lang(@"STR_NOT_NUM") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
        }else{
            [dataManager.dataExtra addEntriesFromDictionary:@{@"email":self.txtFieldEmail.text}];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"fintech_name":self.txtFieldStoran.text}];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"amount":self.txtFieldStoran.text}];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"zakat":stateCheckedZakat}];
            [super openNextTemplate];
        }
    
}

#pragma mark change actionBatal to goToNext
- (IBAction)actionBatal:(id)sender {
    [self goToNext];
}

-(void) goToCancel{
    [self backToRoot];
}

- (IBAction)actionRootBatal:(id)sender {
    [self backToRoot];
}

- (IBAction)actionOpenWeb:(id)sender {
    checkedViewLink = true;
    if(checkedViewLink){
        [_btnCheckTwo setEnabled:true];
        [_lblCheckTwo setTextColor:[UIColor blackColor]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: urlSyarat]];
    }
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,50)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

- (void) createPicker : (UITextField*) textField withTag : (NSInteger) tag{
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    picker.tag = tag;
    textField.inputView = picker;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return listFintech.count;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        return [listFintech[row] objectForKey:@"fintech_name"];
    }
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(pickerView.tag == 0){
        self.txtFieldStoran.text = [listFintech[row] objectForKey:@"fintech_name"];
        [dataManager.dataExtra setValue:[listFintech[row] objectForKey:@"fintech"] forKey:@"fintech"];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject objectForKey:@"response_code"]  isEqual: @"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                NSString *response = [jsonObject valueForKey:@"response"];
                NSMutableDictionary *dict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                NSString *info = [dict objectForKey:@"info"];
                listFintech = [dict objectForKey:@"list_fintech"];
                int amt = 0;
                if ([dict objectForKey:@"amount"] == nil) {
                    amt = 0;
                } else {
                    amt = [[dict objectForKey:@"amount"] intValue];
                }
                NSArray *stringArray = [info componentsSeparatedByString:@"|"];
                if (amt == 100000) {
                    self.txtFieldStoran.text = [NSString stringWithFormat:@"%d",amt];
                    [self.txtFieldStoran setEnabled:NO];
                } else {
                    if([stateCheckedOne isEqualToString:@"N"]){
                        [self.txtFieldStoran setEnabled:NO];
                    }else{
                        [self.txtFieldStoran setEnabled:YES];
                    }
                    
                }
                if (stringArray.count == 1) {
                    _lblText.text = stringArray[0];
                    
                    urlSyarat = @"";
                    //_lblAgree3.text = @"";
                }
                else if (stringArray.count == 2) {
                    _lblText.text = stringArray[0];
                    urlSyarat = stringArray[1];
                    //_lblAgree3.text = @"";
                }
                else if (stringArray.count == 3) {
                    _lblText.text = stringArray[0];
                    urlSyarat = stringArray[1];
                    //_lblAgree3.text = @"";
                }
                else if (stringArray.count == 4) {
                    _lblText.text = stringArray[0];
                    urlSyarat = stringArray[1];
                    _lblCheckOne.text = stringArray[3];
                    
//                    CGRect frmLblText = _lblText.frame;
//                    CGRect frmLblCheckOne = _lblCheckOne.frame;
//
//                    CGSize constraint = CGSizeMake(SCREEN_WIDTH, 20000.0f);
//                    CGSize sizeLblDesc = [_lblCheckOne.text sizeWithFont:[UIFont systemFontOfSize:15.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
//
//                    frmLblText.size.height = [Utility dynamicLabelHeight:_lblText];
//                    frmLblCheckOne.size.height = sizeLblDesc.height;
//                    frmLblCheckOne.size.width = _vwCheckOne.frame.size.width - frmLblCheckOne.origin.x - 16;
//
//                    _lblText.frame = frmLblText;
//                    _lblCheckOne.frame = frmLblCheckOne;
                }
                [self setupLayout];
                
            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}
- (void)errorLoadData:(NSError *)error{
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100) {
        [super openActivation];
    } else {
        [self backToRoot];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}


@end
