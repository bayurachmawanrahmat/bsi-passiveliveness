//
//  LV08ViewController.m
//  BSM-Mobile
//
//  Created by BSM on 10/10/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "LV08ViewController.h"
#import "Utility.h"
#import "Connection.h"

@interface LV08ViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate >{
    NSArray *listAction;
    NSArray *listOrigin;
    NSDictionary *dataObject;
    NSDictionary *dataLocal;
    BOOL mustAddToManager;
    NSString *key;
    UIToolbar *toolBar;
    CGRect screenBound;
    CGSize screenSize;
    CGFloat screenWidth;
    CGFloat screenHeight;
}

@property (weak, nonatomic) IBOutlet UIImageView *iconHeaderLV08;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;


@end

@implementation LV08ViewController

extern NSString* requestType;
extern NSString *lv_selected;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *dt = [userDefault objectForKey:@"indexMenu"];
    if (dt) {
        [[DataManager sharedManager]resetObjectData];
        NSArray *temp = [[DataManager sharedManager] getJSONData:[dt intValue]];
        NSDictionary *object = [temp objectAtIndex:1];
        [self setJsonData:object];
        [userDefault removeObjectForKey:@"indexMenu"];
    }
    self.tableView.tableFooterView = [[UIView alloc] init];
    NSString *img = [userDefault valueForKey:@"imgIcon"];
    _iconHeaderLV08.image = [UIImage imageNamed:img];
    _iconHeaderLV08.contentMode = UIViewContentModeScaleToFill;
    
    screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenBound.size;
    screenWidth = screenSize.width;
    screenHeight = screenSize.height;
    
    CGRect frmTitle = self.vwTitle.frame;
    CGRect frmLblTitle = self.labelTitle.frame;
    CGRect frmTable = self.tableView.frame;
    
    frmTitle.size.width = screenWidth;
    frmTitle.size.height = 50;
    frmTitle.origin.y = TOP_NAV;
    frmTitle.origin.x = 0;
    
    frmLblTitle.origin.x = 8;
    frmLblTitle.origin.y = 5;
    frmLblTitle.size.width = frmTitle.size.width - 16;
    frmLblTitle.size.height = 40;
    
    frmTable.origin.x = 0;
    frmTable.origin.y = frmTitle.origin.y + frmTitle.size.height;
    frmTable.size.width = screenWidth;
    
    self.vwTitle.frame = frmTitle;
    self.labelTitle.frame = frmLblTitle;
    self.tableView.frame = frmTable;
    
    NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
    requestType = [dataParam valueForKey:@"request_type"];
    
    dataObject = [dataManager getObjectData];
    
    [self.labelTitle setText:[dataObject valueForKey:@"title"]];
    
    //update to amanah styles
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if([[dataObject valueForKey:@"url"]boolValue]){
        if([requestType isEqualToString:@"list_denom"]) {
            NSString *url = [NSString stringWithFormat:@"%@,code=0125",[self.jsonData valueForKey:@"url_parm"]];
            NSString *mid = [self.jsonData valueForKey:@"menu_id"];
            [dataManager setMenuId:mid];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
    }
    else {
        id data =  [dataObject objectForKey:@"content"];
        if([data isKindOfClass:[NSString class]]){
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else {
            mustAddToManager = true;
            listAction = data;
            [self.tableView reloadData];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listAction.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LV08Cell" forIndexPath:indexPath];
    UILabel *label = (UILabel *)[cell viewWithTag:1];
    UIView *line = (UIView *)[cell viewWithTag:2];
    CGRect frmCL = line.frame;
    frmCL.size.width = screenWidth - 20;
    line.frame = frmCL;
    
    NSDictionary *data = [listAction objectAtIndex:indexPath.row];
    if([data objectForKey:@"title"]){
        [label setText:[data valueForKey:@"title"]];
    }else if([data objectForKey:@"name"]){
        [label setText:[data valueForKey:@"name"]];
    }
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [label.text sizeWithFont:label.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    
    CGRect frame = label.frame;
    frame.size.height = height;
    [label setFrame:frame];
    
    [label setFont:[UIFont fontWithName:const_font_name1 size:14]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = @"";
    
    NSDictionary *data = [listAction objectAtIndex:indexPath.row];
    if([data objectForKey:@"title"]){
        text = [data valueForKey:@"title"];
    }else if([data objectForKey:@"name"]){
        text = [data valueForKey:@"name"];
    }
    
    CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH, 20000.0f);
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat height = MAX(size.height, 33.0);
    return height+20.0;
}

#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
    
    if(mustAddToManager){
        NSString *value = @"";
        NSDictionary *data = [listAction objectAtIndex:indexPath.row];
        BOOL haveAction = false;
        for(NSString *a in data.allKeys){
            if([a isEqualToString:@"action"]){
                haveAction = true;
            }else if(![a isEqualToString:@"title"] && ![a isEqualToString:@"name"]){
                key = a;
                value = [data valueForKey:key];
                break;
            }
        }
        
        lv_selected = value;
        [dataManager.dataExtra addEntriesFromDictionary:@{key:value}];
        if(haveAction){
            [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
            dataManager.currentPosition=-1;
        }
    }
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *last = [userDefault objectForKey:@"last"];
    if (![last isEqualToString:@"-"]) {
        [userDefault setValue:@"-" forKey:@"last"];
    }

    if ([lv_selected isEqualToString:@"0"]) {
        [super openNextTemplate];
    }
    else {
        dataManager.currentPosition++;
        [super openNextTemplate];
    }
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([jsonObject isKindOfClass:[NSDictionary class]]){
        if ([[jsonObject valueForKey:@"response_code"] isEqualToString:@"00"]) {
            if (![requestType isEqualToString:@"check_notif"]) {
                if ([requestType isEqualToString:@"list_denom"]) {
                    NSArray *listData = (NSArray *)[jsonObject valueForKey:@"response"];
                    NSMutableArray *newData = [[NSMutableArray alloc]init];
                    NSString *temp1;
                    NSString *temp2;
                    
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                    NSString *desc = @"";
                    
                    for(NSDictionary *temp in listData){
                        if ([[temp valueForKey:@"id_denom"] isEqualToString:@"0"]) {
                            if([lang isEqualToString:@"id"]) {
                                desc = @"Nominal lainnya";
                            } else {
                                desc = @"Other nominal";
                            }
                            temp1 = desc;
                            temp2 = [temp valueForKey:@"id_denom"];
                        }
                        else {
                            [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                        }
                    }
                    [newData addObject:@{@"title":temp1,@"id_denom":temp2}];
                    
                    key = @"id_denom";
                    mustAddToManager = true;
                    listAction = newData;
                    [self.tableView reloadData];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
                
            }
        }
        else {
            /*NSString *msg = @"";
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            
            if([lang isEqualToString:@"id"]) {
                msg = @"Permintaan tidak dapat diproses";
            } else {
                msg = @"Request time out";
            }
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];*/
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }else{
        if([requestType isEqualToString:@"list_denom"]){
            NSArray *listData = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            NSString *temp1;
            NSString *temp2;
            
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
            NSString *desc = @"";
            
            for(NSDictionary *temp in listData){
                if ([[temp valueForKey:@"id_denom"] isEqualToString:@"0"]) {
                    if([lang isEqualToString:@"id"]) {
                        desc = @"Nominal lainnya";
                    } else {
                        desc = @"Other nominal";
                    }
                    temp1 = desc;
                    temp2 = [temp valueForKey:@"id_denom"];
                }
                else {
                    [newData addObject:@{@"title":[temp valueForKey:@"name"],@"id_denom":[temp valueForKey:@"id_denom"]}];
                }
            }
            [newData addObject:@{@"title":temp1,@"id_denom":temp2}];
            
            key = @"id_denom";
            mustAddToManager = true;
            listAction = newData;
            [self.tableView reloadData];
        }
    }
}

- (void)errorLoadData:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100)
        [super openActivation];
    else
        [self backToRoot];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //NSLog(@"Will begin dragging");
    NSDictionary* userInfo = @{@"position": @(1112)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
}

@end
