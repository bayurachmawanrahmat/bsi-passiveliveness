//
//  TAS01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 21/04/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "OA01ViewController.h"
#import "Styles.h"
#import "Utility.h"

@interface OA01ViewController ()<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>{
    
    NSUserDefaults* userDefaults;
    NSString *language, *toolBarTitle;
    
    NSArray *listFrequency, *listAccount, *listDate, *arrIntervalFreq, *listName;
    
    UIDatePicker *datePickerTarget;
    UIPickerView *datePickerStart, *pickerListName;
    NSDate *targetedDate;

    NSNumberFormatter *currencyFormatter;

    UIToolbar *toolbar;
    double fundTargeted;
    BOOL isRequested;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitleBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet UILabel *labelSavingName;
@property (weak, nonatomic) IBOutlet UITextField *tfSavingName;
@property (weak, nonatomic) IBOutlet UILabel *labelFundTarget;
@property (weak, nonatomic) IBOutlet UITextField *tfFundTarget;
@property (weak, nonatomic) IBOutlet UILabel *labelFrekuensi;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeTarget;
@property (weak, nonatomic) IBOutlet UILabel *subLabelTimeTarget;
@property (weak, nonatomic) IBOutlet UITextField *tfTimeTarget;
@property (weak, nonatomic) IBOutlet UITextField *tfFrekuensi;
@property (weak, nonatomic) IBOutlet UILabel *labelDateStart;
@property (weak, nonatomic) IBOutlet UITextField *tfDateStart;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelSourceAccount;
@property (weak, nonatomic) IBOutlet UITextField *tfSourceAccount;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vwConstant;
@property (weak, nonatomic) IBOutlet UIView *vwInsideFrequency;
@property (weak, nonatomic) IBOutlet UITextField *tfInsideFrekuensi;
@property (weak, nonatomic) IBOutlet UILabel *lblInsideFrekuensi;

@end

@implementation OA01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    listDate = [[NSArray alloc]init];
    currencyFormatter = [[NSNumberFormatter alloc]init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [currencyFormatter setGroupingSeparator:@","];
    
    [self.labelTitleBar setText:[self.jsonData objectForKey:@"title"]];
    [self.iconTitleBar setImage:[UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    [self.iconTitleBar setHidden:YES];
    self.labelTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.labelTitleBar.textAlignment = const_textalignment_title;
    self.labelTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);

    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    [self setupStyles];
    [self setupLanguage];
    [self getListAccount];
    [self setRequest];

    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:toolBarTitle style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];

    datePickerTarget = [[UIDatePicker alloc]init];
    if (@available(iOS 13.4, *)) {
        [datePickerTarget setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
        [datePickerTarget setPreferredDatePickerStyle:UIDatePickerStyleWheels];
     }
    [datePickerTarget setLocale:[[NSLocale alloc]initWithLocaleIdentifier:language?@"id":@"en"]];
    [datePickerTarget setDatePickerMode:UIDatePickerModeDate];
    [datePickerTarget setBackgroundColor:[UIColor whiteColor]];
    [datePickerTarget setMinimumDate:[NSDate date]];
    [datePickerTarget addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    self.tfSavingName.inputAccessoryView = toolbar;
//    self.tfSavingName.delegate = self;
//    [self createPicker:self.tfSavingName withTag:3];
    
    self.tfTimeTarget.inputAccessoryView = toolbar;
    self.tfTimeTarget.inputView = datePickerTarget;
    self.tfTimeTarget.delegate = self;
    
    self.tfFundTarget.inputAccessoryView = toolbar;
    self.tfFundTarget.keyboardType = UIKeyboardTypeNumberPad;
    self.tfFundTarget.delegate = self;
    
    self.tfSourceAccount.inputAccessoryView = toolbar;
    self.tfSourceAccount.delegate = self;
    [self createPicker:self.tfSourceAccount withTag:0];
    
    self.tfFrekuensi.inputAccessoryView = toolbar;
    self.tfFrekuensi.delegate = self;
    [self createPicker:self.tfFrekuensi withTag:1];
    
    self.tfDateStart.inputAccessoryView = toolbar;
    self.tfDateStart.delegate = self;
    [self createPicker:self.tfDateStart withTag:2];
    
    self.tfAmount.enabled = NO;
    
    self.vwConstant.constant = 0;
    [self.vwInsideFrequency setHidden:YES];
    
    
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
}

- (void) createPicker : (UITextField *)textField withTag:(int) tfTag{
    UIPickerView *objPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150)];
    objPicker.showsSelectionIndicator = YES;
    objPicker.delegate = self;
    objPicker.dataSource = self;
    objPicker.tag = tfTag;
    textField.inputView = objPicker;
}

- (void)dateChanged : (UIDatePicker *)datePicker{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];

    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *selectedDate = [dateFormatter stringFromDate:datePicker.date];
    
    if(datePicker == datePickerTarget){
        self.tfTimeTarget.text = selectedDate;
        targetedDate = datePickerTarget.date;
        [self createDateList:targetedDate withType:1];
    }
}

- (void) setRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *url =[NSString stringWithFormat:@"%@",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:url needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (void) setupStyles{
    [Styles setTopConstant:_topConstraint];
    
    [Styles setStyleTextFieldBottomLine:_tfSavingName];
    [Styles setStyleTextFieldBottomLine:_tfFundTarget];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfTimeTarget imageNamed:@"ic_frm_calendar"];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfFrekuensi imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfInsideFrekuensi imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfDateStart imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];
    [Styles setStyleTextFieldBottomLine:_tfAmount];
    [Styles setStyleTextFieldBottomLineWithRightImage:_tfSourceAccount imageNamed:@"baseline_keyboard_arrow_down_black_24pt"];

}

- (void) setupLanguage{
    if([language isEqualToString:@"id"]){
        toolBarTitle = @"selesai";
        
        _labelSavingName.text = @"Nama tabungan";
        _tfSavingName.placeholder = @"Berikan nama tabunganmu sesuai tujuan";
        
        _labelFundTarget.text = @"Target dana";
        _tfFundTarget.placeholder = @"Tuliskan target dana yang ingin dicapai";
        
        _labelTimeTarget.text = @"Target waktu dana terkumpul";
        _tfTimeTarget.placeholder = @"Pilih Tanggal";
        _subLabelTimeTarget.text = @"(tanggal/hari pendebetan akan mengikuti target waktu)";
        
        _labelFrekuensi.text = @"Frekuensi setoran";
        _tfFrekuensi.placeholder = @"Pilih frekuensi";
        
        _labelDateStart.text = @"Tanggal mulai";
        _tfDateStart.placeholder = @"Pilih Tanggal";
        
        _labelAmount.text = @"Jumlah setoran per bulan/minggu";
        _tfAmount.placeholder = @"Rp.";
        
        _labelSourceAccount.text = @"Rekening debet/sumber dana";
        _tfSourceAccount.placeholder = @"Pilih rekening";
        
        [_btnNext setTitle:@"Selanjutnya" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Kembali" forState:UIControlStateNormal];
        
        
        listFrequency = @[@"Setiap minggu", @"Setiap bulan"];
        
        [self.labelContent setText:@"Bank Syariah Indonesia memudahkan Anda untuk disiplin menyisihkan tabungan Anda dengan Tabungan Autosave, sehingga impian anda dapat terwujud tepat waktu"];
        
    }else{
        [self.labelContent setText:@"Bank Syariah Indonesia makes easier for you to discipline your savings. So that your dreams can be realized on time"];
        listFrequency = @[@"Weekly", @"Monthly"];

        toolBarTitle = @"done";
        
        _labelSavingName.text = @"Savings names";
        _tfSavingName.placeholder = @"Give the name of your savings as intended";
        
        _labelFundTarget.text = @"Funds target";
        _tfFundTarget.placeholder = @"Write down the target funds to be achieved";
        
        _labelTimeTarget.text = @"Target time for funds to be collected";
        _tfTimeTarget.placeholder = @"Select date";
        _subLabelTimeTarget.text = @"(date/day of debiting will follow target time)";
        
        _labelFrekuensi.text = @"Frequency of deposits";
        _tfFrekuensi.placeholder = @"Select frequency";
        
        _labelDateStart.text = @"Start date";
        _tfDateStart.placeholder = @"Select date";
        
        _labelAmount.text = @"Amount of deposit per month/week";
        _tfAmount.placeholder = @"Rp.";
        
        _labelSourceAccount.text = @"Debit/Source of funds";
        _tfSourceAccount.placeholder = @"Select account";
        
        [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
        [_btnCancel setTitle:@"Back" forState:UIControlStateNormal];
    }
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if(pickerView.tag == 1){
        return listFrequency[row];
    }else if(pickerView.tag == 2){
//        return arrIntervalFreq[row];
        if(listDate.count != 0){
            NSString *str = [NSString stringWithFormat:@"%@",[self formatterNSDate: listDate[listDate.count - (row+1)]]];
            return str;
        }else{
            return @"not found";
        }
    }else if(pickerView.tag == 3){
        if(listName.count != 0){
            if(row==0){
                if([language isEqualToString:@"id"]){
                    return @"Pilih nama tabungan";
                }else{
                    return @"Select saving name";
                }
            }else{
                return listName[row-1];
            }
        }else{
            return @"not found";
        }
    }else{
        return listAccount[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(pickerView.tag == 1){
        
        self.tfFrekuensi.text = listFrequency[row];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", row+1] forKey:@"frekuensi"];
        [self createDateList:targetedDate withType:(int)row+1];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld", row+1] forKey:@"recurrence_type"];
//        [self setupFrekuensi:row];
//        NSDateComponents *components;
        NSInteger days, months, years;
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents * dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: [NSDate date] toDate:targetedDate options:0];

//        components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit
//                fromDate: [NSDate date] toDate: targetedDate options: 0];
        days = [dateComponents day];
        months = [dateComponents month];
        years = [dateComponents year];
        if(row+1 == 1){
            if(days < 7 && years < 1 & months < 1){
                NSString *msg = @"";
                if([language isEqualToString:@"id"]){
                    msg = @"Target waktu minimum 7 hari dari hari ini";
                }else{
                    msg = @"Time target minimum 7 days from today";
                }
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self.tfTimeTarget setText:@""];
                    [self.tfFrekuensi setText:@""];
                    [self.view endEditing:YES];
                    [self.tfFrekuensi resignFirstResponder];
                    [self.tfTimeTarget becomeFirstResponder];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            if(months < 1 && years < 1){
                NSString *msg = @"";
                if([language isEqualToString:@"id"]){
                    msg = @"Target waktu minimum 1 bulan dari hari ini";
                }else{
                    msg = @"Time target minimum 1 month from today";
                }
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self.tfTimeTarget setText:@""];
                    [self.tfFrekuensi setText:@""];
                    [self.view endEditing:YES];
                    [self.tfFrekuensi resignFirstResponder];
                    [self.tfTimeTarget becomeFirstResponder];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
                
            }
        }
        
    }else if(pickerView.tag == 2){
//        self.tfInsideFrekuensi.text = arrIntervalFreq[row];
        if(listDate.count != 0){

            NSDate *dateStart = listDate[listDate.count - (row+1)];
            
            NSString *str = [NSString stringWithFormat:@"%@",[self formatterNSDate: dateStart]];
            self.tfDateStart.text = str;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *dateStartToSent = [dateFormatter stringFromDate:dateStart];
            [dataManager.dataExtra setValue:dateStartToSent forKey:@"target_mulai"];
            
            double debit = fundTargeted/(listDate.count - row);
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setMaximumFractionDigits:0];
            [formatter setRoundingMode: NSNumberFormatterRoundUp];
            NSString *debitString = [formatter stringFromNumber:[NSNumber numberWithDouble:debit]];
            
            [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%.f",fundTargeted] forKey:@"target_harga"];
            [dataManager.dataExtra setValue:debitString forKey:@"setoran"];
            
            self.tfAmount.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:[debitString doubleValue]]];
        }
        
    }else if(pickerView.tag == 3){
        if(row == 0){
            self.tfSavingName.text = @"";
        }else{
            self.tfSavingName.text = listName[row];
//            [dataManager.dataExtra setValue:listName[row] forKey:@"id_account"];
        }
    }
    else{
        if(row == 0){
            self.tfSourceAccount.text = @"";
        }else{
            self.tfSourceAccount.text = listAccount[row];
            [dataManager.dataExtra setValue:listAccount[row] forKey:@"id_account"];
        }
    }
}

- (NSString *) formatterNSDate : (NSDate*) date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE dd-MM-yyyy"];
    if([language isEqualToString:@"id"]){
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"id_ID"]];
    }else{
        [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_EN"]];
    }
    NSString *dateString = [dateFormatter stringFromDate:date];
    return dateString;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if(pickerView.tag == 1){
       return listFrequency.count;
    }else if(pickerView.tag == 2){
//        return arrIntervalFreq.count;
//        return listDate.count;
        if([[dataManager.dataExtra valueForKey:@"frekuensi"] isEqualToString:@"1"]){
            if(listDate.count > 4){
                return 4;
            }else{
                return listDate.count;
            }
        }else{
            if(listDate.count > 2){
                return 2;
            }else{
                return listDate.count;
            }
        }
    }else if(pickerView.tag == 3){
        return listName.count;
    }else{
        return listAccount.count;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.tfFundTarget){
        NSString* newText = [[[[textField.text stringByReplacingCharactersInRange:range withString:string] stringByReplacingOccurrencesOfString:currencyFormatter.currencySymbol withString:@""] stringByReplacingOccurrencesOfString:currencyFormatter.groupingSeparator withString:[NSString string]] stringByReplacingOccurrencesOfString:currencyFormatter.decimalSeparator withString:[NSString string]];
         NSInteger newValue = [newText integerValue];
        
        fundTargeted = newValue;
        
        
         if ([newText length] == 0 || newValue == 0)
             textField.text = nil;
         else if ([newText length] > currencyFormatter.maximumSignificantDigits)
             textField.text = textField.text;
         else
             textField.text = [currencyFormatter stringFromNumber:[NSNumber numberWithDouble:newValue]];
        
         return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.tfTimeTarget){
        [self.tfFrekuensi setText:@""];
        [self.tfFrekuensi becomeFirstResponder];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSString *msg = @"";
    if(textField == self.tfFundTarget){
        if([self.tfSavingName.text isEqualToString:@""]){
            msg = [self getMessageAlert];
            [self.tfFundTarget resignFirstResponder];
            [self.tfSavingName becomeFirstResponder];
        }
        [self.tfFrekuensi setText:@""];
        [self.tfDateStart setText:@""];
        [self.tfAmount setText:@""];
    }
    
    if(textField == self.tfTimeTarget){
        [self.tfFrekuensi setText:@""];
        [self.tfDateStart setText:@""];
        [self.tfAmount setText:@""];
    }
    
    if(textField == self.tfFrekuensi){
        [self.tfDateStart setText:@""];
        [self.tfAmount setText:@""];
    }
    
//    if(![msg isEqualToString:@""]){
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INF0") message:msg preferredStyle:UIAlertControllerStyleAlert];
//
//        [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//        }]];
//
//        [self presentViewController:alert animated:YES completion:nil];
//    }
}

- (NSString*)getMessageAlert{
    NSString *msg = @"";
        if([self.tfSavingName.text isEqualToString:@""]){
            if([language isEqualToString:@"id"]){
                msg = @"Anda harus mengisi nama tabungan anda";
            }else{
                msg = @"You have to fill saving name";
            }
            [self.tfSavingName becomeFirstResponder];
        }else if([self.tfFundTarget.text isEqualToString:@""]){
            if([language isEqualToString:@"id"]){
                msg = @"Tuliskan target dana anda";
            }else{
                msg = @"Fill your target fund";
            }
        }else if([self.tfTimeTarget.text isEqualToString:@""]){
            if([language isEqualToString:@"id"]){
                msg = @"Pilih tanggal dana terkumpul";
            }else{
                msg = @"Please select the date of fund collected";

            }
        }else if([self.tfFrekuensi.text isEqualToString:@""]){
            if([language isEqualToString:@"id"]){
                msg = @"Pilih frekuensi auto simpan";
            }else{
                msg = @"Select autosave frequency";

            }
        }else if([self.tfDateStart.text isEqualToString:@""]){
            if([language isEqualToString:@"id"]){
                msg = @"Pilih tanggal mulai";
            }else{
                msg = @"Select starting date of collecting fund";
            }
        }
    //    else if([self.tfInsideFrekuensi.text isEqualToString:@""]){
    //        if([self.tfInsideFrekuensi.text isEqualToString:listFrequency[0]]){
    //            if([language isEqualToString:@"id"]){
    //                msg = @"pilih hari penjadwalan mingguan auto simpan";
    //            }else{
    //                msg = @"select day of weekly autosave";
    //            }
    //        }else{
    //            if([language isEqualToString:@"id"]){
    //                msg = @"pilih tanggal penjadwalan mingguan auto simpan";
    //            }else{
    //                msg = @"select day of monthly autosave";
    //            }
    //        }
    //    }
//        else if([self.tfAmount.text isEqualToString:@""]){
//            if([language isEqualToString:@"id"]){
//                msg = @"isi setoran auto simpan anda";
//            }else{
//                msg = @"fill deposit of autosave";
//            }
//        }
        else if([self.tfSourceAccount.text isEqualToString:@""]){
            if([language isEqualToString:@"id"]){
                msg = @"Pilih rekening sumber pengumpulan dana";
            }else{
                msg = @"Select source account for collectible fund";
            }
        }
    return msg;
}

- (BOOL) validate{
    NSString *msg = [self getMessageAlert];
    
    if(![msg isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    
    return YES;
}

- (void) actionNext{
    if([self validate]){
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *dateConverted = [dateFormatter dateFromString:self.tfTimeTarget.text];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:dateConverted];
        
        [dataManager.dataExtra setValue:dateString forKey:@"target_waktu"];
        [dataManager.dataExtra setValue:self.tfSavingName.text forKey:@"account_title"];
        [self openNextTemplate];
    }
}

- (void) actionCancel{
    [self backToR];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"list_account1"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            
            [userDefaults setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefaults setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefaults setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefaults synchronize];
            
            if(!isRequested){
                isRequested = YES;
                [self getListAccount];
            }
            
        }else{
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER, [jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alerts addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alerts animated:YES completion:nil];
        }
    }
    
    if([requestType isEqualToString:@""]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            
        }else{
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER, [jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alerts addAction: [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alerts animated:YES completion:nil];
        }
    }
}

- (void) createDateList : (NSDate *)date withType:(int)type{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
    
    if(type == 1){
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld",(long)dateComponents.weekday] forKey:@"dayofweek"];
        [dataManager.dataExtra setValue:@"" forKey:@"dayofmonth"];

    }else{
        if(dateComponents.day > 28){
            [dateComponents setDay:28];
            date = [calendar dateFromComponents:dateComponents];
        }
        [dataManager.dataExtra setValue:@"" forKey:@"dayofweek"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%ld",(long)dateComponents.day] forKey:@"dayofmonth"];
    }
    
    NSDate *dateToShow = date;

    NSMutableArray *listDates = [[NSMutableArray alloc]init];
    if([dateToShow compare:[NSDate date]] == NSOrderedDescending){
        while([dateToShow compare:[NSDate date]] == NSOrderedDescending){
            [listDates addObject:dateToShow];
            if(type == 1){
                [dateComponents setDay:(dateComponents.day - 7)];
                
            }else{
                NSInteger day = dateComponents.day;
                [dateComponents setMonth:(dateComponents.month - 1)];
                [dateComponents setDay:day];
            }
            dateToShow  = [calendar dateFromComponents:dateComponents];
            dateComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitMonth fromDate: dateToShow];
        }
    }else{
        NSString *msg = @"";
        if([language isEqualToString:@"id"]){
            msg = @"Tanggal target dibawah tanggal hari ini";
        }else{
            msg = @"Time Target below today's date";
        }
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
            [self.tfTimeTarget setText:@""];
            [self.tfFrekuensi setText:@""];
            [self.view endEditing:YES];
            [self.tfTimeTarget becomeFirstResponder];
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    listDate = listDates;
}

- (void)getListAccount{
        
        NSMutableDictionary *dataParam = [Utility translateParam:[self.jsonData valueForKey:@"url_parm"]];
        NSArray *arRolespesial = [[userDefaults valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
        NSArray *arRoleFinance = [[userDefaults valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
        BOOL isSpesial, isFinance;
            isSpesial = false;
            isFinance = false;
        NSArray *listAcct = nil;
        
        
        if(arRolespesial.count > 0 || arRolespesial != nil){
            for(NSString *xmenuid in arRolespesial){
                if([[dataParam valueForKey:@"code"]boolValue]){
                    if([[dataParam objectForKey:@"code"]isEqualToString:xmenuid]){
                        isSpesial = true;
                        break;
                    }
                }
                if([dataManager.dataExtra objectForKey:@"code"]){
                    if([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuid]){
                        isSpesial = true;
                        break;
                    }
                }
            }
            
        }
            
        if (isSpesial) {
            listAcct = (NSArray *) [userDefaults objectForKey:OBJ_SPESIAL];
        }else{
            if (arRoleFinance.count > 0 || arRoleFinance != nil) {
                for(NSString *xmenuId in arRoleFinance){
                    if ([xmenuId isEqualToString:@"00021"]) {
                        isFinance = true;
                        break;
                    }
                }
                
                if (isFinance) {
                    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_FINANCE];
                }else{
                    listAcct = (NSArray *) [userDefaults objectForKey:OBJ_ALL_ACCT];
                }
            }
        }
        
        if (listAcct.count != 0 || listAcct != nil) {
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            
            if ([language isEqualToString:@"id"]) {
                [newData addObject:@"Pilih nomor rekening"];
            }else{
                [newData addObject:@"Select account number"];
            }
            
            for(NSDictionary *temp in listAcct){
                [newData addObject:[temp valueForKey:@"id_account"]];
            }
            listAccount = newData;
            
            if ([newData count] == 1) {
                NSDictionary *idAccount = [newData objectAtIndex:0];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":[idAccount valueForKey:@"id_account"]}];
            }
        }else{
            isRequested = NO;
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_account1,menu_id,device,device_type,ip_address,language,date_local,customer_id"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
        }
}

//- (void) setupFrekuensi:(NSInteger) row{
//    self.vwConstant.constant = 70;
//    [self.vwInsideFrequency setHidden:NO];
//    [self.tfInsideFrekuensi setText:@""];
//
//    if(row == 0){
//        if([language isEqualToString:@"id"]){
//            [self.lblInsideFrekuensi setText:@"Pilih Hari"];
//            [self.tfInsideFrekuensi setPlaceholder:@"Pilih Hari"];
//            arrIntervalFreq = @[@"Minggu", @"Senin", @"Selasa", @"Rabu", @"Kamis", @"Jumat", @"Sabtu"];
//        }else{
//            [self.lblInsideFrekuensi setText:@"Select Day"];
//            [self.tfInsideFrekuensi setPlaceholder:@"Select Day"];
//            arrIntervalFreq = @[@"Sunday", @"Monday", @"Tuesday", @"Wednesday", @"Thrusday", @"Friday", @"Saturday"];
//        }
//    }else{
//        if([language isEqualToString:@"id"]){
//            [self.lblInsideFrekuensi setText:@"Pilih Tanggal"];
//            [self.tfInsideFrekuensi setPlaceholder:@"Pilih Tanggal"];
//        }else{
//            [self.lblInsideFrekuensi setText:@"Select Date"];
//            [self.tfInsideFrekuensi setPlaceholder:@"Select Date"];
//        }
//
//        NSMutableArray *arTanggal = [[NSMutableArray alloc]init];
//        for(int i=1 ; i<32; i++){
//            [arTanggal addObject:[NSString stringWithFormat:@"%d", i]];
//        }
//        if ([language isEqualToString:@"id"]) {
//            [arTanggal addObject:@"Akhir Bulan"];
//        }else{
//            [arTanggal addObject:@"End of Month"];
//        }
//        arrIntervalFreq = (NSArray*) arTanggal;
//    }
//
//    self.tfInsideFrekuensi.inputAccessoryView = toolbar;
//    [self createPicker:self.tfInsideFrekuensi withTag:2];
//
//}
@end
