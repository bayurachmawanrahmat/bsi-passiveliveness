//
//  OD01ViewController.m
//  BSM-Mobile
//
//  Created by ARS on 25/03/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "OD01ViewController.h"
#import "Utility.h"
#import "Styles.h"

@interface OD01ViewController (){
    int checkState1;
    int checkState2;
    NSString *urlSyarat;
    NSUserDefaults *userDefaults;
    NSString *language;
    
    UIView* blankView;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *iconTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet CustomBtn *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *label1;

@property (weak, nonatomic) IBOutlet UIView *viewCheck1;
@property (weak, nonatomic) IBOutlet UIView *viewCheck2;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck1;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheck2;
@property (weak, nonatomic) IBOutlet UILabel *labelCheck1;
@property (weak, nonatomic) IBOutlet UILabel *labelCheck2;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *vwContentScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *vwEmail;

@property (weak, nonatomic) IBOutlet UIView *vwSyarat;
@property (weak, nonatomic) IBOutlet UILabel *labelSyarat;

@end

@implementation OD01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    [_labelTitle setText:[self.jsonData objectForKey:@"title"]];
    [_iconTitle setImage: [UIImage imageNamed:[userDefaults objectForKey:@"imgIcon"]]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.nextButton setColorSet:PRIMARYCOLORSET];
    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    
    if([language isEqualToString:@"id"]){
        [_tfEmail setPlaceholder:@"Masukkan Email anda"];
        [_labelSyarat setText:@"Syarat dan Ketentuan (wajib di klik)"];
        [_cancelButton setTitle:@"BATAL" forState:UIControlStateNormal];
        [_nextButton setTitle:@"SELANJUTNYA" forState:UIControlStateNormal];
    }else{
        [_tfEmail setPlaceholder:@"Enter Your Email Here"];
        [_labelSyarat setText:@"Terms and Conditions (must be clicked)"];
        [_cancelButton setTitle:@"CANCEL" forState:UIControlStateNormal];
        [_nextButton setTitle:@"NEXT" forState:UIControlStateNormal];
    }
    
    blankView = [[UIView alloc]initWithFrame:CGRectMake(0, self.vwContentScroll.frame.origin.y, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [blankView setBackgroundColor:[UIColor whiteColor]];
    [self.vwContentScroll addSubview:blankView];
    
    [_vwEmail setHidden:YES];
    
    [_viewCheck1 setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapCheck1 =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(actionCheck1)];
    [_viewCheck1 addGestureRecognizer:tapCheck1];
    
    [_labelCheck2 setTextColor:UIColorFromRGB(const_color_gray)];
    
    [_vwSyarat setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapSyarat =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(openURLSyarat)];
    [_vwSyarat addGestureRecognizer:tapSyarat];
    [_labelSyarat setTextColor:UIColorFromRGB(const_color_primary)];
    
    [_cancelButton addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [_nextButton addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [Styles setTopConstant:_topConstraint];
    [Styles setStyleTextFieldBottomLine:_tfEmail];
//    [Styles setStyleButton:_cancelButton];
//    [Styles setStyleButton:_nextButton];
    
//    if(![[userDefaults objectForKey:@"email"]isEqualToString:@""]){
//        [_tfEmail setText:[userDefaults objectForKey:@"email"]];
//    }
    
    [self registerForKeyboardNotifications];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.bottomConstraint.constant = 30 + keyboardSize.height - self.vwContentScroll.safeAreaInsets.bottom;

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height/2 +30, 0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.bottomConstraint.constant =  30;

    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) createToolbarButton : (UITextField *)textField{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneEditButtonTapped:)];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [toolbar setItems:@[flexSpace,doneButton]];
    
    textField.inputAccessoryView = toolbar;
}

-(void)doneEditButtonTapped:(id)done{
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [self doRequest];
}

- (void) doRequest{
    NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
    [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];

}

- (void) openURLSyarat{
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:urlSyarat];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            [self->_labelCheck2 setTextColor:[UIColor blackColor]];
            [self->_viewCheck2 setUserInteractionEnabled:YES];
            UITapGestureRecognizer *tapCheck2 =
            [[UITapGestureRecognizer alloc] initWithTarget:self
                                                    action:@selector(actionCheck2)];
            [self->_viewCheck2 addGestureRecognizer:tapCheck2];
        }
    }];
}

- (void) actionCheck1{
    if(checkState1 == 0){
        checkState1 = 1;
        [_imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
    }else{
        checkState1 = 0;
        [_imgCheck1 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
    }
}

- (void) actionCheck2{
    if(checkState2 == 0){
        checkState2 = 1;
        [_imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
    }else{
        checkState2 = 0;
        [_imgCheck2 setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
    }
}

- (void) actionCancel{
    [self backToR];
}

- (void) actionNext{
    NSString *message = @"";
    if(checkState1 == 1 && checkState2 == 1){
        [self openNextTemplate];

//        if(![_tfEmail.text isEqualToString:@""]){
//            if([Utility validateEmailWithString:_tfEmail.text]){
//                [dataManager.dataExtra setValue:_tfEmail.text forKey:@"email"];
//                [self openNextTemplate];
//            }else{
//                if([language isEqualToString:@"id"]){
//                    message = @"Email anda tidak valid";
//                }else{
//                    message = @"Your Email is not valid";
//                }
//            }
//        }else{
//            if([language isEqualToString:@"id"]){
//                message = @"Email Wajib di isi";
//            }else{
//                message = @"You Have to Enter Email";
//            }
//        }
    }else{
        
        if([language isEqualToString:@"id"]){
            message = @"Anda wajib untuk checklis point diatas";
        }else{
            message = @"You have to check the point above";
        }
    }
    
    if(![message isEqualToString:@""]){
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:lang(@"INFO")
                                   message:message
                                   preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                       handler:nil];

        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"account_opening_info"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            
            _label1.text = [mResponse componentsSeparatedByString:@"|"][0];
            urlSyarat = [mResponse componentsSeparatedByString:@"|"][1];
            _labelCheck1.text = [mResponse componentsSeparatedByString:@"|"][3];
            if([language isEqualToString:@"id"]){
                _labelCheck2.text = @"Saya Telah Membaca Syarat Khusus Pembukaan Rekening dan Setuju.";
            }else{
                _labelCheck2.text = @"I have read the Account Opening Special Terms and Agree.";
            }
            
            [blankView removeFromSuperview];

        }else{
            [Utility showMessage:[jsonObject objectForKey:@"response"] mVwController:self mTag:[[jsonObject objectForKey:@"response_code"]intValue]];
        }
    }
    
}

@end
