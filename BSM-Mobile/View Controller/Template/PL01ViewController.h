//
//  PL01ViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 03/08/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PL01ViewController : TemplateViewController

- (void) setPassingData:(NSDictionary*)object;

@end

NS_ASSUME_NONNULL_END
