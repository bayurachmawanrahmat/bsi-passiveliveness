//
//  IBCardViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 12/03/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface IBCardViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
