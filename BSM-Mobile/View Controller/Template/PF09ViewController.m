//
//  PF03ViewController.m
//  BSM-Mobile
//
//  Created by Alikhsan on 06/08/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "PF09ViewController.h"

@interface PF09ViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSString *lang;
    NSUserDefaults *userDefault;
}

@property (weak, nonatomic) IBOutlet UITableView *tblPortofolio;
@property (nonatomic, retain) NSArray *dataSource;

@end

@implementation PF09ViewController

-(instancetype)initWithData : (NSArray *) mDataSource{
    self = [super init];
    if (self) {
        //[self.view setBackgroundColor:[UIColor greenColor]];
        [self setupLayout];
        userDefault = [NSUserDefaults standardUserDefaults];
        lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        self.tblPortofolio.delegate = self;
        self.tblPortofolio.dataSource = self;
        //[self.tblPortofolio setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        self.dataSource = mDataSource;
         //[self.tblPortofolio reloadData];
        [self.tblPortofolio beginUpdates];
        NSDictionary *dta = [self.dataSource objectAtIndex:0];
        UILabel *lbl = [[UILabel alloc]init];
        [lbl setText:[dta valueForKey:[dta.allKeys objectAtIndex:0]]];
        
        [self.view addSubview:lbl];
        NSLog(@"data source : %@", self.dataSource);
        NSLog(@"data label : %@", lbl.text);
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [self.tblPortofolio reloadData];
}


-(void) setupLayout{
    CGRect frmTblPortofolio = self.tblPortofolio.frame;
    frmTblPortofolio.origin.x = 0;
    frmTblPortofolio.origin.y = 0;
    frmTblPortofolio.size.width = SCREEN_WIDTH;
    frmTblPortofolio.size.height = SCREEN_HEIGHT;
    self.tblPortofolio.frame = frmTblPortofolio;
}

#pragma mark - table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"data Source : %lu", (unsigned long)self.dataSource.count);
    return self.dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PortCell" forIndexPath:indexPath];
    
    UIView *vwBackGrnd = (UIView *) [cell viewWithTag:114];
    UILabel *lblTitle = (UILabel *) [cell viewWithTag:110];
    UILabel *lblKey1 = (UILabel *) [cell viewWithTag:111];
    UILabel *lblKey2 = (UILabel *) [cell viewWithTag:112];
    UILabel *lblKey3 = (UILabel *) [cell viewWithTag:113];
    UILabel *lblVal1 = (UILabel *) [cell viewWithTag:114];
    UILabel *lblVal2 = (UILabel *) [cell viewWithTag:115];
    UILabel *lblVal3 = (UILabel *) [cell viewWithTag:116];
    
    
    NSDictionary *dataDict = [self.dataSource objectAtIndex:indexPath.row];
    [lblTitle setText:[dataDict valueForKey:@"key"]];
    NSArray *valArray = [dataDict objectForKey:@"value"];
    NSMutableDictionary *mutContent = [[NSMutableDictionary alloc]init];
    for (int i =0; i<valArray.count; i++) {
        [mutContent setValue:[valArray objectAtIndex:i] forKey:[NSString stringWithFormat:@"content_%d",i]];
    }
    NSDictionary *content_0 = [mutContent valueForKey:@"content_0"];
    [lblKey1 setText:[content_0.allKeys objectAtIndex:0]];
    [lblVal1 setText:[content_0 valueForKey:[content_0.allKeys objectAtIndex:0]]];
    
    NSDictionary *content_1 = [mutContent valueForKey:@"content_1"];
    [lblKey2 setText:[content_1.allKeys objectAtIndex:0]];
    [lblVal2 setText:[content_1 valueForKey:[content_1.allKeys objectAtIndex:0]]];
    
    NSDictionary *content_2 = [mutContent valueForKey:@"content_2"];
    [lblKey3 setText:[content_2.allKeys objectAtIndex:0]];
    [lblVal3 setText:[content_2 valueForKey:[content_2.allKeys objectAtIndex:0]]];
    
    //[vwBackGrnd setBackgroundColor:[UIColor grayColor]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 159;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
