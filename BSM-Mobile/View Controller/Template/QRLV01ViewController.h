//
//  QRLV01ViewController.h
//  BSM-Mobile
//
//  Created by BSM on 1/23/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

@interface QRLV01ViewController : TemplateViewController

- (void)setDataLocal:(NSDictionary*)object;
- (void)setFirstLV:(BOOL)firstLV;
- (void)setFromHome:(BOOL)fromHome;
- (void)setSpecial:(BOOL)special;
@property (nonatomic, retain) NSString * menu_file;
@end
