//
//  SPLV02ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/12/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPLV02ViewController.h"
#import "SPIBViewController.h"
#import "Styles.h"
#import "ShariapointConnect.h"
#import "CellLabel.h"
#import "Utility.h"

@interface SPLV02ViewController ()<UITableViewDelegate, UITableViewDataSource, ShariapointConnectDelegate>{
    NSArray *listData;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

@implementation SPLV02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Styles setTopConstant:self.topConstant];
    
    self.labelTitle.text = [dataManager.dataExtra valueForKey:@"redeemName"];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"CellLabel" bundle:nil] forCellReuseIdentifier:@"CELLLABEL"];
    self.tableView.tableFooterView = [[UIView alloc]init];
    
    if([self isBillTypeSV]){
        Connection *connn = [[Connection alloc]initWithDelegate:self];
        [connn sendPostParmUrl:@"request_type=list_denom,device,device_type,ip_address,language,date_local,customer_id,code" needLoading:YES encrypted:YES banking:nil];
    }else{
        ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
        [conn sendPostData:@"billerid,amount_transaction,cust_phone,charge_number,norek" endPoint:@"inquiry" needLoading:YES textLoading:@"" encrypted:YES];
    }
}

- (BOOL)isBillTypeSV{
    if([[dataManager.dataExtra valueForKey:@"type"]isEqualToString:@"SV"]){
        return YES;
    }
    return NO;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([jsonObject count]>0){
        listData = jsonObject;
        [self.tableView reloadData];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
            [self.navigationController pushViewController:sp animated:YES];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        if([endPoint isEqualToString:@"inquiry"]){
            NSLog(@"%@",dict);
            if([[dict objectForKey:@"response_code"]isEqualToString:@"00"]){
                NSDictionary *data = [dict objectForKey:@"message"];
                listData = [data objectForKey:@"BillItemList"];
                
                if([self isBillTypeSV]){
                    [self.labelTitle setText:lang(@"SELECT_AMOUNT")];
                }else{
                    for (NSDictionary *obj in [data objectForKey:@"BillInfoList"]) {
                        if([obj valueForKey:@"PEMBELIAN"]){
                            [self.labelTitle setText:[obj valueForKey:@"value"]];
                        }
                    }
                }
                
                [self.tableView reloadData];
            }else{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[dict objectForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                if (@available(iOS 13.0, *)) {
                    [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellLabel *cell = (CellLabel*)[tableView dequeueReusableCellWithIdentifier:@"CELLLABEL" forIndexPath:indexPath];
    
    NSString *name = @"";
    
    if([self isBillTypeSV]){
        name = [listData[indexPath.row] objectForKey:@"name"];
        cell.labelText.text = name;
        
    }else{
        name = [NSString stringWithFormat:@"%@ - %@",[listData[indexPath.row] objectForKey:@"billName"],
                [Utility formatCurrencyValue:[[listData[indexPath.row] valueForKey:@"billAmount"]doubleValue]]];
        cell.labelText.text = name;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if([self isBillTypeSV]){
        [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"id_denom"] forKey:@"billAmount"];
        [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"id_denom"] forKey:@"amount_transaction"];
        [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"id_denom"] forKey:@"billCode"];
    }else{
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%@",[listData[indexPath.row] objectForKey:@"billAmount"]] forKey:@"amount_transaction"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%@",[listData[indexPath.row] objectForKey:@"billCode"]] forKey:@"billCode"];
        [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%@",[listData[indexPath.row] objectForKey:@"billName"]] forKey:@"billerName"];
//        [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"billAmount"] forKey:@"amount_transaction"];
//        [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"billCode"] forKey:@"billCode"];
//        [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"billName"] forKey:@"billerName"];
    }
    
//    TemplateViewController *spcf = [self.storyboard instantiateViewControllerWithIdentifier:@"SPCF"];
//    [self.navigationController pushViewController:spcf animated:YES];
    SPIBViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"SPIB"];
    [next setCategory:@"pin"];
    [self.navigationController pushViewController:next animated:YES];
//    SPIBViewController *spInformation = [[UIStoryboard storyboardWithName:@"Shariapoint" bundle:nil] instantiateViewControllerWithIdentifier: @"SPIB"];
//    [self.navigationController pushViewController:spInformation animated:YES];
}

@end
