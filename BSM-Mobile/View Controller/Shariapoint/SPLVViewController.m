//
//  SPLVViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPLVViewController.h"
#import "SPIBViewController.h"
#import "CellLabel.h"
#import "Styles.h"
#import "ShariapointConnect.h"
//#import "SPDataManager.h"

@interface SPLVViewController ()<UITableViewDelegate, UITableViewDataSource, ShariapointConnectDelegate>{
    NSArray *listData;
    NSString *endpointurlbillerid;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;

@end

@implementation SPLVViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Styles setTopConstant:self.topConstant];
    
//    listData = @[@"XL Pra Bayar",@"Telkomsel", @"Tri Pra Bayar", @"Smartfren Pra Bayar", @"Indosat Pra Bayar"];
    self.labelTitle.text = [dataManager.dataExtra valueForKey:@"redeemName"];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"CellLabel" bundle:nil] forCellReuseIdentifier:@"CELLLABEL"];
    self.tableView.tableFooterView = [[UIView alloc]init];
    
    //https://taurus.akhdani.net/api/syariahpoint/detail-biller-category?billercategoryid=2
//    billerCategoryID = [[SPDataManager sharedManager].dataExtra valueForKey:@"billercategoryid"];
    endpointurlbillerid = [NSString stringWithFormat:@"list-biller?billercategoryid=%@",billerCategoryID];
    ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
    [conn getDataFromEndPoint:endpointurlbillerid needLoading:YES textLoading:@"" encrypted:NO];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellLabel *cell = (CellLabel*)[tableView dequeueReusableCellWithIdentifier:@"CELLLABEL" forIndexPath:indexPath];
    cell.labelText.text = [listData[indexPath.row] objectForKey:@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"billerid"] forKey:@"billerid"];
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"service_code"] forKey:@"code"];
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"isneedinquiry"] forKey:@"isNeedInquiry"];
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"description"] forKey:@"description"]; //for title
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"name"] forKey:@"billerName"];
    
    [dataManager.dataExtra setValue:[[listData[indexPath.row] objectForKey:@"type"]
                                     stringByReplacingOccurrencesOfString:@" " withString:@""]
                             forKey:@"type"];
    [dataManager.dataExtra setValue:@"0" forKey:@"amount_transaction"];
    
    SPIBViewController *spInformation = [[UIStoryboard storyboardWithName:@"Shariapoint" bundle:nil] instantiateViewControllerWithIdentifier: @"SPIB"];
    [self.navigationController pushViewController:spInformation animated:YES];
}

- (void)setBillerCategoryID:(NSString*)number{
    billerCategoryID = number;
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
            [self.navigationController pushViewController:sp animated:YES];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        if([endPoint isEqualToString:@"list-biller"]){
            NSArray *arrData = [dict objectForKey:@"response"];
            listData = arrData;
            [self.tableView reloadData];
        }
    }
}

@end
