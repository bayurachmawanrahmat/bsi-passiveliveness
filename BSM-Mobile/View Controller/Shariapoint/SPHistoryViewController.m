//
//  SPHistoryViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPHistoryViewController.h"
#import "SPListHistoryViewController.h"
#import "Styles.h"
#import "ShariapointConnect.h"

@interface SPHistoryViewController ()<UITableViewDataSource, UITableViewDelegate, ShariapointConnectDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
    
    UIToolbar *toolbar;
    UIDatePicker *datePickerFrom;
    UIDatePicker *datePickerTo;
    
    NSArray *lisData;
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *titleBarIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwHistoriForm;
@property (weak, nonatomic) IBOutlet UILabel *lblDateStart;
@property (weak, nonatomic) IBOutlet UITextField *tfDateStart;
@property (weak, nonatomic) IBOutlet UILabel *lblDateEnd;
@property (weak, nonatomic) IBOutlet UITextField *tfDateEnd;
@property (weak, nonatomic) IBOutlet UITableView *tableViewHistori;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleForm;
@property (weak, nonatomic) IBOutlet UILabel *labelCurrentPoint;
@property (weak, nonatomic) IBOutlet UILabel *labelCollectedPoint;
@property (weak, nonatomic) IBOutlet UILabel *labelRedeemedPoint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation SPHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [Styles setTopConstant:self.topConstant];
    
    [self.vwHistoriForm setHidden:NO];
    [self.tableViewHistori setHidden:YES];
    
    self.titleBarIcon.text = lang(@"BSI_POINT");
    self.lblTitle.text = lang(@"HISTORY_POINT");
    self.labelTitleForm.text = lang(@"HISTORY_POINT");
    self.lblDateStart.text = lang(@"DATE_START");
    self.lblDateEnd.text = lang(@"DATE_END");
    [self.btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [self.btnNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    
    self.labelCurrentPoint.text = [NSString stringWithFormat:@"%@ :", lang(@"CURRENT_POINT")];
    self.labelRedeemedPoint.text = [NSString stringWithFormat:@"%@ :", lang(@"REDEEMED_POINT")];
    self.labelCollectedPoint.text = [NSString stringWithFormat:@"%@ :", lang(@"COLLECTED_POINT")];
    
    self.titleBarIcon.textColor = UIColorFromRGB(const_color_title);
    self.titleBarIcon.textAlignment = const_textalignment_title;
    self.titleBarIcon.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBarIcon.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    
    [self.btnCancel addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];

    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self stylingTextField:self.tfDateEnd];
    [self stylingTextField:self.tfDateStart];
    
    ShariapointConnect *conn1 = [[ShariapointConnect alloc]initWithDelegate:self];
    ShariapointConnect *conn2 = [[ShariapointConnect alloc]initWithDelegate:self];
    ShariapointConnect *conn3 = [[ShariapointConnect alloc]initWithDelegate:self];
    
    NSString *urls1 = [NSString stringWithFormat:@"total-generated-point?norek=%@",[userDefaults objectForKey:@"first_account"]];
    NSString *urls2 = [NSString stringWithFormat:@"total-redeem-point?norek=%@",[userDefaults objectForKey:@"first_account"]];
    NSString *urls3 = [NSString stringWithFormat:@"total-point?norek=%@",[userDefaults objectForKey:@"first_account"]];

    [conn1 getDataFromEndPoint:urls1 needLoading:YES textLoading:@"" encrypted:NO];
    [conn2 getDataFromEndPoint:urls2 needLoading:YES textLoading:@"" encrypted:NO];
    [conn3 getDataFromEndPoint:urls3 needLoading:YES textLoading:@"" encrypted:NO];

    [self setDatePickerLayout];
}


- (void) stylingTextField : (UITextField*)textField{
    CGFloat height = (textField.frame.size.height)-20;
    
    UIImageView *dropIcon = [[UIImageView alloc] init];
    [dropIcon setFrame:CGRectMake(0, 0, height, height)];
    dropIcon.image = [UIImage imageNamed:@"baseline_keyboard_arrow_down_black_48pt"];
    dropIcon.tintColor = UIColorFromRGB(const_color_primary);
    dropIcon.contentMode = UIViewContentModeScaleToFill;
    
    UIView *sideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, height, height)];
    [sideView addSubview:dropIcon];
    textField.rightViewMode = UITextFieldViewModeAlways;
    textField.rightView = sideView;
}

-(void)setDatePickerLayout{
    
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(actionDone)]];
    
    datePickerFrom = [[UIDatePicker alloc]init];
    
    if (@available(iOS 13.4, *)) {
        [datePickerFrom setPreferredDatePickerStyle:UIDatePickerStyleWheels];
    }
    
    datePickerFrom.tag = 1;
    [datePickerFrom setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"id_ID"]];
    [datePickerFrom setBackgroundColor:[UIColor whiteColor]];
    [datePickerFrom setDatePickerMode:UIDatePickerModeDate];
    [datePickerFrom addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
//    datePickerFrom.minimumDate = [self getDateDifference:-30 withDate:[NSDate date]];
//    datePickerFrom.maximumDate = [NSDate date];
    
    self.tfDateStart.inputAccessoryView = toolbar;
    self.tfDateStart.inputView = datePickerFrom;
    
    datePickerTo = [[UIDatePicker alloc]init];
    if (@available(iOS 13.4, *)) {
        [datePickerTo setPreferredDatePickerStyle:UIDatePickerStyleWheels];
    }
    datePickerTo.tag = 2;
    [datePickerTo setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"id_ID"]];
    [datePickerTo setBackgroundColor:[UIColor whiteColor]];
    [datePickerTo setDatePickerMode:UIDatePickerModeDate];
    [datePickerTo addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
//    datePickerTo.minimumDate = [self getDateDifference:-30 withDate:[NSDate date]];
//    datePickerTo.maximumDate = [NSDate date];
    
    self.tfDateEnd.inputAccessoryView = toolbar;
    self.tfDateEnd.inputView = datePickerTo;
}

- (void)dateChanged:(UIDatePicker*)pickerDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString* selectedDate = [dateFormat stringFromDate:pickerDate.date];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"SPH_MAX_HISTORY") preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    
    if(pickerDate == datePickerFrom){
        self.tfDateStart.text = selectedDate;
//        datePickerTo.minimumDate = pickerDate.date;
//        datePickerTo.maximumDate = [self getDateDifference:+30 withDate:pickerDate.date];
//        NSLog(@"%@",pickerDate.date);
//        NSLog(@"%@",datePickerTo.date);
//        NSLog(@"%@",[self getDateDifference:+30 withDate:pickerDate.date]);
        
        if(![self.tfDateEnd.text isEqualToString:@""]){
            if([datePickerTo.date compare:[self getDateDifference:+30 withDate:pickerDate.date]] == NSOrderedDescending){
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
                    self.tfDateStart.text = @"";
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }else{
        self.tfDateEnd.text = selectedDate;
        if(![self.tfDateStart.text isEqualToString:@""]){
            
//            NSLog(@"%@",pickerDate.date);
//            NSLog(@"%@",datePickerFrom.date);
//            NSLog(@"%@",[self getDateDifference:-30 withDate:pickerDate.date]);
            if([datePickerFrom.date compare:[self getDateDifference:-30 withDate:pickerDate.date]] == NSOrderedAscending){
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
                    self.tfDateEnd.text = @"";
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
    }
}

- (NSDate*)getDateDifference:(NSInteger)number withDate:(NSDate*)date{
    NSDate *today = date;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay: +(number)]; // note that I'm setting it to -1
    NSDate *endOfWorldWar3 = [gregorian dateByAddingComponents:offsetComponents toDate:today options:0];
    return endOfWorldWar3;
}

- (void)actionDone{
    [self.view endEditing:YES];
}

- (void) actionBack{
    TemplateViewController *spPointInfo = [self.storyboard instantiateViewControllerWithIdentifier: @"Shariapoint"];
    [self.navigationController pushViewController:spPointInfo animated:YES];}

- (void) actionNext{
    if([self.tfDateStart.text isEqualToString:@""] && [self.tfDateEnd.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"DATE_REQUIRED") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else if([self.tfDateStart.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"STAR_DATE_REQUIRED") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else if([self.tfDateEnd.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"END_DATE_REQUIRED") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        UIStoryboard *storyboardUI = [UIStoryboard storyboardWithName:@"Shariapoint" bundle:nil];
        SPListHistoryViewController *spListHistory = [storyboardUI instantiateViewControllerWithIdentifier: @"SPLHistory"];
        [spListHistory setDateEnd:self.tfDateEnd.text];
        [spListHistory setDateStart:self.tfDateStart.text];
        [self.navigationController pushViewController:spListHistory animated:YES];
    }
    
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
            [self.navigationController pushViewController:sp animated:YES];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        NSLog(@"%@",dict);

        if([endPoint isEqualToString:@"total-generated-point"]){
            NSString* point = [dict valueForKey:@"response"];
            [self.labelCollectedPoint setText:[NSString stringWithFormat:@"%@ : %@",lang(@"COLLECTED_POINT"),[Utility formatCurrencyValue:[point doubleValue] withSeparator:@"."]]];
        }else if ([endPoint isEqualToString:@"total-redeem-point"]){
            NSString* point = [dict valueForKey:@"response"];
            [self.labelRedeemedPoint setText:[NSString stringWithFormat:@"%@ : %@",lang(@"REDEEMED_POINT"),[Utility formatCurrencyValue:[point doubleValue] withSeparator:@"."]]];
        }else if([endPoint isEqualToString:@"total-point"]){
            NSDictionary *data = [dict objectForKey:@"response"];
            NSString* point = [data valueForKey:@"point"];
            [self.labelCurrentPoint setText:[NSString stringWithFormat:@"%@ : %@",lang(@"CURRENT_POINT"),[Utility formatCurrencyValue:[point doubleValue] withSeparator:@"."]]];
        }
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
