//
//  SPGENCFViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPGENCFViewController.h"
#import "CustomBtn.h"
#import "ShariapointConnect.h"
#import "InboxHelper.h"
#import "Utility.h"
#import "AESCipher.h"

@interface SPGENCFViewController ()<ShariapointConnectDelegate>

@property (nonatomic) AESCipher *aesChiper;
@property (weak, nonatomic) IBOutlet UIView *vwBackground;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonOK;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonShare;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *msgTitle;
@property (weak, nonatomic) IBOutlet UILabel *msgContent;
@property (weak, nonatomic) IBOutlet UILabel *msgFooter;

@end

@implementation SPGENCFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.aesChiper = [[AESCipher alloc]init];
    
    [self.labelTitle setText:[dataManager.dataExtra valueForKey:@"redeemName"]];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.vwBackground setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ic_strklogo_sgl_iover_v2"]]];
    
    [self.buttonShare addTarget:self action:@selector(actionShare) forControlEvents:UIControlEventTouchUpInside];
    if ([Utility isLanguageID]) {
        [_buttonShare setTitle:@"Bagikan" forState:UIControlStateNormal];
    }else{
        [_buttonShare setTitle:@"Share" forState:UIControlStateNormal];
    }
    [_buttonShare setImage:[UIImage imageNamed:@"ic_bt_share_prm"] forState:UIControlStateNormal];
    _buttonShare.layer.borderColor = [UIColorFromRGB(const_color_darkprimary)CGColor];
    _buttonShare.backgroundColor = UIColorFromRGB(const_color_basecolor);
    _buttonShare.layer.borderWidth = 2;
    [_buttonShare setTitleColor:UIColorFromRGB(const_color_primary) forState:UIControlStateNormal];
    [_buttonShare setTintColor:UIColorFromRGB(const_color_primary)];
    
    [self.buttonOK addTarget:self action:@selector(actionOK) forControlEvents:UIControlEventTouchUpInside];
    _buttonOK.layer.borderWidth = 2;
    _buttonOK.layer.backgroundColor = [UIColorFromRGB(0xF8AD3C)CGColor];
    _buttonOK.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
    
    if([self isBillTypeSV]){
        [dataManager.dataExtra setValue:@"" forKey:@"timezone"];
    }else{
        [dataManager.dataExtra setValue:@"UTC%2B7" forKey:@"timezone"];
    }
    
    ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
    [conn sendPostData:@"billerid,billCode,timezone,amount_transaction,cust_phone,charge_number,norek,cust_name,inq_transaction_id,inq_transaction_fee" endPoint:@"redeem-point" needLoading:YES textLoading:@"" encrypted:YES];
    
}

- (BOOL)isBillTypeSV{
    if([[dataManager.dataExtra valueForKey:@"type"]isEqualToString:@"SV"]){
        return YES;
    }
    return NO;
}

- (void) actionOK{
    TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
    [self.navigationController pushViewController:sp animated:YES];
}

-(void)actionShare{
    NSDictionary* userInfo = @{@"position": @(1114)};
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"listenerNavigate" object:self userInfo:userInfo];
    
    UIGraphicsBeginImageContextWithOptions(self.vwContent.bounds.size, YES, 0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.vwContent.layer renderInContext:context];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    NSArray* sharedObjects=[NSArray arrayWithObjects:@"",  snapshotImage, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc]                                                                initWithActivityItems:sharedObjects applicationActivities:nil];
    activityViewController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityViewController animated:YES completion:nil];
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
            [self.navigationController pushViewController:sp animated:YES];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        NSLog(@"%@",dict);
        if([[dict objectForKey:@"response_code"] isEqualToString:@"00"]){
            
            NSString *response = [dict objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            
            
            NSString *title = [[dataDict valueForKey:@"title"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            NSString *trxref = [dataDict valueForKey:@"trxref"];
            NSString *msg = @"";
            NSString *footer_msg = @"";
            NSString *transaction_id = @"";
            
            if([Utility isLanguageID]){
                msg = [dataDict valueForKey:@"message"];
                footer_msg = [dataDict valueForKey:@"footer_message"];
            }else{
                msg = [dataDict valueForKey:@"message_en"];
                footer_msg = [dataDict valueForKey:@"footer_message_en"];
            }

//            msg = [msg stringByReplacingOccurrencesOfString:@"\\\n" withString:@"\n"];
//            footer_msg = [footer_msg stringByReplacingOccurrencesOfString:@"\\\n" withString:@"\n"];
//
            msg = [msg stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
            footer_msg = [footer_msg stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];

            transaction_id = [NSString stringWithFormat:@"%@", [dict valueForKey:@"transaction_id"]];

            
            self.msgTitle.text = title;
            self.msgContent.text = msg;
            self.msgFooter.text = footer_msg;
            
            NSMutableDictionary *dictodb = [[NSMutableDictionary alloc]init];
            [dictodb setValue:trxref forKey:@"trxref"];
            [dictodb setValue:title forKey:@"title"];
            [dictodb setValue:msg forKey:@"msg"];
            [dictodb setValue:footer_msg forKey:@"footer_msg"];
            [dictodb setValue:transaction_id forKey:@"transaction_id"];
            
            [self saveToDB:dictodb];
            
        }else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void) responseUBP:(NSDictionary *)dict{
    if([[dict objectForKey:@"response_code"] isEqualToString:@"00"]){
        
        NSDictionary *resp = [dict objectForKey:@"response"];
        
        NSString *title = [resp valueForKey:@"title"];
        NSString *msg = [self replacementText:[resp valueForKey:@"message"]];
        NSString *footer_msg = [resp valueForKey:@"footer_message"];

        NSString *trxref = [self replacementText:[resp valueForKey:@"trxref"]];
        
        NSMutableDictionary *dictodb = [[NSMutableDictionary alloc]init];
        [dictodb setValue:trxref forKey:@"trxref"];
        [dictodb setValue:title forKey:@"title"];
        [dictodb setValue:msg forKey:@"msg"];
        [dictodb setValue:footer_msg forKey:@"footer_msg"];
        [dictodb setValue:[dict objectForKey:@"transaction_id"] forKey:@"transaction_id"];
        
        [self saveToDB:dictodb];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (NSString*)replacementText : (NSString*)str{
    NSString *result = str;
    
    result = [result stringByReplacingOccurrencesOfString:@"{NOMORREFERENCE}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"reference"]];
    result = [result stringByReplacingOccurrencesOfString:@"{NOMORVOUCHER}" withString:@""];
    result = [result stringByReplacingOccurrencesOfString:@"{NOMINALTRANS}" withString:@""];
    result = [result stringByReplacingOccurrencesOfString:@"{NAMACUSTOMER}" withString:@""];

    result = [result stringByReplacingOccurrencesOfString:@"{REFERENCE_TRANSAKSI}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"reference"]];
    result = [result stringByReplacingOccurrencesOfString:@"{NOMINAL}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"reference"]];
    result = [result stringByReplacingOccurrencesOfString:@"{KWH}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"18_kwh"]];
    result = [result stringByReplacingOccurrencesOfString:@"{NOMOR_TOKEN}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"19_token"]];
    result = [result stringByReplacingOccurrencesOfString:@"{BIAYA_ADMIN}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"1_biaya_admin"]];//transaction_fee
    result = [result stringByReplacingOccurrencesOfString:@"{SEGMENT}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"3_segment"]];
    result = [result stringByReplacingOccurrencesOfString:@"{CATEGORY}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"4_category"]];
    result = [result stringByReplacingOccurrencesOfString:@"{ID_PELANGGAN}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"24_id_pelanggan"]];
    result = [result stringByReplacingOccurrencesOfString:@"{METER_ID}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"23_meter_id"]];
    result = [result stringByReplacingOccurrencesOfString:@"{NAMA_CUSTOMER}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"name"]];
    result = [result stringByReplacingOccurrencesOfString:@"{NOMOR_REFERENCE_PAYMENT}" withString:[[dataManager.dataExtra valueForKey:@"private_additional_data"]valueForKey:@"2_switcher_reference"]];
    
    
    return result;
}

- (void) saveToDB:(NSDictionary*) dict{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSString *currentDate = [dateFormatter stringFromDate:date];
    
    NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
    [dictList setValue:[dict objectForKey:@"trxref"] forKey:@"trxref"];
    [dictList setValue:[dict objectForKey:@"title"] forKey:@"title"];
    [dictList setValue:[dict objectForKey:@"msg"] forKey:@"msg"];
    [dictList setValue:[dict objectForKey:@"footer_msg"] forKey:@"footer_msg"];
    [dictList setValue:[dict objectForKey:@"transaction_id"] forKey:@"transaction_id"];
    [dictList setValue:@"GENCF02" forKey:@"template"];
    [dictList setValue:@"General" forKey:@"jenis"];
    [dictList setValue:currentDate forKey:@"time"];
    [dictList setValue:@"0" forKey:@"marked"];
    
    InboxHelper *inboxHelper = [[InboxHelper alloc]init];
    [inboxHelper insertDataInbox:dictList];
    
//    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    NSString *strCID = [NSString stringWithFormat:@"%@",[NSUserdefaultsAes getValueForKey:@"customer_id"]];
    
    NSMutableArray *arBackupInbox = [[NSMutableArray alloc]init];
    
    NSDictionary *backupInbox = [Utility dictForKeychainKey:strCID];
    NSMutableDictionary *contentBackup = [[NSMutableDictionary alloc]init];
    
    if(backupInbox){
        NSArray *arrLastBackup = [backupInbox valueForKey:@"data"];
        for(NSDictionary *dictLastBackup in arrLastBackup){
            if([dictLastBackup valueForKey:@"content"] != nil){
                NSDictionary *data = @{@"content" : [dictLastBackup valueForKey:@"content"]};
                [arBackupInbox addObject:data];
            }
        }
    }
    
    NSDictionary *dataDd = [self.aesChiper dictAesEncryptString:dictList];
    
    [contentBackup setObject:dataDd forKey:@"content"];
    [arBackupInbox addObject:contentBackup];
    
    if(arBackupInbox.count > 0){
        NSMutableDictionary *storeBackupInbox = [[NSMutableDictionary alloc]init];
        [storeBackupInbox setObject:arBackupInbox forKey:@"data"];
        if(storeBackupInbox){
            [Utility removeForKeychainKey:strCID];
            [Utility setDict:storeBackupInbox forKey:strCID];
        }
    }
    
}

@end
