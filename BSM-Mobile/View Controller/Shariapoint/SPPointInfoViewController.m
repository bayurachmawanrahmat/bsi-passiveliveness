//
//  SPPointInfoViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPPointInfoViewController.h"
#import "Styles.h"
#import "ShariapointConnect.h"

@interface SPPointInfoViewController ()<ShariapointConnectDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightVwContent;

@end

@implementation SPPointInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    [Styles setTopConstant:self.topConstant];
    
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.lblPoints setTextColor:UIColorFromRGB(const_color_primary)];
    [self.lblPoints.layer setBorderWidth:1];
    [self.lblPoints.layer setBorderColor:[UIColorFromRGB(const_color_secondary)CGColor]];
    [self.lblPoints.layer setCornerRadius:10];
    
//    [self viewPointInformation];
    ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
    [conn getDataFromEndPoint:@"api/syariahpoint/detail-point?pointid=1" needLoading:YES textLoading:@"" encrypted:NO];
    
    ShariapointConnect *conna = [[ShariapointConnect alloc]initWithDelegate:self];
    [conna getDataFromEndPoint:@"api/syariahpoint/total-point?cif=7001305217" needLoading:YES textLoading:@"" encrypted:NO];
}

- (void) viewPointInformation{
    
    CGFloat sizeCricle = (SCREEN_WIDTH - 80)/2;
    CGFloat originXCircle = 50;
    CGFloat originYCircle = 0;
    CGFloat originXRect = (sizeCricle - (sqrt(0.5*(sizeCricle*sizeCricle))))/2;
    CGFloat sizeRect = sizeCricle - (originXRect*2);
    CGFloat radius = sizeCricle/2;
    CGFloat borderWidth = 2;
    
    UIView *circle1 = [[UIView alloc]initWithFrame:CGRectMake(originXCircle, originYCircle , sizeCricle, sizeCricle)];
    [circle1.layer setCornerRadius:radius];
    [circle1.layer setBorderWidth:borderWidth];
    [circle1.layer setBorderColor:[[UIColor yellowColor]CGColor]];
    
    UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(originXRect, originXRect, sizeRect, sizeRect)];
    [lbl1 setAttributedText:[self getText:@"101.000" with:[UIColor yellowColor]]];
    [lbl1 setTextAlignment:NSTextAlignmentCenter];
    [lbl1 setNumberOfLines:0];
    [lbl1 setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat originXCircle2 = originXCircle+sizeRect;
    CGFloat originYCircle2 = originYCircle+sizeCricle-(sizeRect/3);
    
    UIView *circle2 = [[UIView alloc]initWithFrame:CGRectMake(originXCircle2, originYCircle2, sizeCricle, sizeCricle)];
    [circle2.layer setCornerRadius:radius];
    [circle2.layer setBorderWidth:borderWidth];
    [circle2.layer setBorderColor:[[UIColor redColor]CGColor]];

    UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(originXRect, originXRect, sizeRect, sizeRect)];
    [lbl2 setAttributedText:[self getText:@"101.000" with:[UIColor redColor]]];
    [lbl2 setTextAlignment:NSTextAlignmentCenter];
    [lbl2 setNumberOfLines:0];
    [lbl2 setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat originXCircle3 = (originXCircle2 - (sizeCricle));
    CGFloat originYCircle3 = (originYCircle2 + (sizeRect/3));
    
    UIView *circle3 = [[UIView alloc]initWithFrame:CGRectMake(originXCircle3, originYCircle3, sizeCricle, sizeCricle)];
    [circle3.layer setCornerRadius:radius];
    [circle3.layer setBorderWidth:borderWidth];
    [circle3.layer setBorderColor:[[UIColor greenColor]CGColor]];

    
    UILabel *lbl3 = [[UILabel alloc]initWithFrame:CGRectMake(originXRect, originXRect, sizeRect, sizeRect)];
    [lbl3 setAttributedText:[self getText:@"101.000" with:[UIColor greenColor]]];
    [lbl3 setTextAlignment:NSTextAlignmentCenter];
    [lbl3 setNumberOfLines:0];
    [lbl3 setLineBreakMode:NSLineBreakByWordWrapping];
    
    [circle1 addSubview:lbl1];
    [self.vwContent addSubview:circle1];
    
    [circle2 addSubview:lbl2];
    [self.vwContent addSubview:circle2];

    [circle3 addSubview:lbl3];
    [self.vwContent addSubview:circle3];
    
    self.heightVwContent.constant = 400;
}

- (void) viewPointInformation:(NSString*) note1 two:(NSString*) note2 three:(NSString*) note3{
    
    CGFloat sizeCricle = (SCREEN_WIDTH - 80)/2;
    CGFloat originXCircle = 50;
    CGFloat originYCircle = 0;
    CGFloat originXRect = (sizeCricle - (sqrt(0.5*(sizeCricle*sizeCricle))))/2;
    CGFloat sizeRect = sizeCricle - (originXRect*2);
    CGFloat radius = sizeCricle/2;
    CGFloat borderWidth = 2;
    
    UIView *circle1 = [[UIView alloc]initWithFrame:CGRectMake(originXCircle, originYCircle , sizeCricle, sizeCricle)];
    [circle1.layer setCornerRadius:radius];
    [circle1.layer setBorderWidth:borderWidth];
    [circle1.layer setBorderColor:[[UIColor yellowColor]CGColor]];
    
    UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(originXRect, originXRect, sizeRect, sizeRect)];
    [lbl1 setText:note1];
    [lbl1 setTextAlignment:NSTextAlignmentCenter];
    [lbl1 setNumberOfLines:0];
    [lbl1 setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat originXCircle2 = originXCircle+sizeRect;
    CGFloat originYCircle2 = originYCircle+sizeCricle-(sizeRect/3);
    
    UIView *circle2 = [[UIView alloc]initWithFrame:CGRectMake(originXCircle2, originYCircle2, sizeCricle, sizeCricle)];
    [circle2.layer setCornerRadius:radius];
    [circle2.layer setBorderWidth:borderWidth];
    [circle2.layer setBorderColor:[[UIColor redColor]CGColor]];

    UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(originXRect, originXRect, sizeRect, sizeRect)];
    [lbl2 setText:note2];
    [lbl2 setTextAlignment:NSTextAlignmentCenter];
    [lbl2 setNumberOfLines:0];
    [lbl2 setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat originXCircle3 = (originXCircle2 - (sizeCricle));
    CGFloat originYCircle3 = (originYCircle2 + (sizeRect/3));
    
    UIView *circle3 = [[UIView alloc]initWithFrame:CGRectMake(originXCircle3, originYCircle3, sizeCricle, sizeCricle)];
    [circle3.layer setCornerRadius:radius];
    [circle3.layer setBorderWidth:borderWidth];
    [circle3.layer setBorderColor:[[UIColor greenColor]CGColor]];

    
    UILabel *lbl3 = [[UILabel alloc]initWithFrame:CGRectMake(originXRect, originXRect, sizeRect, sizeRect)];
    [lbl3 setText:note3];
    [lbl3 setTextAlignment:NSTextAlignmentCenter];
    [lbl3 setNumberOfLines:0];
    [lbl3 setLineBreakMode:NSLineBreakByWordWrapping];
    
    [circle1 addSubview:lbl1];
    [self.vwContent addSubview:circle1];
    
    [circle2 addSubview:lbl2];
    [self.vwContent addSubview:circle2];

    [circle3 addSubview:lbl3];
    [self.vwContent addSubview:circle3];
    
    self.heightVwContent.constant = 400;
}

- (NSMutableAttributedString*) getText : (NSString*) point with: (UIColor*)color{
    NSMutableAttributedString *amount = [[NSMutableAttributedString alloc]initWithString:point attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:15], NSForegroundColorAttributeName : color}];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc]initWithString:@"Jumlah Poin Terkumpul " attributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:15]}];
    
    NSMutableAttributedString *final = [[NSMutableAttributedString alloc]initWithString:@""];

    [final appendAttributedString:text];
    [final appendAttributedString:amount];
    
    return final;
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        
    }else{
        if([endPoint isEqualToString:@"detail-point"]){
            NSDictionary *data = [dict objectForKey:@"response"];
            [self viewPointInformation:[data objectForKey:@"note1"] two:[data objectForKey:@"note2"] three:[data objectForKey:@"note3"]];
        }else if([endPoint isEqualToString:@"total-point"]){
            NSDictionary *data = [dict objectForKey:@"response"];
            [self.lblPoints setText:[data objectForKey:@"point"]];
        }
    }
}

@end
