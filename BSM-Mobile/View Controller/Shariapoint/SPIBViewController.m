//
//  SPIBViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPIBViewController.h"
#import "Styles.h"
#import "CustomBtn.h"

@interface SPIBViewController (){
    NSString *inCategory;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelInputLabel;
@property (weak, nonatomic) IBOutlet UITextField *tfInput;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonBatal;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@end

@implementation SPIBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 35)];
    toolbar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(actionDone)];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
    
    [toolbar setItems:@[flexSpace, doneButton]];
    [self.tfInput setInputAccessoryView:toolbar];
    
    [Styles setTopConstant:self.topConstant];
    [Styles setStyleTextFieldBottomLine:self.tfInput];
    
    self.titleLabel.text = [dataManager.dataExtra valueForKey:@"redeemName"];
    self.titleLabel.textColor = UIColorFromRGB(const_color_title);
    self.titleLabel.textAlignment = const_textalignment_title;
    self.titleLabel.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleLabel.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    if([inCategory isEqualToString:@"pin"]){
        self.labelInputLabel.text = lang(@"INSERT_PIN");
        self.tfInput.keyboardType = UIKeyboardTypeNumberPad;
        self.tfInput.secureTextEntry = YES;
    }else{
        self.labelInputLabel.text = lang(@"INSERT_IDNUMBER");
        self.tfInput.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    [self.buttonNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.buttonBatal setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonBatal addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    [self.buttonBatal setColorSet:SECONDARYCOLORSET];
}

- (void) actionDone{
    [self.view endEditing:YES];
}

- (void) actionNext{
    if([self.tfInput.text isEqualToString:@""]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"NOT_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
    
        if([inCategory isEqualToString:@"pin"]){
            [dataManager setPinNumber:self.tfInput.text];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:@"request_type=verify_pin,pin" needLoading:YES textLoading:@"" encrypted:YES banking:YES];
        }else{
            [dataManager.dataExtra setValue:self.tfInput.text forKey:@"charge_number"];
            [dataManager.dataExtra setValue:self.tfInput.text forKey:@"cust_phone"];
//            [dataManager.dataExtra setValue:@"0" forKey:@"amount_transaction"];
            
            TemplateViewController *spcf = [self.storyboard instantiateViewControllerWithIdentifier:@"SPLV02"];
            [self.navigationController pushViewController:spcf animated:YES];
        }
    }
}

- (void) actionCancel{
    TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
    [self.navigationController pushViewController:sp animated:YES];
}

- (void)setCategory:(NSString *)category{
    inCategory = category;
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
        TemplateViewController *spcf = [self.storyboard instantiateViewControllerWithIdentifier:@"SPCF"];
        [self.navigationController pushViewController:spcf animated:YES];
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
