//
//  SPLV02ViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/12/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPLV02ViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
