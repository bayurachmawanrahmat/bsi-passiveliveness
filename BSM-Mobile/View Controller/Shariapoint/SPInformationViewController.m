//
//  SPInformationViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPInformationViewController.h"
#import "Styles.h"
#import "ShariapointConnect.h"

@interface SPInformationViewController ()<ShariapointConnectDelegate>{
    NSUserDefaults *userDefaults;
    NSString *language;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextView *textViewInformation;

@end

@implementation SPInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [Styles setTopConstant:self.topConstant];
    
    self.textViewInformation.text = @"";
    
    ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
    [conn getDataFromEndPoint:@"sharia-point-information" needLoading:YES textLoading:@"" encrypted:NO];
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
            [self.navigationController pushViewController:sp animated:YES];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        NSDictionary *data = [[dict objectForKey:@"response"]objectAtIndex:0];
        
        self.textViewInformation.attributedText = [Utility htmlAtributString:[data objectForKey:@"nilai"]];
        self.textViewInformation.font = [UIFont fontWithName:const_font_name1 size:15];
        self.textViewInformation.editable = NO;
    }
}

@end
