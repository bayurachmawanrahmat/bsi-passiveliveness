//
//  ShariapointViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "ShariapointViewController.h"
#import "Styles.h"
#import "Utility.h"
#import "ShariapointConnect.h"
#import "PopUpLoginViewController.h"
#import "NSUserDefaultsAes.h"

@interface ShariapointViewController ()<ShariapointConnectDelegate, PopupLoginDelegate>{
    UIStoryboard *storyboardUI;
    NSUserDefaults *userDefaults;
    NSString *language;
    BOOL isRequestedAcc;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIView *vwIcons;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePoint;

@end

@implementation ShariapointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    storyboardUI = [UIStoryboard storyboardWithName:@"Shariapoint" bundle:nil];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.lblTitleBar.text = lang(@"BSI_POINT");
    
    [Styles setTopConstant:self.topConstant];
    
    self.lblTitlePoint.text = lang(@"CURRENT_POINT");
    [self.lblPoints setTextColor:UIColorFromRGB(const_color_primary)];
    [self.lblPoints.layer setBorderWidth:2];
    [self.lblPoints.layer setBorderColor:[UIColorFromRGB(const_color_secondary)CGColor]];
    [self.lblPoints.layer setCornerRadius:10];
    [self cekLogin];
    
}

- (IBAction)buttonHistoryAction:(id)sender{
    TemplateViewController *spHistory = [storyboardUI instantiateViewControllerWithIdentifier: @"SPHistory"];
    [self.navigationController pushViewController:spHistory animated:YES];
}
- (IBAction)buttonRedeemAction:(id)sender{
    TemplateViewController *spRedeem = [storyboardUI instantiateViewControllerWithIdentifier: @"SPRedeem"];
    [self.navigationController pushViewController:spRedeem animated:YES];
}
- (IBAction)buttonInformationAction:(id)sender{
    TemplateViewController *spInformation = [storyboardUI instantiateViewControllerWithIdentifier: @"SPInformation"];
    [self.navigationController pushViewController:spInformation animated:YES];
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        if([endPoint isEqualToString:@"total-point"]){
            NSLog(@"%@",dict);
            
            NSDictionary *data = [dict objectForKey:@"response"];
            NSString *strPoint = [data objectForKey:@"point"];
            
            [self.lblPoints setText:[Utility formatCurrencyValue:[strPoint doubleValue] withSeparator:@"."]];
            [dataManager.dataExtra setValue:[data objectForKey:@"cif"] forKey:@"cif"];
        }
    }
}

- (void)getAccount{
    if([userDefaults objectForKey:@"first_account"] == nil || [[userDefaults objectForKey:@"first_account"] isEqualToString:@""]){
        NSArray *listAcct = [userDefaults objectForKey:OBJ_ALL_ACCT];
        if(listAcct.count > 0 && listAcct != nil){
            [userDefaults setValue:[listAcct[0] valueForKey:@"id_account"] forKey:@"first_account"];
            [self getData];
        }else{
            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
        }
    }else{
        [self getData];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]){
        
        [Utility addAccountData:jsonObject];
        
        if(!isRequestedAcc){
            isRequestedAcc = true;
            [self getAccount];
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void) cekLogin{
    NSString *flagCID = [NSUserdefaultsAes getValueForKey:@"customer_id"];
    NSString *hasLogin = [[NSUserDefaults standardUserDefaults] objectForKey:@"hasLogin"];
    
    if ((flagCID == nil) || ([flagCID isEqualToString:@""])) {

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ACTIVATION_CONFIRM") preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        if ([hasLogin isEqualToString:@"NO"] || hasLogin == nil) {
            [self.tabBarController setSelectedIndex:0];
            UIStoryboard *ui = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PopUpLoginViewController *popLogin = [ui instantiateViewControllerWithIdentifier:@"PopupLogin"];
            [popLogin setDelegate:self];
            [popLogin setIdentfier:@"shariapoint"];
            [self presentViewController:popLogin animated:YES completion:nil];
        }
        else {
            [self getAccount];
        }
    }
}

- (void)loginDoneState:(NSString *)identifier{
    [self getAccount];
}

- (void) getData{
    ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
    [dataManager.dataExtra setValue:[userDefaults objectForKey:@"first_account"] forKey:@"norek"];
    NSString *urls = [NSString stringWithFormat:@"total-point?norek=%@",[userDefaults objectForKey:@"first_account"]];
    NSLog(@"%@",urls);
    [conn getDataFromEndPoint:urls needLoading:YES textLoading:@"" encrypted:NO];
}

@end
