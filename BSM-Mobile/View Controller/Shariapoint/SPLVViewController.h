//
//  SPLVViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPLVViewController : TemplateViewController{
    NSString *billerCategoryID;
}


- (void) setBillerCategoryID:(NSString*)billerID;

@end

NS_ASSUME_NONNULL_END
