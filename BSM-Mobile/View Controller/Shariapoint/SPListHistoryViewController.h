//
//  SPListHistoryViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 28/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPListHistoryViewController : TemplateViewController

- (void) setDateStart: (NSString*)dateStart;
- (void) setDateEnd:(NSString*)dateEnd;
@end

NS_ASSUME_NONNULL_END
