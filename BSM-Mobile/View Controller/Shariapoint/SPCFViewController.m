//
//  SPCFViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/09/20.
//  Copyright © 2020 lds. All rights reserved.
//
#import "SPCFViewController.h"
#import "CustomBtn.h"
#import "Styles.h"
#import "ShariapointConnect.h"
#import "SPIBViewController.h"
#import "Utility.h"

@interface SPCFViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UITextView *twConfirmation;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@end

@implementation SPCFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    
    [self.titleBar setText:[dataManager.dataExtra valueForKey:@"redeemName"]];
    self.titleBar.textColor = UIColorFromRGB(const_color_title);
    self.titleBar.textAlignment = const_textalignment_title;
    self.titleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.titleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
    [self.buttonNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    
    [self.buttonCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [self.buttonCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
    
    [self.twConfirmation setEditable:NO];
    
    [dataManager.dataExtra setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"customer_name"] forKey:@"cust_name"];
    
    if(![[dataManager.dataExtra valueForKey:@"isNeedInquiry"]boolValue]){
        [dataManager.dataExtra setValue:@"" forKey:@"timezone"];
        if([Utility isLanguageID]){
            self.twConfirmation.text = [self replacementText:[dataManager.dataExtra valueForKey:@"confirm_text"]];
        }else{
            self.twConfirmation.text = [self replacementText:[dataManager.dataExtra valueForKey:@"confirm_text_en"]];
        }
//        [self actionNext];
    }else{
        [dataManager.dataExtra setValue:@"UTC%2B7" forKey:@"timezone"];
        ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
        [conn sendPostData:@"billCode,billerid,amount_transaction,cust_phone,charge_number,norek,timezone" endPoint:@"inquiry" needLoading:YES textLoading:@"" encrypted:YES];
    }
    

}

- (void) actionNext{
    TemplateViewController *spcf = [self.storyboard instantiateViewControllerWithIdentifier:@"SPGENCF"];
    [self.navigationController pushViewController:spcf animated:YES];
//    SPIBViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"SPIB"];
//    [next setCategory:@"pin"];
//    [self.navigationController pushViewController:next animated:YES];
}

- (void) actionCancel{
    TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
    [self.navigationController pushViewController:sp animated:YES];
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToR];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        NSLog(@"%@",dict);
        
        if([[dict objectForKey:@"response_code"] isEqualToString:@"00"]){
            
            
            [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%@",[dict objectForKey:@"transaction_id"]] forKey:@"inq_transaction_id"];
            [dataManager.dataExtra setValue:[NSString stringWithFormat:@"%@",[dict objectForKey:@"transaction_fee"]] forKey:@"inq_transaction_fee"];
            
            [self addData:[dict objectForKey:@"message"]];
            
            if([Utility isLanguageID]){
                self.twConfirmation.text = [self replacementText:[dict valueForKey:@"response"]];
            }else{
                self.twConfirmation.text = [self replacementText:[dict valueForKey:@"response_en"]];
            }

        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (NSString*) replacementText : (NSString*)str{
    
    NSString *amount = [Utility formatCurrencyValue:[[dataManager.dataExtra valueForKey:@"amount_transaction"]doubleValue]];
    NSString *billerName = [dataManager.dataExtra valueForKey:@"billerName"];
    NSString *billerID = [dataManager.dataExtra valueForKey:@"billerid"];
    
    double billAmountPlusCharge = [[dataManager.dataExtra valueForKey:@"amount_transaction"]doubleValue]+
    [[dataManager.dataExtra valueForKey:@"inq_transaction_fee"]doubleValue];
    
    NSString *result = str;
    result = [result stringByReplacingOccurrencesOfString:@"${billAmount}" withString:amount];
    result = [result stringByReplacingOccurrencesOfString:@"${amount_transaction}" withString:amount];

    result = [result stringByReplacingOccurrencesOfString:@"${billName}" withString:billerName];
    result = [result stringByReplacingOccurrencesOfString:@"${billInfo}" withString:billerName];
    result = [result stringByReplacingOccurrencesOfString:@"${billInfoLabel}" withString:billerName];
    result = [result stringByReplacingOccurrencesOfString:@"${billInfoValue}" withString:billerName];
    
    result = [result stringByReplacingOccurrencesOfString:@"${billCode}" withString:billerID];
    
    result = [result stringByReplacingOccurrencesOfString:@"${billAmountPlusCharge}" withString:[NSString stringWithFormat:@"%.f", billAmountPlusCharge]];
    
    result = [result stringByReplacingOccurrencesOfString:@"${phone_number}" withString:[dataManager.dataExtra valueForKey:@"cust_phone"]];
    
    if([dataManager.dataExtra objectForKey:@"inq_transaction_fee"]!= nil){
        result = [result stringByReplacingOccurrencesOfString:@"${customerChargeAmount}" withString:[dataManager.dataExtra valueForKey:@"inq_transaction_fee"]];
    }else{
        result = [result stringByReplacingOccurrencesOfString:@"${customerChargeAmount}" withString:@""];
    }
    result = [result stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
    return result;
}

- (void)addData:(NSDictionary*)dicta{
    if([dicta objectForKey:@"private_additional_data"]){
        NSDictionary *dict = [dicta objectForKey:@"private_additional_data"];
        [dataManager.dataExtra setValue:dict forKey:@"private_additional_data"];
    }
}

@end
