//
//  SPListHistoryViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 28/04/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "SPListHistoryViewController.h"
#import "ShariapointConnect.h"
#import "SPHistoryCell.h"

@interface SPListHistoryViewController ()<UITableViewDelegate, UITableViewDataSource, ShariapointConnectDelegate>{
    
    NSUserDefaults *userDefaults;
    UIView *imgEmpty;
    NSArray *listData;
    NSString *startDate;
    NSString *endDate;
}
@property (weak, nonatomic) IBOutlet UITableView *tableViewHistori;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation SPListHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Styles setTopConstant:self.topConstant];
    imgEmpty = [[UIView alloc]init];

    [self.tableViewHistori registerNib:[UINib nibWithNibName:@"SPHistoryCell" bundle:nil] forCellReuseIdentifier:@"SPHistoryCell"];

    self.tableViewHistori.delegate = self;
    self.tableViewHistori.dataSource = self;
    self.tableViewHistori.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableViewHistori.tableFooterView = [[UIView alloc]init];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *endpointurl = [NSString stringWithFormat:@"history-point?norek=%@&startdate=%@&enddate=%@",[userDefaults objectForKey:@"first_account"], startDate, endDate];
    NSLog(@"%@", endpointurl);
    //history-point?norek=7000000072&startdate=2021-06-09&enddate=2021-06-09
    ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
    [conn getDataFromEndPoint:endpointurl needLoading:YES textLoading:@"" encrypted:NO];
    
}

- (void)setDateEnd:(NSString *)dateEnd{
    endDate = dateEnd;
}

- (void)setDateStart:(NSString *)dateStart{
    startDate = dateStart;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SPHistoryCell *cell = (SPHistoryCell*) [tableView dequeueReusableCellWithIdentifier:@"SPHistoryCell" forIndexPath:indexPath];
        
    NSArray *date = [[listData[indexPath.row] valueForKey:@"createdate"] componentsSeparatedByString:@" "];
    NSArray *hour = [date[1] componentsSeparatedByString:@"."];
    NSString *labe = [NSString stringWithFormat:@"%@ : %@ \n%@ : %@ \n%@ : %@ \n%@ : %@ \n%@ : %@",
                      lang(@"SPH_DATE"),
                      date[0],
                      lang(@"SPH_HOURS"),
                      hour[0],
                      lang(@"SPH_TRANSACTION_TYPE"),
                      [listData[indexPath.row] objectForKey:@"src_desc"],
                      lang(@"SPH_DESC"),
                      [listData[indexPath.row] objectForKey:@"keterangan"],
                      lang(@"SPH_REFERENCE"),
                      [listData[indexPath.row] objectForKey:@"note1"]
                      ];
    
//    cell.lblTitle.text = @"Tanggal : 11 Sept 2020 \nJam : 17:40:00 \nTipe Transaksi : Bonus Poin \nKeterangan : QRIS BSM Payment \nReferensi : FT5170875";
    cell.lblTitle.text = labe;
    cell.lblTitle.numberOfLines = 0;
    cell.lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
    
//    cell.lblPoints.text = @"+200";
    NSString *points = [listData[indexPath.row] objectForKey:@"point"];
    cell.lblPoints.text = [Utility formatCurrencyValue:[points doubleValue] withSeparator:@"."];
    if([[listData[indexPath.row] objectForKey:@"entry"] isEqualToString:@"C"]){
        cell.lblPoints.text = [NSString stringWithFormat:@"+%@",[Utility formatCurrencyValue:[points doubleValue] withSeparator:@"."]];
        cell.lblPoints.textColor = UIColorFromRGB(const_color_primary);
    }else if([[listData[indexPath.row] objectForKey:@"entry"] isEqualToString:@"D"]){
        cell.lblPoints.text = [NSString stringWithFormat:@"-%@",[Utility formatCurrencyValue:[points doubleValue] withSeparator:@"."]];
        cell.lblPoints.textColor = UIColorFromRGB(const_color_red);
    }else{
        cell.lblPoints.text = [NSString stringWithFormat:@"%@",[Utility formatCurrencyValue:[points doubleValue] withSeparator:@"."]];
        cell.lblPoints.textColor = [UIColor blackColor];
    }
    
    cell.vwBase.layer.borderWidth = 1;
    cell.vwBase.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    cell.vwBase.layer.cornerRadius = 5;
        
    return cell;
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
            [self.navigationController pushViewController:sp animated:YES];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        NSLog(@"%@",dict);
        if([endPoint isEqualToString:@"history-point"]){
            listData = [dict objectForKey:@"response"];
            if(listData.count < 1){
                [self addTextNoData];
            }
            [self.tableViewHistori reloadData];
        }
    }
}

- (void) addTextNoData{
    imgEmpty = [Utility showTextNoData:self.tableViewHistori];
    [self.tableViewHistori addSubview:imgEmpty];
}

- (IBAction)actionBack:(id)sender {
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

@end
