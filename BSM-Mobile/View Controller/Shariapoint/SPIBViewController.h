//
//  SPIBViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 11/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SPIBViewController : TemplateViewController

-(void) setCategory:(NSString* _Nullable)category;

@end



NS_ASSUME_NONNULL_END
