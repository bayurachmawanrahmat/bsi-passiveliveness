//
//  SPRedeemViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/09/20.
//  Copyright © 2020 lds. All rights reserved.
//

#import "SPRedeemViewController.h"
#import "Styles.h"
#import "CellLabel01.h"
#import "ShariapointConnect.h"
#import "SPIBViewController.h"
#import "SPLVViewController.h"
//#import "SPDataManager.h"

@interface SPRedeemViewController ()<UITableViewDelegate, UITableViewDataSource, ShariapointConnectDelegate>{
    
    NSUserDefaults *userDefaults;
    NSString *language;
    
    NSArray *listData;
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleBar;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UITableView *tableViewRedeem;


@end

@implementation SPRedeemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    userDefaults = [NSUserDefaults standardUserDefaults];
    language = [[userDefaults objectForKey:@"AppleLanguages"]objectAtIndex:0];
    
    
    [Styles setTopConstant:self.topConstant];
    
    ShariapointConnect *conn = [[ShariapointConnect alloc]initWithDelegate:self];
    [conn getDataFromEndPoint:@"list-biller-category" needLoading:YES textLoading:@"" encrypted:NO];
    
    [self.lblTitle setTextColor:UIColorFromRGB(const_color_primary)];
    self.lblTitleBar.textColor = UIColorFromRGB(const_color_title);
    self.lblTitleBar.textAlignment = const_textalignment_title;
    self.lblTitleBar.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.lblTitleBar.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    
//    listData = @[@"Beli Voucher HP",@"Beli PLN Prepaid", @"Beli Paket Data", @"TOP UP"];
    
    listData = [[NSArray alloc]init];
    
    [self.tableViewRedeem registerNib:[UINib nibWithNibName:@"CellLabel01" bundle:nil] forCellReuseIdentifier:@"CELLLABEL01"];
    self.tableViewRedeem.hidden = NO;
    self.tableViewRedeem.delegate = self;
    self.tableViewRedeem.dataSource = self;
    self.tableViewRedeem.separatorStyle = UITableViewCellSelectionStyleNone;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellLabel01 *cell = (CellLabel01*)[tableView dequeueReusableCellWithIdentifier:@"CELLLABEL01" forIndexPath:indexPath];
    [cell.labelText setText:[listData[indexPath.row] objectForKey:@"name"]];
    [cell.labelText setTextAlignment:NSTextAlignmentCenter];
    [cell.labelText.layer setBorderWidth:1];
    [cell.labelText.layer setBorderColor:[UIColorFromRGB(const_color_primary)CGColor]];
    [cell.labelText.layer setCornerRadius:10];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listData.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SPLVViewController *spInformation = [[UIStoryboard storyboardWithName:@"Shariapoint" bundle:nil] instantiateViewControllerWithIdentifier: @"SPLV"];
    [spInformation setBillerCategoryID:[listData[indexPath.row] objectForKey:@"billercategoryid"]];
    
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"name"] forKey:@"redeemName"];
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"isneedinquiry"] forKey:@"isNeedInquiry"];
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"confirm_text"] forKey:@"confirm_text"];
    [dataManager.dataExtra setValue:[listData[indexPath.row] objectForKey:@"confirm_text_en"] forKey:@"confirm_text_en"];

    [self.navigationController pushViewController:spInformation animated:YES];
    
}

- (void)didFinishLoadData:(NSDictionary *)dict withEndpoint:(NSString *)endPoint completeWithError:(NSError *)error{
    if(error){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            TemplateViewController *sp = [self.storyboard instantiateViewControllerWithIdentifier:@"Shariapoint"];
            [self.navigationController pushViewController:sp animated:YES];
        }]];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:alert animated:YES completion:nil];
    }else{
        NSLog(@"%@",dict);
                
        NSArray *arrBiller = [dict objectForKey:@"response"];
        listData = arrBiller;
        [self.tableViewRedeem reloadData];
    }
}

@end
