//
//  PK01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 17/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PK01ViewController.h"
#import "PaylaterProductCell.h"
#import "PopupAkadCRViewController.h"

@interface PK01ViewController ()<AkadCRDelegate>{
    NSString *urlInfo;
    NSString *urlImage;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIView *viewProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelTitlebar;
@property (weak, nonatomic) IBOutlet UILabel *productTitle;
@property (weak, nonatomic) IBOutlet UILabel *productSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *productContent;
@property (weak, nonatomic) IBOutlet UILabel *productInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *productAgreementLabel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UIView *vwContent;


@end

@implementation PK01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    self.labelTitlebar.text = [self.jsonData objectForKey:@"title"];
    [self.checkImage setUserInteractionEnabled:YES];
    
    UITapGestureRecognizer *tapInfo = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionInfo)];
    [self.productInfoLabel addGestureRecognizer:tapInfo];
    [self.productInfoLabel setUserInteractionEnabled:YES];
    
    self.buttonNext.enabled = false;
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [self.buttonCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
    
    [self.viewProduct.layer setCornerRadius:10];
    [self.viewProduct.layer setBorderColor:[UIColorFromRGB(const_color_gray)CGColor]];
    [self.viewProduct.layer setBorderWidth:1];
    
    [self.vwContent setHidden:YES];
    [self doRequest];
    

    if([Utility isLanguageID]){
        [self.buttonNext setTitle:@"Minat" forState:UIControlStateNormal];
        [self.buttonCancel setTitle:@"Belum Minat" forState:UIControlStateNormal];
    }else{
        [self.buttonNext setTitle:@"Next" forState:UIControlStateNormal];
        [self.buttonCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
}

- (void) actionCancel{
    [self backToR];
}

- (void) actionNext{
    [self openNextTemplate];
}

- (void) actionInfo{
    PopupAkadCRViewController *popup;
    popup = (PopupAkadCRViewController*)[self routeTemplateController:@"POPAKADCR"];
    [popup setDelegate:self];
    [popup setData:urlInfo];
    [popup setButtonTitle:@"OK"];
    [self presentViewController:popup animated:YES completion:nil];
}

- (void)completionAkadCR:(BOOL)state{
    if(state){
        UITapGestureRecognizer *tapCheck =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(actionCheck)];
        [self.checkImage addGestureRecognizer:tapCheck];
    }
}

- (void) actionCheck{
    if(self.checkImage.image == [UIImage imageNamed:@"check"]){
        self.checkImage.image = [UIImage imageNamed:@"blank_check"];
        self.buttonNext.enabled = false;
    }else{
        self.checkImage.image = [UIImage imageNamed:@"check"];
        self.buttonNext.enabled = true;
    }
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            self.productTitle.text = [dataDict objectForKey:@"title"];
            self.productSubtitle.text = [[dataDict objectForKey:@"sub_title"] stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\n"];
            self.productContent.text = [[dataDict objectForKey:@"content"]stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\n"];
            self.productInfoLabel.text = [dataDict objectForKey:@"label_link"];
            self.productAgreementLabel.text = [dataDict objectForKey:@"agreement"];
//            urlInfo = [dataDict objectForKey:@"url_link"];
            urlInfo = [dataDict valueForKey:@"content_link"];
            urlImage = [dataDict valueForKey:@"url_image"];
            
            [self.productImage setImageWithURL:[NSURL URLWithString:urlImage] placeholderImage:nil];
            [self.productImage setContentMode:UIViewContentModeScaleAspectFit];
            
            [self.vwContent setHidden:NO];
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

@end
