//
//  PP01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 17/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PP01ViewController.h"
#import "PopupAkadCRViewController.h"

@interface PP01ViewController ()<AkadCRDelegate>{
    NSString *urlInfoLink;
    NSString *lblLink;
    NSString *lblAgreement;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitlebar;
@property (weak, nonatomic) IBOutlet UIImageView *imgContent;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;

@property (weak, nonatomic) IBOutlet UIView *viewAgreements;
@property (weak, nonatomic) IBOutlet UILabel *labelAgreement1;
@property (weak, nonatomic) IBOutlet UILabel *labelAgreement2;
@property (weak, nonatomic) IBOutlet UIImageView *imageAgreemnt2;

@property (weak, nonatomic) IBOutlet UILabel *labelDisclaimer;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;
@property (weak, nonatomic) IBOutlet UIView *vwContent;


@end

@implementation PP01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    self.labelTitlebar.text = [self.jsonData valueForKey:@"title"];
    
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
    
    [self.buttonNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.buttonCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    [self.buttonCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    self.buttonNext.enabled = false;
    
    UITapGestureRecognizer *tapInfo = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionInfo)];
    [self.labelAgreement1 addGestureRecognizer:tapInfo];
    [self.labelAgreement1 setUserInteractionEnabled:YES];
    
    self.viewAgreements.layer.cornerRadius = 10;
    self.viewAgreements.layer.borderColor = [UIColorFromRGB(const_color_secondary)CGColor];
    self.viewAgreements.layer.borderWidth = 1;
    
    [self.vwContent setHidden:YES];
    [self doRequest];
}

- (void) actionCancel{
    [self backToR];
}

- (void) actionNext{
    [self openNextTemplate];
}

- (void) actionInfo{
    PopupAkadCRViewController *popup;
    popup = (PopupAkadCRViewController*)[self routeTemplateController:@"POPAKADCR"];
    [popup setDelegate:self];
    [popup setData:urlInfoLink];
    [self presentViewController:popup animated:YES completion:nil];
}

- (void)completionAkadCR:(BOOL)state{
    if(state){
        UITapGestureRecognizer *tapCheck =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(actionCheck)];
        [self.imageAgreemnt2 addGestureRecognizer:tapCheck];
        [self.imageAgreemnt2 setUserInteractionEnabled:YES];
    }
}

- (void) actionCheck{
    if(self.imageAgreemnt2.image == [UIImage imageNamed:@"check"]){
        self.imageAgreemnt2.image = [UIImage imageNamed:@"blank_check"];
        self.buttonNext.enabled = false;
    }else{
        self.imageAgreemnt2.image = [UIImage imageNamed:@"check"];
        self.buttonNext.enabled = true;
    }
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            [self.imgContent setImageWithURL:[NSURL URLWithString:[dataDict valueForKey:@"url_image"]] placeholderImage:nil];
            self.labelContent.text = [[dataDict objectForKey:@"content"]stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\n"];
            
            lblLink = [dataDict valueForKey:@"label_link"];
            lblAgreement = [dataDict valueForKey:@"agreement"];
            urlInfoLink = [dataDict valueForKey:@"content_link"];
            [dataManager.dataExtra setValue:urlInfoLink forKey:@"content_link"];
            [dataManager.dataExtra addEntriesFromDictionary:[[dataDict valueForKey:@"data"] mutableCopy]];
            
            [self labelStyling];
            
            [self.vwContent setHidden:NO];
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}


- (void)labelStyling{
    
    NSMutableAttributedString *attrStringLblLink;
    attrStringLblLink = [[NSMutableAttributedString alloc] initWithString:lblLink];
    [attrStringLblLink addAttribute:NSUnderlineStyleAttributeName value:@1 range:NSMakeRange(0, [attrStringLblLink length])];
    [attrStringLblLink addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Lato-Bold" size:14] range:NSMakeRange(0, [attrStringLblLink length])];
    
    [self.labelAgreement1 setAttributedText:attrStringLblLink];
    [self.labelAgreement1 setTextAlignment:NSTextAlignmentJustified];
    
    [self.labelAgreement2 setAttributedText:
            [[NSAttributedString alloc]
                      initWithData: [lblAgreement dataUsingEncoding:NSUTF8StringEncoding]
                           options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                documentAttributes: nil
                             error: nil]];
    [self.labelAgreement2 setTextAlignment:NSTextAlignmentJustified];
    [self.labelAgreement2 setFont:[UIFont fontWithName:@"Lato-Regular" size:14]];
    [self.labelAgreement2 setTextColor:UIColorFromRGB(const_color_primary)];
    
}

@end
