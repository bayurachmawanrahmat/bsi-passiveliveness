//
//  PK03ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 17/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PK03ViewController.h"

@interface PK03ViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitlebar;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonAction;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIView *vwContent;


@end

@implementation PK03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:self.topConstant];
    self.labelTitlebar.text = [self.jsonData valueForKey:@"title"];
    
    [self.imageView setHidden:YES];
    [self.loader setHidden:NO];

    [self.buttonAction addTarget:self action:@selector(actionButton) forControlEvents:UIControlEventTouchUpInside];
    
    [self.vwContent setHidden:YES];
    [self doRequest];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [NSString stringWithFormat:@"%@,email,nik,mothername,birthdate,id_account",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            self.descriptionLabel.text = [[dataDict valueForKey:@"content"] stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\n"];
            if([dataDict objectForKey:@"url_image"] != nil || ![[dataDict objectForKey:@"url_image"]isEqualToString:@""]){
                [self.imageView setHidden:YES];
                [self.imageView setImageWithURL:[NSURL URLWithString:[dataDict objectForKey:@"url_image"]] placeholderImage:nil];
                [self.loader setHidden:NO];
            }else{
                [self.loader setHidden:NO];
            }
            
            [self.vwContent setHidden:NO];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void) actionButton{
    [self backToR];
}



@end
