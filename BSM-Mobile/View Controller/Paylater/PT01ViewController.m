//
//  PT01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 06/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PT01ViewController.h"
#import "PT01HeadTableViewCell.h"
#import "PL01Cell.h"

@interface PT01ViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *headData;
    NSArray *descData;
    NSArray *listTenor;
    NSString *merchantName;
    NSString *billAmount;
    BOOL checkState;
}

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIView *titleBarView;
@property (weak, nonatomic) IBOutlet UIImageView *iconBarImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
//@property (weak, nonatomic) IBOutlet UILabel *labelTitleLimit;
//@property (weak, nonatomic) IBOutlet UILabel *lblLimitAmount;
//@property (weak, nonatomic) IBOutlet UILabel *labelTitleInvoice;
//@property (weak, nonatomic) IBOutlet UILabel *lblInvoiceAmount;
//@property (weak, nonatomic) IBOutlet UILabel *lblTitleRincian;
//@property (weak, nonatomic) IBOutlet UILabel *lblPenerima;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePeriod;
@property (weak, nonatomic) IBOutlet UITableView *tableViewPeriod;
@property (weak, nonatomic) IBOutlet UITableView *tableHead;
@property (weak, nonatomic) IBOutlet UILabel *lblFeeUjroh;
@property (weak, nonatomic) IBOutlet UIImageView *checkListImage;
@property (weak, nonatomic) IBOutlet UILabel *lblCheck;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headTableHeight;
@property (weak, nonatomic) IBOutlet UIView *vwContent;

@end

@implementation PT01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    checkState = false;
    [Styles setTopConstant:_topConstant];
    self.titleBar.text = [self.jsonData objectForKey:@"title"];
    
    [self.tableViewPeriod registerNib:[UINib nibWithNibName:@"PL01Cell" bundle:nil] forCellReuseIdentifier:@"PL01CELL"];
    [self.tableHead registerNib:[UINib nibWithNibName:@"PT01HeadTableViewCell" bundle:nil] forCellReuseIdentifier:@"PT01HeadTableViewCell"];
    
    self.tableHead.delegate = self;
    self.tableHead.dataSource = self;
    self.tableHead.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableHead.rowHeight = UITableViewAutomaticDimension;
    
    self.tableViewPeriod.delegate = self;
    self.tableViewPeriod.dataSource = self;
    self.tableViewPeriod.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableViewPeriod.rowHeight = UITableViewAutomaticDimension;
    
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    
    [self.btnNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    [self.btnCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.checkListImage addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapCheck)]];
//    [self.lblCheck setTextColor:UIColorFromRGB(const_color_lightgray)];
    
    [self.titleBarView.layer insertSublayer:[Utility gradientForFrame:CGRectMake(0, 0, self.titleBarView.frame.size.width, self.titleBarView.frame.size.height)] atIndex:0];
    
    [self.vwContent setHidden:YES];
    [self doRequest];
}

- (void) actionCancel{
    [self backToR];
}

- (void) actionNext{
    if(checkState){
        [self openNextTemplate];
    }
}

- (void) tapCheck{
    if(!checkState){
        checkState = YES;
        [self.checkListImage setImage:[UIImage imageNamed:@"ic_checkbox_active.png"]];
        [self.btnNext setEnabled:YES];
    }else{
        checkState = NO;
        [self.checkListImage setImage:[UIImage imageNamed:@"ic_checkbox_inactive.png"]];
        [self.btnNext setEnabled:NO];
    }
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@",urlData] needLoading:true encrypted:true banking:true favorite:nil];
    }
}

#pragma mark TableViewDelegate and TableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == self.tableHead){
        PT01HeadTableViewCell *cell = (PT01HeadTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PT01HeadTableViewCell"];
        cell.label.text = [headData[indexPath.row] objectForKey:@"label"];
        cell.value.text = [headData[indexPath.row] objectForKey:@"value"];
        return cell;
    }else{
        PL01Cell *cell = (PL01Cell*)[tableView dequeueReusableCellWithIdentifier:@"PL01CELL"];
        
        cell.label1.text = [listTenor[indexPath.row] objectForKey:@"label"];
        cell.label2.text = [listTenor[indexPath.row] objectForKey:@"date"];
        
        [cell layoutSubviews];
        [cell layoutIfNeeded];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == self.tableViewPeriod){
        [dataManager.dataExtra setValue:billAmount forKey:@"bill_amount"];
        [dataManager.dataExtra setValue:merchantName forKey:@"merchant_name"];
        
        NSString *tenorMonth = [listTenor[indexPath.row] valueForKey:@"tenor_month"];
        [dataManager.dataExtra setValue:tenorMonth forKey:@"tenor_month"];
        [self.checkListImage setUserInteractionEnabled:YES];
        self.lblCheck.textColor = [UIColor blackColor];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == self.tableHead){
        return headData.count;
    }else{
        return listTenor.count;
    }
}

#pragma mark ConnectinoDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[[jsonObject valueForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            headData = [object valueForKey:@"head"];
            descData = [object valueForKey:@"p"];
            listTenor = [object valueForKey:@"tenor"];
            merchantName = [object valueForKey:@"merchant_name"];
            billAmount = [object valueForKey:@"bill_amount"];
            _lblCheck.text = [object valueForKey:@"agreement"];
            
            NSString *description = @"";
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            for(NSDictionary *dict in descData)
            {
                [arr addObject:[dict objectForKey:@"desc"]];
            }
            description = [arr componentsJoinedByString:@"\n"];
            self.lblTitlePeriod.text = description;
            
            [self.tableHead reloadData];
            [self.tableViewPeriod reloadData];
            
            self.headTableHeight.constant = self.tableHead.contentSize.height + 30;
            self.tableHeight.constant = self.tableViewPeriod.contentSize.height + 20;
            
            [self.vwContent setHidden:NO];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

@end
