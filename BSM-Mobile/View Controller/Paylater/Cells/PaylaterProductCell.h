//
//  PaylaterProductCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 17/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomBtn.h"

NS_ASSUME_NONNULL_BEGIN

static NSString * const paylaterproductcellidentifier = @"paylaterproductcellidentifier";

@interface PaylaterProductCell : UITableViewCell{
}

@property (weak, nonatomic) IBOutlet UIImageView *productimage;
@property (weak, nonatomic) IBOutlet UILabel *producttitle;
@property (weak, nonatomic) IBOutlet UILabel *productsubtitle;
@property (weak, nonatomic) IBOutlet UILabel *productdescription;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@end

NS_ASSUME_NONNULL_END
