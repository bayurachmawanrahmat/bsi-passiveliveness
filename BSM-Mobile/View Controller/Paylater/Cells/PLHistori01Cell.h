//
//  PLHistori01Cell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 02/08/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLHistori01Cell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *merchantTitle;
@property (weak, nonatomic) IBOutlet UILabel *purposes;
@property (weak, nonatomic) IBOutlet UILabel *price;

@end

NS_ASSUME_NONNULL_END
