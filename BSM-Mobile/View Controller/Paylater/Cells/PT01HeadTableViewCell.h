//
//  PT01HeadTableViewCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PT01HeadTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *value;

@end

NS_ASSUME_NONNULL_END
