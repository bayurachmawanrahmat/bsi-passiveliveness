//
//  PLIconCollectionViewCell.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 07/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLIconCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIView *rectView;

@end

NS_ASSUME_NONNULL_END
