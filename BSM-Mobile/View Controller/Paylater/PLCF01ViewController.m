//
//  PLCF01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 08/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PLCF01ViewController.h"

@interface PLCF01ViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIView *titleBarView;
@property (weak, nonatomic) IBOutlet UILabel *titleBarLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentTextview;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet CustomBtn *nextButton;
@property (weak, nonatomic) IBOutlet UIImageView *iconBarImage;
@property (weak, nonatomic) IBOutlet UIView *vwContent;

@end

@implementation PLCF01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    self.titleBarView.backgroundColor = UIColorFromRGB(const_color_basecolor);
    self.titleBarLabel.text = [self.jsonData valueForKey:@"title"];
    
    self.iconBarImage.layer.cornerRadius = 4;
    self.iconBarImage.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    
    self.contentTextview.editable = NO;
    self.contentTextview.selectable = NO;
    
    [self.cancelButton setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    [self.cancelButton addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    
    [self.nextButton setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.nextButton setColorSet:PRIMARYCOLORSET];
    [self.nextButton addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [self.titleBarView.layer insertSublayer:[Utility gradientForFrame:CGRectMake(0, 0, self.titleBarView.frame.size.width, self.titleBarView.frame.size.height)] atIndex:0];
    
    [self.vwContent setHidden:YES];
    [self doRequest];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,transaction_id",urlData] needLoading:true encrypted:true banking:true favorite:nil];
    }
}

#pragma mark action button
- (void) actionNext{
    [self openNextTemplate];
}

- (void) actionCancel{
    [self backToR];
}

#pragma mark connection delegation
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
        NSString *text = @"";
        text = [[jsonObject valueForKey:@"response"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"<br>"];
//        self.contentTextview.text = text;
        [self setContent:text];
        [dataManager.dataExtra setValue:[jsonObject valueForKey:@"transaction_id_confirm"] forKey:@"transaction_id_confirm"];
        [dataManager.dataExtra setValue:[jsonObject valueForKey:@"no_ref"] forKey:@"no_ref"];
        
        [self.vwContent setHidden:NO];
        
    }else{
        NSString * msg = [Utility messageGenerated:[jsonObject valueForKey:@"response_code"] defValu:[jsonObject valueForKey:@"response"]];
        UIAlertController *aler = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,msg] preferredStyle:UIAlertControllerStyleAlert];
        [aler addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToRoot];
        }]];
        if (@available(iOS 13.0, *)) {
            [aler setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [self presentViewController:aler animated:YES completion:nil];
    }
    
}
    
- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (void) setContent:(NSString*)info{
    info = [NSString stringWithFormat:@"<body style='font-family:Lato-Regular;font-size: 15px;'>%@</body>",info];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
              initWithData: [info dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName:[UIFont fontWithName:const_font_name1 size:16] }
        documentAttributes: nil
                     error: nil
    ];

    self.contentTextview.attributedText = attributedString;
}
@end
