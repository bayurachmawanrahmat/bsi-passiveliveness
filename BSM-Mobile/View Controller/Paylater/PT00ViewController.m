//
//  PT00ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 05/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PT00ViewController.h"
#import "PLIconCollectionViewCell.h"
#import "PLHistori01Cell.h"

@interface PT00ViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource>{
    NSArray *content;
    NSMutableArray *contentIcon;
    NSString *limitPaylater;
    NSArray *historiList;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitlebar;
@property (weak, nonatomic) IBOutlet UILabel *labelHistoriTrans;
@property (weak, nonatomic) IBOutlet UILabel *labeltitleSaldo;
@property (weak, nonatomic) IBOutlet UILabel *labelSaldo;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (weak, nonatomic) IBOutlet UIView *viewBanner;
@property (weak, nonatomic) IBOutlet UIImageView *imageBanner;
@property (weak, nonatomic) IBOutlet UIImageView *bgBanner;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *vwContentScroll;

@end

@implementation PT00ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Styles setTopConstant:_topConstant];
    [self.labelTitlebar setText:[self.jsonData objectForKey:@"title"]];
    self.labeltitleSaldo.text = @"";
    self.labelSaldo.text = @"";
    self.labelHistoriTrans.text = lang(@"HISTORY_TRANSACTION");
    
    content = [self.jsonData objectForKey:@"content"];
    contentIcon = [[NSMutableArray alloc]init];
    
    //ecommerce
    [contentIcon addObject:@{@"code":@"00185",
                             @"label_name":@"Ecommerce",
                             @"icon_name":@"ic_pl_menu_ecommerce"}];
    //beli
    [contentIcon addObject:@{@"code":@"",
                             @"label_name":lang(@"LABEL_BUY"),
                             @"icon_name":@"ic_sdmenu_pembelian_v2"}];
    //tarik tunai
    [contentIcon addObject:@{@"code":@"",
                             @"label_name":lang(@"LABEL_CASH_WITDRAWAL"),
                             @"icon_name":@"ic_mmenu_paylater_tariktunai"}];
    //qris
    [contentIcon addObject:@{@"code":@"",
                             @"label_name":lang(@"LABEL_QRIS"),
                             @"icon_name":@"ic_transferlist_qris"}];
    //informasi portofolio
    [contentIcon addObject:@{@"code":@"",
                             @"label_name":lang(@"LABEL_PORTOFOLIO_INFORMATION"),
                             @"icon_name":@"ic_pl_menu_infoportofolio"}];
    
    //jadwal angsuran
    [contentIcon addObject:@{@"code":@"",
                             @"label_name":lang(@"LABEL_SCHEDULE_INSTALLMENT"),
                             @"icon_name":@"ic_pl_menu_jadwalangsuran"}];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"PLIconCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PLIconCollectionViewCell"];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.tableview registerNib:[UINib nibWithNibName:@"PLHistori01Cell" bundle:nil] forCellReuseIdentifier:@"PLHistori01Cell"];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
//    [layout setSectionInset:UIEdgeInsetsMake(10, 5, 10, 5)];
//    [layout setItemSize:CGSizeMake(85, 85)];
//    [layout setMinimumInteritemSpacing:5];
//    [layout setMinimumLineSpacing:10];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    self.collectionView.collectionViewLayout = layout;

    [self.bgBanner setHidden:YES];

    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setPath:[[self createShapeUp] CGPath]];
    shapeLayer.strokeColor = [[UIColor grayColor]CGColor];
    shapeLayer.backgroundColor = [[UIColor clearColor]CGColor];
    CAGradientLayer *layerGradient = [Utility gradientForFrame:CGRectMake(0, 0, self.bgBanner.frame.size.width, self.bgBanner.frame.size.height)];
    layerGradient.frame = CGRectMake(0, 0, self.bgBanner.frame.size.width, self.bgBanner.frame.size.height);
    [layerGradient setMask:shapeLayer];
    
    [self.viewBanner.layer insertSublayer:layerGradient atIndex:0];
    [self.viewBanner setBackgroundColor:[UIColor whiteColor]];
    
    [self.vwContentScroll setHidden:YES];
    [self doRequest];
}

- (UIBezierPath *)createShapeUp
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    CGFloat height = self.viewBanner.frame.size.height*0.8;
    CGFloat width = self.viewBanner.frame.size.width;
    CGFloat centerHeight = height * 0.85;
    
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(width, 0)];
    [path addLineToPoint:CGPointMake(width, centerHeight)];
    [path addCurveToPoint:CGPointMake(0, height)
      controlPoint1:CGPointMake(width*0.5,centerHeight)
      controlPoint2:CGPointMake(width*0.25,height + height*0.2)];
    [path closePath];
    [path stroke];
    
    return path;
}

- (void)viewDidAppear:(BOOL)animated{
    CGFloat height = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    _collectionViewHeight.constant = height;
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@",urlData] needLoading:true encrypted:true banking:true favorite:nil];
    }
//    [self setupDummy];
}

- (void) setupDummy{
    NSString *mResponse = @"{\"label\": \"Sisa Limit BSI Paylater\",\"value\": \"Rp3.500.000,-\",\"limit_paylater\": \"3500000\",\"history\": [{\"date\": \"17 Juli 2021\",\"data\": [{\"transaction\": \"SHOPEE\",\"category\": \"pembayaran\",\"amount\": \"Rp 50.000\"}]},{\"date\": \"25 Juni 2021\",\"data\": [{\"transaction\": \"TOKOPEDIA\",\"category\": \"pembayaran\",\"amount\": \"Rp 150.000\"}, {\"transaction\": \"QRIS\",\"category\": \"pembayaran\",\"amount\": \"Rp 300.000\"}]}]}";
    NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
    
    self.labeltitleSaldo.text = [object objectForKey:@"label"];
    self.labelSaldo.text = [object objectForKey:@"value"];
    limitPaylater = [object objectForKey:@"limit_paylater"];
    historiList = [object objectForKey:@"history"];
    [self.tableview reloadData];
}

#pragma mark ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            self.labeltitleSaldo.text = [object objectForKey:@"label"];
            self.labelSaldo.text = [object objectForKey:@"value"];
            limitPaylater = [object objectForKey:@"limit_paylater"];
            historiList = [object objectForKey:@"history"];
            [self.tableview reloadData];
            [dataManager.dataExtra setValue:limitPaylater forKey:@"limit_paylater"];
            [dataManager.dataExtra setValue:[jsonObject valueForKey:@"transaction_id_limit"] forKey:@"transaction_id_limit"];
            
            [self.vwContentScroll setHidden:NO];
        }else if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"01"]){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

#pragma mark CollectionViewDelegate and CollectionViewDataSource
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PLIconCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PLIconCollectionViewCell" forIndexPath:indexPath];
    
    /*based on content menu
     **/
//    cell.title.text = [content[indexPath.row] objectForKey:@"name"];
//    cell.image.image = [self getImageBasedOnCode:[content[indexPath.row] objectForKey:@"code"]];
    
    /*with harcoded list
     **/
    cell.title.text = [contentIcon[indexPath.row] objectForKey:@"label_name"];
    cell.image.image = [UIImage imageNamed:[contentIcon[indexPath.row] objectForKey:@"icon_name"]];
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
//    return (int)ceil((double)content.count/3);
//    return (int)ceil((double)contentIcon.count/3);
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if([[contentIcon[indexPath.row] valueForKey:@"code"]isEqualToString:@""]){
        UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"MENU_EMPTY") preferredStyle:UIAlertControllerStyleAlert];
        [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alerts animated:YES completion:nil];
    }else{
        NSDictionary *data = [content objectAtIndex:indexPath.row];
        if([data isKindOfClass:[NSString class]]){
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@\n%@",ERROR_JSON_INVALID,@"Invalid JSON Structure",data] preferredStyle:UIAlertControllerStyleAlert];
            if (@available(iOS 13.0, *)) {
                [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alerts animated:YES completion:nil];
        }else{
            @try {
                [dataManager setAction:[data objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
                [dataManager.dataExtra setValue:[data objectForKey:@"code"] forKey:@"code"];
                dataManager.currentPosition=-1;
                [self openNextTemplate];
            } @catch (NSException *exception) {
                UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
                if (@available(iOS 13.0, *)) {
                    [alerts setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
                }
                [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alerts animated:YES completion:nil];
            }

        }
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"%f",(collectionView.frame.size.width-21)/3);
    return CGSizeMake((collectionView.frame.size.width-21)/3, 85);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    NSLog(@"%lu",(section+1) == (int)ceil((double)content.count/3) && content.count%3 != 0 ? content.count%3 : 3);
//    return (section+1) == (int)ceil((double)content.count/3) && content.count%3 != 0 ? content.count%3 : 3;
//    return (section+1) == (int)ceil((double)contentIcon.count/3) && contentIcon.count%3 != 0 ? contentIcon.count%3 : 3;
    return contentIcon.count;
}

#pragma mark TableViewDelegate and TableViewDataSource

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView*)view;
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, 24);
        gradient.colors = [NSArray arrayWithObjects:(id)[UIColorFromRGB(const_color_primary) CGColor],(id)[UIColorFromRGB(const_color_lightgray) CGColor], nil];
        gradient.locations = @[@0.0, @0.75];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [header.contentView.layer insertSublayer:gradient atIndex:0];
        header.contentView.backgroundColor = [UIColor clearColor];

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 24;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return historiList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionList = [historiList[section]objectForKey:@"data"];
    return sectionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PLHistori01Cell *cell = (PLHistori01Cell*)[tableView dequeueReusableCellWithIdentifier:@"PLHistori01Cell"];
    
    NSArray *sectionList = [historiList[indexPath.section] objectForKey:@"data"];
    cell.merchantTitle.text = [sectionList[indexPath.row]objectForKey:@"transaction"];
    cell.purposes.text = [sectionList[indexPath.row]objectForKey:@"category"];
    cell.price.text = [sectionList[indexPath.row]objectForKey:@"amount"];
    cell.price.textColor = UIColorFromRGB(const_color_secondary);
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [historiList[section] valueForKey:@"date"];
}

- (UIImage*) getImageBasedOnCode:(NSString*)code{
    UIImage *image = [UIImage imageNamed:@"ic_transferlist_bsi"];
    if([code isEqualToString:@"00185"]){
        image = [UIImage imageNamed:@"ic_mmenu_pembayaran_mx_v3"];
    }
    return image;
}

@end
