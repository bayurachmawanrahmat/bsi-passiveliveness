//
//  PK02ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 21/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PK02ViewController.h"

@interface PK02ViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>{
    UIToolbar *toolBar;
    NSArray* listAccount;
    UIPickerView *picker;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *titleBarLabel;

@property (weak, nonatomic) IBOutlet UILabel *nikLabel;
@property (weak, nonatomic) IBOutlet UITextField *nikTextField;
@property (weak, nonatomic) IBOutlet UILabel *dobLabel;
@property (weak, nonatomic) IBOutlet UITextField *dobTextField;
@property (weak, nonatomic) IBOutlet UILabel *motherNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *motherNameTextField;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UILabel *desclaimerLabel;
@property (weak, nonatomic) IBOutlet UILabel *norekLabel;
@property (weak, nonatomic) IBOutlet UITextField *norekTextField;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet CustomBtn *requestButton;
@property (weak, nonatomic) IBOutlet UIScrollView *viewScroll;
@property (weak, nonatomic) IBOutlet UIView *vwContent;

@end

@implementation PK02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    self.titleBarLabel.text = [self.jsonData objectForKey:@"title"];
    
    self.dobLabel.text = lang(@"DOB_LABEL");
    self.motherNameLabel.text = lang(@"MOTHERSNAME_LABEL");
    self.desclaimerLabel.text = lang(@"DATA_DISCLAIMER_LABEL");
    
    
    self.norekLabel.text = lang(@"TITLE_FIELD_NOREK");
    self.norekTextField.placeholder = lang(@"PLACEHOLDER_NOREK");
    
    self.emailTextField.placeholder = lang(@"PLACEHOLDER_EMAIL");
    
    self.dobTextField.enabled = false;
    self.nikTextField.enabled = false;
    self.motherNameTextField.enabled = false;
    self.dobTextField.backgroundColor = UIColorFromRGB(const_color_lightgray);
    self.nikTextField.backgroundColor = UIColorFromRGB(const_color_lightgray);
    self.motherNameTextField.backgroundColor = UIColorFromRGB(const_color_lightgray);
    
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    
    self.emailTextField.inputAccessoryView = toolBar;
    
    [self.requestButton setColorSet:PRIMARYCOLORSET];
    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    
    if([Utility isLanguageID]){
        [self.requestButton setTitle:@"Ajukan Paylater" forState:UIControlStateNormal];
        [self.cancelButton setTitle:@"Batal" forState:UIControlStateNormal];
    }else{
        [self.requestButton setTitle:@"Apply" forState:UIControlStateNormal];
        [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    }
    
    [self.cancelButton addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.requestButton addTarget:self action:@selector(actionRequest) forControlEvents:UIControlEventTouchUpInside];
    
    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, SCREEN_WIDTH, 150)];
    picker.dataSource = self;
    picker.delegate = self;
    picker.showsSelectionIndicator = true;
    self.norekTextField.inputView = picker;
    self.norekTextField.inputAccessoryView = toolBar;
    
    [self registerForKeyboardNotifications];
    [self.vwContent setHidden:YES];
    [self doRequest];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.norekTextField.text = listAccount[row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return listAccount[row];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return listAccount.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        
        [dataManager.dataExtra setValue:[NSUserdefaultsAes getValueForKey:@"usercif"] forKey:@"cif"];
        
        NSString *urlData = [NSString stringWithFormat:@"%@,cif,list_acno,transaction_id",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) actionCancel{
    [self backToR];
}

- (void) actionRequest{
    if([self.emailTextField.text isEqualToString:@""]){
        [Utility showMessage:lang(@"EMAIL_NOT_EMPTY") instance:self];
    }else if([self.norekTextField.text isEqualToString:@""]){
        [Utility showMessage:self.norekTextField.placeholder instance:self];
    }else{
        if(![Utility validateEmailWithString:self.emailTextField.text]){
            [Utility showMessage:lang(@"INVALID_EMAIL") instance:self];
        }else{
            [dataManager.dataExtra setValue:self.emailTextField.text forKey:@"email"];
            [dataManager.dataExtra setValue:self.norekTextField.text forKey:@"id_account"];
            [self openNextTemplate];
        }
        
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            self.nikTextField.text = [dataDict valueForKey:@"nik"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"nik"] forKey:@"nik"];
            
            self.dobTextField.text = [dataDict valueForKey:@"birthdate"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"birthdate"] forKey:@"birthdate"];
            
            self.motherNameTextField.text = [dataDict valueForKey:@"mothername"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"mothername"] forKey:@"mothername"];
            
            self.emailTextField.text = [dataDict valueForKey:@"email"];
            [dataManager.dataExtra setValue:[dataDict valueForKey:@"email"] forKey:@"email"];
            
            listAccount = [[dataDict objectForKey:@"listacno"] componentsSeparatedByString:@","];
            [picker reloadAllComponents];
            
            [self.vwContent setHidden:NO];

        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
@end
