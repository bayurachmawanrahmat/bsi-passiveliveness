//
//  PK00ViewController.h
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 15/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PK00ViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
