//
//  PK00ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 15/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PK00ViewController.h"
#import "UIImageView+AFNetworking.h"

@interface PK00ViewController ()
@property (weak, nonatomic) IBOutlet CustomBtn *buttonOK;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleBarLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *vwContent;


@end

@implementation PK00ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    self.titleBarLabel.text = [self.jsonData valueForKey:@"title"];
    self.scrollView.hidden = YES;
    
    [self.buttonOK setColorSet:PRIMARYCOLORSET];
    [self.buttonOK addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.imageView setImage:[UIImage imageNamed:@""]];
    
    [self.vwContent setHidden:YES];
    [self doRequest];
}

- (void)actionBack{
    [self backToR];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,app_no",urlData] needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            NSString *menuidResponse = [object valueForKey:@"menuid"];
            [NSUserdefaultsAes setObject:[object valueForKey:@"cif"] forKey:@"usercif"];
            [dataManager.dataExtra setValue:[object valueForKey:@"list_acno"] forKey:@"list_acno"];
            
            [self openTemplateByMenuID:menuidResponse];
            
        }else if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"01"]){
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            self.messageLabel.text = [object objectForKey:@"content"];
            [NSUserdefaultsAes setObject:[object valueForKey:@"cif"] forKey:@"usercif"];
            [dataManager.dataExtra setValue:[object valueForKey:@"list_acno"] forKey:@"list_acno"];

            [self.imageView setImageWithURL:[NSURL URLWithString:[object objectForKey:@"url_image"]] placeholderImage:nil];
            self.scrollView.hidden = NO;
            self.vwContent.hidden = NO;
            
        }else{
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            NSString *msg = @"";
            if([object valueForKey:@"content"] != nil || [[object valueForKey:@"content"] isEqualToString:@""]){
                msg = [object valueForKey:@"content"];
            }else{
                msg = mResponse;
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

@end
