//
//  KP01ViewController.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 24/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "KP01ViewController.h"
#import "NSString+HTML.h"

@interface KP01ViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *titleBarLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *footerLabe;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (weak, nonatomic) IBOutlet UIView *vwContent;

@end

@implementation KP01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    self.titleBarLabel.text = [self.jsonData valueForKey:@"title"];
    
    [self.buttonNext setColorSet:PRIMARYCOLORSET];
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
    
    [self.buttonNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.buttonCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonCancel addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.vwContent setHidden:YES];
    [self doRequest];
}

- (void) actionNext{
    [self openNextTemplate];
}

- (void) actionBack{
    [self backToR];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"%@,app_no",urlData] needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"paylater_kafalah"]){
        if([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            
            NSString *mResponse = [jsonObject objectForKey:@"response"];
            NSDictionary *object = [NSJSONSerialization JSONObjectWithData:[mResponse dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            [self.imageView setImageWithURL:[NSURL URLWithString:[object valueForKey:@"url_image"]]];
            if([[object valueForKey:@"url_image"] isEqualToString:@""]){
                self.imageWidth.constant = 0;
                self.imageHeight.constant = 0;
            }
            self.titleLabel.text = [[object valueForKey:@"title"]stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\n"];
            self.contentLabel.text = [[object valueForKey:@"content"]stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\n"];
            
//            NSString *strEscapeChar = [[object valueForKey:@"content"]stringByDecodingHTMLEntities];
//            NSString *strDescIsoEncode = [NSString stringWithCString:[strEscapeChar cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//            strDescIsoEncode = [[strDescIsoEncode stringByReplacingOccurrencesOfString:@"[CR]" withString:@"<br>"] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
//
//            [self setContent:strDescIsoEncode];
            
            self.footerLabe.text = [[object valueForKey:@"footer"]stringByReplacingOccurrencesOfString:@"[CR]" withString:@"\n"];
            
            [self.vwContent setHidden:NO];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[jsonObject objectForKey:@"response"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

//- (void) setContent:(NSString*)info{
//    info = [NSString stringWithFormat:@"<body style='font-family:Lato-Regular;font-size: 15px;'>%@</body>",info];
//    NSAttributedString *attributedString = [[NSAttributedString alloc]
//              initWithData: [info dataUsingEncoding:NSUnicodeStringEncoding]
//                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
//                                                        NSFontAttributeName:[UIFont fontWithName:const_font_name1 size:16] }
//        documentAttributes: nil
//                     error: nil
//    ];
//
//    self.contentLabel.attributedText = attributedString;
//}


@end
