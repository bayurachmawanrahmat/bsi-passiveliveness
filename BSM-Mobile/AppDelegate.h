//
//  AppDelegate.h
//  BSM Mobile
//
//  Created by lds on 4/29/14.
//  Copyright (c) 2014 Inmagine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Connection.h"
#import "Reachability.h"
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, ConnectionDelegate, UIAlertViewDelegate, FIRMessagingDelegate, UNUserNotificationCenterDelegate>{
    NSArray *dataArray;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSString *this_version;
@property (nonatomic, retain) NSString * menu_file;
//@property (strong, nonatomic) UIImageView *splashImageView;
@property (strong, nonatomic) UIView *splashImageView;
//@property (strong, nonatomic) Reachability *reach;
//@property BOOL wasConnected;
@end
