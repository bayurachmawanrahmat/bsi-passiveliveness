//
//  QRISCPM02ViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 02/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QRISCPM02ViewController : TemplateViewController

- (void)setDataLocal:(NSDictionary*)object;

@end

NS_ASSUME_NONNULL_END
