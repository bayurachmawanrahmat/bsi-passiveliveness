//
//  QRISCPM02ViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 02/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "QRISCPM02ViewController.h"
#import "QRPopupStatusViewController.h"
#import "GENCF02ViewController.h"
#import "InboxHelper.h"
#import "AESCipher.h"
#import "Utility.h"

@interface QRISCPM02ViewController ()<ConnectionDelegate, QRPopupStatusViewDelegate>{
    NSUserDefaults *userDefault;
    NSTimer *timerCountdown;
    NSTimer *timerCallerApi;
    int currMinute;
    int currSeconds;
    NSMutableDictionary *dataResponseSend;
    NSString *transactionId;
    AESCipher *aesChiper;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UIView *bgHoldingAcc;
@property (weak, nonatomic) IBOutlet UILabel *lblHoldingAccAltname;
@property (weak, nonatomic) IBOutlet UILabel *lblHoldingAcc;
@property (weak, nonatomic) IBOutlet UILabel *lblQrInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgQR;
@property (weak, nonatomic) IBOutlet UIView *imgQRBorder;
@property (weak, nonatomic) IBOutlet UIView *imgQRMask;
@property (weak, nonatomic) IBOutlet UILabel *lblExpired;
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblQrSupported;
@property (weak, nonatomic) IBOutlet UIButton *btnQrRegenerate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightQRContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightQRRegenerate;

@end

@implementation QRISCPM02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    aesChiper = [[AESCipher alloc] init];
    userDefault = [NSUserDefaults standardUserDefaults];
    dataResponseSend = [[NSMutableDictionary alloc]init];
    
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [_labelTitle setText:[[dataManager getObjectData] valueForKey:@"title"]];
    _lblHoldingAccAltname.text = [dataManager.dataExtra valueForKey:@"holding_accname"];
    _lblHoldingAcc.text = [dataManager.dataExtra valueForKey:@"holding_acclabel"];
    _lblQrInfo.text = lang(@"QR_DUMMY_INFO2");
    _lblExpired.text = lang(@"QR_EXPIRED");
    _lblQrSupported.text = lang(@"QR_SUPPORTED");
    _lblTimer.text = @"03:00";
    _heightQRContent.constant = self.view.frame.size.width-60;
    [_btnQrRegenerate setTitle:[lang(@"QR_REGENERATE") uppercaseString] forState:UIControlStateNormal];
    
    // border & corner radius
    [_bgHoldingAcc.layer setCornerRadius:10.0f];
    [_bgHoldingAcc.layer setBorderWidth:1.0f];
    [_bgHoldingAcc.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    [_imgQRBorder.layer setBorderWidth:1.0f];
    [_imgQRBorder.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    
    // Setup gesture
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
    [self.btnQrRegenerate addTarget:self action:@selector(actionBtnQrGenerate) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)viewDidDisappear:(BOOL)animated{
    [timerCountdown invalidate];
    [timerCallerApi invalidate];
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)navigationBack{
    [self showPopupQRDialog:@"qrnavback"
                     tTitle:lang(@"QR_RG_TITLE")
                    iStatus:@"ic_circle_wavy_question"
                      tDesc:lang(@"QR_RG_DESC")
                    aButton:UILayoutConstraintAxisHorizontal
                 txtButtonP:@"OK"
              hiddenButtonN:NO];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData =[NSString stringWithFormat:@"%@,date_qris",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)doRegenerateQrcode{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=qris_cpm_qrcode,menu_id,device,device_type,ip_address,language,date_local,date_qris,id_account,code,pin,customer_id,email"] needLoading:true encrypted:true banking:true favorite:nil];
}

- (void)doCallIntervalPaymentApi {
    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":transactionId}];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=qris_cpm_payment,menu_id,device,device_type,ip_address,language,date_local,date_qris,id_account,code,pin,customer_id,email,transaction_id"] needLoading:false encrypted:true banking:true favorite:nil];
}

- (void)doCallUpdateExpiredQrCpmApi {
    [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":transactionId}];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=qris_cpm_expiredqr,menu_id,device,device_type,ip_address,language,date_local,date_qris,id_account,customer_id,transaction_id"] needLoading:false encrypted:true banking:true favorite:nil];
}

- (void)initTimer {
    currMinute=03;
    currSeconds=00;
    timerCountdown=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    [self timerCallApiStart];
}

- (void)timerCallApiStart {
    // setup call api every 5 secs
    timerCallerApi=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(doCallIntervalPaymentApi) userInfo:nil repeats:YES];
}

- (void)timerFired {
    if((currMinute>0 || currSeconds>=0) && currMinute>=0){
        if(currSeconds==0){
            currMinute-=1;
            currSeconds=59;
        } else if(currSeconds>0){
            currSeconds-=1;
        }
        if(currMinute>-1)
            [_lblTimer setText:[NSString stringWithFormat:@"%d%@%02d",currMinute,@":",currSeconds]];
    } else {
        [timerCountdown invalidate];
        [timerCallerApi invalidate];
        [self.imgQRMask setHidden:NO];
//        [self.heightQRRegenerate setConstant:80];
//        [self.btnQrRegenerate setHidden:NO];
        [self showPopupQRDialog:@"qrtimeout"
                         tTitle:lang(@"INFO")
                        iStatus:@"ic_circle_wavy_exclamation"
                          tDesc:lang(@"QR_DESC_TIMEOUT")
                        aButton:UILayoutConstraintAxisVertical
                     txtButtonP:lang(@"QR_RETRX")
                  hiddenButtonN:NO];
    }
}

- (void)actionBtnQrGenerate{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:lang(@"QR_RG_TITLE") message:lang(@"QR_RG_DESC") preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    
    //Add Buttons
    UIAlertAction* cancelButton = [UIAlertAction actionWithTitle:lang(@"CANCEL") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    }];
    [cancelButton setValue:[UIColor darkGrayColor] forKey:@"titleTextColor"];
    
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //call service regenerate
        [self.imgQRMask setHidden:NO];
        [self doRegenerateQrcode];
    }];

    //Add your buttons to alert controller
    [alert addAction:cancelButton];
    [alert addAction:okButton];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showPopupQRDialog:(NSString *)identifier tTitle:(NSString *)tTitle iStatus:(NSString*)iStatus tDesc:(NSString*)tDesc aButton:(UILayoutConstraintAxis)aButton txtButtonP:(NSString*)txtButtonP hiddenButtonN:(BOOL)hiddenButtonN{
    if([identifier isEqualToString:@"qrsucces"] || [identifier isEqualToString:@"qrtimeout"] || [identifier isEqualToString:@"qrfailed"]){
        _lblTimer.text = @"00:00";
        [timerCountdown invalidate];
        [timerCallerApi invalidate];
    }
    QRPopupStatusViewController *dialogStatus = [self routeTemplateController:@"QRSTATUS"];
    [dialogStatus setDelegate:self];
    [dialogStatus setIdentfier:identifier];
    [dialogStatus setTitle:tTitle];
    [dialogStatus setDesc:tDesc];
    [dialogStatus setImage:iStatus];
    [dialogStatus setButtonAxis:aButton];
    [dialogStatus setButtonPositive:NO withText:txtButtonP];
    [dialogStatus setButtonNegative:hiddenButtonN withText:lang(@"CANCEL")];
    
    [dialogStatus setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogStatus setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogStatus animated:YES completion:nil];
}

- (void)showErrorAlert:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert show];
}

- (void)generateQRImage:(NSString *)encStr {
    NSData *stringData = [encStr dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = _imgQR.frame.size.width / qrImage.extent.size.width;
    float scaleY = _imgQR.frame.size.height / qrImage.extent.size.height;
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    _imgQR.image = [UIImage imageWithCIImage:qrImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"qris_cpm_qrcode"]){
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            NSString *response = [jsonObject objectForKey:@"response"];
            NSDictionary *getData = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            transactionId = [jsonObject objectForKey:@"transaction_id"];
            [self generateQRImage:[getData objectForKey:@"qr_code"]];
            [self.imgQRMask setHidden:YES];
            [self.heightQRRegenerate setConstant:0];
            [self.btnQrRegenerate setHidden:YES];
            // start count downTim
            [self initTimer];
        } else {
            NSString *errMsg = [jsonObject objectForKey:@"response"];
            [self showErrorAlert:errMsg];
        }
    }
    
    if([requestType isEqualToString:@"qris_cpm_payment"]){
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            NSString *response = [jsonObject objectForKey:@"response"];
            NSMutableDictionary *dictResp = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            // send data to next view
            dataResponseSend = [dictResp mutableCopy];
            [dataResponseSend setValue:transactionId forKey:@"transaction_id"];
            [self showPopupQRDialog:@"qrsucces"
                             tTitle:lang(@"QR_TITLE_SUCCESS")
                            iStatus:@"ic_circle_wavy_check"
                              tDesc:lang(@"QR_DESC_SUCCESS")
                            aButton:UILayoutConstraintAxisVertical
                         txtButtonP:@"OK"
                      hiddenButtonN:YES];
        } else if (![[jsonObject objectForKey:@"response_code"] isEqualToString:@"100"]) {
            NSString *respMsg = [jsonObject objectForKey:@"response"];
            [self showPopupQRDialog:@"qrfailed"
                             tTitle:lang(@"INFO")
                            iStatus:@"ic_circle_wavy_exclamation"
                              tDesc:respMsg
                            aButton:UILayoutConstraintAxisVertical
                         txtButtonP:lang(@"QR_RETRX")
                      hiddenButtonN:NO];
        }
//        else {
//            //debug
//            // send data to next view
//            NSString *responsedebug = @"{\"trxref\":\"FT22007X7HS1\",\"footer_msg\":\"Thank you for using BSI Mobile.\\nHopefully our services bring blessings to you.\",\"title\":\"QRIS CPM Payment\",\"msg\":\"Status: SUCCESS\\n\\nTransaction No.: FT22007X7HS1\\nReference: 000000000156\\nTransaction Date: 2021-12-24 11:22:19\\nReceipt No.: 00000001220000000122\\nTerminal ID: A7031983        \\n\\nAcquirer Name: Bank Commonwealth\\nMerchant Name: MERCHANT BAKSO MAS DEDE  Merchant Location: SEMARANG     \\n\\nCustomer PAN: 9360045170636816222\\nSource of Funds: XXXXXX1622\\n\\nAmount: Rp 90,000.00\\nTips: Rp 0\\nDebit Total: Rp 90,000.00\"}";
//            NSDictionary *dictResp = [NSJSONSerialization JSONObjectWithData:[responsedebug dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
//            dataResponseSend = [dictResp mutableCopy];
//            [dataResponseSend setValue:transactionId forKey:@"transaction_id"];
//            [self showPopupQRDialog:@"qrsucces"
//                             tTitle:lang(@"QR_TITLE_SUCCESS")
//                            iStatus:@"ic_circle_wavy_check"
//                              tDesc:lang(@"QR_DESC_SUCCESS")
//                            aButton:UILayoutConstraintAxisVertical
//                         txtButtonP:@"OK"
//                      hiddenButtonN:YES];
//        }
    }
}


- (void)qrStatusPositiveState:(NSString *)identifier{
    if([identifier isEqualToString:@"qrsucces"]){
        TemplateViewController *templateView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GENCF02"];
        GENCF02ViewController *viewCont = (GENCF02ViewController *) templateView;
        [dataResponseSend setValue:@"1" forKey:@"pay_infaq"];
        [viewCont setDataLocal:dataResponseSend];
        [viewCont setFromTrx:YES];
        [self saveToDB:dataResponseSend strTransactionId:transactionId];
        [self.navigationController pushViewController:templateView animated:YES];
    } else if ([identifier isEqualToString:@"qrtimeout"] || [identifier isEqualToString:@"qrfailed"]){
        dataManager.currentPosition--;
        UINavigationController *navigationCont = self.navigationController;
        [navigationCont popViewControllerAnimated:YES];
    } else if ([identifier isEqualToString:@"qrnavback"]){
        [self doCallUpdateExpiredQrCpmApi];
        [self backToR];
    }
    
}

- (void)qrStatusNegativeState:(NSString *)identifier{
    if ([identifier isEqualToString:@"qrnavback"]){
        [self timerCallApiStart];
    } else if ([identifier isEqualToString:@"qrtimeout"] || [identifier isEqualToString:@"qrfailed"]){
        [self backToR];
    }
}

-(void)saveToDB : (NSDictionary *) mData strTransactionId : (NSString *) mTransactionId {
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *currentDate = [dateFormat stringFromDate:date];
    NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
    [dictList setValue:[mData valueForKey:@"trxref"] forKey:@"trxref"];
    [dictList setValue:[mData valueForKey:@"title"] forKey:@"title"];
    [dictList setValue:[mData valueForKey:@"msg"] forKey:@"msg"];
    [dictList setValue:[mData valueForKey:@"footer_msg"]  forKey:@"footer_msg"];
    [dictList setValue:mTransactionId forKey:@"transaction_id"];
    [dictList setValue:@"GENCF02" forKey:@"template"];
    [dictList setValue:@"Qr Pay" forKey:@"jenis"];
    [dictList setValue:currentDate forKey:@"time"];
    [dictList setValue:@"0" forKey:@"marked"];
    
    InboxHelper *inboxHeldper = [[InboxHelper alloc]init];
    [inboxHeldper insertDataInbox:dictList];
    
    NSString *strCID = [NSString stringWithFormat:@"%@", [NSUserdefaultsAes getValueForKey:@"customer_id"]];
    NSMutableArray *arBackUpInbox = [[NSMutableArray alloc]init];
    NSDictionary *backUpInbox = [Utility dictForKeychainKey:strCID];
    
    NSMutableDictionary *contentBackup = [[NSMutableDictionary alloc]init];
    NSDictionary *dataEncrypt = [aesChiper dictAesEncryptString:dictList];
    
    //disini validasi jika sudah ada data dikeychain ambil data timpa ke array baru
    if (backUpInbox) {
        NSArray *arrLastBackup = [backUpInbox valueForKey:@"data"];
        for(NSDictionary *dictLastBackup in arrLastBackup){
            if([dictLastBackup valueForKey:@"content"]!= nil){
                NSDictionary *data = @{@"content" : [dictLastBackup valueForKey:@"content"]};
                [arBackUpInbox addObject:data];
            }
        }
       
    }
    [contentBackup setObject:dataEncrypt forKey:@"content"];
    [arBackUpInbox addObject:contentBackup];
    
    if (arBackUpInbox.count > 0) {
        NSLog(@"Data Array For Backup : %@", arBackUpInbox);
        NSMutableDictionary *storeBackupInbox = [[NSMutableDictionary alloc]init];
        [storeBackupInbox setObject:arBackUpInbox forKey:@"data"];
        if (storeBackupInbox) {
            NSLog(@"Data Dict For Backup : %@", storeBackupInbox);
            [Utility removeForKeychainKey:strCID];
            [Utility setDict:storeBackupInbox forKey:strCID];
        }
    }
}

@end
