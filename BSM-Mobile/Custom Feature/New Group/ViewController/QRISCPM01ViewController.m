//
//  QRISCPM01ViewController.m
//  BSM-Mobile
//
//  Created by BinZ on 27/08/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "QRISCPM01ViewController.h"
#import "PopupPinTrxViewController.h"
#import "Utility.h"
#import "PLD01ViewController.h"

@interface QRISCPM01ViewController ()<ConnectionDelegate, PLD01ViewDelegate, PopupPinTrxViewDelegate>{
    NSUserDefaults *userDefault;
    BOOL isRequested;
    NSArray *listDataAcc;
    BOOL mustAddToManager;
    NSDictionary *data;
    NSInteger selectedHoldingAccIdx;
    NSString *selectedHoldingAccNo;
}

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UIView *bgHoldingAcc;
@property (weak, nonatomic) IBOutlet UILabel *lblHoldingAccAltname;
@property (weak, nonatomic) IBOutlet UILabel *lblHoldingAcc;
@property (weak, nonatomic) IBOutlet UILabel *lblQrInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblQrSupported;
@property (weak, nonatomic) IBOutlet UIButton *btnQrGenerate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightQRContent;

@end

@implementation QRISCPM01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userDefault = [NSUserDefaults standardUserDefaults];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [_labelTitle setText:[[dataManager getObjectData] valueForKey:@"title"]];
    _lblHoldingAccAltname.hidden = YES;
    _lblHoldingAcc.text = lang(@"SELECT_DEBIT_ACC");
    _lblQrInfo.text = lang(@"QR_DUMMY_INFO");
    _lblQrSupported.text = lang(@"QR_SUPPORTED");
    _heightQRContent.constant = self.view.frame.size.width-30;
    [_btnQrGenerate setTitle:[lang(@"QR_GENERATE") uppercaseString] forState:UIControlStateNormal];
    
    // border & corner radius
    [_bgHoldingAcc.layer setCornerRadius:10.0f];
    [_bgHoldingAcc.layer setBorderWidth:1.0f];
    [_bgHoldingAcc.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    
    // Setup gesture
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
    [self.bgHoldingAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnHoldingAcc)]];
    [self.bgHoldingAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnHoldingAcc)]];
    [self.btnQrGenerate addTarget:self action:@selector(actionBtnQrGenerate) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidAppear:(BOOL)animated{
    // Setup Service - getListAccount
    [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)navigationBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)actionBtnHoldingAcc{
    PLD01ViewController *dialogList = [self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:lang(@"SELECT_DEBIT_ACC")];
    [dialogList setPlaceholder:lang(@"SEARCH_ACC_NO")];
    [dialogList setFieldName:@"fieldHoldingAcc"];
    [dialogList setList:listDataAcc];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData fieldName:(NSString *)fname{
    data = [arrData objectAtIndex:indexRow];
    if ([fname isEqualToString:@"fieldHoldingAcc"]) {
        selectedHoldingAccIdx = indexRow;
        NSString *titleAltname = [data objectForKey:@"title"];
        if ([titleAltname isEqualToString:@""]){
            _lblHoldingAccAltname.hidden = YES;
        } else {
            _lblHoldingAccAltname.hidden = NO;
        }
        _lblHoldingAccAltname.text = titleAltname;
        _lblHoldingAcc.text = [data objectForKey:@"label"];
        selectedHoldingAccNo = [data objectForKey:@"id_account"];
        _btnQrGenerate.backgroundColor = [UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1];
    }
}

- (void)actionBtnQrGenerate{
    if([_lblHoldingAcc.text isEqualToString:lang(@"SELECT_DEBIT_ACC")]){
        [self showErrorAlert:lang(@"SELECT_DEBIT_ACC")];
    } else {
        [dataManager.dataExtra removeObjectForKey:@"openfromtab"];
        [dataManager.dataExtra setValue:_lblHoldingAccAltname.text forKey:@"holding_accname"];
        [dataManager.dataExtra setValue:_lblHoldingAcc.text forKey:@"holding_acclabel"];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"holding_accname":_lblHoldingAccAltname.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"holding_acclabel":_lblHoldingAcc.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"code":@"00187"}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":selectedHoldingAccNo}];
        NSString *email = [NSUserdefaultsAes getValueForKey:@"email"] != nil ? [NSUserdefaultsAes getValueForKey:@"email"] : @"-";
        [dataManager.dataExtra addEntriesFromDictionary:@{@"email":email}];
        
        PopupPinTrxViewController *dialogPin = [self routeTemplateController:@"POPUPPIN"];
        [dialogPin setDelegate:self];
        [dialogPin setIdentfier:@"qriscpm"];
        [dialogPin setModalPresentationStyle:UIModalPresentationOverFullScreen];
        [dialogPin setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:dialogPin animated:YES completion:nil];
    }
}

- (void)doCheckPin{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_pin,pin,language"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}


- (void)showErrorAlert:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert show];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
            if(!isRequested){
                isRequested = YES;
                [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                
            }
        }else{
            NSString *errMsg = [jsonObject objectForKey:@"response"];
            [self showErrorAlert:errMsg];
        }
    }
    
    if ([requestType isEqualToString:@"verify_pin"]) {
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [self openNextTemplate];
        } else {
            NSString *errMsg = [NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]];
            [self showErrorAlert:errMsg];
        }
    }
}

-(void) getListAccountFromPersitance : (NSString *) urlParam strMenuId : (NSString *) menuId {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct = nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
            if ([dataManager.dataExtra objectForKey:@"code"]) {
                if ([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
        }
    }
    
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];
                
            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct != nil) {
        isRequested = true;
        if(listAcct.count == 0){
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki nomor rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account number for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            newData = [self getListAccountFormatData:listAcct];
            mustAddToManager = true;
            listDataAcc = newData;
        }
    }else{
        if (!isRequested) {
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            
            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
        }else{
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (NSMutableArray*)getListAccountFormatData:(NSArray*)listAcct{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listAcct){
        NSString *title = @"";
        NSString *label = @"";
        if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
            if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                title = [NSString stringWithFormat:@"%@",[temp valueForKey:@"altname"]];
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else{
                label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
            }
        }
        else{
            label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
        }
        [newData addObject:@{@"title":title,@"label":label,@"id_account":[temp valueForKey:@"id_account"]}];
    }
    
    return newData;
}

- (void)pinDoneState:(NSString *)menuID{
    [self doCheckPin];
}

@end
