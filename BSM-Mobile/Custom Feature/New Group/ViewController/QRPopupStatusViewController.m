//
//  QRPopupStatusViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 05/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "QRPopupStatusViewController.h"

@interface QRPopupStatusViewController (){
    id delegate;
    NSString *identifier;
    NSString *tTitle;
    NSString *iStatus;
    NSString *tDesc;
    UILayoutConstraintAxis bAxis;
    NSString *tButtonP;
    NSString *tButtonN;
    BOOL *isHiddenButtonP;
    BOOL *isHiddenButtonN;
}

@property (weak, nonatomic) IBOutlet UILabel *titleStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;
@property (weak, nonatomic) IBOutlet UILabel *descStatus;
@property (weak, nonatomic) IBOutlet UIStackView *btnContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnPositive;
@property (weak, nonatomic) IBOutlet UIButton *btnNegative;

@end

@implementation QRPopupStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _titleStatus.text = tTitle;
    _imgStatus.image = [UIImage imageNamed:iStatus];
    _descStatus.text = tDesc;
    [_btnContainer setAxis:bAxis];
    if(bAxis == UILayoutConstraintAxisHorizontal){
        [_btnContainer removeArrangedSubview:_btnPositive];
        [_btnContainer addArrangedSubview:_btnPositive];
    }
    [_btnPositive setTitle:tButtonP forState:UIControlStateNormal];
    [_btnNegative setTitle:tButtonN forState:UIControlStateNormal];
    [_btnPositive setHidden:isHiddenButtonP];
    [_btnNegative setHidden:isHiddenButtonN];
}

- (IBAction)actionDone:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        [self->delegate qrStatusPositiveState:self->identifier];
    }];
}

- (IBAction)actionCancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        [self->delegate qrStatusNegativeState:self->identifier];
    }];
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setIdentfier:(NSString *)newIdentifier{
    identifier = newIdentifier;
}

- (void)setTitle:(NSString *)txtTitle{
    tTitle = txtTitle;
}

- (void)setImage:(NSString *)imgStatus{
    iStatus = imgStatus;
}

- (void)setDesc:(NSString *)txtDesc{
    tDesc = txtDesc;
}

- (void)setButtonAxis:(UILayoutConstraintAxis)btnAxis{
    bAxis = btnAxis;
}

- (void)setButtonPositive:(BOOL)hidden withText:(NSString *)txtButton{
    isHiddenButtonP = hidden;
    tButtonP = txtButton;
}

- (void)setButtonNegative:(BOOL)hidden withText:(NSString *)txtButton{
    isHiddenButtonN = hidden;
    tButtonN = txtButton;
}

@end
