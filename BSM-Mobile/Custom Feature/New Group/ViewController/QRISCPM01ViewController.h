//
//  QRISCPM01ViewController.h
//  BSM-Mobile
//
//  Created by BinZ on 27/08/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QRISCPM01ViewController : TemplateViewController

- (void)setDataLocal:(NSDictionary*)object;

@end

NS_ASSUME_NONNULL_END
