//
//  QRPopupStatusViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 05/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface QRPopupStatusViewController : TemplateViewController

- (void) setDelegate:(id)newDelagate;
- (void) setIdentfier : (NSString*) newIdentifier;
- (void) setMenuID : (NSString*) newMenuID;
- (void) setTitle : (NSString*) txtTitle;
- (void) setImage : (NSString*) imgStatus;
- (void) setDesc : (NSString*) txtDesc;
- (void) setButtonAxis:(UILayoutConstraintAxis) btnAxis;
- (void) setButtonPositive:(BOOL)hidden withText:(NSString *)txtButton;
- (void) setButtonNegative:(BOOL)hidden withText:(NSString *)txtButton;

@end

@protocol QRPopupStatusViewDelegate

@required

- (void) qrStatusPositiveState : (NSString*) identifier;
- (void) qrStatusNegativeState : (NSString*) identifier;

@end


NS_ASSUME_NONNULL_END
