//
//  TGRadioView.h
//  BSM-Mobile
//
//  Created by Amini on 26/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TGRadioView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgRadio;
@property (weak, nonatomic) IBOutlet UILabel *labelRadio;

@end

NS_ASSUME_NONNULL_END
