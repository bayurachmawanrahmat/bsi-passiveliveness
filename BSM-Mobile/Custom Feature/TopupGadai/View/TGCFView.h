//
//  TGCFView.h
//  BSM-Mobile
//
//  Created by Amini on 29/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TGCFView : UIView
@property (weak, nonatomic) IBOutlet UILabel *keyLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end

NS_ASSUME_NONNULL_END
