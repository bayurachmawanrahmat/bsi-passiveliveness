//
//  TGTextFieldView.h
//  BSM-Mobile
//
//  Created by Amini on 26/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TGTextFieldView : UIView

@property(nonatomic) NSString *errorMessage;
@property(nonatomic,getter=isError) BOOL error;
@property (weak, nonatomic) IBOutlet UILabel *fieldLabel;
@property (weak, nonatomic) IBOutlet UITextField *fieldBox;
@property (weak, nonatomic) IBOutlet UILabel *labelError;

@end

NS_ASSUME_NONNULL_END
