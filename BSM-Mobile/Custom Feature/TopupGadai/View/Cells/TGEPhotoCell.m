//
//  TGEPhotoCell.m
//  BSM-Mobile
//
//  Created by Amini on 19/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGEPhotoCell.h"

@implementation TGEPhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
