//
//  TGEPhotoCell.h
//  BSM-Mobile
//
//  Created by Amini on 19/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TGEPhotoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *labelPhoto;

@end

NS_ASSUME_NONNULL_END
