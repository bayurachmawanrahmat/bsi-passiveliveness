//
//  PortoTopupGadaiCell.h
//  BSM-Mobile
//
//  Created by Amini on 21/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PortoTopupGadaiCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelLD;
@property (weak, nonatomic) IBOutlet UILabel *labelCustomerName;
@property (weak, nonatomic) IBOutlet UILabel *labelProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelOutstanding;
@property (weak, nonatomic) IBOutlet UILabel *labelFee;
@property (weak, nonatomic) IBOutlet UIButton *buttonTopup;
@property (weak, nonatomic) IBOutlet UIButton *buttonViewphoto;

@end

NS_ASSUME_NONNULL_END
