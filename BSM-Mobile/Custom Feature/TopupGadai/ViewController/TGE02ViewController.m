//
//  TGE02ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 22/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGE02ViewController.h"
#import "PortoTopupGadaiCell.h"
#import "TGEPhotoViewController.h"
#import "TGE03ViewController.h"

@interface TGE02ViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *listDataProtofolio;
}
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation TGE02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"PortoTopupGadaiCell" bundle:nil] forCellReuseIdentifier:@"PortoTopupGadaiCell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
        
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void) doRequest{

    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}



#pragma mark UITableViewDelegate and UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PortoTopupGadaiCell *cell = (PortoTopupGadaiCell*)[tableView dequeueReusableCellWithIdentifier:@"PortoTopupGadaiCell"];
    
    NSDictionary* dataIndex = listDataProtofolio[indexPath.row];
    cell.labelLD.text = [dataIndex objectForKey:@"NomorLD"];
    cell.labelCustomerName.text = [dataIndex objectForKey:@"NamaNasabah"];
    cell.labelProduct.text = [dataIndex objectForKey:@"JenisProduk"];
    
    
    NSString *tanggalCair = [dataIndex objectForKey:@"TanggalCair"];
    NSString *tanggalJatemp = [dataIndex objectForKey:@"TanggalJatemp"];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];

    NSDate *tglCair = [dateFormat dateFromString:tanggalCair];
    NSDate *tglJatemp = [dateFormat dateFromString:tanggalJatemp];
    
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    tanggalCair = [dateFormat stringFromDate:tglCair];
    tanggalJatemp = [dateFormat stringFromDate:tglJatemp];
    
    cell.labelDate.text = [NSString stringWithFormat:@"%@-%@",tanggalCair, tanggalJatemp];
    
    
    cell.labelOutstanding.text = [NSString stringWithFormat:@"Outstanding Pokok: Rp. %@",[dataIndex objectForKey:@"Outstanding"]];
    cell.labelFee.text = [NSString stringWithFormat:@"Biaya Pemeliharan 4 Bulan: Rp. %@",[dataIndex objectForKey:@"BiayaUjroh"]];
    
    /*
     "BiayaUjroh":"1,000,000.00","Outstanding":"100,000,000.00","NomorLD":"LD1111111111","TanggalCair":"1/1/2020","JenisProduk":"Gadai Emas","TanggalJatemp":"5/1/2020","KOLCIF":"2","KolLD":"1","NamaNasabah":"Fahri Rohman"}
     **/
    
    
    cell.buttonTopup.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    cell.buttonTopup.layer.borderWidth = 1;
    cell.buttonTopup.layer.cornerRadius = 4;
    cell.buttonTopup.tag = indexPath.row;
    [cell.buttonTopup addTarget:self action:@selector(actionBtnTopUP:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonTopup setTitle:@"Top Up" forState:UIControlStateNormal];

    
    cell.buttonViewphoto.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    cell.buttonViewphoto.layer.borderWidth = 1;
    cell.buttonViewphoto.layer.cornerRadius = 4;
    cell.buttonViewphoto.tag = indexPath.row;
    [cell.buttonViewphoto addTarget:self action:@selector(actionViewPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [cell.buttonViewphoto setTitle:lang(@"VIEW_PHOTO") forState:UIControlStateNormal];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listDataProtofolio.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

#pragma mark button action
-(IBAction)actionBtnTopUP:(id)sender{
    UIButton *btn = (UIButton*)sender;
    NSLog(@"%ld",(long)btn.tag);
    [dataManager.dataExtra setValue:[listDataProtofolio[btn.tag] valueForKey:@"NoApplikasi"] forKey:@"NoApplikasi"];
    [self openNextTemplate];
}

-(IBAction)actionViewPhoto:(id)sender{
    UIButton *btn = (UIButton*)sender;
    NSLog(@"%ld",(long)btn.tag);
    [dataManager.dataExtra setValue:[listDataProtofolio[btn.tag] valueForKey:@"NoApplikasi"] forKey:@"FilterNoAplikasi"];

    TGEPhotoViewController *openLink = (TGEPhotoViewController*)[self routeTemplateController:@"TGEPhoto"];
    [self.navigationController pushViewController:openLink animated:YES];
}

#pragma mark connection delegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"gold_topupgadai"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject objectForKey:@"transaction_id"]}];
            [[NSUserDefaults standardUserDefaults] setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            listDataProtofolio = [jsonObject objectForKey:@"response"];
            [self.tableView reloadData];
        }else{
            NSString *response = [jsonObject valueForKey:@"response"];
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,response] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

@end
