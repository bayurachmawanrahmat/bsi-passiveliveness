//
//  TGAKADViewController.m
//  BSM-Mobile
//
//  Created by Amini on 29/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGAKADViewController.h"

@interface TGAKADViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleBar;

@property (weak, nonatomic) IBOutlet UILabel *labelTitleContent;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;

@property (weak, nonatomic) IBOutlet UILabel *labelAgreement;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckAgreement;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonPrevious;

@end

@implementation TGAKADViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:_topConstant];
    
    
    [self.buttonPrevious setColorSet:SECONDARYCOLORSET];
    
    [self.buttonPrevious addTarget:self action:@selector(actionPrev) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
}

- (void) actionPrev{
    [self backPrevious];
}

- (void) actionNext{
    [self openNextTemplate];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"gold_topupgadai"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *responses = [jsonObject valueForKey:@"response"];
            NSError *error;
            NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:[responses dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
            
            self.labelContent.text = [dict objectForKey:@"content"];
            self.labelTitleContent.text = [dict objectForKey:@"title"];
            self.labelAgreement.text = [dict objectForKey:@"footer_msg"];
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
        }
    }
}

@end
