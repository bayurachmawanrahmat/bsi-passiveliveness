//
//  TGECFViewController.m
//  BSM-Mobile
//
//  Created by Amini on 10/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGECFViewController.h"

@interface TGECFViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonPrevious;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *labelContentHeader;
@property (weak, nonatomic) IBOutlet UILabel *labelContentFooter;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@end

@implementation TGECFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    
}

@end
