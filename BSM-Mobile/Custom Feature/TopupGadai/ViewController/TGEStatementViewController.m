//
//  TGEStatementViewController.m
//  BSM-Mobile
//
//  Created by Amini on 09/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGEStatementViewController.h"
#import "Styles.h"
#import "CustomBtn.h"

@interface TGEStatementViewController (){
    BOOL checkToggle;
}

@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *viewCheck;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;
@property (weak, nonatomic) IBOutlet UILabel *checkLabel;
@property (weak, nonatomic) IBOutlet CustomBtn *cancelButton;
@property (weak, nonatomic) IBOutlet CustomBtn *okButton;

@end

@implementation TGEStatementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles addShadow:self.view];
    
    self.viewFrame.layer.cornerRadius = 10;
    self.viewFrame.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    self.viewFrame.layer.shadowOffset = CGSizeMake(2,2);
    self.viewFrame.layer.shadowRadius = 3;
    self.viewFrame.layer.shadowOpacity = 0.8f;
    
    [self.textView setEditable:NO];
    [self.textView setText:agreementText];
    
    self.viewCheck.userInteractionEnabled = true;
    self.checkLabel.userInteractionEnabled = true;
    
    [self.viewCheck addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCheck)]];
    
    [self.okButton addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton setColorSet:SECONDARYCOLORSET];
    
    self.checkImage.image = [[UIImage imageNamed:@"blank_check"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.checkImage.tintColor = UIColorFromRGB(const_color_secondary);
    
    checkToggle = false;
    self.okButton.enabled = false;
}

- (void) actionCheck{
    if(checkToggle){
        self.checkImage.image = [[UIImage imageNamed:@"blank_check"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.okButton.enabled = false;
        checkToggle = false;
    }else{
        self.checkImage.image = [[UIImage imageNamed:@"check"]imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.okButton.enabled = true;
        checkToggle = true;
    }
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setText:(NSString*)text{
    agreementText = text;
}

- (void) actionNext{
    [self dismissViewControllerAnimated:YES completion:^(){
        [self->delegate didTGEStatementAccepted];
    }];}

- (void) actionCancel{
    [self dismissViewControllerAnimated:YES completion:^(){
        [self->delegate didTGEStatementCanceled];
    }];
}


@end
