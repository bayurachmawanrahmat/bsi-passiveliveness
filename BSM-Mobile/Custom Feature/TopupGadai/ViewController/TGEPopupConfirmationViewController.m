//
//  TGEPopupConfirmationViewController.m
//  BSM-Mobile
//
//  Created by Amini on 29/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGEPopupConfirmationViewController.h"
#import "Styles.h"
#import "CustomBtn.h"

@interface TGEPopupConfirmationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonOk;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@end

@implementation TGEPopupConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles addShadow:self.view];
    
    self.viewContent.layer.cornerRadius = 10;
    self.viewContent.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    self.viewContent.layer.shadowOffset = CGSizeMake(2, 2);
    self.viewContent.layer.shadowRadius = 3;
    self.viewContent.layer.shadowOpacity = 0.8f;
    
    [self.buttonCancel setColorSet:SECONDARYCOLORSET];
    [self.buttonCancel addTarget:self action:@selector(actionCancel) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonOk addTarget:self action:@selector(actionAjukan) forControlEvents:UIControlEventTouchUpInside];

}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)actionCancel{
    [self dismissViewControllerAnimated:YES completion:^(){
        [self->delegate didTGEPopupConfirmationCanceled];
    }];
}

- (void)actionAjukan{
    [self dismissViewControllerAnimated:YES completion:^(){
        [self->delegate didTGEPopupConfirmationAccepted];
    }];
}

@end
