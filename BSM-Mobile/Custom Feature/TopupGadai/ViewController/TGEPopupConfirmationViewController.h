//
//  TGEPopupConfirmationViewController.h
//  BSM-Mobile
//
//  Created by Amini on 29/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TGEPopupConfirmationViewController : UIViewController{
    id delegate;
}

- (void) setDelegate:(id)newDelegate;

@end

@protocol TGEPopupConfirmationDelegate

@required

- (void)didTGEPopupConfirmationCanceled;
- (void)didTGEPopupConfirmationAccepted;

@end

NS_ASSUME_NONNULL_END
