//
//  TGE01ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 29/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGE01ViewController.h"

@interface TGE01ViewController (){
    BOOL toggleCheck;
}

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *viewOutline;
@property (weak, nonatomic) IBOutlet UIView *viewInside;

@property (weak, nonatomic) IBOutlet UIImageView *btnNavBack;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@property (weak, nonatomic) IBOutlet UILabel *labelInformation;
@property (weak, nonatomic) IBOutlet UILabel *labelAttention;

@property (weak, nonatomic) IBOutlet UIImageView *agreementCheck;
@property (weak, nonatomic) IBOutlet UILabel *agreementLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation TGE01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [Styles setTopConstant:_topConstant];
    
    [self.vwContent setHidden:true];
    [self.labelTitle setText:[self.jsonData valueForKey:@"title"]];
    
    // setup back button
    [self.btnNavBack setUserInteractionEnabled:true];
    [self.btnNavBack addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBack)]];
    
    // setup check button
    toggleCheck = false;
    self.agreementCheck.image = [UIImage imageNamed:@"ic_checkbox_inactive"];

    [self.agreementCheck setUserInteractionEnabled:true];
    [self.agreementCheck addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionCheck)]];
    
    [self.buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    self.viewOutline.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    self.viewOutline.layer.shadowOffset = CGSizeMake(2.f, .2f);
    self.viewOutline.layer.shadowRadius = 3.0f;
    self.viewOutline.layer.shadowOpacity = 0.8f;
    self.viewOutline.layer.cornerRadius = 16.0f;
    
    self.viewInside.layer.shadowColor = [[UIColor lightGrayColor]CGColor];
    self.viewInside.layer.shadowOffset = CGSizeMake(2.f, .2f);
    self.viewInside.layer.shadowRadius = 3.0f;
    self.viewInside.layer.shadowOpacity = 0.8f;
    self.viewInside.layer.cornerRadius = 8.0f;
}

- (void) actionBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void) actionNext{
    [self openNextTemplate];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void) actionCheck{
    if(toggleCheck){
        self.agreementCheck.image = [UIImage imageNamed:@"ic_checkbox_inactive"];
        self.buttonNext.enabled = false;
        toggleCheck = false;
    }else{
        self.agreementCheck.image = [UIImage imageNamed:@"ic_checkbox_active"];
        self.buttonNext.enabled = true;
        toggleCheck = true;
    }
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"gold_topupgadai"]){
        NSString *response = [jsonObject objectForKey:@"response"];
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

            self.labelInformation.attributedText = [self setTextHtml:[dataDict valueForKey:@"informasi"]];
            self.labelAttention.attributedText = [self setTextHtml:[dataDict valueForKey:@"attention"]];

            self.agreementLabel.attributedText = [self setTextHtml:[dataDict valueForKey:@"agreement"]];
            self.agreementLabel.textAlignment = NSTextAlignmentJustified;
            self.agreementLabel.font = [UIFont fontWithName:@"Lato-Regular" size:15];
            
            [self.vwContent setHidden:false];

        }else{
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,response] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

- (NSAttributedString*) setTextHtml:(NSString*)htmlString{
    
    htmlString = [NSString stringWithFormat:@"<body style='font-family:Lato-Regular;font-size: 15px; text-align:justify'>%@</body>",htmlString];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
              initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                        NSFontAttributeName:[UIFont fontWithName:const_font_name1 size:15] }
        documentAttributes: nil
                     error: nil
    ];
    
    return attributedString;
}

@end
