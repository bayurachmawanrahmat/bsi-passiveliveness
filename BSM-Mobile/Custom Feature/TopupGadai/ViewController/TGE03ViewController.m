//
//  TGE03ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 22/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGE03ViewController.h"
#import "TGTextFieldView.h"
#import "TGRadioView.h"
#import "TGEPopupConfirmationViewController.h"
#import "TGEStatementViewController.h"

@interface TGE03ViewController ()<UITextFieldDelegate, TGEStatementDelegate, TGEPopupConfirmationDelegate>{
    
    TGTextFieldView *fieldNamaCabang;
    TGTextFieldView *fieldNPPembiayaanBaru;
    TGTextFieldView *fieldNPPembiayaanSebelumnya;
    TGTextFieldView *fieldDanaTopup;
    TGTextFieldView *fieldBiayaPemeliharaanSbelumnya;
    TGTextFieldView *fieldBiayaAdmin;
    TGTextFieldView *fieldSisaDanaTopup;
    TGTextFieldView *fieldPBTaksiranBaru;
    TGTextFieldView *fieldPBPembiayaanBaru;
    TGTextFieldView *fieldPBBiayaPemeliharaanBaru;
    TGTextFieldView *fieldPBJatuhTempo;
    TGTextFieldView *fieldEmail;
    
    UILabel *labelNilaiPembiayaan;
    UILabel *labelPembiayaanBaru;
    UILabel *labelMasukkanEmail;
    UILabel *labelMetodePerpanjangan;
    
    TGRadioView *radioOtomatis;
    TGRadioView *radioManual;
    BOOL isMethodOtomatis;
    BOOL isMethodManual;
    
    UIToolbar* keyboardDoneButtonView;
    
    
    NSString *agreementText;
}
@property (weak, nonatomic) IBOutlet UIView *vwContent;

@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UIImageView *btnBack;

@property (weak, nonatomic) IBOutlet UIView *viewWarning;

@property (weak, nonatomic) IBOutlet CustomBtn *buttonPrevious;
@property (weak, nonatomic) IBOutlet CustomBtn *buttonNext;

@property (weak, nonatomic) IBOutlet UIScrollView *viewScroll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation TGE03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    [_btnBack setUserInteractionEnabled:true];
    [_btnBack addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBack)]];
    [_btnBack setHidden:true];
    
    [_buttonNext setTitle:[lang(@"NEXT") uppercaseString] forState:UIControlStateNormal];
    [_buttonNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [_buttonPrevious setTitle:[lang(@"PREV") uppercaseString] forState:UIControlStateNormal];
    [_buttonPrevious addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [_buttonPrevious setColorSet:SECONDARYCOLORSET];
    
    [_viewWarning.layer setCornerRadius:8];
    
    [self loadContent];
    [self addAccessoryKeypad];
    [self registerForKeyboardNotifications];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void) doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

-(void)addAccessoryKeypad{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    fieldNPPembiayaanBaru.fieldBox.inputAccessoryView = keyboardDoneButtonView;
    fieldEmail.fieldBox.inputAccessoryView = keyboardDoneButtonView;
}

- (IBAction)doneClicked:(id)sender {
    [self.view endEditing:YES];
}

- (void) loadContent{
    [Utility removeAllArrangedViewOf:_stackView];
    
    fieldNamaCabang = [[TGTextFieldView alloc]init];
    fieldNamaCabang.fieldLabel.text = @"Nama Cabang";
    fieldNamaCabang.fieldBox.text = @"KC Jakarta Thamrin";
    fieldNamaCabang.fieldBox.enabled = false;
    
    labelNilaiPembiayaan = [[UILabel alloc]init];
    labelNilaiPembiayaan.font = [UIFont fontWithName:@"Lato-Bold" size:15];
    labelNilaiPembiayaan.text = @"Nilai Pembiayaan";
    
    fieldNPPembiayaanBaru = [[TGTextFieldView alloc]init];
    fieldNPPembiayaanBaru.fieldLabel.text = @"Pembiayaan Baru";
    fieldNPPembiayaanBaru.errorMessage = @"Minimal Pembiayaan yang diberikan adalah Rp. 80.640.000";
    fieldNPPembiayaanBaru.fieldBox.delegate = self;
    fieldNPPembiayaanBaru.fieldBox.text = @"Rp. 80.750.000";
    fieldNPPembiayaanBaru.fieldBox.keyboardType = UIKeyboardTypeNumberPad;
    [fieldNPPembiayaanBaru.fieldBox addTarget:self action:@selector(nominalDidChange:) forControlEvents:UIControlEventEditingChanged];
    [fieldNPPembiayaanBaru.fieldBox addTarget:self action:@selector(pembiayaanBaruDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];

    
    fieldNPPembiayaanSebelumnya = [[TGTextFieldView alloc]init];
    fieldNPPembiayaanSebelumnya.fieldLabel.text = @"Pembiayaan Sebelumnya";
    fieldNPPembiayaanSebelumnya.fieldBox.text = @"Rp. 80.750.000";
    fieldNPPembiayaanSebelumnya.fieldBox.enabled = false;
    
    fieldDanaTopup = [[TGTextFieldView alloc]init];
    fieldDanaTopup.fieldLabel.text = @"Dana Top Up";
    fieldDanaTopup.fieldBox.text = @"Rp. 80.750.000";
    fieldDanaTopup.fieldBox.enabled = false;
    
    fieldBiayaPemeliharaanSbelumnya = [[TGTextFieldView alloc]init];
    fieldBiayaPemeliharaanSbelumnya.fieldLabel.text = @"Biaya Pemeliharaan Sebelumnya";
    fieldBiayaPemeliharaanSbelumnya.fieldBox.text = @"Rp. 80.750.000";
    fieldBiayaPemeliharaanSbelumnya.fieldBox.enabled = false;
    
    fieldBiayaAdmin = [[TGTextFieldView alloc]init];
    fieldBiayaAdmin.fieldLabel.text = @"Biaya Administrasi";
    fieldBiayaAdmin.fieldBox.text = @"Rp. 80.750.000";
    fieldBiayaAdmin.fieldBox.enabled = false;
    
    fieldSisaDanaTopup = [[TGTextFieldView alloc]init];
    fieldSisaDanaTopup.fieldLabel.text = @"Sisa Dana Topup";
    fieldSisaDanaTopup.fieldBox.text = @"Rp. 80.750.000";
    fieldSisaDanaTopup.fieldBox.enabled = false;
    
    UILabel *labelForSpace1 = [[UILabel alloc]init];
    [labelForSpace1 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [labelForSpace1 setText:@"space\nspace"];
    [labelForSpace1 setTextColor:[UIColor clearColor]];
    [labelForSpace1 setNumberOfLines:2];
    
    UILabel *labelForSpace2 = [[UILabel alloc]init];
    [labelForSpace2 setFont:[UIFont fontWithName:@"Lato-Regular" size:15]];
    [labelForSpace2 setText:@"space\nspace"];
    [labelForSpace2 setTextColor:[UIColor clearColor]];
    [labelForSpace2 setNumberOfLines:2];
    
    labelPembiayaanBaru = [[UILabel alloc]init];
    labelPembiayaanBaru.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    labelPembiayaanBaru.text = @"Pembiayaan baru anda menjadi :";
    
    fieldPBTaksiranBaru = [[TGTextFieldView alloc]init];
    fieldPBTaksiranBaru.fieldLabel.text = @"Taksiran baru";
    fieldPBTaksiranBaru.fieldBox.text = @"Rp. 80.750.000";
    fieldPBTaksiranBaru.fieldBox.enabled = false;
    
    fieldPBPembiayaanBaru = [[TGTextFieldView alloc]init];
    fieldPBPembiayaanBaru.fieldLabel.text = @"Pembiayaan Baru";
    fieldPBPembiayaanBaru.fieldBox.text = @"Rp. 80.765.000";
    fieldPBPembiayaanBaru.fieldBox.enabled = false;
    
    fieldPBBiayaPemeliharaanBaru = [[TGTextFieldView alloc]init];
    fieldPBBiayaPemeliharaanBaru.fieldLabel.text = @"Biaya Pemeliharaan Baru";
    fieldPBBiayaPemeliharaanBaru.fieldBox.text = @"Rp. 4.876.000";
    fieldPBBiayaPemeliharaanBaru.fieldBox.enabled = false;
    
    fieldPBJatuhTempo = [[TGTextFieldView alloc]init];
    fieldPBJatuhTempo.fieldLabel.text = @"Jatuh Tempo";
    fieldPBJatuhTempo.fieldBox.text = @"21 April 2021";
    fieldPBJatuhTempo.fieldBox.enabled = false;
    
    labelMasukkanEmail = [[UILabel alloc]init];
    labelMasukkanEmail.font = [UIFont fontWithName:@"Lato-Bold" size:15];
    labelMasukkanEmail.text = @"Masukkan Email Anda :";
    
    fieldEmail = [[TGTextFieldView alloc]init];
    fieldEmail.fieldLabel.text = @"Email";
    fieldEmail.fieldBox.text = [NSUserdefaultsAes getValueForKey:@"email"];
    fieldEmail.errorMessage = @"Wrong Email Format";
    fieldEmail.fieldBox.delegate = self;
    [fieldEmail.fieldBox addTarget:self action:@selector(emailDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    
    labelMetodePerpanjangan = [[UILabel alloc]init];
    labelMetodePerpanjangan.font = [UIFont fontWithName:@"Lato-Regular" size:15];
    labelMetodePerpanjangan.text = @"Metode Perpanjangan";

    radioOtomatis = [[TGRadioView alloc]init];
    radioOtomatis.labelRadio.text = @"Otomatis (Auto Debit rekening saat jatuh tempo)";
    radioOtomatis.userInteractionEnabled = true;
    radioOtomatis.tag = 0;
    [radioOtomatis addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionMethod:)]];
    radioOtomatis.imgRadio.image = [[UIImage imageNamed:@"radio_unselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    radioOtomatis.imgRadio.tintColor = UIColorFromRGB(const_color_secondary);
    
    radioManual = [[TGRadioView alloc]init];
    radioManual.labelRadio.text = @"Manual (Pengajuan ke Cabang saat jatuh tempo)";
    radioManual.userInteractionEnabled = true;
    radioManual.tag = 1;
    [radioManual addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionMethod:)]];
    radioManual.imgRadio.image = [[UIImage imageNamed:@"radio_unselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    radioManual.imgRadio.tintColor = UIColorFromRGB(const_color_secondary);
    
    [_stackView addArrangedSubview:fieldNamaCabang];
    
    [_stackView addArrangedSubview:labelNilaiPembiayaan];
    [_stackView addArrangedSubview:fieldNPPembiayaanBaru];
    [_stackView addArrangedSubview:fieldNPPembiayaanSebelumnya];
    
    [_stackView addArrangedSubview:fieldDanaTopup];
    [_stackView addArrangedSubview:fieldBiayaPemeliharaanSbelumnya];
    [_stackView addArrangedSubview:fieldBiayaAdmin];
    [_stackView addArrangedSubview:fieldDanaTopup];
    
    [_stackView addArrangedSubview:labelForSpace1];
    
    [_stackView addArrangedSubview:labelPembiayaanBaru];
    [_stackView addArrangedSubview:fieldPBTaksiranBaru];
    [_stackView addArrangedSubview:fieldPBPembiayaanBaru];
    [_stackView addArrangedSubview:fieldPBBiayaPemeliharaanBaru];
    [_stackView addArrangedSubview:fieldPBJatuhTempo];
    
    [_stackView addArrangedSubview:labelForSpace2];
    
    [_stackView addArrangedSubview:labelMasukkanEmail];
    [_stackView addArrangedSubview:fieldEmail];
    
    [_stackView addArrangedSubview:labelMetodePerpanjangan];
    [_stackView addArrangedSubview:radioOtomatis];
    [_stackView addArrangedSubview:radioManual];
    
    isMethodManual = false;
    isMethodOtomatis = false;
    _buttonNext.enabled = false;
}

- (void)nominalDidChange:(UITextField *) sender {
    double amountInput = [[self stringNominalReplace:sender.text] doubleValue];
    sender.text = [Utility localFormatCurrencyValue:amountInput showSymbol:false];
}

- (void)pembiayaanBaruDidEndEditing:(UITextField *) sender {
    double amountInput = [[self stringNominalReplace:sender.text] doubleValue];
    fieldNPPembiayaanBaru.error = (amountInput < 10);
    
    /*
      "request_type": "gold_topupgadai",
      "step": "doEditNominal",
      "NoApplikasi": "string",
      "TotalPinjamanMaximum": "string",
      "PinjamanGadaiDiambil": "string",
      "UjrohTaksiran": "string",
      "TotalTaksiranBaru": 0,
     **/
    
    [dataManager.dataExtra setValue:@"" forKey:@"TotalPinjamanMaximum"];
    [dataManager.dataExtra setValue:@"" forKey:@"PinjamanGadaiDiambil"];
    [dataManager.dataExtra setValue:@"" forKey:@"UjrohTaksiran"];
    [dataManager.dataExtra setValue:@"" forKey:@"TotalTaksiranBaru"];
    
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=gold_topupgadai,step=doEditNominal,NoApplikasi,TotalPinjamanMaximum,PinjamanGadaiDiambil,UjrohTaksiran,TotalTaksiranBaru" needLoading:true encrypted:true banking:true favorite:nil];
    
    [self enablingNextButton];
}

- (void)emailDidEndEditing:(UITextField *) sender {
    fieldEmail.error = ![Utility validateEmailWithString:sender.text];
    [self enablingNextButton];
}

- (NSString *)stringNominalReplace:(NSString *) txtString{
    NSString *tfR1 = [txtString stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *tfR2 = [tfR1 stringByReplacingOccurrencesOfString:@"Rp " withString:@""];
    return  tfR2;
}

- (void) enablingNextButton{
    if(fieldNPPembiayaanBaru.error || [fieldNPPembiayaanBaru.fieldBox.text boolValue] == 0){
        _buttonNext.enabled = false;
    }else if (fieldEmail.error || [fieldEmail.fieldBox.text isEqualToString:@""]){
        _buttonNext.enabled = false;
    }else if(!isMethodManual && !isMethodOtomatis){
        _buttonNext.enabled = false;
    }else{
        _buttonNext.enabled = true;
    }
}

- (void)actionMethod:(UITapGestureRecognizer *)sender
{
    if (sender.view.tag == 0 && !isMethodOtomatis){
        TGEStatementViewController *openStatement = (TGEStatementViewController*)[self routeTemplateController:@"TGEStatement"];
        [openStatement setDelegate:self];
        [openStatement setText:agreementText];
        [self presentViewController:openStatement animated:YES completion:nil];
        
    }else if(sender.view.tag == 1 && isMethodOtomatis){
        radioOtomatis.imgRadio.image = [[UIImage imageNamed:@"radio_unselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        radioManual.imgRadio.image = [[UIImage imageNamed:@"radio_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        isMethodOtomatis = false;
        isMethodManual = true;
    }else if(sender.view.tag == 1 && !isMethodManual && !isMethodOtomatis){
        radioOtomatis.imgRadio.image = [[UIImage imageNamed:@"radio_unselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        radioManual.imgRadio.image = [[UIImage imageNamed:@"radio_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

        isMethodOtomatis = false;
        isMethodManual = true;
    }
    radioOtomatis.imgRadio.tintColor = UIColorFromRGB(const_color_secondary);
    radioManual.imgRadio.tintColor = UIColorFromRGB(const_color_secondary);
    [self enablingNextButton];
}

- (void)actionNext{
    
    TGEPopupConfirmationViewController *showConfirmation = (TGEPopupConfirmationViewController*)[self routeTemplateController:@"TGEPopupConfirmation"];
    [showConfirmation setDelegate:self];
    [self presentViewController:showConfirmation animated:YES completion:nil];
    
}

- (void)actionBack{
    [self backPrevious];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"gold_topupgadai"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            NSString *response = [jsonObject objectForKey:@"response"];
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];

            agreementText = [dataDict objectForKey:@"aggrement"];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
        }
    }
}

#pragma mark keyboard notification handler
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{

    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.viewScroll.contentInset = contentInsets;
    self.viewScroll.scrollIndicatorInsets = contentInsets;
}

#pragma mark action handler in form
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];

    return [super canPerformAction:action withSender:sender];
}


#pragma mark TGEStatemenDelegate
- (void)didTGEStatementAccepted{
    radioOtomatis.imgRadio.image = [[UIImage imageNamed:@"radio_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    radioManual.imgRadio.image = [[UIImage imageNamed:@"radio_unselect"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];

    isMethodOtomatis = true;
    isMethodManual = false;
    [self enablingNextButton];
}

- (void)didTGEStatementCanceled{
    
    [self enablingNextButton];
}

#pragma mark TGEPopupConfirmation
- (void)didTGEPopupConfirmationCanceled{
    //do nothing
}

- (void)didTGEPopupConfirmationAccepted{
    [self openNextTemplate];
}

@end
