//
//  TGEStatementViewController.h
//  BSM-Mobile
//
//  Created by Amini on 09/11/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TGEStatementViewController : UIViewController{
    id delegate;
    NSString *agreementText;
}

- (void) setDelegate:(id)newDelegate;
- (void) setText:(NSString*)text;

@end

@protocol TGEStatementDelegate

@required

- (void)didTGEStatementCanceled;
- (void)didTGEStatementAccepted;

@end

NS_ASSUME_NONNULL_END
