//
//  WKTG01ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 26/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TGEPhotoViewController.h"
#import "TGEPhotoCell.h"

@interface TGEPhotoViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *listPhoto;
}
@property (weak, nonatomic) IBOutlet UIImageView *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TGEPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    [_backButton setUserInteractionEnabled:true];
    [_backButton addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBack)]];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TGEPhotoCell" bundle:nil] forCellReuseIdentifier:@"TGEPhotoCell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void) doRequest{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:@"request_type=gold_topupgadai,step=getCollateralPhoto" needLoading:true encrypted:true banking:true favorite:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listPhoto.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TGEPhotoCell *cell = (TGEPhotoCell*)[tableView dequeueReusableCellWithIdentifier:@"TGEPhotoCell"];
    
    cell.viewFrame.layer.borderColor = [[UIColor blackColor] CGColor];
    cell.viewFrame.layer.borderWidth = 1;
    
    NSString *base64img = [listPhoto[indexPath.row] objectForKey:@"Image"];
    [cell.imgPhoto setImage:[self decodeBase64ToImage:base64img]];
    
    cell.labelPhoto.text = [listPhoto[indexPath.row] objectForKey:@"Description"];

    
    return cell;
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
  NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
  return [UIImage imageWithData:data];
}


- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"gold_topupgadai"]){
        if([[jsonObject objectForKey:@"response_code"]isEqualToString:@"00"]){
            listPhoto = [jsonObject objectForKey:@"response"];
            [self.tableView reloadData];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
        }
    }
}

- (void)actionBack{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
