//
//  VeriJelasView.swift
//  BSM-Mobile
//
//  Created by Bayu Rachmawan Rahmat on 07/03/22.
//  Copyright © 2022 lds. All rights reserved.
//

import SwiftUI
import ActivePassiveLivenessSdkIosOlive

struct VeriJelasView: View {
    var body: some View {
        Text("Testing schenario Olive")
        Button("schenario 1"){
            openLivenessDetectorActivity(
                delegate: LivenessDetectorDelegate(
                    livenessOn: { resultLiveness in
                        let jsonData = resultLiveness.data(using: .utf8)!
                        let resultSdk: ResultLivenessOlive = try! JSONDecoder().decode(ResultLivenessOlive.self,from: jsonData)
                        
                        print(resultSdk.image)
                        print(resultSdk.data.passed)
                        print(resultSdk.data.score)
                        
                        guard let jsonData = try? JSONEncoder().encode(
                            OliveDataConvert(
                                passed: resultSdk.data.passed,
                                score: String(resultSdk.data.score)
                            )
                        )
                        else {
                            showAlertFromIos(title: "Message", message: "Bad request: Try again")
                            print("cannot parsing data")
                            return
                        }
                        let responseStrData: String = String(data: jsonData, encoding: .utf8) ?? "no response"
                        print(responseStrData)
                        
                        let resultExampleForYouWant = [ "data": responseStrData, "image": resultSdk.image ] as [String : Any]
                        print(resultExampleForYouWant)
                    },
                    livenessPickerError: { resultLivenessError in
                        showAlertFromIos(title: "Message", message: resultLivenessError)
                        print(resultLivenessError)
                    },
                    onCancelLiveness: {print("cancel by user") }
                ),
                schenario: "schenario1",
                license: Keys.licenseKey,
                apiKey: Keys.apiKey,
                repeatOnNotPassed: false,
                colorPrimary: "#F5D25D"
                )
            }
    }
}

struct VeriJelasView_Previews: PreviewProvider {
    static var previews: some View {
        VeriJelasView() .frame(width: nil)
    }
}

struct Keys {
    static let licenseKey = "2B6AA-C0938-D21C5-AD1A0"
    static let apiKey = "9Oskf1W8jMZJz0WWHnvTpg2BCmOnQXEJ"
}

struct ResultLivenessOlive: Codable {
    let status: String
    let code: String?
    let message: String?
    var image: String
    let data: OliveData
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case code = "code"
        case message = "message"
        case image = "image"
        case data }
}

struct OliveData: Codable {
    let externalID: String?
    let passed: Bool
    let score, selfiePhoto: String
    enum CodingKeys: String, CodingKey{
        case externalID = "external_id"
        case passed, score
        case selfiePhoto = "selfie_photo"
    }
}

struct OliveDataConvert: Codable {
    let passed: Bool
    let score: String
    enum CodingKeys: String, CodingKey{
        case passed, score
    }
}

func showAlertFromIos(title: String, message: String) {
    DispatchQueue.main.async {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(
                title: "OK",
                style: .default,
                handler: { action in
                    switch action.style {
                        case .default: print("default")
                        case .cancel: print("cancel")
                        case .destructive: print("destructive")
                        @unknown default: print("not return")
                    }
                }
            )
        )
        
        let keyWindow = UIApplication.shared.windows.first(where: { $0.isKeyWindow })
        let rootViewController = keyWindow?.rootViewController
        
        DispatchQueue.main.async {
            rootViewController?.present(alert, animated: true)
        }
    }
}
