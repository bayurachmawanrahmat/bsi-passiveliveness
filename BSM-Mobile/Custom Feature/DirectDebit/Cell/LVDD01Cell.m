//
//  LVDD01Cell.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 12/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "LVDD01Cell.h"

@implementation LVDD01Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_btnEdit.layer setCornerRadius:6.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
