//
//  LVDD01Cell.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 12/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LVDD01Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

@end

NS_ASSUME_NONNULL_END
