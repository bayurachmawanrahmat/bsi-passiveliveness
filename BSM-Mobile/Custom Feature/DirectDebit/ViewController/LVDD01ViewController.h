//
//  LVDD01ViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 12/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LVDD01ViewController : TemplateViewController

- (void)setDataLocal:(NSDictionary*)object;

@end

NS_ASSUME_NONNULL_END
