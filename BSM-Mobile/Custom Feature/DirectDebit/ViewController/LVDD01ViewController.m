//
//  LVDD01ViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 12/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "LVDD01ViewController.h"
#import "Utility.h"
#import "LVDD01Cell.h"
#import "Connection.h"

@interface LVDD01ViewController ()<UITableViewDataSource, UITableViewDelegate, ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate, UIScrollViewDelegate>{
    NSArray *listDataInfoDD;
    NSDictionary *dataLocal;
    UIView *imgEmpty;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewEmpty;

@end

@implementation LVDD01ViewController

NSString *lvCell = @"LVDD01Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:_constraintTopView];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    [_labelTitle setText:[[dataManager getObjectData] valueForKey:@"title"]];
    
    // Register Cell
    [self.tableView registerNib:[UINib nibWithNibName:lvCell bundle:nil] forCellReuseIdentifier:lvCell];
    imgEmpty = [Utility showNoData:self.viewEmpty];
    imgEmpty.hidden = YES;
    [self.tableView addSubview:imgEmpty];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData =[NSString stringWithFormat:@"%@,code",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (void)setDataLocal:(NSDictionary*) object{
    [self setJsonData:object];
}

- (void)showErrorAlert:(NSString *)message gotoRoot:(BOOL)toRoot{
    if(toRoot){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,message] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToRoot];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_info_dd"]){
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            listDataInfoDD = [response objectForKey:@"result_data"];
            if(listDataInfoDD.count > 0){
                imgEmpty.hidden = YES;
                [self.tableView reloadData];
            } else {
                imgEmpty.hidden = NO;
            }
        } else {
            NSString *errMsg = [jsonObject objectForKey:@"response"];
            [self showErrorAlert:errMsg gotoRoot:true];
        }
    }
}

- (void)goToEditView:(UIButton *) sender {
    NSString *cardNumber = [listDataInfoDD[sender.tag] objectForKey:@"card_number"];
    NSString *cardStatus = [listDataInfoDD[sender.tag] objectForKey:@"card_status"];
    NSString *merchantId = [listDataInfoDD[sender.tag] objectForKey:@"merchant_id"];
    NSString *expDate = [listDataInfoDD[sender.tag] objectForKey:@"exp_date"];
    NSString *currentLimit = [listDataInfoDD[sender.tag] objectForKey:@"current_limit"];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"card_number":cardNumber}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"card_status":cardStatus}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"merchant_id":merchantId}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"exp_date":expDate}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"current_limit":currentLimit}];
    [super openNextTemplate];
}

- (void)errorLoadData:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - TableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return listDataInfoDD.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LVDD01Cell *cell = (LVDD01Cell*)[tableView dequeueReusableCellWithIdentifier:lvCell];
    if(listDataInfoDD.count > 0){
        NSDictionary *itemData = [listDataInfoDD objectAtIndex:indexPath.row];
        cell.number.text = [NSString stringWithFormat:@"%ld.", indexPath.row+1];
        cell.content.text = [itemData objectForKey:@"list_result"];
        [cell.btnEdit setTag:indexPath.row];
        [cell.btnEdit addTarget:self action:@selector(goToEditView:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

@end
