//
//  CFDD01ViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 15/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "CFDD01ViewController.h"
#import "Utility.h"
#import "Connection.h"

@interface CFDD01ViewController ()<ConnectionDelegate, UIAlertViewDelegate>{
    NSDictionary *listDataInfoDD;
    NSString *cardNumber;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopView;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoContent;
@property (weak, nonatomic) IBOutlet UILabel *lblChangeLimit;
@property (weak, nonatomic) IBOutlet UITextField *tfCardLimit;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoLimit;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;
@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;

@end

@implementation CFDD01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [Styles setTopConstant:_constraintTopView];
    self.labelTitle.textColor = UIColorFromRGB(const_color_title);
    self.labelTitle.textAlignment = const_textalignment_title;
    self.labelTitle.font = [UIFont fontWithName:const_font_name3 size:const_font_size1];
    self.labelTitle.superview.backgroundColor = UIColorFromRGB(const_color_topbar);
    self.lblConfirmation.text = @"";
    [self addAccessoryKeypad];
    [_labelTitle setText:[[dataManager getObjectData] valueForKey:@"title"]];
    [_tfCardLimit addTarget:self action:@selector(nominalTransactionDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_vwContent setHidden:true];

    // Setup button
    [self.btnNext setColorSet:PRIMARYCOLORSET];
    [_btnNext setTitle:lang(@"NEXT") forState:UIControlStateNormal];
    [self.btnCancel setColorSet:SECONDARYCOLORSET];
    [_btnCancel setTitle:lang(@"CANCEL") forState:UIControlStateNormal];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData =[NSString stringWithFormat:@"%@,code",[self.jsonData valueForKey:@"url_parm"]];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (void)reloadContentData{
    [_vwContent setHidden:false];
    [_btnStatus setSelected: [[listDataInfoDD objectForKey:@"card_status"] isEqualToString:@"ACTIVE"] ? NO : YES];
    [self setStatusButton];
    cardNumber = [dataManager.dataExtra valueForKey:@"card_number"];
    self.lblInfoContent.text = [listDataInfoDD objectForKey:@"content"];
    self.lblChangeLimit.text = lang(@"LABEL_CHANGE_LIMIT");
    self.tfCardLimit.text = [Utility localFormatCurrencyValue:[[listDataInfoDD objectForKey:@"current_limit"]doubleValue]  showSymbol:true];
    self.lblInfoLimit.text = [listDataInfoDD objectForKey:@"info_limit"];
}

- (void)setStatusButton{
    if(_btnStatus.isSelected) {
        [_btnStatus setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        _lblConfirmation.text = [NSString stringWithFormat:lang(@"CONFIRM_DISABLE_CARD"), cardNumber];
        _tfCardLimit.userInteractionEnabled = NO;
    } else {
        [_btnStatus setImage:[UIImage imageNamed:@"blank_check"] forState:UIControlStateNormal];
        _lblConfirmation.text = @"";
        _tfCardLimit.userInteractionEnabled = YES;
    }
}

- (void)nominalTransactionDidChange:(UITextField *) sender {
    double new_limit = [[self stringNominalReplace:sender.text] doubleValue];
    double current_limit = [[listDataInfoDD objectForKey:@"current_limit"]doubleValue];
    double info_limit_min = [[listDataInfoDD objectForKey:@"info_limit_min"]doubleValue];
    double info_limit_max = [[listDataInfoDD objectForKey:@"info_limit_max"]doubleValue];
    sender.text = [Utility localFormatCurrencyValue:new_limit showSymbol:true];
    
    if(new_limit == current_limit){
        _lblConfirmation.text = @"";
    } else if (new_limit > info_limit_max){
        [self showErrorAlert:lang(@"LIMIT_ALERT") gotoRoot:false];
        sender.text = [Utility localFormatCurrencyValue:current_limit showSymbol:true];
        _lblConfirmation.text = @"";
    } else {
        _lblConfirmation.text = [NSString stringWithFormat:lang(@"CONFIRM_LIMIT_CARD"), cardNumber];
    }
}

-(NSString *)stringNominalReplace:(NSString *) txtString{
    NSString *tfR1 = [txtString stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *tfR2 = [tfR1 stringByReplacingOccurrencesOfString:@"Rp " withString:@""];
    return  tfR2;
}

-(void)addAccessoryKeypad{
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    _tfCardLimit.inputAccessoryView = keyboardDoneButtonView;
}

- (void)showErrorAlert:(NSString *)message gotoRoot:(BOOL)toRoot{
    if(toRoot){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,message] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self backToRoot];
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)doneClicked:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark change next to goToCancel
- (IBAction)actionStatus:(UIButton *)sender {
    [_btnStatus setSelected: !sender.isSelected];
    [self setStatusButton];
}

#pragma mark change next to goToCancel
- (IBAction)actionNext:(id)sender {
    NSString *currentLimit = [listDataInfoDD objectForKey:@"current_limit"];
    NSString *newLimit = [self stringNominalReplace:_tfCardLimit.text] ;
    NSString *cardStatus = _btnStatus.isSelected ? @"INACTIVE" : @"ACTIVE";
    [dataManager.dataExtra addEntriesFromDictionary:@{@"current_limit":currentLimit}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"new_limit":newLimit}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"card_status":cardStatus}];
    [super openNextTemplate];
}

#pragma mark change batal to goToNext;
- (IBAction)actionCancel:(id)sender {
    [super backToRoot];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"inq_info_dd"]){
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            listDataInfoDD = response;
            [self reloadContentData];
        } else {
            NSString *errMsg = [jsonObject objectForKey:@"response"];
            [self showErrorAlert:errMsg gotoRoot:true];
        }
    }
}

@end
