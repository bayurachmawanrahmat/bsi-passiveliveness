//
//  RecentlyTransferCell.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 23/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecentlyTransferCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *fvInitial;
@property (weak, nonatomic) IBOutlet UILabel *lblInitial;
@property (weak, nonatomic) IBOutlet UILabel *lblAccName;
@property (weak, nonatomic) IBOutlet UILabel *lblBankName;
@property (weak, nonatomic) IBOutlet UILabel *lblAccNo;

@end

NS_ASSUME_NONNULL_END
