//
//  SubmenuCell.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 22/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "CollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubmenuCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *fvSubmenu;
@property (weak, nonatomic) IBOutlet UIView *bgSubmenu;
@property (weak, nonatomic) IBOutlet UIImageView *smImage;
@property (weak, nonatomic) IBOutlet UILabel *smLabel;

@end

NS_ASSUME_NONNULL_END
