//
//  RecentlyTransferCell.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 23/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "RecentlyTransferCell.h"

@implementation RecentlyTransferCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_fvInitial.layer setCornerRadius:25.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
