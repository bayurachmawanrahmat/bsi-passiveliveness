//
//  SubmenuCell.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 22/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "SubmenuCell.h"

//static CGFloat radius = 10;
//static int shadowOffsetWidth = 0;
//static int shadowOffsetHeight = 3;
//static float shadowOpacity = 0.5;

@implementation SubmenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_bgSubmenu.layer setCornerRadius:10.0f];
    [_fvSubmenu.layer setCornerRadius:10.0f];
    [_fvSubmenu.layer setBorderWidth:0.2f];
    [_fvSubmenu.layer setBorderColor:[UIColor colorWithRed:202/255.0 green:202/255.0 blue:202/255.0 alpha:1].CGColor];
    [_fvSubmenu.layer setMasksToBounds:false];
    [_fvSubmenu.layer setShadowOffset:CGSizeMake(0.0, 0.5)];
    [_fvSubmenu.layer setShadowRadius:0.4f];
    [_fvSubmenu.layer setShadowOpacity:0.3f];
}

@end
