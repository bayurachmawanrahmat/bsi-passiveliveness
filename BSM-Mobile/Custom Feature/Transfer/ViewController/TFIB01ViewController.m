//
//  TFIB01ViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 15/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TFIB01ViewController.h"
#import "PLD01ViewController.h"
#import "Connection.h"
#import <AVFoundation/AVFoundation.h>
#import "Utility.h"

@interface TFIB01ViewController ()<ConnectionDelegate, PLD01ViewDelegate, UITextFieldDelegate, AVCaptureMetadataOutputObjectsDelegate, CAAnimationDelegate>{
    NSUserDefaults *userDefault;
    NSString *menuId;
    NSArray *arrayLabel;
    NSArray *listDataAcc;
    NSMutableArray *listDestTransfer;
    NSArray *listDataBank;
    NSArray *listDataFav;
    NSInteger optTransferIdx;
    BOOL isShowing;
    BOOL isRequested;
    BOOL mustAddToManager;
    NSString *key;
    NSArray *listAction;
    NSDictionary *data;
    NSString *selectedSrcAccNo;
    NSString *selectedDestAccNo;
    NSInteger selectedSrcAccIdx;
    UIView *viewPreviewQR;
    UIView *viewFlash;
    UIView *kotakScan;
    UIButton *btnFlash;
    UILabel *lblFlash;
    BOOL stateFlash;
    AVCaptureSession *session;
    AVCaptureDevice *device;
    AVCaptureDeviceInput *input;
    AVCaptureMetadataOutput *output;
    AVCaptureVideoPreviewLayer *previewLayer;
    NSString *lang;
    UIToolbar* keyboardDoneButtonView;
    CGSize keyboardSize;
    CGFloat originHeightContent;
}

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwFooter;
@property (weak, nonatomic) IBOutlet UIView *vwNavBar;
@property (weak, nonatomic) IBOutlet UIView *vwQRFrame;
@property (weak, nonatomic) IBOutlet UIView *vwQRNavBar;
@property (weak, nonatomic) IBOutlet UIView *vwQRContent;
@property (weak, nonatomic) IBOutlet UIImageView *navQRBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblQRTitleNav;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAccAltname;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAcc;
@property (weak, nonatomic) IBOutlet UIView *bgSourceAcc;
@property (weak, nonatomic) IBOutlet UIImageView *btnSourceAcc;
@property (weak, nonatomic) IBOutlet UISegmentedControl *optionTransferToggle;
@property (weak, nonatomic) IBOutlet UIView *fvDestAcc;
@property (weak, nonatomic) IBOutlet UILabel *lblDestAcc;
@property (weak, nonatomic) IBOutlet UITextField *tfDestAcc;
@property (weak, nonatomic) IBOutlet UIImageView *btnDestAcc;
@property (weak, nonatomic) IBOutlet UIView *fvDestAccNo;
@property (weak, nonatomic) IBOutlet UILabel *lblDestAccNo;
@property (weak, nonatomic) IBOutlet UIStackView *svDestAccNo;
@property (weak, nonatomic) IBOutlet UITextField *tfDestAccNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleDestAccNo;
@property (weak, nonatomic) IBOutlet UILabel *lblValDestAccNo;
@property (weak, nonatomic) IBOutlet UIView *fvBtnDestAccNo;
@property (weak, nonatomic) IBOutlet UIImageView *btnDestAccNo;
@property (weak, nonatomic) IBOutlet UIView *fvQrDestAccNo;
@property (weak, nonatomic) IBOutlet UIImageView *btnScanQR;
@property (weak, nonatomic) IBOutlet UIImageView *btnFindQR;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightVwDestAccNo;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightTfDestAccNo;
@property (weak, nonatomic) IBOutlet UIView *fvMethodTrf;
@property (weak, nonatomic) IBOutlet UILabel *lblMethodTrf;
@property (weak, nonatomic) IBOutlet UIView *fvRbMethodTrf;
@property (weak, nonatomic) IBOutlet UIView *fvMethodTrfOnline;
@property (weak, nonatomic) IBOutlet UIImageView *rbMethodTrfOnline;
@property (weak, nonatomic) IBOutlet UIView *fvMethodTrfSkn;
@property (weak, nonatomic) IBOutlet UIImageView *rbMethodTrfSkn;
@property (weak, nonatomic) IBOutlet UIView *fvAmountTrf;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountTrf;
@property (weak, nonatomic) IBOutlet UITextField *tfAmountTrf;
@property (weak, nonatomic) IBOutlet UIView *fvDescTrf;
@property (weak, nonatomic) IBOutlet UILabel *lblDescTrf;
@property (weak, nonatomic) IBOutlet UITextField *tfDescTrf;
@property (weak, nonatomic) IBOutlet UIView *fvReffNo;
@property (weak, nonatomic) IBOutlet UILabel *lblReffNo;
@property (weak, nonatomic) IBOutlet UITextField *tfReffNo;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightVwFooter;

@end

extern NSString *lv_selected;

@implementation TFIB01ViewController

NSString *menuIdTfInternal = @"00027";
NSString *menuIdTfEksternal = @"00028";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    listDataFav = [[NSMutableArray alloc] init];
    userDefault = [NSUserDefaults standardUserDefaults];
    menuId = [self.jsonData valueForKey:@"menu_id"];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"menu_id":menuId}];
    // Setup navigationn
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
    _lblTitleNav.text = [self.jsonData valueForKey:@"title"];
    [self.navQRBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationQRBack)]];
    _lblQRTitleNav.text = @"Scan QR";//[self.jsonData valueForKey:@"title"];
    
    // Setup view source account
    _lblSourceAccAltname.hidden = YES;
    _lblSourceAcc.text = lang(@"SELECT_DEBIT_ACC");
    // border & corner radius
    [_bgSourceAcc.layer setCornerRadius:10.0f];
    [_bgSourceAcc.layer setBorderWidth:1.0f];
    [_bgSourceAcc.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    
    // Setup Tab Option Transfer
    [_optionTransferToggle setTitle:lang(@"TAB_NEW_TRANSFER") forSegmentAtIndex:0];
    [_optionTransferToggle setTitle:lang(@"TAB_FAV_TRANSFER") forSegmentAtIndex:1];
    
    // Setup field
    if([menuId isEqualToString:menuIdTfInternal]){ // Antar BSI
        [self showNewTransferInternal];
    } else { // Antar Bank
        [self showNewTransferEksternal];
    }
    _lblMethodTrf.text = lang(@"SELECT_METHOD_TRF");
    _lblAmountTrf.text = lang(@"INPUT_AMOUNT_TRF");
    _lblDescTrf.text = lang(@"INPUT_DESC_TRF");
    _lblReffNo.text = lang(@"INPUT_REFF_NO");
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _fvRbMethodTrf.bounds  byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft | UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii: (CGSize){10.0,10.0,10.0,10.0}].CGPath;
    [_fvRbMethodTrf.layer setMask:maskLayer];
    [_fvRbMethodTrf.layer setCornerRadius:10.0f];
    [_fvRbMethodTrf.layer setBorderWidth:1.0f];
    [_fvRbMethodTrf.layer setBorderColor:[UIColor colorWithRed:221/255.0 green:221/255.0 blue:221/255.0 alpha:1].CGColor];
    
    // Setup gesture
    [self.bgSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    [self.btnSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    [self.tfDestAcc addTarget:self action:@selector(actionBtnDestAcc) forControlEvents:UIControlEventTouchDown];
    [self.btnDestAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnDestAcc)]];
    [self.btnDestAccNo addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnDestAccNo)]];
    [self.btnScanQR addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showScanQR)]];
    [self.btnFindQR addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showBrowseQR)]];
    [self.fvMethodTrfOnline addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionMethodTransfer:)]];
    [self.fvMethodTrfSkn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionMethodTransfer:)]];
    [self.tfAmountTrf addTarget:self action:@selector(nominalTransactionDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext setTitle:[lang(@"NEXT") uppercaseString] forState:UIControlStateNormal];
    
    // Misc method
    originHeightContent = _vwContent.frame.size.height;
    listDestTransfer = [[NSMutableArray alloc]init];
    [listDestTransfer addObject:@{@"key":@"1",@"title":@"",@"label":lang(@"OWN_ACC")}];
    [listDestTransfer addObject:@{@"key":@"2",@"title":@"",@"label":lang(@"OTHERS_ACC")}];
    [self addAccessoryKeypad];
    [self resetFormTransfer];
    [self checkIfTypeTransferExist];
    
    // Setup Service
    // getListAccount
    [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
    
}

- (void)checkIfTypeTransferExist {
    NSString *typeTrf = [self.jsonData valueForKey:@"type_transfer"];
    if(typeTrf){
        [dataManager.dataExtra addEntriesFromDictionary:@{@"type_transfer":typeTrf}];
        [self setMethodTransferButton];
    }
}

- (void)resetFormTransfer {
    selectedSrcAccNo = @"";
    selectedDestAccNo = @"";
    [dataManager.dataExtra removeObjectForKey:@"type_transfer"];
}

- (void)viewDidLayoutSubviews {
    CGRect bounds = viewPreviewQR.layer.bounds;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    previewLayer.bounds=bounds;
    previewLayer.position=CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
}

- (void)navigationBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)navigationQRBack{
    [self closeScanner];
}

- (IBAction)switcherOptionTransfer:(UISegmentedControl*)sender {
    [self setOptionTransferView:sender.selectedSegmentIndex];
}

- (void)reloadDataFromRecently {
    if ([[dataManager.dataExtra valueForKey:@"from_recently"] boolValue]){
        NSString *descBankCode = [dataManager.dataExtra valueForKey:@"code"];
        NSString *destAccNo = [dataManager.dataExtra valueForKey:@"payment_id"];
        if([descBankCode isEqualToString:@"451"]){
            int idxdest = 0;
            if ([self checkArray:listDataAcc containsSubstring:destAccNo]) {
                idxdest = 0;
                _lblDestAccNo.text = lang(@"SELECT_DEST_ACCNO");
                self.fvBtnDestAccNo.hidden = NO;
                self.fvQrDestAccNo.hidden = YES;
                [self.tfDestAccNo addTarget:self action:@selector(actionBtnDestAccNo) forControlEvents:UIControlEventTouchDown];
            } else {
                idxdest = 1;
                _lblDestAccNo.text = lang(@"INPUT_DEST_ACCNO");
                self.fvBtnDestAccNo.hidden = YES;
                self.fvQrDestAccNo.hidden = NO;
                [self.tfDestAccNo removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
            }
            self.fvDestAccNo.hidden = NO;
            self.fvAmountTrf.hidden = NO;
            self.fvDescTrf.hidden = NO;
            _tfDestAcc.text = [[listDestTransfer objectAtIndex:idxdest] objectForKey:@"label"];
        } else {
            if([dataManager.dataExtra valueForKey:@"destbank_name"]){
                _tfDestAcc.text = [dataManager.dataExtra valueForKey:@"destbank_name"];
            } else {
                NSInteger idx = [self checkIdxArray:listDataBank containsSubstring:descBankCode];
                _tfDestAcc.text = [[listDataBank objectAtIndex:idx] valueForKey:@"label"];
            }
        }
        _tfDestAccNo.text = destAccNo;
        selectedDestAccNo = destAccNo;
    }
}

- (BOOL)checkArray:(NSArray *)array containsSubstring:(NSString *)substring {
    for (NSDictionary *dict in array) {
        if ([[dict valueForKey:@"id_account"] isEqualToString:substring]) {
            return YES;
            break;
        }
    }
    return NO;
}

- (NSInteger)checkIdxArray:(NSArray *)array containsSubstring:(NSString *)substring {
    NSInteger index = 0;
    for (NSDictionary *dict in array) {
        if ([[dict valueForKey:@"code"] isEqualToString:substring]) {
            return index;
            break;
        }
        index++;
    }
    return index;
}

- (void)setOptionTransferView:(NSInteger)indexSelected{
    optTransferIdx = indexSelected;
    switch(indexSelected) {
        case 0:
            if([menuId isEqualToString:menuIdTfInternal]){
                [self showNewTransferInternal];
            } else {
                [self showNewTransferEksternal];
            }
            break;
        case 1:
            if([menuId isEqualToString:menuIdTfInternal]){
                [self showFavTransferInternal];
            } else {
                [self showFavTransferEksternal];
            }
            break;
        default:
            break;
    }
}

- (void)showNewTransferInternal{
    [self resetFieldTransfer];
    self.fvDestAcc.hidden = NO;
    self.fvDestAccNo.hidden = YES;
    self.fvMethodTrf.hidden = YES;
    self.fvAmountTrf.hidden = YES;
    self.fvDescTrf.hidden = YES;
    self.fvReffNo.hidden = YES;
    self.lblDestAcc.text = lang(@"SELECT_DEST_ACC");
}

- (void)showFavTransferInternal{
    [self resetFieldTransfer];
    [self reloadDataFavorite];
    self.fvDestAcc.hidden = YES;
    self.fvDestAccNo.hidden = NO;
    self.fvBtnDestAccNo.hidden = NO;
    self.fvQrDestAccNo.hidden = YES;
    self.fvMethodTrf.hidden = YES;
    self.fvAmountTrf.hidden = NO;
    self.fvDescTrf.hidden = NO;
    self.fvReffNo.hidden = YES;
    self.lblDestAccNo.text = lang(@"SELECT_DEST_ACCNO");
    [self.tfDestAccNo addTarget:self action:@selector(actionBtnDestAccNo) forControlEvents:UIControlEventTouchDown];
    [self.svDestAccNo addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnDestAccNo)]];
}

- (void)showNewTransferEksternal{
    [self resetFieldTransfer];
    [self reloadDataListBank];
    self.fvDestAcc.hidden = NO;
    self.fvDestAccNo.hidden = NO;
    self.fvBtnDestAccNo.hidden = YES;
    self.fvQrDestAccNo.hidden = YES;
    self.fvMethodTrf.hidden = NO;
    self.fvAmountTrf.hidden = NO;
    self.fvDescTrf.hidden = NO;
    self.fvReffNo.hidden = NO;
    self.tfDestAccNo.hidden = NO;
    self.lblDestAcc.text = lang(@"SELECT_DEST_BANK");
    self.lblDestAccNo.text = lang(@"INPUT_DEST_ACCNO");
    [self.tfDestAccNo removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
}

- (void)showFavTransferEksternal{
    [self resetFieldTransfer];
    [self reloadDataFavorite];
    self.fvDestAcc.hidden = YES;
    self.fvDestAccNo.hidden = NO;
    self.fvBtnDestAccNo.hidden = NO;
    self.fvQrDestAccNo.hidden = YES;
    self.fvMethodTrf.hidden = NO;
    self.fvAmountTrf.hidden = NO;
    self.fvDescTrf.hidden = NO;
    self.fvReffNo.hidden = NO;
    self.lblDestAccNo.text = lang(@"SELECT_DEST_ACCNO");
    [self.tfDestAccNo addTarget:self action:@selector(actionBtnDestAccNo) forControlEvents:UIControlEventTouchDown];
    [self.svDestAccNo addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnDestAccNo)]];
}

- (void)resetFieldTransfer{
    self.tfDestAcc.text = @"";
    self.tfDestAccNo.text = @"";
    self.lblTitleDestAccNo.text = @"";
    self.lblValDestAccNo.text = @"";
    self.tfAmountTrf.text = @"";
    self.tfDescTrf.text = @"";
    self.tfReffNo.text = @"";
    self.heightVwDestAccNo.constant = 72;
    self.heightTfDestAccNo.constant = 36;
}

- (void)actionBtnSourceAcc{
    PLD01ViewController *dialogList = [self.storyboard instantiateViewControllerWithIdentifier:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:lang(@"SELECT_DEBIT_ACC")];
    [dialogList setPlaceholder:lang(@"SEARCH_ACC_NO")];
    [dialogList setFieldName:@"fieldSourceAcc"];
    [dialogList setList:listDataAcc];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)actionBtnDestAcc{
    PLD01ViewController *dialogList = [self.storyboard instantiateViewControllerWithIdentifier:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:_lblDestAcc.text];
    if([menuId isEqualToString:menuIdTfInternal]){ // Antar BSI
        [dialogList setHiddenSearchBox:YES];
        [dialogList setHeightViewContent:78+(44*listDestTransfer.count)];
        [dialogList setFieldName:@"fieldDestAcc"];
        [dialogList setList:listDestTransfer];
    } else { // Antar Bank
        [dialogList setHiddenSearchBox:NO];
        [dialogList setHeightViewContent:118+(44*10)];
        [dialogList setFieldName:@"fieldDestBank"];
        [dialogList setList:listDataBank];
    }
    
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)actionBtnDestAccNo{
    if (selectedSrcAccNo == nil){
        [self showErrorAlert:lang(@"ERROR_SELECT_ACC_FIRST")];
    } else {
        NSMutableArray *listDestAccNo = [listDataAcc mutableCopy];
        [listDestAccNo removeObjectAtIndex:selectedSrcAccIdx];
        PLD01ViewController *dialogList = [self.storyboard instantiateViewControllerWithIdentifier:@"PLD01VC"];
        [dialogList setDelegate:self];
        [dialogList setTitle:_lblDestAccNo.text];
        [dialogList setPlaceholder:lang(@"SEARCH_ACC_NO")];
        [dialogList setFieldName:@"fieldDestAccNo"];
        if (optTransferIdx == 0) {
            [dialogList setList:listDestAccNo];
        } else {
            [dialogList setList:listDataFav];
        }
        [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
        [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:dialogList animated:YES completion:nil];
    }
}

- (void)actionMethodTransfer:(UITapGestureRecognizer *)tapGesture{
    if(tapGesture.view.tag == 1001){
        [dataManager.dataExtra addEntriesFromDictionary:@{@"type_transfer":@"1"}];
    } else {
        [dataManager.dataExtra addEntriesFromDictionary:@{@"type_transfer":@"2"}];
    }
    [self setMethodTransferButton];
}

- (void)setMethodTransferButton{
//    UIImage *imgSelected = [[UIImage imageNamed:@"radio_select"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIImage *imgSelected = [UIImage imageNamed:@"radio_select"];
    UIImage *imgUnselected = [UIImage imageNamed:@"radio_unselect"];
    UIColor *clrSelected = [UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1];
    UIColor *clrUnselected = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
    
    if([[dataManager.dataExtra valueForKey:@"type_transfer"] isEqualToString:@"1"]){
        [_rbMethodTrfOnline setImage:imgSelected];
        [_fvMethodTrfOnline setBackgroundColor:clrSelected];
        [_rbMethodTrfSkn setImage:imgUnselected];
        [_fvMethodTrfSkn setBackgroundColor:clrUnselected];
    } else {
        [_rbMethodTrfSkn setImage:imgSelected];
        [_fvMethodTrfSkn setBackgroundColor:clrSelected];
        [_rbMethodTrfOnline setImage:imgUnselected];
        [_fvMethodTrfOnline setBackgroundColor:clrUnselected];
    }
}

- (void)nominalTransactionDidChange:(UITextField *) sender {
    double amountInput = [[self stringNominalReplace:sender.text] doubleValue];
    sender.text = [Utility localFormatCurrencyValue:amountInput showSymbol:false];
}

- (NSString *)stringNominalReplace:(NSString *) txtString{
    NSString *tfR1 = [txtString stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *tfR2 = [tfR1 stringByReplacingOccurrencesOfString:@"Rp " withString:@""];
    return  tfR2;
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData fieldName:(NSString *)fname{
    data = [arrData objectAtIndex:indexRow];
    if ([fname isEqualToString:@"fieldSourceAcc"]) {
        selectedSrcAccIdx = indexRow;
        NSString *titleAltname = [data objectForKey:@"title"];
        if ([titleAltname isEqualToString:@""]){
            _lblSourceAccAltname.hidden = YES;
        } else {
            _lblSourceAccAltname.hidden = NO;
        }
        _lblSourceAccAltname.text = titleAltname;
        _lblSourceAcc.text = [data objectForKey:@"label"];
        selectedSrcAccNo = [data objectForKey:@"id_account"];
    } else if ([fname isEqualToString:@"fieldDestAcc"]) {
        NSString *key = [data objectForKey:@"key"];
        if ([key isEqualToString:@"1"]) {
            _lblDestAccNo.text = lang(@"SELECT_DEST_ACCNO");
            self.fvBtnDestAccNo.hidden = NO;
            self.fvQrDestAccNo.hidden = YES;
            [self.tfDestAccNo addTarget:self action:@selector(actionBtnDestAccNo) forControlEvents:UIControlEventTouchDown];
        } else {
            _lblDestAccNo.text = lang(@"INPUT_DEST_ACCNO");
            _tfDestAccNo.text = @"";
            self.tfDestAccNo.hidden = NO;
            self.fvBtnDestAccNo.hidden = YES;
            self.fvQrDestAccNo.hidden = NO;
            [self.tfDestAccNo removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
        }
        self.fvDestAccNo.hidden = NO;
        self.fvAmountTrf.hidden = NO;
        self.fvDescTrf.hidden = NO;
        _tfDestAcc.text = [data objectForKey:@"label"];
    } else if ([fname isEqualToString:@"fieldDestBank"]) {
        _tfDestAcc.text = [data objectForKey:@"label"];
        NSString *code = [data objectForKey:@"code"];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"code":code}];
    } else if ([fname isEqualToString:@"fieldDestAccNo"]) {
        if (optTransferIdx == 0) {
            selectedDestAccNo = [data objectForKey:@"id_account"];
            _tfDestAccNo.text = selectedDestAccNo;
        } else {
            NSArray *paramArray = [[data objectForKey:@"params"] componentsSeparatedByString: @","];
            for(NSString *params in paramArray){
                NSArray *string = [params componentsSeparatedByString: @"="];
                [dataManager.dataExtra addEntriesFromDictionary:@{string[0]:string[1]}];
                if([string[0] isEqualToString:@"payment_id"]){
                    selectedDestAccNo = string[1];
                }
            }
            _tfDestAccNo.hidden = YES;
            _tfDestAccNo.text = selectedDestAccNo;
            if([menuId isEqualToString:menuIdTfInternal]){
                _heightVwDestAccNo.constant = 126;
                _heightTfDestAccNo.constant = 90;
            } else {
                _heightVwDestAccNo.constant = 146;
                _heightTfDestAccNo.constant = 110;
                [self setMethodTransferButton];
            }
            _lblTitleDestAccNo.text = [data objectForKey:@"title"];
            _lblValDestAccNo.text = [data objectForKey:@"label"];
        }
    }
}

- (void)reloadDataFavorite{
    if(!listDataFav || listDataFav.count == 0){
        dataManager.menuId = menuId;
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        conn.fav = true;
        // type=3 is Transfer
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_favorite,menu_id=%@,type=%d",menuId,3] needLoading:true encrypted:true banking:false];
    }
}

- (void)reloadDataListBank{
    if(!listDataBank){
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_bank2"] needLoading:true encrypted:true banking:false];
    }
}

-(void)actionNext{
    // setup field
    if(![selectedDestAccNo isEqualToString:_tfDestAccNo.text] || [selectedDestAccNo isEqualToString:@""]){
        selectedDestAccNo = _tfDestAccNo.text;
    }
    
    // validation
    NSString *errMsg = @"";
    if([selectedSrcAccNo isEqualToString:@""]){
        errMsg = lang(@"SELECT_DEBIT_ACC");
    } else if([_tfDestAcc.text isEqualToString:@""] && (optTransferIdx == 0)){
        errMsg = [menuId isEqualToString:menuIdTfInternal] ? lang(@"SELECT_DEST_ACC") : lang(@"SELECT_DEST_BANK");
    } else if([_tfDestAccNo.text isEqualToString:@""]){
        errMsg = _lblDestAccNo.text;
    } else if(![dataManager.dataExtra valueForKey:@"type_transfer"] && [menuId isEqualToString:menuIdTfEksternal]){
        errMsg = lang(@"SELECT_METHOD_TRF");
    } else if([_tfAmountTrf.text isEqualToString:@""]){
        errMsg = lang(@"INPUT_AMOUNT_TRF");
    } else if([selectedSrcAccNo isEqualToString:selectedDestAccNo]){
        errMsg = lang(@"ERROR_SRCDEST_ACC_ISSAME");
    }
    
    // process
    if ([errMsg isEqualToString:@""]){
        [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":selectedSrcAccNo}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"payment_id":selectedDestAccNo}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"amount":[self stringNominalReplace:_tfAmountTrf.text]}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"txt1":_tfDescTrf.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"txt2":_tfReffNo.text}];
        [super openNextTemplate];
    } else {
        [self showErrorAlert:errMsg];
    }
}

-(void)addAccessoryKeypad{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    _tfDestAcc.inputAccessoryView = keyboardDoneButtonView;
    _tfDestAccNo.inputAccessoryView = keyboardDoneButtonView;
    _tfAmountTrf.inputAccessoryView = keyboardDoneButtonView;
    _tfDescTrf.inputAccessoryView = keyboardDoneButtonView;
    _tfReffNo.inputAccessoryView = keyboardDoneButtonView;
}

- (IBAction)doneClicked:(id)sender {
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat keyboardShowupViewHeight = self.view.superview.frame.size.height-self->keyboardSize.height-(self.vwFooter.frame.size.height+12);
        CGRect f = CGRectMake(0,self.vwContent.frame.origin.y,self.view.frame.size.width,keyboardShowupViewHeight);
        self.vwContent.frame = f;
        self.heightVwFooter.constant = 0;
        [self.vwFooter setHidden:YES];
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat keyboardHiddenViewHeight = self->originHeightContent;//self.view.superview.frame.size.height;
        CGRect f = CGRectMake(0,self.vwContent.frame.origin.y,self.view.frame.size.width,keyboardHiddenViewHeight);
        self.vwContent.frame = f;
        self.heightVwFooter.constant = 80;
        [self.vwFooter setHidden:NO];
    }];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
            if(!isRequested){
                isRequested = YES;
                [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                
            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            if (!isShowing) {
                [alert show];
                isShowing = true;
            }
        }
    }
    
    if([requestType isEqualToString:@"list_bank2"]){
        if(jsonObject && [jsonObject isKindOfClass:[NSArray class]]){
            NSArray *listData = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in listData){
                [newData addObject:@{@"title":@"",@"label":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
            }
            listDataBank = newData;
            [self reloadDataFromRecently];
        } else if (jsonObject){
            if ([jsonObject objectForKey:@"response_code"]) {
                if (![[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
                    NSString *errMsg = [jsonObject objectForKey:@"response"];
                    [self showErrorAlert:errMsg];
                }
            }
        }
    }
    
    if([requestType isEqualToString:@"fav"] || [requestType isEqualToString:@"list_favorite"]){
        if(jsonObject && [jsonObject isKindOfClass:[NSArray class]]){
            listDataFav = [self getListFavoriteFormatData:jsonObject];
            if ([listDataFav count] == 0) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *errMsg = @"";
                if([lang isEqualToString:@"id"]){
                    errMsg = @"Daftar favorit kosong";
                } else {
                    errMsg = @"Favorites list is empty";
                }
                [self setOptionTransferView:0];
                [self showErrorAlert:errMsg];
            }
        } else if (jsonObject){
            if ([requestType isEqualToString:@"action_favorite"]) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                if([jsonObject count] > 0){
                    [userDefault setObject:jsonObject forKey:@"actionFav"];
                }else{
                    [userDefault setObject:[[[dataManager getJsonDataWithMenuID:[data objectForKey:@"menu_id"] andList:dataManager.listMenu]objectAtIndex:1]objectForKey:@"action"]
                                    forKey:@"actionFav"];
                }
                [userDefault synchronize];
            }
            
            if ([jsonObject objectForKey:@"response_code"]) {
                if (![[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
                    NSString *errMsg = [jsonObject objectForKey:@"response"];
                    [self showErrorAlert:errMsg];
                }
            }
        }
    }
}

- (void)errorLoadData:(NSError *)error { 
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert show];
}

- (void)reloadApp { 
    [self backToRoot];
    [BSMDelegate reloadApp];
}

-(void) getListAccountFromPersitance : (NSString *) urlParam strMenuId : (NSString *) menuId {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct = nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
            if ([dataManager.dataExtra objectForKey:@"code"]) {
                if ([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
        }
    }
    
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];
                
            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct != nil) {
        isRequested = true;
        if(listAcct.count == 0){
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki nomor rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account number for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            newData = [self getListAccountFormatData:listAcct];
            mustAddToManager = true;
            key = @"id_account";
            listAction = newData;
            listDataAcc = newData;
        }
    }else{
        if (!isRequested) {
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            
            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
        }else{
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [self reloadDataFromRecently];
}

- (NSMutableArray*)getListFavoriteFormatData:(NSArray*)listFavs{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listFavs){
        NSArray *labelArray = [[temp valueForKey:@"name"] componentsSeparatedByString: @"\n"];
        NSString *labelTitle = @"";
        NSString *labelBank = @"";
        NSString *labelAccNo= @"";
        NSString *labelAccName = @"";
        if(labelArray.count == 4){
            labelTitle = labelArray[0];
            labelBank = labelArray[1];
            labelAccNo = labelArray[2];
            labelAccName = labelArray[3];
        } else {
            labelBank = labelArray[0];
            labelAccNo = labelArray[1];
            labelAccName = labelArray[2];
        }
        NSString *label = [NSString stringWithFormat:@"%@\n%@\n%@",labelBank,labelAccNo,labelAccName];
        [newData addObject:@{@"title":labelTitle,@"label":label,@"params":[temp valueForKey:@"url_parm"],@"id_favorite":[temp valueForKey:@"id_favorite"]}];
    }
    return newData;
}

- (NSMutableArray*)getListAccountFormatData:(NSArray*)listAcct{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listAcct){
        NSString *title = @"";
        NSString *label = @"";
        if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
            if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                title = [NSString stringWithFormat:@"%@",[temp valueForKey:@"altname"]];
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else{
                label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
            }
        }
        else{
            label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
        }
        [newData addObject:@{@"title":title,@"label":label,@"id_account":[temp valueForKey:@"id_account"]}];
    }
    
    return newData;
}

- (void)showErrorAlert:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert show];
}

- (void)showBrowseQR {
    //    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Select File option:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
    //                            @"Choose File",nil];
    //
    //    popup.tag = 1;
    //    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select File option:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Choose File" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        self->imagePicker = [[UIImagePickerController alloc] init];
        self->imagePicker.delegate = self;
        [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        [self presentViewController:self->imagePicker animated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark- Open Image Picker Delegate to select image from Gallery or Camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UploadType=@"Image";
    [arrimg removeAllObjects];
    [arrimg addObject:myImage];
    
    //UIImage *image = [UIImage imageNamed:@"qr3"];
    CIImage *ciimage = [CIImage imageWithData:UIImageJPEGRepresentation(myImage, 1.0f)];
    
    NSDictionary *detectorOptions = @{ CIDetectorAccuracy : CIDetectorAccuracyHigh };
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:detectorOptions];
    
    NSArray *features = [faceDetector featuresInImage:ciimage];
    CIQRCodeFeature *faceFeature;
    NSString *strQR = @"";
    for(faceFeature in features) {
        strQR = [NSString stringWithFormat:@"%@",faceFeature.messageString];
        NSLog(@"Found feature: %@", strQR);
        if (([strQR isEqualToString:@""]) || (strQR == nil)) {
            
        } else {
            [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": strQR}];
        }
        //break;
    }
    if (([strQR isEqualToString:@""]) || (strQR == nil)) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"This is not a valid QR Code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        if (@available(iOS 13.0, *)) {
            [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
        }
        [alert show];
    } else {
        //[super openNextTemplate];
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString *selectedAccount = [userDefault objectForKey:@"valNoRek"];
        if ([strQR isEqualToString:selectedAccount]) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:@"You can't transfer to the same account." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alert show];
        } else {
            [self.tfDestAccNo setText:strQR];
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    NSString *QRCode = nil;
    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode]) {
            
            QRCode = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            break;
        }
    }
    if (QRCode) {
        [session stopRunning];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"qrcode": QRCode}];
        [self.tfDestAccNo setText:QRCode];
        [self closeScanner];
        //[super openNextTemplate];
    }
    
    NSLog(@"QR Code: %@", QRCode);
}

- (void)closeScanner {
    [session stopRunning];
    [viewPreviewQR removeFromSuperview];
    [_vwQRFrame setHidden:YES];
}

- (void)showScanQR{
    self.tabBarController.tabBar.hidden = NO;
    
    [_vwQRFrame setHidden:NO];
    viewPreviewQR = [[UIView alloc]init];
    viewFlash = [[UIView alloc]init];
    [viewFlash setUserInteractionEnabled:YES];
    lblFlash = [[UILabel alloc]init];
    btnFlash = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFlash setImage:[UIImage imageNamed:@"ic_fpop_flashlight"] forState:UIControlStateNormal];
    [viewFlash addSubview:lblFlash];
    [viewFlash addSubview:btnFlash];
    [btnFlash addTarget:self action:@selector(actionFlashOnOff:) forControlEvents:UIControlEventTouchUpInside];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionFlashOnOff:)];
    [viewFlash addGestureRecognizer:singleFingerTap];
    
    stateFlash = false;
    if([Utility isLanguageID]){
        lblFlash.text = @"Senter";
    }else{
        lblFlash.text = @"Flaslight";
    }
    [lblFlash setFont:[UIFont fontWithName:@"Lato-Regular" size:13]];
    [lblFlash setTextColor:[UIColor whiteColor]];
    [lblFlash setTextAlignment:NSTextAlignmentCenter];
    [lblFlash sizeToFit];
    
    CGRect frmVwPreviewQr = viewPreviewQR.frame;
    CGRect frmVwBtnFlash = viewFlash.frame;
    CGRect frmBtnFlash = btnFlash.frame;
    CGRect frmLblBtnFlash = lblFlash.frame;
    
    frmVwBtnFlash.origin.x = 16;
    frmVwBtnFlash.origin.y = _vwQRContent.bounds.size.height - 80;
    frmVwBtnFlash.size.height = 80;
    frmVwBtnFlash.size.width = 80;
    
    frmBtnFlash.size.width = 100;
    frmBtnFlash.size.height = 40;
    
    frmLblBtnFlash.origin.x = 3;
    frmLblBtnFlash.origin.y = frmBtnFlash.origin.y + frmBtnFlash.size.height;
    frmLblBtnFlash.size.width = frmVwBtnFlash.size.width;
    
    frmBtnFlash.origin.x = frmVwBtnFlash.size.width/2 - frmBtnFlash.size.width/2;
    
    frmVwPreviewQr.origin.y = 0;
    frmVwPreviewQr.origin.x = 0;
    frmVwPreviewQr.size.width = _vwQRContent.frame.size.width;
    frmVwPreviewQr.size.height = _vwQRContent.frame.size.height;
    
    viewPreviewQR.frame = frmVwPreviewQr;
    viewFlash.frame = frmVwBtnFlash;
    lblFlash.frame = frmLblBtnFlash;
    btnFlash.frame = frmBtnFlash;
    
    session = [[AVCaptureSession alloc] init];
    device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (input) {
        [session addInput:input];
    } else {
        NSLog(@"Error: %@", error);
    }
    if (error == nil) {
//        self.btnBatalScanQR.hidden = NO;
        output = [[AVCaptureMetadataOutput alloc] init];
        [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [session addOutput:output];
        
        output.metadataObjectTypes = [output availableMetadataObjectTypes];
        
        previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        previewLayer.frame = viewPreviewQR.frame;
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.bounds = viewPreviewQR.bounds;
        [viewPreviewQR.layer addSublayer:previewLayer];
        
        //[[UIApplication sharedApplication].keyWindow addSubview:viewPreviewQR];
        [self.vwQRContent addSubview:viewPreviewQR];
        [self.vwQRContent addSubview:viewFlash];
        [self.view bringSubviewToFront:_vwQRFrame];
        [session startRunning];
        
        [self makeFrameScanner];
        
        CGRect rectBorderArea = [previewLayer metadataOutputRectOfInterestForRect:[Utility converRectOfInterest:viewPreviewQR.bounds]];
        output.rectOfInterest = rectBorderArea;
    }else{
        NSString *strMsg, *strBtn;
        NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
        if ([lang isEqualToString:@"id"]) {
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"Sepertinya pengaturan privasi Anda mencegah kami mengakses kamera Anda untuk melakukan pemindaian barcode. Anda dapat memperbaikinya dengan melakukan hal berikut: \n\nSentuh tombol Buka di bawah untuk membuka aplikasi Pengaturan. \n\nHidupkan Kamera. \n\nBuka aplikasi ini dan coba lagi."];
            strBtn = @"Buka";
        }else{
            strMsg = [NSString stringWithFormat:@"%@, %@", [error localizedDescription], @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following: \n\nTouch the Go button below to open the Settings app.\n\nTurn the Camera on.\n\nOpen this app and try again."];
            strBtn = @"Go";
        }
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:strMsg delegate:self cancelButtonTitle:strBtn otherButtonTitles:nil, nil];
        alert.tag = 1;
        [alert show];
    }
    
}

-(void) makeFrameScanner{
    UILabel *lblTitle = [Utility lblTitleQR];
    
    CGRect bounds = _vwQRContent.layer.bounds;
    CGRect frmLblTitle = lblTitle.frame;
    
    frmLblTitle.origin.x = 16;
    frmLblTitle.origin.y = 32;
    frmLblTitle.size.width = bounds.size.width - (frmLblTitle.origin.x * 2);
    
    lblTitle.frame = frmLblTitle;
    
    [viewPreviewQR.layer addSublayer:[self frameScanner:bounds]];
    [viewPreviewQR addSubview:[self borderScanner:bounds]];
    [viewPreviewQR addSubview:lblTitle];
}


- (CAShapeLayer *) frameScanner : (CGRect ) vwScanner {
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:vwScanner];
    
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = ((vwScanner.size.height - transparantWidth) * 0.5);
    
    [overlayPath appendPath:[UIBezierPath bezierPathWithRect:CGRectMake(transparantX,
                                                                        transparantY,
                                                                        transparantWidth, transparantWidth)]];
    
    overlayPath.usesEvenOddFillRule = true;
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.path = overlayPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [[[UIColor blackColor] colorWithAlphaComponent:0.5]CGColor];
    
    return fillLayer;
}

-(UIView *) borderScanner : (CGRect )vwScanner{
    CGFloat transparantWidth = vwScanner.size.width - 80;
    CGFloat transparantX = (vwScanner.size.width - transparantWidth) *0.5;
    CGFloat transparantY = ((vwScanner.size.height - transparantWidth) * 0.5);
    
    UIView *vwKotakScanner = [[UIView alloc]initWithFrame:CGRectMake(transparantX,
                                                                     transparantY,
                                                                     transparantWidth, transparantWidth)];
    
    CGFloat height = transparantWidth +4;
    CGFloat width = transparantWidth +4;
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake(1, 25)];
    [path addLineToPoint:CGPointMake(1, 1)];
    [path addLineToPoint:CGPointMake(25, 1)];
    
    [path moveToPoint:CGPointMake(width - 30, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 1)];
    [path addLineToPoint:CGPointMake(width - 5, 25)];
    
    [path moveToPoint:CGPointMake(1, height - 30)];
    [path addLineToPoint:CGPointMake(1, height - 5)];
    [path addLineToPoint:CGPointMake(25, height - 5)];
    
    [path moveToPoint:CGPointMake(width - 30, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 5)];
    [path addLineToPoint:CGPointMake(width - 5, height - 30)];
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor = [UIColorFromRGB(const_color_primary) CGColor];
    pathLayer.lineWidth = 3.0f;
    pathLayer.fillColor = nil;
    
    UIView *vwLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, vwKotakScanner.frame.size.width, 1)];
    [vwLine setBackgroundColor:[UIColor yellowColor]];
    
    CGPoint start = CGPointMake(vwKotakScanner.frame.size.width/2,0);
    CGPoint end = CGPointMake(vwKotakScanner.frame.size.width/2, vwKotakScanner.frame.size.height+4);
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.delegate = self;
    animation.fromValue = [NSValue valueWithCGPoint:start];
    animation.toValue = [NSValue valueWithCGPoint:end];
    animation.duration = 5;
    
    animation.repeatCount = HUGE_VALF;
    animation.autoreverses = YES;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    
    [vwKotakScanner.layer addSublayer:pathLayer];
    [vwKotakScanner addSubview:vwLine];
    [vwLine.layer addAnimation:animation forKey:@"position"];
    return vwKotakScanner;
}

-(void)actionFlashOnOff : (UIButton *) sender{
    if (stateFlash) {
        if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] &&
            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOn){
            stateFlash = NO;
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOff];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
            [btnFlash setImage:[UIImage imageNamed:@"ic_fpop_flashlight"] forState:UIControlStateNormal];
            [lblFlash setTextColor:[UIColor whiteColor]];
        }
    }else{
        if ([[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] hasTorch] &&
            [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo].torchMode == AVCaptureTorchModeOff){
            stateFlash = YES;
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] lockForConfiguration:nil];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] setTorchMode:AVCaptureTorchModeOn];
            [[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] unlockForConfiguration];
            UIImage *imageIcon = [[UIImage imageNamed:@"ic_fpop_flashlight"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            UIColor *primaryColor = [UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1];
            [btnFlash setImage:imageIcon forState:UIControlStateNormal];
            [btnFlash setTintColor:primaryColor];
            [lblFlash setTextColor:primaryColor];
        }
    }
}

@end

