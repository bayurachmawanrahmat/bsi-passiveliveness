//
//  TFIB01ViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 15/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TFIB01ViewController : TemplateViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,APIManagerDelegate,UIDocumentPickerDelegate, UIDocumentMenuDelegate>

{
    UIImagePickerController *imagePicker;
    NSMutableArray *arrimg;
    
    NSString * UploadType;
    NSString * ImageName;
}

@end

NS_ASSUME_NONNULL_END
