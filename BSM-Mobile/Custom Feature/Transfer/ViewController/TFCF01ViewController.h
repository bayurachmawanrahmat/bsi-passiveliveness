//
//  TFCF01ViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 15/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TFCF01ViewController : TemplateViewController

@property (weak, nonatomic) IBOutlet UILabel *lblNamaAsal;
@property (weak, nonatomic) IBOutlet UILabel *lblRekAsal;
@property (weak, nonatomic) IBOutlet UILabel *lblNamaTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblRekTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblDari;
@property (weak, nonatomic) IBOutlet UILabel *lblTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblKonfirmasi;
@property (weak, nonatomic) IBOutlet UILabel *lblBankTujuan;
@property (weak, nonatomic) IBOutlet UILabel *valBankTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblMethod;
@property (weak, nonatomic) IBOutlet UILabel *valMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblJumlah;
@property (weak, nonatomic) IBOutlet UILabel *valJumlah;
@property (weak, nonatomic) IBOutlet UILabel *lblKeterangan;
@property (weak, nonatomic) IBOutlet UILabel *valKeterangan;
@property (weak, nonatomic) IBOutlet UILabel *lblReffNo;
@property (weak, nonatomic) IBOutlet UILabel *valReffNo;
@property (weak, nonatomic) IBOutlet UILabel *lblFee;
@property (weak, nonatomic) IBOutlet UILabel *valFee;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@end

NS_ASSUME_NONNULL_END
