//
//  PLD01ViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 22/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PLD01ViewController.h"
#import "PLD01Cell.h"
#import "Styles.h"

@interface PLD01ViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>{
    NSArray *listData, *listFiltered;
    BOOL isSearchActive;
    UIToolbar *toolBar;
    NSString *lblTitleDialog, *tfPlacholder;
    BOOL isHidden;
    CGFloat heightView;
    NSString* fieldName;
}

@property (weak, nonatomic) IBOutlet UIView *bgSearchBox;

@end

@implementation PLD01ViewController

- (IBAction)actionClose:(id)sender {
    [self dissmissList];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // setup view
    _lblTitle.text = lblTitleDialog;
    _vwSearchBox.hidden = isHidden;
    _heightVwContent.constant = heightView ? heightView : 500;
    // border & corner radius
    [_bgSearchBox.layer setCornerRadius:13.0f];
    [_bgSearchBox.layer setBorderWidth:1.0f];
    [_bgSearchBox.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    
    if (@available(iOS 13.0, *)) {
       self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    isSearchActive = NO;
    
    [self.listView registerNib:[UINib nibWithNibName:@"PLD01Cell" bundle:nil]
    forCellReuseIdentifier:@"PLD01CELL"];
    
    [self.tfSearch setDelegate:self];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    self.tfSearch.inputAccessoryView = toolBar;
    
    [self.listView setDelegate:self];
    [self.listView setDataSource:self];
    [self.listView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.listView reloadData];
    
    self.vwContent.layer.cornerRadius = 10;
    self.vwContent.layer.borderWidth = 1;
    self.vwContent.layer.borderColor = [UIColorFromRGB(const_color_gray)CGColor];
    
    
    [_searchImg setUserInteractionEnabled:YES];
    [_searchImg addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionClearSearch)]];
    
    [Styles addShadow:self.view];
    
    _tfSearch.placeholder = tfPlacholder;
    
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (void) actionClearSearch{
    _tfSearch.text = @"";
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    isSearchActive = YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@",@"label",newText];
    NSArray *filterlist = [listData filteredArrayUsingPredicate:predicate];
    
    listFiltered = filterlist;
    if([newText isEqualToString:@""]){
        listFiltered = listData;
    }
    [self.listView reloadData];
    
    return YES;
}

- (void) dissmissList{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isSearchActive){
        [delegate selectedRow:indexPath.row withList:listFiltered fieldName:fieldName];
        [self dissmissList];
    }else{
        [delegate selectedRow:indexPath.row withList:listData fieldName:fieldName];
        [self dissmissList];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PLD01Cell *cell = (PLD01Cell*)[tableView dequeueReusableCellWithIdentifier:@"PLD01CELL"];
    
    if(isSearchActive){
        NSString *titleText = [listFiltered[indexPath.row] objectForKey:@"title"];
        if ([titleText isEqualToString:@""]){
            cell.title.hidden = YES;
        } else {
            cell.title.hidden = NO;
            cell.title.text = titleText;
        }
        cell.label.text = [listFiltered[indexPath.row] objectForKey:@"label"];
    }else{
        NSString *titleText = [listData[indexPath.row] objectForKey:@"title"];
        if ([titleText isEqualToString:@""]){
            cell.title.hidden = YES;
        } else {
            cell.title.hidden = NO;
            cell.title.text = titleText;
        }
        cell.label.text = [listData[indexPath.row] objectForKey:@"label"];
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isSearchActive){
        return listFiltered.count;
    }else{
        return listData.count;
    }
}

- (void) setTitle:(NSString *)title{
    lblTitleDialog = title;
}

- (void)setDelegate:(id)newDelagate{
    delegate = newDelagate;
}

- (void)setList:(NSArray *)list{
    listData = [[NSArray alloc]init];
    listFiltered = [[NSArray alloc]init];
    listData = list;
    listFiltered = list;
    [self.listView reloadData];
}

- (void)setPlaceholder:(NSString *)placeholder{
    tfPlacholder = placeholder;
}

- (void)setHeightViewContent:(CGFloat)height{
    heightView = height;
}

- (void)setHiddenSearchBox:(BOOL)status{
    isHidden = status;
}

- (void)setFieldName:(NSString *)fname{
    fieldName = fname;
}

@end
