//
//  PopupPinTrxViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 22/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PopupPinTrxViewController.h"

@interface PopupPinTrxViewController ()<UITextFieldDelegate>{
    NSString *language;
}

@end

@implementation PopupPinTrxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblPin.text = @"Masukan PIN BSI Mobile";
    } else {
        self.lblPin.text = @"Insert BSI Mobile PIN";
    }
    
    [self.textFieldPin setDelegate:self];
    [self.textFieldPin setSecureTextEntry:true];
    [self.textFieldPin becomeFirstResponder];
    [self.textFieldPin setKeyboardType:UIKeyboardTypeNumberPad];
    [_btnCancel setTitle:[lang(@"CANCEL") uppercaseString] forState:UIControlStateNormal];
    [_btnNext setTitle:@"OK" forState:UIControlStateNormal];
    
    // Styles
    [_textFieldPin.layer setCornerRadius:10.0f];
    [_textFieldPin.layer setBorderWidth:1.0f];
    [_textFieldPin.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    [_btnCancel.layer setCornerRadius:20.0f];
    [_btnCancel.layer setBorderWidth:1.0f];
    [_btnCancel.layer setBorderColor:[UIColor colorWithRed:146/255.0 green:146/255.0 blue:146/255.0 alpha:1].CGColor];
    [_btnNext.layer setCornerRadius:20.0f];
    [_btnNext.layer setBorderWidth:1.0f];
    [_btnNext.layer setBorderColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1].CGColor];
    [_vwBackground.layer setCornerRadius:10.0f];
    [_vwBackground.layer setBorderWidth:1.0f];
    [_vwBackground.layer setBorderColor: (__bridge CGColorRef _Nullable)(UIColor.clearColor)];
    [_vwContent.layer setCornerRadius:10.0f];
    [_vwContent.layer setBorderWidth:1.0f];
    [_vwContent.layer setBorderColor: (__bridge CGColorRef _Nullable)(UIColor.clearColor)];
    
}

- (void) dissmissList{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showErrorAlert:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)actionNext:(id)sender{
    NSString *errMsg = @"";
    if([_textFieldPin.text isEqualToString:@""]){
        if([language isEqualToString:@"id"]){
            errMsg = @"PIN Tidak Boleh Kosaong";
        }else{
            errMsg = @"PIN Cannot Be Empty";
        }
    }
    
    if ([errMsg isEqualToString:@""]){
        [dataManager setPinNumber:_textFieldPin.text];
        [self dismissViewControllerAnimated:YES completion:^{
            [self->delegate pinDoneState:self->menuID];
        }];
    } else {
        [self showErrorAlert:errMsg];
    }
}

- (IBAction)btnEye:(id)sender{
    _btnEye.selected = !_btnEye.selected;
    if ([_btnEye isSelected]) {
        [self.textFieldPin setSecureTextEntry:true];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_hide"] forState:UIControlStateNormal];
    }else{
        [self.textFieldPin setSecureTextEntry:false];
        [self.btnEye setImage:[UIImage imageNamed:@"ic_frm_ksandi_show"] forState:UIControlStateNormal];
    }
}

- (IBAction)actionClose:(id)sender {
    [self dissmissList];
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setIdentfier:(NSString *)newIdentifier{
    identifier = newIdentifier;
}

- (void)setMenuID:(NSString *)newMenuID{
    menuID = newMenuID;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.textFieldPin){
        if (textField.text.length < 6 || string.length == 0){
            return YES;
        } else{
            return NO;
        }
    }
    return YES;
}

@end
