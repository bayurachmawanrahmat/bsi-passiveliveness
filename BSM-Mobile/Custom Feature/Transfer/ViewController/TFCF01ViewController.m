//
//  TFCF01ViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 15/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TFCF01ViewController.h"
#import "PopupPinTrxViewController.h"
#import "Connection.h"
#import <AVFoundation/AVFoundation.h>

@interface TFCF01ViewController ()<ConnectionDelegate, PopupPinTrxViewDelegate, AVCaptureMetadataOutputObjectsDelegate, CAAnimationDelegate>{
    NSArray *listData;
    int lastY;
    UIToolbar *toolbar;
    NSString *menuId;
}

@property (weak, nonatomic) IBOutlet UIView *vwNavBar;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwInfoAccount;
@property (weak, nonatomic) IBOutlet UIView *vwSourceAccount;
@property (weak, nonatomic) IBOutlet UIView *vwDestAccount;
@property (weak, nonatomic) IBOutlet UIView *vwMethodTrf;
@property (weak, nonatomic) IBOutlet UIView *vwReffNo;
@property (weak, nonatomic) IBOutlet UIView *vwFee;
@property (weak, nonatomic) IBOutlet UIView *vwAttention;
@property (weak, nonatomic) IBOutlet UILabel *lblAttention;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *widthSrcLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *widthDestLabel;

@end

@implementation TFCF01ViewController

NSString *menuIdTfBsi = @"00027";
NSString *menuIdTfOtherBank = @"00028";

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    CGFloat widthInfoLabel = 60;
    if([lang isEqualToString:@"en"]){
        widthInfoLabel = 60;
    }
    
    self.lblAttention.text = lang(@"LABEL_ATTENTION");
    self.lblKonfirmasi.text = lang(@"LABEL_CONFIRM");
    self.lblDari.text = lang(@"LABEL_SOURCE");
    self.lblTujuan.text = lang(@"LABEL_BENEFICIARY");
    self.lblBankTujuan.text = lang(@"LABEL_DEST_BANK");
    self.lblMethod.text = lang(@"LABEL_METHOD_TRF");
    self.lblJumlah.text = lang(@"LABEL_AMOUNT");
    self.lblKeterangan.text = lang(@"LABEL_DESC");
    self.lblReffNo.text = lang(@"LABEL_REFF_NO");
    self.lblFee.text = lang(@"LABEL_FEE");
    self.lblInfo.text = lang(@"LABEL_INFO");
    
    self.widthSrcLabel.constant = widthInfoLabel;
    self.widthDestLabel.constant = widthInfoLabel;
    
    // Setup navigationn
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
    _lblTitleNav.text = [self.jsonData valueForKey:@"title"];
    [_vwContent setHidden:true];
    
    // Setup content
    menuId = [self.jsonData valueForKey:@"menu_id"];
    if ([menuId isEqualToString:menuIdTfBsi]){
        [_vwAttention setHidden:YES];
        [_vwMethodTrf setHidden:YES];
        [_vwReffNo setHidden:YES];
        [_vwFee setHidden:YES];
    } else {
        [_vwAttention setHidden:YES];
        [_vwMethodTrf setHidden:NO];
        [_vwReffNo setHidden:NO];
        [_vwFee setHidden:NO];
    }
    
    // border & corner radius view info
    [_vwInfoAccount.layer setCornerRadius:10.0f];
    [_vwInfoAccount.layer setBorderWidth:1.0f];
    [_vwInfoAccount.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    [_vwSourceAccount.layer setCornerRadius:10.0f];
    [_vwDestAccount.layer setCornerRadius:10.0f];
    
    [_btnNext addTarget:self action:@selector(actionBtnTransfer) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)navigationBack{
    dataManager.currentPosition--;
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)actionBtnTransfer{
    PopupPinTrxViewController *dialogPin = [self.storyboard instantiateViewControllerWithIdentifier:@"POPUPPIN"];
    [dialogPin setDelegate:self];
    [dialogPin setMenuID:menuId];
    [dialogPin setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogPin setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogPin animated:YES completion:nil];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        if ([[self.jsonData valueForKey:@"menu_id"] isEqualToString:menuIdTfOtherBank]) {
            urlData =[NSString stringWithFormat:@"%@,type_transfer",[self.jsonData valueForKey:@"url_parm"]];
        }
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)doCheckPin{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_pin,pin,language"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"transfer_inquiry2"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [_vwContent setHidden:false];
            [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject objectForKey:@"transaction_id"]}];
            [[NSUserDefaults standardUserDefaults] setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            
//            [dataManager.dataExtra addEntriesFromDictionary:@{@"bankname":_tfDestAcc.text}];
            
            NSDictionary *data = [jsonObject objectForKey:@"data"];
            self.lblNamaAsal.text = [data objectForKey:@"source_name"];
            self.lblRekAsal.text = [data objectForKey:@"account"];
            self.lblNamaTujuan.text = [data objectForKey:@"beneficiary_name"];
            self.lblRekTujuan.text = [data objectForKey:@"beneficiary_account"];
            self.valBankTujuan.text = [data objectForKey:@"code_name"];
            self.valJumlah.text = [data objectForKey:@"amount"];
            self.valKeterangan.text = [data objectForKey:@"detail"];
            
            if ([menuId isEqualToString:menuIdTfOtherBank]){
                self.vwAttention.hidden = [[dataManager.dataExtra valueForKey:@"type_transfer"] isEqualToString:@"2"] ? NO : YES;
                self.valMethod.text = [[dataManager.dataExtra valueForKey:@"type_transfer"] isEqualToString:@"1"] ? @"ONLINE" : @"SKN";
                self.valReffNo.text = [dataManager.dataExtra valueForKey:@"txt2"];
                self.valFee.text = [data objectForKey:@"charged"];
            }
            [dataManager.dataExtra setValue:[data objectForKey:@"code_name"] forKey:@"beneficiary_bank"];
            [dataManager.dataExtra setValue:[data objectForKey:@"beneficiary_name"] forKey:@"beneficiary_name"];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                [self backToRoot];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
    if ([requestType isEqualToString:@"verify_pin"]) {
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [self openNextTemplate];
        } else {
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

- (void)pinDoneState:(NSString *)menuID{
    [self doCheckPin];
}

@end
