//
//  MTFViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 22/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "MTFViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>
#import "RecentlyHelper.h"
#import "SubmenuCell.h"
#import "RecentlyTransferCell.h"
#import "PLD01ViewController.h"

@interface MTFViewController ()<ConnectionDelegate, UIAlertViewDelegate, UITextFieldDelegate, AVCaptureMetadataOutputObjectsDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, CAAnimationDelegate, UINavigationControllerDelegate> {
    NSArray *listAction;
    NSArray *listData;
    NSArray *arDataRecently;
    NSString* requestType;
    RecentlyHelper *mHelper;
}

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UICollectionView *cvSubmenu;
@property (weak, nonatomic) IBOutlet UILabel *lblLatestTransfer;
@property (weak, nonatomic) IBOutlet UITableView *tblRecentlyTrx;
@property (weak, nonatomic) IBOutlet UIView *vwRecently;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightMenuView;

@end

NSString *menuIdTrfInternal = @"00027";
NSString *menuIdTrfEksternal = @"00028";
NSString *menuIdQris = @"00047";
NSString *menuIdBiFast = @"00187";

@implementation MTFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    arDataRecently = [[NSArray alloc]init];
    mHelper = [[RecentlyHelper alloc]init];
    
    self.titleBar.text = [self.jsonData valueForKey:@"title"];
    [_cvSubmenu setDataSource:self];
    [_cvSubmenu setDelegate:self];
    [_cvSubmenu registerNib:[UINib nibWithNibName:@"SubmenuCell" bundle:nil] forCellWithReuseIdentifier:@"SubmenuCell"];
    
    NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
    if([lang isEqualToString:@"id"]){
        self.lblLatestTransfer.text = @"Transfer Terbaru";
    } else {
        self.lblLatestTransfer.text = @"Latest Transfer";
    }
    listAction = [self.jsonData objectForKey:@"action"];
    if(listAction.count > 3){
        _heightMenuView.constant = ((listAction.count / 3)+1)*110;
    } else {
        _heightMenuView.constant = 130;
    }
    
    [self.vwRecently setHidden:YES];
    [self showingRecentlyTransfer];
}

- (void)viewDidAppear:(BOOL)animated{
    [dataManager.dataExtra removeObjectForKey:@"from_recently"];
}

-(void) showingRecentlyTransfer {
    arDataRecently = [mHelper readListRecentlyTransfer:menuIdTrfInternal menuId2:menuIdTrfEksternal];
    if (arDataRecently.count > 0) {
        [self.vwRecently setHidden:NO];
        [_tblRecentlyTrx setDataSource:self];
        [_tblRecentlyTrx setDelegate:self];
        [_tblRecentlyTrx registerNib:[UINib nibWithNibName:@"RecentlyTransferCell" bundle:nil] forCellReuseIdentifier:@"RecentlyTransferCell"];
        [self.tblRecentlyTrx setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
        //        [self.tblRecentlyTrx beginUpdates];
        //        [self.tblRecentlyTrx endUpdates];
    }
}

#pragma UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return listAction.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SubmenuCell *cell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"SubmenuCell" forIndexPath:indexPath];
    NSDictionary *selectedMenu = [[listAction objectAtIndex:indexPath.row] objectAtIndex:1];
    NSString *menuId = [[listAction objectAtIndex:indexPath.row] objectAtIndex:0];
    NSString *titleSubmenu = [selectedMenu objectForKey:@"title"];
    if ([menuId isEqualToString:menuIdTrfInternal]) {
        [cell.smImage setImage:[UIImage imageNamed:@"ic_transferlist_bsi"]];
    } else if ([menuId isEqualToString:menuIdTrfEksternal]) {
        [cell.smImage setImage:[UIImage imageNamed:@"ic_transferlist_banklain"]];
    } else if ([menuId isEqualToString:menuIdQris]) {
        [cell.smImage setImage:[UIImage imageNamed:@"ic_transferlist_qris"]];
    } else if ([menuId isEqualToString:menuIdBiFast]) {
        [cell.smImage setImage:[UIImage imageNamed:@"ic_menu_bifast"]];
    } else {
        [cell.smImage setImage:[UIImage imageNamed:@"ic_transferlist_cardless"]];
    }
    cell.smLabel.text = titleSubmenu;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(collectionView.frame), (CGRectGetHeight(collectionView.frame)));
}

#pragma UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
    if([[[selectedMenu objectAtIndex:1]objectForKey:@"action"] isKindOfClass:[NSArray class]]){
        @try {
            NSDictionary *object = [selectedMenu objectAtIndex:1];
            NSString *template = [object objectForKey:@"template"];
            TemplateViewController *tmpVC = [self routeTemplateController:template];
            [tmpVC setJsonData:object];
            [self.navigationController pushViewController:tmpVC animated:YES];
        } @catch (NSException *exception) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:@"Template not Found" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }

    }else{
        if([[selectedMenu objectAtIndex:0] isEqualToString:@"00047"]){ // if QRIS clicked
//            NSMutableArray *listFeatureQRIS = [[NSMutableArray alloc]init];
//            [listFeatureQRIS addObject:@{@"key":@"1",@"title":@"",@"label":@"Scan QRIS"}];
//            [listFeatureQRIS addObject:@{@"key":@"2",@"title":@"",@"label":@"QRIS CPM"}];
//            PLD01ViewController *dialogList = [self.storyboard instantiateViewControllerWithIdentifier:@"PLD01VC"];
//            [dialogList setDelegate:self];
//            [dialogList setTitle:@"QRIS"];
//            [dialogList setHiddenSearchBox:YES];
//            [dialogList setHeightViewContent:78+(44*listFeatureQRIS.count)];
//            [dialogList setFieldName:@"fieldFeatureQRIS"];
//            [dialogList setList:listFeatureQRIS];
//            [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
//            [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//            [self presentViewController:dialogList animated:YES completion:nil];
            [self goToQrPayment];
        } else {
            [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
            [super openNextTemplate];
        }
    }
}

- (void)goToQrPayment{
    TemplateViewController *templateViews = [self routeTemplateController:@"TABQRIS"];
    [self.navigationController pushViewController:templateViews animated:YES];
}

#pragma PLD01ViewController
- (void)selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData fieldName:(NSString *)fname{
    if ([fname isEqualToString:@"fieldFeatureQRIS"]) {
        NSString *key = [[arrData objectAtIndex:indexRow] objectForKey:@"key"];
        NSString *menuIdMutasi = [key isEqualToString:@"1"] ? @"00047" : @"00198";
        NSArray *selectedMenu = [dataManager getJsonDataWithMenuID:menuIdMutasi andList:dataManager.listMenu];
        [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
        [super openNextTemplate];
    }
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arDataRecently.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RecentlyTransferCell*cell = (RecentlyTransferCell *)[tableView dequeueReusableCellWithIdentifier:@"RecentlyTransferCell" forIndexPath:indexPath];
    
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    NSArray *codeItems = [[data valueForKey:F_CODE] componentsSeparatedByString:@"|"];
    NSString *nBankName = @"";
    if (codeItems.count > 1 && ![[codeItems objectAtIndex:1] isEqualToString:@""]) {
        [cell.lblBankName setHidden:NO];
        nBankName = [codeItems objectAtIndex:1];
    } else {
        [cell.lblBankName setHidden:YES];
    }
    
    cell.lblInitial.text = [[data valueForKey:F_ACCT_TRSCT_NAME] substringWithRange:NSMakeRange(0, 1)];
    cell.lblAccName.text = [data valueForKey:F_ACCT_TRSCT_NAME];
    cell.lblBankName.text = [nBankName isEqualToString:@"BSI"] ? @"Bank Syariah Indonesia" : nBankName;
    cell.lblAccNo.text = [data valueForKey:F_ACCT_REK];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *data = [arDataRecently objectAtIndex:indexPath.row];
    NSArray *codeItems = [[data valueForKey:F_CODE] componentsSeparatedByString:@"|"];
    NSArray *selectedMenu = [listAction objectAtIndex:[[data objectForKey:F_MENU_ID] isEqualToString:menuIdTrfInternal] ? 0 : 1];
    [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"code":[codeItems objectAtIndex:0]}];
    [dataManager.dataExtra addEntriesFromDictionary:@{@"payment_id":[data valueForKey:F_ACCT_REK]}];
    if (codeItems.count > 1){
        [dataManager.dataExtra addEntriesFromDictionary:@{@"destbank_name":[codeItems objectAtIndex:1]}];
    }
    [dataManager.dataExtra setValue:@"1" forKey:@"from_recently"];
    [super openNextTemplate];
}

@end
