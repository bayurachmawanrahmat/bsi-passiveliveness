//
//  PLD01ViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 22/06/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PLD01ViewController : UIViewController{
    id delegate;
}
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *searchImg;
@property (weak, nonatomic) IBOutlet UITableView *listView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightVwContent;
@property (weak, nonatomic) IBOutlet UIView *vwSearchBox;

- (void) setDelegate:(id)newDelagate;
- (void) setTitle:(NSString *)title;
- (void) setList:(NSArray*)list;
- (void) setPlaceholder:(NSString*)placeholder;
- (void) setHeightViewContent:(CGFloat)height;
- (void) setHiddenSearchBox:(BOOL)status;
- (void) setFieldName:(NSString*)fname;
    
@end

@protocol PLD01ViewDelegate

@required

- (void) selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData fieldName:(NSString*)fname;

@end

NS_ASSUME_NONNULL_END
