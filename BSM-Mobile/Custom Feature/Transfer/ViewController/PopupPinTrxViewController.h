//
//  PopupPinTrxViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 22/07/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupPinTrxViewController : TemplateViewController{
    id delegate;
    NSString *identifier;
    NSString *menuID;
}
@property (weak, nonatomic) IBOutlet UIView *vwBackground;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *lblPin;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPin;
@property (weak, nonatomic) IBOutlet CustomBtn *btnCancel;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnEye;

- (void) setDelegate:(id)newDelagate;
- (void) setIdentfier : (NSString*) newIdentifier;
- (void) setMenuID : (NSString*) newMenuID;

@end

@protocol PopupPinTrxViewDelegate

@required

- (void) pinDoneState : (NSString*) identifier;

@end

NS_ASSUME_NONNULL_END
