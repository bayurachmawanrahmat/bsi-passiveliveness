//
//  MMFPanelViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 07/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MMFPanelViewController : ViewController

- (void) setDelegate:(id)newDelagate;

@end

@protocol MMFPanelViewDelegate

@required

- (void) panelCloseState : (NSString*) state;

@end

NS_ASSUME_NONNULL_END
