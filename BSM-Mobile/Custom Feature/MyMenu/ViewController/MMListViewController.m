//
//  MMListViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 06/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "MMListViewController.h"
#import "MMSortingViewController.h"
#import "MMListCell.h"
#import "MymenuHelper.h"
#import "MMFPanelViewController.h"

static NSString * const mmCell = @"MMListCell";

@interface MMListViewController ()<ConnectionDelegate, MMFPanelViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource> {
    NSArray *arrMenuData;
    MymenuHelper *mHelper;
    UIToolbar *toolBar;
    BOOL isSearchActive;
    NSArray *listData, *listFiltered;
    NSMutableArray *listSortOpt;
    NSInteger selectedSortRow;
}

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwEmptyData;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UITableView *tblMymenu;
@property (weak, nonatomic) IBOutlet UILabel *titleSub;
@property (weak, nonatomic) IBOutlet UILabel *titleEmptyData;
@property (weak, nonatomic) IBOutlet UILabel *descEmptyData;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnEditMenu;

@end

@implementation MMListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.titleBar.text = lang(@"MYMENU_TITLE");
    self.titleSub.text = lang(@"MYMENU_TITLESUB");
    self.titleEmptyData.text = lang(@"MYMENU_EMPTY_TITLE");
    self.descEmptyData.text = lang(@"MYMENU_EMPTY_DESC");
    self.tfSearch.placeholder = lang(@"MYMENU_SEARCH");
    [_btnAddMenu setTitle:lang(@"MYMENU_BTNADD") forState:UIControlStateNormal];
    [_btnEditMenu setTitle:lang(@"EDIT") forState:UIControlStateNormal];
    
    arrMenuData = [[NSArray alloc]init];
    mHelper = [[MymenuHelper alloc]init];

    [_vwContent setHidden:YES];
    [_vwEmptyData setHidden:YES];
    [_tblMymenu setDataSource:self];
    [_tblMymenu setDelegate:self];
    [_tblMymenu registerNib:[UINib nibWithNibName:mmCell bundle:nil] forCellReuseIdentifier:mmCell];
    [_tblMymenu setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    // Setup search textbox
    [self.tfSearch setDelegate:self];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    self.tfSearch.inputAccessoryView = toolBar;
    
    // Setup data sorting
    listSortOpt = [[NSMutableArray alloc]init];
    [listSortOpt addObject:@{@"key":@"1",@"title":@"",@"label":lang(@"MYMENU_FILTER_OPT1")}];
    [listSortOpt addObject:@{@"key":@"2",@"title":@"",@"label":lang(@"MYMENU_FILTER_OPT2")}];
    selectedSortRow = 0;

}

- (void)viewDidAppear:(BOOL)animated{
    [self showingMymenuList];
}

-(void) showingMymenuList {
    listData = [self sortingArray:[mHelper readListMymenu] opt:selectedSortRow key:@"feature"];
    if (listData.count > 0) {
        [_vwContent setHidden:NO];
        [_vwEmptyData setHidden:YES];
    } else {
        [_vwContent setHidden:YES];
        [_vwEmptyData setHidden:NO];
    }
    [_tblMymenu reloadData];
}

- (IBAction)modifyClicked:(id)sender {
    MMFPanelViewController *panel = [self.storyboard instantiateViewControllerWithIdentifier:@"MMFPanel"];
    [panel setDelegate:self];
    [panel setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [panel setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentViewController:panel animated:YES completion:nil];
}

- (IBAction)sortingClicked:(id)sender {
    MMSortingViewController *dialogList = [self.storyboard instantiateViewControllerWithIdentifier:@"MMSortingVC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:lang(@"MYMENU_FILTER")];
    [dialogList setHiddenSearchBox:YES];
    [dialogList setHeightViewContent:78+(44*listSortOpt.count)];
    [dialogList setFieldName:@"fieldSortBy"];
    [dialogList setList:listSortOpt];
    [dialogList setSelectedRow:selectedSortRow];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
}
    
- (void) openFeature : (NSString*) identifier{
    [self selectedFeature:[identifier intValue]];
}
    
- (void) openStore{
    [self openAppStore];
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData fieldName:(NSString *)fname{
    selectedSortRow = indexRow;
    if(isSearchActive){
        listFiltered = [self sortingArray:listFiltered opt:selectedSortRow key:@"feature"];
    } else {
        listData = [self sortingArray:listData opt:selectedSortRow key:@"feature"];
    }
    [_tblMymenu reloadData];
}

- (NSMutableArray*) sortingArray:(NSArray*)arrData opt:(NSInteger) optSort key:(NSString*)keyString{
    BOOL isAsc = optSort == 0 ? YES : NO;
    NSMutableArray *listSorted = [arrData mutableCopy];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:keyString ascending:isAsc];
    [listSorted sortUsingDescriptors:@[sortDescriptor]];
    return listSorted;
}

#pragma TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    isSearchActive = YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@",@"feature",newText];
    NSArray *filterlist = [listData filteredArrayUsingPredicate:predicate];
    
    listFiltered = filterlist;
    if([newText isEqualToString:@""]){
        listFiltered = [self sortingArray:listData opt:selectedSortRow key:@"feature"];
    }
    
    [self.tblMymenu reloadData];
    
    return YES;
}

#pragma MMFPanelViewDelegate
- (void)panelCloseState:(NSString *)state{
    [self showingMymenuList];
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isSearchActive){
        return listFiltered.count;
    }else{
        return listData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MMListCell*cell = (MMListCell *)[tableView dequeueReusableCellWithIdentifier:mmCell forIndexPath:indexPath];
    
    if(isSearchActive){
        arrMenuData = listFiltered[indexPath.row];
    } else {
        arrMenuData = listData[indexPath.row];
    }
    
    NSString *nameMenu = [arrMenuData valueForKey:@"feature"];
    cell.initialMenu.text = [nameMenu substringToIndex:1];
    cell.nameMenu.text = nameMenu;
    [cell.vwBtnModify setHidden:YES];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(isSearchActive){
        NSString *dataString = [self->listFiltered[indexPath.row] valueForKey:@"data"];
        NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        
        if([[dataDict objectForKey:@"launcher"] isEqualToString:@"-1"]){
            [self openFeature:[dataDict objectForKey:@"menuid"]];
        }else if([[dataDict objectForKey:@"launcher"] isEqualToString:@""]){
            [self openStore];
        }else{
            if([[dataDict objectForKey:@"servicecode"] isEqualToString:@""]){
                [self openNodes:[dataDict objectForKey:@"launcher"]];
            }else{
                NSString *nodes = [NSString stringWithFormat:@"%@/code=%@",[dataDict objectForKey:@"launcher"],[dataDict objectForKey:@"servicecode"]];
                [self openNodes:nodes];
            }
        }
    } else {
        NSString *dataString = [self->listData[indexPath.row] valueForKey:@"data"];
        NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
        
        // start trapping for Transfer feature
        if([[dataDict objectForKey:@"feature"] containsString:@"Transfer"]){
            if([[dataDict objectForKey:@"feature"] containsString:@"Online"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"type_transfer":@"1"}];
            } else if([[dataDict objectForKey:@"feature"] containsString:@"SKN"]){
                [dataManager.dataExtra addEntriesFromDictionary:@{@"type_transfer":@"2"}];
            }
        }
        // end trapping for Transfer feature
        
        if([[dataDict objectForKey:@"launcher"] isEqualToString:@"-1"]){
            [self openFeature:[dataDict objectForKey:@"menuid"]];
        }else if([[dataDict objectForKey:@"launcher"] isEqualToString:@""]){
            [self openStore];
        }else{
            if([[dataDict objectForKey:@"servicecode"] isEqualToString:@""]){
                [self openNodes:[dataDict objectForKey:@"launcher"]];
            }else{
                NSString *nodes = [NSString stringWithFormat:@"%@/=%@",[dataDict objectForKey:@"launcher"],[dataDict objectForKey:@"servicecode"]];
                [self openNodes:nodes];
            }
        }
    }
}

@end
