//
//  MMSettingViewController.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MMSettingViewController : TemplateViewController

@end

NS_ASSUME_NONNULL_END
