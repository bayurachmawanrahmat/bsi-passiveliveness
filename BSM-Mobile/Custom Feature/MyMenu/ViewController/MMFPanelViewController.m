//
//  MMFPanelViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 07/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "MMFPanelViewController.h"
#import "MMSortingViewController.h"
#import "MMListCell.h"
#import "MymenuHelper.h"

static NSString * const mmCell = @"MMListCell";
static NSMutableArray * listData;
static NSArray * listFiltered;

@interface MMFPanelViewController()<ConnectionDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource> {
    id delegate;
    MymenuHelper *mymenuHelper;
    NSString *state;
    NSArray *arrMenuData;
    NSArray *arrMenuDataLocal;
    NSMutableDictionary *arrMenuTemp;
    UIToolbar *toolBar;
    BOOL isSearchActive;
    NSArray *arrMenuSorted;
//    NSArray *arrMenuT;
//    NSArray *listData, *listFiltered;
    NSMutableArray *listSortOpt;
    NSInteger *selectedSortRow;
}

@property (weak, nonatomic) IBOutlet UIView *vwBackground;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UITextField *tfSearch;
@property (weak, nonatomic) IBOutlet UITableView *tblMymenu;
@property (weak, nonatomic) IBOutlet UILabel *titleSub;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveMenu;

@end

@implementation MMFPanelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleBar.text = lang(@"MYMENU_TITLE");
    self.titleSub.text = lang(@"MYMENU_TITLESUB");
    self.tfSearch.placeholder = lang(@"MYMENU_SEARCH");
    [_btnSaveMenu setTitle:lang(@"SAVE") forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view.
    mymenuHelper = [[MymenuHelper alloc]init];
    arrMenuTemp = [[NSMutableDictionary alloc]init];
    
    [_vwContent setHidden:YES];
    [_tblMymenu setDataSource:self];
    [_tblMymenu setDelegate:self];
    [_tblMymenu registerNib:[UINib nibWithNibName:mmCell bundle:nil] forCellReuseIdentifier:mmCell];
    [_tblMymenu setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    // Setup search textbox
    [self.tfSearch setDelegate:self];
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                      [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneClicked:)]];
    self.tfSearch.inputAccessoryView = toolBar;
    
    [self reloadLocalData];
    [self doRequest];
    
    // Setup data sorting
    listSortOpt = [[NSMutableArray alloc]init];
    [listSortOpt addObject:@{@"key":@"1",@"title":@"",@"label":lang(@"MYMENU_FILTER_OPT1")}];
    [listSortOpt addObject:@{@"key":@"2",@"title":@"",@"label":lang(@"MYMENU_FILTER_OPT2")}];
    [listSortOpt addObject:@{@"key":@"3",@"title":@"",@"label":lang(@"MYMENU_FILTER_OPT3")}];
    [listSortOpt addObject:@{@"key":@"4",@"title":@"",@"label":lang(@"MYMENU_FILTER_OPT4")}];
    
    if(listData.count > 0){
        selectedSortRow = 0;
        listData = [self generateListMyMenuWithFlag:listData];
    }
    
}

- (void)viewDidAppear:(BOOL)animated{
    [self animatedBackground];
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

-(void)reloadLocalData {
    arrMenuDataLocal = [mymenuHelper readListMymenu];
    if(arrMenuDataLocal.count > 0){
        for (NSMutableDictionary *dict in arrMenuDataLocal) {
            NSString *dataString = [dict valueForKey:@"data"];
            NSArray *dataArr = [NSJSONSerialization JSONObjectWithData:[dataString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            [arrMenuTemp setObject:dataArr forKey:[NSString stringWithFormat:@"%@",[dataArr valueForKey:@"order"]]];
        }
    }
    [_tblMymenu setHidden:NO];
    [_tblMymenu reloadData];
}

-(void)animatedBackground {
    [UIView animateWithDuration:0.2 animations:^(void) {
        self->_vwBackground.alpha = 1;
    }];
}

-(void)dissmissPanel {
    _vwBackground.alpha = 0;
    [self dismissViewControllerAnimated:YES completion:^{
        [self->delegate panelCloseState:@"done"];
    }];
}

-(void) doneClicked:(id)sender{
    [self.view endEditing:YES];
}

- (IBAction)sortingClicked:(id)sender {
    MMSortingViewController *dialogList = [self.storyboard instantiateViewControllerWithIdentifier:@"MMSortingVC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:lang(@"MYMENU_FILTER")];
    [dialogList setHiddenSearchBox:YES];
    [dialogList setHeightViewContent:78+(44*listSortOpt.count)];
    [dialogList setFieldName:@"fieldSortBy"];
    [dialogList setList:listSortOpt];
    [dialogList setSelectedRow:selectedSortRow];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (IBAction)closeClicked:(id)sender {
    [self dissmissPanel];
}

- (IBAction)saveClicked:(id)sender {
    [mymenuHelper dropTblMymenu];
    [mymenuHelper createTblMymenu];
    
    BOOL iStatus = YES;
    for(id key in arrMenuTemp){
        NSMutableDictionary *dictList = [[NSMutableDictionary alloc]init];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[arrMenuTemp objectForKey:key] options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [dictList setValue:[[arrMenuTemp objectForKey:key] objectForKey:@"id_feature"] forKey:@"id_feature"];
        [dictList setValue:[[arrMenuTemp objectForKey:key] objectForKey:@"feature"] forKey:@"feature"];
        [dictList setValue:jsonString forKey:@"data"];
        iStatus += [mymenuHelper insertDataMymenu:dictList];
    }
    
    if(iStatus){
        [self dissmissPanel];
    }
}

- (void)doRequest{
    if(listData.count == 0){
        NSString *strUrl = [NSString stringWithFormat:@"request_type=list_menuku"];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:nil];
    } else {
        [_vwContent setHidden:NO];
    }
}

- (void)goToModifyList:(UIButton *) sender {
    NSString *setBtnTitle = @"";
    UIColor *setBgBtnTitle = [[UIColor alloc]init];
    NSArray *lData = isSearchActive ? listFiltered[sender.tag] : listData[sender.tag];
    NSString *key = [NSString stringWithFormat:@"%@", [lData valueForKey:@"order"]];
    
    if([sender.currentTitle isEqualToString:@"+"]){
        [arrMenuTemp setObject:lData forKey:key];
        setBtnTitle = @"-";
        setBgBtnTitle = [UIColor colorWithRed:252/255.0 green:95/255.0 blue:73/255.0 alpha:1];
    } else {
        [arrMenuTemp removeObjectForKey:key];
        setBtnTitle = @"+";
        setBgBtnTitle = [UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1];
    }
    [sender setTitle:setBtnTitle forState:UIControlStateNormal];
    [sender setBackgroundColor:setBgBtnTitle];
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData fieldName:(NSString *)fname{
    selectedSortRow = indexRow;
    if(isSearchActive){
        if(indexRow == 0 || indexRow == 1){
            listFiltered = [self sortingArray:listFiltered opt:indexRow key:@"feature"];
        } else {
            listFiltered = [self sortingArray:listFiltered opt:(indexRow == 3 ? 0 : 1) key:@"favorited"];
        }
    } else {
        if(indexRow == 0 || indexRow == 1){
            listData = [self sortingArray:listData opt:indexRow key:@"feature"];
        } else {
            listData = [self sortingArray:listData opt:(indexRow == 3 ? 0 : 1) key:@"favorited"];
        }
    }
    
    [_tblMymenu reloadData];
}

- (NSMutableArray*) sortingArray:(NSArray*)arrData opt:(NSInteger) optSort key:(NSString*)keyString{
    BOOL isAsc = optSort == 0 ? YES : NO;
    NSMutableArray *listSorted = [arrData mutableCopy];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:keyString ascending:isAsc];
    [listSorted sortUsingDescriptors:@[sortDescriptor]];
    return listSorted;
}

#pragma Connection
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"list_menuku"]){
        if([[jsonObject valueForKey:@"responsecode"]isEqualToString:@"00"]){
            [_vwContent setHidden:NO];
            listData = [self generateListMyMenuWithFlag:[jsonObject objectForKey:@"info"]];
            [_tblMymenu reloadData];
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"responsemsg"]]  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                [self dissmissPanel];
            }]];
        }
    }
}

- (NSMutableArray*)generateListMyMenuWithFlag:(NSMutableArray *)listObject{
    NSMutableArray *arrMyMenu = [[NSMutableArray alloc]init];
    for(NSMutableDictionary *data in listObject){
        NSString *featureId = [data valueForKey:@"id_feature"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@",@"id_feature",featureId];
        NSArray *filterdata = [arrMenuDataLocal filteredArrayUsingPredicate:predicate];
        NSString *favorited = @"no";
        if([featureId isEqualToString:[filterdata.firstObject valueForKey:@"id_feature"]]){
            favorited = @"yes";
        }
        NSMutableDictionary *menu = [data mutableCopy];
        [menu setObject:favorited forKey:@"favorited"];
        [arrMyMenu addObject:menu];
    }
    return [self sortingArray:arrMyMenu opt:0 key:@"feature"];
}

#pragma TextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    isSearchActive = YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K CONTAINS[c] %@",@"feature",newText];
    NSArray *filterlist = [listData filteredArrayUsingPredicate:predicate];
    
    listFiltered = filterlist;
    if([newText isEqualToString:@""]){
        listFiltered = [self sortingArray:listData opt:selectedSortRow key:@"feature"];
    }
    
    [self.tblMymenu reloadData];
    
    return YES;
}

#pragma UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(isSearchActive){
        return listFiltered.count;
    }else{
        return listData.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MMListCell *cell = (MMListCell *)[tableView dequeueReusableCellWithIdentifier:mmCell forIndexPath:indexPath];
    if(isSearchActive){
        arrMenuData = listFiltered[indexPath.row];
    } else {
        arrMenuData = listData[indexPath.row];
    }
    
    NSString *nameMenu = [arrMenuData valueForKey:@"feature"];
    NSString *favorited = [arrMenuData valueForKey:@"favorited"];
    NSString *orderId = [arrMenuData valueForKey:@"order"];
    cell.initialMenu.text = [nameMenu substringToIndex:1];
    cell.nameMenu.text = nameMenu;
    [cell.btnModify setTag:indexPath.row];
    [cell.btnModify setAccessibilityIdentifier:orderId];
    [cell.btnModify addTarget:self action:@selector(goToModifyList:) forControlEvents:UIControlEventTouchUpInside];
    if([favorited isEqualToString:@"yes"]){
        [cell.btnModify setTitle:@"-" forState:UIControlStateNormal];
        [cell.btnModify setBackgroundColor:[UIColor colorWithRed:252/255.0 green:95/255.0 blue:73/255.0 alpha:1]];
    } else {
        [cell.btnModify setTitle:@"+" forState:UIControlStateNormal];
        [cell.btnModify setBackgroundColor:[UIColor colorWithRed:0/255.0 green:163/255.0 blue:157/255.0 alpha:1]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

@end
