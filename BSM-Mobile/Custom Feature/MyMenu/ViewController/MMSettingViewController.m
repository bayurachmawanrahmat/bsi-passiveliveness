//
//  MMSettingViewController.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "MMSettingViewController.h"

@interface MMSettingViewController ()

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UILabel *lblActivate;
@property (weak, nonatomic) IBOutlet UISwitch *switchActivate;

@end

@implementation MMSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.titleBar.text = lang(@"MYMENU_TITLE_SETTING");
    self.lblActivate.text = lang(@"MYMENU_SETTING_LBLACT");
    [self.switchActivate addTarget:self action:@selector(actionSwitchActivate:) forControlEvents:UIControlEventValueChanged];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mmSetting = [userDefault objectForKey:@"showMyMenu"];
    if([mmSetting isEqualToString:@"YES"]){
        [_switchActivate setOn:YES];
    } else {
        [_switchActivate setOn:NO];
    }
}

- (void)actionSwitchActivate:(UISwitch *)twistedSwitch {
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([twistedSwitch isOn] && (![twistedSwitch isSelected])){
        [userDefault setObject:@"YES" forKey:@"showMyMenu"];
    } else {
        [userDefault setObject:@"NO" forKey:@"showMyMenu"];
    }
    [userDefault synchronize];
}

@end
