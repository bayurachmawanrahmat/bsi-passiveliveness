//
//  MMListCell.h
//  BSM-Mobile
//
//  Created by Angger Binuko on 07/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MMListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *initialMenu;
@property (weak, nonatomic) IBOutlet UILabel *nameMenu;
@property (weak, nonatomic) IBOutlet UIView *vwBtnModify;
@property (weak, nonatomic) IBOutlet UIButton *btnModify;


@end

NS_ASSUME_NONNULL_END
