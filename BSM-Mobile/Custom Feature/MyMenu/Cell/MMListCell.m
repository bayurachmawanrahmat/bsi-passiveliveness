//
//  MMListCell.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 07/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "MMListCell.h"

@implementation MMListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
