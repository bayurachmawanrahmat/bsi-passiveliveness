//
//  SortingCell.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 13/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "SortingCell.h"

@implementation SortingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
