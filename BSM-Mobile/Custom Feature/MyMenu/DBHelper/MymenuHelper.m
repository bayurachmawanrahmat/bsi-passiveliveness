//
//  MymenuHelper.m
//  BSM-Mobile
//
//  Created by Angger Binuko on 07/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "MymenuHelper.h"
#import "DBHelper.h"
#import "AESCipher.h"
#import <fmdb/FMResultSet.h>
#import <fmdb/FMDatabase.h>
#import <fmdb/FMDatabaseQueue.h>
#import "NSUserdefaultsAes.h"


static NSString * const kTblMymenu = @"tbl_mymenu";

@interface MymenuHelper()
@property (nonatomic) FMDatabase *db;
@property (nonatomic) DBHelper *dbHelper;
@property (nonatomic) AESCipher *aesChiper;
@property (nonatomic) NSUserDefaults *userDefault;
@end

@implementation MymenuHelper

-(void) openConn{
    self.dbHelper = [[DBHelper alloc] init];
    self.aesChiper = [[AESCipher alloc] init];
    self.db = self.dbHelper.connection;
    self.userDefault = [NSUserDefaults standardUserDefaults];
}

-(void)dropTblMymenu{
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"DROP TABLE IF EXISTS %@", kTblMymenu];
        [self.db executeUpdate:query];
        [self.db close];
    }
}

-(void) createTblMymenu{
    [self openConn];
    if (self.db != nil) {
        NSString *query  = [NSString stringWithFormat:@""
                           @"CREATE TABLE IF NOT EXISTS %@ "
                           @"( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                           @"id_feature TEXT UNIQUE, "
                           @"feature TEXT, "
                           @"data TEXT )", kTblMymenu];
        [self.db executeUpdate:query];
        [self.db close];
    }
}

-(NSArray *) readListMymenu {
    NSArray *dataList = [[NSArray alloc]init];
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"SELECT * FROM %@ "
                           @"LIMIT 20 ", kTblMymenu];
        FMResultSet *rs = [self.db executeQuery:query];
        NSMutableArray *dataMutArr = [[NSMutableArray alloc] init];
        while ([rs next]) {
            NSMutableDictionary *dataDict = [[NSMutableDictionary alloc]init];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"id_feature"]] forKey:@"id_feature"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"feature"]] forKey:@"feature"];
            [dataDict setValue:[_aesChiper aesDecryptString:[rs stringForColumn:@"data"]] forKey:@"data"];
            [dataMutArr addObject:dataDict];
        }
        dataList = (NSArray *) dataMutArr;
        [self.db closeOpenResultSets];
    }
    return dataList;
}

-(BOOL)insertDataMymenu : (NSDictionary *) contentValue{
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"INSERT OR REPLACE INTO %@ "
                           @"(id_feature, feature, data ) "
                           @"VALUES (?,?,?)", kTblMymenu];
        NSDictionary *dictEncrypt = [_aesChiper dictAesEncryptString:contentValue];
        NSLog(@"enkripsi: %@", dictEncrypt);
        bool result = [self.db executeUpdate:query,
                       [dictEncrypt valueForKey:@"id_feature"],
                       [dictEncrypt valueForKey:@"feature"],
                       [dictEncrypt valueForKey:@"data"]];
        
        if (result) {
            NSLog(@"%d",(int) [self.db lastInsertRowId]);
            return YES;
        }else{
            NSLog(@"Gagal Insert");
            return NO;
        }
        [self.db close];
        
    }
    return NO;
}

-(BOOL) deleteDataMymenu :(NSString *) featureId{
    BOOL state = false;
    [self openConn];
    if (self.db != nil) {
        NSString *query = [NSString stringWithFormat:@""
                           @"DELETE FROM %@ "
                           @"WHERE id_feature = ? ", kTblMymenu];
        state = [self.db executeUpdate:query, [_aesChiper aesEncryptString:featureId]];
        [self.db close];
    }
    return state;
}

@end
