//
//  MymenuHelper.h
//  BSM-Mobile
//
//  Created by  Angger Binuko on 07/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MymenuHelper : NSObject

-(void) dropTblMymenu;
-(void) createTblMymenu;
-(NSArray *) readListMymenu;
-(BOOL) insertDataMymenu : (NSDictionary *) contentValue;
-(BOOL) deleteDataMymenu :(NSString *) featureId;

@end

NS_ASSUME_NONNULL_END
