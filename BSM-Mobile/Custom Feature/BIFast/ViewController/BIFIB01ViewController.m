//
//  BIFIB01ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFIB01ViewController.h"
#import "PLD01ViewController.h"
#import "PopupOTPViewController.h"
#import "Validation.h"

@interface BIFIB01ViewController ()<PLD01ViewDelegate, PopupOTPDelegate, UITextFieldDelegate>{
    NSUserDefaults *userDefault;
    Validation *validate;
    
    BOOL isRequested;
    BOOL mustAddToManager;
    
    NSString *key;
    NSString *selectedSrcAccNo;
    NSString *selectedProxyType;
    
    NSInteger selectedSrcAccIdx;
    
    NSArray *listAction;
    NSArray *listDataAcc;
    
    NSMutableArray *listProxyType;
    
    NSDictionary *data;
    
    CGSize keyboardSize;
    CGFloat originHeightContent;
    
    UIToolbar* keyboardDoneButtonView;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargin;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwFooter;
@property (weak, nonatomic) IBOutlet UIView *vwNavBar;

@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;

@property (weak, nonatomic) IBOutlet UILabel *lblSourceAccAltname;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAcc;
@property (weak, nonatomic) IBOutlet UIView *bgSourceAcc;
@property (weak, nonatomic) IBOutlet UIImageView *btnSourceAcc;

@property (weak, nonatomic) IBOutlet UIView *fvProxyType;
@property (weak, nonatomic) IBOutlet UILabel *lblProxyType;
@property (weak, nonatomic) IBOutlet UITextField *tfProxyType;
@property (weak, nonatomic) IBOutlet UIImageView *btnProxyType;

@property (weak, nonatomic) IBOutlet UIView *fvProxyAlias;
@property (weak, nonatomic) IBOutlet UILabel *lblProxyAlias;
@property (weak, nonatomic) IBOutlet UITextField *tfProxyAlias;

@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightVwFooter;

@end

@implementation BIFIB01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topMargin];
    
    userDefault = [NSUserDefaults standardUserDefaults];
    validate = [[Validation alloc]initWith:self];
    
    originHeightContent = _vwContent.frame.size.height;
    
    _lblTitleNav.text = [self.jsonData valueForKey:@"title"];
    
    // Setup view source account
    _lblSourceAccAltname.hidden = YES;
    _lblSourceAcc.text = lang(@"SELECT_DEBIT_ACC");
    
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(navigationBack)]];
    
    if([Utility isLanguageID]){
        [_lblProxyType setText:@"Pilih Jenis Proxy"];
        [_tfProxyType setPlaceholder:@"Pilih Jenis Proxy"];
    }else{
        [_lblProxyType setText:@"Select Proxy Type"];
        [_tfProxyType setPlaceholder:@"Select Proxy Type"];
    }
    
    [self addAccessoryKeypad];
    
    [_bgSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    [_btnSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    
    [_btnProxyType addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBtnProxyType)]];
    [_fvProxyType addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBtnProxyType)]];
    
    [_tfProxyType setDelegate:self];
    
    [_fvProxyAlias setHidden:YES];
    
    listProxyType = [[NSMutableArray alloc]init];
    [listProxyType addObject:@{@"key":@"01",
                               @"title":@"",
                               @"label":lang(@"LABEL_NOHAPE")}];
    [listProxyType addObject:@{@"key":@"02",
                               @"title":@"",
                               @"label":@"Email"}];
    
    // getListAccount
    [self getListAccountFromPresistence:[self.jsonData valueForKey:@"url_param"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
    
    [self.btnNext setTitle:[lang(@"NEXT") uppercaseString] forState:UIControlStateNormal];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)actionBtnSourceAcc{
    PLD01ViewController *dialogList = (PLD01ViewController*)[self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:_lblSourceAcc.text];
    [dialogList setPlaceholder:lang(@"SEARCH_ACC_NO")];
    [dialogList setFieldName:@"fieldSourceAcc"];
    [dialogList setList:listDataAcc];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)actionBtnProxyType{
    PLD01ViewController *dialogList = (PLD01ViewController*)[self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:_lblProxyType.text];
    [dialogList setHiddenSearchBox:YES];
    [dialogList setHeightViewContent:78+(44*listProxyType.count)];
    [dialogList setFieldName:@"fieldProxyType"];
    [dialogList setList:listProxyType];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray *)arrData fieldName:(NSString *)fname{
    data = [arrData objectAtIndex:indexRow];
    if ([fname isEqualToString:@"fieldSourceAcc"]) {
        selectedSrcAccIdx = indexRow;
        NSString *titleAltname = [data objectForKey:@"title"];
        if ([titleAltname isEqualToString:@""]){
            _lblSourceAccAltname.hidden = YES;
        } else {
            _lblSourceAccAltname.hidden = NO;
        }
        _lblSourceAccAltname.text = titleAltname;
        _lblSourceAcc.text = [data objectForKey:@"label"];
        selectedSrcAccNo = [data objectForKey:@"id_account"];
    } else if ([fname isEqualToString:@"fieldProxyType"]) {

        self.fvProxyAlias.hidden = NO;
        _tfProxyType.text = [data objectForKey:@"label"];
        
        _lblProxyAlias.text = [data objectForKey:@"label"];
        
        _tfProxyAlias.text = @"";
        _tfProxyAlias.placeholder = [data valueForKey:@"label"];
        
        selectedProxyType = [data objectForKey:@"key"];
        
        if([selectedProxyType isEqualToString:@"01"]){
            _tfProxyAlias.keyboardType = UIKeyboardTypeNumberPad;
        }else{
            _tfProxyAlias.keyboardType = UIKeyboardTypeEmailAddress;
        }
        
        [dataManager.dataExtra setValue:selectedProxyType forKey:@"proxyType"];
        [dataManager.dataExtra setValue:_tfProxyType.text forKey:@"proxyTypeValue"];
    }
}

- (void)navigationBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

-(void)actionNext{
    
    // validation
    NSString *errMsg = @"";
    if([selectedSrcAccNo isEqualToString:@""] || selectedSrcAccNo == nil){
        errMsg = lang(@"SELECT_DEBIT_ACC");
    } else if([_tfProxyType.text isEqualToString:@""]){
        errMsg = _tfProxyType.placeholder;
    } else if([_tfProxyAlias.text isEqualToString:@""]){
        errMsg = _tfProxyAlias.placeholder;
    } else if ([selectedProxyType isEqualToString:@"02"]) {
        [validate validateEmail:_tfProxyAlias.text];
    }
    
    // process
    if ([errMsg isEqualToString:@""]){
        NSString *pVal = _tfProxyAlias.text;
        if([[pVal substringToIndex:1] isEqualToString:@"0"]){
            pVal = [NSString stringWithFormat:@"%@%@",@"62",[pVal substringFromIndex:1]];
        }
        [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":selectedSrcAccNo}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"proxyValue":pVal}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"proxyType":selectedProxyType}];
        
        [self gotoOTP];
    } else {
        [self showErrorAlert:errMsg];
    }
}

- (void)gotoOTP{
    PopupOTPViewController *otp = (PopupOTPViewController*)[self routeTemplateController:@"POPUPOTP"];
    [otp setRequestType:@"registration_bi_fast"];
    if([Utility isLanguageID]){
        [otp setTitleBar:@"Masukkan OTP"];
    }else{
        [otp setTitleBar:@"Insert OTP"];
    }
    [otp setOTPLine:_tfProxyType.text andValue:_tfProxyAlias.text];
    [otp setOTPExpiredTime:60];
    [otp setDelegate:self];
    [self presentViewController:otp animated:YES completion:nil];
}

- (void)otpSucceed{
    [self openNextTemplate];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat keyboardShowupViewHeight = self.view.superview.frame.size.height-self->keyboardSize.height-(self.vwFooter.frame.size.height+12);
        CGRect f = CGRectMake(0,self.vwContent.frame.origin.y,self.view.frame.size.width,keyboardShowupViewHeight);
        self.vwContent.frame = f;
        self.heightVwFooter.constant = 0;
        [self.vwFooter setHidden:YES];
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat keyboardHiddenViewHeight = self->originHeightContent;//self.view.superview.frame.size.height;
        CGRect f = CGRectMake(0,self.vwContent.frame.origin.y,self.view.frame.size.width,keyboardHiddenViewHeight);
        self.vwContent.frame = f;
        self.heightVwFooter.constant = 80;
        [self.vwFooter setHidden:NO];
    }];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if(textField == _tfProxyType){
        [self actionBtnProxyType];
        return NO;
    }
    return YES;
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
            if(!isRequested){
                isRequested = YES;
                [self getListAccountFromPresistence:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                
            }
        }else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}

- (void)errorLoadData:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reloadApp {
    [self backToRoot];
    [BSMDelegate reloadApp];
}

-(void) getListAccountFromPresistence : (NSString *) urlParam strMenuId : (NSString *) menuId {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct = nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
            if ([dataManager.dataExtra objectForKey:@"code"]) {
                if ([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
        }
    }
    
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];
                
            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct != nil) {
        isRequested = true;
        if(listAcct.count == 0){
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki nomor rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account number for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            newData = [self getListAccountFormatData:listAcct];
            mustAddToManager = true;
            key = @"id_account";
            listAction = newData;
            listDataAcc = newData;
        }
    }else{
        if (!isRequested) {
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            
            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
        }else{
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (NSMutableArray*)getListAccountFormatData:(NSArray*)listAcct{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listAcct){
        NSString *title = @"";
        NSString *label = @"";
        if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
            if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                title = [NSString stringWithFormat:@"%@",[temp valueForKey:@"altname"]];
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else{
                label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
            }
        }
        else{
            label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
        }
        [newData addObject:@{@"title":title,@"label":label,@"id_account":[temp valueForKey:@"id_account"]}];
    }
    
    return newData;
}

- (void)showErrorAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }

    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)addAccessoryKeypad{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    _tfProxyType.inputAccessoryView = keyboardDoneButtonView;
    _tfProxyAlias.inputAccessoryView = keyboardDoneButtonView;
}

- (IBAction)doneClicked:(id)sender {
    [self.view endEditing:YES];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
