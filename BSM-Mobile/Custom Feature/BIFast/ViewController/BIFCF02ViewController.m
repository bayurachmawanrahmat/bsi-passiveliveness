//
//  BIFCF02ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 11/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFCF02ViewController.h"
#import "PopupPinTrxViewController.h"
#import "BIFCFView.h"

@interface BIFCF02ViewController ()<PopupPinTrxViewDelegate>{
    
    NSArray *listData;

    int lastY;
    UIToolbar *toolbar;
    
}

@property (weak, nonatomic) IBOutlet UIView *vwNavBar;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwInfoAccount;
@property (weak, nonatomic) IBOutlet UIView *vwSourceAccount;
@property (weak, nonatomic) IBOutlet UIView *vwDestAccount;
@property (weak, nonatomic) IBOutlet UIView *vwAttention;
@property (weak, nonatomic) IBOutlet UILabel *lblAttention;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *widthSrcLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *widthDestLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblNamaAsal;
@property (weak, nonatomic) IBOutlet UILabel *lblRekAsal;
@property (weak, nonatomic) IBOutlet UILabel *lblNamaTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblRekTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblDari;
@property (weak, nonatomic) IBOutlet UILabel *lblTujuan;
@property (weak, nonatomic) IBOutlet UILabel *lblKonfirmasi;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@end

@implementation BIFCF02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.lblTitleNav.text = [dataManager.dataExtra valueForKey:@"menu_title"];
    self.lblKonfirmasi.text = [self.jsonData valueForKey:@"title"];
    self.lblDari.text = lang(@"LABEL_SOURCE");
    self.lblTujuan.text = lang(@"LABEL_BENEFICIARY");
    
    // Setup navigationn
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
//    _lblTitleNav.text = [self.jsonData valueForKey:@"title"];
    [_vwContent setHidden:true];
    
    // border & corner radius view info
    [_vwInfoAccount.layer setCornerRadius:10.0f];
    [_vwInfoAccount.layer setBorderWidth:1.0f];
    [_vwInfoAccount.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    [_vwSourceAccount.layer setCornerRadius:10.0f];
    [_vwDestAccount.layer setCornerRadius:10.0f];
    
    [_btnNext addTarget:self action:@selector(actionBtnTransfer) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)navigationBack{
    dataManager.currentPosition--;
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)actionBtnTransfer{
    PopupPinTrxViewController *dialogPin = (PopupPinTrxViewController*)[self routeTemplateController:@"POPUPPIN"];
    [dialogPin setDelegate:self];
    [dialogPin setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogPin setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogPin animated:YES completion:nil];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        urlData =[NSString stringWithFormat:@"%@",[self.jsonData valueForKey:@"url_parm"]];
        
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
    }
}

- (void)doCheckPin{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_pin,pin,language"] needLoading:true encrypted:true banking:true favorite:nil];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"inq_transfer_bifast"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            
            @try {
                
                
                NSString *response = [jsonObject objectForKey:@"response"];
                NSError *error;
                NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                        options:NSJSONReadingAllowFragments
                                                                          error:&error];
                
                [_vwContent setHidden:false];
                [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject objectForKey:@"transaction_id"]}];
                [[NSUserDefaults standardUserDefaults] setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
                
                
                NSDictionary *data = [dataDict objectForKey:@"data"];
                
                self.lblNamaAsal.text = [data objectForKey:@"sender_name"];
                self.lblRekAsal.text = [data objectForKey:@"sender_account_no"];
                
                self.lblNamaTujuan.text = [data objectForKey:@"recipient_name"];
                self.lblRekTujuan.text = [data objectForKey:@"recipient_account_no"];

                NSString *desc = @"";
                desc = [data valueForKey:@"description"];
                if([desc isEqualToString:@""]){
                    desc = @"-";
                }
                // bank tujuan, jumlah, keterangan,  biaya admin
                listData = @[
                    @{@"title": lang(@"LABEL_DEST_BANK"), @"value": [data valueForKey:@"recipient_bank_name"]},
                    @{@"title": lang(@"LABEL_AMOUNT"), @"value": [Utility localFormatCurrencyValue:[[data valueForKey:@"amount"]doubleValue] showSymbol:YES]},
                    @{@"title": lang(@"LABEL_DESC"), @"value": [data valueForKey:@"description"]},
                    @{@"title": lang(@"LABEL_FEE"), @"value": [Utility localFormatCurrencyValue:[[data valueForKey:@"fee_amount"]doubleValue] showSymbol:YES]}
                ];
                
            } @catch (NSException *exception) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR")  preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                    [self backToRoot];
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            [self loadContent];
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                [self backToRoot];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
    if ([requestType isEqualToString:@"verify_pin"]) {
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [self openNextTemplate];
        } else {
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
}

- (void)pinDoneState:(NSString *)menuID{
    [self doCheckPin];
}

- (void) loadContent{
    if(listData.count > 0){
        [Utility removeAllArrangedViewOf:_stackView];
        
        for (NSDictionary *dict in listData){
            BIFCFView *contentView = [[BIFCFView alloc]init];
            contentView.labelTitle.text = [dict objectForKey:@"title"];
            contentView.labelValue.text = [dict objectForKey:@"value"];
            [_stackView addArrangedSubview:contentView];
        }
        
    }
}

@end
