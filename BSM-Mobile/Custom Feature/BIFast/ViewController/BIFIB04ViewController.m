//
//  BIFIB04ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 11/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFIB04ViewController.h"
#import "PLD01ViewController.h"

@interface BIFIB04ViewController ()<PLD01ViewDelegate>{
    NSString *selectedDate;
    NSString *selectedNoref;
    
    NSArray *listDataNoref;
    
    UIDatePicker *datePikcer;
    
    UIToolbar *toolbar;
}
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;

@property (weak, nonatomic) IBOutlet UIView *fvNoRef;
@property (weak, nonatomic) IBOutlet UILabel *lblNoRef;
//@property (weak, nonatomic) IBOutlet UITextField *tfNoRef;
@property (weak, nonatomic) IBOutlet UILabel *lblValueNoRef;
@property (weak, nonatomic) IBOutlet UIImageView *btnNoRef;

@property (weak, nonatomic) IBOutlet UIView *fvTrxDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTrxDate;
@property (weak, nonatomic) IBOutlet UITextField *tfTrxDate;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation BIFIB04ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [Styles setTopConstant:_topConstant];
    
    selectedDate = @"";
    selectedNoref = @"";
    
    _lblTitleNav.text = [dataManager.dataExtra valueForKey:@"menu_title"];
    _lblTitle.text = [self.jsonData valueForKey:@"title"];
    
    //setup navigation
    [_navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
    
    [_btnNext setTitle:[lang(@"NEXT") uppercaseString] forState:UIControlStateNormal];
    [_btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    
    //setup action No Referensi
    [_lblValueNoRef setUserInteractionEnabled:YES];
    [_btnNoRef setUserInteractionEnabled:YES];
    
    [_lblValueNoRef addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBtnNoref)]];
    [_btnNoRef addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBtnNoref)]];
    
    _lblNoRef.text = lang(@"LABEL_TRXNO");
    _lblTrxDate.text = lang(@"LABEL_TRXDATE");
    
    [self setupDatepicker];
    [self setupToolbar];
    
    [self.vwContent setHidden:YES];
}

- (void)actionDone{
    [self.view endEditing:YES];
}

- (void) setupToolbar{
    toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
    toolbar.barStyle = UIBarStyleDefault;
    toolbar.items = @[[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], [[UIBarButtonItem alloc]initWithTitle:lang(@"DONE") style:UIBarButtonItemStyleDone target:self action:@selector(actionDone)]];
    
    _tfTrxDate.inputAccessoryView = toolbar;
}

- (void) setupDatepicker{
    datePikcer = [[UIDatePicker alloc]init];
    
    [datePikcer setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"id_ID"]];
    [datePikcer setDatePickerMode:UIDatePickerModeDate];
    [datePikcer addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    if (@available(iOS 13.4, *)) {
        [datePikcer setPreferredDatePickerStyle:UIDatePickerStyleWheels];
    }
    _tfTrxDate.inputView = datePikcer;
}

- (void)dateChanged:(UIDatePicker*)pickerDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    selectedDate = [dateFormat stringFromDate:pickerDate.date];
    
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate.date];
    _tfTrxDate.text = dateString;
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
        urlData =[NSString stringWithFormat:@"%@",[self.jsonData valueForKey:@"url_parm"]];
        
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)navigationBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)actionBtnNoref{

    PLD01ViewController *dialogList = (PLD01ViewController*)[self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:_lblNoRef.text];
    [dialogList setPlaceholder:@""];
    [dialogList setFieldName:@"fieldNoref"];
    [dialogList setList:listDataNoref];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    if(listDataNoref.count < 1){
        [Utility showMessage:lang(@"NO_REF_DATA") instance:self];
    }else{
        [self presentViewController:dialogList animated:YES completion:nil];
    }
    
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray *)arrData fieldName:(NSString *)fname{
    selectedNoref = [[arrData objectAtIndex:indexRow]objectForKey:@"request_id"];
    _lblValueNoRef.text = [[arrData objectAtIndex:indexRow]objectForKey:@"label"];
    _lblValueNoRef.backgroundColor = UIColorFromRGB(const_bgcolor_lightorange);
}

-(void)actionNext{
    NSString *errMsg = @"";
    if([selectedNoref isEqualToString:@""]){
        errMsg = _lblNoRef.text;
    }
    else if([selectedDate isEqualToString:@""]){
        errMsg = _lblTrxDate.text;
    }

    // process
    if ([errMsg isEqualToString:@""]){
        [dataManager.dataExtra addEntriesFromDictionary:@{@"request_id":selectedNoref}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_date":selectedDate}];
        [super openNextTemplate];
    } else {
        [self showErrorAlert:errMsg];
    }
}

- (void)showErrorAlert:(NSString *)message{
    
    UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] preferredStyle:UIAlertControllerStyleAlert];
    [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    }]];
    [self presentViewController:alerts animated:YES completion:nil];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"list_ct_status"]){
        @try {
            NSArray *listData = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in listData){
                [newData addObject:@{@"title":@"",
                                     @"label":[NSString stringWithFormat:@"%@\n%@",[temp valueForKey:@"creditor_name"],[temp valueForKey:@"ref_id"]],
                                     @"creditor_name":[temp valueForKey:@"creditor_name"],
                                     @"ref_id":[temp valueForKey:@"ref_id"],
                                     @"request_id":[temp valueForKey:@"request_id"]
                                   }];
            }
            listDataNoref = newData;
            [self.vwContent setHidden:NO];
        } @catch (NSException *exception) {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)reloadApp{
    [self backToRoot];
    [BSMDelegate reloadApp];
}

@end
