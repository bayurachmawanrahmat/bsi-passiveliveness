//
//  LVBIF01ViewController.h
//  BSM-Mobile
//
//  Created by Amini on 17/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LVBIF01ViewController : TemplateViewController

@end

@interface BIFSettingTapGesture : UITapGestureRecognizer

@property NSInteger settingState;
@property (nonatomic, strong) NSDictionary *selectedData;

@end
NS_ASSUME_NONNULL_END
