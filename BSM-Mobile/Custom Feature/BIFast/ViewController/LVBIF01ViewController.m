//
//  LVBIF01ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 17/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "LVBIF01ViewController.h"
#import "BIFInfoCell.h"
#import "PLD01ViewController.h"
#import "BIFInfoView.h"

NS_ENUM(NSInteger){
    UPDATE = 0,
    PORTING = 1,
    ACTIVATION = 2,
    NONE = 3
};

@interface LVBIF01ViewController ()<UITableViewDelegate, UITableViewDataSource, PLD01ViewDelegate, UISearchBarDelegate>{
    
    NSMutableArray *listUpdate;
    NSArray *listAction;
    
    NSArray *listData, *filteredListData;
    NSDictionary *selectedList;
    BOOL isSearchActive;
}

@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlebar;

@property (weak, nonatomic) IBOutlet UILabel *lblSearch;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

//@property (weak, nonatomic) IBOutlet UITextField *txtFieldSearch;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation LVBIF01ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    isSearchActive = NO;
    
    [_tableView setDataSource:self];
    [_tableView setDelegate:self];
    [_tableView registerNib:[UINib nibWithNibName:@"BIFInfoCell" bundle:nil] forCellReuseIdentifier:@"BIFInfoCell"];
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _lblTitleNav.text = [dataManager.dataExtra valueForKey:@"menu_title"];
    _lblTitlebar.text = [self.jsonData valueForKey:@"title"];
    _lblSearch.text = lang(@"LABEL_REGISTERED_PROXY");
 
    
    _searchBar.layer.cornerRadius = 8;
    _searchBar.layer.borderWidth = 1;
    _searchBar.layer.borderColor = [UIColorFromRGB(const_color_secondary)CGColor];

    
    [_searchBar setDelegate:self];
    
    listAction = [self.jsonData objectForKey:@"content"];
    
    [_navBackBtn setUserInteractionEnabled:YES];
    [_navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(navigationBack)]];
    
}

- (void)navigationBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

#pragma mark UITableViewDelegate and UITableViewDatasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return isSearchActive ? filteredListData.count : listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *listResult;
    NSString *proxyStatus;
    NSString *proxyStatusConv;
    
    BIFInfoCell *cell = (BIFInfoCell *)[tableView dequeueReusableCellWithIdentifier:@"BIFInfoCell" forIndexPath:indexPath];
    BIFSettingTapGesture *singleTap = [[BIFSettingTapGesture alloc] initWithTarget:self action:@selector(setupSettingAction:)];

    
    if(isSearchActive){
        listResult = [filteredListData[indexPath.row] objectForKey:@"list_result"];
        proxyStatus = [filteredListData[indexPath.row] valueForKey:@"proxy_status"];
        proxyStatusConv = [filteredListData[indexPath.row] valueForKey:@"proxy_status_convert"];
        singleTap.selectedData = filteredListData[indexPath.row];
        
    }else{
        listResult = [listData[indexPath.row] objectForKey:@"list_result"];
        proxyStatus = [listData[indexPath.row] valueForKey:@"proxy_status"];
        proxyStatusConv = [listData[indexPath.row] valueForKey:@"proxy_status_convert"];
        singleTap.selectedData = listData[indexPath.row];
    }
    
    
    NSString *number = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
    cell.labelNumber.text = number;
    cell.labelNumber.textColor = [UIColor whiteColor];
    
    
    BOOL isBankBSI = false;
    [Utility removeAllArrangedViewOf:cell.stackView];
    for(NSDictionary* dict in listResult){
        BIFInfoView *infoView = [[BIFInfoView alloc]init];
        
        NSString *label = [dict valueForKey:@"label"];
        NSString *value = [dict valueForKey:@"value"];
        
        [infoView setLabel:label andValue:value];
        [cell.stackView addArrangedSubview:infoView];
        

        if([label isEqualToString:@"Bank"] && [value isEqualToString:@"BSI"]){
            isBankBSI = true;
        }
        
    }
        
    cell.lblStatus.text = proxyStatusConv;
    UIColor *color;
    
    if([proxyStatus isEqualToString:@"ACTV"]){
        color = UIColorFromRGB(const_color_primary);
        if(isBankBSI){
            singleTap.settingState = UPDATE;
        }else{
            singleTap.settingState = PORTING;
        }
    }else{
        color = UIColorFromRGB(const_color_secondary);
        if(isBankBSI){
            singleTap.settingState = ACTIVATION;
        }else{
            singleTap.settingState = NONE;
        }
    }
    
    cell.labelNumber.backgroundColor = color;
    
    cell.lblStatus.textColor = color;
    
    
    cell.vwParentContent.layer.borderWidth = 1;
    cell.vwParentContent.layer.borderColor = [color CGColor];
    cell.vwParentContent.layer.cornerRadius = 8;
    
    [cell.imgSetting setImage:[UIImage imageNamed:@"ic_gear"]];
    [cell.imgSetting setUserInteractionEnabled:YES];
    [cell.imgSetting addGestureRecognizer:singleTap];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [self.jsonData valueForKey:@"url_parm"];
        [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];

        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"list_proxy_bySID"]){
        if ([[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]){
            
            NSString *transaction_id = [jsonObject valueForKey:@"transacation_id"];
            if(transaction_id != nil){
                [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
                [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject objectForKey:@"transaction_id"]}];
                [[NSUserDefaults standardUserDefaults] setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            }
            
            NSString *response = [jsonObject objectForKey:@"response"];
            NSError *error;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[response dataUsingEncoding:NSUTF8StringEncoding]
                                                                    options:NSJSONReadingAllowFragments
                                                                      error:&error];
            listData = [dataDict objectForKey:@"result_data"];
            [_tableView reloadData];
            
            if(listData.count < 1){
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"NO_DATA") preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self backToR];
                }]];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
        }else{
            
            NSString *msg = lang(@"ERROR_MSG");
            if([jsonObject objectForKey:@"response"] != nil){
                msg = [jsonObject objectForKey:@"response"];
            }
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:msg preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)setupSettingAction:(UITapGestureRecognizer *)tapRecognizer {

    BIFSettingTapGesture *tapped = (BIFSettingTapGesture *)tapRecognizer;
    
    PLD01ViewController *dialogList = (PLD01ViewController *)[self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setHiddenSearchBox:YES];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [dialogList setTitle:lang(@"LABEL_SETTING")];
    [dialogList setFieldName:[@(tapped.settingState) stringValue]];

    //setup list
    [self setSettingListFor:tapped.settingState];
        
    [dialogList setHeightViewContent:78+(44*listUpdate.count)];
    [dialogList setList:listUpdate];
    
    selectedList = tapped.selectedData;
    
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)setSettingListFor:(NSInteger) state{
    
    listUpdate = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in listAction){
        NSMutableDictionary *newObject = [[NSMutableDictionary alloc]init];
        NSString *code = [dict valueForKey:@"code"];
        
        //update
        if(state == UPDATE){
            
            if([code isEqualToString:@"00300"] || [code isEqualToString:@"00301"] || [code isEqualToString:@"00302"]){
                
                [newObject addEntriesFromDictionary:dict];
                [newObject setObject:[dict valueForKey:@"code"] forKey:@"key"];
                [newObject setObject:[dict valueForKey:@"name"] forKey:@"label"];
            }
        }
        
        //Activation
        if(state == ACTIVATION){
            if ([code isEqualToString:@"00303"]){
                [newObject addEntriesFromDictionary:dict];
                [newObject setObject:[dict valueForKey:@"code"] forKey:@"key"];
                [newObject setObject:[dict valueForKey:@"name"] forKey:@"label"];
            }
        }
        
        
        //Porting
        if(state == PORTING){
            
            if ([code isEqualToString:@"00304"]){
                [newObject addEntriesFromDictionary:dict];
                [newObject setObject:[dict valueForKey:@"code"] forKey:@"key"];
                [newObject setObject:[dict valueForKey:@"name"] forKey:@"label"];
            }
            
        }
        
        if(newObject.count > 0){
            [newObject setObject:@"" forKey:@"title"];
            [listUpdate addObject:newObject];
        }
        
    }

}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray *)arrData fieldName:(NSString *)fname{

    NSMutableArray *listField = [[NSMutableArray alloc]init];
    [dataManager.dataExtra setValue:selectedList forKey:@"selected_data"];
    
    NSString *proxyType = @"";
    if([[selectedList valueForKey:@"proxy_type"]isEqualToString:@"01"]){
        proxyType = lang(@"LABEL_NOHAPE");
    }else if([[selectedList valueForKey:@"proxy_type"]isEqualToString:@"02"]){
        proxyType = @"Email";
    }
    
    [dataManager.dataExtra setValue:[selectedList valueForKey:@"account_id"] forKey:@"account_id_proxy_old"];
    [dataManager.dataExtra setValue:[selectedList valueForKey:@"registration_id"] forKey:@"registration_id"];
    [dataManager.dataExtra setValue:[selectedList valueForKey:@"customer_name"] forKey:@"customer_name"];
    
    
    if([fname intValue] == PORTING)
    {
        [listField addObject:@{@"input_type":@"default",
                               @"enabled":@"NO",
                               @"label":lang(@"LABEL_PROXYTYPE"),
                               @"value":proxyType}];
        [listField addObject:@{@"input_type":@"default",
                               @"enabled":@"NO",
                               @"label":proxyType,
                               @"value":[selectedList valueForKey:@"proxy_value"]}];
        [listField addObject:@{@"input_type":@"default",
                               @"enabled":@"NO",
                               @"label":@"Bank",
                               @"value":[selectedList valueForKey:@"bank_name"]}];
        
    }
    else if([fname intValue] == UPDATE)
    {
        [listField addObject:@{@"input_type":@"default",
                               @"enabled":@"NO",
                               @"label":lang(@"LABEL_REGISTERED_PROXY"),
                               @"value":[selectedList valueForKey:@"proxy_value"]}];
    }
        
    [dataManager.dataExtra addEntriesFromDictionary:@{
        @"proxy_type":[selectedList valueForKey:@"proxy_type"],
        @"proxy_value":[selectedList valueForKey:@"proxy_value"],
        @"account_id_proxy_old":[selectedList valueForKey:@"account_id"],
        @"registration_status":[selectedList valueForKey:@"proxy_status"],
        @"secondary_id_type":[selectedList valueForKey:@"secondary_id_type"],
        @"secondary_id_value":[selectedList valueForKey:@"secondary_id_value"]
    }];
    
    if([[selectedList valueForKey:@"proxy_status"] isEqualToString:@"SUSP"]){
        [dataManager.dataExtra setValue:@"ACTV" forKey:@"registration_status"];
    }
    
    [dataManager setAction:[arrData[indexRow] objectForKey:@"action"] andMenuId:[self.jsonData valueForKey:@"menu_id"]];
    [dataManager.dataExtra setValue:listField forKey:@"input_field"];
    
    [dataManager.dataExtra addEntriesFromDictionary:@{@"code":[arrData[indexRow] objectForKey:@"code"]}];
    
    [dataManager.dataExtra setValue:listData forKey:@"list_data_proxy"];
    
    [super openNextTemplate];
    
    /*
        ____update proxy/alias
            - rekening
            - proxy terdaftar
        ----porting
            - rekening
            - jenis proxy -> picker
            - nomor hape -> number
            - bank
        ----hapus proxy
            - direct to konfirmasi
        ----blokir
            - direct to konfirmasi
        ----aktivasi
            - direct to konfirmasi
     
     */
}


#pragma mark SearchbarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    isSearchActive = YES;
    filteredListData = listData;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    isSearchActive = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    isSearchActive = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSPredicate *predicateBankName = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"bank_name", searchText];
    NSPredicate *predicateAccount = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"account_id", searchText];
    NSPredicate *predicateProxyAlias = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"proxy_value", searchText];
    NSPredicate *predicateProxyStatus = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"proxy_status_convert", searchText];
    
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicateBankName, predicateAccount, predicateProxyAlias, predicateProxyStatus]];

    NSArray *filterlist = [listData filteredArrayUsingPredicate:predicate];
    
    filteredListData = filterlist;
    if([searchText isEqualToString:@""]){
        filteredListData = listData;
    }
    [self.tableView reloadData];
}

@end


@implementation BIFSettingTapGesture


@end


