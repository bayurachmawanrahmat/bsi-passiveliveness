//
//  BIFIB03ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 08/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFIB03ViewController.h"
#import "PLD01ViewController.h"


@interface BIFIB03ViewController ()<PLD01ViewDelegate, UITextFieldDelegate>{
    NSUserDefaults *userDefault;
    NSString *menuId;
    NSArray *arrayLabel;
    NSArray *listDataAcc;
    NSMutableArray *listDestTransfer;
    NSArray *listDataBank;
    NSArray *listDataFav;
    NSInteger optTransferIdx;

    BOOL isRequested;
    BOOL mustAddToManager;
    NSString *key;
    NSArray *listAction;
    NSDictionary *data;
    
    NSString *selectedSrcAccNo;
    NSString *selectedBankCode;

    NSInteger selectedSrcAccIdx;
    
    UIView *viewPreviewQR;
    UIView *viewFlash;
    UIView *kotakScan;

    
    NSString *lang;
    UIToolbar* keyboardDoneButtonView;
    CGSize keyboardSize;
    CGFloat originHeightContent;
    
    int maxLengtDescription;
}

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwFooter;
@property (weak, nonatomic) IBOutlet UIView *vwNavBar;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAccAltname;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAcc;
@property (weak, nonatomic) IBOutlet UIView *bgSourceAcc;
@property (weak, nonatomic) IBOutlet UIImageView *btnSourceAcc;
@property (weak, nonatomic) IBOutlet UISegmentedControl *optionTransferToggle;

@property (weak, nonatomic) IBOutlet UIView *fvDestBank;
@property (weak, nonatomic) IBOutlet UILabel *lblDestBank;
@property (weak, nonatomic) IBOutlet UILabel *lblValDestBank;
@property (weak, nonatomic) IBOutlet UITextField *tfDestBank;
@property (weak, nonatomic) IBOutlet UIImageView *btnDestBank;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFtDestBank;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFvDestBank;

@property (weak, nonatomic) IBOutlet UIView *fvDestAcc;
@property (weak, nonatomic) IBOutlet UILabel *lblDestAcc;
@property (weak, nonatomic) IBOutlet UITextField *tfDestAcc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFvDestAcc;

@property (weak, nonatomic) IBOutlet UIView *fvAmountTrf;
@property (weak, nonatomic) IBOutlet UILabel *lblAmountTrf;
@property (weak, nonatomic) IBOutlet UITextField *tfAmountTrf;

@property (weak, nonatomic) IBOutlet UIView *fvDescTrf;
@property (weak, nonatomic) IBOutlet UILabel *lblDescTrf;
@property (weak, nonatomic) IBOutlet UITextField *tfDescTrf;

@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightVwFooter;

@end

@implementation BIFIB03ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    maxLengtDescription = 100;
    listDataFav = [[NSMutableArray alloc] init];
    userDefault = [NSUserDefaults standardUserDefaults];
    
    // Setup navigationn
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
    _lblTitleNav.text = [self.jsonData valueForKey:@"title"];
    
    // Setup view source account
    _lblSourceAccAltname.hidden = YES;
    _lblSourceAcc.text = lang(@"SELECT_DEBIT_ACC");
    // border & corner radius
    [_bgSourceAcc.layer setCornerRadius:10.0f];
    [_bgSourceAcc.layer setBorderWidth:1.0f];
    [_bgSourceAcc.layer setBorderColor:[UIColor colorWithRed:236/255.0 green:176/255.0 blue:83/255.0 alpha:1].CGColor];
    
    // Setup Tab Option Transfer
    [_optionTransferToggle setTitle:lang(@"TAB_NEW_TRANSFER") forSegmentAtIndex:0];
    [_optionTransferToggle setTitle:lang(@"TAB_FAV_TRANSFER") forSegmentAtIndex:1];
    

    _lblDestBank.text = lang(@"SELECT_DEST_BANK");
    _lblDestAcc.text = lang(@"INPUT_DESC_PROXY");
    _lblAmountTrf.text = lang(@"INPUT_AMOUNT_TRF");
    _lblDescTrf.text = lang(@"INPUT_DESC_TRF");
    
    if((self.view.frame.size.width <= 428 && _lblDestAcc.text.length >= 55) ||
       (self.view.frame.size.width <= 390 && _lblDestAcc.text.length >= 50)){
        _heightFvDestAcc.constant = _fvDestAcc.frame.size.height+19;
    }
    
    // Setup gesture
    [self.bgSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    [self.btnSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    
    [self.tfDestBank addTarget:self action:@selector(actionBtnDestBank) forControlEvents:UIControlEventTouchDown];
    [self.btnDestBank addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionBtnDestBank)]];
    
    
    
    [self.tfAmountTrf addTarget:self action:@selector(nominalTransactionDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    [_btnNext setTitle:[lang(@"NEXT") uppercaseString] forState:UIControlStateNormal];
    
    // Misc method
    originHeightContent = _vwContent.frame.size.height;
    listDestTransfer = [[NSMutableArray alloc]init];
    [listDestTransfer addObject:@{@"key":@"1",@"title":@"",@"label":lang(@"OWN_ACC")}];
    [listDestTransfer addObject:@{@"key":@"2",@"title":@"",@"label":lang(@"OTHERS_ACC")}];
    [self addAccessoryKeypad];
    [self resetFormTransfer];
    
    
    [self.vwContent setHidden:YES];
    
    // Setup Service
    // getListAccount
    [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
    [self resetFieldTransfer];
    [self reloadDataListBank];
}


- (void)resetFormTransfer {
    selectedSrcAccNo = @"";
    selectedBankCode = @"";
    [dataManager.dataExtra removeObjectForKey:@"type_transfer"];
}

- (void)navigationBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}


- (IBAction)switcherOptionTransfer:(UISegmentedControl*)sender {
    [self setOptionTransferView:sender.selectedSegmentIndex];
}

- (void)reloadDataFromRecently {
    if ([[dataManager.dataExtra valueForKey:@"from_recently"] boolValue]){
        NSString *descBankCode = [dataManager.dataExtra valueForKey:@"code"];
        NSString *destAccNo = [dataManager.dataExtra valueForKey:@"payment_id"];
        if([descBankCode isEqualToString:@"451"]){
            int idxdest = 0;
            if ([self checkArray:listDataAcc containsSubstring:destAccNo]) {
                idxdest = 0;

            } else {
                idxdest = 1;

            }

            self.fvAmountTrf.hidden = NO;
            self.fvDescTrf.hidden = NO;
            _tfDestBank.text = [[listDestTransfer objectAtIndex:idxdest] objectForKey:@"label"];
        } else {
            if([dataManager.dataExtra valueForKey:@"destbank_name"]){
                _tfDestBank.text = [dataManager.dataExtra valueForKey:@"destbank_name"];
            } else {
                NSInteger idx = [self checkIdxArray:listDataBank containsSubstring:descBankCode];
                _tfDestBank.text = [[listDataBank objectAtIndex:idx] valueForKey:@"label"];
            }
        }
        
//        selectedDestAccNo = destAccNo;
    }
}

- (BOOL)checkArray:(NSArray *)array containsSubstring:(NSString *)substring {
    for (NSDictionary *dict in array) {
        if ([[dict valueForKey:@"id_account"] isEqualToString:substring]) {
            return YES;
            break;
        }
    }
    return NO;
}

- (NSInteger)checkIdxArray:(NSArray *)array containsSubstring:(NSString *)substring {
    NSInteger index = 0;
    for (NSDictionary *dict in array) {
        if ([[dict valueForKey:@"code"] isEqualToString:substring]) {
            return index;
            break;
        }
        index++;
    }
    return index;
}

- (void)resetFieldTransfer{
    self.tfDestAcc.text = @"";
    self.tfDestBank.text = @"";
//    self.lblTitleDestAccNo.text = @"";
    self.lblValDestBank.text = @"";
    self.tfAmountTrf.text = @"";
    self.tfDescTrf.text = @"";

    self.heightFvDestBank.constant = 72;
    self.heightFtDestBank.constant = 36;
}

- (void)setOptionTransferView:(NSInteger)indexSelected{
    optTransferIdx = indexSelected;
    switch(indexSelected) {
        case 0:
            [self.fvDestAcc setHidden:NO];
            _tfDestBank.hidden = NO;
            [self resetFieldTransfer];
            break;
        case 1:
            [self resetFieldTransfer];
            [self reloadDataFavorite];
            [self.fvDestAcc setHidden:YES];
            break;
        default:
            break;
    }
}

- (void)actionBtnSourceAcc{
    PLD01ViewController *dialogList = (PLD01ViewController*)[self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:_lblSourceAcc.text];
    [dialogList setPlaceholder:lang(@"SEARCH_ACC_NO")];
    [dialogList setFieldName:@"fieldSourceAcc"];
    [dialogList setList:listDataAcc];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)actionBtnDestBank{
    PLD01ViewController *dialogList = (PLD01ViewController*)[self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:_lblDestBank.text];

    if(optTransferIdx == 0){
        [dialogList setHiddenSearchBox:NO];
        [dialogList setHeightViewContent:118+(44*10)];
        [dialogList setFieldName:@"fieldDestBank"];
        [dialogList setList:listDataBank];
    }else{
        [dialogList setHiddenSearchBox:YES];
        [dialogList setHeightViewContent:118+(44*10)];
        [dialogList setFieldName:@"fieldDataFav"];
        [dialogList setList:listDataFav];
    }
    
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)actionBtnDestAccNo{
    NSMutableArray *listDestAccNo = [listDataAcc mutableCopy];
    [listDestAccNo removeObjectAtIndex:selectedSrcAccIdx];
    PLD01ViewController *dialogList = [self.storyboard instantiateViewControllerWithIdentifier:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setPlaceholder:lang(@"SEARCH_ACC_NO")];
    [dialogList setFieldName:@"fieldDestAccNo"];
        [dialogList setList:listDataFav];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)nominalTransactionDidChange:(UITextField *) sender {
    double amountInput = [[self stringNominalReplace:sender.text] doubleValue];
    sender.text = [Utility localFormatCurrencyValue:amountInput showSymbol:true];
}

- (NSString *)stringNominalReplace:(NSString *) txtString{
    NSString *tfR1 = [txtString stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *tfR2 = [tfR1 stringByReplacingOccurrencesOfString:@"Rp " withString:@""];
    return  tfR2;
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray*)arrData fieldName:(NSString *)fname{
    data = [arrData objectAtIndex:indexRow];
    if ([fname isEqualToString:@"fieldSourceAcc"]) {
        selectedSrcAccIdx = indexRow;
        NSString *titleAltname = [data objectForKey:@"title"];
        if ([titleAltname isEqualToString:@""]){
            _lblSourceAccAltname.hidden = YES;
        } else {
            _lblSourceAccAltname.hidden = NO;
        }
        _lblSourceAccAltname.text = titleAltname;
        _lblSourceAcc.text = [data objectForKey:@"label"];
        selectedSrcAccNo = [data objectForKey:@"id_account"];
    } else if ([fname isEqualToString:@"fieldDestBank"]) {
//        if(optTransferIdx == 0){
            _tfDestBank.text = [data objectForKey:@"label"];
            selectedBankCode = [data objectForKey:@"code"];

//        }else{
    }else{
            
            NSArray *paramArray = [[data objectForKey:@"params"] componentsSeparatedByString: @","];
            for(NSString *params in paramArray){
                NSArray *string = [params componentsSeparatedByString: @"="];
                [dataManager.dataExtra addEntriesFromDictionary:@{string[0]:string[1]}];
                if([string[0] isEqualToString:@"code"]){
                    selectedBankCode = string[1];
                }
            }
            _tfDestBank.text = [self getBankNameByCode:selectedBankCode];
            _tfDestAcc.text = [dataManager.dataExtra valueForKey:@"payment_id"];
            _tfDestBank.hidden = YES;
           
            _heightFvDestBank.constant = 146;
            _heightFtDestBank.constant = 110;
         
            _lblDestBank.text = [data objectForKey:@"title"];
            _lblValDestBank.text = [data objectForKey:@"label"];
            _lblValDestBank.backgroundColor = UIColorFromRGB(const_bgcolor_lightorange);
    }
}

- (NSString*)getBankNameByCode:(NSString*)code{
    
    for(NSDictionary *dict in listDataBank){
        if([[dict objectForKey:@"code"] isEqualToString:code]){
            return [dict objectForKey:@"label"];
        }
    }
    return nil;
}

- (void)reloadDataFavorite{
    if(!listDataFav || listDataFav.count == 0){
        menuId = @"00190";
        dataManager.menuId = menuId;
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        conn.fav = true;
        // type=4 is Transfer BI Fast
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_favorite,menu_id=%@,type=%d",menuId,4] needLoading:true encrypted:true banking:false];
    }
}

- (void)reloadDataListBank{
    if(!listDataBank){
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=list_bank_bifast"] needLoading:false encrypted:true banking:false];
    }
}

-(void)actionNext{
    // setup field
    
    //id_account,code,customer_id,payment_id,amount,recipient_bank_name,recipient_bank_code,description
    // validation
    
    NSString *errMsg = @"";
    if([selectedSrcAccNo isEqualToString:@""]){
        errMsg = lang(@"SELECT_DEBIT_ACC");
    }
    else if([_tfDestBank.text isEqualToString:@""]){
        errMsg = lang(@"SELECT_DEST_BANK");
    }
    else if ([_tfDestAcc.text isEqualToString:@""]){
        errMsg = lang(@"INPUT_DESC_PROXY");
    }
    else if ([_tfAmountTrf.text isEqualToString:@""]){
        errMsg = lang(@"INPUT_BIFAST_NOMINAL");
    }

    // process
    if ([errMsg isEqualToString:@""]){
        [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":selectedSrcAccNo}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"recipient_bank_name":_tfDestBank.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"recipient_bank_code":selectedBankCode}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"code":selectedBankCode}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"payment_id":_tfDestAcc.text}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"amount":[self stringNominalReplace:_tfAmountTrf.text]}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"description":_tfDescTrf.text}];
        [super openNextTemplate];
    } else {
        [self showErrorAlert:errMsg];
    }
}

-(void)addAccessoryKeypad{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    
    _tfDestBank.inputAccessoryView = keyboardDoneButtonView;
    
    
    _tfDestAcc.inputAccessoryView = keyboardDoneButtonView;
    _tfDestAcc.keyboardType = UIKeyboardTypeEmailAddress;
    
    _tfAmountTrf.inputAccessoryView = keyboardDoneButtonView;
    _tfAmountTrf.keyboardType = UIKeyboardTypeNumberPad;
    
    _tfDescTrf.inputAccessoryView = keyboardDoneButtonView;
    _tfDescTrf.keyboardType = UIKeyboardTypeDefault;
    _tfDescTrf.delegate = self;
}

- (IBAction)doneClicked:(id)sender {
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat keyboardShowupViewHeight = self.view.superview.frame.size.height-self->keyboardSize.height-(self.vwFooter.frame.size.height+12);
        CGRect f = CGRectMake(0,self.vwContent.frame.origin.y,self.view.frame.size.width,keyboardShowupViewHeight);
        self.vwContent.frame = f;
        self.heightVwFooter.constant = 0;
        [self.vwFooter setHidden:YES];
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.2 animations:^{
        CGFloat keyboardHiddenViewHeight = self->originHeightContent;//self.view.superview.frame.size.height;
        CGRect f = CGRectMake(0,self.vwContent.frame.origin.y,self.view.frame.size.width,keyboardHiddenViewHeight);
        self.vwContent.frame = f;
        self.heightVwFooter.constant = 80;
        [self.vwFooter setHidden:NO];
    }];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
            if(!isRequested){
                isRequested = YES;
                [self getListAccountFromPersitance:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                
            }
        }else{
            
            UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self backToR];
            }]];
            [self presentViewController:alerts animated:YES completion:nil];
            
        }
    }
    
    if([requestType isEqualToString:@"list_bank_bifast"]){
        if(jsonObject && [jsonObject isKindOfClass:[NSArray class]]){
            NSArray *listData = (NSArray *)jsonObject;
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            for(NSDictionary *temp in listData){
                [newData addObject:@{@"title":@"",@"label":[temp valueForKey:@"name"],@"code":[temp valueForKey:@"code"]}];
            }
            listDataBank = newData;
            [self reloadDataFromRecently];
            [self.vwContent setHidden:NO];
        } else if (jsonObject){
            if ([jsonObject objectForKey:@"response_code"]) {
                if (![[jsonObject objectForKey:@"response_code"] isEqualToString:@"00"]) {
                    NSString *errMsg = [jsonObject objectForKey:@"response"];
                    [self showErrorAlert:errMsg];
                }
            }
        }
    }
    
    if([requestType isEqualToString:@"fav"] || [requestType isEqualToString:@"list_favorite"]){
        if(jsonObject && [jsonObject isKindOfClass:[NSArray class]]){
            listDataFav = [self getListFavoriteFormatData:jsonObject];
            if ([listDataFav count] == 0) {
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                NSString *lang = [[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0];
                NSString *errMsg = @"";
                if([lang isEqualToString:@"id"]){
                    errMsg = @"Daftar favorit kosong";
                } else {
                    errMsg = @"Favorites list is empty";
                }
//                [self setOptionTransferView:0];
                [self showErrorAlert:errMsg];
            }
        }
    }
}

- (void)errorLoadData:(NSError *)error {
    
    UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
    [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self backToR];
    }]];
    [self presentViewController:alerts animated:YES completion:nil];
}

- (void)reloadApp {
    [self backToRoot];
    [BSMDelegate reloadApp];
}

-(void) getListAccountFromPersitance : (NSString *) urlParam strMenuId : (NSString *) menuId {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct = nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
            if ([dataManager.dataExtra objectForKey:@"code"]) {
                if ([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
        }
    }
    
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];
                
            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct != nil) {
        isRequested = true;
        if(listAcct.count == 0){
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki nomor rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account number for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            newData = [self getListAccountFormatData:listAcct];
            mustAddToManager = true;
            key = @"id_account";
            listAction = newData;
            listDataAcc = newData;
        }
    }else{
        if (!isRequested) {
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            
            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
        }else{
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    [self reloadDataFromRecently];
}

- (NSMutableArray*)getListFavoriteFormatData:(NSArray*)listFavs{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listFavs){
        NSArray *labelArray = [[temp valueForKey:@"name"] componentsSeparatedByString: @"\n"];
        NSString *labelTitle = @"";
        NSString *labelBank = @"";
        NSString *labelAccNo= @"";
        NSString *labelAccName = @"";
        if(labelArray.count == 4){
            labelTitle = labelArray[0];
            labelBank = labelArray[1];
            labelAccNo = labelArray[2];
            labelAccName = labelArray[3];
        } else {
            labelBank = labelArray[0];
            labelAccNo = labelArray[1];
            labelAccName = labelArray[2];
        }
        NSString *label = [NSString stringWithFormat:@"%@\n%@\n%@",labelBank,labelAccNo,labelAccName];
        [newData addObject:@{@"title":labelTitle,@"label":label,@"params":[temp valueForKey:@"url_parm"],@"id_favorite":[temp valueForKey:@"id_favorite"]}];
    }
    return newData;
}

- (NSMutableArray*)getListAccountFormatData:(NSArray*)listAcct{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listAcct){
        NSString *title = @"";
        NSString *label = @"";
        if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
            if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                title = [NSString stringWithFormat:@"%@",[temp valueForKey:@"altname"]];
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else{
                label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
            }
        }
        else{
            label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
        }
        [newData addObject:@{@"title":title,@"label":label,@"id_account":[temp valueForKey:@"id_account"]}];
    }
    
    return newData;
}

- (void)showErrorAlert:(NSString *)message{
    
    UIAlertController *alerts = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",message] preferredStyle:UIAlertControllerStyleAlert];
    [alerts addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    }]];
    [self presentViewController:alerts animated:YES completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField == _tfDescTrf){
        NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if(newText.length <= maxLengtDescription){
//            _lblLimitKeterangan.text = [NSString stringWithFormat:@"%lu /%d", (unsigned long)newText.length, maxLengthKeterangan];
            return YES;
        }else{
            return NO;
        }

    }
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}
@end
