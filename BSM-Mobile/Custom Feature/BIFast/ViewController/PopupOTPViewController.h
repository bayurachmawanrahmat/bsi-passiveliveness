//
//  PopupOTPViewController.h
//  BSM-Mobile
//
//  Created by Amini on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "TemplateViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PopupOTPViewController : UIViewController{
    id delegate;
    NSString *requestyp;
}

- (void) setDelegate:(id)newDelegate;
- (void) setTitleBar:(NSString*)title;
- (void) setOTPLine:(NSString*)line andValue:(NSString*)value;
- (void) setOTPExpiredTime:(double)time;
- (void) setRequestType:(NSString*)request_type;


@end

@protocol PopupOTPDelegate

@required

- (void) otpSucceed;

@end

NS_ASSUME_NONNULL_END
