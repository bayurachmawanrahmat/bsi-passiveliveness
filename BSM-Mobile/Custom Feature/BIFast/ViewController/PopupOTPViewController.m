//
//  PopupOTPViewController.m
//  BSM-Mobile
//
//  Created by Amini on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "PopupOTPViewController.h"
#import "KeynumbCell.h"
#import <QuartzCore/QuartzCore.h>

NS_ENUM(NSInteger, OTPSTATE){
    REQUEST = 0,
    VERIFY = 1
};

@interface PopupOTPViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, ConnectionDelegate>{
    
    NSArray *listNumber;
    
    NSString *titlebar;
    NSString *otpLine;
    NSString *otpLineValue;
    double otpTime;
    
    NSInteger position;
    
    NSTimer *timer;
    int currMinute;
    int currSecond;
    
    NSInteger otpState;
}
@property (weak, nonatomic) IBOutlet UIView *vwBackground;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *titlebarTop;

@property (weak, nonatomic) IBOutlet UIStackView *viewParentField;
@property (weak, nonatomic) IBOutlet UITextField *textField1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;
@property (weak, nonatomic) IBOutlet UITextField *textField3;
@property (weak, nonatomic) IBOutlet UITextField *textField4;
@property (weak, nonatomic) IBOutlet UITextField *textField5;
@property (weak, nonatomic) IBOutlet UITextField *textField6;

@property (weak, nonatomic) IBOutlet UILabel *labelWrongOTP;

@property (weak, nonatomic) IBOutlet UILabel *labelLine;
@property (weak, nonatomic) IBOutlet UILabel *labelLineValue;
@property (weak, nonatomic) IBOutlet UILabel *labelTimer;

@property (weak, nonatomic) IBOutlet UICollectionView *keypadCollection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keypadCollectionHeight;

@end

@implementation PopupOTPViewController

- (IBAction)actionClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    position = 1;
    
    currMinute=0;
    currSecond=otpTime;
    
    listNumber = @[@"1", @"2", @"3",
                   @"4", @"5", @"6",
                   @"7", @"8", @"9",
                   @"del",@"0",@"ok"];
    
    [_keypadCollection setDataSource:self];
    [_keypadCollection setDelegate:self];
    [_keypadCollection registerNib:[UINib nibWithNibName:@"KeynumbCell" bundle:nil] forCellWithReuseIdentifier:@"KeynumbCell"];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [layout setSectionInset:UIEdgeInsetsMake(5, 5, 0, 0)];
    _keypadCollection.collectionViewLayout = layout;
    
    
    _titlebarTop.text = titlebar;
    
    if([Utility isLanguageID]){
        _labelLine.text = [NSString stringWithFormat:@"Kode OTP dikirimkan via %@", otpLine];
        _labelLineValue.text = otpLineValue;
        _labelTimer.text = [NSString stringWithFormat:@"Kirim ulang OTP (setelah %.0f detik)", otpTime];
    }else{
        _labelLine.text = [NSString stringWithFormat:@"OTP Code sent via %@", otpLine];
        _labelLineValue.text = otpLineValue;
        _labelTimer.text = [NSString stringWithFormat:@"Resend OTP (after %0.f seconds)", otpTime];
    }
    
    
    [_labelTimer setUserInteractionEnabled:NO];
    [_labelTimer addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionRequestOTP)]];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:_labelTimer.text];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    [_labelTimer setAttributedText:attributeString];
    
    _vwBackground.layer.cornerRadius = 10;
    _vwBackground.layer.borderWidth = 2;
    _vwBackground.layer.borderColor = [UIColorFromRGB(const_color_secondary)CGColor];
    
    [_textField1 setTag:1];
    [_textField2 setTag:2];
    [_textField3 setTag:3];
    [_textField4 setTag:4];
    [_textField5 setTag:5];
    [_textField6 setTag:6];
    
    _textField1.delegate = self;
    _textField2.delegate = self;
    _textField3.delegate = self;
    _textField4.delegate = self;
    _textField5.delegate = self;
    _textField6.delegate = self;
    
    [Styles addShadow:self.view];
    [self setupSytleField];
}

- (void)viewDidAppear:(BOOL)animated{
    CGFloat height = self.keypadCollection.collectionViewLayout.collectionViewContentSize.height;
    self.keypadCollectionHeight.constant = height;
    
    [self requestOTP];
}

- (void)setDelegate:(id)newDelegate{
    delegate = newDelegate;
}

- (void)setTitleBar:(NSString *)title{
    titlebar = title;
}

- (void)setOTPLine:(NSString *)line andValue:(nonnull NSString *)value{
    otpLine = line;
    otpLineValue = value;
}

- (void)setOTPExpiredTime:(double)time{
    otpTime = time;
}

#pragma mark collectionViewHandler

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return listNumber.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    KeynumbCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"KeynumbCell" forIndexPath:indexPath];
    
    cell.label.text = [listNumber[indexPath.row] uppercaseString];
    cell.image.hidden = YES;
    
    if([listNumber[indexPath.row] isEqualToString:@"del"]){
        cell.image.image = [UIImage imageNamed:@"ic_inkbr_remove"];
        cell.image.hidden = NO;
        cell.label.hidden = YES;
    }else{
        cell.contentView.layer.borderWidth = 1;
        cell.contentView.layer.borderColor = [UIColorFromRGB(const_color_primary)CGColor];
        cell.contentView.layer.cornerRadius = 8;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%f",collectionView.frame.size.width);
    double size = ceil((collectionView.frame.size.width-40)/3);
    NSLog(@"%f",size);
    return CGSizeMake(size, size/2);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self actionNumber:listNumber[indexPath.row]];
}

#pragma mark UITextfieldDelegate...
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == _textField6){
        NSString* newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if(newText.length == 1){
            textField.text = newText;
            
            return NO;
        }
    }
    return YES;
}

- (UITextField*) findCurrentField{
    for (UIView *current in _viewParentField.subviews) {
        
        UITextField *tfield = [(UITextField*)current viewWithTag:position];
        if([tfield.text isEqualToString:@""]) return tfield;
    }
    return nil;
}

- (BOOL) validate{
    NSString *inputOTP = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                          _textField1.text,
                          _textField2.text,
                          _textField3.text,
                          _textField4.text,
                          _textField5.text,
                          _textField6.text];
    if(inputOTP.length == 6){
        [[DataManager sharedManager].dataExtra setValue:inputOTP forKey:@"otp"];
        return YES;
    }
    return NO;
}

- (void)actionNext{
    if([self validate]){
        [self verifyOTP];
    }
}

- (void) actionNumber:(NSString*)key{
    if([key isEqualToString:@"del"]){
        [self actionDelete];
    }else if([key isEqualToString:@"ok"]){
        [self actionNext];
    }else{
        UITextField *activeField = [self findCurrentField];
        if(activeField){
            [activeField setText:key];
            position = position + 1;
        }else{
            [self actionNext];
        }
    }
}

- (void) actionDelete{
    if(position > 1){
        position = position - 1;
    }
    for (UIView *current in _viewParentField.subviews) {
        
        UITextField *tfield = [(UITextField*)current viewWithTag:position];
        tfield.text = @"";
    }
}

- (void) actionRequestOTP{
    [self requestOTP];
}

- (void) otpFailed{
    for (UIView *current in _viewParentField.subviews) {
        if([current isKindOfClass:[UITextField class]]){
            UITextField *tField = (UITextField*)current;
            [tField.layer setBorderColor:[[UIColor redColor]CGColor]];
            [tField.layer setBorderWidth:1];
        }
    }
    [self.labelWrongOTP setHidden:NO];
    [self shake:_viewParentField];
}

- (void) setupSytleField{
    for (UIView *current in _viewParentField.subviews) {
        if([current isKindOfClass:[UITextField class]]){
            UITextField *tField = (UITextField*)current;
            [tField.layer setBorderColor:[UIColorFromRGB(const_color_gray) CGColor]];
            [tField.layer setBorderWidth:1];
            [tField.layer setCornerRadius:8];
        }
    }
    [self.labelWrongOTP setHidden:YES];
}

- (void) shake:(UIView*)view{
    CABasicAnimation *animation =
                             [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:4];
    [animation setAutoreverses:NO];
    [animation setFromValue:[NSValue valueWithCGPoint:
                   CGPointMake([view center].x - 10.0f, [view center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                   CGPointMake([view center].x + 10.0f, [view center].y)]];
    [[_viewParentField layer] addAnimation:animation forKey:@"position"];
}


- (void) startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
}

- (void) timerFired
{
    if(currSecond>0)
    {
        currSecond-=1;
        if(currSecond>-1)
            
            [_labelTimer setText:[NSString stringWithFormat:@" 00:%02d", currSecond]];
        
        if([Utility isLanguageID]){
            _labelTimer.text = [NSString stringWithFormat:@"Kirim ulang OTP (setelah 00:%02d detik)", currSecond];
        }else{
            _labelTimer.text = [NSString stringWithFormat:@"Resend OTP (after 00:%02d seconds)", currSecond];
        }
        [_labelTimer setUserInteractionEnabled:false];
    }
    else
    {
        [timer invalidate];
        [_labelTimer setUserInteractionEnabled:true];
        _labelTimer.text = lang(@"RESEND_OTP");
    }
}

- (void)setRequestType:(NSString *)request_type{
    requestyp = request_type;
}

- (void) requestOTP{
    otpState = REQUEST;
    
    currMinute=0;
    currSecond=otpTime;
    
    [[DataManager sharedManager].dataExtra setValue:[NSUserdefaultsAes getValueForKey:@"msisdn"] forKey:@"msisdn"];
    
    NSString *urlData = [NSString stringWithFormat:@"request_type=%@,step=request_otp,menu_id,device,device_type,ip_address,language,date_local,id_account,customer_id,proxyType,proxyValue,transaction_id,msisdn,code",requestyp];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

- (void) verifyOTP{
    otpState = VERIFY;
    [[DataManager sharedManager].dataExtra setValue:[NSUserdefaultsAes getValueForKey:@"msisdn"] forKey:@"msisdn"];

    NSString *urlData = [NSString stringWithFormat:@"request_type=%@,step=verify_otp,otp,menu_id,device,device_type,ip_address,language,date_local,id_account,customer_id,proxyType,proxyValue,transaction_id,msisdn,code",requestyp];
    Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
}

- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if ([requestType isEqualToString:requestyp]) {
        
        NSString *rc = [jsonObject valueForKey:@"response_code"];
        
        if(otpState == VERIFY){
            if([rc isEqualToString:@"00"]){
                [self dismissViewControllerAnimated:YES completion:^(){
                    [self->delegate otpSucceed];
                }];
            }else{
                self.labelWrongOTP.text = [NSString stringWithFormat:@"%@",[jsonObject valueForKey:@"response"]];
                [self otpFailed];
            }
        }else{
            if(![rc isEqualToString:@"00"]){
                UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
                [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self dismissViewControllerAnimated:YES completion:nil];
                }]];
                [self presentViewController:alertCont animated:YES completion:nil];
            }
            
            [self startTimer];
        }
    }
}

@end
