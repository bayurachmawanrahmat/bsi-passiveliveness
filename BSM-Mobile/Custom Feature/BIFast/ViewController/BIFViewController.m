//
//  BIFViewController.m
//  BSM-Mobile
//
//  Created by Amini on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFViewController.h"
#import "SubmenuCell.h"

@interface BIFViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate>{
    
    NSArray *listAction;
}

@property (weak, nonatomic) IBOutlet UILabel *titleBar;
@property (weak, nonatomic) IBOutlet UICollectionView *cvSubmenu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;

@end

@implementation BIFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    self.titleBar.text = [self.jsonData valueForKey:@"title"];
    [dataManager.dataExtra setValue:[self.jsonData valueForKey:@"title"] forKey:@"menu_title"];
    
    listAction = [self.jsonData objectForKey:@"action"];
    [_cvSubmenu setDataSource:self];
    [_cvSubmenu setDelegate:self];
    [_cvSubmenu registerNib:[UINib nibWithNibName:@"SubmenuCell" bundle:nil] forCellWithReuseIdentifier:@"SubmenuCell"];
    
}

#pragma mark UICollectionViewDataSource etc..
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return listAction.count;
}

#pragma mark UICollectionViewDelegate etc..

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SubmenuCell *cell = [collectionView  dequeueReusableCellWithReuseIdentifier:@"SubmenuCell" forIndexPath:indexPath];
    NSDictionary *selectedMenu = [[listAction objectAtIndex:indexPath.row] objectAtIndex:1];
    NSString *titleSubmenu = [selectedMenu objectForKey:@"menu_name"];
    
    switch (indexPath.row) {
        case 0:
            [cell.smImage setImage:[UIImage imageNamed:@"ic_bifast_menu_createproxy"]];
            break;
        case 1:
            [cell.smImage setImage:[UIImage imageNamed:@"ic_bifast_menu_information"]];
            break;
        case 2:
            [cell.smImage setImage:[UIImage imageNamed:@"ic_bifast_menu_transfer"]];
            break;
        case 3:
            [cell.smImage setImage:[UIImage imageNamed:@"ic_bifast_menu_transactionstatus"]];
            break;
        default:
            [cell.smImage setImage:[UIImage imageNamed:@"ic_bifast_menu_transfer"]];
            break;
    }
    cell.smLabel.text = titleSubmenu;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(CGRectGetWidth(collectionView.frame), (CGRectGetHeight(collectionView.frame)));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSArray *selectedMenu = [listAction objectAtIndex:indexPath.row];
    [dataManager setAction:[[selectedMenu objectAtIndex:1]objectForKey:@"action"] andMenuId:[selectedMenu objectAtIndex:0]];
    [self openNextTemplate];
    
}

@end
