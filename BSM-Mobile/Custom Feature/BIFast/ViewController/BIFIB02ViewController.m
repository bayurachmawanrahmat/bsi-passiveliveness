//
//  BIFIB02ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 30/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFIB02ViewController.h"
#import "PLD01ViewController.h"
#import "BIFFieldView.h"
#import "PopupOTPViewController.h"
#import "BIFCF01ViewController.h"


@interface BIFIB02ViewController ()<PLD01ViewDelegate, UITextFieldDelegate>{
    BOOL isRequested;
    BOOL mustAddToManager;
    
    NSString *key;
    NSString *selectedSrcAccNo;
    
    
    NSInteger selectedSrcAccIdx;
    
    NSArray *listAction;
    NSArray *listDataAcc;
    
    NSMutableArray *listProxyType;
    
    NSDictionary *data;
    
    CGSize keyboardSize;
    CGFloat originHeightContent;
    
    UIToolbar *keyboardDoneButtonView;
    
    NSString *codeMenu;
    NSString *otpLine, *otpValue;
}

@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIView *vwFooter;
@property (weak, nonatomic) IBOutlet UIView *vwNavBar;

@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;

@property (weak, nonatomic) IBOutlet UILabel *lblSourceAccAltname;
@property (weak, nonatomic) IBOutlet UILabel *lblSourceAcc;
@property (weak, nonatomic) IBOutlet UIView *bgSourceAcc;
@property (weak, nonatomic) IBOutlet UIImageView *btnSourceAcc;

@property (weak, nonatomic) IBOutlet UIScrollView *vwScroll;
@property (weak, nonatomic) IBOutlet UIStackView *stackViewField;

@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *heightVwFooter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMargin;

@end

@implementation BIFIB02ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topMargin];
    originHeightContent = _vwContent.frame.size.height;
    
    _lblTitleNav.text = [self.jsonData valueForKey:@"title"];
    
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(navigationBack)]];
    
    [self.bgSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    [self.btnSourceAcc addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionBtnSourceAcc)]];
    
    // getListAccount
    [self getListAccountFromPresistence:[self.jsonData valueForKey:@"url_param"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
    
    [self.btnNext setTitle:[lang(@"NEXT") uppercaseString] forState:UIControlStateNormal];
    [self.btnNext addTarget:self action:@selector(actionNext) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self addAccessoryKeypad];
    
    codeMenu = [dataManager.dataExtra valueForKey:@"code"];
    
}

- (void)gotoOTP{
    
    if([[dataManager.dataExtra valueForKey:@"proxy_type"] isEqualToString:@"01"]){
        otpLine = lang(@"LABEL_NOHAPE");
    }else{
        otpLine = @"Email";
    }
    otpValue = [dataManager.dataExtra valueForKey:@"proxy_value"];
    [dataManager.dataExtra setValue:[dataManager.dataExtra valueForKey:@"proxy_value"] forKey:@"proxyValue"];
    [dataManager.dataExtra setValue:[dataManager.dataExtra valueForKey:@"proxy_type"] forKey:@"proxyType"];
    [dataManager.dataExtra setValue:otpLine forKey:@"proxyTypeValue"];
    
    PopupOTPViewController *otp = (PopupOTPViewController*)[self routeTemplateController:@"POPUPOTP"];
    
    [otp setRequestType:@"inq_maintenance_proxy"];
    
    if([Utility isLanguageID]){
        [otp setTitleBar:@"Masukkan OTP"];
    }else{
        [otp setTitleBar:@"Insert OTP"];
    }
    [otp setOTPLine:otpLine andValue:otpValue];
    [otp setOTPExpiredTime:60];
    [otp setDelegate:self];
    [self presentViewController:otp animated:YES completion:nil];
}

- (void)otpSucceed{
    [self openNextTemplate];
}

- (void)viewDidAppear:(BOOL)animated{

    [self setupField];
}

- (void) setupField{
    NSArray *listField = [dataManager.dataExtra valueForKey:@"input_field"];
    
    if(listField.count > 0){
        [self removeAllArrangedViewOf:self.stackViewField];
        
        for (NSDictionary *dict in listField){
            BIFFieldView *fieldView = [[BIFFieldView alloc]init];
            
            [fieldView setTextTitle:[dict valueForKey:@"label"]];
            
            fieldView.textField.text = [dict valueForKey:@"value"];
            fieldView.textField.delegate = self;
            fieldView.textField.inputAccessoryView = keyboardDoneButtonView;
            
            if([[dict valueForKey:@"enabled"] isEqualToString:@"NO"]){
                fieldView.textField.enabled = NO;
                
            }
            
            [self.stackViewField addArrangedSubview:fieldView];
        }
        
    }
    
    NSString *oldIdAccount = [dataManager.dataExtra valueForKey:@"account_id_proxy_old"];
    if(oldIdAccount){
        int indxRow = 0;
        for(NSDictionary *dataAcc in listDataAcc){
            if([[dataAcc valueForKey:@"id_account"] isEqualToString:oldIdAccount]){
                    
                    selectedSrcAccIdx = indxRow;
                    NSString *titleAltname = [dataAcc objectForKey:@"title"];
                    if ([titleAltname isEqualToString:@""]){
                        _lblSourceAccAltname.hidden = YES;
                    } else {
                        _lblSourceAccAltname.hidden = NO;
                    }
                    _lblSourceAccAltname.text = titleAltname;
                    _lblSourceAcc.text = [dataAcc objectForKey:@"label"];
                    selectedSrcAccNo = [dataAcc objectForKey:@"id_account"];
                
                    indxRow = indxRow + 1;
            }
        }
    }
}

-(void) getListAccountFromPresistence : (NSString *) urlParam strMenuId : (NSString *) menuId {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUrlParam = (NSDictionary *) [Utility translateParam:urlParam];
    NSArray *arRoleSpesial = [[userDefault valueForKey:OBJ_ROLE_SPESIAL]componentsSeparatedByString:@","];
    NSArray *arRoleFinance = [[userDefault valueForKey:OBJ_ROLE_FINANCE]componentsSeparatedByString:@","];
    BOOL isSpesial, isFinance;
    isSpesial = false;
    isFinance = false;
    NSArray *listAcct = nil;
    
    if (arRoleSpesial.count > 0 || arRoleSpesial != nil) {
        for(NSString *xmenuId in arRoleSpesial){
            if ([[dictUrlParam valueForKey:@"code"]boolValue]) {
                if ([[dictUrlParam objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
            if ([dataManager.dataExtra objectForKey:@"code"]) {
                if ([[dataManager.dataExtra objectForKey:@"code"] isEqualToString:xmenuId]) {
                    isSpesial = true;
                    break;
                }
            }
            
        }
    }
    
    
    if (isSpesial) {
        listAcct = (NSArray *) [userDefault objectForKey:OBJ_SPESIAL];
    }else{
        if (arRoleFinance.count > 0 || arRoleFinance != nil) {
            for(NSString *xmenuId in arRoleFinance){
                if ([xmenuId isEqualToString:menuId]) {
                    isFinance = true;
                    break;
                }
            }
            
            if (isFinance) {
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_FINANCE];
                
            }else{
                listAcct = (NSArray *) [userDefault objectForKey:OBJ_ALL_ACCT];
            }
        }
    }
    
    if (listAcct != nil) {
        isRequested = true;
        if(listAcct.count == 0){
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki nomor rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account number for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }else{
            NSMutableArray *newData = [[NSMutableArray alloc]init];
            newData = [self getListAccountFormatData:listAcct];
            mustAddToManager = true;
            key = @"id_account";
            listAction = newData;
            listDataAcc = newData;
        }
    }else{
        if (!isRequested) {
            [dataManager setMenuId:[self.jsonData valueForKey:@"menu_id"]];
            
            NSString *strUrl = [NSString stringWithFormat:@"request_type=list_account2,customer_id"];
            Connection *conn = [[Connection alloc]initWithDelegate:self];
            [conn sendPostParmUrl:strUrl needLoading:true encrypted:true banking:true favorite:false];
        }else{
            NSString *mes = @"";
            if([[[userDefault objectForKey:@"AppleLanguages"]objectAtIndex:0]isEqualToString:@"id"]){
                mes = @"Maaf, anda tidak memiliki rekening yang dibutuhkan untuk fitur ini";
            }else{
                mes = @"Sorry, you do not have account for this feature";
            }
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:mes
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                [self backToR];
            }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (NSMutableArray*)getListAccountFormatData:(NSArray*)listAcct{
    NSMutableArray *newData = [[NSMutableArray alloc]init];
    for(NSDictionary *temp in listAcct){
        NSString *title = @"";
        NSString *label = @"";
        if([temp objectForKey:@"altname"] != nil && [temp objectForKey:@"type"] != nil){
            if(![[temp valueForKey:@"altname"] isEqualToString:@""]){
                title = [NSString stringWithFormat:@"%@",[temp valueForKey:@"altname"]];
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else if(![[temp valueForKey:@"type"] isEqualToString:@""]){
                label = [NSString stringWithFormat:@"%@ - %@",[temp valueForKey:@"name"], [temp valueForKey:@"type"]];
            }else{
                label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
            }
        }
        else{
            label = [NSString stringWithFormat:@"%@",[temp valueForKey:@"name"]];
        }
        [newData addObject:@{@"title":title,@"label":label,@"id_account":[temp valueForKey:@"id_account"]}];
    }
    
    return newData;
}

- (void)showErrorAlert:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:message preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }

    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)reloadApp {
    [self backToRoot];
    [BSMDelegate reloadApp];
}

- (void)errorLoadData:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"message"] ] preferredStyle:UIAlertControllerStyleAlert];
    if (@available(iOS 13.0, *)) {
        [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - ConnectionDelegate
- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    if([requestType isEqualToString:@"zpk"]){
        [super didFinishLoadData:jsonObject withRequestType:requestType];
        return;
    }
    
    if([requestType isEqualToString:@"list_account2"]){
        if ([[jsonObject objectForKey:@"rc"] isEqualToString:@"00"]) {
            NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
            [userDefault setObject:[jsonObject valueForKey:OBJ_FINANCE] forKey:OBJ_FINANCE];
            [userDefault setObject:[jsonObject valueForKey:OBJ_SPESIAL] forKey:OBJ_SPESIAL];
            [userDefault setObject:[jsonObject valueForKey:OBJ_ALL_ACCT] forKey:OBJ_ALL_ACCT];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_FINANCE] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_FINANCE];
            [userDefault setObject:[[[jsonObject valueForKey:OBJ_ROLE_SPESIAL] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"\n" withString:@""] forKey:OBJ_ROLE_SPESIAL];
            [userDefault synchronize];
            
            if(!isRequested){
                isRequested = YES;
                [self getListAccountFromPresistence:[self.jsonData valueForKey:@"url_parm"] strMenuId:[self.jsonData valueForKey:@"menu_id"]];
                
            }
        }else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@",ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}

-(void)keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.vwScroll.contentInset = contentInsets;
    self.vwScroll.scrollIndicatorInsets = contentInsets;
}



#pragma mark actions buttons and selectors

- (void)actionBtnSourceAcc{
    PLD01ViewController *dialogList = (PLD01ViewController*)[self routeTemplateController:@"PLD01VC"];
    [dialogList setDelegate:self];
    [dialogList setTitle:_lblSourceAcc.text];
    [dialogList setPlaceholder:lang(@"SEARCH_ACC_NO")];
    [dialogList setFieldName:@"fieldSourceAcc"];
    [dialogList setList:listDataAcc];
    [dialogList setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogList setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogList animated:YES completion:nil];
}

- (void)navigationBack{
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}


-(void)actionNext{
    
    // validation
    NSString *errMsg = @"";
    if([selectedSrcAccNo isEqualToString:@""] || selectedSrcAccNo == nil){
        errMsg = lang(@"SELECT_DEBIT_ACC");
    }
    
    // process
    if ([errMsg isEqualToString:@""]){
        
        [dataManager.dataExtra addEntriesFromDictionary:@{@"id_account":selectedSrcAccNo}];
        [dataManager.dataExtra addEntriesFromDictionary:@{@"account_id_proxy_new":selectedSrcAccNo}];
        
        if([codeMenu isEqualToString:@"00304"]){
            [self gotoOTP];
        }else{
            if([codeMenu isEqualToString:@"00301"]){
                if([self checkWithOldAccount]){
                    [self openNextTemplate];
                }else{
                    //Rekening terpilih sudah terdaftar / Selected account has been registered
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ALERT_SAME_OLD_ACCOUNT") preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }else{
                [super openNextTemplate];
            }
        }
    } else {
        [self showErrorAlert:errMsg];
    }
}

- (BOOL) checkWithOldAccount{
    if([[dataManager.dataExtra valueForKey:@"account_id_proxy_old"] isEqualToString:selectedSrcAccNo]){
        return false;
    }
    return true;
}

- (BOOL) checkSameAccount{
    NSArray *listDataProxy = [dataManager.dataExtra valueForKey:@"list_data_proxy"];
    for(NSDictionary *dict in listDataProxy){
        if([[dict valueForKey:@"account_id"] isEqualToString:selectedSrcAccNo]){
            return false;
            break;
        }
    }
    return true;
}

- (void)selectedRow:(NSInteger)indexRow withList:(NSArray *)arrData fieldName:(NSString *)fname{
    data = [arrData objectAtIndex:indexRow];
    if ([fname isEqualToString:@"fieldSourceAcc"]) {
        selectedSrcAccIdx = indexRow;
        NSString *titleAltname = [data objectForKey:@"title"];
        if ([titleAltname isEqualToString:@""]){
            _lblSourceAccAltname.hidden = YES;
        } else {
            _lblSourceAccAltname.hidden = NO;
        }
        _lblSourceAccAltname.text = titleAltname;
        _lblSourceAcc.text = [data objectForKey:@"label"];
        selectedSrcAccNo = [data objectForKey:@"id_account"];
    } 
}


-(void)addAccessoryKeypad{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:lang(@"DONE")
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    

}

- (IBAction)doneClicked:(id)sender {
    [self.view endEditing:YES];
}

- (void)removeAllArrangedViewOf:(UIStackView*) stackview{
    
    for(UIView* subview in stackview.arrangedSubviews){
        [stackview removeArrangedSubview:subview]; // <-- Removes Constraints as well
        [subview removeConstraints:subview.constraints];//  <--- Constraints will be invalid or non existing
        [subview removeFromSuperview];
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

@end
