//
//  BIFCF01ViewController.m
//  BSM-Mobile
//
//  Created by Amini on 15/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFCF01ViewController.h"
#import "PopupPinTrxViewController.h"
#import "BIFCFView.h"
#import "PopupOTPViewController.h"
@interface BIFCF01ViewController ()<PopupPinTrxViewDelegate>{
    NSArray *listContent;
    NSString *codeMenu;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstant;
@property (weak, nonatomic) IBOutlet UIView *vwNavBar;
@property (weak, nonatomic) IBOutlet UIImageView *navBackBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNav;
@property (weak, nonatomic) IBOutlet UIView *vwContent;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;

@property (weak, nonatomic) IBOutlet UIView *vwInfoAccount;
@property (weak, nonatomic) IBOutlet UIView *vwSourceAccount;

@property (weak, nonatomic) IBOutlet UILabel *lblKonfirmasi;

@property (weak, nonatomic) IBOutlet UILabel *lblNamaNasabah;
@property (weak, nonatomic) IBOutlet UILabel *valNamaNasabah;

@property (weak, nonatomic) IBOutlet UILabel *lblNomorRekening;
@property (weak, nonatomic) IBOutlet UILabel *valNomorRekening;

@property (weak, nonatomic) IBOutlet UILabel *lblProxyType;
@property (weak, nonatomic) IBOutlet UILabel *valProxyType;

@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet CustomBtn *btnNext;
@end

@implementation BIFCF01ViewController

NSString *codePorting = @"00304";
NSString *codeActivation = @"00303";
NSString *codeUpdate = @"00301";
NSString *codeDeregistration = @"00300";
NSString *codeBlokir = @"00302";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Styles setTopConstant:_topConstant];
    
    // Setup navigationn
    [self.navBackBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBack)]];
    _lblTitleNav.text = [dataManager.dataExtra valueForKey:@"menu_title"];
    _lblKonfirmasi.text = [self.jsonData valueForKey:@"title"];
    
    self.lblNamaNasabah.text = lang(@"LABEL_CUSTNAME");
    self.lblNomorRekening.text = lang(@"LABEL_ACCNO");
    
    [self.vwInfoAccount.layer setCornerRadius:10.0f];
    
    [_vwContent setHidden:true];
    
    [_lblInfo setText:@"..."];
    [_btnNext setTitle:@"..." forState:UIControlStateNormal];
    [_btnNext addTarget:self action:@selector(actionBtnTransfer) forControlEvents:UIControlEventTouchUpInside];
    
    [self removeAllArrangedViewOf:_stackView];
    
    codeMenu = [dataManager.dataExtra valueForKey:@"code"];
}

- (void)viewDidAppear:(BOOL)animated{
    [self doRequest];
}

- (void)navigationBack{
    dataManager.currentPosition--;
    UINavigationController *navigationCont = self.navigationController;
    [navigationCont popViewControllerAnimated:YES];
}

- (void)actionBtnTransfer{
    PopupPinTrxViewController *dialogPin = (PopupPinTrxViewController*)[self routeTemplateController:@"POPUPPIN"];
    [dialogPin setDelegate:self];
    [dialogPin setModalPresentationStyle:UIModalPresentationOverFullScreen];
    [dialogPin setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:dialogPin animated:YES completion:nil];
}


- (void) loadContent{
    if(listContent.count > 0){
        [self removeAllArrangedViewOf:_stackView];
        
        for (NSDictionary *dict in listContent){
            BIFCFView *contentView = [[BIFCFView alloc]init];
            contentView.labelTitle.text = [dict objectForKey:@"title"];
            contentView.labelValue.text = [dict objectForKey:@"value"];
            [_stackView addArrangedSubview:contentView];
        }
        
    }
}

- (void)doRequest{
    BOOL needRequest = [[self.jsonData valueForKey:@"url"]boolValue];
    if(needRequest){
        NSString *urlData = [NSString stringWithFormat:@"%@,proxyTypeValue",[self.jsonData valueForKey:@"url_parm"]];
        Connection *conn = [[Connection alloc]initWithDelegate:self];
        [conn sendPostParmUrl:urlData needLoading:true encrypted:true banking:true favorite:nil];
    }
}

- (void)doCheckPin{
    Connection *conn = [[Connection alloc]initWithDelegate:self];
    [conn sendPostParmUrl:[NSString stringWithFormat:@"request_type=verify_pin,pin,language"] needLoading:true encrypted:true banking:true favorite:[[self.jsonData valueForKey:@"favorite"]boolValue]];
}


- (void)didFinishLoadData:(id)jsonObject withRequestType:(NSString *)requestType{
    
    if([requestType isEqualToString:@"registration_bi_fast"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [_vwContent setHidden:false];
            [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject objectForKey:@"transaction_id"]}];
            [[NSUserDefaults standardUserDefaults] setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
            
            @try {
                
                NSDictionary *data = [response objectForKey:@"data"];
                
                self.valNamaNasabah.text = [data valueForKey:@"name"];
                self.valNomorRekening.text = [data valueForKey:@"account"];
                
                
                listContent = @[
                                 @{@"title": lang(@"LABEL_PROXYTYPE"), @"value": [data valueForKey:@"proxyTypeValue"]},
                                 @{@"title": lang(@"LABEL_TRXNO"), @"value": [data valueForKey:@"trans_ref"]},
                                 @{@"title": lang(@"LABEL_TRXDATE"), @"value": [data valueForKey:@"date_time"]},
                                 @{@"title": lang(@"LABEL_PROXYALIAS"), @"value": [data valueForKey:@"proxyValue"]}
                                ];
                
                [_lblInfo setText:lang(@"LABEL_INFO_ACTIVATE")];
                [_btnNext setTitle:lang(@"BTN_ACTIVATE") forState:UIControlStateNormal];
                
            } @catch (NSException *exception) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            [self loadContent];
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                [self backToRoot];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
    if ([requestType isEqualToString:@"verify_pin"]) {
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [self openNextTemplate];
        } else {
            UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@\n%@", ERROR_FROM_SERVER,[jsonObject valueForKey:@"response"]] preferredStyle:UIAlertControllerStyleAlert];
            [alertCont addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertCont animated:YES completion:nil];
        }
    }
    
    if([requestType isEqualToString:@"inq_maintenance_proxy"]){
        if([[jsonObject valueForKey:@"response_code"]isEqualToString:@"00"]){
            [_vwContent setHidden:false];
            [dataManager.dataExtra setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            [dataManager.dataExtra addEntriesFromDictionary:@{@"transaction_id":[jsonObject objectForKey:@"transaction_id"]}];
            [[NSUserDefaults standardUserDefaults] setValue:[jsonObject objectForKey:@"transaction_id"] forKey:@"transaction_id"];
            
            @try {
                NSDictionary *data = [NSJSONSerialization JSONObjectWithData:[[jsonObject objectForKey:@"response"] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                    
                codeMenu = [dataManager.dataExtra valueForKey:@"code"];
                if([codeMenu isEqualToString:codeActivation]){ //activation
                    self.valNamaNasabah.text = [data valueForKey:@"account_name"];
                    self.valNomorRekening.text = [data valueForKey:@"account_id_proxy"];
                    

                    listContent = @[
                                     @{@"title": lang(@"LABEL_PROXYTYPE"), @"value": [data valueForKey:@"proxy_type_name"]},
                                     @{@"title": lang(@"LABEL_TRXNO"), @"value": [jsonObject objectForKey:@"transaction_id"]},
                                     @{@"title": lang(@"LABEL_TRXDATE"), @"value": [data valueForKey:@"transaction_date"]},
                                     @{@"title": lang(@"LABEL_PROXYALIAS"), @"value": [data valueForKey:@"proxy_value"]}
                                    ];
                    
                    [_lblInfo setText:lang(@"LABEL_INFO_ACTIVATE")];
                    [_btnNext setTitle:[lang(@"BTN_ACTIVATE") uppercaseString] forState:UIControlStateNormal];
                    
                }else if([codeMenu isEqualToString:codeUpdate]){ //update
                    self.valNamaNasabah.text = [data valueForKey:@"account_name"];
                    self.valNomorRekening.text = [data valueForKey:@"account_id_proxy_new"];
                    

                    listContent = @[
                                     @{@"title": lang(@"OLD_ACCOUNT"), @"value": [data valueForKey:@"account_id_proxy_old"]},
                                     @{@"title": lang(@"LABEL_PROXYALIAS"), @"value": [data valueForKey:@"proxy_value"]}
                                    ];
                    
                    [_lblInfo setText:lang(@"LABEL_INFO_UPDATE")];
                    [_btnNext setTitle:[lang(@"BTN_UPDATE") uppercaseString] forState:UIControlStateNormal];
                }
                else if([codeMenu isEqualToString:codeDeregistration]){ //Deregistration
                    
                    self.valNamaNasabah.text = [data valueForKey:@"account_name"];
                    self.valNomorRekening.text = [data valueForKey:@"account_id_proxy_old"];
                    

                    listContent = @[
                                        @{@"title": lang(@"LABEL_PROXYTYPE"), @"value": [self getProxyTypeValue:[data valueForKey:@"proxy_type"]]},
                                        @{@"title": lang(@"LABEL_TRXNO"), @"value": [data objectForKey:@"trx_id"]},
                                        @{@"title": lang(@"LABEL_TRXDATE"), @"value": [data valueForKey:@"date_time"]},
                                        @{@"title": lang(@"LABEL_PROXYALIAS"), @"value": [data valueForKey:@"proxy_value"]}
                                    ];
                    [dataManager.dataExtra setValue:[data objectForKey:@"account_id_proxy_old"] forKey:@"accountIdProxyOld"];
                    [dataManager.dataExtra setValue:[data objectForKey:@"proxy_value"] forKey:@"proxyValue"];
                    [dataManager.dataExtra setValue:[data objectForKey:@"proxy_type"] forKey:@"proxyType"];
                    //accountIdProxyOld
//                    proxyValue
//                    proxyType
                    
                    [_lblInfo setText:lang(@"LABEL_INFO_DEREGISTRATION")];
                    [_btnNext setTitle:[lang(@"BTN_DEREGISTRATION") uppercaseString] forState:UIControlStateNormal];
                }
                else if([codeMenu isEqualToString:codeBlokir]){ //Blokir
                    self.valNamaNasabah.text = [data valueForKey:@"account_name"];
                    self.valNomorRekening.text = [data valueForKey:@"account_id_proxy_old"];
                    

                    listContent = @[
                                        @{@"title": lang(@"LABEL_PROXYTYPE"), @"value": [self getProxyTypeValue:[data valueForKey:@"proxy_type"]]},
                                        @{@"title": lang(@"LABEL_TRXNO"), @"value": [data objectForKey:@"trx_id"]},
                                        @{@"title": lang(@"LABEL_TRXDATE"), @"value": [data valueForKey:@"date_time"]},
                                        @{@"title": lang(@"LABEL_PROXYALIAS"), @"value": [data valueForKey:@"proxy_value"]}
                                    ];
                    [dataManager.dataExtra setValue:[data objectForKey:@"account_id_proxy_old"] forKey:@"accountIdProxyOld"];
                    [dataManager.dataExtra setValue:[data objectForKey:@"proxy_value"] forKey:@"proxyValue"];
                    [dataManager.dataExtra setValue:[data objectForKey:@"proxy_type"] forKey:@"proxyType"];
                    //accountIdProxyOld
//                    proxyValue
//                    proxyType
                    
                    [_lblInfo setText:lang(@"LABEL_INFO_BLOKIR")];
                    [_btnNext setTitle:[lang(@"BTN_BLOKIR") uppercaseString] forState:UIControlStateNormal];
                }
                else if([codeMenu isEqualToString:codePorting]){ //Porting
                    self.valNamaNasabah.text = [data valueForKey:@"account_name"];
                    self.valNomorRekening.text = [data valueForKey:@"account_id_proxy_new"];

                    NSString *proxyTypeValue = @"";
                    if([[data valueForKey:@"proxy_type"] isEqualToString:@"01"]){
                       proxyTypeValue = lang(@"LABEL_NOHAPE");
                    }else{
                       proxyTypeValue = @"Email";
                    }

                    listContent = @[
                                    @{@"title": lang(@"LABEL_PROXYTYPE"), @"value": proxyTypeValue},
                                    @{@"title": lang(@"LABEL_TRXNO"), @"value": [jsonObject objectForKey:@"transaction_id"]},
                                    @{@"title": lang(@"LABEL_TRXDATE"), @"value": [data valueForKey:@"date_time"]},
                                    @{@"title": lang(@"LABEL_PROXYALIAS"), @"value": [data valueForKey:@"proxy_value"]}
                                   ];
                    
                    [_lblInfo setText:lang(@"LABEL_INFO_PORTING")];
                    [_btnNext setTitle:[lang(@"BTN_PORTING") uppercaseString] forState:UIControlStateNormal];
                }
                
            } @catch (NSException *exception) {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:lang(@"ERROR") preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            [self loadContent];
            
        }else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:lang(@"INFO") message:[NSString stringWithFormat:@"%@",[jsonObject objectForKey:@"response"]]  preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *act){
                [self backToRoot];
            }]];
            if (@available(iOS 13.0, *)) {
                [alert setOverrideUserInterfaceStyle:UIUserInterfaceStyleLight];
            }
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)pinDoneState:(NSString *)menuID{
    [self doCheckPin];
}

- (void)removeAllArrangedViewOf:(UIStackView*) stackview{
    
    for(UIView* subview in stackview.arrangedSubviews){
        [stackview removeArrangedSubview:subview]; // <-- Removes Constraints as well
        [subview removeConstraints:subview.constraints];//  <--- Constraints will be invalid or non existing
        [subview removeFromSuperview];
    }
}

-(NSString*)getProxyTypeValue:(NSString*)proxyType{
    if([proxyType isEqualToString:@"01"]){
        return lang(@"LABEL_NOHAPE");
    }else if([proxyType isEqualToString:@"02"]){
        return @"Email";
    }
    return @"";
}

@end
