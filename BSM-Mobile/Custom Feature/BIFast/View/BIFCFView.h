//
//  BIFCFView.h
//  BSM-Mobile
//
//  Created by Amini on 06/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BIFCFView : UIView

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelValue;

@end

NS_ASSUME_NONNULL_END
