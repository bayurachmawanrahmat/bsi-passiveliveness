//
//  BIFInfoView.m
//  BSM-Mobile
//
//  Created by Amini on 20/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFInfoView.h"

@implementation BIFInfoView


- (instancetype)init
{
    self = [super init];
    if (self) {
        if (self.subviews.count == 0) {
            [self loadViewFromNib];
        }
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        if (self.subviews.count == 0) {
            [self loadViewFromNib];
        }
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            [self loadViewFromNib];
        }    }
    return self;
}

- (void) loadViewFromNib{
    UIView *view = (UIView*)[[[NSBundle mainBundle]loadNibNamed:@"BIFInfoView" owner:self options:nil] objectAtIndex:0];
    bifView = (BIFInfoView*)view;

    [self addSubview:view];
    [self stretch:view];
}

- (void) stretch : (UIView*)view{
    
    view.translatesAutoresizingMaskIntoConstraints = false;
    [NSLayoutConstraint activateConstraints:@[
        [view.topAnchor constraintEqualToAnchor:view.superview.topAnchor],
        [view.leftAnchor constraintEqualToAnchor:view.superview.leftAnchor],
        [view.rightAnchor constraintEqualToAnchor:view.superview.rightAnchor],
        [view.bottomAnchor constraintEqualToAnchor:view.superview.bottomAnchor]]];
    
}

- (void)setLabel:(NSString *)label andValue:(NSString*)value{
    BIFInfoView *view = (BIFInfoView*) bifView;
    view.labelInfo.text = label;
    view.valueInfo.text = value;
}



@end
