//
//  BIFFieldView.h
//  BSM-Mobile
//
//  Created by Amini on 01/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BIFFieldView : UIView{
//    BIFFieldView *biffView;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UITextField *textField;


- (void) setTextTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
