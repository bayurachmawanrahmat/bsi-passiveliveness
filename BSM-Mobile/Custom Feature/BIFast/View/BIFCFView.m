//
//  BIFCFView.m
//  BSM-Mobile
//
//  Created by Amini on 06/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFCFView.h"

@implementation BIFCFView

- (instancetype)init
{
    self = [super init];
    if (self) {
        if (self.subviews.count == 0) {
            self = [self initializeSubviews];
        }
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        if (self.subviews.count == 0) {
            self = [self initializeSubviews];
        }
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            self = [self initializeSubviews];
        }    }
    return self;
}


-(instancetype)initializeSubviews {
    id view =   [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];

    return view;
}

@end
