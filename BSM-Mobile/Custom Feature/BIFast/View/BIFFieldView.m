//
//  BIFFieldView.m
//  BSM-Mobile
//
//  Created by Amini on 01/10/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import "BIFFieldView.h"

@implementation BIFFieldView

- (instancetype)init
{
    self = [super init];
    if (self) {
        if (self.subviews.count == 0) {
            self = [self initializeSubviews];
        }
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        if (self.subviews.count == 0) {
            self = [self initializeSubviews];
        }
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (self.subviews.count == 0) {
            self = [self initializeSubviews];
        }    }
    return self;
}


-(instancetype)initializeSubviews {
    id view =   [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];

    return view;
}

- (void) stretch : (UIView*)view{
    
    view.translatesAutoresizingMaskIntoConstraints = false;
    [NSLayoutConstraint activateConstraints:@[
        [view.topAnchor constraintEqualToAnchor:view.superview.topAnchor],
        [view.leftAnchor constraintEqualToAnchor:view.superview.leftAnchor],
        [view.rightAnchor constraintEqualToAnchor:view.superview.rightAnchor],
        [view.bottomAnchor constraintEqualToAnchor:view.superview.bottomAnchor]]];
    
}

- (void) setTextTitle:(NSString *)title{
    self.title.text = title;
}



@end
