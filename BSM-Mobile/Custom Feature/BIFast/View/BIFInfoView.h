//
//  BIFInfoView.h
//  BSM-Mobile
//
//  Created by Amini on 20/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BIFInfoView : UIView{
    UIView *bifView;
}

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *labelInfo;
@property (weak, nonatomic) IBOutlet UILabel *valueInfo;


- (void)setLabel:(NSString*)label andValue:(NSString*)value;

@end

NS_ASSUME_NONNULL_END
