//
//  BIFInfoCell.h
//  BSM-Mobile
//
//  Created by Amini on 17/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BIFInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *vwParentContent;
@property (weak, nonatomic) IBOutlet UILabel *labelNumber;
@property (weak, nonatomic) IBOutlet UIView *vwContent;

@property (weak, nonatomic) IBOutlet UIImageView *imgSetting;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@property (weak, nonatomic) IBOutlet UIStackView *stackView;


@end

NS_ASSUME_NONNULL_END
