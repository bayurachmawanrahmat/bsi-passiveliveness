//
//  KeynumbCell.h
//  BSM-Mobile
//
//  Created by Amini on 16/09/21.
//  Copyright © 2021 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KeynumbCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end

NS_ASSUME_NONNULL_END
