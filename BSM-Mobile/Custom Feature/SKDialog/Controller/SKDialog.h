//
//  SKDialog.h
//  BSM-Mobile
//
//  Created by Bayu Rachmawan Rahmat on 19/02/22.
//  Copyright © 2022 lds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SKDialog : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblDeskripsi;
@property (weak, nonatomic) IBOutlet UIButton * btnOk;

@end

NS_ASSUME_NONNULL_END
