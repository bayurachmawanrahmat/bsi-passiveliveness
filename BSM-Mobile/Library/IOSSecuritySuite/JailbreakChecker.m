//
//  JailBreakChecker.m
//  BSM-Mobile
//
//  Created by Novriansyah Amini on 17/02/21.
//  Copyright © 2021 lds. All rights reserved.
//  Reference Anthony Viriya - github.com/avltree9798/isJailbroken
//  and IOSSecuritySuite - github.com/securing - securing.pl

#import "JailbreakChecker.h"
//#import "EmulatorChecker.h"
#include <sys/stat.h>
#import <TargetConditionals.h>
#import <UIKit/UIKit.h>
#include <mach-o/dyld.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/sysctl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <dlfcn.h>
#import <DTTJailbreakDetection/DTTJailbreakDetection.h>

@implementation JailbreakChecker

#define A(c)            (c) - 0x19
#define HIDE_STR(str)   do { char *p = str;  while (*p) *p++ -= 0x19; } while (0)
typedef int (*ptrace_ptr_t)(int _request, pid_t _pid, caddr_t _addr, int _data);
#if !defined(PT_DENY_ATTACH)
#define PT_DENY_ATTACH 31
#endif
//
BOOL DEBUGGING = YES;

#if TARGET_IPHONE_SIMULATOR && !defined(LC_ENCRYPTION_INFO)
#define LC_ENCRYPTION_INFO 0x21
struct encryption_info_command {
    uint32_t cmd;
    uint32_t cmdsize;
    uint32_t cryptoff;
    uint32_t cryptsize;
    uint32_t cryptid;
};
#endif

void LOG(NSString* loc)
{
    NSLog(@"Found: %@", loc);
}

CFRunLoopSourceRef gSocketSource;
BOOL fileExist(NSString* path)
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = NO;
    if([fileManager fileExistsAtPath:path isDirectory:&isDirectory]){
        return YES;
    }
    return NO;
}

BOOL directoryExist(NSString* path)
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = YES;
    if([fileManager fileExistsAtPath:path isDirectory:&isDirectory]){
        return YES;
    }
    return NO;
}

BOOL canOpen(NSString* path)
{
    FILE *file = fopen([path UTF8String], "r");
    if(file==nil){
        return fileExist(path) || directoryExist(path);
    }
    fclose(file);
    return YES;
}

// Preventing libobjc hooked, strstr implementation
const char* tuyul(const char* X, const char* Y)
{
    if (*Y == '\0')
        return X;

    for (int i = 0; i < strlen(X); i++)
    {
        if (*(X + i) == *Y)
        {
            const char* ptr = tuyul(X + i + 1, Y + 1);
            return (ptr) ? ptr - 1 : NULL;
        }
    }

    return NULL;
}

BOOL isRunningOnMac()
{
    BOOL isiOSAppOnMac = NO;
    if (@available(iOS 14.0, *)) {
        isiOSAppOnMac = [NSProcessInfo processInfo].isiOSAppOnMac;
    }
    return isiOSAppOnMac;
}

BOOL checkURLScheme(){
    __block bool result = YES;
    
    NSArray *urlSchemes = @[@"undecimus://",@"cydia://",@"sileo://",@"zbra://"];
    
    if([NSThread isMainThread]){
        for(NSString* url in urlSchemes){
            if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]){
                return NO;
            }
        }
    } else {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        dispatch_async(dispatch_get_main_queue(), ^{
           // do work here to Usually to update the User Interface
            for(NSString* url in urlSchemes){
                NSLog(@"check -> %@", url);
                if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]){
                    NSLog(@"found -> %@", url);
                    result = NO;
                }
            }
            dispatch_semaphore_signal(sema);
        });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    return YES;
}

char* UNHIDE_STR(char* str){
    do { char *p = str;  while (*p) *p++ += 0x19; } while (0);
    return str;
}

char* decryptString(char* str){
    str = UNHIDE_STR(str);
    str[strlen(str)]='\0';
    return str;
}

BOOL isInjectedWithDynamicLibrary()
{
    int i=0;
    while(true){
        const char *name = _dyld_get_image_name(i++);
        if(name==NULL){
            break;
        }
        if (name != NULL) {
            char cyinjectHide[] = {
                A('c'),
                A('y'),
                A('i'),
                A('n'),
                A('j'),
                A('e'),
                A('c'),
                A('t'),
                0
            };
            char libcycriptHide[] = {
                A('l'),
                A('i'),
                A('b'),
                A('c'),
                A('y'),
                A('c'),
                A('r'),
                A('i'),
                A('p'),
                A('t'),
                0
            };
            
            char libfridaHide[] = {
                A('F'),
                A('r'),
                A('i'),
                A('d'),
                A('a'),
                A('G'),
                A('a'),
                A('d'),
                A('g'),
                A('e'),
                A('t'),
                0
            };
            char zzzzLibertyDylibHide[] = {
                A('z'),
                A('z'),
                A('z'),
                A('z'),
                A('L'),
                A('i'),
                A('b'),
                A('e'),
                A('r'),
                A('t'),
                A('y'),
                A('.'),
                A('d'),
                A('y'),
                A('l'),
                A('i'),
                A('b'),
                0
            };
            char sslkillswitch2dylib[] = {
                A('S'),
                A('S'),
                A('L'),
                A('K'),
                A('i'),
                A('l'),
                A('l'),
                A('S'),
                A('w'),
                A('i'),
                A('t'),
                A('c'),
                A('h'),
                A('2'),
                A('.'),
                A('d'),
                A('y'),
                A('l'),
                A('i'),
                A('b'),
                0
            };
            
            char zeroshadowdylib[] = {
                A('0'),
                A('S'),
                A('h'),
                A('a'),
                A('d'),
                A('o'),
                A('w'),
                A('.'),
                A('d'),
                A('y'),
                A('l'),
                A('i'),
                A('b'),
                0
            };
            
            char mobilesubstratedylib[] = {
                A('M'),
                A('o'),
                A('b'),
                A('i'),
                A('l'),
                A('e'),
                A('S'),
                A('u'),
                A('b'),
                A('s'),
                A('t'),
                A('r'),
                A('a'),
                A('t'),
                A('e'),
                A('.'),
                A('d'),
                A('y'),
                A('l'),
                A('i'),
                A('b'),
                0
            };
            
            char libsparkapplistdylib[] = {
                A('l'),
                A('i'),
                A('b'),
                A('s'),
                A('p'),
                A('a'),
                A('r'),
                A('k'),
                A('a'),
                A('p'),
                A('p'),
                A('l'),
                A('i'),
                A('s'),
                A('t'),
                A('.'),
                A('d'),
                A('y'),
                A('l'),
                A('i'),
                A('b'),
                0
            };
            
            char SubstrateInserterdylib[] = {
                A('S'),
                A('u'),
                A('b'),
                A('s'),
                A('t'),
                A('r'),
                A('a'),
                A('t'),
                A('e'),
                A('I'),
                A('n'),
                A('s'),
                A('e'),
                A('r'),
                A('t'),
                A('e'),
                A('r'),
                A('.'),
                A('d'),
                A('y'),
                A('l'),
                A('i'),
                A('b'),
                0
            };
            
            char zzzzzzUnSubdylib[] = {
                A('z'),
                A('z'),
                A('z'),
                A('z'),
                A('z'),
                A('z'),
                A('U'),
                A('n'),
                A('S'),
                A('u'),
                A('b'),
                A('.'),
                A('d'),
                A('y'),
                A('l'),
                A('i'),
                A('b'),
                0
                
            };
            
            char kor[] = {
                A('.'),
                A('.'),
                A('.'),
                A('!'),
                A('@'),
                A('#'),
                0
            };
            char cephei[] = {
                A('/'),A('u'),A('s'),A('r'),A('/'),A('l'),A('i'),A('b'),A('/'),A('C'),A('e'),A('p'),A('h'),A('e'),A('i'),A('.'),A('f'),A('r'),A('a'),A('m'),A('e'),A('w'),A('o'),A('r'),A('k'),A('/'),A('C'),A('e'),A('p'),A('h'),A('e'),A('i'),
                0
            };
            if (tuyul(name, decryptString(cephei)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(kor)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(mobilesubstratedylib)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if(tuyul(name, decryptString(libsparkapplistdylib)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(cyinjectHide)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(libcycriptHide)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(libfridaHide)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(zzzzLibertyDylibHide)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(sslkillswitch2dylib)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(zeroshadowdylib)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(SubstrateInserterdylib)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
            if (tuyul(name, decryptString(zzzzzzUnSubdylib)) != NULL){
                if(DEBUGGING){LOG([[NSString alloc] initWithFormat:@"%s", name]);}
                return YES;
            }
        }
    }
    return NO;
}


BOOL isJailBreak(){
    //list file existence checking
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://package/com.avl.com"]])
    {
        return YES;
    }
    NSArray* checks = [[NSArray alloc]initWithObjects:@"/Application/Cydia.app",
                            @"/bin/bash",
                            @"/bin/sh",
                            @"/usr/sbin/sshd",
                            @"/etc/apt",
                            @"/usr/bin/ssh",
                            @"/private/var/lib/apt",
                            @"/private/var/lib/cydia",
                            @"/private/var/tmp/cydia.log",
                            @"/Applications/WinterBoard.app",
                            @"/var/lib/cydia",
                            @"/private/etc/dpkg/origins/debian",
                            @"/bin.sh",
                            @"/private/etc/apt",
                            @"/etc/ssh/sshd_config",
                            @"/private/etc/ssh/sshd_config",
                            @"/Applications/SBSetttings.app",
                            @"/private/var/mobileLibrary/SBSettingsThemes/",
                            @"/private/var/stash",
                            @"/usr/libexec/sftp-server",
                            @"/usr/libexec/cydia/",
                            @"/usr/bin/cycript",
                            @"/usr/local/bin/cycript",
                            @"/usr/lib/libcycript.dylib",
                            @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
                            @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",
                            @"/Applications/FakeCarrier.app",
                            @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",
                            @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",
                            @"/usr/libexec/ssh-keysign",
                            @"/Applications/blackra1n.app",
                            @"/Applications/IntelliScreen.app",
                            @"/Applications/Snoop-itConfig.app",
                            @"/var/checkra1n.dmg",
                            @"/var/binpack",
                            @"/usr/sbin/frida-server", // frida
                            @"/etc/apt/sources.list.d/electra.list", // electra
                            @"/etc/apt/sources.list.d/sileo.sources", // electra
                            @"/.bootstrapped_electra", // electra
                            @"/usr/lib/libjailbreak.dylib", // electra
                            @"/jb/lzma", // electra
                            @"/.cydia_no_stash", // unc0ver
                            @"/.installed_unc0ver", // unc0ver
                            @"/jb/offsets.plist", // unc0ver
                            @"/usr/share/jailbreak/injectme.plist", // unc0ver
                            @"/etc/apt/undecimus/undecimus.list", // unc0ver
                            @"/var/lib/dpkg/info/mobilesubstrate.md5sums", // unc0ver
                            @"/Library/MobileSubstrate/MobileSubstrate.dylib",
                            @"/jb/jailbreakd.plist", // unc0ver
                            @"/jb/amfid_payload.dylib", // unc0ver
                            @"/jb/libjailbreak.dylib", // unc0ver
                            @"/usr/libexec/cydia/firmware.sh",
                            @"/private/var/lib/apt",
                            @"/private/var/Users/",
                            @"/var/log/apt",
                            @"/Applications/Cydia.app",
                            @"/private/var/lib/apt/",
                            @"/private/var/cache/apt/",
                            @"/private/var/log/syslog",
                            @"/Applications/Icy.app",
                            @"/Applications/MxTube.app",
                            @"/Applications/RockApp.app",
                            @"/Applications/SBSettings.app",
                            @"/Library/MobileSubstrate/CydiaSubstrate.dylib",
                           nil];
        
    //    // These files can give false positive in the emulator
    //    if !EmulatorChecker.amIRunInEmulator() {
    //        paths += [
    //        "/bin/bash",
    //        "/usr/sbin/sshd",
    //        "/usr/libexec/ssh-keysign",
    //        "/bin/sh",
    //        "/etc/ssh/sshd_config",
    //        "/usr/libexec/sftp-server",
    //        "/usr/bin/ssh"
    //        ]
    //    }
    
        //Check installed app
        for(NSString* check in checks)
        {
            if(canOpen(check))
            {
                if(DEBUGGING){LOG(check);}
                return YES;
            }
        }
        //symlink verification
        struct stat sym;
        if(lstat("/Applications", &sym) || lstat("/var/stash/Library/Ringtones", &sym) ||
           lstat("/var/stash/Library/Wallpaper", &sym) ||
           lstat("/var/stash/usr/include", &sym) ||
           lstat("/var/stash/usr/libexec", &sym)  ||
           lstat("/var/stash/usr/share", &sym) ||
           lstat("/var/stash/usr/arm-apple-darwin9", &sym))
        {
            if(sym.st_mode & S_IFLNK)
            {
                if(DEBUGGING){LOG(@"Symlink");}
                return YES;
            }
        }
        
        //Check process forking
        int pid = fork();
        if(!pid)
        {
            exit(1);
        }
        if(pid >= 0)
        {
            if(DEBUGGING){LOG(@"Fork");}
            return YES;
        }
        
        //Check permission to write to /private move to checkwriteable
        NSString *path = @"/private/bsimo.txt";
        NSFileManager *fileManager = [NSFileManager defaultManager];
        @try {
            NSError* error;
            NSString *test = @"bsimo was here";
            [test writeToFile:test atomically:NO encoding:NSStringEncodingConversionAllowLossy error:&error];
            [fileManager removeItemAtPath:path error:nil];
            if(error==nil)
            {
                if(DEBUGGING){LOG(@"File creation");}
                return YES;
            }
            return NO;
        } @catch (NSException *exception) {
            return NO;
        }
}

BOOL isDebugged()
{
    int junk;
    int mib[4];
    struct kinfo_proc info;
    size_t size;
    info.kp_proc.p_flag = 0;
    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();
    size = sizeof(info);
    junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
    assert(junk == 0);
    NSLog(@"isdebugged result -> %d", (info.kp_proc.p_flag & P_TRACED));
    return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
}

BOOL isFromAppStore()
{
    #if TARGET_IPHONE_SIMULATOR
        return NO;
    #else
        NSString *provisionPath = [[NSBundle mainBundle] pathForResource:@"embedded" ofType:@"mobileprovision"];
        if (nil == provisionPath || 0 == provisionPath.length) {
            NSLog(@"isFromAppStore result YES");
            return YES;
        }
        NSLog(@"isFromAppStore result NO");
        return NO;
    #endif
}

BOOL checkSuspiciousFilesCanBeOpened(){
    NSArray* paths = [[NSArray alloc]initWithObjects:
                      @"/.installed_unc0ver",
                              @"/.bootstrapped_electra",
                              @"/Applications/Cydia.app",
                              @"/Library/MobileSubstrate/MobileSubstrate.dylib",
                              @"/etc/apt",
                              @"/var/log/apt", nil];

    for(NSString *path in paths){
        if([[NSFileManager defaultManager]isReadableFileAtPath:path]){
            return NO;
        }
    }
    return YES;
}

BOOL checkRestrictedDirectoriesWritable(){
    NSArray* paths = [[NSArray alloc]initWithObjects:
                      @"/",
                      @"/root/",
                      @"/private/",
                      @"/jb/", nil];
    
    for(NSString *path in paths){
        //            let pathWithSomeRandom = path+UUID().uuidString
        NSString *pathWithSomeRandom = [NSString stringWithFormat:@"%@%@",path,[NSUUID UUID].UUIDString];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        @try {
            NSError* error;
            NSString *test = @"bsimo was here";
            [test writeToFile:pathWithSomeRandom atomically:NO encoding:NSStringEncodingConversionAllowLossy error:&error];
            [fileManager removeItemAtPath:pathWithSomeRandom error:nil];
            if(error==nil)
            {
                if(DEBUGGING){LOG(@"File creation");}
                return NO;
            }
            return YES;
        } @catch (NSException *exception) {
            return YES;
        }
    }
    return YES;
}

BOOL checkSymbolicLinks(){
    NSArray* paths = [[NSArray alloc]initWithObjects:
                       @"/var/lib/undecimus/apt", // unc0ver
                       @"/Applications",
                       @"/Library/Ringtones",
                       @"/Library/Wallpaper",
                       @"/usr/arm-apple-darwin9",
                       @"/usr/include",
                       @"/usr/libexec",
                      @"/usr/share", nil];
    
    for(NSString* path in paths){
        @try {
            NSString *result = [[NSFileManager defaultManager]destinationOfSymbolicLinkAtPath:path error:nil];
            if(result != nil || ![result isEqualToString:@""]){
                return false;
            }
        } @catch (NSException *exception) {
        }
    }
    return true;
}

BOOL isSecurityCheckPassed() {
    if ([AppProperties checkJbProtectIsOn]) {
        if(TARGET_IPHONE_SIMULATOR)return NO;
        if(TARGET_OS_OSX)return NO;
        return !isJailBreak() &&
        !isInjectedWithDynamicLibrary() &&
    //    !isDebugged() &&
    //    isFromAppStore() &&
        checkURLScheme() &&
        checkRestrictedDirectoriesWritable() &&
        checkSuspiciousFilesCanBeOpened() &&
        ![DTTJailbreakDetection isJailbroken];
    } else {
        return YES;
    }
}

@end
