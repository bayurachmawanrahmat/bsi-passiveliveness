//
//  DESChiper.h
//  BSM-Mobile
//
//  Created by lds on 7/25/14.
//  Copyright (c) 2014 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DESChiper : NSObject

- (NSString *)generateDESKey;
- (NSString *)doDES:(NSString *)plainData key:(NSString *)key;
- (NSString *)doDecryptDES:(NSData *)plainData key:(NSString *)key;
- (NSString *)doPinBlock:(NSString *)plainText key:(NSString *)key;
@end
