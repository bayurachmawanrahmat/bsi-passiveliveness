//
//  DESChiper.m
//  BSM-Mobile
//
//  Created by lds on 7/25/14.
//  Copyright (c) 2014 lds. All rights reserved.
//

#import "DESChiper.h"
#import "NSData+Hex.h"
#import "NSData+MD5Digest.h"

@implementation DESChiper

- (NSString *)generateDESKey{
    uint8_t bytes[20];
    while(bytes == NULL){
        SecRandomCopyBytes(kSecRandomDefault, 20, bytes);
    }
    NSData *tempData = [NSData dataWithBytes:(const void *)bytes length:20];
    return [tempData base64EncodedString];
}


- (NSData *)convertPinBlock:(NSString *)command{
    NSMutableData *commandToSend= [[NSMutableData alloc] init];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i;
    for (i=0; i < [command length]/2; i++) {
        byte_chars[0] = [command characterAtIndex:i*2];
        byte_chars[1] = [command characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [commandToSend appendBytes:&whole_byte length:1];
    }
    return commandToSend;
}

- (NSString *)doPinBlock:(NSString *)plainText key:(NSString *)key {
    plainText = [plainText stringByPaddingToLength:16 withString:@"F" startingAtIndex:0];
    NSData *plainData = [self convertPinBlock:plainText];
    const void *vplainText;
    size_t plainTextBufferSize;
    
    plainTextBufferSize = [plainData length];
    vplainText = (const void *)[plainData bytes];
    
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    // uint8_t ivkCCBlockSize3DES;
    
    bufferPtrSize = (plainTextBufferSize + kCCBlockSize3DES) & ~(kCCBlockSize3DES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    // memset((void *) iv, 0x0, (size_t) sizeof(iv));
    
    NSData *dataKey = [self convertPinBlock:key];
    //    const void *vinitVec = (const void *) [initVec UTF8String];
    
    ccStatus = CCCrypt(kCCEncrypt,
                       kCCAlgorithmDES,
                       kCCOptionECBMode,
                       [dataKey bytes], //"123456789012345678901234", //key
                       kCCKeySizeDES,
                       nil, //"init Vec", //iv,
                       [plainData bytes], //"Your Name", //plainText,
                       [plainData length],
                       (void *)bufferPtr,
                       [plainData length],
                       &movedBytes);
    if (ccStatus == kCCSuccess) NSLog(@"SUCCESS");
    else if (ccStatus == kCCParamError) NSLog( @"PARAM ERROR");
    else if (ccStatus == kCCBufferTooSmall) NSLog( @"BUFFER TOO SMALL");
    else if (ccStatus == kCCMemoryFailure) NSLog( @"MEMORY FAILURE");
    else if (ccStatus == kCCAlignmentError) NSLog( @"ALIGNMENT");
    else if (ccStatus == kCCDecodeError) NSLog( @"DECODE ERROR");
    else if (ccStatus == kCCUnimplemented) NSLog( @"UNIMPLEMENTED");
    
    NSData *myData = [NSData dataWithBytes:(const void *)bufferPtr length:(NSUInteger)movedBytes];
    
    return [myData hexRepresentationWithSpaces_AS:NO];
}


- (NSString *)doDES:(NSString *)plainData key:(NSString *)key{
    
    NSData *dataIn = [key dataUsingEncoding:NSASCIIStringEncoding];
    NSMutableData *dataKey = [NSMutableData dataWithLength:CC_MD5_DIGEST_LENGTH ];
    CC_MD5(dataIn.bytes, dataIn.length, dataKey.mutableBytes);
    
    NSData *data = [plainData dataUsingEncoding:NSASCIIStringEncoding];
    
    size_t numBytesEncrypted = 0;
    size_t bufferSize = data.length + kCCBlockSizeDES;
    void *buffer = malloc(bufferSize);
    
    CCCryptorStatus result = CCCrypt( kCCEncrypt, kCCAlgorithmDES, kCCOptionPKCS7Padding|kCCOptionECBMode,
                                     dataKey.bytes, kCCKeySizeDES,
                                     NULL,
                                     data.bytes, data.length,
                                     buffer, bufferSize,
                                     &numBytesEncrypted);
    NSData *output = [NSData dataWithBytes:buffer length:numBytesEncrypted];
    free(buffer);
    if( result == kCCSuccess )
    {
        return [output base64EncodedString];
    } else {
        NSLog(@"Failed DES encrypt...");
        return nil;
    }
}

- (NSString *)doDecryptDES:(NSData *)data key:(NSString *)key {
    
    NSData *dataIn = [key dataUsingEncoding:NSASCIIStringEncoding];
    NSMutableData *dataKey = [NSMutableData dataWithLength:CC_MD5_DIGEST_LENGTH ];
    CC_MD5(dataIn.bytes, dataIn.length, dataKey.mutableBytes);
    
    //    NSData *data = [plainData dataUsingEncoding:NSASCIIStringEncoding];
    
    size_t numBytesEncrypted = 0;
    size_t bufferSize = data.length + kCCBlockSizeDES;
    void *buffer = malloc(bufferSize);
    
    CCCryptorStatus result = CCCrypt( kCCDecrypt, kCCAlgorithmDES, kCCOptionECBMode|kCCOptionPKCS7Padding,
                                     dataKey.bytes, kCCKeySizeDES,
                                     NULL,
                                     data.bytes, data.length,
                                     buffer, bufferSize,
                                     &numBytesEncrypted);
    NSData *output = [NSData dataWithBytes:buffer length:numBytesEncrypted];
    free(buffer);
    if( result == kCCSuccess )
    {
        return [[NSString alloc]initWithBytes:output.bytes length:output.length encoding:NSUTF8StringEncoding];
        //        return [output base64EncodedString];
    } else {
        NSLog(@"Failed DES encrypt...");
        return nil;
    }
}
@end
