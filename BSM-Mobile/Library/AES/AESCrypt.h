//
//  NSData+AES128.h
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/09/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AESCrypt : NSObject

+ (NSString *)encrypt:(NSString *)message;
+ (NSString *)decrypt:(NSString *)base64EncodedString;

@end
