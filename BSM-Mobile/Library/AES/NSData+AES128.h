//
//  NSData+AES128.h
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/09/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES128)

- (NSData *)AES128EncryptedDataWithKey:(NSString *)key;

- (NSData *)AES128DecryptedDataWithKey:(NSString *)key;

- (NSData *)AES128EncryptedDataWithKey:(NSString *)key iv:(NSString *)iv;

- (NSData *)AES128DecryptedDataWithKey:(NSString *)key iv:(NSString *)iv;

@end
