//
//  AESCipher.m
//  BSM-Mobile
//
//  Created by Alikhsan on 11/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "AESCipher.h"
#import "AppProperties.h"
#import <CommonCrypto/CommonCryptor.h>

NSString const *kInitVector = @"A-16-Byte-String";
size_t const kKeySize = kCCKeySizeAES128;
//static NSString * const mKeyChiper = @"B5m@M0b!l3!05APp";
//static NSString * const mKeyChiper = MKEY_CHIPER;

@implementation AESCipher

NSData * cipherOperation(NSData *contentData, NSData *keyData, CCOperation operation) {
    NSUInteger dataLength = contentData.length;
    
    void const *initVectorBytes = [kInitVector dataUsingEncoding:NSUTF8StringEncoding].bytes;
    void const *contentBytes = contentData.bytes;
    void const *keyBytes = keyData.bytes;
    
    size_t operationSize = dataLength + kCCBlockSizeAES128;
    void *operationBytes = malloc(operationSize);
    if (operationBytes == NULL) {
        return nil;
    }
    size_t actualOutSize = 0;
    
    CCCryptorStatus cryptStatus = CCCrypt(operation,
                                          kCCAlgorithmAES,
                                          kCCOptionPKCS7Padding,
                                          keyBytes,
                                          kKeySize,
                                          initVectorBytes,
                                          contentBytes,
                                          dataLength,
                                          operationBytes,
                                          operationSize,
                                          &actualOutSize);
    
    if (cryptStatus == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:operationBytes length:actualOutSize];
    }
    free(operationBytes);
    operationBytes = NULL;
    return nil;
}

- (NSString *) aesEncryptString : (NSString *) content{
    if (content != nil) {
        NSData *contentData = [content dataUsingEncoding:NSUTF8StringEncoding];
        NSData *keyData = [MKEY_CHIPER dataUsingEncoding:NSUTF8StringEncoding];
        if (contentData.length > 0) {
            NSCParameterAssert(content);
            NSCParameterAssert(MKEY_CHIPER);
            
            NSData *encrptedData = [self aesEncryptData:contentData aesKeyData:keyData];
            return [encrptedData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        }else{
            return @"";
        }
        
    }else{
        return @"";
    }
   
    
    
    
}
- (NSString *) aesDecryptString : (NSString *) content{
    if (content != nil) {
        NSData *contentData = [[NSData alloc] initWithBase64EncodedString:content options:0];
        NSData *keyData = [MKEY_CHIPER dataUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"%lu", (unsigned long)contentData.length);
        if (contentData.length > 0) {
            NSCParameterAssert(content);
            NSCParameterAssert(MKEY_CHIPER);
            
            NSData *decryptedData = [self aesDecryptData : contentData aesKeyData:keyData];
            return [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
        }else{
            return @"";
        }
    }else{
        return @"";
    }
    
   
}
- (NSData *) aesEncryptData : (NSData *) data
                 aesKeyData : (NSData *) keyData{
    NSCParameterAssert(data);
    NSCParameterAssert(keyData);
    
    NSString *hint = [NSString stringWithFormat:@"The key size of AES-%lu should be %lu bytes!", kKeySize * 8, kKeySize];
    NSCAssert(keyData.length == kKeySize, hint);
    return cipherOperation(data, keyData, kCCEncrypt);
}
- (NSData *) aesDecryptData : (NSData *) data
                 aesKeyData : (NSData *) keyData{
    
    NSCParameterAssert(data);
    NSCParameterAssert(keyData);
    
    NSString *hint = [NSString stringWithFormat:@"The key size of AES-%lu should be %lu bytes!", kKeySize * 8, kKeySize];
    NSCAssert(keyData.length == kKeySize, hint);
    return cipherOperation(data, keyData, kCCDecrypt);
}


-(NSDictionary *) dictAesEncryptString : (NSDictionary *) dictContent{
    NSMutableDictionary *dictEncrpt = [[NSMutableDictionary alloc]init];
    for (NSString *aKey in dictContent.allKeys) {
        [dictEncrpt setValue:[self aesEncryptString:[dictContent valueForKey:aKey]] forKey:aKey];
    }
    
    return  dictEncrpt;
}

-(NSDictionary *) dictAesDecryptString : (NSDictionary *) dictContent{
    NSMutableDictionary *dictDecrypt = [[NSMutableDictionary alloc]init];
    
    for (NSString *aKey in dictContent.allKeys) {
        [dictDecrypt setValue:[self aesDecryptString:[dictContent valueForKey:aKey]] forKey:aKey];
    }
    
    return dictDecrypt;
}

@end
