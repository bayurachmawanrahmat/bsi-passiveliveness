//
//  NSData+AES128.h
//  BSM-Mobile
//
//  Created by Abdul Muhamad Rosyid on 13/09/18.
//  Copyright © 2018 lds. All rights reserved.
//

#import "AESCrypt.h"
#import "NSData+AES128.h"

@implementation AESCrypt

+ (NSString *)encrypt:(NSString *)message{
    NSData *cipherData = [[message dataUsingEncoding:NSUTF8StringEncoding] AES128EncryptedDataWithKey:KEY_RETRIVAL];
    NSLog(@"crypt AES128 encrypt : %@", cipherData);
    // ----    AES128 -> base64
    // base 64 encryption
    return [cipherData base64EncodedStringWithOptions:0];
}

+ (NSString *)decrypt:(NSString *)base64EncodedString {
    // ----    base64 -> AES128 -> plain text
    // NSData from the Base64 encoded str
    NSData *cipherData = [[NSData alloc] initWithBase64EncodedString:base64EncodedString
                                                     options:0];
    NSLog(@"decrypt AES128: %@", cipherData);
    return [[NSString alloc] initWithData:[cipherData AES128DecryptedDataWithKey:KEY_RETRIVAL]
                                       encoding:NSUTF8StringEncoding];
}

//NSString *lg = @"";
//NSString *dt = @"";
//NSData *cipherData;
//NSString *plainText = @"";
//
//if([lang isEqualToString:@"id"]) {
//    lg = @"id";
//} else {
//    lg = @"en";
//}
//plainText = [NSString stringWithFormat:@"%@#%@", self.textFieldInput.text, lg];
//
//############## Request(crypt) ##############
//----    Plain text -> AES128
//cipherData = [[plainText dataUsingEncoding:NSUTF8StringEncoding] AES128EncryptedDataWithKey:KEY_RETRIVAL];
//NSLog(@"crypt AES128: %@", cipherData);
//----    AES128 -> base64
// base 64 encryption
//dt = [cipherData base64EncodedStringWithOptions:0];
//
//NSLog(@"crypt AES128+base64: %@", dt);
//
//############## Response(decrypt) ##############
///---- AES128 -> plain text
//plainText  = [[NSString alloc] initWithData:[cipherData AES128DecryptedDataWithKey:KEY_RETRIVAL]
//                                   encoding:NSUTF8StringEncoding];
//
//NSLog(@"decrypt AES128: %@",plainText);
//
//----    base64 -> AES128 -> plain text
// NSData from the Base64 encoded str
//cipherData = [[NSData alloc] initWithBase64EncodedString:dt
//                                                 options:0];
//NSLog(@"decrypt chiper: %@", cipherData);
//plainText  = [[NSString alloc] initWithData:[cipherData AES128DecryptedDataWithKey:KEY_RETRIVAL]
//                                   encoding:NSUTF8StringEncoding];
//NSLog(@"decrypt AES128+base64: %@", plainText);

@end
