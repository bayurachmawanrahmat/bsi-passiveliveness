//
//  AESCipher.h
//  BSM-Mobile
//
//  Created by Alikhsan on 11/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AESCipher : NSObject

- (NSString *) aesEncryptString : (NSString *) content;
- (NSString *) aesDecryptString : (NSString *) content;

-(NSDictionary *) dictAesEncryptString : (NSDictionary *) dictContent;
-(NSDictionary *) dictAesDecryptString : (NSDictionary *) dictContent;

- (NSData *) aesEncryptData : (NSData *) data
                 aesKeyData : (NSData *) keyData;
- (NSData *) aesDecryptData : (NSData *) data
                 aesKeyData : (NSData *) keyData;

@end


