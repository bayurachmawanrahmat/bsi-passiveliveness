// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
// swift-module-flags: -target x86_64-apple-ios9.3-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=none -O -module-name OracleLive
import AVFoundation
import CallKit
import CoreFoundation
import CoreImage
import CoreLocation
import CoreTelephony
import Foundation
@_exported import OracleLive
import PushKit
import QuartzCore
import ReplayKit
import Security
import Swift
import SystemConfiguration
import UIKit
import WebRTC
import wscSDK
public struct Notifications {
  public static let LiveEndedNotification: Foundation.NSNotification.Name
  public static let LiveCanceledNotification: Foundation.NSNotification.Name
  public static let LiveErrorNotification: Foundation.NSNotification.Name
  public static let LiveConnectingNotification: Foundation.NSNotification.Name
  public static let LiveConnectedNotification: Foundation.NSNotification.Name
}
extension Controller {
  @objc public class ContextAttributes : ObjectiveC.NSObject {
    @objc public func removeAllObjects()
    @objc override dynamic public func setValue(_ value: Any?, forKey key: Swift.String)
    @objc override dynamic public func value(forKey key: Swift.String) -> Any?
    @objc override dynamic public init()
    @objc deinit
  }
}
public protocol ScreenCaptureOutputPixelBufferDelegate : AnyObject {
  func didSet(size: CoreGraphics.CGSize)
  func output(pixelBuffer: CoreVideo.CVPixelBuffer, withPresentationTime: CoreMedia.CMTime)
}
extension Controller {
  @objc public class Service : ObjectiveC.NSObject {
    @objc public var address: Swift.String
    @objc public var tenantID: Swift.String
    @objc public var clientID: Swift.String
    @objc public var skipRecordingPermissionRequest: Swift.Bool
    @objc public var kycScanReference: Swift.String
    @objc public var language: Swift.String
    @objc public var authToken: Swift.String? {
      @objc get
      @objc set
    }
    @objc public var userID: Swift.String {
      @objc get
      @objc set
    }
    @objc public var AvatarLocation: Swift.Dictionary<Swift.String, Any> {
      @objc get
      @objc set
    }
    @objc public var pstnCallbacksAvailable: Swift.Bool
    @objc public var inAppCallbacksAvailable: Swift.Bool
    @objc public var translations: [Swift.String : Swift.String]?
    @objc public var offlineMode: Swift.Bool {
      @objc get
      @objc set
    }
    @objc override dynamic public init()
    @objc deinit
  }
}
public enum StreamType : Swift.String {
  case audioVideoStream
  case screenShareStream
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
public protocol StatsReportObserver {
  func onReport(streamType: OracleLive.StreamType, streamId: Swift.String, statsReports: [WebRTC.RTCLegacyStatsReport])
}
public enum CallEndedReason : Swift.String {
  case failed
  case remoteEnded
  case unanswered
  case answeredElsewhere
  case declinedElsewhere
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
@objc final public class URLSessionMock : Foundation.URLSession {
  public init(data: Foundation.Data? = nil, response: Foundation.URLResponse? = nil, error: Swift.Error? = nil)
  @objc override final public func dataTask(with request: Foundation.URLRequest, completionHandler: @escaping (Foundation.Data?, Foundation.URLResponse?, Swift.Error?) -> Swift.Void) -> Foundation.URLSessionDataTask
  @available(iOS, introduced: 7.0, deprecated: 13.0)
  @objc override dynamic public init()
  @objc deinit
}
public enum ConnectionStatus {
  case none
  case connecting
  case connected
  public static func == (a: OracleLive.ConnectionStatus, b: OracleLive.ConnectionStatus) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public enum CallTransferStatus {
  case none
  case transferring
  public static func == (a: OracleLive.CallTransferStatus, b: OracleLive.CallTransferStatus) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public enum LocalVideoTransition {
  case EnterFreeze
  case LeaveFreeze
  case EnterAnnotate
  case LeaveAnnotate
  case LeaveFreezeAndAnnotate
  case RemoteDisconnectFreezeAnnotate
  case RemoteDisconnectAnnotate
  public static func == (a: OracleLive.LocalVideoTransition, b: OracleLive.LocalVideoTransition) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
@objc final public class Controller : ObjectiveC.NSObject {
  @objc final public var service: OracleLive.Controller.Service {
    get
    }
  @objc final public var contextAttributes: OracleLive.Controller.ContextAttributes {
    get
    }
  @objc final public var settings: OracleLive.Controller.Settings {
    get
    }
  final public var connectionStatus: OracleLive.ConnectionStatus {
    get
  }
  final public var stats: OracleLive.Controller.Stats {
    get
    }
  @objc final public var preCallIdentityCheckInProgress: Swift.Bool
  @objc public static let shared: OracleLive.Controller
  @objc final public func registerDeviceForPushNotifiction(applicationID: Swift.String, deviceID: Swift.String, deviceToken: Swift.String, phoneNumber: Swift.String)
  @available(iOS 10.0, *)
  @objc final public func reportIncomingCall(payload: PushKit.PKPushPayload)
  @objc final public func requestAuthorization()
  @objc final public func addComponent(viewController: UIKit.UIViewController?, forceUpdate: Swift.Bool = false, autoUpdated: Swift.Bool = false)
  @objc final public func clearIdVerificationState()
  @objc final public func setMeetingURL(url: Swift.String)
  final public func adjustCallConnectTimerDuration(newDuration: Swift.Int)
  final public func dismissCallbackOfferDialogIfShowing()
  @objc deinit
}
public enum EventSourceState {
  case connecting
  case open
  case closed
  public static func == (a: OracleLive.EventSourceState, b: OracleLive.EventSourceState) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public protocol EventSourceProtocol {
  var headers: [Swift.String : Swift.String] { get }
  var retryTime: Swift.Int { get }
  var url: Foundation.URL { get }
  var lastEventId: Swift.String? { get }
  var readyState: OracleLive.EventSourceState { get }
  func connect(lastEventId: Swift.String?)
  func disconnect()
  func events() -> [Swift.String]
  func onOpen(_ onOpenCallback: @escaping (() -> Swift.Void))
  func onComplete(_ onComplete: @escaping ((Swift.Int?, Swift.Bool?, Foundation.NSError?) -> Swift.Void))
  func onMessage(_ onMessageCallback: @escaping ((Swift.String?, Swift.String?, Swift.String?) -> Swift.Void))
  func addEventListener(_ event: Swift.String, handler: @escaping ((Swift.String?, Swift.String?, Swift.String?) -> Swift.Void))
  func removeEventListener(_ event: Swift.String)
}
@objc open class EventSource : ObjectiveC.NSObject, OracleLive.EventSourceProtocol, Foundation.URLSessionDataDelegate {
  final public let url: Foundation.URL
  public var lastEventId: Swift.String? {
    get
    }
  public var retryTime: Swift.Int {
    get
    }
  public var headers: [Swift.String : Swift.String] {
    get
    }
  public var readyState: OracleLive.EventSourceState {
    get
    }
  public init(url: Foundation.URL, headers: [Swift.String : Swift.String] = [:])
  public func connect(lastEventId: Swift.String? = nil)
  public func disconnect()
  public func onOpen(_ onOpenCallback: @escaping (() -> Swift.Void))
  public func onComplete(_ onComplete: @escaping ((Swift.Int?, Swift.Bool?, Foundation.NSError?) -> Swift.Void))
  public func onMessage(_ onMessageCallback: @escaping ((Swift.String?, Swift.String?, Swift.String?) -> Swift.Void))
  public func addEventListener(_ event: Swift.String, handler: @escaping ((Swift.String?, Swift.String?, Swift.String?) -> Swift.Void))
  public func removeEventListener(_ event: Swift.String)
  public func events() -> [Swift.String]
  @objc open func urlSession(_ session: Foundation.URLSession, dataTask: Foundation.URLSessionDataTask, didReceive data: Foundation.Data)
  @objc open func urlSession(_ session: Foundation.URLSession, dataTask: Foundation.URLSessionDataTask, didReceive response: Foundation.URLResponse, completionHandler: @escaping (Foundation.URLSession.ResponseDisposition) -> Swift.Void)
  @objc open func urlSession(_ session: Foundation.URLSession, task: Foundation.URLSessionTask, didCompleteWithError error: Swift.Error?)
  @objc open func urlSession(_ session: Foundation.URLSession, task: Foundation.URLSessionTask, willPerformHTTPRedirection response: Foundation.HTTPURLResponse, newRequest request: Foundation.URLRequest, completionHandler: @escaping (Foundation.URLRequest?) -> Swift.Void)
  @objc override dynamic public init()
  @objc deinit
}
extension Controller {
  @objc public class Stats : ObjectiveC.NSObject {
    public var statsReportObserver: OracleLive.StatsReportObserver?
    @objc override dynamic public init()
    @objc deinit
  }
}
extension Controller {
  @objc public class Settings : ObjectiveC.NSObject {
    @objc public var startVideoInFullScreen: Swift.Bool {
      @objc get
      @objc set(newValue)
    }
    @objc public var startVideoWithFrontCamera: Swift.Bool {
      @objc get
      @objc set(newValue)
    }
    @objc public var hideEndCallButton: Swift.Bool {
      @objc get
      @objc set(newValue)
    }
    @objc public var hidePauseButton: Swift.Bool {
      @objc get
      @objc set(newValue)
    }
    @objc public var idVerificationConfig: Any? {
      @objc get
      @objc set(newValue)
    }
    @objc public var startCallDirectly: Swift.Bool {
      @objc get
      @objc set(newValue)
    }
    @objc public var hideQueueLengthIndicator: Swift.Bool {
      @objc get
      @objc set(newValue)
    }
    @objc override dynamic public init()
    @objc deinit
  }
}
extension OracleLive.StreamType : Swift.Equatable {}
extension OracleLive.StreamType : Swift.Hashable {}
extension OracleLive.StreamType : Swift.RawRepresentable {}
extension OracleLive.CallEndedReason : Swift.Equatable {}
extension OracleLive.CallEndedReason : Swift.Hashable {}
extension OracleLive.CallEndedReason : Swift.RawRepresentable {}
extension OracleLive.ConnectionStatus : Swift.Hashable {}
extension OracleLive.CallTransferStatus : Swift.Hashable {}
extension OracleLive.LocalVideoTransition : Swift.Equatable {}
extension OracleLive.LocalVideoTransition : Swift.Hashable {}
extension OracleLive.EventSourceState : Swift.Equatable {}
extension OracleLive.EventSourceState : Swift.Hashable {}
