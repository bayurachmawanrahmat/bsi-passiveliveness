//
//  ObjC.h
//  OracleLive
//
//  Created by Diwakar Goel on 10/24/16.
//  Copyright © 2016 Oracle. All rights reserved.
//

#ifndef ObjC_h
#define ObjC_h


#endif /* ObjC_h */
#import <UIKit/UIKit.h>


@interface ObjC : NSObject

+ (BOOL)catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error;

@end
