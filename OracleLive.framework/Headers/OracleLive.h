/* Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved. */

#import <UIKit/UIKit.h>

//! Project version number for OracleLive1.
FOUNDATION_EXPORT double OracleLive1VersionNumber;

//! Project version string for OracleLive1.
FOUNDATION_EXPORT const unsigned char OracleLive1VersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OracleLive1/PublicHeader.h>
#import <wscSDK/wscSDK.h>
#import <WebRTC/WebRTC.h>

// Already in bridging header but needed here (and made public) so ui/Classes can see it
#import "ObjC.h"
