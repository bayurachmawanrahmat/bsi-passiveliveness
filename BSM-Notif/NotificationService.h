//
//  NotificationService.h
//  BSM-Notif
//
//  Created by Alikhsan on 23/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
