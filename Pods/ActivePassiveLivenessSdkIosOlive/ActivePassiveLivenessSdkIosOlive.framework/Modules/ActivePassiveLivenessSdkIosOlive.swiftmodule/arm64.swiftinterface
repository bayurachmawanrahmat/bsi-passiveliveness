// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target arm64-apple-ios14.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name ActivePassiveLivenessSdkIosOlive
import AVFoundation
@_exported import ActivePassiveLivenessSdkIosOlive
import Combine
import Foundation
import MLKitFaceDetection
import MLKitVision
import Swift
import SwiftUI
import UIKit
import _Concurrency
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public struct BoundsPreferenceKey : SwiftUI.PreferenceKey {
  public typealias Value = SwiftUI.Anchor<CoreGraphics.CGRect>?
  public static var defaultValue: ActivePassiveLivenessSdkIosOlive.BoundsPreferenceKey.Value
  public static func reduce(value: inout ActivePassiveLivenessSdkIosOlive.BoundsPreferenceKey.Value, nextValue: () -> ActivePassiveLivenessSdkIosOlive.BoundsPreferenceKey.Value)
}
public func openLivenessDetectorActivity(delegate: ActivePassiveLivenessSdkIosOlive.LivenessDetectorDelegate, schenario: Swift.String?, license: Swift.String?, apiKey: Swift.String?, repeatOnNotPassed: Swift.Bool?, colorPrimary: Swift.String?, gestureList: [Swift.String], scoreIsPassed: Swift.Int?)
@objc @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) public class LivenessViewController : UIKit.UIViewController {
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func viewDidLoad()
  @objc @_Concurrency.MainActor(unsafe) public func closeLivenessDetectorPage()
  @objc deinit
}
public protocol MyLivenessDetectorDelegateProtocol : ObjectiveC.NSObjectProtocol {
  func livenessPickerDidCancel(_ picker: ActivePassiveLivenessSdkIosOlive.LivenessViewController)
  func livenessPicker(_ picker: ActivePassiveLivenessSdkIosOlive.LivenessViewController, didSelect contact: Swift.String)
  func livenessPickerError(_ picker: ActivePassiveLivenessSdkIosOlive.LivenessViewController, didSelect contact: Swift.String)
}
@objc public class LivenessDetectorDelegate : ObjectiveC.NSObject, Foundation.ObservableObject, ActivePassiveLivenessSdkIosOlive.MyLivenessDetectorDelegateProtocol {
  public var livenessOn: (Swift.String) -> Swift.Void
  public var livenessPickerError: (Swift.String) -> Swift.Void
  public var onCancelLiveness: () -> Swift.Void
  public var schenario: Swift.String?
  public var license: Swift.String?
  public var apiKey: Swift.String?
  public var repeatOnNotPassed: Swift.Bool?
  public var colorPrimary: Swift.String?
  public var gestureList: [Swift.String]
  public var scoreIsPassed: Swift.Int?
  public init(livenessOn: @escaping (Swift.String) -> Swift.Void, livenessPickerError: @escaping (Swift.String) -> Swift.Void, onCancelLiveness: @escaping () -> Swift.Void)
  public func livenessPicker(_ picker: ActivePassiveLivenessSdkIosOlive.LivenessViewController, didSelect contact: Swift.String)
  public func livenessPickerError(_ picker: ActivePassiveLivenessSdkIosOlive.LivenessViewController, didSelect contact: Swift.String)
  public func livenessPickerDidCancel(_ picker: ActivePassiveLivenessSdkIosOlive.LivenessViewController)
  public typealias ObjectWillChangePublisher = Combine.ObservableObjectPublisher
  @objc deinit
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public struct CircledIconView : SwiftUI.View {
  public var image: SwiftUI.Image
  public var width: CoreGraphics.CGFloat
  public var color: SwiftUI.Color
  public var strokeColor: SwiftUI.Color
  public init(image: SwiftUI.Image, width: CoreGraphics.CGFloat, color: SwiftUI.Color = Color.black, strokeColor: SwiftUI.Color = Colors.blue(.lightSky).rawValue)
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s32ActivePassiveLivenessSdkIosOlive15CircledIconViewV4bodyQrvp", 0) __
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public struct NumberedCircleView : SwiftUI.View {
  public init(text: Swift.String, width: CoreGraphics.CGFloat = 30.0, color: SwiftUI.Color = Colors.teal.rawValue, delay: Swift.Double = 0.0, triggerAnimation: Swift.Bool = false)
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s32ActivePassiveLivenessSdkIosOlive18NumberedCircleViewV4bodyQrvp", 0) __
}
public var screenWidth: CoreGraphics.CGFloat {
  get
}
public var screenHeight: CoreGraphics.CGFloat {
  get
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public struct StepperView : SwiftUI.View {
  public init()
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s32ActivePassiveLivenessSdkIosOlive11StepperViewV4bodyQrvp", 0) __
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public enum StepperAlignment : Swift.String, Swift.CaseIterable {
  case top
  case center
  case bottom
  public init?(rawValue: Swift.String)
  public typealias AllCases = [ActivePassiveLivenessSdkIosOlive.StepperAlignment]
  public typealias RawValue = Swift.String
  public static var allCases: [ActivePassiveLivenessSdkIosOlive.StepperAlignment] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public enum StepperIndicationType<Content> where Content : SwiftUI.View {
  public typealias Width = CoreGraphics.CGFloat
  case circle(SwiftUI.Color, ActivePassiveLivenessSdkIosOlive.StepperIndicationType<Content>.Width)
  case image(SwiftUI.Image, ActivePassiveLivenessSdkIosOlive.StepperIndicationType<Content>.Width)
  case animation(ActivePassiveLivenessSdkIosOlive.NumberedCircleView)
  case custom(Content)
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public enum StepperLineOptions {
  case defaults
  case custom(CoreGraphics.CGFloat, SwiftUI.Color)
  case rounded(CoreGraphics.CGFloat, CoreGraphics.CGFloat, SwiftUI.Color, SwiftUI.Color = Color.gray.opacity(0.5))
}
public enum StepperMode : Swift.String, Swift.CaseIterable {
  case vertical
  case horizontal
  public init?(rawValue: Swift.String)
  public typealias AllCases = [ActivePassiveLivenessSdkIosOlive.StepperMode]
  public typealias RawValue = Swift.String
  public static var allCases: [ActivePassiveLivenessSdkIosOlive.StepperMode] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
public enum StepLifeCycle : Swift.String, Swift.CaseIterable {
  case pending
  case completed
  public init?(rawValue: Swift.String)
  public typealias AllCases = [ActivePassiveLivenessSdkIosOlive.StepLifeCycle]
  public typealias RawValue = Swift.String
  public static var allCases: [ActivePassiveLivenessSdkIosOlive.StepLifeCycle] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
extension SwiftUI.View {
  public func heightPreference(column: Swift.Int) -> some SwiftUI.View
  
  public func widthPreference(column: Swift.Int) -> some SwiftUI.View
  
  public func widthKey() -> some SwiftUI.View
  
  public func heightKey() -> some SwiftUI.View
  
  public func verticalHeightPreference(column: Swift.Int? = 0) -> some SwiftUI.View
  
  public func pitstopHeightPreference(column: Swift.Int? = 0) -> some SwiftUI.View
  
  public func cgRectPreference() -> some SwiftUI.View
  
  public func setAlignment(type: ActivePassiveLivenessSdkIosOlive.StepperAlignment) -> some SwiftUI.View
  
  public func getAlignment(type: ActivePassiveLivenessSdkIosOlive.StepperAlignment) -> SwiftUI.VerticalAlignment
  public func embedINNavigationView() -> some SwiftUI.View
  
  public func eraseToAnyView() -> SwiftUI.AnyView
  public func getYPosition(for alignment: ActivePassiveLivenessSdkIosOlive.StepperAlignment) -> CoreGraphics.CGFloat
  public func addSteps<Cell>(_ steps: [Cell]) -> some SwiftUI.View where Cell : SwiftUI.View
  
  public func alignments(_ alignments: [ActivePassiveLivenessSdkIosOlive.StepperAlignment] = []) -> some SwiftUI.View
  
  public func stepLifeCycles(_ lifecycle: [ActivePassiveLivenessSdkIosOlive.StepLifeCycle] = [.completed, .completed, .completed]) -> some SwiftUI.View
  
  public func indicators<Cell>(_ indicators: [ActivePassiveLivenessSdkIosOlive.StepperIndicationType<Cell>] = []) -> some SwiftUI.View where Cell : SwiftUI.View
  
  public func stepIndicatorMode(_ mode: ActivePassiveLivenessSdkIosOlive.StepperMode) -> some SwiftUI.View
  
  public func spacing(_ value: CoreGraphics.CGFloat) -> some SwiftUI.View
  
  public func autoSpacing(_ value: Swift.Bool) -> some SwiftUI.View
  
  public func stepperEdgeInsets(_ value: SwiftUI.EdgeInsets) -> some SwiftUI.View
  
  public func lineOptions(_ options: ActivePassiveLivenessSdkIosOlive.StepperLineOptions) -> some SwiftUI.View
  
  public func ifTrue<Content>(_ conditional: Swift.Bool, content: (Self) -> Content) -> some SwiftUI.View where Content : SwiftUI.View
  
  public func addPitStops(_ steps: [SwiftUI.AnyView]) -> some SwiftUI.View
  
  public func pitStopLineOptions(_ options: [ActivePassiveLivenessSdkIosOlive.StepperLineOptions]) -> some SwiftUI.View
  
  public func animateSteps(_ count: Swift.Int) -> some SwiftUI.View
  
  public func loadingAnimationTime(_ time: Swift.Double) -> some SwiftUI.View
  
  public func log(_ log: Swift.String) -> SwiftUI.EmptyView
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public struct TextView : SwiftUI.View {
  public var text: Swift.String
  public var font: SwiftUI.Font
  public init(text: Swift.String, font: SwiftUI.Font = .caption)
  @_Concurrency.MainActor(unsafe) public var body: some SwiftUI.View {
    get
  }
  public typealias Body = @_opaqueReturnTypeOf("$s32ActivePassiveLivenessSdkIosOlive8TextViewV4bodyQrvp", 0) __
}
public struct Utils {
}
extension Swift.Dictionary {
  public func printAsJSON()
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
public enum Colors {
  case red(ActivePassiveLivenessSdkIosOlive.Colors.RedSubType)
  case green(ActivePassiveLivenessSdkIosOlive.Colors.GreenSubType)
  case blue(ActivePassiveLivenessSdkIosOlive.Colors.BlueSubType)
  case gray(ActivePassiveLivenessSdkIosOlive.Colors.GraySubType)
  case teal
  case lavendar
  case orange
  case black
  case yellow(ActivePassiveLivenessSdkIosOlive.Colors.YellowSubType)
  case cyan
  case polar
  public enum GraySubType {
    case dark
    case darker
    case medium
    case dim
    case light
    case lighter
    case lightest
    case silver
    case mediumSilver
    case darkSilver
    case pale
    case battleShip
    case slate
    case charcoal
    case bright
    case paleSky
    case iron
    case cod
    public static func == (a: ActivePassiveLivenessSdkIosOlive.Colors.GraySubType, b: ActivePassiveLivenessSdkIosOlive.Colors.GraySubType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum GreenSubType {
    case normal
    case dark
    case light
    case lighter
    case medium
    case fair
    case kiwi
    case darkLime
    case kermit
    case teal
    case lightTeal
    case lighterTeal
    case regular
    public static func == (a: ActivePassiveLivenessSdkIosOlive.Colors.GreenSubType, b: ActivePassiveLivenessSdkIosOlive.Colors.GreenSubType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum BlueSubType {
    case sky
    case lightSky
    case tiffany
    case aqua
    case aquaMarine
    case turquoise
    case teal
    case sea
    case bright
    public static func == (a: ActivePassiveLivenessSdkIosOlive.Colors.BlueSubType, b: ActivePassiveLivenessSdkIosOlive.Colors.BlueSubType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum YellowSubType {
    case regular
    case sunFlower
    public static func == (a: ActivePassiveLivenessSdkIosOlive.Colors.YellowSubType, b: ActivePassiveLivenessSdkIosOlive.Colors.YellowSubType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum RedSubType {
    case normal
    case light
    public static func == (a: ActivePassiveLivenessSdkIosOlive.Colors.RedSubType, b: ActivePassiveLivenessSdkIosOlive.Colors.RedSubType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
}
@available(iOS 13.0, macOS 10.15, tvOS 13.0, watchOS 6.0, *)
extension ActivePassiveLivenessSdkIosOlive.Colors {
  public typealias RawValue = SwiftUI.Color
  public var rawValue: ActivePassiveLivenessSdkIosOlive.Colors.RawValue {
    get
  }
}
