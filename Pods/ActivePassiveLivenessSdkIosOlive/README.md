# Active Passive Liveness Sdk iOS

Active Passive Liveness Sdk is a simple library used to verification

## Configuration
First thing you need to do is create a project xcworkspace (with podfile) and config ios minimum version 14.0 


## Instalation 

add this on your podfile then run this command pod install (arch -x86_64 pod install) for m1 

```
target 'ExampleYourProject' do
  # Comment the next line if you don't want to use dynamic frameworks
  use_frameworks!
  
  
  # and these lines Pods for ExampleYourProject
	pod 'ActivePassiveLivenessSdkIosOlive',
        :git => "https://credence_name:credence_token@gitlab.com/oliveness/active-passive-liveness-sdk-ios.git",
        :tag => "0.0.2"
    pod 'GoogleMLKit/FaceDetection', '~>2.3.0'
end


post_install do |installer|
	# add these lines:
	installer.pods project.build_configurations.each do |config|
		config.build_settings["EXCLUDED_ARCHS[sdk=iphonesimulator*]"] = "armv7" 
		config.build_settings["EXCLUDED_ARCHS[sdk=iphonesimulator*]"] = "arm64" 		
		config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES' 		
		config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '14.0' 		
	end 
end
```






## Notes for IOS


1 **Check IOS minimum version**

Active Passive Liveness Sdk functionality works on iOS 14.0 or higher. If compiling for any version lower than
14.0, face detection feactures maybe not run, or cannot build

Make sure your podfile is at the top  platform :ios 14.0


2 **Add Camera Permission**


Add One rows to the ios/Runner/Info.plist:

one with the key Privacy - Camera Usage Description and a usage description.

<key>NSCameraUsageDescription</key>
<string>Can I use the camera please?</string>



## how to use it iOS

### 1. Create a file for config example **Config.swift** 


Then add your `apiKey` and `LicenseKey` in your project file to called this sdk

```
import Foundation

struct Keys {
    static let licenseKey = "Your License Key"
    static let apiKey = "Your Api Key"
}


```



### 2. Create a button for call this function whatever you want to add it

```
import ActivePassiveLivenessIosSdkOlive

Button("schenario 1"){
            openLivenessDetectorActivity(
                delegate:
                    LivenessDetectorDelegate(
                        livenessOn: { resultLiveness in
                            print(resultLiveness)
                            return
                            
                        },
                        livenessPickerError: { resultLivenessError in
                            print(resultLivenessError)
                            return
                            
                        },
                        onCancelLiveness: {
                            print("cancel by user")
                        }),
                schenario: "schenario1",
                license: Keys.licenseKey,
                apiKey: Keys.apiKey,
                repeatOnNotPassed: false,
                colorPrimary: "#F5D25D"
            )
        }


```


### 3. Output from this sdk
livenessOn 



  	"status": "OK",
    "code": "201",
    "message": "fsdfsadf",
    "data": {
        "passed": true,
        "score": "0.59",
        "selfie_photo": ""
    }


livenessPickerError



  	"just message error"
  	
onCancelLiveness



  	call action for whatever you want if user cancel or close this screen sdk




## Full Example Code one file with parsing json
```

import SwiftUI
import ActivePassiveLivenessSdkIosOlive



struct ContentView: View {
    var body: some View {
        Text("Testing schenario Olive")
        Button("schenario 1"){
            openLivenessDetectorActivity(
                delegate:
                    LivenessDetectorDelegate(
                        livenessOn: { resultLiveness in
                            let jsonData = resultLiveness.data(using: .utf8)!
                            let resultSdk: ResultLivenessOlive = try! JSONDecoder().decode(ResultLivenessOlive.self, from: jsonData)
                            print(resultSdk.image)
                            print(resultSdk.data.passed)
                            print(resultSdk.data.score)
                            
                            guard let jsonData = try? JSONEncoder().encode(OliveDataConvert(passed: resultSdk.data.passed, score: String(resultSdk.data.score))) else {
                                showAlertFromIos(title: "Message", message: "Bad request: Try again")
                                print("cannot parsing data")
                                return
                            }
                            let responseStrData: String = String(data: jsonData, encoding: .utf8) ?? "no response"
                            print(responseStrData)
                            let resultExampleForYouWant = [
                                "data": responseStrData,
                                "image": resultSdk.image
                            ] as [String : Any]
                            print(resultExampleForYouWant)
                            
                        },
                        livenessPickerError: { resultLivenessError in
                            showAlertFromIos(title: "Message", message: resultLivenessError)
                            print(resultLivenessError)
                            
                        },
                        onCancelLiveness: {
                            print("cancel by user")
                        }),
                schenario: "schenario1",
                license: Keys.licenseKey,
                apiKey: Keys.apiKey,
                repeatOnNotPassed: false,
                colorPrimary: "#F5D25D"
            )
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .frame(width: nil)
        
    }
}



struct Keys {
    static let licenseKey = "your license key"
    static let apiKey = "your api key"
}


struct ResultLivenessOlive: Codable  {
    let status: String
    let code: String?
    let message: String?
    var image: String
    let data: OliveData
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case code = "code"
        case message = "message"
        case image = "image"
        case data
    }
}

struct OliveData: Codable {
    let externalID: String?
    let passed: Bool
    let score, selfiePhoto: String
    
    enum CodingKeys: String, CodingKey{
        case externalID = "external_id"
        case passed, score
        case selfiePhoto = "selfie_photo"
    }
}




struct OliveDataConvert: Codable {
    let passed: Bool
    let score: String
    
    enum CodingKeys: String, CodingKey{
        case passed, score
    }
}


func showAlertFromIos(title: String, message: String) {
    DispatchQueue.main.async {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(
            UIAlertAction(
                title: "OK", style: .default,
                handler: { action in
                    switch action.style {
                    case .default:
                        print("default")
                        
                    case .cancel:
                        print("cancel")
                        
                    case .destructive:
                        print("destructive")
                        
                    @unknown default:
                        print("not return")
                    }
                }))
        
        let keyWindow = UIApplication.shared.windows.first(where: { $0.isKeyWindow })
        let rootViewController = keyWindow?.rootViewController
        DispatchQueue.main.async {
            rootViewController?.present(alert, animated: true)
        }
    }
}

``` 


## Schenario available

this is a list of schenario available in this SDK

- schenario1 ("Tersenyum","Kedipkan Mata","Membuka Mulut","Gelengkan Kepala")
- schenario2 ("Membuka Mulut","Kedipkan Mata","Gelengkan Kepala","Tersenyum")
- schenario3 ("Kedipkan Mata","Membuka Mulut","Gelengkan Kepala","Tersenyum")
- schenario4 ("Tersenyum","Membuka Mulut", "Kedipkan Mata","Gelengkan Kepala")
- schenario5 ("Gelengkan Kepala","Tersenyum","Kedipkan Mata","Membuka Mulut")
- schenario6 ("Tersenyum","Gelengkan Kepala", "Membuka Mulut","Kedipkan Mata")
- schenario7 ("Membuka Mulut","Gelengkan Kepala","Kedipkan Mata","Tersenyum")
- schenario8 ("custom gesture")

## Gesture name list

only for custom gesture

1. GestureType.smile
2. GestureType.openMounth
3. GestureType.shakeHead
4. GestureType.blinkEyes
