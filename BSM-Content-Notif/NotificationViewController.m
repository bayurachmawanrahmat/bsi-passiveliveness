//
//  NotificationViewController.m
//  BSM-Content-Notif
//
//  Created by Alikhsan on 23/07/19.
//  Copyright © 2019 lds. All rights reserved.
//

#import "NotificationViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>

@interface NotificationViewController () <UNNotificationContentExtension>

@property IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *imgNotification;

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any required interface initialization here.
}

- (void)didReceiveNotification:(UNNotification *)notification {
    self.label.text = notification.request.content.body;
    NSArray *attachements = notification.request.content.attachments;
    NSLog(@"%@", attachements);
}

@end
