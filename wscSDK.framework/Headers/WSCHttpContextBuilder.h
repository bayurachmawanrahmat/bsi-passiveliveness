/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import <Foundation/Foundation.h>
#import "WSCHttpContext.h"

@class WSCHttpContext;

/**
 * Builder for the WSCHttpContext class.
 */
@interface WSCHttpContextBuilder : NSObject

/** The context headers. */
@property (readonly) NSMutableDictionary *headers;

/** The context sslContext. */
@property (readonly) SSLContextRef *sslContextRef;

/**
 * Creates a new builder.
 */
+(WSCHttpContextBuilder *)create;

/**
 * Creates a new builder with an array of headers.
 *
 * @param name header name
 * @param values Header values as an array
 * @return WSCHttpContextBuilder
 */
-(WSCHttpContextBuilder *)withHeaders:(NSString *)name values:(NSArray *)values;

/**
 * Creates a new builder with a specific header.
 *
 * @param name header name
 * @param value Header value
 * @return WSCHttpContextBuilder
 */
-(WSCHttpContextBuilder *)withHeader:(NSString *)name value:(NSString *)value;

/**
 * Creates a new builder with an SSL context reference.
 *
 * @param sslContext sslContext
 * @return WSCHttpContextBuilder
 */
-(WSCHttpContextBuilder *)withSSLContextRef:(SSLContextRef *)sslContext;

/**
 * Builds the WSCHttpContext object.
 */
-(WSCHttpContext *)build;

@end
