/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import <Foundation/Foundation.h>

/**
 * WSCIceServer representation holding URL, username and password for an ICE server.
 *
 * The allowed formats for the url string are: <br>
 * "TYPE:203.0.113.2:3478" - Indicates a specific IP address and port for the server.<br>
 * "TYPE:relay.example.net:3478" - Indicates a specific host and port for the server; the user agent
 * will look up the IP address in DNS.<br>
 * "TYPE:example.net" - Indicates a specific domain for the server; the user agent will look up
 * the IP address and port in DNS.<br>
 *
 * The "TYPE" is one of:
 * <ul>
 * <li>STUN - Indicates a STUN server</li>
 * <li>STUNS - Indicates a STUN server that is to be contacted using a TLS session.</li>
 * <li>TURN - Indicates a TURN server</li>
 * <li>TURNS - Indicates a TURN server that is to be contacted using a TLS session.</li>
 * </ul>
 */
@interface WSCIceServer : NSObject

/**
 * URL of ICE Server. For example, "turn:10.111.12.13:3478".
 */
@property(nonatomic, copy, readonly) NSString *url;

/**
 * Username for the ICE Server.
 */
@property(nonatomic, copy, readonly) NSString *username;

/**
 * Password for the ICE Server.
 */
@property(nonatomic, copy, readonly) NSString *password;

/**
 * Returns an initialized WSCCall object URL.
 *
 * @param url ICE Server URL
 */
-(instancetype)initWithUrl:(NSString *)url;

/**
 * Returns an initialized WSCCall object with URL, username, and apassword.
 *
 * @param url ICE server URL
 * @param username ICE server username
 * @param password ICE server password
 */
-(instancetype)initWithUrl:(NSString *)url
                  username:(NSString *) username
                  password:(NSString *) password;

/** Disallows init. */
-(instancetype) init __attribute__((unavailable("init not available")));

@end
