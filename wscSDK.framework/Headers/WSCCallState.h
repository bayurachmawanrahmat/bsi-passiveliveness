/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/** Represents the various states of a WSCCall. */
typedef NS_ENUM(NSInteger, WSCCallState) {
  /** Initial call state. Call has been created. */
  WSCCallStateNone,
  /** The call has started. */
  WSCCallStateStarted,
  /**
   * For the call initiator, this state means the call request has been responded to by the remote side;
   * for the call receiver,
   * this state means the receiver has responded to the incoming request.
   */
  WSCCallStateResponded,
  /** The call has been established. */
  WSCCallStateEstablished,
  /** The call met with an exception and has been closed abnormally. */
  WSCCallStateFailed,
  /** The call has ended due to rejection. */
  WSCCallStateRejected,
  /** The call has been closed normally. */
  WSCCallStateEnded
};
