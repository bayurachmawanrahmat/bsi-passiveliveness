/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

/**
 * WSCMessagingMessage represents the message object that is handed over to the application when an incoming
 * message arrives over SIP. It contains the sender of the message along with the contents of the message.
 */
@interface WSCMessagingMessage : NSObject

/** The message Id - generated internally by the SDK. */
@property(nonatomic, readonly) NSString* messageId;

/** The sender of the message. */
@property(nonatomic, readonly) NSString* initiator;

/** The receiver of the message. */
@property(nonatomic, readonly) NSString* target;

/** The content of the message. */
@property(nonatomic, readonly) NSString* content;

/**
 * Initiates a WSCMessagingMessage object.
 *
 * @param msgId the msgId
 */
-(instancetype)initWithId:(NSString *)msgId;

/**
 * Disallows init.
 */
//-(instancetype) init __attribute__((unavailable("init not available")));

@end