//
//  WSCErrorType.h
//  wscSDK
//
//  Created by zxz on 17/2/6.
//  Copyright © 2017 Oracle. All rights reserved.
//

#ifndef WSCErrorType_h
#define WSCErrorType_h


#endif /* WSCErrorType_h */

typedef NS_ENUM (NSInteger, WSCErrorType) {
  
  SIGNALING_ERROR,
  
  GENERATE_SDP_ERROR,
  
  ICE_CONNECTION_ERROR,
  
  SET_LOCAL_SDP_ERROR,
  
  SET_REMOTE_SDP_ERROR
};
