/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCHttpContext.h"
#import "WSCIceServerConfig.h"
#import "WSCServiceAuthHandler.h"
#import "WSCSessionState.h"
#import "WSCStatusCode.h"
#import "WSCSubSession.h"
#import "WSCSessionBuilder.h"
#import "WSCHibernateParams.h"
#import "WSCFrame.h"

@class WSCSessionBuilder;
@class WSCSubSession;

/**
 * WSCSession represents a session between the client and the WebRTC Session Controller server.
 * A client application is expected to create only one session towards the WebRTC Session Controller server for
 * each user. A session may have one or more sub-sessions. <br/>
 * <br/>
 * When receiving an incoming message from WebRTC Session Controller, the session dispatches the message to the
 * appropriate package handler like WSCCallPackage in order to process the message.
 */
@interface WSCSession : NSObject

/**
 * Integer property in seconds to check and send any unacknowledged messages.
 * Default is 60 seconds.
 * Usage: [NSNumber numberWithInt: 60]
 */
extern NSString * const WSC_PROP_ACK_INTERVAL;

/**
 * Integer property in seconds for the ping interval when busy (active sub-sessions).
 * Default is 3 seconds.
 * Usage: [NSNumber numberWithInt: 5]
 */
extern NSString * const WSC_PROP_BUSY_PING_INTERVAL;

/**
 * Integer property in seconds for the ping interval when idle (no sub-sessions).
 * Default is 10 seconds.
 * Usage: [NSNumber numberWithInt: 20]
 */
extern NSString * const WSC_PROP_IDLE_PING_INTERVAL;

/**
 * Integer property in seconds for how often a reconnect should be attempted when the connection is lost.
 * Default is 2 seconds.
 * Usage: [NSNumber numberWithInt: 10]
 */
extern NSString * const WSC_PROP_RECONNECT_INTERVAL;

/**
 * Integer property in seconds for the maximum reconnect period after which reconnect attempts are stopped
 * and session is closed.
 * Default is 60 seconds.
 * Usage: [NSNumber numberWithInt: 100]
 */
extern NSString * const WSC_PROP_RECONNECT_TIMEOUT;

/**
 * Boolean property that specifies whether the SSL trust chain should <b>not</b> be evaluated.
 * Setting this value to YES allows connections to any root certificates.
 * Default is NO.
 * Usage: [NSNumber numberWithBool: YES]
 */
extern NSString * const WSC_PROP_ALLOW_UNTRUSTED_SSL_CERTS;

/**
 * ICE Server Configuration.
 */
@property(nonatomic, readonly) WSCIceServerConfig *iceServerConfig;

@property(nonatomic, readonly) NSArray *systemIceServers;

@property(nonatomic,copy) NSDate *signalingStartTime;

@property(nonatomic,copy) NSDate *signalingEndTime;

@property(nonatomic) BOOL  enableVerbose;

@property (nonatomic,copy) NSString *accessToken;

@property (nonatomic,copy) NSString *registerSubSessionId;



/**
 * Returns an initialized WSCSession object with session builder.
 * @param builder WSCSessionBuilder.
 */
- (instancetype)initWithWSCSessionBuilder:(WSCSessionBuilder *)builder;

/**
 * Opens the WebRTC Session Controller session based on initialization parameters to the Server.
 * Status will be sent to WSCSessionObserverDelegate.
 */
- (void)open;

/**
 * Closes the connection towards the Server.
 */
- (void)close;

/**
 * Initiates a hibernate request towards the server.
 * The deviceToken should have been passed during the session creation process. If not, the request will fail.
 *
 * @param params Parameters for hibernation
 */
-(void)hibernate:(WSCHibernateParams *) params;

/**
 * Shuts down the currently open connection to the server and returns the state information.
 *
 * @return NSString
 */
-(NSString *)suspend;

/**
 * Generates a correlation ID based on current outbound sequence number of this session.
 * For example, if the current outbound sequence number is 100, "c101" is returned by this method
 */
- (NSString *)generateCorrelationId;

/**
 * Generates a random UUID subsession id according to RFC 4122 v4.
 */
- (NSString *)generateSubSessionId;

/**
 * Returns a package from the WebRTC Session Controller session.
 *
 * @param packageType Package name for package to return
 */
- (WSCPackage *)getPackage:(NSString *)packageType;

/**
 * Returns all current packages in the WebRTC Session Controller session.
 */
- (NSArray *)getPackages;

/**
 * Used to determine if UPDATE should be sent on ICE response.  Used when handling relayFailed messages.
 * @return true or false
 */
- (BOOL) isUpdateOnIceResponse;

/**
 * Set to true of an update is required on ICE response.  Used when a relayfailed message occurs.
 * @param updateOnIceResponse true or false
 */
- (void) setUpdateOnIceResponse:(BOOL) updateOnIceResponse;

/**
 * Used to determine if START should be sent on ICE response. Used when refreshing empty ICE list on a new call.
 * @return true or false
 */
- (BOOL) isStartOnIceResponse;

/**
 * Set to true if an start is required on ICE response. Used when refreshing empty ICE list on new call.
 * @param updateOnIceResponse true or false
 */
- (void) setStartOnIceResponse:(BOOL) updateOnIceResponse;

/**
 * Returns all sub-sessions for a registered package.
 *
 * @param packageName Package name
 */
- (NSArray *)getSubSessionsByPackage:(NSString *)packageName;

/**
 * Returns a sub-session.
 *
 * @param subSessionId sub-session identifier for sub-session to return
 */
- (WSCSubSession *)getSubSession:(NSString *)subSessionId;

/**
 * Adds a sub-session to the session.
 *
 * @param subSession sub-session to add
 */
- (void)putSubSession:(WSCSubSession *)subSession;

/**
 * Removes a sub-session from the session.
 *
 * @param subSessionId sub-session identifier.
 */
- (void)removeSubSession:(NSString *)subSessionId;

/**
 * Gets all the sub-sessions in this session.
 */
- (NSArray *)getSubSessions;

/**
 * Gets the registered username associated with this session.
 */
- (NSString*)getUsername;

/**
 * Gets the session ID.
 */
- (NSString*)getSessionId;

/**
 * Gets the session state.
 */
- (WSCSessionState)getSessionState;

/**
 * Gets the property value for a given property name.
 *
 * @param name Name for the property
 */
- (NSObject *)getProperty:(NSString*)name;

/**
 * Sends a WebRTC Session Controller message frame to the session.
 * @param frame WebRTC Session Controller frame to be sent.
 */
-(void)sendMessage:(WSCFrame *)frame;

/** Disallows init. */
-(instancetype) init __attribute__((unavailable("init not available")));

@end
