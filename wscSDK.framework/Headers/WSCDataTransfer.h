/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

#import <WebRTC/RTCDataChannel.h>

/**
 * Represents the sender of WSCDataTransfer that can send data over the underlying data channel.
 */
@interface WSCDataSender : NSObject

/**
 * Sends data.
 *
 * @param data RTCDataBuffer to be sent.
 */
-(void) send:(RTCDataBuffer *)data;

/**
 * Disallows init.
 */
-(instancetype) init __attribute__((unavailable("init not available")));

@end


/**
 * Delegate observer protocol that the application can implement to be informed of incoming messages
 * in the WSDDataReceiver.
 */
@protocol WSCDataReceiverObserverDelegate <NSObject>

/**
 * Invoked when there is an incoming message.
 *
 * @param data RTCDataBuffer which is received by this data channel
 */
-(void) onMessage:(RTCDataBuffer *)data;

@end


/**
 * Represents the receiver of WSCDataTransfer that can receive raw data over the underlying data channel.
 */
@interface WSCDataReceiver : NSObject

/**
 * Call observer delegate WSCDataReceiverObserverDelegate to be informed of incoming messages.
 */
@property (nonatomic, weak) id<WSCDataReceiverObserverDelegate> observerDelegate;

/**
 * Disallow init.
 */
-(instancetype) init __attribute__((unavailable("init not available")));

@end


/**
 * Define the state of the WSCDataTransfer.
 */
typedef NS_ENUM(NSInteger, WSCDataTransferState) {
  /** The DataTransfer object is created but no data channel is established or initializing. */
  WSCDataTransferNone,
  /** The data channel is initializing or in negotiation to be established. */
  WSCDataTransferStarting,
  /** The data channel is established successfully and the ready state of the data channel is open. */
  WSCDataTransferOpen,
  /** The data channel has been closed. */
  WSCDataTransferClosed
};


@class WSCDataTransfer;

/**
 *  Delegate observer protocol that the application can implement to be informed of changes in the WSDDataTransfer.
 */
@protocol WSCDataTransferObserverDelegate <NSObject>

/**
 * Invoked when the data channel of a WSCDataTransfer object is open.
 *
 * @param dataTransfer The WSCDataTransfer of this delegate.
 */
-(void) onOpen:(WSCDataTransfer *)dataTransfer;

/**
 * Invoked when the data channel of a WSCDataTransfer object is closed.
 *
 * @param dataTransfer The WSCDataTransfer of this delegate.
 */
-(void) onClose:(WSCDataTransfer *)dataTransfer;

/**
 * Invoked when the data channel of WSCDataTransfer object encounters an error.
 *
 * @param dataTransfer The WSCDataTransfer of this delegate.
 */
-(void) onError:(WSCDataTransfer *)dataTransfer;

@end


/**
 * Represents handle to all underlying data channel operations.
 * An instance is created by the WSCCall object if the WSCCallConfig includes WSCDataChannelConfig.
 */
@interface WSCDataTransfer : NSObject

/**
 * The label to identify this WSCDataTransfer
 */
@property(nonatomic, readonly) NSString *label;

/**
 * The receiver of WSCDataTransfer that can receive raw data over the underlying data channel.
 */
@property(nonatomic, readonly) WSCDataReceiver *receiver;

/**
 * The sender of WSCDataTransfer that can send data over the underlying data channel
 */
@property(nonatomic, readonly) WSCDataSender *sender;

/**
 * The state of the WSCDataTransfer
 */
@property(nonatomic, readonly) WSCDataTransferState state;

/**
 *  Data transfer observer delegate for being informed of changes in the Data transfer.
 */
@property(nonatomic, weak) id<WSCDataTransferObserverDelegate> observerDelegate;

/**
 * Disallow init.
 */
-(instancetype) init __attribute__((unavailable("init not available")));

@end

