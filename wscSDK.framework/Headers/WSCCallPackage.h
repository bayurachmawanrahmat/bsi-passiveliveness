/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCPackage.h"
#import "WSCCall.h"
#import "WSCCallConfig.h"

/**
 * Delegate observer protocol for CallPackage to receive asynchronous messages.
 */
@protocol WSCCallPackageObserverDelegate <NSObject>

/**
 * Called when a new incoming WSCCall has arrived.
 *
 * @param call Call that was received
 * @param callConfig Call configuration for the call received
 * @param extHeaders Dictionary of extension headers
 */
-(void)callArrived:(WSCCall *)call
        callConfig:(WSCCallConfig *)callConfig
        extHeaders:(NSDictionary *)extHeaders;



/**
 * Called when a WSCCall is resurrected.
 *
 * @param call Call that was resurrected
 * @param callConfig WSCCallConfig config of resurrected call
 */
-(void)callResurrected:(WSCCall *)call
            callConfig:(WSCCallConfig *)callConfig;

@end

/**
 * Default package type.
 */
extern NSString * const PACKAGE_TYPE_CALL;

/**
 * Package handler that enables call applications. An object of the WSCCallPackage class
 * can be created by applications to manage WSCCall objects. The object also dispatches
 * received messages to corresponding Call objects.
 */
@interface WSCCallPackage : WSCPackage

/**
 * WSCCallPackage delegate for callbacks.
 */
@property(nonatomic, weak) id <WSCCallPackageObserverDelegate> observerDelegate;

/**
 * Returns an initialized WSCCallPackage object.
 */
-(instancetype)init;

/**
 * Returns an initialized WSCCallPackage object with package type.
 *
 * @param packageType Package type
 */
-(instancetype)initWithPackageType:(NSString *)packageType;

/**
 * Creates a new call.
 *
 * @param target Callee target for call
 */
-(WSCCall *)createCall:(NSString *)target;

/**
 * This is internal method, will be used in Cloud sdk, the new call will not put into subsession map.
 *
 * @param caller Caller initiator for call
 * @param callee Callee target for call
 * @param subSessionId subsession Id
 */
-(WSCCall *)createInnerCall:(NSString *)caller
                     callee:(NSString *)callee
               subSessionId:(NSString *)subSessionId;

/**
 * This is internal method, will be used in Cloud sdk. When the call is ended, dispose it.
 *
 * @param call the given call
 */
-(void)disposeInnerCall:(WSCCall *)call;
@end
