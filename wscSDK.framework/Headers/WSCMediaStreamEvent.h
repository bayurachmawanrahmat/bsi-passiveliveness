/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/**
 * Media stream event state.
 */
typedef NS_ENUM(NSInteger, WSCMediaStreamEvent) {
  /**
   * Local media stream was added to the call.
   */
  WSCMediaStreamEventLocalStreamAdded,
  /**
   * Local media stream was removed from the call.
   */
  WSCMediaStreamEventLocalStreamRemoved,
  /**
   * Error occurred with the local media stream.
   */
  WSCMediaStreamEventLocalStreamError,
  /**
   * Remote media stream was added to the call.
   */
  WSCMediaStreamEventRemoteStreamAdded,
  /**
   * Remote media stream was removed from the call.
   */
  WSCMediaStreamEventRemoteStreamRemoved,
  /**
   * Error occurred with the remote media stream.
   */
  WSCMediaStreamEventRemoteStreamError
};
