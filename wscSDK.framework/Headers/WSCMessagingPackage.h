/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

#import "WSCPackage.h"
#import "WSCMessagingMessage.h"
#import "WSCCause.h"

/**
 * Delegate protocol for Messaging to receive incoming messages and acknowledgments.
 */
@protocol WSCMessagingDelegate <NSObject>

/**
 * Called when a new incoming Message has arrived.
 *
 * @param message Message that was received
 * @param extHeaders Dictionary of extension headers
 */
-(void)onNewMessage:(WSCMessagingMessage *) message
         extHeaders:(NSDictionary *)extHeaders;

/**
 * Called when an acknowledgement (accepted on the other side) for sent message has arrived.
 *
 * @param message Message for which ack is received
 * @param extHeaders Dictionary of extension headers
 */
-(void)onSuccessResponse:(WSCMessagingMessage *) message
              extHeaders:(NSDictionary *)extHeaders;

/**
 * Called when a negative acknowledgement (rejected on the other side) for sent message has arrived.
 *
 * @param message Message for which ack is received.
 * @param cause cause for error.
 * @param reason textual description on the error response.
 * @param extHeaders Dictionary of extension headers.
 */
-(void)onErrorResponse:(WSCMessagingMessage *) message
                 cause:(WSCCause *)cause
                reason:(NSString*)reason
            extHeaders:(NSDictionary *)extHeaders;

@end

/**
 * Default package type.
 */
extern NSString * const PACKAGE_TYPE_MESSAGING;

/**
 * Package handler that enables SIP-based messaging. An object of the WSCMessagingPackage class
 * can be created by applications and registered while creating a session. A WSCMessaging object should then be used to
 * send and receive messages.
 */
@interface WSCMessagingPackage : WSCPackage

/**
 * Returns an initialized WSCChatPackage object.
 */
-(instancetype)init;

/**
 * Returns an initialized WSCChatPackage object with package type.
 *
 * @param packageType Package type
 */
-(instancetype)initWithPackageType:(NSString *)packageType;

/**
 * Messaging delegate for being informed of new or acknowledgements of sent messages.
 */
@property(nonatomic, weak) id <WSCMessagingDelegate> observerDelegate;

/**
 * Sends a message to a given target.
 * @param textMessage Text Message to be sent
 * @param target user to whom the message should be sent
 * @param extHeaders Dictionary of extension headers
 */
-(NSString *)send:(NSString *)textMessage
           target:(NSString *)target
       extHeaders:(NSDictionary *)extHeaders;

/**
 * Acknowledgement that a message was received and accepted by the user. This should be called on the incoming message.
 * @param message message that should be acked
 * @param extHeaders Dictionary of extension headers
 */
-(void)accept:(WSCMessagingMessage *)message
   extHeaders:(NSDictionary *)extHeaders;

/**
 * Acknowledgement that a message was received but rejected by the user. This should be called on the incoming message.
 * @param message message that should be acked
 * @param cause cause for rejecting the message
 * @param reason textual description on why the message is rejected
 * @param extHeaders Dictionary of extension headers
 */
-(void)reject:(WSCMessagingMessage *)message
        cause:(WSCCause *)cause
       reason:(NSString *)reason
   extHeaders:(NSDictionary *)extHeaders;

/**
 * Gets the handle to the message.
 * @param messageId unique id for the message obtained by send(...) api
 */
-(WSCMessagingMessage *)getMessage:(NSString *)messageId;

@end
