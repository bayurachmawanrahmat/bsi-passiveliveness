/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/** Represents the state of the WSCSession. */
typedef NS_ENUM(NSInteger, WSCSessionState) {
  /** Initial session state. Session has been created. */
  WSCSessionStateNone,
  /** Session has connected with the server. */
  WSCSessionStateConnected,
  /** Session's connection with the server is broken; client is reconnecting to server. */
  WSCSessionStateReconnecting,
  /** Session has been hibernated. */
  WSCSessionStateHibernated,
  /** Session is handed over and has been closed abnormally by code 1001 for suspension purposes. */
  WSCSessionStateSuspended,
  /** Session failed to connect or was abnormally closed. */
  WSCSessionStateFailed,
  /** Session has been normally terminated. */
  WSCSessionStateClosed
};
