/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCMediaStreamEvent.h"
#import "WSCPackage.h"
#import "WSCCallConfig.h"
#import "WSCCause.h"
#import "WSCCallUpdateEvent.h"
#import "WSCCallState.h"
#import "WSCTrickleIceMode.h"
#import "WSCCallState.h"
#import "WSCErrorType.h"
#import "WSCErrorData.h"
#import "WSCEventData.h"
#import "WSCEventTimeType.h"
#import "WSCEventTimeData.h"
#import "WSCEventTimeTypeHandler.h"
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCPeerConnectionFactory.h>
#import <WebRTC/RTCPeerConnection.h>

@class WSCOfferAnswerState;
@class WSCDataTransfer;

/**
 * Delegate observer protocol that the application can implement to be informed of changes in the WSCCall.
 */
@protocol WSCCallObserverDelegate <NSObject>

/**
 * Called when a call has had a state change.
 *
 * @param callState the WSCCallState indicating current call state
 * @param cause The WSCCause cause for the notification
 * @param extHeaders Extension headers if provided
 */
-(void)stateChanged:(WSCCallState)callState
              cause:(WSCCause *)cause
         extHeaders:(NSDictionary *)extHeaders;
/**
 * Called when a call has had a media state change.
 *
 * @param mediaStreamEvent the WSCMediaStreamEvent indicating the media stream event
 * @param mediaStream Media stream being updated, or NIL if not applicable
 */
-(void)mediaStateChanged:(WSCMediaStreamEvent)mediaStreamEvent
             mediaStream:(RTCMediaStream *)mediaStream;

/**
 * Called when receiving an update event for an established call.
 *
 * @param event Update event
 * @param callConfig New call configuration
 * @param cause The source cause for the notification
 * @param extHeaders Extension headers if provided
 */
-(void)callUpdated:(WSCCallUpdateEvent)event
        callConfig:(WSCCallConfig *)callConfig
             cause:(WSCCause *)cause
        extHeaders:(NSDictionary *)extHeaders;

/**
 * Called when a WSCDataTransfer object is created.
 *
 * @param dataTransfer the WSCDataTransfer object is created for application
 */
-(void)onDataTransfer:(WSCDataTransfer *)dataTransfer;

-(void)onError:(WSCErrorType)errorType
             params:(NSArray *)params;

@end

@protocol WSCIceObserver <NSObject>

 -(void)onIceConnectionChange:(RTCIceConnectionState)iceConnectionState;

@end
/**
 * Represents a call which could have any combination of Audio/Video (AV) capabilities.
 *
 * The AV capabilities of a WSCCall are determined when it is initialized and are based on the WSCCallConfig data
 * provided in the WSCCallPackage provided at initialization. The AV capabilities can be updated at a later date
 * by the client, or they can be updated by an incoming call state change from the network.
 * The latter would be initiated by the other call party and signaled through the CallObserverDelegate.
 */
@interface WSCCall : WSCSubSession

/** Compatibility flag to force a new peer connection on every SDP exchange. Default is false. */
extern NSString * const WSC_PROP_PEER_CONNECTION_FORCE_NEW;

/**
 * Call observer delegate for being informed of changes in the Call.
 */
@property(nonatomic, weak) id <WSCCallObserverDelegate> observerDelegate;

/**
 * Caller for the call.
 */
@property(nonatomic, readonly, copy) NSString *caller;

/**
 * Callee for the call.
 */
@property(nonatomic, readonly, copy) NSString *callee;

/**
 * Stream Id.
 */
@property(nonatomic, copy) NSString *streamId;

/**
 * Stream Type.
 */
@property(nonatomic, readonly, copy) NSString *streamType;

/**
 * Call peer connection.
 */
@property(nonatomic, readonly) RTCPeerConnection *peerConnection;

/**
 * Call WSCTrickleIceMode. This determines what Trickle ICE mode the call uses.
 */
@property(nonatomic) WSCTrickleIceMode trickleIceMode;

@property(nonatomic,retain) NSMutableArray<WSCErrorData *> *errorList;

@property(nonatomic,retain) NSMutableArray<WSCEventData *> *eventList;

@property(nonatomic,retain) NSMutableDictionary<NSString*,WSCEventTimeData*> *eventTimeDic;

@property(nonatomic) NSString *userIP;

@property(nonatomic) NSString *url;

@property(nonatomic) NSString *sessionType;

@property(nonatomic) NSString *protocolType;

@property(nonatomic, weak) id <WSCIceObserver> iceObsercer;

@property(nonatomic) BOOL isRecord;


/**
 * Returns an initialized WSCCall object for application triggered calls.
 *
 * @param package The corresponding WSCPackage
 * @param caller Caller of current call
 * @param callee Callee of current call
 */
-(instancetype)initWithPackage:(WSCPackage *)package
                        caller:(NSString *)caller
                        callee:(NSString *)callee;

/**
 * Returns an initialized WSCCall object for network triggered calls.
 *
 * @param package The corresponding WSCPackage
 * @param caller Caller of current call
 * @param callee Callee of current call
 * @param subSessionId The existing WSCSubSession subSessionId of this call
 */
-(instancetype)initWithPackage:(WSCPackage *)package
                        caller:(NSString *)caller
                        callee:(NSString *)callee
                  subSessionId:(NSString *)subSessionId;

/**
 * Gets the peer connection factory.
 */
+(RTCPeerConnectionFactory *)getPeerConnectionFactory;

/**
 * Starts the call represented by itself.
 *
 * @param config an instance of the CallConfig object that represents the capabilities of the call
 * @param headers extension headers. If provided, they are inserted into the JSON message. <br/>
 *        For example, an extHeaders formatted like this,
 *        <code>{'customerKey1':'value1','customerKey2':'value2'}</code><br/>
 *        will be in the message formatted like this : <br/>
 *        <code>{ "control" : {}, "header" : {...,'customerKey1':'value1','customerKey2':'value2'},
 *        "payload" : {}}</code>
 * @param localStreams Local media streams to be attached to call
 */
-(void)start:(WSCCallConfig *)config
     headers:(NSDictionary *)headers
     streams:(NSArray *)localStreams;

/**
 * Starts the call.
 *
 * @param config an instance of the CallConfig object that represents the capabilities of the call
 * @param localStreams Local media streams to be attached to call
 */
-(void)start:(WSCCallConfig *)config
     streams:(NSArray *)localStreams;

/**
 * Starts the call represented by itself.
 *
 * @param config an instance of the CallConfig object that represents the capabilities of the call
 * @param headers extension headers. If provided, they are inserted into the JSON message. <br/>
 *        For example, an extHeaders formatted like this,
 *        <code>{'customerKey1':'value1','customerKey2':'value2'}</code><br/>
 *        will be in the message formatted like this : <br/>
 *        <code>{ "control" : {}, "header" : {...,'customerKey1':'value1','customerKey2':'value2'},
 *        "payload" : {}}</code>
 */
-(void)start:(WSCCallConfig *)config
     headers:(NSDictionary *)headers;

/**
 * Accepts an incoming call.
 *
 * @param config Local capability configuration of the Call.
 * <p>
 *        This callConfig must not conflict with rules of RFC 3264. For example,
 *        if the callConfig of call.onIncomingCall or call.onUpdate is (SENDONLY, NONE),
 *        this callConfig cannot be (SENDONLY, NONE), but it can be (RECVONLY, NONE)
 *        or (NONE, NONE).
 * @param extHeaders Extension headers. If provided, they are inserted into the JSON message.
 * <p>
 *        For example, an extHeaders formatted like this,
 *        <code>{'customerKey1':'value1','customerKey2':'value2'}</code><br/>
 *        will be in the message formatted like this : <br/>
 *        <code>{ "control" : {}, "header" : {...,'customerKey1':'value1','customerKey2':'value2'},
 *        "payload" : {}}</code>
 * @param localStreams Local media streams to be attached to the call
 */
-(void)accept:(WSCCallConfig *)config
   extHeaders:(NSDictionary *)extHeaders
      streams:(RTCMediaStream *)localStreams;

/**
 * Accepts an incoming call.
 *
 * @param config Local capability configuration of the Call.
 * <p>
 *        This callConfig must not conflict with rules of RFC 3264. For example,
 *        if the callConfig of call.onIncomingCall or call.onUpdate is (SENDONLY, NONE),
 *        this callConfig cannot be (SENDONLY, NONE), but it can be (RECVONLY, NONE)
 *        or (NONE, NONE).
 * @param localStreams Local media streams to be attached to the call
 */
-(void)accept:(WSCCallConfig *)config
      streams:(RTCMediaStream *)localStreams;

/**
 * Accepts an incoming call.
 *
 * @param config Local capability configuration of the Call.
 * <p>
 *        This callConfig must not conflict with rules of RFC 3264. For example,
 *        if the callConfig of call.onIncomingCall or call.onUpdate is (SENDONLY, NONE),
 *        this callConfig cannot be (SENDONLY, NONE), but it can be (RECVONLY, NONE)
 *        or (NONE, NONE).
 * @param extHeaders Extension headers. If provided, they are inserted into the JSON message.
 * <p>
 *        For example, an extHeaders formatted like this,
 *        <code>{'customerKey1':'value1','customerKey2':'value2'}</code><br/>
 *        will be in the message formatted like this : <br/>
 *        <code>{ "control" : {}, "header" : {...,'customerKey1':'value1','customerKey2':'value2'},
 *        "payload" : {}}</code>
 */
-(void)accept:(WSCCallConfig *)config
   extHeaders:(NSDictionary *)extHeaders;

/**
 * Declines an incoming call.
 *
 * @param code The decline code reason. The default value is <strong>603</strong>, decline.<br/>
 * The available reason codes are:
 * <ul>
 * <li><strong>486</strong>: busy here</li>
 * <li><strong>603</strong>: decline</li>
 * <li><strong>600</strong>: busy everywhere</li>
 * </ul>
 *
 */
-(void)decline:(NSInteger)code;

/**
 * Declines an incoming call.
 *
 * @param code The decline code reason. The default value is <strong>603</strong>, decline.<br/>
 * The available reason codes are:
 * <ul>
 * <li><strong>486</strong>: busy here</li>
 * <li><strong>603</strong>: decline</li>
 * <li><strong>600</strong>: busy everywhere</li>
 * </ul>
 * @param headers Headers containing extra data about the decline call. If provided, they are inserted into the JSON message. <br/>
 *        For example an extHeaders formatted like this,
 *        <code>{'customerKey1':'value1','customerKey2':'value2'}</code><br/>
 *        will be in the message formatted like this : <br/>
 *        <code>{ "control" : {}, "header" : {...,'customerKey1':'value1','customerKey2':'value2'},
 *        "payload" : {}}</code>
 */
-(void)decline:(NSInteger)code
       headers:(NSDictionary *)headers;

/**
 * End the current call.
 */
-(void)end;

/**
 * Ends the current call.
 *
 * @param headers extension headers. If provided, they are inserted into the JSON message. <br/>
 *        For example, an extHeaders formatted like this,
 *        <code>{'customerKey1':'value1','customerKey2':'value2'}</code><br/>
 *        will be in the message formatted like this : <br/>
 *        <code>{ "control" : {}, "header" : {...,'customerKey1':'value1','customerKey2':'value2'},
 *        "payload" : {}}</code>
 */
-(void)end:(NSDictionary *)headers;

-(BOOL)mute:(BOOL)isMute;

-(BOOL)isMuted;

-(void)handleMessage: (WSCFrame *)frame;

-(void)handleRelayFailed;

-(void)handleEmptyIceList;

-(NSArray *)getMediasStreams;


/**
 * Pause or resume the local audio/video tracks.
 * @param audioPaused Pause or resume audio flag
 * @param videoPaused Pause or resume video flag
 * @return handling result
 */
-(BOOL)pauseAudioTrack:(BOOL)audioPaused videoTrack:(BOOL)videoPaused;

/**
 * Updates the audio/video capabilities of the call.
 *
 * @param config The WSCCallConfig representing the updated call configuration.
 * @param headers Extension headers. If provided, they are inserted into the JSON message. <br/>
 * For example, a headers parameter formatted like this,
 * <code>{'customerKey1':'value1','customerKey2':'value2'}</code><br/>
 * will be in the message formatted like this : <br/>
 * <code>{ "control" : {}, "header" : {...,'customerKey1':'value1','customerKey2':'value2'},
 * "payload" : {}}</code>
 * @param localStreams Local media streams to be attached to call
 */
-(void)update:(WSCCallConfig *)config
      headers:(NSDictionary *)headers
      streams:(NSArray *)localStreams;

/**
 * Updates the AV capabilities of the call.
 *
 * @param config The WSCCallConfig representing the updated call configuration
 * @param localStreams Local media streams to be attached to call
 */
-(void)update:(WSCCallConfig *)config
      streams:(NSArray *)localStreams;

-(void)update:(WSCCallConfig *)config
      headers:(NSDictionary *)headers;
/**
 * Gets the call configuration for the current call.
 */
-(WSCCallConfig *) getCallConfig;

/**
 * Gets the current state of a call.
 */
- (WSCCallState)getCallState;

/**
 * Gets the DataTransfer object by label.
 * @param label the label of the dataChannel of DataTransfer object
 */
- (WSCDataTransfer *)getDataTransferWithLabel:(NSString *)label;

/** Disallow init. */
-(instancetype) init __attribute__((unavailable("init not available")));

@end
