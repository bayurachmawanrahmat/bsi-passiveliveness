/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

#import "WSCHibernateParams.h"
#import "WSCStatusCode.h"

/**
 * Delegate for hibernation callbacks.
 */
@protocol WSCHibernationHandler <NSObject>

/**
 * On success response for Hibernate requests originated from Client.
 */
-(void)onSuccess;

/**
 * On failure response for Hibernate requests originated from Client.
 * @param code WSCStatusCode
 */
-(void)onFailure:(WSCStatusCode)code;

/**
 * Returns a WSCHibernateParams object on request for Hibernate from the Server.
 * @return WSCHibernateParams
 */
-(WSCHibernateParams *)onRequest;

/**
 * On completion of request for Hibernate originated from the Server.
 * @param code status code
 */
-(void)onRequestCompleted:(WSCStatusCode)code;

@end