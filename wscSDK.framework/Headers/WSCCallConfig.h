/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCMediaDirection.h"

/**
 * Call configuration interface.
 * Call Configuration describes the audio/video send and receive capabilities of a call.
 */
@interface WSCCallConfig : NSObject

/** The audio direction configuration. */
@property(readonly) WSCMediaDirection audioConfig;

/** The video direction configuration. */
@property(readonly) WSCMediaDirection videoConfig;

/** The data channel configurations. */
@property(readonly) NSArray *dataChannelConfigs;

/** The maximum video bandwidth for the call, in Kbps */
@property(readonly) NSInteger maxVideoBandwidthKbps;

/**
 * Constructs a WSCCallConfig with audio and video configurations set to WSCMediaDirectionNone.
 */
- (instancetype)init;

/**
 * Returns an initialized WSCCallConfig object with audio/video directions.
 *
 * @param audioMediaDirection The direction of the local audio media stream
 * @param videoMediaDirection The direction of the local video media stream
 * @param dataChannelConfigs  The configurations of data channel
 */
- (instancetype)initWithAudioDirection:(WSCMediaDirection)audioMediaDirection
                    withVideoDirection:(WSCMediaDirection)videoMediaDirection
                       withDataChannel:(NSArray *)dataChannelConfigs;

/** Update a WSCCallConfig object with the maximum bandwidth
 */
- (void)setMaxVideoBandwidthKbps:(NSInteger)maxVideoBandwidthKbps;

/**
 * Checks if audio send capability is supported.
 *
 * @return YES if audio capability is supported and NO otherwise
 */
- (BOOL)shouldSendAudio;

/**
 * Checks if video send capability is supported.
 *
 * @return YES if video capability is supported and NO otherwise
 */
- (BOOL)shouldSendVideo;

/**
 * Checks if audio receive capability is supported.
 *
 * @return YES if audio capability is supported and NO otherwise
 */
- (BOOL)shouldReceiveAudio;

/**
 * Checks if video receive capability is supported.
 *
 * @return YES if video capability is supported and NO otherwise
 */
- (BOOL)shouldReceiveVideo;

/**
 * Returns a string that represents the call config.
 */
- (NSString *)description;

@end
