/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCSessionProtocols.h"
#import "WSCSession.h"
#import "WSCServiceAuthHandler.h"
#import "WSCIceServerConfig.h"
#import "WSCHibernationHandler.h"

@class WSCPackage;
@class WSCSession;

/**
 * Builder for the WSCSession class.
 */
@interface WSCSessionBuilder : NSObject

/**
 * Creates a new WSCSessionBuilder.
 * @param webSocketURL web socket URL
 * @return WSCSessionBuilder
 */
+(WSCSessionBuilder *)create:(NSURL *)webSocketURL;

/**
 * Returns the same instance with a connection callback listener.
 * @param value Callback object
 * @return WSCSessionBuilder
 */
-(WSCSessionBuilder *)withConnectionDelegate:(id <WSCSessionConnectionDelegate>)value;

/**
 * Returns the same instance with a HTTP context.
 * @param value Http context
 * @return WSCSessionBuilder
 */
-(WSCSessionBuilder *)withHttpContext:(WSCHttpContext *)value;

/**
 * Returns the same instance with an observer.
 *
 * @param value Observer
 * @return Builder
 */
-(WSCSessionBuilder *)withObserverDelegate:(id <WSCSessionObserverDelegate>)value;

/**
 * Returns the same instance with a session id. Should only be set when attempting to rehydrate an existing session.
 *
 * @param value Session id value
 * @return Builder
 */
-(WSCSessionBuilder *)withSessionId:(NSString *)value;

/**
 * Returns the same instance with a username which will be sent with session connect message.
 *
 * @param value Username value
 * @return Builder
 */
-(WSCSessionBuilder *)withUserName:(NSString *)value;

/**
 * Returns the same instance with extension headers which will be sent as part of session connect.
 *
 * @param value Headers
 * @return Builder
 */
-(WSCSessionBuilder *)withExtHeaders:(NSDictionary *)value;

/**
 * Returns the same instance with a property that allows configuration of various internal behaviors.
 *
 * @param name Property name
 * @param value Property value
 * @return Builder
 */
-(WSCSessionBuilder *)withProperty:(NSString *)name value:(NSObject *)value;

/**
 * Returns the same instance with a package.
 *
 * @param value Package
 * @return Builder
 */
-(WSCSessionBuilder *)withPackage:(WSCPackage *)value;

/**
 * Returns the same instance with a service authentication handler.
 *
 * @param handler ServiceAuthHandler
 * @return Builder
 */
-(WSCSessionBuilder *)withServiceAuthHandler:(id<WSCServiceAuthHandler>) handler;

/**
 * Returns the same instance with an ICE server configuration.
 *
 * @param value ICE server config
 * @return Builder
 */
-(WSCSessionBuilder *)withIceServerConfig:(WSCIceServerConfig *)value;

/**
 * Returns the same instance with hibernation handler configuration.
 *
 * @param handler WSCHibernationHandler
 * @return Builder
 */
-(WSCSessionBuilder *)withHibernationHandler:(id<WSCHibernationHandler>)handler;

/**
 * Returns the same instance with a device token configuration.
 *
 * @param token NSData
 * @return Builder
 */
-(WSCSessionBuilder *)withDeviceToken:(NSData *)token;

/**
 * Returns the same instance with a state information configuration.
 *
 * @param stateInfo NSString
 * @return Builder
 */
-(WSCSessionBuilder *)withStateInfo:(NSString *)stateInfo;

/**
 * Builds the session out of this builder.
 *
 * @return session
 */
-(WSCSession *)build;

@end
