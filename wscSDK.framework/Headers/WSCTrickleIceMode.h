/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/** Represents the various Trickle ICE modes. */
typedef NS_ENUM(NSInteger, WSCTrickleIceMode) {
  /** Trickle ICE mode: off. */
  WSCTrickleIceModeOff,
  /** Trickle ICE mode: half. */
  WSCTrickleIceModeHalf,
  /** Trickle ICE mode: full. */
  WSCTrickleIceModeFull
};
