//
//  WSCEventData.h
//  wscSDK
//
//  Created by zxz on 17/2/6.
//  Copyright © 2017 Oracle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSCEventType.h"


@interface WSCEventData : NSObject

@property(nonatomic, readonly) WSCEventType eventType;

@property(nonatomic, readonly, copy) NSDate *date;

@property(nonatomic, readonly, copy) NSArray *params;


-(instancetype)initWithEventType:(WSCEventType)eventType
                            date:(NSDate *)date
                          params:(NSArray *)params;

-(NSString *)toString;


@end
