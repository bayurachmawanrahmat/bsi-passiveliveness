/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCPackage.h"

/**
 * WSCRegisterPackage extension of WSCPackage. Used for registration handling.
 */
@interface WSCRegisterPackage : WSCPackage

/**
 * Default package type.
 */
extern NSString * const REG_PACKAGE_TYPE;

/**
 * Creates a RegisterPackage.
 */
- (instancetype)init;

@end