/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCHttpContextBuilder.h"

@class WSCHttpContextBuilder;

/**
 * Used to pass the login information between the Http client and the Websocket client API.
 * Also acts as a way for the client to pass any additional headers or client certificates to the WebSocket handshake.
 */
@interface WSCHttpContext : NSObject

/**
 * Returns an initialized WSCHttpContext object.
 *
 * @param builder HttpContextBuilder
 */
-(instancetype)initWithHttpContextBuilder:(WSCHttpContextBuilder *)builder;

/**
 * Gets the SSLContextRef.
 *
 * @return SSL context reference
 */
-(SSLContextRef *)getSSLContextRef;

/**
 * Gets all possible values of the header indicated by headerName.
 *
 * @param headerName header name
 * @return String formatted values
 */
-(NSArray *)getHeader:(NSString *)headerName;

/**
 * Returns a map of the header fields and values.
 *
 * @return the headers map Dictionary of NSString -> NSArray<NSString>
 */
-(NSDictionary *)getHeaders;

@end

