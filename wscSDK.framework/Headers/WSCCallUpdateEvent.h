/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/** Represents the various states of a WSCCall update event. */
typedef NS_ENUM(NSInteger, WSCCallUpdateEvent) {
  /** Call update received from remote peer. */
  WSCCallUpdateEventReceived,
  /** Call update sent to remote peer. */
  WSCCallUpdateEventSent,
  /** Call update was accepted. */
  WSCCallUpdateEventAccepted,
  /** Call update was rejected. */
  WSCCallUpdateEventRejected
};