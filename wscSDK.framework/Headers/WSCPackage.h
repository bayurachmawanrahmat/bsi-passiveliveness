/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCSession.h"
#import "WSCSubSession.h"

@class WSCSession;
@class WSCSubSession;

/**
 * Base interface for all WSCPackage extensions.
 */
@interface WSCPackage : NSObject

/**
 * Package type.
 */
@property(nonatomic, readonly, copy) NSString *packageType;

/**
 * Returns an initialized WSCPackage object with package type.
 *
 * @param packageType The package type
 */
- (instancetype)initWithPackageType:(NSString *)packageType;

/**
 * Returns the WebRTC Session Controller session associated with this package.
 * The associated session or null if session is not yet created
 */
- (WSCSession *) session;

/**
 * Put the subsession.
 * @param subSession the sub-session to add to the session.
 */
- (void)putSubSession:(WSCSubSession *) subSession;

/**
 * Set the new session.
 * @param session the session the set.
 */
- (void)setSession:(WSCSession *) session;

@end
