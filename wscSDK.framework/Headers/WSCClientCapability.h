/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

/**
 * An object representation of a client capability.
 */
@interface WSCClientCapability : NSObject

/**
 * Returns an initialized WSCClientCapability object with initialized instance variables.
 * @param family NSString
 * @param version NSString
 * @param appPackage NSString
 */
- (instancetype)init : (NSString*) family
            version  : (NSString*) version
          appPackage : (NSString*) appPackage;

/**
 * Returns a NSDictionary object containing the object's instance variables.
 * @
 */
- (NSDictionary *)toJson;

/**
 * Returns a NSString object containing the object's instance variables.
 */
- (NSString *)jsonString;

/**
 * Returns a JSON string representation of the WSCClientCapability object.
 * @return NSString
 */
- (NSString *)description;

@end
