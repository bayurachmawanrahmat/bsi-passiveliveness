/* Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved. */

#import "WSCFrame.h"
@class WSCFrame;

/**
 * Message section (as a data transfer object) for sending data over the wire as a JSON string.
 */
@interface WSCHeaders : NSObject

/**
 * Initializer.
 *
 * @param parent Parent
 */
-(instancetype)init:(WSCFrame *)parent;

/**
 * Gets action (web verb).
 *
 * @return Action
 */
-(NSString *)getAction;

/***
 * Sets action (web verb).
 *
 * @param action Action
 */
-(void)setAction:(NSString *)action;

/**
 * Gets initiator.
 *
 * @return Initiator
 */
-(NSString *)getInitiator;

/**
 * Sets initiator.
 *
 * @param initiator initiator
 */
-(void)setInitiator:(NSString *)initiator;

/**
 * Gets target.
 *
 * @return Target
 */
-(NSString *)getTarget;

/**
 * Sets target.
 *
 * @param target Target
 */
-(void)setTarget:(NSString *)target;

/**
 * Gets the extension headers.
 *
 * @return Ext headers
 */
-(NSDictionary *)getExtensionHeaders;
/**
 * Sets the extension headers.
 *
 * @param extHeaders extension headers
 */
-(void)setExtensionHeaders:(NSDictionary *)extHeaders;

/**
 * Adds extension headers.
 *
 * @param key extension header key
 * @param value extension header value
 */
-(void)addExtensionHeaders:(NSString *) key value:(NSArray *) value;

/**
; * Gets connect CSLR
 *
 * @return CSLR
 */
-(NSInteger)getConnectCslr;

/**
 * Sets connect CSLR
 *
 *@param connectSlr CSLR
 */
-(void)setConnectCslr:(NSInteger)connectSlr;

/**
 * Gets connect CSLW
 *
 * @return CSLW
 */
-(NSInteger)getConnectCslw;

/**
 * Sets connect CSLW
 *
 *@param connectSlw CSLW
 */

-(void)setConnectCslw:(NSInteger)connectSlw;

/**
 * Gets connect CSUW
 *
 * @return CSUW
 */
-(NSInteger)getConnectCsuw;

/**
 * Sets connect CSUW
 *
 * @param connectSuw CSUW
 */
-(void)setConnectCsuw:(NSInteger)connectSuw;

/**
 * Gets connect SSLR
 *
 * @return the connect SSLR
 */
-(NSInteger)getConnectSslr;

/**
 * Sets connect SSLR
 *
 * @param connectSslr SSLR
 */
-(void)setConnectSslr:(NSInteger)connectSslr;

/**
 * Gets error code
 *
 * @return Error code
 */
-(NSInteger)getErrorCode;

/**
 * Sets Error code
 *
 * @param code Error code
 */
-(void)setErrorCode:(NSInteger)code;

/**
 * Gets reason
 *
 * @return Reason
 */
-(NSString *)getReason;

/**
 * Sets reason
 *
 * @param reason Reason
 */
-(void)setReason:(NSString *)reason;

/**
 * Gets response code
 *
 * @return Response code
 */
-(NSInteger)getResponseCode;

/**
 * Sets response code
 *
 * @param code Response code
 */
-(void)setResponseCode:(NSInteger)code;

/**
 * Gets reliable flag
 *
 * @return Reliability flag
 */
-(BOOL)isReliable;

/**
 * Gets authorization
 * @return Authorization information
 */
-(NSDictionary *)getAuthorization;

/**
 * Sets authorization
 *
 * @param authorization Authorization
 */
-(void)setAuthorization:(NSDictionary *)authorization;

/**
 * Gets authentication
 *
 * @return the authentication
 */
-(NSDictionary *)getAuthenticate;

/**
 * Sets authentication
 *
 * @param authenticate Authentication
 */
-(void)setAuthenticate:(NSDictionary *)authenticate;

/**
 * Gets hibernation TTL
 *
 * @return Hibernation TTL
 */
-(NSInteger)getHibernateTTL;

/**
 * Sets hibernation ttl
 *
 * @param ttl Hibernation TTL
 */
-(void)setHibernateTTL:(NSInteger)ttl;

@end
