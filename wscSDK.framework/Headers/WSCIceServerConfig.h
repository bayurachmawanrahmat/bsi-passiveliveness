/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import <Foundation/Foundation.h>
#import "WSCIceServer.h"

/**
 * Ice server config - Initializes and maintains a list of WSCIceServers.
 */
@interface WSCIceServerConfig : NSObject

/**
 * Returns an initialized WSCIceServerConfig object with ICE servers.
 *
 * @param firstObject First ICE server
 * @param ... The rest of the set of ICE servers
 */
- (instancetype)initWithIceServers:(WSCIceServer *)firstObject, ... NS_REQUIRES_NIL_TERMINATION;

/**
 * Gets the list of ice servers.
 *
 * @return the ice servers
 */
- (NSArray *)iceServers;

@end
