//
//  WSCEventTimeTypeHandler.h
//  wscSDK
//
//  Created by zxz on 17/2/10.
//  Copyright © 2017 Oracle. All rights reserved.
//

#import "WSCEventTimeType.h"

@interface WSCEventTimeTypeHandler :NSObject

+(NSString *)getName:(WSCEventTimeType)eventTimeType;

@end
