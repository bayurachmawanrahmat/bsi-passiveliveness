/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/**
 * Direction for the Media stream.
 */
typedef NS_ENUM(NSInteger, WSCMediaDirection) {
  /**
   * No Direction.
   */
  WSCMediaDirectionNone = 1,
  /**
   * Both send and receive.
   */
  WSCMediaDirectionSendRecv = 2,
  /**
   * Send only.
   */
  WSCMediaDirectionSendOnly = 3,
  /**
   * Receive only.
   */
  WSCMediaDirectionRecvOnly = 4,
};
