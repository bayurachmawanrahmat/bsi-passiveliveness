/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCFrame.h"
#import "WSCClientCapability.h"


@class WSCFrame;
/**
 * Payload.
 */
@interface WSCPayload : NSObject

/**
 * Constructor.
 *
 * @param parent Parent
 */
-(instancetype)init:(WSCFrame *)parent;

/**
 * Gets the SDP.
 *
 * @return the SDP
 */
-(NSString *)getSdp;

/**
 * Sets the SDP
 *
 * @param sdp SDP
 */
-(void)setSdp:(NSString *)sdp;

/**
 * Gets the candidates.
 *
 * @return Candidates
 */
-(NSString *)getCandidates;

/**
 * Sets the candidates.
 *
 * @param candidates Candidates
 */
-(void)setCandidates:(NSString *)candidates;

/**
 * Gets the device token.
 *
 * @return Device token
 */
-(NSString *)getDeviceToken;

/**
 * Sets the device token.
 *
 * @param token Device token
 */
-(void)setDeviceToken:(NSString *)token;

/**
 * Sets the client capabilities.
 *
 * @param capability Client capabilities
 */
-(void)setCapability:(WSCClientCapability *)capability;

/**
 * Gets the payload content.
 *
 * @return Payload content
 */
-(NSString *)getContent;

/**
 * Sets the payload content.
 *
 * @param content Payload content
 */
-(void)setContent:(NSString *)content;

/**
 * Gets the list of ICE servers.
 *
 * @return List of ICE servers
 */
-(NSArray *)getIceServers;


-(NSDictionary*)getAuditData;

-(void)setAuditData: (NSDictionary *) auditData;

@end
