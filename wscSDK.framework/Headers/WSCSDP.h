/* Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved. */

#import <Foundation/Foundation.h>
#import "WSCCallConfig.h"

/**
 * Container for parsed SDP attributes.
 */
@interface WSCSDP : NSObject

extern NSString * const SDP_MEDIA_FIELD;

extern NSString * const SDP_ATTRIBUTE_FIELD;

extern NSString * const SDP_ORIGIN_FIELD;

extern NSString * const SDP_BANDWIDTH_FIELD;

extern NSString * const SDP_MEDIA_LINE_PREFIX;

extern NSString * const SDP_ATTRIBUTE_PREFIX;

extern NSString * const SDP_MEDIA_TYPE_AUDIO;

extern NSString * const SDP_MEDIA_TYPE_VIDEO;

extern NSString * const SDP_AUDIO_LINE_HEADER;

extern NSString * const SDP_VIDEO_LINE_HEADER;

extern NSString * const SDP_AS_BANDWIDTH_PREFIX;

extern NSString * const SDP_CONNECTION_FIELD;

extern NSString * const SDP_CONNECTION_LINE_PREFIX;

-(instancetype)initWithSDP:(NSString *)sdp;

/**
 * Returns a list of {@link MediaDescription} elements from the SDP.
 *
 * @return list of media descriptions
 */
-(NSArray *)getMediaDescriptions;

/**
 * Gets the CallConfig according to the SDP.
 *
 * @return call configuration created from SDP
 */
-(WSCCallConfig *)getCallConfig;

-(WSCCallConfig *)getReversedCallConfig;

/**
 * Parses session ID from SDP string.
 * @param sdp SDP
 * @return Session id or nil if not found
 */
+(NSString *)parseSessionId:(NSString *)sdp;

@end
