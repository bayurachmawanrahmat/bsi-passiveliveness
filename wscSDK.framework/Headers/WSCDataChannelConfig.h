/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

/**
 * Provides options for configuring the data channel.
 */
@interface WSCDataChannelOption : NSObject

/**
 * Indicates if the data channel should guarantee order or not.
 */
@property(nonatomic) BOOL ordered;

/**
 * Indicates the maximum time to try and retransmit a failed message in milliseconds.
 */
@property(nonatomic) NSInteger maxRetransmitTimeMs;

/**
 * Indicates the maximum number of times to try and retransmit a failed message.
 */
@property(nonatomic) NSInteger maxRetransmits;

/**
 * Indicates the protocol to be used for the data channel.
 */
@property(nonatomic) NSString *protocol;

/**
 * Indicates if the underlying channel has been externally negotiated.
 * If unsupported, the data-channel will fail.
 */
@property(nonatomic) BOOL negotiated;

/**
 * The stream ID for the channel.
 */
@property(nonatomic) NSInteger id;

/**
 * Returns a JSON string representation of the WSCDataChannelOption object.
 *
 * @return NSString
 */
- (NSDictionary *)toJson;
@end


/**
 * Configuration for an underlying DataChannel.
 */
@interface WSCDataChannelConfig : NSObject

/**
 * Label used to identify underlying data channel.
 */
@property (nonatomic,readonly) NSString *label;

/**
 * Configuration items for the underlying data channel.
 */
@property (nonatomic,readonly) WSCDataChannelOption *option;

/**
 * Initiates a WSCDataChannelConfig object.
 *
 * @param label the label is used to identify this data channel
 * @param option the option is to describe the data channel configuration
 */
-(instancetype)initWithLabel:(NSString *)label
                  withOption:(WSCDataChannelOption *)option;

/**
 * Returns a JSON string representation of the WSCDataChannelConfig object.
 *
 * @return NSString
 */
- (NSDictionary *)toJson;

/**
 * Disallow init.
 */
-(instancetype) init __attribute__((unavailable("init not available")));

@end
