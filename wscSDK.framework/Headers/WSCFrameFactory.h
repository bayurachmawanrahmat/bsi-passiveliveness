/* Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved. */

#import "WSCFrame.h"

/**
 * Helper class to create JSON WSCFrame instances.
 */
@interface WSCFrameFactory : NSObject

/**
 * Creates a JSON frame with arguments and a correlation id.
 *
 * @param messageType Frame type
 * @param verb Frame verb
 * @param pack Package
 * @param sessionId Session Id
 * @param subSessionId SubSession Id
 * @param correlationId Correlation Id
 * @return the newly created frame
 */
+ (WSCFrame *)createFrame:(MessageType)messageType
                 verb:(NSString *)verb
                 pack:(NSString *)pack
            sessionId:(NSString *)sessionId
         subSessionId:(NSString *)subSessionId
        correlationId:(NSString *)correlationId;

/**
 * Creates a JSON frame of type {@link Type#ERROR} from the original frame.
 *
 * @param origFrame the original Frame
 * @param code error code
 * @param reason failure reason
 * @return JSON error frame
 */
+ (WSCFrame *)createError:(WSCFrame *)origFrame
                 code:(int) code
               reason:(NSString *)reason;

/**
 * Creates a JSON frame of type {@link Type#ACKNOWLEDGEMENT}.
 *
 * @param frame the original frame
 * @return the ACK frame
 */
+ (WSCFrame *)createAck:(WSCFrame *) frame;

/**
 * Creates a JSON frame based on an existing frame
 * @param origFrame Original frame
 * @param type Messaeg type
 *
 */
+ (WSCFrame *)createFrame:(WSCFrame *)origFrame type:(MessageType)type;

@end

