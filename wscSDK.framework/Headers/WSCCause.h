/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCStatusCode.h"

/**
 * Identifies the WSCStatusCode and extension headers related to an event.
 */
@interface WSCCause : NSObject

/** The status code if available. */
@property WSCStatusCode statusCode;

/** The extension headers if any. */
@property NSMutableDictionary *extHeaders;

/**
 * Initializes a newly allocated WSCCause.
 *
 * @param statusCode statusCode
 */
-(instancetype)init:(WSCStatusCode)statusCode;

/**
 * Initializes a newly allocated WSCCause.
 *
 * @param statusCode statusCode
 * @param extHeaders the extenstion headers
 */
-(instancetype)init:(WSCStatusCode)statusCode extHeaders:(NSDictionary*)extHeaders;

/**
 Returns a string that represents the WSCCause.
 */
-(NSString *)description;

@end
