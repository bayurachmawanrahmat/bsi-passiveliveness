//
//  WSCErrorData.h
//  wscSDK
//
//  Created by zxz on 17/2/6.
//  Copyright © 2017 Oracle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSCErrorType.h"


@interface WSCErrorData : NSObject 

@property(nonatomic, readonly) WSCErrorType errorType;

@property(nonatomic, readonly, copy) NSDate *date;

@property(nonatomic, readonly, copy) NSArray *params;


-(instancetype)initWithErrorType:(WSCErrorType)errorType
                        date:(NSDate *)date
                        params:(NSArray *)params;

-(NSString *)toString;


@end
