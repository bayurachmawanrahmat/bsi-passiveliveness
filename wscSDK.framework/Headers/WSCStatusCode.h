/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/** Represents the various WebRTC Session Controller status codes. */
typedef NS_ENUM(NSInteger, WSCStatusCode) {
  /** Destination user agent received INVITE, and is alerting user of call. */
  WSCStatusCodeRinging = 180,
  /** This response may be used to send extra information for a call which is still being set up. */
  WSCStatusCodeSessionProgress = 183,
  /** Indicates the request was successful. */
  WSCStatusCodeOk = 200,
  /** The request could not be understood due to malformed syntax. */
  WSCStatusCodeBadRequest = 400,
  /** The request requires user authentication. This response is issued by servers and registrars. */
  WSCStatusCodeUnauthorized = 401,
  /** The server understood the request, but is refusing to fulfil it. */
  WSCStatusCodeForbidden = 403,
  /** The server has definitive information that the user does not exist at the domain specified in the Request-URI.
  */
  WSCStatusCodeResourceUnavailable = 404,
  /**
   * This code is similar to 401 (UNAUTHORIZED), but indicates that the client MUST first authenticate itself with the
   * proxy.
   */
  WSCStatusCodeProxyAuthRequired = 407,
  /** Couldn't find the user in time. The server could not produce a response within a suitable amount of time. */
  WSCStatusCodeRequestTimeout = 408,
  /** The callee's end system was contacted successfully but the callee is currently unavailable. */
  WSCStatusCodeTemporarilyUnavailable = 480,
  /**
   * The callee's end system was contacted successfully, but the callee is currently not willing or able to take
   * additional calls at this end system.
   */
  WSCStatusCodeBusyHere = 486,
  /** Request has terminated by bye or cancel. */
  WSCStatusCodeRequestTerminated = 487,
  /** Server has some pending request from the same dialog. */
  WSCStatusCodeRequestPending = 491,
  /** The server could not fulfill the request due to some unexpected condition. */
  WSCStatusCodeServerError = 500,
  /** The error code for WebSocket. */
  WSCStatusCodeWebsocketError = 599,
  /**
   * The callee's end system was contacted successfully but the callee is busy and does not wish to take the call at
   * this time.
   */
  WSCStatusCodeBusyEverywhere = 600,
  /** The callee's machine was successfully contacted but the user explicitly does not wish to or cannot participate. */
  WSCStatusCodeDeclined = 603,
  /** Unknown status. */
  WSCStatusCodeUnknown = 699
};
