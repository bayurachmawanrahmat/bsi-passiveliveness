//* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCFrame.h"
#import "Foundation/Foundation.h"
@class WSCFrame;

/**
 * Control section (as a Data Transfer Object) for sending data over the wire as a JSON string.
 */
@interface WSCControl : NSObject

/** Type of Message. */
typedef NS_ENUM(NSInteger, MessageType) {
  WSC_MT_REQUEST,
  WSC_MT_RESPONSE,
  WSC_MT_MESSAGE,
  WSC_MT_ACKNOWLEDGEMENT,
  WSC_MT_ERROR
};

/** Type of RESPONSE message. */
typedef NS_ENUM(NSInteger, MessageState) {
  WSC_MS_SUBSEQUENT,
  WSC_MS_FINAL
};

/** Control parameter. */
extern NSString * const KEY_CONTROL;

/**
 * Constructor.
 *
 * @param parent Parent
 */
-(instancetype)init:(WSCFrame *) parent;

/**
 * Gets the message type.
 * @return Message type
 */
-(MessageType)getType;

/**
 * Sets the message type.
 * @param type Message type
 */
-(void)setType:(MessageType) type;

/**
 * Gets the message state.
 * @return Message state
 */
-(MessageState)getMessageState;

/**
 * Set messate state
 * @param messageState Message state.
 */
-(void)setMessageState:(MessageState) messageState;

/**
 * Gets the version.
 * @return Version
 */
-(NSString *)getVersion;

/**
 * Sets the version.
 * @param version Version
 */
-(void)setVersion:(NSString *) version;

/**
 * Gets the sequence.
 * @return Sequence
 */
-(NSInteger)getSequence;

/**
 * Sets the sequence.
 * @param sequence sequence
 */
-(void)setSequence:(NSInteger) sequence;

/**
 * Gets the session id.
 * @return Session id
 */
-(NSString *)getSessionId;

/**
 * Sets the session id.
 * @param sessionId session id
 */
-(void)setSessionId:(NSString *) sessionId;

/**
 * Gets the subsession id.
 * @return Subsession id
 */
-(NSString *)getSubSessionId;

/**
 * Sets the subsession id.
 * @param subSessionId subSession id
 */
-(void)setSubSessionId:(NSString *) subSessionId;

/**
 * Gets the correlation id.
 * @return Correlation id
 */
-(NSString *)getCorrelationId;

/**
 * Sets the correlation id.
 * @param correlationId correlation id
 */
-(void)setCorrelationId:(NSString *) correlationId;

/**
 * Gets the ack sequence.
 * @return Ack sequence
 */
-(NSInteger)getAckSequence;

/**
 * Sets the ack sequence.
 * @param ackSequence Ack sequence
 */
-(void)setAckSequence:(NSInteger) ackSequence;

/**
 * Gets package type.
 * @return Package type
 */
-(NSString *)getPackageType;

/**
 * Sets package type.
 * @param packageType package type
 */
-(void)setPackageType:(NSString *) packageType;

/**
 * Gets the WebRTC Session Controller id.
 * @return WebRTC Session Controller id
 */
-(NSString *)getWscId;

/**
 * Set WebRTC Session Controller id.
 * @param wscId WebRTC Session Controller id
 */
-(void)setWscId:(NSString *)wscId;

@end
