/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

/**
 * Identifies the Session Hibernate Parameters.
 */
@interface WSCHibernateParams : NSObject

/**
 * Time in seconds for how long a hibernated session can live on the Server.
 */
@property(readonly) NSInteger timeToLive;

/**
 * Gets an WSCHibernateParams object containing an initialized timeToLive property.
 * @param timeToLive time in seconds
 */
-(instancetype)initWithTTL:(NSInteger) timeToLive;

/**
 * Returns a JSON dictionary.
 * @return NSDictionary
 */
-(NSDictionary *)toJson;

/**
 * Returns a string representation
 * @return NSString
 */
-(NSString *)description;

@end
