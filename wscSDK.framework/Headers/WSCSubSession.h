/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCPackage.h"
#import "WSCFrame.h"
@class WSCPackage;

/**
 * Base interface for all subsessions.
 */
@interface WSCSubSession : NSObject 

/**
 * Subsession identifier.
 */
@property (readonly) NSString *subSessionId;

/**
 * Subsession package.
 */
@property (weak, readonly) WSCPackage *package;

/**
 * Returns an initialized WSCSubSession object with package.
 *
 * @param package the package
 */
-(instancetype)initWithPackage:(WSCPackage *)package;

/**
 * Returns an initialized WSCSubSession object with package and subsession id.
 *
 * @param package the package
 * @param subSessionId the subSessionId
 */
-(instancetype)initWithPackage:(WSCPackage *)package
                        withId:(NSString *)subSessionId;
/**
 * Disposes of any resources allocated by the subsession.
 */
- (void)dispose;

/**
 * Handles inbound messages on an existing subsession.
 @param frame Frame
 */
- (void)onMessage:(WSCFrame *)frame;

/**
 * Sends a message.
 @param frame Frame
 */
- (void)sendMessage:(WSCFrame *)frame;

/**
 * Disposes of the subssession.
 */
- (void)doDispose;

- (long)getTod;

/**
 * Checks if the session is disposed.
 */
- (BOOL)isDisposed;

/**
 * Returns a JSON dictionary description of the WSCSubSession object.
 *
 * @return NSDictionary
 */
//- (NSDictionary *) toJson;

/**
 * Handles relay failure on an existing subsession.
 */
- (void)handleRelayFailed;

/**
 * Handles relay failure on an existing subsession.
 */
- (void)handleEmptyIceList;
@end
