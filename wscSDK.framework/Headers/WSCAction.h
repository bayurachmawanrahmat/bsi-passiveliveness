/* Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved. */

/**
 * Default actions supported.
 */
typedef NS_ENUM(NSInteger, WSCAction) {
  /**
   * Establishes a protocol level session with the server. Equivalent to a MBWS Connect request.
   */
  WSCActionConnect,

  /**
   * Message that starts a session with a particular package.
   */
  WSCActionStart,

  /**
   * Shuts down a session started by a particular request message.
   */
  WSCActionShutdown,

  /**
   * Indicates that the media session has been established.
   */
  WSCActionComplete,

  /**
   * Equivalent to Notification of the Notification Server.
   */
  WSCActionNotify,

  /**
   * Pre-acknowledge provisional responses.
   */
  WSCActionPrack,

  /**
   * Indicates that the session has Hibernated
   */
  WSCActionHibernate,

  /**
   * Ice enquiry to query for ice server information.
   */
  WSCActionIceEnquiry,

  /**
   * Enquiry conversation config.
   */
  WSCActionEnquiry,

  /**
   * Relay conversation config.
   */
  WSCActionRelay,

  /**
   * Reliable relay conversation config.
   */
  WSCActionReliableRelay,

  /**
   * Relay message received on TURN server failure.
   */
  WSCActionRelayFailed,

  /**
   * Modify conversation.
   */
  WSCActionModify,

  /**
   * For sending SIP chat messages.
   */
  WSCActionSend,

  /**
   * Trickle ICE to exchange ICE candidates.
   */
  WSCTRICKLE,
  
  WSCActionAudit
};

