//
//  WSCEventTimeData.h
//  wscSDK
//
//  Created by zxz on 17/2/7.
//  Copyright © 2017 Oracle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSCEventTimeType.h"

@interface WSCEventTimeData : NSObject

@property(nonatomic, readonly) WSCEventTimeType eventTimeType;

@property(nonatomic, copy) NSDate *startTime;

@property(nonatomic, copy) NSDate *endTime;

-(instancetype)initWithEventTimeType:(WSCEventTimeType)eventTimeType;

-(long)getConsumed;


@end
