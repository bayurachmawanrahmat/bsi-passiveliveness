/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCStatusCode.h"
#import "WSCSessionState.h"

/**
 * WebRTC Session Controller Session delegate for connection callbacks.
 */
@protocol WSCSessionConnectionDelegate <NSObject>

/**
 * Connection success.
 */
-(void)onSuccess;

/**
 * Connection error.
 *
 * @param code Connection error
 */
-(void)onFailure:(WSCStatusCode)code;

@end

/**
 * WebRTC Session Controller session observer delegate.
 */
@protocol WSCSessionObserverDelegate <NSObject>

/**
 * Called when the WebRTC Session Controller Session changes state.
 *
 * @param sessionState SessionState
 */
-(void)stateChanged:(WSCSessionState) sessionState;

/**
 * Called when the ice servers has been set in WSCSession.
 *
 */
-(void)onIceServersReceived;

@end
