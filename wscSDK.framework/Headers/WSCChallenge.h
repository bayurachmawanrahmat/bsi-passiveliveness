/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

/**
 * WSCChallenge protocol. Used for setting the authentication username and password by an application.
 */
@protocol WSCChallenge <NSObject>

/** Authentication realm. For example, "example.com". */
@property (readonly) NSString *realm;

/**
 * Sets the username and password for authentication challenges.
 *
 * @param username Username
 * @param password Password
 */
-(void)setUsername:(NSString *) username withPassword:(NSString *) password;

@end
