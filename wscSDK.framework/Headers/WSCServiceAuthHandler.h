/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCChallenge.h"

/**
 * WSCServiceAuthHandler protocol to be implemented by applications for SIP challenge authentications.
 */
@protocol WSCServiceAuthHandler <NSObject>

/**
 * Handles setting the username and password of the challenge.
 *
 * @param challenge Authentication challenge
 */
-(void)handle:(id<WSCChallenge>) challenge;

@end


