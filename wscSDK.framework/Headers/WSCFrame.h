/* Copyright (c) 2014, Oracle and/or its affiliates. All rights reserved. */

#import "WSCHeaders.h"
#import "WSCControl.h"
#import "WSCPayload.h"

@class WSCHeaders;
@class WSCControl;
@class WSCPayload;

/**
 * Master Data Transfer Object class for all JSON messages.
 */
@interface WSCFrame : NSObject

/**
 * Object representation for header block of frame.
 */
@property (readonly) WSCHeaders *headers;

/**
 * Object representation for control block of frame.
 */
@property (readonly) WSCControl *control;

/**
 * Object representation for payload block of frame.
 */
@property (readonly) WSCPayload *payload;

/**
 * Constructor with empty JSON data.
 */
-(instancetype)init;

/**
 * Constructor with a given JSON string.
 *
 * @param json JSON data
 */
-(instancetype)initWithJson:(NSString *)json;

/**
 * Converts to a JSON string.
 * @return JSON string
 */
-(NSString *)description;

/**
 * Converts to a dictionary.
 * @return Dictionary representation of frame.
 */
-(NSDictionary *)getJsonDictionary;

/**
 * Gets a JSON element.
 *
 * @param key Key (such as "control", "header", "payload")
 * @param param Parameter name (for example, "initiator")
 * @return the value or null
 */
-(id)get:(NSString *)key param:(NSString *)param;

/**
 * Gets a JSON string.
 * @param key Key (such as "control", "header", "payload")
 * @param param Parameter name (for example, "initiator")
 * @return String value or null
 */
-(NSString *)getString:(NSString *)key param:(NSString *)param;

/**
 * Gets a JSON int.
 * @param key Key (such as "control", "header", "payload")
 * @param param Parameter name (for example, "initiator")
 * @return String value or null
 */
-(NSInteger)getInt:(NSString *)key param:(NSString *)param;

/**
 * Sets a JSON element.
 * @param key Key (such as "control", "header", "payload")
 * @param param Parameter name (for example, "initiator")
 * @param value Value to set
 */
-(void)set:(NSString *)key param:(NSString *)param value:(NSObject *)value;

/**
 * Adds an empty JSON element.
 * @param key Key (such as "control", "header", "payload")
 */
-(void)addKey:(NSString *)key;

/**
 * Sets a string parameter.
 * @param key Key (such as "control", "header", "payload")
 * @param param Parameter name (for example, "initiator")
 * @param value Value to set
 */
-(void)setString:(NSString *)key param:(NSString *)param value:(NSString *)value;

/**
 * Sets a integer parameter.
 * @param key Key (such as "control", "header", "payload")
 * @param param Parameter name (for example, "initiator")
 * @param value Value to set
 */
-(void)setInt:(NSString *)key param:(NSString *)param value:(NSInteger)value;

@end
