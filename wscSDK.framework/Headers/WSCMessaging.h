/* Copyright (c) 2015, Oracle and/or its affiliates. All rights reserved. */

#import "WSCSubSession.h"

@class WSCMessagingMessage;

/**
 * Represents the Messaging subsession.
 */
@interface WSCMessaging : WSCSubSession

/** The message for this subsession. */
@property(nonatomic) WSCMessagingMessage *message;

/**
 * Returns an initialized WSCMessaging object for application triggered messages.
 *
 * @param package The corresponding WSCPackage
 */
-(instancetype)initWithPackage:(WSCPackage *)package;

/**
 * Returns an initialized WSCMessaging object for network triggered messages.
 *
 * @param package The corresponding WSCPackage
 * @param subSessionId The existing WSCSubSession subSessionId of this call
 */
-(instancetype)initWithPackage:(WSCPackage *)package
                  subSessionId:(NSString *)subSessionId;

/** Disallows init. */
-(instancetype) init __attribute__((unavailable("init not available")));

@end
