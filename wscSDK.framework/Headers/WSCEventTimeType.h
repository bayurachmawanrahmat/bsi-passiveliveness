//
//  WSCEventTimeType.h
//  wscSDK
//
//  Created by zxz on 17/2/7.
//  Copyright © 2017 Oracle. All rights reserved.
//

#ifndef WSCEventTimeType_h
#define WSCEventTimeType_h


#endif /* WSCEventTimeType_h */

typedef NS_ENUM (NSInteger, WSCEventTimeType) {
  
  timeToNewPeerConnection,
  
  timeToMakeCall,
  
  timeToSetupSignalingChannel,
  
  timeToGetIceConnection,
  
  timeToGetIceCandidates,
  
  timeToGetRemoteMedia
};
