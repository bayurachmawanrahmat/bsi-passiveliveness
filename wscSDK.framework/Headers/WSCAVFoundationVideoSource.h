//
//  WSCAVFoundationVideoSource.h
//  wscSDK
//
//  Created by changtai on 8/6/18.
//  Copyright © 2018 Oracle. All rights reserved.
//

#import <WebRTC/WebRTC.h>

@interface WSCAVFoundationVideoSource : RTCVideoSource

- (instancetype)initWithPeerConnectionFactory:(RTCPeerConnectionFactory *)factory;

/** Returns whether rear-facing camera is available for use. */
@property(nonatomic, readonly) BOOL canUseBackCamera;

/** Switches the camera being used (either front or back). */
@property(nonatomic, assign) BOOL useBackCamera;

@property(nonatomic, readonly) RTCVideoTrack *videoTrack;

@property(nonatomic, readonly) RTCVideoSource *videoSource;

@property(nonatomic, readonly) RTCPeerConnectionFactory *peerConnectionFactory;

-(void) startCameraCapture;

-(void) stopRunning;

@end
