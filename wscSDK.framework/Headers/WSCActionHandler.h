/* Copyright (c) 2016, Oracle and/or its affiliates. All rights reserved. */

#import "WSCAction.h"
#import "WSCFrame.h"

/**
 * Handler class for the Action enum.
 */
@interface WSCActionHandler : NSObject

/**
 * Gets Action by value.
 *
 * @param value Action string value
 * @return Action or null if not part of the enumeration
 */
+(WSCAction)getByValue:(NSString *)value;

/**
 * Gets value of an Action.
 *
 * @param action Action
 * @return Action string value
 */
+(NSString *)getValue:(WSCAction)action;


/**
 * Extracts the frame action to the {@link Action} enum.
 *
 * @param frame JSON Frame
 * @return Action enum or null if code not listed in the enumeration
 */
+(WSCAction)getAction:(WSCFrame *)frame;

@end
